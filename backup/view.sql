--DROP VIEW APPS.XXABRL_VENDOR_MASTER_V;

/* Formatted on 2021/08/26 18:02 (Formatter Plus v4.8.8) */
CREATE OR REPLACE FORCE VIEW apps.xxabrl_vendor_master_v (operating_unit,
                                                          vendor_number,
                                                          vendor_name,
                                                          alternate_vendor_name,
                                                          vendor_type,
                                                          primary_vendor_category,
                                                          site_name,
                                                          vendor_site_id,
                                                          address_line_1,
                                                          address_line_2,
                                                          address_line_3,
                                                          address_line_4,
                                                          city,
                                                          state,
                                                          country,
                                                          postal_code,
                                                          bill_to_location,
                                                          ship_to_location,
                                                          pay_group,
                                                          payment_method,
                                                          payment_terms,
                                                          payment_hold_all,
                                                          payment_hold_unmatched,
                                                          payment_hold_unvalidated,
                                                          payment_hold_reason,
                                                          liability_account,
                                                          prepayment_account,
                                                          gst_number,
                                                          tax_region,
                                                          pan_no,
                                                          reporting_code,
                                                          tds_vendor_type,
                                                          contact_name,
                                                          email_address,
                                                          phone_number,
                                                          mobile_number,
                                                          status,
                                                          vendor_bank_name,
                                                          vendor_bank_branch,
                                                          vendor_account_number,
                                                          beneficiary_neft_ifsc_code,
                                                          beneficiary_rtgs_ifsc_code,
                                                          account_type,
                                                          retek_supplier_number,
                                                          email_address_1,
                                                          email_address_2,
                                                          email_address_3,
                                                          organization_id,
                                                          msmed_type,
                                                          date_of_creation,
                                                          date_of_amendment
                                                         )
AS
   SELECT   aa.operating_unit, aa.vendor_number, aa.vendor_name,
            aa.alternate_vendor_name, aa.vendor_type,
            aa.primary_vendor_category, aa.site_name, aa.vendor_site_id,
            aa.address_line_1, aa.address_line_2, aa.address_line_3,
            aa.address_line_4, aa.city city, aa.state, aa.country country,
            aa.postal_code,
--AA.Site_Purpose,
                           aa.bill_to_location, aa.ship_to_location,
            aa.pay_group, aa.payment_method, aa.payment_terms,
            aa.payment_hold_all, aa.payment_hold_unmatched,
            aa.payment_hold_unvalidated, aa.payment_hold_reason,
            aa.liability_account, aa.prepayment_account, aa.gst_number,
            tax_region, aa.pan_no, aa.reporting_code, aa.tds_vendor_type,
            aa.contact_name, aa.email_address, aa.phone_number,
            aa.mobile_number, aa.status, aa.vendor_bank_name,
            aa.vendor_bank_branch, aa.vendor_account_number,
            aa.beneficiary_neft_ifsc_code, aa.beneficiary_rtgs_ifsc_code,
            aa.account_type, aa.retek_supplier_number, aa.email_address_1,
            aa.email_address_2, aa.email_address_3, aa.organization_id,
            aa.msmed_type, aa.date_of_creation, aa.date_of_amendment
       FROM (SELECT hou.NAME operating_unit, po.segment1 vendor_number,
                    po.vendor_name vendor_name,
                    po.vendor_name_alt alternate_vendor_name,
                    po.vendor_type_lookup_code vendor_type,
                    po.global_attribute1 primary_vendor_category,
                    povs.vendor_site_code site_name, povs.vendor_site_id,
                    povs.address_line1 address_line_1,
                    povs.address_line2 address_line_2,
                    povs.address_line3 address_line_3,
                    povs.address_line4 address_line_4, povs.city city,
                    povs.country country,
                    DECODE (   NVL (povs.pay_site_flag, 'N')
                            || ','
                            || NVL (povs.rfq_only_site_flag, 'N')
                            || ','
                            || NVL (povs.purchasing_site_flag, 'N'),
                            'Y,Y,Y', 'Payment,Purchasing,RFQ',
                            'N,Y,N', 'RFQ',
                            'N,N,Y', 'Purchasing',
                            'Y,N,N', 'Payment',
                            'Y,Y,N', 'Payment,RFQ',
                            'N,Y,Y', 'RFQ,Purchasing',
                            'Y,N,Y', 'Payment,Purchasing'
                           ) site_purpose,
                    hr_bill.location_code bill_to_location,
                    hr_ship.location_code ship_to_location,
                    povs.invoice_amount_limit invoice_limit,
                    aptt.tolerance_name invoice_tolerance,
                    DECODE (povs.default_pay_site_id,
                            NULL, povs.vendor_site_code_alt,
                            povs.vendor_site_code
                           ) alternate_pay_site,
                    povs.create_debit_memo_flag create_debit_memo_for_rts,
                    povs.gapless_inv_num_flag gapless_invoice_numbering,
                    po_pay.displayed_field pay_group,
                    iepm.payment_method_code payment_method,
                    apt.NAME payment_terms, povs.terms_date_basis terms_date,
                    povs.pay_date_basis_lookup_code pay_date_basis,
                    povs.payment_priority payment_priority,
                    povs.hold_all_payments_flag payment_hold_all,
                    povs.hold_unmatched_invoices_flag payment_hold_unmatched,
                    povs.hold_future_payments_flag payment_hold_unvalidated,
                    povs.hold_reason payment_hold_reason,
                    DECODE (NVL (povs.bank_charge_bearer, 'N'),
                            'N', 'Supplier/Negotiated',
                            'S', 'Supplier/Standard',
                            'I', 'No',
                            povs.bank_charge_bearer
                           ) deduct_bank_charge,
                    povs.always_take_disc_flag always_take_discount,
                    povs.exclude_freight_from_discount
                                                exclude_freight_from_discount,
                    povs.fob_lookup_code fob,
                    povs.freight_terms_lookup_code freight_terms,
                    povs.shipping_control transport_arrangement,
                    gl_lia.concatenated_segments liability_account,
                    gl_pre.concatenated_segments prepayment_account,
                    gl_bill.concatenated_segments bills_payable_account,
                    apd.distribution_set_name distribution_set,
                    jcvs.cst_reg_no cst_reg_no, jcvs.vat_reg_no vat_reg_no,
                    jcvs.st_reg_no lst_reg_no,
                    jcvs.service_tax_regno st_reg_no, jat.pan_no, jat.tan_no,
                    jcvs.excise_duty_reg_no ed_reg_no,
                    jcvs.ec_code excise_control_code,
                    jat.tds_vendor_type_lookup_code tds_vendor_type,
                    pvc.first_name || ' ' || pvc.last_name contact_name,
                    povs.email_address, povs.phone phone_number,
                    povs.area_code mobile_number,
                    DECODE (SIGN (  TRUNC (SYSDATE)
                                  - NVL (povs.inactive_date, TRUNC (SYSDATE))
                                 ),
                            '0', 'Active',
                            '-1', 'Active',
                            'IN Active'
                           ) status,
                    po_pay.lookup_code, povs.attribute1 navision_code,
                    hou.organization_id, povs.attribute2 vendor_bank_name,
                    povs.attribute3 vendor_bank_branch,
                    povs.attribute4 vendor_account_number,
                    povs.attribute7 beneficiary_neft_ifsc_code,
                    povs.attribute8 beneficiary_rtgs_ifsc_code,
                    povs.attribute9 account_type, povs.zip postal_code,
                    povs.attribute14 retek_supplier_number,
                    povs.attribute11 email_address_1,
                    povs.attribute12 email_address_2,
                    povs.attribute13 email_address_3, povs.state,
                    (SELECT DISTINCT registration_number
                                FROM jai_party_regs jpr,
                                     jai_party_reg_lines jprl
                               WHERE jpr.party_reg_id = jprl.party_reg_id
                                 AND jpr.party_id = po.vendor_id
                                 AND jpr.party_site_id = povs.vendor_site_id
                                 AND jprl.effective_to IS NULL
                                 AND jprl.registration_type_code = 'GSTIN'
                                 AND ROWNUM = 1) gst_number,
                    (SELECT global_attribute10
                       FROM ap_suppliers
                      WHERE vendor_id = po.vendor_id) tax_region,
                    (SELECT DISTINCT reporting_code_description
                                FROM jai_party_regs jpr,
                                     jai_reporting_associations jra
                               WHERE jpr.party_reg_id = jra.entity_id
                                 AND reporting_type_name =
                                                   'ABRL Trade Classification'
                                 AND jpr.party_site_id = povs.vendor_site_id
                                 AND jpr.party_id = po.vendor_id
                                 AND jpr.org_id = hou.organization_id
                                 AND ROWNUM = 1) reporting_code,
                    povs.global_attribute8 msmed_type,
                    TO_CHAR (po.creation_date,
                             'dd-mm-yyyy hh:mi:ss'
                            ) date_of_creation,
                    TO_CHAR (povs.last_update_date,
                             'dd-mm-yyyy hh:mi:ss'
                            ) date_of_amendment
               FROM apps.po_vendors po,
                    apps.po_vendors po_parent,
                    apps.po_vendor_sites_all povs,
                    apps.iby_external_payees_all iby,
                    apps.iby_ext_party_pmt_mthds iepm,
                    apps.po_lookup_codes po_pay,
                    apps.ap_terms apt,
                    apps.po_vendor_contacts pvc,
                    apps.hr_locations_all hr_bill,
                    apps.hr_locations_all hr_ship,
                    apps.ap_tolerance_templates aptt,
                    apps.gl_code_combinations_kfv gl_lia,
                    apps.gl_code_combinations_kfv gl_pre,
                    apps.gl_code_combinations_kfv gl_bill,
                    apps.ap_distribution_sets_all apd,
                    apps.hr_operating_units hou,
                    apps.jai_cmn_vendor_sites jcvs,
                    apps.jai_ap_tds_vendor_hdrs jat
              WHERE 1 = 1
                AND po.vendor_id = povs.vendor_id
                AND po_parent.vendor_id(+) = po.parent_vendor_id
                AND povs.vendor_site_id = iby.supplier_site_id(+)
                AND iby.ext_payee_id = iepm.ext_pmt_party_id(+)
                AND iepm.primary_flag(+) = 'Y'
                AND po_pay.lookup_type(+) = 'PAY GROUP'
                AND povs.pay_group_lookup_code = po_pay.lookup_code(+)
                AND povs.vendor_site_id = jcvs.vendor_site_id(+)
                AND povs.vendor_site_id = jat.vendor_site_id(+)
                AND povs.terms_id = apt.term_id(+)
                AND povs.org_id = hou.organization_id
                AND povs.bill_to_location_id = hr_bill.location_id(+)
                AND povs.ship_to_location_id = hr_ship.location_id(+)
                AND povs.tolerance_id = aptt.tolerance_id(+)
                AND povs.prepay_code_combination_id = gl_pre.code_combination_id(+)
                AND povs.accts_pay_code_combination_id = gl_lia.code_combination_id(+)
                AND povs.future_dated_payment_ccid = gl_bill.code_combination_id(+)
                AND povs.distribution_set_id = apd.distribution_set_id(+)
                AND povs.vendor_site_id = pvc.vendor_site_id(+)) aa
   --WHERE 1 = 1 AND vendor_number = '3000024';
   ORDER BY vendor_number, organization_id;






--GRANT SELECT ON APPS.XXABRL_VENDOR_MASTER_V TO FMS;

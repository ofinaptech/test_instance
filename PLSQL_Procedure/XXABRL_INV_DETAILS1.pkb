CREATE OR REPLACE PACKAGE BODY APPS.xxabrl_inv_details1
IS
   PROCEDURE xxabrl_main_pkg (
      errbuf    OUT      VARCHAR2,
      retcode   OUT      NUMBER,
      p_type    IN       VARCHAR2
   )
   IS
   BEGIN
      IF p_type = 'Daily'
      THEN
         xxabrl_inv_daily_data;
      END IF;

      IF p_type = 'Dump'
      THEN
         xxabrl_inv_dump;
      END IF;
   END xxabrl_main_pkg;

   PROCEDURE xxabrl_inv_daily_data
   IS
   BEGIN
      EXECUTE IMMEDIATE 'truncate table  xxabrliproc.xxabrl_invoice_details';

      FOR buf IN
         (SELECT DISTINCT hou.NAME ou, hou.organization_id org_id,
                          asp.vendor_id, asp.segment1 vendor_code,
                          asp.vendor_name,
--                          (SELECT pan_no
--                             FROM apps.jai_ap_tds_vendor_hdrs
--                            WHERE vendor_id = aia.vendor_id
--                              AND vendor_site_id = aia.vendor_site_id
--                              AND ROWNUM <= 1) vendor_pan,
 (SELECT DISTINCT nvl(registration_number,secondary_registration_number)
                            FROM apps.jai_party_regs jpr,
                                 apps.jai_party_reg_lines jprl
                           WHERE 1 = 1
                             AND jpr.party_reg_id = jprl.party_reg_id
                             AND ROWNUM <= 1
                             AND aia.vendor_id = jpr.party_id
                             AND aia.vendor_site_id = jpr.party_site_id
                            AND (REGISTRATION_TYPE_CODE='PAN'  or  SEC_REGISTRATION_TYPE_CODE = 'PAN') 
                             ) vendor_pan, 
                          assa.vendor_site_id, assa.vendor_site_code,
                          aida.invoice_id, aia.invoice_date, aia.invoice_num,
                          aia.description, aia.doc_sequence_value,
                          aia.invoice_type_lookup_code, aida.accounting_date,
                          aida.invoice_line_number,
                          aida.distribution_line_number,
                          DECODE (aila.line_type_lookup_code,
                                  'ITEM', 'Invoice',
                                  'MISCELLANEOUS', 'Tax'
                                 ) amount_type,
                          ROUND (aida.amount, 14) amount,
                          aida.description dist_description,
                          ap_invoices_pkg.get_approval_status
                                (aia.invoice_id,
                                 aia.invoice_amount,
                                 aia.payment_status_flag,
                                 aia.invoice_type_lookup_code
                                ) approval_status,
                          DECODE (aida.match_status_flag,
                                  'A', 'Validated',
                                  'N', 'Never validated',
                                  'T', 'Tested but not validated'
                                 ) invoice_status,
                          aila.po_header_id, aila.po_line_id,
                          aila.po_line_location_id, aida.po_distribution_id,
                          ROUND (aida.quantity_invoiced,
                                 14) quantity_invoiced,
                          ROUND (aida.unit_price, 14) unit_price,
                          gcc.segment1, gcc.segment2, gcc.segment3,
                          gcc.segment4, gcc.segment5, gcc.segment6,
                          gcc.segment7, gcc.segment8,
                          (SELECT segment6
                             FROM apps.gl_code_combinations_kfv
                            WHERE code_combination_id =
                                     aia.accts_pay_code_combination_id)
                                                               liablity_acct,
                          aida.cancellation_flag, aida.rcv_transaction_id,
                          aida.invoice_distribution_id,
                          aida.line_type_lookup_code, aida.period_name,
                          aida.created_by, aida.creation_date,
                          (SELECT user_name
                             FROM apps.fnd_user
                            WHERE user_id = aida.created_by) created_by_code,
                          (SELECT description
                             FROM apps.fnd_user
                            WHERE user_id = aida.created_by) created_by_name,
                          aida.last_updated_by,
                          (SELECT user_name
                             FROM apps.fnd_user
                            WHERE user_id =
                                     aida.last_updated_by)
                                                        last_updated_by_code,
                          aida.last_update_date,
                          (SELECT description
                             FROM apps.fnd_user
                            WHERE user_id =
                                     aida.last_updated_by)
                                                        last_updated_by_name
                     FROM apps.ap_invoices_all aia,
                          apps.ap_suppliers asp,
                          apps.ap_supplier_sites_all assa,
                          apps.ap_invoice_lines_all aila,
                          apps.ap_invoice_distributions_all aida,
                          apps.gl_code_combinations_kfv gcc,
                          apps.hr_operating_units hou
                    WHERE aia.invoice_id = aila.invoice_id
                      AND aila.invoice_id = aida.invoice_id
                      AND aia.vendor_id = asp.vendor_id
                      AND aia.vendor_site_id = assa.vendor_site_id
                      AND asp.vendor_id = assa.vendor_id
                      AND aia.org_id = hou.organization_id
                      --AND NVL (aila.cancelled_flag, 'N') != 'Y'
                      AND aia.invoice_type_lookup_code = 'STANDARD'
                      AND aila.match_type IN
                                           ('ITEM_TO_RECEIPT', 'NOT_MATCHED')
                      AND asp.vendor_type_lookup_code<>'MERCHANDISE'
                      AND aila.line_type_lookup_code IN
                                                    ('ITEM', 'MISCELLANEOUS')
                      AND aila.po_distribution_id IS NOT NULL
                      AND aila.line_number = aida.invoice_line_number
                      AND aida.dist_code_combination_id =
                                                       gcc.code_combination_id
                      AND TRUNC (aia.creation_date) = TRUNC (SYSDATE) - 1
                  --    AND TRUNC (aida.accounting_date) >= '01-jul-17'
                 -- AND aia.invoice_id = 445087568
          ORDER BY        aida.invoice_id)
      LOOP
         INSERT INTO xxabrliproc.xxabrl_invoice_details
                     (ou, org_id, vendor_id, vendor_code,
                      vendor_name, vendor_pan, vendor_site_id,
                      vendor_site_code, invoice_id,
                      invoice_date, invoice_num, description,
                      doc_sequence_value, invoice_type_lookup_code,
                      accounting_date, invoice_line_number,
                      distribution_line_number, amount_type,
                      amount, dist_description, approval_status,
                      invoice_status, po_header_id, po_line_id,
                      po_line_location_id, po_distribution_id,
                      quantity_invoiced, unit_price, segment1,
                      segment2, segment3, segment4,
                      segment5, segment6, segment7,
                      segment8, liablity_acct,
                      cancellation_flag, rcv_transaction_id,
                      invoice_distribution_id,
                      line_type_lookup_code, period_name,
                      created_by, creation_date,
                      created_by_code, created_by_name,
                      last_updated_by, last_updated_by_code,
                      last_update_date, last_updated_by_name,
                      importdate, process_flag
                     )
              VALUES (buf.ou, buf.org_id, buf.vendor_id, buf.vendor_code,
                      buf.vendor_name, buf.vendor_pan, buf.vendor_site_id,
                      buf.vendor_site_code, buf.invoice_id,
                      buf.invoice_date, buf.invoice_num, buf.description,
                      buf.doc_sequence_value, buf.invoice_type_lookup_code,
                      buf.accounting_date, buf.invoice_line_number,
                      buf.distribution_line_number, buf.amount_type,
                      buf.amount, buf.dist_description, buf.approval_status,
                      buf.invoice_status, buf.po_header_id, buf.po_line_id,
                      buf.po_line_location_id, buf.po_distribution_id,
                      buf.quantity_invoiced, buf.unit_price, buf.segment1,
                      buf.segment2, buf.segment3, buf.segment4,
                      buf.segment5, buf.segment6, buf.segment7,
                      buf.segment8, buf.liablity_acct,
                      buf.cancellation_flag, buf.rcv_transaction_id,
                      buf.invoice_distribution_id,
                      buf.line_type_lookup_code, buf.period_name,
                      buf.created_by, buf.creation_date,
                      buf.created_by_code, buf.created_by_name,
                      buf.last_updated_by, buf.last_updated_by_code,
                      buf.last_update_date, buf.last_updated_by_name,
                      TRUNC (SYSDATE), 'D'
                     );

         COMMIT;
      END LOOP;
   END xxabrl_inv_daily_data;

   PROCEDURE xxabrl_inv_dump
   IS
   BEGIN
      EXECUTE IMMEDIATE 'truncate table  xxabrliproc.xxabrl_invoice_details';

      FOR buf IN
         (SELECT DISTINCT hou.NAME ou, hou.organization_id org_id,
                          asp.vendor_id, asp.segment1 vendor_code,
                          asp.vendor_name,
--                          (SELECT pan_no
--                             FROM apps.jai_ap_tds_vendor_hdrs
--                            WHERE vendor_id = aia.vendor_id
--                              AND vendor_site_id = aia.vendor_site_id
--                              AND ROWNUM <= 1) vendor_pan,
 (SELECT DISTINCT nvl(registration_number,secondary_registration_number)
                            FROM apps.jai_party_regs jpr,
                                 apps.jai_party_reg_lines jprl
                           WHERE 1 = 1
                             AND jpr.party_reg_id = jprl.party_reg_id
                             AND ROWNUM <= 1
                             AND aia.vendor_id = jpr.party_id
                             AND aia.vendor_site_id = jpr.party_site_id
                            AND (REGISTRATION_TYPE_CODE='PAN'  or  SEC_REGISTRATION_TYPE_CODE = 'PAN') 
                             ) vendor_pan, 
                          assa.vendor_site_id, assa.vendor_site_code,
                          aida.invoice_id, aia.invoice_date, aia.invoice_num,
                          aia.description, aia.doc_sequence_value,
                          aia.invoice_type_lookup_code, aida.accounting_date,
                          aida.invoice_line_number,
                          aida.distribution_line_number,
                          DECODE (aila.line_type_lookup_code,
                                  'ITEM', 'Invoice',
                                  'MISCELLANEOUS', 'Tax'
                                 ) amount_type,
                          ROUND (aida.amount, 14) amount,
                          aida.description dist_description,
                          ap_invoices_pkg.get_approval_status
                                (aia.invoice_id,
                                 aia.invoice_amount,
                                 aia.payment_status_flag,
                                 aia.invoice_type_lookup_code
                                ) approval_status,
                          DECODE (aida.match_status_flag,
                                  'A', 'Validated',
                                  'N', 'Never validated',
                                  'T', 'Tested but not validated'
                                 ) invoice_status,
                          aila.po_header_id, aila.po_line_id,
                          aila.po_line_location_id, aida.po_distribution_id,
                          ROUND (aida.quantity_invoiced,
                                 14) quantity_invoiced,
                          ROUND (aida.unit_price, 14) unit_price,
                          gcc.segment1, gcc.segment2, gcc.segment3,
                          gcc.segment4, gcc.segment5, gcc.segment6,
                          gcc.segment7, gcc.segment8,
                          (SELECT segment6
                             FROM apps.gl_code_combinations_kfv
                            WHERE code_combination_id =
                                     aia.accts_pay_code_combination_id)
                                                               liablity_acct,
                          aida.cancellation_flag, aida.rcv_transaction_id,
                          aida.invoice_distribution_id,
                          aida.line_type_lookup_code, aida.period_name,
                          aida.created_by, aida.creation_date,
                          (SELECT user_name
                             FROM apps.fnd_user
                            WHERE user_id = aida.created_by) created_by_code,
                          (SELECT description
                             FROM apps.fnd_user
                            WHERE user_id = aida.created_by) created_by_name,
                          aida.last_updated_by,
                          (SELECT user_name
                             FROM apps.fnd_user
                            WHERE user_id =
                                     aida.last_updated_by)
                                                        last_updated_by_code,
                          aida.last_update_date,
                          (SELECT description
                             FROM apps.fnd_user
                            WHERE user_id =
                                     aida.last_updated_by)
                                                        last_updated_by_name
                     FROM apps.ap_invoices_all aia,
                          apps.ap_suppliers asp,
                          apps.ap_supplier_sites_all assa,
                          apps.ap_invoice_lines_all aila,
                          apps.ap_invoice_distributions_all aida,
                          apps.gl_code_combinations_kfv gcc,
                          apps.hr_operating_units hou
                    WHERE aia.invoice_id = aila.invoice_id
                      AND aila.invoice_id = aida.invoice_id
                      AND aia.vendor_id = asp.vendor_id
                      AND aia.vendor_site_id = assa.vendor_site_id
                      AND asp.vendor_id = assa.vendor_id
                      AND aia.org_id = hou.organization_id
                      --AND NVL (aila.cancelled_flag, 'N') != 'Y'
                      AND aia.invoice_type_lookup_code = 'STANDARD'
                      AND aila.match_type IN
                                           ('ITEM_TO_RECEIPT', 'NOT_MATCHED')
                      AND asp.vendor_type_lookup_code <>'MERCHANDISE'
                      AND aila.line_type_lookup_code IN
                                                    ('ITEM', 'MISCELLANEOUS')
                      AND aila.po_distribution_id IS NOT NULL
                      AND aila.line_number = aida.invoice_line_number
                      AND aida.dist_code_combination_id =
                                                       gcc.code_combination_id
                      AND TRUNC (aida.accounting_date) >= '01-jul-17'
                 -- AND aia.invoice_id = 445087568
          ORDER BY        aida.invoice_id)
      LOOP
         INSERT INTO xxabrliproc.xxabrl_invoice_details
                     (ou, org_id, vendor_id, vendor_code,
                      vendor_name, vendor_pan, vendor_site_id,
                      vendor_site_code, invoice_id,
                      invoice_date, invoice_num, description,
                      doc_sequence_value, invoice_type_lookup_code,
                      accounting_date, invoice_line_number,
                      distribution_line_number, amount_type,
                      amount, dist_description, approval_status,
                      invoice_status, po_header_id, po_line_id,
                      po_line_location_id, po_distribution_id,
                      quantity_invoiced, unit_price, segment1,
                      segment2, segment3, segment4,
                      segment5, segment6, segment7,
                      segment8, liablity_acct,
                      cancellation_flag, rcv_transaction_id,
                      invoice_distribution_id,
                      line_type_lookup_code, period_name,
                      created_by, creation_date,
                      created_by_code, created_by_name,
                      last_updated_by, last_updated_by_code,
                      last_update_date, last_updated_by_name,
                      importdate, process_flag
                     )
              VALUES (buf.ou, buf.org_id, buf.vendor_id, buf.vendor_code,
                      buf.vendor_name, buf.vendor_pan, buf.vendor_site_id,
                      buf.vendor_site_code, buf.invoice_id,
                      buf.invoice_date, buf.invoice_num, buf.description,
                      buf.doc_sequence_value, buf.invoice_type_lookup_code,
                      buf.accounting_date, buf.invoice_line_number,
                      buf.distribution_line_number, buf.amount_type,
                      buf.amount, buf.dist_description, buf.approval_status,
                      buf.invoice_status, buf.po_header_id, buf.po_line_id,
                      buf.po_line_location_id, buf.po_distribution_id,
                      buf.quantity_invoiced, buf.unit_price, buf.segment1,
                      buf.segment2, buf.segment3, buf.segment4,
                      buf.segment5, buf.segment6, buf.segment7,
                      buf.segment8, buf.liablity_acct,
                      buf.cancellation_flag, buf.rcv_transaction_id,
                      buf.invoice_distribution_id,
                      buf.line_type_lookup_code, buf.period_name,
                      buf.created_by, buf.creation_date,
                      buf.created_by_code, buf.created_by_name,
                      buf.last_updated_by, buf.last_updated_by_code,
                      buf.last_update_date, buf.last_updated_by_name,
                      TRUNC (SYSDATE), 'D'
                     );

         COMMIT;
      END LOOP;
   END xxabrl_inv_dump;
END xxabrl_inv_details1; 
/


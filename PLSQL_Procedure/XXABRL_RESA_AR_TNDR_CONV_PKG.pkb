CREATE OR REPLACE PACKAGE BODY APPS.xxabrl_resa_ar_tndr_conv_pkg
IS
   gn_conc_req_id   NUMBER := fnd_global.conc_request_id;
   -- To print the concurrent request id
   gn_user_id       NUMBER := fnd_global.user_id;

/*************************************************************************************************************/
/* Main Procedure calling */
/*************************************************************************************************************/
   PROCEDURE main_proc (
      x_err_buf      OUT      VARCHAR2,
      x_ret_code     OUT      NUMBER,
      p_action       IN       VARCHAR2,
      p_debug_flag   IN       VARCHAR2
   )
   IS
      ln_val_ins_retcode       NUMBER         := 0;
      --variable to return the status
      ln_val_retcode           NUMBER         := 0;
      ln_val_new_retcode       NUMBER         := 0;
      ln_val_ins_new_retcode   NUMBER         := 0;
      v_tot_cnt                NUMBER         := 0;
      v_tot_cnt1               NUMBER         := 0;
      v_tot_cnt2               NUMBER         := 0;
      v_succ_cnt               NUMBER         := 0;
      v_succ_cnt1              NUMBER         := 0;
      v_succ_cnt2              NUMBER         := 0;
      v_fail_cnt               NUMBER         := 0;
      v_fail_cnt1              NUMBER         := 0;
      v_fail_cnt2              NUMBER         := 0;
      v_user_name              VARCHAR2 (100) := NULL;
      v_ret_code               NUMBER;
   BEGIN
      validate_resa (p_debug_flag, v_ret_code);
      insert_resa_counted_amt ('ABRL_RESA', v_ret_code);
      x_ret_code := v_ret_code;

      IF p_action = 'N'
      THEN
         insert_resa ('ABRL_RESA', v_ret_code);
         x_ret_code := v_ret_code;
      END IF;
   END main_proc;

/*************************************************************************************************************/
/* Procedure for Printing Log */
/*************************************************************************************************************/
   PROCEDURE print_log (p_msg IN VARCHAR2, p_debug_flag VARCHAR2)
   AS
   BEGIN
      IF p_debug_flag = 'Y'
      THEN
         fnd_file.put_line (fnd_file.LOG, p_msg);
      END IF;
   END print_log;

   PROCEDURE validate_resa (p_debug_flag IN VARCHAR2, p_ret_code OUT NUMBER)
   AS
      CURSOR cur_val
      IS
         SELECT tt.ROWID, tt.*
           FROM xxabrl_resa_ar_int tt
          WHERE interface_flag IN ('N', 'E')
            AND TO_CHAR (tt.rollup_level_1) <> TO_CHAR (tt.id_type)
            AND TRUNC (tt.accounting_date) >= '1-apr-17'; --added by govind on 27-oct-17

--            AND TRUNC (creation_date) = TRUNC (SYSDATE)

      ---AND TRUNC(ACCOUNTING_DATE) >='08-FEB-2010'
      ---AND ORG_ID = 144;
      --AND TRUNC(ACCOUNTING_DATE) ='04-NOV-2009';
      v_error_str      VARCHAR2 (2000);
      x_error_msg      VARCHAR2 (2000);
      v_valid_cnt      NUMBER          := 0;
      v_total_cnt      NUMBER          := 0;
      v_error_cnt      NUMBER          := 0;
      x_nature         VARCHAR2 (240);
      x_tran_type      VARCHAR2 (240);
      x_description    VARCHAR2 (240);
      x_tran_err_msg   VARCHAR2 (240);
   BEGIN
      /*
      -----========ADDED UPDATE SCRIPT TO UPDATE THE STORE,ACCOUNT NUMBER,SEGMENT4,ROLLUP1 AND ROLLUP2 FOR THE KOVAIPUDUR STORE(As Discussed with Bala sir)

        BEGIN

             update XXABRL_RESA_AR_INT
                set store='1336',account_number='2201336',segment4='2201336', ROLLUP_LEVEL_1 = '1336'
                WHERE 1=1
                AND TRUNC(ACCOUNTING_DATE) > '31-JAN-2011'
            --    AND ((STORE='2041' AND ORG_ID='91' AND ROLLUP_LEVEL_1 <> STORE AND ROLLUP_LEVEL_2 <> STORE)
            AND  ((STORE='2041'  AND ORG_ID='91' AND ROLLUP_LEVEL_1='2041' AND ROLLUP_LEVEL_1 = STORE));
            commit;

            update XXABRL_RESA_AR_INT
            set store='1336',account_number='2201336',segment4='2201336', ROLLUP_LEVEL_2='1336'
            WHERE 1=1
                AND TRUNC(ACCOUNTING_DATE) > '31-JAN-2011'
            AND ((STORE='2041'  AND ORG_ID='91' AND ROLLUP_LEVEL_2='2041' AND ROLLUP_LEVEL_2 = STORE));
            commit;

            update XXABRL_RESA_AR_INT
                set store='1336',account_number='2201336',segment4='2201336'
                WHERE 1=1
                AND TRUNC(ACCOUNTING_DATE) > '31-JAN-2011'
                AND ((STORE='2041' AND ORG_ID='91' AND ROLLUP_LEVEL_1 <> STORE AND ROLLUP_LEVEL_2 <> STORE));
                commit;

        EXCEPTION
        WHEN OTHERS THEN
           Fnd_File.PUT_LINE(fnd_file.LOG,'Exception WHILE UPDATING STAGE-1 FOR STORE CODES LIKE KOVAI PUDUR: '||SQLERRM);
           p_ret_code :=1;
        END; */
      fnd_file.put_line (fnd_file.LOG, '### ReSA Validation Starts ###');

      FOR v_cur_val IN cur_val
      LOOP
         v_error_str := NULL;
         fnd_file.put_line (fnd_file.LOG,
                               'Validating SEQ_NO#('
                            || v_cur_val.s_num
                            || ')  --------------------'
                           );

         --=== Store / Customer Validation
         IF v_cur_val.STORE IS NOT NULL
         THEN
            --  Print_log('sTART OF cUSTOMER vALIDATION: ' , p_debug_flag);
            validate_customer (v_cur_val.STORE,
                               v_cur_val.ROWID,
                               v_cur_val.org_id,
                               x_error_msg
                              );

            -- Print_log('eND OF cUSTOMER vALIDATION: ' , p_debug_flag);
            IF x_error_msg IS NOT NULL
            THEN
               v_error_str := v_error_str || ';' || x_error_msg;
            END IF;
         ELSE
            v_error_str := v_error_str || ';' || 'STORE_CODE is NULL';
         END IF;

         -----===============Derive Transaction Type
         IF v_cur_val.id_type IS NOT NULL
         THEN
            validate_tran_type (p_id_type             => v_cur_val.id_type,
                                p_rollup_level_1      => v_cur_val.rollup_level_1,
                                p_rollup_level_2      => v_cur_val.rollup_level_2,
                                p_nature              => x_nature,
                                p_tran_type           => x_tran_type,
                                p_description         => x_description,
                                p_error_msg           => x_tran_err_msg
                               );

            IF x_tran_err_msg IS NOT NULL
            THEN
               v_error_str := v_error_str || ';' || x_tran_err_msg;
            END IF;
         ELSE
            v_error_str := v_error_str || ';' || 'ID_TYPE is NULL';
         END IF;

         --=== Curr Code Validation
         IF v_cur_val.currency_code IS NOT NULL
         THEN
            validate_currency_code (v_cur_val.currency_code, x_error_msg);

            IF x_error_msg IS NOT NULL
            THEN
               v_error_str := v_error_str || ';' || x_error_msg;
            END IF;
         ELSE
            v_error_str := v_error_str || ';' || 'CURR_CODE is NULL';
         END IF;

         --=== Curr Conv Code Validation
         IF v_cur_val.currency_conversion_type IS NOT NULL
         THEN
            validate_curr_conv_code (v_cur_val.currency_conversion_type,
                                     x_error_msg
                                    );

            IF x_error_msg IS NOT NULL
            THEN
               v_error_str := v_error_str || ';' || x_error_msg;
            END IF;
         ELSE
            v_error_str := v_error_str || ';' || 'CURR_CONV_CODE is NULL';
         END IF;

         --=== CCID Validation
         IF v_cur_val.code_combination_id <> 0
         THEN
            validate_ccid (v_cur_val.code_combination_id,
                           v_cur_val.ROWID,
                           x_error_msg
                          );

            IF x_error_msg IS NOT NULL
            THEN
               v_error_str := v_error_str || ';' || x_error_msg;
            END IF;
         ELSIF v_cur_val.code_combination_id = 0
         THEN
            validate_segments (v_cur_val.segment1,
                               v_cur_val.segment2,
                               v_cur_val.segment3,
                               v_cur_val.segment4,
                               v_cur_val.segment5,
                               v_cur_val.segment6,
                               v_cur_val.segment7,
                               v_cur_val.segment8,
                               v_cur_val.ROWID,
                               x_error_msg
                              );

            IF x_error_msg IS NOT NULL
            THEN
               v_error_str := v_error_str || ';' || x_error_msg;
            END IF;
         ELSE
            v_error_str := v_error_str || ';' || 'CCID is NULL';
         END IF;

---Validation for VAT Regime Accounts due to GST (on 19-jul-17 commented by Govind)
         IF     v_cur_val.segment6 IN ('364026')
            AND v_cur_val.accounting_date > '30-jun-17'
         THEN
            v_error_str :=
                  v_error_str
               || ';'
               || 'VAT Regime Accounts are not accepted in GST Regime for account 364026';
         END IF;

         --=== TRX/GL Date Validation
         IF v_cur_val.accounting_date IS NULL
         THEN
            v_error_str := v_error_str || ';' || 'TRX/GL Date is NULL';
         END IF;

         --=====
         IF v_error_str IS NOT NULL
         THEN
            UPDATE xxabrl_resa_ar_int int_ln
               SET interface_flag = 'E',
                   error_message = v_error_str
             WHERE ROWID = v_cur_val.ROWID;

            print_log ('***Record Error: ' || v_error_str, p_debug_flag);
--        BEGIN
--        UPDATE XXABRL_RESA_AR_INT int_ln
--           SET INTERFACE_FLAG = 'E'
--         WHERE store =v_cur_val.store
--           AND ACCOUNTING_DATE = v_cur_val.ACCOUNTING_DATE;
--           COMMIT;
--        END;
            v_error_cnt := v_error_cnt + 1;
         ELSE
            UPDATE xxabrl_resa_ar_int int_ln
               SET interface_flag = 'V',
                   error_message = v_error_str
             WHERE ROWID = v_cur_val.ROWID;

            print_log ('Record Valid: ' || v_error_str, p_debug_flag);
            v_valid_cnt := v_valid_cnt + 1;
         END IF;

         v_total_cnt := v_total_cnt + 1;
         COMMIT;
      END LOOP;

      fnd_file.put_line
                   (fnd_file.LOG,
                    '========================================================'
                   );
      fnd_file.put_line (fnd_file.LOG, 'Total Records: ' || v_total_cnt);
      fnd_file.put_line (fnd_file.LOG, 'Total Valid: ' || v_valid_cnt);
      fnd_file.put_line (fnd_file.LOG, 'Total Errors: ' || v_error_cnt);
      fnd_file.put_line
                   (fnd_file.LOG,
                    '========================================================'
                   );

      IF v_error_cnt > 0
      THEN
         p_ret_code := 1;
      END IF;

      fnd_file.put_line (fnd_file.LOG, '### ReSA Validation Ends ###');
   END validate_resa;

   --== Validation for Customer /Store
   PROCEDURE validate_customer (
      p_store       IN       VARCHAR2,
      p_rowid                VARCHAR2,
      p_org_id      IN       VARCHAR2,                                --suresh
      p_error_msg   OUT      VARCHAR2
   )
   AS
      v_cust_account_id     NUMBER;
      v_cust_acct_site_id   NUMBER;
      v_account_number      VARCHAR2 (25);
      v_gl_id_rec           VARCHAR2 (25);
      v_store_rece          VARCHAR2 (25);
      v_org_id              VARCHAR2 (25);
      v_organization_code   VARCHAR2 (25);
   BEGIN
      --last four number of Cust-account number
      BEGIN
         /*SELECT cust_account_id, account_number
           INTO v_CUST_ACCOUNT_ID, v_ACCOUNT_NUMBER
           FROM hz_cust_accounts hca
          WHERE REVERSE(SUBSTR(REVERSE(hca.account_number), 1, 4)) = p_store;*/-- following code added by shailesh on 25th aug 2009
          -- Added 2 more conditions customer class code should be STORE and
          -- customer site should be active.
          ---  added 19 -dec-19  because of Retek vs O Fin Location Code Variance
         IF p_store = '3175'
         THEN
            BEGIN
               SELECT hca.cust_account_id, hca.account_number
                 INTO v_cust_account_id, v_account_number
                 FROM hz_cust_accounts hca, hz_cust_acct_sites_all hcac
                WHERE REVERSE (SUBSTR (REVERSE (hca.account_number), 1, 4)) =
                                                                        '1990'
                  --p_store
                  AND customer_class_code = 'STORE'
                  AND hca.cust_account_id = hcac.cust_account_id
                  AND hcac.status = 'A'
                  AND hcac.org_id = p_org_id;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  p_error_msg := 'No store found ' || p_store;
               WHEN TOO_MANY_ROWS
               THEN
                  p_error_msg :=
                              'Multiple Store Receivable  Found A' || p_store;
               WHEN OTHERS
               THEN
                  p_error_msg :=
                     'Exception while getting the store account  ' || p_store;
            END;
         ELSIF p_store = '3177'
         THEN
            BEGIN
               SELECT hca.cust_account_id, hca.account_number
                 INTO v_cust_account_id, v_account_number
                 FROM hz_cust_accounts hca, hz_cust_acct_sites_all hcac
                WHERE REVERSE (SUBSTR (REVERSE (hca.account_number), 1, 4)) =
                                                                        '3094'
                  AND customer_class_code = 'STORE'
                  AND hca.cust_account_id = hcac.cust_account_id
                  AND hcac.status = 'A'
                  AND hcac.org_id = p_org_id;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  p_error_msg := 'No store found ' || p_store;
               WHEN TOO_MANY_ROWS
               THEN
                  p_error_msg :=
                              'Multiple Store Receivable  Found A' || p_store;
               WHEN OTHERS
               THEN
                  p_error_msg :=
                     'Exception while getting the store account  ' || p_store;
            END;
------------------------------------
         ELSIF p_store = '3176'
         THEN
            BEGIN
               SELECT hca.cust_account_id, hca.account_number
                 INTO v_cust_account_id, v_account_number
                 FROM hz_cust_accounts hca, hz_cust_acct_sites_all hcac
                WHERE REVERSE (SUBSTR (REVERSE (hca.account_number), 1, 4)) =
                                                                        '2044'
                  AND customer_class_code = 'STORE'
                  AND hca.cust_account_id = hcac.cust_account_id
                  AND hcac.status = 'A'
                  AND hcac.org_id = p_org_id;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  p_error_msg := 'No store found ' || p_store;
               WHEN TOO_MANY_ROWS
               THEN
                  p_error_msg :=
                              'Multiple Store Receivable  Found A' || p_store;
               WHEN OTHERS
               THEN
                  p_error_msg :=
                     'Exception while getting the store account  ' || p_store;
            END;
------------------------------------
         ELSIF p_store = '3178'
         THEN
            BEGIN
               SELECT hca.cust_account_id, hca.account_number
                 INTO v_cust_account_id, v_account_number
                 FROM hz_cust_accounts hca, hz_cust_acct_sites_all hcac
                WHERE REVERSE (SUBSTR (REVERSE (hca.account_number), 1, 4)) =
                                                                        '1990'
                  AND customer_class_code = 'STORE'
                  AND hca.cust_account_id = hcac.cust_account_id
                  AND hcac.status = 'A'
                  AND hcac.org_id = p_org_id;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  p_error_msg := 'No store found ' || p_store;
               WHEN TOO_MANY_ROWS
               THEN
                  p_error_msg :=
                              'Multiple Store Receivable  Found A' || p_store;
               WHEN OTHERS
               THEN
                  p_error_msg :=
                     'Exception while getting the store account  ' || p_store;
            END;
         ELSE
            BEGIN
               SELECT hca.cust_account_id, hca.account_number
                 INTO v_cust_account_id, v_account_number
                 FROM hz_cust_accounts hca, hz_cust_acct_sites_all hcac
                WHERE REVERSE (SUBSTR (REVERSE (hca.account_number), 1, 4)) =
                                                                       p_store
                  AND customer_class_code = 'STORE'
                  AND hca.cust_account_id = hcac.cust_account_id
                  AND hcac.status = 'A'
                  AND hcac.org_id = p_org_id;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  p_error_msg := 'No store found ' || p_store;
               WHEN TOO_MANY_ROWS
               THEN
                  p_error_msg :=
                              'Multiple Store Receivable  Found A' || p_store;
               WHEN OTHERS
               THEN
                  p_error_msg :=
                     'Exception while getting the store account  ' || p_store;
            END;
         END IF;

         ---  added 19 -dec-19  because of Retek vs O Fin Location Code Variance
         BEGIN
            SELECT hcas.cust_acct_site_id,
                   hcsu.gl_id_rec                --Customer Receivable account
              --, hcas.org_id    --suresh
            INTO   v_cust_acct_site_id,
                   v_gl_id_rec                                     --,v_org_id
              FROM hz_cust_accounts_all hca,
                   hz_cust_acct_sites_all hcas,
                   hz_party_sites hps,
                   hz_cust_site_uses_all hcsu
             WHERE hca.cust_account_id = v_cust_account_id
               AND hca.cust_account_id = hcas.cust_account_id
               AND hcas.org_id = p_org_id
               --Need to validate for all ORG's data ***--suresh
               AND hcas.org_id NOT IN
                        (201) -- A wrong ORG created/ cust site created in 201
               AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id(+)
               AND hcas.party_site_id = hps.party_site_id
               --And hps.Party_Site_Number = xac.OF_Customer_Site_Code
               AND hcsu.site_use_code = 'BILL_TO'
               AND hcas.bill_to_flag = 'P'
               AND hcas.status = 'A'
               AND hcsu.status = 'A';

            -----------added by Naresh  on 24-may-10 to pick only active bill_to_site
            print_log ('Derived v_cust_Acct_site_id ' || v_cust_acct_site_id,
                       'Y'
                      );
            print_log ('Derived v_GL_ID_REC ' || v_gl_id_rec, 'Y');
            print_log ('Derived v_org_id ' || v_org_id, 'Y');

            IF v_gl_id_rec IS NULL
            THEN
               p_error_msg :=
                          'Store Revenue account not defined for ' || p_store;
            ELSE
               BEGIN
                  SELECT segment6
                    INTO v_store_rece
                    FROM gl_code_combinations_kfv
                   WHERE code_combination_id = v_gl_id_rec
                     AND enabled_flag = 'Y';
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     p_error_msg :=
                         'Store Receivable CCID/GL not Found for ' || p_store;
                  WHEN TOO_MANY_ROWS
                  THEN
                     p_error_msg :=
                           'Multiple Store Receivable CCID/GL Found for '
                        || p_store;
                  WHEN OTHERS
                  THEN
                     p_error_msg :=
                           'Exception while deriving  Receivable CCID/GL for '
                        || p_store;
               END;
            END IF;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               p_error_msg := 'Bill_TO Site not Found for ' || p_store;
            WHEN TOO_MANY_ROWS
            THEN
               p_error_msg := 'Multiple Bill_TO Site Found for ' || p_store;
            WHEN OTHERS
            THEN
               p_error_msg :=
                     'Exception while deriving  Bill_TO Site for ' || p_store;
         END;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            p_error_msg := 'Store not defined in Oracle ' || p_store;
         WHEN TOO_MANY_ROWS
         THEN
            p_error_msg := 'Multiple Store defined for ' || p_store;
         WHEN OTHERS
         THEN
            p_error_msg := 'Exception while deriving Store for ' || p_store;
      END;

      print_log (p_error_msg, 'Y');

      --== Find ORG CODE
      BEGIN
         SELECT short_code
           INTO v_organization_code
           FROM hr_operating_units
          WHERE organization_id = p_org_id;              --v_org_id; ---suresh
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            p_error_msg := 'Store Org_ID not defined in Oracle ' || p_store;
         WHEN TOO_MANY_ROWS
         THEN
            p_error_msg := 'Multiple Store Org_ID defined for ' || p_store;
         WHEN OTHERS
         THEN
            p_error_msg :=
                      'Exception while deriving Store Org_ID for ' || p_store;
      END;                                                            --suresh

      --== Updating Derived Customer/Store Data
      IF p_error_msg IS NULL
      THEN
         BEGIN
            UPDATE xxabrl_resa_ar_int int_ln
               SET int_ln.nv_customer_id = v_cust_account_id,
                   account_number = v_account_number,
                   cust_rec_acct = v_store_rece,
                   org_code = v_organization_code
             WHERE ROWID = p_rowid;
         EXCEPTION
            WHEN OTHERS
            THEN
               p_error_msg :=
                      'Exception while derived Store details for ' || p_store;
         END;
      END IF;

      print_log (p_error_msg, 'Y');
   EXCEPTION
      WHEN OTHERS
      THEN
         fnd_file.put_line (fnd_file.LOG,
                            'Exception in Store/Customer validation'
                           );
   END validate_customer;

--==========================================
   PROCEDURE validate_currency_code (
      p_curr_code   IN       VARCHAR2,
      p_error_msg   OUT      VARCHAR2
   )
   AS
      v_fun_curr   VARCHAR2 (240);
   BEGIN
      SELECT currency_code
        INTO v_fun_curr
        FROM gl_sets_of_books
       WHERE set_of_books_id = v_set_of_bks_id;

      p_error_msg := NULL;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         p_error_msg := 'Curr_Code not defined in Oracle ' || p_curr_code;
      WHEN TOO_MANY_ROWS
      THEN
         p_error_msg := 'Multiple Curr_Code defined for ' || p_curr_code;
      WHEN OTHERS
      THEN
         p_error_msg :=
                     'Exception while deriving Curr_Code for ' || p_curr_code;
   END validate_currency_code;

   PROCEDURE validate_curr_conv_code (
      p_curr_conv_code   IN       VARCHAR2,
      p_error_msg        OUT      VARCHAR2
   )
   AS
      v_curr_conv_code   VARCHAR2 (240);
   BEGIN
      SELECT conversion_type
        INTO v_curr_conv_code
        FROM gl_daily_conversion_types
       WHERE UPPER (conversion_type) = UPPER (p_curr_conv_code);

      p_error_msg := NULL;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         p_error_msg :=
                  'Curr_Conv_Code not defined in Oracle ' || p_curr_conv_code;
      WHEN TOO_MANY_ROWS
      THEN
         p_error_msg :=
                   'Multiple Curr_Conv_Code defined for ' || p_curr_conv_code;
      WHEN OTHERS
      THEN
         p_error_msg :=
            'Exception while deriving Curr_Conv_Code for '
            || p_curr_conv_code;
   END validate_curr_conv_code;

   PROCEDURE validate_ccid (
      p_ccid        IN       VARCHAR2,
      p_rowid                VARCHAR2,
      p_error_msg   OUT      VARCHAR2
   )
   AS
      v_ccid       VARCHAR2 (240);
      v_segment1   VARCHAR2 (240);
      v_segment2   VARCHAR2 (240);
      v_segment3   VARCHAR2 (240);
      v_segment4   VARCHAR2 (240);
      v_segment5   VARCHAR2 (240);
      v_segment6   VARCHAR2 (240);
      v_segment7   VARCHAR2 (240);
      v_segment8   VARCHAR2 (240);
   BEGIN
      SELECT code_combination_id, segment1, segment2, segment3, segment4,
             segment5, segment6, segment7, segment8
        INTO v_ccid, v_segment1, v_segment2, v_segment3, v_segment4,
             v_segment5, v_segment6, v_segment7, v_segment8
        FROM gl_code_combinations_kfv
       WHERE code_combination_id = p_ccid AND enabled_flag = 'Y'
                                                                --and char_of_account_id =50328
      ;

      BEGIN
         UPDATE xxabrl_resa_ar_int int_ln
            SET segment1 = v_segment1,
                segment2 = v_segment2,
                segment3 = v_segment3,
                segment4 = v_segment4,
                segment5 = v_segment5,
                segment6 = v_segment6,
                segment7 = v_segment7,
                segment8 = v_segment8
          WHERE ROWID = p_rowid;
      EXCEPTION
         WHEN OTHERS
         THEN
            p_error_msg :=
                  'Exception while updating segments Inv_CCID for ' || p_ccid;
      END;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         p_error_msg := 'CCID not defined in Oracle ' || p_ccid;
      WHEN TOO_MANY_ROWS
      THEN
         p_error_msg := 'Multiple CCID defined for ' || p_ccid;
      WHEN OTHERS
      THEN
         p_error_msg := 'Exception while deriving CCID for ' || p_ccid;
   END validate_ccid;

   -------------- new routine added  getting dynamic ccid  24-May-16------
   PROCEDURE validate_segments (
      p_segment1    IN       VARCHAR2,
      p_segment2    IN       VARCHAR2,
      p_segment3    IN       VARCHAR2,
      p_segment4    IN       VARCHAR2,
      p_segment5    IN       VARCHAR2,
      p_segment6    IN       VARCHAR2,
      p_segment7    IN       VARCHAR2,
      p_segment8    IN       VARCHAR2,
      p_rowid                VARCHAR2,
      p_error_msg   OUT      VARCHAR2
   )
   AS
      v_code_combination_id     NUMBER          := NULL;
      x_concatenated_segments   VARCHAR2 (1000) := NULL;
      v_count_ccid              NUMBER          := 0;
   BEGIN
      v_code_combination_id := 0;
      x_concatenated_segments := NULL;
      v_count_ccid := 0;
      x_concatenated_segments :=
            p_segment1
         || '.'
         || p_segment2
         || '.'
         || p_segment3
         || '.'
         || p_segment4
         || '.'
         || p_segment5
         || '.'
         || p_segment6
         || '.'
         || p_segment7
         || '.'
         || p_segment8;
      fnd_file.put_line (fnd_file.LOG,
                         'x_concatenated_segments=>'
                         || x_concatenated_segments
                        );

      SELECT COUNT (code_combination_id)
        INTO v_count_ccid
        FROM gl_code_combinations
       WHERE 1 = 1
         AND enabled_flag = 'Y'
         AND segment1 = p_segment1
         AND segment2 = p_segment2
         AND segment3 = p_segment3
         AND segment4 = p_segment4
         AND segment5 = p_segment5
         AND segment6 = p_segment6
         AND segment7 = p_segment7
         AND segment8 = p_segment8;

      IF v_count_ccid <> 0
      THEN
         BEGIN
            SELECT code_combination_id
              INTO v_code_combination_id
              FROM gl_code_combinations
             WHERE 1 = 1
               AND enabled_flag = 'Y'
               AND segment1 = p_segment1
               AND segment2 = p_segment2
               AND segment3 = p_segment3
               AND segment4 = p_segment4
               AND segment5 = p_segment5
               AND segment6 = p_segment6
               AND segment7 = p_segment7
               AND segment8 = p_segment8;

            fnd_file.put_line (fnd_file.LOG,
                                  'v_code_combination_id1=>'
                               || v_code_combination_id
                              );
         EXCEPTION
            WHEN OTHERS
            THEN
               p_error_msg :=
                     'Exception in
                     segments=>'
                  || x_concatenated_segments;
         END;
      ELSE
         BEGIN
            SELECT fnd_flex_ext.get_ccid ('SQLGL',
                                          'GL#',
                                          50328,
                                          TO_CHAR (SYSDATE,
                                                   'YYYY/MM/DD HH24:MI:SS'
                                                  ),
                                          x_concatenated_segments
                                         )
              INTO v_code_combination_id
              FROM DUAL;

            fnd_file.put_line (fnd_file.LOG,
                                  'v_code_combination_id2=>'
                               || v_code_combination_id
                              );

            IF v_code_combination_id = 0
            THEN
               p_error_msg :=
                    'Exception invalid segments=>' || x_concatenated_segments;
            END IF;
         END;
      END IF;

      BEGIN
         UPDATE xxabrl_resa_ar_int int_ln
            SET code_combination_id = v_code_combination_id
          WHERE ROWID = p_rowid;
      EXCEPTION
         WHEN OTHERS
         THEN
            p_error_msg :=
                  'Exception while updating CCID for =>'
               || x_concatenated_segments;
      END;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         p_error_msg :=
                     'CCID not defined in Oracle ' || x_concatenated_segments;
      WHEN TOO_MANY_ROWS
      THEN
         p_error_msg :=
                      'Multiple CCID defined for ' || x_concatenated_segments;
      WHEN OTHERS
      THEN
         p_error_msg :=
              'Exception while deriving CCID for ' || x_concatenated_segments;
   END;

   ---==============Validate transaction Type
   PROCEDURE validate_tran_type (
      p_id_type          IN       VARCHAR2,
      p_rollup_level_1   IN       NUMBER,
      p_rollup_level_2   IN       NUMBER,
      p_nature           OUT      VARCHAR2,
      p_tran_type        OUT      VARCHAR2,
      p_description      OUT      VARCHAR2,
      p_error_msg        OUT      VARCHAR2
   )
   AS
      v_roll_up2      VARCHAR2 (240);
      v_nature        VARCHAR2 (240);
      v_tran_type     VARCHAR2 (240);
      v_description   VARCHAR2 (240);
   BEGIN
      print_log ('Derive Tran-Type for ID_TYPE: ' || p_id_type, 'N');
      print_log ('Derive Tran-Type for Roll_Up1: ' || p_rollup_level_1, 'N');
      print_log ('Derive Tran-Type for Roll_Up2: ' || p_rollup_level_2, 'N');

      BEGIN
         SELECT attribute2, attribute3, attribute4, attribute5
           INTO v_roll_up2, v_nature, v_tran_type, v_description
           FROM fnd_lookup_values_vl
          WHERE SYSDATE BETWEEN NVL (start_date_active, SYSDATE)
                            AND NVL (end_date_active, SYSDATE)
            AND enabled_flag = 'Y'
            AND lookup_type = 'RESA_AR_TOTAL_ID_LKP'
            AND attribute_category = 'RESA_AR_TOTAL_ID_LKP'
            AND description = p_id_type
            AND DECODE (attribute1, 'All', p_rollup_level_1, attribute1) =
                                                              p_rollup_level_1
            AND DECODE (attribute2, 'All', p_rollup_level_2, attribute2) =
                                                              p_rollup_level_2;

         print_log ('Derived Nature: ' || v_nature, 'N');
         print_log ('Derived Tran-Type: ' || v_tran_type, 'N');
         print_log ('Derived Desc: ' || v_description, 'N');
         p_nature := v_nature;
         p_tran_type := v_tran_type;
         p_description := v_description;
         RETURN;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            p_error_msg :=
                  'Tran-Type Lookup not defined for '
               || p_id_type
               || '/'
               || p_rollup_level_1;
         WHEN TOO_MANY_ROWS
         THEN
            p_error_msg :=
                  'Multiple Tran-Type Lookup defined for '
               || p_id_type
               || '/'
               || p_rollup_level_1;
         WHEN OTHERS
         THEN
            p_error_msg :=
                  'Exception while deriving Tran-Type Lookup for '
               || p_id_type
               || '/'
               || p_rollup_level_1;
      END;

      print_log (p_error_msg, 'Y');
   END validate_tran_type;

   --== Derive_Tran_Type
   PROCEDURE derive_tran_type (
      p_resa_cur      IN       xxabrl_resa_ar_int%ROWTYPE,
      p_nature        OUT      VARCHAR2,
      p_tran_type     OUT      VARCHAR2,
      p_description   OUT      VARCHAR2,
      p_error_msg     OUT      VARCHAR2
   )
   AS
      v_roll_up2      VARCHAR2 (240);
      v_nature        VARCHAR2 (240);
      v_tran_type     VARCHAR2 (240);
      v_description   VARCHAR2 (240);
   BEGIN
      print_log ('Derive Tran-Type for ID_TYPE: ' || p_resa_cur.id_type, 'N');
      print_log ('Derive Tran-Type for Roll_Up1: '
                 || p_resa_cur.rollup_level_1,
                 'N'
                );

      BEGIN
         SELECT
                --MEANING,
                --DESCRIPTION,
                --ATTRIBUTE1, --<<in where clause
                attribute2, attribute3, attribute4, attribute5
           INTO v_roll_up2, v_nature, v_tran_type, v_description
           FROM fnd_lookup_values_vl
          WHERE SYSDATE BETWEEN NVL (start_date_active, SYSDATE)
                            AND NVL (end_date_active, SYSDATE)
            AND enabled_flag = 'Y'
            AND lookup_type = 'RESA_AR_TOTAL_ID_LKP'
            AND attribute_category = 'RESA_AR_TOTAL_ID_LKP'
            AND description = p_resa_cur.id_type
            AND DECODE (attribute1,
                        'All', p_resa_cur.rollup_level_1,
                        attribute1
                       ) = p_resa_cur.rollup_level_1
            AND DECODE (attribute2,
                        'All', p_resa_cur.rollup_level_2,
                        attribute2
                       ) = p_resa_cur.rollup_level_2
--       AND ATTRIBUTE1 = decode(v_ATTRIBUTE1,'All','All',p_Resa_cur.ROLLUP_LEVEL_1)
--       AND ATTRIBUTE2 = decode(v_ATTRIBUTE2,'All','All',p_Resa_cur.ROLLUP_LEVEL_2)
--       AND ATTRIBUTE2 = decode(DESCRIPTION,'TENDER_TOT','All','ROUNDAMT','All','NETSALES','All',p_Resa_cur.ROLLUP_LEVEL_2)
         ;

         print_log ('Derived Nature: ' || v_nature, 'N');
         print_log ('Derived Tran-Type: ' || v_tran_type, 'N');
         print_log ('Derived Desc: ' || v_description, 'N');
         p_nature := v_nature;
         p_tran_type := v_tran_type;
         p_description := v_description;
         RETURN;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            p_error_msg :=
                  'Tran-Type Lookup not defined for '
               || p_resa_cur.id_type
               || '/'
               || p_resa_cur.rollup_level_1;
         WHEN TOO_MANY_ROWS
         THEN
            p_error_msg :=
                  'Multiple Tran-Type Lookup defined for '
               || p_resa_cur.id_type
               || '/'
               || p_resa_cur.rollup_level_1;
         WHEN OTHERS
         THEN
            p_error_msg :=
                  'Exception while deriving Tran-Type Lookup for '
               || p_resa_cur.id_type
               || '/'
               || p_resa_cur.rollup_level_1;
      END;

      print_log (p_error_msg, 'Y');
   END derive_tran_type;

   PROCEDURE derive_bank_dtl (
      p_store                 IN       xxabrl_resa_ar_int%ROWTYPE,
      p_tran_type             IN       VARCHAR2,
      x_bank_account_name     OUT      VARCHAR2,
      x_bank_account_number   OUT      VARCHAR2,
      x_description           OUT      VARCHAR2,
      x_error_msg             OUT      VARCHAR2
   )
   AS
      v_bank_account_name     VARCHAR2 (240);
      v_bank_account_number   VARCHAR2 (240);
      v_description           VARCHAR2 (240);
   BEGIN
      IF p_tran_type IS NOT NULL
      THEN
         print_log ('Find Bank Details for :' || p_tran_type, 'Y');

         --Print_log('afetr bank details','Y');
         BEGIN
            SELECT description
              INTO v_description
              FROM fnd_flex_values_vl
             WHERE UPPER (flex_value_meaning) = UPPER (p_store.rollup_level_1)
               AND flex_value_set_id =
                      (SELECT flex_value_set_id
                         FROM fnd_flex_value_sets
                        WHERE UPPER (flex_value_set_name) =
                                                      UPPER ('RESA Roll up 1'));

            x_description := v_description;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               x_error_msg :=
                     'Description not Found for Rol_up1'
                  || p_store.rollup_level_1;
            WHEN TOO_MANY_ROWS
            THEN
               x_error_msg :=
                     'Multiple Description Found for Rol_up1'
                  || p_store.rollup_level_1;
            WHEN OTHERS
            THEN
               x_error_msg :=
                     'Exception in Description for Rol_up1'
                  || p_store.rollup_level_1
                  || SQLERRM;
         END;

         --   Print_log('starting the getting bank acct name and number' ,'Y');
         BEGIN
            SELECT
                   --MEANING,
                   --DESCRIPTION,
                   --ATTRIBUTE1,
                   attribute2, attribute3
              INTO v_bank_account_name, v_bank_account_number
              FROM fnd_lookup_values_vl
             WHERE SYSDATE BETWEEN NVL (start_date_active, SYSDATE)
                               AND NVL (end_date_active, SYSDATE)
               AND enabled_flag = 'Y'
               AND lookup_type = 'RESA_AR_BANK_LKP'
               AND attribute_category = 'RESA_AR_BANK_LKP'
               AND attribute1 = p_tran_type
               AND description = p_store.account_number
               AND DECODE (attribute4,
                           NULL, p_store.rollup_level_1,
                           attribute4
                          ) = p_store.rollup_level_1;

            x_bank_account_name := v_bank_account_name;
            x_bank_account_number := v_bank_account_number;
         --   Print_log('Bank account name and account number found....' ,'Y');
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               -- Print_log('Bank account name and account number not found....' ,'Y');
               x_error_msg :=
                     'Bank Acct Name/Number not Found for '
                  || p_store.account_number;
            WHEN TOO_MANY_ROWS
            THEN
               -- Print_log('Bank account name and account number not found....' ,'Y');
               x_error_msg :=
                     'Multiple Bank Acct Name/Number Found for '
                  || p_store.account_number;
            WHEN OTHERS
            THEN
               -- Print_log('Bank account name and account number not found....' ,'Y');
               x_error_msg :=
                     'Exception while deriving Bank Acct Name/Number for '
                  || p_store.account_number;
         END;
      END IF;

      NULL;
   END;

   FUNCTION account_seg_status (p_seg_value IN VARCHAR2, p_seg_desc IN VARCHAR2)
      RETURN VARCHAR2
   IS
      v_count   NUMBER := 0;
   BEGIN
      SELECT COUNT (ffvv.flex_value)
        INTO v_count
        FROM fnd_flex_values_vl ffvv, fnd_flex_value_sets ffvs
       WHERE UPPER (ffvs.flex_value_set_name) = UPPER (p_seg_desc)
         AND ffvs.flex_value_set_id = ffvv.flex_value_set_id
         AND ffvv.flex_value = p_seg_value;

      IF v_count = 1
      THEN
         RETURN NULL;
      ELSE
         RETURN 'Invalid Value';
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 'Invalid Value';
   END account_seg_status;

   PROCEDURE insert_resa (p_source_name VARCHAR2, p_ret_code OUT NUMBER)
   AS
      CURSOR cur_store_code
      IS
         SELECT DISTINCT xrai.account_number, xrai.accounting_date
                    FROM xxabrl_resa_ar_int xrai
                   WHERE 1 = 1 AND TRUNC (xrai.accounting_date) >= '01-Apr-17' ---added by govind on 27th OCT-17
                   AND interface_flag IN ('V')
                ORDER BY xrai.account_number;

      CURSOR cur_ins (p_store_code VARCHAR2, p_acc_date DATE)
      IS
         SELECT tt.*
           FROM xxabrl_resa_ar_int tt
          WHERE cust_rec_acct <> segment6
            AND interface_flag IN ('V')
            AND TO_CHAR (tt.rollup_level_1) <> TO_CHAR (tt.id_type)
            AND TRUNC (tt.accounting_date) >= '01-Apr-17' ---added by govind on 27th OCT-17
            AND account_number = p_store_code
            AND accounting_date = p_acc_date;

      v_interface_line_context        VARCHAR2 (240)  := 'ABRL_RESA';
      v_interface_line_attribute1     VARCHAR2 (240);
      v_interface_line_attribute2     NUMBER          := 0;
      v_batch_source_name             VARCHAR2 (240)  := 'ReSA';
      v_set_of_book_name              VARCHAR2 (240)
                                     := fnd_profile.VALUE ('GL_SET_OF_BKS_ID');
      v_line_type                     VARCHAR2 (240)  := 'LINE';
      v_description                   VARCHAR2 (240)  := NULL;
      --<< from 'Derive_Tran_Type'
      v_currency_code                 VARCHAR2 (240)  := 'INR';
      v_amount                        VARCHAR2 (240)  := 0;
      v_transaction_type              VARCHAR2 (240)  := NULL;
      v_trx_date                      DATE            := NULL;
      v_gl_date                       DATE            := NULL;
      v_orig_system_batch_name        VARCHAR2 (240)  := NULL;
      v_store_name                    VARCHAR2 (240)  := NULL;
      v_orig_system_bill_address_id   VARCHAR2 (240)  := NULL;
      v_conversion_type               VARCHAR2 (240)  := 'Corporate';
      v_conversion_date               DATE            := NULL;
      v_conversion_rate               VARCHAR2 (240)  := NULL;
      v_term_id                       VARCHAR2 (240)  := 'IMMEDIATE';
      v_attribute_category            VARCHAR2 (240)
                                                 := 'ReSA RECEIVABLES INVOICE';
      v_attribute1_tax                VARCHAR2 (240)  := NULL;
      v_operating_unit                VARCHAR2 (240)  := NULL;
      v_org_id                        VARCHAR2 (240)  := NULL;        --suresh
      v_created_by                    VARCHAR2 (240)
                                              := fnd_profile.VALUE ('USER_ID');
      v_creation_date                 DATE            := SYSDATE;
      v_last_updated_by               NUMBER  := fnd_profile.VALUE ('USER_ID');
      v_last_update_date              DATE            := SYSDATE;
      v_amount_dr                     NUMBER          := 0;
      v_counted_amt                   NUMBER          := 0;
      v_receipt_number                VARCHAR2 (30);
      v_trx_type                      VARCHAR2 (30);
--==========================================
      x_nature                        VARCHAR2 (240);
      x_tran_type                     VARCHAR2 (240);
      x_description                   VARCHAR2 (240);
      x_error_msg                     VARCHAR2 (240);
      x_bank_account_name             VARCHAR2 (240);
      x_bank_account_number           VARCHAR2 (240);
      x_bnk_err_msg                   VARCHAR2 (240);
      x_tran_err_msg                  VARCHAR2 (240);
      x_vc_nature                     VARCHAR2 (240);
      v_valid_process                 NUMBER          := 0;
      v_invalid_process               NUMBER          := 0;
      v_s_num                         NUMBER          := -1;
      v_ins_exception                 EXCEPTION;
      v_insert_exception              EXCEPTION;
      v_error_msg                     VARCHAR2 (2400);
      v_er_tender_type                VARCHAR2 (30);
   BEGIN
      SELECT COUNT (1)
        INTO v_valid_process
        FROM apps.xxabrl_resa_ar_int tt
       WHERE cust_rec_acct <> segment6
         AND TRUNC (accounting_date) NOT BETWEEN '01-jul-17' AND '31-jul-17'
         and TRUNC (accounting_date)>='1-apr-17' --added by govind on 27th OCT-17
         --included on 18-aug-17 due to tax correction entries
         AND interface_flag IN ('V', 'P');

      SELECT COUNT (1)
        INTO v_invalid_process
        FROM xxabrl_resa_ar_int tt
       WHERE cust_rec_acct = segment6
         AND TRUNC (accounting_date) NOT BETWEEN '01-jul-17' AND '31-jul-17'
         and TRUNC (accounting_date)>='1-apr-17' --added by govind on 27th OCT-17
         --included on 18-aug-17 due to tax correction entries
         AND interface_flag IN ('V', 'P');

      print_log ('Valid Records for ReSA Invoice/Receipts: '
                 || v_valid_process,
                 'Y'
                );
      print_log (   'In-Valid Records for ReSA Invoice/Receipts: '
                 || v_invalid_process,
                 'Y'
                );

      IF v_valid_process <> v_invalid_process
      THEN
         RAISE v_ins_exception;
      ---commented by govind on 13-jul-17 due to GST Tax Reversal
      END IF;

      fnd_file.put_line (fnd_file.LOG, '### ReSA Data-Population Starts ###');
      --v_INTERFACE_LINE_ATTRIBUTE2:=0; Commtented on 25/03/2010
      print_log ('inserting data', 'Y');

      FOR v_store_code IN cur_store_code
      LOOP
         BEGIN
            FOR v_cur_ins IN cur_ins (v_store_code.account_number,
                                      v_store_code.accounting_date
                                     )
            LOOP
               fnd_file.put_line (fnd_file.LOG,
                                  'Inserting (' || v_cur_ins.s_num || ')'
                                 );

               BEGIN
                  SELECT    TO_CHAR (v_cur_ins.accounting_date, 'DDMMYY')
                         || 'S'
                         || v_cur_ins.STORE         -- << Need to take as date
                                           ,
                         ROUND (v_cur_ins.amount_cr, 2),
                         ROUND (v_cur_ins.amount_dr, 2),
                         ROUND (v_cur_ins.counted_amount, 2),
                         v_cur_ins.accounting_date,
                         v_cur_ins.accounting_date,
                            'ReSA'
                         || TO_CHAR (v_cur_ins.accounting_date, 'DDMMYY'),
                         v_cur_ins.account_number, v_cur_ins.org_code,
                         v_cur_ins.org_id                            ---suresh
                                         , v_cur_ins.s_num
                    INTO v_interface_line_attribute1,
                         v_amount,
                         v_amount_dr,
                         v_counted_amt,
                         v_trx_date,
                         v_gl_date,
                         v_orig_system_batch_name,
                         v_store_name, v_operating_unit,
                         v_org_id                                     --suresh
                                 , v_s_num
                    FROM DUAL;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     print_log
                             (   'Exception in Data-Selection from cursor: '
                              || SQLERRM,
                              'Y'
                             );
               END;

               print_log ('Roll-Up#1: ' || v_cur_ins.rollup_level_1, 'Y');
               print_log ('Roll-Up#2: ' || v_cur_ins.rollup_level_2, 'Y');
               print_log ('Amount CR: ' || v_amount, 'Y');
               print_log ('Amount DR: ' || v_amount_dr, 'Y');
               derive_tran_type (p_resa_cur         => v_cur_ins,
                                 p_nature           => x_nature,
                                 p_tran_type        => x_tran_type,
                                 p_description      => x_description,
                                 p_error_msg        => x_tran_err_msg
                                );
               v_transaction_type := NULL;                           --Mapping
               print_log (':Tran_Type: ' || x_tran_type, 'Y');
               fnd_file.put_line (fnd_file.LOG, ':Line Nature: ' || x_nature);
               print_log (':Derive_Tran_Type Error Msg: ' || x_tran_err_msg,
                          'Y'
                         );

               IF x_nature = 'Transaction' AND (x_tran_err_msg IS NULL)
               THEN
                   --Fnd_File.PUT_LINE(fnd_file.LOG, 'As Transaction ('||v_cur_ins.S_NUM||')');
                  --Insert invoice
                  IF x_tran_type IS NOT NULL
                  THEN
                     BEGIN
                        SELECT TYPE
                          INTO v_trx_type
                          FROM ra_cust_trx_types_all tran,
                               hr_operating_units ood
                         WHERE UPPER (tran.NAME) = UPPER (x_tran_type)
                           AND ood.organization_id = tran.org_id
                           AND ood.short_code = v_cur_ins.org_code;

                        IF v_trx_type = 'CM'
                        THEN
                           --v_AMOUNT := v_AMOUNT_DR * -1;
                           IF v_amount > 0
                           THEN
                              v_amount := v_amount * -1;         --08/12/2008
                           ELSIF v_amount < 0
                           THEN
                              v_amount := ABS (v_amount);
                              null;
                           END IF;
                        END IF;
                     EXCEPTION
                        WHEN NO_DATA_FOUND
                        THEN
                           fnd_file.put_line (fnd_file.LOG,
                                                 'Invalid Transaction Type  '
                                              || x_tran_type
                                             );
                        WHEN TOO_MANY_ROWS
                        THEN
                           fnd_file.put_line
                              (fnd_file.LOG,
                                  'Multiple definition for Transaction Type '
                               || x_tran_type
                              );
                        WHEN OTHERS
                        THEN
                           fnd_file.put_line
                              (fnd_file.LOG,
                                  'Exception while validating Transaction Type '
                               || x_tran_type
                              );
                     END;
                  END IF;

                  --v_INTERFACE_LINE_ATTRIBUTE2 := v_INTERFACE_LINE_ATTRIBUTE2+1;
                  BEGIN
                     SELECT xxabrl_tender_resa_seq.NEXTVAL
                       INTO v_interface_line_attribute2
                       FROM DUAL;
                  END;

                  --   Print_log('type : '||x_Nature,'Y');
                  BEGIN
                     -- Print_log('Inserting Line','Y');
                     INSERT INTO xxabrl_navi_ar_int_line_stg2
                                 
                                 --XXABRL_RESA_RA_INT_LINES_ALL
                     (            interface_line_context,
                                  interface_line_attribute1,
                                  interface_line_attribute2,
                                  batch_source_name,
                                  sob_id,
                                  line_type, description,
                                  currency_code, tender_amount,
                                  amount,
                                  transaction_type, trx_date, gl_date,
                                  orig_system_batch_name, customer_name,
                                  orig_system_bill_address_id,
                                  conversion_type, conversion_date,
                                  conversion_rate, term_id,
                                  attribute_category,
                                  attribute1,
                                  org_code, created_by,
                                  creation_date, last_updated_by,
                                  last_update_date, interfaced_flag,
                                  freeze_flag, org_id,
                                  --suresh
                                  error_message             --to carry Seq_Num
                                 )
                          VALUES (v_interface_line_context,
                                  v_interface_line_attribute1,
                                  v_interface_line_attribute2,
                                  v_batch_source_name,
                                  --'ABRL LEDGER',
                                  NVL ((SELECT NAME
                                          FROM apps.gl_sets_of_books
                                         WHERE set_of_books_id =
                                                  (SELECT set_of_books_id
                                                     FROM hr_operating_units
                                                    WHERE organization_id =
                                                                      v_org_id)),
                                       'ABRL LEDGER'
                                      ),
                                  v_line_type, x_description,
                                  v_currency_code, NVL (v_amount, 0),
                                  NVL (DECODE (v_cur_ins.id_type,
                                               'TENDER', v_counted_amt,
                                               v_amount
                                              ),
                                       0
                                      ),
                                  x_tran_type, v_trx_date, v_gl_date,
                                  v_orig_system_batch_name, v_store_name,
                                  v_orig_system_bill_address_id,
                                  v_conversion_type, v_conversion_date,
                                  v_conversion_rate, v_term_id,
                                  NULL,
                                  --v_ATTRIBUTE_CATEGORY , --Tax
                                  CASE
                                     WHEN LENGTH (v_cur_ins.rollup_level_1) >
                                                                             4
                                        THEN v_cur_ins.rollup_level_1
                                  END,     --attribute1 for ABG rollup_level_1
                                  v_operating_unit, v_created_by,
                                  v_creation_date, v_last_updated_by,
                                  v_last_update_date, 'I',
                                  'N', v_org_id,                      --suresh
                                  v_s_num
                                 );

                     UPDATE xxabrl_resa_ar_int
                        SET interface_flag = 'P'
                      WHERE record_number = v_cur_ins.record_number;

                     ---commit;
                     print_log ('Line Inserted', 'Y');
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        fnd_file.put_line
                           (fnd_file.LOG,
                               'Exception in while inserting data for Invoice-Lines: '
                            || SQLERRM
                           );
                        v_error_msg :=
                              v_error_msg
                           || 'Error Raise while inserting data for ReSA';
                        RAISE v_insert_exception;
                  END;

                  --Insert Distribution
                  BEGIN
                     -- Print_log('Inserting distribution','Y');
                     INSERT INTO xxabrl_navi_ra_int_dist_stg2
                                 
                                 ---XXABRL_RESA_RA_INT_DIST_ALL
                     (            interface_distribution_id,
                                  interface_line_id, interface_line_context,
                                  interface_line_attribute1,
                                  interface_line_attribute2, account_class,
                                  tender_amount,
                                  amount,
                                  org_id, description, code_combination_id,
                                  segment1, segment2,
                                  segment3, segment4,
                                  segment5, segment6,
                                  segment7, segment8,
                                  created_by, creation_date,
                                  last_updated_by, last_update_date,
                                  last_update_login, interfaced_flag,
                                  error_message             --to carry Seq_Num
                                 )
                          VALUES (NULL,
                                  NULL, v_interface_line_context,
                                  v_interface_line_attribute1,
                                  v_interface_line_attribute2, 'REV',
                                  NVL (v_amount, 0),
                                  NVL (DECODE (v_cur_ins.id_type,
                                               'TENDER', v_counted_amt,
                                               v_amount
                                              ),
                                       0
                                      ),                    ----v_COUNTED_AMT,
                                  v_operating_unit,
                                                   ----v_org_id, ---v_OPERATING_UNIT,  --suresh
                                                   x_description, NULL, --CCID
                                  v_cur_ins.segment1, v_cur_ins.segment2,
                                  v_cur_ins.segment3, v_cur_ins.segment4,
                                  v_cur_ins.segment5, v_cur_ins.segment6,
                                  v_cur_ins.segment7, v_cur_ins.segment8,
                                  v_created_by, v_creation_date,
                                  v_last_updated_by, v_last_update_date,
                                  NULL, 'I',
                                  v_s_num
                                 );

                     UPDATE xxabrl_resa_ar_int
                        SET interface_flag = 'P'
                      WHERE record_number = v_cur_ins.record_number;

                     --commit;
                     print_log ('Line-Dist Inserted', 'Y');
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        fnd_file.put_line
                           (fnd_file.LOG,
                               'Exception in while inserting data for Distributions: '
                            || SQLERRM
                           );
                        RAISE v_insert_exception;
                  END;
               ELSIF x_nature = 'Receipt'
               THEN
                  derive_bank_dtl (v_cur_ins,
                                   x_tran_type,
                                   x_bank_account_name,
                                   x_bank_account_number,
                                   x_description,
                                   x_bnk_err_msg
                                  );
                  fnd_file.put_line (fnd_file.LOG,
                                        'Bank derived successfully shailesh '
                                     || x_bnk_err_msg
                                    );

                  BEGIN
                     SELECT b.description
                       INTO v_er_tender_type
                       FROM apps.fnd_flex_value_sets a,
                            apps.fnd_flex_values_vl b
                      WHERE a.flex_value_set_id = b.flex_value_set_id
                        AND flex_value_set_name = 'RESA Roll up 1'
                        AND b.flex_value_meaning =
                                            TO_CHAR (v_cur_ins.rollup_level_1);
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        print_log
                           (   'Exception While Selecting the Tender Type from Value Set'
                            || v_er_tender_type
                            || v_cur_ins.rollup_level_1
                            || SQLERRM,
                            'Y'
                           );
                  END;

                  IF x_bnk_err_msg IS NULL
                  THEN
                     --Insert receipt data
                     BEGIN
                        INSERT INTO xxabrl_navi_ar_receipt_stg2
                                    
                                    ---XXABRL_RESA_AR_RECEIPT_INT
                        (            data_source,
                                     receipt_number,
                                     operating_unit, receipt_date,
                                     deposit_date, gl_date, currency_code,
                                     exchange_rate_type, exchange_rate,
                                     exchange_date, tender_amount,
                                     ---RECEIPT_AMOUNT,
                                     receipt_amount,
                                     er_customer_number, er_dc_code,
                                     comments,
                                     er_tender_type,
                                     sales_transaction_number,
                                     of_customer_number, customer_id,
                                     customer_site_id, receipt_method,
                                     bank_account_name,
                                     bank_account_number, invoice_number,
                                     amount_applied, org_name,
                                     created_by, creation_date,
                                     interfaced_flag, freeze_flag,
                                     error_message          --to carry Seq_Num
                                    )
                             VALUES (v_interface_line_context
--              ,v_INTERFACE_LINE_ATTRIBUTE1||substr(x_Tran_Type,1,19)
                        ,
                                        v_interface_line_attribute1
                                     || SUBSTR (x_description, 1, 19),
                                     v_operating_unit, v_trx_date,
                                     v_trx_date, v_trx_date, 'INR',
                                     NULL, NULL,
                                     NULL, NVL (v_amount_dr, 0)     --v_AMOUNT
                                                               ,
                                     NVL (v_counted_amt, 0),
                                     v_cur_ins.account_number, 'NV_DC_CODE',
                                     'COMMENTS',
                                     v_er_tender_type     ----'NV_TENDER_TYPE'
                                                     ,
                                     v_interface_line_attribute1,
                                     NULL, NULL,
                                     NULL, x_tran_type,
                                     x_bank_account_name,
                                     x_bank_account_number, NULL,
                                     v_amount, v_operating_unit,
                                     v_created_by, v_creation_date,
                                     'I', 'N',
                                     v_s_num
                                    );

                        UPDATE xxabrl_resa_ar_int
                           SET interface_flag = 'P'
                         WHERE record_number = v_cur_ins.record_number;

                        --commit;
                        print_log ('Receipt Inserted', 'Y');
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           fnd_file.put_line
                              (fnd_file.LOG,
                                  'Exception in while inserting data for Receipts: '
                               || SQLERRM
                              );
                           RAISE v_insert_exception;
                     END;

                     NULL;
                  ELSIF x_bnk_err_msg IS NOT NULL
                  THEN
                     v_error_msg :=
                           v_error_msg
                        || 'Error Raise while Bank-Deriving for ReSA ';
                     RAISE v_insert_exception;
                  END IF;
               ELSIF x_tran_err_msg IS NOT NULL
               THEN
                  v_error_msg :=
                        v_error_msg
                     || 'Error Raise while Tran-Deriving for ReSA ';
                  RAISE v_insert_exception;
               END IF;

               fnd_file.put_line
                   (fnd_file.LOG,
                    '--------------------------------------------------------'
                   );
            END LOOP;

            COMMIT;
         -- Below exception added by Urmila on 29-MAY-2013
         EXCEPTION
            WHEN v_ins_exception
            THEN
               fnd_file.put_line
                  (fnd_file.LOG,
                   'Count for Valid_Process / Invalid_Process Records not same'
                  );
               p_ret_code := 1;
            WHEN v_insert_exception
            THEN
               fnd_file.put_line
                              (fnd_file.LOG,
                                  'Error Raise while inserting data for ReSA'
                               || SQLERRM
                              );
               ROLLBACK;
               p_ret_code := 1;
            WHEN OTHERS
            THEN
               fnd_file.put_line (fnd_file.LOG,
                                  'Exception in ReSA Insert: ' || SQLERRM
                                 );
               p_ret_code := 1;
         END;
      END LOOP;                                       -- cursor cur_store_code
   EXCEPTION
      WHEN v_ins_exception
      THEN
         fnd_file.put_line
                (fnd_file.LOG,
                 'Count for Valid_Process / Invalid_Process Records not same'
                );
         p_ret_code := 1;
      WHEN v_insert_exception
      THEN
         fnd_file.put_line (fnd_file.LOG,
                               'Error Raise while inserting data for ReSA'
                            || SQLERRM
                           );
         ROLLBACK;
         p_ret_code := 1;
      WHEN OTHERS
      THEN
         fnd_file.put_line (fnd_file.LOG,
                            'Exception in ReSA Insert: ' || SQLERRM
                           );
         p_ret_code := 1;
   END insert_resa;

   PROCEDURE insert_resa_counted_amt (
      p_source_name         VARCHAR2,
      p_ret_code      OUT   NUMBER
   )
   AS
      CURSOR cur_ins
      IS
         SELECT                                              /*tt.row_num, */
                tt.*,
                DECODE (tt.amount_dr,
                        '0', tt.amount_cr,
                        tt.amount_dr
                       ) counted_amt
           FROM xxabrl_resa_ar_int tt
          WHERE 1 = 1
            --CUST_REC_ACCT <> segment6
            AND interface_flag IN ('N')
            AND TO_CHAR (tt.rollup_level_1) = TO_CHAR (tt.id_type);
   ---AND TRUNC(ACCOUNTING_DATE) >='08-FEB-2010'
   --AND ORG_ID = 144;
   BEGIN
      FOR v_cur_ins IN cur_ins
      LOOP
         /*Fnd_File.PUT_LINE(fnd_file.LOG, 'Inserting Counted Amt('||v_cur_ins.S_NUM||')');
         Print_log('Roll-Up#1: '||v_cur_ins.ROLLUP_LEVEL_1,'Y');
         Print_log('Roll-Up#2: '||v_cur_ins.ROLLUP_LEVEL_2,'Y');
         Print_log('Counted Amount: '||ROUND(v_cur_ins.COUNTED_AMT,2),'Y');

           UPDATE XXABRL_RESA_AR_INT tt
              SET COUNTED_AMOUNT = v_cur_ins.COUNTED_AMT
            WHERE ROLLUP_LEVEL_1 = v_cur_ins.ID_TYPE
              AND ROLLUP_LEVEL_1 = v_cur_ins.ROLLUP_LEVEL_1
              AND to_char(ID_TYPE) <> to_char(ROLLUP_LEVEL_1)
              AND ROLLUP_LEVEL_2 = v_cur_ins.ROLLUP_LEVEL_2
              AND STORE = v_cur_ins.STORE
              AND ACCOUNTING_DATE = v_cur_ins.ACCOUNTING_DATE
              AND INTERFACE_FLAG IN ('V');
           */
         UPDATE xxabrl_resa_ar_int tt
            SET interface_flag = 'V'
          WHERE TO_CHAR (tt.rollup_level_1) = TO_CHAR (tt.id_type)
            AND TRUNC (accounting_date) = v_cur_ins.accounting_date
            AND org_id = v_cur_ins.org_id
            AND interface_flag IN ('N');

         COMMIT;
      END LOOP;
   END;
END xxabrl_resa_ar_tndr_conv_pkg; 
/


CREATE OR REPLACE PACKAGE BODY APPS.xx_vendor_addrs_site_creation
IS
   PROCEDURE xxabrl_vendor_creation (errbuf OUT VARCHAR2, retcode OUT NUMBER)
   IS
      error_flag               VARCHAR2 (100) := NULL;
      xx_vendor_count          NUMBER := 0;
      xx_lookup_code           VARCHAR2 (100);
      xx_term_id               NUMBER;
      xx_vendor_interface_id   NUMBER;
      error1                   VARCHAR (1000) := NULL;
      error2                   VARCHAR (1000) := NULL;
      error3                   VARCHAR (1000) := NULL;

      CURSOR vendor_rec
      IS
         SELECT ROWID, uv.*
           FROM xxabrl.xx_supplier_staging uv
          WHERE status_flag IS NULL;
   BEGIN
      FOR xx_vendor_rec IN vendor_rec
      LOOP
         ---checking vendor name is already exit or not
         BEGIN
            SELECT COUNT (vendor_name)
              INTO xx_vendor_count
              FROM apps.ap_suppliers
             WHERE TRIM (vendor_name) = TRIM (xx_vendor_rec.vendor_name)
                   AND segment1 = xx_vendor_rec.vendor_no;

            IF xx_vendor_count > 0
            THEN
               error1 := 'vendor name already exited';
               error_flag := 'Y';
            ELSE
               error_flag := 'N';
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               error1 := SQLCODE || ': ' || SQLERRM;
               error_flag := 'Y';
         END;

         ---- checking lookup code
         BEGIN
            SELECT lookup_code
              INTO xx_lookup_code
              FROM po_lookup_codes
             WHERE lookup_type(+) = 'VENDOR TYPE'
                   AND UPPER (lookup_code) =
                          UPPER (xx_vendor_rec.vendor_type_lookup_code);
         EXCEPTION
            WHEN OTHERS
            THEN
               error2 :=
                     'lookup_code not found'
                  || ':'
                  || SQLCODE
                  || ': '
                  || SQLERRM;
               error_flag := 'Y';
         END;

         ---fetch the terms id
         BEGIN
            SELECT term_id
              INTO xx_term_id
              FROM apps.ap_terms
             WHERE NAME = xx_vendor_rec.terms_name;
         EXCEPTION
            WHEN OTHERS
            THEN
               error3 :=
                  'terms_id not found' || ':' || SQLCODE || ': ' || SQLERRM;
               error_flag := 'Y';
         END;

         IF error_flag = 'N'
         THEN
            SELECT apps.ap_suppliers_int_s.NEXTVAL
              INTO xx_vendor_interface_id
              FROM DUAL;

            INSERT INTO apps.ap_suppliers_int (vendor_interface_id,
                                               vendor_name,
                                               vendor_name_alt,
                                               segment1,
                                               vendor_type_lookup_code,
                                               terms_id,
                                               terms_name,
                                               global_attribute_category,
                                               global_attribute1,
                                               invoice_currency_code,
                                               payment_currency_code,
                                               creation_date,
                                               last_update_date,
                                               global_attribute13,
                                               global_attribute14)
                 VALUES (xx_vendor_interface_id,
                         xx_vendor_rec.vendor_name,
                         xx_vendor_rec.vendor_name_alt,
                         xx_vendor_rec.vendor_no,
                         xx_vendor_rec.vendor_type_lookup_code,
                         xx_term_id,
                         xx_vendor_rec.terms_name,
                         'ABRL Supplier Additional Info',
                         xx_vendor_rec.primary_vendor_category,
                         xx_vendor_rec.invoice_currency_code,
                         xx_vendor_rec.payment_currency_code,
                         SYSDATE,
                         SYSDATE,
                         xx_vendor_rec.CIN_NUMBER,
                         xx_vendor_rec.FIRM_TYPE );

            COMMIT;

            UPDATE xxabrl.xx_supplier_staging
               SET status_flag = 'Y', error_msg = NULL
             WHERE ROWID = xx_vendor_rec.ROWID;
         ELSE
            UPDATE xxabrl.xx_supplier_staging
               SET status_flag = NULL,
                   error_msg = error1 || ':' || error2 || ':' || error3
             WHERE ROWID = xx_vendor_rec.ROWID;
         END IF;

         error_flag := NULL;
         error1 := NULL;
         error2 := NULL;
         error3 := NULL;
         COMMIT;
      END LOOP;
   END;

   PROCEDURE xx_vendor_location_creation (errbuf    OUT VARCHAR2,
                                          retcode   OUT NUMBER)
   IS
      p_location_rec    hz_location_v2pub.location_rec_type;
      x_location_id     NUMBER;
      x_return_status   VARCHAR2 (2000);
      x_msg_count       NUMBER;
      x_msg_data        VARCHAR2 (2000);
      x_check           NUMBER;
      p_count           NUMBER := 0;
      xx_errbuf         VARCHAR2 (1000);
      xx_retcode        NUMBER;

      CURSOR cur1
      IS
         SELECT ROWID, s.*
           FROM apps.xx_supplier_address_staging s
          WHERE staus_flag IS NULL;
   BEGIN
      FOR i1 IN cur1
      LOOP
         ----create location in hz_locations-----
         p_location_rec.country := i1.country;
         p_location_rec.address1 := RTRIM (i1.address_line_1);
         p_location_rec.address2 := RTRIM (i1.address_line_2);
         p_location_rec.address3 := RTRIM (i1.address_line_3);
         p_location_rec.address4 := RTRIM (i1.address_line_4);
         p_location_rec.city := RTRIM (i1.city);
         p_location_rec.postal_code := i1.postal_code;
         p_location_rec.state := RTRIM (i1.state);
         p_location_rec.province := i1.province;
         p_location_rec.county := i1.county;
         -- P_location_rec.ORIG_SYSTEM_REFERENCE:='357091';
         p_location_rec.created_by_module := 'BO_API';
         ---fnd_message.set_string('Calling the API hz_location_v2pub.create_location');
         ---fnd_message.show;
         --      DBMS_OUTPUT.put_line
         --                         ('Calling the API hz_location_v2pub.create_location');
         hz_location_v2pub.create_location (
            p_init_msg_list   => fnd_api.g_true,
            p_location_rec    => p_location_rec,
            x_location_id     => x_location_id,
            x_return_status   => x_return_status,
            x_msg_count       => x_msg_count,
            x_msg_data        => x_msg_data);
         COMMIT;

         IF (x_return_status <> 'S')
         THEN
            IF x_msg_count > 1
            THEN
               FOR i IN 1 .. x_msg_count
               LOOP
                  fnd_file.put_line (
                     fnd_file.LOG,
                     SUBSTR (fnd_msg_pub.get (p_encoded => fnd_api.g_false),
                             1,
                             255));
               END LOOP;
            END IF;
         ELSE
            UPDATE apps.xx_supplier_address_staging
               SET location_id = x_location_id, staus_flag = 'Y'
             WHERE     retek_supplier = i1.retek_supplier
                   AND supplier_number = i1.supplier_number
                   AND ROWID = i1.ROWID;

            COMMIT;

            BEGIN
               xx_vendor_party_site_creation (xx_errbuf, xx_retcode);
            END;
         END IF;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         fnd_file.put_line (fnd_file.LOG,
                            'error' || SQLCODE || ':' || SQLERRM);
   END xx_vendor_location_creation;

   PROCEDURE xx_vendor_party_site_creation (errbuf    OUT VARCHAR2,
                                            retcode   OUT NUMBER)
   IS
      p_party_site_rec      hz_party_site_v2pub.party_site_rec_type;
      x_party_site_id       NUMBER;
      x_party_site_number   VARCHAR2 (2000);
      v_party_id            NUMBER;
      p_count               NUMBER := 0;
      x_return_status       VARCHAR2 (2000);
      x_msg_count           NUMBER;
      x_msg_data            VARCHAR2 (2000);
      error_flag            VARCHAR2 (100) := NULL;
      v_count               NUMBER;
      error1                VARCHAR2 (2000);
      error2                VARCHAR2 (2000);

      CURSOR cur1
      IS
         SELECT ROWID, s.*
           FROM apps.xx_supplier_address_staging s
          WHERE     staus_flag = 'Y'
                AND location_id IS NOT NULL
                AND party_site_id IS NULL;
   BEGIN
      FOR i1 IN cur1
      LOOP
         --create hz_parties------------
         BEGIN
            SELECT party_id
              INTO v_party_id
              FROM apps.ap_suppliers
             WHERE segment1 = i1.supplier_number;
         EXCEPTION
            WHEN OTHERS
            THEN
               error_flag := 'Y';
               error1 := ' party id error => ' || SQLCODE || ': ' || SQLERRM;
         END;

         SELECT COUNT (*)
           INTO v_count
           FROM apps.hz_party_sites
          WHERE location_id = i1.location_id
                AND party_site_name = i1.address_name;

         IF v_count < 1 AND error_flag IS NULL
         THEN
            p_party_site_rec.party_id := v_party_id;
            p_party_site_rec.location_id := i1.location_id;
            p_party_site_rec.attribute_category := i1.attribute_category;
            p_party_site_rec.attribute1 := i1.tax_region;
            p_party_site_rec.attribute2 := i1.tax_payer_type;
            p_party_site_rec.attribute3 := i1.fsa_number;
            p_party_site_rec.attribute4 := i1.pimary_ho_flag;
            p_party_site_rec.identifying_address_flag := 'Y';
            p_party_site_rec.created_by_module := 'BO_API';
            p_party_site_rec.party_site_name := i1.address_name;
            hz_party_site_v2pub.create_party_site (
               p_init_msg_list       => fnd_api.g_true,
               p_party_site_rec      => p_party_site_rec,
               x_party_site_id       => x_party_site_id,
               x_party_site_number   => x_party_site_number,
               x_return_status       => x_return_status,
               x_msg_count           => x_msg_count,
               x_msg_data            => x_msg_data);
            COMMIT;

            IF (x_return_status <> 'S')
            THEN
               fnd_file.put_line (
                  fnd_file.LOG,
                  'Encountered ERROR in party site creation !!!');
               fnd_file.put_line (fnd_file.LOG,
                                  'fail ' || x_msg_data || x_msg_count);

               --   fnd_file.put_line (fnd_file.log , 'x_vendor_site_id' || x_vendor_site_id);
               IF x_msg_count > 1
               THEN
                  FOR i IN 1 .. x_msg_count
                  LOOP
                     fnd_file.put_line (
                        fnd_file.LOG,
                        SUBSTR (
                           fnd_msg_pub.get (p_encoded => fnd_api.g_false),
                           1,
                           255));
                  END LOOP;
               END IF;
            ELSE
               UPDATE apps.xx_supplier_address_staging xx
                  SET party_site_id = x_party_site_id,
                      staus_flag = 'Y',
                      party_site_flag = 'Y',
                      error_msg = NULL
                WHERE     location_id = i1.location_id
                      AND retek_supplier = i1.retek_supplier
                      AND ROWID = i1.ROWID;

               COMMIT;
            END IF;
         ELSE
            -- error2 := 'location already created for this retek_supplier ';
            fnd_file.put_line (fnd_file.LOG,
                               '--------------rejected----------------');

            UPDATE apps.xx_supplier_address_staging
               SET error_msg = error1 || ':' || error2, staus_flag = 'N'
             WHERE ROWID = i1.ROWID;
         END IF;

         error_flag := NULL;
         error1 := NULL;
         error2 := NULL;
         COMMIT;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         fnd_file.put_line (fnd_file.LOG,
                            'error' || SQLCODE || ':' || SQLERRM);
   END xx_vendor_party_site_creation;

   PROCEDURE xx_vendor_site_creation (errbuf    OUT VARCHAR2,
                                      retcode   OUT NUMBER)
   IS
      v_org_id              NUMBER;
      v_location_id         NUMBER;
      v_party_site_id       NUMBER;
      v_ven_site_int_id     NUMBER;
      l_vendor_id           NUMBER;
      v_party_id            NUMBER;
      count_party_site_id   NUMBER := 0;
      x_count               NUMBER := 0;
      error_falg            VARCHAR2 (1000) := NULL;
      x_check               NUMBER;
      error1                VARCHAR2 (2000);
      error2                VARCHAR2 (2000);
      error3                VARCHAR2 (2000);

      --select * from xx_vendor_site_staging
      CURSOR site_cur
      IS
         SELECT ROWID, s.*
           FROM apps.xx_supplier_sites_staging s
          WHERE status_flag IS NULL;
   BEGIN
      FOR site_rec IN site_cur
      LOOP
         BEGIN
            SELECT vendor_id, party_id
              INTO l_vendor_id, v_party_id
              FROM apps.ap_suppliers
             WHERE segment1 = site_rec.supplier_no;
         EXCEPTION
            WHEN OTHERS
            THEN
               fnd_file.put_line (
                  fnd_file.LOG,
                     'Party Id Error'
                  || 'supplier_no  :'
                  || site_rec.supplier_no);
               error1 :=
                     'vendor id and party id error => '
                  || SQLCODE
                  || ': '
                  || SQLERRM;
               error_falg := 'Y';
         END;

         BEGIN
            SELECT hou.organization_id
              INTO v_org_id
              FROM apps.hr_operating_units hou
             WHERE hou.NAME = TRIM (site_rec.operating_unit);
         EXCEPTION
            WHEN OTHERS
            THEN
               fnd_file.put_line (fnd_file.LOG, 'Org Id Error');
               error2 := 'org id error => ' || SQLCODE || ': ' || SQLERRM;
               error_falg := 'Y';
         END;

         BEGIN
            SELECT COUNT (hl.party_site_id)
              INTO count_party_site_id
              FROM apps.xx_supplier_address_staging hl
             WHERE NVL (UPPER (hl.country), 'dummy') =
                      NVL (UPPER (site_rec.country), 'dummy')
                   AND NVL (UPPER (hl.address_line_1), 'dummy') =
                          NVL (UPPER (site_rec.address_line_1), 'dummy')
                   AND NVL (UPPER (hl.address_line_2), 'dummy') =
                          NVL (UPPER (site_rec.address_line_2), 'dummy')
                   AND NVL (UPPER (hl.address_line_3), 'dummy') =
                          NVL (UPPER (site_rec.address_line_3), 'dummy')
                   AND NVL (UPPER (hl.address_line_4), 'dummy') =
                          NVL (UPPER (site_rec.address_line_4), 'dummy')
                   AND NVL (UPPER (hl.city), 'dummy') =
                          NVL (UPPER (site_rec.city), 'dummy')
                   AND NVL (UPPER (hl.state), 'dummy') =
                          NVL (UPPER (site_rec.state), 'dummy')
                   AND NVL (UPPER (hl.postal_code), 'dummy') =
                          NVL (UPPER (site_rec.postal_code), 'dummy')
                   AND UPPER (hl.address_name) =
                          UPPER (site_rec.address_name)
                   AND hl.supplier_number = site_rec.supplier_no
                   AND hl.retek_supplier = site_rec.retek_supplier_no;
         END;

         IF count_party_site_id > 0
         THEN
            BEGIN
               SELECT MAX (hl.location_id), MAX (hl.party_site_id)
                 INTO v_location_id, v_party_site_id
                 FROM apps.xx_supplier_address_staging hl
                WHERE NVL (UPPER (hl.country), 'dummy') =
                         NVL (UPPER (site_rec.country), 'dummy')
                      AND NVL (UPPER (hl.address_line_1), 'dummy') =
                             NVL (UPPER (site_rec.address_line_1), 'dummy')
                      AND NVL (UPPER (hl.address_line_2), 'dummy') =
                             NVL (UPPER (site_rec.address_line_2), 'dummy')
                      AND NVL (UPPER (hl.address_line_3), 'dummy') =
                             NVL (UPPER (site_rec.address_line_3), 'dummy')
                      AND NVL (UPPER (hl.address_line_4), 'dummy') =
                             NVL (UPPER (site_rec.address_line_4), 'dummy')
                      AND NVL (UPPER (hl.city), 'dummy') =
                             NVL (UPPER (site_rec.city), 'dummy')
                      AND NVL (UPPER (hl.state), 'dummy') =
                             NVL (UPPER (site_rec.state), 'dummy')
                      AND NVL (UPPER (hl.postal_code), 'dummy') =
                             NVL (UPPER (site_rec.postal_code), 'dummy')
                      AND UPPER (hl.address_name) =
                             UPPER (site_rec.address_name)
                      AND hl.supplier_number = site_rec.supplier_no
                      AND hl.retek_supplier = site_rec.retek_supplier_no;
            --         EXCEPTION
            --            WHEN OTHERS
            --            THEN
            --               fnd_file.put_line (fnd_file.LOG,
            --                                     'Location Id Error : '
            --                                  || site_rec.supplier_no
            --                                 );
            --               error3 := 'Location Id Error => ' || SQLCODE || ':' || SQLERRM;
            --               error_falg := 'Y';
            END;
         ELSE
            -- trying to find out whether Party Site Name already exist in (hz_party_sites) and corresponding address 1 2 3... in (hz_locations)
            -- if no rows exist then Address to be created 1st and then load sites
            BEGIN
               SELECT MAX (hl.location_id), MAX (hps.party_site_id)
                 INTO v_location_id, v_party_site_id
                 FROM apps.hz_locations hl,
                      apps.hz_party_sites hps,
                      apps.po_vendors pv
                WHERE NVL (UPPER (hl.country), 'dummy') =
                         NVL (UPPER (site_rec.country), 'dummy')
                      AND NVL (UPPER (hl.address1), 'dummy') =
                             NVL (UPPER (site_rec.address_line_1), 'dummy')
                      AND NVL (UPPER (hl.address2), 'dummy') =
                             NVL (UPPER (site_rec.address_line_2), 'dummy')
                      AND NVL (UPPER (hl.address3), 'dummy') =
                             NVL (UPPER (site_rec.address_line_3), 'dummy')
                      AND NVL (UPPER (hl.address4), 'dummy') =
                             NVL (UPPER (site_rec.address_line_4), 'dummy')
                      AND NVL (UPPER (hl.city), 'dummy') =
                             NVL (UPPER (site_rec.city), 'dummy')
                      AND NVL (UPPER (hl.state), 'dummy') =
                             NVL (UPPER (site_rec.state), 'dummy')
                      AND NVL (UPPER (hl.postal_code), 'dummy') =
                             NVL (UPPER (site_rec.postal_code), 'dummy')
                      --               AND NVL (UPPER (hl.province), 'dummy') =
                      --                                                   NVL (UPPER (NULL), 'dummy')
                      AND NVL (UPPER (hl.county), 'dummy') =
                             NVL (UPPER (NULL), 'dummy')
                      --               AND NVL (UPPER (fl.nls_language), 'dummy') =
                      --                                                   NVL (UPPER (NULL), 'dummy')
                      --               AND NVL (UPPER (hl.address_style), 'dummy') =
                      --                                                   NVL (UPPER (NULL), 'dummy')
                      AND NOT EXISTS
                                 (SELECT party_site_id
                                    FROM apps.ap_supplier_sites_all s
                                   WHERE s.vendor_id = hps.party_id
                                         AND s.party_site_id =
                                                hps.party_site_id
                                         AND s.org_id = v_org_id)
                      --                  AND NOT EXISTS (
                      --                         SELECT party_site_id
                      --                           FROM apps.xx_supplier_sites_staging s
                      --                          WHERE party_site_id = hps.party_site_id
                      --                            AND operating_unit = site_rec.operating_unit)
                      AND hl.location_id = hps.location_id
                      AND hps.party_id = pv.party_id
                      AND pv.vendor_id = l_vendor_id
                      AND UPPER (hps.party_site_name) =
                             UPPER (site_rec.address_name);

               IF v_party_site_id IS NULL
               THEN
                  error3 :=
                     'party_site_id not found and location_id not found';
                  error_falg := 'Y';
               ELSE
                  fnd_file.put_line (fnd_file.LOG,
                                     'v_party_site_id' || v_party_site_id);
                  fnd_file.put_line (fnd_file.LOG,
                                     'v_location_id' || v_location_id);
               END IF;
            --         EXCEPTION
            --            WHEN OTHERS
            --            THEN
            --               fnd_file.put_line (fnd_file.LOG,
            --                                     'Location Id Error : '
            --                                  || site_rec.supplier_no
            --                                 );
            --               error3 := 'Location Id Error => ' || SQLCODE || ':' || SQLERRM;
            --               error_falg := 'Y';
            END;
         END IF;

         BEGIN
            SELECT apps.ap_supplier_sites_int_s.NEXTVAL
              INTO v_ven_site_int_id
              FROM DUAL;
         EXCEPTION
            WHEN OTHERS
            THEN
               fnd_file.put_line (fnd_file.LOG, 'VENDOR SITE INT Id Error');
               error_falg := 'Y';
         END;

         BEGIN
            IF error_falg = 'Y'
            THEN
               UPDATE apps.xx_supplier_sites_staging
                  --xx_vendor_site_stag_rec
                  SET error_msg = error1 || ':' || error2 || ':' || error3
                WHERE ROWID = site_rec.ROWID;

               fnd_file.put_line (fnd_file.LOG, 'rejected');
            ELSE
               INSERT
                 INTO apps.ap_supplier_sites_int (vendor_site_interface_id,
                                                  vendor_id,
                                                  vendor_site_code,
                                                  address_line1,
                                                  address_line2,
                                                  address_line3,
                                                  address_line4,
                                                  city,
                                                  state,
                                                  country,
                                                  zip,
                                                  attribute5,
                                                  attribute6,
                                                  area_code,
                                                  phone,
                                                  fax_area_code,
                                                  fax,
                                                  payment_method_lookup_code,
                                                  terms_date_basis,
                                                  terms_name,
                                                  purchasing_site_flag,
                                                  pay_site_flag,
                                                  org_id,
                                                  operating_unit_name,
                                                  attribute_category,
                                                  attribute14,
                                                  attribute15,
                                                  party_site_id,
                                                  location_id,
                                                  creation_date,
                                                  attribute8,
                                                  attribute7,
                                                  global_attribute2,
                                                  global_attribute5,
                                                  global_attribute6,
                                                  global_attribute7,
                                                  global_attribute8,
                                                  global_attribute_category,
                                                  -----Addded  by Lokesh Poojari on 11-May-2020
                                                  attribute1,
                                                  attribute2,
                                                  attribute3,
                                                  attribute4,
                                                  attribute9,
                                                  attribute10,
                                                  attribute11,
                                                  attribute12,
                                                  attribute13,
                                                  vendor_site_code_alt,
                                                  pay_group_lookup_code,
                                                  match_option,
                                                  invoice_currency_code,
                                                  hold_all_payments_flag,
                                                  hold_unmatched_invoices_flag,
                                                  hold_future_payments_flag,
                                                  hold_reason)
               VALUES (v_ven_site_int_id,
                       l_vendor_id,
                       SUBSTR (site_rec.new_site_name, 1, 15),
                       RTRIM (site_rec.address_line_1),
                       RTRIM (site_rec.address_line_2),
                       RTRIM (site_rec.address_line_3),
                       RTRIM (site_rec.address_line_4),
                       RTRIM (site_rec.city),
                       RTRIM (site_rec.state),
                       site_rec.country,
                       site_rec.postal_code,
                       site_rec.contact_first_name,
                       site_rec.contact_last_name,
                       site_rec.phone_area_code,
                       site_rec.phone_number,
                       site_rec.fax_area_code,
                       site_rec.fax_number,
                       site_rec.payment_methode,
                       site_rec.term_basis,
                       site_rec.header_payment_terms,
                       site_rec.purchase_site_flag,
                       site_rec.pay_site_flag,
                       v_org_id,
                       site_rec.operating_unit,
                       site_rec.attribute_category,
                       site_rec.retek_supplier_no,
                       NULL,
                       v_party_site_id,
                       -- site_rec.party_site_id,        --
                       -- site_rec.location_id,
                       v_location_id,
                       SYSDATE,
                       site_rec.rtgs_ifsc_code,
                       site_rec.neft_ifsc_code,
                       site_rec.mic_code,
                       site_rec.apmc_registration,
                       site_rec.drug_license_number,
                       site_rec.liquor_license_number,
                       site_rec.msmed,
                       site_rec.global_attribute_category,
                       -----Addded  by Lokesh Poojari on 11-May-2020
                       site_rec.principal_vendor,
                       site_rec.vendor_bank_name,
                       site_rec.vendor_bank_branch_name,
                       site_rec.vendor_account_number,
                       site_rec.account_type,
                       site_rec.payable_location,
                       site_rec.email_id1,
                       site_rec.email_id2,
                       site_rec.email_id3,
                       SUBSTR (site_rec.new_site_name, 1, 15),
                       site_rec.pay_group_lookup_code,
                       site_rec.invoice_match_option,
                       site_rec.invoice_currency,
                       site_rec.hold_payments_all_invoices,
                       site_rec.hold_payments_unmatched_inv,
                       site_rec.hold_payments_unvalidated_inv,
                       site_rec.purchase_hold_reason);

               COMMIT;

               UPDATE apps.xx_supplier_sites_staging
                  SET status_flag = 'Y',
                      error_msg = NULL,
                      location_id = v_location_id,
                      party_site_id = v_party_site_id
                WHERE     1 = 1
                      AND ROWID = site_rec.ROWID
                      AND supplier_no = site_rec.supplier_no
                      AND operating_unit = site_rec.operating_unit
                      AND retek_supplier_no = site_rec.retek_supplier_no;

               COMMIT;
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               fnd_file.put_line (fnd_file.LOG, 'Error in Insertion');
         END;

         error_falg := NULL;
         error1 := NULL;
         error2 := NULL;
         error3 := NULL;
         count_party_site_id := 0;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         fnd_file.put_line (fnd_file.LOG,
                            'error' || SQLCODE || ':' || SQLERRM);
   END xx_vendor_site_creation;
END xx_vendor_addrs_site_creation; 
/


CREATE OR REPLACE PACKAGE APPS.gst_abrl_nav_ar_bkdt_load_pkg
IS
   PROCEDURE xxabrl_bak_dt_load_prc (errbuf OUT VARCHAR2, retcode OUT NUMBER);

   PROCEDURE xxabrl_wait_for_req_prc (p_request_id NUMBER, p_req_name VARCHAR2);

   PROCEDURE xxabrl_resa_ar_dt_load_prc (
      errbuf    OUT   VARCHAR2,
      retcode   OUT   NUMBER
   );

   PROCEDURE xxabrl_reim_ap_dt_load_prc (
      errbuf    OUT   VARCHAR2,
      retcode   OUT   NUMBER
   );

   PROCEDURE xxabrl_is_resa_ar_dt_load_prc (
      errbuf    OUT   VARCHAR2,
      retcode   OUT   NUMBER
   );

   PROCEDURE xxabrl_resa_ar_bef_freezing (
      errbuf    OUT   VARCHAR2,
      retcode   OUT   NUMBER
   );
END  ; 
/


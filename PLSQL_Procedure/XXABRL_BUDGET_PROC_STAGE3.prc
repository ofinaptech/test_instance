CREATE OR REPLACE procedure APPS.XXABRL_BUDGET_PROC_STAGE3(ERRBUF  OUT VARCHAR2,
                                                RETCODE OUT VARCHAR2,
                                               V_MONTH NUMBER ) AS

/**********************************************************************************************************************************************
                        WIPRO Infotech Ltd, Mumbai, India

            Name: ABRL Budget Report Stage3 Loader

            Change Record:
           =========================================================================================================================
           Version   Date            Author                    Remarks                  Documnet Ref
           =======   ==========    =============             ==================  =====================================================
           1.0.0      01-Apr-13     Sravan Kumar                Initial Version
           
  ************************************************************************************************************************************************/

CURSOR CUR_STG2 IS select 
       stg2.MONTH,
       null MTD,
       stg2.FORMAT,
       --dec.MTD_YTD,
       stg2.budget_code,
       stg2.budget_description,
       stg2.COMPANY,
       stg2.SBU,
       stg2.SBU_DESCRIPTION,
       stg2.LOCATION,
       stg2.LOCATION_DESCRIPTION,
       stg2.GL_ACCOUNT,
       stg2.ACCOUNT_DESCRIPTION,
       stg2.OPEN_PO,
       stg2.OPEN_PR,
       stg2.GL_BALANCE ,--'DEC_GLB',
       stg2.actual_amount,-- 'DEC_AA',
       stg2.BUDGET_AMOUNT ,--'DEC_BA',
       stg2.BALANCE,
       stg2.VARIANCE,
      NOV.GL_BALANCE  NOV_GLB,
      NOV.ACTUAL_AMOUNT NOV_AA,
     -- NOV.BUDEGET_AMOUNT NOV_BA,
     (stg2.OPEN_PO - NOV.OPEN_PO)MTD_OPEN_PO,
     (stg2.OPEN_PR - NOV.OPEN_PR ) MTD_OPEN_PR,
     (stg2.GL_BALANCE-NOV.GL_BALANCE) MTD_GL_BALANCE,
       (stg2.actual_amount-NOV.actual_amount) MTD_ACTUAL_AMOUNT,
       (stg2.BUDGET_AMOUNT-nov.BUDGET_AMOUNT)MTD_BUDGET_AMOUNT,
       (STG2.LOCATION_BUDGET-NOV.LOCATION_BUDGET)MTD_LOCATION_BUDGET,
       (STG2.LOCATION_BUDGET_DIST-NOV.LOCATION_BUDGET_DIST)MTD_LOCATION_BUDGET_DIST
from XXABRL.XXABRL_BUDGET_REPORT_STAGE2 stg2,
     (select COMPANY,SBU,LOCATION,GL_ACCOUNT,GL_BALANCE,ACTUAL_AMOUNt,BUDGET_AMOUNT,LOCATION_BUDGET,LOCATION_BUDGET_DIST,OPEN_PO,OPEN_PR  
     from XXABRL.XXABRL_BUDGET_REPORT_STAGE2 where budget_code='OP13SMME0001'and  month=V_MONTH-1)NOV
where stg2.month=V_MONTH
and stg2.budget_code='OP13SMME0001'
AND stg2.COMPANY = NOV.COMPANY
and stg2.sbu=nov.sbu
and   stg2.location=nov.location
and   stg2.gl_account=nov.gl_account;
--and   dec.sbu='562'
--and   dec.location='8001671'
--and   dec.gl_account='514002'
 BEGIN
  
        DELETE FROM XXABRL.XXABRL_BUDGET_REPORT_STAGE3;
       COMMIT;
  
    begin
     
     FOR REC_BUDGET3 IN CUR_STG2
      LOOP
     INSERT INTO XXABRL.XXABRL_BUDGET_REPORT_STAGE3
     VALUES('2013',
             REC_BUDGET3.MONTH,
             'MTD',
             REC_BUDGET3.FORMAT,
             REC_BUDGET3.budget_code,
             REC_BUDGET3.budget_description,
             REC_BUDGET3.COMPANY,
             REC_BUDGET3.SBU,
             REC_BUDGET3.SBU_DESCRIPTION,
             REC_BUDGET3.LOCATION,
             REC_BUDGET3.LOCATION_DESCRIPTION,
             REC_BUDGET3.GL_ACCOUNT,
             REC_BUDGET3.ACCOUNT_DESCRIPTION,
             REC_BUDGET3.MTD_OPEN_PO,
             REC_BUDGET3.MTD_OPEN_PR,
             REC_BUDGET3.MTD_GL_BALANCE,
             REC_BUDGET3.MTD_ACTUAL_AMOUNT,
             REC_BUDGET3.MTD_BUDGET_AMOUNT,
             NULL,
             NULL,
             REC_BUDGET3.MTD_LOCATION_BUDGET,
             REC_BUDGET3.MTD_LOCATION_BUDGET_DIST
             
             
      );
     
     END LOOP;
     COMMIT;
     END;  
 
 END XXABRL_BUDGET_PROC_STAGE3; 
/


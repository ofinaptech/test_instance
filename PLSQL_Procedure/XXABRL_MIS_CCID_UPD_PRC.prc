CREATE OR REPLACE PROCEDURE APPS.XXABRL_MIS_CCID_UPD_PRC(ERRBUF  OUT VARCHAR2,
                                                    RETCODE OUT VARCHAR2) AS
 /**********************************************************************************************************************************************
                        WIPRO Infotech Ltd, Mumbai, India
            Name: ABRL MIS CCID Update Program
            Change Record:
           =========================================================================================================================
           Version   Date          Author                    Remarks                  Documnet Ref
           =======   ==========   =============             ==================  =====================================================
           1.0.0     04-Sep-2012   Amresh Kumar Chutke      Initial Version
  ************************************************************************************************************************************************/
CURSOR C_MIS_CCID IS
        SELECT GCC.ROWID, GCC.CODE_COMBINATION_ID,GCC.CONCATENATED_SEGMENTS,GCC.START_DATE_ACTIVE,GCC.END_DATE_ACTIVE
        FROM GL_CODE_COMBINATIONS_KFV GCC ,XXABRL_MIS_CCID XMC
            WHERE 1=1
            AND GCC.CONCATENATED_SEGMENTS=XMC.CONCATENATED_SEGMENTS
            AND XMC.INTERFACE_STATUS ='N';
--            WHERE CONCATENATED_SEGMENTS='11.000.102.0000421.00.361905.000.0000' --4856485
--            AND CODE_COMBINATION_ID=4856485;
 BEGIN
    FOR R_MIS_CCID IN C_MIS_CCID
    LOOP
        BEGIN
        UPDATE XXABRL_MIS_CCID XMC
        SET XMC.CCID = R_MIS_CCID.CODE_COMBINATION_ID,
            XMC.CCID_START_DATE = R_MIS_CCID.START_DATE_ACTIVE,
            XMC.CCID_END_DATE = R_MIS_CCID.END_DATE_ACTIVE,
            XMC.INTERFACE_STATUS = 'P',
            XMC.LAST_UPDATE_DATE = SYSDATE
        WHERE XMC.CONCATENATED_SEGMENTS= R_MIS_CCID.CONCATENATED_SEGMENTS;
        COMMIT;
        EXCEPTION
                    WHEN OTHERS THEN
                    UPDATE XXABRL_MIS_CCID XMC
                    SET XMC.INTERFACE_STATUS = 'E'
                    WHERE XMC.CONCATENATED_SEGMENTS = R_MIS_CCID.CONCATENATED_SEGMENTS;
                DBMS_OUTPUT.PUT_LINE('Unable to Update Stage Table' );
        END;
    END LOOP;
 END XXABRL_MIS_CCID_UPD_PRC; 
/


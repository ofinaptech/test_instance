CREATE OR REPLACE PROCEDURE APPS.xxmrl_pr_out_gen (
   errbuf           OUT VARCHAR2,
   retcode          OUT VARCHAR2,
   p_from_date   IN     VARCHAR2)
AS
   P_RUN_DATE          VARCHAR2 (11)
      := TO_CHAR (TO_DATE (SUBSTR (p_from_date, 1, 10), 'RRRR/MM/DD'),
                  'DD-MON-YYYY');
   g_conc_request_id   NUMBER := fnd_profile.VALUE ('CONC_REQUEST_ID');

   CURSOR pr_out
   IS
        SELECT ou_name,
               vendor_num,
               vendor_name,
               vendor_site,
               pr_ref_num,
               pr_date,
               pr_num,
               pr_item_num,
               pr_item_desc,
               pr_qty,
               pr_amnt,
               po_date,
               po_num,
               po_item_num,
               po_item_desc,
               po_qty,
               po_base_amnt,
               po_tax_amnt,
               po_base_amnt + po_tax_amnt po_tot_amt,
               grn_date,
               grn_num,
               grn_item_num,
               grn_item_desc,
               grn_qty,
               grn_base_amnt,
               grn_tax_amnt,
               grn_base_amnt + grn_tax_amnt grn_tot_amt,
               invoice_num,
               invoice_doc_num,
               invoice_base_amnt,
               invoice_tax_amnt,
               invoice_base_amnt + invoice_tax_amnt invoice_tot_amnt,
               invoice_date,
               invoice_gl_date,
               pay_doc_num,
               payment_amnt,
               payment_date,
               outbound_date
          FROM (SELECT DISTINCT
                       hou.NAME ou_name,
                       NULL vendor_num,
                       NULL vendor_name,
                       NULL vendor_site,
                       NULL pr_ref_num,
                       TRUNC (prh.creation_date) pr_date,
                       prh.segment1 pr_num,
                       (SELECT DISTINCT segment1
                          FROM apps.mtl_system_items_b msib
                         WHERE 1 = 1 AND msib.inventory_item_id = prl.item_id)
                          pr_item_num,
                       prl.item_description pr_item_desc,
                       prl.quantity pr_qty,
                       prl.unit_price pr_amnt,
                       NULL po_date,
                       NULL po_num,
                       NULL po_item_num,
                       NULL po_item_desc,
                       NULL po_qty,
                       NULL po_base_amnt,
                       NULL po_tax_amnt,
                       NULL grn_date,
                       NULL grn_num,
                       NULL grn_item_num,
                       NULL grn_item_desc,
                       NULL grn_qty,
                       NULL grn_base_amnt,
                       NULL grn_tax_amnt,
                       NULL invoice_num,
                       NULL invoice_doc_num,
                       NULL invoice_base_amnt,
                       NULL line_number,
                       NULL invoice_tax_amnt,
                       --NULL invoice_tot_amnt,
                       NULL invoice_date,
                       NULL invoice_gl_date,
                       NULL pay_doc_num,
                       NULL payment_amnt,
                       NULL payment_date,
                       SYSDATE outbound_date
                  FROM apps.po_requisition_headers_all prh,
                       apps.po_requisition_lines_all prl,
                       apps.po_req_distributions_all prd,
                       apps.mtl_system_items_b msib,
                       apps.hr_operating_units hou
                 WHERE 1 = 1
                       AND prh.requisition_header_id =
                              prl.requisition_header_id
                       AND prl.requisition_line_id = prd.requisition_line_id
                       AND prh.org_id = hou.organization_id
                       AND prl.item_id = msib.inventory_item_id
                       AND msib.ITEM_TYPE = 'ABRL CAPEX'
                       AND NVL (prl.cancel_flag, 'N') = 'N'
                       --                 AND prh.segment1 = '640100010729'
                       AND TRUNC (prh.last_update_date) = p_run_date
                       AND NOT EXISTS
                                  (SELECT 1
                                     FROM apps.po_distributions_all pda
                                    WHERE 1 = 1
                                          AND pda.req_distribution_id =
                                                 prd.distribution_id)
                UNION ALL
                SELECT DISTINCT
                       hou.NAME ou_name,
                       aps.segment1 vendor_num,
                       aps.vendor_name,
                       apsa.vendor_site_code vendor_site,
                       NULL pr_ref_num,
                       TRUNC (prh.creation_date) pr_date,
                       prh.segment1 pr_num,
                       (SELECT DISTINCT segment1
                          FROM apps.mtl_system_items_b msib
                         WHERE 1 = 1 AND msib.inventory_item_id = prl.item_id)
                          pr_item_num,
                       prl.item_description pr_item_desc,
                       prl.quantity pr_qty,
                       prl.unit_price pr_amnt,
                       TRUNC (pha.creation_date) po_date,
                       pha.segment1 po_num,
                       (SELECT DISTINCT segment1
                          FROM apps.mtl_system_items_b msib
                         WHERE 1 = 1 AND msib.inventory_item_id = pla.item_id)
                          po_item_num,
                       pla.item_description po_item_desc,
                       pla.quantity po_qty,
                       pla.unit_price po_base_amnt,
                       (SELECT NVL (
                                  SUM (
                                     DECODE (jtl.rec_tax_amt_tax_curr,
                                             '0', jtl.nrec_tax_amt_tax_curr,
                                             jtl.rec_tax_amt_tax_curr)),
                                  0)
                                  po_tax
                          FROM apps.jai_tax_lines jtl
                         WHERE     1 = 1
                               AND jtl.trx_id = pha.po_header_id
                               AND jtl.trx_line_id = pla.po_line_id)
                          po_tax_amnt,
                       NULL grn_date,
                       NULL grn_num,
                       NULL grn_item_num,
                       NULL grn_item_desc,
                       NULL grn_qty,
                       NULL grn_base_amnt,
                       NULL grn_tax_amnt,
                       NULL invoice_num,
                       NULL invoice_doc_num,
                       NULL invoice_base_amnt,
                       NULL line_number,
                       NULL invoice_tax_amnt,
                       --  NULL invoice_tot_amnt,
                       NULL invoice_date,
                       NULL invoice_gl_date,
                       NULL pay_doc_num,
                       NULL payment_amnt,
                       NULL payment_date,
                       SYSDATE outbound_date
                  FROM apps.po_requisition_headers_all prh,
                       apps.po_requisition_lines_all prl,
                       apps.po_req_distributions_all prd,
                       apps.po_headers_all pha,
                       apps.po_lines_all pla,
                       apps.po_distributions_all pda,
                       apps.mtl_system_items_b msib,
                       apps.hr_operating_units hou,
                       apps.ap_suppliers aps,
                       apps.ap_supplier_sites_all apsa
                 WHERE 1 = 1
                       AND prh.requisition_header_id =
                              prl.requisition_header_id
                       AND prl.requisition_line_id = prd.requisition_line_id
                       AND NVL (pla.cancel_flag, 'N') = 'N'
                       AND pda.req_distribution_id = prd.distribution_id
                       AND pha.po_header_id = pla.po_header_id
                       AND pla.po_line_id = pda.po_line_id
                       AND prh.org_id = hou.organization_id
                       AND pha.org_id = hou.organization_id
                       AND aps.vendor_id = apsa.vendor_id
                       AND aps.vendor_id = pha.vendor_id
                       AND apsa.vendor_site_id = pha.vendor_site_id
                       AND prl.item_id = msib.inventory_item_id
                       AND pla.item_id = msib.inventory_item_id
                       AND msib.ITEM_TYPE = 'ABRL CAPEX'
                       --                 AND pha.segment1 = '171015636'
                       AND TRUNC (pha.last_update_date) = p_run_date
                       AND NOT EXISTS
                                  (SELECT 1
                                     FROM apps.rcv_transactions rt
                                    WHERE 1 = 1
                                          AND rt.po_header_id =
                                                 pha.po_header_id)
                UNION ALL
                SELECT DISTINCT
                       hou.NAME ou_name,
                       aps.segment1 vendor_num,
                       aps.vendor_name,
                       apsa.vendor_site_code vendor_site,
                       NULL pr_ref_num,
                       TRUNC (prh.creation_date) pr_date,
                       prh.segment1 pr_num,
                       (SELECT DISTINCT segment1
                          FROM apps.mtl_system_items_b msib
                         WHERE 1 = 1 AND msib.inventory_item_id = prl.item_id)
                          pr_item_num,
                       prl.item_description pr_item_desc,
                       prl.quantity pr_qty,
                       prl.unit_price pr_amnt,
                       TRUNC (pha.creation_date) po_date,
                       pha.segment1 po_num,
                       (SELECT DISTINCT segment1
                          FROM apps.mtl_system_items_b msib
                         WHERE 1 = 1 AND msib.inventory_item_id = pla.item_id)
                          po_item_num,
                       pla.item_description po_item_desc,
                       pla.quantity po_qty,
                       pla.unit_price po_base_amnt,
                       (SELECT NVL (
                                  SUM (
                                     DECODE (jtl.rec_tax_amt_tax_curr,
                                             '0', jtl.nrec_tax_amt_tax_curr,
                                             jtl.rec_tax_amt_tax_curr)),
                                  0)
                                  po_tax
                          FROM apps.jai_tax_lines jtl
                         WHERE     1 = 1
                               AND jtl.trx_id = pha.po_header_id
                               AND jtl.trx_line_id = pla.po_line_id)
                          po_tax_amnt,
                       TRUNC (rsh.creation_date) grn_date,
                       rsh.receipt_num grn_num,
                       (SELECT DISTINCT segment1
                          FROM apps.mtl_system_items_b msib
                         WHERE 1 = 1 AND msib.inventory_item_id = rsl.item_id)
                          grn_item_num,
                       rsl.item_description grn_item_desc,
                       rt.quantity grn_qty,
                       rt.po_unit_price grn_base_amnt,
                       (SELECT NVL (
                                  SUM (
                                     DECODE (rec_tax_amt_tax_curr,
                                             0, nrec_tax_amt_trx_curr,
                                             rec_tax_amt_tax_curr)),
                                  0)
                                  grn_tax_amount
                          FROM apps.jai_tax_lines jtl
                         WHERE     entity_code = 'RCV_TRANSACTION'
                               AND trx_type = 'RECEIVE'
                               AND jtl.trx_id = rsh.shipment_header_id
                               AND jtl.trx_line_id = rsl.shipment_line_id)
                          grn_tax_amnt,
                       NULL invoice_num,
                       NULL invoice_doc_num,
                       NULL invoice_base_amnt,
                       rsl.line_num line_number,
                       NULL invoice_tax_amnt,
                       --NULL invoice_tot_amnt,
                       NULL invoice_date,
                       NULL invoice_gl_date,
                       NULL pay_doc_num,
                       NULL payment_amnt,
                       NULL payment_date,
                       SYSDATE outbound_date
                  FROM apps.po_requisition_headers_all prh,
                       apps.po_requisition_lines_all prl,
                       apps.po_req_distributions_all prd,
                       apps.po_headers_all pha,
                       apps.po_lines_all pla,
                       apps.po_distributions_all pda,
                       apps.rcv_shipment_headers rsh,
                       apps.rcv_shipment_lines rsl,
                       apps.rcv_transactions rt,
                       apps.mtl_system_items_b msib,
                       apps.hr_operating_units hou,
                       apps.ap_suppliers aps,
                       apps.ap_supplier_sites_all apsa
                 WHERE 1 = 1
                       AND prh.requisition_header_id =
                              prl.requisition_header_id
                       AND prl.requisition_line_id = prd.requisition_line_id
                       AND pha.po_header_id = pla.po_header_id
                       AND NVL (pla.cancel_flag, 'N') = 'N'
                       AND pla.po_line_id = pda.po_line_id
                       AND prd.distribution_id = pda.req_distribution_id
                       AND rsh.shipment_header_id = rsl.shipment_header_id
                       AND rsl.shipment_line_id = rt.shipment_line_id
                       AND rsh.shipment_header_id = rt.shipment_header_id
                       AND pha.po_header_id = rt.po_header_id
                       AND pla.po_line_id = rt.po_line_id
                       AND pha.org_id = hou.organization_id
                       AND rt.transaction_type = 'RECEIVE'
                       AND prl.item_id = msib.inventory_item_id
                       AND pla.item_id = msib.inventory_item_id
                       AND rsl.item_id = msib.inventory_item_id
                       AND msib.ITEM_TYPE = 'ABRL CAPEX'
                       AND aps.vendor_id = apsa.vendor_id
                       AND aps.vendor_id = pha.vendor_id
                       AND apsa.vendor_site_id = pha.vendor_site_id
                       AND aps.vendor_id = rsh.vendor_id
                       AND apsa.vendor_site_id = rsh.vendor_site_id
                       --  AND rsh.receipt_num = '635300014615'
                       AND TRUNC (rsh.last_update_date) = p_run_date
                       AND NOT EXISTS
                                  (SELECT 1
                                     FROM apps.ap_invoice_lines_all ail
                                    WHERE 1 = 1
                                          AND ail.rcv_transaction_id =
                                                 rt.transaction_id)
                UNION ALL
                SELECT DISTINCT
                       hou.NAME ou_name,
                       aps.segment1 vendor_num,
                       aps.vendor_name,
                       apsa.vendor_site_code vendor_site,
                       NULL pr_ref_num,
                       TRUNC (prh.creation_date) pr_date,
                       prh.segment1 pr_num,
                       (SELECT DISTINCT segment1
                          FROM apps.mtl_system_items_b msib
                         WHERE 1 = 1 AND msib.inventory_item_id = prl.item_id)
                          pr_item_num,
                       prl.item_description pr_item_desc,
                       prl.quantity pr_qty,
                       prl.unit_price pr_amnt,
                       TRUNC (pha.creation_date) po_date,
                       pha.segment1 po_num,
                       (SELECT DISTINCT segment1
                          FROM apps.mtl_system_items_b msib
                         WHERE 1 = 1 AND msib.inventory_item_id = pla.item_id)
                          po_item_num,
                       pla.item_description po_item_desc,
                       pla.quantity po_qty,
                       pla.unit_price po_base_amnt,
                       (SELECT NVL (
                                  SUM (
                                     DECODE (jtl.rec_tax_amt_tax_curr,
                                             '0', jtl.nrec_tax_amt_tax_curr,
                                             jtl.rec_tax_amt_tax_curr)),
                                  0)
                                  po_tax
                          FROM apps.jai_tax_lines jtl
                         WHERE     1 = 1
                               AND jtl.trx_id = pha.po_header_id
                               AND jtl.trx_line_id = pla.po_line_id)
                          po_tax_amnt,
                       TRUNC (rsh.creation_date) grn_date,
                       rsh.receipt_num grn_num,
                       (SELECT DISTINCT segment1
                          FROM apps.mtl_system_items_b msib
                         WHERE 1 = 1 AND msib.inventory_item_id = rsl.item_id)
                          grn_item_num,
                       rsl.item_description grn_item_desc,
                       rt.quantity grn_qty,
                       rt.po_unit_price grn_base_amnt,
                       (SELECT NVL (
                                  SUM (
                                     DECODE (rec_tax_amt_tax_curr,
                                             0, nrec_tax_amt_trx_curr,
                                             rec_tax_amt_tax_curr)),
                                  0)
                                  grn_tax_amount
                          FROM apps.jai_tax_lines jtl
                         WHERE     entity_code = 'RCV_TRANSACTION'
                               AND trx_type = 'RECEIVE'
                               AND jtl.trx_id = rsh.shipment_header_id
                               AND jtl.trx_line_id = rsl.shipment_line_id)
                          grn_tax_amnt,
                       aia.invoice_num,
                       aia.doc_sequence_value invoice_doc_num,
                       ail.amount invoice_base_amnt,
                       ail.line_number,
                       (SELECT NVL (
                                  SUM (
                                     DECODE (jtl.rec_tax_amt_tax_curr,
                                             '0', jtl.nrec_tax_amt_tax_curr,
                                             jtl.rec_tax_amt_tax_curr)),
                                  0)
                                  invoice_tax_amount
                          FROM apps.jai_tax_lines jtl
                         WHERE     1 = 1
                               AND jtl.trx_id = aia.invoice_id
                               AND jtl.trx_line_id = ail.line_number)
                          invoice_tax_amnt,
                       aia.invoice_date,
                       aia.gl_date invoice_gl_date,
                       (SELECT doc_sequence_value
                          FROM apps.ap_checks_all apc,
                               apps.ap_invoice_payments_all aip
                         WHERE     1 = 1
                               AND apc.check_id = aip.check_id
                               AND TRUNC (aip.last_update_date) = p_run_date
                               AND aia.invoice_id = aip.invoice_id)
                          pay_doc_num,
                       (SELECT amount
                          FROM apps.ap_invoice_payments_all aip
                         WHERE     1 = 1
                               AND TRUNC (aip.last_update_date) = p_run_date
                               AND aip.invoice_id = aia.invoice_id)
                          payment_amnt,
                       (SELECT check_date
                          FROM apps.ap_checks_all apc,
                               apps.ap_invoice_payments_all aip
                         WHERE     1 = 1
                               AND apc.check_id = aip.check_id
                               AND TRUNC (aip.last_update_date) = p_run_date
                               AND aia.invoice_id = aip.invoice_id)
                          payment_date,
                       SYSDATE outbound_date
                  FROM apps.po_requisition_headers_all prh,
                       apps.po_requisition_lines_all prl,
                       apps.po_req_distributions_all prda,
                       apps.po_headers_all pha,
                       apps.po_lines_all pla,
                       apps.po_distributions_all pda,
                       apps.hr_operating_units hou,
                       apps.rcv_shipment_headers rsh,
                       apps.rcv_shipment_lines rsl,
                       apps.rcv_transactions rt,
                       apps.ap_invoices_all aia,
                       apps.ap_invoice_lines_all ail,
                       apps.mtl_system_items_b msib,
                       --   apps.ap_invoice_payments_all aipa,
                       --    apps.ap_checks_all aca,
                       apps.ap_suppliers aps,
                       apps.ap_supplier_sites_all apsa
                 WHERE 1 = 1
                       AND prh.requisition_header_id =
                              prl.requisition_header_id
                       AND prh.org_id = hou.organization_id
                       AND prl.requisition_line_id = prda.requisition_line_id
                       AND prda.distribution_id = pda.req_distribution_id
                       AND pha.po_header_id = pla.po_header_id
                       AND pla.po_line_id = pda.po_line_id
                       AND rsh.shipment_header_id = rsl.shipment_header_id
                       AND rsl.shipment_line_id = rt.shipment_line_id
                       AND pha.po_header_id = rt.po_header_id
                       AND NVL (pla.cancel_flag, 'N') = 'N'
                       AND pla.po_line_id = rt.po_line_id
                       AND aia.invoice_id = ail.invoice_id
                       AND rt.transaction_id = ail.rcv_transaction_id
                       AND prl.item_id = msib.inventory_item_id
                       AND pla.item_id = msib.inventory_item_id
                       AND rsl.item_id = msib.inventory_item_id
                       AND msib.ITEM_TYPE = 'ABRL CAPEX'
                       --  AND aia.invoice_id = aipa.invoice_id
                       -- AND aipa.check_id = aca.check_id
                       AND TRUNC (aia.last_update_date) = p_run_date
                       --  AND prh.segment1 = '112022424'
                       --and trunc(prh.creation_date)>='01-jun-2019'
                       AND transaction_type = 'RECEIVE'
                       AND ail.line_type_lookup_code = 'ITEM'
                       AND aps.vendor_id = apsa.vendor_id
                       AND aia.vendor_id = aps.vendor_id
                       AND aia.vendor_site_id = apsa.vendor_site_id
                       AND pha.vendor_site_id = apsa.vendor_site_id
                       AND aps.vendor_id = rsh.vendor_id
                       AND apsa.vendor_site_id = rsh.vendor_site_id
                       AND pha.org_id = hou.organization_id
                       AND aia.org_id = hou.organization_id)
         WHERE 1 = 1
      --AND pr_num IN ('640100010729', '172017331', '112022424')
      --'112022424'
      --- in ( '640100010729','172017331')
      --and po_num='111019091'
      ORDER BY pr_num;

   x_id                UTL_FILE.file_type;
BEGIN
   x_id :=
      UTL_FILE.fopen ('IDFC',
                      'PR_OUTBOUND_FILE_' || g_conc_request_id || '.csv',
                      'w');
   UTL_FILE.put_line (
      x_id,
         'OU_NAME'
      || '^'
      || 'VENDOR_NUM'
      || '^'
      || 'VENDOR_NAME'
      || '^'
      || 'IVENDOR_SITE'
      || '^'
      || 'PR_REF_NUM'
      || '^'
      || 'PR_DATE'
      || '^'
      || 'PR_NUM'
      || '^'
      || 'PR_ITEM_NUM'
      || '^'
      || 'PR_ITEM_DESC'
      || '^'
      || 'PR_QTY'
      || '^'
      || 'PR_AMNT'
      || '^'
      || 'PO_DATE'
      || '^'
      || 'PO_NUM'
      || '^'
      || 'PO_ITEM_NUM'
      || '^'
      || 'PO_ITEM_DESC'
      || '^'
      || 'PO_QTY'
      || '^'
      || 'PO_BASE_AMNT'
      || '^'
      || 'PO_TAX_AMNT'
      || '^'
      || 'PO_TOT_AMT'
      || '^'
      || 'GRN_DATE'
      || '^'
      || 'GRN_NUM'
      || '^'
      || 'GRN_ITEM_NUM'
      || '^'
      || 'GRN_ITEM_DESC'
      || '^'
      || 'GRN_QTY'
      || '^'
      || 'GRN_BASE_AMNT'
      || '^'
      || 'GRN_TAX_AMNT'
      || '^'
      || 'GRN_TOT_AMT'
      || '^'
      || 'INVOICE_NUM'
      || '^'
      || 'INVOICE_DOC_NUM'
      || '^'
      || 'INVOICE_BASE_AMNT'
      || '^'
      || 'INVOICE_TAX_AMNT'
      || '^'
      || 'INVOICE_TOT_AMNT'
      || '^'
      || 'INVOICE_DATE'
      || '^'
      || 'INVOICE_GL_DATE'
      || '^'
      || 'PAY_DOC_NUM'
      || '^'
      || 'PAYMENT_AMNT'
      || '^'
      || 'PAYMENT_DATE'
      || '^'
      || 'OUTBOUND_DATE');

   FOR i IN pr_out
   LOOP
      UTL_FILE.put_line (
         x_id,
            i.ou_name
         || '^'
         || i.vendor_num
         || '^'
         || i.vendor_name
         || '^'
         || i.vendor_site
         || '^'
         || i.pr_ref_num
         || '^'
         || i.pr_date
         || '^'
         || i.pr_num
         || '^'
         || i.pr_item_num
         || '^'
         || i.pr_item_desc
         || '^'
         || i.pr_qty
         || '^'
         || i.pr_amnt
         || '^'
         || i.po_date
         || '^'
         || i.po_num
         || '^'
         || i.po_item_num
         || '^'
         || i.po_item_desc
         || '^'
         || i.po_qty
         || '^'
         || i.po_base_amnt
         || '^'
         || i.po_tax_amnt
         || '^'
         || i.po_tot_amt
         || '^'
         || i.grn_date
         || '^'
         || i.grn_num
         || '^'
         || i.grn_item_num
         || '^'
         || i.grn_item_desc
         || '^'
         || i.grn_qty
         || '^'
         || i.grn_base_amnt
         || '^'
         || i.grn_tax_amnt
         || '^'
         || i.grn_tot_amt
         || '^'
         || i.invoice_num
         || '^'
         || i.invoice_doc_num
         || '^'
         || i.invoice_base_amnt
         || '^'
         || i.invoice_tax_amnt
         || '^'
         || i.invoice_tot_amnt
         || '^'
         || i.invoice_date
         || '^'
         || i.invoice_gl_date
         || '^'
         || i.pay_doc_num
         || '^'
         || i.payment_amnt
         || '^'
         || i.payment_date
         || '^'
         || i.outbound_date);
   END LOOP;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      UTL_FILE.fclose (x_id);
      fnd_file.put_line (fnd_file.LOG, 'No Data Found');
   WHEN UTL_FILE.invalid_path
   THEN
      UTL_FILE.fclose (x_id);
      fnd_file.put_line (fnd_file.LOG, 'Invalid Path');
   WHEN UTL_FILE.invalid_filehandle
   THEN
      UTL_FILE.fclose (x_id);
      fnd_file.put_line (fnd_file.LOG, 'Invalid File Handle');
   WHEN UTL_FILE.invalid_operation
   THEN
      UTL_FILE.fclose (x_id);
      fnd_file.put_line (fnd_file.LOG, 'Invalid Operation');
   WHEN UTL_FILE.read_error
   THEN
      UTL_FILE.fclose (x_id);
      fnd_file.put_line (fnd_file.LOG, 'Read Error');
   WHEN UTL_FILE.write_error
   THEN
      UTL_FILE.fclose (x_id);
      fnd_file.put_line (fnd_file.LOG, 'Write Error');
   WHEN UTL_FILE.internal_error
   THEN
      UTL_FILE.fclose (x_id);
      fnd_file.put_line (fnd_file.LOG, 'Internal Error');
   WHEN UTL_FILE.file_open
   THEN
      UTL_FILE.fclose (x_id);
      fnd_file.put_line (fnd_file.LOG, 'File is Open');
   WHEN UTL_FILE.invalid_filename
   THEN
      UTL_FILE.fclose (x_id);
      fnd_file.put_line (fnd_file.LOG, 'Invalid File Name');
   WHEN OTHERS
   THEN
      UTL_FILE.fclose (x_id);
      fnd_file.put_line (fnd_file.LOG, 'Unknown Error' || SQLERRM);
      UTL_FILE.fclose (x_id);
END; 
/


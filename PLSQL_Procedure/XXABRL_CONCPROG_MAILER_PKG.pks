CREATE OR REPLACE PACKAGE APPS.XXABRL_CONCPROG_MAILER_PKG
AS
PROCEDURE MAILER(              
                               errbuf   OUT VARCHAR2,
                               RetCode  OUT NUMBER,
                               p_Param  IN VARCHAR2
                               );
  PROCEDURE Print_log(
                      p_str IN VARCHAR2, 
                      p_debug_flag IN VARCHAR2
                      );
END XXABRL_CONCPROG_MAILER_PKG;
/


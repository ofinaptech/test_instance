CREATE OR REPLACE PACKAGE BODY APPS.xxar_einv_pkg
AS
------------------------------------------------------------------------------------------------------------------------------
-- Version     Date            Modified By            Description                                                           --
------------------------------------------------------------------------------------------------------------------------------
-- 1.0         01-Oct-2020     Pawan Sahu             Initial Version                                                       --
-- 1.1         31-Dec-2020     Pawan Sahu             Company Name Change to "More Retail Private Limited"                  --
-- 1.2         05-Apr-2021     Pawan Sahu             Modified to pick transactions from last program start time            --
-- 1.3         09-Jun-2021     Pawan Sahu             Introduced Dynamic QR code for B2C Transactions                       --
------------------------------------------------------------------------------------------------------------------------------
PROCEDURE export_einv (errbuf             out varchar2,
                       retcode            out number,
                       p_reprocess_from_date in varchar2)
IS
 l_last_run_timestamp date;
 l_prog_running       varchar2(1);
 l_json_data          clob;
 l_supply_type        varchar2(10); -- V1.3 Added
 l_buyer_gstin        varchar2(50); -- V1.3 Added
 -- Check if request is already running
 -- Fetch last successful run timestamp

 cursor c_inv (p_last_run_timestamp in date) is
 select distinct rct.customer_trx_id,
                hca.cust_account_id,
                hcsu.site_use_id,
                rctt.name header,
                --'B2B' supply_type, -- V1.3 Commented
                decode(rctt.type, 'CM', 'CRN', 'INV') doc_type,
                rct.trx_number document_number,
                rct.trx_date doc_date,
                hou.name operating_unit,
                /*(select jprl.registration_number
                   from jai_party_regs_v jpr, jai_party_reg_lines_v jprl
                  where jpr.org_id = rct.org_id
                    and jpr.party_reg_id = jprl.party_reg_id
                    and jpr.reg_class_code = 'first_party'
--                    and jpr.party_class_code = 't'
                    ---and jprl.regime_id = 12345
                    and nvl(jprl.effective_to, trunc(sysdate)) >= trunc(sysdate)
                    and jprl.registration_type_code = 'gst') seller_gstin, */
                (select distinct first_party_primary_reg_num
                   from apps.jai_tax_lines
                  where entity_code = 'TRANSACTIONS'
                    and trx_id = rct.customer_trx_id
                    and first_party_primary_reg_name = 'GSTIN') seller_gstin,
                --'More Retail Limited' seller_legal_name, -- V1.1 Commented
                'More Retail Private Limited' seller_legal_name, -- V1.1 Added
                loc.address_line_1 seller_address_1,
                loc.address_line_2 seller_address_2,
                loc.loc_information15 seller_city,
                loc.postal_code seller_pincode,
                (select distinct registration_number
                   from apps.jai_party_regs jpr, apps.jai_party_reg_lines jprl
                  where jpr.party_reg_id = jprl.party_reg_id
                    and jpr.party_id = rct.bill_to_customer_id
                    and jpr.party_site_id = hcas.cust_acct_site_id
                    and jpr.customer_flag = 'Y'
                    and jpr.org_id = rct.org_id
                    and registration_type_code = 'GSTIN'
                    and jprl.effective_to is null) buyer_gstin,
                hp_sub.party_name buyer_legal_name,
                hp_sub.party_name buyer_trade_name,
                hl.address1 buyer_address_1,
                hl.address2 buyer_address_2,
                /*(select distinct substr(registration_number, 1, 2)
                   from apps.jai_party_regs jpr, apps.jai_party_reg_lines jprl
                  where jpr.party_reg_id = jprl.party_reg_id
                    and jpr.party_id = rct.bill_to_customer_id
                    and jpr.party_site_id = hcas.cust_acct_site_id
                    and jpr.customer_flag = 'Y'
                    and jpr.org_id = rct.org_id
                    and registration_type_code = 'GSTIN'
                    and jprl.effective_to is null) buyer_pos, -- first two letters of buyer_gstin */ -- V1.3 Commented
                (select flv.meaning from fnd_lookup_values flv
                  where lookup_type = 'JA_IN_STATE_CODE'
                    and flv.description = hl.state) buyer_pos, -- V1.3 added
                hl.city buyer_city,
                hl.postal_code buyer_pincode,
                rctl.line_number serial_number,
                --decode (length(jtdflv.hsn_code), 8, 'n', 6, 'y') is_service,
                case when length(trim(nvl(jtdflv.hsn_code, jtdflv.sac_code))) >= 8 then 'N'
                     when length(trim(nvl(jtdflv.hsn_code, jtdflv.sac_code))) = 6 then 'Y'
                     else 'N' end is_service,
                trim(nvl(jtdflv.hsn_code, jtdflv.sac_code)) hsn_code,
                nvl(decode(rctl.uom_code, 'Ea', 'NOS'), 'NOS') units,
                abs(round(decode(rctt.type, 'CM', rctl.quantity_credited, rctl.quantity_invoiced), 2)) qty,
                abs(round(rctl.unit_selling_price, 2)) unit_price,
                abs(round(jtl.line_amt * nvl(jtl.currency_conversion_rate, 1), 2)) line_amount,
                (select sum(jtlv.tax_rate_percentage)
                   from jai_tax_lines_v jtlv
                  where jtlv.trx_id = rct.customer_trx_id
                    and rctl.line_number = jtlv.trx_line_number
                    and jtlv.trx_line_id = rctl.customer_trx_line_id
                    and jtlv.application_id = 222
                    and jtlv.entity_code = 'TRANSACTIONS'
                    and jtlv.precedence_1 = 0
                    and (jtlv.tax_type_name like '%CGST%' or jtlv.tax_type_name like '%SGST%' or jtlv.tax_type_name like '%IGST%')) gst_rate,
                null cess_rate,
                null advlrm_amount,
           abs(round(((jtl.line_amt
                  +
                  (select sum(decode(jtlv.recoverable_flag,
                                        'Y',
                                        jtlv.rec_tax_amt_funcl_curr,
                                        jtlv.nrec_tax_amt_funcl_curr))
                   from jai_tax_lines_v jtlv
                  where jtlv.trx_id = rct.customer_trx_id
                    and jtlv.trx_line_number = rctl.line_number
                    and jtlv.trx_line_id = rctl.customer_trx_line_id
                    and jtlv.application_id = 222
                    and jtlv.entity_code = 'TRANSACTIONS'
                    and jtlv.precedence_1 = 0
                    and (jtlv.tax_type_name like '%CGST%' or jtlv.tax_type_name like '%SGST%' or jtlv.tax_type_name like '%IGST%')))
                * nvl(jtl.currency_conversion_rate, 1)), 2)) total_item_value,
          abs(round((select sum(decode(rctt.type, 'CM', rctl1.quantity_credited, rctl1.quantity_invoiced) * rctl1.unit_selling_price)
                   from ra_customer_trx_lines_all rctl1
                  where rctl1.customer_trx_id = rctl.customer_trx_id
                    and rctl1.line_type = 'LINE'), 2)) total_assessable_value,
         abs(round(((select sum(decode(rctt.type, 'CM', rctl1.quantity_credited, rctl1.quantity_invoiced) * rctl1.unit_selling_price)
                   from ra_customer_trx_lines_all rctl1
                  where rctl1.customer_trx_id = rctl.customer_trx_id
                    and rctl1.line_type in ('LINE'))
               +(select sum(rctl1.extended_amount)
                   from ra_customer_trx_lines_all rctl1
                  where rctl1.customer_trx_id = rctl.customer_trx_id
                    and rctl1.line_type in ('TAX'))), 2)) final_invoice_value,
                null transporter_id,
                null distance,
                hp_sub.party_name customer_name
  from ra_customer_trx_all       rct,
       ra_customer_trx_lines_all rctl,
       ra_cust_trx_line_gl_dist_all rctlgd,
       ra_cust_trx_types_all     rctt,
       jai_tax_lines_v           jtl,
       jai_tax_det_fct_lines_v   jtdflv,
       hr_all_organization_units hou,
       hz_parties                hp_sub,
       hz_cust_accounts          hca,
       hz_party_sites            hps,
       hz_locations              hl,
       hz_cust_site_uses_all     hcsu,
       hz_cust_acct_sites_all    hcas,
       hr_locations_all          loc
 where 1 = 1
   and rct.customer_trx_id = rctl.customer_trx_id
   and rct.org_id = rctl.org_id
   and rctl.line_type = 'LINE'
   and rct.customer_trx_id = rctlgd.customer_trx_id
   and rctl.customer_trx_line_id = rctlgd.customer_trx_line_id
   and rctlgd.account_class = 'REV'
   and trunc(rctlgd.gl_date) >= trunc(to_date('01-OCT-2020', 'DD-MON-RRRR'))
   and rct.customer_trx_id = jtl.trx_id
   and rctl.customer_trx_line_id = jtl.trx_line_id
   and jtdflv.det_factor_id = jtl.det_factor_id
   and jtl.application_id = 222 and jtdflv.application_id = 222
   and hou.organization_id = rct.org_id
   and hp_sub.party_type = 'ORGANIZATION'
   and hca.party_id = hp_sub.party_id
   and hps.party_id = hp_sub.party_id
   and hca.status = 'A'
   and hps.status = 'A'
   and hp_sub.party_type <> 'PARTY_RELATIONSHIP'
   and hps.location_id = hl.location_id
   and hca.cust_account_id = rct.bill_to_customer_id
   and hcsu.site_use_id = rct.bill_to_site_use_id
   and hcsu.cust_acct_site_id = hcas.cust_acct_site_id
   and hcas.party_site_id = hps.party_site_id
   and hou.location_id = loc.location_id
   and rctt.cust_trx_type_id = rct.cust_trx_type_id
   and nvl(rct.complete_flag, 'N') = 'Y' -- Completed Transaction
   and rct.last_update_date >= nvl(to_date(p_reprocess_from_date, 'yyyy/mm/dd hh24:mi:ss'), p_last_run_timestamp) -- V1.3 added reprocess date parameter
   and not exists (select distinct 1 from apps.xxar_gst_einv_json xgeh -- not already processed in custom table
                     where xgeh.customer_trx_id = rct.customer_trx_id
                       and xgeh.processed_flag = 'Y')
--and rct.trx_number in ('62000015378', '62000015382')
order by rct.customer_trx_id, rctl.line_number;

    cursor c_json_header (p_request_id in number)
    is
    select distinct stg.customer_trx_id,
                    stg.cust_account_id,
                    stg.site_use_id,
                    stg.header,
                    stg.supply_type,
                    stg.doc_type,
                    stg.document_number,
                    stg.doc_date,
                    --stg.operating_unit,
                    stg.seller_gstin,
                    stg.seller_legal_name,
                    stg.seller_address_1,
                    stg.seller_address_2,
                    stg.seller_city,
                    stg.seller_pincode,
                    stg.buyer_gstin,
                    stg.buyer_legal_name,
                    stg.buyer_trade_name,
                    stg.buyer_address_1,
                    stg.buyer_address_2,
                    stg.buyer_pos,
                    stg.buyer_city,
                    stg.buyer_pincode,
                    stg.total_assessable_value,
                    stg.final_invoice_value,
                    stg.transporter_id,
                    stg.distance,
                    stg.customer_name,
                    stg.request_date,
                    stg.requested_by
                    --'{"invoices":[{"CustDocNo":"'||stg.document_number||'","TranDtls":{"SupTyp":"'||stg.supply_type||'"},"DocDtls":{"Typ":"'||stg.doc_type||'","No":"'||stg.document_number||'","Dt":"'||to_char(stg.doc_date, 'DD/MM/YYYY')||'"},"SellerDtls": {"Gstin":"'||stg.seller_gstin||'","LglNm":"'||stg.seller_legal_name||'","Addr1":"'||stg.seller_address_1||'","Addr2":"'||stg.seller_address_2||'","Loc":"'||stg.seller_city||'","Pin":"'||stg.seller_pincode||'"},"BuyerDtls": {"Gstin":"'||stg.buyer_gstin||'","LglNm":"'||stg.buyer_legal_name||'","TrdNm":"'||stg.buyer_trade_name||'","Addr1":"'||stg.buyer_address_1||'","Addr2":"'||stg.buyer_address_2||'","Pos":"'||stg.buyer_pos||'","Loc":"'||stg.buyer_city||'","Pin":"'||stg.buyer_pincode||'"},"ItemList": [{"SlNo":"'||stg.serial_number||'","IsServc":"'||stg.is_service||'","HsnCd":"'||stg.hsn_code||'","Qty":"'||stg.qty||'","Unit":"'||stg.units||'","UnitPrice":"'||stg.unit_price||'","TotAmt":"'||stg.total_amount||'","AssAmt":"'||stg.assessable_amount||'","GstRt":"'||stg.gst_rate||'","CesRt":"'||stg.cess_rate||'","CesNonAdvlAmt":"'||stg.advlrm_amount||'","TotItemVal":"'||stg.total_item_value||'"}],"ValDtls":{"AssVal":"'||stg.total_assessable_value||'","TotInvVal":"'||stg.final_invoice_value||'"}}]}' json_data
      from xxabrl.xxar_gst_einv_headers stg
     where stg.request_id = p_request_id
     order by stg.customer_trx_id;

    cursor c_json_lines (p_request_id in number, p_customer_trx_id in number)
    is
    select stg.customer_trx_id, stg.serial_number,
           stg.is_service,
           stg.hsn_code,
           stg.units,
           stg.qty,
           stg.unit_price,
           stg.total_amount,
           stg.assessable_amount,
           stg.gst_rate,
           stg.cess_rate,
           stg.advlrm_amount,
           stg.total_item_value
          --'{"invoices":[{"CustDocNo":"'||stg.document_number||'","TranDtls":{"SupTyp":"'||stg.supply_type||'"},"DocDtls":{"Typ":"'||stg.doc_type||'","No":"'||stg.document_number||'","Dt":"'||to_char(stg.doc_date, 'DD/MM/YYYY')||'"},"SellerDtls": {"Gstin":"'||stg.seller_gstin||'","LglNm":"'||stg.seller_legal_name||'","Addr1":"'||stg.seller_address_1||'","Addr2":"'||stg.seller_address_2||'","Loc":"'||stg.seller_city||'","Pin":"'||stg.seller_pincode||'"},"BuyerDtls": {"Gstin":"'||stg.buyer_gstin||'","LglNm":"'||stg.buyer_legal_name||'","TrdNm":"'||stg.buyer_trade_name||'","Addr1":"'||stg.buyer_address_1||'","Addr2":"'||stg.buyer_address_2||'","Pos":"'||stg.buyer_pos||'","Loc":"'||stg.buyer_city||'","Pin":"'||stg.buyer_pincode||'"},"ItemList": [{"SlNo":"'||stg.serial_number||'","IsServc":"'||stg.is_service||'","HsnCd":"'||stg.hsn_code||'","Qty":"'||stg.qty||'","Unit":"'||stg.units||'","UnitPrice":"'||stg.unit_price||'","TotAmt":"'||stg.total_amount||'","AssAmt":"'||stg.assessable_amount||'","GstRt":"'||stg.gst_rate||'","CesRt":"'||stg.cess_rate||'","CesNonAdvlAmt":"'||stg.advlrm_amount||'","TotItemVal":"'||stg.total_item_value||'"}],"ValDtls":{"AssVal":"'||stg.total_assessable_value||'","TotInvVal":"'||stg.final_invoice_value||'"}}]}' json_data
      from xxabrl.xxar_gst_einv_headers stg
     where stg.request_id = p_request_id
       and stg.customer_trx_id = p_customer_trx_id
     order by stg.customer_trx_id;

BEGIN

   fnd_file.put_line (fnd_file.LOG, 'p_reprocess_from_date : '||p_reprocess_from_date);

   begin
     l_prog_running := 'N';
     select distinct 'Y'
       into l_prog_running
       from fnd_conc_req_summary_v
      where program_short_name = 'XXAR_EINV_SUPERTAX_INT'
        and phase_code = 'R'
        and request_id not in (g_request_id); -- to ignore this run
   exception when others then
     l_prog_running := 'N';
   end;

   begin
     l_last_run_timestamp := null;
     select max(actual_start_date) -- max(actual_completion_date) -- V1.2 Modified on 05-Apr-2021
       into l_last_run_timestamp
       from fnd_conc_req_summary_v
      where program_short_name = 'XXAR_EINV_SUPERTAX_INT'
        and phase_code = 'C' and status_code = 'C';
   exception when others then
     l_last_run_timestamp := sysdate -1; -- Consider last 24 hours
   end;

   if l_prog_running = 'Y' then--- program already running, exit
      fnd_file.put_line (fnd_file.LOG, 'Program already running...');
   else
     fnd_file.put_line (fnd_file.LOG, 'Fetching data from base tables and Inserting into XXAR_GST_EINV_HEADERS table...');
     fnd_file.put_line (fnd_file.LOG, '---------------------------------------------------------------------------------------------------');

     for i in c_inv (l_last_run_timestamp) loop

       -- V1.3 Start
       l_supply_type := null;
       l_buyer_gstin := null;
       -- If first two characters of GSTN are numeric (that means GSTN registered) then sypply_type -> B2B else B2C
       -- If supply_type -> B2C then GSTN -> URP
       select decode(apps.is_num(substr(nvl(i.buyer_gstin, 'XX'), 1, 2)), 'T', 'B2B', 'F', 'B2C'),
              decode(apps.is_num(substr(nvl(i.buyer_gstin, 'XX'), 1, 2)), 'T', i.buyer_gstin, 'F', 'URP')
         into l_supply_type,
              l_buyer_gstin
         from dual;
       -- V1.3 End

       begin
       insert into xxabrl.xxar_gst_einv_headers (customer_trx_id, cust_account_id, site_use_id, header, supply_type, doc_type, document_number, doc_date,
                   seller_gstin, seller_legal_name, seller_address_1, seller_address_2, seller_city, seller_pincode,
                   buyer_gstin, buyer_legal_name, buyer_trade_name, buyer_address_1, buyer_address_2, buyer_pos, buyer_city, buyer_pincode,
                   serial_number, is_service, hsn_code, units, qty, unit_price, total_amount, assessable_amount, gst_rate, cess_rate,
                   advlrm_amount, total_item_value, total_assessable_value, final_invoice_value, transporter_id, distance, customer_name,
                   request_id, request_date, requested_by, processed_flag)
           values (i.customer_trx_id, i.cust_account_id, i.site_use_id, i.header, l_supply_type, i.doc_type, i.document_number, i.doc_date,
                   i.seller_gstin, i.seller_legal_name, i.seller_address_1, i.seller_address_2, i.seller_city, i.seller_pincode,
                   l_buyer_gstin, i.buyer_legal_name, i.buyer_trade_name, i.buyer_address_1, i.buyer_address_2, i.buyer_pos, i.buyer_city, i.buyer_pincode,
                   i.serial_number, i.is_service, i.hsn_code, i.units, i.qty, i.unit_price, i.line_amount, i.line_amount, i.gst_rate, i.cess_rate,
                   i.advlrm_amount, i.total_item_value, i.total_assessable_value, i.final_invoice_value, i.transporter_id, i.distance, i.customer_name,
                   g_request_id, sysdate, g_user_id, 'N');

         fnd_file.put_line (fnd_file.LOG, 'Transaction ID # '||i.customer_trx_id ||'. '||'Transaction Number # '|| i.document_number);
       exception when others then
         fnd_file.put_line (fnd_file.LOG, 'Exception # 1: Insert "xxabrl.xxar_gst_einv_headers": Trx #: '|| i.document_number||'. '||SQLERRM);
       end;
     end loop;
     commit;

     fnd_file.put_line (fnd_file.LOG, '-');
     fnd_file.put_line (fnd_file.LOG, 'Formatting data into JSON format and Inserting into XXAR_GST_EINV_JSON table...');
     fnd_file.put_line (fnd_file.LOG, '---------------------------------------------------------------------------------------------------');

     l_json_data := null;
     for j in c_json_header (g_request_id) loop
       begin
       if (j.supply_type              is not null and
           j.doc_type                 is not null and
           j.document_number          is not null and
           j.doc_date                 is not null and
           j.seller_gstin             is not null and
           j.seller_legal_name        is not null and
           j.seller_address_1         is not null and
           j.seller_city              is not null and
           j.seller_pincode           is not null and
           j.buyer_gstin              is not null and
           j.buyer_legal_name         is not null and
           j.buyer_trade_name         is not null and
           j.buyer_address_1          is not null and
           j.buyer_pos                is not null and
           j.buyer_city               is not null and
           j.buyer_pincode            is not null and
           /*j.serial_number            is not null and
           j.is_service               is not null and
           j.hsn_code                 is not null and
           j.unit_price               is not null and
           j.total_amount             is not null and
           j.assessable_amount        is not null and
           j.gst_rate                 is not null and
           j.total_item_value         is not null and */
           j.total_assessable_value   is not null and
           j.final_invoice_value      is not null) then

         l_json_data := l_json_data ||'{"invoices":[{"CustDocNo":"'||j.document_number||'","TranDtls":{"SupTyp":"'||j.supply_type||'"},"DocDtls":{"Typ":"'||j.doc_type||'","No":"'||j.document_number||'","Dt":"'||to_char(j.doc_date, 'DD/MM/YYYY')||'"},"SellerDtls": {"Gstin":"'||j.seller_gstin||'","LglNm":"'||j.seller_legal_name||'","Addr1":"'||j.seller_address_1||'","Addr2":"'||j.seller_address_2||'","Loc":"'||j.seller_city||'","Pin":"'||j.seller_pincode||'"},"BuyerDtls": {"Gstin":"'||j.buyer_gstin||'","LglNm":"'||j.buyer_legal_name||'","TrdNm":"'||j.buyer_trade_name||'","Addr1":"'||j.buyer_address_1||'","Addr2":"'||j.buyer_address_2||'","Pos":"'||j.buyer_pos||'","Loc":"'||j.buyer_city||'","Pin":"'||j.buyer_pincode||'"},';

         l_json_data := l_json_data ||'"ItemList":[';

         for k in c_json_lines (g_request_id, j.customer_trx_id) loop
            l_json_data := l_json_data ||'{"SlNo":"'||k.serial_number||'","IsServc":"'||k.is_service||'","HsnCd":"'||k.hsn_code||'","Qty":"'||k.qty||'","Unit":"'||k.units||'","UnitPrice":"'||k.unit_price||'","TotAmt":"'||k.total_amount||'","AssAmt":"'||k.assessable_amount||'","GstRt":"'||k.gst_rate||'","CesRt":"'||k.cess_rate||'","CesNonAdvlAmt":"'||k.advlrm_amount||'","TotItemVal":"'||k.total_item_value||'"},';
         end loop;

         l_json_data := substr(l_json_data, 1, length(l_json_data)-1) ||'],'; -- To remove last Comma from ItemList block

         l_json_data := l_json_data ||'"ValDtls":{"AssVal":"'||j.total_assessable_value||'","TotInvVal":"'||j.final_invoice_value||'"}}]}';

         insert into apps.xxar_gst_einv_json (customer_trx_id, document_number, json_data, irn, json_qr_code, qr_code, request_id, request_date, requested_by, processed_flag)
         values     (j.customer_trx_id, j.document_number, l_json_data, null, null, null, g_request_id, j.request_date, j.requested_by, 'N');

         commit;
         l_json_data := null;
         fnd_file.put_line (fnd_file.LOG, 'JSON inserted for Transaction ID # '||j.customer_trx_id ||'. '||'Transaction Number # '|| j.document_number||', Customer # '||j.customer_name);
       else
         update xxabrl.xxar_gst_einv_headers header
            set processed_flag = 'E', remarks = 'Mandatory fields missing.'
          where header.customer_trx_id = j.customer_trx_id
            and header.request_id = g_request_id
            --and header.rowid = j.rowid
            ;
         commit;
         fnd_file.put_line (fnd_file.LOG, '--');
         fnd_file.put_line (fnd_file.LOG, 'JSON NOT inserted for Transaction ID # '||j.customer_trx_id ||'. '||'Transaction Number # '|| j.document_number ||', Customer # '||j.customer_name||'. Check Mandatory fields.');
       end if;
       exception when others then
         fnd_file.put_line (fnd_file.LOG, 'Exception # 2: Insert "xxabrl.xxar_gst_einv_json": Trx #: '|| j.document_number||'. '||SQLERRM);
       end;
     end loop;
   end if;
EXCEPTION WHEN OTHERS THEN
   fnd_file.put_line (fnd_file.LOG, 'Exception in Main Proc: '||SQLCODE||': '||SQLERRM);
END export_einv;

END xxar_einv_pkg;
/


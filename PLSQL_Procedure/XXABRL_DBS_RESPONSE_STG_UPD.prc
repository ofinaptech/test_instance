CREATE OR REPLACE PROCEDURE APPS.XXABRL_DBS_RESPONSE_STG_UPD(ERRBUF  OUT VARCHAR2,
                                                        RETCODE OUT VARCHAR2) AS
 /**********************************************************************************************************************************************
                        WIPRO Infotech Ltd, Mumbai, India

    Name        : ABRL DBS Response Program 
    Description: This Procedure will update the DBS_PAYMENT_TABLE based on the DBS_RETURN_STATUS table (response file sent by the DBS Bank)

    Change Record:
   =========================================================================================================================
   Version   Date          Author                    Remarks                  Documnet Ref
   =======   ==========   =============             ==================  =====================================================
   1.0.0     09-Jul-2012   Amresh Kumar Chutke      Initial Version
          
  ************************************************************************************************************************************************/

    /*V_CHECK_ID    NUMBER;
    V_TRANSACTION_REF_NO  VARCHAR2(25);
    V_STATUS      VARCHAR2(50);
    V_DESCRIPTION VARCHAR2(250);*/
    
-- Response Cursor    
CURSOR CUR_RES IS
            SELECT DRS.* FROM APPS.DBS_RETURN_STATUS DRS, APPS.DBS_PAYMENT_TABLE DPT
            WHERE DRS.CHECK_ID = DPT.CHECK_ID
            AND TRUNC(DRS.INTERFACE_DATE)=TRUNC(SYSDATE)
            AND DRS.INTERFACE_FLAG='N'; 
 BEGIN
 
    FOR REC_RES IN CUR_RES
    LOOP
        BEGIN
         /*SELECT DRS.CHECK_ID,DRS.TRANSACTION_REF_NO,DRS.STATUS,DRS.DESCRIPTION 
            INTO V_CHECK_ID,V_TRANSACTION_REF_NO,V_STATUS,V_DESCRIPTION
            FROM APPS.DBS_RETURN_STATUS DRS
            WHERE TRUNC(DRS.INTERFACE_DATE)=TRUNC(SYSDATE)
                  AND DRS.INTERFACE_FLAG='N';*/
            
            -- Updating Payment Table with the status sent by DBS Bank      
            UPDATE APPS.DBS_PAYMENT_TABLE DPT
            SET DPT.ATTRIBUTE1 = REC_RES.STATUS
               ,DPT.ATTRIBUTE2 = REC_RES.DESCRIPTION
               ,DPT.ATTRIBUTE3 = REC_RES.TRANSACTION_REF_NO
               ,DPT.ATTRIBUTE_CATEGORY='DBS Additional Information'
               --,DPT.TRANSACTION_STATUS = 'PROCESSED'
            WHERE DPT.CHECK_ID = REC_RES.CHECK_ID
            AND DPT.TRANSACTION_STATUS = 'PROCESSED';
            
            --Updating Return Status table with interface flag 'Y'
            UPDATE  APPS.DBS_RETURN_STATUS DRS
            SET INTERFACE_FLAG = 'Y'
            WHERE DRS.CHECK_ID = REC_RES.CHECK_ID
            AND INTERFACE_FLAG='N';
            
        EXCEPTION
            WHEN OTHERS THEN
            UPDATE APPS.DBS_RETURN_STATUS DRS
            SET INTERFACE_FLAG='N', REMARKS='Unable to Update Stage Table'
            WHERE DRS.CHECK_ID = REC_RES.CHECK_ID;
            
        DBMS_OUTPUT.PUT_LINE('Unable to Update Stage Table' );
        
        END;
             
    END LOOP;
    
 END XXABRL_DBS_RESPONSE_STG_UPD; 
/


CREATE OR REPLACE PROCEDURE APPS.XXABRL_TPCA_ENABLE_PRC(ERRBUF  OUT VARCHAR2,
                                                    RETCODE OUT VARCHAR2) AS


V_GL_ACCESS_SET_ID   NUMBER;
LN_LOADER_REQUEST_ID NUMBER;

CURSOR C_TPCA_ENABLE IS
    SELECT XTPE.COMPILED_VALUE_ATTRIBUTES_E,FFVL.* FROM XXABRL.XXABRL_TPCA_ENABLE XTPE, APPS.FND_FLEX_VALUES_VL FFVL
    WHERE XTPE.FLEX_VALUE = FFVL.FLEX_VALUE
    AND FFVL.FLEX_VALUE_SET_ID=1013469;
    
 BEGIN
       

    FOR R_TPCA_ENABLE IN C_TPCA_ENABLE 
    LOOP

        BEGIN
        
        FND_FLEX_VALUES_PKG.UPDATE_ROW
                (
                 X_FLEX_VALUE_ID    =>       R_TPCA_ENABLE.FLEX_VALUE_ID
                ,X_ATTRIBUTE_SORT_ORDER    =>       R_TPCA_ENABLE.ATTRIBUTE_SORT_ORDER
                ,X_FLEX_VALUE_SET_ID    =>       R_TPCA_ENABLE.FLEX_VALUE_SET_ID
                ,X_FLEX_VALUE    =>       R_TPCA_ENABLE.FLEX_VALUE
                ,X_ENABLED_FLAG    =>       R_TPCA_ENABLE.ENABLED_FLAG
                ,X_SUMMARY_FLAG    =>       R_TPCA_ENABLE.SUMMARY_FLAG
                ,X_START_DATE_ACTIVE    =>       R_TPCA_ENABLE.START_DATE_ACTIVE
                ,X_END_DATE_ACTIVE    =>       R_TPCA_ENABLE.END_DATE_ACTIVE
                ,X_PARENT_FLEX_VALUE_LOW    =>       R_TPCA_ENABLE.PARENT_FLEX_VALUE_LOW
                ,X_PARENT_FLEX_VALUE_HIGH    =>       R_TPCA_ENABLE.PARENT_FLEX_VALUE_HIGH
                ,X_STRUCTURED_HIERARCHY_LEVEL    =>       R_TPCA_ENABLE.STRUCTURED_HIERARCHY_LEVEL
                ,X_HIERARCHY_LEVEL    =>       R_TPCA_ENABLE.HIERARCHY_LEVEL
                ,X_COMPILED_VALUE_ATTRIBUTES    =>       R_TPCA_ENABLE.COMPILED_VALUE_ATTRIBUTES_E -- Third Party Control Account Enabled
                ,X_VALUE_CATEGORY    =>   R_TPCA_ENABLE.VALUE_CATEGORY
                ,X_ATTRIBUTE1    =>       R_TPCA_ENABLE.ATTRIBUTE1
                ,X_ATTRIBUTE2    =>       R_TPCA_ENABLE.ATTRIBUTE2
                ,X_ATTRIBUTE3    =>       R_TPCA_ENABLE.ATTRIBUTE3
                ,X_ATTRIBUTE4    =>       R_TPCA_ENABLE.ATTRIBUTE4
                ,X_ATTRIBUTE5    =>       R_TPCA_ENABLE.ATTRIBUTE5
                ,X_ATTRIBUTE6    =>       R_TPCA_ENABLE.ATTRIBUTE6
                ,X_ATTRIBUTE7    =>       R_TPCA_ENABLE.ATTRIBUTE7
                ,X_ATTRIBUTE8    =>       R_TPCA_ENABLE.ATTRIBUTE8
                ,X_ATTRIBUTE9    =>       R_TPCA_ENABLE.ATTRIBUTE9
                ,X_ATTRIBUTE10    =>       R_TPCA_ENABLE.ATTRIBUTE10
                ,X_ATTRIBUTE11    =>       R_TPCA_ENABLE.ATTRIBUTE11
                ,X_ATTRIBUTE12    =>       R_TPCA_ENABLE.ATTRIBUTE12
                ,X_ATTRIBUTE13    =>       R_TPCA_ENABLE.ATTRIBUTE13
                ,X_ATTRIBUTE14    =>       R_TPCA_ENABLE.ATTRIBUTE14
                ,X_ATTRIBUTE15    =>       R_TPCA_ENABLE.ATTRIBUTE15
                ,X_ATTRIBUTE16    =>       R_TPCA_ENABLE.ATTRIBUTE16
                ,X_ATTRIBUTE17    =>       R_TPCA_ENABLE.ATTRIBUTE17
                ,X_ATTRIBUTE18    =>       R_TPCA_ENABLE.ATTRIBUTE18
                ,X_ATTRIBUTE19    =>       R_TPCA_ENABLE.ATTRIBUTE19
                ,X_ATTRIBUTE20    =>       R_TPCA_ENABLE.ATTRIBUTE20
                ,X_ATTRIBUTE21    =>       R_TPCA_ENABLE.ATTRIBUTE21
                ,X_ATTRIBUTE22    =>       R_TPCA_ENABLE.ATTRIBUTE22
                ,X_ATTRIBUTE23    =>       R_TPCA_ENABLE.ATTRIBUTE23
                ,X_ATTRIBUTE24    =>       R_TPCA_ENABLE.ATTRIBUTE24
                ,X_ATTRIBUTE25    =>       R_TPCA_ENABLE.ATTRIBUTE25
                ,X_ATTRIBUTE26    =>       R_TPCA_ENABLE.ATTRIBUTE26
                ,X_ATTRIBUTE27    =>       R_TPCA_ENABLE.ATTRIBUTE27
                ,X_ATTRIBUTE28    =>       R_TPCA_ENABLE.ATTRIBUTE28
                ,X_ATTRIBUTE29    =>       R_TPCA_ENABLE.ATTRIBUTE29
                ,X_ATTRIBUTE30    =>       R_TPCA_ENABLE.ATTRIBUTE30
                ,X_ATTRIBUTE31    =>       R_TPCA_ENABLE.ATTRIBUTE31
                ,X_ATTRIBUTE32    =>       R_TPCA_ENABLE.ATTRIBUTE32
                ,X_ATTRIBUTE33    =>       R_TPCA_ENABLE.ATTRIBUTE33
                ,X_ATTRIBUTE34    =>       R_TPCA_ENABLE.ATTRIBUTE34
                ,X_ATTRIBUTE35    =>       R_TPCA_ENABLE.ATTRIBUTE35
                ,X_ATTRIBUTE36    =>       R_TPCA_ENABLE.ATTRIBUTE36
                ,X_ATTRIBUTE37    =>       R_TPCA_ENABLE.ATTRIBUTE37
                ,X_ATTRIBUTE38    =>       R_TPCA_ENABLE.ATTRIBUTE38
                ,X_ATTRIBUTE39    =>       R_TPCA_ENABLE.ATTRIBUTE39
                ,X_ATTRIBUTE40    =>       R_TPCA_ENABLE.ATTRIBUTE40
                ,X_ATTRIBUTE41    =>       R_TPCA_ENABLE.ATTRIBUTE41
                ,X_ATTRIBUTE42    =>       R_TPCA_ENABLE.ATTRIBUTE42
                ,X_ATTRIBUTE43    =>       R_TPCA_ENABLE.ATTRIBUTE43
                ,X_ATTRIBUTE44    =>       R_TPCA_ENABLE.ATTRIBUTE44
                ,X_ATTRIBUTE45    =>       R_TPCA_ENABLE.ATTRIBUTE45
                ,X_ATTRIBUTE46    =>       R_TPCA_ENABLE.ATTRIBUTE46
                ,X_ATTRIBUTE47    =>       R_TPCA_ENABLE.ATTRIBUTE47
                ,X_ATTRIBUTE48    =>       R_TPCA_ENABLE.ATTRIBUTE48
                ,X_ATTRIBUTE49    =>       R_TPCA_ENABLE.ATTRIBUTE49
                ,X_ATTRIBUTE50    =>       R_TPCA_ENABLE.ATTRIBUTE50
                ,X_FLEX_VALUE_MEANING    =>       R_TPCA_ENABLE.FLEX_VALUE_MEANING
                ,X_DESCRIPTION    =>       R_TPCA_ENABLE.DESCRIPTION
                ,X_LAST_UPDATE_DATE    =>  SYSDATE --     R_TPCA_ENABLE.LAST_UPDATE_DATE
                ,X_LAST_UPDATED_BY    =>   '4191' --    R_TPCA_ENABLE.LAST_UPDATED_BY
                ,X_LAST_UPDATE_LOGIN    =>       R_TPCA_ENABLE.LAST_UPDATE_LOGIN 
                );
               
         
        UPDATE  XXABRL.XXABRL_TPCA_ENABLE XTPE
        SET XTPE.STATUS_FLAG = 'P', XTPE.LAST_UPDATE_DATE = SYSDATE
        WHERE XTPE.FLEX_VALUE = R_TPCA_ENABLE.FLEX_VALUE;
        
        COMMIT;

        EXCEPTION
                    WHEN OTHERS THEN
                    UPDATE  XXABRL.XXABRL_TPCA_ENABLE XTPE
                    SET XTPE.STATUS_FLAG = 'E',XTPE.LAST_UPDATE_DATE = SYSDATE
                    WHERE XTPE.FLEX_VALUE = R_TPCA_ENABLE.FLEX_VALUE;
                    
                DBMS_OUTPUT.PUT_LINE('Unable to Update Stage Table' );
            
        END;
                 
    END LOOP;
    
    
     V_GL_ACCESS_SET_ID := FND_PROFILE.VALUE('GL_ACCESS_SET_ID');
          
            IF V_GL_ACCESS_SET_ID IS NULL THEN
              FND_FILE.PUT_LINE(FND_FILE.LOG,
                                'GL_ACCESS_SET_ID / Ledger ID Setup Incomplete...');
            ELSE
              FND_FILE.PUT_LINE(FND_FILE.LOG,
                                'GL_ACCESS_SET_ID: ' || V_GL_ACCESS_SET_ID);
            END IF;
    
     BEGIN
          LN_LOADER_REQUEST_ID :=
             fnd_request.submit_request
                                        ('SQLGL', -- application
                                         'GLNSVI', -- program
                                         '', -- description
                                         '',  -- start_time
                                         FALSE, -- sub_request
                                         V_GL_ACCESS_SET_ID,--1120, -- argument1
                                         'Yes' -- argument2
                                          );
         
         COMMIT;
         
    IF (LN_LOADER_REQUEST_ID = 0) THEN
            
              fnd_file.put_line(fnd_file.log,
                                'Request Not Submitted due to "' ||
                                fnd_message.get || '".');
              fnd_file.put_line(FND_FILE.Log,
                                'Concurrent Request For Loading STD. GL IMPORT Generated an error, Submitted with request id ' ||
                                ln_loader_request_id);
            
            ELSE
              fnd_file.put_line(FND_FILE.Log,
                                'Submitted Loader Program with request id : ' ||
                                TO_CHAR(ln_loader_request_id));
            END IF;
          
    EXCEPTION
      WHEN OTHERS
      THEN
         fnd_file.put_line
                (fnd_file.LOG,
                    'Exception while Request submit '
                 || SQLERRM
                );
    END;
                
 END XXABRL_TPCA_ENABLE_PRC; 
/


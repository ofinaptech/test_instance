CREATE OR REPLACE PROCEDURE APPS.xxabrl_third_party_sites (
   errbuf    OUT   VARCHAR2,
   retcode   OUT   NUMBER
)
IS
   v_party_reg_id               NUMBER;
   v_party_reg_line_id          NUMBER;
   p_assoc_rec                  jai_reporting_associations%ROWTYPE;
   v_reporting_association_id   NUMBER;
   v_reporting_code_id          NUMBER;
   process_flag                 VARCHAR2 (20);
   err_msg                      VARCHAR2 (4000);
   v_tot_rec                    NUMBER;
   v_tot_err                    NUMBER;
   v_tot_val                    NUMBER;
   v_regime_code                VARCHAR2 (200);
   v_reporting_code             VARCHAR2 (200);
   v_reporting_type_name        VARCHAR2 (200);
   insert_msg                   VARCHAR2 (4000);
   v_tot_v                      NUMBER;
   v_tot_e                      NUMBER;
   v_dup                        NUMBER;
   v_tot_re                     NUMBER;

   CURSOR c1
   IS
      SELECT DISTINCT stg.ROWID, stg.operating_unit, hou.organization_id,
                      stg.party_name, stg.party_site_name, asp.vendor_id,
                      jr.regime_id, stg.regime_code, assa.vendor_site_id,
                      stg.party_number party_number, stg.registration_number,
                      stg.reporting_type_name, stg.primary_registration_type,
                      stg.reporting_code, stg.status, stg.error_msg
                 FROM xxabrliproc.xxabrl_party_reg_ou_site stg,
                      apps.jai_regimes jr,
                      apps.ap_suppliers asp,
                      apps.ap_supplier_sites_all assa,
                      apps.hr_operating_units hou
                WHERE 1 = 1
                  AND NVL (stg.status, 'N') IN ('N', 'E')
                  AND stg.regime_code = jr.regime_code
                  AND stg.party_number = asp.segment1
                  AND stg.party_site_name = assa.vendor_site_code
                  AND asp.vendor_id = assa.vendor_id
                  AND assa.org_id = hou.organization_id
                  --AND asp.vendor_id = 4898304
                  AND hou.NAME = stg.operating_unit
             ORDER BY stg.party_name, stg.party_site_name;

   CURSOR c2
   IS
      SELECT DISTINCT stg.operating_unit, hou.organization_id, asp.vendor_id,
                      assa.vendor_site_id, stg.party_number,
                      stg.party_site_name
                 FROM xxabrliproc.xxabrl_party_reg_ou_site stg,
                      apps.jai_regimes jr,
                      apps.jai_reporting_codes jrc,
                      apps.ap_suppliers asp,
                      apps.ap_supplier_sites_all assa,
                      apps.hr_operating_units hou
                WHERE 1 = 1
                  AND stg.status = 'V'
                  AND stg.regime_code = jr.regime_code
                  AND stg.party_number = asp.segment1
                  AND stg.party_site_name = assa.vendor_site_code
                  AND assa.org_id = hou.organization_id
                  AND asp.vendor_id = assa.vendor_id
                  AND NOT EXISTS (
                         SELECT 1
                           FROM apps.jai_party_regs
                          WHERE supplier_flag = 'Y'
                            AND site_flag = 'Y'
                            AND party_id = asp.vendor_id
                            AND party_site_id = assa.vendor_site_id
                            AND org_id = assa.org_id)
                  AND assa.org_id = hou.organization_id
                  -- AND stg.party_number = '6000208'
                  AND stg.reporting_code = jrc.reporting_code
                  AND hou.NAME = stg.operating_unit
             ORDER BY asp.vendor_id, assa.vendor_site_id, hou.organization_id;

   CURSOR c3 (
      p_vendor_id        IN   NUMBER,
      p_vendor_site_id   IN   NUMBER,
      p_org_id           IN   NUMBER
   )
   IS
      SELECT DISTINCT stg.ROWID, jr.regime_id, stg.regime_code,
                      stg.party_number, stg.registration_number,
                      stg.reporting_type_name, stg.primary_registration_type,
                      stg.reporting_code, stg.party_site_name, asp.vendor_id,
                      assa.vendor_site_id, jrc.reporting_code_description,
                      jrc.reporting_type_id, jrc.reporting_code_id,
                      jrt.reporting_usage, stg.secondary_registration_type,
                      stg.secondary_registration_number,
                      CASE
                         WHEN primary_registration_type = 'PAN'
                            THEN (SELECT creation_date
                                    FROM apps.ap_suppliers
                                   WHERE segment1 =
                                                   asp.segment1
                                     AND ROWNUM <= 1)
                         WHEN primary_registration_type = 'GSTIN'
                            THEN TO_DATE ('1-jul-17', 'DD-MON-YY')
                      END creation_date
                 FROM xxabrliproc.xxabrl_party_reg_ou_site stg,
                      apps.jai_regimes jr,
                      apps.jai_reporting_codes jrc,
                      apps.jai_reporting_types jrt,
                      apps.ap_suppliers asp,
                      apps.ap_supplier_sites_all assa,
                      apps.hr_operating_units hou
                WHERE 1 = 1
                  AND stg.status = 'V'
                  AND stg.regime_code = jr.regime_code
                  AND stg.party_number = asp.segment1
                  AND stg.party_site_name = assa.vendor_site_code
                  AND assa.org_id = hou.organization_id
                  AND asp.vendor_id = assa.vendor_id
                  AND stg.reporting_code = jrc.reporting_code
                  AND stg.reporting_type_name = jrt.reporting_type_name
                  AND asp.vendor_id = p_vendor_id
--                  AND NOT EXISTS (
--                         SELECT 1
--                           FROM apps.jai_party_regs
--                          WHERE supplier_flag = 'Y'
--                            AND site_flag = 'Y'
--                            AND party_id = asp.vendor_id
--                            AND party_site_id = assa.vendor_site_id
--                            AND org_id = assa.org_id)
                  AND assa.vendor_site_id = p_vendor_site_id
                  AND hou.organization_id = p_org_id
                  AND hou.NAME = stg.operating_unit
             ORDER BY asp.vendor_id, assa.vendor_site_id;
BEGIN
   v_tot_rec := 0;
   v_tot_err := 0;
   v_tot_val := 0;

   FOR i IN c1
   LOOP
      BEGIN
         process_flag := NULL;
         err_msg := NULL;
         fnd_file.put_line (fnd_file.LOG, 'OU ' || '->' || i.operating_unit);
         fnd_file.put_line (fnd_file.LOG,
                            'Vendor Code' || '->' || i.party_number
                           );
         fnd_file.put_line (fnd_file.LOG,
                            'Vendor Site' || '->' || i.party_site_name
                           );
         DBMS_OUTPUT.put_line ('Vendor Code' || '->' || i.party_number);
         DBMS_OUTPUT.put_line ('Vendor Site' || '->' || i.party_site_name);

         --validation for Regime
         BEGIN
            SELECT regime_code
              INTO v_regime_code
              FROM apps.jai_regimes
             WHERE regime_code = i.regime_code;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               process_flag := 'N';
               err_msg :=
                     err_msg
                  || 'regime_code is not existed in OFIN'
                  || '->'
                  || i.regime_code;
               fnd_file.put_line (fnd_file.LOG,
                                     'regime_code is not existed in OFIN'
                                  || '->'
                                  || i.regime_code
                                 );
               DBMS_OUTPUT.put_line (   'regime_code is not existed in OFIN'
                                     || '->'
                                     || i.regime_code
                                    );
            WHEN OTHERS
            THEN
               process_flag := 'N';
               err_msg :=
                     err_msg
                  || 'error in regime_code for '
                  || '->'
                  || i.regime_code;
               fnd_file.put_line (fnd_file.LOG,
                                     'error in regime_code for '
                                  || '->'
                                  || i.regime_code
                                 );
         END;

         fnd_file.put_line (fnd_file.LOG,
                            'Vendor Code' || '->' || i.reporting_code
                           );
         DBMS_OUTPUT.put_line ('reporting_code' || '->' || i.reporting_code);

         --validation for Reporting Code
         BEGIN
            SELECT reporting_code
              INTO v_reporting_code
              FROM apps.jai_reporting_codes
             WHERE reporting_code = TRIM (i.reporting_code);
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               process_flag := 'N';
               err_msg :=
                     err_msg
                  || 'REPORTING_CODE is not existed in OFIN'
                  || '->'
                  || i.reporting_code;
               fnd_file.put_line (fnd_file.LOG,
                                     'REPORTING_CODE is not existed in OFIN'
                                  || '->'
                                  || i.reporting_code
                                 );
               DBMS_OUTPUT.put_line
                                   (   'REPORTING_CODE is not existed in OFIN'
                                    || '->'
                                    || i.reporting_code
                                   );
            WHEN OTHERS
            THEN
               process_flag := 'N';
               err_msg :=
                     err_msg
                  || 'error in REPORTING_CODE for '
                  || '->'
                  || i.reporting_code;
               fnd_file.put_line (fnd_file.LOG,
                                     'error in REPORTING_CODE for '
                                  || '->'
                                  || i.reporting_code
                                 );
               DBMS_OUTPUT.put_line (   'error in REPORTING_CODE for '
                                     || '->'
                                     || i.reporting_code
                                    );
         END;

         fnd_file.put_line (fnd_file.LOG,
                               'reporting_type_name'
                            || '->'
                            || i.reporting_type_name
                           );
         DBMS_OUTPUT.put_line (   'reporting_type_name'
                               || '->'
                               || i.reporting_type_name
                              );

         --validation for REPORTING_TYPE_NAME
         BEGIN
            SELECT reporting_type_name
              INTO v_reporting_type_name
              FROM apps.jai_reporting_types
             WHERE reporting_type_name = i.reporting_type_name;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               process_flag := 'N';
               err_msg :=
                     err_msg
                  || 'REPORTING_type is not existed in OFIN'
                  || '->'
                  || i.reporting_type_name;
               fnd_file.put_line (fnd_file.LOG,
                                     'REPORTING_type is not existed in OFIN'
                                  || '->'
                                  || i.reporting_type_name
                                 );
               DBMS_OUTPUT.put_line
                                   (   'REPORTING_type is not existed in OFIN'
                                    || '->'
                                    || i.reporting_type_name
                                   );
            WHEN OTHERS
            THEN
               process_flag := 'N';
               err_msg :=
                     err_msg
                  || 'error in REPORTING_TYPE_NAME for '
                  || '->'
                  || i.reporting_code;
               fnd_file.put_line (fnd_file.LOG,
                                     'error in REPORTING_TYPE_NAME for '
                                  || '->'
                                  || i.reporting_type_name
                                 );
               DBMS_OUTPUT.put_line (   'error in REPORTING_TYPE_NAME for '
                                     || '->'
                                     || i.reporting_type_name
                                    );
         END;

--validation for Duplicate Third Party Registration for Suppliers
         BEGIN
            SELECT COUNT (*)
              INTO v_dup
              FROM apps.jai_party_regs jpr
             WHERE party_id = i.vendor_id
               AND party_site_id = i.vendor_site_id
               AND org_id = i.organization_id
               AND supplier_flag = 'Y'
               AND site_flag = 'Y';

            IF v_dup > 0
            THEN
               process_flag := 'N';
               err_msg :=
                     err_msg
                  || 'Third Party Registration  is  existed in OFIN for vendor_id - Vendor_site_id-org_id '
                  || '->'
                  || i.vendor_id
                  || '-'
                  || i.vendor_site_id
                  || '-'
                  || i.organization_id;
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               process_flag := 'N';
               err_msg :=
                     err_msg
                  || 'error in third Party registration for '
                  || '->'
                  || i.vendor_id
                  || '-'
                  || i.vendor_site_id;
               fnd_file.put_line (fnd_file.LOG,
                                     'error in third Party registration for '
                                  || '->'
                                  || i.vendor_id
                                  || '-'
                                  || i.vendor_site_id
                                 );
               DBMS_OUTPUT.put_line
                                  (   'error in third Party registration for '
                                   || '->'
                                   || i.vendor_id
                                   || '-'
                                   || i.vendor_site_id
                                  );
         END;

         IF process_flag = 'N'
         THEN
            UPDATE xxabrliproc.xxabrl_party_reg_ou_site
               SET status = 'E',
                   error_msg = err_msg
             WHERE ROWID = i.ROWID;

            v_tot_val := v_tot_val + 1;
         ELSE
            UPDATE xxabrliproc.xxabrl_party_reg_ou_site
               SET status = 'V',
                   error_msg = err_msg
             WHERE ROWID = i.ROWID;

            v_tot_err := v_tot_err + 1;
         END IF;

         v_tot_rec := v_tot_rec + 1;
         COMMIT;
         fnd_file.put_line (fnd_file.LOG, '---------------------');
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (fnd_file.LOG,
                               'error in ' || '->' || SQLERRM || '-'
                               || SQLCODE
                              );
      END;
   END LOOP;

   fnd_file.put_line (fnd_file.LOG, 'Total Records' || '->' || v_tot_rec);
   fnd_file.put_line (fnd_file.LOG,
                      'Total Valid Records' || '->' || v_tot_val);
   fnd_file.put_line (fnd_file.LOG,
                      'Total Error Records' || '->' || v_tot_err);
   fnd_file.put_line (fnd_file.output, 'Insert into jai_party_regs');
   v_tot_v := 0;
   v_tot_e := 0;
   v_tot_rec := 0;

   FOR j IN c2
   LOOP
      BEGIN
         insert_msg := NULL;
         fnd_file.put_line (fnd_file.output,
                            'OU' || '->' || j.operating_unit);
         fnd_file.put_line (fnd_file.output,
                            'Vendor Code' || '->' || j.party_number
                           );
--      fnd_file.put_line (fnd_file.LOG, 'Regime Code' || '->' || j.regime_code);
         fnd_file.put_line (fnd_file.output,
                            'Party Site Code' || '->' || j.party_site_name
                           );
         insert_msg := NULL;

         BEGIN
            v_party_reg_id := jai_party_regs_s.NEXTVAL;

            INSERT INTO jai_party_regs
                        (party_reg_id, party_type_code, supplier_flag,
                         customer_flag, site_flag, party_id, party_site_id,
                         org_classification_code, item_category_list,
                         invoice_tax_category_id, org_id, creation_date,
                         created_by, last_update_date, last_update_login,
                         last_updated_by, record_type_code
                        )
                 VALUES (v_party_reg_id, 'THIRD_PARTY_SITE', 'Y',
                         'N', 'Y', j.vendor_id, j.vendor_site_id,
                         NULL, NULL,
                         NULL, j.organization_id, SYSDATE,
                         4191, SYSDATE, USERENV ('SESSIONID'),
                         4191, 'DEFINED'
                        );

            DBMS_OUTPUT.put_line ('party_reg_id:-->' || v_party_reg_id);
            fnd_file.put_line (fnd_file.output,
                                  'Third Party Registration id :'
                               || v_party_reg_id
                              );
         EXCEPTION
            WHEN OTHERS
            THEN
               fnd_file.put_line
                  (fnd_file.output,
                      'Third Party Registration id is not generated for Vendor:'
                   || '--'
                   || j.party_number
                   || '-'
                   || j.party_site_name
                  );
               insert_msg :=
                     'Third Party Registration id is not generated for Vendor:'
                  || '--'
                  || j.party_number
                  || j.party_site_name;
         END;

         FOR k IN c3 (j.vendor_id, j.vendor_site_id, j.organization_id)
         LOOP
            IF v_party_reg_id <> 0
            THEN
               BEGIN
                  v_party_reg_line_id := jai_party_reg_lines_s.NEXTVAL;

                  INSERT INTO jai_party_reg_lines
                              (party_reg_id, party_reg_line_id,
                               line_context, regime_id,
                               registration_type_code,
                               registration_number,
                               sec_registration_type_code,
                               secondary_registration_number,
                               num_of_return_days, tax_authority_id,
                               tax_authority_site_id,
                               assessable_price_list_id,
                               default_section_code, effective_from,
                               effective_to, exemption_type, tracking_num,
                               exemption_num, intercompany_receivable_ccid,
                               intercompany_payable_ccid, creation_date,
                               created_by, last_update_date,
                               last_update_login, last_updated_by,
                               record_type_code
                              )
                       VALUES (v_party_reg_id, v_party_reg_line_id,
                               'REGISTRATIONS', k.regime_id,
                               k.primary_registration_type,
                               k.registration_number,
                               k.secondary_registration_type,
                               k.secondary_registration_number,
                               NULL, NULL,
                               NULL,
                               NULL,
                               NULL, k.creation_date,
                               NULL, NULL, NULL,
                               NULL, NULL,
                               NULL, SYSDATE,
                               4191, SYSDATE,
                               USERENV ('SESSIONID'), 4191,
                               'DEFINED'
                              );

                  DBMS_OUTPUT.put_line (   'party_reg_line_id:->'
                                        || v_party_reg_line_id
                                       );
                  fnd_file.put_line (fnd_file.output,
                                        'Party reg line id '
                                     || '-'
                                     || v_party_reg_line_id
                                    );
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     DBMS_OUTPUT.put_line (   'Party line is not generated'
                                           || k.party_number
                                          );
                     fnd_file.put_line
                        (fnd_file.output,
                            'Third Party Registration line id is not generated for Vendor:'
                         || '--'
                         || k.party_number
                         || k.party_site_name
                        );
                     insert_msg :=
                           'Third Party Registration line id is not generated for Vendor:'
                        || '--'
                        || k.party_number
                        || k.party_site_name;
               END;
            END IF;

            IF v_party_reg_id <> 0 AND v_party_reg_line_id <> 0
            THEN
               BEGIN
                  v_reporting_association_id :=
                                         jai_reporting_associations_s.NEXTVAL;
                  p_assoc_rec.reporting_association_id :=
                                                   v_reporting_association_id;
                  p_assoc_rec.reporting_type_name := k.reporting_type_name;
                  p_assoc_rec.reporting_usage := k.reporting_usage;
                  p_assoc_rec.reporting_code_description :=
                                                 k.reporting_code_description;
                  p_assoc_rec.reporting_code := k.reporting_code;
                  p_assoc_rec.entity_code := 'THIRD_PARTY';
                  p_assoc_rec.entity_id := v_party_reg_id;     --party_reg_id
                  p_assoc_rec.entity_source_table := 'JAI_PARTY_REGS';
                  p_assoc_rec.effective_from := k.creation_date;
                  p_assoc_rec.effective_to := NULL;
                  p_assoc_rec.creation_date := SYSDATE;
                  p_assoc_rec.created_by := 4191;
                  p_assoc_rec.last_update_date := SYSDATE;
                  p_assoc_rec.last_update_login := USERENV ('SESSIONID');
                  p_assoc_rec.last_updated_by := 4191;
                  p_assoc_rec.record_type_code := 'DEFINED';
                  p_assoc_rec.reporting_code_id := k.reporting_code_id;
                  p_assoc_rec.reporting_type_id := k.reporting_type_id;
                  p_assoc_rec.regime_id := k.regime_id;
                  p_assoc_rec.stl_hdr_id := NULL;

                  INSERT INTO jai_reporting_associations
                              (reporting_association_id,
                               reporting_type_name,
                               reporting_usage,
                               reporting_code_description,
                               reporting_code,
                               entity_code,
                               entity_id,
                               entity_source_table,
                               effective_from,
                               effective_to,
                               creation_date,
                               created_by,
                               last_update_date,
                               last_update_login,
                               last_updated_by,
                               record_type_code,
                               reporting_code_id,
                               reporting_type_id,
                               regime_id, stl_hdr_id
                              )
                       VALUES (p_assoc_rec.reporting_association_id,
                               p_assoc_rec.reporting_type_name,
                               p_assoc_rec.reporting_usage,
                               p_assoc_rec.reporting_code_description,
                               p_assoc_rec.reporting_code,
                               p_assoc_rec.entity_code,
                               p_assoc_rec.entity_id,
                               p_assoc_rec.entity_source_table,
                               p_assoc_rec.effective_from,
                               p_assoc_rec.effective_to,
                               p_assoc_rec.creation_date,
                               p_assoc_rec.created_by,
                               p_assoc_rec.last_update_date,
                               p_assoc_rec.last_update_login,
                               p_assoc_rec.last_updated_by,
                               p_assoc_rec.record_type_code,
                               p_assoc_rec.reporting_code_id,
                               p_assoc_rec.reporting_type_id,
                               p_assoc_rec.regime_id, p_assoc_rec.stl_hdr_id
                              );

                  DBMS_OUTPUT.put_line (   'party_reporting_id:->'
                                        || v_reporting_association_id
                                       );
                  fnd_file.put_line (fnd_file.output,
                                        'Party reporting id '
                                     || '-'
                                     || v_reporting_association_id
                                    );
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     DBMS_OUTPUT.put_line
                                       (   'Party Reporting is not generated'
                                        || k.party_number
                                       );
                     fnd_file.put_line
                        (fnd_file.output,
                            'Third Party Reporting is not generated for Vendor:'
                         || '--'
                         || k.party_number
                         || k.party_site_name
                        );
                     insert_msg :=
                           'Third Party Reporting is not generated for Vendor:'
                        || '--'
                        || k.party_number
                        || k.party_site_name;
               END;
            END IF;

            IF     v_party_reg_line_id <> 0
               AND v_party_reg_id <> 0
               AND v_reporting_association_id <> 0
            THEN
               UPDATE xxabrliproc.xxabrl_party_reg_ou_site
                  SET status = 'Y',
                      error_msg = NULL
                WHERE party_number = k.party_number AND ROWID = k.ROWID;

               v_tot_v := v_tot_v + 1;
               COMMIT;
            ELSE
               UPDATE xxabrliproc.xxabrl_party_reg_ou_site
                  SET status = 'E',
                      error_msg = insert_msg
                WHERE party_number = k.party_number AND ROWID = k.ROWID;

               v_tot_e := v_tot_v + 1;
            END IF;
         END LOOP;

         fnd_file.put_line (fnd_file.output, '----------------------');
         v_tot_re := v_tot_rec + 1;
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (fnd_file.LOG,
                               'error in ' || '->' || SQLERRM || '-'
                               || SQLCODE
                              );
      END;

      COMMIT;
   END LOOP;

   fnd_file.put_line (fnd_file.output, 'Total Records :' || '-' || v_tot_re);
   fnd_file.put_line (fnd_file.output,
                      'Total Error Records :' || '-' || v_tot_e
                     );
   fnd_file.put_line (fnd_file.output,
                      'Total Valid Records :' || '-' || v_tot_v
                     );
END; 
/


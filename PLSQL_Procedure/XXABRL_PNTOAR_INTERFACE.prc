CREATE OR REPLACE PROCEDURE APPS.XXABRL_PNTOAR_INTERFACE(ERRBUF OUT VARCHAR2,
                                                    RETCODE OUT VARCHAR2)
AS
v_batch_source_id NUMBER;
v_amount          NUMBER;
v_gl_date         DATE;
v_total_tax_amt   NUMBER;
v_line_tax_amt    NUMBER;  
v_tax_service_account NUMBER;
v_organization_id  NUMBER;
v_location_id      NUMBER;
v_tax_category_id NUMBER;
v_inventory_item_id  NUMBER:=0;
v_line_no           NUMBER:=0;
v_vat_tax_id        NUMBER:=0;
v_tax_type          VARCHAR2(50);
v_tax_account_id    NUMBER;
v_full_amount              NUMBER;
v_full_tax_amt              NUMBER:=0;
v_org_id          NUMBER:=fnd_profile.VALUE('ORG_ID');

---fnd_file.put_line (fnd_file.output, 'Testing1 : ' || Testing1);
CURSOR cur_batch_source
   IS
      SELECT batch_source_id
        FROM ra_batch_sources_all
       WHERE NAME ='Property Manager Batch Source'
         AND org_id = v_org_id;


CURSOR cur_trx_hdr(p_batch_source_id NUMBER)
   IS
   SELECT cta.trx_number, 
              cta.batch_source_id, 
              cta.set_of_books_id,
              cta.primary_salesrep_id, 
              cta.customer_trx_id, 
              cta.org_id,
              hou.location_id, 
              invoice_currency_code, 
              cta.trx_date,
                cta.br_amount, 
              cta.bill_to_customer_id,
                hcasa.CUST_ACCT_SITE_ID AS bill_to_address_id,
              INTERFACE_HEADER_ATTRIBUTE10 
         FROM ra_customer_trx_all cta,
                hr_organization_units hou,
                 hz_cust_acct_sites_all hcasa,
                HZ_PARTY_SITES hps,
              HZ_CUST_SITE_USES_all su
          WHERE hou.organization_id = cta.org_id
          AND hcasa.cust_account_id = cta.bill_to_customer_id
          AND su.site_use_id = cta.bill_to_site_use_id
            AND SU.CUST_ACCT_SITE_ID = hcasa.CUST_ACCT_SITE_ID
            AND su.site_use_code = 'BILL_TO'
            AND hcasa.PARTY_SITE_ID = hps.PARTY_SITE_ID
          AND cta.org_id = v_org_id
            AND cta.batch_source_id = p_batch_source_id             
          AND cta.customer_trx_id NOT IN (SELECT customer_trx_id
                                            FROM jai_ar_trxs                                
                                           WHERE batch_source_id = p_batch_source_id
                                             AND ORGANIZATION_ID = v_org_id
                                                     AND TAX_AMOUNT<>0);
                                             
   CURSOR cur_trx_ln (p_customer_trx_id NUMBER)
   IS
      SELECT customer_trx_line_id, 
             customer_trx_id, 
             line_number,
             inventory_item_id, 
             description, 
             uom_code, 
             quantity_invoiced,
             unit_selling_price, 
             extended_amount
        FROM ra_customer_trx_lines_all
       WHERE customer_trx_id = p_customer_trx_id AND org_id = v_org_id;
       
       
   CURSOR cur_tax_lines (p_customer_trx_line_id NUMBER)
   IS
      SELECT tax_amount, 
             tax_id
        FROM jai_ar_trx_tax_lines
       WHERE link_to_cust_trx_line_id = p_customer_trx_line_id;

    
   
   BEGIN
       OPEN cur_batch_source;
        FETCH cur_batch_source INTO v_batch_source_id;
        CLOSE cur_batch_source;
   
   
         FOR var_cur_trx_hdr IN cur_trx_hdr(v_batch_source_id)
            LOOP

             BEGIN
              SELECT SUM (extended_amount)
                  INTO v_amount
                FROM ra_customer_trx_lines_all
               WHERE customer_trx_id = var_cur_trx_hdr.customer_trx_id AND org_id = v_org_id
               GROUP BY customer_trx_id;

            EXCEPTION
            WHEN OTHERS THEN
            NULL;
            END;

            BEGIN
              SELECT gl_date
                  INTO v_gl_date
                  FROM ra_cust_trx_line_gl_dist_all
                 WHERE customer_trx_id = var_cur_trx_hdr.customer_trx_id AND account_class = 'REC';
      
            EXCEPTION
            WHEN OTHERS THEN
            NULL;
            END;
    
            BEGIN
            SELECT batch_source_id
              INTO v_batch_source_id
                  FROM ra_batch_sources_all
             WHERE NAME ='Property Manager Batch Source'
               AND org_id = v_org_id;
                EXCEPTION
                WHEN OTHERS THEN
            NULL;
            END;

            BEGIN
          SELECT hl.location_id
            INTO v_location_id
            FROM hr_locations_all hl,
                     JAI_RGM_PARTIES jp
           WHERE hl.inventory_organization_id=v_org_id
               AND hl.inventory_organization_id=jp.ORGANIZATION_ID
             AND hl.LOCATION_ID=jp.LOCATION_ID
             AND ROWNUM=1;
          EXCEPTION
          WHEN OTHERS
          THEN
             fnd_file.put_line
                          (fnd_file.LOG,
                              'Error to find location for Organization id : '
                           || v_organization_id
                          );
          END;

          BEGIN
          SELECT pv.attribute7
            INTO v_organization_id
                FROM pn_payment_terms_all pv,
                     pn_leases_all pl ---Added by Sandeep
               WHERE pl.lease_num = var_cur_trx_hdr.INTERFACE_HEADER_ATTRIBUTE10 ---Changed by Sandeep from pv.lease_id
           AND pl.lease_id=pv.lease_id    ----Added by sandeep
             AND pv.attribute7 IS NOT NULL
             AND ROWNUM=1;
         EXCEPTION
            WHEN OTHERS
            THEN
                 fnd_file.put_line (fnd_file.LOG,
                                 'Error to find l  Organization id : '
                              || v_organization_id
                             );
         END;
         
         BEGIN

          SELECT jc.tax_category_id
              INTO v_tax_category_id 
          FROM jai_cmn_tax_ctgs_all jc, pn_payment_terms_all pv
         WHERE pv.lease_id = var_cur_trx_hdr.INTERFACE_HEADER_ATTRIBUTE10
           AND jc.tax_category_name = pv.attribute1
           AND jc.org_id = v_org_id AND ROWNUM=1;
          EXCEPTION
            WHEN OTHERS THEN
             v_tax_category_id:=6;
          END;


        INSERT INTO jai_ar_trxs
                  (customer_trx_id, organization_id,
                   location_id, update_rg_flag, once_completed_flag,
                   vat_invoice_no, vat_invoice_date, total_amount,
                   line_amount, tax_amount, trx_number,
                   batch_source_id, creation_date, created_by,
                   last_update_date, last_updated_by, last_update_login,
                   set_of_books_id,
                   primary_salesrep_id,
                   invoice_currency_code, exchange_rate_type, exchange_date,
                   exchange_rate, created_from
                  )
           VALUES (var_cur_trx_hdr.customer_trx_id, v_organization_id,
                   v_location_id, 'N', 'N',
                   NULL, NULL, v_amount,
                   v_amount, 0, var_cur_trx_hdr.trx_number,
                   var_cur_trx_hdr.batch_source_id, SYSDATE, 0,
                   SYSDATE, 0, 0,
                   var_cur_trx_hdr.set_of_books_id,
                   var_cur_trx_hdr.primary_salesrep_id,
                   var_cur_trx_hdr.invoice_currency_code, NULL, NULL,
                   NULL, 'AXIND_INTERFACE'
                  );



     FOR var_cur_trx_ln IN cur_trx_ln (var_cur_trx_hdr.customer_trx_id)
    LOOP
     fnd_file.put_line
                          (fnd_file.output,
                              'Inserting into LINES : '
                           || var_cur_trx_ln.customer_trx_line_id
                          );
         BEGIN
         INSERT INTO jai_ar_trx_lines
                     (customer_trx_line_id,
                      customer_trx_id,
                      line_number,
                      inventory_item_id,
                      description, unit_code,
                      quantity,
                      unit_selling_price, tax_category_id,
                      line_amount,
                      vat_assessable_value, tax_amount, total_amount,
                      gl_date, auto_invoice_flag,
                      assessable_value, payment_register, excise_invoice_no,
                      excise_invoice_date, creation_date, created_by,
                      last_update_date, last_updated_by, last_update_login,
                      preprinted_excise_inv_no
                     )
              VALUES (var_cur_trx_ln.customer_trx_line_id,
                      var_cur_trx_ln.customer_trx_id,
                      var_cur_trx_ln.line_number,
                      var_cur_trx_ln.inventory_item_id,
                      var_cur_trx_ln.description, var_cur_trx_ln.uom_code,
                      var_cur_trx_ln.quantity_invoiced,
                      var_cur_trx_ln.unit_selling_price, v_tax_category_id,
                      var_cur_trx_ln.extended_amount,
                      var_cur_trx_ln.extended_amount, 0, 0,
                      NVL (v_gl_date, var_cur_trx_hdr.trx_date), 'N',
                      var_cur_trx_ln.extended_amount, NULL, NULL,
                      NULL, SYSDATE, 0,
                      SYSDATE, 0, 0,
                      NULL
                     );
     
    EXCEPTION
        WHEN OTHERS THEN
         NULL;
         END;
       
       COMMIT;
            
         JAI_CMN_TAX_DEFAULTATION_PKG.ja_in_calc_prec_taxes
                     (transaction_name            => 'AR_LINES',
                      p_tax_category_id           => v_tax_category_id,
                      p_header_id                 => var_cur_trx_ln.customer_trx_id,
                      p_line_id                   => var_cur_trx_ln.customer_trx_line_id,
                      p_assessable_value          =>   var_cur_trx_ln.extended_amount
                                                     * var_cur_trx_ln.extended_amount,
                      p_tax_amount                => var_cur_trx_ln.extended_amount,
                      p_inventory_item_id         => v_inventory_item_id,
                      p_line_quantity             => var_cur_trx_ln.quantity_invoiced,
                      p_uom_code                  => var_cur_trx_ln.uom_code,
                      p_vendor_id                 => NULL,
                      p_currency                  => NULL,
                      p_currency_conv_factor      => 1,
                      p_creation_date             => SYSDATE,
                      p_created_by                => 0,
                      p_last_update_date          => SYSDATE,
                      p_last_updated_by           => 0,
                      p_last_update_login         => 0,
                      p_operation_flag            => NULL,
                      p_vat_assessable_value      => var_cur_trx_ln.extended_amount
                     );

           
    COMMIT;
       BEGIN
       SELECT SUM (tax_amount)
         INTO v_line_tax_amt
             FROM jai_ar_trx_tax_lines
            WHERE link_to_cust_trx_line_id = var_cur_trx_hdr.customer_trx_id;
        EXCEPTION
        WHEN OTHERS THEN 
        NULL;
           END;
           
           fnd_file.put_line
                          (fnd_file.output,
                              'Inserting into LINES : '
                           || v_line_tax_amt || var_cur_trx_ln.customer_trx_line_id
                          );

    v_total_tax_amt := v_total_tax_amt + NVL (v_line_tax_amt, 0);
       
       BEGIN

         UPDATE jai_ar_trx_lines
            SET tax_amount = NVL (v_line_tax_amt, 0),
                total_amount = line_amount + NVL (v_line_tax_amt, 0)
          WHERE customer_trx_line_id = var_cur_trx_ln.customer_trx_line_id
            AND customer_trx_id = var_cur_trx_ln.customer_trx_id;
       
       EXCEPTION
          WHEN OTHERS THEN
        NULL;
       END;
        COMMIT;

      BEGIN
            SELECT MAX (line_number)
              INTO v_line_no
              FROM ra_customer_trx_lines_all
             WHERE link_to_cust_trx_line_id = var_cur_trx_ln.customer_trx_line_id;--var_cur_trx_hdr.customer_trx_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               v_line_no := 1;
         END;

     BEGIN
      FOR cur_var_tax_lines IN
            cur_tax_lines (var_cur_trx_ln.customer_trx_line_id)
           LOOP
          
     fnd_file.put_line
                          (fnd_file.output,
                              'Inserting into TRANX LINES : '
                           || var_cur_trx_hdr.customer_trx_id
                          );
           
            v_line_no := NVL (v_line_no, 0) + 1;

            INSERT INTO ra_customer_trx_lines_all
                        (extended_amount,
                         taxable_amount,
                         customer_trx_line_id, last_update_date,
                         last_updated_by, creation_date, created_by,
                         last_update_login, customer_trx_id, line_number,
                         set_of_books_id,
                         link_to_cust_trx_line_id, line_type,
                         org_id, uom_code, autotax, vat_tax_id
                        )
                 VALUES (cur_var_tax_lines.tax_amount,
                         var_cur_trx_ln.extended_amount,
                         ra_customer_trx_lines_s.NEXTVAL, SYSDATE,
                         0, SYSDATE, 0,
                         0, var_cur_trx_ln.customer_trx_id, v_line_no,
                         var_cur_trx_hdr.set_of_books_id,
                         var_cur_trx_ln.customer_trx_line_id, 'TAX',
                         v_org_id, var_cur_trx_ln.uom_code, 'N', v_vat_tax_id
                        );
              fnd_file.put_line
                          (fnd_file.output,
                              'aFTER Inserting into TRANX LINES1 : '
                           || v_tax_type || cur_var_tax_lines.tax_id
                          ); 
 
        BEGIN
            
             
          SELECT tax_type, 
                 tax_account_id
             INTO v_tax_type, 
                 v_tax_account_id
            FROM jai_cmn_taxes_all
           WHERE org_id = v_org_id 
             AND tax_id = cur_var_tax_lines.tax_id;

           END;
            fnd_file.put_line
                          (fnd_file.output,
                              'aFTER Inserting into TRANX LINES2 : '
                           || v_tax_type || cur_var_tax_lines.tax_id
                          );

          BEGIN
          SELECT orr1.attribute_value
            INTO v_tax_service_account
        FROM jai_rgm_org_regns_v orr, jai_rgm_org_regns_v orr1
           WHERE orr.regime_id = orr1.regime_id
             AND orr1.parent_registration_id = orr.registration_id
         AND orr.registration_type = 'TAX_TYPES'
         AND orr1.registration_type = 'ACCOUNTS'
         AND orr1.attribute_code = 'INTERIM_LIABILITY'
         AND orr.organization_id = v_organization_id              ---p_org_id
         AND orr1.organization_id = orr.organization_id
         AND orr.attribute_code = v_tax_type;
          END;
        
        
        fnd_file.put_line
                          (fnd_file.output,
                              'aFTER SELECTING ATTRIBUTE VALUE : '
                           || v_tax_type || v_tax_account_id ||v_tax_service_account
                          );
            
        INSERT INTO ra_cust_trx_line_gl_dist_all
                        (account_class, account_set_flag, acctd_amount,
                         amount,
                         code_combination_id,
                         cust_trx_line_gl_dist_id,
                         cust_trx_line_salesrep_id,
                         customer_trx_id,
                         customer_trx_line_id,
                         gl_date, last_update_date,
                         last_updated_by, creation_date, created_by,
                         last_update_login, org_id, PERCENT, set_of_books_id
                        )
                 VALUES ('TAX', 'N', cur_var_tax_lines.tax_amount,
                         cur_var_tax_lines.tax_amount,
                         NVL (v_tax_service_account,v_tax_account_id),
                         ra_cust_trx_line_gl_dist_s.NEXTVAL,
                         var_cur_trx_hdr.primary_salesrep_id,
                         var_cur_trx_ln.customer_trx_id,
                         ra_customer_trx_lines_s.CURRVAL,
                         NVL (v_gl_date, var_cur_trx_hdr.trx_date), SYSDATE,
                         0, SYSDATE, 0,
                         0, v_org_id, 100, var_cur_trx_hdr.set_of_books_id
                        );    
        END LOOP;
      
      BEGIN
      SELECT SUM(EXTENDED_AMOUNT) 
        INTO v_full_amount        
        FROM ra_customer_trx_lines_all
       WHERE CUSTOMER_TRX_ID =var_cur_trx_hdr.customer_trx_id;
       EXCEPTION
        WHEN OTHERS THEN 
        v_full_amount:=0;
       END;
       
       BEGIN
      
       SELECT NVL(SUM(EXTENDED_AMOUNT),0) 
        INTO v_full_tax_amt        
        FROM ra_customer_trx_lines_all
       WHERE CUSTOMER_TRX_ID =var_cur_trx_hdr.customer_trx_id
         AND LINE_TYPE = 'TAX';
      
         EXCEPTION 
         WHEN OTHERS THEN
         v_full_tax_amt:=0;
         END;
         
      
      UPDATE jai_ar_trxs
         SET tax_amount = v_full_tax_amt,
             total_amount = line_amount + v_full_tax_amt
       WHERE customer_trx_id = var_cur_trx_hdr.customer_trx_id;
         
      UPDATE jai_ar_trx_lines
         SET tax_amount = v_full_tax_amt,
             total_amount = line_amount + v_full_tax_amt
       WHERE customer_trx_id = var_cur_trx_hdr.customer_trx_id;
         
      UPDATE ra_cust_trx_line_gl_dist_all
         SET amount = v_full_amount,
             acctd_amount = acctd_amount + v_full_tax_amt
       WHERE customer_trx_id = var_cur_trx_hdr.customer_trx_id
         AND account_class = 'REC';

      UPDATE ra_customer_trx_all
         SET interface_header_context = ''
       WHERE customer_trx_id = var_cur_trx_hdr.customer_trx_id;

       
--       fnd_file.put_line(fnd_file.output,'AMOUNT_DUE_ORIGINAL->'||AMOUNT_DUE_ORIGINAL);
      fnd_file.put_line(fnd_file.output,'customer_trx_id->'||var_cur_trx_hdr.customer_trx_id);
       fnd_file.put_line(fnd_file.output,'v_full_tax_amt->'||v_full_tax_amt);
       
       
       UPDATE  ar_payment_schedules_all
        SET  AMOUNT_DUE_ORIGINAL=AMOUNT_DUE_ORIGINAL+v_full_tax_amt,
             AMOUNT_DUE_REMAINING=AMOUNT_DUE_REMAINING+v_full_tax_amt,
             TAX_ORIGINAL=v_full_tax_amt,
             TAX_REMAINING=v_full_tax_amt
      WHERE CUSTOMER_TRX_ID = var_cur_trx_hdr.customer_trx_id
        AND  org_id = v_org_id;
    
      COMMIT;
      END;
      END LOOP;
     END LOOP;
     END XXABRL_PNTOAR_INTERFACE; 
/


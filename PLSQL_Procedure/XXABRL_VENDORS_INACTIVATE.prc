CREATE OR REPLACE PROCEDURE APPS.xxabrl_vendors_inactivate (
   x_err_buf    OUT   VARCHAR2,
   x_ret_code   OUT   NUMBER
)
IS
   lv_return_status   VARCHAR2 (1);
   lv_msg_count       NUMBER;
   lv_msg_data        VARCHAR2 (2000);
   --
   lv_vendor_id       ap_suppliers.vendor_id%TYPE;
   lv_vendor_rec      ap_vendor_pub_pkg.r_vendor_rec_type;

   CURSOR xx_rec
   IS
      SELECT asp.segment1, asp.vendor_name, vendor_type_lookup_code,
             asp.creation_date, asp.vendor_id
        FROM apps.ap_suppliers asp, apps.ap_supplier_sites_all asl
       WHERE asp.vendor_id = asl.vendor_id
         AND (   SUBSTR (asp.segment1, 1, 2) = '95'
              OR SUBSTR (asp.segment1, 1, 2) = '97'
             )
         AND asp.end_date_active IS NULL
         AND asl.inactive_date IS NULL
         AND asl.org_id NOT IN (84, 87, 89, 91, 86, 93, 94, 95, 96, 663)
         AND vendor_type_lookup_code = 'MERCHANDISE'
         AND TRUNC (asp.creation_date) >= '10-Sep-14';
BEGIN
   fnd_global.apps_initialize (0, 52299, 200);
   mo_global.init ('S');
   COMMIT;

   FOR xx_rec1 IN xx_rec
   LOOP
      lv_vendor_id := xx_rec1.vendor_id;
      lv_vendor_rec.end_date_active := SYSDATE;        --FND_API.G_MISS_DATE;
      --
      ap_vendor_pub_pkg.update_vendor
                           (p_api_version           => 1.0,
                            p_init_msg_list         => fnd_api.g_true,
                            p_commit                => fnd_api.g_false,
                            p_validation_level      => fnd_api.g_valid_level_full,
                            x_return_status         => lv_return_status,
                            x_msg_count             => lv_msg_count,
                            x_msg_data              => lv_msg_data,
                            p_vendor_rec            => lv_vendor_rec,
                            p_vendor_id             => lv_vendor_id
                           );
      COMMIT;
      --
--      DBMS_OUTPUT.put_line (   lv_return_status
--                            || '-'
--                            || lv_msg_count
--                            || '-'
--                            || lv_msg_data
--                           );
      fnd_file.put_line (fnd_file.LOG,
                         'vendor_blocked: ' || xx_rec1.segment1
                        );
      fnd_file.put_line (fnd_file.LOG,
                            lv_return_status
                         || '-'
                         || lv_msg_count
                         || '-'
                         || lv_msg_data
                        );
   END LOOP;
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      fnd_file.put_line (fnd_file.LOG,
                         'Exception :: ' || SQLCODE || ': ' || SQLERRM
                        );
END; 
/


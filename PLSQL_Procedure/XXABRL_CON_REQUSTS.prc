CREATE OR REPLACE PROCEDURE APPS.xxabrl_con_requsts (
   errbuf    OUT      VARCHAR2,
   retcode   OUT      NUMBER,
   p_nod     IN       NUMBER
)
IS
   CURSOR c1
   IS
      (SELECT a.request_id, a.request_date, a.phase_code, a.status_code,
              a.priority, requested_start_date, d.concurrent_queue_name,
              a.queue_method_code, a.responsibility_id, a.parent_request_id,
              a.controlling_manager, a.completion_text, a.logfile_name,
              a.outfile_name, a.argument_text, a.oracle_process_id,
              a.oracle_session_id, a.cancel_or_hold, a.lfile_size,
              (SELECT responsibility_name
                 FROM apps.fnd_responsibility_vl
                WHERE responsibility_id =
                                      a.responsibility_id)
                                                         responsibility_name,
              (SELECT NAME
                 FROM apps.hr_operating_units
                WHERE organization_id = a.org_id) operating_unit,
              a.concurrent_program_id, b.user_concurrent_program_name,
              a.argument_input_method_code,
              (SELECT user_name || '-' || description
                 FROM apps.fnd_user
                WHERE user_id = a.requested_by) submitted_by,
              a.number_of_arguments, a.actual_start_date,
              a.actual_completion_date, a.argument1, a.argument2,
              a.argument3, a.argument4, a.argument5, a.argument6,
              a.argument7, a.argument8, a.argument9, a.argument10,
              a.argument11, a.argument12, a.argument13, a.argument14,
              a.argument15, a.argument16, a.argument17, a.argument18,
              a.argument19, a.argument20, a.argument21, a.argument22,
              a.argument23, a.outfile_node_name, a.logfile_node_name
         FROM apps.fnd_concurrent_requests a,
              apps.fnd_concurrent_programs_vl b,
              apps.fnd_concurrent_processes c,
              apps.fnd_concurrent_queues d
        WHERE a.concurrent_program_id = b.concurrent_program_id
--          AND TRUNC (a.request_date) = TRUNC (SYSDATE) - 7
----          AND a.request_id = 1195100810
          AND TRUNC (a.actual_completion_date) < TRUNC (SYSDATE - p_nod)
          AND a.controlling_manager = c.concurrent_process_id(+)
          AND c.concurrent_queue_id = d.concurrent_queue_id(+));
BEGIN
   FOR i IN c1
   LOOP
      INSERT INTO xxabrl.xxabrl_concurrent_req
                  (request_id, request_date, phase_code,
                   status_code, priority, requested_start_date,
                   concurrent_queue_name, queue_method_code,
                   responsibility_id, parent_request_id,
                   controlling_manager, completion_text,       --logfile_name,
                   --  outfile_name,
                   argument_text, oracle_process_id,
                   oracle_session_id, cancel_or_hold, lfile_size,
                   responsibility_name, operating_unit,
                   concurrent_program_id, user_concurrent_program_name,
                   argument_input_method_code, submitted_by,
                   number_of_arguments, actual_start_date,
                   actual_completion_date, argument1, argument2,
                   argument3, argument4, argument5, argument6,
                   argument7, argument8, argument9, argument10,
                   argument11, argument12, argument13, argument14,
                   argument15, argument16, argument17, argument18,
                   argument19, argument20, argument21, argument22,
                   argument23, outfile_node_name, logfile_node_name,
                   importdate
                  )
           VALUES (i.request_id, i.request_date, i.phase_code,
                   i.status_code, i.priority, i.requested_start_date,
                   i.concurrent_queue_name, i.queue_method_code,
                   i.responsibility_id, i.parent_request_id,
                   i.controlling_manager, i.completion_text, --i.logfile_name,
                   -- i.outfile_name,
                   i.argument_text, i.oracle_process_id,
                   i.oracle_session_id, i.cancel_or_hold, i.lfile_size,
                   i.responsibility_name, i.operating_unit,
                   i.concurrent_program_id, i.user_concurrent_program_name,
                   i.argument_input_method_code, i.submitted_by,
                   i.number_of_arguments, i.actual_start_date,
                   i.actual_completion_date, i.argument1, i.argument2,
                   i.argument3, i.argument4, i.argument5, i.argument6,
                   i.argument7, i.argument8, i.argument9, i.argument10,
                   i.argument11, i.argument12, i.argument13, i.argument14,
                   i.argument15, i.argument16, i.argument17, i.argument18,
                   i.argument19, i.argument20, i.argument21, i.argument22,
                   i.argument23, i.outfile_node_name, i.logfile_node_name,
                   SYSDATE
                  );

      COMMIT;
   END LOOP;
END; 
/


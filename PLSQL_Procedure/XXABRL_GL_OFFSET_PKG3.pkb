CREATE OR REPLACE PACKAGE BODY APPS.xxabrl_gl_offset_pkg3
AS
/*
=========================================================================================================
||   Filename   : Xxabrl_gl_offset_pkg3.sql
||   Description : Script is used to get GL Data
||
||   Version     Date            Author              Modification
||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
||   1.0.0       23-feb-2010       Govind Biradar    removed created by but opening bal ...not
||   1.0.1       15-jun-2010       Shailesh Bharambe  Added logic to get the customer number and customer name from the extra logic.
||   1.0.2       06-JUL-2010        Govindraj          Added logic to get customer number(as updated by Shailesh earlier)
||   1.0.3       12-JAN-2011       Ravi Jadhav         Addel logic to get the correct Document Sequence Number
 ||  1.0.4       08-FEB-2011        Mitul              Added OU Name
  ||  1.0.5       25-FEB-2011        Mitul            Closing Balance showing wrong
  ||  1.0.6       02-Apr-2011        Mitul            Increased ou_name  Size
  ||  1.0.7       02-AUG-2011    Umamahesh/Narasimha  Added Trx ,Deposit Date and Invoice ,payment Date columns in report

||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
||
||   Usage : This script is used to get the gl account analysys data
||
========================================================================================================*/
   PROCEDURE xxabrl_gl_offset_proc (
      errbuff           OUT   VARCHAR2,
      retcode           OUT   NUMBER,
      p_from_co               VARCHAR2,
      p_to_co                 VARCHAR2,
      p_from_sbu              NUMBER,
      p_to_sbu                NUMBER,
      p_from_location         NUMBER,
      p_to_location           NUMBER,
      p_from_account          NUMBER,
      p_to_account            NUMBER,
      p_from_gl_date          DATE,
      p_to_gl_date            DATE,
      p_batch_name            VARCHAR2
   )
   IS
      query_string        LONG;
      query_string1       LONG;
      order_by_string     LONG;
      batch_string        LONG;
      inv_count           NUMBER;
      co_string           LONG;
      sbu_string          LONG;
      location_string     LONG;
      gl_account_string   LONG;
      gl_date_string      LONG;
      gl_date_string1     LONG;
      closing_balance     NUMBER;
      cr_total            NUMBER;
      dr_total            NUMBER;
      v_dr_amount         NUMBER          := 0;
      v_cr_amount         NUMBER          := 0;

      TYPE ref_cur IS REF CURSOR;

      TYPE ref_cur1 IS REF CURSOR;

      c                   ref_cur;
      c1                  ref_cur1;

      TYPE c_rec IS RECORD (
         v_batch_name            gl_je_batches.NAME%TYPE,
         v_source                gl_je_headers.je_source%TYPE,
         v_category              gl_je_headers.je_category%TYPE,
         v_gl_date               gl_je_headers.default_effective_date%TYPE,
         v_line_number           gl_je_lines.je_line_num%TYPE,
         v_account_code          gl_code_combinations_kfv.concatenated_segments%TYPE,
         v_sbu_desc              VARCHAR2 (250),
         v_location_desc         VARCHAR2 (250),
         v_account_desc          VARCHAR2 (250),
         v_dr_amount             gl_je_lines.accounted_dr%TYPE,
         v_cr_amount             gl_je_lines.accounted_cr%TYPE,
         v_journal_description   gl_je_lines.description%TYPE,
         v_customer_number       VARCHAR2 (250),
         v_customer_name         VARCHAR2 (250),
         v_document_number       gl_je_headers.doc_sequence_value%TYPE,
         v_batch_status          VARCHAR2 (100),
         v_created_by            fnd_user.user_name%TYPE,
         ou_name                 VARCHAR2 (50),
         ae_header_id            NUMBER,
         application_id          NUMBER
      );

      TYPE c_rec1 IS RECORD (
         v_dr_amount   gl_je_lines.accounted_dr%TYPE,
         v_cr_amount   gl_je_lines.accounted_cr%TYPE
      );

      v_rec               c_rec;
      v_rec1              c_rec1;
      v_ledger_id         NUMBER;
      v_resp_id           NUMBER          := fnd_profile.VALUE ('RESP_ID');
      v_ledger_cond       VARCHAR2 (1000);
      l_trx_date          DATE;
      l_deposit_date      DATE;
      l_entity_code       VARCHAR2 (40);
   BEGIN
      query_string :=
         'SELECT AA.batch_name,
AA.SOURCE,
AA.CATEGORY,
AA.gl_date,
AA.line_number,
AA.account_code,
AA.SBU_desc,
AA.Location_desc,
AA.account_desc,
AA.dr_amount,
AA.cr_amount,
AA.journal_description,
AA.Customer_Number,
AA.Customer_Name,
AA.document_number,
AA.batch_status,
AA.Created_By,
(select NAME from hr_operating_units where ORGANIZATION_ID =aa.org_id) OU_NAME,
AA.ae_header_id,
AA.application_id  
FROM
(SELECT GLB.NAME batch_name,
       glh.je_source SOURCE, glh.je_category CATEGORY,
       glh.default_effective_date gl_date,
       gll.je_line_num line_number,
       gl.concatenated_segments account_code,
       (SELECT FFVL.description FROM fnd_flex_values_vl ffvl
        WHERE ffvl.FLEX_VALUE_SET_ID=1013466
        AND ffvl.FLEX_VALUE=gl.SEGMENT3) SBU_desc,
         (SELECT FFVL.description FROM fnd_flex_values_vl ffvl
        WHERE ffvl.FLEX_VALUE_SET_ID=1013467
        AND ffvl.FLEX_VALUE=gl.SEGMENT4) Location_desc,
       (SELECT FFVL.description FROM fnd_flex_values_vl ffvl
        WHERE ffvl.FLEX_VALUE_SET_ID=1013469
        AND ffvl.FLEX_VALUE=gl.SEGMENT6) account_desc,
       decode(xla.accounted_dr,null,decode(xla.accounted_cr,null, gll.accounted_dr),xla.accounted_dr) dr_amount,
       decode(xla.accounted_cr,null,decode(xla.accounted_dr,null, gll.accounted_cr),xla.accounted_cr) cr_amount,
       gll.description journal_description,
      CASE WHEN UPPER(glh.je_source)=''PAYABLES''
            THEN (SELECT po.segment1
                 FROM po_vendors po
                 WHERE po.VENDOR_ID=xla.PARTY_ID
                 AND ROWNUM<2)
            WHEN UPPER(glh.je_source)=''RECEIVABLES''
            THEN /*(SELECT  ar.account_number
                 FROM hz_parties hzp, hz_cust_accounts ar
                 WHERE ar.party_id = hzp.party_id
                 AND ar.cust_account_id=xla.party_id
                 AND ROWNUM<2 ) */
                DECODE(glh.je_category ,''Receipts'',(select ACCOUNT_NUMBER  
                                                      from apps.hz_cust_accounts_all hca
                                                          ,apps.hz_parties hp
                                                          ,apps.ar_cash_receipts_all   acra 
                                                          ,xla.xla_transaction_entities xte
                                                          ,apps.xla_ae_headers xah
                                                      where hca.cust_account_id=acra.PAY_FROM_CUSTOMER
                                                        and hca.PARTY_ID=hp.party_id
                                                        and acra.cash_receipt_id=xte.SOURCE_ID_INT_1
                                                        and xah.ENTITY_ID=xte.ENTITY_ID
                                                        and xah.AE_HEADER_ID=xla.AE_HEADER_ID
                                                      ),
                 (SELECT  ar.account_number
                 FROM hz_parties hzp, hz_cust_accounts ar
                 WHERE ar.party_id = hzp.party_id
                 AND ar.cust_account_id=xla.party_id
                 AND ROWNUM<2 ))
        END AS Customer_Number,
        CASE WHEN UPPER(glh.je_source)=''PAYABLES''
            THEN (SELECT po.vendor_name
                 FROM po_vendors po
                 WHERE po.VENDOR_ID=xla.PARTY_ID
                 )
            WHEN UPPER(glh.je_source)=''RECEIVABLES''
            THEN /*(SELECT  hzp.party_name
                 FROM hz_parties hzp, hz_cust_accounts ar
                 WHERE ar.party_id = hzp.party_id
                 AND ar.cust_account_id=xla.party_id
                 )*/
                 decode( glh.je_category ,''Receipts'',(select hp.party_name  
                                                      from hz_cust_accounts_all hca
                                                          ,hz_parties hp
                                                          ,apps.ar_cash_receipts_all   acra 
                                                          ,xla.xla_transaction_entities xte
                                                          ,xla_ae_headers xah
                                                      where hca.cust_account_id=acra.PAY_FROM_CUSTOMER
                                                        and hca.PARTY_ID=hp.party_id
                                                        and acra.cash_receipt_id=xte.SOURCE_ID_INT_1
                                                        and xah.ENTITY_ID=xte.ENTITY_ID
                                                        and xah.AE_HEADER_ID=xla.AE_HEADER_ID
                                                      )
                 ,(SELECT  hzp.party_name
                 FROM hz_parties hzp, hz_cust_accounts ar
                 WHERE ar.party_id = hzp.party_id
                 AND ar.cust_account_id=xla.party_id
                 ))
         END AS Customer_Name,
       /*CASE WHEN UPPER(glh.je_source)=''PAYABLES''
            THEN (SELECT po.segment1
                 FROM po_vendors po
                 WHERE po.VENDOR_ID=xla.PARTY_ID
                 AND ROWNUM<2)
            WHEN UPPER(glh.je_source)=''RECEIVABLES''
            THEN (SELECT  ar.account_number
                 FROM hz_parties hzp, hz_cust_accounts ar
                 WHERE ar.party_id = hzp.party_id
                 AND ar.cust_account_id=xla.party_id
                 AND ROWNUM<2 )
        END AS Customer_Number,
        CASE WHEN UPPER(glh.je_source)=''PAYABLES''
            THEN (SELECT po.vendor_name
                 FROM po_vendors po
                 WHERE po.VENDOR_ID=xla.PARTY_ID
                 )
            WHEN UPPER(glh.je_source)=''RECEIVABLES''
            THEN (SELECT  hzp.party_name
                 FROM hz_parties hzp, hz_cust_accounts ar
                 WHERE ar.party_id = hzp.party_id
                 AND ar.cust_account_id=xla.party_id
                 )
         END AS Customer_Name,*/
      /* NVL(glir.SUBLEDGER_DOC_SEQUENCE_VALUE,glh.doc_sequence_value) document_number,*/
      /*decode( glh.je_category,''Credit Memos'',(select distinct rac.DOC_SEQUENCE_VALUE 
                                    from 
                                    XLA_AE_HEADERS xxla,
                                    RA_CUST_TRX_LINE_GL_DIST_ALL cust,
                                    RA_CUSTOMER_TRX_ALL rac
                                    where
                                    xxla.EVENT_ID=cust.EVENT_ID and
                                    cust.CUSTOMER_TRX_ID=rac.CUSTOMER_TRX_ID and
                                    cust.ORG_ID=rac.ORG_ID and
                                    xxla.AE_HEADER_ID=xla.AE_HEADER_ID),
                              ''Purchase Invoices'',(SELECT XXLA.DOC_SEQUENCE_VALUE FROM           
                                                        XLA_AE_HEADERS xxla
                                                        WHERE
                                                        XXLA.AE_HEADER_ID=XLA.AE_HEADER_ID )                              
                              ,nvl(glir.SUBLEDGER_DOC_SEQUENCE_VALUE,glh.doc_sequence_value)) document_number,*/
                              nvl(decode( glh.je_category,''Credit Memos'',(select distinct rac.DOC_SEQUENCE_VALUE 
                                    from 
                                    XLA_AE_HEADERS xxla,
                                    RA_CUST_TRX_LINE_GL_DIST_ALL cust,
                                    RA_CUSTOMER_TRX_ALL rac
                                    where
                                    xxla.EVENT_ID=cust.EVENT_ID and
                                    cust.CUSTOMER_TRX_ID=rac.CUSTOMER_TRX_ID and
                                    cust.ORG_ID=rac.ORG_ID and
                                    xxla.AE_HEADER_ID=xla.AE_HEADER_ID)
                              ,(SELECT XXLA.DOC_SEQUENCE_VALUE FROM           
                                                        XLA_AE_HEADERS xxla
                                                        WHERE
                                                        XXLA.AE_HEADER_ID=XLA.AE_HEADER_ID )                              
                              ),nvl(glir.SUBLEDGER_DOC_SEQUENCE_VALUE,glh.doc_sequence_value)) document_number,
       DECODE (GLB.status,
               ''P'', ''Posted'',
               ''U'', ''Unposted'',
               GLB.status
              ) batch_status,
         null Created_By,
          gl.segment1,
        gl.segment3 SBU,
       gl.segment4 LOCATION,
       gl.segment6 GL_account,
            GLH.LEDGER_ID LEDGER_ID,gl.CODE_COMBINATION_ID,xla.CREATED_BY CREATED_BYxla ,gll.CREATED_BY CREATED_BYgll,
               (select distinct SECURITY_ID_INT_1 from xla.xla_transaction_entities xlat,xla_ae_headers xlah where 
                xlat.APPLICATION_ID=xla.APPLICATION_ID
   and xlat.ENTITY_ID=xlah.ENTITY_ID 
   and xlah.AE_HEADER_ID=xla.AE_HEADER_ID
   )    org_id,
   xla.AE_HEADER_ID,
   xla.APPLICATION_ID
  FROM gl_je_headers glh,
       gl_je_lines gll,
       gl_code_combinations_kfv gl,
       gl_je_batches GLB,
       xla_ae_lines xla,
   --    xla_ae_headers xlah,
       gl_import_references glir
 WHERE glh.je_header_id = gll.je_header_id
   AND gll.je_header_id = glir.je_header_id (+)
   AND gll.je_line_num = glir.je_line_num (+)
   AND glir.gl_sl_link_id = xla.gl_sl_link_id (+)
   AND glir.gl_sl_link_table = xla.gl_sl_link_table (+)
   AND gl.code_combination_id = gll.code_combination_id
 --  and xla.AE_HEADER_ID=xlah.AE_HEADER_ID
   AND glh.je_batch_id = GLB.je_batch_id
   AND UPPER(glh.je_source) IN (''PAYABLES'',''RECEIVABLES'')
   AND NVL(xla.accounted_cr,999999)!=0
   AND NVL(xla.accounted_dr,999999)!=0
UNION ALL
SELECT GLB.NAME batch_name,
       glh.je_source SOURCE, glh.je_category CATEGORY,
       glh.default_effective_date gl_date,
       gll.je_line_num line_number,
       gl.concatenated_segments account_code,
       (SELECT FFVL.description FROM fnd_flex_values_vl ffvl
        WHERE ffvl.FLEX_VALUE_SET_ID=1013466
        AND ffvl.FLEX_VALUE=gl.SEGMENT3) SBU_desc,
         (SELECT FFVL.description FROM fnd_flex_values_vl ffvl
        WHERE ffvl.FLEX_VALUE_SET_ID=1013467
        AND ffvl.FLEX_VALUE=gl.SEGMENT4) Location_desc,
       (SELECT FFVL.description FROM fnd_flex_values_vl ffvl
        WHERE ffvl.FLEX_VALUE_SET_ID=1013469
        AND ffvl.FLEX_VALUE=gl.SEGMENT6) account_desc,
      GLL.ACCOUNTED_DR dr_amount,
      GLL.ACCOUNTED_CR cr_amount,
      gll.description journal_description,
      NULL customer_name,
      NULL customer_Number,
       glh.doc_sequence_value document_number,
       DECODE (GLB.status,
               ''P'', ''Posted'',
               ''U'', ''Unposted'',
               GLB.status
              ) batch_status,
     fu.user_name Created_By,
      gl.segment1,
     gl.segment3 SBU,
     gl.segment4 LOCATION,
     gl.segment6 GL_account,
     GLH.LEDGER_ID LEDGER_ID,null,null,null,null,0,0
FROM
       gl_je_headerS glh,
       gl_je_lines gll,
       gl_code_combinations_kfv gl,
       fnd_user fu,
       gl_je_batches GLB
WHERE glh.je_header_id = gll.je_header_id
  AND gl.code_combination_id = gll.code_combination_id
  AND glh.je_batch_id = GLB.je_batch_id
  AND gll.created_by=fu.user_id
 -- AND NVL (glh.accrual_rev_status, ''NR'') <> ''R'' 
  AND UPPER(glh.je_source) NOT IN (''PAYABLES'',''RECEIVABLES'')
) AA
WHERE 1=1 ';
      query_string1 :=
         'SELECT 
SUM(dr_amount) dr_amount,
sum(cr_amount) cr_amount 
from
(SELECT GLB.NAME batch_name,
        glh.default_effective_date gl_date,
        decode(xla.accounted_dr,null,decode(xla.accounted_cr,null, gll.accounted_dr),xla.accounted_dr) dr_amount,
        decode(xla.accounted_cr,null,decode(xla.accounted_dr,null, gll.accounted_cr),xla.accounted_cr) cr_amount,
        GLH.LEDGER_ID LEDGER_ID,
         gl.segment1,
        gl.segment3 SBU,
       gl.segment4 LOCATION,
       gl.segment6 GL_account      
  FROM gl_je_headers glh,
       gl_je_lines gll,
       gl_code_combinations_kfv gl,
       gl_je_batches GLB,
       xla_ae_lines xla,
       gl_import_references glir
 WHERE glh.je_header_id = gll.je_header_id
   AND gll.je_header_id = glir.je_header_id (+)
   AND gll.je_line_num = glir.je_line_num (+)
   AND glir.gl_sl_link_id = xla.gl_sl_link_id (+)
   AND glir.gl_sl_link_table = xla.gl_sl_link_table (+)
   AND gl.code_combination_id = gll.code_combination_id
   AND glh.je_batch_id = GLB.je_batch_id
   AND gl_account_type IN (''L'',''A'',''O'')   -----added on 15-apr-15
   AND UPPER(glh.je_source) IN (''PAYABLES'',''RECEIVABLES'')
   AND NVL(xla.accounted_cr,999999)!=0
   AND NVL(xla.accounted_dr,999999)!=0
UNION ALL
SELECT GLB.NAME batch_name,
       glh.default_effective_date gl_date,
       GLL.ACCOUNTED_DR dr_amount,
       GLL.ACCOUNTED_CR cr_amount,
       GLH.LEDGER_ID LEDGER_ID,
       gl.segment1,
        gl.segment3 SBU,
       gl.segment4 LOCATION,
       gl.segment6 GL_account
 FROM
       gl_je_headerS glh,
       gl_je_lines gll,
       gl_code_combinations_kfv gl,
       fnd_user fu,
       gl_je_batches GLB
WHERE glh.je_header_id = gll.je_header_id
  AND gl.code_combination_id = gll.code_combination_id
  AND glh.je_batch_id = GLB.je_batch_id
   AND gl_account_type IN (''L'',''A'',''O'')  --added on 15-apr-15
  AND gll.created_by=fu.user_id
 -- AND NVL (glh.accrual_rev_status, ''NR'') <> ''R'' 
  AND UPPER(glh.je_source) NOT IN (''PAYABLES'',''RECEIVABLES'')
) aa
WHERE 1=1';

      BEGIN
         SELECT default_ledger_id
           INTO v_ledger_id
           FROM fnd_profile_option_values fpop,
                fnd_profile_options fpo,
                gl_access_sets gas
          WHERE            --level_value=51664--cp_resp_id  --52299 -- resp id
                fpop.profile_option_id = fpo.profile_option_id
            AND gas.access_set_id = fpop.profile_option_value
            AND level_value = v_resp_id                                --51664
            AND profile_option_name = 'GL_ACCESS_SET_ID';
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (fnd_file.LOG, 'NO LEDGER FOUND. ');
            v_ledger_id := 0;
      END;

      co_string :=
            ' AND AA.segment1 BETWEEN '''
         || p_from_co
         || ''' AND '''
         || p_to_co
         || '''';
      sbu_string :=
         ' AND AA.SBU BETWEEN ''' || p_from_sbu || ''' AND ''' || p_to_sbu
         || '''';
      location_string :=
            ' AND TO_NUMBER(AA.LOCATION) BETWEEN '''
         || p_from_location
         || ''' AND '''
         || p_to_location
         || '''';
      gl_account_string :=
            ' AND AA.GL_ACCOUNT BETWEEN '''
         || p_from_account
         || ''' AND '''
         || p_to_account
         || '''';
      gl_date_string :=
            ' AND to_date(to_char(AA.Gl_Date,''DD-MON-YY'')) BETWEEN '''
         || p_from_gl_date
         || ''' AND '''
         || p_to_gl_date
         || '''';
      gl_date_string1 :=
            ' AND to_date(to_char(AA.Gl_Date,''DD-MON-YY'')) < '''
         || p_from_gl_date
         || '''';
      batch_string := ' AND AA.batch_name = ''' || p_batch_name || '''';
      order_by_string :=
                ' ORDER BY AA.batch_name,AA.SOURCE,AA.CATEGORY,AA.line_number';
      v_ledger_cond := ' AND AA.LEDGER_ID = ' || v_ledger_id;
      -- ADDING LEDGER ID CONDITION
      query_string := query_string || v_ledger_cond;
      query_string1 := query_string1 || v_ledger_cond;

      -- Checking For the from SBU and To SBU Parameter
      IF (p_from_co IS NULL AND p_to_co IS NULL)
      THEN
         query_string := query_string;
         query_string1 := query_string1;
      ELSE
         query_string := query_string || co_string;
         query_string1 := query_string1 || co_string;
      END IF;

      IF (p_from_sbu IS NULL AND p_to_sbu IS NULL)
      THEN
         query_string := query_string;
         query_string1 := query_string1;
      ELSE
         query_string := query_string || sbu_string;
         query_string1 := query_string1 || sbu_string;
      END IF;

      -- Checking For the from LOCATION and To LOCATION Parameter
      IF (p_from_location IS NULL AND p_to_location IS NULL)
      THEN
         query_string := query_string;
         query_string1 := query_string1;
      ELSE
         query_string := query_string || location_string;
         query_string1 := query_string1 || location_string;
      END IF;

      -- Checking For the from GL ACCOUNT and To GL ACCOUNT Parameter
      IF (p_from_account IS NULL AND p_to_account IS NULL)
      THEN
         query_string := query_string;
         query_string1 := query_string1;
      ELSE
         query_string := query_string || gl_account_string;
         query_string1 := query_string1 || gl_account_string;
      END IF;

      -- Checking  For Batch name Parameter
      IF (p_batch_name IS NULL)
      THEN
         query_string := query_string;
         query_string1 := query_string1;
      ELSE
         query_string := query_string || batch_string;
         query_string1 := query_string1 || batch_string;
      END IF;

      -- Checking GL Date Parameter
      IF (p_from_gl_date IS NOT NULL AND p_to_gl_date IS NOT NULL)
      THEN
         query_string := query_string || gl_date_string;
         query_string1 := query_string1 || gl_date_string1;
      ELSIF (p_from_gl_date IS NULL AND p_to_gl_date IS NULL)
      THEN
         query_string := query_string;
         query_string1 := query_string1;
      END IF;

      fnd_file.put_line (fnd_file.output, 'ABRL General Ledger Offset Report');
      fnd_file.put_line (fnd_file.output, ' ');
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'As on Date'
                         || CHR (9)
                         || SYSDATE
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output, ' ');
      fnd_file.put_line (fnd_file.output, 'REPORT PARAMETRS ');
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'From CO'
                         || CHR (9)
                         || p_from_co
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'To CO'
                         || CHR (9)
                         || p_to_co
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'From SBU'
                         || CHR (9)
                         || p_from_sbu
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'To SBU'
                         || CHR (9)
                         || p_to_sbu
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'From Location'
                         || CHR (9)
                         || p_from_location
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'To Location'
                         || CHR (9)
                         || p_to_location
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'From GL Account'
                         || CHR (9)
                         || p_from_account
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'To GL Account'
                         || CHR (9)
                         || p_to_account
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'From GL Date'
                         || CHR (9)
                         || p_from_gl_date
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'To GL Date'
                         || CHR (9)
                         || p_to_gl_date
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Batch Name'
                         || CHR (9)
                         || p_batch_name
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output, ' ');
      fnd_file.put_line (fnd_file.output, ' ');
      fnd_file.put_line (fnd_file.output,
                            'Batch Name'
                         || CHR (9)
                         || 'Source'
                         || CHR (9)
                         || 'Category'
                         || CHR (9)
                         || 'GL Date'
                         || CHR (9)
                         || 'Line Number'
                         || CHR (9)
                         || 'Account Code'
                         || CHR (9)
                         || 'State SBU'
                         || CHR (9)
                         || 'Location'
                         || CHR (9)
                         || 'GL Account'
                         || CHR (9)
                         || 'Dr Amount'
                         || CHR (9)
                         || 'Cr Amount'
                         || CHR (9)
                         || 'Journal Description'
                         || CHR (9)
                         || 'Vendor \ Customer Name'
                         || CHR (9)
                         || 'Vendor \ Customer Number'
                         || CHR (9)
                         || 'Document Number'
                         || CHR (9)
                         || 'Batch Status'
                         || CHR (9)
                         || 'Created by (Entry in Subledger)'
                         || CHR (9)
                         || '(Trx\Invoice)Date'
                         || CHR (9)
                         || '(Deposit\Payment)Date'
                         || CHR (9)
                         || 'OU Name'
                        );
      query_string := query_string;
      fnd_file.put_line (fnd_file.LOG, 'string: ' || query_string);
      inv_count := 0;
      cr_total := 0;
      dr_total := 0;

      OPEN c1 FOR query_string1;

      LOOP
         FETCH c1
          INTO v_rec1;

         EXIT WHEN c1%NOTFOUND;

         IF v_rec1.v_dr_amount > NVL (v_rec1.v_cr_amount, 0)
         THEN
            v_dr_amount :=
                     NVL (v_rec1.v_dr_amount, 0)
                     - NVL (v_rec1.v_cr_amount, 0);
            v_cr_amount := 0;
         ELSE
            v_dr_amount := 0;
            v_cr_amount := v_rec1.v_cr_amount - v_rec1.v_dr_amount;
         END IF;
      END LOOP;

      CLOSE c1;

      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Opening Balance'
                         || CHR (9)
                         || v_dr_amount
                         || CHR (9)
                         || v_cr_amount
                        );

      OPEN c FOR query_string;

      LOOP
         FETCH c
          INTO v_rec;

         EXIT WHEN c%NOTFOUND;

         --1.0.7       02-AUG-2011    Umamahesh/Narasimha  Added Trx ,Deposit Date and Invoice ,payment Date columns in report
         --Fnd_File.put_line (Fnd_File.log,1||'-'||v_rec.AE_HEADER_ID||'-'||v_rec.application_id||'TRX_DATE:'||L_TRX_DATE||'~Deposite date'||l_deposit_date||'Documents num'||v_rec.v_document_number);
         IF UPPER (v_rec.v_source) = 'RECEIVABLES'
         THEN
            BEGIN
               SELECT DISTINCT TO_CHAR (rac.trx_date)
                          INTO l_trx_date
                          FROM xla.xla_transaction_entities xlat,
                               xla_ae_headers xlah,
                               ra_customer_trx_all rac
                         WHERE 1 = 1
                           --xlat.APPLICATION_ID=V_REC.APPLICATION_ID
                           AND xlat.entity_id = xlah.entity_id
                           AND xlah.ae_header_id = v_rec.ae_header_id
                           and rac.DOC_SEQUENCE_VALUE=v_rec.v_document_number --added on '12-Jan-2017'
                           AND xlat.source_id_int_1 = rac.customer_trx_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_trx_date := NULL;
            END;

            BEGIN
               SELECT DISTINCT TO_CHAR (ract.deposit_date)
                          INTO l_deposit_date
                          FROM xla.xla_transaction_entities xlat,
                               xla_ae_headers xlah,
                               ar_cash_receipts_all ract
                         WHERE 1 = 1
                           --xlat.APPLICATION_ID=V_REC.APPLICATION_ID
                           AND xlat.entity_id = xlah.entity_id
                           AND xlah.ae_header_id = v_rec.ae_header_id
                           AND xlat.source_id_int_1 = ract.cash_receipt_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_deposit_date := NULL;
            END;
         -- Fnd_File.put_line (Fnd_File.log,1.2||'-'||v_rec.AE_HEADER_ID||'-'||v_rec.application_id||'TRX_DATE:'||L_TRX_DATE||'~Deposite date'||l_deposit_date||'Documents num'||v_rec.v_document_number);
         ELSIF UPPER (v_rec.v_source) = 'PAYABLES'
         THEN
            BEGIN
               SELECT entity_code
                 INTO l_entity_code
                 FROM xla.xla_transaction_entities xlat
                WHERE xlat.entity_id IN (
                         SELECT xlah.entity_id
                           FROM xla_ae_headers xlah
                          WHERE xlah.ae_header_id = v_rec.ae_header_id
                            AND xlah.application_id = v_rec.application_id)
                  AND xlat.application_id = v_rec.application_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_entity_code := NULL;
--Fnd_File.put_line (Fnd_File.log,2.3||'-'||v_rec.AE_HEADER_ID||'-'||v_rec.application_id||'TRX_DATE:'||L_TRX_DATE||'~Deposite date'||l_deposit_date||'Documents num'||v_rec.v_document_number);
            END;

            IF l_entity_code = 'AP_INVOICES'
            THEN
               --Fnd_File.put_line (Fnd_File.log,1.3||'-'||v_rec.AE_HEADER_ID||'-'||v_rec.application_id||'TRX_DATE:'||L_TRX_DATE||'~Deposite date'||l_deposit_date||'Documents num'||v_rec.v_document_number);
               -------Deposit date start
               BEGIN
                  SELECT DISTINCT TO_CHAR (aca.check_date)
                             INTO l_deposit_date
                             FROM xla.xla_transaction_entities xlat,
                                  xla_ae_headers xlah,
                                  ap_invoices_all aia,
                                  ap_invoice_payments_all aip,
                                  ap_checks_all aca
                            WHERE 1 = 1
                              AND xlat.application_id = v_rec.application_id
                              AND xlat.entity_id = xlah.entity_id
                              AND xlah.ae_header_id = v_rec.ae_header_id
                              AND xlat.source_id_int_1 = aia.invoice_id
                              AND xlat.entity_code = 'AP_INVOICES'
                              --AND xlat.SOURCE_ID_INT_1=2056023
                              AND aip.invoice_id = aia.invoice_id
                              AND aip.check_id = aca.check_id;
--Fnd_File.put_line (Fnd_File.log,2.2||'-'||v_rec.AE_HEADER_ID||'-'||v_rec.application_id||'TRX_DATE:'||L_TRX_DATE||'~Deposite date'||l_deposit_date||'Documents num'||v_rec.v_document_number);
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     l_deposit_date := NULL;
                  WHEN TOO_MANY_ROWS
                  THEN
                     BEGIN
                        SELECT DISTINCT TO_CHAR (aca.check_date)
                                   INTO l_deposit_date
                                   FROM xla.xla_transaction_entities xlat,
                                        xla_ae_headers xlah,
                                        ap_invoices_all aia,
                                        ap_invoice_payments_all aip,
                                        ap_checks_all aca
                                  WHERE 1 = 1
                                    AND xlat.application_id =
                                                          v_rec.application_id
                                    AND xlat.entity_id = xlah.entity_id
                                    AND xlah.ae_header_id = v_rec.ae_header_id
                                    AND xlat.source_id_int_1 = aia.invoice_id
                                    AND xlat.entity_code = 'AP_INVOICES'
                                    --AND xlat.SOURCE_ID_INT_1=2056023
                                    AND aip.invoice_id = aia.invoice_id
                                    AND aip.check_id = aca.check_id
                                    --AND AIP.INVOICE_BASE_AMOUNT >0
                                    AND ROWNUM = 1;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           l_deposit_date := NULL;
                     END;
                  WHEN OTHERS
                  THEN
                     l_deposit_date := NULL;
               END;

               --Fnd_File.put_line (Fnd_File.log,1.4||'-'||v_rec.AE_HEADER_ID||'-'||v_rec.application_id||'TRX_DATE:'||L_TRX_DATE||'~Deposite date'||l_deposit_date||'Documents num'||v_rec.v_document_number);

               -----END
               BEGIN
                  SELECT DISTINCT TO_CHAR (aia.invoice_date)
                             INTO l_trx_date
                             FROM xla.xla_transaction_entities xlat,
                                  xla_ae_headers xlah,
                                  ap_invoices_all aia
                            WHERE 1 = 1
                              AND xlat.application_id = v_rec.application_id
                              AND xlat.entity_id = xlah.entity_id
                              AND xlah.ae_header_id = v_rec.ae_header_id
                              AND xlat.source_id_int_1 = aia.invoice_id
                              AND xlat.entity_code = 'AP_INVOICES'
                                                                  --AND xlat.SOURCE_ID_INT_1=2056023
                  ;
--Fnd_File.put_line (Fnd_File.log,1.5||'-'||v_rec.AE_HEADER_ID||'-'||v_rec.application_id||'TRX_DATE:'||L_TRX_DATE||'~Deposite date'||l_deposit_date||'Documents num'||v_rec.v_document_number);
               EXCEPTION
                  WHEN TOO_MANY_ROWS
                  THEN
                     BEGIN
                        SELECT DISTINCT TO_CHAR (aia.invoice_date)
                                   INTO l_trx_date
                                   FROM xla.xla_transaction_entities xlat,
                                        xla_ae_headers xlah,
                                        ap_invoices_all aia
                                  WHERE 1 = 1
                                    AND xlat.application_id =
                                                          v_rec.application_id
                                    AND xlat.entity_id = xlah.entity_id
                                    AND xlah.ae_header_id = v_rec.ae_header_id
                                    AND xlat.source_id_int_1 = aia.invoice_id
                                    AND xlat.entity_code = 'AP_INVOICES'
                                    AND ROWNUM = 1
                                                  --AND xlat.SOURCE_ID_INT_1=2056023
                        ;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           l_trx_date := NULL;
                     END;
                  WHEN OTHERS
                  THEN
                     l_trx_date := NULL;
               END;
--Fnd_File.put_line (Fnd_File.log,1.6||'-'||v_rec.AE_HEADER_ID||'-'||v_rec.application_id||'TRX_DATE:'||L_TRX_DATE||'~Deposite date'||l_deposit_date||'Documents num'||v_rec.v_document_number);
            ELSE
               BEGIN
                  SELECT DISTINCT TO_CHAR (aia.invoice_date)
                             INTO l_trx_date
                             FROM xla.xla_transaction_entities xlat,
                                  xla_ae_headers xlah,
                                  ap_invoice_payments_all aip,
                                  ap_invoices_all aia,
                                  ap_checks_all aca
                            WHERE 1 = 1
                              AND xlat.application_id = v_rec.application_id
                              AND xlat.entity_id = xlah.entity_id
                              AND xlah.ae_header_id = v_rec.ae_header_id
                              AND xlat.source_id_int_1 = aca.check_id
                              AND aip.invoice_id = aia.invoice_id
                              AND xlat.entity_code = 'AP_PAYMENTS'
                              AND aip.check_id = aca.check_id
                                                             --AND xlat.SOURCE_ID_INT_1=2056023
                  ;
               EXCEPTION
                  WHEN TOO_MANY_ROWS
                  THEN
                     BEGIN
                        SELECT DISTINCT TO_CHAR (aia.invoice_date)
                                   INTO l_trx_date
                                   FROM xla.xla_transaction_entities xlat,
                                        xla_ae_headers xlah,
                                        ap_invoice_payments_all aip,
                                        ap_invoices_all aia,
                                        ap_checks_all aca
                                  WHERE 1 = 1
                                    AND xlat.application_id =
                                                          v_rec.application_id
                                    AND xlat.entity_id = xlah.entity_id
                                    AND xlah.ae_header_id = v_rec.ae_header_id
                                    AND xlat.source_id_int_1 = aca.check_id
                                    AND aip.invoice_id = aia.invoice_id
                                    AND xlat.entity_code = 'AP_PAYMENTS'
                                    AND aip.check_id = aca.check_id
                                    AND aia.invoice_amount > 0
                                    AND ROWNUM = 1
                                                  --AND xlat.SOURCE_ID_INT_1=2056023
                        ;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           l_trx_date := NULL;
                     END;
--Fnd_File.put_line (Fnd_File.log,1.7||'-'||v_rec.AE_HEADER_ID||'-'||v_rec.application_id||'TRX_DATE:'||L_TRX_DATE||'~Deposite date'||l_deposit_date||'Documents num'||v_rec.v_document_number);
                  WHEN OTHERS
                  THEN
                     l_trx_date := NULL;
               END;
            END IF;

            IF l_entity_code = 'AP_PAYMENTS'
            THEN
----Deposite details

               --Fnd_File.put_line (Fnd_File.log,1.8||'-'||v_rec.AE_HEADER_ID||'-'||v_rec.application_id||'TRX_DATE:'||L_TRX_DATE||'~Deposite date'||l_deposit_date||'Documents num'||v_rec.v_document_number);
               BEGIN
                  SELECT DISTINCT TO_CHAR (aca.check_date)
                             INTO l_deposit_date
                             FROM xla.xla_transaction_entities xlat,
                                  xla_ae_headers xlah,
                                  ap_invoices_all aia,
                                  ap_invoice_payments_all aip,
                                  ap_checks_all aca
                            WHERE 1 = 1
                              AND xlat.application_id = v_rec.application_id
                              AND xlat.entity_id = xlah.entity_id
                              AND xlah.ae_header_id = v_rec.ae_header_id
                              AND xlat.source_id_int_1 = aca.check_id
                              AND xlat.entity_code = 'AP_PAYMENTS'
                              --AND xlat.SOURCE_ID_INT_1=2056023
                              AND aip.invoice_id = aia.invoice_id
                              AND aip.check_id = aca.check_id;
--AND AL.AE_LINE_NUM=
--Fnd_File.put_line (Fnd_File.log,1.9||'-'||v_rec.AE_HEADER_ID||'-'||v_rec.application_id||'TRX_DATE:'||L_TRX_DATE||'~Deposite date'||l_deposit_date||'Documents num'||v_rec.v_document_number);
               EXCEPTION
                  WHEN TOO_MANY_ROWS
                  THEN
                     BEGIN
                        SELECT DISTINCT TO_CHAR (aca.check_date)
                                   INTO l_deposit_date
                                   FROM xla.xla_transaction_entities xlat,
                                        xla_ae_headers xlah,
                                        ap_invoices_all aia,
                                        ap_invoice_payments_all aip,
                                        ap_checks_all aca
                                  WHERE 1 = 1
                                    AND xlat.application_id =
                                                          v_rec.application_id
                                    AND xlat.entity_id = xlah.entity_id
                                    AND xlah.ae_header_id = v_rec.ae_header_id
                                    AND xlat.source_id_int_1 = aca.check_id
                                    AND xlat.entity_code = 'AP_PAYMENTS'
                                    --AND xlat.SOURCE_ID_INT_1=2056023
                                    AND aip.invoice_id = aia.invoice_id
                                    AND aip.check_id = aca.check_id
                                    --AND AIA.INVOICE_AMOUNT >0
                                    AND ROWNUM = 1;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           l_deposit_date := NULL;
                     END;
--AND AL.AE_LINE_NUM=
                  WHEN OTHERS
                  THEN
                     l_deposit_date := NULL;
               END;
--Fnd_File.put_line (Fnd_File.log,11||'-'||v_rec.AE_HEADER_ID||'-'||v_rec.application_id||'TRX_DATE:'||L_TRX_DATE||'~Deposite date'||l_deposit_date||'Documents num'||v_rec.v_document_number);
-------problem
--  L_DEPOSIT_DATE :=null;
            END IF;
         ELSE
            l_trx_date := NULL;
            l_deposit_date := NULL;
         END IF;

-- 1.0.7       02-AUG-2011    Umamahesh/Narasimha  Added Trx ,Deposit Date and Invoice ,payment Date columns in report
--Fnd_File.put_line (Fnd_File.log,2||'-'||v_rec.AE_HEADER_ID||'-'||v_rec.application_id||'TRX_DATE:'||L_TRX_DATE||'~Deposite date'||l_deposit_date||'Documents num'||v_rec.v_document_number);
         fnd_file.put_line (fnd_file.output,
                               v_rec.v_batch_name
                            || CHR (9)
                            || v_rec.v_source
                            || CHR (9)
                            || v_rec.v_category
                            || CHR (9)
                            || v_rec.v_gl_date
                            || CHR (9)
                            || v_rec.v_line_number
                            || CHR (9)
                            || v_rec.v_account_code
                            || CHR (9)
                            || v_rec.v_sbu_desc
                            || CHR (9)
                            || v_rec.v_location_desc
                            || CHR (9)
                            || v_rec.v_account_desc
                            || CHR (9)
                            || v_rec.v_dr_amount
                            || CHR (9)
                            || v_rec.v_cr_amount
                            || CHR (9)
                            || v_rec.v_journal_description
                            || CHR (9)
                            || v_rec.v_customer_number
                            || CHR (9)
                            || v_rec.v_customer_name
                            || CHR (9)
                            || v_rec.v_document_number
                            || CHR (9)
                            || v_rec.v_batch_status
                            || CHR (9)
                            || v_rec.v_created_by
                            || CHR (9)
                            || l_trx_date
                            || CHR (9)
                            || l_deposit_date
                            || CHR (9)
                            || v_rec.ou_name
                           );
         cr_total := cr_total + NVL (v_rec.v_cr_amount, 0);
         dr_total := dr_total + NVL (v_rec.v_dr_amount, 0);
         inv_count := inv_count + 1;
      END LOOP;

      cr_total := NVL (cr_total, 0) + NVL (v_cr_amount, 0);
      dr_total := NVL (dr_total, 0) + NVL (v_dr_amount, 0);
      fnd_file.put_line (fnd_file.LOG, 'CR Total : ' || cr_total);
      fnd_file.put_line (fnd_file.LOG, 'DR Total : ' || dr_total);
      fnd_file.put_line (fnd_file.output, ' ');
      fnd_file.put_line (fnd_file.output,
                            ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || 'Grand Total'
                         || CHR (9)
                         || dr_total
                         || CHR (9)
                         || cr_total
                        );
      closing_balance := (cr_total) - (dr_total);
      closing_balance := closing_balance * (-1);
      fnd_file.put_line (fnd_file.output, ' ');
      fnd_file.put_line (fnd_file.output,
                            ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || 'Closing Balance  '
                         || CHR (9)
                         || closing_balance
                        );

      CLOSE c;
   END xxabrl_gl_offset_proc;
END xxabrl_gl_offset_pkg3; 
/


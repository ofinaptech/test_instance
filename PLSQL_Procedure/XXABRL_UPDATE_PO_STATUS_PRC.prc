CREATE OR REPLACE PROCEDURE APPS.xxabrl_update_po_status_prc (
   errbuf    VARCHAR2
-- is a Out parameter which is used to store error message whenever a program gets into an exception block
,
   retcode   NUMBER
-- is a Out parameter which is used to record the status of the concurrent request
)
IS
/************************************************************************************************
                              Aditya Birla Retail Limited
  *************************************************************************************************
  =========================================================================================================
  ||   Filename     : XXABRL_UPDATE_PO_STATUS_PRC.sql
  ||   Description : Script is used to Update PO Status for the POs which has expied but still in OPEN Status.
  ||
  ||   Version     Date            Author              Modification
  ||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
  ||   1.0.0       06-AUG-2012    Vikash Kumar        New Development
   ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
   Usage:- Script is used to Update CLOSED_CODE for POs which has expied but they are still in OPEN status
  ========================================================================================================*/---declaring local variable
   l_record_updated   NUMBER := 0;                      --initiallized with 0

   ---Defining Cursor Query to get all POs which has been expired but still in OPEN status---
   CURSOR po_expiry_cur
   IS
      SELECT ph.po_header_id, ph.segment1, ph.attribute12, ph.closed_code
        FROM apps.po_headers_all ph
       WHERE 1 = 1
         AND ph.authorization_status = 'APPROVED'
         AND TRUNC (SYSDATE) - 21 > TRUNC (TO_DATE (ph.attribute12))
         --As per Bala sir instructions, i have changed the days from 7 to 21
         AND NVL (ph.closed_code, 'OPEN') = 'OPEN'
         AND ph.attribute12 IS NOT NULL;
---end of cursor query----
BEGIN                --Main begin for Procedure XXABRL_UPDATE_PO_STATUS_PRC---
--fetching cusror data
   FOR po_expiry_rec IN po_expiry_cur
   LOOP
--Checking the POs, whether there is any record is available for Updation or not--
      IF po_expiry_rec.po_header_id IS NOT NULL
      THEN
---Starts of Updation--

         --Updating PO_HEADERS_ALL
         BEGIN
            UPDATE apps.po_headers_all poh
               SET poh.closed_code = 'CLOSED'
             WHERE 1 = 1 AND poh.po_header_id = po_expiry_rec.po_header_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               fnd_file.put_line
                         (fnd_file.LOG,
                          'exception found during Updation of PO_HEADERS_ALL'
                         );
         END;

--Updating PO_LINES_ALL
         BEGIN
            UPDATE apps.po_lines_all pol
               SET pol.closed_code = 'CLOSED'
             WHERE 1 = 1 AND pol.po_header_id = po_expiry_rec.po_header_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               fnd_file.put_line
                           (fnd_file.LOG,
                            'exception found during Updation of PO_LINES_ALL'
                           );
         END;

--Updating PO_LINE_LOCATIONS_ALL
         BEGIN
            UPDATE apps.po_line_locations_all plla
               SET plla.closed_code = 'CLOSED'
             WHERE 1 = 1 AND plla.po_header_id = po_expiry_rec.po_header_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               fnd_file.put_line
                  (fnd_file.LOG,
                   'exception found during Updation of PO_LINE_LOCATIONS_ALL'
                  );
         END;

         l_record_updated := l_record_updated + 1;
         fnd_file.put_line
                      (fnd_file.output,
                          'PO status is updated succesfully for PO_NUMBER    '
                       || po_expiry_rec.segment1
                      );
--end of Updation
      END IF;
   END LOOP;

   COMMIT;
   fnd_file.put_line (fnd_file.output,
                         'Total number of record updated are    '
                      || l_record_updated
                     );
END; 
/


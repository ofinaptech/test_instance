CREATE OR REPLACE PACKAGE APPS.il_gst_analyzer_pkg AUTHID DEFINER AS

TYPE section_rec IS RECORD(
  name           VARCHAR2(255),
  result         VARCHAR2(1), -- E,W,S
  error_count    NUMBER,
  warn_count     NUMBER,
  success_count  NUMBER,
  print_count    NUMBER);

TYPE family_area_tbl IS TABLE OF VARCHAR2(10) INDEX BY VARCHAR(32);
TYPE rep_section_tbl IS TABLE OF section_rec INDEX BY BINARY_INTEGER;
TYPE hash_tbl_2k     IS TABLE OF VARCHAR2(2000) INDEX BY VARCHAR2(255);
TYPE hash_tbl_4k     IS TABLE OF VARCHAR2(4000) INDEX BY VARCHAR2(255);
TYPE hash_tbl_8k     IS TABLE OF VARCHAR2(8000) INDEX BY VARCHAR2(255);
TYPE col_list_tbl    IS TABLE OF DBMS_SQL.VARCHAR2_TABLE;
TYPE varchar_tbl     IS TABLE OF VARCHAR2(255);
TYPE results_hash    IS TABLE OF NUMBER INDEX BY VARCHAR(1);
TYPE parameter_rec   IS RECORD(
  pname        VARCHAR2(255),
  pvalue       VARCHAR2(2000));
TYPE parameter_hash IS TABLE OF parameter_rec;


TYPE signature_rec IS RECORD(
  sigrepo_id       VARCHAR2(10),
  sig_sql          VARCHAR2(32000),
  title            VARCHAR2(255),
  fail_condition   VARCHAR2(4000),
  problem_descr    VARCHAR2(32000),
  solution         VARCHAR2(4000),
  success_msg      VARCHAR2(4000),
  print_condition  VARCHAR2(8),
  fail_type        VARCHAR2(1),
  print_sql_output VARCHAR2(2),
  limit_rows       VARCHAR2(1),
  extra_info       HASH_TBL_4K,
  child_sigs       VARCHAR_TBL := VARCHAR_TBL(),
  include_in_xml   VARCHAR2(1),
  styles           HASH_TBL_2K,
  version          VARCHAR2(10)  -- EBSAF-177
);

TYPE signature_tbl IS TABLE OF signature_rec INDEX BY VARCHAR2(255);
TYPE colsType      IS TABLE OF VARCHAR(126) INDEX BY VARCHAR(126);
TYPE hyperlinkColType IS RECORD(
   cols    colsType
);
TYPE sourceToDestType IS TABLE OF hyperlinkColType INDEX BY VARCHAR2(126);
TYPE destToSourceType IS TABLE OF hyperlinkColType INDEX BY VARCHAR2(126);

TYPE resultType      IS TABLE OF VARCHAR2(32) INDEX BY VARCHAR2(32);
TYPE dx_pr_type      IS TABLE OF INTEGER INDEX BY VARCHAR(320);

TYPE sig_record IS RECORD(
   sig_id      VARCHAR2(320),
   sig_name    VARCHAR2(320),
   sig_result  VARCHAR2(10)
);
TYPE signatures_tbl IS TABLE OF sig_record;

TYPE section_record IS RECORD(
   name          VARCHAR2(320),
   title         VARCHAR (320),
   sigs          signatures_tbl,
   results       results_hash
);
TYPE section_record_tbl IS TABLE OF section_record;

-- EBSAF-177 Capture signature performance details
TYPE sig_stats_rec IS RECORD(
    sig_id          VARCHAR2(320),
    version         NUMBER,
    row_count       NUMBER,
    query_start     TIMESTAMP,
    query_time      NUMBER, -- in seconds
    process_start   TIMESTAMP,
    process_time    NUMBER -- in seconds
);
TYPE sig_stats_tbl IS TABLE OF sig_stats_rec INDEX BY VARCHAR(320);


PROCEDURE main
(            p_trx_type                     IN VARCHAR2    DEFAULT NULL
           ,p_invoice_id                   IN NUMBER      DEFAULT NULL
           ,p_po_header_id                 IN NUMBER      DEFAULT NULL
           ,p_rcv_header_id                IN NUMBER      DEFAULT NULL
           ,p_so_header_id                 IN NUMBER      DEFAULT NULL
           ,p_so_line_id                   IN NUMBER      DEFAULT NULL
           ,p_cust_trx_id                  IN NUMBER      DEFAULT NULL
           ,p_cash_receipt_id              IN NUMBER      DEFAULT NULL
           ,p_book_type_code_id            IN VARCHAR2    DEFAULT NULL
           ,p_from_date                    IN DATE        DEFAULT sysdate-90
           ,p_end_date                     IN DATE        DEFAULT sysdate
           ,p_max_output_rows              IN NUMBER      DEFAULT 100
           ,p_debug_mode                   IN VARCHAR2    DEFAULT 'Y')
;


PROCEDURE main_cp (
            errbuf                         OUT VARCHAR2
           ,retcode                        OUT VARCHAR2
           ,p_trx_type                     IN VARCHAR2    DEFAULT NULL
           ,p_invoice_id                   IN NUMBER      DEFAULT NULL
           ,p_po_header_id                 IN NUMBER      DEFAULT NULL
           ,p_rcv_header_id                IN NUMBER      DEFAULT NULL
           ,p_so_header_id                 IN NUMBER      DEFAULT NULL
           ,p_so_line_id                   IN NUMBER      DEFAULT NULL
           ,p_cust_trx_id                  IN NUMBER      DEFAULT NULL
           ,p_cash_receipt_id              IN NUMBER      DEFAULT NULL
           ,p_book_type_code_id            IN VARCHAR2    DEFAULT NULL
           ,p_from_date                    IN VARCHAR2    DEFAULT NULL
           ,p_end_date                     IN VARCHAR2    DEFAULT NULL
           ,p_max_output_rows              IN NUMBER      DEFAULT 100
           ,p_debug_mode                   IN VARCHAR2    DEFAULT 'Y'
);

-----------------------------------------------
-- Diagnostic specific functions and procedures
-----------------------------------------------
FUNCTION il_event_status RETURN VARCHAR2;

FUNCTION is_il_licensed RETURN VARCHAR2;


END il_gst_analyzer_pkg;
/


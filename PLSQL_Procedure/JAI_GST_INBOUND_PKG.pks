CREATE OR REPLACE PACKAGE APPS.JAI_GST_INBOUND_PKG AUTHID CURRENT_USER AS
  /* $Header: jai_gst_inbound_pkg.pls 120.0.12010000.4 2020/03/12 12:35:58 aelluru noship $ */
--
-----------------------------------------
--Public Variable Declarations
-----------------------------------------
   p_org_id                      NUMBER;
   p_first_pty_reg_num           VARCHAR2 (50);                       --GSTIN
   p_request_id                  NUMBER;                         --Requist id
   p_period_name                 VARCHAR2 (30);

   TYPE inb_param_rec_type IS RECORD (
      first_party_reg_num   VARCHAR2 (100),
      return_period         VARCHAR2 (50),
      form_type             VARCHAR2 (50),
      section_code          VARCHAR2 (50)  DEFAULT NULL,
      tax_inv_number        VARCHAR2 (100) DEFAULT NULL,
      file_id               NUMBER ,
      file_name             VARCHAR2 (50)
   );

   TYPE inbd_tbl_typ IS TABLE OF jai_gst_rept_inbound_interface%ROWTYPE index by binary_integer;

    TYPE irn_tbl_typ IS TABLE OF jai_irn_details%ROWTYPE;

--TL_INBD_TBL INBD_TBL_TYP;
   TYPE retsumary_tbl_typ IS TABLE OF jai_gst_return_summary%ROWTYPE;

   g_module_name        CONSTANT VARCHAR2 (50)
                                           := 'JAI.PLSQL.JAI_GST_INBOUND_PKG.';
   g_current_runtime_level       NUMBER;
   g_level_statement    CONSTANT NUMBER         := fnd_log.level_statement;
   g_level_procedure    CONSTANT NUMBER         := fnd_log.level_procedure;
   g_level_event        CONSTANT NUMBER         := fnd_log.level_event;
   g_level_unexpected   CONSTANT NUMBER         := fnd_log.level_unexpected;
   g_error_buffer                VARCHAR2 (100);
   g_limit_size                  NUMBER         := 10000;
   pg_debug                      VARCHAR2 (1)
                             := NVL (fnd_profile.VALUE ('AFLOG_ENABLED'), 'N');

   --TL_SUMMARY_TBL RETSUMARY_TBL_TYP;

   /*===========================================================================+
    | PROCEDURE                                                                 |
    |   jai_load_jason                                                          |
    |                                                                           |
    | DESCRIPTION                                                               |
    |    Invoked from Repository Review UI to upload json file                  |
    |                                                                           |
    |   p_file_id                                                               |
    |   p_file_type     IN       VARCHAR2,                                      |
    |   p_status        OUT      VARCHAR2,                                      |
    |   p_process_msg   OUT      VARCHAR2                                       |
    |  SCOPE - Public                                                           |
    |                                                                           |
    | NOTES                                                                     |
    |                                                                           |
    | MODIFICATION HISTORY                                                      |
    |                                                                           |
    +===========================================================================*/
   PROCEDURE jai_load_jason_file (
      p_file_id                NUMBER,
      p_file_type     IN       VARCHAR2,
      p_status       in OUT   nocopy   VARCHAR2,
      p_process_msg   in OUT   nocopy      VARCHAR2
   );

/*===========================================================================+
  | PROCEDURE                                                                 |
  |   jai_process_inbound                                                               |
  |                                                                           |
  | DESCRIPTION                                                               |
  |    Invoked from concurrent 'India-GSTIN Inbound Process'                   |
  |    Main procedure to process the GSTIN data from (jai_gst_inbound_interface|
  |    table.                                                                  |
  |    P_FORM_TYPE='ANX1' then Update repository with the error information.   |
  |    P_FORM_TYPE='ANX2' then update repository with error info if any and    |
  |                       also process the recovery based on the action flag   |
  | SCOPE - Public                                                             |
  |                                                                            |
  | NOTES                                                                      |
  |                                                                            |
  | MODIFICATION HISTORY                                                       |
  |                                                                            |
  +===========================================================================*/
   PROCEDURE jai_process_inbound_main (
      errbuff                 OUT NOCOPY      VARCHAR2,
      retcode                 OUT NOCOPY      VARCHAR2,
      p_file_id               IN              NUMBER DEFAULT NULL,
      p_file_name             IN              VARCHAR2 DEFAULT NULL,
      p_form_type             IN              VARCHAR2 DEFAULT NULL,
      p_tax_regime            IN              VARCHAR2,
      p_first_party_reg_num   IN              VARCHAR2,
      p_return_period         IN              VARCHAR2,
      p_section               IN              VARCHAR2 DEFAULT NULL
   );

    /*===========================================================================+
   | PROCEDURE                                                                 |
   |   jai_gst_process_Recovery                                                   |
   |                                                                           |
   | DESCRIPTION                                                               |
   |   Can Submitted as  standalone concurrent also invoked from  froms          |
   |    does the final Accounting based on the user action                      |
   |                                                                           |
   | SCOPE - Public                                                            |
   |                                                                           |
   | NOTES                                                                     |
   |                                                                           |
   | MODIFICATION HISTORY                                                      |
   |                                                                           |
   +===========================================================================*/
   PROCEDURE jai_gst_process_recovery (
      errbuff         OUT NOCOPY      VARCHAR2,
      retcode         OUT NOCOPY      VARCHAR2,
      p_batch_id      IN              NUMBER DEFAULT NULL,
      p_form_type     IN              VARCHAR2 DEFAULT NULL,
      p_first_party   IN              VARCHAR2 DEFAULT NULL,
      p_period        IN              VARCHAR2 DEFAULT NULL,
      p_section       IN              VARCHAR2 DEFAULT NULL,
      p_tax_regime    IN              VARCHAR2 DEFAULT NULL,
      p_tax_invnum    IN              VARCHAR2 DEFAULT NULL
   );



/*===========================================================================+
  | PROCEDURE                                                                 |
  |   JAI_PROCESS_ANX2                                                              |
  |                                                                           |
  | DESCRIPTION                                                               |
  |    Invoked from jai_process_inbound  if form_type is ANX_2               |
  |    1) update the error information if any ,action info from jai_gst_inbound_interface    |
  |    table to Tax repository of ANX-2 details.                               |
  |    2) Invoke  JAI_ANX2_ACTION for processing the recovery.                 |
    | SCOPE - Public                                                             |
  |                                                                            |
  | NOTES                                                                      |
  |                                                                            |
  | MODIFICATION HISTORY                                                       |
  |                                                                            |
  +===========================================================================*/
   PROCEDURE jai_process_anx2 (
      p_param_rec_type                    inb_param_rec_type,
      p_call_from         IN              VARCHAR2 DEFAULT NULL,
      p_process_status    OUT NOCOPY      VARCHAR2,
      p_process_message   OUT NOCOPY      VARCHAR2
   );

 procedure jai_populate_interface (p_inbd_tbl     inbd_tbl_typ,
                              p_file_id           IN NUMBER DEFAULT NULL,
    p_form_type         IN VARCHAR2 DEFAULT NULL,
    p_request_id        IN VARCHAR2 default null,
	P_CALL_FROM        IN VARCHAR2 default null,
    p_process_status    IN OUT NOCOPY VARCHAR2,
    p_process_message   IN OUT NOCOPY VARCHAR2
                              );



   /*===========================================================================+
    | PROCEDURE                                                                 |
    |   jai_debug                                                               |
    |                                                                           |
    | DESCRIPTION                                                               |
    |    this is used for writing the debug/error messages                      |
    |    tax data into tax extract GT tables                                    |
    |                                                                           |
    | SCOPE - Public                                                            |
    |                                                                           |
    | NOTES                                                                     |
    |                                                                           |
    | MODIFICATION HISTORY                                                      |
    |                                                                           |
    +===========================================================================*/
   PROCEDURE jai_debug (
      p_api_name   IN   VARCHAR2,
      p_msg_type   IN   VARCHAR2,
      p_msg_desc   IN   VARCHAR2
   );

/*===========================================================================+
  | PROCEDURE                                                                 |
  |   jai_file_to_clob                                                               |
  |                                                                           |
  | DESCRIPTION                                                               |
  |   json information stored in text file will be returned as CLOB output                  |
  |                                  |
  |                                                                           |
  | SCOPE - Public                                                            |
  |                                                                           |
  | NOTES                                                                     |
  |                                                                           |
  | MODIFICATION HISTORY                                                      |
  |                                                                           |
  +===========================================================================*/
   FUNCTION jai_file_to_clob (
      p_file_path   IN   VARCHAR2,
      p_filename    IN   VARCHAR2,
      p_src_csid    IN   NUMBER := DBMS_LOB.default_csid
   )
      RETURN CLOB;

	  PROCEDURE jai_return_repository_update(
     	   p_file_id           IN NUMBER DEFAULT NULL,
    p_form_type         IN VARCHAR2 DEFAULT NULL,
	 p_first_party_reg_num   IN              VARCHAR2 DEFAULT NULL,
      p_return_period         IN              VARCHAR2  DEFAULT NULL,
      p_section      IN              VARCHAR2  DEFAULT NULL,
      p_call_from         IN              VARCHAR2 DEFAULT NULL,
      p_request_id        IN              NUMBER DEFAULT NULL,
      p_process_status    OUT NOCOPY      VARCHAR2,
      p_process_message   OUT NOCOPY      VARCHAR2
   );



END jai_gst_inbound_pkg;
/


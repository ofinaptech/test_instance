CREATE OR REPLACE PACKAGE APPS.XXABRL_NAV_AR_INVIS_IMP_PKG IS
  PROCEDURE INVOICE_VALIDATE(Errbuf         OUT VARCHAR2,
                             RetCode        OUT NUMBER,
                             /*P_Org_Id       In NUMBER,*/
                             P_BATCH_Source IN VARCHAR2,
                             p_action VARCHAR2,
                             P_GL_DATE VARCHAR2
                             );
  PROCEDURE INVOICE_INSERT(P_Org_Id IN NUMBER, P_BATCH_Source IN VARCHAR2,P_GL_DATE IN DATE ,x_ret_code OUT NUMBER);
  FUNCTION ACCOUNT_SEG_STATUS(P_Seg_Value IN VARCHAR2,
                              P_Seg_Desc  IN VARCHAR2) RETURN VARCHAR2;
END XXABRL_NAV_AR_INVIS_IMP_PKG; 
/


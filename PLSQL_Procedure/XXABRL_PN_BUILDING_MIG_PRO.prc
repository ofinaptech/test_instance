CREATE OR REPLACE procedure APPS.xxabrl_pn_building_mig_pro(ERRBUFF OUT VARCHAR2,
                                                            RETCODE OUT NUMBER) is
cursor cur_addr is
SELECT * FROM XXABRL.XXABRL_PN_ADDRESSES_ALL_STG
where 1=1
--and address_id=1114
--and org_id=89
;

cursor cur_loc(p_address_id number) is
SELECT * FROM XXABRL.XXABRL_PN_LOCATIONS_ALL_STG
where 1=1
--and location_id=3516
--and org_id=89
and address_id=p_address_id;

cursor cur_lease(p_location_id number) is
SELECT * FROM XXABRL.XXABRL_PN_LEASES_ALL_STG
where 1=1
--and lease_id=6789
--and org_id=89
and  location_id=p_location_id;

cursor cur_lease_trans(p_lease_id number) is
SELECT * FROM XXABRL.XXABRL_PN_LEASE_TRANS_ALL_STG
where 1=1
--and lease_id=6789
--and org_id=89
and lease_id=p_lease_id;

cursor cur_lease_change(p_lease_id number,
p_lease_transaction_id number) is
SELECT * FROM XXABRL.XXABRL_PN_LEASE_CHANGE_ALL_STG
where 1=1
--and lease_id=6789
--and org_id=89
and  lease_id=p_lease_id
and LEASE_TRANSACTION_ID=p_lease_transaction_id;

cursor cur_lease_detail(p_lease_id number,
p_lease_change_id number) is
SELECT * FROM XXABRL.XXABRL_PN_LEASE_DETAIL_ALL_STG
where 1=1
--and lease_id=6789
--and org_id=89
and  LEASE_ID=p_lease_id
and lease_change_id=p_lease_change_id;

cursor cur_tenancies(p_location_id number,
 p_lease_id number
 --,p_lease_change_id number
 ) is
SELECT * FROM XXABRL.XXABRL_PN_TENANCIES_ALL_STG
where 1=1
--and lease_id=6789
--and org_id=89
and location_id=p_location_id
and lease_id=p_lease_id
--and lease_change_id=p_lease_change_id
;

cursor cur_right(p_lease_id number
--,p_lease_change_id number
) is
SELECT * FROM XXABRL.XXABRL_PN_RIGHTS_ALL_STG
where 1=1
--and lease_id=6789
--and org_id=89
and LEASE_ID=p_lease_id
--and lease_change_id=p_lease_change_id
;

cursor cur_option(p_lease_id number
--,p_lease_change_id number
) is
SELECT * FROM XXABRL.XXABRL_PN_OPTIONS_ALL_STG
where 1=1
--and lease_id=6789
--and org_id=89
and LEASE_ID=p_lease_id
--and lease_change_id=p_lease_change_id
;

cursor cur_note(p_lease_id number) is 
SELECT * FROM XXABRL.XXABRL_PN_NOTE_HEADERS_STG
where 1=1
--and lease_id=6789
and  LEASE_ID=p_lease_id;

cursor cur_payment(--p_location_id number,
 p_lease_id number
 --,p_lease_change_id number
 --p_vendor_id number,
 --p_vendor_site_id number,
 --p_set_of_books_id number,
 --p_legal_entity_id number
 ) is
SELECT * FROM XXABRL.XXABRL_PN_PAYMENT_TERM_ALL_STG
where 1=1
--and lease_id=6789
--and org_id=89
--and location_id=p_location_id
and LEASE_ID=p_lease_id
--and lease_change_id=p_lease_change_id
--and vendor_id=p_vendor_id
--and vendor_site_id=p_vendor_site_id
--and SET_OF_BOOKS_ID=p_set_of_books_id
--and LEGAL_ENTITY_ID=p_legal_entity_id
;

cursor cur_distribution(p_payment_term_id number) is
SELECT * FROM XXABRL.XXABRL_PN_DIST_ALL_STG
where 1=1
--and payment_term_id in(10129,13162,33202)
--and org_id=89
and  PAYMENT_TERM_ID=p_payment_term_id;


ln_loc_id number:=null;
ln_address_id number:=null;
ln_lease_id number:=null;
ln_lease_transaction_id number:=null;
ln_lease_change_id number:=null;
ln_lease_detail_id number:=null;
ln_tenancy_id number:=null;
ln_right_id number:=null;
ln_option_id number:=null;
ln_note_header_id number:=null;
ln_payment_term_id number:=null;
ln_distribution_id number:=null;
v_vendor_id number:=null;
v_vendor_site_id number:=null;
v_legal_entity_id number:=null;

 begin

                for i in cur_addr
                loop
                            select APPS.xxabrl_pn_address_id_seq.nextval into ln_address_id from dual;
                            FND_FILE.PUT_LINE(FND_FILE.LOG,'Address Id : '||ln_address_id);
        
                            --inserting in to PN_ADDRESSES_ALL--
        
                        begin
                                insert into APPS.PN_ADDRESSES_ALL
                                       (ADDRESS_ID,
                                        ADDRESS_LINE1,
                                        ADDRESS_LINE2,
                                        ADDRESS_LINE3,
                                        ADDRESS_LINE4,
                                        COUNTY,
                                        CITY,
                                        STATE,
                                        PROVINCE,
                                        ZIP_CODE,
                                        COUNTRY,
                                        ADDRESS_STYLE,
                                        TERRITORY_ID,
                                        LAST_UPDATE_DATE,
                                        LAST_UPDATED_BY,
                                        CREATION_DATE,
                                        CREATED_BY,
                                        LAST_UPDATE_LOGIN,
                                        ATTRIBUTE_CATEGORY,
                                        ATTRIBUTE1,
                                        ATTRIBUTE2,
                                        ATTRIBUTE3,
                                        ATTRIBUTE4,
                                        ATTRIBUTE5,
                                        ATTRIBUTE6,
                                        ATTRIBUTE7,
                                        ATTRIBUTE8,
                                        ATTRIBUTE9,
                                        ATTRIBUTE10,
                                        ATTRIBUTE11,
                                        ATTRIBUTE12,
                                        ATTRIBUTE13,
                                        ATTRIBUTE14,
                                        ATTRIBUTE15,
                                        ORG_ID,
                                        ADDR_ATTRIBUTE_CATEGORY,
                                        ADDR_ATTRIBUTE1,
                                        ADDR_ATTRIBUTE2,
                                        ADDR_ATTRIBUTE3,
                                        ADDR_ATTRIBUTE4,
                                        ADDR_ATTRIBUTE5,
                                        ADDR_ATTRIBUTE6,
                                        ADDR_ATTRIBUTE7,
                                        ADDR_ATTRIBUTE8,
                                        ADDR_ATTRIBUTE9,
                                        ADDR_ATTRIBUTE10,
                                        ADDR_ATTRIBUTE11,
                                        ADDR_ATTRIBUTE12,
                                        ADDR_ATTRIBUTE13,
                                        ADDR_ATTRIBUTE14,
                                        ADDR_ATTRIBUTE15)
                                        values
                                            (ln_address_id,
                                              i.ADDRESS_LINE1,
                                              i.ADDRESS_LINE2,
                                              i.ADDRESS_LINE3,
                                              i.ADDRESS_LINE4,
                                              i.COUNTY,
                                                i.CITY,
                                                i.STATE,
                                                i.PROVINCE,
                                                i.ZIP_CODE,
                                                i.COUNTRY,
                                                i.ADDRESS_STYLE,
                                                i.TERRITORY_ID,
                                                sysdate,
                                                FND_GLOBAL.USER_ID,
                                                sysdate,
                                                FND_GLOBAL.USER_ID,
                                                FND_GLOBAL.USER_ID,
                                                i.ATTRIBUTE_CATEGORY,
                                                i.ATTRIBUTE1,
                                                i.ATTRIBUTE2,
                                                i.ATTRIBUTE3,
                                                i.ATTRIBUTE4,
                                                i.ATTRIBUTE5,
                                                i.ATTRIBUTE6,
                                                i.ATTRIBUTE7,
                                                i.ATTRIBUTE8,
                                                i.ATTRIBUTE9,
                                                i.ATTRIBUTE10,
                                                i.ATTRIBUTE11,
                                                i.ATTRIBUTE12,
                                                i.ATTRIBUTE13,
                                                i.ATTRIBUTE14,
                                                i.ATTRIBUTE15,
                                                i.org_id,
                                                i.ADDR_ATTRIBUTE_CATEGORY,
                                                i.ADDR_ATTRIBUTE1,
                                                i.ADDR_ATTRIBUTE2,
                                                i.ADDR_ATTRIBUTE3,
                                                i.ADDR_ATTRIBUTE4,
                                                i.ADDR_ATTRIBUTE5,
                                                i.ADDR_ATTRIBUTE6,
                                                i.ADDR_ATTRIBUTE7,
                                                i.ADDR_ATTRIBUTE8,
                                                i.ADDR_ATTRIBUTE9,
                                                i.ADDR_ATTRIBUTE10,
                                                i.ADDR_ATTRIBUTE11,
                                                i.ADDR_ATTRIBUTE12,
                                                i.ADDR_ATTRIBUTE13,
                                                i.ADDR_ATTRIBUTE14,
                                                i.ADDR_ATTRIBUTE15);
                    
                        FND_FILE.PUT_LINE(FND_FILE.LOG,'Successfully inserted in PN_ADDRESSES_ALL'); 
                        exception when others then
                        FND_FILE.PUT_LINE(FND_FILE.LOG,sqlcode||'-'||sqlerrm||'Error in inserting in PN_ADDRESSES_ALL');
               
                    end;
            commit;

                                FND_FILE.PUT_LINE(FND_FILE.LOG,'Parameter for PN_LOCATIONS_ALL'||'  '||i.ADDRESS_ID);

     for j in cur_loc(i.ADDRESS_ID)
        loop 
                    SELECT APPS.pn_locations_s.NEXTVAL    
                        INTO ln_loc_id
                        FROM DUAL;
                             FND_FILE.PUT_LINE(FND_FILE.LOG,'Location Id : '||ln_loc_id);
        
                    --inserting in to pn_locations_all--
        begin
                    insert into APPS.pn_locations_all
                                (LOCATION_ID,
                                 LAST_UPDATE_DATE,
                                LAST_UPDATED_BY,
                                CREATION_DATE,
                                CREATED_BY,
                                LAST_UPDATE_LOGIN,
                                LOCATION_PARK_ID,
                                LOCATION_TYPE_LOOKUP_CODE,
                                LOCATION_CODE,
                                LOCATION_ALIAS,
                                BUILDING,
                                LEASE_OR_OWNED,
                                FLOOR,
                                OFFICE,
                                ADDRESS_ID,
                                MAX_CAPACITY,
                                OPTIMUM_CAPACITY,
                                RENTABLE_AREA,
                                USABLE_AREA,
                                ALLOCATE_COST_CENTER_CODE,
                                UOM_CODE,
                                DESCRIPTION,
                                PARENT_LOCATION_ID,
                                INTERFACE_FLAG,
                                --REQUEST_ID,
                                --PROGRAM_APPLICATION_ID,
                                --PROGRAM_ID,
                                --PROGRAM_UPDATE_DATE,
                                STATUS,
                                SPACE_TYPE_LOOKUP_CODE,
                                ATTRIBUTE_CATEGORY,
                                ATTRIBUTE1,
                                ATTRIBUTE2,
                                ATTRIBUTE3,
                                ATTRIBUTE4,
                                ATTRIBUTE5,
                                ATTRIBUTE6,
                                ATTRIBUTE7,
                                ATTRIBUTE8,
                                ATTRIBUTE9,
                                ATTRIBUTE10,
                                ATTRIBUTE11,
                                ATTRIBUTE12,
                                ATTRIBUTE13,
                                ATTRIBUTE14,
                                ATTRIBUTE15,
                                ORG_ID,
                                SOURCE,
                                PROPERTY_ID,
                                CLASS,
                                STATUS_TYPE,
                                SUITE,
                                GROSS_AREA,
                                ASSIGNABLE_AREA,
                                COMMON_AREA,
                                COMMON_AREA_FLAG,
                                FUNCTION_TYPE_LOOKUP_CODE,
                                STANDARD_TYPE_LOOKUP_CODE,
                                ACTIVE_START_DATE,
                                ACTIVE_END_DATE,
                                BOOKABLE_FLAG,
                                OCCUPANCY_STATUS_CODE,
                                ASSIGNABLE_EMP,
                                ASSIGNABLE_CC,
                                ASSIGNABLE_CUST,
                                DISPOSITION_CODE,
                                ACC_TREATMENT_CODE)
                                values
                                    (ln_loc_id,
                                    sysdate,
                                    FND_GLOBAL.USER_ID,
                                    sysdate,
                                    FND_GLOBAL.USER_ID,
                                    FND_GLOBAL.USER_ID,
                                    j.LOCATION_PARK_ID,
                                    j.LOCATION_TYPE_LOOKUP_CODE,
                                    j.LOCATION_CODE,
                                    j.LOCATION_ALIAS,
                                    j.BUILDING,
                                    j.LEASE_OR_OWNED,
                                    j.FLOOR,
                                    j.OFFICE,
                                    ln_address_id,
                                    j.MAX_CAPACITY,
                                    j.OPTIMUM_CAPACITY,
                                    j.RENTABLE_AREA,
                                    j.USABLE_AREA,
                                        j.ALLOCATE_COST_CENTER_CODE,
                                    j.UOM_CODE,
                                    j.DESCRIPTION,
                                    j.PARENT_LOCATION_ID,
                                    j.INTERFACE_FLAG,
                                    --j.REQUEST_ID,
                                    --j.PROGRAM_APPLICATION_ID,
                                    --j.PROGRAM_ID,
                                    --j.PROGRAM_UPDATE_DATE,
                                    j.STATUS,
                                    j.SPACE_TYPE_LOOKUP_CODE,
                                    j.ATTRIBUTE_CATEGORY,
                                    j.ATTRIBUTE1,
                                    j.ATTRIBUTE2,
                                    j.ATTRIBUTE3,
                                    j.ATTRIBUTE4,
                                    j.ATTRIBUTE5,
                                    j.ATTRIBUTE6,
                                    j.ATTRIBUTE7,
                                    j.ATTRIBUTE8,
                                    j.ATTRIBUTE9,
                                    j.ATTRIBUTE10,
                                    j.ATTRIBUTE11,
                                    j.ATTRIBUTE12,
                                    j.ATTRIBUTE13,
                                    j.ATTRIBUTE14,
                                    j.ATTRIBUTE15,
                                    j.org_id,
                                    j.SOURCE,
                                    j.PROPERTY_ID,
                                    j.CLASS,
                                    j.STATUS_TYPE,
                                    j.SUITE,
                                    j.GROSS_AREA,
                                    j.ASSIGNABLE_AREA,
                                    j.COMMON_AREA,
                                    j.COMMON_AREA_FLAG,
                                    j.FUNCTION_TYPE_LOOKUP_CODE,
                                    j.STANDARD_TYPE_LOOKUP_CODE,
                                    '01-apr-2012',
                                    j.ACTIVE_END_DATE,
                                    j.BOOKABLE_FLAG,
                                    j.OCCUPANCY_STATUS_CODE,
                                    j.ASSIGNABLE_EMP,
                                    j.ASSIGNABLE_CC,
                                    j.ASSIGNABLE_CUST,
                                    j.DISPOSITION_CODE,
                                    j.ACC_TREATMENT_CODE);

            FND_FILE.PUT_LINE(FND_FILE.LOG,'Successfully inserted in PN_LOCATIONS_ALL');
                exception when others then
               FND_FILE.PUT_LINE(FND_FILE.LOG,sqlcode||'-'||sqlerrm||'Error in inserting in PN_LOCATIONS_ALL');
               

            end;



                    commit;

            FND_FILE.PUT_LINE(FND_FILE.LOG,'Parameter for PN_LEASES_ALL'||'  '||j.location_id);

                 for k in cur_lease(j.location_id)
                    loop
                    SELECT APPS.PN_LEASES_S.NEXTVAL into ln_lease_id from dual;

                        fnd_file.put_line(fnd_file.log,'Lease Id : '||ln_lease_id);

                    --inserting in to pn_leases_all--

                begin

                        insert into APPS.pn_leases_all
                                (LEASE_ID,
                                LAST_UPDATE_DATE,
                                LAST_UPDATED_BY,
                                CREATION_DATE,
                                CREATED_BY,
                                LAST_UPDATE_LOGIN,
                                NAME,
                                LEASE_NUM,
                                PARENT_LEASE_ID,
                                ADDRESS_LOCATION_ID,
                                LEASE_TYPE_CODE,
                                PAYMENT_TERM_PRORATION_RULE,
                                ABSTRACTED_BY_USER,
                                COMMENTS,
                                STATUS,
                                ORG_ID,
                                LEASE_CLASS_CODE,
                                LEASE_STATUS,
                                LOCATION_ID,
                                CUSTOMER_ID,
                                CAL_START)
                                values
                                (ln_lease_id,
                                sysdate,
                                fnd_global.user_id,
                                sysdate,
                                fnd_global.user_id,
                                fnd_global.user_id,
                                k.NAME,
                                ln_lease_id,
                                k.PARENT_LEASE_ID,
                                k.ADDRESS_LOCATION_ID,
                                k.LEASE_TYPE_CODE,
                                k.PAYMENT_TERM_PRORATION_RULE,
                                k.ABSTRACTED_BY_USER,
                                k.COMMENTS,
                                k.STATUS,
                                k.ORG_ID,
                                k.LEASE_CLASS_CODE,
                                k.LEASE_STATUS,
                                ln_loc_id,
                                k.CUSTOMER_ID,
                                k.CAL_START);

                    FND_FILE.PUT_LINE(FND_FILE.LOG,'Successfully inserted in PN_LEASES_ALL');

                exception when others then
               FND_FILE.PUT_LINE(FND_FILE.LOG,sqlcode||'-'||sqlerrm||'Error in inserting in PN_LEASES_ALL');
               
               end;



            commit;

                FND_FILE.PUT_LINE(FND_FILE.LOG,'Parameter for PN_LEASE_TRANSACTIONS_ALL'||'  '||k.lease_id);

                    for l in cur_lease_trans(k.lease_id)
                    loop

                        select APPS.PN_LEASE_TRANSACTIONS_S.NEXTVAL into ln_lease_transaction_id from dual;

                        fnd_file.put_line(fnd_file.log,'Lease Transaction Id : '||ln_lease_transaction_id);

                            --inserting in to PN_LEASE_TRANSACTIONS_ALL--

          begin

                                insert into APPS.PN_LEASE_TRANSACTIONS_ALL
                                        (LEASE_TRANSACTION_ID,
                                        LEASE_ID,
                                        TRANSACTION_TYPE,
                                        LAST_UPDATE_DATE,
                                        LAST_UPDATED_BY,
                                        CREATION_DATE,
                                        CREATED_BY,
                                        LAST_UPDATE_LOGIN,
                                        DATE_EFFECTIVE,
                                        ORG_ID)
                                        values
                                        (ln_lease_transaction_id,
                                        ln_lease_id,
                                        l.TRANSACTION_TYPE,
                                        sysdate,
                                        fnd_global.user_id,
                                        sysdate,
                                        fnd_global.user_id,
                                        fnd_global.user_id,
                                        sysdate,
                                        l.ORG_ID);


                    FND_FILE.PUT_LINE(FND_FILE.LOG,'Successfully inserted in PN_LEASE_TRANSACTIONS_ALL');
               exception when others then
               FND_FILE.PUT_LINE(FND_FILE.LOG,sqlcode||'-'||sqlerrm||'Error in inserting in PN_LEASE_TRANSACTIONS_ALL');
               end;

        commit;

        FND_FILE.PUT_LINE(FND_FILE.LOG,'Parameter for PN_LEASE_CHANGES_ALL'||'  '||k.lease_id||'  '||l.LEASE_TRANSACTION_ID);


        for m in cur_lease_change(k.lease_id,l.LEASE_TRANSACTION_ID)
        loop

                    select APPS.PN_LEASE_CHANGES_S.NEXTVAL into ln_lease_change_id from dual;

                        fnd_file.put_line(fnd_file.log,'Lease Change Id : '||ln_lease_change_id);

                    --inserting in to PN_LEASE_CHANGES_ALL--


            begin

                            insert into APPS.PN_LEASE_CHANGES_ALL
                                (LEASE_CHANGE_ID,
                                LEASE_ID,
                                LEASE_TRANSACTION_ID,
                                LAST_UPDATE_DATE,
                                LAST_UPDATED_BY,
                                CREATION_DATE,
                                CREATED_BY,
                                LAST_UPDATE_LOGIN,
                                LEASE_CHANGE_NAME,
                                LEASE_CHANGE_NUMBER,
                                ABSTRACTED_BY_USER,
                                RESPONSIBLE_USER,
                                CHANGE_COMMENCEMENT_DATE,
                                CHANGE_TERMINATION_DATE,
                                CHANGE_TYPE_LOOKUP_CODE,
                                CHANGE_EXECUTION_DATE,
                                ATTRIBUTE_CATEGORY,
                                ATTRIBUTE1,
                                ATTRIBUTE2,
                                ATTRIBUTE3,
                                ATTRIBUTE4,
                                ATTRIBUTE5,
                                ATTRIBUTE6,
                                ATTRIBUTE7,
                                ATTRIBUTE8,
                                ATTRIBUTE9,
                                ATTRIBUTE10,
                                ATTRIBUTE11,
                                ATTRIBUTE12,
                                ATTRIBUTE13,
                                ATTRIBUTE14,
                                ATTRIBUTE15,
                                ORG_ID,
                                CUTOFF_DATE)
                         values
                                    (ln_lease_change_id,
                                    ln_lease_id,
                                    ln_lease_transaction_id,
                                    sysdate,
                                    fnd_global.user_id,
                                    sysdate,
                                    fnd_global.user_id,
                                    fnd_global.user_id,
                                    m.LEASE_CHANGE_NAME,
                                    m.LEASE_CHANGE_NUMBER,
                                    m.ABSTRACTED_BY_USER,
                                    m.RESPONSIBLE_USER,
                                    m.CHANGE_COMMENCEMENT_DATE,
                                    m.CHANGE_TERMINATION_DATE,
                                    m.CHANGE_TYPE_LOOKUP_CODE,
                                    m.CHANGE_EXECUTION_DATE,
                                    m.ATTRIBUTE_CATEGORY,
                                    m.ATTRIBUTE1,
                                    m.ATTRIBUTE2,
                                    m.ATTRIBUTE3,
                                    m.ATTRIBUTE4,
                                    m.ATTRIBUTE5,
                                    m.ATTRIBUTE6,
                                    m.ATTRIBUTE7,
                                    m.ATTRIBUTE8,
                                    m.ATTRIBUTE9,
                                    m.ATTRIBUTE10,
                                    m.ATTRIBUTE11,
                                    m.ATTRIBUTE12,
                                    m.ATTRIBUTE13,
                                    m.ATTRIBUTE14,
                                    m.ATTRIBUTE15,
                                    m.ORG_ID,
                                    m.CUTOFF_DATE);
            
                 FND_FILE.PUT_LINE(FND_FILE.LOG,'Successfully inserted in PN_LEASE_CHANGES_ALL');

                exception when others then
               FND_FILE.PUT_LINE(FND_FILE.LOG,sqlcode||'-'||sqlerrm||'Error in inserting in PN_LEASE_CHANGES_ALL');
               end;
               
                commit;

        FND_FILE.PUT_LINE(FND_FILE.LOG,'Parameter for PN_LEASE_DETAILS_ALL'||'  '||k.lease_id||'  '||m.lease_change_id);



            for n in cur_lease_detail(k.lease_id,m.LEASE_CHANGE_ID)
                loop

                        select APPS.PN_LEASE_DETAILS_S.NEXTVAL into ln_lease_detail_id from dual;

                        fnd_file.put_line(fnd_file.log,'Lease Detail Id : '||ln_lease_detail_id);

                    --inserting in to PN_LEASE_DETAILS_ALL--


                BEGIN

                                insert into APPS.PN_LEASE_DETAILS_ALL
                                (LEASE_DETAIL_ID,
                                LEASE_CHANGE_ID,
                                LEASE_ID,
                                RESPONSIBLE_USER,
                                EXPENSE_ACCOUNT_ID,
                                LEASE_COMMENCEMENT_DATE,
                                LEASE_TERMINATION_DATE,
                                LEASE_EXECUTION_DATE,
                                LAST_UPDATE_DATE,
                                LAST_UPDATED_BY,
                                CREATION_DATE,
                                CREATED_BY,
                                LAST_UPDATE_LOGIN,
                                ATTRIBUTE_CATEGORY,
                                ATTRIBUTE1,
                                ATTRIBUTE2,
                                ATTRIBUTE3,
                                ATTRIBUTE4,
                                ATTRIBUTE5,
                                ATTRIBUTE6,
                                ATTRIBUTE7,
                                ATTRIBUTE8,
                                ATTRIBUTE9,
                                ATTRIBUTE10,
                                ATTRIBUTE11,
                                ATTRIBUTE12,
                                ATTRIBUTE13,
                                ATTRIBUTE14,
                                ATTRIBUTE15,
                                ORG_ID,
                                ACCRUAL_ACCOUNT_ID,
                                RECEIVABLE_ACCOUNT_ID,
                                SEND_ENTRIES,
                                TERM_TEMPLATE_ID,
                                LEASE_EXTENSION_END_DATE,
                                GROUPING_RULE_ID)
                                values
                                (ln_lease_detail_id,
                                ln_lease_change_id,
                                ln_lease_id,
                                n.RESPONSIBLE_USER,
                                n.NEW_EXPENSE_ACCOUNT_ID,
                                n.LEASE_COMMENCEMENT_DATE,
                                n.LEASE_TERMINATION_DATE,
                                n.LEASE_EXECUTION_DATE,
                                sysdate,
                                fnd_global.user_id,
                                sysdate,
                                fnd_global.user_id,
                                fnd_global.user_id,
                                n.ATTRIBUTE_CATEGORY,
                                n.ATTRIBUTE1,
                                n.ATTRIBUTE2,
                                n.ATTRIBUTE3,
                                n.ATTRIBUTE4,
                                n.ATTRIBUTE5,
                                n.ATTRIBUTE6,
                                n.ATTRIBUTE7,
                                n.ATTRIBUTE8,
                                n.ATTRIBUTE9,
                                n.ATTRIBUTE10,
                                n.ATTRIBUTE11,
                                n.ATTRIBUTE12,
                                n.ATTRIBUTE13,
                                n.ATTRIBUTE14,
                                n.ATTRIBUTE15,
                                n.ORG_ID,
                                n.ACCRUAL_ACCOUNT_ID,
                                n.RECEIVABLE_ACCOUNT_ID,
                                n.SEND_ENTRIES,
                                n.TERM_TEMPLATE_ID,
                                n.LEASE_EXTENSION_END_DATE,
                                n.GROUPING_RULE_ID);

                        FND_FILE.PUT_LINE(FND_FILE.LOG,'Successfully inserted in PN_LEASE_DETAILS_ALL');


                exception when others then
               FND_FILE.PUT_LINE(FND_FILE.LOG,sqlcode||'-'||sqlerrm||'Error in inserting in PN_LEASE_DETAILS_ALL');
               end;
               
                commit;

             FND_FILE.PUT_LINE(FND_FILE.LOG,'Parameter for PN_TENANCIES_ALL'||'  '||j.location_id||'  '||k.lease_id);


                    for o in cur_tenancies(j.location_id,k.lease_id
                        --,m.LEASE_CHANGE_ID
                                    )
                            loop
                    select APPS.PN_TENANCIES_S.NEXTVAL into ln_tenancy_id from dual;

                            fnd_file.put_line(fnd_file.log,'Tenancy Id : '||ln_tenancy_id);

                        --inserting in to PN_TENANCIES_ALL--


                        BEGIN

                                    insert into APPS.PN_TENANCIES_ALL
                                            (TENANCY_ID,
                                            LAST_UPDATE_DATE,
                                            LAST_UPDATED_BY,
                                            CREATION_DATE,
                                            CREATED_BY,
                                            LAST_UPDATE_LOGIN,
                                            LOCATION_ID,
                                            LEASE_ID,
                                            LEASE_CHANGE_ID,
                                            TENANCY_USAGE_LOOKUP_CODE,
                                            PRIMARY_FLAG,
                                            ESTIMATED_OCCUPANCY_DATE,
                                            OCCUPANCY_DATE,
                                            EXPIRATION_DATE,
                                            ASSIGNABLE_FLAG,
                                            SUBLEASEABLE_FLAG,
                                            TENANTS_PROPORTIONATE_SHARE,
                                            STATUS,
                                            ATTRIBUTE_CATEGORY,
                                            ATTRIBUTE1,
                                            ATTRIBUTE2,
                                            ATTRIBUTE3,
                                            ATTRIBUTE4,
                                            ATTRIBUTE5,
                                            ATTRIBUTE6,
                                            ATTRIBUTE7,
                                            ATTRIBUTE8,
                                            ATTRIBUTE9,
                                            ATTRIBUTE10,
                                            ATTRIBUTE11,
                                            ATTRIBUTE12,
                                            ATTRIBUTE13,
                                            ATTRIBUTE14,
                                            ATTRIBUTE15,
                                            ORG_ID,
                                            RECOVERY_TYPE_CODE,
                                            RECOVERY_SPACE_STD_CODE,
                                            FIN_OBLIG_END_DATE,
                                            CUSTOMER_ID,
                                            CUSTOMER_SITE_USE_ID,
                                            LEASE_RENTABLE_AREA,
                                            LEASE_USABLE_AREA,
                                            LEASE_ASSIGNABLE_AREA,
                                            LEASE_LOAD_FACTOR,
                                            LOCATION_RENTABLE_AREA,
                                            LOCATION_USABLE_AREA,
                                            LOCATION_ASSIGNABLE_AREA,
                                            LOCATION_LOAD_FACTOR,
                                            ALLOCATED_AREA,
                                            ALLOCATED_AREA_PCT)
                                            values
                                            (ln_tenancy_id,
                                            sysdate,
                                            fnd_global.user_id,
                                            sysdate,
                                            fnd_global.user_id,
                                            fnd_global.user_id,
                                            ln_loc_id,
                                            ln_lease_id,
                                            ln_lease_change_id,
                                            o.TENANCY_USAGE_LOOKUP_CODE,
                                            o.PRIMARY_FLAG,
                                            o.ESTIMATED_OCCUPANCY_DATE,
                                            o.OCCUPANCY_DATE,
                                            o.EXPIRATION_DATE,
                                            o.ASSIGNABLE_FLAG,
                                            o.SUBLEASEABLE_FLAG,
                                            o.TENANTS_PROPORTIONATE_SHARE,
                                            o.STATUS,
                                            o.ATTRIBUTE_CATEGORY,
                                            o.ATTRIBUTE1,
                                            o.ATTRIBUTE2,
                                            o.ATTRIBUTE3,
                                            o.ATTRIBUTE4,
                                            o.ATTRIBUTE5,
                                            o.ATTRIBUTE6,
                                            o.ATTRIBUTE7,
                                            o.ATTRIBUTE8,
                                            o.ATTRIBUTE9,
                                            o.ATTRIBUTE10,
                                            o.ATTRIBUTE11,
                                            o.ATTRIBUTE12,
                                            o.ATTRIBUTE13,
                                            o.ATTRIBUTE14,
                                            o.ATTRIBUTE15,
                                            o.ORG_ID,
                                            o.RECOVERY_TYPE_CODE,
                                            o.RECOVERY_SPACE_STD_CODE,
                                            o.FIN_OBLIG_END_DATE,
                                            o.CUSTOMER_ID,
                                            o.CUSTOMER_SITE_USE_ID,
                                            o.LEASE_RENTABLE_AREA,
                                            o.LEASE_USABLE_AREA,
                                            o.LEASE_ASSIGNABLE_AREA,
                                            o.LEASE_LOAD_FACTOR,
                                            o.LOCATION_RENTABLE_AREA,
                                            o.LOCATION_USABLE_AREA,
                                            o.LOCATION_ASSIGNABLE_AREA,
                                            o.LOCATION_LOAD_FACTOR,
                                            o.ALLOCATED_AREA,
                                            o.ALLOCATED_AREA_PCT);


                             FND_FILE.PUT_LINE(FND_FILE.LOG,'Successfully inserted in PN_TENANCIES_ALL');

                exception when others then
               FND_FILE.PUT_LINE(FND_FILE.LOG,sqlcode||'-'||sqlerrm||'Error in inserting in PN_TENANCIES_ALL');
               end;
               

                commit;

             FND_FILE.PUT_LINE(FND_FILE.LOG,'Parameter for PN_RIGHTS_ALL'||'  '||k.lease_id);



                        for p in cur_right(k.lease_id
                            --,m.LEASE_CHANGE_ID
                                            )
                                loop

                                    select APPS.PN_RIGHTS_S.NEXTVAL into ln_right_id from dual;

                                        fnd_file.put_line(fnd_file.log,'Right Id : '||ln_right_id);

                                        --inserting in to PN_RIGHTS_ALL--


                       BEGIN
                                    insert into APPS.PN_RIGHTS_ALL
                                                    (RIGHT_ID,
                                                    LAST_UPDATE_DATE,
                                                    LAST_UPDATED_BY,
                                                    CREATION_DATE,
                                                    CREATED_BY,
                                                    LAST_UPDATE_LOGIN,
                                                    LEASE_ID,
                                                    LEASE_CHANGE_ID,
                                                    RIGHT_NUM,
                                                    RIGHT_TYPE_CODE,
                                                    RIGHT_STATUS_CODE,
                                                    RIGHT_REFERENCE,
                                                    START_DATE,
                                                    EXPIRATION_DATE,
                                                    RIGHT_COMMENTS,
                                                    ATTRIBUTE_CATEGORY,
                                                    ATTRIBUTE1,
                                                    ATTRIBUTE2,
                                                    ATTRIBUTE3,
                                                    ATTRIBUTE4,
                                                    ATTRIBUTE5,
                                                    ATTRIBUTE6,
                                                    ATTRIBUTE7,
                                                    ATTRIBUTE9,
                                                    ATTRIBUTE8,
                                                    ATTRIBUTE10,
                                                    ATTRIBUTE11,
                                                    ATTRIBUTE12,
                                                    ATTRIBUTE13,
                                                    ATTRIBUTE14,
                                                    ATTRIBUTE15,
                                                    ORG_ID)
                                                    values
                                                    (ln_right_id,
                                                    sysdate,
                                                    fnd_global.user_id,
                                                    sysdate,
                                                    fnd_global.user_id,
                                                    fnd_global.user_id,
                                                    ln_lease_id,
                                                    ln_lease_change_id,
                                                    p.RIGHT_NUM,
                                                    p.RIGHT_TYPE_CODE,
                                                    p.RIGHT_STATUS_CODE,
                                                    p.RIGHT_REFERENCE,
                                                    p.START_DATE,
                                                    p.EXPIRATION_DATE,
                                                    p.RIGHT_COMMENTS,
                                                    p.ATTRIBUTE_CATEGORY,
                                                    p.ATTRIBUTE1,
                                                    p.ATTRIBUTE2,
                                                    p.ATTRIBUTE3,
                                                    p.ATTRIBUTE4,
                                                    p.ATTRIBUTE5,
                                                    p.ATTRIBUTE6,
                                                    p.ATTRIBUTE7,
                                                    p.ATTRIBUTE9,
                                                    p.ATTRIBUTE8,
                                                    p.ATTRIBUTE10,
                                                    p.ATTRIBUTE11,
                                                    p.ATTRIBUTE12,
                                                    p.ATTRIBUTE13,
                                                    p.ATTRIBUTE14,
                                                    p.ATTRIBUTE15,
                                                    p.ORG_ID);

                         FND_FILE.PUT_LINE(FND_FILE.LOG,'Successfully inserted in PN_RIGHTS_ALL');

                exception when others then
               FND_FILE.PUT_LINE(FND_FILE.LOG,sqlcode||'-'||sqlerrm||'Error in inserting in PN_RIGHTS_ALL');
               end;
               

                        commit;

                    FND_FILE.PUT_LINE(FND_FILE.LOG,'Parameter for PN_OPTIONS_ALL'||'  '||k.lease_id);


                        for q in cur_option(k.lease_id
                            --,m.LEASE_CHANGE_ID
                                            )
             loop

                                    select APPS.PN_OPTIONS_S.NEXTVAL into ln_option_id from dual;

                                    fnd_file.put_line(fnd_file.log,'Option Id : '||ln_option_id);

                                        --inserting in to PN_OPTIONS_ALL--
    
                BEGIN

                                            insert into APPS.PN_OPTIONS_ALL
                                                        (OPTION_ID,
                                                        LAST_UPDATE_DATE,
                                                        LAST_UPDATED_BY,
                                                        CREATION_DATE,
                                                        CREATED_BY,
                                                        LAST_UPDATE_LOGIN,
                                                        LEASE_ID,
                                                        LEASE_CHANGE_ID,
                                                        OPTION_NUM,
                                                        OPTION_TYPE_CODE,
                                                        START_DATE,
                                                        EXPIRATION_DATE,
                                                        OPTION_SIZE,
                                                        UOM_CODE,
                                                        OPTION_STATUS_LOOKUP_CODE,
                                                        ATTRIBUTE_CATEGORY,
                                                        ATTRIBUTE1,
                                                        ATTRIBUTE2,
                                                        ATTRIBUTE3,
                                                        ATTRIBUTE4,
                                                        ATTRIBUTE5,
                                                        ATTRIBUTE6,
                                                        ATTRIBUTE7,
                                                        ATTRIBUTE8,
                                                        ATTRIBUTE9,
                                                        ATTRIBUTE10,
                                                        ATTRIBUTE11,
                                                        ATTRIBUTE12,
                                                        ATTRIBUTE13,
                                                        ATTRIBUTE14,
                                                        ATTRIBUTE15,
                                                        ORG_ID,
                                                        OPTION_EXER_START_DATE,
                                                        OPTION_EXER_END_DATE,
                                                        OPTION_ACTION_DATE,
                                                        OPTION_COST,
                                                        OPTION_AREA_CHANGE,
                                                        OPTION_REFERENCE,
                                                        OPTION_NOTICE_REQD,
                                                        OPTION_COMMENTS)
                                                        values
                                                        (ln_option_id,
                                                        sysdate,
                                                        fnd_global.user_id,
                                                        sysdate,
                                                        fnd_global.user_id,
                                                        fnd_global.user_id,
                                                        ln_lease_id,
                                                        ln_lease_change_id,
                                                        q.OPTION_NUM,
                                                        q.OPTION_TYPE_CODE,
                                                        q.START_DATE,
                                                        q.EXPIRATION_DATE,
                                                        q.OPTION_SIZE,
                                                        q.UOM_CODE,
                                                        q.OPTION_STATUS_LOOKUP_CODE,
                                                        q.ATTRIBUTE_CATEGORY,
                                                        q.ATTRIBUTE1,
                                                        q.ATTRIBUTE2,
                                                        q.ATTRIBUTE3,
                                                        q.ATTRIBUTE4,
                                                        q.ATTRIBUTE5,
                                                        q.ATTRIBUTE6,
                                                        q.ATTRIBUTE7,
                                                        q.ATTRIBUTE8,
                                                        q.ATTRIBUTE9,
                                                        q.ATTRIBUTE10,
                                                        q.ATTRIBUTE11,
                                                        q.ATTRIBUTE12,
                                                        q.ATTRIBUTE13,
                                                        q.ATTRIBUTE14,
                                                        q.ATTRIBUTE15,
                                                        q.ORG_ID,
                                                        q.OPTION_EXER_START_DATE,
                                                        q.OPTION_EXER_END_DATE,
                                                        q.OPTION_ACTION_DATE,
                                                        q.OPTION_COST,
                                                        q.OPTION_AREA_CHANGE,
                                                        q.OPTION_REFERENCE,
                                                        q.OPTION_NOTICE_REQD,
                                                        q.OPTION_COMMENTS);



                    FND_FILE.PUT_LINE(FND_FILE.LOG,'Successfully inserted in PN_OPTIONS_ALL');
                    exception when others then
                    FND_FILE.PUT_LINE(FND_FILE.LOG,sqlcode||'-'||sqlerrm||'Error in inserting in PN_OPTIONS_ALL');
               end;

                commit;

                     FND_FILE.PUT_LINE(FND_FILE.LOG,'Parameter for PN_NOTE_HEADERS'||'  '||k.lease_id);



                for r in cur_note(k.lease_id)
              loop

                                select APPS.PN_NOTE_HEADERS_S.NEXTVAL into ln_note_header_id from dual;

                                fnd_file.put_line(fnd_file.log,'Note Header Id : '||ln_note_header_id);

                                --inserting in to PN_NOTE_HEADERS--

                    BEGIN

                                                insert into APPS.PN_NOTE_HEADERS
                                                            (NOTE_HEADER_ID,
                                                            NOTE_DATE,
                                                            LAST_UPDATED_BY,
                                                            LAST_UPDATE_DATE,
                                                            LAST_UPDATE_LOGIN,
                                                            CREATED_BY,
                                                            CREATION_DATE,
                                                            NOTE_TYPE_LOOKUP_CODE,
                                                            LEASE_ID,
                                                            ATTRIBUTE_CATEGORY,
                                                            ATTRIBUTE1,
                                                            ATTRIBUTE2,
                                                            ATTRIBUTE3,
                                                            ATTRIBUTE4,
                                                            ATTRIBUTE5,
                                                            ATTRIBUTE6,
                                                            ATTRIBUTE7,
                                                            ATTRIBUTE8,
                                                            ATTRIBUTE9,
                                                            ATTRIBUTE10,
                                                            ATTRIBUTE11,
                                                            ATTRIBUTE12,
                                                            ATTRIBUTE13,
                                                            ATTRIBUTE14,
                                                            ATTRIBUTE15)
                                                            values
                                                            (ln_note_header_id,
                                                            sysdate,
                                                            fnd_global.user_id,
                                                            sysdate,
                                                            fnd_global.user_id,
                                                            fnd_global.user_id,
                                                            sysdate,
                                                            r.NOTE_TYPE_LOOKUP_CODE,
                                                            ln_lease_id,
                                                            r.ATTRIBUTE_CATEGORY,
                                                            r.ATTRIBUTE1,
                                                            r.ATTRIBUTE2,
                                                            r.ATTRIBUTE3,
                                                            r.ATTRIBUTE4,
                                                            r.ATTRIBUTE5,
                                                            r.ATTRIBUTE6,
                                                            r.ATTRIBUTE7,
                                                            r.ATTRIBUTE8,
                                                            r.ATTRIBUTE9,
                                                            r.ATTRIBUTE10,
                                                            r.ATTRIBUTE11,
                                                            r.ATTRIBUTE12,
                                                            r.ATTRIBUTE13,
                                                            r.ATTRIBUTE14,
                                                            r.ATTRIBUTE15);

                                                        FND_FILE.PUT_LINE(FND_FILE.LOG,'Successfully inserted in PN_NOTE_HEADERS_ALL');

                                        exception when others then
               FND_FILE.PUT_LINE(FND_FILE.LOG,sqlcode||'-'||sqlerrm||'Error in inserting in PN_NOTE_HEADERS_ALL');
               end;

                commit;

                                                 FND_FILE.PUT_LINE(FND_FILE.LOG,'Parameter for PN_PAYMENTS_TERMS_ALL'||'  '||k.lease_id);


                            for s in cur_payment(--j.location_id,
                                    k.lease_id
                            --,m.LEASE_CHANGE_ID
                                )
                     loop

                                select APPS.PN_PAYMENT_TERMS_S.NEXTVAL 
                                into ln_payment_term_id from dual;

                                        fnd_file.put_line(fnd_file.log,'Payment Term Id : '||ln_payment_term_id);
                                    begin
                                        select vendor_site_id into v_vendor_site_id 
                                        from APPS.po_vendor_sites_all
                                        where vendor_id=s.vendor_id
                                        and org_id=s.ORG_ID;
                                        exception
                                        when no_data_found then
                                        fnd_file.put_line(fnd_file.log,sqlcode||'-'||sqlerrm||'Vendor_site_id does not exists for vendor_id'||s.vendor_id);
                                        when others then
                                        fnd_file.put_line(fnd_file.log,sqlcode||'-'||sqlerrm||'More than one Vendor_site_id exists for vendor_id'||s.vendor_id);
                                        end;

                                        fnd_file.put_line(fnd_file.log,'Vendor Site Id : '||v_vendor_site_id);

                                    select DEFAULT_LEGAL_CONTEXT_ID into v_legal_entity_id 
                                    from APPS.hr_operating_units
                                    where organization_id=s.ORG_ID;

                    fnd_file.put_line(fnd_file.log,'Legal Entity Id : '||v_legal_entity_id);

                        --inserting in to PN_PAYMENT_TERMS_ALL--

                        BEGIN

                                            insert into APPS.PN_PAYMENT_TERMS_ALL
                                                        (PAYMENT_TERM_ID,
                                                        LAST_UPDATE_DATE,
                                                        LAST_UPDATED_BY,
                                                        CREATION_DATE,
                                                        CREATED_BY,
                                                        LAST_UPDATE_LOGIN,
                                                        PAYMENT_PURPOSE_CODE,
                                                        PAYMENT_TERM_TYPE_CODE,
                                                        FREQUENCY_CODE,
                                                        LEASE_ID,
                                                        LEASE_CHANGE_ID,
                                                        START_DATE,
                                                        END_DATE,
                                                        CODE_COMBINATION_ID,
                                                        VENDOR_ID,
                                                        VENDOR_SITE_ID,
                                                        TARGET_DATE,
                                                        ACTUAL_AMOUNT,
                                                        ESTIMATED_AMOUNT,
                                                        SET_OF_BOOKS_ID,
                                                        CURRENCY_CODE,
                                                        RATE,
                                                        ATTRIBUTE_CATEGORY,
                                                        ATTRIBUTE1,
                                                        ATTRIBUTE2,
                                                        ATTRIBUTE3,
                                                        ATTRIBUTE4,
                                                        ATTRIBUTE5,
                                                        ATTRIBUTE6,
                                                        ATTRIBUTE7,
                                                        ATTRIBUTE8,
                                                        ATTRIBUTE9,
                                                        ATTRIBUTE10,
                                                        ATTRIBUTE11,
                                                        ATTRIBUTE12,
                                                        ATTRIBUTE13,
                                                        ATTRIBUTE14,
                                                        ATTRIBUTE15,
                                                        ORG_ID,
                                                        CUSTOMER_ID,
                                                        CUSTOMER_SITE_USE_ID,
                                                        NORMALIZE,
                                                        LOCATION_ID,
                                                        SCHEDULE_DAY,
                                                        CUST_SHIP_SITE_ID,
                                                        AP_AR_TERM_ID,
                                                        CUST_TRX_TYPE_ID,
                                                        PROJECT_ID,
                                                        TASK_ID,
                                                        ORGANIZATION_ID,
                                                        EXPENDITURE_TYPE,
                                                        EXPENDITURE_ITEM_DATE,
                                                        TAX_GROUP_ID,
                                                        TAX_CODE_ID,
                                                        TAX_INCLUDED,
                                                        DISTRIBUTION_SET_ID,
                                                        INV_RULE_ID,
                                                        ACCOUNT_RULE_ID,
                                                        SALESREP_ID,
                                                        APPROVED_BY,
                                                        STATUS,
                                                        INDEX_PERIOD_ID,
                                                        INDEX_TERM_INDICATOR,
                                                        PO_HEADER_ID,
                                                        CUST_PO_NUMBER,
                                                        RECEIPT_METHOD_ID,
                                                        PROJECT_ATTRIBUTE_CATEGORY,
                                                        PROJECT_ATTRIBUTE1,
                                                        PROJECT_ATTRIBUTE2,
                                                        PROJECT_ATTRIBUTE3,
                                                        PROJECT_ATTRIBUTE4,
                                                        PROJECT_ATTRIBUTE5,
                                                        PROJECT_ATTRIBUTE6,
                                                        PROJECT_ATTRIBUTE7,
                                                        PROJECT_ATTRIBUTE8,
                                                        PROJECT_ATTRIBUTE9,
                                                        PROJECT_ATTRIBUTE10,
                                                        PROJECT_ATTRIBUTE11,
                                                        PROJECT_ATTRIBUTE12,
                                                        PROJECT_ATTRIBUTE13,
                                                        PROJECT_ATTRIBUTE14,
                                                        PROJECT_ATTRIBUTE15,
                                                        VAR_RENT_INV_ID,
                                                        VAR_RENT_TYPE,
                                                        CHANGED_FLAG,
                                                        NORM_START_DATE,
                                                        TERM_TEMPLATE_ID,
                                                        EVENT_TYPE_CODE,
                                                        LEASE_STATUS,
                                                        NORM_END_DATE,
                                                        RECOVERABLE_FLAG,
                                                        PERIOD_BILLREC_ID,
                                                        AMOUNT_TYPE,
                                                        REC_AGR_LINE_ID,
                                                        GROUPING_RULE_ID,
                                                        AREA_TYPE_CODE,
                                                        AREA,
                                                        TERM_ALTERED_FLAG,
                                                        SOURCE_CODE,
                                                        TERM_COMMENTS,
                                                        LEGAL_ENTITY_ID,
                                                        TAX_CLASSIFICATION_CODE,
                                                        INDEX_NORM_FLAG,
                                                        PARENT_TERM_ID,
                                                        INCLUDE_IN_VAR_RENT,
                                                        UPDATE_NBP_FLAG,
                                                        RECUR_BB_CALC_DATE,
                                                        OPEX_TYPE,
                                                        OPEX_AGR_ID,
                                                        OPEX_RECON_ID)
                                                        values
                                                        (ln_payment_term_id,
                                                        sysdate,
                                                        fnd_global.user_id,
                                                        sysdate,
                                                        fnd_global.user_id,
                                                        fnd_global.user_id,
                                                        s.PAYMENT_PURPOSE_CODE,
                                                        s.PAYMENT_TERM_TYPE_CODE,
                                                        s.FREQUENCY_CODE,
                                                        ln_lease_id,
                                                        ln_lease_change_id,
                                                        s.START_DATE,
                                                        s.END_DATE,
                                                        s.CODE_COMBINATION_ID,
                                                        s.VENDOR_ID,
                                                        v_vendor_site_id,
                                                        s.TARGET_DATE,
                                                        s.ACTUAL_AMOUNT,
                                                        s.ESTIMATED_AMOUNT,
                                                        FND_PROFILE.value('GL_SET_OF_BKS_ID'),
                                                        s.CURRENCY_CODE,
                                                        s.RATE,
                                                        s.ATTRIBUTE_CATEGORY,
                                                        s.ATTRIBUTE1,
                                                        s.ATTRIBUTE2,
                                                        s.ATTRIBUTE3,
                                                        s.ATTRIBUTE4,
                                                        null,
                                                        s.ATTRIBUTE6,
                                                        s.ATTRIBUTE7,
                                                        s.ATTRIBUTE8,
                                                        s.ATTRIBUTE9,
                                                        s.ATTRIBUTE10,
                                                        s.ATTRIBUTE11,
                                                        s.ATTRIBUTE12,
                                                        s.ATTRIBUTE13,
                                                        s.ATTRIBUTE14,
                                                        s.ATTRIBUTE15,
                                                        s.ORG_ID,
                                                        s.CUSTOMER_ID,
                                                        s.CUSTOMER_SITE_USE_ID,
                                                        s.NORMALIZE,
                                                        ln_loc_id,
                                                        s.SCHEDULE_DAY,
                                                        s.CUST_SHIP_SITE_ID,
                                                        s.AP_AR_TERM_ID,
                                                        s.CUST_TRX_TYPE_ID,
                                                        s.PROJECT_ID,
                                                        s.TASK_ID,
                                                        s.ORGANIZATION_ID,
                                                        s.EXPENDITURE_TYPE,
                                                        s.EXPENDITURE_ITEM_DATE,
                                                        s.TAX_GROUP_ID,
                                                        s.TAX_CODE_ID,
                                                        s.TAX_INCLUDED,
                                                        s.DISTRIBUTION_SET_ID,
                                                        s.INV_RULE_ID,
                                                        s.ACCOUNT_RULE_ID,
                                                        s.SALESREP_ID,
                                                        s.APPROVED_BY,
                                                        s.STATUS,
                                                        s.INDEX_PERIOD_ID,
                                                        s.INDEX_TERM_INDICATOR,
                                                        s.PO_HEADER_ID,
                                                        s.CUST_PO_NUMBER,
                                                        s.RECEIPT_METHOD_ID,
                                                        s.PROJECT_ATTRIBUTE_CATEGORY,
                                                        s.PROJECT_ATTRIBUTE1,
                                                        s.PROJECT_ATTRIBUTE2,
                                                        s.PROJECT_ATTRIBUTE3,
                                                        s.PROJECT_ATTRIBUTE4,
                                                        s.PROJECT_ATTRIBUTE5,
                                                        s.PROJECT_ATTRIBUTE6,
                                                        s.PROJECT_ATTRIBUTE7,
                                                        s.PROJECT_ATTRIBUTE8,
                                                        s.PROJECT_ATTRIBUTE9,
                                                        s.PROJECT_ATTRIBUTE10,
                                                        s.PROJECT_ATTRIBUTE11,
                                                        s.PROJECT_ATTRIBUTE12,
                                                        s.PROJECT_ATTRIBUTE13,
                                                        s.PROJECT_ATTRIBUTE14,
                                                        s.PROJECT_ATTRIBUTE15,
                                                        s.VAR_RENT_INV_ID,
                                                        s.VAR_RENT_TYPE,
                                                        s.CHANGED_FLAG,
                                                        s.NORM_START_DATE,
                                                        s.TERM_TEMPLATE_ID,
                                                        s.EVENT_TYPE_CODE,
                                                        s.LEASE_STATUS,
                                                        s.NORM_END_DATE,
                                                        s.RECOVERABLE_FLAG,
                                                        s.PERIOD_BILLREC_ID,
                                                        s.AMOUNT_TYPE,
                                                        s.REC_AGR_LINE_ID,
                                                        s.GROUPING_RULE_ID,
                                                        s.AREA_TYPE_CODE,
                                                        s.AREA,
                                                        s.TERM_ALTERED_FLAG,
                                                        s.SOURCE_CODE,
                                                        s.TERM_COMMENTS,
                                                        v_legal_entity_id,
                                                        s.TAX_CLASSIFICATION_CODE,
                                                        s.INDEX_NORM_FLAG,
                                                        s.PARENT_TERM_ID,
                                                        s.INCLUDE_IN_VAR_RENT,
                                                        s.UPDATE_NBP_FLAG,
                                                        s.RECUR_BB_CALC_DATE,
                                                        s.OPEX_TYPE,
                                                        s.OPEX_AGR_ID,
                                                        s.OPEX_RECON_ID);

                        FND_FILE.PUT_LINE(FND_FILE.LOG,'Successfully inserted in PN_PAYMENT_TERMS_ALL');

                        exception when others then
                    FND_FILE.PUT_LINE(FND_FILE.LOG,sqlcode||'-'||sqlerrm||'Error in inserting in PN_PAYMENT_TERMS_ALL');
                    end;

                    commit;

                    FND_FILE.PUT_LINE(FND_FILE.LOG,'Parameter for PN_DISTRIBUTIONS_ALL'||'  '||s.PAYMENT_TERM_ID);


                    for t in cur_distribution(s.PAYMENT_TERM_ID)
                 loop

                        select APPS.PN_DISTRIBUTIONS_S.NEXTVAL
                        into ln_distribution_id from dual;

                         fnd_file.put_line(fnd_file.log,'Distribution id : '||ln_distribution_id);

                            --inserting in to pn_distributions_all--

                    BEGIN

                                                  insert into APPS.pn_distributions_all
                                                                    (DISTRIBUTION_ID,
                                                                    PAYMENT_TERM_ID,
                                                                    TERM_TEMPLATE_ID,
                                                                    ACCOUNT_ID,
                                                                    ACCOUNT_CLASS,
                                                                    PERCENTAGE,
                                                                    LINE_NUMBER,
                                                                    LAST_UPDATE_DATE,
                                                                    LAST_UPDATED_BY,
                                                                    CREATION_DATE,
                                                                    CREATED_BY,
                                                                    LAST_UPDATE_LOGIN,
                                                                    ATTRIBUTE_CATEGORY,
                                                                    ATTRIBUTE1,
                                                                    ATTRIBUTE2,
                                                                    ATTRIBUTE3,
                                                                    ATTRIBUTE4,
                                                                    ATTRIBUTE5,
                                                                    ATTRIBUTE6,
                                                                    ATTRIBUTE7,
                                                                    ATTRIBUTE8,
                                                                    ATTRIBUTE9,
                                                                    ATTRIBUTE10,
                                                                    ATTRIBUTE11,
                                                                    ATTRIBUTE12,
                                                                    ATTRIBUTE13,
                                                                    ATTRIBUTE14,
                                                                    ATTRIBUTE15,
                                                                    ORG_ID)
                                                                    values
                                                                    (ln_distribution_id,
                                                                    ln_payment_term_id,
                                                                    t.TERM_TEMPLATE_ID,
                                                                    t.NEW_ACCOUNT_ID,
                                                                    t.ACCOUNT_CLASS,
                                                                    t.PERCENTAGE,
                                                                    t.LINE_NUMBER,
                                                                    sysdate,
                                                                    fnd_global.user_id,
                                                                    sysdate,
                                                                    fnd_global.user_id,
                                                                    fnd_global.user_id,
                                                                    t.ATTRIBUTE_CATEGORY,
                                                                    t.ATTRIBUTE1,
                                                                    t.ATTRIBUTE2,
                                                                    t.ATTRIBUTE3,
                                                                    t.ATTRIBUTE4,
                                                                    t.ATTRIBUTE5,
                                                                    t.ATTRIBUTE6,
                                                                    t.ATTRIBUTE7,
                                                                    t.ATTRIBUTE8,
                                                                    t.ATTRIBUTE9,
                                                                    t.ATTRIBUTE10,
                                                                    t.ATTRIBUTE11,
                                                                    t.ATTRIBUTE12,
                                                                    t.ATTRIBUTE13,
                                                                    t.ATTRIBUTE14,
                                                                    t.ATTRIBUTE15,
                                                                    t.ORG_ID);


                        FND_FILE.PUT_LINE(FND_FILE.LOG,'Successfully inserted in PN_DISTRIBUTIONS_ALL');


                        exception when others then
                       FND_FILE.PUT_LINE(FND_FILE.LOG,sqlcode||'-'||sqlerrm||'Error in inserting in PN_DISTRIBUTIONS_ALL');
                       end;

                    commit;


                  end loop;
                end loop;
               end loop;
               end loop;
            end loop;
           end loop;
          end loop;
         end loop;
       end loop;
      end loop;
     end loop;
    end loop;
      commit;
  end; 
/


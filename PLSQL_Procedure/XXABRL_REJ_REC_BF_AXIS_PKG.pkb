CREATE OR REPLACE PACKAGE BODY APPS.XXABRL_REJ_REC_BF_AXIS_PKG AS

  PROCEDURE XXABRL_REJ_REC_BF_AXIS_PRC(errbuf  OUT VARCHAR2,
                                        retcode OUT VARCHAR2,
                                        P_FROM_DATE  VARCHAR2 ,
                                        P_TO_DATE   VARCHAR2  )

   IS

    --TO SELECT BANK (AXIS BANK)
    CURSOR BANK_CUR IS
      SELECT bankorgprofile.party_id bank_id,
             bankparty.party_name    BANK_NAME
        FROM hz_parties               bankparty,
             hz_organization_profiles bankorgprofile,
             hz_code_assignments      bankca
       WHERE 1 = 1
         AND bankparty.party_id = bankorgprofile.party_id
         AND bankca.owner_table_name = 'HZ_PARTIES'
         AND bankca.owner_table_id = bankparty.party_id
         AND bankca.class_category = 'BANK_INSTITUTION_TYPE'
         AND bankca.class_code IN ('BANK', 'CLEARINGHOUSE')
         -- AND bankparty.party_id = 3053
         AND UPPER(bankorgprofile.organization_name) IN ('AXIS BANK LTD');

    -- TO SELECT ALL PAYMENTS RELATED TO AXIS
    CURSOR PAYMENT_CUR(p_bank_id number) IS
    select
        decode(aca.payment_type_flag, 'A',(select cb.bank_account_num
            from ce_bank_acct_uses_all cbau,ce_bank_accounts cb
            where cbau.bank_acct_use_id =aca.ce_bank_acct_use_id
            and   cbau.bank_account_id = cb.bank_account_id),aca.bank_account_num) bank_account_num,--Payment Batch
        --aca.bank_account_num,
        cba.bank_account_type,
        hou.name op_name,
        aca.checkrun_name run_identification,
        fu.user_name,
        apss.attribute10 pay_location,--
        aca.checkrun_name batch_id,
        cbb.city corp_bank_city,
        cbb.bank_branch_name corp_bank_brnch,
        /*(select distinct ai.invoice_type_lookup_code
            from ap_invoices_all ai,ap_invoice_payments_all aip
               where ai.invoice_id = aip.invoice_id
               and   aip.check_id=  aca.check_id
         )pay_type_code,*/ --Commented on 21-AUG-2010
        aps.segment1 sup_no,
        aca.amount pay_amount,
        aca.currency_code currency,
        decode(aca.payment_method_code,'N','N','RTGS','R','CHECK','C','DD','D','IFT','B') prod_code,
        nvl(aca.anticipated_value_date,aca.check_date) trx_val_date,
        aca.check_number ,
        aca.check_date pay_run_date,
        aca.check_date pay_inst_date,
        apss.attribute4 supp_acc_no,--
        apss.attribute2 supp_bank_name,--
        apss.attribute9 supp_bank_acc_type,--
        decode(aca.payment_method_code,'RTGS',apss.attribute8,apss.attribute7) supp_ifsc_code,--
        nvl(aps.vendor_name_alt,aps.vendor_name) sup_name,
        aps.vendor_name sup_alt_name,
        --aps.vendor_name sup_name,
        --aps.vendor_name_alt sup_alt_name,
         (apss.address_line1 || ';' || apss.address_line2 || ';' ||
             apss.address_line2 || ';' || apss.state || ';' || apss.zip) site_address,
        apss.vendor_site_code,
        apss.zip pin_code,
        (apss.attribute11||','||apss.attribute12||','||apss.attribute13) email_id,
        apss.phone ,
        --apss.attribute11 supp_bank_acc_city, -- Commented already attribute11 is used for mail id
        NULL supp_bank_acc_city,
        apss.attribute3 supp_bank_bran,--
        aca.check_id,
        aca.org_id,
        aca.payment_id,
        aca.doc_sequence_value voucher_no,
        apss.vendor_site_id,
        cba.bank_id,
        cba.bank_branch_id,
        aca.ce_bank_acct_use_id,
        aca.party_id,
        aps.vendor_id,
        aca.created_by,
        aca.creation_date,
        aca.last_update_date,
        aca.last_updated_by
        from ap_checks_all         aca,
             ap_suppliers          aps,
             ap_supplier_sites_all apss,
             iby_payments_all      ip,
             ce_bank_accounts      cba,
             hr_operating_units    hou,
             fnd_user fu,
             cebv_bank_branches cbb
       where 1 = 1
         and aca.vendor_id = aps.vendor_id
         and aps.vendor_id = apss.vendor_id
         and aca.vendor_site_id = apss.vendor_site_id
         and aca.payment_id = ip.payment_id
         and ip.internal_bank_account_id = cba.bank_account_id
         and ip.payment_status in ('FORMATTED', 'ISSUED')
         and aca.org_id = hou.organization_id
         and aca.void_date is null
         and aca.created_by = fu.user_id
         and cba.bank_id =p_bank_id
         and cba.bank_branch_id = cbb.bank_branch_id
         and cba.bank_account_id in (SELECT LOOKUP_CODE
                                        from  fnd_lookup_values
                                        where lookup_type ='XXABRL_AXIS_BANK_H2H_ACCOUNT'
                                        and   TO_NUMBER(LOOKUP_CODE) =cba.bank_account_id
                                           and nvl(aca.anticipated_value_date,aca.check_date)>=start_date_active 
                                        -- above start date condition added by sai 06-OCT-2010
                                     )
         and UPPER(aca.bank_account_name) = UPPER(cba.bank_account_name)
         and trunc(aca.creation_date) between TO_DATE(P_FROM_DATE,'YYYY/MM/DD HH24:MI:SS')
                                                    and    TO_DATE(P_TO_DATE,'YYYY/MM/DD HH24:MI:SS')
         --Restrict the Duplicate Payments
         and not exists (select check_id
                                    from axis_payment_table apt
                                    where apt.check_id = aca.check_id) ;


    --TO SELECT INVOICES AGAINST EACH PAYMENT (ONLY FOR AXIS BANK)
    CURSOR INV_CUR(p_check_id NUMBER ) IS
     select ac.check_id, ai.invoice_num,ai.invoice_date,ai.invoice_amount,ai.invoice_type_lookup_code
      from ap_checks_all           ac,
           ap_invoice_payments_all aip,
           ap_invoices_all         ai
     where 1 = 1
       and ac.check_id =p_check_id
       and ac.check_id = aip.check_id
       and aip.invoice_id = ai.invoice_id;

    --local Variable Declaration
    p_bank_id           NUMBER;
    p_check_id          NUMBER;
    g_ever_failed       BOOLEAN         := FALSE;
    l_msg               VARCHAR2(2000);
--    l_header            VARCHAR2(4000);

BEGIN

    errbuf  := '';
    retcode := 0;

    fnd_file.put_line (
                   fnd_file.output,
                        CHR(9)||CHR(9)||CHR(9)||CHR(9)||'Axis Payments Rejected Report Due To Required Data Missing'
               );

  FND_FILE.PUT_LINE (FND_FILE.OUTPUT, CHR(9)||CHR(9)||CHR(9)||CHR(9)||'Report Run Date'|| trunc(sysdate));


          fnd_file.put_line(fnd_file.output, 'Bank Name'
                         || CHR(9)
                         ||'Circle Name'
                         || CHR (9)
                         || 'Bank Account type'
                         || CHR (9)
                         ||'Supplier Name'
                         || CHR (9)
                         || 'Supplier No'
                         || CHR (9)
                         || 'Supplier Site'
                         || CHR (9)
                         || 'Supplier Site Address'
                         || CHR (9)
                         || 'Check No'
                         || CHR (9)
                         || 'Supplier IFSC Code'
                         || CHR (9)
                         || 'Supplier Account No'
                         || CHR (9)
                         ||'Supplier Bank Account Type'
                         ||CHR (9)
                         || 'Payment Amount'
                         || CHR (9)
                         || 'Payment Method Code'
                         || CHR (9)
                         || 'Payment Date'
                         || CHR(9)
                         || 'Reject Reason'
                          );


   FOR v_bank IN BANK_CUR LOOP

    FOR v_payment IN PAYMENT_CUR(v_bank.bank_id) LOOP

       l_msg := NULL;

      fnd_file.put_line (
               fnd_file.LOG,
                  '  Processing Check ID.. '
               ||v_payment.check_id
            );
            -- Each time we loop through items
            -- g_ever_failed flag is set to FALSE
            g_ever_failed := FALSE;

        IF v_bank.bank_name  IS NULL THEN
             g_ever_failed := TRUE;
             l_msg := 'Bank Name is Null'
                   || ','
                                 || 'Bank Name is Mandatory ';

        END IF;


        IF v_payment.bank_account_num is null then
             g_ever_failed := TRUE;
         l_msg := 'Bank Account Number is Null'
                   || ','
                                 || 'Bank Account Number is Mandatory ' ;



        END IF;

         IF v_payment.op_name is null then
             g_ever_failed := TRUE;
         l_msg := 'Operating Unit is Null'
                   || ','
                                 || 'Operating Unit is Mandatory ' ;

        END IF;

        IF v_payment.bank_account_type is null then
             g_ever_failed := TRUE;
         l_msg := 'Corporate Bank Account Type is Null'
                   || ','
                                 || 'Corporate Bank Account Type is Mandatory ' ;


        END IF;

        if v_payment.sup_name is null then
             g_ever_failed := TRUE;
         l_msg := 'Supplier Name is Null'
                   || ','
                                 || 'Supplier Name is Mandatory ' ;



        end if;

          if v_payment.sup_no is null then
             g_ever_failed := TRUE;
         l_msg := 'Supplier Number is Null'
                   || ','
                                || 'Supplier Number is Mandatory ' ;

        end if;

        IF v_payment.vendor_site_code  IS NULL THEN
             g_ever_failed := TRUE;
             l_msg := 'Supplier Site Code is Null'
                   || ','
                                 || 'Supplier Site Code is Mandatory ' ;


        END IF;

        IF v_payment.site_address  IS NULL THEN
             g_ever_failed := TRUE;
             l_msg := 'Supplier Site Address is Null'
                   || ','
                                || 'Supplier Site Address is Mandatory ' ;


        END IF;


         IF v_payment.supp_bank_name  IS NULL THEN
             g_ever_failed := TRUE;
             l_msg := 'Supplier Bank Name is Null'
                   || ','
                                 || 'Supplier Bank Name is Mandatory ' ;


        END IF;
            
     --New Condition added for RTGS and NEFT 11-OCT-2010
                 
        IF v_payment.prod_code = 'R' AND  (v_payment.pay_amount) < 200000 
              THEN
                 g_ever_failed := TRUE;
                 l_msg := 'Payment Amount is less than 2 Lakh.'
                       || ','
                                     || 'For RTGS Payments Amount Should be morethan or equal to 2 Lakh.' ;
        END IF;

         IF v_payment.check_number  IS NULL THEN
             g_ever_failed := TRUE;
             l_msg := 'Supplier Check Number is Null'
                   || ','
                                 || 'Supplier Check Number is Mandatory ' ;


        END IF;

         IF v_payment.prod_code in ('R','N') AND 
                (v_payment.supp_ifsc_code  IS NULL or length(v_payment.supp_ifsc_code) <> 11) 
             THEN
             g_ever_failed := TRUE;
             l_msg := 'Supplier IFSC Code is Invalid/Null'
                   || ','
                                 || 'Supplier IFSC Code is Mandatory for RTGS and NEFT' ;



        END IF;


        IF v_payment.prod_code in ('R','N','B') AND v_payment.supp_acc_no  IS NULL THEN
             g_ever_failed := TRUE;
             l_msg := 'Supplier Account No is Null'
                   || ','
                                 || 'Supplier Account No is Mandatory ' ;


        END IF;

         IF v_payment.prod_code in ('R','N') AND v_payment.supp_bank_acc_type  IS NULL THEN
             g_ever_failed := TRUE;
             l_msg := 'Supplier Account Type is Null'
                   || ','
                                 || 'Supplier Account Type is Mandatory ';


        END IF;

        IF v_payment.pay_amount  IS NULL THEN
             g_ever_failed := TRUE;
             l_msg := 'Payment Amount is Null'
                   || ','
                                 || 'Payment Amount is Mandatory ' ;


        END IF;

        IF v_payment.prod_code  IS NULL THEN
             g_ever_failed := TRUE;
             l_msg := 'Payment Method is Null'
                   || ','
                                 || 'Payment Method is Mandatory ' ;

        END IF;

        IF v_payment.pay_run_date  IS NULL THEN
             g_ever_failed := TRUE;
             l_msg := 'Payment Date is Null'
                   || ','
                                 || 'Payment Date is Mandatory ';

        END IF;



      /*
          FOR v_inv in INV_CUR(v_payment.check_id) LOOP

                     IF v_inv.invoice_num  IS NULL THEN
                         g_ever_failed := TRUE;
                         l_msg := 'Invoice Number is Null'
                               || ','
                                             || 'Invoice Number is Mandatory ' ;
                        fnd_file.put_line (
                               fnd_file.output,
                                  l_msg ||'-'||v_payment.check_number
                            );
                    END IF;


                    IF v_inv.invoice_amount  IS NULL THEN
                         g_ever_failed := TRUE;
                         l_msg := 'Invoice Amount is Null'
                               || ','
                                             || 'Invoice Amount is Mandatory ' ;
                        fnd_file.put_line (
                               fnd_file.output,
                                  l_msg ||'-'||v_payment.check_number
                            );
                    END IF;

          END LOOP;
    */

       IF  g_ever_failed = TRUE THEN

              FND_FILE.PUT_LINE (FND_FILE.OUTPUT,
                                        v_payment.bank_account_num
                                     || CHR (9)
                                     || v_payment.op_name
                                     || CHR (9)
                                     || v_payment.bank_account_type
                                     || CHR (9)
                                     || v_payment.sup_name
                                     || CHR (9)
                                     || v_payment.sup_no
                                     || CHR (9)
                                     || v_payment.vendor_site_code
                                     || CHR (9)
                                     || v_payment.site_address
                                     || CHR (9)
                                     || v_payment.check_number
                                     || CHR (9)
                                     || v_payment.supp_ifsc_code
                                     || CHR (9)
                                     || v_payment.supp_acc_no
                                     || CHR (9)
                                     || v_payment.supp_bank_acc_type
                                     ||CHR (9)
                                     || v_payment.pay_amount
                                     || CHR (9)
                                     || v_payment.prod_code
                                     || CHR (9)
                                     || v_payment.pay_run_date
                                     || chr(9)
                                     || 'For the check number'||' '||v_payment.check_number||','||l_msg
                                     --|| CHR (9)
                                    -- || v_inv.invoice_num
                                    -- || CHR (9)
                                     --|| v_inv.invoice_amount
                                    );

        ELSE

         FND_FILE.PUT_LINE (FND_FILE.LOG,'Check number'||' '||v_payment.check_number||'is Processed');

         END IF;


    END LOOP;
      COMMIT;

   END LOOP;

  EXCEPTION

            WHEN NO_DATA_FOUND THEN
                l_msg := 'All Records are Processed';
                 FND_FILE.PUT_LINE (FND_FILE.OUTPUT,l_msg);

            WHEN OTHERS THEN
                errbuf := 'Other Exceptions '||SQLERRM;
                retcode := '1';


END XXABRL_REJ_REC_BF_AXIS_PRC;


END XXABRL_REJ_REC_BF_AXIS_PKG; 
/


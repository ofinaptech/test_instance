CREATE OR REPLACE PACKAGE BODY APPS.xxabrl_host_to_host_pgm
IS
   PROCEDURE xxabrl_host_host_import (
      x_err_buf    OUT   VARCHAR2,
      x_ret_code   OUT   NUMBER
   )
   AS
      v_req_id            NUMBER;
      le_load             EXCEPTION;
      lb_wait             BOOLEAN;
      lc_phase            VARCHAR2 (500);
      lc_status           VARCHAR2 (500);
      lc_dev_phase        VARCHAR2 (500);
      lc_dev_status       VARCHAR2 (500);
      lc_message          VARCHAR2 (2000);
      lc_error_flag       VARCHAR2 (1);
      xx_segment1         NUMBER             := 0;
      xx_retak_no         NUMBER             := 0;
      xx_navi_no          NUMBER             := 0;
      query1              VARCHAR2 (1000);
      gn_error            NUMBER             := 2;
      query_string        LONG;
      v_file              UTL_FILE.file_type;
      vmaxlinesize        NUMBER             := 32700;
      g_conc_request_id   NUMBER     := fnd_profile.VALUE ('CONC_REQUEST_ID');

      TYPE ref_cur IS REF CURSOR;

      xx_main             ref_cur;

      TYPE c_rec IS RECORD (
         two_series_vendor_name        ap_suppliers.vendor_name%TYPE,
         seven_series_vendor_name      ap_suppliers.vendor_name%TYPE,
         seven_series_vendor_no        ap_suppliers.segment1%TYPE,
         two_series_vendor_no          ap_suppliers.segment1%TYPE,
         two_series_vendor_site_id     ap_supplier_sites_all.vendor_site_id%TYPE,
         seven_series_vendor_site_id   ap_supplier_sites_all.vendor_site_id%TYPE,
         two_series_vendor_site_code   ap_supplier_sites_all.vendor_site_code%TYPE,
         two_s_purchasing_site_flag    VARCHAR2 (100),
         two_series_pay_site_flag      VARCHAR2 (100),
         attribute1                    ap_supplier_sites_all.attribute1%TYPE,
         attribute2                    ap_supplier_sites_all.attribute2%TYPE,
         attribute3                    ap_supplier_sites_all.attribute3%TYPE,
         attribute4                    ap_supplier_sites_all.attribute4%TYPE,
         attribute5                    ap_supplier_sites_all.attribute5%TYPE,
         attribute6                    ap_supplier_sites_all.attribute6%TYPE,
         attribute7                    ap_supplier_sites_all.attribute7%TYPE,
         attribute8                    ap_supplier_sites_all.attribute8%TYPE,
         attribute9                    ap_supplier_sites_all.attribute9%TYPE,
         attribute10                   ap_supplier_sites_all.attribute10%TYPE,
         attribute11                   ap_supplier_sites_all.attribute11%TYPE,
         attribute12                   ap_supplier_sites_all.attribute12%TYPE,
         attribute13                   ap_supplier_sites_all.attribute13%TYPE,
         attribute14                   ap_supplier_sites_all.attribute14%TYPE,
         org_id                        NUMBER,
         operating_name                VARCHAR2 (100),
         terms_id                      NUMBER,
         terms_name                    VARCHAR2 (1000),
         pay_group_lookup_code         VARCHAR2 (1000),
         status                        VARCHAR2 (100),
         vendor_site_code              ap_supplier_sites_all.vendor_site_code%TYPE,
         header_end_active             ap_suppliers.end_date_active%TYPE,
         site_end_active               ap_supplier_sites_all.inactive_date%TYPE
      );

      xx_rec              c_rec;
   BEGIN
      xx_segment1 := 0;
      xx_retak_no := 0;
      xx_navi_no := 0;
      query1 := NULL;
      query_string :=
         'SELECT NULL two_series_vendor_name, vendor_name seven_series_vendor_name,
       asp.segment1 seven_series_vendor_no, NULL two_series_vendor_no,
       NULL two_series_vendor_site_id,
       asl.vendor_site_id seven_series_vendor_site_id,
       NULL two_series_vendor_site_code, NULL two_s_purchasing_site_flag,
       NULL two_series_pay_site_flag, asl.attribute1, asl.attribute2,
       asl.attribute3, asl.attribute4, asl.attribute5, asl.attribute6,
       asl.attribute7, asl.attribute8, asl.attribute9, asl.attribute10,
       asl.attribute11, asl.attribute12, asl.attribute13, asl.attribute14,
       asl.org_id, hr.NAME operating_name, apt.term_id terms_id,apt.name terms_name,asl.PAY_GROUP_LOOKUP_CODE, ''N'' status,
       asl.vendor_site_code,asp.END_DATE_ACTIVE,asl.INACTIVE_DATE
  FROM apps.ap_suppliers asp,
       apps.ap_supplier_sites_all asl,
       hr_operating_units hr,
       ap_terms apt
 WHERE asp.vendor_id = asl.vendor_id
   AND hr.organization_id = asl.org_id
   AND apt.term_id = asl.terms_id AND ';

      BEGIN
         EXECUTE IMMEDIATE ' truncate table xxabrl.xxabrl_update_host_info ';
      END;

      fnd_file.put_line (fnd_file.LOG, 'Host to host import Program started');
      v_req_id :=
         fnd_request.submit_request
            ('XXABRL',
             'HOST_TO_HOST_SQL_LOADER',
             '',
             '',
             FALSE,
             '/ebiz/shared_u01/abrlftp/ftpdir/interface/Navision/AP/Host-host-import-file.csv'
            -- change the path
            );
      COMMIT;

      IF (v_req_id = 0)
      THEN
         RAISE le_load;
      ELSE
         fnd_file.put_line
                         (fnd_file.LOG,
                             'Host to host import Program  with request id :'
                          || TO_CHAR (v_req_id)
                         );
      END IF;

        -- +===================================================================+
-- Waiting for the conccurrent request to get completed
-- +===================================================================+
      lb_wait :=
         fnd_concurrent.wait_for_request (request_id      => v_req_id,
                                          INTERVAL        => 10,
                                          max_wait        => 0,
                                          phase           => lc_phase,
                                          status          => lc_status,
                                          dev_phase       => lc_dev_phase,
                                          dev_status      => lc_dev_status,
                                          MESSAGE         => lc_message
                                         );
      -- Do not remove this commit, concurrent program will not start without this
      COMMIT;

         -- +===================================================================+
-- Checking   h2h import Program status
-- +===================================================================+
      IF    (    (   UPPER (lc_dev_status) = 'WARNING'
                  OR UPPER (lc_dev_status) = 'ERROR'
                 )
             AND UPPER (lc_dev_phase) = 'COMPLETE'
            )
         OR (    (UPPER (lc_status) = 'WARNING' OR UPPER (lc_status) = 'ERROR'
                 )
             AND UPPER (lc_phase) = 'COMPLETED'
            )
      THEN
         RAISE le_load;
      ELSE
         BEGIN
            SELECT COUNT (seven_series_vendor_no), COUNT (attribute14),
                   COUNT (attribute1)
              INTO xx_segment1, xx_retak_no,
                   xx_navi_no
              FROM xxabrl.xxabrl_update_host_info s;

            IF xx_segment1 > 0
            THEN
               fnd_file.put_line (fnd_file.LOG,
                                  'xx_segment1::' || xx_segment1
                                 );
               query1 :=
                  'asp.segment1 in(select seven_series_vendor_no from xxabrl.xxabrl_update_host_info)';
            END IF;

            IF xx_retak_no > 0
            THEN
               fnd_file.put_line (fnd_file.LOG,
                                  'xx_retak_no::' || xx_retak_no
                                 );
               query1 :=
                  'asp.segment1 in(select attribute14 from xxabrl.xxabrl_update_host_info )';
            END IF;

            IF xx_navi_no > 0
            THEN
               fnd_file.put_line (fnd_file.LOG, 'xx_navi_no::' || xx_navi_no);
               query1 :=
                  'asp.segment1 in(select attribute1 from xxabrl.xxabrl_update_host_info )';
            END IF;

            fnd_file.put_line (fnd_file.LOG, 'query1::' || query1);
            query_string := query_string || ' ' || query1;
            fnd_file.put_line (fnd_file.LOG, 'query_string::' || query_string);

            OPEN xx_main FOR query_string;

--            v_file :=
--               UTL_FILE.fopen ('/usr/tmp',
--                                  'Host-host-import-file'
--                               || g_conc_request_id
--                               || '.xls',
--                               'w',
--                               vmaxlinesize
--                              );
           -- UTL_FILE.put_line (v_file,
            fnd_file.put_line (fnd_file.output,
                                  'TWO_SERIES_VENDOR_NAME'
                               || '^'
                               || 'SEVEN_SERIES_VENDOR_NAME'
                               || '^'
                               || 'SEVEN_SERIES_VENDOR_NO'
                               || '^'
                               || 'TWO_SERIES_VENDOR_NO'
                               || '^'
                               || 'TWO_SERIES_VENDOR_SITE_ID'
                               || '^'
                               || 'SEVEN_SERIES_VENDOR_SITE_ID'
                               || '^'
                               || 'TWO_SERIES_VENDOR_SITE_CODE'
                               || '^'
                               || 'TWO_S_PURCHASING_SITE_FLAG'
                               || '^'
                               || 'TWO_SERIES_PAY_SITE_FLAG'
                               || '^'
                               || 'ATTRIBUTE1'
                               || '^'
                               || 'ATTRIBUTE2'
                               || '^'
                               || 'ATTRIBUTE3'
                               || '^'
                               || 'ATTRIBUTE4'
                               || '^'
                               || 'ATTRIBUTE5'
                               || '^'
                               || 'ATTRIBUTE6'
                               || '^'
                               || 'ATTRIBUTE7'
                               || '^'
                               || 'ATTRIBUTE8'
                               || '^'
                               || 'ATTRIBUTE9'
                               || '^'
                               || 'ATTRIBUTE10'
                               || '^'
                               || 'ATTRIBUTE11'
                               || '^'
                               || 'ATTRIBUTE12'
                               || '^'
                               || 'ATTRIBUTE13'
                               || '^'
                               || 'ATTRIBUTE14'
                               || '^'
                               || 'ORG_ID'
                               || '^'
                               || 'OPERATING_NAME'
                               || '^'
                               || 'TERMS_ID'
                               || '^'
                               || 'TERMS_NAME'
                               || '^'
                               || 'PAY_GROUP_LOOKUP_CODE'
                               || '^'
                               || 'STATUS'
                               || '^'
                               || 'VENDOR_SITE_CODE'
                               || '^'
                               || 'HEADER_INACTIVE_DATE'
                               || '^'
                               || 'SITES_INACTIVE_DATE'
                              );

            LOOP
               FETCH xx_main
                INTO xx_rec;

               EXIT WHEN xx_main%NOTFOUND;
               -- UTL_FILE.put_line (v_file,
               fnd_file.put_line (fnd_file.output,
                                     xx_rec.two_series_vendor_name
                                  || '^'
                                  || xx_rec.seven_series_vendor_name
                                  || '^'
                                  || xx_rec.seven_series_vendor_no
                                  || '^'
                                  || xx_rec.two_series_vendor_no
                                  || '^'
                                  || xx_rec.two_series_vendor_site_id
                                  || '^'
                                  || xx_rec.seven_series_vendor_site_id
                                  || '^'
                                  || xx_rec.two_series_vendor_site_code
                                  || '^'
                                  || xx_rec.two_s_purchasing_site_flag
                                  || '^'
                                  || xx_rec.two_series_pay_site_flag
                                  || '^'
                                  || xx_rec.attribute1
                                  || '^'
                                  || xx_rec.attribute2
                                  || '^'
                                  || xx_rec.attribute3
                                  || '^'
                                  || xx_rec.attribute4
                                  || '^'
                                  || xx_rec.attribute5
                                  || '^'
                                  || xx_rec.attribute6
                                  || '^'
                                  || xx_rec.attribute7
                                  || '^'
                                  || xx_rec.attribute8
                                  || '^'
                                  || xx_rec.attribute9
                                  || '^'
                                  || xx_rec.attribute10
                                  || '^'
                                  || xx_rec.attribute11
                                  || '^'
                                  || xx_rec.attribute12
                                  || '^'
                                  || xx_rec.attribute13
                                  || '^'
                                  || xx_rec.attribute14
                                  || '^'
                                  || xx_rec.org_id
                                  || '^'
                                  || xx_rec.operating_name
                                  || '^'
                                  || xx_rec.terms_id
                                  || '^'
                                  || xx_rec.terms_name
                                  || '^'
                                  || xx_rec.pay_group_lookup_code
                                  || '^'
                                  || xx_rec.status
                                  || '^'
                                  || xx_rec.vendor_site_code
                                  || '^'
                                  || xx_rec.header_end_active
                                  || '^'
                                  || xx_rec.site_end_active
                                 );
            END LOOP;

            CLOSE xx_main;

            EXECUTE IMMEDIATE ' truncate table xxabrl.xxabrl_update_host_info ';

            fnd_file.put_line (fnd_file.LOG, 'succesfully imported:');
            fnd_file.put_line (fnd_file.LOG, 'Path:: /usr/tmp');
            UTL_FILE.fclose (v_file);
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               UTL_FILE.fclose (v_file);
               fnd_file.put_line (fnd_file.LOG, 'No Data Found');
            WHEN UTL_FILE.invalid_path
            THEN
               UTL_FILE.fclose (v_file);
               fnd_file.put_line (fnd_file.LOG, 'Invalid Path');
               x_ret_code := gn_error;
            WHEN UTL_FILE.invalid_filehandle
            THEN
               UTL_FILE.fclose (v_file);
               fnd_file.put_line (fnd_file.LOG, 'Invalid File Handle');
            WHEN UTL_FILE.invalid_operation
            THEN
               UTL_FILE.fclose (v_file);
               fnd_file.put_line (fnd_file.LOG, 'Invalid Operation');
            WHEN UTL_FILE.read_error
            THEN
               UTL_FILE.fclose (v_file);
               fnd_file.put_line (fnd_file.LOG, 'Read Error');
            WHEN UTL_FILE.write_error
            THEN
               UTL_FILE.fclose (v_file);
               fnd_file.put_line (fnd_file.LOG, 'Write Error');
            WHEN UTL_FILE.internal_error
            THEN
               UTL_FILE.fclose (v_file);
               fnd_file.put_line (fnd_file.LOG, 'Internal Error');
            WHEN UTL_FILE.file_open
            THEN
               UTL_FILE.fclose (v_file);
               fnd_file.put_line (fnd_file.LOG, 'File is Open');
            WHEN UTL_FILE.invalid_filename
            THEN
               UTL_FILE.fclose (v_file);
               fnd_file.put_line (fnd_file.LOG, 'Invalid File Name');
            WHEN OTHERS
            THEN
               x_ret_code := gn_error;
               fnd_file.put_line
                                (fnd_file.LOG,
                                    'ERROR IN HOST TO HOST EXPORT PROGRAM :-'
                                 || SQLERRM
                                 || ':'
                                 || SQLCODE
                                );
         END;
      END IF;
   EXCEPTION
      WHEN le_load
      THEN
         x_ret_code := gn_error;
         x_err_buf := 'Import Items Program completed with error';
      WHEN OTHERS
      THEN
         fnd_file.put_line (fnd_file.LOG,
                            'error code 2: ' || SQLCODE || ':' || SQLERRM
                           );
         ROLLBACK;
   END xxabrl_host_host_import;

   PROCEDURE xxabrl_update_host_host (
      x_err_buf    OUT      VARCHAR2,
      x_ret_code   OUT      NUMBER,
      v_flag       IN       VARCHAR2
   )
   IS
      lv_return_status     VARCHAR2 (1);
      lv_msg_count         NUMBER;
      lv_msg_data          VARCHAR2 (2000);
      xx_pay_ccid          NUMBER;
      xx_pre_ccid          NUMBER;
      lv_vendor_site_id    ap_supplier_sites.vendor_site_id%TYPE;
      lv_vendor_site_rec   ap_vendor_pub_pkg.r_vendor_site_rec_type;

      CURSOR xx_rec
      IS
         SELECT ROWID, s.*
           FROM xxabrl.xxabrl_update_host_info s
          WHERE seven_series_vendor_no IS NOT NULL AND status = 'N';
   BEGIN
      fnd_global.apps_initialize (0, 52299, 200);
      mo_global.init ('S');
      COMMIT;

      IF NVL (v_flag, 'Z') = 'Z'
      THEN
         FOR m_rec IN xx_rec
         LOOP
            UPDATE apps.ap_supplier_sites_all
               SET attribute1 = NVL (m_rec.attribute1, attribute1),
                   attribute2 = NVL (m_rec.attribute2, attribute2),
                   attribute3 = NVL (m_rec.attribute3, attribute3),
                   attribute4 = NVL (m_rec.attribute4, attribute4),
                   attribute5 = NVL (m_rec.attribute5, attribute5),
                   attribute6 = NVL (m_rec.attribute6, attribute6),
                   attribute7 = NVL (m_rec.attribute7, attribute7),
                   attribute8 = NVL (m_rec.attribute8, attribute8),
                   attribute9 = NVL (m_rec.attribute9, attribute9),
                   attribute10 = NVL (m_rec.attribute10, attribute10),
                   attribute11 = NVL (m_rec.attribute11, attribute11),
                   attribute12 = NVL (m_rec.attribute12, attribute12),
                   attribute13 = NVL (m_rec.attribute13, attribute13),
                   attribute14 = NVL (m_rec.attribute14, attribute14),
                   terms_id = NVL (m_rec.terms_id, terms_id),
                   pay_group_lookup_code =
                      NVL (m_rec.pay_group_lookup_code, pay_group_lookup_code),
                   attribute_category = 'Supplier Site Additional Info',
                   last_update_date = SYSDATE
             WHERE vendor_site_id = m_rec.seven_series_vendor_site_id;

            UPDATE xxabrl_update_host_info
               SET status = 'Y'
             WHERE ROWID = m_rec.ROWID;

            COMMIT;
         END LOOP;
      ELSIF v_flag = 'Y'
      THEN
         FOR m_rec IN xx_rec
         LOOP
            lv_vendor_site_id := m_rec.seven_series_vendor_site_id;
            lv_vendor_site_rec.inactive_date := SYSDATE;
            ap_vendor_pub_pkg.update_vendor_site
                           (p_api_version           => 1.0,
                            p_init_msg_list         => fnd_api.g_true,
                            p_commit                => fnd_api.g_false,
                            p_validation_level      => fnd_api.g_valid_level_full,
                            x_return_status         => lv_return_status,
                            x_msg_count             => lv_msg_count,
                            x_msg_data              => lv_msg_data,
                            p_vendor_site_rec       => lv_vendor_site_rec,
                            p_vendor_site_id        => lv_vendor_site_id
                           );
            COMMIT;

--
            IF (lv_return_status <> 'S')
            THEN
               IF lv_msg_count > 1
               THEN
                  FOR i IN 1 .. lv_msg_count
                  LOOP
                     fnd_file.put_line
                        (fnd_file.LOG,
                         SUBSTR
                               (fnd_msg_pub.get (p_encoded      => fnd_api.g_false),
                                1,
                                255
                               )
                        );
                  END LOOP;
               END IF;
            ELSE
               fnd_file.put_line (fnd_file.LOG, 'Supplier Site updated!!!');
            END IF;
         END LOOP;
      ELSIF v_flag = 'N'
      THEN
         FOR m_rec IN xx_rec
         LOOP
            lv_vendor_site_id := m_rec.seven_series_vendor_site_id;
            lv_vendor_site_rec.inactive_date := fnd_api.g_null_date;
            ap_vendor_pub_pkg.update_vendor_site
                           (p_api_version           => 1.0,
                            p_init_msg_list         => fnd_api.g_true,
                            p_commit                => fnd_api.g_false,
                            p_validation_level      => fnd_api.g_valid_level_full,
                            x_return_status         => lv_return_status,
                            x_msg_count             => lv_msg_count,
                            x_msg_data              => lv_msg_data,
                            p_vendor_site_rec       => lv_vendor_site_rec,
                            p_vendor_site_id        => lv_vendor_site_id
                           );
            COMMIT;

--
            IF (lv_return_status <> 'S')
            THEN
               IF lv_msg_count > 1
               THEN
                  FOR i IN 1 .. lv_msg_count
                  LOOP
                     fnd_file.put_line
                        (fnd_file.LOG,
                         SUBSTR
                               (fnd_msg_pub.get (p_encoded      => fnd_api.g_false),
                                1,
                                255
                               )
                        );
                  END LOOP;
               END IF;
            ELSE
               fnd_file.put_line (fnd_file.LOG, 'Supplier Site updated!!!');
            END IF;
         END LOOP;
      END IF;

      fnd_file.put_line (fnd_file.LOG, 'succesfully imported:');
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         fnd_file.put_line (fnd_file.LOG,
                            'error' || SQLCODE || ':' || SQLERRM
                           );
   END;
END; 
/


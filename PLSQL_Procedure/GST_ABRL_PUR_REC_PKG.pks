CREATE OR REPLACE PACKAGE APPS.GST_ABRL_pur_rec_pkg
AS
   PROCEDURE GST_ABRL_main_pkg (
      errbuff          OUT      VARCHAR2,
      retcode          OUT      NUMBER,
      p_type           IN       VARCHAR2,
      p_gl_from_date   IN       VARCHAR2,
      p_gl_to_date     IN       VARCHAR2,
      p_gl_acct_from   IN       VARCHAR2,
      p_gl_acct_to     IN       VARCHAR2
   );

   PROCEDURE xxabrl_financial_proc (
      p_gl_from_date   IN   VARCHAR2,
      p_gl_to_date     IN   VARCHAR2,
      p_gl_acct_from   IN   VARCHAR2,
      p_gl_acct_to     IN   VARCHAR2
   );

   PROCEDURE xxabrl_costmanagement_proc (
      p_gl_from_date   IN   VARCHAR2,
      p_gl_to_date     IN   VARCHAR2,
      p_gl_acct_from   IN   VARCHAR2,
      p_gl_acct_to     IN   VARCHAR2
   );
END GST_ABRL_pur_rec_pkg; 
/


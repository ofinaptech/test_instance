CREATE OR REPLACE PACKAGE APPS.xxabrl_acct_ap_bkdt_load_pkg
IS
   PROCEDURE xxabrl_wait_for_req_prc2 (p_request_id NUMBER, p_req_name VARCHAR2);

   PROCEDURE xxabrl_acct_ap_dt_load_prc (
      errbuf    OUT   VARCHAR2,
      retcode   OUT   NUMBER
   );
   
END xxabrl_acct_ap_bkdt_load_pkg; 
/


CREATE OR REPLACE Package Body APPS.XXABRL_RESA_AR_INV_IMP_PKG IS
  Procedure INVOICE_VALIDATE(errbuf         OUT VARCHAR2,
                             retcode        OUT NUMBER,
                             /*P_Org_Id       In NUMBER,*/
                             P_BATCH_Source IN VARCHAR2,
                             p_action  in varchar2,
                              P_Org_Id       In NUMBER
                             ) is
    --
    -- Declaring cursor 1
    --
    CURSOR ART_C1_Validate(CP_ORG_CODE VARCHAR2, CP_Batch_Source VARCHAR2) IS    
      Select BATCH_SOURCE_NAME,
             ORG_CODE,
             INTERFACE_LINE_CONTEXT,
             INTERFACE_LINE_ATTRIBUTE1 INTERFACE_LINE_ATTRIBUTE1,
             NV_CUSTOMER_ID,
             NV_CUST_SITE_ID
        from XXABRL_RESA_RA_INT_LINES_ALL
       Where /*NVL(BATCH_SOURCE_NAME, CP_Batch_Source) = CP_Batch_Source 
               And*/ 
               NVL(ORG_CODE, CP_ORG_CODE) = CP_ORG_CODE
               And
       NVL(INTERFACED_FLAG, 'N') in ('N', 'E')
       Group by BATCH_SOURCE_NAME,
                ORG_CODE,
                INTERFACE_LINE_CONTEXT,
                INTERFACE_LINE_ATTRIBUTE1,
                NV_CUSTOMER_ID,
                NV_CUST_SITE_ID;
    CURSOR ART_C2(CP_BATCH_SOURCE_NAME VARCHAR2, CP_ORG_CODE VARCHAR2, CP_INTERFACE_LINE_ATTRIBUTE1 VARCHAR2, CP_NV_CUSTOMER_NUMBER VARCHAR2, CP_NV_CUST_SITE_ID VARCHAR2) IS
      Select ROWID,
             BATCH_SOURCE_NAME,
             CUSTOMER_NAME,
             ORG_CODE,
             INTERFACE_LINE_CONTEXT,
             INTERFACE_LINE_ATTRIBUTE1 INTERFACE_LINE_ATTRIBUTE1,
             INTERFACE_LINE_ATTRIBUTE2 INTERFACE_LINE_ATTRIBUTE2,
             LINE_TYPE,
             TRX_DATE,
             GL_DATE,
             AMOUNT,
             CURRENCY_CODE,
             CONVERSION_TYPE,
             CONVERSION_RATE,
             CONVERSION_DATE,
             TRANSACTION_TYPE,
             DESCRIPTION,
             TERM_ID,
             NV_CUSTOMER_ID,
             NV_CUST_SITE_ID,
             ATTRIBUTE1,
             ATTRIBUTE_CATEGORY
        from XXABRL_RESA_RA_INT_LINES_ALL
       Where NVL(BATCH_SOURCE_NAME, CP_BATCH_SOURCE_NAME) =
             CP_BATCH_SOURCE_NAME
         And NVL(ORG_CODE, CP_ORG_CODE) = CP_ORG_CODE
         And INTERFACE_LINE_ATTRIBUTE1 = CP_INTERFACE_LINE_ATTRIBUTE1
         /*And NVL(NV_CUSTOMER_ID, CP_NV_CUSTOMER_NUMBER) =
             CP_NV_CUSTOMER_NUMBER
         And NVL(NV_CUST_SITE_ID, CP_NV_CUST_SITE_ID) = CP_NV_CUST_SITE_ID*/
         And NVL(INTERFACED_FLAG, 'N') in ('N', 'E')
       Order by INTERFACE_LINE_ATTRIBUTE1, INTERFACE_LINE_ATTRIBUTE2;
    CURSOR ART_C3(CP_BATCH_SOURCE_NAME VARCHAR2, CP_ORG_CODE VARCHAR2, CP_INTERFACE_LINE_ATTRIBUTE1 VARCHAR2, CP_INTERFACE_LINE_ATTRIBUTE2 VARCHAR2, CP_NV_CUSTOMER_NUMBER VARCHAR2, CP_NV_CUST_SITE_ID VARCHAR2) IS
      Select ROWID,
             --             BATCH_SOURCE_NAME,
             ORG_ID,
             INTERFACE_LINE_CONTEXT,
             INTERFACE_LINE_ATTRIBUTE1 INTERFACE_LINE_ATTRIBUTE1,
             INTERFACE_LINE_ATTRIBUTE2 INTERFACE_LINE_ATTRIBUTE2,
             ACCOUNT_CLASS,
             AMOUNT,
             SEGMENT1 CO,
             SEGMENT2 CC,
             SEGMENT3 STATE_SBU,
             SEGMENT4 LOCATION,
             SEGMENT5 MERCHANDISE,
             SEGMENT6 ACCOUNT,
             SEGMENT7 INTERCOMPANY,
             SEGMENT8 FUTURE,
             RECORD_NUMBER
        From XXABRL_RESA_RA_INT_DIST_ALL
       Where /*NVL(BATCH_SOURCE_NAME, CP_BATCH_SOURCE_NAME) =
                   CP_BATCH_SOURCE_NAME
               And */
       /*NVL(ORG_ID, CP_ORG_CODE) = CP_ORG_CODE
       And */INTERFACE_LINE_ATTRIBUTE1 = CP_INTERFACE_LINE_ATTRIBUTE1
       And INTERFACE_LINE_ATTRIBUTE2 = CP_INTERFACE_LINE_ATTRIBUTE2
      /*         And NVL(NV_CUSTOMER_NUMBER, CP_NV_CUSTOMER_NUMBER) =
                   CP_NV_CUSTOMER_NUMBER
               And NVL(NV_CUST_SITE_ID, CP_NV_CUST_SITE_ID) = CP_NV_CUST_SITE_ID*/
       --And NVL(INTERFACED_FLAG, 'N') in ('N', 'E')
       ;
   -- v_Orgid               Number := Fnd_Profile.VALUE('ORG_ID');
    v_set_of_bks_id       Number := Fnd_Profile.VALUE('GL_SET_OF_BKS_ID');
    V_user_Id             Number := Fnd_profile.VALUE('USER_ID');
    ORG_CODE              hr_operating_units.Short_Code%type;
    V_Batch_Source        RA_BATCH_SOURCES_ALL.Name%Type;
    V_Fun_Curr            Varchar2(10);
    V_Error_Count         Number := 0;
    v_OK_Rec_count        Number := 0;
    V_Error_Hmsg          Varchar2(1000);
    V_Error_Lmsg          Varchar2(1000);
    v_record_count        Number := 0;
    V_Data_Count          Number := 0;
    v_line_count          Number := 0;
    v_currency            fnd_currencies.currency_code%Type;
    v_tax_name            varchar2(50);
    V_CC_id               Number;
    V_State_Sbu           gl_code_combinations.Segment3%type;
    v_Code_Combination_id number;
    V_CUST_TRX_TYPE_ID    Number;
    V_Gl_ID_REV           Number;
    V_GL_ID_REC           Number;
    V_Cust_Account_Id     Number;
    V_cust_site_id        Number;
    --V_TERM_ID      RA_TERMS.Name%Type;           
    V_Term_id     Number;
    V_UOM_Name    Varchar2(20);
    V_Description Varchar2(240);
    V_Seg_Status  Varchar2(100);
   -- P_Org_Id number;
    --P_BATCH_Source varchar2(50);
    v_cust_Acct_site_id number;
    vn_structure_id NUMBER;
    p_gl_id_rev varchar2(300);
    x_gl_rev_acct_id number;
    v_Conc_Segments varchar2(250);
    v_TRX_TYPE varchar2(15);    
    v_ATTRIBUTE_CATEGORY    varchar2(240);
  Begin
    Begin
    --P_Org_Id := Fnd_Profile.VALUE('ORG_ID');
    --P_BATCH_Source := 'ABRL NAVISION TO AR';
    select short_code
        into ORG_CODE
         from hr_operating_units
         where organization_id = P_Org_Id;        
    EXCEPTION
      When no_data_found then
        fnd_file.put_line(fnd_file.output,
                          'Selected Org Id does not exist in Oracle Financials');
        fnd_file.put_line(fnd_file.output,
                          '........................................................................');
      When too_many_rows then
        fnd_file.put_line(fnd_file.output,
                          'Multiple Org Id exist in Oracle Financials');
        fnd_file.put_line(fnd_file.output,
                          '........................................................................');
      When Others THEN
        fnd_file.put_line(fnd_file.output, 'Invalid Org Id Selected ');
        fnd_file.put_line(fnd_file.output,
                          '........................................................................');
    End;
    Update XXABRL_RESA_RA_INT_LINES_ALL
       Set CREATED_BY = v_user_id,
       BATCH_SOURCE_NAME = BATCH_SOURCE_NAME,
       TRANSACTION_TYPE = initcap(TRANSACTION_TYPE);
    Update XXABRL_RESA_RA_INT_LINES_ALL
       Set CREATED_BY = v_user_id
     Where Trim(Upper(BATCH_SOURCE_NAME)) = Trim(Upper(P_BATCH_Source))
       And Trim(Upper(ORG_CODE)) = Trim(Upper(ORG_CODE))
       And NVL(INTERFACED_FLAG, 'N') = 'N'
       And CREATED_BY is Null;
    Update XXABRL_RESA_RA_INT_DIST_ALL
       Set CREATED_BY = v_user_id
     Where /*Trim(Upper(BATCH_SOURCE_NAME)) = Trim(Upper(P_BATCH_Source))
           And */
     Trim(Upper(ORG_CODE)) = Trim(Upper(ORG_CODE))
     And NVL(INTERFACED_FLAG, 'N') = 'N'
     And CREATED_BY is Null;
    Commit;
    --Deleting previous error message if any--
    UPDATE XXABRL_RESA_RA_INT_LINES_ALL
       SET ERROR_MESSAGE = Null
     Where Trim(Upper(BATCH_SOURCE_NAME)) = Trim(Upper(P_BATCH_Source))
       And Trim(Upper(ORG_CODE)) = Trim(Upper(ORG_CODE))
       And NVL(INTERFACED_FLAG, 'N') in ('N', 'E')
       And ERROR_MESSAGE is not null;
    UPDATE XXABRL_RESA_RA_INT_DIST_ALL
       SET ERROR_MESSAGE = Null
     Where /*Trim(Upper(BATCH_SOURCE_NAME)) = Trim(Upper(P_BATCH_Source))
           And */
     Trim(Upper(ORG_CODE)) = Trim(Upper(ORG_CODE))
     And NVL(INTERFACED_FLAG, 'N') in ('N', 'E')
     And ERROR_MESSAGE is not null;
    Commit;
    Begin
      Select Currency_code
        Into V_Fun_Curr
        from GL_SETS_OF_BOOKS
       where set_of_books_id = v_set_of_bks_id;
       fnd_file.put_line(fnd_file.output,'GL SOB_ID :' ||v_set_of_bks_id);
    Exception
      When Others then
        V_Fun_Curr := Null;
        fnd_file.put_line(fnd_file.output,'Exception in Currency Code');        
    End;
    --************************************************************************************            
    --**  Additional Loop to Update Customer Site-id and Tran-ID.                        *             
    --**  After updating base table, updated data will be used in Re-Call of same loop   *
    --************************************************************************************            
/**/    fnd_file.put_line(fnd_file.output,'-------------------------------------------------');                
/**/    fnd_file.put_line(fnd_file.output,'***Running update Script for Cust-Site-ID / Tran-ID');                
/**/    FOR ART_R1 IN ART_C1_Validate(ORG_CODE, P_BATCH_Source) LOOP
/**/      EXIT WHEN ART_C1_Validate%NOTFOUND;
/**/
/**/      FOR ART_R2 IN ART_C2(ART_R1.BATCH_SOURCE_NAME,
/**/                           ART_R1.ORG_CODE,
/**/                           ART_R1.INTERFACE_LINE_ATTRIBUTE1,
/**/                           ART_R1.NV_CUSTOMER_ID,
/**/                           ART_R1.NV_CUST_SITE_ID) LOOP
/**/        EXIT WHEN ART_C2%NOTFOUND;
/**/        
/**/        V_Error_Hmsg      := Null;
/**/        V_Data_Count      := Null;
/**/        V_CC_id           := Null;
/**/        V_Cust_Account_Id := Null;
/**/        V_cust_site_id    := Null;
/**/        V_Term_id         := Null;
/**/        V_Description     := Null;
/**/        
/**/        V_Error_Hmsg :=NULL;
/**/             
/**/        --#######################################
/**/        -- Validating / Updating Customer ID
/**/        --#######################################                        
/**/        if ART_R2.CUSTOMER_NAME is not NULL then
/**/          BEGIN        
/**/            select 
/**/            cust_account_id into v_CUST_ACCOUNT_ID 
/**/            from 
/**/            hz_cust_accounts hca
/**/            /*, hz_parties hp */
/**/            where 
/**/            hca.account_number = ART_R2.CUSTOMER_NAME
/**/            /*upper(hp.party_name) = upper(ART_R2.CUSTOMER_NAME)*/
/**/            /*and hp.party_id = hca.party_id*/
/**/            ;
/**/            
/**/            Begin
/**/            update XXABRL_RESA_RA_INT_LINES_ALL int_ln
/**/            set int_ln.NV_CUSTOMER_ID = v_CUST_ACCOUNT_ID
/**/            where rowid = ART_R2.rowid;
/**/                  BEGIN
/**/                          Select  hcas.cust_Acct_site_id into v_cust_Acct_site_id
/**/                     From hz_cust_accounts_all         hca,
/**/                          hz_cust_acct_sites_all       hcas,
/**/                          hz_party_sites               hps,
/**/                          hz_cust_site_uses_all        hcsu
/**/                    Where hca.cust_account_id = v_CUST_ACCOUNT_ID
/**/                      And hca.cust_account_id = hcas.cust_account_id
/**/                      And hcas.org_id = P_Org_id
/**/                      And hcas.cust_acct_site_id = hcsu.cust_acct_site_id(+)
/**/                      And hcas.party_site_id = hps.party_site_id
/**/        --              And hps.Party_Site_Number = xac.OF_Customer_Site_Code
/**/                      And hcsu.site_use_code = 'BILL_TO'
/**/                      And hcas.bill_to_flag = 'P'
/**/                      And hcas.status = 'A';                
/**/                      
/**/                      BEGIN
/**/                        update XXABRL_RESA_RA_INT_LINES_ALL int_ln
/**/                        set int_ln.NV_CUST_SITE_ID = v_cust_Acct_site_id
/**/                        where rowid = ART_R2.rowid;
/**/
/**/                      EXCEPTION
/**/                      when others then
/**/                      fnd_file.put_line(fnd_file.output,'Exception while updating Customer Site '||ART_R2.CUSTOMER_NAME); 
/**/                      END;
/**/
/**/                      
/**/                  EXCEPTION
/**/                      when NO_DATA_FOUND then
/**/                      V_Error_Hmsg := V_Error_Hmsg || 'Bill_TO Site not Found for '||ART_R2.CUSTOMER_NAME ||' -->>';
/**/                      when too_many_rows then
/**/                      V_Error_Hmsg := V_Error_Hmsg || 'Multiple Bill_TO Site Found for '||ART_R2.CUSTOMER_NAME ||' -->>';
/**/                  End;
/**/            
/**/            
/**/            EXCEPTION
/**/                when others then
/**/                fnd_file.put_line(fnd_file.output,'Exception while updating Customer Id '||ART_R2.CUSTOMER_NAME); 
/**/            END;
/**/
/**/          EXCEPTION
/**/          when NO_DATA_FOUND then
/**/          V_Error_Hmsg := V_Error_Hmsg || 'CUSTOMER '||ART_R2.CUSTOMER_NAME || '  not defined-->>';
/**/          when too_many_rows then
/**/          V_Error_Hmsg := V_Error_Hmsg || 'Multiple CUSTOMER  defined-->>';          
/**/          END;
/**/        
/**/        else
/**/            V_Error_Hmsg := V_Error_Hmsg || 'Customer Name Type is Blank-->>';  
/**/        End if;        
/**/
/**/        --#######################################
/**/        -- Validating / Updating Transaction Type/ID
/**/        --#######################################                
/**/        if ART_R2.TRANSACTION_TYPE is not NULL then
/**/          BEGIN        
/**/            select 
/**/            CUST_TRX_TYPE_ID, TYPE 
/**/            into v_CUST_TRX_TYPE_ID ,v_TRX_TYPE
/**/            from ra_cust_trx_types_all
/**/            where 
/**/            upper(name) = upper(ART_R2.TRANSACTION_TYPE)
/**/            and org_id = P_Org_Id
/**/            ;
/**/            
/**/            Begin
/**/            if v_TRX_TYPE = 'CM' then
/**/              update XXABRL_RESA_RA_INT_LINES_ALL int_ln
/**/              set 
/**/              int_ln.CUST_TRX_TYPE_ID = v_CUST_TRX_TYPE_ID
/**/              ,Term_id_new =NULL
/**/              ,Term_id     =NULL
/**/              where rowid = ART_R2.rowid;
/**/            else
/**/              update XXABRL_RESA_RA_INT_LINES_ALL int_ln
/**/              set int_ln.CUST_TRX_TYPE_ID = v_CUST_TRX_TYPE_ID
/**/              where rowid = ART_R2.rowid;            
/**/            End If;
/**/            
/**/            EXCEPTION
/**/                when others then
/**/                fnd_file.put_line(fnd_file.output,'Exception while updating Transaction Id '||ART_R2.TRANSACTION_TYPE); 
/**/            END;
/**/
/**/          EXCEPTION
/**/          when NO_DATA_FOUND then
/**/               V_Error_Hmsg := V_Error_Hmsg || 'Transaction Type not defined-->>';
/**/          when too_many_rows then
/**/               V_Error_Hmsg := V_Error_Hmsg || 'Multiple Transaction Type defined-->>';
/**/          when Others then
/**/               V_Error_Hmsg := V_Error_Hmsg || 'Exception in Transaction Type Validation-->>';                    
/**/          END;
/**/        
/**/        else
/**/            V_Error_Hmsg := V_Error_Hmsg || 'Transaction Type is Blank-->>';  
/**/        End if;
/**/        END LOOP;
/**/    END LOOP;  
    --************************************************************************************            
    --**  Additional Loop to Update Customer Site-id and Tran-ID.                        *             
    --**  After updating base table, updated data will be used in Re-Call of same loop   *
    --************************************************************************************            
    --************************************************************************************Ends....                
    fnd_file.put_line(fnd_file.output,
                      '          ### AR Transaction Information Validating Log ###');
    fnd_file.put_line(fnd_file.output,
                      '........................................................................');
    FOR ART_R1 IN ART_C1_Validate(ORG_CODE, P_BATCH_Source) LOOP
      EXIT WHEN ART_C1_Validate%NOTFOUND;
      v_record_count := ART_C1_Validate%ROWCOUNT;
      fnd_file.put_line(fnd_file.output,'Batch:'||P_BATCH_Source||' / Org_Code: '||ORG_CODE);      
      FOR ART_R2 IN ART_C2(ART_R1.BATCH_SOURCE_NAME,
                           ART_R1.ORG_CODE,
                           ART_R1.INTERFACE_LINE_ATTRIBUTE1,
                           ART_R1.NV_CUSTOMER_ID,
                           ART_R1.NV_CUST_SITE_ID) LOOP
        EXIT WHEN ART_C2%NOTFOUND;
        V_Error_Hmsg      := Null;
        V_Data_Count      := Null;
        V_CC_id           := Null;
        V_Cust_Account_Id := Null;
        V_cust_site_id    := Null;
        V_Term_id         := Null;
        V_Description     := Null;
        fnd_file.put_line(fnd_file.output,'----------------------');                
        fnd_file.put_line(fnd_file.output,'Line# '||ART_R2.INTERFACE_LINE_ATTRIBUTE1||'('||ART_R2.INTERFACE_LINE_ATTRIBUTE2||')');                
        V_Error_Hmsg :=NULL;
        --#######################################
        -- Validating Bacth Source
        --#######################################        
        If ART_R2.BATCH_SOURCE_NAME is Null Then
          V_Error_Hmsg := V_Error_Hmsg || 'Batch Source is Null-->>';
        Else
          V_Batch_Source := Null;
          BEGIN
            Select Name
              Into V_Batch_Source
              from RA_BATCH_SOURCES_ALL
             where Name = ART_R2.BATCH_SOURCE_NAME
               And Org_id = P_Org_ID
               And Status = 'A'
               And Batch_Source_Type = 'FOREIGN'
               And Sysdate between Nvl(Start_Date, Sysdate) and
                   Nvl(End_Date, sysdate);
          EXCEPTION
            When no_data_found then
              V_Error_Hmsg := V_Error_Hmsg ||
                              'Batch Source does not exist in Oracle Financials-->>';
            When too_many_rows then
              V_Batch_Source := Null;
              V_Error_Hmsg   := V_Error_Hmsg ||
                                'Multiple Batch Source found-->>';
            When Others THEN
              V_Error_Hmsg := V_Error_Hmsg || 'Wrong Batch Source Code-->>';
          END;
        End If;
        --#######################################
        -- Validating ORG Code
        --#######################################        
        If ART_R2.ORG_CODE is Null Then
          V_Error_Hmsg := V_Error_Hmsg || 'Operating Unit is Null-->>';
        End If;
        --#######################################
        -- Validating LINE Type
        --#######################################                        
        If ART_R2.LINE_TYPE is Null Then
          V_Error_Hmsg := V_Error_Hmsg || 'Line Type is Null-->>';
        Elsif Upper(ART_R2.LINE_TYPE) <> 'LINE' Then
          V_Error_Hmsg := V_Error_Hmsg || 'Invalid Line Type-->>';
        End If;
        --#######################################
        -- Validating TRX Date / GL Period
        --#######################################        
        If ART_R2.TRX_DATE is Null then
          V_Error_Hmsg := V_Error_Hmsg || 'Transaction Date is null-->>';
        End If;
        If ART_R2.GL_DATE is null then
          V_Error_Hmsg := V_Error_Hmsg || 'GL Date is null-->>';
        Else
          BEGIN
            V_Data_Count := Null;
            Select Count(gps.Period_Name)
              Into V_Data_Count
              From gl_period_statuses gps, fnd_application fna
             Where fna.application_short_name = 'AR'
               And fna.application_id = gps.application_id
               And gps.closing_status = 'O'
               And gps.set_of_books_id = v_set_of_bks_id
               And ART_R2.GL_DATE between gps.start_date and gps.end_date;
            If V_Data_Count = 0 then
              V_Error_Hmsg := V_Error_Hmsg ||
                              'GL date is not in AR Open Period -->>';
            End If;
          EXCEPTION
            WHEN OTHERS then
              V_Error_Hmsg := V_Error_Hmsg ||
                              'GL Period not open in AR -->>';
          END;
        End If;
        --#######################################
        -- Validating Invoice Amount
        --#######################################        
        If ART_R2.AMOUNT is Null then
          V_Error_Hmsg := V_Error_Hmsg || 'Invoice Amount is null-->>';
        End If;
        --#######################################
        -- Validating Currency Code
        --#######################################        
        If ART_R2.Currency_Code is null then
          v_error_Hmsg := v_error_Hmsg || 'Currency Code is null -->';
        else
          BEGIN
            SELECT currency_code
              Into v_currency
              FROM fnd_currencies
             WHERE upper(currency_code) = Trim(upper(ART_R2.Currency_Code));
            If upper(trim(v_currency)) <> upper(trim(V_Fun_Curr)) and
               ART_R2.CONVERSION_DATE is null Then
              v_error_hmsg := v_error_hmsg || 'Exchange Date is Null -->>';
            End if;
          EXCEPTION
            When Others Then
              v_error_hmsg := v_error_hmsg || 'Invalid Currency -->>';
          END;
        End If;
        --#######################################
        -- Validating Transaction Type
        --#######################################        
        If ART_R2.TRANSACTION_TYPE is Null Then
          v_error_hmsg := v_error_hmsg || 'Transaction Type Is Null -->>';
        Else
          BEGIN
            V_CUST_TRX_TYPE_ID := null;
            Select CUST_TRX_TYPE_ID
              Into V_CUST_TRX_TYPE_ID
              From RA_CUST_TRX_TYPES_ALL
             Where upper(Name) = upper(Trim(ART_R2.TRANSACTION_TYPE))
               And org_id = P_Org_Id;
            If V_CUST_TRX_TYPE_ID is Null Then
              v_error_hmsg := v_error_hmsg ||
                              'Invalid TRANSACTION TYPE  -->>';
            End If;
          EXCEPTION
            When no_data_found then
              V_Error_Hmsg := V_Error_Hmsg ||
                              'Transaction Type does not exist in Oracle Financials-->>';
            When too_many_rows then
              V_Error_Hmsg := V_Error_Hmsg ||
                              'Multiple Transaction Type found-->>';
            When Others THEN
              v_error_hmsg := v_error_hmsg ||
                              'Invalid Transaction Type  -->>';
          End;
        End If;
        --#######################################
        -- Validating Payment Term
        --#######################################                       
        IF ART_R2.TERM_ID IS Not NULL THEN
          v_term_id := null;
          BEGIN
          IF upper(ART_R2.TERM_ID) = 'CREDIT NOTE' then
             v_term_id:=NULL;
          ELSE   
            Select Term_id
              Into v_term_id
              From RA_TERMS
             Where Upper(Name) = upper(trim(ART_R2.TERM_ID))
               And sysdate between nvl(start_date_active, sysdate) and
                   nvl(end_date_active, sysdate);
          END IF;         
          EXCEPTION
            when no_data_found then
              v_error_hmsg := v_error_hmsg ||
                              'Payment Terms does not exist in Oracle Financials-->>';
            when too_many_rows then
              v_term_id    := null;
              v_error_hmsg := v_error_hmsg ||
                              'Multiple Payment Terms found-->>';
            When Others Then
              v_term_id    := null;
              v_error_hmsg := v_error_hmsg || 'Invalid Payment Term -->>';
          END;
        END IF;
        --###########################################
        -- Validating Customer ID / Customer Site ID
        --###########################################        
        If ART_R2.NV_CUSTOMER_ID Is Null Then
          v_error_hmsg := v_error_hmsg ||
                          'E-Retail Customer / Store Code Is Null -->>';
        End If;
        If ART_R2.NV_CUST_SITE_ID Is Null Then
          v_error_hmsg := v_error_hmsg || 'E-Retail DC Code Is Null -->>';
        End If;
        If ART_R2.NV_CUSTOMER_ID Is Not Null and
           ART_R2.NV_CUST_SITE_ID Is Not Null Then
          Begin
            V_Cust_Account_Id := ART_R2.NV_CUSTOMER_ID;
            V_cust_site_id    := ART_R2.NV_CUST_SITE_ID;
            /*Select Unique hca.Cust_Account_Id, hcas.cust_Acct_site_id
             Into V_Cust_Account_Id, V_cust_site_id
             From hz_cust_accounts_all         hca,
                  hz_cust_acct_sites_all       hcas,
                  hz_party_sites               hps,
                  hz_cust_site_uses_all        hcsu,
                  xxabrl_ar_cust_bkacc_map_int xac
            Where NV_CUST_SITE_ID = ART_R2.NV_CUST_SITE_ID
              And NV_CUSTOMER_NUMBER = ART_R2.NV_CUSTOMER_NUMBER
              And hca.Account_Number = xac.OF_Customer_Number
              And hca.cust_account_id = hcas.cust_account_id
              And hcas.org_id = P_Org_id
              And hcas.cust_acct_site_id = hcsu.cust_acct_site_id(+)
              And hcas.party_site_id = hps.party_site_id
              And hps.Party_Site_Number = xac.OF_Customer_Site_Code
              And hcsu.site_use_code = 'BILL_TO'
              And hcas.bill_to_flag = 'P'
              And hcas.status = 'A';*/
          Exception
            When no_data_found then
              v_error_hmsg := v_error_hmsg ||
                              'Customer Number Or Site in not exist in this Org -->>';
            When too_many_rows then
              v_error_hmsg := v_error_hmsg ||
                              'Multiple Customer Number Or Site found in this Org -->>';
            When Others Then
              v_error_hmsg := v_error_hmsg ||
                              'Customer Number Or Site in not exist in this Org -->>';
          End;
        End If;
        --#######################################
        -- Validating Tax Code - Context Value
        --#######################################   
        v_ATTRIBUTE_CATEGORY :=NULL;     
        if ART_R2.ATTRIBUTE_CATEGORY is not null then
          Begin
            SELECT DESCRIPTIVE_FLEX_CONTEXT_CODE
              into v_ATTRIBUTE_CATEGORY
              FROM FND_DESCR_FLEX_CONTEXTS_VL
             WHERE upper(DESCRIPTIVE_FLEX_CONTEXT_CODE) =
                   upper(ART_R2.ATTRIBUTE_CATEGORY)
               and (DESCRIPTIVE_FLEXFIELD_NAME LIKE 'RA_CUSTOMER_TRX_LINES')
               and enabled_flag = 'Y'
               ;
               Begin
                   Update XXABRL_RESA_RA_INT_LINES_ALL
                   set ATTRIBUTE_CATEGORY   = v_ATTRIBUTE_CATEGORY
                   where rowid = ART_R2.rowid;
               Exception
               when others then
                   v_error_hmsg := v_error_hmsg ||
                      'Exception while updating derived Tax ATTRIBUTE_CATEGORY for:'||Trim(upper(ART_R2.ATTRIBUTE_CATEGORY))||' -->>';        
               End;    
          Exception
          when no_data_found then
                v_error_hmsg := v_error_hmsg ||
                                'Tax ATTRIBUTE_CATEGORY does not exists for:'||Trim(upper(ART_R2.ATTRIBUTE_CATEGORY))||' -->>';        
          when too_many_rows then
                v_error_hmsg := v_error_hmsg ||
                                'Too many rows defined for Tax ATTRIBUTE_CATEGORY:'||Trim(upper(ART_R2.ATTRIBUTE_CATEGORY))||' -->>';        
          when others then
                v_error_hmsg := v_error_hmsg ||
                                'Exception while deriving Tax ATTRIBUTE_CATEGORY:'||Trim(upper(ART_R2.ATTRIBUTE_CATEGORY))||' -->>'||SQLERRM;        
          End;
        End if;     
        --#######################################
        -- Validating Tax Code
        --#######################################        
        IF ART_R2.ATTRIBUTE1 Is Not Null then
          BEGIN
            SELECT Tax_name
              Into v_tax_name
              FROM JAI_CMN_TAXES_ALL
             WHERE upper(Tax_Name) = Trim(upper(ART_R2.ATTRIBUTE1))
               AND org_id = P_org_id
               AND Sysdate Between nvl(Start_date, sysdate) and
                   nvl(End_Date, sysdate);
          EXCEPTION
            When no_data_found then
              v_error_hmsg := v_error_hmsg ||
                              'Tax Code does not exists for:'||Trim(upper(ART_R2.ATTRIBUTE1))||' -->>';
            When too_many_rows then
              v_error_hmsg := v_error_hmsg ||
                              'Tax Code does not exists for:'||Trim(upper(ART_R2.ATTRIBUTE1))||' -->>';
            When Others Then
              v_error_hmsg := v_error_hmsg ||
                              'Exception for Tax Code :'||Trim(upper(ART_R2.ATTRIBUTE1))||' -->>';
          END;
        End If;
        --#######################################
        -- Validating Distribution Segments
        --#######################################        
--        Fnd_file.PUT_LINE(fnd_file.Output,'---Validation for Segments--');
--        Fnd_file.PUT_LINE(fnd_file.Output,'ART_R2.INTERFACE_LINE_ATTRIBUTE1 '||ART_R2.INTERFACE_LINE_ATTRIBUTE1);
--        Fnd_file.PUT_LINE(fnd_file.Output,'ART_R2.INTERFACE_LINE_ATTRIBUTE2 '||ART_R2.INTERFACE_LINE_ATTRIBUTE2);
--        Fnd_file.PUT_LINE(fnd_file.Output,'ART_R2.NV_CUSTOMER_ID '||ART_R2.NV_CUSTOMER_ID);
--        Fnd_file.PUT_LINE(fnd_file.Output,'ART_R2.NV_CUST_SITE_ID '||ART_R2.NV_CUST_SITE_ID);
        FOR ART_R3 IN ART_C3(ART_R2.BATCH_SOURCE_NAME,
                             ART_R2.ORG_CODE,
                             ART_R2.INTERFACE_LINE_ATTRIBUTE1,
                             ART_R2.INTERFACE_LINE_ATTRIBUTE2,
                             ART_R2.NV_CUSTOMER_ID,
                             ART_R2.NV_CUST_SITE_ID) LOOP
          EXIT WHEN ART_C3%NOTFOUND;
          V_Error_Lmsg          := Null;
          V_Code_Combination_id := Null;
          If ART_R3.AMOUNT is Null then
            V_Error_Lmsg := 'Invoice Account Line Amount is null-->>';
          Elsif ART_R3.AMOUNT <> ART_R2.AMOUNT Then
            V_Error_Lmsg := 'Invoice Line Amount is not equal to Account Line Amount-->>';
          End If;
          If ART_R3.CO is Not Null then
            --  V_Error_Lmsg:=V_Error_Lmsg||'Accounting Segment Company Value is Null-->>';     
            --Else
            V_Seg_Status := Null;
            V_Seg_Status := ACCOUNT_SEG_STATUS(ART_R3.CO, 'ABRL_GL_CO');
            If V_Seg_Status is Not Null then
              V_Error_Lmsg := V_Error_Lmsg || 'Invalid Company Segment ' ||
                              ART_R3.CO || '-->>';
            End If;
          End If;
          -- Validation for CC Seg
          If ART_R3.CC is Not Null then
            --  V_Error_Lmsg:=V_Error_Lmsg||'Accounting Segment Cost Center Value is Null-->>';             
            --Else
            V_Seg_Status := Null;
            V_Seg_Status := ACCOUNT_SEG_STATUS(ART_R3.CC, 'ABRL_GL_CC');
            If V_Seg_Status is Not Null then
              V_Error_Lmsg := V_Error_Lmsg ||
                              'Invalid Cost Center Segment ' || ART_R3.CC ||
                              '-->>';
            End If;
          End If;
          -- Validation for Location Seg
          If ART_R3.Location is Not Null then
            -- V_Error_Lmsg:=V_Error_Lmsg||'Accounting Segment Location Value is Null-->>';    
            --Else
            V_Seg_Status := Null;
            V_Seg_Status := ACCOUNT_SEG_STATUS(ART_R3.Location,
                                               'ABRL_GL_Location');
            If V_Seg_Status is Not Null then
              V_Error_Lmsg := V_Error_Lmsg || 'Invalid Location Segment ' ||
                              ART_R3.Location || '-->>';
            End If;
          End If;
          -- Validation for Merchandise Seg
          If ART_R3.Merchandise is Not Null then
            --  V_Error_Lmsg:=V_Error_Lmsg||'Accounting Segment Merchandise Value is Null-->>';              
            --Else
            V_Seg_Status := Null;
            V_Seg_Status := ACCOUNT_SEG_STATUS(ART_R3.Merchandise,
                                               'ABRL_GL_Merchandize');
            If V_Seg_Status is Not Null then
              V_Error_Lmsg := V_Error_Lmsg ||
                              'Invalid Merchandise Segment ' ||
                              ART_R3.Merchandise || '-->>';
            End If;
          End If;
          -- Validation for Account Seg
          If ART_R3.Account is Not Null then
            --  V_Error_Lmsg:=V_Error_Lmsg||'Accounting Segment Natural Account Value is Null-->>';             
            --Else
            V_Seg_Status := Null;
            V_Seg_Status := ACCOUNT_SEG_STATUS(ART_R3.Account,
                                               'ABRL_GL_Account');
            If V_Seg_Status is Not Null then
              V_Error_Lmsg := V_Error_Lmsg || 'Invalid Account Segment ' ||
                              ART_R3.Account || '-->>';
            End If;
          End If;
          -- Validation for Intercompany Seg
          If ART_R3.Intercompany is Not Null then
            --  V_Error_Lmsg:=V_Error_Lmsg||'Accounting Segment Intercompany Value is Null-->>';             
            --Else
            V_Seg_Status := Null;
            V_Seg_Status := ACCOUNT_SEG_STATUS(ART_R3.Intercompany,
                                               'ABRL_GL_STATE_SBU');
            If V_Seg_Status is Not Null then
              V_Error_Lmsg := V_Error_Lmsg ||
                              'Invalid Intercompany Segment ' ||
                              ART_R3.Intercompany || '-->>';
            End If;
          End If;
          -- Validation for Future Seg
          If ART_R3.Future is Not Null then
            --  V_Error_Lmsg:=V_Error_Lmsg||'Accounting Segment Future Value is Null-->>';             
            --Else
            V_Seg_Status := Null;
            V_Seg_Status := ACCOUNT_SEG_STATUS(ART_R3.Future,
                                               'ABRL_GL_Future');
            If V_Seg_Status is Not Null then
              V_Error_Lmsg := V_Error_Lmsg || 'Invalid Future Segment ' ||
                              ART_R3.Future || '-->>';
            End If;
          End If;
          If V_Cust_Account_Id is not null and V_cust_site_id is not null then
            V_State_Sbu := Null;
            Begin
              Select gcc.segment3
                Into V_State_Sbu
                from gl_code_combinations   gcc,
                     hz_cust_acct_sites_all hcas,
                     hz_cust_site_uses_all  hcsu
               where hcas.cust_account_id = V_Cust_Account_Id
               -- added by shailesh on 12 nov 2008 passing the org id from parameter 
                 and hcas.org_id = p_org_id
                 and hcas.cust_acct_site_id = hcsu.cust_acct_site_id(+)
                 and hcas.cust_Acct_site_id = V_cust_site_id
                 and hcsu.gl_id_rev = gcc.Code_Combination_Id;
                 --fnd_file.put_line(Fnd_file.OUTPUT,'Derived SBU: '||V_State_Sbu);
            EXCEPTION
              When no_data_found then
                V_Error_Lmsg := V_Error_Lmsg ||
                                'Accounting Code missing at Customer ' ||
                                ART_R2.CUSTOMER_NAME || ' Site  -->>';
                V_State_Sbu  := Null;
              When too_many_rows then
                V_Error_Lmsg := V_Error_Lmsg ||
                                'Multiple Charge Account Code exists for ' ||
                                ART_R2.CUSTOMER_NAME || ' Site-->>';
                V_State_Sbu  := Null;
              When Others Then
                V_Error_Lmsg := V_Error_Lmsg ||
                                'Accounting Code missing at Vendor Site ' ||
                                ART_R2.CUSTOMER_NAME || ' Site-->>';
                V_State_Sbu  := Null;
            End;
          End If;
          --Fnd_File.PUT_LINE(fnd_File.OUTPUT,'Error SBU: '||V_Error_Lmsg);
          If ART_R3.CO is not Null and ART_R3.CC is not Null and
             ART_R3.Location is not Null and ART_R3.Account is not Null Then
            Begin
              V_Code_Combination_id := Null;
                SELECT id_flex_num
                INTO   vn_structure_id
                FROM   FND_ID_FLEX_STRUCTURES_VL
                WHERE  UPPER(id_flex_structure_code) = 'ABRL_ACCOUNTING_FLEX'
                AND    id_flex_code                  = 'GL#' ;
              p_gl_id_rev :=  ART_R3.CO ||'.'|| ART_R3.CC ||'.'|| V_State_Sbu ||'.'|| ART_R3.Location ||'.'|| ART_R3.Merchandise ||'.'|| ART_R3.Account ||'.'|| ART_R3.Intercompany ||'.'|| ART_R3.Future;
              --Fnd_file.PUT_LINE(fnd_file.OUTPUT,'Generating CCID for :'||p_gl_id_rev);
                  x_gl_rev_acct_id := fnd_flex_ext.get_ccid (
                              application_short_name => 'SQLGL',
                              key_flex_code          => 'GL#',
                              structure_number       => vn_structure_id, -- 50308,
                              validation_date        => to_char(SYSDATE,'DD-MON-YYYY'),
                              concatenated_segments  => p_gl_id_rev );
              if x_gl_rev_acct_id <=0 then
                 Fnd_File.Put_Line(fnd_File.OUTPUT,'Error creating GL Revenue Acct:' ||fnd_flex_ext.get_message|| p_gl_id_rev);                  
                 V_Error_Lmsg := V_Error_Lmsg ||'Error creating GL Revenue Acct:' ||fnd_flex_ext.get_message;
              Else
                 --Fnd_File.Put_Line(fnd_File.OUTPUT,'Newly Generated CCID:' ||x_gl_rev_acct_id);                  
                  Begin              
/*                      select segment1||'.'||segment2||'.'||segment3||'.'||segment4||'.'||segment5||'.'||segment6||'.'||segment7||'.'||segment8
                      into v_Conc_Segments
                      from 
                      XXABRL_RESA_RA_INT_DIST_ALL
                      where record_number = ART_R3.record_number;
                      Fnd_File.Put_Line(fnd_File.OUTPUT,'Concatenated Segments in Dist Staging:' ||v_Conc_Segments);                  
                      Update XXABRL_RESA_RA_INT_DIST_ALL 
                      set Code_Combination_Id = x_gl_rev_acct_id
                      where segment1||'.'||segment2||'.'||segment3||'.'||segment4||'.'||segment5||'.'||segment6||'.'||segment7||'.'||segment8 = ART_R3.record_number
                      --where int_dist.INTERFACE_LINE_ATTRIBUTE1 = ART_R3.INTERFACE_LINE_ATTRIBUTE1
                      --and int_dist.INTERFACE_LINE_ATTRIBUTE2 = ART_R3.INTERFACE_LINE_ATTRIBUTE2
                      ;
*/
                      --Fnd_File.Put_Line(fnd_File.OUTPUT,'Derived CCID :' ||x_gl_rev_acct_id);                                      
                      /*INSERT into
                      XXABRL_AR_SEGMENTS_CCIDS
                      (
                      CONC_SEGMENTS,
                      CCID ,
                      RECORD_NUMBER)
                      Values
                      (
                      p_gl_id_rev,
                      x_gl_rev_acct_id,
                      ART_R3.record_number
                      );
                      Commit;
                      Update XXABRL_RESA_RA_INT_DIST_ALL t1
                      set t1.Code_Combination_Id = 
                      (select CCID 
                      from XXABRL_AR_SEGMENTS_CCIDS t2
                      where t2.record_number = t1.record_number
                      and rownum =1
                      );*/
                      NULL;
                  Exception
                  when no_data_found then
                       Fnd_File.Put_Line(fnd_File.OUTPUT,'Concatenated Segments in Dist Staging Not Found');                                    
                  when others then
                       Fnd_File.Put_Line(fnd_File.OUTPUT,'Error while updating CCID for '||SQLERRM ||'>>>'|| p_gl_id_rev);                  
                  End;
              End If;
/*              Select Code_Combination_id
                Into V_Code_Combination_id
                From GL_CODE_COMBINATIONS
               where Segment1 = ART_R3.CO
                 And Segment2 = ART_R3.CC
                 And Segment3 = V_State_Sbu
                 And Segment4 = ART_R3.Location
                 And Segment5 = ART_R3.Merchandise
                 And Segment6 = ART_R3.Account
                 And Segment7 = ART_R3.Intercompany
                 And Segment8 = ART_R3.Future;*/
            EXCEPTION
              When no_data_found then
                V_Error_Lmsg := V_Error_Lmsg ||
                                'ABRL_ACCOUNTING_FLEX does not exists-->>';
              When too_many_rows then
                V_Error_Lmsg := V_Error_Lmsg ||
                                'Multiple ABRL_ACCOUNTING_FLEX exists-->>';
              When Others Then
                V_Error_Lmsg := V_Error_Lmsg ||
                                'Exception in ABRL_ACCOUNTING_FLEX >>'||SQLERRM;
            End;
          End If;
          --Updating the error message in table
          IF V_Error_Lmsg is not null then
            Update XXABRL_RESA_RA_INT_DIST_ALL
               Set Error_message = V_Error_Lmsg, Interfaced_Flag = 'E'
             Where ROWID = ART_R3.ROWID;
             fnd_file.put_line(fnd_file.output,'Error in Dist: '||V_Error_Lmsg);
            Commit;
          Else
            Update XXABRL_RESA_RA_INT_DIST_ALL
               Set CODE_COMBINATION_ID = V_Code_Combination_id,
                   Org_Id              = P_Org_Id,
                   --                   Customer_Id              = V_Cust_Account_Id,
                   --                   Customer_Site_Id         = V_cust_site_id,
                   Interfaced_Flag = 'V'
             Where ROWID = ART_R3.ROWID;
             fnd_file.put_line(fnd_file.output,'Distribution Valid');
            Commit;
          End If;
        End Loop;
        If V_Error_Hmsg is not null Then
          Update XXABRL_RESA_RA_INT_LINES_ALL
             Set Error_message = V_Error_Hmsg, Interfaced_Flag = 'E'
           Where ROWID = ART_R2.ROWID;
           fnd_file.put_line(fnd_file.output,'Error in Line: '||V_Error_Hmsg);
          Commit;
          fnd_file.put_line(fnd_file.output,
                            'Invoice Line Number       :' ||
                            ART_R2.INTERFACE_LINE_ATTRIBUTE2);
          fnd_file.put_line(fnd_file.output,
                            'Line Record Error         :' || V_Error_Hmsg);
          V_Error_Count := V_Error_Count + 1;
          If V_Error_Lmsg is not null Then
            fnd_file.put_line(fnd_file.output,
                              'Distribution Record Error :' || V_Error_Lmsg);
          End If;
        Elsif V_Error_Hmsg is null And V_Error_Lmsg is not null Then
          Update XXABRL_RESA_RA_INT_LINES_ALL
             Set Error_message   = 'Error In Invoice Accounting Record '||V_Error_Lmsg,
                 Interfaced_Flag = 'E'
           Where ROWID = ART_R2.ROWID;
          Commit;
          V_Error_Count := V_Error_Count + 1;
          fnd_file.put_line(fnd_file.output,
                            'Invoice Line Number       :' ||
                            ART_R2.INTERFACE_LINE_ATTRIBUTE2);
          fnd_file.put_line(fnd_file.output,
                            'Distribution Error :' || V_Error_Lmsg);
        Else
          Update XXABRL_RESA_RA_INT_LINES_ALL
             Set NV_Customer_Id      = V_Cust_Account_Id,
                 NV_Cust_Site_Id = V_cust_site_id, 
                 Org_id = P_Org_Id,
                 TERM_ID_NEW  = v_term_id,
                 CUST_TRX_TYPE_ID = V_CUST_TRX_TYPE_ID,
                 Interfaced_Flag = 'V'
           Where ROWID = ART_R2.ROWID;
           fnd_file.put_line(fnd_file.output,'Line Valid');
          Commit;
          v_OK_Rec_count := v_OK_Rec_count + 1;
        End If;
      End Loop;
        Begin
          V_Data_Count := 0;
          Select  Count(INTERFACE_LINE_ATTRIBUTE1)
          Into    V_Data_Count
          From    XXABRL_RESA_RA_INT_LINES_ALL
          Where   BATCH_SOURCE_NAME      = ART_R1.BATCH_SOURCE_NAME
          And     ORG_CODE         = ART_R1.ORG_CODE
          And   INTERFACE_LINE_CONTEXT     = ART_R1.INTERFACE_LINE_CONTEXT
          And   INTERFACE_LINE_ATTRIBUTE1  = ART_R1.INTERFACE_LINE_ATTRIBUTE1
          And   NV_CUSTOMER_ID     = ART_R1.NV_CUSTOMER_ID      
          And   NV_CUST_SITE_ID         = ART_R1.NV_CUST_SITE_ID
          And   ERROR_MESSAGE      is not null;
          If V_Data_Count >0 then 
            Update  XXABRL_RESA_RA_INT_LINES_ALL
            Set     ERROR_MESSAGE      = 'Error In Invoice Lines>>>' ||ERROR_MESSAGE
            Where   BATCH_SOURCE_NAME      = ART_R1.BATCH_SOURCE_NAME
            And     ORG_CODE         = ART_R1.ORG_CODE
            And   INTERFACE_LINE_CONTEXT     = ART_R1.INTERFACE_LINE_CONTEXT
            And   INTERFACE_LINE_ATTRIBUTE1  = ART_R1.INTERFACE_LINE_ATTRIBUTE1
            And   NV_CUSTOMER_ID     = ART_R1.NV_CUSTOMER_ID
            And   NV_CUST_SITE_ID         = ART_R1.NV_CUST_SITE_ID
            And   ERROR_MESSAGE      is not null;
          End If;
        Exception 
          When Others Then
            V_Data_Count:=0;
        End;
    End Loop;
    fnd_file.put_line(fnd_file.output,
                      '........................................................................');
    fnd_file.put_line(fnd_file.output,
                      'Number of Valid Records    :' || v_OK_Rec_count);
    fnd_file.put_line(fnd_file.output,
                      'Number of Error Records  :' || V_Error_Count);
    fnd_file.put_line(fnd_file.output,
                      '........................................................................');
    if V_Error_Count >0 then
       RetCode :=1;
    End if;                  
    If p_action ='N' and V_Error_Count =0 then
       fnd_file.put_line(fnd_file.output,'--Inserting Data to Interface Tables--');
       fnd_file.put_line(fnd_file.output,'P_ORG_ID: ' ||nvl(to_char(P_Org_Id),'NULL'));
       fnd_file.put_line(fnd_file.output,'P_BATCH_Source: '||nvl(P_BATCH_Source,'NULL'));
       INVOICE_INSERT(P_Org_Id, P_BATCH_Source,RetCode);
    End if;          
  END INVOICE_VALIDATE;
  PROCEDURE INVOICE_INSERT(P_Org_Id IN NUMBER, P_BATCH_Source IN VARCHAR2, x_ret_code OUT number) is
    CURSOR ART_C1(CP_ORG_ID Number, CP_BATCH_SOURCE_NAME VARCHAR2) IS
      Select ROWID,
             BATCH_SOURCE_NAME,
             ORG_ID,
             INTERFACE_LINE_CONTEXT,
             INTERFACE_LINE_ATTRIBUTE1 INTERFACE_LINE_ATTRIBUTE1,
             INTERFACE_LINE_ATTRIBUTE2 INTERFACE_LINE_ATTRIBUTE2,
             LINE_TYPE,
             TRX_DATE,
             GL_DATE,
             AMOUNT,
             CURRENCY_CODE,
             CONVERSION_TYPE,
             CONVERSION_RATE,
             CONVERSION_DATE,
             TRANSACTION_TYPE,
             DESCRIPTION,     --COMMENTS,
             CUST_TRX_TYPE_ID,
             NV_CUSTOMER_ID,
             NV_CUST_SITE_ID,
             ATTRIBUTE1,
             ATTRIBUTE_CATEGORY,
             --CUSTOMER_ID,
             --CUSTOMER_SITE_ID,
             TERM_ID_NEW TERM_ID /*,
                   CUST_TRX_TYPE_ID*/
        From XXABRL_RESA_RA_INT_LINES_ALL RNLV
       Where /*BATCH_SOURCE_NAME = CP_BATCH_SOURCE_NAME
         And ORG_CODE = CP_ORG_ID
         And */NVL(INTERFACED_FLAG, 'N') = 'V'
          And Not Exists
      (Select INTERFACE_LINE_ATTRIBUTE1
                From XXABRL_RESA_RA_INT_LINES_ALL RNLU
               Where RNLU.BATCH_SOURCE_NAME = RNLV.BATCH_SOURCE_NAME
                 And RNLU.ORG_CODE = RNLV.ORG_CODE
                 And RNLU.NV_CUSTOMER_ID = RNLV.NV_CUSTOMER_ID
                 And RNLU.NV_CUST_SITE_ID = RNLV.NV_CUST_SITE_ID
                 And RNLU.INTERFACE_LINE_ATTRIBUTE1 =
                     RNLV.INTERFACE_LINE_ATTRIBUTE1
                 And NVL(RNLU.INTERFACED_FLAG, 'N') in ('N', 'E'))
       Order By /*ORG_CODE,*/ BATCH_SOURCE_NAME,
                INTERFACE_LINE_CONTEXT,
                INTERFACE_LINE_ATTRIBUTE1,
                INTERFACE_LINE_ATTRIBUTE2 /*,
                      CUSTOMER_ID,
                      CUSTOMER_SITE_ID*/
      ;
    CURSOR ART_C2(CP_BATCH_SOURCE_NAME VARCHAR2, CP_ORG_ID NUMBER, CP_INTERFACE_LINE_ATTRIBUTE1 VARCHAR2, CP_INTERFACE_LINE_ATTRIBUTE2 VARCHAR2, CP_CUSTOMER_ID NUMBER, CP_CUSTOMER_SITE_ID NUMBER) IS
      Select ROWID,
                   --BATCH_SOURCE_NAME,
                   --ORG_CODE,
                   INTERFACE_LINE_CONTEXT,
                   INTERFACE_LINE_ATTRIBUTE1 INTERFACE_LINE_ATTRIBUTE1,
                   INTERFACE_LINE_ATTRIBUTE2 INTERFACE_LINE_ATTRIBUTE2,
                   ACCOUNT_CLASS,
                   AMOUNT,
                   SEGMENT1 CO,
                   SEGMENT2 CC,
                   SEGMENT3 STATE_SBU,
                   SEGMENT4 LOCATION,
                   SEGMENT5 MERCHANDISE,
                   SEGMENT6 ACCOUNT,
                   SEGMENT7 INTERCOMPANY,
                   SEGMENT8 FUTURE,
                   ORG_ID,
                   CODE_COMBINATION_ID
        From XXABRL_RESA_RA_INT_DIST_ALL
       Where /*BATCH_SOURCE_NAME = CP_BATCH_SOURCE_NAME
               And */ 
       ORG_ID = CP_ORG_ID
       And
       INTERFACE_LINE_ATTRIBUTE1 = CP_INTERFACE_LINE_ATTRIBUTE1
       And INTERFACE_LINE_ATTRIBUTE2 = CP_INTERFACE_LINE_ATTRIBUTE2
/*       And CUSTOMER_ID = CP_CUSTOMER_ID
               And CUSTOMER_SITE_ID = CP_CUSTOMER_SITE_ID
*/       And
       NVL(INTERFACED_FLAG, 'N') in ('V');
    v_set_of_bks_id Number := Fnd_Profile.VALUE('GL_SET_OF_BKS_ID');
    v_user_id       number := FND_PROFILE.value('USER_ID');
    v_resp_id       number := FND_PROFILE.value('RESP_ID');
    v_appl_id       number := FND_PROFILE.value('RESP_APPL_ID');
    v_req_id        number;
    v_record_count  number := 0;
    v_error_msg varchar2(999);
       e_insert_int exception;
  Begin
    --XXABRL_UPDATE_ARINVDIST_CCID; --External Procedure to update CCIDs
    fnd_file.put_line(fnd_file.output,
                      '........................................................................');
    fnd_file.put_line(fnd_file.output,
                      'Insert Records Into Interface Table');
    fnd_file.put_line(fnd_file.output,'........................................................................');
    --fnd_file.put_line(fnd_file.output,'P_ORG_ID: ' ||nvl(P_ORG_ID,'NULL'));
    --fnd_file.put_line(fnd_file.output,'P_BATCH_Source: '||nvl(P_BATCH_Source,'NULL'));
    FOR ART_R1 IN ART_C1(to_char(P_ORG_ID), P_BATCH_Source) LOOP
      EXIT WHEN ART_C1%NOTFOUND;
      v_record_count := ART_C1%ROWCOUNT;
      fnd_file.put_line(fnd_file.output,' ');      
      fnd_file.put_line(fnd_file.output,'------------------------------------------------------------------------');
      fnd_file.put_line(fnd_file.output,'Inserting AR Line: '||Trim(ART_R1.INTERFACE_LINE_ATTRIBUTE1)||' ( '||Trim(ART_R1.INTERFACE_LINE_ATTRIBUTE2)||' )');                           
      Begin 
      INSERT INTO ra_interface_lines_all
        (--interface_line_id, --@Navisite
         interface_line_context,
         interface_line_attribute1,
         interface_line_attribute2,
         batch_source_name,
         set_of_books_id,
         line_type,
         description,
         currency_code,
         amount,
         cust_trx_type_id,
         trx_date,
         gl_date,
         orig_system_batch_name,
         orig_system_bill_customer_ID,
         orig_system_bill_address_ID,
         conversion_type,
         conversion_date,
         conversion_rate,
         term_id,
         comments,
         attribute_category,
         attribute1,
         org_id,
         created_by,
         creation_date,
         last_updated_by,
         last_update_date)
      Values
        (--XXABRL_AR_CNV_S.NEXTVAL,
         Trim(ART_R1.INTERFACE_LINE_CONTEXT),
         Trim(ART_R1.INTERFACE_LINE_ATTRIBUTE1),
         Trim(ART_R1.INTERFACE_LINE_ATTRIBUTE2),
         Trim(ART_R1.BATCH_SOURCE_NAME),
         v_set_of_bks_id,
         Trim(ART_R1.LINE_TYPE),
         Trim(ART_R1.DESCRIPTION),
         Trim(ART_R1.CURRENCY_CODE),
         Trim(ART_R1.AMOUNT),
         Trim(ART_R1.CUST_TRX_TYPE_ID),
         Trim(ART_R1.TRX_DATE),
         ART_R1.GL_DATE,
         Trim(ART_R1.BATCH_SOURCE_NAME) || Sysdate,
         Trim(ART_R1.NV_CUSTOMER_ID),
         Trim(ART_R1.NV_CUST_SITE_ID),
         Trim(NVL(ART_R1.CONVERSION_TYPE, 'Corporate')),
         Trim(ART_R1.CONVERSION_DATE),
         Trim(ART_R1.CONVERSION_RATE),
         Trim(ART_R1.TERM_ID),
         NULL,                        --Trim(ART_R1.DESCRIPTION) ,Trim(ART_R1.COMMENTS),
         Trim(ART_R1.ATTRIBUTE_CATEGORY), --'E-RETAIL RECEIVABLES INVOICE'
         Trim(ART_R1.ATTRIBUTE1), --Tax
         Trim(ART_R1.ORG_ID),
         V_user_Id,
         Sysdate,
         V_user_Id,
         Sysdate);
      Update XXABRL_RESA_RA_INT_LINES_ALL
         Set INTERFACED_FLAG = 'Y', INTERFACED_DATE = SYSDATE
       Where ROWID = ART_R1.ROWID;
       fnd_file.put_line(fnd_file.output,'Line Inserted Successfully');       
       Exception
       when others then
       v_error_msg := 'Error-Rollback: Exception in Invoice Line insert to Interface: '||SQLERRM;
       Raise e_insert_int;
       End;
      --Commit;
        fnd_file.put_line(fnd_file.output,'After 1 commit');                           
      FOR ART_R2 IN ART_C2(ART_R1.BATCH_SOURCE_NAME,
                           ART_R1.ORG_ID,
                           ART_R1.INTERFACE_LINE_ATTRIBUTE1,
                           ART_R1.INTERFACE_LINE_ATTRIBUTE2,
                           ART_R1.NV_CUSTOMER_ID,
                           ART_R1.NV_CUST_SITE_ID) LOOP
        EXIT WHEN ART_C2%NOTFOUND;
        fnd_file.put_line(fnd_file.output,'Inserting AR Line Distribution: '||Trim(ART_R1.INTERFACE_LINE_ATTRIBUTE1)||' ( '||Trim(ART_R1.INTERFACE_LINE_ATTRIBUTE2)||' )');
        Begin
          INSERT INTO ra_interface_distributions_all
            (interface_line_context,
             interface_line_attribute1,
             interface_line_attribute2,
             account_class,
             amount,
--             code_combination_id,
             org_id,
             created_by,
             creation_date,
             last_updated_by,
             last_update_date,
             percent,
             segment1,
             segment2,
             segment3,
             segment4,
             segment5,
             segment6,
             segment7,
             segment8
             )
          Values
            (Trim(ART_R2.INTERFACE_LINE_CONTEXT),
             Trim(ART_R2.INTERFACE_LINE_ATTRIBUTE1),
             Trim(ART_R2.INTERFACE_LINE_ATTRIBUTE2),
             Trim(ART_R2.ACCOUNT_CLASS),
             Trim(ART_R2.AMOUNT),
--             Trim(ART_R2.CODE_COMBINATION_ID),
             Trim(ART_R2.ORG_ID),
             V_user_Id,
             Sysdate,
             V_user_Id,
             Sysdate,
             100,
             ART_R2.CO,
             ART_R2.CC,
             ART_R2.STATE_SBU,
             ART_R2.LOCATION,
             ART_R2.MERCHANDISE,
             ART_R2.ACCOUNT,
             ART_R2.INTERCOMPANY,
             ART_R2.FUTURE
             );
        --End If;
        Update XXABRL_RESA_RA_INT_DIST_ALL
           Set INTERFACED_FLAG = 'Y', INTERFACED_DATE = SYSDATE
         Where ROWID = ART_R2.ROWID;
         fnd_file.put_line(fnd_file.output,'AR Line Distribution Inserted Successfully');         
       Exception
       when others then
       v_error_msg := 'Error-Rollback: Exception in Invoice Line-Distribution insert: '||SQLERRM;
       Raise e_insert_int;
       End;         
        --Commit;
      End Loop;
    End Loop;
    commit;
    fnd_file.put_line(fnd_file.output,
                      'Number of Records (Invoices Lines/Dist )Inserted in Interface Table :' ||
                      v_record_count);
    fnd_file.put_line(fnd_file.output,
                      '........................................................................');
    -----
    -- Run Auto Invoice to upload data into Oracle Receivables
    ----
    /*If v_record_count > 0 Then
      fnd_global.apps_initialize(user_id      => v_user_id,
                                 resp_id      => v_resp_id,
                                 resp_appl_id => v_appl_id);
      Commit;
      v_req_id := fnd_request.submit_request('AR',
                                             'RAXTRX',
                                             'Autoinvoice Import Program' ||
                                             P_BATCH_Source,
                                             NULL,
                                             FALSE,
                                             'MAIN',
                                             'T',
                                             1021,
                                             P_Batch_Source, -- Parameter Data source
                                             Sysdate,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             'N',
                                             'Y',
                                             NULL,
                                             p_org_id,
                                             CHR(0));
      Commit;
      fnd_file.put_line(fnd_file.output,
                        'Please see the output of Payables OPEN Invoice Import program request id :' ||
                        v_req_id);
      fnd_file.put_line(fnd_file.output,
                        '........................................................................');
    End If;*/
  Exception
  when e_insert_int then
       fnd_file.put_line(fnd_file.output,v_error_msg);
       fnd_file.put_line(fnd_file.output,'*** Insert process Rollbacked, No data inserted to Interface');
       rollback;
       x_ret_code :=1;
  End INVOICE_INSERT;
  FUNCTION ACCOUNT_SEG_STATUS(P_Seg_Value IN Varchar2,
                              P_Seg_Desc  IN Varchar2) return Varchar2 Is
    V_Count Number := 0;
  Begin
    Select Count(FFVV.Flex_Value)
      Into V_Count
      From FND_FLEX_VALUES_VL FFVV, FND_FLEX_VALUE_SETS FFVS
     Where Upper(FFVS.Flex_Value_Set_Name) = Upper(P_Seg_Desc)
       And FFVS.Flex_Value_Set_Id = FFVV.Flex_Value_Set_Id
       And FFVV.Flex_Value = P_Seg_Value;
    If V_Count = 1 Then
      Return Null;
    Else
      Return 'Invalid Value';
    End If;
  EXCEPTION
    When Others Then
      Return 'Invalid Value';
  End ACCOUNT_SEG_STATUS;
END XXABRL_RESA_AR_INV_IMP_PKG; 
/


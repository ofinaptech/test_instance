CREATE OR REPLACE PACKAGE BODY APPS.XXTSRL_CONCPROG_SCHEDULER_PKG IS


/*
=========================================================================================================
||   Filename   : XXTSRL_CONCPROG_SCHEDULER_PKG.sql
||   Description : Script is used to Load GL Data (GL Conversion)
||
||   Version     Date            Author              Modification
||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
||   1.0.0        24-feb-2010    Naresh Hasti       New Development
||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
||
||   Usage : This script is used to upload all the E-Retail files to stage table and move the loaded file to another folder
||
========================================================================================================*/


 procedure xxabrl_wait_for_req_prc(p_request_id in number,
                                     p_req_name in varchar2
                                     ,p_phase out varchar2
                                     ,p_status out varchar2) as
   
   
l_suv_phase   varchar2(100);
l_suv_status  varchar2(100);
l_suv_phase1  varchar2(100);
l_suv_status1 varchar2(100);
l_sup_errbuff varchar2(500);
l_sup_b_call_status boolean;
   
   begin
   
   IF p_request_id = 0
         THEN

            fnd_file.put_line
                        (fnd_file.LOG,
                         'Error in Invoice Validation AND Conversion Program'
                        );
         ELSE
            COMMIT;
            l_sup_b_call_status :=
               fnd_concurrent.wait_for_request
                                         (request_id      => p_request_id,
                                          INTERVAL        => 5,
                                          max_wait        => 0,
                                          phase           => l_suv_phase,
                                          status          => l_suv_status,
                                          dev_phase       => l_suv_phase1,
                                          dev_status      => l_suv_status1,
                                          MESSAGE         => l_sup_errbuff
                                         );

            --
            
            p_phase  := l_suv_phase1;
            p_status := l_suv_status1;
            
            IF l_sup_b_call_status
            THEN
               --
               IF (l_suv_phase1 = 'COMPLETE' AND l_suv_status1 = 'NORMAL')
               THEN
                  fnd_file.put_line (fnd_file.LOG,
                                        p_req_name
                                     || TO_CHAR (p_request_id)
                                     || 'Completed Succesfully'
                                    );
               ELSE
                  fnd_file.put_line (fnd_file.LOG,
                                        p_req_name
                                     || TO_CHAR (p_request_id)
                                     || ' Error'
                                    );
               END IF;
            --
            ELSE
               fnd_file.put_line (fnd_file.LOG,
                                     p_req_name
                                  || TO_CHAR (p_request_id)
                                  || ' Not Completed'
                                 );
            END IF;
         END IF;
   
end xxabrl_wait_for_req_prc;
 

  PROCEDURE ER_CONCPROG_SCHEDULER(
                             Errbuf         OUT VARCHAR2,
                             RetCode        OUT NUMBER
                             ) IS 
success BOOLEAN;
stop_program EXCEPTION;
v_phase varchar2(10);
v_status varchar2(10);
v_errmsg        VARCHAR2(250);
V_H_File        VARCHAR2(250);
V_L_File        VARCHAR2(250);
v_IN_DIR        VARCHAR2(300) :='/ebiz/shared_u01/PROD/apps/apps_st/appl/xxabrl/12.0.0/interface/E_Retail/AP/Uploading/' ;
 v_req_id number:=0;
--V_DC_CODE VARCHAR2(100);
                             
      CURSOR CUR1  IS  SELECT LOOKUP_CODE
           --INTO V_DC_CODE
           FROM FND_LOOKUP_VALUES_VL
          WHERE SYSDATE BETWEEN NVL(START_DATE_ACTIVE, SYSDATE) AND
                NVL(END_DATE_ACTIVE, SYSDATE)
            AND ENABLED_FLAG = 'Y'
            AND LOOKUP_TYPE = 'ER_AP_DC_OU_MAP';
            --and lookup_code='TSAP_GD';
            
            BEGIN
            
            fnd_file.put_line(FND_FILE.LOG, 'Before cursor');
        
        for r1 in cur1 loop
                
        SELECT 'ER_AP_'||r1.LOOKUP_CODE||'_H_'||TO_CHAR(SYSDATE-1,'DDMMYY')||'.csv' INTO V_H_File FROM dual;
        SELECT 'ER_AP_'||r1.LOOKUP_CODE||'_L_'||TO_CHAR(SYSDATE-1,'DDMMYY')||'.csv' INTO V_L_File FROM dual;

          fnd_file.put_line(FND_FILE.LOG, 'Loading Header File');
         v_req_id := fnd_request.submit_request (  'XXABRL', 
                                                   'XXABRL_AP_INVOICE_INT_LOAD',
                                                   '',
                                                   '',
                                                   FALSE,
                                                   V_IN_DIR||V_H_File
                                                );
                                                
        fnd_file.put_line(fnd_file.output,' E-Retail file load for '|| V_IN_DIR||V_H_File||' Request ID-> '||v_req_id);                                       
        
        xxabrl_wait_for_req_prc(v_req_id,'E-Retail Header File Load',v_phase,v_status);
        
        fnd_file.put_line(FND_FILE.LOG, 'Phase - Status  '||v_phase||' - '||v_status);
        
        if v_phase = UPPER('Complete') and v_status = UPPER('Warning') then 
        
        fnd_file.put_line(FND_FILE.LOG, 'File Can not move as the request got warning for file '|| V_H_File||'   '|| V_L_File);
        
        else  
        
        
        fnd_file.put_line(FND_FILE.LOG, 'Loading Line File');
        
        v_req_id := fnd_request.submit_request (  'XXABRL', 
                                                   'XXABRL_AP_INV_LINES_INT_LOAD',
                                                   '',
                                                   '',
                                                   FALSE,
                                                   V_IN_DIR||V_L_File
                                                );
                                                
        fnd_file.put_line(fnd_file.output,' E-Retail file load for '|| V_IN_DIR||V_H_File||' Request ID-> '||v_req_id);                                       
        
        xxabrl_wait_for_req_prc(v_req_id,'E-Retail Line File Load',v_phase,v_status);
        
        end if;
        
                fnd_file.put_line(FND_FILE.LOG, 'Phase - Status  '||v_phase||' - '||v_status);
        if v_phase = upper('Complete') and v_status = upper('Warning') then 
        
        fnd_file.put_line(FND_FILE.LOG, 'File Can not move as the request got warning for file '|| V_H_File||'   '|| V_L_File);
        
        else  
        fnd_file.put_line(FND_FILE.LOG, 'Moving the file to Archived folder');
        
        v_req_id := fnd_request.submit_request (  'XXABRL', 
                                                   'XXABRL_AP_INV_INT_FILE_MOVE',
                                                   '',
                                                   '',
                                                   FALSE,
                                                   V_H_File,
                                                   V_L_File
                                                );
                                                
        fnd_file.put_line(fnd_file.output,' E-Retail file load for '|| V_IN_DIR||V_H_File||' Request ID-> '||v_req_id);                                       
        
        xxabrl_wait_for_req_prc(v_req_id,'E-Retail File Move',v_phase,v_status);
        
        end if;
                                             
        end loop;
        
        /*
            for r1 in cur1 loop
        --V_IN_DIR := V_IN_DIR||'AP/';
        
        
        success := fnd_submit.set_request_set('XXABRL','XXABRL_INV_LOAD_REQSET');
              
        SELECT 'ER_AP_'||r1.LOOKUP_CODE||'_H_'||TO_CHAR(SYSDATE-1,'DDMMYY')||'.csv' INTO V_H_File FROM dual;
        SELECT 'ER_AP_'||r1.LOOKUP_CODE||'_L_'||TO_CHAR(SYSDATE-1,'DDMMYY')||'.csv' INTO V_L_File FROM dual;


        --Submit Header Loader
        IF (success) THEN
        fnd_file.put_line(FND_FILE.LOG, 'header file upload');
           --Print_Log('Submitting AP-Header Loader...',v_Print_log);
           success := fnd_submit.submit_program('XXABRL',
                                         'XXABRL_AP_INVOICE_INT_LOAD',
                                         'STAGE_10',
                                         V_IN_DIR||V_H_File);


                                   
                                         
          IF (NOT success) THEN
             v_errmsg := 'Error:in AP-Header Loader Submit';
             fnd_file.put_line(FND_FILE.LOG, 'error in Header file upload');
             RAISE stop_program;
          END IF;
     fnd_file.put_line(FND_FILE.LOG, 'error in Header file upload after if loop');     
   END IF;
  


        --Submit Line Loader
        IF (success) THEN
          -- Print_Log('Submitting AP-Line Loader...',v_Print_log);
           success := fnd_submit.submit_program('XXABRL',
                                         'XXABRL_AP_INV_LINES_INT_LOAD',
                                         'STAGE_20',
                                         V_IN_DIR||V_L_File);

          IF (NOT success) THEN
             v_errmsg := 'Error:in AP-Line Loader Submit';
             fnd_file.put_line(FND_FILE.LOG, 'error in Line file upload');
             RAISE stop_program;
          END IF;

        END IF;


        --Submit File Move
        IF (success) THEN

          fnd_file.put_line(FND_FILE.LOG, 'Submitting File_Move...');

          success := fnd_submit.submit_program('XXABRL',
                                               'XXABRL_AP_INV_INT_FILE_MOVE',
                                               'STAGE_30',
                                               V_H_File,
                                               V_L_File
                                               );
          IF (NOT success) THEN
             v_errmsg := 'Error:in Navision AP-File Move Submit';
             fnd_file.put_line(FND_FILE.LOG, 'error in File move');
             RAISE stop_program;
          END IF;

        END IF;
           
       end loop;
       
       EXCEPTION
        WHEN OTHERS THEN
          fnd_file.put_line(FND_FILE.LOG,v_errmsg);
          fnd_file.put_line(FND_FILE.LOG,
                            'Exception in E-Retail AP Request-Submit: ' || SQLERRM);       
        
           */
           
                        
  END ER_CONCPROG_SCHEDULER;                           
                             
END XXTSRL_CONCPROG_SCHEDULER_PKG; 
/


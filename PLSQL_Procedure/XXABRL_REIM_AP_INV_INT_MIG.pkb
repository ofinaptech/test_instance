CREATE OR REPLACE PACKAGE BODY APPS.XXABRL_REIM_AP_INV_INT_MIG IS
/***********************************************************************************************************
                 OBJECT INFORMATION

 OBJECT NAME : XXABRL_REIM_AP_INV_INT_MIG

 DESCRIPTION : MIGRATION OF INVOICES FROM REIM TO ORACLE PAYABLES

  CHANGE RECORD:
 ==============================================================================================
 VERSION    DATE            AUTHOR                              REMARKS
 =======   ==========     =============                         ============================================
 1.1     10-NOV-2009      PRAVEEN KUMAR (WIPRO INFOTECH LTD)    TO ADD THE CONDITION FOT INACTIVE SUPPLIERS
************************************************************************************************************


  /*======================================VARIABLE DECLARATION ============================================*/

  PROCEDURE VALIDATE_INVOICES(ERRBUF OUT VARCHAR2, RETCODE OUT NUMBER) IS
    V_INVOICE_NUM                VARCHAR2(50) := NULL;
    V_COUNT                      NUMBER := NULL;
    V_DATA_COUNT                 NUMBER;
    V_INACTIVE_SITE              NUMBER;
    V_ERROR_FLAG                 VARCHAR2(1) := NULL;
    V_ERROR_LINE_FLAG            VARCHAR2(1) := NULL;
    V_HDR_ERR_MSG                VARCHAR2(1000) := NULL;
    V_LINE_ERR_MSG               VARCHAR2(1000) := NULL;
    V_ERROR_ALMSG                VARCHAR2(4000) := NULL;
    V_VENDOR_ID                  NUMBER(15) := NULL;
    V_VENDOR_SITE_ID             NUMBER(15) := NULL;
    V_VENDOR_SITE_CODE           VARCHAR2(30) := NULL;
    V_INVOICE_CURRENCY_CODE      VARCHAR2(30) := NULL;
    V_PAYMENT_METHOD_LOOKUP_CODE VARCHAR2(30) := NULL;
    V_DIST_CODE_CONCATENATED     VARCHAR2(300) := NULL;

    -- FOLLOWING VARIAVBLE CREATED BY SHAILESH ON 25 OCT 2008 FOR STORING THE DEFAULT ACCOUNTING SEGMENT VALUES FOR VENDOR
    L_DIST_CODE_CONCATENATED VARCHAR2(300) := NULL;
    L_NAT_ACCT               VARCHAR2(10);
    -- WILL STORE THE TERMS_ID FROM THE VENDOR MASTER SEE THE VALIDATION FOR DETAILS
    L_TERMS_ID NUMBER;
    -- CAPTURE THE LOCATION FROM DIST_CODE_COMBINITION (SEGMENT4) VALUE
    L_LOCATION VARCHAR2(10);

    --REC_INV_HDR.VENDOR_NUM                         VARCHAR2(50)        := NULL;
    V_CHECK_FLAG            VARCHAR2(1) := NULL;
    V_FUN_CURR              VARCHAR2(15) := NULL;
    V_FLAG_COUNT            NUMBER := 0;
    V_SET_OF_BOOKS_ID       NUMBER := FND_PROFILE.VALUE('GL_SET_OF_BKS_ID');
    V_PAY_LOOKUP_CODE       VARCHAR2(50) := NULL;
    V_TERM_NAME             AP_TERMS_TL.NAME%TYPE;
    V_ERROR_HEAD_COUNT      NUMBER := 0;
    V_SUCCESS_RECORD_COUNT  NUMBER := 0;
    V_ERROR_LINE_COUNT      NUMBER := 0;
    V_LINE_TYPE_LOOKUP_CODE VARCHAR2(25) := NULL;
    V_INV_LINE_AMT          NUMBER := 0;
    --V_USER_ID                                  NUMBER              :=FND_PROFILE.VALUE('USER_ID');
    --V_RESP_ID                                  NUMBER              :=FND_PROFILE.VALUE('RESP_ID');
    --V_APPL_ID                                  NUMBER              :=FND_PROFILE.VALUE('RESP_APPL_ID');
    V_INV_TYPE            VARCHAR2(30) := NULL;
    V_TOTAL_ERROR_RECORDS NUMBER := 0;
    V_LINE_COUNT          NUMBER := 0;
    V_TOTAL_RECORDS       NUMBER := 0;
    V_SUP_STATUS          VARCHAR2(1) := NULL;

    V_DUMMY_FLAG                 VARCHAR2(1) := NULL;
    V_PURCHASING_SITE_FLAG       VARCHAR2(1) := NULL;
    V_RFQ_ONLY_SITE_FLAG         VARCHAR2(1) := NULL;
    P_ORG_ID                     NUMBER := NULL;
    P_VENDOR_SITE_ID             NUMBER(15) := NULL;
    P_VENDOR_ID                  NUMBER(15) := NULL;
    P_PAYMENT_METHOD_LOOKUP_CODE VARCHAR2(30) := NULL;
    V_PAY_SITE_FLAG              VARCHAR2(1) := NULL;
    V_PAY_COUNT                  NUMBER;
    V_RFQ_COUNT                  NUMBER;
    V_PUR_COUNT                  NUMBER;
    V_HO_COUNT                   NUMBER;
    V_ORG_CNT NUMBER:=0;
    P_VENDOR_NUM                NUMBER;
    /*=============================DECLARATION OF 'INVOICES' CURSOR ==================================================*/

    -- ADDED BY SHAILESH ON 25 OCT 2008
    -- ABOVE COMMENTED CURSOR REPLACED BY NEW CURSOR

    CURSOR CUR_INV_HDR IS
      SELECT APS.ROWID,
             APS.INVOICE_NUM,
             APS.INVOICE_TYPE_LOOKUP_CODE,
             APS.INVOICE_DATE,
             APS.VENDOR_NUM,
             APS.TERMS_ID,
             ROUND(APS.INVOICE_AMOUNT, 2) INVOICE_AMOUNT,
             APS.INVOICE_CURRENCY_CODE,
             APS.EXCHANGE_DATE,
             APS.EXCHANGE_RATE_TYPE,
             APS.DESCRIPTION,
             APS.GL_DATE,
             APS.GROUP_ID,
             APS.VENDOR_EMAIL_ADDRESS,
             APS.TERMS_DATE,
             APS.ACCTS_PAY_CODE_CONCATENATED,
             APS.ERROR_FLAG,
             APS.ERROR_MSG,
             APS.GOODS_RECEIVED_DATE, -- NEW COLOUMN ADDED IN THE  XXABRL.XXABRL_AP_HEADER_STAGE
             APS.ORG_ID,
             APS.SOURCE,
             APS.VENDOR_SITE_ID,
             APS.VENDOR_ID,
             APS.PAYMENT_METHOD_LOOKUP_CODE
        FROM APPS.XXABRL_AP_HEADER_STAGE APS
       WHERE NVL(APS.ERROR_FLAG, 'N') IN ('N', 'E');
    --AND     APS.INVOICE_NUM IN ('INV_10563');

    
        -- ADDED BY SHAILESH ON 25 OCT 2008
    -- ABOVE COMMENTED CURSOR REPLACED BY NEW CURSOR

    CURSOR CUR_INV_LINE(P_INV_NUM VARCHAR2 --, P_VENDOR_SITE_ID NUMBER,     
    ,P_VENDOR_NUM NUMBER) IS
      SELECT ALS.ROWID,
             ALS.VENDOR_NUM,
             ALS.VENDOR_SITE_ID,
             ALS.INVOICE_NUM,
             ALS.LINE_NUMBER,
             ALS.LINE_TYPE_LOOKUP_CODE,
             ALS.AMOUNT AMOUNT,
             ALS.ACCOUNTING_DATE,
             ALS.DIST_CODE_CONCATENATED,
             ALS.DESCRIPTION,
             ALS.TDS_TAX_NAME,
             ALS.WCT_TAX_NAME,
             ALS.SERVICE_TAX,
             ALS.VAT
        FROM APPS.XXABRL_AP_LINE_STAGE ALS
       WHERE NVL(ALS.ERROR_FLAG, 'N') <> 'Y' --IN('N','E')
         AND ALS.INVOICE_NUM = P_INV_NUM
         AND ALS.VENDOR_NUM = P_VENDOR_NUM;

    CURSOR CUR_CHECK_FLAGS(P_VENDOR_NUM VARCHAR2,CP_ORG_ID NUMBER) IS
      SELECT ASUPS.ORG_ID,
             ASUPS.VENDOR_SITE_ID,
             ASUP.VENDOR_ID,
             ASUPS.PAYMENT_METHOD_LOOKUP_CODE,
             ASUPS.PAY_SITE_FLAG,
             ASUPS.PURCHASING_SITE_FLAG,
             ASUPS.RFQ_ONLY_SITE_FLAG
        FROM AP_SUPPLIERS ASUP, AP_SUPPLIER_SITES_ALL ASUPS
       WHERE ASUP.VENDOR_ID = ASUPS.VENDOR_ID
         AND ((NVL(ASUPS.PAY_SITE_FLAG, 'N') = 'N' AND
             NVL(ASUPS.PURCHASING_SITE_FLAG, 'N') = 'N' AND
             NVL(ASUPS.RFQ_ONLY_SITE_FLAG, 'N') = 'Y') OR
             (NVL(ASUPS.PAY_SITE_FLAG, 'N') = 'N' AND
             NVL(ASUPS.PURCHASING_SITE_FLAG, 'N') = 'Y' AND
             NVL(ASUPS.RFQ_ONLY_SITE_FLAG, 'N') = 'N') OR
             (NVL(ASUPS.PAY_SITE_FLAG, 'N') = 'Y' AND
             NVL(ASUPS.PURCHASING_SITE_FLAG, 'N') = 'N' AND
             NVL(ASUPS.RFQ_ONLY_SITE_FLAG, 'N') = 'N') OR
             (NVL(ASUPS.PAY_SITE_FLAG, 'N') = 'N' AND
             NVL(ASUPS.PURCHASING_SITE_FLAG, 'N') = 'N' AND
             NVL(ASUPS.RFQ_ONLY_SITE_FLAG, 'N') = 'N'))
         AND ASUP.SEGMENT1 = P_VENDOR_NUM
         -- ADDED BY SHAILESH ON 24 APR 2009 NEW COL ADDED IN FILE ORG ID SO THE SUPPLER SITE SHOULD COME ON THE BASIS OF ORG ID
         AND ASUPS.ORG_ID = CP_ORG_ID
         AND NVL(TRUNC(ASUPS.INACTIVE_DATE), TRUNC(SYSDATE)) >= TRUNC(SYSDATE);


/*    

10.04.2012    NOT REQUIRED FOR TSRL LEDGER MIGRATION TO ABRL LEDGER 

  BEGIN

            UPDATE APPS.XXABRL_AP_HEADER_STAGE APS
            SET ACCTS_PAY_CODE_CONCATENATED=SUBSTR(APS.ACCTS_PAY_CODE_CONCATENATED,1,7)||'752'
            ||SUBSTR(APS.ACCTS_PAY_CODE_CONCATENATED,11,37),
            ORG_ID=801
            WHERE  NVL(APS.ERROR_FLAG, 'N') IN ('N', 'E')
           AND GL_DATE>'31-MAR-2012'
            AND ORG_ID=84;


            COMMIT;


            UPDATE APPS.XXABRL_AP_HEADER_STAGE APS
            SET ACCTS_PAY_CODE_CONCATENATED=SUBSTR(APS.ACCTS_PAY_CODE_CONCATENATED,1,7)||'812'
            ||SUBSTR(APS.ACCTS_PAY_CODE_CONCATENATED,11,37),
            ORG_ID=901
            WHERE  NVL(APS.ERROR_FLAG, 'N') IN ('N', 'E')
           AND GL_DATE>'31-MAR-2012'
            AND ORG_ID=89;


             COMMIT;

            UPDATE APPS.XXABRL_AP_HEADER_STAGE APS
            SET ACCTS_PAY_CODE_CONCATENATED=SUBSTR(APS.ACCTS_PAY_CODE_CONCATENATED,1,7)||'772'
            ||SUBSTR(APS.ACCTS_PAY_CODE_CONCATENATED,11,37),
            ORG_ID=861
            WHERE   NVL(APS.ERROR_FLAG, 'N') IN ('N', 'E')
            AND GL_DATE>'31-MAR-2012'
            AND ORG_ID=87;


            COMMIT;



             UPDATE APPS.XXABRL_AP_HEADER_STAGE APS
            SET ACCTS_PAY_CODE_CONCATENATED=SUBSTR(APS.ACCTS_PAY_CODE_CONCATENATED,1,7)||'792'
            ||SUBSTR(APS.ACCTS_PAY_CODE_CONCATENATED,11,37),
            ORG_ID=961
            WHERE     NVL(APS.ERROR_FLAG, 'N') IN ('N', 'E')
            AND GL_DATE>'31-MAR-2012'
            AND ORG_ID=91;


          COMMIT;

          ----LINES TABLES


            UPDATE APPS.XXABRL_AP_LINE_STAGE ALS
            SET DIST_CODE_CONCATENATED=SUBSTR(DIST_CODE_CONCATENATED,1,7)||'752'
            ||SUBSTR(DIST_CODE_CONCATENATED,11,37)
             WHERE  (INVOICE_NUM,VENDOR_NUM) IN (SELECT INVOICE_NUM,VENDOR_NUM FROM XXABRL_AP_HEADER_STAGE APS
            WHERE   NVL(APS.ERROR_FLAG, 'N') IN ('N', 'E')
           AND GL_DATE>'31-MAR-2012'
            AND ORG_ID=801)
          AND ACCOUNTING_DATE>'31-MAR-2012'
            AND NVL(ALS.ERROR_FLAG, 'N') IN ('N', 'E');


            UPDATE APPS.XXABRL_AP_LINE_STAGE ALS
            SET DIST_CODE_CONCATENATED=SUBSTR(DIST_CODE_CONCATENATED,1,7)||'812'
            ||SUBSTR(DIST_CODE_CONCATENATED,11,37)
            WHERE  (INVOICE_NUM,VENDOR_NUM) IN (SELECT INVOICE_NUM,VENDOR_NUM FROM XXABRL_AP_HEADER_STAGE APS
            WHERE GL_DATE>'31-MAR-2012'
            AND ORG_ID=901
            AND NVL(APS.ERROR_FLAG, 'N') IN ('N', 'E'))
            AND ACCOUNTING_DATE>'31-MAR-2012'
            AND NVL(ALS.ERROR_FLAG, 'N') IN ('N', 'E');


            UPDATE APPS.XXABRL_AP_LINE_STAGE ALS
            SET DIST_CODE_CONCATENATED=SUBSTR(DIST_CODE_CONCATENATED,1,7)||'772'
            ||SUBSTR(DIST_CODE_CONCATENATED,11,37)
            WHERE (INVOICE_NUM,VENDOR_NUM) IN (SELECT INVOICE_NUM,VENDOR_NUM FROM XXABRL_AP_HEADER_STAGE APS
            WHERE GL_DATE>'31-MAR-2012'
            AND ORG_ID=861
            AND NVL(APS.ERROR_FLAG, 'N') IN ('N', 'E'))
            AND ACCOUNTING_DATE>'31-MAR-2012'
            AND NVL(ALS.ERROR_FLAG, 'N') IN ('N', 'E');


            UPDATE APPS.XXABRL_AP_LINE_STAGE ALS
            SET DIST_CODE_CONCATENATED=SUBSTR(DIST_CODE_CONCATENATED,1,7)||'792'
            ||SUBSTR(DIST_CODE_CONCATENATED,11,37)
            WHERE  (INVOICE_NUM,VENDOR_NUM) IN (SELECT INVOICE_NUM,VENDOR_NUM FROM XXABRL_AP_HEADER_STAGE APS
            WHERE  NVL(APS.ERROR_FLAG, 'N') IN ('N', 'E')
            AND GL_DATE>'31-MAR-2012'
            AND ORG_ID=961)
            AND ACCOUNTING_DATE>'31-MAR-2012'
            AND NVL(ALS.ERROR_FLAG, 'N') IN ('N', 'E');

            COMMIT;


    END;
    
10.04.2012    NOT REQUIRED FOR TSRL LEDGER MIGRATION TO ABRL LEDGER         

*/



/*    HELLO 2   */
 BEGIN

    FOR REC_INV_HDR IN CUR_INV_HDR 
    LOOP
      EXIT WHEN CUR_INV_HDR%NOTFOUND;

     P_ORG_ID:=0;
     P_VENDOR_SITE_ID :=0;
     P_VENDOR_ID:=0;



      V_ERROR_FLAG                 := NULL;
      V_HDR_ERR_MSG                := NULL;
      V_VENDOR_ID                  := NULL;
      V_VENDOR_SITE_ID             := NULL;
      V_INVOICE_CURRENCY_CODE      := NULL;
      V_PAYMENT_METHOD_LOOKUP_CODE := NULL;
       REC_INV_HDR.VENDOR_NUM                        := NULL;
      V_CHECK_FLAG      := 'N';
      V_PAY_LOOKUP_CODE := NULL;
      --V_ERROR_HEAD_COUNT                   := 0;
      V_INV_LINE_AMT := 0;
      V_INV_TYPE     := NULL;
      --V_SUCCESS_RECORD_COUNT                := 0;
      V_SUP_STATUS := 'N';

      --    P_ORG_ID                               := NULL;
      --    P_VENDOR_SITE_ID                       := NULL;
      --    P_VENDOR_ID                            := NULL;
      --    P_PAYMENT_METHOD_LOOKUP_CODE           := NULL;
      --    V_PAY_SITE_FLAG                        := NULL;
      V_FLAG_COUNT := 0;
      V_PAY_COUNT  := 0;
      V_RFQ_COUNT  := 0;
      V_PUR_COUNT  := 0;
      V_HO_COUNT   := 0;


      -- ADDED BY SHAILESH ON 19 JUNE 2009 AS ORG ID IS COMMING NULL IN DATA FILE SO IF ORG ID IS NULL
     -- VALIDATION FOR THE ORG ID IF IT IS NULL IN STAGING TABLE IT WILL GIVE ERROR

        BEGIN

            FND_FILE.PUT_LINE(FND_FILE.LOG,'VALIDATION FOR ORG ID -> '||REC_INV_HDR.ORG_ID);


          SELECT COUNT(ORGANIZATION_ID)
          INTO V_ORG_CNT
          FROM APPS.HR_OPERATING_UNITS HOU
          WHERE HOU.ORGANIZATION_ID=REC_INV_HDR.ORG_ID;

        ---- WE NEED TO SKIP FULL VALIDATION IN THIS CASE IF ORG_ID IS NULL

          EXCEPTION

          WHEN NO_DATA_FOUND THEN
           V_ERROR_FLAG  := 'Y';
           V_HDR_ERR_MSG := 'ORG IS NULL OR ORG NOT AVAILABLE';
           FND_FILE.PUT_LINE(FND_FILE.LOG,'ORG IS NULL OR ORG NOT AVAILABLE-> '||V_ERROR_FLAG);
          WHEN OTHERS THEN
                 V_ERROR_FLAG  := 'Y';
                 V_HDR_ERR_MSG := 'ORG IS NULL OR ORG NOT AVAILABLE';

         -- FND_FILE.PUT_LINE(FND_FILE.LOG,'ERROR IN ORG ID -> '||V_ERROR_FLAG);

        END ;

        IF V_ORG_CNT = 0 THEN
             V_ERROR_FLAG := 'Y';
             V_HDR_ERR_MSG := 'ORG IS NULL OR ORG NOT AVAILABLE';
              P_VENDOR_SITE_ID             := NULL;
              P_VENDOR_ID                  := NULL;
              P_PAYMENT_METHOD_LOOKUP_CODE := NULL;
           --  FND_FILE.PUT_LINE(FND_FILE.LOG,'ERROR OCCURED IN IF COND-> '||V_ERROR_FLAG);
         END IF;


    --    FND_FILE.PUT_LINE(FND_FILE.LOG,'ERROR FLAG -> '||V_ERROR_FLAG);


        IF REC_INV_HDR.VENDOR_NUM IS NULL THEN

            DBMS_OUTPUT.PUT_LINE('NO SUPPLIER FOR INVOICE NUMBER');

        END IF;


/*    10.04.2012    SUPPLIER SITE VALIDATION BEING CARRIED OUT ONLY FOR RETEK DATA        */

        IF    UPPER(REC_INV_HDR.SOURCE) =  'RETEK'        THEN

            FND_FILE.PUT_LINE(FND_FILE.LOG,'ORG ID PARAMETER --> CUR_INV_HDR --> '||REC_INV_HDR.ORG_ID);

                SELECT COUNT(*)
                  INTO V_FLAG_COUNT
                  FROM AP_SUPPLIERS ASUP, AP_SUPPLIER_SITES_ALL ASUPS
                 WHERE ASUP.VENDOR_ID = ASUPS.VENDOR_ID
                   AND ASUP.SEGMENT1 = REC_INV_HDR.VENDOR_NUM
                   -- FOLLOWING CONDITION ADDED BY SHAILESH ON  27 APR 2009 COUNT WILL COME ACCORDING TO ORG_ID
                   AND ASUPS.ORG_ID=REC_INV_HDR.ORG_ID
                   --AND ASUPS.INACTIVE_DATE IS NULL
                  AND NVL(TRUNC(ASUPS.INACTIVE_DATE), TRUNC(SYSDATE)) >= TRUNC(SYSDATE)
                   AND ((NVL(ASUPS.PAY_SITE_FLAG, 'N') = 'N' AND
                       NVL(ASUPS.PURCHASING_SITE_FLAG, 'N') = 'N' AND
                       NVL(ASUPS.RFQ_ONLY_SITE_FLAG, 'N') = 'Y') OR
                       (NVL(ASUPS.PAY_SITE_FLAG, 'N') = 'N' AND
                       NVL(ASUPS.PURCHASING_SITE_FLAG, 'N') = 'Y' AND
                       NVL(ASUPS.RFQ_ONLY_SITE_FLAG, 'N') = 'N') OR
                       (NVL(ASUPS.PAY_SITE_FLAG, 'N') = 'Y' AND
                       NVL(ASUPS.PURCHASING_SITE_FLAG, 'N') = 'N' AND
                       NVL(ASUPS.RFQ_ONLY_SITE_FLAG, 'N') = 'N') OR
                       (NVL(ASUPS.PAY_SITE_FLAG, 'N') = 'N' AND
                       NVL(ASUPS.PURCHASING_SITE_FLAG, 'N') = 'N' AND
                       NVL(ASUPS.RFQ_ONLY_SITE_FLAG, 'N') = 'N'));

                SELECT COUNT(*)
                  INTO V_PAY_COUNT
                  FROM AP_SUPPLIERS ASUP, AP_SUPPLIER_SITES_ALL ASUPS
                 WHERE ASUP.VENDOR_ID = ASUPS.VENDOR_ID
                   AND ASUP.SEGMENT1 = REC_INV_HDR.VENDOR_NUM
                    -- FOLLOWING CONDITION ADDED BY SHAILESH ON  27 APR 2009 COUNT WILL COME ACCORDING TO ORG_ID
                   AND ASUPS.ORG_ID=REC_INV_HDR.ORG_ID
                   AND NVL(TRUNC(ASUPS.INACTIVE_DATE), TRUNC(SYSDATE)) >= TRUNC(SYSDATE)
                   AND NVL(ASUPS.PAY_SITE_FLAG, 'N') = 'Y';

                SELECT COUNT(*)
                  INTO V_RFQ_COUNT
                  FROM AP_SUPPLIERS ASUP, AP_SUPPLIER_SITES_ALL ASUPS
                 WHERE ASUP.VENDOR_ID = ASUPS.VENDOR_ID
                  -- FOLLOWING CONDITION ADDED BY SHAILESH ON  27 APR 2009 COUNT WILL COME ACCORDING TO ORG_ID
                   AND ASUPS.ORG_ID=REC_INV_HDR.ORG_ID
                   AND NVL(TRUNC(ASUPS.INACTIVE_DATE), TRUNC(SYSDATE)) >= TRUNC(SYSDATE)
                   AND ASUP.SEGMENT1 = REC_INV_HDR.VENDOR_NUM
                   AND NVL(ASUPS.RFQ_ONLY_SITE_FLAG, 'N') = 'Y';

                SELECT COUNT(*)
                  INTO V_PUR_COUNT
                  FROM AP_SUPPLIERS ASUP, AP_SUPPLIER_SITES_ALL ASUPS
                 WHERE ASUP.VENDOR_ID = ASUPS.VENDOR_ID
                  -- FOLLOWING CONDITION ADDED BY SHAILESH ON  27 APR 2009 COUNT WILL COME ACCORDING TO ORG_ID
                   AND ASUPS.ORG_ID=REC_INV_HDR.ORG_ID
                   AND NVL(TRUNC(ASUPS.INACTIVE_DATE), TRUNC(SYSDATE)) >= TRUNC(SYSDATE)
                   AND ASUP.SEGMENT1 = REC_INV_HDR.VENDOR_NUM
                   AND NVL(ASUPS.PURCHASING_SITE_FLAG, 'N') = 'Y';

                SELECT COUNT(*)
                  INTO V_HO_COUNT
                  FROM AP_SUPPLIERS ASUP, AP_SUPPLIER_SITES_ALL ASUPS
                 WHERE ASUP.VENDOR_ID = ASUPS.VENDOR_ID
                  -- FOLLOWING CONDITION ADDED BY SHAILESH ON  27 APR 2009 COUNT WILL COME ACCORDING TO ORG_ID
                   AND ASUPS.ORG_ID=REC_INV_HDR.ORG_ID
                   AND ASUP.SEGMENT1 = REC_INV_HDR.VENDOR_NUM
                   AND NVL(TRUNC(ASUPS.INACTIVE_DATE), TRUNC(SYSDATE)) >= TRUNC(SYSDATE)
                   AND NVL(ASUPS.PAY_SITE_FLAG, 'N') = 'N'
                   AND NVL(ASUPS.RFQ_ONLY_SITE_FLAG, 'N') = 'N'
                   AND NVL(ASUPS.PURCHASING_SITE_FLAG, 'N') = 'N';

        END IF;

                   /*    IF    UPPER(REC_INV_HDR.SOURCE) =  'RETEK'        THEN        */
    

      /*======================== CHECK FOR SUPPLIERS STATUS====================================================*/

      IF REC_INV_HDR.VENDOR_NUM IS NOT NULL THEN

        BEGIN

          SELECT 'Y'
            INTO V_SUP_STATUS
            FROM AP_SUPPLIERS
           WHERE SEGMENT1 = REC_INV_HDR.VENDOR_NUM
             AND SYSDATE BETWEEN NVL(START_DATE_ACTIVE, SYSDATE) AND
                 NVL(END_DATE_ACTIVE, SYSDATE); -- ADDED BY LALIT DT 27-JUN-2008


        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            V_ERROR_FLAG  := 'Y';
            V_HDR_ERR_MSG := 'INVALID SUPPLIER';
            FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                              RPAD(NVL(REC_INV_HDR.VENDOR_NUM, ' '), 20) ||
                              RPAD(REC_INV_HDR.INVOICE_NUM, 20) ||
                              V_HDR_ERR_MSG);
        END;

      END IF;


        IF    UPPER(REC_INV_HDR.SOURCE) =  'RETEK'        THEN

              IF V_SUP_STATUS = 'Y' THEN

                FOR REC_CHECK_FLAGS IN CUR_CHECK_FLAGS(REC_INV_HDR.VENDOR_NUM,REC_INV_HDR.ORG_ID) 
                LOOP
                  EXIT WHEN CUR_CHECK_FLAGS%NOTFOUND;

                  --- ADDED BY GOVIND ON 24 JUNE 2009 VENDOR SITE ID VALIDATION.
                  BEGIN
                       IF  REC_CHECK_FLAGS.VENDOR_SITE_ID IS NULL THEN
                       V_ERROR_FLAG  := 'Y';

                       FND_FILE.PUT_LINE(FND_FILE.LOG,'ORG IS NULL OR ORG NOT AVAILABLE ');
                       FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'VENDOR SITE IS NOT AVAILABLE-> '||V_ERROR_FLAG);
                       P_VENDOR_SITE_ID             := NULL;
                       END IF;
                  END;

                  IF CUR_CHECK_FLAGS%NOTFOUND THEN
                    V_ERROR_FLAG  := 'Y';
                    V_HDR_ERR_MSG := 'SUPPLIER IS NOT AVIALABLE';
                    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                                      RPAD(NVL(REC_INV_HDR.VENDOR_NUM, ' '), 20) ||
                                      RPAD(REC_INV_HDR.INVOICE_NUM, 20) ||
                                      V_HDR_ERR_MSG);
                    CLOSE CUR_CHECK_FLAGS;
                 ELSE
                 
                    IF V_PAY_COUNT > 1 THEN
                      V_ERROR_FLAG  := 'Y';
                      V_HDR_ERR_MSG := 'MULTIPLE PAY SITE DEFINED';
                      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                                        RPAD(NVL(REC_INV_HDR.VENDOR_NUM, ' '), 20) ||
                                        RPAD(REC_INV_HDR.INVOICE_NUM, 20) ||
                                        V_HDR_ERR_MSG);
                      EXIT;
                    END IF;

                    IF V_PAY_COUNT = 1 AND V_RFQ_COUNT = 1 AND V_PUR_COUNT = 1 AND V_HO_COUNT = 1 THEN

                              IF V_FLAG_COUNT = 4 THEN

                                IF NVL(REC_CHECK_FLAGS.PAY_SITE_FLAG, 'N') = 'Y' THEN
                ---*********************************************************************************
                                  P_ORG_ID                     := REC_CHECK_FLAGS.ORG_ID;
                                  P_VENDOR_SITE_ID             := REC_CHECK_FLAGS.VENDOR_SITE_ID;
                                  P_VENDOR_ID                  := REC_CHECK_FLAGS.VENDOR_ID;
                                  P_PAYMENT_METHOD_LOOKUP_CODE := REC_CHECK_FLAGS.PAYMENT_METHOD_LOOKUP_CODE;



                             --- REIMED BY SHAILESH ON 24 APR 2009 REMOVED THE CONDITION FROM IT AS ITS DIRECTLY COMMING FROM FILE

                               --   UPDATE APPS.XXABRL_AP_HEADER_STAGE
                               --   SET ORG_ID = REC_CHECK_FLAGS.ORG_ID
                               --   WHERE ROWID = REC_INV_HDR.ROWID;

                                END IF;

                              ELSE

                                V_ERROR_FLAG  := 'Y';
                                V_HDR_ERR_MSG := 'FOUR SUPPLIER SITES NOT DEFINED IN APPS';
                                FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                                                  RPAD(NVL(REC_INV_HDR.VENDOR_NUM, ' '), 20) ||
                                                  RPAD(REC_INV_HDR.INVOICE_NUM, 20) ||
                                                  V_HDR_ERR_MSG);
                                EXIT;
                              END IF;
                        
                        ELSE
                          
                          IF V_PAY_COUNT = 0 THEN
                            V_ERROR_FLAG  := 'Y';
                            V_HDR_ERR_MSG := 'NO PAY SITE DEFINED FOR THE VENDOR';
                            FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                                              RPAD(NVL(REC_INV_HDR.VENDOR_NUM, ' '), 20) ||
                                              RPAD(REC_INV_HDR.INVOICE_NUM, 20) ||
                                              V_HDR_ERR_MSG);
                            EXIT;
                          END IF;
                          
                          IF V_RFQ_COUNT = 0 THEN
                            V_ERROR_FLAG  := 'Y';
                            V_HDR_ERR_MSG := 'NO RETURN SITE DEFINED FOR THE VENDOR';
                            FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                                              RPAD(NVL(REC_INV_HDR.VENDOR_NUM, ' '), 20) ||
                                              RPAD(REC_INV_HDR.INVOICE_NUM, 20) ||
                                              V_HDR_ERR_MSG);
                            EXIT;
                          END IF;
                         
                         IF V_PUR_COUNT = 0 THEN
                            V_ERROR_FLAG  := 'Y';
                            V_HDR_ERR_MSG := 'NO PURCHASE SITE DEFINED FOR THE VENDOR';
                            FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                                              RPAD(NVL(REC_INV_HDR.VENDOR_NUM, ' '), 20) ||
                                              RPAD(REC_INV_HDR.INVOICE_NUM, 20) ||
                                              V_HDR_ERR_MSG);
                            EXIT;
                          END IF;
                    
                        IF V_HO_COUNT = 0 THEN
                        
                            V_ERROR_FLAG  := 'Y';
                            V_HDR_ERR_MSG := 'NO HO SITE DEFINED FOR THE VENDOR';
                            FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                                              RPAD(NVL(REC_INV_HDR.VENDOR_NUM, ' '), 20) ||
                                              RPAD(REC_INV_HDR.INVOICE_NUM, 20) ||
                                              V_HDR_ERR_MSG);
                            EXIT;
                          END IF;
                          V_ERROR_FLAG  := 'Y';
                          V_HDR_ERR_MSG := 'SUPPLIER SITES SETUP IN APPS NOT AS PER VENDOR OUTBOUND DEFINATION';
                          FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                                            RPAD(NVL(REC_INV_HDR.VENDOR_NUM, ' '), 20) ||
                                            RPAD(REC_INV_HDR.INVOICE_NUM, 20) ||
                                            V_HDR_ERR_MSG);
                          EXIT;
                
                    END IF;

                  END IF;

                END LOOP;

              END IF;

                    ELSE
                    
                        P_ORG_ID                                         := REC_INV_HDR.ORG_ID;
                        P_VENDOR_SITE_ID                             := REC_INV_HDR.VENDOR_SITE_ID;
                        P_VENDOR_ID                                      := REC_INV_HDR.VENDOR_ID;
                        P_PAYMENT_METHOD_LOOKUP_CODE     := REC_INV_HDR.PAYMENT_METHOD_LOOKUP_CODE;

    
        END IF;            /*    IF    UPPER(REC_INV_HDR.SOURCE) =  'RETEK'        THEN    */
    
    END LOOP;



    /*        HELLO2        */


    -- ERROR REPORT HEADER
    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                      '                             AP INVOICE INTERFACE VALIDATION REPORT');
    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                      'RUN DATE      : ' ||
                      TO_CHAR(TRUNC(SYSDATE), 'DD-MON-RRRR'));
    FND_FILE.PUT_LINE(FND_FILE.OUTPUT, CHR(10));
    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                      '-------------------------------------------------------------------------------------------------');
    -- FND_FILE.PUT_LINE (FND_FILE.OUTPUT,CHR(10));
    --FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'FILE DATE     : '||TO_CHAR(C_FILE_REC.FILE_DATE,'DD-MON-RRRR:HH24:MI:SS'));
    --  FND_FILE.PUT_LINE (FND_FILE.OUTPUT,CHR(10));
    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                      RPAD('VENDOR NO', 20) || RPAD('INVOICE NO', 20) ||
                      RPAD('ERROR MESSAGE', 400));

    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                      'SET OF BOOK ID  ->  ' || V_SET_OF_BOOKS_ID);

        SELECT COUNT(INVOICE_NUM)
          INTO V_TOTAL_RECORDS
          FROM APPS.XXABRL_AP_HEADER_STAGE
         WHERE ERROR_FLAG IN ('N', 'E');

    --GET FUNCTIONAL CURRENCY
        BEGIN
          SELECT CURRENCY_CODE
            INTO V_FUN_CURR
            FROM GL_SETS_OF_BOOKS
           WHERE SET_OF_BOOKS_ID = V_SET_OF_BOOKS_ID;

        EXCEPTION
          WHEN OTHERS THEN
            V_FUN_CURR := NULL;
        END;

/*        HELLO 1        */

        FOR REC_INV_HDR IN CUR_INV_HDR 
        LOOP
          EXIT WHEN CUR_INV_HDR%NOTFOUND;
          V_ERROR_FLAG                 := NULL;
          V_HDR_ERR_MSG                := NULL;
          V_VENDOR_ID                  := NULL;
          V_VENDOR_SITE_ID             := NULL;
          V_INVOICE_CURRENCY_CODE      := NULL;
          V_PAYMENT_METHOD_LOOKUP_CODE := NULL;
          -- REC_INV_HDR.VENDOR_NUM                        := NULL;
          V_CHECK_FLAG      := 'N';
          V_PAY_LOOKUP_CODE := NULL;
          --V_ERROR_HEAD_COUNT                   := 0;
          V_INV_LINE_AMT := 0;
          V_INV_TYPE     := NULL;
          --V_SUCCESS_RECORD_COUNT                := 0;
          V_SUP_STATUS := 'N';

          --    P_ORG_ID                               := NULL;
          --    P_VENDOR_SITE_ID                       := NULL;
          --    P_VENDOR_ID                            := NULL;
          --    P_PAYMENT_METHOD_LOOKUP_CODE           := NULL;
          --    V_PAY_SITE_FLAG                        := NULL;
          V_FLAG_COUNT := 0;
          V_PAY_COUNT  := 0;
          V_RFQ_COUNT  := 0;
          V_PUR_COUNT  := 0;
          V_HO_COUNT   := 0;

          BEGIN

            -- ORG ID VALIDATION ADDED BY SHAILESH ON 22 JUNE 2009

            FND_FILE.PUT_LINE(FND_FILE.LOG,'VALIDATION FOR ORG ID -> '||REC_INV_HDR.ORG_ID);

          SELECT COUNT(ORGANIZATION_ID)
          INTO V_ORG_CNT
          FROM APPS.HR_OPERATING_UNITS HOU
          WHERE HOU.ORGANIZATION_ID=REC_INV_HDR.ORG_ID;

          EXCEPTION
          WHEN NO_DATA_FOUND THEN
           V_ERROR_FLAG  := 'Y';
             V_HDR_ERR_MSG := 'ORG IS NULL OR ORG NOT AVAILABLE';
           FND_FILE.PUT_LINE(FND_FILE.LOG,'ORG IS NULL OR ORG NOT AVAILABLE-> '||V_ERROR_FLAG);
          WHEN OTHERS THEN
           V_ERROR_FLAG  := 'Y';
             V_HDR_ERR_MSG := 'ORG IS NULL OR ORG NOT AVAILABLE';
           FND_FILE.PUT_LINE(FND_FILE.LOG,'ERROR IN ORG ID -> '||V_ERROR_FLAG);

          END ;


         IF V_ORG_CNT = 0 THEN
            V_ERROR_FLAG := 'Y';
            V_HDR_ERR_MSG := 'ORG IS NULL OR ORG NOT AVAILABLE';
            P_VENDOR_SITE_ID             := NULL;
          P_VENDOR_ID                  := NULL;
          P_PAYMENT_METHOD_LOOKUP_CODE := NULL;
            FND_FILE.PUT_LINE(FND_FILE.LOG,'INVALID ORG ID -> '||V_ERROR_FLAG);
         END IF;


    /*    10.04.2012    SUPPLIER SITE VALIDATION BEING CARRIED OUT ONLY FOR RETEK DATA        */

            IF    UPPER(REC_INV_HDR.SOURCE) =  'RETEK'        THEN


                  IF REC_INV_HDR.VENDOR_NUM IS NULL THEN

                    DBMS_OUTPUT.PUT_LINE('NO SUPPLIER FOR INVOICE NUMBER');

                  ELSE

                  FND_FILE.PUT_LINE(FND_FILE.LOG,'ORG ID PARAMETER --> CUR_INV_HDR 2ND FOR LOOP --> '||REC_INV_HDR.ORG_ID);

                    SELECT COUNT(*)
                      INTO V_FLAG_COUNT
                      FROM AP_SUPPLIERS ASUP, AP_SUPPLIER_SITES_ALL ASUPS
                     WHERE ASUP.VENDOR_ID = ASUPS.VENDOR_ID
                       AND ASUP.SEGMENT1 =REC_INV_HDR.VENDOR_NUM
                         -- FOLLOWING CONDITION ADDED BY SHAILESH ON  27 APR 2009 COUNT WILL COME ACCORDING TO ORG_ID
                       AND ASUPS.ORG_ID=REC_INV_HDR.ORG_ID
                           AND NVL(TRUNC(ASUPS.INACTIVE_DATE), TRUNC(SYSDATE)) >= TRUNC(SYSDATE)
                       AND ((NVL(ASUPS.PAY_SITE_FLAG, 'N') = 'N' AND
                           NVL(ASUPS.PURCHASING_SITE_FLAG, 'N') = 'N' AND
                           NVL(ASUPS.RFQ_ONLY_SITE_FLAG, 'N') = 'Y') OR
                           (NVL(ASUPS.PAY_SITE_FLAG, 'N') = 'N' AND
                           NVL(ASUPS.PURCHASING_SITE_FLAG, 'N') = 'Y' AND
                           NVL(ASUPS.RFQ_ONLY_SITE_FLAG, 'N') = 'N') OR
                           (NVL(ASUPS.PAY_SITE_FLAG, 'N') = 'Y' AND
                           NVL(ASUPS.PURCHASING_SITE_FLAG, 'N') = 'N' AND
                           NVL(ASUPS.RFQ_ONLY_SITE_FLAG, 'N') = 'N') OR
                           (NVL(ASUPS.PAY_SITE_FLAG, 'N') = 'N' AND
                           NVL(ASUPS.PURCHASING_SITE_FLAG, 'N') = 'N' AND
                           NVL(ASUPS.RFQ_ONLY_SITE_FLAG, 'N') = 'N'));


                    SELECT COUNT(*)
                      INTO V_PAY_COUNT
                      FROM AP_SUPPLIERS ASUP, AP_SUPPLIER_SITES_ALL ASUPS
                     WHERE ASUP.VENDOR_ID = ASUPS.VENDOR_ID
                       AND ASUP.SEGMENT1 = REC_INV_HDR.VENDOR_NUM
                        -- FOLLOWING CONDITION ADDED BY SHAILESH ON  27 APR 2009 COUNT WILL COME ACCORDING TO ORG_ID
                       AND ASUPS.ORG_ID=REC_INV_HDR.ORG_ID
                       AND NVL(TRUNC(ASUPS.INACTIVE_DATE), TRUNC(SYSDATE)) >= TRUNC(SYSDATE)
                       AND NVL(ASUPS.PAY_SITE_FLAG, 'N') = 'Y';


                    SELECT COUNT(*)
                      INTO V_RFQ_COUNT
                      FROM AP_SUPPLIERS ASUP, AP_SUPPLIER_SITES_ALL ASUPS
                     WHERE ASUP.VENDOR_ID = ASUPS.VENDOR_ID
                       AND ASUP.SEGMENT1 = REC_INV_HDR.VENDOR_NUM
                        -- FOLLOWING CONDITION ADDED BY SHAILESH ON  27 APR 2009 COUNT WILL COME ACCORDING TO ORG_ID
                       AND ASUPS.ORG_ID=REC_INV_HDR.ORG_ID
                       AND NVL(TRUNC(ASUPS.INACTIVE_DATE), TRUNC(SYSDATE)) >= TRUNC(SYSDATE)
                       AND NVL(ASUPS.RFQ_ONLY_SITE_FLAG, 'N') = 'Y';


                    SELECT COUNT(*)
                      INTO V_PUR_COUNT
                      FROM AP_SUPPLIERS ASUP, AP_SUPPLIER_SITES_ALL ASUPS
                     WHERE ASUP.VENDOR_ID = ASUPS.VENDOR_ID
                       AND ASUP.SEGMENT1 = REC_INV_HDR.VENDOR_NUM
                        -- FOLLOWING CONDITION ADDED BY SHAILESH ON  27 APR 2009 COUNT WILL COME ACCORDING TO ORG_ID
                       AND ASUPS.ORG_ID=REC_INV_HDR.ORG_ID
                       AND NVL(TRUNC(ASUPS.INACTIVE_DATE), TRUNC(SYSDATE)) >= TRUNC(SYSDATE)
                       AND NVL(ASUPS.PURCHASING_SITE_FLAG, 'N') = 'Y';


                    SELECT COUNT(*)
                      INTO V_HO_COUNT
                      FROM AP_SUPPLIERS ASUP, AP_SUPPLIER_SITES_ALL ASUPS
                     WHERE ASUP.VENDOR_ID = ASUPS.VENDOR_ID
                       AND ASUP.SEGMENT1 = REC_INV_HDR.VENDOR_NUM
                        -- FOLLOWING CONDITION ADDED BY SHAILESH ON  27 APR 2009 COUNT WILL COME ACCORDING TO ORG_ID
                       AND ASUPS.ORG_ID=REC_INV_HDR.ORG_ID
                       AND NVL(TRUNC(ASUPS.INACTIVE_DATE), TRUNC(SYSDATE)) >= TRUNC(SYSDATE)
                       AND NVL(ASUPS.PAY_SITE_FLAG, 'N') = 'N'
                       AND NVL(ASUPS.RFQ_ONLY_SITE_FLAG, 'N') = 'N'
                       AND NVL(ASUPS.PURCHASING_SITE_FLAG, 'N') = 'N';

                  END IF;

            END IF;         /*    IF    UPPER(REC_INV_HDR.SOURCE) =  'RETEK'        THEN        */


     /*======================== CHECK FOR SUPPLIERS STATUS====================================================*/


              IF REC_INV_HDR.VENDOR_NUM IS NOT NULL THEN

                BEGIN
                  SELECT 'Y'
                    INTO V_SUP_STATUS
                    FROM AP_SUPPLIERS
                   WHERE SEGMENT1 = REC_INV_HDR.VENDOR_NUM
                     AND SYSDATE BETWEEN NVL(START_DATE_ACTIVE, SYSDATE) AND
                         NVL(END_DATE_ACTIVE, SYSDATE); -- ADDED BY LALIT DT 27-JUN-2008
                EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                    V_ERROR_FLAG  := 'Y';
                    V_HDR_ERR_MSG := 'INVALID SUPPLIER';
                    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                                      RPAD(NVL(REC_INV_HDR.VENDOR_NUM, ' '), 20) ||
                                      RPAD(REC_INV_HDR.INVOICE_NUM, 20) ||
                                      V_HDR_ERR_MSG);
                END;

              END IF;



            IF  UPPER(REC_INV_HDR.SOURCE) =  'RETEK'        THEN


                  IF V_SUP_STATUS = 'Y' THEN

                    FOR REC_CHECK_FLAGS IN CUR_CHECK_FLAGS(REC_INV_HDR.VENDOR_NUM,REC_INV_HDR.ORG_ID) 
                    LOOP
                    EXIT WHEN CUR_CHECK_FLAGS%NOTFOUND;

                        --- ADDED BY GOVIND ON 24 JUNE 3009 VENDOR SITE VALIDATION

                      BEGIN
                           IF  REC_CHECK_FLAGS.VENDOR_SITE_ID IS NULL THEN
                           V_ERROR_FLAG  := 'Y';
                           FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'VENDOR SITE IS NOT AVAILABLE-> '||V_ERROR_FLAG);
                           P_VENDOR_SITE_ID             := NULL;
                           END IF;
                      END;


                      IF CUR_CHECK_FLAGS%NOTFOUND THEN
                        V_ERROR_FLAG  := 'Y';
                        V_HDR_ERR_MSG := 'SUPPLIER IS NOT AVIALABLE';
                        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                                          RPAD(NVL(REC_INV_HDR.VENDOR_NUM, ' '), 20) ||
                                          RPAD(REC_INV_HDR.INVOICE_NUM, 20) ||
                                          V_HDR_ERR_MSG);
                        CLOSE CUR_CHECK_FLAGS;
                      ELSE
                            IF V_PAY_COUNT > 1 THEN
                              V_ERROR_FLAG  := 'Y';
                              V_HDR_ERR_MSG := 'MULTIPLE PAY SITE DEFINED';
                              FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                                                RPAD(NVL(REC_INV_HDR.VENDOR_NUM, ' '), 20) ||
                                                RPAD(REC_INV_HDR.INVOICE_NUM, 20) ||
                                                V_HDR_ERR_MSG);
                              EXIT;
                            END IF;
                            
                            IF V_PAY_COUNT = 1 AND V_RFQ_COUNT = 1 AND V_PUR_COUNT = 1 AND
                               V_HO_COUNT = 1 THEN
                              IF V_FLAG_COUNT = 4 THEN
                                IF NVL(REC_CHECK_FLAGS.PAY_SITE_FLAG, 'N') = 'Y' THEN
                      ---********************************************************************************************
                                  P_ORG_ID                     := REC_CHECK_FLAGS.ORG_ID;
                                  P_VENDOR_SITE_ID             := REC_CHECK_FLAGS.VENDOR_SITE_ID;
                                  P_VENDOR_ID                  := REC_CHECK_FLAGS.VENDOR_ID;
                                  P_PAYMENT_METHOD_LOOKUP_CODE := REC_CHECK_FLAGS.PAYMENT_METHOD_LOOKUP_CODE;

                                END IF;
                              ELSE
                                V_ERROR_FLAG  := 'Y';
                                V_HDR_ERR_MSG := 'FOUR SUPPLIER SITES NOT DEFINED IN APPS';
                                FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                                                  RPAD(NVL(REC_INV_HDR.VENDOR_NUM, ' '), 20) ||
                                                  RPAD(REC_INV_HDR.INVOICE_NUM, 20) ||
                                                  V_HDR_ERR_MSG);
                                EXIT;
                              END IF;
                            ELSE
                              IF V_PAY_COUNT = 0 THEN
                                V_ERROR_FLAG  := 'Y';
                                V_HDR_ERR_MSG := 'NO PAY SITE DEFINED FOR THE VENDOR';
                                FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                                                  RPAD(NVL(REC_INV_HDR.VENDOR_NUM, ' '), 20) ||
                                                  RPAD(REC_INV_HDR.INVOICE_NUM, 20) ||
                                                  V_HDR_ERR_MSG);
                                EXIT;
                              END IF;
                              
                              IF V_RFQ_COUNT = 0 THEN
                                V_ERROR_FLAG  := 'Y';
                                V_HDR_ERR_MSG := 'NO RETURN SITE DEFINED FOR THE VENDOR';
                                FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                                                  RPAD(NVL(REC_INV_HDR.VENDOR_NUM, ' '), 20) ||
                                                  RPAD(REC_INV_HDR.INVOICE_NUM, 20) ||
                                                  V_HDR_ERR_MSG);
                                EXIT;
                              END IF;
                              
                              IF V_PUR_COUNT = 0 THEN
                                V_ERROR_FLAG  := 'Y';
                                V_HDR_ERR_MSG := 'NO PURCHASE SITE DEFINED FOR THE VENDOR';
                                FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                                                  RPAD(NVL(REC_INV_HDR.VENDOR_NUM, ' '), 20) ||
                                                  RPAD(REC_INV_HDR.INVOICE_NUM, 20) ||
                                                  V_HDR_ERR_MSG);
                                EXIT;
                              END IF;
                              
                              IF V_HO_COUNT = 0 THEN
                                V_ERROR_FLAG  := 'Y';
                                V_HDR_ERR_MSG := 'NO HO SITE DEFINED FOR THE VENDOR';
                                FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                                                  RPAD(NVL(REC_INV_HDR.VENDOR_NUM, ' '), 20) ||
                                                  RPAD(REC_INV_HDR.INVOICE_NUM, 20) ||
                                                  V_HDR_ERR_MSG);
                                EXIT;
                              END IF;
                              V_ERROR_FLAG  := 'Y';
                              V_HDR_ERR_MSG := 'SUPPLIER SITES SETUP IN APPS NOT AS PER VENDOR OUTBOUND DEFINATION';
                              FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                                                RPAD(NVL(REC_INV_HDR.VENDOR_NUM, ' '), 20) ||
                                                RPAD(REC_INV_HDR.INVOICE_NUM, 20) ||
                                                V_HDR_ERR_MSG);
                              EXIT;
                            END IF;
                      END IF;
                    END LOOP;

                    --            IF P_ORG_ID IS NOT NULL THEN
                    --               SELECT  SET_OF_BOOKS_ID
                    --               INTO V_SET_OF_BOOKS_ID
                    --               FROM HR_OPERATING_UNITS
                    --               WHERE ORGANIZATION_ID=P_ORG_ID;
                    --             END IF;

                    /*======================== VALIDATION FOR INACTIVE SITE====================================================*/

            /*
                    IF P_VENDOR_SITE_ID IS NOT NULL AND P_VENDOR_ID IS NOT NULL THEN

                      V_INACTIVE_SITE := 0;
                      BEGIN

                        SELECT COUNT(*)
                          INTO V_INACTIVE_SITE
                          FROM AP_SUPPLIERS ASUP, AP_SUPPLIER_SITES_ALL ASUPS
                         WHERE ASUP.VENDOR_ID = ASUPS.VENDOR_ID
                           AND ASUPS.VENDOR_SITE_ID = P_VENDOR_SITE_ID
                           AND ASUP.VENDOR_ID = P_VENDOR_ID;
                         --  AND NVL(TRUNC(ASUPS.INACTIVE_DATE), TRUNC(SYSDATE)) <= TRUNC(SYSDATE);

                        IF V_INACTIVE_SITE > 0 THEN
                          V_ERROR_FLAG  := 'Y';
                          V_HDR_ERR_MSG := 'VENDOR SITE IS INACTIVE';
                          FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                                            RPAD(NVL(REC_INV_HDR.VENDOR_NUM, ' '), 20) ||
                                            RPAD(REC_INV_HDR.INVOICE_NUM, 20) ||
                                            V_HDR_ERR_MSG);
                        END IF;
                      EXCEPTION
                        WHEN OTHERS THEN
                          V_ERROR_FLAG  := 'Y';
                          V_HDR_ERR_MSG := 'VENDOR SITE IS INACTIVE';
                          FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                                            RPAD(NVL(REC_INV_HDR.VENDOR_NUM, ' '), 20) ||
                                            RPAD(REC_INV_HDR.INVOICE_NUM, 20) ||
                                            V_HDR_ERR_MSG);
                      END;

                    END IF;

             */

                -- ADDED BY SHAILESH ON 24 JUNE 2009 AS VENDOR SITE ID IS NOT AVAILABLE IN OFIN FOR SOME VENDOR.

                        IF P_VENDOR_SITE_ID IS NULL THEN

                            V_ERROR_FLAG  := 'Y';
                              V_HDR_ERR_MSG :=  V_HDR_ERR_MSG||'-'|| ' VENDOR SITE NOT FOUND FOR VENDOR '||REC_INV_HDR.VENDOR_NUM ||'  ';
                              FND_FILE.PUT_LINE(FND_FILE.LOG,'ORG IS NULL OR ORG NOT AVAILABLE-> '||V_ERROR_FLAG);
                              FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                                                RPAD(NVL(REC_INV_HDR.VENDOR_NUM, ' '), 20) ||
                                                RPAD(REC_INV_HDR.INVOICE_NUM, 20) ||
                                                V_HDR_ERR_MSG);
                        END IF;


                  END IF;                    /*       IF V_SUP_STATUS = 'Y' THEN    */


                    ELSE
                    
                        P_ORG_ID                                         := REC_INV_HDR.ORG_ID;
                        P_VENDOR_SITE_ID                             := REC_INV_HDR.VENDOR_SITE_ID;
                        P_VENDOR_ID                                      := REC_INV_HDR.VENDOR_ID;
                        P_PAYMENT_METHOD_LOOKUP_CODE     := REC_INV_HDR.PAYMENT_METHOD_LOOKUP_CODE;


            END IF;       /*            IF    UPPER(REC_INV_HDR.SOURCE) =  'RETEK'        THEN            */


    /*======================== VALIDATION FOR INVOICE TYPE LOOKUP CODE====================================================*/

              IF REC_INV_HDR.INVOICE_TYPE_LOOKUP_CODE IS NULL THEN
                V_HDR_ERR_MSG := V_HDR_ERR_MSG || 'INVOICE TYPE IS NULL';
                
              ELSE
              
                BEGIN
                  SELECT LOOKUP_CODE
                    INTO V_INV_TYPE
                    FROM AP_LOOKUP_CODES
                   WHERE UPPER(LOOKUP_CODE) =
                         UPPER(REC_INV_HDR.INVOICE_TYPE_LOOKUP_CODE)
                     AND LOOKUP_TYPE = 'INVOICE TYPE';
                EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                    V_ERROR_FLAG  := 'Y';
                    V_HDR_ERR_MSG := 'INVOICE TYPE DOES NOT EXISTS';
                    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                                      RPAD(NVL(REC_INV_HDR.VENDOR_NUM, ' '), 20) ||
                                      RPAD(REC_INV_HDR.INVOICE_NUM, 20) ||
                                      V_HDR_ERR_MSG);
                  WHEN TOO_MANY_ROWS THEN
                    V_ERROR_FLAG  := 'Y';
                    V_HDR_ERR_MSG := 'MULTIPLE INVOICE TYPE FOUND';
                  WHEN OTHERS THEN
                    V_ERROR_FLAG  := 'Y';
                    V_HDR_ERR_MSG := 'INVALID INVOICE TYPE';
                    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                                      RPAD(NVL(REC_INV_HDR.VENDOR_NUM, ' '), 20) ||
                                      RPAD(REC_INV_HDR.INVOICE_NUM, 20) ||
                                      V_HDR_ERR_MSG);
                END;
              END IF;

          /*============================CHECK INVOICE NUMBER ALREADY EXISTS AND INVOICE NUMBER DUPLICATION====================================*/

              IF REC_INV_HDR.INVOICE_NUM IS NOT NULL AND V_SUP_STATUS = 'Y' THEN
                BEGIN
                  V_DATA_COUNT := 0;
                  SELECT COUNT(INVOICE_NUM)
                    INTO V_DATA_COUNT
                    FROM AP_INVOICES_ALL
                   WHERE INVOICE_NUM = REC_INV_HDR.INVOICE_NUM
                     AND VENDOR_ID = P_VENDOR_ID
                     -- ADDED BY SHAILESH ON 20 JUNE 2009 AS ORG ID COMMING FROM CSV FILE TO STAGING TABLE.
                     --AND ORG_ID = P_ORG_ID;
                     AND ORG_ID = REC_INV_HDR.ORG_ID;

                  IF V_DATA_COUNT <> 0 THEN
                    V_ERROR_FLAG  := 'Y';
                    V_HDR_ERR_MSG := 'INVOICE NUMBER (' || ' ' ||
                                     REC_INV_HDR.INVOICE_NUM || ' ' ||
                                     ') ALREADY EXISTS FOR SUPPLIER' || ' ' ||
                                     REC_INV_HDR.VENDOR_NUM;
                    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                                      RPAD(NVL(REC_INV_HDR.VENDOR_NUM, ' '), 20) ||
                                      RPAD(REC_INV_HDR.INVOICE_NUM, 20) ||
                                      V_HDR_ERR_MSG);
                  END IF;
                  V_DATA_COUNT := 0;
                EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                    V_ERROR_FLAG  := 'Y';
                    V_HDR_ERR_MSG := 'INVOICE NUMBER DOES NOT EXISTS';
                    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                                      RPAD(NVL(REC_INV_HDR.VENDOR_NUM, ' '), 20) ||
                                      RPAD(REC_INV_HDR.INVOICE_NUM, 20) ||
                                      V_HDR_ERR_MSG);
                  WHEN OTHERS THEN
                    V_ERROR_FLAG  := 'Y';
                    V_HDR_ERR_MSG := SQLERRM;
                END;
                
                BEGIN
                  V_DATA_COUNT := 0;
                  SELECT COUNT(INVOICE_NUM)
                    INTO V_DATA_COUNT
                    FROM XXABRL_AP_HEADER_STAGE
                   WHERE INVOICE_NUM = REC_INV_HDR.INVOICE_NUM
                     AND VENDOR_NUM = REC_INV_HDR.VENDOR_NUM;

                  IF V_DATA_COUNT > 1 THEN
                    V_ERROR_FLAG  := 'Y';
                    V_HDR_ERR_MSG := 'DUPLICATE INVOICE NUMBER.THIS INVOICE ALREADY EXISTS FOR' ||
                                     CHR(10) || RPAD(' ', 20) || '' ||
                                     RPAD(' ', 20) || 'SUPPLIER ' || ' ' ||
                                     REC_INV_HDR.VENDOR_NUM || ' ' ||
                                     'AND SUPPLIER SITE' || ' ' ||
                                     V_VENDOR_SITE_CODE;
                    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                                      RPAD(NVL(REC_INV_HDR.VENDOR_NUM, ' '), 20) ||
                                      RPAD(REC_INV_HDR.INVOICE_NUM, 20) ||
                                      V_HDR_ERR_MSG);
                  END IF;
                  V_DATA_COUNT := 0;
                EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                    V_ERROR_FLAG  := 'Y';
                    V_HDR_ERR_MSG := 'INVOICE NUMBER DOES NOT EXISTS';
                    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                                      RPAD(NVL(REC_INV_HDR.VENDOR_NUM, ' '), 20) ||
                                      RPAD(REC_INV_HDR.INVOICE_NUM, 20) ||
                                      V_HDR_ERR_MSG);
                  WHEN OTHERS THEN
                    V_ERROR_FLAG  := 'Y';
                    V_HDR_ERR_MSG := V_HDR_ERR_MSG || ' ' || SQLERRM;
                END;
                --   ELSE
                --         V_ERROR_FLAG := 'Y';
                --         V_HDR_ERR_MSG := V_HDR_ERR_MSG ||' '|| 'INVOICE NUMBER SHOULD NOT BE NULL';
                --         FND_FILE.PUT_LINE (FND_FILE.OUTPUT,V_HDR_ERR_MSG);
              END IF;

          /*============================VALIDATE FOR INVOICE AMOUNT WITH TRANSACTION TYPE====================================*/

              IF UPPER(REC_INV_HDR.INVOICE_TYPE_LOOKUP_CODE) = 'STANDARD' AND
                 REC_INV_HDR.INVOICE_AMOUNT < 0 THEN
                V_ERROR_FLAG  := 'Y';
                V_HDR_ERR_MSG := 'INVOICE AMOUNT CAN NOT BE NEGATIVE FOR STANDARD INVOICE';
                FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                                  RPAD(NVL(REC_INV_HDR.VENDOR_NUM, ' '), 20) ||
                                  RPAD(REC_INV_HDR.INVOICE_NUM, 20) ||
                                  V_HDR_ERR_MSG);
              ELSIF UPPER(REC_INV_HDR.INVOICE_TYPE_LOOKUP_CODE) = 'CREDIT' AND
                    REC_INV_HDR.INVOICE_AMOUNT >= 0 THEN
                V_ERROR_FLAG  := 'Y';
                V_HDR_ERR_MSG := 'INVOICE AMOUNT CAN NOT BE POSITIVE FOR CREDIT MEMO';
                FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                                  RPAD(NVL(REC_INV_HDR.VENDOR_NUM, ' '), 20) ||
                                  RPAD(REC_INV_HDR.INVOICE_NUM, 20) ||
                                  V_HDR_ERR_MSG);
              END IF;

          /*============================VALIDATE THE CURRENCY_CODE IN INVOICE HEADER TABLE====================================*/

              IF REC_INV_HDR.INVOICE_CURRENCY_CODE IS NOT NULL THEN
                BEGIN
                  SELECT CURRENCY_CODE
                    INTO V_INVOICE_CURRENCY_CODE
                    FROM FND_CURRENCIES
                   WHERE UPPER(CURRENCY_CODE) =
                         TRIM(UPPER(REC_INV_HDR.INVOICE_CURRENCY_CODE));

                  IF UPPER(TRIM(V_INVOICE_CURRENCY_CODE)) <>
                     UPPER(TRIM(V_FUN_CURR)) AND REC_INV_HDR.EXCHANGE_DATE IS NULL THEN
                    V_ERROR_FLAG  := 'Y';
                    V_HDR_ERR_MSG := 'EXCHANGE DATE IS NULL';
                    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                                      RPAD(NVL(REC_INV_HDR.VENDOR_NUM, ' '), 20) ||
                                      RPAD(REC_INV_HDR.INVOICE_NUM, 20) ||
                                      V_HDR_ERR_MSG);
                  END IF;
                EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                    V_ERROR_FLAG  := 'Y';
                    V_HDR_ERR_MSG := REC_INV_HDR.INVOICE_CURRENCY_CODE || ' _ ' ||
                                     ' INVOICE_CURRENCY_CODE IS NOT VALID';
                    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                                      RPAD(NVL(REC_INV_HDR.VENDOR_NUM, ' '), 20) ||
                                      RPAD(REC_INV_HDR.INVOICE_NUM, 20) ||
                                      V_HDR_ERR_MSG);
                END;
                
              ELSE
              
                V_ERROR_FLAG  := 'Y';
                V_HDR_ERR_MSG := 'INVOICE CURRENCY CODE SHOULD NOT BE NULL';
                FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                                  RPAD(NVL(REC_INV_HDR.VENDOR_NUM, ' '), 20) ||
                                  RPAD(REC_INV_HDR.INVOICE_NUM, 20) ||
                                  V_HDR_ERR_MSG);
              END IF;

          /*============================VALIDATE THE PAYMENT_METHOD_LOOKUP_CODE IN INVOICE HEADER ==================================*/

          /*           IF REC_INV_HDR.PAYMENT_METHOD_LOOKUP_CODE IS NOT NULL THEN
             BEGIN
                 SELECT  LOOKUP_CODE
                INTO       V_PAY_LOOKUP_CODE
                FROM    AP_LOOKUP_CODES
                WHERE   LOOKUP_TYPE='PAYMENT METHOD'
                AND     UPPER(LOOKUP_CODE)=TRIM(UPPER(REC_INV_HDR.PAYMENT_METHOD_LOOKUP_CODE))
                AND       NVL(ENABLED_FLAG,'N')='Y'
                AND      SYSDATE BETWEEN NVL(START_DATE_ACTIVE,SYSDATE) AND NVL(INACTIVE_DATE,SYSDATE);
              EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                      V_ERROR_FLAG := 'Y';
                      V_HDR_ERR_MSG := REC_INV_HDR.PAYMENT_METHOD_LOOKUP_CODE||' _ '||'PAYMENT METHOD LOOKUP CODE DOES NOT EXISTS IN APPLICATION';
                   FND_FILE.PUT_LINE (FND_FILE.OUTPUT,V_HDR_ERR_MSG);
                WHEN TOO_MANY_ROWS THEN
                      V_ERROR_FLAG := 'Y';
                      V_HDR_ERR_MSG := ' MULTIPLE PAYMENT METHOD LOOKUP CODE FOUND';
                   FND_FILE.PUT_LINE (FND_FILE.OUTPUT,V_HDR_ERR_MSG);
                WHEN OTHERS THEN
                      V_ERROR_FLAG := 'Y';
                      V_HDR_ERR_MSG := ' INVALID PAYMENT METHOD LOOKUP CODE';
                   FND_FILE.PUT_LINE (FND_FILE.OUTPUT,V_HDR_ERR_MSG);
              END;
           END IF;
          */
          /*============================VALIDATE PAYMENT TERMS IN INVOICE HEADER ==================================*/
          /*
             IF REC_INV_HDR.TERMS_ID IS NOT NULL THEN
              BEGIN
                  SELECT  NAME
                 INTO       V_TERM_NAME
                 FROM    AP_TERMS_TL
                 WHERE   TERM_ID =REC_INV_HDR.TERMS_ID
                 AND       NVL(ENABLED_FLAG,'N')='Y'
                 AND      SYSDATE BETWEEN NVL(START_DATE_ACTIVE,SYSDATE) AND NVL(END_DATE_ACTIVE,SYSDATE);
               EXCEPTION
                   WHEN NO_DATA_FOUND THEN
                       V_ERROR_FLAG := 'Y';
                       V_HDR_ERR_MSG := 'TERM ID DOES NOT EXISTS IN APPLICATION';
                    FND_FILE.PUT_LINE (FND_FILE.OUTPUT,RPAD(NVL(REC_INV_HDR.VENDOR_NUM,' '), 20)
                                                       || RPAD(REC_INV_HDR.INVOICE_NUM, 20)
                                                       || V_HDR_ERR_MSG);
                 WHEN TOO_MANY_ROWS THEN
                       V_ERROR_FLAG := 'Y';
                       V_HDR_ERR_MSG := 'MULTIPLE TERM IDS FOUND';
                    FND_FILE.PUT_LINE (FND_FILE.OUTPUT,RPAD(NVL(REC_INV_HDR.VENDOR_NUM,' '), 20)
                                                       || RPAD(REC_INV_HDR.INVOICE_NUM, 20)
                                                       || V_HDR_ERR_MSG);
                 WHEN OTHERS THEN
                       V_ERROR_FLAG := 'Y';
                       V_HDR_ERR_MSG := ' INVALID TERM ID';
                    FND_FILE.PUT_LINE (FND_FILE.OUTPUT,RPAD(NVL(REC_INV_HDR.VENDOR_NUM,' '), 20)
                                                       || RPAD(REC_INV_HDR.INVOICE_NUM, 20)
                                                       || V_HDR_ERR_MSG);
               END;
            END IF;
          */
          /*============================VALIDATE PAYMENT TERMS IN INVOICE HEADER ==================================*/
          --- ABOVE CODE COMMENTED BY SHAILESH ON 25 OCT 2008 AND WRITTEN FOLLOWING NEW VALIDATION FOR TERMS ID
          --- TERMS_ID IS TAKEN FROM VENDOR MASTER

          BEGIN

            SELECT NAME, APT.TERM_ID
              INTO V_TERM_NAME, L_TERMS_ID
              FROM AP_TERMS_TL APT, PO_VENDOR_SITES_ALL PVSA
             WHERE PVSA.TERMS_ID = APT.TERM_ID
               AND PVSA.VENDOR_ID = P_VENDOR_ID
               AND PVSA.VENDOR_SITE_ID = P_VENDOR_SITE_ID
               AND NVL(APT.ENABLED_FLAG, 'N') = 'Y'
               AND SYSDATE BETWEEN NVL(APT.START_DATE_ACTIVE, SYSDATE) AND
                   NVL(APT.END_DATE_ACTIVE, SYSDATE);

          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              L_TERMS_ID := NULL;

            WHEN TOO_MANY_ROWS THEN
              V_ERROR_FLAG  := 'Y';
              V_HDR_ERR_MSG := 'MULTIPLE TERM IDS FOUND';
              FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                                RPAD(NVL(REC_INV_HDR.VENDOR_NUM, ' '), 20) ||
                                RPAD(REC_INV_HDR.INVOICE_NUM, 20) ||
                                V_HDR_ERR_MSG);
            WHEN OTHERS THEN
              V_ERROR_FLAG  := 'Y';
              V_HDR_ERR_MSG := ' TERM ID';
              FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                                RPAD(NVL(REC_INV_HDR.VENDOR_NUM, ' '), 20) ||
                                RPAD(REC_INV_HDR.INVOICE_NUM, 20) ||
                                V_HDR_ERR_MSG);
          END;

          --   APPS.XXABRL_AP_HEADER_STAGE  L_DIST_CODE_CONCATENATED

          /*============================VALIDATION FOR  ACCTS_PAY_CODE_CONCATENATED ==================================*/

          -- NEW VALIDATION FOR THE DIST CODE CONCATENATED  ADDED BY SHAILESH ON 25 OCT 2008
          -- DIST CODE CONCATENATED SEGMENTS - DEFAULT ACCOUNTING SEGMENTS FROM VENDOR MASTER TABLE
          -- IT WILL PICK UP THE ONLY SEGM,ENT6 (NATURAL ACCT0 VALUE AND UPDATE THE ACCT CODE CONCATENATED FROM THE HDR TABLE.

          BEGIN

            SELECT SUBSTR(REC_INV_HDR.ACCTS_PAY_CODE_CONCATENATED, 1, 22) ||
                   GCC.SEGMENT6 ||
                   SUBSTR(REC_INV_HDR.ACCTS_PAY_CODE_CONCATENATED, 29, 9)
              INTO L_DIST_CODE_CONCATENATED
              FROM PO_VENDORS           PV,
                   PO_VENDOR_SITES_ALL  PVSA,
                   GL_CODE_COMBINATIONS GCC
             WHERE PV.VENDOR_ID = PVSA.VENDOR_ID
               AND GCC.CODE_COMBINATION_ID = PVSA.ACCTS_PAY_CODE_COMBINATION_ID
               AND PV.VENDOR_ID = P_VENDOR_ID
               AND PVSA.VENDOR_SITE_ID = P_VENDOR_SITE_ID;

          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              V_ERROR_LINE_FLAG := 'Y';
              V_LINE_ERR_MSG    := 'ACCOUNTING SEGMENTS NOT MAPPED WITH VENDOR MASTER';

            --FND_FILE.PUT_LINE (FND_FILE.OUTPUT,RPAD(REC_INV_LINE.VENDOR_NUM, 20)
            --                                      || RPAD(REC_INV_LINE.INVOICE_NUM, 20)
            --                                     || V_LINE_ERR_MSG);
            WHEN OTHERS THEN
              V_ERROR_LINE_FLAG := 'Y';
              V_LINE_ERR_MSG    := 'INVALID DIST CODE CONCATENATED VALUE';
              --FND_FILE.PUT_LINE (FND_FILE.OUTPUT,V_LINE_ERR_MSG);

          END;

          /*============================VALIDATION FOR GLDATE ==================================*/

          BEGIN
            SELECT
            SET_OF_BOOKS_ID INTO V_SET_OF_BOOKS_ID
            FROM ORG_ORGANIZATION_DEFINITIONS
            WHERE ORGANIZATION_ID = REC_INV_HDR.ORG_ID;

            FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'SOB-ID: '||V_SET_OF_BOOKS_ID);

          EXCEPTION
            WHEN NO_DATA_FOUND THEN
            FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'SOB NOT DEFINED FOR ORG ID: '||REC_INV_HDR.ORG_ID);
            WHEN TOO_MANY_ROWS THEN
            FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'MULTIPLE SOB DEFINED FOR ORG ID: '||REC_INV_HDR.ORG_ID);
            WHEN OTHERS THEN
            FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'EXCEPTION WHILE DERIVING SOB FOR ORG ID: '||REC_INV_HDR.ORG_ID);
          END;



          IF REC_INV_HDR.GL_DATE IS NULL THEN
            V_ERROR_FLAG  := 'Y';
            V_HDR_ERR_MSG := 'GL DATE IS NULL';
            FND_FILE.PUT_LINE(FND_FILE.OUTPUT, V_HDR_ERR_MSG);
          ELSE
            V_DATA_COUNT := 0;
            BEGIN

              SELECT COUNT(*)
                INTO V_DATA_COUNT
                FROM GL_PERIOD_STATUSES GPS, FND_APPLICATION FNA
               WHERE FNA.APPLICATION_SHORT_NAME = 'SQLAP'
                 AND GPS.APPLICATION_ID = FNA.APPLICATION_ID
                 AND GPS.CLOSING_STATUS = 'O'
                 AND GPS.SET_OF_BOOKS_ID = V_SET_OF_BOOKS_ID
                 AND TRUNC(NVL(REC_INV_HDR.GL_DATE, SYSDATE)) BETWEEN
                     GPS.START_DATE AND GPS.END_DATE;
              --   AND     TO_DATE('08-OCT-08','DD-MON-YYYY')) BETWEEN GPS.START_DATE AND GPS.END_DATE

              --   AND     TRUNC(NVL(TO_DATE('08-OCT-08','DD-MON-YYYY',SYSDATE)) BETWEEN GPS.START_DATE AND GPS.END_DATE;

              IF V_DATA_COUNT = 0 THEN
                V_ERROR_FLAG  := 'Y';
                V_HDR_ERR_MSG := 'GL DATE IS NOT IN OPEN PERIOD';
                FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                                  RPAD(NVL(REC_INV_HDR.VENDOR_NUM, ' '), 20) ||
                                  RPAD(REC_INV_HDR.INVOICE_NUM, 20) ||
                                  V_HDR_ERR_MSG);
              END IF;
            EXCEPTION
              WHEN OTHERS THEN
                V_ERROR_FLAG  := 'Y';
                V_HDR_ERR_MSG := 'GL DATE IS NOT IN OPEN PERIOD';
                FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                                  RPAD(NVL(REC_INV_HDR.VENDOR_NUM, ' '), 20) ||
                                  RPAD(REC_INV_HDR.INVOICE_NUM, 20) ||
                                  V_HDR_ERR_MSG);
            END;
          END IF;

          --DBMS_OUTPUT.PUT_LINE('OUTSITE IF PART' || ' ' || V_ERROR_FLAG || ' ' || 'VALUE FOR ');

          V_ERROR_LINE_COUNT := 0;
          V_ERROR_ALMSG      := NULL;

          FOR REC_INV_LINE IN CUR_INV_LINE(REC_INV_HDR.INVOICE_NUM
                                           ---REC_INV_HDR.VENDOR_SITE_ID
                                          ,REC_INV_HDR.VENDOR_NUM) 
              LOOP
                V_LINE_COUNT             := CUR_INV_LINE%ROWCOUNT;
                V_DIST_CODE_CONCATENATED := NULL;
                V_ERROR_LINE_FLAG        := NULL;
                V_LINE_ERR_MSG           := NULL;
                V_LINE_TYPE_LOOKUP_CODE  := NULL;
                V_DATA_COUNT             := 0;

                -- IF REC_INV_LINE.LINE_TYPE_LOOKUP_CODE IS NOT NULL THEN
                --    SELECT LINE_TYPE_LOOKUP_CODE
                --    INTO   V_LINE_TYPE_LOOKUP_CODE
                --    FROM   APPS.XXABRL_AP_LINE_STAGE
                --    WHERE  INVOICE_NUM := REC_INV_LINE.INVOICE_NUM
                --    AND    LINE_NUMBER := REC_INV_LINE.LINE_NUMBER
                --    AND    LINE_TYPE_LOOKUP_CODE := REC_INV_LINE.LINE_TYPE_LOOKUP_CODE;

                IF REC_INV_LINE.LINE_TYPE_LOOKUP_CODE <> 'ITEM' THEN
                  V_LINE_TYPE_LOOKUP_CODE := 'MISCELLANEOUS';
                ELSE
                  V_LINE_TYPE_LOOKUP_CODE := 'ITEM';
                END IF;
                -- END IF;

                /*============================VALIDATION FOR ACCOUNTING DATE ==================================*/

                IF REC_INV_LINE.ACCOUNTING_DATE IS NULL THEN
                  V_ERROR_LINE_FLAG := 'Y';
                  V_LINE_ERR_MSG    := 'ACCOUNTING DATE IS NULL';
                  --FND_FILE.PUT_LINE (FND_FILE.OUTPUT,V_LINE_ERR_MSG);
                ELSE
                  BEGIN
                    SELECT COUNT(*)
                      INTO V_DATA_COUNT
                      FROM GL_PERIOD_STATUSES GPS, FND_APPLICATION FNA
                     WHERE FNA.APPLICATION_SHORT_NAME = 'SQLAP'
                       AND GPS.APPLICATION_ID = FNA.APPLICATION_ID
                       AND GPS.CLOSING_STATUS = 'O'
                       AND GPS.SET_OF_BOOKS_ID = V_SET_OF_BOOKS_ID
                       AND TRUNC(NVL(REC_INV_LINE.ACCOUNTING_DATE, SYSDATE)) BETWEEN
                           GPS.START_DATE AND GPS.END_DATE;

                    IF V_DATA_COUNT = 0 THEN
                      V_ERROR_LINE_FLAG := 'Y';
                      V_LINE_ERR_MSG    := 'ACCOUNTING DATE IS NOT IN OPEN PERIOD' ||
                                           RPAD(' ', 20) || RPAD(' ', 20) ||
                                           CHR(10) || RPAD(' ', 20) ||
                                           RPAD(' ', 20);
                      --FND_FILE.PUT_LINE (FND_FILE.OUTPUT,V_LINE_ERR_MSG);
                    END IF;
                  EXCEPTION
                    WHEN OTHERS THEN
                      V_ERROR_LINE_FLAG := 'Y';
                      V_LINE_ERR_MSG    := 'ACCOUNTING DATE IS NOT IN OPEN PERIOD';
                      --  FND_FILE.PUT_LINE (FND_FILE.OUTPUT,V_LINE_ERR_MSG);
                  END;
                END IF;

                /*============================VALIDATION FOR INVOICE LINE AMOUNT=====================================*/

                IF REC_INV_LINE.AMOUNT IS NULL THEN
                  V_ERROR_LINE_FLAG := 'Y';
                  V_LINE_ERR_MSG    := 'INVOICE LINE AMOUNT IS NULL' ||
                                       RPAD(' ', 20) || RPAD(' ', 20) || CHR(10) ||
                                       RPAD(' ', 20) || RPAD(' ', 20);
                  FND_FILE.PUT_LINE (FND_FILE.OUTPUT,V_LINE_ERR_MSG);
                END IF;

                FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'REC_INV_LINE.AMOUNT: ' ||NVL(REC_INV_LINE.AMOUNT, 0));

                V_INV_LINE_AMT := NVL(V_INV_LINE_AMT, 0) +
                                  NVL(REC_INV_LINE.AMOUNT, 0);
                --V_INV_LINE_AMT :=  ROUND( V_INV_LINE_AMT,2);

                /*============================VALIDATE THE CONCATENATED_SEGMENTS IN INVOICE LINES=====================================*/
                -- FOLLOWING VALIDATION OF CONCATENATED SEGMENTS REMOVED BY SHAILESH ON 25 OCT 2008.
                -- AS SYSTEM WILL TAKE DEFAULT ACCOUNTING SEGMENT VALUE FROM VENDOR MASTER.

                IF REC_INV_LINE.DIST_CODE_CONCATENATED IS NULL THEN
                  V_ERROR_LINE_FLAG := 'Y';
                  V_LINE_ERR_MSG    := 'DIST CODE CONCATENATED VALUE IS NULL' ||
                                       RPAD(' ', 20) || RPAD(' ', 20) || CHR(10) ||
                                       RPAD(' ', 20) || RPAD(' ', 20);
                  --FND_FILE.PUT_LINE (FND_FILE.OUTPUT,V_LINE_ERR_MSG);
                ELSE
                  BEGIN
                    SELECT CODE_COMBINATION_ID
                      INTO V_DIST_CODE_CONCATENATED
                      FROM GL_CODE_COMBINATIONS_KFV
                     WHERE TRIM(CONCATENATED_SEGMENTS) =
                           TRIM(REC_INV_LINE.DIST_CODE_CONCATENATED);
                  EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                      V_ERROR_LINE_FLAG := 'Y';
                      V_LINE_ERR_MSG    := 'DIST CODE CONCATENATED VALUE' || ' ' ||
                                           REC_INV_LINE.DIST_CODE_CONCATENATED || ' ' ||
                                           'IS NOT VALID';
                      --FND_FILE.PUT_LINE (FND_FILE.OUTPUT,RPAD(REC_INV_LINE.VENDOR_NUM, 20)
                    --                                      || RPAD(REC_INV_LINE.INVOICE_NUM, 20)
                    --                                     || V_LINE_ERR_MSG);
                    WHEN OTHERS THEN
                      V_ERROR_LINE_FLAG := 'Y';
                      V_LINE_ERR_MSG    := 'INVALID DIST CODE CONCATENATED VALUE';
                      --FND_FILE.PUT_LINE (FND_FILE.OUTPUT,V_LINE_ERR_MSG);
                  END;
                END IF;


                      FND_FILE.PUT_LINE(FND_FILE.LOG,'SSS -LINE  ERROR FLAG '||V_ERROR_LINE_FLAG);

                /*====================================================UPDATING LINES STAGING============================================*/
                IF NVL(V_ERROR_LINE_FLAG, 'N') = 'Y' THEN

                  --DBMS_OUTPUT.PUT_LINE('UPDATING LINES STAGING' || ' ' || V_ERROR_FLAG || ' ' || 'VALUE FOR ');

                 -- FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'SHAILESH - UPDATING LINE STAGING ERROR ');

                  UPDATE APPS.XXABRL_AP_LINE_STAGE
                     SET ERROR_FLAG = 'E', ERROR_MSG = V_LINE_ERR_MSG
                   WHERE ROWID = REC_INV_LINE.ROWID;
                  COMMIT;

                  --                  UPDATE  APPS.XXABRL_AP_HEADER_STAGE
                  --                  SET   ERROR_FLAG ='E'
                  --                   ,ERROR_MSG = 'LINE VALIDATION FAILED'|| V_LINE_ERR_MSG
                  --                  WHERE ROWID = REC_INV_HDR.ROWID
                  --               AND INVOICE_NUM=REC_INV_LINE.INVOICE_NUM;

                  V_ERROR_LINE_COUNT := V_ERROR_LINE_COUNT + 1;
                ELSE

           --      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'SHAILESH - UPDATING LINE STAGING NOT IN ERROR ');

                  UPDATE APPS.XXABRL_AP_LINE_STAGE
                     SET ERROR_FLAG            = 'V',
                         LINE_TYPE_LOOKUP_CODE = V_LINE_TYPE_LOOKUP_CODE,
                         ERROR_MSG             = NULL
                   WHERE ROWID = REC_INV_LINE.ROWID;

                  COMMIT;

                END IF;

                IF V_LINE_ERR_MSG IS NOT NULL THEN
                  V_ERROR_ALMSG := V_ERROR_ALMSG || 'LINE NO :' ||
                                   TO_CHAR(V_LINE_COUNT) || ' ' || V_LINE_ERR_MSG;
                END IF;

              END LOOP;


          FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'LINE AMOUNT   ->  '||V_INV_LINE_AMT);
          FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'HDR AMOUNT   ->  '||REC_INV_HDR.INVOICE_AMOUNT);


              IF (ROUND(V_INV_LINE_AMT, 2) - ROUND(REC_INV_HDR.INVOICE_AMOUNT, 2)) BETWEEN -1 AND 1 THEN  --H@N$/SANKARA/01-11-08
        --      IF ROUND(V_INV_LINE_AMT, 2) <> ROUND(REC_INV_HDR.INVOICE_AMOUNT, 2) THEN
                NULL;
              ELSE
                V_ERROR_FLAG  := 'Y';
                V_HDR_ERR_MSG := 'INVOICE HEADER AND LINE AMOUNT IS NOT MATCHING';
                FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                                  RPAD(NVL(REC_INV_HDR.VENDOR_NUM, ' '), 20) ||
                                  RPAD(REC_INV_HDR.INVOICE_NUM, 20) ||
                                  V_HDR_ERR_MSG);
              END IF;

           FND_FILE.PUT_LINE(FND_FILE.LOG,'SSS - HDR ERROR FLAG '||V_ERROR_FLAG);
          /*====================================================UPDATING  HEADER STAGING============================================*/
              IF NVL(V_ERROR_FLAG, 'N') = 'Y' THEN



               --   FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'SHAILESH - UPDATING HDR STAGING ERROR ');

                UPDATE APPS.XXABRL_AP_HEADER_STAGE
                   SET ERROR_FLAG = 'E', ERROR_MSG = V_HDR_ERR_MSG
                 WHERE ROWID = REC_INV_HDR.ROWID;
                COMMIT;

                          --- ADDED BY GONVIND ON 25 JUNE 2009
                   P_VENDOR_SITE_ID             := NULL;
                --              UPDATE  APPS.XXABRL_AP_LINE_STAGE
                --              SET     ERROR_FLAG ='E',ERROR_MSG = 'HEADER VALIDATION FAILED'|| V_HDR_ERR_MSG
                --              WHERE   1=1--ROWID = REC_INV_LINE.ROWID
                --             AND     INVOICE_NUM = REC_INV_HDR.INVOICE_NUM;

                V_ERROR_HEAD_COUNT := V_ERROR_HEAD_COUNT + 1;

                IF V_ERROR_ALMSG IS NOT NULL THEN
                  FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                                    RPAD(NVL(REC_INV_HDR.VENDOR_NUM, ' '), 20) ||
                                    RPAD(REC_INV_HDR.INVOICE_NUM, 20) ||
                                    'LINE ERROR :' || V_ERROR_ALMSG);
                END IF;

              ELSIF V_HDR_ERR_MSG IS NULL AND V_ERROR_LINE_COUNT <> 0 THEN

             --     FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'SHAILESH - UPDATING HDR STAGING ERROR ');

                UPDATE APPS.XXABRL_AP_HEADER_STAGE
                   SET ERROR_FLAG = 'E',
                       ERROR_MSG  = V_ERROR_LINE_COUNT || 'ERROR IN LINES'
                 WHERE ROWID = REC_INV_HDR.ROWID;
                           --- ADDED BY GONVIND ON 25 JUNE 2009
                   P_VENDOR_SITE_ID             := NULL;
                IF V_ERROR_ALMSG IS NOT NULL THEN
                  FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                                    RPAD(NVL(REC_INV_HDR.VENDOR_NUM, ' '), 20) ||
                                    RPAD(REC_INV_HDR.INVOICE_NUM, 20) ||
                                    'LINE ERROR :' || V_ERROR_ALMSG);
                END IF;

              ELSE

                --DBMS_OUTPUT.PUT_LINE('INSEIDE ELSE PART' || ' ' || V_ERROR_FLAG || ' ' || 'VALUE FOR ');
                /*
                UPDATE  APPS.XXABRL_AP_HEADER_STAGE
                 SET     ERROR_FLAG ='V',
                        ---VENDOR_SITE_ID=V_VENDOR_SITE_ID,
                        VENDOR_NUM=REC_INV_HDR.VENDOR_NUM ,
                        ERROR_MSG = NULL
                 WHERE ROWID = REC_INV_HDR.ROWID;
                 */
                -- UPDATED BY SHAILESH ON 25 OCT 2008
                -- AS INSERT PROGRAM IS RUNNING ONLY FOR THE ONE VENDOR SITE FOR ALL VENDORS IN STG TABLE SO WE INCLUDE THE CLOUMN VENDOR SITE ID IN HDR STG TABLE
                -- AND IT WILL STORE THE SITE ID FOR RESPECTIVE VENDOR
                -- IT WILL ALSO STORE THE TERMS ID IF IT IS AVAILABLE IN VENDOR MASTER



               --   FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'SHAILESH - UPDATING HDR STAGING NOT IN ERROR PAYMENT METHOD LOOKUP CODE ' ||  P_PAYMENT_METHOD_LOOKUP_CODE);

                UPDATE APPS.XXABRL_AP_HEADER_STAGE
                   SET ERROR_FLAG = 'V',
                       VENDOR_NUM                  = REC_INV_HDR.VENDOR_NUM,
--                     VENDOR_SITE_ID              = P_VENDOR_SITE_ID,
                       TERMS_ID                    = L_TERMS_ID,
--            ORG_ID                      = P_ORG_ID, 
--                       VENDOR_ID                   = P_VENDOR_ID,
                       PAYMENT_METHOD_LOOKUP_CODE     =  P_PAYMENT_METHOD_LOOKUP_CODE,
                       ACCTS_PAY_CODE_CONCATENATED = L_DIST_CODE_CONCATENATED,
                       ERROR_MSG                   = NULL
                 WHERE ROWID = REC_INV_HDR.ROWID;
                ----IF YOU WANT TO INSERT THE DATA FOR MULTI ORG THEN CREATE COLUMN ORG_ID IN  APPS.XXABRL_AP_HEADER_STAGE TABLE
                --AND  PUT THIS STMT IN ABOVE UPDATE CONDITION  ORG_ID=P_ORG_ID
                -- ALSO INCLUDE THIS COLOUMN IN INSERT STMT WHILE INSERTING TO INTERFACE TABLE FOR HDR


                COMMIT;
                --- ADDED BY GONVIND ON 25 JUNE 2009
                   P_VENDOR_SITE_ID             := NULL;

                --RETEK MATCHED INVOICES

                V_SUCCESS_RECORD_COUNT := V_SUCCESS_RECORD_COUNT + 1;

                --DBMS_OUTPUT.PUT_LINE('BEFORE END LOOP' || ' ' || V_SUCCESS_RECORD_COUNT || ' ' || 'VALUE FOR ');

              END IF;
              
--          END;     /* THIS IS PROBLEM */

        END LOOP;

       FND_FILE.PUT_LINE(FND_FILE.LOG,'ORG ID --> INSERT TRANSACTION PASS ORG_ID -> '||P_ORG_ID);

    INSERT_TRANSACTION(P_ORG_ID,
                       P_VENDOR_SITE_ID,
                       P_VENDOR_ID,
                       P_PAYMENT_METHOD_LOOKUP_CODE);

    --IF NVL(V_LINE_ERR_MSG,'N') <> NULL THEN
    --   V_ERROR_HEAD_COUNT := V_ERROR_LINE_COUNT +1 ;
    --END IF;

    --V_ERROR_HEAD_COUNT := V_ERROR_LINE_COUNT +1;

    --SELECT COUNT(INVOICE_NUM)
    --INTO   V_TOTAL_RECORDS
    --FROM   APPS.XXABRL_AP_HEADER_STAGE
    --WHERE  NVL(ERROR_FLAG,'N') IN ('N','E');

        IF V_ERROR_HEAD_COUNT > 0 THEN
          RETCODE := 1;
        END IF;

        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                          '-------------------------------------------------------------------------------------------------');
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT, CHR(10));
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT, CHR(10));
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT, CHR(10));
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                          'TOTAL NUMBER OF INVOICES FETCHED :' ||
                          V_TOTAL_RECORDS);
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                          'TOTAL NUMBER OF RECORDS WITH ERROR :' ||
                          V_ERROR_HEAD_COUNT);
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                          'TOTAL NUMBER OF INVOICES PROCESSED :' ||
                          V_SUCCESS_RECORD_COUNT);
        --DBMS_OUTPUT.PUT_LINE('TOTAL NUMBER OF RECORDS WITH ERROR :'|| V_ERROR_HEAD_COUNT);
        --DBMS_OUTPUT.PUT_LINE('TOTAL NUMBER OF INVOICES PROCESSED      :'|| V_SUCCESS_RECORD_COUNT);

        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                          '........................................................................');
 END VALIDATE_INVOICES;
  /*=================================END OF VALIDATE_INVOICES FOR ALL TABLES===========================================*/

  /*=================================PROCEDURE INSERT_TRANSACTION ======================================================*/

  PROCEDURE INSERT_TRANSACTION(P_ORG_ID                     IN NUMBER,
                               P_VENDOR_SITE_ID             IN NUMBER,
                               P_VENDOR_ID                  IN NUMBER,
                               P_PAYMENT_METHOD_LOOKUP_CODE IN VARCHAR2) IS
    V_ERROR_FLAG   VARCHAR2(1);
    V_ERRMSG       VARCHAR2(6000);
    V_HEADER_ID    NUMBER;
    V_LINE_ID      NUMBER;
    V_ERRORINVOICE BOOLEAN;
    V_ERRORLINES   BOOLEAN;
    --V_ORG_ID                                NUMBER :=NULL;-- := 82;
    --V_USER_ID                                  NUMBER     :=FND_PROFILE.VALUE('USER_ID');
    --V_RESP_ID                                  NUMBER     :=FND_PROFILE.VALUE('RESP_ID');
    --V_APPL_ID                                  NUMBER     :=FND_PROFILE.VALUE('RESP_APPL_ID');
    --V_REQ_ID                                    NUMBER;
    V_RECORD_COUNT               NUMBER := 0;
    V_ORG_ID                     NUMBER;
    V_VENDOR_SITE_ID             NUMBER(15);
    V_VENDOR_ID                  NUMBER(15);
    V_PAYMENT_METHOD_LOOKUP_CODE VARCHAR2(30);

    -- ADDED BY SHAILESH FOR STORING THE LOCATION OF THE RETAKE PAYABLE INVOICE
    L_LOCATION VARCHAR2(50);

    /*
       CURSOR   CUR_INV_HDR IS
          SELECT    ROWID,APS.*
       FROM        APPS.XXABRL_AP_HEADER_STAGE APS
       WHERE       NVL(ERROR_FLAG,'N') = 'V';
    */

    -- ADDED BY SHAILESH ON 25 OCT 2008
    -- ABOVE COMMENTED CURSOR REPLACED BY NEW CURSOR
    CURSOR CUR_INV_HDR IS

      SELECT APS.ROWID,
             APS.INVOICE_NUM,
             APS.INVOICE_TYPE_LOOKUP_CODE,
             APS.INVOICE_DATE,
             APS.VENDOR_NUM,
             APS.TERMS_ID,
             ROUND(APS.INVOICE_AMOUNT, 2) INVOICE_AMOUNT,
             APS.INVOICE_CURRENCY_CODE,
             APS.EXCHANGE_DATE,
             APS.EXCHANGE_RATE_TYPE,
             APS.DESCRIPTION,
             APS.GL_DATE,
             APS.GROUP_ID,
             APS.VENDOR_EMAIL_ADDRESS,
             APS.TERMS_DATE,
             APS.ACCTS_PAY_CODE_CONCATENATED,
             APS.ERROR_FLAG,
             APS.ERROR_MSG,
             APS.VENDOR_SITE_ID,
             APS.ORG_ID,
             APS.VENDOR_ID,
             APS.PAYMENT_METHOD_LOOKUP_CODE,
             APS.GOODS_RECEIVED_DATE -- NEW COLOUMN ADDED IN THE  XXABRL.XXABRL_AP_HEADER_STAGE
        FROM APPS.XXABRL_AP_HEADER_STAGE APS
       WHERE NVL(ERROR_FLAG, 'N') = 'V';

    /*
    CURSOR   CUR_INV_LINE(P_INVOICE_NUM VARCHAR2,
                          P_VENDOR_NUM NUMBER) IS
     SELECT   ROWID,ALS.*
     FROM     APPS.XXABRL_AP_LINE_STAGE  ALS
     WHERE    NVL(ERROR_FLAG,'N') = 'V'
     AND      ALS.INVOICE_NUM = P_INVOICE_NUM
    ---AND      ALS.VENDOR_SITE_ID = P_VENDOR_SITE_ID
    AND      ALS.VENDOR_NUM = P_VENDOR_NUM;
    */

    -- ADDED BY SHAILESH ON 25 OCT 2008
    -- ABOVE COMMENTED CURSOR REPLACED BY NEW CURSOR
    CURSOR CUR_INV_LINE(P_INVOICE_NUM VARCHAR2, P_VENDOR_NUM NUMBER) IS

      SELECT ALS.ROWID,
             ALS.VENDOR_NUM,
             ALS.VENDOR_SITE_ID,
             ALS.INVOICE_NUM,
             ALS.LINE_NUMBER,
             ALS.LINE_TYPE_LOOKUP_CODE,
             ROUND(ALS.AMOUNT, 2) AMOUNT,
             ALS.ACCOUNTING_DATE,
             ALS.DIST_CODE_CONCATENATED,
             ALS.DESCRIPTION,
             ALS.TDS_TAX_NAME,
             ALS.WCT_TAX_NAME,
             ALS.SERVICE_TAX,
             ALS.VAT,
             ALS.ERROR_FLAG,
             ALS.ERROR_MSG
        FROM APPS.XXABRL_AP_LINE_STAGE ALS
       WHERE NVL(ERROR_FLAG, 'N') = 'V'
         AND ALS.INVOICE_NUM = P_INVOICE_NUM
            ---AND      ALS.VENDOR_SITE_ID = P_VENDOR_SITE_ID
         AND ALS.VENDOR_NUM = P_VENDOR_NUM;

  BEGIN

--   COMMENTED BY SHAILESH ON 20TH JUNE 2009
    --V_ORG_ID                     := P_ORG_ID;
    --V_VENDOR_SITE_ID             := P_VENDOR_SITE_ID;
    --V_VENDOR_ID                  := P_VENDOR_ID;
    --V_PAYMENT_METHOD_LOOKUP_CODE := P_PAYMENT_METHOD_LOOKUP_CODE;

    FOR REC_INV_HDR IN CUR_INV_HDR 
    LOOP
      EXIT WHEN CUR_INV_HDR%NOTFOUND;
      V_HEADER_ID    := NULL;
      V_ERRORINVOICE := NULL;
      V_ERRMSG       := NULL;
      V_RECORD_COUNT := CUR_INV_HDR%ROWCOUNT;
      --V_PAYMENT_METHOD_LOOKUP_CODE          := NULL;
      --V_ORG_ID                              := NULL;
      --V_VENDOR_ID                           := NULL;

      --           SELECT ASR.VENDOR_ID,ASSA.ORG_ID,ASSA.PAYMENT_METHOD_LOOKUP_CODE
      --           INTO   V_VENDOR_ID,V_ORG_ID,V_PAYMENT_METHOD_LOOKUP_CODE
      --           FROM   AP_SUPPLIERS ASR,AP_SUPPLIER_SITES_ALL ASSA
      --           WHERE  ASR.VENDOR_ID=ASSA.VENDOR_ID
      --           AND    ASR.SEGMENT1=REC_INV_HDR.VENDOR_NUM --'9800012'--'987676'--'9800012'
      --           AND       ASSA.VENDOR_SITE_ID=REC_INV_HDR.VENDOR_SITE_ID
      --           AND    ORG_ID = FND_PROFILE.VALUE('ORG_ID');

      SELECT AP_INVOICES_INTERFACE_S.NEXTVAL INTO V_HEADER_ID FROM DUAL;

      BEGIN

      FND_FILE.PUT_LINE(FND_FILE.LOG,'INSERTING RECORD FOR  -> '||REC_INV_HDR.INVOICE_NUM||'  '||REC_INV_HDR.ORG_ID);



        INSERT INTO AP_INVOICES_INTERFACE
          (INVOICE_ID,
           INVOICE_NUM,
           INVOICE_TYPE_LOOKUP_CODE,
           INVOICE_DATE,
           VENDOR_ID,
           VENDOR_SITE_ID,
           INVOICE_AMOUNT,
           INVOICE_CURRENCY_CODE
           --,EXCHANGE_RATE
          ,
           EXCHANGE_RATE_TYPE,
           EXCHANGE_DATE,
           TERMS_ID,
           DESCRIPTION
           --,TERMS_NAME
          ,
           LAST_UPDATE_DATE,
           LAST_UPDATED_BY,
           LAST_UPDATE_LOGIN,
           CREATION_DATE,
           CREATED_BY
           --,GROUP_ID
          ,
           GL_DATE,
           ORG_ID,
           VENDOR_EMAIL_ADDRESS,
           SOURCE,
           TERMS_DATE,
           PAYMENT_METHOD_CODE,
           GOODS_RECEIVED_DATE -- ADDED BY SHAILESH ON 25 OCT 2008
          ,
           ACCTS_PAY_CODE_CONCATENATED)
        VALUES
          (V_HEADER_ID,
           REC_INV_HDR.INVOICE_NUM,
           REC_INV_HDR.INVOICE_TYPE_LOOKUP_CODE,
           REC_INV_HDR.INVOICE_DATE,
           REC_INV_HDR.VENDOR_ID,
           REC_INV_HDR.VENDOR_SITE_ID -- CHANGE BY SHAILESH ON 25 OCT2008 ORIGINAL IS  V_VENDOR_SITE_ID
          ,
           REC_INV_HDR.INVOICE_AMOUNT,
           REC_INV_HDR.INVOICE_CURRENCY_CODE
           --,REC_INV_HDR.EXCHANGE_RATE
          ,
           REC_INV_HDR.EXCHANGE_RATE_TYPE,
           REC_INV_HDR.EXCHANGE_DATE,
           REC_INV_HDR.TERMS_ID,
           REC_INV_HDR.DESCRIPTION
           --,REC_INV_HDR.TERMS_NAME
          ,
           SYSDATE,
           3,
           3,
           SYSDATE,
           3
           --,REC_INV_HDR.GROUP_ID
          ,
           REC_INV_HDR.GL_DATE,
           REC_INV_HDR.ORG_ID,
           REC_INV_HDR.VENDOR_EMAIL_ADDRESS,
           'TSRL_LGR_MIG',
           REC_INV_HDR.TERMS_DATE,
          -- V_PAYMENT_METHOD_LOOKUP_CODE,
           REC_INV_HDR.PAYMENT_METHOD_LOOKUP_CODE, -- ABOVE STMT IS CHANGED BY SHAILESH ON 20TH JUNE 2009
           REC_INV_HDR.GOODS_RECEIVED_DATE -- ADDED BY SHAILESH ON 25 OCT 2008
          ,
           REC_INV_HDR.ACCTS_PAY_CODE_CONCATENATED);
        COMMIT;
        V_ERRORINVOICE := FALSE;
      EXCEPTION
        WHEN OTHERS THEN
          V_ERRORINVOICE := TRUE;
          V_ERRMSG       := 'AFTER INSERT INTO AP INVOICES INTERFACE FAILED IN EXCEPTION' ||
                            SUBSTR(SQLERRM, 1, 150);
          FND_FILE.PUT_LINE(FND_FILE.OUTPUT, V_ERRMSG);
      END;
      
      IF V_ERRORINVOICE = TRUE THEN
        UPDATE APPS.XXABRL_AP_HEADER_STAGE
           SET ERROR_FLAG = 'E', ERROR_MSG = V_ERRMSG
         WHERE ROWID = REC_INV_HDR.ROWID;
        --AND CURRENT OF CUR_INV_HDR;

        UPDATE APPS.XXABRL_AP_LINE_STAGE
           SET ERROR_FLAG = 'E',
               ERROR_MSG  = 'HEADER INSERTING FAILED' || V_ERRMSG
         WHERE INVOICE_NUM = REC_INV_HDR.INVOICE_NUM;
        --AND CURRENT OF CUR_INV_HDR;
      ELSE
        UPDATE APPS.XXABRL_AP_HEADER_STAGE
           SET ERROR_FLAG = 'P',
               ERROR_MSG  = 'SUCCESSFULLY INSERTED IN HEADER INTERFACE TABLE'
         WHERE ROWID = REC_INV_HDR.ROWID;
        --AND CURRENT OF CUR_INV_HDR;
      END IF;

      FOR REC_INV_LINE IN CUR_INV_LINE(REC_INV_HDR.INVOICE_NUM
                                       --,REC_INV_HDR.VENDOR_SITE_ID
                                      ,REC_INV_HDR.VENDOR_NUM) 
      LOOP
        EXIT WHEN CUR_INV_LINE%NOTFOUND;
        V_ERRORINVOICE := FALSE;
        V_ERRMSG       := NULL;
        V_LINE_ID      := NULL;

        SELECT AP_INVOICE_LINES_INTERFACE_S.NEXTVAL
          INTO V_LINE_ID
          FROM DUAL;
        BEGIN

          -- FOLLOWING CODE ADDTED BY SHAILESH ON 26 OCT 2008
          -- IT WILL EXTRACT THE SEGMENT4 FROM DIST_CODE_COMBINITION AND  GET THE LOCATION  FOR THE LINE LEVEL DFF

          BEGIN

            SELECT FFVV.FLEX_VALUE --,FFVV.DESCRIPTION
              INTO L_LOCATION
              FROM FND_FLEX_VALUES_VL FFVV, FND_FLEX_VALUE_SETS FFVS
             WHERE FLEX_VALUE_SET_NAME = 'ABRL_GL_LOCATION'
               AND FFVS.FLEX_VALUE_SET_ID = FFVV.FLEX_VALUE_SET_ID
               AND FFVV.FLEX_VALUE =
                   SUBSTR(REC_INV_LINE.DIST_CODE_CONCATENATED, 12, 7);

          EXCEPTION
            WHEN OTHERS THEN
              L_LOCATION := NULL;
          END;

          /*
          SELECT * FROM AP_INVOICES_INTERFACE
          WHERE INVOICE_NUM='INV_9983'

          SELECT * FROM AP_INVOICE_LINES_INTERFACE
          WHERE  INVOICE_ID=700042
          */

          /*
          SELECT * FROM AP_INVOICES_INTERFACE
          WHERE INVOICE_NUM IN ('INV_9983','INV_10373')


          SELECT * FROM AP_INVOICE_LINES_INTERFACE
          WHERE INVOICE_ID= 700041 --700042
          */
          INSERT INTO AP_INVOICE_LINES_INTERFACE
            (INVOICE_ID,
             INVOICE_LINE_ID,
             LINE_NUMBER,
             LINE_TYPE_LOOKUP_CODE,
             AMOUNT
             --,TAX_CODE
            ,
             DIST_CODE_CONCATENATED,
             DESCRIPTION,
             ACCOUNTING_DATE,
             ORG_ID,
             ATTRIBUTE_CATEGORY --ADDED BY SHAILESH
            ,
             ATTRIBUTE1 --ADDED BY SHAILESH
            ,
             LAST_UPDATED_BY,
             LAST_UPDATE_DATE,
             LAST_UPDATE_LOGIN,
             CREATED_BY,
             CREATION_DATE)
          VALUES
            (V_HEADER_ID,
             V_LINE_ID --,REC_INV_LINE.INVOICE_LINE_ID
            ,
             REC_INV_LINE.LINE_NUMBER,
             REC_INV_LINE.LINE_TYPE_LOOKUP_CODE,
             REC_INV_LINE.AMOUNT
             --,REC_INV_LINE.TAX_CODE
            ,
             REC_INV_LINE.DIST_CODE_CONCATENATED,
             REC_INV_LINE.DESCRIPTION,
             REC_INV_LINE.ACCOUNTING_DATE,
             --V_ORG_ID,
             -- ADDED BY SHAILESH ON 11 MAY 2009  ORG ID IS COMMING FROM HEADER
             REC_INV_HDR.ORG_ID,
             'RETEK PAYABLE INVOICE' --ADDED BY SHAILESH
            ,
             L_LOCATION -- ADDED BY SHAILESH
            ,
             3,
             SYSDATE,
             3,
             3,
             SYSDATE);
          COMMIT;
          V_ERRORLINES := FALSE;
        EXCEPTION
          WHEN OTHERS THEN
            V_ERRORLINES := TRUE;
            V_ERRMSG     := 'AFTER INSERT INTO AP INVOICE LINES INTERFACE FAILED IN EXCEPTION' ||
                            SUBSTR(SQLERRM, 1, 150);
            FND_FILE.PUT_LINE(FND_FILE.OUTPUT, V_ERRMSG);
        END;
        
        IF V_ERRORLINES = TRUE THEN
          UPDATE APPS.XXABRL_AP_LINE_STAGE
             SET ERROR_FLAG = 'E', ERROR_MSG = V_ERRMSG
           WHERE ROWID = REC_INV_LINE.ROWID;
          --AND CURRENT OF CUR_INV_LINE;
        ELSE
          UPDATE APPS.XXABRL_AP_LINE_STAGE
             SET ERROR_FLAG = 'P',
                 ERROR_MSG  = 'SUCCESSFULLY INSERTED IN LINE INTERFACE TABLE'
           WHERE ROWID = REC_INV_LINE.ROWID;
          -- AND CURRENT OF CUR_INV_LINE;
        END IF;
      END LOOP;

    -----
    -- RUN PAYABLE OPEN ITERFACE FOR INTERFACE DATA INTO ORACLE PAYABLES
    ----
    /*      IF V_RECORD_COUNT>0 THEN
                      FND_GLOBAL.APPS_INITIALIZE (
                            USER_ID=> V_USER_ID,
                            RESP_ID=> V_RESP_ID,
                            RESP_APPL_ID=> V_APPL_ID
                         );
                   COMMIT;
                   V_REQ_ID :=FND_REQUEST.SUBMIT_REQUEST (
                                  'SQLAP',
                                'APXIIMPT',
                                 'PAYABLES OPEN INTERFACE IMPORT'||P_DATA_SOURCE,
                            NULL,
                                  FALSE,
                         P_ORG_ID,
                                P_DATA_SOURCE, -- PARAMETER DATA SOURCE
                                 NULL, -- GROUP ID
                              V_BATCH_NAME,
                                 NULL,
                              NULL,
                              NULL,
                              'N',
                        'N',
                           'N',
                        'Y',
                                 CHR (0)
                            );
                   COMMIT;
                   FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'PLEASE SEE THE OUTPUT OF PAYABLES OPEN INVOICE IMPORT PROGRAM REQUEST ID :'||V_REQ_ID);
                   FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'........................................................................' );
                   LOOP
                      BEGIN
                         V_STATUS:=NULL;
                         SELECT STATUS_CODE
                         INTO     V_STATUS
                         FROM      FND_CONCURRENT_REQUESTS
                         WHERE  REQUEST_ID=V_REQ_ID;
                         IF V_STATUS IN ('C','D','E','G','X') THEN
                               EXIT;
                         END IF;
                      EXCEPTION
                         WHEN OTHERS THEN
                               EXIT;
                      END;
                   END LOOP;
             END IF;*/
    END LOOP;
    -- ADDED ON 21-JUN-2008

    ---- COMMENTED BY SHAILESH ON 22 JUNE 2009 FOR TEMP TESTING.
 --   DELETE FROM XXABRL_AP_HEADER_STAGE WHERE ERROR_FLAG = 'P';
 --   DELETE FROM XXABRL_AP_LINE_STAGE WHERE ERROR_FLAG = 'P';
    --
    COMMIT;
  END INSERT_TRANSACTION;
  /*=================================END OF PROCEDURE INSERT_TRANSACTION===================================================*/
END XXABRL_REIM_AP_INV_INT_MIG; 
/


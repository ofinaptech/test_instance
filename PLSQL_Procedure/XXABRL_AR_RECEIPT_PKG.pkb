CREATE OR REPLACE PACKAGE BODY APPS.XXABRL_AR_RECEIPT_PKG AS
PROCEDURE  XXABRL_AR_RECEIPT_PROC (ERRBUFF OUT VARCHAR2,
                                    RETCODE OUT NUMBER,
                                    P_OPERATING_UNIT IN VARCHAR2,
                                    P_FROM_CUSTOMER  VARCHAR2,
                                    P_TO_CUSTOMER   VARCHAR2,
                                    P_LIST_CUSTOMER  VARCHAR2,
                                    P_RECEIPT_FROM_NUM VARCHAR2,
                                    P_RECEIPT_TO_NUM  VARCHAR2,
                                    P_RECEIPT_FROM_DATE  DATE,
                                    P_RECEIPT_TO_DATE DATE,
                                    P_RECEIPT_METHOD VARCHAR2,
                                    P_BANK_ACCOUNT VARCHAR2,
                                    P_RECEIPT_STATUS VARCHAR2
                                    ) IS

-- To Ristrict the responcibility Security

CURSOR CUR_ORG_ID(cp_resp_id NUMBER) IS
SELECT aa.ORGANIZATION_ID FROM (
SELECT organization_id
  FROM fnd_profile_option_values fpop,
       fnd_profile_options fpo,
       per_security_organizations_v pso
 WHERE fpop.profile_option_id = fpo.profile_option_id
   AND level_value = cp_resp_id
   AND pso.security_profile_id = fpop.profile_option_value
   AND profile_option_name = 'XLA_MO_SECURITY_PROFILE_LEVEL'
UNION
SELECT TO_NUMBER (fpop.profile_option_value) ORGANIZATION_ID
  FROM fnd_profile_option_values fpop, fnd_profile_options fpo
 WHERE fpop.profile_option_id = fpo.profile_option_id
   AND level_value = cp_resp_id
   AND profile_option_name = 'ORG_ID')AA;



 QUERY_STRING  LONG;
LIST_CUST_STRING  LONG;
RANGE_CUST_STRING LONG;
RANGE_RCPT_DATE_STRING LONG;
RCPT_DATE_STRING LONG;
ORDER_BY_STRING LONG;
RCPT_NUM_STRING LONG;
RCPT_METHOD_STRING LONG;
BANK_ACT_STRING LONG;
RCPT_STATUS_STRING LONG;

op_string       LONG; -- Added By Praveen 04-03-09
LIST_ORG_ID     LONG; -- Added By Praveen 04-03-09
PARA_ORG_ID     LONG; -- Added By Praveen 04-03-09

V_RESPID             NUMBER := Fnd_Profile.VALUE('RESP_ID');


v_total_unapp_amt NUMBER(30,2);
v_total_app_amt  NUMBER(30,2);
V_REC_CUSTOMER    VARCHAR(250);
v_grand_total_app_amt       NUMBER(30,2);
v_grand_total_unapp_amt    NUMBER(30,2);
cust_count        NUMBER(15);



 TYPE REF_CUR IS REF CURSOR ;
C REF_CUR;

TYPE C_REC IS RECORD(
V_ORG_NAME                  ORG_ORGANIZATION_DEFINITIONS.ORGANIZATION_NAME%TYPE,
V_RECEIPT_METHOD             AR_RECEIPT_METHODS.NAME%TYPE,
V_RECEIPT_NUMBER              AR_CASH_RECEIPTS_V.receipt_number%TYPE,
V_CUSTOMER_NUMBER            AR_CASH_RECEIPTS_V.CUSTOMER_NUMBER %TYPE,
V_CUSTOMER_NAME              AR_CASH_RECEIPTS_V.CUSTOMER_NAME %TYPE,
V_RECEIPT_STATUS              AR_LOOKUPS.MEANING%TYPE,
V_AMOUNT_APPLIED                 AR_PAYMENT_SCHEDULES_ALL.AMOUNT_APPLIED%TYPE,
V_AMOUNT_DUE_REMAINING              AR_PAYMENT_SCHEDULES_ALL.AMOUNT_DUE_REMAINING%TYPE,
V_REMIT_BANK_NAME                  AR_CASH_RECEIPTS_V.REMIT_BANK_NAME%TYPE,
V_REMIT_BANK_BRANCH            AR_CASH_RECEIPTS_V.REMIT_BANK_BRANCH%TYPE,
V_REMIT_BANK_ACCOUNT         AR_CASH_RECEIPTS_V.REMIT_BANK_ACCOUNT%TYPE,
v_RECEIPT_DATE                 AR_CASH_RECEIPTS_V.RECEIPT_DATE%TYPE
);

V_REC C_REC;

 BEGIN

 

 
 
 QUERY_STRING:='select ood.organization_name
,ARM.NAME             RECEIPT_METHOD
,ACR.RECEIPT_NUMBER         RECEIPT_NUMBER
,ACR.CUSTOMER_NUMBER        CUSTOMER_NUMBER
,ACR.CUSTOMER_NAME          CUSTOMER_NAME
,AL.MEANING                 RECEIPT_STATUS
,NVL(APS.AMOUNT_APPLIED*(-1),0)         AMOUNT_APPLIED
,NVL(APS.AMOUNT_DUE_REMAINING*(-1),0)   AMOUNT_DUE_REMAINING
,ACR.REMIT_BANK_NAME        REMIT_BANK_NAME
,ACR.REMIT_BANK_BRANCH      REMIT_BANK_BRANCH
,ACR.REMIT_BANK_ACCOUNT     REMIT_BANK_ACCOUNT
,acr.RECEIPT_DATE            RECEIPT_DATE
FROM
AR_RECEIPT_METHODS   ARM
,AR_CASH_RECEIPTS_V  ACR
,AR_PAYMENT_SCHEDULES_ALL APS
--,CE_BANK_ACCOUNTS   CBA
 ,HZ_CUST_ACCOUNTS_ALL HCA
 --,HZ_CUST_ACCT_SITES_ALL  HCAS
 --,HZ_CUST_SITE_USES_ALL HCASU
 --,HZ_PARTIES  HP
 --,HZ_PARTY_SITES HS
 ,AR_LOOKUPS AL
 ,ORG_ORGANIZATION_DEFINITIONS OOD
WHERE
ACR.RECEIPT_METHOD_ID=ARM.RECEIPT_METHOD_ID
AND ACR.CASH_RECEIPT_ID=APS.CASH_RECEIPT_ID
--AND ACR.REMIT_BANK_ACCT_USE_ID =CBA.BANK_ACCOUNT_ID
AND  APS.CASH_RECEIPT_ID= ACR.CASH_RECEIPT_ID
 AND HCA.CUST_ACCOUNT_ID=ACR.CUSTOMER_ID
-- AND hp.PARTY_ID=hs.PARTY_ID
--AND  hs.PARTY_ID=hca.PARTY_ID
--AND hca.CUST_ACCOUNT_ID=hcas.CUST_ACCOUNT_ID
--AND hcas.CUST_ACCT_SITE_ID=hcasu.SITE_USE_ID
AND AL.LOOKUP_CODE=ACR.RECEIPT_STATUS
AND ACR.ORG_ID=OOD.ORGANIZATION_ID
AND LOOKUP_TYPE=''CHECK_STATUS''';

--AND ood.organization_name = '''||P_OPERATING_UNIT||'''';


LIST_CUST_STRING         := ' AND ACR.CUSTOMER_NUMBER IN ('||P_LIST_CUSTOMER||') ' ;
RANGE_CUST_STRING         := ' AND ACR.CUSTOMER_NUMBER between '''||P_FROM_CUSTOMER||''' AND '''||P_TO_CUSTOMER||''' ';
RANGE_RCPT_DATE_STRING     :=' AND ACR.RECEIPT_DATE  BETWEEN '''||P_RECEIPT_FROM_DATE||''' AND '''||P_RECEIPT_TO_DATE||''' ';
RCPT_DATE_STRING         :=' AND ACR.RECEIPT_DATE = ACR.RECEIPT_DATE ';
RCPT_NUM_STRING             :=' AND ACR.RECEIPT_NUMBER BETWEEN '''||P_RECEIPT_FROM_NUM||''' AND '''||P_RECEIPT_TO_NUM||''' ';
RCPT_METHOD_STRING         :=' AND ARM.NAME='''||P_RECEIPT_METHOD||''' ';
BANK_ACT_STRING             :=' AND ACR.REMIT_BANK_ACCOUNT='''||P_BANK_ACCOUNT||''' ';
RCPT_STATUS_STRING         :=' AND AL.MEANING='''||P_RECEIPT_STATUS||''' ';
op_string                 :=' AND ood.organization_name = '''|| p_operating_unit|| '''';
ORDER_BY_STRING             :=' ORDER BY ACR.CUSTOMER_NAME,AL.MEANING';

-- Added By Praveen on 04-03-09

LIST_ORG_ID:='';
     PARA_ORG_ID  := ' AND OOD.ORGANIZATION_ID in (' ;

    FOR K IN CUR_ORG_ID(V_RESPID) LOOP
         LIST_ORG_ID := K.ORGANIZATION_ID||','||NVL(LIST_ORG_ID,'0')  ;
    END LOOP;

     LIST_ORG_ID:=PARA_ORG_ID || LIST_ORG_ID ||') ';


-- Checking  For Operating Unit Parameter

         IF(p_operating_unit IS NULL) THEN
         query_string:=query_string||LIST_ORG_ID;
         ELSE
         query_string :=query_string||op_string;
         END IF;


      IF (P_FROM_CUSTOMER IS NULL AND P_TO_CUSTOMER IS NULL) AND P_LIST_CUSTOMER IS NOT NULL THEN
   QUERY_STRING:= QUERY_STRING || LIST_CUST_STRING;
ELSIF (P_FROM_CUSTOMER IS NOT NULL AND P_TO_CUSTOMER IS NOT NULL) AND P_LIST_CUSTOMER IS NULL THEN
   QUERY_STRING:= QUERY_STRING || RANGE_CUST_STRING;
 ELSIF (P_FROM_CUSTOMER IS NOT NULL AND P_TO_CUSTOMER IS NOT NULL)  AND P_LIST_CUSTOMER IS NOT NULL THEN
    QUERY_STRING:= QUERY_STRING || LIST_CUST_STRING;
     END IF;

     IF (P_RECEIPT_FROM_DATE IS NOT NULL AND P_RECEIPT_TO_DATE IS NOT NULL) THEN
QUERY_STRING:= QUERY_STRING ||RANGE_RCPT_DATE_STRING;
   ELSIF (P_RECEIPT_FROM_DATE  IS  NULL AND P_RECEIPT_TO_DATE IS NULL) THEN
QUERY_STRING:= QUERY_STRING ||RCPT_DATE_STRING;
END IF;

IF P_RECEIPT_METHOD  IS NOT NULL THEN
QUERY_STRING:= QUERY_STRING ||RCPT_METHOD_STRING;
END IF;
    IF P_RECEIPT_FROM_NUM IS NOT NULL AND P_RECEIPT_TO_NUM IS NOT NULL THEN
   QUERY_STRING:= QUERY_STRING || RCPT_NUM_STRING;
     END IF;

   IF P_BANK_ACCOUNT IS NOT NULL THEN
QUERY_STRING:= QUERY_STRING ||BANK_ACT_STRING;
END IF;

  IF P_RECEIPT_STATUS IS NOT NULL THEN
QUERY_STRING:= QUERY_STRING ||RCPT_STATUS_STRING;
END IF;


         Fnd_File.put_line(Fnd_File.OUTPUT,'ABRL COLLECTION REGISTER REPORT');
        Fnd_File.put_line(Fnd_File.OUTPUT,' ');

         Fnd_File.put_line(Fnd_File.OUTPUT,
                      CHR(9) || CHR(9) || CHR(9) || 'As on Date' || CHR(9) ||
                      SYSDATE || CHR(9));
          Fnd_File.put_line(Fnd_File.OUTPUT,' ');
        Fnd_File.put_line(Fnd_File.OUTPUT,'REPORT PARAMETRS ');
     Fnd_File.put_line(Fnd_File.OUTPUT,
                      CHR(9) || CHR(9) || CHR(9) || 'Operting Unit' || CHR(9) ||
                      P_OPERATING_UNIT|| CHR(9));
                          Fnd_File.put_line(Fnd_File.OUTPUT,
                      CHR(9) || CHR(9) || CHR(9) || ' Customer Low' || CHR(9) ||
                      P_FROM_CUSTOMER|| CHR(9));
                       Fnd_File.put_line(Fnd_File.OUTPUT,
                      CHR(9) || CHR(9) || CHR(9) || ' Customer High' || CHR(9) ||
                      P_TO_CUSTOMER|| CHR(9));
                       Fnd_File.put_line(Fnd_File.OUTPUT,
                      CHR(9) || CHR(9) || CHR(9) || ' Customer List' || CHR(9) ||
                      P_LIST_CUSTOMER || CHR(9));
                      Fnd_File.put_line(Fnd_File.OUTPUT,
                      CHR(9) || CHR(9) || CHR(9) || 'Receipt Number Low' || CHR(9) ||
                      P_RECEIPT_FROM_NUM || CHR(9));
                      Fnd_File.put_line(Fnd_File.OUTPUT,
                      CHR(9) || CHR(9) || CHR(9) || 'Receipt Number High' || CHR(9) ||
                      P_RECEIPT_TO_NUM || CHR(9));
                       Fnd_File.put_line(Fnd_File.OUTPUT,
                      CHR(9) || CHR(9) || CHR(9) || 'Receipt Date Low' || CHR(9) ||
                      P_RECEIPT_FROM_DATE|| CHR(9));
                      Fnd_File.put_line(Fnd_File.OUTPUT,
                      CHR(9) || CHR(9) || CHR(9) || 'Receipt Date High' || CHR(9) ||
                      P_RECEIPT_TO_DATE|| CHR(9));
                      Fnd_File.put_line(Fnd_File.OUTPUT,
                      CHR(9) || CHR(9) || CHR(9) || 'Receipt Method' || CHR(9) ||
                     P_RECEIPT_METHOD|| CHR(9));
                      Fnd_File.put_line(Fnd_File.OUTPUT,
                      CHR(9) || CHR(9) || CHR(9) || ' Bank Account No:' || CHR(9) ||
                      P_BANK_ACCOUNT || CHR(9));
                       Fnd_File.put_line(Fnd_File.OUTPUT,
                      CHR(9) || CHR(9) || CHR(9) || 'Receipt Status' || CHR(9) ||
                       P_RECEIPT_STATUS || CHR(9));



           Fnd_File.put_line(Fnd_File.OUTPUT,' ');
              Fnd_File.put_line(Fnd_File.OUTPUT,' ');

          fnd_File.put_line(Fnd_File.OUTPUT,
                          'Operating Unit'          || CHR(9) ||
                          'Receipt Method'          || CHR(9) ||
                          'Receipt Number'          || CHR(9) ||
                          'Customer Number'      || CHR(9) ||
                          'Customer Name'        || CHR(9) ||
                          'Receipt Status'        || CHR(9) ||
                          'Amount Applied'       || CHR(9) ||
                          'Amount Unapplied'    || CHR(9) ||
                          'Remittance Bank'         || CHR(9) ||
                          'Remittance Branch'         || CHR(9) ||
              'Remittance  Account Number'         || CHR(9) ||
              'Receipt Date'         || CHR(9)
                         );


            QUERY_STRING:=QUERY_STRING||ORDER_BY_STRING;

 Fnd_File.put_line(Fnd_File.LOG,'string: '||QUERY_STRING);

v_total_app_amt:=0;
 v_total_unapp_amt:=0;

 v_grand_total_app_amt:=0;
 v_grand_total_unapp_amt:=0;

 cust_count:=0;



OPEN C FOR QUERY_STRING;

          LOOP
        FETCH C INTO V_REC;

        EXIT WHEN c%NOTFOUND;
       IF NVL(V_REC_CUSTOMER,'X') <> V_REC.V_CUSTOMER_NAME  THEN
 IF cust_count >0 THEN
         Fnd_File.put_line(Fnd_File.OUTPUT,
                      'TOTAL' || CHR(9) ||
                      ' ' || CHR(9) ||
                      ' ' || CHR(9) ||
                      ' ' || CHR(9) ||
                      ' '|| CHR(9) ||
                      ' '|| CHR(9) ||
                     v_total_app_amt   || CHR(9) ||
                      v_total_unapp_amt || CHR(9) ||
                      ' '  || CHR(9) ||
                       ' '|| CHR(9)||
                       ' '|| CHR(9)||
              ' '|| CHR(9)
                       );

               END IF;

         V_REC_CUSTOMER:= V_REC.V_CUSTOMER_NAME;
        v_total_app_amt:=V_REC.V_AMOUNT_APPLIED ;
        v_total_unapp_amt :=V_REC.V_AMOUNT_DUE_REMAINING;
         ELSE
       v_total_app_amt:=v_total_app_amt+V_REC.V_AMOUNT_APPLIED;
v_total_unapp_amt:=v_total_unapp_amt +V_REC.V_AMOUNT_DUE_REMAINING;

       END IF;


            Fnd_File.put_line(Fnd_File.OUTPUT,
                      V_REC.V_ORG_NAME        || CHR(9) ||
                      V_REC.V_RECEIPT_METHOD    || CHR(9) ||
                      V_REC.V_RECEIPT_NUMBER   || CHR(9) ||
                      V_REC.V_CUSTOMER_NUMBER || CHR(9) ||
                      V_REC.V_CUSTOMER_NAME  || CHR(9) ||
                      V_REC.V_RECEIPT_STATUS|| CHR(9) ||
                      V_REC.V_AMOUNT_APPLIED   || CHR(9) ||
                      V_REC.V_AMOUNT_DUE_REMAINING || CHR(9) ||
                      V_REC.V_REMIT_BANK_NAME    || CHR(9) ||
                       V_REC.V_REMIT_BANK_BRANCH  || CHR(9)||
              V_REC.V_REMIT_BANK_ACCOUNT || CHR(9) ||
              V_REC.V_RECEIPT_DATE || CHR(9)
                       );

               cust_count:=cust_count+1;

 v_grand_total_app_amt:=v_grand_total_app_amt+V_REC.V_AMOUNT_APPLIED;
 v_grand_total_unapp_amt:= v_grand_total_unapp_amt+V_REC.V_AMOUNT_DUE_REMAINING;

             END LOOP;

           Fnd_File.put_line(Fnd_File.OUTPUT,
                      'TOTAL'   || CHR(9) ||
                      ' '  || CHR(9) ||
                     ' '|| CHR(9) ||
                      ' '  || CHR(9) ||
                     ' '|| CHR(9) ||
                     ' '|| CHR(9) ||
                      v_total_app_amt  || CHR(9) ||
                      v_total_unapp_amt || CHR(9) ||
                      ' '  || CHR(9) ||
                       ' ' || CHR(9)||
                       ' ' || CHR(9)||
              ' ' || CHR(9)
                       );


              Fnd_File.put_line(Fnd_File.OUTPUT,
                      'GRAND TOTAL' || CHR(9) ||
                      ' '   || CHR(9) ||
                      ' ' || CHR(9) ||
                      ' '  || CHR(9) ||
                       ' ' || CHR(9) ||
                       ' '|| CHR(9) ||
                     v_grand_total_app_amt  || CHR(9) ||
                     v_grand_total_unapp_amt|| CHR(9) ||
                       ' '    || CHR(9) ||
                         ' '   || CHR(9)||
                         ' ' || CHR(9)||
                ' ' || CHR(9)
                       );

            CLOSE C;

            END XXABRL_AR_RECEIPT_PROC;
             END XXABRL_AR_RECEIPT_PKG; 
/


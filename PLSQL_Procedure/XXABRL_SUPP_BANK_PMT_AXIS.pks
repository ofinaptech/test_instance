CREATE OR REPLACE PACKAGE APPS.XXABRL_SUPP_BANK_PMT_AXIS AS

  /**********************************************************************************************************************************************
                        WIPRO Infotech Ltd, Mumbai, India

            Name        : Supplier Bank Outbound interface for Axis Bank

            Change Record:
           =========================================================================================================================
           Version   Date          Author               Remarks                  Documnet Ref
           =======   ==========   =============        ============================================================================
           1.0.0     22-Jul-2010    Sayikrishna Reddi  Initial Version
           1.0.1     23-Jul-2010    Sayikrishna Reddi  Bank Account number hardcoded
           1.0.2     28-Jul-2010    Sayikrishna Reddi  Passing Null Value in Supplier Bank City Column.No attribute is
                                                        Created in Application for Bank City.
           1.0.3     03-AUG-2010    Sayikrishna Reddi  users has entered some payments and due to some issues at Axis Bank end
                                                        Its Not Processed , SO Changing SYSDATE-1
                                                        -- Changed SYSDATE to from and To date Parameters on 04-AUG-2010
           1.0.4     12-AUG-2010    Sayikrishna Reddi   New Procedure Added to Update the AXIS_PAYMENT_TABLE Status as AUTHORIZED
                                                        If any payments Rectified by User ,
           1.0.5     16-AUG-2010    Sayikrishna Reddi   Attribute value Changed as "Success" from "Transaction Success" in XXABRL_UPDATE_AXIS_INFO Procedure
           1.0.6     26-AUG-2010    Sayikrishna Reddi   Paytype Code is added in  in axis_payment_invoice table
           1.0.7     08-SEP-2010    Sayikrishna Reddi   below New Conditions are added
                                                        --1)    RTGS Payment amount should be greater than or equal to 1 lac
                                                        --2)    IFSC code length should be 11.
                                                        --3)    Supplier bank name is mandatory for NEFT and RTGS payments
          1.0.8     27-SEP-2010    Sayikrishna Reddi    In update statement new condition added updated_in_app='N'
          1.0.9     07-OCT-2010    Sayikrishna Reddi    In lookup Startdate active condtion added for bank accounts
          1.0.10    25-OCT-2010    Sayikrishna Reddi    Modified Primary Name Column with  Secondary Name and Primary Name Column with  Secondary Name
          1.0.11    12-Nov-2010    Sayikrishna Reddi    Modified DATE Parameters  and Email id with attribute12,13 AND Increased the datatype size to
                                                            500 in AXIS PAYMENT TABLE
          1.0.12    02-dec-2010    Sayikrishna Reddi , Mitul   For RBI Rules we updated limit of ammount now is 2 Lacks
                                                       New Procedure added for Schdeling.
           1.0.13    15-Jan-2011    Mitul              E-mail Address Not avalid Format Supplier LEVEL
          1.0.14    08-Dec-2011    Narasimhulu         Axis bank OU Name codification added
          1.0.15    15-Dec-2011    Narasimhulu         Axis bank g_ever_failed flag column  (True or false) assigning has been changed
          1.0.16    02-Jan-2012    Narasimhulu         AMOUNT_PAID column has been modified

  ************************************************************************************************************************************************/

PROCEDURE XXABRL_AXISBANK_PAYMENT_SUPP ( errbuf     out VARCHAR2
                                  ,      retcode    out VARCHAR2
                                  ,      P_FROM_DATE  VARCHAR2
                                  ,      P_TO_DATE   VARCHAR2   );
--This Procedure will Process all Valid Payments into AXIS_PAYMENT_TABLE

PROCEDURE XXABRL_UPDATE_AXIS_INFO (  errbuf     out VARCHAR2
                                  ,  retcode    out VARCHAR2);
/*This Program will Update (AP_CHECKS_ALL) the DFF in Payment Screen(AXIS Bank Payment Information),
   Once the Axis Bank will Provide
*/
PROCEDURE XXABRL_USER_RECTIFIED_PAY( errbuf     out VARCHAR2
                                  ,  retcode    out VARCHAR2
                                  ,  p_from_check_id NUMBER
                                  ,  p_to_check_id NUMBER );
/*  This Program will Update the AXIS_PAYMENT_TABLE ,
    with all Corrected data by User as well as Transaction Status as
*/

PROCEDURE XXABRL_SCD_AXIS_PAYMENT_SUPP( errbuf     out VARCHAR2
                                         ,  retcode    out VARCHAR2);
/*  This Procedure will Process all Valid Payments into AXIS_PAYMENT_TABLE thru Schdule Program
*/

END; 
/


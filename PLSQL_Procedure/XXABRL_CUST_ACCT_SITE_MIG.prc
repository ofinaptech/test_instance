CREATE OR REPLACE PROCEDURE APPS.xxabrl_cust_acct_site_mig 
--(
--   x_err_buf    OUT   VARCHAR2,
--   x_ret_code   OUT   NUMBER
--)
AS
   p_party_site_number            VARCHAR2 (100);
   p_party_site_id                VARCHAR2 (100);
   p_account_number               VARCHAR2 (100);
   p_cust_acct_site_id            VARCHAR2 (100);
   p_status                       VARCHAR2 (100);
   p_bill_to_flag                 VARCHAR2 (100);
   p_market_flag                  VARCHAR2 (100);
   p_ship_to_flag                 VARCHAR2 (100);
   p_customer_category_code       VARCHAR2 (100);
   p_territory                    VARCHAR2 (100);
   p_translated_customer_name     VARCHAR2 (100);
   p_created_by_module            VARCHAR2 (100);
   p_orig_system                  VARCHAR2 (100);
   l_cust_acct_site_rec           hz_cust_account_site_v2pub.cust_acct_site_rec_type;
   l_party_site_id                NUMBER;
   l_cust_account_id              NUMBER;
   l_cust_acct_site_id            NUMBER;
   x_return_status                VARCHAR2 (200);
   x_msg_count                    NUMBER;
   x_msg_data                     VARCHAR2 (200);
   l_error_message                VARCHAR2 (1000);
   ----uses all api
   p_profile_class_id             NUMBER;
   p_cust_acct_site_id            NUMBER;
   p_cust_account_id              NUMBER;
   p_party_id                     NUMBER;
   p_party_site_id                NUMBER;
   --x_return_status         VARCHAR2(100);
   x_site_use_id                  NUMBER;
   x_cust_site_use_status         VARCHAR2 (100);
   x_error_msg                    VARCHAR2 (100);
   lc_cust_site_use_rec           hz_cust_account_site_v2pub.cust_site_use_rec_type;
   lc_customer_site_profile_rec   hz_customer_profile_v2pub.customer_profile_rec_type;
   lc_cust_profile_amt_rec        hz_customer_profile_v2pub.cust_profile_amt_rec_type;
   lc_cust_acct_profile_amt_id    NUMBER;
   lc_return_status               VARCHAR2 (10);
   --x_msg_count                NUMBER;
   --x_msg_data                 VARCHAR2(2000);
   ln_msg_index_out               NUMBER;
   ln_cust_account_profile_id     NUMBER;
   p_location                     NUMBER;                             -- H@N$
   lc_cust_site_use_status        VARCHAR2 (240);
   v_orgid                        NUMBER      := fnd_profile.VALUE ('ORG_ID');

   CURSOR cus_acct_site
   IS
      SELECT ROWID, s.*
        FROM apps.xxabrl_cust_header_mig s
       WHERE success_flag IS NULL;          -- AND cust_account_id = 1214050;

   CURSOR art_use (p_site_id NUMBER)
   IS
      SELECT ROWID, hcsua.*
        FROM apps.xxabrl_cust_line_mig hcsua
       WHERE hcsua.cust_acct_site_id = p_site_id AND success_flag IS NULL;
BEGIN
   --fnd_client_info.set_org_context('84');
    --MO_GLOBAL.SET_POLICY_CONTEXT('S',)
   mo_global.init ('AR');
   fnd_global.apps_initialize (user_id           => 0,
                               resp_id           => 58601,
                               resp_appl_id      => 222
                              );
   mo_global.set_policy_context ('S', 1481);

--    mo_global.
     --fnd_global.set_nls_context('AMERICAN');
   FOR rec_cus_acct_site IN cus_acct_site
   LOOP
      l_cust_acct_site_rec.cust_account_id :=
                                            rec_cus_acct_site.cust_account_id;
      l_cust_acct_site_rec.party_site_id := rec_cus_acct_site.party_site_id;
      l_cust_acct_site_rec.created_by_module :=
                                          rec_cus_acct_site.created_by_module;
      l_cust_acct_site_rec.orig_system_reference :=
                                      rec_cus_acct_site.orig_system_reference;
      l_cust_acct_site_rec.status := rec_cus_acct_site.status;
      hz_cust_account_site_v2pub.create_cust_acct_site
                               (p_init_msg_list           => fnd_api.g_true,
                                p_cust_acct_site_rec      => l_cust_acct_site_rec,
                                x_cust_acct_site_id       => l_cust_acct_site_id,
                                x_return_status           => x_return_status,
                                x_msg_count               => x_msg_count,
                                x_msg_data                => x_msg_data
                               );
      COMMIT;

      FOR rec_cus_acct_use IN art_use (rec_cus_acct_site.cust_acct_site_id)
      LOOP
         IF x_return_status = fnd_api.g_ret_sts_success
         THEN
            lc_cust_site_use_rec.gl_id_rev := rec_cus_acct_use.gl_id_rev_new;
            lc_cust_site_use_rec.gl_id_freight :=
                                           rec_cus_acct_use.gl_id_freight_new;
            lc_cust_site_use_rec.gl_id_rec := rec_cus_acct_use.gl_id_rec_new;
            --added on 20-JUN-08
            lc_cust_site_use_rec.gl_id_tax := rec_cus_acct_use.gl_id_tax_new;
            --added on 20-JUN-08
            lc_cust_site_use_rec.gl_id_clearing :=
                                          rec_cus_acct_use.gl_id_clearing_new;
            --added on 20-JUN-08
            lc_cust_site_use_rec.gl_id_unbilled :=
                                          rec_cus_acct_use.gl_id_unbilled_new;
            --added on 20-JUN-08
            lc_cust_site_use_rec.gl_id_unearned :=
                                          rec_cus_acct_use.gl_id_unearned_new;
            --added on 20-JUN-08
            lc_cust_site_use_rec.cust_acct_site_id := l_cust_acct_site_id;
            --7575;
            lc_cust_site_use_rec.site_use_code :=
                                               rec_cus_acct_use.site_use_code;
            --'SHIP_TO';
            lc_cust_site_use_rec.created_by_module :=
                                           rec_cus_acct_use.created_by_module;
            --'AWC_LEGACY';
            lc_cust_site_use_rec.ship_via := rec_cus_acct_use.ship_via;
            lc_cust_site_use_rec.payment_term_id :=
                                             rec_cus_acct_use.payment_term_id;
            lc_cust_site_use_rec.primary_salesrep_id := NULL;
            --l_salesrep_id;
            lc_cust_site_use_rec.LOCATION := rec_cus_acct_use.LOCATION;
            --'DELHI'; --<<abrl---?
            lc_cust_site_use_rec.org_id := rec_cus_acct_use.org_id;
            --lc_cust_site_use_rec.order_type_id         := NULL;
            lc_cust_site_use_rec.application_id := '222';
            lc_cust_site_use_rec.warehouse_id :=
                                                rec_cus_acct_use.warehouse_id;
            --'204';
            lc_cust_site_use_rec.primary_flag :=
                                                rec_cus_acct_use.primary_flag;

            --'Y';
            IF lc_cust_site_use_rec.site_use_code = 'BILL_TO'
            THEN
               -- Create Profile class only if site is BILL_TO
                     --printlog('This is a BILL_TO site' , gc_debug_flag);
                     --lc_customer_site_profile_rec.profile_class_id  := p_profile_class_id;
               lc_customer_site_profile_rec.status := rec_cus_acct_use.status;
               lc_customer_site_profile_rec.override_terms := 'Y';
                                                            --For AR INVOICES
               --lc_customer_site_profile_rec.credit_classification := p_customer.credit_class_code;
               lc_customer_site_profile_rec.created_by_module :=
                                           rec_cus_acct_use.created_by_module;
               lc_customer_site_profile_rec.application_id := '222';
               lc_customer_site_profile_rec.cust_account_id :=
                                             rec_cus_acct_use.cust_account_id;
               --'9072';--cursor
               lc_customer_site_profile_rec.party_id :=
                                                    rec_cus_acct_use.party_id;
               --'47280';
               lc_customer_site_profile_rec.standard_terms := NULL;
               hz_cust_account_site_v2pub.create_cust_site_use
                     (p_init_msg_list             => 'T',
                      p_cust_site_use_rec         => lc_cust_site_use_rec,
                      p_customer_profile_rec      => lc_customer_site_profile_rec,
                      p_create_profile            => 'T',                --'T'
                      p_create_profile_amt        => 'T',
                      --lc_cust_profile_amt_rec,
                      x_site_use_id               => x_site_use_id,
                      x_return_status             => x_return_status,
                      x_msg_count                 => x_msg_count,
                      x_msg_data                  => x_msg_data
                     );

               IF x_return_status = fnd_api.g_ret_sts_success
               THEN
                  COMMIT;
                  fnd_file.put_line
                      (fnd_file.LOG,
                       'Creation of BILL_TO CUSTOMER SITE USE is Successful '
                      );
                  -- DBMS_OUTPUT.PUT_LINE('Creation of CUSTOMER SITE USE is Successful ');
                  fnd_file.put_line (fnd_file.LOG, 'Output information ....');
                  -- DBMS_OUTPUT.PUT_LINE('Output information ....');
                  fnd_file.put_line (fnd_file.LOG,
                                     'x_cust_acct_site_id  : '
                                     || x_site_use_id
                                    );
               --   DBMS_OUTPUT.PUT_LINE('x_cust_acct_site_id  : '||x_site_use_id);
                  --DBMS_OUTPUT.PUT_LINE('x_account_number   : '||x_account_number);
                  --DBMS_OUTPUT.PUT_LINE('x_party_id         : '||x_party_id);
                  --DBMS_OUTPUT.PUT_LINE('x_party_number     : '||x_party_number);
               ELSE
                  fnd_file.put_line
                               (fnd_file.LOG,
                                   'Creation of BILL_TO SITE USE got failed:'
                                || x_msg_data
                               );
                  --DBMS_OUTPUT.PUT_LINE ('Creation of SITE USE got failed:'||x_msg_data);
                  ROLLBACK;

                  FOR i IN 1 .. x_msg_count
                  LOOP
                     x_msg_data :=
                         fnd_msg_pub.get (p_msg_index      => i,
                                          p_encoded        => 'F');
                     fnd_file.put_line (fnd_file.LOG,
                                        i || ') ' || x_msg_data);
                  -- dbms_output.put_line( i|| ') '|| x_msg_data);
                  END LOOP;
               END IF;

               fnd_file.put_line (fnd_file.LOG, 'Completion of API');
            --  DBMS_OUTPUT.PUT_LINE('Completion of API');
            ELSIF lc_cust_site_use_rec.site_use_code = 'SHIP_TO'
            THEN
               lc_cust_site_use_rec.gl_id_rev := NULL;
               lc_cust_site_use_rec.gl_id_freight := NULL;
               lc_cust_site_use_rec.gl_id_rec := NULL;
               lc_cust_site_use_rec.gl_id_tax := NULL;
               lc_cust_site_use_rec.gl_id_clearing := NULL;
               lc_cust_site_use_rec.gl_id_unbilled := NULL;
               lc_cust_site_use_rec.gl_id_unearned := NULL;
               hz_cust_account_site_v2pub.create_cust_site_use
                     (p_init_msg_list             => 'T',
                      p_cust_site_use_rec         => lc_cust_site_use_rec,
                      p_customer_profile_rec      => lc_customer_site_profile_rec,
                      p_create_profile            => 'F',
                      p_create_profile_amt        => 'F',
                      x_site_use_id               => x_site_use_id,
                      x_return_status             => x_return_status,
                      x_msg_count                 => x_msg_count,
                      x_msg_data                  => x_msg_data
                     );

               IF x_return_status = fnd_api.g_ret_sts_success
               THEN
                  COMMIT;
                  fnd_file.put_line
                      (fnd_file.LOG,
                       'Creation of CUSTOMER SHIP_TO SITE USE is Successful '
                      );
                  --DBMS_OUTPUT.PUT_LINE('Creation of CUSTOMER SITE USE is Successful ');
                  fnd_file.put_line (fnd_file.LOG, 'Output information ....');
                  --DBMS_OUTPUT.PUT_LINE('Output information ....');
                  fnd_file.put_line (fnd_file.LOG,
                                     'x_cust_acct_site_id  : '
                                     || x_site_use_id
                                    );
               -- DBMS_OUTPUT.PUT_LINE('x_cust_acct_site_id  : '||x_site_use_id);
                --DBMS_OUTPUT.PUT_LINE('x_account_number   : '||x_account_number);
                --DBMS_OUTPUT.PUT_LINE('x_party_id         : '||x_party_id);
                --DBMS_OUTPUT.PUT_LINE('x_party_number     : '||x_party_number);
               ELSE
                  fnd_file.put_line
                               (fnd_file.LOG,
                                   'Creation of SHIP_TO SITE USE got failed:'
                                || x_msg_data
                               );
                  -- DBMS_OUTPUT.PUT_LINE ('Creation of SITE USE got failed:'||x_msg_data);
                  ROLLBACK;

                  FOR i IN 1 .. x_msg_count
                  LOOP
                     x_msg_data :=
                         fnd_msg_pub.get (p_msg_index      => i,
                                          p_encoded        => 'F');
                     fnd_file.put_line (fnd_file.LOG,
                                        i || ') ' || x_msg_data);
                  --dbms_output.put_line( i|| ') '|| x_msg_data);
                  END LOOP;
               END IF;

               fnd_file.put_line (fnd_file.LOG, 'Completion of API');
            --DBMS_OUTPUT.PUT_LINE('Completion of API');
            END IF;

            fnd_file.put_line (fnd_file.LOG,
                               'Creation of cus_account_site is Successful '
                              );
            -- DBMS_OUTPUT.PUT_LINE('Creation of cus_account_site is Successful ');
            fnd_file.put_line (fnd_file.LOG, 'Output information ....');
            -- DBMS_OUTPUT.PUT_LINE('Output information ....');
            fnd_file.put_line (fnd_file.LOG,
                               'x_cust_acct_site_id  : '
                               || l_cust_acct_site_id
                              );

            UPDATE xxabrl_cust_header_mig
               SET success_flag = 'Y',
                   error_msg = NULL
             WHERE ROWID = rec_cus_acct_site.ROWID;

            UPDATE xxabrl_cust_line_mig
               SET success_flag = 'Y',
                   error_msg = NULL
             WHERE ROWID = rec_cus_acct_use.ROWID;

            COMMIT;
         ELSE
            fnd_file.put_line (fnd_file.LOG,
                                  'Creation of cust_account_site got failed:'
                               || x_msg_data
                              );
            COMMIT;
            ROLLBACK;

            FOR i IN 1 .. x_msg_count
            LOOP
               x_msg_data :=
                         fnd_msg_pub.get (p_msg_index      => i,
                                          p_encoded        => 'F');
               fnd_file.put_line (fnd_file.LOG, i || ') ' || x_msg_data);

               UPDATE xxabrl_cust_header_mig
                  SET success_flag = 'E',
                      error_msg = i || ') ' || x_msg_data
                WHERE ROWID = rec_cus_acct_site.ROWID;

               UPDATE xxabrl_cust_line_mig
                  SET success_flag = 'E',
                      error_msg = i || ') ' || x_msg_data
                WHERE ROWID = rec_cus_acct_use.ROWID;

               COMMIT;
            END LOOP;
         END IF;

         fnd_file.put_line (fnd_file.LOG, 'Completion of API');
      END LOOP;
   END LOOP;
END xxabrl_cust_acct_site_mig; 
/


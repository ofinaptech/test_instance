CREATE OR REPLACE PROCEDURE APPS.gst_abrl_inv_tds (
   errbuf              OUT      VARCHAR2,
   retcode             OUT      VARCHAR2,
   p_from_date         IN       VARCHAR2,
   p_to_date           IN       VARCHAR2,
   p_org_id            IN       VARCHAR2,
   p_tds_section       IN       VARCHAR2,
   p_vendor_name       IN       VARCHAR2,
   p_vendor_num_from   IN       VARCHAR2,
   p_vendor_num_to     IN       VARCHAR2,
   p_vendor_site_num   IN       VARCHAR2
)
IS
----------------------------------------------------------------------------------------------------------------------------------
-- Version      Date            Changed By                     Description                                                      --
----------------------------------------------------------------------------------------------------------------------------------
-- 1.1          30-Dec-2020     Pawan Sahu                     Corrected Date format in where condition to be compatible with   --
--                                                             FND_STANDARD_DATE value set                                      --
--                                                             Exception block uncommented as well                              --
----------------------------------------------------------------------------------------------------------------------------------


   inv_date_from   DATE;
   inv_date_to     DATE;
BEGIN
   fnd_file.put_line
      (fnd_file.output,
       'OU_NAME|VENDOR_NUMBER|VENDOR_NAME|PAN NUMBER|TDS VENDOR TYPE|VENDOR_SITE|INVOICE_TYPE|INVOICE_NUMBER|GL_DATE|INVOICE_AMOUNT|INVOICE LINE AMOUNT|TAX_RATE|SECTION_CODE|TAX_CATEGORY_NAME|TAX RATE NAME|TDS_AMOUNT|BASE_AMOUNT|NET_AMOUNT_DUE|TDS_INVOICE_NUM|APPROVAL_STATUS|USER_NAME|CREATION_DATE'
      );

   FOR tds_inv_reg IN
      (

      SELECT DISTINCT ood.organization_name ou_name,
                       aps.segment1 vendor_number,
                       aps.vendor_name vendor_name,
                       apsa.vendor_site_code vendor_site, api.invoice_id,
                       api.invoice_type_lookup_code invoice_type,
                       REPLACE (api.invoice_num, CHR (9),
                                '''') invoice_number,
                       apida.accounting_date gl_date,
                       api.invoice_amount invoice_amount,
                       jati.invoice_amount invoice_dist_amount,
                       jtr.tax_rate_name, jtrd.tax_rate_percentage tax_rate,
                       jati.tds_section section_code, jtc.tax_category_name,
                       DECODE (api.invoice_amount,
                               0, 0,
                               NVL (jati.tds_amount, 0)
                              ) tds_amount,
                       DECODE
                          (api.invoice_amount,
                           0, 0,
                           CEIL (  jati.tds_amount
                                 / NVL (DECODE (jtrd.tax_rate_percentage,
                                                0, 1
                                               ),
                                        1
                                       )
                                )
                          ) base_amount,
                         api.invoice_amount
                       - DECODE (api.invoice_amount,
                                 0, 0,
                                 NVL (jati.tds_amount, 0)
                                ) net_amount_due,
                       REPLACE (jati.tds_invoice_num,
                                CHR (9),
                                ''''
                               ) tds_invoice_num,
                       apps.ap_invoices_pkg.get_approval_status
                                (api.invoice_id,
                                 api.invoice_amount,
                                 api.payment_status_flag,
                                 api.invoice_type_lookup_code
                                ) approval_status,
                       api.creation_date, fu.user_name,
                       (SELECT DISTINCT jprl.registration_number
                                   FROM apps.jai_party_regs_v jpr,
                                        apps.jai_party_reg_lines jprl
                                  WHERE 1 = 1
                                    AND jpr.party_reg_id = jprl.party_reg_id
                                    AND aps.vendor_id = jp.party_id
                                    AND jpr.reg_class_code = 'THIRD_PARTY'
                                    AND jprl.effective_to IS NULL
                                    AND jprl.registration_type_code = 'PAN'
                                    AND jpr.party_id = aps.vendor_id
                                    AND jpr.party_site_id =
                                                           apsa.vendor_site_id
                                    AND jpr.party_reg_id = jp.party_reg_id
                                    and rownum=1)
                                                                vendor_pan_no,
                       (SELECT DISTINCT reporting_code_description
                                   FROM apps.jai_reporting_associations jra,
                                        apps.jai_party_regs jpr
                                  WHERE jpr.party_id = aps.vendor_id
                                    AND jpr.party_site_id =
                                                           apsa.vendor_site_id
                                    AND aps.vendor_id = jp.party_id
                                    AND apsa.vendor_site_id = jp.party_site_id
                                    AND jpr.org_id = apsa.org_id
                                    AND jra.entity_id = jpr.party_reg_id
                                    AND jpr.party_reg_id = jp.party_reg_id
--                             AND jprl.effective_to IS NULL
                                    AND jra.effective_to IS NULL
                                    AND reporting_type_name = 'Vendor Type'
                                    AND entity_source_table = 'JAI_PARTY_REGS'
                                    and Rownum = 1)
                                                                  vendor_type
                  FROM apps.ap_invoices_all api,
                       apps.ap_invoice_lines_all apil,
                       apps.ap_suppliers aps,
                       apps.ap_supplier_sites_all apsa,
                       apps.ap_invoice_distributions_all apida,
                       apps.jai_ap_tds_invoices jati,
                       apps.org_organization_definitions ood,
                       apps.jai_tax_categories jtc,
                       apps.jai_tax_category_lines jrctl,
--                       apps.ap_invoices_all api1,
--                       apps.jai_tax_lines_v jlt,
                       apps.jai_party_regs jp,
                       apps.jai_tax_rates jtr,
                       apps.jai_tax_rate_details jtrd,
                       apps.fnd_user fu
                 WHERE api.invoice_id = apil.invoice_id
                   AND apida.invoice_id = api.invoice_id
                   AND apida.invoice_line_number = apil.line_number
                   --AND api.invoice_num IN ('VRS2017007')
--                   AND api.invoice_id = jlt.trx_id
                   AND jati.invoice_id = api.invoice_id
                   AND aps.vendor_id = apsa.vendor_id
                   AND api.vendor_id = aps.vendor_id
                   AND api.vendor_site_id = apsa.vendor_site_id
                   AND ood.organization_id = api.org_id
                   AND jati.tax_category_id = jtc.tax_category_id
                   AND jtc.tax_category_id = jrctl.tax_category_id
                   AND jrctl.tax_rate_id = jtr.tax_rate_id
                   AND jtr.tax_rate_id = jtrd.tax_rate_id
                   AND api.CANCELLED_BY is null
                   AND fu.user_id = api.created_by
--                   AND apida.parent_invoice_id = api1.invoice_id(+)
                   AND aps.segment1 BETWEEN NVL (p_vendor_num_from,
                                                 aps.segment1
                                                )
                                        AND NVL (p_vendor_num_to,
                                                 aps.segment1)
                   AND apsa.vendor_site_code = NVL (p_vendor_site_num, apsa.vendor_site_code)
                   AND aps.vendor_id = NVL (p_vendor_name, aps.vendor_id)
                   AND aps.vendor_id = jp.party_id
                   AND apsa.vendor_site_id = jp.party_site_id
                   /*AND to_char(apida.accounting_date,'DD-MON-YYYY') BETWEEN TO_char (TO_DATE(p_from_date),'DD-MON-YYYY') AND TO_char (TO_DATE(p_to_date), 'DD-MON-YYYY') */ -- V1.1 Commented 
                   AND apida.accounting_date BETWEEN TO_DATE (p_from_date, 'YYYY/MM/DD HH24:MI:SS') AND TO_DATE (p_to_date, 'YYYY/MM/DD HH24:MI:SS') -- V1.1 Added
                   AND jati.tds_section = NVL (p_tds_section, jati.tds_section)
--                   AND TRUNC (apida.accounting_date)
--                          BETWEEN TRUNC (inv_date_from)
--                              AND TRUNC (inv_date_to)
                   AND api.org_id = NVL (p_org_id, api.org_id)
              GROUP BY ood.organization_name,
                       aps.segment1,
                       aps.vendor_name,
                       apsa.vendor_site_code,
                       aps.vendor_id,
                       apsa.vendor_site_id,
                       api.invoice_type_lookup_code,
                       REPLACE (api.invoice_num, CHR (9), ''''),
                       apida.accounting_date,
                       api.invoice_amount,
                       jtc.tax_category_name,
                       NVL (jati.tds_amount, 0),
                       NVL (api.invoice_amount, 0) - NVL (jati.tds_amount, 0),
                       REPLACE (jati.tds_invoice_num, CHR (9), ''''),
                       jati.tds_invoice_id,
                       api.creation_date,
                       fu.user_name,
                       jtr.tax_rate_name,
                       jtrd.tax_rate_percentage,
                       apsa.org_id,
                       jati.tds_section,
                       jati.tds_amount,
                       jp.party_id,
                       jp.party_site_id,
                       jati.invoice_amount,
                       api.invoice_id,
                       api.payment_status_flag,
                       jp.party_reg_id

                       )
   LOOP
      fnd_file.put_line (fnd_file.output,
                            tds_inv_reg.ou_name
                         || '|'
                         || tds_inv_reg.vendor_number
                         || '|'
                         || tds_inv_reg.vendor_name
                         || '|'
                         || tds_inv_reg.vendor_pan_no
                         || '|'
                         || tds_inv_reg.vendor_type
                         || '|'
                         || tds_inv_reg.vendor_site
                         || '|'
                         || tds_inv_reg.invoice_type
                         || '|'
                         || tds_inv_reg.invoice_number
                         || '|'
                         || tds_inv_reg.gl_date
                         || '|'
                         || tds_inv_reg.invoice_amount
                         || '|'
                         || tds_inv_reg.invoice_dist_amount
                         || '|'
                         || tds_inv_reg.tax_rate
                         || '|'
                         || tds_inv_reg.section_code
                         || '|'
                         || tds_inv_reg.tax_category_name
                         || '|'
                         || tds_inv_reg.tax_rate_name
                         || '|'
                         || tds_inv_reg.tds_amount
                         || '|'
                         || tds_inv_reg.base_amount
                         || '|'
                          --       || tds_inv_reg.tax_rate_percentage
                         --        || '|'
                         || tds_inv_reg.net_amount_due
                         || '|'
                         || tds_inv_reg.tds_invoice_num
                         || '|'
                         || tds_inv_reg.approval_status
                         || '|'
                         || tds_inv_reg.user_name
                         || '|'
                         || tds_inv_reg.creation_date
                        );
   END LOOP;
EXCEPTION --V1.1 Uncommented by Pawan
   WHEN OTHERS
   THEN
      fnd_file.put_line (fnd_file.LOG, SUBSTR ('Error ' || TO_CHAR (SQLCODE) || ': ' || SQLERRM, 1, 255));
END; 
/


CREATE OR REPLACE PACKAGE BODY APPS.xxabrl_sales_trans_offset_pkg AS
/*-----------------------------------------------------------------------------------------------------------------------------------------
   -----------------------------------------------------------------------------------------------------------------------------------------
   -- Filename        : xxabrl_sales_trans_offset_pkg.sql
   --  Description    : ABRL Sales Transaction Register Offset Report
 -- Version     Date            Author              Description
 --  1.0.1       18-Mar-2013    Dhiresh        added new parameters and 3 new columns in existing sales tranasaction report
 -------------------------------------------------------------------------------------------------------------------------------------------
 -------------------------------------------------------------------------------------------------------------------------------------------*/
PROCEDURE xxabrl_sales_trn_offset_proc(ERRBUFF             OUT VARCHAR2,
                                RETCODE             OUT NUMBER,
                                p_cust_num_list     IN VARCHAR2,
                                p_cust_num_from     IN VARCHAR2,
                                p_cust_num_to       IN VARCHAR2,
                                p_trans_type_from   IN VARCHAR2,
                                p_trans_type_to     IN VARCHAR2,
                                p_trans_date_from   IN VARCHAR2,
                                p_trans_date_to     IN VARCHAR2,
                                p_gl_date_from      IN VARCHAR2,
                                p_gl_date_to        IN VARCHAR2,
                                p_can_trans         IN VARCHAR2,
                                p_sbu_code          IN VARCHAR2,
                                P_ORG_ID            IN NUMBER
                               ) AS
-- variable declaration
query_string                     LONG;
TYPE ref_cur IS REF CURSOR;
c                                ref_cur;
CUST_LIST_NUM                   LONG;
CUST_NUM_RANGE                  LONG;
CUST_TRANS_RANGE                LONG;
CUST_TRANS_DATE_RANGE           LONG;
TRAN_GL_DATE                    LONG;
SBU_CODE                        LONG;
CANC_TRAN_STATUS                LONG;
order_by_string                 LONG;
v_trans_date_from               DATE;
v_trans_date_to                 DATE;
v_gl_date_from                  DATE;
v_gl_date_to                    DATE;
V_RESPID                        NUMBER := Fnd_Profile.VALUE('RESP_ID');
LIST_ORG_ID                     LONG;
PARA_ORG_ID                     LONG;
V_ORG_ID                        NUMBER;
inv_count                       NUMBER:=0;
v_group_by                      LONG;
CURSOR CUR_ORG_ID(cp_resp_id NUMBER) IS
     SELECT ORGANIZATION_ID
     FROM FND_PROFILE_OPTION_VALUES fpop,
     FND_PROFILE_OPTIONS fpo,
     PER_SECURITY_ORGANIZATIONS_V  pso
     WHERE fpop.PROFILE_OPTION_ID =fpo.PROFILE_OPTION_ID
     AND level_value=cp_resp_id  --52299 -- resp id
     AND pso.security_profile_id=fpop.PROFILE_OPTION_VALUE
     AND PROFILE_OPTION_NAME='XLA_MO_SECURITY_PROFILE_LEVEL';
TYPE c_rec IS RECORD (  v_operating_unit             org_organization_definitions.ORGANIZATION_NAME%TYPE,
                        v_customer_number         RA_CUSTOMER_TRX_PARTIAL_V.RAC_BILL_TO_CUSTOMER_NUM%TYPE,
                        v_customer_name            RA_CUSTOMER_TRX_PARTIAL_V.RAC_BILL_TO_CUSTOMER_NAME%TYPE,
            v_transaction_source       RA_BATCH_SOURCES_ALL.NAME%TYPE,
                        v_transaction_type           RA_CUSTOMER_TRX_PARTIAL_V.CTT_TYPE_NAME%TYPE,
                        v_transaction_number      RA_CUSTOMER_TRX_PARTIAL_V.TRX_NUMBER%TYPE,
                        v_transaction_date           RA_CUSTOMER_TRX_PARTIAL_V.TRX_DATE%TYPE,
            v_document_number        RA_CUSTOMER_TRX_PARTIAL_V.DOC_SEQUENCE_VALUE%TYPE,
                        v_GL_date                        RA_CUSTOMER_TRX_PARTIAL_V.GD_GL_DATE%TYPE,
                        V_ACCOUNT_CLASS         ra_cust_trx_line_gl_dist_ALL.ACCOUNT_CLASS%TYPE,
                       -- v_transaction_amount    number(30,2),
                       -- v_Line_number               ra_customer_trx_lines_v.LINE_NUMBER%TYPE,
                        v_line_amount                  ra_cust_trx_line_gl_dist_ALL.AMOUNT%TYPE,
                        v_gl_account_combination     gl_code_combinations_kfv.CONCATENATED_SEGMENTS%TYPE,
                        V_GL_DESC                         fnd_flex_values_vl.description%TYPE,
                        v_transaction_status           ar_lookups.meaning%TYPE,
            v_line_description    RA_CUSTOMER_TRX_LINES_ALL.DESCRIPTION%TYPE
                        );
v_rec                    c_rec;
BEGIN
-- query string for the tds
--Umamahesh added the column called  rctp.COMPLETE_FLAG
query_string:=
'SELECT
OOD.ORGANIZATION_NAME Operating_Unit_Name
,HCAA.ACCOUNT_NUMBER Customer_Number
,party.party_name Customer_Name
,RBSA.NAME Transaction_Source
,RCTTA.NAME TRANSACTION_TYPE
,rctp.TRX_NUMBER  Transaction_number
,rctp.TRX_DATE Transaction_Date
,rctp.DOC_SEQUENCE_VALUE Document_Number
,rctlgd.GL_DATE GL_DATE
,rctlgd.ACCOUNT_CLASS
--,DECODE(line_number,1,SUM(rctlgd.AMOUNT),NULL) Transaction_amount
--,rctlgd.LINE_NUMBER
,rctlgd.AMOUNT Transaction_amount
,gcck.CONCATENATED_SEGMENTS GL_Account_Combinition
,FFVV1.description dist_acct_description
,al.meaning Transaction_status
,(select description from RA_CUSTOMER_TRX_LINES_ALL where CUSTOMER_TRX_LINE_ID=rctlgd.CUSTOMER_TRX_LINE_ID) Line_Description
FROM
RA_CUSTOMER_TRX_ALL rctp,
--ra_customer_trx_lines_ALL rctl,
ra_cust_trx_line_gl_dist_ALL rctlgd,
RA_BATCH_SOURCES_ALL RBSA,
gl_code_combinations_kfv gcck,
fnd_flex_value_sets ffv1,
fnd_flex_values_vl ffvv1 ,
org_organization_definitions OOD,
ra_cust_trx_types_all RCTTA,
HZ_CUST_ACCOUNTS_ALL HCAA,
apps.hz_parties party,
ar_lookups al
WHERE  --rctp.CUSTOMER_TRX_ID=rctl.CUSTOMER_TRX_ID
    rctlgd.CUSTOMER_TRX_ID=rctp.CUSTOMER_TRX_ID
    AND rctp.BATCH_SOURCE_ID=RBSA.BATCH_SOURCE_ID
    --  AND rctlgd.CUSTOMER_TRX_LINE_ID= rctl.CUSTOMER_TRX_LINE_ID
    AND gcck.CODE_COMBINATION_ID=rctlgd.CODE_COMBINATION_ID
    AND RCTTA.cust_trx_type_id=RCTP.CUST_TRX_TYPE_ID
    AND HCAA.CUST_ACCOUNT_ID=RCTP.BILL_TO_CUSTOMER_ID
    AND rctp.COMPLETE_FLAG=''Y''                       
    AND al.lookup_type=''INVOICE_TRX_STATUS''
    AND lookup_code=rctp.STATUS_TRX
    AND party.party_id = hcaa.party_id
    AND OOD.ORGANIZATION_ID = rctp.ORG_ID
    AND  RCTTA.org_id=rctp.ORG_ID
    AND gccK.segment6 = ffvv1.flex_value
    AND ffv1.flex_value_set_name = ''ABRL_GL_Account''
    AND ffv1.flex_value_set_id = ffvv1.flex_value_set_id
    AND RBSA.BATCH_SOURCE_TYPE=''INV''';
   /*AND OOD.ORGANIZATION_ID =  ' || P_ORG_ID ;*/
/*
v_group_by:='   GROUP BY
OOD.ORGANIZATION_NAME
,HCAA.ACCOUNT_NUMBER
,HCAA.ACCOUNT_NAME
,RCTTA.NAME
,rctp.TRX_NUMBER
,rctp.TRX_DATE
,rctlgd.GL_DATE
,rctl.LINE_NUMBER
,rctl.EXTENDED_AMOUNT
,gcck.CONCATENATED_SEGMENTS
,al.meaning  ' ;
        */
  v_group_by:= '';
-- settign where conditions according to parameters
CUST_LIST_NUM          :=  ' AND HCAA.ACCOUNT_NUMBER IN ( '|| p_cust_num_list ||') ';
CUST_LIST_NUM          :=  ' AND HCAA.ACCOUNT_NUMBER IN ('''||REPLACE(p_cust_num_list,',',''',''')||''') ';
CUST_NUM_RANGE         :=  ' AND HCAA.ACCOUNT_NUMBER between '''||p_cust_num_from||''' AND '''||p_cust_num_to||''' ';
CUST_TRANS_RANGE       :=  ' AND upper(RCTTA.NAME) between upper('''||p_trans_type_from||''') AND  upper('''||p_trans_type_to||''') ';
-- date is comming in varchar2 from apps so converting it into date
--select to_date(to_char(p_trans_date_from, 'DD-MON-RRRR'),'RRRR/MM/DD:HH24:MI:SS') into v_trans_date_from from dual;
--select to_date(to_char(p_trans_date_to, 'DD-MON-RRRR'),'RRRR/MM/DD:HH24:MI:SS') into v_trans_date_to  from dual;
SELECT TO_DATE(p_trans_date_from,'yyyy/mm/dd:HH24:MI:SS') INTO v_trans_date_from FROM dual;
SELECT TO_DATE(p_trans_date_to,'yyyy/mm/dd:HH24:MI:SS') INTO v_trans_date_to FROM dual;
CUST_TRANS_DATE_RANGE  :=  ' AND rctp.TRX_DATE between '''|| v_trans_date_from||''' AND '''||v_trans_date_to||''' ';
SELECT TO_DATE(p_gl_date_from,'yyyy/mm/dd:HH24:MI:SS') INTO v_gl_date_from FROM dual;
SELECT TO_DATE(p_gl_date_to,'yyyy/mm/dd:HH24:MI:SS') INTO v_gl_date_to FROM dual;
TRAN_GL_DATE           :=  ' AND rctlgd.GL_DATE  between  '''|| v_gl_date_from||''' AND '''||v_gl_date_to||''' ';
CANC_TRAN_STATUS       :=  ' AND al.meaning    =  '||  p_can_trans ||' ';
SBU_CODE    := ' AND gcck.SEGMENT3   =  '|| p_sbu_code  ||' ';
order_by_string        := '  order by  OOD.ORGANIZATION_ID,rctp.TRX_NUMBER,rctlgd.ACCOUNT_CLASS  ';
-- all ou data logic v_org_id stores the profile org_id value
 BEGIN
 SELECT fpop.PROFILE_OPTION_VALUE
        INTO V_ORG_ID
        FROM FND_PROFILE_OPTION_VALUES fpop,
        FND_PROFILE_OPTIONS fpo
        WHERE fpop.PROFILE_OPTION_ID =fpo.PROFILE_OPTION_ID
        AND level_value=V_RESPID --51638
        AND PROFILE_OPTION_NAME='ORG_ID';
EXCEPTION WHEN OTHERS THEN
V_ORG_ID:=NULL;
END;
        -- if profile org_id is not available then it will take all org id
IF v_org_id IS NULL THEN
    -- if parameter org_id is null then it will take all orgs
   IF p_org_id IS NULL THEN
    -- this condition will execute for those responsibily having all ou access
     LIST_ORG_ID:='';
     PARA_ORG_ID  := ' AND    OOD.ORGANIZATION_ID in (' ;
      Fnd_File.put_line (Fnd_File.LOG,'Taking ALL ORGS As running for multi org access RESP where ALL OU data accessible');
     FOR K IN CUR_ORG_ID(V_RESPID) LOOP
         LIST_ORG_ID := LIST_ORG_ID ||','|| K.ORGANIZATION_ID;
         -- this flag will set if data comes from all ou
     END LOOP;
     LIST_ORG_ID := PARA_ORG_ID ||' ' ||SUBSTR(LIST_ORG_ID,2,LENGTH(LIST_ORG_ID)) ||')';
     -- if in multi org resp. ou is selected then only perticular ou data will come
   ELSE
        Fnd_File.put_line (Fnd_File.LOG,'Taking one(parameter org is selected ) ORGS As running for multi org access RESP where ALL OU data accessible');
        LIST_ORG_ID := ' AND    OOD.ORGANIZATION_ID =   ' || P_ORG_ID ||'  ';
   END IF;
ELSE
Fnd_File.put_line (Fnd_File.LOG,'Taking one  ORG As running for single org access RESP where only 1 OU data accessible');
 LIST_ORG_ID := ' AND    OOD.ORGANIZATION_ID =   ' || V_ORG_ID ||'  ';
END IF;
IF (p_cust_num_from IS NULL OR p_cust_num_to IS NULL) AND p_cust_num_list IS NOT NULL THEN
        QUERY_STRING:= QUERY_STRING || CUST_LIST_NUM ;
  ELSIF(p_cust_num_from IS NOT NULL OR p_cust_num_to IS NOT NULL) AND p_cust_num_list IS NULL THEN
        QUERY_STRING:= QUERY_STRING || CUST_NUM_RANGE;
  ELSIF(p_cust_num_from IS NOT NULL OR p_cust_num_to IS NOT NULL) AND p_cust_num_list IS NOT NULL THEN
        QUERY_STRING:= QUERY_STRING || CUST_LIST_NUM;
END IF;
/*IF  p_trans_type_from IS NOT NULL AND p_trans_type_to IS NOT NULL THEN
    QUERY_STRING:= QUERY_STRING || CUST_TRANS_RANGE;
--ELSIF v_trans_date_from IS NOT NULL AND v_trans_date_to IS NOT NULL THEN
ELSIF  p_trans_date_from IS NOT NULL AND  p_trans_date_TO IS NOT NULL THEN
        QUERY_STRING:= QUERY_STRING || CUST_TRANS_DATE_RANGE;
ELSIF   p_gl_date IS NOT NULL THEN
        QUERY_STRING:= QUERY_STRING || TRAN_GL_DATE ;
ELSIF   p_can_trans IS NOT NULL THEN
         QUERY_STRING:= QUERY_STRING || CANC_TRAN_STATUS ;
END IF;*/
--umamahesah modify the code for Trasaction date and Gl date 
IF  p_trans_type_from IS NOT NULL AND p_trans_type_to IS NOT NULL THEN
    QUERY_STRING:= QUERY_STRING || CUST_TRANS_RANGE;
END IF ;  
--ELSIF v_trans_date_from IS NOT NULL AND v_trans_date_to IS NOT NULL THEN
IF  p_trans_date_from IS NOT NULL AND  p_trans_date_TO IS NOT NULL THEN
        QUERY_STRING:= QUERY_STRING || CUST_TRANS_DATE_RANGE;
END IF;
IF  p_gl_date_from IS NOT NULL AND  p_gl_date_TO IS NOT NULL THEN
        QUERY_STRING:= QUERY_STRING || TRAN_GL_DATE;
END IF;
IF   p_can_trans IS NOT NULL THEN
         QUERY_STRING:= QUERY_STRING || CANC_TRAN_STATUS ;
END IF;
IF   p_sbu_code IS NOT NULL THEN
         QUERY_STRING:= QUERY_STRING || SBU_CODE ;
END IF;
query_string :=
         query_string ||  LIST_ORG_ID || v_group_by || order_by_string;
         Fnd_File.put_line (Fnd_File.LOG,QUERY_STRING);
-- parameter printing
Fnd_File.put_line (Fnd_File.output,
                         'ABRL ST Provider (Receivables) Register'
                        );
      Fnd_File.put_line (Fnd_File.output, ' ');
      Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'As Of Date'
                         || CHR (9)
                         || SYSDATE
                         || CHR (9)
                        );
      Fnd_File.put_line (Fnd_File.output, ' ');
      Fnd_File.put_line (Fnd_File.output, 'REPORT PARAMETRS ');
    /*  Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Customer Number'
                         || CHR (9)
                         ||  p_cust_num_list
                         || CHR (9)
                        );
      Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Customer Number From'
                         || CHR (9)
                         ||  p_cust_num_from
                         || CHR (9)
                        );
      Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Customer Number To'
                         || CHR (9)
                         ||  p_cust_num_to
                         || CHR (9)
                        );
      Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Transaction Type From'
                         || CHR (9)
                         ||  p_trans_type_from
                         || CHR (9)
                        );
      Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Transaction Type To'
                         || CHR (9)
                         || p_trans_type_to
                         || CHR (9)
                        );
      Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Transaction Date From'
                         || CHR (9)
                         ||   p_trans_date_from
                         || CHR (9)
                        );
      Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Transaction Date To'
                         || CHR (9)
                         ||  p_trans_date_to
                         || CHR (9)
                        );*/
      Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'GL Date From'
                         || CHR (9)
                         || p_gl_date_from
                         || CHR (9)
                        );
      Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'GL Date To'
                         || CHR (9)
                         || p_gl_date_to
                         || CHR (9)
                        );
      Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'SBU Code'
                         || CHR (9)
                         || p_sbu_code
                         || CHR (9)
                        );
   /*  Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Cancelled Transaction'
                         || CHR (9)
                         || p_can_trans
                         || CHR (9)
                        );*/
-- display labels of the report
      Fnd_File.put_line (Fnd_File.output, ' ');
      Fnd_File.put_line (Fnd_File.output, ' ');
      Fnd_File.put_line (Fnd_File.output,
                           'Operating Unit'
                         || CHR (9)
                         || 'Customer Number'
                         || CHR (9)
                         || 'Customer Name'
                         || CHR (9)
                         || 'Transaction Source'
                         || CHR (9)
                         || 'Transaction Type'
                         || CHR (9)
                         || 'Transaction Number'
                         || CHR (9)
                         || 'Transaction Date'
                         || CHR (9)
                         || 'Document Number '
                         || CHR (9)
                         || 'GL Date'
                         || CHR (9)
                         || 'Account Class'
                         || CHR (9)
                       --  || 'Amount Rs.'
                       --  || CHR (9)
                      --   || 'Line Number'
                       --  || CHR (9)
                         || 'Amount'
                         || CHR (9)
                         || 'GL Account Combination'
                         || CHR (9)
                         || 'GL Account Description'
                         || CHR (9)
                         || 'Transaction Status'
                         || CHR (9)
                         || 'Line Description'
        );
-- set default value 0 to all summery cols.
Fnd_File.put_line (Fnd_File.LOG,QUERY_STRING);
       OPEN c
       FOR query_string;
      LOOP
         FETCH c
          INTO v_rec;
          EXIT WHEN c%NOTFOUND;
          Fnd_File.put_line (Fnd_File.output,
                               v_rec.v_operating_unit
                            || CHR (9)
                            || v_rec.v_customer_number
                            || CHR (9)
                            || v_rec.v_customer_name
                            || CHR (9)
                            || v_rec.v_transaction_source
                            || CHR (9)                            
                || v_rec.v_transaction_type
                            || CHR (9)
                            || v_rec.v_transaction_number
                            || CHR (9)
                            || v_rec.v_transaction_date
                            || CHR (9)
                            || v_rec.v_document_number
                            || CHR (9)
                            || v_rec.v_GL_date
                            || CHR (9)
                            || v_rec.V_ACCOUNT_CLASS
                            || CHR (9)
                         --   || v_rec.v_transaction_amount
                         --   || CHR (9)
                         --   || v_rec.v_Line_number
                         --   || CHR (9)
                            || v_rec.v_line_amount
                            || CHR (9)
                            || v_rec.v_gl_account_combination
                            || CHR (9)
                            || v_rec.V_GL_DESC
                            || CHR (9)
                            || v_rec.v_transaction_status
                            || CHR (9)
                            || v_rec.v_line_description
                      );
       END LOOP;
    CLOSE c;
END xxabrl_sales_trn_offset_proc;
END  xxabrl_sales_trans_offset_pkg; 
/


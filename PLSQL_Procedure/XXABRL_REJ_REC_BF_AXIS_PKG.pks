CREATE OR REPLACE PACKAGE APPS.XXABRL_REJ_REC_BF_AXIS_PKG AS

  /**********************************************************************************************************************************************
                        WIPRO Infotech Ltd, Mumbai, India

            Name        : Payments Rejected Report due to required data missing before inserting to AXIS Payment Table.

            Change Record:
           =========================================================================================================================
           Version   Date          Author               Remarks                  Documnet Ref
           =======   ==========   =============        ============================================================================
           1.0.0     10-Aug-2010    Sayikrishna Reddi  Initial Version
           1.0.1     26-AUG-2010    Sayikrishna Reddi  Added Paytype code in AXIS Invoice table
           1.0.2     08-Sep-2010    Sayikrishna Reddi  Added condition .IFSC code length should be 11.
           1.0.3     19-OCT-2010    Sayikrishna Reddi  In lookup Startdate active condtion added for bank accounts
                                     and RTGS condition added
           1.0.4     25-OCT-2010    Sayikrishna Reddi    Modified Primary Name Column with  Secondary Name and Primary Name Column with  Secondary Name
           1.0.5     12-NOV-2010    Sayikrishna Reddi    Modified DATE Parameters  and Email id with attribute12,13
           1.0.6    02-DEC-2010    Sayikrishna Reddi , Mitul   For RBI Rules we updated limit of ammount now is 2 Lacks  
  ************************************************************************************************************************************************/

PROCEDURE XXABRL_REJ_REC_BF_AXIS_PRC( errbuf     out VARCHAR2
                                  ,  retcode    out VARCHAR2
                                  ,  P_FROM_DATE  VARCHAR2
                                  ,  P_TO_DATE   VARCHAR2
                                  );
END; 
/


CREATE OR REPLACE PACKAGE APPS.xxabrl_inv_details
IS
   PROCEDURE xxabrl_main_pkg (
      errbuf    OUT      VARCHAR2,
      retcode   OUT      NUMBER,
      p_type    IN       VARCHAR2
   );

   PROCEDURE xxabrl_inv_header;

   PROCEDURE xxabrl_inv_lines;

   PROCEDURE xxabrl_inv_distribution;
END xxabrl_inv_details; 
/


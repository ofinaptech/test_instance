CREATE OR REPLACE package APPS.xxabrl_sales_trans_offset_pkg as

procedure xxabrl_sales_trn_offset_proc(ERRBUFF             OUT VARCHAR2,
                                RETCODE             OUT NUMBER,
                                p_cust_num_list     IN VARCHAR2,
                                p_cust_num_from     IN VARCHAR2,
                                p_cust_num_to       IN VARCHAR2,
                                p_trans_type_from   IN VARCHAR2,
                                p_trans_type_to     IN VARCHAR2,
                                p_trans_date_from   IN VARCHAR2,
                                p_trans_date_to     IN VARCHAR2,
                                p_gl_date_from           IN VARCHAR2,
                                p_gl_date_to           IN VARCHAR2,
                                p_can_trans         IN VARCHAR2,
                                p_sbu_code           IN VARCHAR2,
                                P_ORG_ID            IN NUMBER
                               );
end xxabrl_sales_trans_offset_pkg; 
/


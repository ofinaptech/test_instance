CREATE OR REPLACE PROCEDURE APPS.XXABRL_CUSTOMER_REFRESH (
   errbuf    VARCHAR2,
   errcode   NUMBER
)
AS
BEGIN
   DBMS_SNAPSHOT.REFRESH ('XXABRL_CUSTOMER_MV', 'A');
   fnd_file.put_line (fnd_file.LOG, 'Refreshed Successfully');
EXCEPTION
   WHEN OTHERS
   THEN
      fnd_file.put_line (fnd_file.LOG,
                         'Error=>' || SQLCODE || 'ErrorMsg=>' || SQLERRM
                        );
END; 
/


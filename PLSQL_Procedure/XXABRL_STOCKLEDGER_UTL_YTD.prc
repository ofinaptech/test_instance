CREATE OR REPLACE PROCEDURE APPS.XXABRL_STOCKLEDGER_UTL_YTD(ERRBUF  OUT VARCHAR2,
                                                    RETCODE OUT VARCHAR2,
                                               P_FROM_DATE Varchar2 ,
                                                  P_TO_DATE Varchar2
                                                   ) AS
                                                   /*
/**********************************************************************************************************************************************
                        FCS SOLUTON PVT LTD, NOIDA, India
            Name: Stock Ledger O/P for YTD  OR MTD
            Change Record:
           =====================
=========================
=========================
=========================
=========================
           Version   Date          Author                    Remarks                  Documnet Ref
           =======   ==========   ==
===========             ==================  ===============
=========================
=============
           1.0.0     23/08/2013   SAMARPIT/AMIT    Initial Version
          1.0.1     15/11/1016   Lokesh Poojari      Changed the logic for to avoide the Duplicate Amount lines 
                                                            
  ************************************************************************************************************************************************/

 
--Declaring Stock Ledger Detail Local Variables
l_file_p            UTL_FILE.file_type;
P_L_FILE     VARCHAR2 (11):= TO_CHAR (TO_DATE (SUBSTR (P_TO_DATE, 1, 10), 'RRRR/mm/dd'),'MONYYYY');
P_F_DATE     VARCHAR2 (11):= TO_CHAR (TO_DATE (SUBSTR (P_FROM_DATE, 1, 10), 'RRRR/mm/dd'),'DD-MON-YYYY');
P_T_DATE     VARCHAR2 (11):= TO_CHAR (TO_DATE (SUBSTR (P_TO_DATE, 1, 10), 'RRRR/mm/dd'),'DD-MON-YYYY');
CURSOR CUR_STOCKLEDGER
      IS
   SELECT   categories, accounts, sbu, locations, period_name,
         SUM (ofin_amount) ofin_amount, effective_date
    FROM (
SELECT CATEGORIES,accounts, sbu, LOCATIONS, period_name,
       DECODE (CATEGORIES,
               'COGS', dr - cr,
               'IST Transfers', cr - dr,
               'BOM and Item Transfers', cr - dr,
               'Shrikage Shrikage Val', cr - dr,
               'Dump', cr - dr,
               'Shrikage Expiry', cr - dr,
               'Shrikage Damage', cr - dr,
               'Transit Loss', cr - dr,
               'Shrikage Consumable', cr - dr,
               'Migrated Value', cr - dr
              ) ofin_amount,
       effective_date
  FROM (SELECT   DECODE (gcc.segment6,
                         512001, 'COGS',
                         628006, 'COGS',
                         512004, 'COGS',
                         221506, 'IST Transfers',
                         221507, 'BOM and Item Transfers',
                         515001, 'Shrikage Shrikage Val',
                         627009, 'Shrikage Shrikage Val', --added on 05-Dec-2017
                         515002, 'Dump',
                         515003, 'Shrikage Expiry',
                         515004, 'Shrikage Damage',
                         515007, 'Transit Loss',
                         618001, 'Shrikage Consumable',
                         999998, 'Migrated Value'
                        ) CATEGORIES,
                        DECODE (gcc.segment6,
                         512001, '512001',
                         628006, '628006',
                         512004,  '512004',
                         221506, '221506',
                         221507, '221507',
                         515001,  '515001',
                          627009, '627009',--added on 05-Dec-2017
                         515002,  '515002',
                         515003,  '515003',
                         515004, '515004',
                         515007, '515007',
                         618001, '618001',
                         999998, '999998'
                        ) accounts ,
                 glh.period_name, gll.effective_date, gcc.segment3 sbu,
                 gcc.segment4 LOCATIONS, 
                 NVL (SUM (gll.entered_dr), 0) dr,
                 NVL (SUM (gll.entered_cr), 0) cr
            FROM apps.gl_je_headers  glh, apps.gl_je_lines_v gll, apps.gl_code_combinations_kfv  gcc
           WHERE 1 = 1
             AND glh.je_header_id = gll.je_header_id
             AND gll.effective_date BETWEEN P_F_date AND P_T_DATE
             AND glh.je_source = '1'
             AND glh.je_category IN ('21', '22','222')
             and gcc.CODE_COMBINATION_ID=gll.CODE_COMBINATION_ID
             /*Condition For restrict Reversal Entries(for First time Received Entries from Retek Team) on 18-Aug-2017--------->Lokesh*/
              AND NVL (glh.accrual_rev_status, 'NR') <> 'R' 
              /*Condition For restrict Reversal Entries (for Second time reversal Entries from OFIN Team) on 18-Aug-2017------>Lokesh*/ 
              AND glh.description not like '%Reverse%RETEK%' 
        GROUP BY glh.je_category,
                  glh.period_name,
                  gll.effective_date,
                  gcc.segment3,
                  gcc.segment4,
                  gcc.segment6
                 ORDER BY glh.je_category) aa
 WHERE CATEGORIES IS NOT NULL
UNION ALL
SELECT CATEGORIES,accounts, sbu, LOCATIONS, period_name,
       DECODE (CATEGORIES,
               'Purchase', cr - dr,
               'WAC and Cost Variance', dr - cr
              ) ofin_amount,
       effective_date
  FROM (SELECT   DECODE (gcc.segment6,
                         361905, 'Purchase',
                         513005, 'WAC and Cost Variance',
                         513006, 'WAC and Cost Variance'
                        ) CATEGORIES,
                         DECODE (gcc.segment6,
                         361905, '361905',
                         513005,  '513005',
                         513006, '513006'
                        ) accounts,gcc.segment3 sbu,
                 glh.period_name, 
                 gcc.segment4 LOCATIONS,
                 NVL (SUM (gll.entered_dr), 0) dr,
                 NVL (SUM (gll.entered_cr), 0) cr,gll.effective_date
            FROM apps.gl_je_headers  glh, apps.gl_je_lines  gll,apps.gl_code_combinations_kfv  gcc
           WHERE 1 = 1
             AND glh.je_header_id = gll.je_header_id
             AND gll.effective_date BETWEEN P_F_DATE AND P_T_DATE
             AND glh.je_source = '1'
             AND glh.je_category = 'Inventory'
             and gcc.CODE_COMBINATION_ID=gll.CODE_COMBINATION_ID
             /*Condition For restrict Reversal Entries(for First time Received Entries from Retek Team) on 18-Aug-2017--------->Lokesh*/
              AND NVL (glh.accrual_rev_status, 'NR') <> 'R' 
              /*Condition For restrict Reversal Entries (for Second time reversal Entries from OFIN Team) on 18-Aug-2017------>Lokesh*/ 
              AND glh.description not like '%Reverse%RETEK%' 
        GROUP BY glh.je_category,
                  glh.period_name,
                  gll.effective_date,
                  gcc.segment3,
                  gcc.segment4,
                  gcc.segment6
        ORDER BY glh.je_category) aa
 WHERE CATEGORIES IS NOT NULL
UNION ALL
SELECT CATEGORIES, ACCOUNTS,Sbu, LOCATIONS, period_name,
       DECODE (CATEGORIES, 'Transfer IN', dr) ofin_amount, effective_date
  FROM (SELECT   DECODE (gcc.segment6,
                         221101, 'Transfer IN',
                         221106, 'Transfer IN'
                        ) CATEGORIES,
                        DECODE (gcc.segment6,
                         221101, '221101',
                         221106,  '221106'
                        ) ACCOUNTS,gcc.segment3 sbu, gcc.segment4 LOCATIONS,
                        glh.period_name, 
                 NVL (SUM (gll.entered_dr), 0) dr,
                 NVL (SUM (gll.entered_cr), 0) cr,
                 gll.effective_date
            FROM apps.gl_je_headers glh, apps.gl_je_lines gll,apps.gl_code_combinations_kfv  gcc
           WHERE 1 = 1
             AND glh.je_header_id = gll.je_header_id
             AND gll.effective_date BETWEEN P_F_DATE AND P_T_DATE
             AND glh.je_source = '1'
             AND glh.je_category IN ('Transfer', 'Intercompany Transfer')
             and gcc.CODE_COMBINATION_ID=gll.CODE_COMBINATION_ID
             /*Condition For restrict Reversal Entries(for First time Received Entries from Retek Team) on 18-Aug-2017--------->Lokesh*/
              AND NVL (glh.accrual_rev_status, 'NR') <> 'R' 
              /*Condition For restrict Reversal Entries (for Second time reversal Entries from OFIN Team) on 18-Aug-2017------>Lokesh*/ 
              AND glh.description not like '%Reverse%RETEK%' 
        GROUP BY glh.je_category,
                  glh.period_name,
                  gll.effective_date,
                  gcc.segment3,
                  gcc.segment4,
                  gcc.segment6
        ORDER BY glh.je_category) aa
 WHERE CATEGORIES IS NOT NULL
UNION ALL
SELECT CATEGORY,ACCOUNTS, sbu, LOCATIONS, period_name,
       DECODE (CATEGORY, 'Transfer OUT', cr) ofin_amount, effective_date
  FROM (SELECT   DECODE (gcc.segment6,
                         221101, 'Transfer OUT',
                         221106, 'Transfer OUT'
                        ) CATEGORY,
                        DECODE (gcc.segment6,
                         221101, '221101',
                         221106, '221106'
                        ) ACCOUNTS,gcc.segment3 sbu,gcc.segment4 LOCATIONS,
                 glh.period_name, 
                 NVL (SUM (gll.entered_dr), 0) dr,
                 NVL (SUM (gll.entered_cr), 0) cr,gll.effective_date
            FROM apps.gl_je_headers  glh, apps.gl_je_lines  gll,apps.gl_code_combinations_kfv  gcc
           WHERE 1 = 1
             AND glh.je_header_id = gll.je_header_id
             AND gll.effective_date BETWEEN P_F_DATE AND P_T_DATE
             AND glh.je_source = '1'
             AND glh.je_category IN ('Transfer', 'Intercompany Transfer')
              and gcc.CODE_COMBINATION_ID=gll.CODE_COMBINATION_ID
              /*Condition For restrict Reversal Entries(for First time Received Entries from Retek Team) on 18-Aug-2017-------->Lokesh*/
              AND NVL (glh.accrual_rev_status, 'NR') <> 'R' 
              /*Condition For restrict Reversal Entries (for Second time reversal Entries from OFIN Team) on 18-Aug-2017------>Lokesh*/ 
              AND glh.description not like '%Reverse%RETEK%'         
        GROUP BY glh.je_category,
                  glh.period_name,
                  gll.effective_date,
                  gcc.segment3,
                  gcc.segment4,
                  gcc.segment6
        ORDER BY glh.je_category) aa
 WHERE CATEGORY IS NOT NULL)bb
  GROUP BY categories, accounts, sbu, locations, period_name, effective_date;
         BEGIN          
          --Creating a file for the below generated Stock Ledger data
            l_file_p := UTL_FILE.fopen ('STOCK_LEDGER_RECO', 'ABRL_OFIN_Stock_Ledger_'||P_L_FILE||'_YTD_'||SYSDATE||'.TXT', 'W');
            utl_file.put_line( l_file_p,  'CATEGORIES'||'|'|| 
                              'ACCOUNTS'||'|'||                                 
                              'SBU'||'|'||
                              'LOCATIONS'||'|'|| 
                              'PERIOD_NAME'||'|'|| 
                              'OFIN_AMOUNT'||'|'||
                              'EFFECTIVE_DATE'
                               );
            FOR REC_STOCKLEDGER IN CUR_STOCKLEDGER
            LOOP
              utl_file.put_line( l_file_p,
                       REC_STOCKLEDGER.CATEGORIES
                  || '|'
                  || REC_STOCKLEDGER.accounts
                  || '|'
                  || REC_STOCKLEDGER.sbu
                  || '|'
                  ||  REC_STOCKLEDGER.LOCATIONS
                  || '|' 
                  || REC_STOCKLEDGER.period_name
                  || '|'                                  
                  || REC_STOCKLEDGER.ofin_amount
                  || '|'
                  || REC_STOCKLEDGER.effective_date);
               
               END LOOP;
            
  
         EXCEPTION
                WHEN NO_DATA_FOUND
                THEN
                   UTL_FILE.fclose (l_file_p);
                   DBMS_OUTPUT.PUT_LINE( 'No Data Found'); 
                WHEN UTL_FILE.invalid_path
                THEN
                   UTL_FILE.fclose (l_file_p);
                   DBMS_OUTPUT.PUT_LINE( 'Invalid Path');
                WHEN UTL_FILE.invalid_filehandle
                THEN
                   UTL_FILE.fclose (l_file_p);
                   DBMS_OUTPUT.PUT_LINE( 'Invalid File Handle');
                WHEN UTL_FILE.invalid_operation
                THEN
                   UTL_FILE.fclose (l_file_p);
                   DBMS_OUTPUT.PUT_LINE( 'Invalid Operation');
                WHEN UTL_FILE.read_error
                THEN
                   UTL_FILE.fclose (l_file_p);
                   DBMS_OUTPUT.PUT_LINE( 'Read Error');
                WHEN UTL_FILE.write_error
                THEN
                   UTL_FILE.fclose (l_file_p);
                   DBMS_OUTPUT.PUT_LINE( 'Write Error');
                WHEN UTL_FILE.internal_error
                THEN
                   UTL_FILE.fclose (l_file_p);
                   DBMS_OUTPUT.PUT_LINE( 'Internal Error');
                WHEN UTL_FILE.file_open
                THEN
                   UTL_FILE.fclose (l_file_p);
                   DBMS_OUTPUT.PUT_LINE( 'File is Open');
                WHEN UTL_FILE.invalid_filename
                THEN
                   UTL_FILE.fclose (l_file_p);
                   DBMS_OUTPUT.PUT_LINE( 'Invalid File Name');
                   WHEN OTHERS
                THEN
                   UTL_FILE.fclose (l_file_P);
                   DBMS_OUTPUT.PUT_LINE( 'Unknown Error' || SQLERRM);

                   UTL_FILE.fclose (l_file_p);      
 END XXABRL_STOCKLEDGER_UTL_YTD; 
/


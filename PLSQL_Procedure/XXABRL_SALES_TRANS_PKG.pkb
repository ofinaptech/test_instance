CREATE OR REPLACE PACKAGE BODY APPS.xxabrl_sales_trans_pkg
AS
/*-----------------------------------------------------------------------------------------------------------------------------------------
   -----------------------------------------------------------------------------------------------------------------------------------------
   -- Filename        : xxabrl_sales_trans_pkg.sql
   --  Description    : ABRL Sales Transaction Register Offset Report
 -- Version     Date            Author              Modification
 --  1.0.1       5-jan-2011     Umamahesh           change the code for Transaction date and Gl Date and added the Column "Complete flag" in where clause
 --10.1  18-jan-18  --Cust GSTIN added by Govind
 -------------------------------------------------------------------------------------------------------------------------------------------
 -------------------------------------------------------------------------------------------------------------------------------------------*/
   PROCEDURE xxabrl_sales_trn_proc (
      errbuff             OUT      VARCHAR2,
      retcode             OUT      NUMBER,
      p_cust_num_list     IN       VARCHAR2,
      p_cust_num_from     IN       VARCHAR2,
      p_cust_num_to       IN       VARCHAR2,
      p_trans_type_from   IN       VARCHAR2,
      p_trans_type_to     IN       VARCHAR2,
      p_trans_date_from   IN       VARCHAR2,
      p_trans_date_to     IN       VARCHAR2,
      p_gl_date           IN       VARCHAR2,
      p_can_trans         IN       VARCHAR2,
      p_org_id            IN       NUMBER
   )
   AS
-- variable declaration
      query_string            LONG;

      TYPE ref_cur IS REF CURSOR;

      c                       ref_cur;
      cust_list_num           LONG;
      cust_num_range          LONG;
      cust_trans_range        LONG;
      cust_trans_date_range   LONG;
      tran_gl_date            LONG;
      canc_tran_status        LONG;
      order_by_string         LONG;
      v_trans_date_from       DATE;
      v_trans_date_to         DATE;
      v1_gl_date              DATE;
      v_respid                NUMBER  := fnd_profile.VALUE ('RESP_ID');
      list_org_id             LONG;
      para_org_id             LONG;
      v_org_id                NUMBER;
      inv_count               NUMBER  := 0;
      v_group_by              LONG;

      CURSOR cur_org_id (cp_resp_id NUMBER)
      IS
         SELECT organization_id
           FROM fnd_profile_option_values fpop,
                fnd_profile_options fpo,
                per_security_organizations_v pso
          WHERE fpop.profile_option_id = fpo.profile_option_id
            AND level_value = cp_resp_id                    --52299 -- resp id
            AND pso.security_profile_id = fpop.profile_option_value
            AND profile_option_name = 'XLA_MO_SECURITY_PROFILE_LEVEL';

      TYPE c_rec IS RECORD (
         v_operating_unit           org_organization_definitions.organization_name%TYPE,
         v_customer_number          ra_customer_trx_partial_v.rac_bill_to_customer_num%TYPE,
         v_customer_name            ra_customer_trx_partial_v.rac_bill_to_customer_name%TYPE,
         v_transaction_type         ra_customer_trx_partial_v.ctt_type_name%TYPE,
         v_transaction_number       ra_customer_trx_partial_v.trx_number%TYPE,
         v_transaction_date         ra_customer_trx_partial_v.trx_date%TYPE,
         v_gl_date                  ra_customer_trx_partial_v.gd_gl_date%TYPE,
         v_account_class            ra_cust_trx_line_gl_dist_all.account_class%TYPE,
         -- v_transaction_amount         number(30,2),
         -- v_Line_number                ra_customer_trx_lines_v.LINE_NUMBER%TYPE,
         v_line_amount              ra_cust_trx_line_gl_dist_all.amount%TYPE,
         v_gl_account_combination   gl_code_combinations_kfv.concatenated_segments%TYPE,
         v_gl_desc                  fnd_flex_values_vl.description%TYPE,
         v_transaction_status       ar_lookups.meaning%TYPE,
         cust_gstin                 VARCHAR2 (2000)
      );

      v_rec                   c_rec;
   BEGIN
-- query string for the tds
--Umamahesh added the column called  rctp.COMPLETE_FLAG
      query_string :=
         'SELECT
OOD.ORGANIZATION_NAME Operating_Unit_Name
,HCAA.ACCOUNT_NUMBER Customer_Number
,party.party_name Customer_Name
,RCTTA.NAME TRANSACTION_TYPE
,rctp.TRX_NUMBER  Transaction_number
,rctp.TRX_DATE Transaction_Date
,rctlgd.GL_DATE GL_DATE
,rctlgd.ACCOUNT_CLASS
--,DECODE(line_number,1,SUM(rctlgd.AMOUNT),NULL) Transaction_amount
--,rctlgd.LINE_NUMBER
,rctlgd.AMOUNT Transaction_amount
,gcck.CONCATENATED_SEGMENTS GL_Account_Combinition
,FFVV1.description dist_acct_description
,al.meaning Transaction_status,
 (SELECT DISTINCT registration_number
                                             FROM apps.jai_party_reg_lines
                                            WHERE party_reg_id IN (
                                                     SELECT jpr.party_reg_id
                                                       FROM apps.jai_party_regs jpr,
                                                            apps.jai_party_reg_lines jprl,
                                                            apps.hz_cust_site_uses_all hcsua,
                                                            apps.hz_cust_acct_sites_all hcaa
                                                      WHERE jpr.party_id =
                                                               rctp.bill_to_customer_id
                                                        AND rctp.bill_to_site_use_id =
                                                               hcsua.site_use_id
                                                        AND hcsua.cust_acct_site_id =
                                                               hcaa.cust_acct_site_id
                                                        AND hcaa.orig_system_reference =
                                                               to_char(jpr.party_site_id)
                                                        AND jpr.party_reg_id =
                                                               jprl.party_reg_id
                                                        AND jprl.effective_to IS NULL
                                                        and rownum<=1
                                                        AND jpr.org_id =
                                                                   rctp.org_id)
                                              AND registration_type_code =
                                                                       ''GSTIN''
                                              AND effective_to IS NULL)
                                                                   cust_gstin
FROM
RA_CUSTOMER_TRX_ALL rctp,
--ra_customer_trx_lines_ALL rctl,
ra_cust_trx_line_gl_dist_ALL rctlgd,
gl_code_combinations_kfv gcck,
fnd_flex_value_sets ffv1,
fnd_flex_values_vl ffvv1 ,
org_organization_definitions OOD,
ra_cust_trx_types_all RCTTA,
HZ_CUST_ACCOUNTS_ALL HCAA,
apps.hz_parties party,
ar_lookups al
WHERE  --rctp.CUSTOMER_TRX_ID=rctl.CUSTOMER_TRX_ID
 rctlgd.CUSTOMER_TRX_ID=rctp.CUSTOMER_TRX_ID
--AND rctlgd.CUSTOMER_TRX_LINE_ID= rctl.CUSTOMER_TRX_LINE_ID
AND gcck.CODE_COMBINATION_ID=rctlgd.CODE_COMBINATION_ID
AND RCTTA.cust_trx_type_id=RCTP.CUST_TRX_TYPE_ID
AND HCAA.CUST_ACCOUNT_ID=RCTP.BILL_TO_CUSTOMER_ID
and rctp.COMPLETE_FLAG=''Y''                       
AND al.lookup_type=''INVOICE_TRX_STATUS''
AND lookup_code=rctp.STATUS_TRX
AND party.party_id = hcaa.party_id
AND OOD.ORGANIZATION_ID = rctp.ORG_ID
 AND  RCTTA.org_id=rctp.ORG_ID
AND gccK.segment6 = ffvv1.flex_value
   AND ffv1.flex_value_set_name = ''ABRL_GL_Account''
     AND ffv1.flex_value_set_id = ffvv1.flex_value_set_id';
   /*AND OOD.ORGANIZATION_ID =  ' || P_ORG_ID ;*/
/*
v_group_by:='   GROUP BY
OOD.ORGANIZATION_NAME
,HCAA.ACCOUNT_NUMBER
,HCAA.ACCOUNT_NAME
,RCTTA.NAME
,rctp.TRX_NUMBER
,rctp.TRX_DATE
,rctlgd.GL_DATE
,rctl.LINE_NUMBER
,rctl.EXTENDED_AMOUNT
,gcck.CONCATENATED_SEGMENTS
,al.meaning  ' ;
        */
      v_group_by := '';
-- settign where conditions according to parameters
      cust_list_num :=
                  ' AND HCAA.ACCOUNT_NUMBER IN ( ' || p_cust_num_list || ') ';
      cust_list_num :=
            ' AND HCAA.ACCOUNT_NUMBER IN ('''
         || REPLACE (p_cust_num_list, ',', ''',''')
         || ''') ';
      cust_num_range :=
            ' AND HCAA.ACCOUNT_NUMBER between '''
         || p_cust_num_from
         || ''' AND '''
         || p_cust_num_to
         || ''' ';
      cust_trans_range :=
            ' AND upper(RCTTA.NAME) between upper('''
         || p_trans_type_from
         || ''') AND  upper('''
         || p_trans_type_to
         || ''') ';

-- date is comming in varchar2 from apps so converting it into date
--select to_date(to_char(p_trans_date_from, 'DD-MON-RRRR'),'RRRR/MM/DD:HH24:MI:SS') into v_trans_date_from from dual;
--select to_date(to_char(p_trans_date_to, 'DD-MON-RRRR'),'RRRR/MM/DD:HH24:MI:SS') into v_trans_date_to  from dual;
      SELECT TO_DATE (p_trans_date_from, 'yyyy/mm/dd:HH24:MI:SS')
        INTO v_trans_date_from
        FROM DUAL;

      SELECT TO_DATE (p_trans_date_to, 'yyyy/mm/dd:HH24:MI:SS')
        INTO v_trans_date_to
        FROM DUAL;

      cust_trans_date_range :=
            ' AND rctp.TRX_DATE between '''
         || v_trans_date_from
         || ''' AND '''
         || v_trans_date_to
         || ''' ';

      SELECT TO_DATE (p_gl_date, 'YYYY/MM/DD:HH24:MI:SS')
        INTO v1_gl_date
        FROM DUAL;

      tran_gl_date :=
            ' AND rctlgd.GL_DATE =  '''
         || TO_DATE (p_gl_date, 'yyyy/mm/dd:HH24:MI:SS')
         || ''' ';
      canc_tran_status := ' AND al.meaning    =  ' || p_can_trans || ' ';
      order_by_string :=
         '  order by  OOD.ORGANIZATION_ID,rctp.TRX_NUMBER,rctlgd.ACCOUNT_CLASS  ';

-- all ou data logic v_org_id stores the profile org_id value
      BEGIN
         SELECT fpop.profile_option_value
           INTO v_org_id
           FROM fnd_profile_option_values fpop, fnd_profile_options fpo
          WHERE fpop.profile_option_id = fpo.profile_option_id
            AND level_value = v_respid                                 --51638
            AND profile_option_name = 'ORG_ID';
      EXCEPTION
         WHEN OTHERS
         THEN
            v_org_id := NULL;
      END;

      -- if profile org_id is not available then it will take all org id
      IF v_org_id IS NULL
      THEN
         -- if parameter org_id is null then it will take all orgs
         IF p_org_id IS NULL
         THEN
            -- this condition will execute for those responsibily having all ou access
            list_org_id := '';
            para_org_id := ' AND    OOD.ORGANIZATION_ID in (';
            fnd_file.put_line
               (fnd_file.LOG,
                'Taking ALL ORGS As running for multi org access RESP where ALL OU data accessible'
               );

            FOR k IN cur_org_id (v_respid)
            LOOP
               list_org_id := list_org_id || ',' || k.organization_id;
            -- this flag will set if data comes from all ou
            END LOOP;

            list_org_id :=
                  para_org_id
               || ' '
               || SUBSTR (list_org_id, 2, LENGTH (list_org_id))
               || ')';
         -- if in multi org resp. ou is selected then only perticular ou data will come
         ELSE
            fnd_file.put_line
               (fnd_file.LOG,
                'Taking one(parameter org is selected ) ORGS As running for multi org access RESP where ALL OU data accessible'
               );
            list_org_id :=
                        ' AND    OOD.ORGANIZATION_ID =   ' || p_org_id || '  ';
         END IF;
      ELSE
         fnd_file.put_line
            (fnd_file.LOG,
             'Taking one  ORG As running for single org access RESP where only 1 OU data accessible'
            );
         list_org_id := ' AND    OOD.ORGANIZATION_ID =   ' || v_org_id || '  ';
      END IF;

      IF     (p_cust_num_from IS NULL OR p_cust_num_to IS NULL)
         AND p_cust_num_list IS NOT NULL
      THEN
         query_string := query_string || cust_list_num;
      ELSIF     (p_cust_num_from IS NOT NULL OR p_cust_num_to IS NOT NULL)
            AND p_cust_num_list IS NULL
      THEN
         query_string := query_string || cust_num_range;
      ELSIF     (p_cust_num_from IS NOT NULL OR p_cust_num_to IS NOT NULL)
            AND p_cust_num_list IS NOT NULL
      THEN
         query_string := query_string || cust_list_num;
      END IF;

/*IF  p_trans_type_from IS NOT NULL AND p_trans_type_to IS NOT NULL THEN
    QUERY_STRING:= QUERY_STRING || CUST_TRANS_RANGE;
--ELSIF v_trans_date_from IS NOT NULL AND v_trans_date_to IS NOT NULL THEN
ELSIF  p_trans_date_from IS NOT NULL AND  p_trans_date_TO IS NOT NULL THEN
        QUERY_STRING:= QUERY_STRING || CUST_TRANS_DATE_RANGE;
ELSIF   p_gl_date IS NOT NULL THEN
        QUERY_STRING:= QUERY_STRING || TRAN_GL_DATE ;
ELSIF   p_can_trans IS NOT NULL THEN
         QUERY_STRING:= QUERY_STRING || CANC_TRAN_STATUS ;
END IF;*/
--umamahesah modify the code for Trasaction date and Gl date
      IF p_trans_type_from IS NOT NULL AND p_trans_type_to IS NOT NULL
      THEN
         query_string := query_string || cust_trans_range;
      END IF;

--ELSIF v_trans_date_from IS NOT NULL AND v_trans_date_to IS NOT NULL THEN
      IF p_trans_date_from IS NOT NULL AND p_trans_date_to IS NOT NULL
      THEN
         query_string := query_string || cust_trans_date_range;
      END IF;

      IF p_gl_date IS NOT NULL
      THEN
         query_string := query_string || tran_gl_date;
      END IF;

      IF p_can_trans IS NOT NULL
      THEN
         query_string := query_string || canc_tran_status;
      END IF;

      query_string :=
                  query_string || list_org_id || v_group_by || order_by_string;
      fnd_file.put_line (fnd_file.LOG, query_string);
-- parameter printing
      fnd_file.put_line (fnd_file.output,
                         'ABRL Sales Tran Register offset Report'
                        );
      fnd_file.put_line (fnd_file.output, ' ');
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'As Of Date'
                         || CHR (9)
                         || SYSDATE
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output, ' ');
      fnd_file.put_line (fnd_file.output, 'REPORT PARAMETRS ');
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Customer Number'
                         || CHR (9)
                         || p_cust_num_list
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Customer Number From'
                         || CHR (9)
                         || p_cust_num_from
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Customer Number To'
                         || CHR (9)
                         || p_cust_num_to
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Transaction Type From'
                         || CHR (9)
                         || p_trans_type_from
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Transaction Type To'
                         || CHR (9)
                         || p_trans_type_to
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Transaction Date From'
                         || CHR (9)
                         || p_trans_date_from
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Transaction Date To'
                         || CHR (9)
                         || p_trans_date_to
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'GL Date'
                         || CHR (9)
                         || p_gl_date
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Cancelled Transaction'
                         || CHR (9)
                         || p_can_trans
                         || CHR (9)
                        );
-- display labels of the report
      fnd_file.put_line (fnd_file.output, ' ');
      fnd_file.put_line (fnd_file.output, ' ');
      fnd_file.put_line (fnd_file.output,
                            'Operating Unit'
                         || CHR (9)
                         || 'Customer Number'
                         || CHR (9)
                         || 'Customer Name'
                         || CHR (9)
                         || 'Transaction Type'
                         || CHR (9)
                         || 'Transaction Number'
                         || CHR (9)
                         || 'Transaction Date'
                         || CHR (9)
                         || 'GL Date'
                         || CHR (9)
                         || 'Account Class'
                         || CHR (9)
                         --  || 'Amount Rs.'
                         --  || CHR (9)
                                    --   || 'Line Number'
                         --  || CHR (9)
                         || 'Amount'
                         || CHR (9)
                         || 'GL Account Combination'
                         || CHR (9)
                         || 'GL Account Description'
                         || CHR (9)
                         || 'Transaction Status'
                         || CHR (9)
                         || 'GST Reg No'
                        );
-- set default value 0 to all summery cols.
      fnd_file.put_line (fnd_file.LOG, query_string);

      OPEN c FOR query_string;

      LOOP
         FETCH c
          INTO v_rec;

         EXIT WHEN c%NOTFOUND;
         fnd_file.put_line (fnd_file.output,
                               v_rec.v_operating_unit
                            || CHR (9)
                            || v_rec.v_customer_number
                            || CHR (9)
                            || v_rec.v_customer_name
                            || CHR (9)
                            || v_rec.v_transaction_type
                            || CHR (9)
                            || v_rec.v_transaction_number
                            || CHR (9)
                            || v_rec.v_transaction_date
                            || CHR (9)
                            || v_rec.v_gl_date
                            || CHR (9)
                            || v_rec.v_account_class
                            || CHR (9)
                            --   || v_rec.v_transaction_amount
                            --   || CHR (9)
                            --   || v_rec.v_Line_number
                            --   || CHR (9)
                            || v_rec.v_line_amount
                            || CHR (9)
                            || v_rec.v_gl_account_combination
                            || CHR (9)
                            || v_rec.v_gl_desc
                            || CHR (9)
                            || v_rec.v_transaction_status
                            || CHR (9)
                            || v_rec.cust_gstin
                           );
      END LOOP;

      CLOSE c;
   END xxabrl_sales_trn_proc;
END xxabrl_sales_trans_pkg; 
/


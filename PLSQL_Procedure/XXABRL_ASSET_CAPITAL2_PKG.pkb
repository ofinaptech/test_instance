CREATE OR REPLACE PACKAGE BODY APPS.XXABRL_ASSET_CAPITAL2_PKG AS


PROCEDURE XXABRL_ASSET_CAPITAL2_PROC( errbuf VARCHAR2
                                     ,retcode NUMBER
                                     ,P_BOOK_CODE        FA_MASS_ADDITIONS.BOOK_TYPE_CODE%TYPE
                     ,P_DATE_FROM        VARCHAR2 --FA_BOOKS.DATE_PLACED_IN_SERVICE%TYPE
                         ,P_DATE_TO          VARCHAR2 --FA_BOOKS.DATE_PLACED_IN_SERVICE%TYPE
                                     ,P_INVOICE_NUM      FA_MASS_ADDITIONS.INVOICE_NUMBER%TYPE
                                             ,dummy              NUMBER
                                     ,P_ASSET_CATEGORY   FA_MASS_ADDITIONS.ASSET_CATEGORY_ID%TYPE
                                     ,P_STATUS           FA_MASS_ADDITIONS.POSTING_STATUS%TYPE
                                   )
IS

CURSOR cur_data --(p_fdate DATE, p_tdate DATE)
IS


SELECT fm.MASS_ADDITION_ID
         ,fm.description
         ,fm.MANUFACTURER_NAME
         ,fm.DATE_PLACED_IN_SERVICE
         ,fm.FIXED_ASSETS_COST
         ,fm.FIXED_ASSETS_UNITS
         ,fm.REVIEWER_COMMENTS
         ,fm.INVOICE_NUMBER
         ,fm.VENDOR_NUMBER
         ,fm.PO_NUMBER
         ,fc.segment1 major
         ,fc.segment2 minor1
             ,fc.segment3 minor2
             ,gl.segment1 seg1_co
             ,gl.SEGMENT2 seg2_dept
             ,gl.segment3 seg3_state_sbu
             ,gl.segment4  seg4_loc
             ,gl.segment5 seg5_merchandize
             ,gl.segment6 seg6_account
             ,gl.segment7 seg7_intercomp
             ,gl.segment8 seg8_future
             ,TO_CHAR(fl.segment1) location_codes
         ,fmd.location_id
             ,fk.segment1 l_YEAR
             ,fk.segment2 project_number
             ,fm.posting_status
             ,fm.queue_name
FROM        fa_mass_additions        fm
           ,fa_massadd_distributions fmd
       ,gl_code_combinations     gl
       ,fa_categories_b          fc
       ,fa_locations             fl
       ---, gl_code_combinations     gl1
       ,fa_asset_keywords fk
    WHERE fm.POSTING_STATUS =  DECODE (P_STATUS,'NEW','NEW','OH HOLD','ON HOLD','BOTH',fm.POSTING_STATUS,fm.POSTING_STATUS)
    AND   fm.POSTING_STATUS IN ( 'NEW','ON HOLD')
  AND   fm.mass_addition_id = fmd.mass_addition_id
    AND   fm.book_type_code = NVL(P_BOOK_CODE,fm.book_type_code)
    AND   decode(P_INVOICE_NUM,null,'1',fm.invoice_number)=NVL(P_INVOICE_NUM,'1')
    AND   gl.code_combination_id(+) = fmd.deprn_expense_ccid  --fm.PAYABLES_CODE_COMBINATION_ID
--	AND   NVL(fc.category_id,0)= (NVL(P_ASSET_CATEGORY, 0 ))--AND   fc.category_id = NVL(P_ASSET_CATEGORY, fc.category_id)
	AND   DECODE(P_ASSET_CATEGORY,NULL,1,fc.category_id)=DECODE(P_ASSET_CATEGORY,NULL,1,P_ASSET_CATEGORY)
	AND   fc.category_id(+) = fm.asset_category_id
/*	AND   fm.date_placed_in_service BETWEEN NVL(TO_DATE(P_DATE_FROM,'YYYY/MM/DD HH24:MI:SS')  ,fm.date_placed_in_service)
		                        AND NVL(TO_DATE(P_DATE_TO,'YYYY/MM/DD HH24:MI:SS')  ,fm.date_placed_in_service)*/
  AND   decode(P_DATE_FROM,null,'01-JAN-2009',fm.date_placed_in_service)
	BETWEEN NVL(TO_CHAR(TO_DATE(P_DATE_FROM,'dd-mon-yy')),'01-JAN-2009')
		                        AND NVL(TO_CHAR(TO_DATE(P_DATE_TO,'dd-mon-yy')),'01-JAN-2009')
	AND   fmd.location_id = fl.location_id(+)
      ---  AND gl1.code_combination_id = fk.code_combination_id  
        AND fk.code_combination_id(+) = fm.asset_key_ccid;


BEGIN


       Fnd_File.put_line(Fnd_File.OUTPUT,CHR(9)            ||'ABRL Asset Capitalization and Transfer Template');
       Fnd_File.put_line(Fnd_File.OUTPUT,CHR(9)            ||' ');
      Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || 'Report Date:'
                         || CHR (9)
                         || SYSDATE
                         || CHR (9)
                        );
      Fnd_File.put_line (Fnd_File.output, ' ');
      Fnd_File.put_line (Fnd_File.output, 'REPORT PARAMETRS ');
      Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || 'BOOK TYPE CODE:'
                         || CHR (9)
                         || P_BOOK_CODE
                         || CHR (9)
                        );
        Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || 'START DATE:'
                         || CHR (9)
                         || P_DATE_FROM
                         || CHR (9)
                        );
        Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || 'END DATE:'
                         || CHR (9)
                         || P_DATE_TO
                         || CHR (9)
                        );
        Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || 'INVOICE NUMBER:'
                         || CHR (9)
                         || P_INVOICE_NUM
                         || CHR (9)
                        );
        Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || 'ASSET CATEGORY:'
                         || CHR (9)
                         || P_ASSET_CATEGORY
                         || CHR (9)
                        );
        Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || 'STATUS:'
                         || CHR (9)
                         || P_STATUS
                         || CHR (9)
                        );
    Fnd_File.put_line (Fnd_File.output, ' ');


       --Printing Output
       Fnd_File.put_line(Fnd_File.OUTPUT,                     'Ref No.'||CHR(9)
                                                       ||'Description'||CHR(9)
                               ||'Major'||CHR(9)
                               ||'Minor1'||CHR(9)
                               ||'Minor2'||CHR(9)
                                                       ||'Manufacturers Name'||CHR(9)
                               ||'Date Placed in Service'||CHR(9)
                                      ||'Fixed Asset Cost ' ||CHR(9)
                                      ||'Fixed Asset Units'||CHR(9)
                               ||'CO'||CHR(9)
                               ||'Dept'||CHR(9)
                               ||'State-SBU'||CHR(9)
                               ||'Location'||CHR(9)
                               ||'MERCHANDIZE'||CHR(9)
                               ||'Account'||CHR(9)
                               ||'InterCompany'||CHR(9)
                               ||'Future'||CHR(9)
                               ||'Location Codes'||CHR(9)
                                   ||'Reviewers Comment'||CHR(9)
                               ||'Invoice Number'||CHR(9)
                                ||'PO Vendor Number'||CHR(9)
                                      ||'PO Number'||CHR(9)
                               ||'Year'||CHR(9)
                               ||'Project Number'||CHR(9)
                 ||'Posting Status'||CHR(9)
                 ||'Queue Name'||CHR(9)
                               );

       --Opening and Fetching Cursor Values into local variables.
       FOR rec_data IN cur_data
       LOOP


                  --Printing Local Variable Values to output.
                  Fnd_File.put_line(Fnd_File.OUTPUT,rec_data.MASS_ADDITION_ID||CHR(9)
                                                       ||rec_data.description||CHR(9)
                                                       ||rec_data.major||CHR(9)
                               ||rec_data.minor1||CHR(9)
                               ||rec_data.minor2||CHR(9)
                                      ||rec_data.MANUFACTURER_NAME||CHR(9)
                                      ||rec_data.DATE_PLACED_IN_SERVICE||CHR(9)
                                   ||rec_data.FIXED_ASSETS_COST||CHR(9)
                               ||rec_data.FIXED_ASSETS_UNITS||CHR(9)
                               ||rec_data.seg1_co||CHR(9)
                               ||rec_data.seg2_dept||CHR(9)
                               ||rec_data.seg3_state_sbu||CHR(9)
                               ||rec_data.seg4_loc||CHR(9)
                               ||rec_data.seg5_merchandize||CHR(9)
                               ||rec_data.seg6_account||CHR(9)
                               ||rec_data.seg7_intercomp||CHR(9)
                               ||rec_data.seg8_future||CHR(9)
                               ||rec_data.location_codes||CHR(9)
                               ||rec_data.REVIEWER_COMMENTS||CHR(9)
                               ||rec_data.INVOICE_NUMBER||CHR(9)
                                      ||rec_data.VENDOR_NUMBER||CHR(9)
                                      ||rec_data.PO_NUMBER||CHR(9)
                               ||rec_data.l_year||CHR(9) -- year
                               ||rec_data.project_number || CHR(9)
                 ||rec_data.posting_status || CHR(9)
                 ||rec_data.queue_name || CHR(9)                   -- project Number
                               );

       END LOOP;

    END XXABRL_ASSET_CAPITAL2_PROC;
END XXABRL_ASSET_CAPITAL2_PKG; 
/


CREATE OR REPLACE PROCEDURE APPS.XXABRL_GL_BAL_INSERT (ERRBUF OUT VARCHAR2,
                                                    RETCODE OUT VARCHAR2,
                                                    P_PERIOD_NAME IN VARCHAR2,
                                                    P_VERSION_NO IN NUMBER)
/*****************************************************************************************************************************************
                 OBJECT INFORMATION

 Object Name : XXABRL_GL_BAL_INSERT  

 Description : Procedure To Insert the data from GL Balance Version Table to Custom GL_BALANCES Table

  Change Record:
 =============================================================================================================
 Version    Date         Author          Remarks
 =======   ==========   ============    ============================================================================
 1.0.0     11-Nov-09    Praveen Kumar    To Insert the data from GL Balance Version Table to Custom GL_BALANCES Table
 1.0.1    28-Jan-09    Praveen Kumar    Commented 2 lines in Update statement 
******************************************************************************************************************************************/
 
                                                    
IS
V_TNAME VARCHAR2(100);
INSERT_STRING LONG;
INSERT_STRING1 VARCHAR2(50);
V_GL_COUNT NUMBER;
V_XXGL_COUNT NUMBER;
V_COUNT NUMBER;
V_INSERT_FLAG CHAR(1);
COUNT_STRING LONG;
BEGIN
 
   BEGIN
    SELECT NVL(INSERT_FLAG,'N')
      INTO V_INSERT_FLAG
      FROM XXABRL_GL_BALANCES_TABLE  A
      WHERE A.PERIOD = P_PERIOD_NAME
       AND A.VERSION_NO = P_VERSION_NO;
   END;  
   
   IF V_INSERT_FLAG = 'N' THEN                                          
       BEGIN   
        SELECT TNAME 
          INTO V_TNAME
          FROM XXABRL_GL_BALANCES_TABLE
         WHERE PERIOD = P_PERIOD_NAME
           AND VERSION_NO = P_VERSION_NO;           
       END;
         
       IF V_TNAME IS NOT NULL THEN
                                    
                EXECUTE IMMEDIATE 'TRUNCATE TABLE XXABRL.XXABRL_GL_BALANCES';                
                
                INSERT_STRING:= '
                  INSERT INTO XXABRL_GL_BALANCES(  
                                                  LEDGER_ID,              
                                                  CODE_COMBINATION_ID,   
                                                  CURRENCY_CODE,         
                                                  PERIOD_NAME,           
                                                  ACTUAL_FLAG,           
                                                  LAST_UPDATE_DATE,      
                                                  LAST_UPDATED_BY,       
                                                  BUDGET_VERSION_ID,     
                                                  ENCUMBRANCE_TYPE_ID,   
                                                  TRANSLATED_FLAG,       
                                                  REVALUATION_STATUS,    
                                                  PERIOD_TYPE,           
                                                  PERIOD_YEAR,          
                                                  PERIOD_NUM,         
                                                  PERIOD_NET_DR,        
                                                  PERIOD_NET_CR,       
                                                  PERIOD_TO_DATE_ADB,    
                                                  QUARTER_TO_DATE_DR,   
                                                  QUARTER_TO_DATE_CR,  
                                                  QUARTER_TO_DATE_ADB, 
                                                  YEAR_TO_DATE_ADB,
                                                  PROJECT_TO_DATE_DR,
                                                  PROJECT_TO_DATE_CR,
                                                  PROJECT_TO_DATE_ADB,
                                                  BEGIN_BALANCE_DR,
                                                  BEGIN_BALANCE_CR,
                                                  PERIOD_NET_DR_BEQ,
                                                  PERIOD_NET_CR_BEQ,
                                                  BEGIN_BALANCE_DR_BEQ,  
                                                  BEGIN_BALANCE_CR_BEQ, 
                                                  TEMPLATE_ID,
                                                  ENCUMBRANCE_DOC_ID,
                                                  ENCUMBRANCE_LINE_NUM,
                                                  QUARTER_TO_DATE_DR_BEQ,
                                                  QUARTER_TO_DATE_CR_BEQ,
                                                  PROJECT_TO_DATE_DR_BEQ,
                                                  PROJECT_TO_DATE_CR_BEQ) SELECT * FROM ';
                         INSERT_STRING1:= V_TNAME;
                         INSERT_STRING:= INSERT_STRING||INSERT_STRING1;
                         EXECUTE IMMEDIATE INSERT_STRING;                           
                         COMMIT;
                         
                         UPDATE XXABRL_GL_BALANCES_TABLE A 
                            SET INSERT_FLAG = 'N'
                          WHERE 1=1; 
                           --A.PERIOD <> P_PERIOD_NAME;
                           -- AND A.VERSION_NO <> P_VERSION_NO;
 
                         UPDATE XXABRL_GL_BALANCES_TABLE A 
                           SET INSERT_FLAG = 'Y'
                         WHERE A.PERIOD = P_PERIOD_NAME
                           AND A.VERSION_NO = P_VERSION_NO;                           
                         
                         COMMIT;
                          ---RETURN (TRUE);
                         FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'Records are inserted');
                         
         END IF;
   ELSIF V_INSERT_FLAG = 'Y' THEN
    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'Records already inserted');
  END IF;
END XXABRL_GL_BAL_INSERT; 
/


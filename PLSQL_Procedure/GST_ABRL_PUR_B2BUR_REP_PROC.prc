CREATE OR REPLACE PROCEDURE APPS.GST_ABRL_PUR_B2BUR_REP_PROC (
   errbuf        OUT      VARCHAR2,
   retcode       OUT      VARCHAR2,
   p_from_date   IN       VARCHAR2,
   p_to_date     IN       VARCHAR2
)
IS
BEGIN
/*
   ========================
=========================
=========================
=========================
====
  ||   Procedure Name  : APPS.GST_ABRL_PUR_B2BUR_REP_PROC
  ||   Description : GST ABRL Purchase B2BUR Report
  ||
  ||        Date                   Author              Modification
  ||      ~~~~~~~~~            ~~~~~~~~~~~~~~         ~~~~~~~~~~~~~~~
  ||      09-NOV-2017          Lokesh Poojari         New Development
      
   ========================
=========================
=========================
=========================
====*/
   fnd_file.put_line
      (fnd_file.output,
       'trans_type_code}fp}ack_sr_no}self_gstin}system_cd}branch_cd}vertical_cd}result_flag}cp_gstin_name}supply_type}invoice_no}invoice_dt}invoice_val}taxable_value}items_serial_no}rate_of_tax}igst_amnt}cgst_amnt}sgst_amnt}cess_amnt}pos}eligibility_of_itc}tot_tax_avlbl_itc_igst}tot_tax_avlbl_itc_cgst}tot_tax_avlbl_itc_sgst}total_tax_itc_cess'
      );

   FOR b2bur IN
      (SELECT DISTINCT trans_type_code, 
                                         fp,
                                        ack_sr_no, 
                                         self_gstin,
                                         system_cd,
                                         branch_cd, 
                                         vertical_cd, 
                                         result_flag,
                                         cp_gstin_name,
                            CASE WHEN nvl(IGST_AMNT,0)>0
                                                          THEN 'INTER'
                                                          WHEN (NVL(CGST_AMNT, 0)>=0 or  nvl(SGST_AMNT,0)>=0 or nvl(IGST_AMNT,0)=0)
                                                          THEN 'INTRA'
                                                           END  supply_type,
                       ack_sr_no invoice_no, invoice_dt, invoice_val,
                       taxable_value, items_serial_no, rate_of_tax, 
                    ltrim(replace(igst_amnt,' '), '0')  igst_amnt,
                    ltrim(replace(cgst_amnt,' '), '0')  cgst_amnt,
                    ltrim(replace(sgst_amnt,' '), '0')  sgst_amnt,
                    ltrim(replace(cess_amnt,' '), '0')  cess_amnt,
                       pos,
                       eligibility_of_itc, tot_tax_avlbl_itc_igst,
                       tot_tax_avlbl_itc_cgst, tot_tax_avlbl_itc_sgst,
                       total_tax_itc_cess
                         --                 orignal_invoice_no,
                     --  orignal_invoice_dt
       FROM            xxabrl.xxabrl_purchase_b2bur
                 WHERE 1 = 1
                  AND APPROVAL_STATUS='APPROVED'
                   AND TRUNC (gl_date) BETWEEN TO_DATE
                                                      (p_from_date,
                                                       'YYYY/MM/DD HH24:MI:SS'
                                                      )
                                           AND TO_DATE
                                                      (p_to_date,
                                                       'YYYY/MM/DD HH24:MI:SS'
                                                      ))
   LOOP
      fnd_file.put_line (fnd_file.output,
                            b2bur.trans_type_code
                         || '}'
                         || b2bur.fp
                         || '}'
                         || b2bur.ack_sr_no
                         || '}'
                         || b2bur.self_gstin
                         || '}'
                         || b2bur.system_cd
                         || '}'
                         || b2bur.branch_cd
                         || '}'
                         || b2bur.vertical_cd
                         || '}'
                         || b2bur.result_flag
                         || '}'
                         || b2bur.cp_gstin_name
                         || '}'
                         || b2bur.supply_type
                         || '}'
                         || b2bur.invoice_no
                         || '}'
                         || b2bur.invoice_dt
                         || '}'
                         || b2bur.invoice_val
                         || '}'
                         || b2bur.taxable_value
                         || '}'
                         || b2bur.items_serial_no
                         || '}'
                         || b2bur.rate_of_tax
                         || '}'
                         || b2bur.igst_amnt
                         || '}'
                         || b2bur.cgst_amnt
                         || '}'
                         || b2bur.sgst_amnt
                         || '}'
                         || b2bur.cess_amnt
                         || '}'
                         || b2bur.pos
                         || '}'
                         || b2bur.eligibility_of_itc
                         || '}'
                         || b2bur.tot_tax_avlbl_itc_igst
                         || '}'
                         || b2bur.tot_tax_avlbl_itc_cgst
                         || '}'
                         || b2bur.tot_tax_avlbl_itc_sgst
                         || '}'
                         || b2bur.total_tax_itc_cess
                        );
   END LOOP;
END; 
/


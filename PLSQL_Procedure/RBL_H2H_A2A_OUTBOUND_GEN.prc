CREATE OR REPLACE PROCEDURE APPS.rbl_h2h_a2a_outbound_gen (
   errbuf    OUT   VARCHAR2,
   retcode   OUT   NUMBER
)
IS
/*
---------------------------------------------------------------------------------------------------------------------------------------------
-- Concurrent Program Name : MRL RBL BANK A2A Outbound Generator
-- Procedure Name          : APPS.rbl_h2h_a2a_outbound_gen
-- Description             : Supplier Bank Outbound interface for RBL Bank generating file on FTP [H2H Interface between MRL and RBL Bank]
---------------------------------------------------------------------------------------------------------------------------------------------
-- Version    Date            Author                 Remarks
---------------------------------------------------------------------------------------------------------------------------------------------
--  1.0       12-Jun-2019     Lokesh Poojari         New Development. Bank Account number hardcoded
--  1.1       19-Jan-2021     Pawan Sahu             Replaced '99999999.99' with '999999999.99', to process 10 crore and above payment
---------------------------------------------------------------------------------------------------------------------------------------------
*/
   lv_mrl_pay_count    NUMBER             := 0;
   lv_mrl_pay_amount   NUMBER             := 0;
   v_dbs_neft_seq1      NUMBER;
   v_dbs_neft_seq       VARCHAR2 (2000);
   v_dbs_pay_seq        NUMBER;
   l_file_p             UTL_FILE.file_type;
   l_pmt_data           VARCHAR2 (4000);
   lv_mrl_adv_count    NUMBER             := 0;
   l_file_a             UTL_FILE.file_type;

   CURSOR xx_rbl_hth (p_acc_no IN VARCHAR2)
   IS
      SELECT   *
          FROM apps.rbl_payment_table
         WHERE transaction_status = 'AUTHORIZED'
           AND TRUNC (creation_date) >= TRUNC (SYSDATE) - 30
           AND product_code = 'N'
           AND corporate_account_number NOT IN ('1007012010000817')
           AND corporate_account_number = p_acc_no
      ORDER BY check_id, transaction_value_date;

   CURSOR cur_dbs_adv (p_acc_no VARCHAR2, p_check_id VARCHAR2)   --CUR_DBS_ADV
   IS
      SELECT   dnpi.*, dpt.vendor_code, dpt.third_party_id, dpt.primary_name,
               dpt.corporate_account_number, dpt.pay_doc_number
          FROM apps.rbl_payment_invoice dnpi, apps.rbl_payment_table dpt
         WHERE dnpi.check_id = dpt.check_id
           AND TRUNC (creation_date) >= TRUNC (SYSDATE) - 30
           -- this will also pick up transactions which were created 2 days prior
           AND dpt.transaction_status = 'AUTHORIZED'
           AND dpt.product_code = 'N'
          AND dpt.corporate_account_number NOT IN ('1007012010000817')
           AND dpt.corporate_account_number = p_acc_no
           AND dnpi.check_id = p_check_id
      ORDER BY dnpi.check_id;

   CURSOR cur_adv_pay_acc_no
   IS
      SELECT   dpt.corporate_account_number
          FROM apps.rbl_payment_invoice dnpi, apps.rbl_payment_table dpt
         WHERE dnpi.check_id = dpt.check_id
           AND TRUNC (creation_date) >= TRUNC (SYSDATE) - 30
           -- this will also pick up transactions which were created 2 days prior
           AND dpt.transaction_status = 'AUTHORIZED'
           AND dpt.product_code = 'N'
          AND dpt.corporate_account_number NOT IN ('1007012010000817')
      GROUP BY dpt.corporate_account_number;

   CURSOR trx_amount
   IS
      SELECT   COUNT (*) pay_count,
               SUM (transactional_amount) transactional_amount,
               corporate_account_number
--     INTO lv_mrl_pay_count, lv_mrl_pay_amount
      FROM     apps.rbl_payment_table
         WHERE 1 = 1
           AND transaction_status = 'AUTHORIZED'
           AND TRUNC (creation_date) >= TRUNC (SYSDATE) - 30
           -- this will also pick up transactions which were created 2 days prior
           AND product_code = 'N'
          AND corporate_account_number NOT IN ('1007012010000817')
      GROUP BY corporate_account_number;
BEGIN
   SELECT COUNT (*), SUM (transactional_amount)
     INTO lv_mrl_pay_count, lv_mrl_pay_amount
     FROM apps.rbl_payment_table
    WHERE 1 = 1
      AND transaction_status = 'AUTHORIZED'
      AND TRUNC (creation_date) >= TRUNC (SYSDATE) - 30
      -- this will also pick up transactions which were created 2 days prior
      AND product_code = 'N'
      AND corporate_account_number NOT IN ('1007012010000817');

   fnd_file.put_line (fnd_file.LOG,
                      'Total Payment count =>' || lv_mrl_pay_count
                     );

   BEGIN
      IF lv_mrl_pay_count > 0
      --AND LV_CORPORATE_ACCOUNT_NUMBER NOT IN ('811200088576')
      THEN
         FOR xx_main IN trx_amount
         LOOP
            fnd_file.put_line
               (fnd_file.LOG,
                '-----------------------------------------------------------------'
               );
            fnd_file.put_line (fnd_file.LOG,
                                  'corporate_account_number=>'
                               || xx_main.corporate_account_number
                              );

            BEGIN
               l_file_p :=
                  UTL_FILE.fopen ('RBLBANK',
                                     '8007002010000696~MORE_SFTP~'
                                  || TO_CHAR (SYSDATE, 'DD')
                                  || TO_CHAR (SYSDATE, 'MM')
                                  || TO_CHAR (SYSDATE, 'YYYY')
                                  || TO_CHAR (SYSDATE, 'HH24MISS')
--                                  || '_'
--                                  || mrl_rbl_a2a_seq.NEXTVAL
                                  || '.TXT',
                                  'W'
                                 );

               FOR rec_dbs_neft IN
                  xx_rbl_hth (xx_main.corporate_account_number)
               LOOP
                  --Creating a file for the below generated payment data

                  fnd_file.put_line (fnd_file.LOG,
                                     'Check id=>' || rec_dbs_neft.check_id
                                    );
                  UTL_FILE.put_line
                     (l_file_p,
                         'P'
                      || '^'
                      || CASE
                            WHEN rec_dbs_neft.product_code = 'N'
                               THEN 'NE'
                            ELSE rec_dbs_neft.product_code
                         END
--                      || rec_dbs_neft.product_code
                      || '^'
                      || 'RBL Bank Ltd'
--                      || rec_dbs_neft.corp_bank_name
                      || '^'
                      || rec_dbs_neft.check_id
                      || '^'
                      || rec_dbs_neft.corporate_account_number
                      || '^'
                      || TO_CHAR (rec_dbs_neft.transaction_value_date,
                                  'YYYY-MM-DD'
                                 )
                      || '^'
                      || rec_dbs_neft.transactional_currency
                      || '^'
                      || TRIM (TO_CHAR (rec_dbs_neft.transactional_amount,
                                        '999999999.99' -- '99999999.99' -- V1.1 Modified by Pawan
                                       )
                              )
                      || '^'
                      || REPLACE
                            (REPLACE
                                (REPLACE
                                    (REPLACE
                                        (REPLACE
                                            (REPLACE
                                                (REPLACE
                                                    (REPLACE
                                                        (REPLACE
                                                            (REPLACE
                                                                (REPLACE
                                                                    (REPLACE
                                                                        (REPLACE
                                                                            (REPLACE
                                                                                (REPLACE
                                                                                    (REPLACE
                                                                                        (SUBSTR
                                                                                            (rec_dbs_neft.primary_name,
                                                                                             0,
                                                                                             35
                                                                                            ),
                                                                                         '&',
                                                                                         ''
                                                                                        ),
                                                                                     '!',
                                                                                     ''
                                                                                    ),
                                                                                 '@',
                                                                                 ''
                                                                                ),
                                                                             '#',
                                                                             ''
                                                                            ),
                                                                         '$',
                                                                         ''
                                                                        ),
                                                                     '%',
                                                                     ''
                                                                    ),
                                                                 '^',
                                                                 ''
                                                                ),
                                                             '*',
                                                             ''
                                                            ),
                                                         '=',
                                                         ''
                                                        ),
                                                     '{',
                                                     ''
                                                    ),
                                                 '}',
                                                 ''
                                                ),
                                             '[',
                                             ''
                                            ),
                                         ']',
                                         ''
                                        ),
                                     '^',
                                     ''
                                    ),
                                 '\',
                                 ''
                                ),
                             ',',
                             ''
                            )
                      || '^'
                      || rec_dbs_neft.vendor_code
                      || '^'
                      || rec_dbs_neft.benefi_acc_num
                      || '^'
                      || rec_dbs_neft.benefi_acc_type
                      || '^'
                      || NULL
                      || '^'
                      || NULL
                      || '^'
                      || rec_dbs_neft.third_party_id
                      --  || NULL
                      || '^'
                      || rec_dbs_neft.benfi_bank_city
                      || '^'
                      || NULL
                      || '^'
                      || NULL
                      || '^'
                      || rec_dbs_neft.benefi_ifsc_code
                      || '^'
                      || rec_dbs_neft.benefi_bank_name
                      || '^'
                      || 'XXXX'
                      || '^'
                      || rec_dbs_neft.pay_doc_number
                      || '^'
                      || TO_CHAR (rec_dbs_neft.transaction_value_date,
                                  'YYYY-MM-DD'
                                 )
                      || '^'
                      || NULL
                      || '^'
                      || NULL
                      || '^'
                      || REGEXP_SUBSTR (REPLACE (rec_dbs_neft.primary_email,
                                                 '^',
                                                 ','
                                                ),
                                        '[^,]+',
                                        1,
                                        1
                                       )
                      || ','
                      || REGEXP_SUBSTR (REPLACE (rec_dbs_neft.primary_email,
                                                 '^',
                                                 ','
                                                ),
                                        '[^,]+',
                                        1,
                                        2
                                       )
                      || ','
                      || REGEXP_SUBSTR (REPLACE (rec_dbs_neft.primary_email,
                                                 '^',
                                                 ','
                                                ),
                                        '[^,]+',
                                        1,
                                        3
                                       )
                      || ','
                      || REGEXP_SUBSTR (REPLACE (rec_dbs_neft.primary_email,
                                                 '^',
                                                 ','
                                                ),
                                        '[^,]+',
                                        1,
                                        4
                                       )
                      || ','
                      || REGEXP_SUBSTR (REPLACE (rec_dbs_neft.primary_email,
                                                 '^',
                                                 ','
                                                ),
                                        '[^,]+',
                                        1,
                                        5
                                       )
                      || '^'
                      || NULL
                      || '^'
                      || NULL
                      || '^'
                      || NULL
                      || '^'
                      || 'XXXX'
                      || '^'
                      || NULL
                      || '^'
                      || NULL
                      || '^'
                      || NULL
                      || '^'
                      || NULL
                      || '^'
                      || NULL
                      || '^'
                      || NULL
                      || '^'
                      || NULL
                      || '^'
                      ||NULL-- 'g.sangrulkar@caltherm.co.in'
                      || '^'
                      || TO_CHAR (SYSDATE, 'YYYY-MM-DD HH24-MI-SS')
                      || '^'
                      || NULL
                      || '^'
                      || NULL
                     );

                  FOR rec_dbs_adv IN
                     cur_dbs_adv (rec_dbs_neft.corporate_account_number,
                                  rec_dbs_neft.check_id
                                 )
                  LOOP
                     fnd_file.put_line
                           (fnd_file.LOG,
                            '-----------------------------------------------'
                           );
                     fnd_file.put_line (fnd_file.LOG,
                                           'Payment Advice check id=>'
                                        || rec_dbs_adv.check_id
                                       );
                     fnd_file.put_line (fnd_file.LOG,
                                           'invoice number=>'
                                        || rec_dbs_adv.invoice_number
                                       );
                     fnd_file.put_line (fnd_file.LOG,
                                           'invoice date=>'
                                        || rec_dbs_adv.invoice_date
                                       );
                     UTL_FILE.put_line
                                (l_file_p,
                                    'I'
                                 || '^'
                                 || rec_dbs_adv.invoice_number
                                 || '^'
                                 || TO_CHAR
                                           (TO_DATE (rec_dbs_adv.invoice_date),
                                            'YYYY-MM-DD HH24-MI-SS'
                                           )
                                 || '^'
                                 || rec_dbs_adv.net_amount
                                 || '^'
                                 || 0
                                 || '^'
                                 || 0
                                 || '^'
                                 || rec_dbs_adv.invoice_amount
                                );
                     fnd_file.put_line
                            (fnd_file.LOG,
                             '-----------------------------------------------'
                            );
                  END LOOP;
               --seq_num := seq_num + 1;
               END LOOP;

               UTL_FILE.fclose (l_file_p);
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  UTL_FILE.fclose (l_file_p);
                  DBMS_OUTPUT.put_line ('No Data Found');
               WHEN UTL_FILE.invalid_path
               THEN
                  UTL_FILE.fclose (l_file_p);
                  DBMS_OUTPUT.put_line ('Invalid Path');
               WHEN UTL_FILE.invalid_filehandle
               THEN
                  UTL_FILE.fclose (l_file_p);
                  DBMS_OUTPUT.put_line ('Invalid File Handle');
               WHEN UTL_FILE.invalid_operation
               THEN
                  UTL_FILE.fclose (l_file_p);
                  DBMS_OUTPUT.put_line ('Invalid Operation');
               WHEN UTL_FILE.read_error
               THEN
                  UTL_FILE.fclose (l_file_p);
                  DBMS_OUTPUT.put_line ('Read Error');
               WHEN UTL_FILE.write_error
               THEN
                  UTL_FILE.fclose (l_file_p);
                  DBMS_OUTPUT.put_line ('Write Error');
               WHEN UTL_FILE.internal_error
               THEN
                  UTL_FILE.fclose (l_file_p);
                  DBMS_OUTPUT.put_line ('Internal Error');
               WHEN UTL_FILE.file_open
               THEN
                  UTL_FILE.fclose (l_file_p);
                  DBMS_OUTPUT.put_line ('File is Open');
               WHEN UTL_FILE.invalid_filename
               THEN
                  UTL_FILE.fclose (l_file_p);
                  DBMS_OUTPUT.put_line ('Invalid File Name');
               WHEN OTHERS
               THEN
                  UTL_FILE.fclose (l_file_p);
                  DBMS_OUTPUT.put_line ('Unknown Error' || SQLERRM);
                  fnd_file.put_line (fnd_file.LOG,
                                        'Unknown Error'
                                     || SQLCODE
                                     || '=>'
                                     || SQLERRM
                                    );
            END;
         END LOOP;

         UPDATE apps.rbl_payment_table
            SET transaction_status = 'PROCESSED',
                interface_status = 'Y'
          WHERE 1 = 1
            AND TRUNC (creation_date) >= TRUNC (SYSDATE) - 30
            AND transaction_status = 'AUTHORIZED'
            AND corporate_account_number NOT IN ('1007012010000817')
            AND product_code = 'N';

         COMMIT;
         fnd_file.put_line (fnd_file.LOG, 'Payment_table Updated');
         fnd_file.put_line (fnd_file.LOG, 'End');
      END IF;
   END;
EXCEPTION
   WHEN OTHERS
   THEN
      UTL_FILE.fclose (l_file_p);
      fnd_file.put_line (fnd_file.LOG,
                         'Unknown Error' || SQLCODE || '=>' || SQLERRM
                        );
END;
/


CREATE OR REPLACE PACKAGE BODY APPS.XXABRL_CONCPROG_MAILER_PKG AS

  PROCEDURE Print_log(p_str IN VARCHAR2, p_debug_flag IN VARCHAR2) AS
  BEGIN
    IF p_debug_flag = 'Y' THEN
      Fnd_File.PUT_LINE(Fnd_File.LOG, p_str);
    END IF;

    IF p_debug_flag = 'O' THEN
      Fnd_File.PUT_LINE(Fnd_File.OUTPUT, p_str);
    END IF;
  END;

  PROCEDURE MAILER(errbuf  OUT VARCHAR2,
                   RetCode OUT NUMBER,
                   p_Param IN VARCHAR2) AS
    v_request_id NUMBER := 0;
    v_errmsg     VARCHAR2(900);
    stop_program EXCEPTION;
    v_log_file_name VARCHAR2(150);
    v_Print_log     VARCHAR2(1) := 'Y';
    v_L_File        VARCHAR2(150);

    v_Param         VARCHAR2(150);
    v_Mail_Type     VARCHAR2(150);
    v_module        VARCHAR2(150);
    v_File_Type     VARCHAR2(150);
    v_org_id        NUMBER;
    v_short_code    VARCHAR2(150);
    v_Email_List    VARCHAR2(999);
    v_Email_Subject VARCHAR2(999);
    v_module_name   VARCHAR2(999);
    p_module_name   VARCHAR2(999);
    -- added by shailesh on 12 feb 2009  for org name in mail subject
    v_org_name      VARCHAR2(999);

    mail_request_id   NUMBER;
    v_conc_request_id NUMBER;
  BEGIN


    Print_log('Param IN: ' || p_Param, 'Y');



      /*
    Begin

      select request_id, file_name, module_name
        into v_conc_request_id, v_L_File, v_module_name
        from Mailer_Request
       where status = 'P'
         and module_name = p_Param
         and rownum = 1;


     Print_Log('Parent-Request found (Table:xxabrl.Mailer_Request): ' ||
                v_conc_request_id,
                v_Print_log);

    Exception
      when no_data_found then
        v_errmsg := 'Parent-Request details not found (Table:xxabrl.Mailer_Request) ';
        Raise stop_program;
    End;
    */


    -- above code replaced by shailesh on 19 feb 2009


     IF  p_Param = 'AR_NAVISION' THEN

         BEGIN

         SELECT request_id, file_name, module_name
        INTO v_conc_request_id, v_L_File, v_module_name
        FROM Mailer_Request_ar
          WHERE status = 'P'
         AND module_name = p_Param
         AND ROWNUM = 1;


         Print_Log('Parent-Request found (Table:xxabrl.Mailer_Request): ' ||
                v_conc_request_id,
                v_Print_log);

         EXCEPTION
          WHEN NO_DATA_FOUND THEN
        v_errmsg := 'Parent-Request details not found (Table:xxabrl.Mailer_Request) ';
        RAISE stop_program;
       END;

     ELSE

         BEGIN

          SELECT request_id, file_name, module_name
          INTO v_conc_request_id, v_L_File, v_module_name
          FROM Mailer_Request
          WHERE status = 'P'
           AND module_name = p_Param
           AND ROWNUM = 1;


          Print_Log('Parent-Request found (Table:xxabrl.Mailer_Request): ' ||
                v_conc_request_id,
                v_Print_log);

         EXCEPTION
          WHEN NO_DATA_FOUND THEN
           v_errmsg := 'Parent-Request details not found (Table:xxabrl.Mailer_Request) ';
           RAISE stop_program;
         END;

     END IF;


    v_org_id  := Fnd_Profile.VALUE('ORG_ID');
    IF v_org_id IS NOT NULL THEN

       BEGIN
           -- v_org_name added by shailesh on 12 feb 2009  for org name in mail subject

         SELECT
         short_code, NAME INTO v_short_code,v_org_name
         FROM hr_operating_units
         WHERE organization_id = v_org_id;

       EXCEPTION
       WHEN NO_DATA_FOUND THEN
            v_errmsg := 'Org ID/Organization name not found';
            RAISE stop_program;
       END;

    END IF;

    IF v_short_code IS NOT NULL THEN
       BEGIN

       SELECT DESCRIPTION||','||TAG --Use TAG column for aditional email IDs
       INTO v_Email_List
           FROM FND_LOOKUP_VALUES_VL
          WHERE SYSDATE BETWEEN NVL(START_DATE_ACTIVE, SYSDATE) AND
                NVL(END_DATE_ACTIVE, SYSDATE)
            AND ENABLED_FLAG = 'Y'
            AND LOOKUP_TYPE = 'ABRL_AUTOMATION_OU_MAIL_MAP'
           -- AND MEANING = v_short_code;
   AND MEANING = DECODE(p_Param,'AR_NAVISION','AR-','AP_NAVISION','AP-','')||v_short_code;
       EXCEPTION
         WHEN NO_DATA_FOUND THEN
           v_errmsg := 'Error:List of Email not defined in Lookup-ABRL_AUTOMATION_OU_MAIL_MAP(AR) :'||v_short_code;
           RAISE stop_program;
       END;
    END IF;


    IF v_conc_request_id > 0 THEN
      --if 1=1 then
      Print_log('*Inside Validation_Log', v_Print_log);
      fnd_file.put_line(FND_FILE.LOG,
                        'Finding Sub-Request details for Parent ID: ' ||
                        v_conc_request_id);

      SELECT
      DECODE(v_module_name,'AR_NAVISION','AR-','AP_NAVISION','AP-','ReIM_AP','ReIM_AP-','RMS_GL','RMS_GL-','XX') --s
      INTO p_module_name
      FROM dual;

      --Details for Staging-Validation Program
      FOR v_Mail_Req IN (SELECT request_id,
                                program_short_name,
                                DECODE(
                                       program_short_name,
                                       'RAXTRX',
                                       '.out',
                                       'XXABRL_NAV_AR_RECPT_IMP_PKG',
                                       '.out',
                                       'XXABRL_NAV_AR_INV_IMP_PKG',
                                       '.out',
                                       'XXABRL_APNAV_PKG_INV',
                                       '.out',
                                       'APXIIMPT',
                                       '.out',
                                       'XXABRL_REIM_AP_INV_INTERFACE',  --s
                                       '.out',                          --s
                                       'APXIIMPT',                      --s
                                       '.out',                          --s
                                       'XXABRL_GL_INTER_PKG',           --s
                                       '.out',                          --s
                                       '.log') File_Type,
                                       --Email Subject
                                        DECODE(program_short_name,
                                       'RAXTRX',
                                       'Navision-AR-AutoInvoice Import-',
                                       'XXABRL_NAV_AR_RECPT_IMP_PKG',
                                       'Navision-AR-Receipt_Conversion-',
                                       'XXABRL_NAV_AR_INV_IMP_PKG',
                                       'Navision-AR-Invoice_Conversion-',
                                       'XXABRL_APNAV_PKG_INV',
                                       'Navision-AP-Invoice_Conversion-',
                                       'APXIIMPT',
                                       'Navision-AP-Payable_Open_Interface-',
                                       'XXABRL_REIM_AP_INV_INTERFACE',                --s
                                       'Navision-ReIM To AP-Invoice_Conversion-',     --s
                                       'APXIIMPT',                                    --s
                                       'Navision-ReIM To AP-Payable_Open_Interface-', --s
                                       'XXABRL_GL_INTER_PKG',                         --s
                                       'Navision-RMS To GL Import-',                  --s
                                       'Automation Status Mail-'
                                       ) Email_Subject
                           FROM FND_CONC_REQ_SUMMARY_V
                          WHERE priority_request_id = v_conc_request_id
                            AND program_short_name IN
                                (--AR
                                 'RAXTRX',
                                 'XXABRL_NAV_AR_RECPT_IMP_PKG',
                                 'XXABRL_NAV_AR_INV_IMP_PKG',
                                 --AP
                                 'XXABRL_APNAV_PKG_INV',
                                 'APXIIMPT',
                                 --reim_ap
                                 'XXABRL_REIM_AP_INV_INTERFACE',  -- s
                                 'APXIIMPT',                       -- s
                                 --rms_gl
                                 'XXABRL_GL_INTER_PKG'  -- s
                                 )) LOOP



        v_request_id := v_Mail_Req.request_id;
        v_File_Type  := v_Mail_Req.File_Type;
        -- ou name  added by shailesh on 12 feb 2009  for org name in mail subject
        v_Email_Subject := 'OU:- '||v_org_name||' '||v_Mail_Req.Email_Subject;

       --  v_Email_List:='yogesh.bhalodia@retail.adityabirla.com,srinivas.thalla-v@retail.adityabirla.com';

      ----------------------------------------------------
      Print_log('request id ' ||v_request_id ||'  file type'||v_File_Type,v_Print_log);
      Print_log('Email Subject ' ||v_Email_Subject,v_Print_log);

        BEGIN

          SELECT DECODE(v_File_Type, '.out', 'o', 'l') || v_request_id ||
                 v_File_Type
            INTO v_log_file_name
            FROM dual;

          Print_log('Log/Output File to attach: ' || v_log_file_name,
                    v_Print_log);

        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            v_errmsg := 'Error:No_Data_Found for Log File Name: ' ||
                        v_request_id;
            RAISE stop_program;
          WHEN TOO_MANY_ROWS THEN
            v_errmsg := 'Error:Too_Many_Rows for Log File Name: ' ||
                        v_request_id;
            RAISE stop_program;
          WHEN OTHERS THEN
            v_errmsg := 'Error:Exception for Log File Name: ' ||
                        v_request_id;
            RAISE stop_program;
        END;

        BEGIN

          --Derive File-Name to rename/add with Log/Out File
          SELECT REPLACE(REPLACE(v_L_File, '.csv', '.txt'), 'L_', '')
            INTO v_L_File
            FROM dual;
          Print_log('Processed File Name : ' || v_L_File, v_Print_log);

        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            v_errmsg := 'Error:No_Data_Found for Processed File Name: ' ||
                        v_L_File;
            RAISE stop_program;
          WHEN TOO_MANY_ROWS THEN
            v_errmsg := 'Error:Too_Many_Rows for Processed File Name: ' ||
                        v_L_File;
            RAISE stop_program;
          WHEN OTHERS THEN
            v_errmsg := 'Error:Exception for Processed File Name: ' ||
                        v_L_File;
            RAISE stop_program;
        END;

        Print_log('Submitting []Mailer/File Move for ' || v_L_File,
                  v_Print_log);
        mail_request_id := fnd_request.submit_request('XXABRL',
                                                      'XXABRL_CONC_SCHEDULER_MAIL',
                                                      '',
                                                      '',
                                                      FALSE,
                                                      v_log_file_name,
                                                      v_L_File,
                                                      v_Email_List,
                                                      v_Email_Subject||v_L_File,
                                                      p_module_name,
                                                      CHR(0) ,
                                                                    '', '', '', '', '', '', '', '', '', '',
                                                                    '', '', '', '', '', '', '', '', '', '',
                                                                    '', '', '', '', '', '', '', '', '', '',
                                                                    '', '', '', '', '', '', '', '', '', '',
                                                                    '', '', '', '', '', '', '', '', '', '',
                                                                    '', '', '', '', '', '', '', '', '', '',
                                                                    '', '', '', '', '', '', '', '', '', '',
                                                                    '', '', '', '', '', '', '', '', '', '',
                                                                    '', '', '', '', '', '', '', '', '', '',
                                                                    '', '', '', '');
        ------
        IF mail_request_id > 0 THEN
          Print_log('[]Mailer/File Move submitted successfully for ' ||
                    v_Mail_Type,
                    v_Print_log);
        ELSE
          Print_log('Failed to submit []Mailer/File Move ' || v_Mail_Type,
                    v_Print_log);
        END IF;

      END LOOP;
    END IF;
  EXCEPTION
    WHEN stop_program THEN
      Print_log(v_errmsg, v_Print_log);
      RetCode := 1;
  END MAILER;
END XXABRL_CONCPROG_MAILER_PKG; 
/


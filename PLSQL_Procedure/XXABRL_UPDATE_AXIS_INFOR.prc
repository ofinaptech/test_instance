CREATE OR REPLACE PROCEDURE APPS.xxabrl_update_axis_infor (
   errbuf    OUT   VARCHAR2,
   retcode   OUT   VARCHAR2
)
IS
   /*
       This Procedure will Update the Attribute Status Once DBS Bank gives response file
   */  -- To get status from the DBS bank
   CURSOR axis_rev_cur
   IS
      SELECT apt.*
        FROM apps.axis_payment_table apt
       WHERE apt.attribute_category IS NOT NULL
         AND apt.updated_in_app = 'N';
--         AND apt.check_id = 413281855;

   --AND DPT.check_id = p_check_id;
   l_updated_flag   VARCHAR2 (1) := 'N';
BEGIN
   l_updated_flag := 'N';

   -- Updating with Statuses whatever bank is provided in Attributes
   FOR upd_chk IN axis_rev_cur
   LOOP
      BEGIN
         UPDATE apps.ap_checks_all
            SET attribute_category = upd_chk.attribute_category,
                attribute1 = upd_chk.attribute1,
                attribute2 = upd_chk.attribute2
          WHERE check_number = upd_chk.pay_doc_number
            AND check_id = upd_chk.check_id;

         l_updated_flag := 'Y';
      EXCEPTION
         WHEN OTHERS
         THEN
            NULL;
      END;

      IF (upd_chk.attribute1 = 'SUCCESS')
      THEN
         IF upd_chk.product_code = 'R' OR upd_chk.product_code = 'N'
         THEN
            UPDATE apps.ap_checks_all
               SET attribute_category = upd_chk.attribute_category,
                   attribute3 = upd_chk.attribute3
             WHERE check_number = upd_chk.pay_doc_number
               AND check_id = upd_chk.check_id;
         ELSIF upd_chk.product_code = 'D' OR upd_chk.product_code = 'C'
         THEN
            UPDATE apps.ap_checks_all
               SET attribute_category = upd_chk.attribute_category,
                   attribute4 = upd_chk.attribute4
             WHERE check_number = upd_chk.pay_doc_number
               AND check_id = upd_chk.check_id;
         ELSIF upd_chk.product_code = 'B'
         THEN
            UPDATE apps.ap_checks_all
               SET attribute_category = upd_chk.attribute_category,
                   attribute5 = upd_chk.attribute5
             WHERE check_number = upd_chk.pay_doc_number
               AND check_id = upd_chk.check_id;
         END IF;
      END IF;

      IF l_updated_flag = 'Y'
      THEN
         UPDATE apps.axis_payment_table apt
            SET updated_in_app = 'Y'
          WHERE apt.pay_doc_number = upd_chk.pay_doc_number
            AND apt.check_id = upd_chk.check_id;
      END IF;
   END LOOP;

   COMMIT;
END xxabrl_update_axis_infor; 
/


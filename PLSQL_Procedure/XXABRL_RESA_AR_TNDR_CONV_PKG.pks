CREATE OR REPLACE PACKAGE APPS.xxabrl_resa_ar_tndr_conv_pkg
IS
   /*
   =========================================================================================================
   ||   Filename   : XXABRL_RESA_AR_CONV_PKG.sql
   ||   Description : Script is used to mold ReSA data for AR
   ||
   ||   Version     Date            Author           Modification
   ||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~   ~~~~~~~~~~~~~~~
   ||   1.0.0       22-Feb-2010   Praveen Kumar    New Development
   ||   1.0.1       08-Mar-2010   Praveen Kumar    Tender Type Logic has included to get the data like Navision
   ||   1.0.2       25-Mar-2010   Praveen Kumar    XXABRL_TENDER_RESA_SEQ Is created for generating the Interface
                                                   line attaribute2 seq number
   ||   1.0.3       24-may-10     Naresh Hasti     included the logic to pick only active bill to site
   ||    1.0.4      17-jul-10     Sayikrishna      cursor cur_store_code IS ADDED
                                                   commented UPDATE Stmet in Insert_ReSA_Counted_Amt.
                                                   Reason : Logic needs be changed after discussion with Mr.Bala
        1.0.5       01-mar-2011   Ravi Jadhav      Includec the logic to pick up the correct ledger name.
                                                   Reason: Leger Name was hard coded to "ABRL Ledger"
                                                   ( And added Update script to update the correct store no from 2041 to 1336
                                                   For Kovai pudur store)
        1.0.6       29-May-2013   Urmila           Added exception section so that correct data should not get stuck because of error


   ||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~  ~~~~~~~~~~~~~~~
   ========================================================================================================*/
   PROCEDURE main_proc (
      x_err_buf      OUT      VARCHAR2,
      x_ret_code     OUT      NUMBER,
      p_action       IN       VARCHAR2,
      p_debug_flag   IN       VARCHAR2
   );

   PROCEDURE print_log (p_msg IN VARCHAR2, p_debug_flag VARCHAR2);

   PROCEDURE validate_resa (p_debug_flag IN VARCHAR2, p_ret_code OUT NUMBER);

   PROCEDURE insert_resa (p_source_name IN VARCHAR2, p_ret_code OUT NUMBER);

   PROCEDURE insert_resa_counted_amt (
      p_source_name   IN       VARCHAR2,
      p_ret_code      OUT      NUMBER
   );

   PROCEDURE validate_customer (
      p_store       IN       VARCHAR2,
      p_rowid                VARCHAR2,
      p_org_id               VARCHAR2,
      p_error_msg   OUT      VARCHAR2
   );

   PROCEDURE validate_currency_code (
      p_curr_code   IN       VARCHAR2,
      p_error_msg   OUT      VARCHAR2
   );

   PROCEDURE validate_curr_conv_code (
      p_curr_conv_code   IN       VARCHAR2,
      p_error_msg        OUT      VARCHAR2
   );

   PROCEDURE validate_ccid (
      p_ccid        IN       VARCHAR2,
      p_rowid                VARCHAR2,
      p_error_msg   OUT      VARCHAR2
   );

   PROCEDURE validate_segments (
      p_segment1    IN       VARCHAR2,
      p_segment2    IN       VARCHAR2,
      p_segment3    IN       VARCHAR2,
      p_segment4    IN       VARCHAR2,
      p_segment5    IN       VARCHAR2,
      p_segment6    IN       VARCHAR2,
      p_segment7    IN       VARCHAR2,
      p_segment8    IN       VARCHAR2,
      p_rowid                VARCHAR2,
      p_error_msg   OUT      VARCHAR2
   );

   PROCEDURE validate_tran_type (
      p_id_type          IN       VARCHAR2,
      p_rollup_level_1   IN       NUMBER,
      p_rollup_level_2   IN       NUMBER,
      p_nature           OUT      VARCHAR2,
      p_tran_type        OUT      VARCHAR2,
      p_description      OUT      VARCHAR2,
      p_error_msg        OUT      VARCHAR2
   );

   PROCEDURE derive_tran_type (
      p_resa_cur      IN       xxabrl_resa_ar_int%ROWTYPE,
      p_nature        OUT      VARCHAR2,
      p_tran_type     OUT      VARCHAR2,
      p_description   OUT      VARCHAR2,
      p_error_msg     OUT      VARCHAR2
   );

   PROCEDURE derive_bank_dtl (
      p_store                 IN       xxabrl_resa_ar_int%ROWTYPE,
      p_tran_type             IN       VARCHAR2,
      x_bank_account_name     OUT      VARCHAR2,
      x_bank_account_number   OUT      VARCHAR2,
      x_description           OUT      VARCHAR2,
      x_error_msg             OUT      VARCHAR2
   );

   FUNCTION account_seg_status (
      p_seg_value   IN   VARCHAR2,
      p_seg_desc    IN   VARCHAR2
   )
      RETURN VARCHAR2;

   g_org_id          NUMBER := fnd_profile.VALUE ('ORG_ID');
   v_set_of_bks_id   NUMBER := fnd_profile.VALUE ('GL_SET_OF_BKS_ID');
END xxabrl_resa_ar_tndr_conv_pkg; 
/


CREATE OR REPLACE PACKAGE APPS.XXMRL_SUPP_BANK_PMT_RBL AS

  /**********************************************************************************************************************************************
            Name        : Supplier Bank Outbound interface for RBL Bank

            Change Record:
           ===========================================================================================
             Date                               Author                   Remarks                                         Documnet Ref
             ==========   =============              ============      =============================================
              12-Jun-2019     Lokesh Poojari              New Development               Bank Account number hardcoded

  ************************************************************************************************************************************************/

PROCEDURE XXMRL_RBLBANK_PAYMENT_SUPP ( errbuf     out VARCHAR2
                                  ,      retcode    out VARCHAR2
                                  ,      P_FROM_DATE  VARCHAR2
                                  ,      P_TO_DATE   VARCHAR2   );
--This Procedure will Process all Valid Payments into RBL_PAYMENT_TABLE

PROCEDURE XXMRL_UPDATE_RBL_INFO (  errbuf     out VARCHAR2
                                  ,  retcode    out VARCHAR2);
/*This Program will Update (AP_CHECKS_ALL) the DFF in Payment Screen(RBL Bank Payment Information),
   Once the RBL Bank will Provide
*/
PROCEDURE XXMRL_USER_RECTIFIED_PAY( errbuf     out VARCHAR2
                                  ,  retcode    out VARCHAR2
                                  ,  p_from_check_id NUMBER
                                  ,  p_to_check_id NUMBER );
/*  This Program will Update the RBL_PAYMENT_TABLE ,
    with all Corrected data by User as well as Transaction Status as
*/

PROCEDURE XXMRL_SCD_RBL_PAYMENT_SUPP( errbuf     out VARCHAR2
                                         ,  retcode    out VARCHAR2);
/*  This Procedure will Process all Valid Payments into RBL_PAYMENT_TABLE thru Schdule Program
*/

END;
/


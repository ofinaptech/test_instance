CREATE OR REPLACE PACKAGE APPS.XXABRL_AP_NAVISION_REC_PKG AS
PROCEDURE XXABRL_AP_NAVISION_REC_PROC (ERRBUFF             OUT VARCHAR2,
                                       RETCODE             OUT NUMBER,
                                       p_source            IN  VARCHAR2,
                                       p_operating_unit    IN  VARCHAR2,
                                       p_inv_date_from     IN VARCHAR2,
                                       p_inv_date_to       IN VARCHAR2
                                      );
end XXABRL_AP_NAVISION_REC_PKG; 
/


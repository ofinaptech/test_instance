CREATE OR REPLACE PACKAGE APPS.xxabrl_mig_ar_recpt_cust_pkg
IS
   PROCEDURE cust_receipt_validate (
      p_errbuf             OUT      VARCHAR2,
      p_retcode            OUT      NUMBER,
      p_action             IN       VARCHAR2,
      p_data_source        IN       VARCHAR2,
      p_receipt_to_apply   IN       VARCHAR2,
      p_apply_all_flag     IN       VARCHAR2,
      p_gl_date            IN       VARCHAR2
   );

   PROCEDURE cust_receipt_insert (
      p_org_id        IN   NUMBER,
      p_data_source   IN   VARCHAR2,
      p_gl_date       IN   DATE
   );
--  PROCEDURE CUST_RECEIPT_APPLY_CALL(P_Ref_Number IN VARCHAR2
--                                   ,P_GL_DATE    IN DATE);

--  PROCEDURE CUST_RECEIPT_APPLY_DATA(
--                                ARR_R2             IN   XXABRL_AR_MIG_RECEIPT_ALL%ROWTYPE
--                               ,p_Rcpt_Amt_To_Apply   NUMBER
--                               ,x_Rcpt_Bal_Amt    OUT NUMBER
--                               ,x_exit_flag       OUT VARCHAR2
--                               );
END xxabrl_mig_ar_recpt_cust_pkg; 
/


CREATE OR REPLACE PACKAGE APPS.XXABRL_NAV_AR_RECPT_IMP_PKG IS
  PROCEDURE RECEIPT_VALIDATE(p_errbuf      OUT VARCHAR2,
                             p_retcode     OUT NUMBER,
                             p_action IN VARCHAR2,
                             P_Data_Source IN VARCHAR2,
                             p_receipt_to_apply IN VARCHAR2,
                             p_apply_all_flag IN VARCHAR2,
                              p_gl_date IN VARCHAR2,
                              p_org_id IN NUMBER
                             );
  PROCEDURE RECEIPT_INSERT(P_Org_Id IN NUMBER, P_Data_Source IN VARCHAR2,p_gl_date IN DATE);

  PROCEDURE RECEIPT_APPLY_CALL(P_Ref_Number IN VARCHAR2,P_GL_DATE IN DATE);

  PROCEDURE RECEIPT_APPLY_DATA(
                               ARR_R2 IN XXABRL_NAVI_AR_RECEIPT_INT%ROWTYPE
                               ,p_Rcpt_Amt_To_Apply NUMBER
                               ,x_Rcpt_Bal_Amt OUT NUMBER
                               ,x_exit_flag OUT VARCHAR2
                               ,x_org_id in number
                               );

END XXABRL_NAV_AR_RECPT_IMP_PKG; 
/


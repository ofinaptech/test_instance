CREATE OR REPLACE PACKAGE APPS.xxmrl_gl_ind116_pkg
IS
   PROCEDURE xxmrl_gl_valid_proc (x_val_retcode OUT NUMBER);

   PROCEDURE xxmrl_gl_int_proc (x_val_ins_retcode OUT NUMBER);

   PROCEDURE main_proc (x_err_buf OUT VARCHAR2, x_ret_code OUT NUMBER);
END xxmrl_gl_ind116_pkg; 
/


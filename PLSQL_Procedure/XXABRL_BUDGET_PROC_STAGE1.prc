CREATE OR REPLACE PROCEDURE APPS.XXABRL_BUDGET_PROC_STAGE1 (
      errbuf           OUT      VARCHAR2,
      retcode          OUT      VARCHAR2,
      p_start_period   IN       VARCHAR2,
      p_end_period     IN       VARCHAR2
   )
   AS
      CURSOR cur_pr (p_start IN VARCHAR2, p_end IN VARCHAR2)
      IS
         SELECT   prh1.segment1 pr_number,
                  (SUM (prl1.quantity * prl1.unit_price)) open_pr_amt,
                  gcc.code_combination_id
             FROM apps.po_requisition_lines_all prl1,
                  apps.po_req_distributions_all prd1,
                  apps.po_requisition_headers_all prh1,
                  apps.gl_code_combinations_kfv gcc
            WHERE prl1.requisition_line_id = prd1.requisition_line_id
              AND prh1.requisition_header_id = prl1.requisition_header_id
              AND gcc.code_combination_id = prd1.code_combination_id
              AND prh1.authorization_status = 'APPROVED'
              AND prh1.closed_code = 'OPEN'
              AND TRUNC (prh1.creation_date) BETWEEN TRUNC (TO_DATE (p_start))
                                                 AND TRUNC (TO_DATE (p_end))
              AND gcc.segment6 IN
                     ('514002', '514004', '514007', '627001', '627002',
                      '627003', '627004', '627005', '627007', '627008',
                      '628004', '628006', '631030', '628001', '628002',
                      '616003', '616008', '616010', '616002', '616005',
                      '616009', '616012', '616011')
              AND prd1.distribution_id NOT IN (
                                      SELECT NVL (pda1.req_distribution_id, 0)
                                        FROM po_distributions_all pda1)
         GROUP BY prh1.segment1, gcc.code_combination_id;

      CURSOR cur_po (p_start IN VARCHAR2, p_end IN VARCHAR2)
      IS
         SELECT   gcc.code_combination_id, ph1.segment1 po_number,
                  (  SUM (pla1.quantity * pla1.unit_price)
                   + SUM (NVL (xx.tax_amount, 0))
                  ) open_po_amt
             FROM apps.po_lines_all pla1,
                  apps.po_headers_all ph1,
                  apps.jai_po_line_locations xx,
                  apps.po_line_locations_all plla1,
                  apps.po_distributions_all pod,
                  apps.gl_code_combinations_kfv gcc
            WHERE pla1.po_header_id = ph1.po_header_id
              AND gcc.code_combination_id = pod.code_combination_id
              AND ph1.authorization_status = 'APPROVED'
              AND ph1.closed_code = 'OPEN'
              AND TRUNC (ph1.creation_date) BETWEEN TRUNC (TO_DATE (p_start))
                                                AND TRUNC (TO_DATE (p_end))
              AND gcc.segment6 IN
                     ('514002', '514004', '514007', '627001', '627002',
                      '627003', '627004', '627005', '627007', '627008',
                      '628004', '628006', '631030', '628001', '628002',
                      '616003', '616008', '616010', '616002', '616005',
                      '616009', '616012', '616011')
              AND xx.line_location_id(+) = plla1.line_location_id
              AND pla1.po_line_id = plla1.po_line_id
              AND pod.po_line_id = pla1.po_line_id
              AND ph1.po_header_id NOT IN (SELECT NVL (po_header_id, 0)
                                             FROM rcv_transactions rt)
         GROUP BY ph1.segment1, gcc.code_combination_id;

      CURSOR cur_gl (p_end IN VARCHAR2)
      IS
         SELECT   gcc1.code_combination_id,
                  (  SUM (GLB.begin_balance_dr - GLB.begin_balance_cr)
                   + SUM (GLB.period_net_dr - GLB.period_net_cr)
                  ) ending_bal
             FROM apps.gl_balances GLB, apps.gl_code_combinations_kfv gcc1
            WHERE gcc1.code_combination_id = GLB.code_combination_id
              AND GLB.period_year LIKE (SUBSTR (p_end, 8, 11) + 1)
              AND GLB.period_name LIKE
                         UPPER (SUBSTR (p_end, 4, 4) || SUBSTR (p_end, 10, 11))
              AND gcc1.segment6 IN
                     ('514002', '514004', '514007', '627001', '627002',
                      '627003', '627004', '627005', '627007', '627008',
                      '628004', '628006', '631030', '628001', '628002',
                      '616003', '616008', '616010', '616002', '616005',
                      '616009', '616012', '616011')
         GROUP BY gcc1.code_combination_id;

           mon           VARCHAR2 (10) := NULL;
      v_from_date   DATE;
      v_to_date     DATE;
      v_end_date    VARCHAR2 (20);
   BEGIN
      --DELETE FROM xxabrl.xxabrl_budget_report_stage1;

      --COMMIT;
      fnd_file.put_line (fnd_file.LOG, 'Begin');
      v_from_date := fnd_date.canonical_to_date (p_start_period);
      v_to_date := fnd_date.canonical_to_date (p_end_period);
      v_end_date := TO_CHAR (v_to_date, 'DD-MON-YYYY');

      SELECT gp.period_num
        INTO mon
        FROM apps.gl_periods gp
       WHERE 1 = 1
         AND UPPER (gp.period_name) = UPPER (SUBSTR (v_end_date, 4, 4) || 2001);

      BEGIN
         FOR rec_budget_pr IN cur_pr (v_from_date, v_to_date)
         LOOP
            INSERT INTO xxabrl.xxabrl_budget_report_stage1
                 VALUES (rec_budget_pr.code_combination_id,
                         rec_budget_pr.pr_number, NULL,
                         rec_budget_pr.open_pr_amt, NULL, 'OPEN_PR', mon);
         END LOOP;

         COMMIT;
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (fnd_file.LOG,
                               'UNABLE TO INSERT BUDGET TABLLE1');
      END;

      BEGIN
         FOR rec_budget_po IN cur_po (v_from_date, v_to_date)
         LOOP
            INSERT INTO xxabrl.xxabrl_budget_report_stage1
                 VALUES (rec_budget_po.code_combination_id,
                         rec_budget_po.po_number, NULL, NULL,
                         rec_budget_po.open_po_amt, 'OPEN_PO', mon);
         END LOOP;

         COMMIT;
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (fnd_file.LOG,
                               'UNABLE TO INSERT BUDGET TABLLE2');
      END;

      BEGIN
         FOR rec_budget_gl IN cur_gl (v_end_date)
         LOOP
            INSERT INTO xxabrl.xxabrl_budget_report_stage1
                 VALUES (rec_budget_gl.code_combination_id, NULL,
                         rec_budget_gl.ending_bal, NULL, NULL, 'GL_BALANCE',
                         mon);
         END LOOP;

         COMMIT;
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (fnd_file.LOG,
                               'UNABLE TO INSERT BUDGET TABLLE3');
      END;

     COMMIT;
   END XXABRL_BUDGET_PROC_STAGE1; 
/


CREATE OR REPLACE PACKAGE APPS.XXABRL_ASSET_CAPITAL2_PKG AS

PROCEDURE    XXABRL_ASSET_CAPITAL2_PROC   ( errbuf VARCHAR2
                                          ,retcode NUMBER
                                          ,P_BOOK_CODE        FA_MASS_ADDITIONS.BOOK_TYPE_CODE%TYPE
					                                ,P_DATE_FROM        varchar2 --FA_BOOKS.DATE_PLACED_IN_SERVICE%TYPE
			                                    ,P_DATE_TO          varchar2 --FA_BOOKS.DATE_PLACED_IN_SERVICE%TYPE
                                          ,P_INVOICE_NUM      FA_MASS_ADDITIONS.INVOICE_NUMBER%TYPE
					                                ,dummy              number
                                          ,P_ASSET_CATEGORY   FA_MASS_ADDITIONS.ASSET_CATEGORY_ID%TYPE
                                          ,P_STATUS           FA_MASS_ADDITIONS.POSTING_STATUS%TYPE
                                          );

END XXABRL_ASSET_CAPITAL2_PKG;
/


CREATE OR REPLACE PACKAGE APPS.XXABRL_GL_OFFSET_PKG4
AS
PROCEDURE XXABRL_GL_OFFSET_PROC(ERRBUFF OUT VARCHAR2,
                                RETCODE OUT NUMBER,
                                 P_FROM_co VARCHAR2,
                                P_TO_co  VARCHAR2,
                                P_FROM_SBU NUMBER,
                                P_TO_SBU  NUMBER,
                                P_FROM_LOCATION NUMBER,
                                P_TO_LOCATION NUMBER,
                                P_FROM_ACCOUNT NUMBER,
                                P_TO_ACCOUNT NUMBER,
                                P_FROM_GL_DATE date,
                                P_TO_GL_DATE date,
                                P_BATCH_NAME VARCHAR2   
                                );
END XXABRL_GL_OFFSET_PKG4; 
/


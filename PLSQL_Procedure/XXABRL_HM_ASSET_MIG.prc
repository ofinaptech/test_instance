CREATE OR REPLACE PROCEDURE APPS.XXABRL_HM_ASSET_MIG (
   errbuf    OUT   VARCHAR2,
   retcode   OUT   NUMBER
)
IS
   CURSOR asset_migration
   IS
      (SELECT stg.ROWID, stg.sno,
              gcc.code_combination_id exp_code_combination_id,
              SUBSTR (stg.asset_description, 1, 80) asset_description,
              fl.location_id asset_location, stg.fixed_assets_cost,
              stg.payables_units, stg.fixed_assets_units, stg.queue_name,
              stg.date_in_service, fak.code_combination_id exp_cc_id,
              fc.category_id asset_category_id, stg.life_in_months
         FROM xxabrl.xxabrl_asset_migration_stg stg,
              apps.fa_categories fc,
              apps.fa_category_books fb,
              apps.fa_locations fl,
              apps.gl_code_combinations_kfv gcc,
              apps.fa_asset_keywords fak
        WHERE stg.major = fc.segment1
          AND stg.minor1 = fc.segment2
          AND stg.minor2 = fc.segment3
          AND fc.category_id = fb.category_id
          AND fb.book_type_code = 'MORE FA BOOK'
          AND stg.asset_location = fl.segment1
          AND stg.segment1 = gcc.segment1
          AND stg.segment2 = gcc.segment2
          AND stg.segment3 = gcc.segment3
          AND stg.segment4 = gcc.segment4
          AND stg.segment5 = gcc.segment5
          AND stg.segment6 = gcc.segment6
          AND stg.segment7 = gcc.segment7
          AND stg.segment8 = gcc.segment8
          AND fak.segment1 = stg.asset_key);
BEGIN
   FOR asset_mig IN asset_migration
   LOOP
      INSERT INTO apps.fa_mass_additions
                  (mass_addition_id, description,
                   asset_category_id, book_type_code,
                   fixed_assets_cost, payables_units,
                   fixed_assets_units,
                   expense_code_combination_id,
                   location_id, posting_status,
                   queue_name, depreciate_flag, asset_key_ccid, created_by,
                   last_updated_by, creation_date, last_update_date,
                   date_placed_in_service, asset_type, deprn_method_code,
                   life_in_months, accounting_date
                  )
           VALUES (fa_mass_additions_s.NEXTVAL, asset_mig.asset_description,
                   asset_mig.asset_category_id, 'MORE FA BOOK',
                   asset_mig.fixed_assets_cost, asset_mig.payables_units,
                   asset_mig.fixed_assets_units,
                   asset_mig.exp_code_combination_id,
                   asset_mig.asset_location, asset_mig.queue_name,
                   asset_mig.queue_name, 'YES', asset_mig.exp_cc_id, 4191,
                   4191, SYSDATE, SYSDATE,
                   asset_mig.date_in_service, 'CAPITALIZED', 'ABRL STL',
                   asset_mig.life_in_months, '1-apr-16'
                  );

      UPDATE xxabrl.xxabrl_asset_migration_stg
         SET process_flag = 'Success'
       WHERE ROWID = asset_mig.ROWID AND sno = asset_mig.sno;

      fnd_file.put_line (fnd_file.output,
                            'SNO'
                         || ' : '
                         || asset_mig.sno
                         || 'is inserted into FA Interface Table'
                        );
      COMMIT;
   END LOOP;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Error in the Input files');
      fnd_file.put_line (fnd_file.LOG, 'Error in the Input files');
END; 
/


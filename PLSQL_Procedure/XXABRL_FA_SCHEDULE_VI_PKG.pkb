CREATE OR REPLACE PACKAGE BODY APPS.XXABRL_FA_SCHEDULE_VI_PKG AS


PROCEDURE XXABRL_FA_SCHEDULE_VI_PROC( errbuf VARCHAR2
                                    ,retcode NUMBER
                                    ,P_DATE_FROM        varchar2 --FA_BOOKS.DATE_PLACED_IN_SERVICE%TYPE
			            ,P_DATE_TO          varchar2 --FA_BOOKS.DATE_PLACED_IN_SERVICE%TYPE
                                    ,P_BOOK_CODE        FA_BOOKS.BOOK_TYPE_CODE%TYPE
--			            ,dummy              number
                                   )
IS

l_full_year      varchar2(20);
l_year           varchar2(10);
l_ass_full_year  varchar2(20);
l_ass_year       varchar2(10);
l_year_end       varchar2(20);
l_year_start     varchar2(20);
l_prev_year      varchar2(20);

l_total_as_on_date                  number(30,2);
l_total_for_the_yr_additions        number(30,2);
l_total_for_the_yr_deductions       number(30,2);
l_total_as_on                       number(30,2);  --
l_total_dep_as_on_date              number(30,2);
l_total_dep_for_the_yr              number(30,2);
l_total_dec_dep_for_the_yr    number(30,2);
l_total_dep_as_on                   number(30,2); --
l_total_netblock1                   number(30,2);
l_total_netblock2                   number(30,2);


lc_balance_gross             number(30,2) := 0;
lc_balance_deprn             number(30,2) := 0;
lc_netblock1                 number(30,2) := 0;
lc_netblock2                 number(30,2) := 0;

l_fixed_asset_cost           number;
l_cwip_end_date              date;

-- Cursor to hold data from the date placed in service till from date parameter
CURSOR cur_data1 --(p_fdate DATE, p_tdate DATE)
IS
 SELECT
		 a.TYPE particulars
		, a.block_id
		,SUM(bk.COST )  as_on_date
		, 0 for_the_yr_additions
      ,SUM(fr.proceeds_of_sale ) for_the_yr_deductions
     , SUM((bk.COST*(a.rate/100)*(d.rate/100)))  dep_as_on_date
	 		, 0 dep_for_the_yr
     ,SUM( (fr.proceeds_of_sale*(a.rate/100)*(d.rate/100)))  deduction_dep_for_the_yr
FROM
		 JAI_FA_AST_BLOCKS     a
		,JAI_FA_DEP_BLOCKS     b
		,fa_retirements        fr
		,fa_books              bk
--		,JAI_FA_AST_BLOCK_DTLS   dtls
		,JAI_FA_AST_PERIOD_RATES d
		,fa_additions_b ad
				,fa_additions_tl  ad2
WHERE
		    a.block_id = b.block_id
		AND b.slno =0
		AND a.opening_wdv >= 0
		AND a.year_ended = b.year_ended
		AND a.closing_wdv IS NOT NULL
		AND a.book_type_code = bk.book_type_code
		AND a.book_type_code = P_BOOK_CODE
		AND fr.transaction_header_id_in(+) = bk.transaction_header_id_in
		AND bk.asset_id = fr.asset_id(+)
		AND bk.book_type_code= fr.book_type_code(+)
--		AND bk.asset_id = dtls.asset_id
--		AND a.block_id = dtls.block_id
		AND bk.date_ineffective IS  NULL
		AND bk.transaction_header_id_out IS   NULL
		AND bk.date_placed_in_service BETWEEN d.start_date  AND d.end_date   --02-02-2002 changed from RR to RRRR
	AND d.year_start = TO_DATE(NVL(P_DATE_FROM,d.year_start),'YYYY/MM/DD HH24:MI:SS')
--	AND  bk.date_placed_in_service BETWEEN TO_DATE(NVL(P_DATE_FROM,a.start_date),'YYYY/MM/DD HH24:MI:SS')  AND NVL(P_DATE_TO,a.year_ended)
AND bk.date_placed_in_service <= NVL(TO_DATE(P_DATE_FROM,'YYYY/MM/DD HH24:MI:SS'),bk.date_placed_in_service)
AND ad.asset_id = bk.asset_id
AND ad.asset_id = ad2.asset_id
     AND ad.parent_asset_id IS NULL
     AND ad2.LANGUAGE = USERENV ('LANG')
		GROUP BY   		 a.block_id
		                              , a.TYPE
                                     ,a.start_date
                                    ,a.year_ended
ORDER BY   SUBSTR(a.TYPE,1,20);


-- Cursor to hold data for the year
CURSOR cur_data2 (p_block_id IN NUMBER)
IS
SELECT
		 a.TYPE particularss
		, a.block_id
		 , 0 as_on_date
		,SUM(bk.COST )  for_the_yr_additions
      ,SUM(fr.proceeds_of_sale ) for_the_yr_deductions
		 , 0 dep_as_on_date
     , SUM((bk.COST*(a.rate/100)*(d.rate/100)))  dep_for_the_yr
     ,SUM( (fr.proceeds_of_sale*(a.rate/100)*(d.rate/100)))  deduction_dep_for_the_yr
FROM
		 JAI_FA_AST_BLOCKS     a
		,JAI_FA_DEP_BLOCKS     b
		,fa_retirements        fr
		,fa_books              bk
--		,JAI_FA_AST_BLOCK_DTLS   dtls
		,JAI_FA_AST_PERIOD_RATES d
		,fa_additions_b ad
				,fa_additions_tl  ad2
WHERE
		    a.block_id = b.block_id
		AND b.slno =0
		AND a.opening_wdv >= 0
		AND a.year_ended = b.year_ended
		AND a.closing_wdv IS NOT NULL
		AND a.book_type_code = bk.book_type_code
		AND a.book_type_code = P_BOOK_CODE
		AND fr.transaction_header_id_in(+) = bk.transaction_header_id_in
		AND bk.asset_id = fr.asset_id(+)
		AND bk.book_type_code= fr.book_type_code(+)
--		AND bk.asset_id = dtls.asset_id
--		AND a.block_id = dtls.block_id
		AND bk.date_ineffective IS  NULL
		AND bk.transaction_header_id_out IS   NULL
		AND bk.date_placed_in_service BETWEEN d.start_date  AND d.end_date   --02-02-2002 changed from RR to RRRR
	AND d.year_start = TO_DATE(NVL(P_DATE_FROM,d.year_start),'YYYY/MM/DD HH24:MI:SS')
	AND  bk.date_placed_in_service BETWEEN TO_DATE(NVL(P_DATE_FROM,a.start_date),'YYYY/MM/DD HH24:MI:SS')  AND NVL(P_DATE_TO,a.year_ended)
-- AND bk.date_placed_in_service <= NVL(TO_DATE(P_DATE_FROM,'YYYY/MM/DD HH24:MI:SS'),bk.date_placed_in_service)
AND ad.asset_id = bk.asset_id
AND ad.asset_id = ad2.asset_id
     AND ad.parent_asset_id IS NULL
     AND ad2.LANGUAGE = USERENV ('LANG')
     AND a.block_id = p_block_id
		GROUP BY   		 a.block_id
		                              , a.TYPE
                                     ,a.start_date
                                    ,a.year_ended
ORDER BY   SUBSTR(a.TYPE,1,20);

BEGIN



			BEGIN

			SELECT SUBSTR(TO_CHAR(d.year_start,'DD-MON-RRRR'),8,4)
			      ,SUBSTR(SUBSTR(TO_CHAR(d.year_start,'DD-MON-RRRR'),8,4)  + 1,3,4)
			      ,TO_CHAR(d.year_end,'DD-MON-RRRR')
			      ,TO_CHAR(d.year_start,'DD-MON-RRRR')
			      ,TO_CHAR((TO_DATE(d.year_start,'DD-MON-RRRR')  -1),'DD-MON-RRRR')
			      ,TO_DATE(d.year_end,'DD-MON-RRRR')
			       INTO l_full_year
			           ,l_year
				   ,l_year_end
				   ,l_year_start
				   ,l_prev_year
				   ,l_cwip_end_date
			FROM JAI_FA_AST_PERIOD_RATES d
			WHERE d.year_start = TO_DATE(NVL(P_DATE_FROM,d.year_start),'YYYY/MM/DD HH24:MI:SS')
			--AND d.start_date = P_DATE_FROM;
			AND d.start_date = TO_DATE(NVL(P_DATE_FROM,d.year_start),'YYYY/MM/DD HH24:MI:SS') ;
			EXCEPTION
			WHEN NO_DATA_FOUND THEN
				Fnd_File.put_line(Fnd_File.log,'Exception found No_dat_found in selecting year');
			WHEN TOO_MANY_ROWS THEN
				Fnd_File.put_line(Fnd_File.log,'Exception found too_many_rows in selecting year');
			WHEN OTHERS THEN
				Fnd_File.put_line(Fnd_File.log,'Others Exception found  in selecting year');
			END;


			BEGIN

			SELECT NVL(SUM(fixed_assets_cost),0)
			INTO l_fixed_asset_cost
			FROM fa_mass_additions ma
			WHERE ma.posting_status = 'NEW'
			AND ma.date_placed_in_service <= l_cwip_end_date;
			EXCEPTION
			WHEN OTHERS THEN
				--Fnd_File.put_line(Fnd_File.log,'Others Exception found  in CWIP Cost Query');
				l_fixed_asset_cost := 0;
			END;


                     -- Assessment year
			l_ass_full_year := l_full_year +1;
			l_ass_year      := l_year +1;


       Fnd_File.put_line(Fnd_File.OUTPUT,CHR(9)            ||'ABRL Fixed Assets Schedule VI Report');
--       Fnd_File.put_line(Fnd_File.OUTPUT,CHR(9)            ||'---------------------------');
       Fnd_File.put_line(Fnd_File.OUTPUT,CHR(9)            ||'                   ');
              Fnd_File.put_line(Fnd_File.OUTPUT,CHR(9)     ||'ADITYA BIRLA RETAIL LTD');
        Fnd_File.put_line(Fnd_File.OUTPUT,CHR(9)           ||'FIXED ASSETS SCHEDULE AS PER BOOKS AS ON '||l_year_end);
      Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || 'As on Date:'
                         || CHR (9)
                         || SYSDATE
                         || CHR (9)
                        );
      Fnd_File.put_line (Fnd_File.output, ' ');
      Fnd_File.put_line (Fnd_File.output, 'REPORT PARAMETRS ');
      Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || 'BOOK TYPE CODE:'
                         || CHR (9)
                         || P_BOOK_CODE
                         || CHR (9)
                        );
        Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || 'Start Date:'
                         || CHR (9)
                         || P_DATE_FROM
                         || CHR (9)
                        );

-------------

	l_total_as_on_date                  := 0;
	l_total_for_the_yr_additions        := 0;
	l_total_for_the_yr_deductions       := 0;
	l_total_as_on                       := 0;
	l_total_dep_as_on_date              := 0;
	l_total_dep_for_the_yr              := 0;
	l_total_dec_dep_for_the_yr    := 0;
	l_total_dep_as_on                   := 0;
	l_total_netblock1                   := 0;
	l_total_netblock2                   := 0;

	Fnd_File.put_line (Fnd_File.output, ' ');
	Fnd_File.put_line (Fnd_File.output, ' ');


       --Printing Output
       Fnd_File.put_line(Fnd_File.OUTPUT,                    'Tangible Assets'|| CHR(9)
                                                           ||'As on '|| l_year_start ||CHR(9)
	                                                   ||'Additions'||CHR(9)
	                                                   ||'Deductions'||CHR(9)
           						   ||'As on ' || l_year_end ||CHR(9)
			           			   ||'Up to '|| l_year_start ||CHR(9)
							   ||'For the year'||CHR(9)
						           ||'Deductions Transfer Out'||CHR(9)
							   ||'As on '|| l_year_end ||CHR(9)
 							   ||'As on '|| l_prev_year ||CHR(9)
           						   ||'As on '|| l_year_end ||CHR(9)
							   );
	Fnd_File.put_line (Fnd_File.output, ' ');

       --Opening and Fetching Cursor Values into local variables.
	   FOR rec_data1 IN cur_data1
	   LOOP
--	   Fnd_File.put_line (Fnd_File.output, 'Inside loop1 ');
		   FOR rec_data2 IN cur_data2(rec_data1.block_id)
		   LOOP

--Fnd_File.put_line (Fnd_File.output, 'Inside loop1 ');
	-- to get balance value
        lc_balance_gross := (NVL(rec_data1.as_on_date,0)+NVL(rec_data2.as_on_date,0) + NVL(rec_data1.for_the_yr_additions,0)+NVL(rec_data2.for_the_yr_additions,0)  - (NVL(rec_data1.for_the_yr_deductions,0)+NVL(rec_data2.for_the_yr_deductions,0))) ;
	lc_balance_deprn := (NVL(rec_data1.dep_as_on_date,0)+NVL(rec_data2.dep_as_on_date,0) + NVL(rec_data1.dep_for_the_yr,0)+NVL(rec_data2.dep_for_the_yr,0) - (NVL(rec_data1.deduction_dep_for_the_yr,0)+NVL(rec_data2.deduction_dep_for_the_yr,0))) ;
	lc_netblock1 := (    (NVL(rec_data1.as_on_date,0)+NVL(rec_data2.as_on_date,0)) - (NVL(rec_data1.dep_as_on_date,0)+NVL(rec_data2.dep_as_on_date,0))          ) ;
	lc_netblock2 := ( (NVL(lc_balance_gross,0) - (NVL(lc_balance_deprn,0))) );

	l_total_as_on_date                  := l_total_as_on_date+NVL(rec_data1.as_on_date,0)+NVL(rec_data2.as_on_date,0) ;
	l_total_for_the_yr_additions        := l_total_for_the_yr_additions+NVL(rec_data1.for_the_yr_additions,0) +NVL(rec_data2.for_the_yr_additions,0);
	l_total_for_the_yr_deductions       := l_total_for_the_yr_deductions+ NVL(rec_data1.for_the_yr_deductions,0)+NVL(rec_data2.for_the_yr_deductions,0);
	l_total_as_on                       := l_total_as_on+ NVL(lc_balance_gross,0);
	l_total_dep_as_on_date              := l_total_dep_as_on_date+ NVL(rec_data1.dep_as_on_date,0)+NVL(rec_data2.dep_as_on_date,0);
	l_total_dep_for_the_yr              := l_total_dep_for_the_yr+NVL(rec_data1.dep_for_the_yr,0)+NVL(rec_data2.dep_for_the_yr,0);
	l_total_dec_dep_for_the_yr          := l_total_dec_dep_for_the_yr+NVL(rec_data1.deduction_dep_for_the_yr,0)+NVL(rec_data2.deduction_dep_for_the_yr,0);
	l_total_dep_as_on                   := l_total_dep_as_on+NVL(lc_balance_deprn,0);
	l_total_netblock1                   := l_total_netblock1+NVL(lc_netblock1,0);
	l_total_netblock2                   := l_total_netblock2+NVL(lc_netblock2,0);

--		,(a.opening_wdv*a.rate/100) +  (bk.COST*(a.rate/100)*(d.rate/100))  total
--		, a.opening_wdv - ((a.opening_wdv*a.rate/100) +  (bk.COST*(a.rate/100)*(d.rate/100))) Net_balance



	              --Printing Local Variable Values to output.
				  Fnd_File.put_line(Fnd_File.OUTPUT,rec_data1.particulars||CHR(9)
	                                                   ||(NVL(rec_data1.as_on_date,0)+NVL(rec_data2.as_on_date,0))||CHR(9)
	                                                   ||(NVL(rec_data1.for_the_yr_additions,0)+NVL(rec_data2.for_the_yr_additions,0))||CHR(9)
							   ||(NVL(rec_data1.for_the_yr_deductions,0)+NVL(rec_data2.for_the_yr_deductions,0))||CHR(9)
           						   ||lc_balance_gross||CHR(9)
			           			   ||(NVL(rec_data1.dep_as_on_date,0)+NVL(rec_data2.dep_as_on_date,0))||CHR(9)
						           ||(NVL(rec_data1.dep_for_the_yr,0)+NVL(rec_data2.dep_for_the_yr,0))||CHR(9)
							   ||(NVL(rec_data1.deduction_dep_for_the_yr,0)+NVL(rec_data2.deduction_dep_for_the_yr,0))||CHR(9)
           						   ||lc_balance_deprn||CHR(9)
			           			   ||lc_netblock1||CHR(9)
							   ||lc_netblock2||CHR(9)
							   );

	   END LOOP;
     END LOOP;

			    -- printing Totals for all the column
			    	  Fnd_File.put_line (Fnd_File.output, ' ');
				  Fnd_File.put_line(Fnd_File.OUTPUT,'Total'||CHR(9)
	                                                   ||l_total_as_on_date||CHR(9)
	                                                   ||l_total_for_the_yr_additions||CHR(9)
							   ||l_total_for_the_yr_deductions||CHR(9)
							   ||l_total_as_on||CHR(9)
           						   ||l_total_dep_as_on_date||CHR(9)
			           			   ||l_total_dep_for_the_yr||CHR(9)
						           ||l_total_dec_dep_for_the_yr||CHR(9)
							   ||l_total_dep_as_on||CHR(9)
							   ||l_total_netblock1||CHR(9)
           						   ||l_total_netblock2||CHR(9)
							   );

                                 Fnd_File.put_line (Fnd_File.output, ' ');
                                 Fnd_File.put_line (Fnd_File.output, 'Add:-CWIP '||CHR(9)||CHR(9)||CHR(9)||CHR(9)||l_fixed_asset_cost );

                                 Fnd_File.put_line (Fnd_File.output, ' ');
                                 Fnd_File.put_line (Fnd_File.output, 'TOTAL '||CHR(9)||CHR(9)||CHR(9)||CHR(9)||(l_total_as_on + l_fixed_asset_cost) );


--Fnd_File.put_line (Fnd_File.output,'The cars taken on lease has been capitalised in the books of account of the Company as per Accounting Standard 19 Accounting for Leases');

    END XXABRL_FA_SCHEDULE_VI_PROC;
END XXABRL_FA_SCHEDULE_VI_PKG;
/


CREATE OR REPLACE PACKAGE BODY APPS.xxap_einv_pkg
AS
------------------------------------------------------------------------------------------------------------------------------
-- Version     Date            Modified By            Description                                                           --
------------------------------------------------------------------------------------------------------------------------------
-- 1.0         01-Oct-2020     Pawan Sahu             Initial Version                                                       --
-- 1.1         31-Dec-2020     Pawan Sahu             Company Name Change to "More Retail Private Limited"                  --
-- 1.2         05-Apr-2021     Pawan Sahu             Modified to pick transactions from last program start time            --
-- 1.3         09-Jun-2021     Pawan Sahu             Introduced Dynamic QR code for B2C Transactions                       --
------------------------------------------------------------------------------------------------------------------------------
PROCEDURE export_einv (errbuf                out varchar2,
                       retcode               out number,
                       p_reprocess_from_date in varchar2)
IS
 l_last_run_timestamp date;
 l_prog_running       varchar2(1);
 l_supply_type        varchar2(10); -- V1.3 Added
 l_buyer_gstin        varchar2(50); -- V1.3 Added
 -- Check if request is already running
 -- Fetch last successful run timestamp

 cursor c_inv (p_last_run_timestamp in date) is
 select aia.invoice_id, aia.vendor_id, aia.vendor_site_id, aia.invoice_type_lookup_code header,
        --'B2B' supply_type, -- V1.3 Commented
        decode(aia.invoice_type_lookup_code, 'STANDARD', 'CRN', 'INV') doc_type,
        aia.invoice_num document_number, aid.accounting_date doc_date,
        (select distinct registration_number
                    from apps.jai_party_regs jpr,
                         apps.jai_party_reg_lines jprl
                   where 1 = 1
                     and jpr.party_reg_id = jprl.party_reg_id
                     and jpr.party_id = aia.org_id
                     and jpr.party_type_code = 'OU'
                     and jpr.org_classification_code = 'TRADING'
                     and jprl.registration_type_code = 'GSTIN') seller_gstin,
        --'More Retail Limited' seller_legal_name, -- V1.1 Commented
        'More Retail Private Limited' seller_legal_name, -- V1.1 Added
        -- fetch below information
        loc.address_line_1 seller_address_1,
        loc.address_line_2 seller_address_2,
        loc.loc_information15 seller_city,
        loc.postal_code seller_pincode,
        --
        (select distinct jprl.registration_number
           from apps.jai_party_regs jpr,
                apps.jai_party_reg_lines jprl
          where 1 = 1
            and jpr.org_id = aia.org_id
            and jpr.party_id = aia.vendor_id
            and jpr.party_site_id = aia.vendor_site_id
            and jpr.party_reg_id = jprl.party_reg_id
            and registration_type_code = 'GSTIN'
            and rownum <= 1) buyer_gstin,
        sup.vendor_name_alt buyer_legal_name, --sup.vendor_name buyer_legal_name,
        sup.vendor_name_alt buyer_trade_name, --sup.vendor_name buyer_trade_name,
        sites.address_line1 buyer_address_1,
        sites.address_line2 buyer_address_2,
        (select flv.meaning from fnd_lookup_values flv
          where lookup_type = 'JA_IN_STATE_CODE'
            and flv.description = sites.state) buyer_pos, -- V1.3 added
        sites.city buyer_city,
        sites.zip buyer_pincode,
        ail.line_number serial_number,
        decode (length(ail.attribute11), 8, 'N', 6, 'Y') is_service,
        ail.attribute11 hsn_code,
        'NOS' units, 1 qty, round(abs(ail.amount), 2) line_amount,
        18 gst_rate, null cess_rate, null advlrm_amount,
        round((select sum (abs(amount))
           from apps.ap_invoice_distributions_all aid1
          where aid1.invoice_id = aia.invoice_id
            --and aid1.invoice_line_number = ail.line_number
            and aid1.line_type_lookup_code in ('ITEM', 'MISCELLANEOUS')), 2) total_item_value,
        round((select sum (abs(amount))
           from apps.ap_invoice_distributions_all aid1
          where aid1.invoice_id = aia.invoice_id
            --and aid1.invoice_line_number = ail.line_number
            and aid1.line_type_lookup_code <> 'MISCELLANEOUS'), 2) total_assessable_value,
        round(abs(aia.invoice_amount), 2) final_invoice_value,
        null transporter_id, null distance,
        sup.vendor_name
        --apps.xxabrl_inv_status(aia.invoice_id, aia.invoice_amount, aia.payment_status_flag, aia.invoice_type_lookup_code) status
   from ap_invoices_all aia, ap_invoice_lines_all ail, ap_invoice_distributions_all aid,
        ap_suppliers sup, ap_supplier_sites_all sites, hr_all_organization_units org, hr_locations_all loc
  where 1=1
    and aia.invoice_id = ail.invoice_id
    and ail.line_type_lookup_code = 'ITEM'
   -- and ail.attribute_category = 'SMDN DEBIT NOTE  SAC CODE'
    and ail.invoice_id = aid.invoice_id
    and ail.line_number = aid.invoice_line_number
    and aid.line_type_lookup_code = 'ITEM'
    and aia.vendor_id = sup.vendor_id
    and aia.vendor_id = sites.vendor_id
    and aia.vendor_site_id = sites.vendor_site_id
    --and aid.dist_code_combination_id = glcc.code_combination_id
    and aia.org_id = org.organization_id
    and org.location_id = loc.location_id(+)
    and apps.xxabrl_inv_status(aia.invoice_id, aia.invoice_amount, aia.payment_status_flag, aia.invoice_type_lookup_code) IN ('APPROVED') -- Validated
    and invoice_type_lookup_code in ('STANDARD', 'DEBIT', 'CREDIT')
    and trunc(aid.accounting_date) >= trunc(to_date('01-OCT-2020', 'DD-MON-RRRR'))
    and aia.last_update_date >= nvl(to_date(p_reprocess_from_date, 'yyyy/mm/dd hh24:mi:ss'), p_last_run_timestamp)
    --and glcc.segment6 in (364048, 364049, 364050)
    and exists (select distinct 1 from ap_invoice_distributions_all aid1, gl_code_combinations glcc1
                 where aid1.invoice_id = aia.invoice_id
                   and aid1.dist_code_combination_id = glcc1.code_combination_id
                   and aid1.line_type_lookup_code = 'MISCELLANEOUS'
                   and glcc1.segment6 in (364048, 364049, 364050))
    and not exists (select distinct 1 from apps.xxap_gst_einv_headers xgeh
                     where xgeh.invoice_id = aia.invoice_id
                       and xgeh.processed_flag = 'Y') -- not already processed in custom table
    order by aia.invoice_id, ail.line_number;

    cursor c_json (p_request_id in number)
    is
    select stg.*,
          '{"invoices":[{"CustDocNo":"'||stg.document_number||'","TranDtls":{"SupTyp":"'||stg.supply_type||'"},"DocDtls":{"Typ":"'||stg.doc_type||'","No":"'||stg.document_number||'","Dt":"'||to_char(stg.doc_date, 'DD/MM/YYYY')||'"},"SellerDtls": {"Gstin":"'||stg.seller_gstin||'","LglNm":"'||stg.seller_legal_name||'","Addr1":"'||stg.seller_address_1||'","Addr2":"'||stg.seller_address_2||'","Loc":"'||stg.seller_city||'","Pin":"'||stg.seller_pincode||'"},"BuyerDtls": {"Gstin":"'||stg.buyer_gstin||'","LglNm":"'||stg.buyer_legal_name||'","TrdNm":"'||stg.buyer_trade_name||'","Addr1":"'||stg.buyer_address_1||'","Addr2":"'||stg.buyer_address_2||'","Pos":"'||stg.buyer_pos||'","Loc":"'||stg.buyer_city||'","Pin":"'||stg.buyer_pincode||'"},"ItemList": [{"SlNo":"'||stg.serial_number||'","IsServc":"'||stg.is_service||'","HsnCd":"'||stg.hsn_code||'","Qty":"'||stg.qty||'","Unit":"'||stg.units||'","UnitPrice":"'||stg.unit_price||'","TotAmt":"'||stg.total_amount||'","AssAmt":"'||stg.assessable_amount||'","GstRt":"'||stg.gst_rate||'","CesRt":"'||stg.cess_rate||'","CesNonAdvlAmt":"'||stg.advlrm_amount||'","TotItemVal":"'||stg.total_item_value||'"}],"ValDtls":{"AssVal":"'||stg.total_assessable_value||'","TotInvVal":"'||stg.final_invoice_value||'"}}]}' json_data
     from xxabrl.xxap_gst_einv_headers stg
    where stg.request_id = p_request_id
    order by stg.invoice_id;

BEGIN

   fnd_file.put_line (fnd_file.LOG, 'p_reprocess_from_date : '||p_reprocess_from_date);

   begin
     l_prog_running := 'N';
     select distinct 'Y'
       into l_prog_running
       from fnd_conc_req_summary_v
      where program_short_name = 'XXAP_EINV_SUPERTAX_INT'
        and phase_code = 'R'
        and request_id not in (g_request_id); -- to ignore this run
   exception when others then
     l_prog_running := 'N';
   end;

   begin
     l_last_run_timestamp := null;
     select max(actual_start_date) -- max(actual_completion_date) -- V1.2 Modified on 05-Apr-2021
       into l_last_run_timestamp
       from fnd_conc_req_summary_v
      where program_short_name = 'XXAP_EINV_SUPERTAX_INT'
        and phase_code = 'C' and status_code = 'C';
   exception when others then
     l_last_run_timestamp := sysdate -1; -- consider last 24 hours
   end;

   if l_prog_running = 'Y' then--- program already running, exit
      fnd_file.put_line (fnd_file.LOG, 'Program already running...');
   else
     fnd_file.put_line (fnd_file.LOG, 'Fetching data from base tables and Inserting into XXAP_GST_EINV_HEADERS table...');
     fnd_file.put_line (fnd_file.LOG, '---------------------------------------------------------------------------------------------------');

     for i in c_inv (l_last_run_timestamp) loop

       -- V1.3 Start
       l_supply_type := null;
       l_buyer_gstin := null;
       -- If first two characters of GSTN are numeric (that means GSTN registered) then sypply_type -> B2B else B2C
       -- If supply_type -> B2C then GSTN -> URP
       select decode(apps.is_num(substr(nvl(i.buyer_gstin, 'XX'), 1, 2)), 'T', 'B2B', 'F', 'B2C'),
              decode(apps.is_num(substr(nvl(i.buyer_gstin, 'XX'), 1, 2)), 'T', i.buyer_gstin, 'F', 'URP')
         into l_supply_type,
              l_buyer_gstin
         from dual;
       -- V1.3 End

       insert into xxabrl.xxap_gst_einv_headers (invoice_id, vendor_id, vendor_site_id, header, supply_type, doc_type, document_number, doc_date,
                   seller_gstin, seller_legal_name, seller_address_1, seller_address_2, seller_city, seller_pincode,
                   buyer_gstin, buyer_legal_name, buyer_trade_name, buyer_address_1, buyer_address_2, buyer_pos, buyer_city, buyer_pincode,
                   serial_number, is_service, hsn_code, units, qty, unit_price, total_amount, assessable_amount, gst_rate, cess_rate,
                   advlrm_amount, total_item_value, total_assessable_value, final_invoice_value, transporter_id, distance, vendor_name,
                   request_id, request_date, requested_by, processed_flag)
           values (i.invoice_id, i.vendor_id, i.vendor_site_id, i.header, l_supply_type, i.doc_type, i.document_number, i.doc_date,
                   i.seller_gstin, i.seller_legal_name, i.seller_address_1, i.seller_address_2, i.seller_city, i.seller_pincode,
                   --i.serial_number, i.is_service, i.hsn_code, i.units, i.qty, i.line_amount, i.line_amount, i.line_amount,
                   l_buyer_gstin, i.buyer_legal_name, i.buyer_trade_name, i.buyer_address_1, i.buyer_address_2, i.buyer_pos, i.buyer_city, i.buyer_pincode,
                   i.serial_number, i.is_service, i.hsn_code, i.units, i.qty, i.line_amount, i.line_amount, i.line_amount, i.gst_rate, i.cess_rate,
                   i.advlrm_amount, i.total_item_value, i.total_assessable_value, i.final_invoice_value, i.transporter_id, i.distance, i.vendor_name,
                   g_request_id, sysdate, g_user_id, 'N');

      fnd_file.put_line (fnd_file.LOG, 'Invoice ID # '||i.invoice_id ||'. '||'Invoice Number # '|| i.document_number);
     end loop;
     commit;

     fnd_file.put_line (fnd_file.LOG, '-');
     fnd_file.put_line (fnd_file.LOG, 'Formatting data into JSON format and Inserting into XXAP_GST_EINV_JSON table...');
     fnd_file.put_line (fnd_file.LOG, '---------------------------------------------------------------------------------------------------');

     for j in c_json (g_request_id) loop
       if (j.supply_type              is not null and
           j.doc_type                 is not null and
           j.document_number          is not null and
           j.doc_date                 is not null and
           j.seller_gstin             is not null and
           j.seller_legal_name        is not null and
           j.seller_address_1         is not null and
           j.seller_city              is not null and
           j.seller_pincode           is not null and
           j.buyer_gstin              is not null and
           j.buyer_legal_name         is not null and
           j.buyer_trade_name         is not null and
           j.buyer_address_1          is not null and
           j.buyer_pos                is not null and
           j.buyer_city               is not null and
           j.buyer_pincode            is not null and
           j.serial_number            is not null and
           j.is_service               is not null and
           j.hsn_code                 is not null and
           j.unit_price               is not null and
           j.total_amount             is not null and
           j.assessable_amount        is not null and
           j.gst_rate                 is not null and
           j.total_item_value         is not null and
           j.total_assessable_value   is not null and
           j.final_invoice_value      is not null) then

         insert into apps.xxap_gst_einv_json (invoice_id, json_data, irn, json_qr_code, qr_code, request_id, request_date, requested_by, document_number)
         values     (j.invoice_id, j.json_data, null, null, null, g_request_id, j.request_date, j.requested_by, j.document_number);

         commit;
         fnd_file.put_line (fnd_file.LOG, 'JSON inserted for Invoice ID # '||j.invoice_id ||'. '||'Invoice Number # '|| j.document_number);
       else
         update xxabrl.xxap_gst_einv_headers header
            set processed_flag = 'E', remarks = 'Mandatory fields missing.'
          where header.invoice_id = j.invoice_id
            and header.request_id = g_request_id;

         fnd_file.put_line (fnd_file.log, '--');
         fnd_file.put_line (fnd_file.log, 'JSON NOT inserted for Invoice ID # '||j.invoice_id ||'. '||'Invoice Number # '|| j.document_number ||'. Check Mandatory fields.');
       end if;
     end loop;

   end if;
exception when others then
   fnd_file.put_line (fnd_file.log, 'Exception in Main Proc: '||sqlcode||': '||sqlerrm);
end export_einv;

END xxap_einv_pkg;
/


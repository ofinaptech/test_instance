CREATE OR REPLACE package APPS.XXABRL_BANK_BALANCE_PKG
As
Procedure Main(
               Err_Buf              Out Varchar2,
               Ret_code             Out Number,
                p_BANK_ACCOUNT_ID IN number,
               p_period             IN Varchar2,
               p_to_date1            IN Varchar2,
               p_show_un_reconciled IN Varchar2,
               p_report_format      IN Varchar2    
               );
Procedure Display_date(p_open_bal_status IN Varchar2);

Procedure Print_log(
                      p_str IN Varchar2, 
                      p_debug_flag in Varchar2
                      );               

End XXABRL_BANK_BALANCE_PKG; 
/


CREATE OR REPLACE PACKAGE APPS.XXABRL_AR_CUST_IMPORT_PKG AS
PROCEDURE main(
 retbuf           OUT NOCOPY  VARCHAR2
,retcode          OUT NOCOPY  NUMBER
,p_action	 IN VARCHAR2
,p_debug_flag     IN VARCHAR2                               
);


PROCEDURE create_customer_account(
 p_customer        IN XXABRL_AR_CUSTOMER_INT%ROWTYPE
,x_party_id        OUT NUMBER
,x_cust_account_id OUT NUMBER
,x_cust_status     OUT VARCHAR2
,x_error_msg       OUT VARCHAR2
);

PROCEDURE printlog(MSG_TEXT IN VARCHAR2, MSG_FLAG VARCHAR2);

/*
PROCEDURE set_party_tax_profile(
 p_customer       IN XXABRL_AR_CUSTOMER_INT%ROWTYPE
,p_party_id       IN NUMBER
,x_status        OUT VARCHAR2
,x_error_msg     OUT VARCHAR2
);												 
*/

PROCEDURE create_customer_location(
 p_customer    IN XXABRL_AR_CUSTOMER_INT%ROWTYPE
,p_party_id    IN NUMBER
,x_location_id OUT NUMBER
,x_loc_status  OUT VARCHAR2
,x_error_msg   OUT VARCHAR2
);

PROCEDURE create_party_site(
 p_customer          IN XXABRL_AR_CUSTOMER_INT%ROWTYPE
,p_party_id          IN NUMBER
,p_location_id       IN NUMBER
,x_party_site_id     OUT NUMBER
,x_party_site_status OUT VARCHAR2
,x_error_msg         OUT VARCHAR2
);

PROCEDURE create_customer_acct_site ( 
 p_customer           IN XXABRL_AR_CUSTOMER_INT%ROWTYPE
,p_cust_account_id    IN NUMBER
,p_party_site_id      IN NUMBER
,x_cust_acct_site_id OUT NUMBER
,x_cust_site_status  OUT VARCHAR2
,x_error_msg         OUT VARCHAR2
);

PROCEDURE create_customer_site_use (
 p_customer              IN XXABRL_AR_CUSTOMER_INT%ROWTYPE
,p_profile_class_id      IN NUMBER
,p_cust_acct_site_id     IN NUMBER
,p_cust_account_id       IN NUMBER
,p_party_id              IN NUMBER
,p_party_site_id         IN NUMBER
,x_site_use_id          OUT NUMBER
,x_cust_site_use_status OUT VARCHAR2
,x_error_msg            OUT VARCHAR2
);

PROCEDURE validate_customer_data(
 x_error_msg  OUT VARCHAR2);

PROCEDURE validate_cust_number(
 p_cust_number  IN VARCHAR2
,x_error_msg   OUT VARCHAR2);

PROCEDURE validate_location(
 p_customer   IN XXABRL_AR_CUSTOMER_INT%ROWTYPE
,x_error_msg OUT VARCHAR2);

PROCEDURE check_customer_exists(
 p_cust_name    IN VARCHAR2
,x_error_msg   OUT VARCHAR2);

PROCEDURE get_gl_rev_acct_id(
 p_org_code        IN VARCHAR2
,p_gl_id_rev       IN VARCHAR2
,x_gl_rev_acct_id OUT NUMBER
,x_error_msg      OUT VARCHAR2);

PROCEDURE get_gl_freight_acct_id(
 p_org_code            IN VARCHAR2
,p_gl_id_rev           IN VARCHAR2
,x_gl_freight_acct_id OUT NUMBER
,x_error_msg          OUT VARCHAR2);

PROCEDURE get_gl_rec_acct_id(
 p_org_code        IN VARCHAR2
,p_gl_id_rev       IN VARCHAR2
,x_gl_rec_acct_id OUT NUMBER
,x_error_msg      OUT VARCHAR2);

PROCEDURE get_gl_tax_acct_id(
 p_org_code        IN VARCHAR2
,p_gl_id_rev       IN VARCHAR2
,x_gl_tax_acct_id OUT NUMBER
,x_error_msg      OUT VARCHAR2);

PROCEDURE get_gl_clearing_acct_id(
 p_org_code             IN VARCHAR2
,p_gl_id_rev            IN VARCHAR2
,x_gl_clearing_acct_id OUT NUMBER
,x_error_msg           OUT VARCHAR2);

PROCEDURE get_gl_unbilled_acct_id(
 p_org_code             IN VARCHAR2
,p_gl_id_rev            IN VARCHAR2
,x_gl_unbilled_acct_id OUT NUMBER
,x_error_msg           OUT VARCHAR2);

PROCEDURE get_gl_unearned_acct_id(
 p_org_code             IN VARCHAR2
,p_gl_id_rev            IN VARCHAR2
,x_gl_unearned_acct_id OUT NUMBER
,x_error_msg           OUT VARCHAR2); 

PROCEDURE get_ship_via_code(
 p_ship_via          IN VARCHAR2
,x_ship_method_code OUT VARCHAR2
,x_error_msg        OUT VARCHAR2);

PROCEDURE get_salesrep_id(
 p_salesperson IN VARCHAR2
,x_sales_rep_id OUT NUMBER
,x_error_msg   OUT VARCHAR2);

PROCEDURE get_payment_term(
 p_sys_cust_ref         IN VARCHAR2
,p_sys_addr_ref         IN VARCHAR2
,x_payment_term_id OUT NUMBER
,x_error_msg       OUT VARCHAR2);

PROCEDURE get_credit_class(
 p_sys_cust_ref         IN VARCHAR2
,p_sys_addr_ref         IN VARCHAR2
,x_credit_class_code  OUT VARCHAR2
,x_error_msg          OUT VARCHAR2);

PROCEDURE get_country_code (
 p_country       IN VARCHAR2
,x_country_code OUT VARCHAR2
,x_error_msg    OUT VARCHAR2
);

PROCEDURE get_business_type(
 p_business_type  IN  VARCHAR2
,x_business_type OUT VARCHAR2
,x_error_msg	 OUT  VARCHAR2);

PROCEDURE validate_address_ref(
 p_orig_system_address_ref  IN  VARCHAR2
,x_error_msg	 OUT  VARCHAR2);

FUNCTION get_org_id(
 p_org_code       IN  VARCHAR2
,x_error_msg	 OUT  VARCHAR2)
RETURN NUMBER;

FUNCTION get_warehouse_id (
 p_warehouse       IN  VARCHAR2
,p_org_id         IN  VARCHAR2
,x_error_msg	 OUT  VARCHAR2
)
RETURN NUMBER;

END  XXABRL_AR_CUST_IMPORT_PKG;
/


CREATE OR REPLACE PACKAGE BODY APPS.xxmrl_pr_bkdt_load_pkg
IS
   PROCEDURE xxmrl_wait_for_req_prc1 (
      p_request_id   IN   NUMBER,
      p_req_name     IN   VARCHAR2
   )
   AS
      /*
      ========================
   =========================
   =========================
   =========================
   ====
      || Concurrent Program Name : MRL Requisition Import
      || Package Name                     : APPS.xxmrl_pr_bkdt_load_pkg
      || Description                            : Auto Submiting the Requisition Import Program For Each Operating Unit
      ===============================================================================================================================
      || Version              Date                                 Author                                                                                  Description                                                                          Remarks
      || ~~~~~~~     ~~~~~~~~~~~            ~~~~~~~~~~~~~~~~~~                                      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~                              ~~~~~~~~~~~~~~~~~
      ||  1.0.0             12-Aug-2019                  Lokesh Poojari                          Auto Submiting the Requisition Import Program For Each Operating Unit          New Development
      ================================================================================================================================
   =========================
   =========================
   =========================
   ====*/
      l_sup_phase           VARCHAR2 (100);
      l_sup_status          VARCHAR2 (100);
      l_sup_phase1          VARCHAR2 (100);
      l_sup_status1         VARCHAR2 (100);
      l_sup_errbuff         VARCHAR2 (500);
      l_sup_b_call_status   BOOLEAN;
   BEGIN
      IF p_request_id = 0
      THEN
         fnd_file.put_line
                        (fnd_file.LOG,
                         'Error in PR Validation  Program'
                        );
      ELSE
         COMMIT;
         l_sup_b_call_status :=
            fnd_concurrent.wait_for_request (request_id      => p_request_id,
                                             INTERVAL        => 2,
                                             max_wait        => 0,
                                             phase           => l_sup_phase,
                                             status          => l_sup_status,
                                             dev_phase       => l_sup_phase1,
                                             dev_status      => l_sup_status1,
                                             MESSAGE         => l_sup_errbuff
                                            );

         --
         IF l_sup_b_call_status
         THEN
            --
            IF (l_sup_phase1 = 'COMPLETE' AND l_sup_status1 = 'NORMAL')
            THEN
               fnd_file.put_line (fnd_file.LOG,
                                     p_req_name
                                  || TO_CHAR (p_request_id)
                                  || 'Completed Succesfully'
                                 );
            ELSE
               fnd_file.put_line (fnd_file.LOG,
                                     p_req_name
                                  || TO_CHAR (p_request_id)
                                  || ' Error'
                                 );
            END IF;
         --
         ELSE
            fnd_file.put_line (fnd_file.LOG,
                                  p_req_name
                               || TO_CHAR (p_request_id)
                               || ' Not Completed'
                              );
         END IF;
      END IF;
   END xxmrl_wait_for_req_prc1;
      
   PROCEDURE xxmrl_po_req_load_prc (
   errbuf    OUT   VARCHAR2,
   retcode   OUT   NUMBER
)
IS
   CURSOR cur_ap_data
   IS
      SELECT DISTINCT org_id
                 FROM po_requisitions_interface_all
                WHERE 1 = 1
                  AND header_attribute6 = 'Capex'
                  AND process_flag IS NULL;

   v_org_name   VARCHAR2 (30);
   --  v_ou_name    VARCHAR2 (100);
   v_req_id     NUMBER;
   v_user_id    NUMBER        := fnd_profile.VALUE ('USER_ID');
   v_resp_id    NUMBER        := fnd_profile.VALUE ('RESP_ID');
   v_appl_id    NUMBER        := fnd_profile.VALUE ('RESP_APPL_ID');
BEGIN
   FOR i IN cur_ap_data
   LOOP
      BEGIN
         SELECT NAME
           INTO v_org_name
           FROM apps.hr_operating_units
          WHERE organization_id = i.org_id;
      --    v_ou_name := v_org_name;
      EXCEPTION
         WHEN OTHERS
         THEN
            v_org_name := '';
      END;

--      BEGIN
--         mo_global.set_policy_context ('M', i.org_id);
--      END;

      -- we will submit this req through all ou
      fnd_global.apps_initialize (user_id           => v_user_id,
                                  resp_id           => v_resp_id,
                                  resp_appl_id      => v_appl_id
                                 );
      mo_global.init ('PO');
      mo_global.set_policy_context ('M', i.org_id);
FND_REQUEST.SET_ORG_ID(i.org_id);
      v_req_id :=
         fnd_request.submit_request (application      => 'PO',
                                     program          => 'REQIMPORT',
                                     description      => NULL,
                                     start_time       => NULL,
                                     sub_request      => FALSE,
                                     argument1        => NULL,
                                     argument2        => NULL,
                                     argument3        => 'POR',
                                     argument4        => NULL,
                                     argument5        => 'ALL',
                                     argument6        => NULL,
                                     argument7        => 'Y',
                                     argument8        => 'Y'
                                    );
      COMMIT;
      fnd_file.put_line (fnd_file.output,
                         ' Requisition Import Request id :' || v_req_id
                        );
                        xxmrl_wait_for_req_prc1 (v_req_id, 'Requisition Import');
   END LOOP;
END xxmrl_po_req_load_prc;
   
END  xxmrl_pr_bkdt_load_pkg; 
/


CREATE OR REPLACE PACKAGE BODY APPS.xxabrl_inst_resa_ar_conv_pkg
IS
 /*
  =========================================================================================================
  ||   Filename   : XXABRL_RESA_AR_CONV_PKG.sql
  ||   Description : Script is used to mold ReSA data for AR
  ||
  ||   Version     Date            Author              Modification
  ||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
  ||   1.0.0       22-JAN-2010    Shailesh Bharambe      New Development
  ||   1.0.1       30-JAN-2010  Shailesh Bharambe      Changes in the transaction type query change from IS_NET to IS_SALES
  ||   1.0.2       08-mar-2011   Ravi/Mitul             Added customer class code (Intercompany , Intracompany) and transaction type id
  ||   1.0.3        09-mar-2011   Ravi/Mitul             Removed the transaction type id for IS_TAX, IS_PROMO,IS_INTERTAX<IS_INTRATAX .
  ||   1.0.4        09-Apr-2011    Ravi                  Added  2 more customer recievble account 222512,222508 as per user request in package.
  ||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
  ========================================================================================================
  */
-- Date     Change Requirement/Done/Impact
--
--
--ABRL ReSA To AR Institutional Sales Data Load  :- XXABRL_IS_RESA_DATA_LOAD
--ABRL ReSA To AR Institutional Sales Conversion :- XXABRL_INST_RESA_AR_CONV
--ABRL Institutional Sales ReSA To AR Validate Program :- XXABRL_INST_RESA_AR_INV_PKG
--request set :- ABRL Institutional Sales  ReSA To AR
--
--
--========================================================================================================
   gn_conc_req_id   NUMBER := fnd_global.conc_request_id;
   -- To print the concurrent request id
   gn_user_id       NUMBER := fnd_global.user_id;

/*************************************************************************************************************/
/* Main Procedure calling */
/*************************************************************************************************************/
   PROCEDURE main_proc (
      x_err_buf      OUT      VARCHAR2,
      x_ret_code     OUT      NUMBER,
      p_action       IN       VARCHAR2,
      p_debug_flag   IN       VARCHAR2
   )
   IS
      ln_val_ins_retcode       NUMBER         := 0;
      --variable to return the status
      ln_val_retcode           NUMBER         := 0;
      ln_val_new_retcode       NUMBER         := 0;
      ln_val_ins_new_retcode   NUMBER         := 0;
      v_tot_cnt                NUMBER         := 0;
      v_tot_cnt1               NUMBER         := 0;
      v_tot_cnt2               NUMBER         := 0;
      v_succ_cnt               NUMBER         := 0;
      v_succ_cnt1              NUMBER         := 0;
      v_succ_cnt2              NUMBER         := 0;
      v_fail_cnt               NUMBER         := 0;
      v_fail_cnt1              NUMBER         := 0;
      v_fail_cnt2              NUMBER         := 0;
      v_user_name              VARCHAR2 (100) := NULL;
      v_ret_code               NUMBER;
   BEGIN
      validate_resa (p_debug_flag, v_ret_code);
      x_ret_code := v_ret_code;

      IF p_action = 'N'
      THEN
         insert_resa ('ReSA_IS', v_ret_code);
         x_ret_code := v_ret_code;
      END IF;
   END main_proc;

/*************************************************************************************************************/
/* Procedure for Printing Log */
/*************************************************************************************************************/
   PROCEDURE print_log (p_msg IN VARCHAR2, p_debug_flag VARCHAR2)
   AS
   BEGIN
      IF p_debug_flag = 'Y'
      THEN
         fnd_file.put_line (fnd_file.LOG, p_msg);
      END IF;
   END print_log;

   PROCEDURE validate_resa (p_debug_flag IN VARCHAR2, p_ret_code OUT NUMBER)
   AS
      CURSOR cur_val
      IS
         SELECT tt.ROWID, tt.STORE, total_id, rollup_level_1, rollup_level_2,
                rollup_level_3, tran_seq_no, tran_type, reason_code,
                account_number, batch_source_name, set_of_books_id,
                amount_dr, amount_cr, currency_code,
                currency_conversion_type, currency_conversion_date,
                accounting_date, code_combination_id, org_unit_id,
                cust_order_no, reference1, reference2, reference3,
                
--             NV_CUSTOMER_ID,
                interface_date, creation_date, created_by, interface_flag,
                error_message,
                              -- S_NUM,
                              org_code, cust_rec_acct, record_number,
                bill_to_use_id, ship_to_use_id
           FROM xxabrl_inst_resa_ar_int tt
          WHERE interface_flag IN ('N', 'E');

      v_error_str                     VARCHAR2 (999);
      x_error_msg                     VARCHAR2 (240);
      v_valid_cnt                     NUMBER                             := 0;
      v_total_cnt                     NUMBER                             := 0;
      v_error_cnt                     NUMBER                             := 0;
      --  v_CUST_ACCOUNT_ID number;
      v_store_rece                    NUMBER;
      v_organization_code             VARCHAR2 (30);
      v_cust_site_id                  NUMBER;
      v_orig_system_customer_id       NUMBER;
      v_orig_system_ship_address_id   NUMBER;
      v_orig_system_bill_address_id   NUMBER;
      bala_org_id                     apps.hr_operating_units.organization_id%TYPE;
      bala_old_segments               apps.gl_code_combinations_kfv.concatenated_segments%TYPE;
      bala_new_segments               apps.gl_code_combinations_kfv.concatenated_segments%TYPE;
      bala_code_combination_id        apps.gl_code_combinations_kfv.code_combination_id%TYPE;
      bala_segment3                   VARCHAR2 (3);
      bala_location                   apps.hz_cust_site_uses_all.LOCATION%TYPE;
      bala_ship_to_use_id             apps.hz_cust_site_uses_all.site_use_id%TYPE;
      bala_bill_to_use_id             apps.hz_cust_site_uses_all.site_use_id%TYPE;
   BEGIN
      fnd_file.put_line (fnd_file.LOG, '### ReSA Validation Starts ###');

-- starting loop
      FOR v_cur_val IN cur_val
      LOOP
         v_error_str := NULL;
         fnd_file.put_line (fnd_file.LOG,
                               'Validating SEQ_NO#RECORD_NUMBER ('
                            || v_cur_val.record_number
                            || ')  --------------------'
                           );
--    CCID Validation
         bala_code_combination_id := v_cur_val.code_combination_id;

         IF v_cur_val.code_combination_id IS NOT NULL
         THEN
            IF    v_cur_val.org_unit_id = '901'
               OR v_cur_val.org_unit_id = '861'
               OR v_cur_val.org_unit_id = '841'
               OR v_cur_val.org_unit_id = '961'
               OR v_cur_val.org_unit_id = '801'
               OR v_cur_val.org_unit_id = '941'
               OR v_cur_val.org_unit_id = '821'
            THEN
               bala_code_combination_id := 0;

               BEGIN
                  SELECT concatenated_segments
                    INTO bala_old_segments
                    FROM apps.gl_code_combinations_kfv
                   WHERE 1 = 1
                     AND code_combination_id = v_cur_val.code_combination_id
                     AND enabled_flag = 'Y';
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     v_error_str :=
                           'CCID not defined in Oracle For Old Org '
                        || v_cur_val.org_unit_id
                        || '  -  '
                        || v_cur_val.code_combination_id;
                  WHEN TOO_MANY_ROWS
                  THEN
                     v_error_str :=
                           'Multiple CCID defined in Oracle For Old Org '
                        || v_cur_val.org_unit_id
                        || '  -  '
                        || v_cur_val.code_combination_id;
                  WHEN OTHERS
                  THEN
                     v_error_str :=
                           'Exception Others in Oracle For Old Org '
                        || v_cur_val.org_unit_id
                        || '  -  '
                        || v_cur_val.code_combination_id;
               END;

               SELECT DECODE (SUBSTR (bala_old_segments, 8, 3),
                              '812', '635',
                              '772', '620',
                              '773', '630',
                              '792', '640',
                              '752', '610',
                              '754', '615',
                              '775', '625',
                              '751', '645'
                             )
                 INTO bala_segment3
                 FROM DUAL;

               bala_new_segments :=
                     '11.'
                  || SUBSTR (bala_old_segments, 4, 4)
                  || bala_segment3
                  || SUBSTR (bala_old_segments, 11, 27);

               BEGIN
/*        IT'S TAKING TOO MUCH TIME

                SELECT CODE_COMBINATION_ID INTO BALA_CODE_COMBINATION_ID
                FROM APPS.GL_CODE_COMBINATIONS_KFV
                WHERE 1=1
                AND    CONCATENATED_SEGMENTS = BALA_NEW_SEGMENTS;

IT'S TAKING TOO MUCH TIME        */

                  -- 31.000.751.0000000.00.362002.000.0000
                  SELECT code_combination_id
                    INTO bala_code_combination_id
                    FROM apps.gl_code_combinations_kfv
                   WHERE 1 = 1
                     AND segment1 = SUBSTR (bala_new_segments, 1, 2)
                     AND segment2 = SUBSTR (bala_new_segments, 4, 3)
                     AND segment3 = SUBSTR (bala_new_segments, 8, 3)
                     AND segment4 = SUBSTR (bala_new_segments, 12, 7)
                     AND segment5 = SUBSTR (bala_new_segments, 20, 2)
                     AND segment6 = SUBSTR (bala_new_segments, 23, 6)
                     AND segment7 = SUBSTR (bala_new_segments, 30, 3)
                     AND segment8 = SUBSTR (bala_new_segments, 34, 4)
                     AND enabled_flag = 'Y';
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     v_error_str :=
                           v_error_str
                        || ' CCID not defined in Oracle For NEW Org '
                        || bala_new_segments;
                  WHEN TOO_MANY_ROWS
                  THEN
                     v_error_str :=
                           v_error_str
                        || ' Multiple CCID defined in Oracle For NEW Org '
                        || bala_new_segments;
                  WHEN OTHERS
                  THEN
                     v_error_str :=
                           v_error_str
                        || ' Exception Others in Oracle For NEW Org '
                        || bala_new_segments;
               END;

               BEGIN
                  SELECT segment6
                    INTO v_store_rece
                    FROM gl_code_combinations_kfv
                   WHERE 1 = 1
                     AND code_combination_id = bala_code_combination_id
                     AND enabled_flag = 'Y';
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     v_error_str :=
                           'Store Receivable CCID/GL not Found for '
                        || v_cur_val.code_combination_id
                        || '  '
                        || v_cur_val.account_number
                        || ' RECORD_NUMBER '
                        || v_cur_val.record_number;
                  WHEN TOO_MANY_ROWS
                  THEN
                     v_error_str :=
                           'Multiple Store Receivable CCID/GL Found for '
                        || v_cur_val.code_combination_id
                        || '  '
                        || v_cur_val.account_number
                        || ' RECORD_NUMBER '
                        || v_cur_val.record_number;
                  WHEN OTHERS
                  THEN
                     v_error_str :=
                           'Exception while deriving  Receivable CCID/GL for '
                        || v_cur_val.code_combination_id
                        || '  '
                        || v_cur_val.account_number
                        || ' RECORD_NUMBER '
                        || v_cur_val.record_number;
               END;
            ELSE                       --        OTHER THAN LE Merger ORG_ID's
               validate_ccid (v_cur_val.code_combination_id,
                              v_cur_val.ROWID,
                              x_error_msg
                             );

               IF x_error_msg IS NOT NULL
               THEN
                  v_error_str := v_error_str || ';' || x_error_msg;
               END IF;

               BEGIN
                  SELECT segment6
                    INTO v_store_rece
                    FROM gl_code_combinations_kfv
                   WHERE 1 = 1
                     AND code_combination_id = v_cur_val.code_combination_id
                     AND enabled_flag = 'Y';
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     v_error_str :=
                           'Store Receivable CCID/GL not Found for '
                        || v_cur_val.code_combination_id
                        || '  '
                        || v_cur_val.account_number
                        || ' RECORD_NUMBER '
                        || v_cur_val.record_number;
                  WHEN TOO_MANY_ROWS
                  THEN
                     v_error_str :=
                           'Multiple Store Receivable CCID/GL Found for '
                        || v_cur_val.code_combination_id
                        || '  '
                        || v_cur_val.account_number
                        || ' RECORD_NUMBER '
                        || v_cur_val.record_number;
                  WHEN OTHERS
                  THEN
                     v_error_str :=
                           'Exception while deriving  Receivable CCID/GL for '
                        || v_cur_val.code_combination_id
                        || '  '
                        || v_cur_val.account_number
                        || ' RECORD_NUMBER '
                        || v_cur_val.record_number;
               END;
            END IF;
         ELSE
            v_error_str := v_error_str || ';' || 'CCID IS NULL';
         END IF;

--     Store / Customer Validation
         IF v_cur_val.account_number IS NOT NULL
         THEN
      --  PRINT_LOG('START OF CUSTOMER VALIDATION: ' , P_DEBUG_FLAG);
      --  VALIDATE_CUSTOMER(V_CUR_VAL.STORE, V_CUR_VAL.ROWID,V_CUR_VAL.ORG_ID, X_ERROR_MSG);
      -- FOLLOWING CODE  REPLACED BY NEW CODE TO FIND OUT THE BILLTO ID AND SHIP TO ID
/*
       Begin
       -- following query gets the cust account id for the customer no
             SELECT hca.cust_account_id,hcac.cust_acct_site_id                 --,hcac.org_id,hca.account_number ,
                                                                -- INTO   v_CUST_ACCOUNT_ID,v_cust_site_id
             FROM   hz_cust_accounts hca
                   ,hz_cust_acct_sites_all hcac
                   ,hz_cust_site_uses_all  uses
             WHERE  hca.account_number = v_cur_val.account_number
             and    CUSTOMER_CLASS_CODE IN ('SCRAP','INSTITUTIONAL')
             and    hca.cust_account_id=hcac.cust_account_id
             AND    hcac.status='A'
             AND    hcac.org_id=v_cur_val.org_unit_id
             AND    hcac.cust_acct_site_id = uses.cust_acct_site_id
             AND    uses.site_use_id =  v_cur_val.NV_CUSTOMER_SHIP_TO_ID;
       EXCEPTION
            WHEN NO_DATA_FOUND THEN
              v_error_str := 'Customer or customer site not found '  ||
                            v_cur_val.account_number|| ' RECORD_NUMBER '|| v_cur_val.RECORD_NUMBER;
            WHEN TOO_MANY_ROWS THEN
              v_error_str := 'Multiple Customer site use Receivable  Found A' ||
                             v_cur_val.account_number|| ' RECORD_NUMBER '|| v_cur_val.RECORD_NUMBER;
            WHEN OTHERS THEN
              v_error_str := 'Exception while getting the store account  ' ||
                             v_cur_val.account_number || ' RECORD_NUMBER '|| v_cur_val.RECORD_NUMBER;
       END;
*/

            --    08.01.2015    LE Merger MODIFICATION
            bala_org_id := v_cur_val.org_unit_id;

            IF    v_cur_val.org_unit_id = 901
               OR v_cur_val.org_unit_id = 861
               OR v_cur_val.org_unit_id = 841
               OR v_cur_val.org_unit_id = 961
               OR v_cur_val.org_unit_id = 801
               OR v_cur_val.org_unit_id = 941
               OR v_cur_val.org_unit_id = 821
            THEN
               bala_org_id := 0;

               SELECT DECODE (v_cur_val.org_unit_id,
                              901, 1524,
                              861, 1521,
                              841, 1523,
                              961, 1525,
                              801, 1481,
                              941, 1522,
                              821, 1721
                             )
                 INTO bala_org_id
                 FROM DUAL;

               IF     v_cur_val.org_unit_id = 801
                  AND SUBSTR (bala_new_segments, 8, 3) = '615'
               THEN
                  bala_org_id := 1501;
               END IF;
            END IF;

-- THIS WILL DETERMIN THE SHIP TO CUSTOMER ID FOR INTERFACE
            BEGIN
/*        08.01.2015            ORIGINAL BEFORE LEGAL ENTITY MERGER

           SELECT HCA.CUST_ACCOUNT_ID,HCAC.CUST_ACCT_SITE_ID
             INTO   V_ORIG_SYSTEM_CUSTOMER_ID,V_ORIG_SYSTEM_SHIP_ADDRESS_ID
             FROM   HZ_CUST_ACCOUNTS HCA
                   ,HZ_CUST_ACCT_SITES_ALL HCAC
                   ,HZ_CUST_SITE_USES_ALL  USES
            WHERE  CUSTOMER_CLASS_CODE IN ('SCRAP', 'INSTITUTIONAL', 'INTRACOMPANY', 'INTERCOMPANY')
              AND    HCA.CUST_ACCOUNT_ID=HCAC.CUST_ACCOUNT_ID
              AND    HCAC.STATUS='A'
          AND    HCAC.ORG_ID=V_CUR_VAL.ORG_UNIT_ID
              AND    HCAC.CUST_ACCT_SITE_ID = USES.CUST_ACCT_SITE_ID
              AND    USES.SITE_USE_ID =  V_CUR_VAL.SHIP_TO_USE_ID
              AND    HCA.ACCOUNT_NUMBER = V_CUR_VAL.ACCOUNT_NUMBER;

08.01.2015        ORIGINAL BEFORE LEGAL ENTITY MERGER
*/
               SELECT distinct hca.cust_account_id,
                      hcac.cust_acct_site_id, uses.site_use_id,
                      uses.LOCATION
                 INTO v_orig_system_customer_id,
                      v_orig_system_ship_address_id, bala_ship_to_use_id,
                      bala_location
                 FROM hz_cust_accounts hca,
                      hz_cust_acct_sites_all hcac,
                      hz_cust_site_uses_all uses
                WHERE 1 = 1
                  AND customer_class_code IN
                         ('SCRAP', 'INSTITUTIONAL', 'INTRACOMPANY',
                          'INTERCOMPANY', 'ABG ONE')
                  AND hca.cust_account_id = hcac.cust_account_id
                  AND hcac.status = 'A'
                  and UPPER (uses.site_use_code) = 'SHIP_TO'
                  AND hcac.org_id = bala_org_id
                  AND hca.account_number = v_cur_val.account_number
                  AND hcac.cust_acct_site_id = uses.cust_acct_site_id
                  AND uses.site_use_id =
                                                      v_cur_val.ship_to_use_id
                  AND uses.site_use_id IN (
                         SELECT bala.site_use_id
                           FROM apps.hz_cust_site_uses_all bala
                          WHERE 1 = 1
                            AND org_id = bala_org_id
                            AND UPPER (LOCATION) =
                                   UPPER
                                      ((SELECT uses.LOCATION
                                          FROM apps.hz_cust_accounts hca,
                                               apps.hz_cust_acct_sites_all hcac,
                                               apps.hz_cust_site_uses_all uses
                                         WHERE customer_class_code IN
                                                  ('SCRAP', 'INSTITUTIONAL',
                                                   'INTRACOMPANY',
                                                   'INTERCOMPANY', 'ABG ONE')
                                           AND hca.cust_account_id =
                                                          hcac.cust_account_id
                                           AND hcac.status = 'A'
                                           AND hcac.org_id =
                                                         v_cur_val.org_unit_id
                                           AND hcac.cust_acct_site_id =
                                                        uses.cust_acct_site_id
                                           AND uses.site_use_id =
                                                      v_cur_val.ship_to_use_id
                                           AND hca.account_number =
                                                      v_cur_val.account_number)
                                      )
                            AND UPPER (site_use_code) = 'SHIP_TO'
                            AND status = 'A');
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  v_error_str :=
                        'SHIP TO CUSTOMER SITE NOT FOUND '
                     || bala_org_id
                     || '  -  '
                     || v_cur_val.account_number
                     || ' RECORD_NUMBER '
                     || v_cur_val.record_number;
               WHEN TOO_MANY_ROWS
               THEN
                  v_error_str :=
                        'MULTIPLE SHIP TO CUSTOMER SITE USE FOUND '
                     || bala_org_id
                     || '  -  '
                     || v_cur_val.account_number
                     || ' RECORD_NUMBER '
                     || v_cur_val.record_number;
               WHEN OTHERS
               THEN
                  v_error_str :=
                        'EXCEPTION WHILE GETTING Ship to Customer Site   '
                     || bala_org_id
                     || '  -  '
                     || v_cur_val.account_number
                     || ' RECORD_NUMBER '
                     || v_cur_val.record_number;
            END;

-- THIS WILL DETERMIN THE BILL  TO CUSTOMER ID FOR INTERFACE
            BEGIN
/*        08.01.2015            ORIGINAL BEFORE LEGAL ENTITY MERGER

             SELECT  hcac.cust_acct_site_id
               INTO V_ORIG_SYSTEM_BILL_ADDRESS_ID
               FROM  hz_cust_accounts hca
                    ,hz_cust_acct_sites_all hcac
                    ,hz_cust_site_uses_all  uses
              WHERE  CUSTOMER_CLASS_CODE IN ('SCRAP', 'INSTITUTIONAL', 'INTRACOMPANY', 'INTERCOMPANY')
                and  hca.cust_account_id=hcac.cust_account_id
                AND  hcac.status='A'
        AND  hcac.org_id=v_cur_val.org_unit_id
                AND  hcac.cust_acct_site_id = uses.cust_acct_site_id
                AND  uses.site_use_id =  v_cur_val.BILL_TO_USE_ID
                and  hca.account_number = v_cur_val.ACCOUNT_NUMBER;

08.01.2015        ORIGINAL BEFORE LEGAL ENTITY MERGER    */
               SELECT distinct hca.cust_account_id,
                      hcac.cust_acct_site_id, uses.site_use_id,
                      uses.LOCATION
                 INTO v_orig_system_customer_id,
                      v_orig_system_bill_address_id, bala_bill_to_use_id,
                      bala_location
                 FROM hz_cust_accounts hca,
                      hz_cust_acct_sites_all hcac,
                      hz_cust_site_uses_all uses
                WHERE 1 = 1
                  AND customer_class_code IN
                         ('SCRAP', 'INSTITUTIONAL', 'INTRACOMPANY',
                          'INTERCOMPANY', 'ABG ONE')
                  AND hca.cust_account_id = hcac.cust_account_id
                  AND hcac.status = 'A'
--                  and UPPER (uses.site_use_code) = 'BILL_TO'
                  AND hcac.org_id = bala_org_id
                  AND hca.account_number = v_cur_val.account_number
--                  and uses.site_use_id=v_cur_val.bill_to_use_id
                  AND hcac.cust_acct_site_id = uses.cust_acct_site_id
                  AND uses.site_use_id =
                                                      v_cur_val.bill_to_use_id
                  AND uses.site_use_id IN (
                         SELECT bala.site_use_id
                           FROM apps.hz_cust_site_uses_all bala
                          WHERE 1 = 1
                            AND org_id = bala_org_id
                            AND UPPER (LOCATION) =
                                   UPPER
                                      ((SELECT uses.LOCATION
                                          FROM apps.hz_cust_accounts hca,
                                               apps.hz_cust_acct_sites_all hcac,
                                               apps.hz_cust_site_uses_all uses
                                         WHERE customer_class_code IN
                                                  ('SCRAP', 'INSTITUTIONAL',
                                                   'INTRACOMPANY',
                                                   'INTERCOMPANY', 'ABG ONE')
                                           AND hca.cust_account_id =
                                                          hcac.cust_account_id
                                           AND hcac.status = 'A'
                                           AND hcac.org_id =
                                                         v_cur_val.org_unit_id
                                           AND hcac.cust_acct_site_id =
                                                        uses.cust_acct_site_id
                                           AND uses.site_use_id =
                                                      v_cur_val.bill_to_use_id
                                           AND hca.account_number =
                                                      v_cur_val.account_number)
                                      )
                            AND UPPER (site_use_code) = 'BILL_TO'
                            AND status = 'A');
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  v_error_str :=
                        'Bill To customer Site not found  '
                     || bala_org_id
                     || '  -  '
                     || v_cur_val.account_number
                     || ' RECORD_NUMBER '
                     || v_cur_val.record_number;
               WHEN TOO_MANY_ROWS
               THEN
                  v_error_str :=
                        'Multiple Bill To customer site use Found '
                     || bala_org_id
                     || '  -  '
                     || v_cur_val.account_number
                     || ' RECORD_NUMBER '
                     || v_cur_val.record_number;
               WHEN OTHERS
               THEN
                  v_error_str :=
                        'Exception Others while getting Bill To Customer Site  '
                     || bala_org_id
                     || '  -  '
                     || v_cur_val.account_number
                     || ' RECORD_NUMBER '
                     || v_cur_val.record_number;
            END;

            --    Find ORG CODE
            BEGIN
               SELECT short_code
                 INTO v_organization_code
                 FROM hr_operating_units
                WHERE 1 = 1 AND organization_id = bala_org_id;
                                                              --    08.01.2015
--            ORGANIZATION_ID = V_CUR_VAL.ORG_UNIT_ID;           --    OLD ORG_ID VALUE FROM TABLE= APPS.XXABRL_INST_RESA_AR_INT
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  v_error_str :=
                        'Store Org_ID not defined in Oracle '
                     || bala_org_id
                     || '  '
                     || v_cur_val.account_number
                     || ' RECORD_NUMBER '
                     || v_cur_val.record_number;
               WHEN TOO_MANY_ROWS
               THEN
                  v_error_str :=
                        'Multiple Store Org_ID defined for '
                     || bala_org_id
                     || '  '
                     || v_cur_val.account_number
                     || ' RECORD_NUMBER '
                     || v_cur_val.record_number;
               WHEN OTHERS
               THEN
                  v_error_str :=
                        'Exception while deriving Store Org_ID for '
                     || bala_org_id
                     || '  '
                     || v_cur_val.account_number
                     || ' RECORD_NUMBER '
                     || v_cur_val.record_number;
            END;

            --    Updating Derived Customer/Store Data
            IF v_error_str IS NULL
            THEN
               BEGIN
                  UPDATE xxabrl_inst_resa_ar_int int_ln
                     SET cust_rec_acct = v_store_rece,
                         org_code = v_organization_code,
                         org_unit_id = bala_org_id,
                         code_combination_id = bala_code_combination_id,
                         orig_system_customer_id = v_orig_system_customer_id,
                         ship_to_use_id = bala_ship_to_use_id,
                         bill_to_use_id = bala_bill_to_use_id,
                         orig_system_bill_address_id =
                                                 v_orig_system_bill_address_id,
                         orig_system_ship_address_id =
                                                 v_orig_system_ship_address_id
-- NV_CUSTOMER_ID = V_CUST_ACCOUNT_ID,
-- NV_CUSTOMER_SITE_ID = V_CUST_SITE_ID
                  WHERE  ROWID = v_cur_val.ROWID;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     v_error_str :=
                           'Exception while derived Store details for '
                        || v_cur_val.account_number
                        || ' RECORD_NUMBER '
                        || v_cur_val.record_number;
               END;
            ELSE
               print_log ('Error :- ' || v_error_str, p_debug_flag);
            END IF;

-- Print_log('eND OF cUSTOMER vALIDATION: ' , p_debug_flag);
            IF x_error_msg IS NOT NULL
            THEN
               v_error_str := v_error_str || ';' || x_error_msg;
            END IF;
         ELSE
            v_error_str := v_error_str || ';' || 'STORE_CODE is NULL';
         END IF;

         --=== Curr Code Validation
         IF v_cur_val.currency_code IS NOT NULL
         THEN
            validate_currency_code (v_cur_val.currency_code, x_error_msg);

            IF x_error_msg IS NOT NULL
            THEN
               v_error_str := v_error_str || ';' || x_error_msg;
            END IF;
         ELSE
            v_error_str := v_error_str || ';' || 'CURR_CODE is NULL';
         END IF;

         --=== Curr Conv Code Validation
         IF v_cur_val.currency_conversion_type IS NOT NULL
         THEN
            validate_curr_conv_code (v_cur_val.currency_conversion_type,
                                     x_error_msg
                                    );

            IF x_error_msg IS NOT NULL
            THEN
               v_error_str := v_error_str || ';' || x_error_msg;
            END IF;
         ELSE
            v_error_str := v_error_str || ';' || 'CURR_CONV_CODE is NULL';
         END IF;

         --=== TRX/GL Date Validation
         IF v_cur_val.accounting_date IS NULL
         THEN
            v_error_str := v_error_str || ';' || 'TRX/GL Date is NULL';
         END IF;

         --=====
         IF v_error_str IS NOT NULL
         THEN
            UPDATE xxabrl_inst_resa_ar_int int_ln
               SET interface_flag = 'E',
                   error_message = v_error_str
             WHERE ROWID = v_cur_val.ROWID;

            print_log ('***Record Error: ' || v_error_str, p_debug_flag);
            v_error_cnt := v_error_cnt + 1;
         ELSE
            UPDATE xxabrl_inst_resa_ar_int int_ln
               SET interface_flag = 'V',
                   error_message = v_error_str
             WHERE ROWID = v_cur_val.ROWID;

            print_log ('Record Valid: ' || v_error_str, p_debug_flag);
            v_valid_cnt := v_valid_cnt + 1;
         END IF;

         v_total_cnt := v_total_cnt + 1;
         COMMIT;
      END LOOP;

      -- ending loop
      fnd_file.put_line
                   (fnd_file.LOG,
                    '========================================================'
                   );
      fnd_file.put_line (fnd_file.LOG, 'Total Records: ' || v_total_cnt);
      fnd_file.put_line (fnd_file.LOG, 'Total Valid: ' || v_valid_cnt);
      fnd_file.put_line (fnd_file.LOG, 'Total Errors: ' || v_error_cnt);
      fnd_file.put_line
                   (fnd_file.LOG,
                    '========================================================'
                   );

      IF v_error_cnt > 0
      THEN
         p_ret_code := 1;
      END IF;

      fnd_file.put_line (fnd_file.LOG, '### ReSA Validation Ends ###');
   END validate_resa;

   /*
   -- Validation for Customer /Store
   PROCEDURE Validate_customer(p_cust_no     IN VARCHAR2,
                               p_rowid     VARCHAR2,
                               p_org_id   IN VARCHAR2,
                               p_customer_site_id number,
                               p_ccid     IN NUMBER,
                               p_error_msg OUT VARCHAR2) AS
      begin
      null;
      END Validate_customer;
   */
   PROCEDURE validate_currency_code (
      p_curr_code   IN       VARCHAR2,
      p_error_msg   OUT      VARCHAR2
   )
   AS
      v_fun_curr   VARCHAR2 (240);
   BEGIN
      SELECT currency_code
        INTO v_fun_curr
        FROM gl_sets_of_books
       WHERE set_of_books_id = v_set_of_bks_id;

      p_error_msg := NULL;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         p_error_msg := 'Curr_Code not defined in Oracle ' || p_curr_code;
      WHEN TOO_MANY_ROWS
      THEN
         p_error_msg := 'Multiple Curr_Code defined for ' || p_curr_code;
      WHEN OTHERS
      THEN
         p_error_msg :=
                     'Exception while deriving Curr_Code for ' || p_curr_code;
   END validate_currency_code;

   PROCEDURE validate_curr_conv_code (
      p_curr_conv_code   IN       VARCHAR2,
      p_error_msg        OUT      VARCHAR2
   )
   AS
      v_curr_conv_code   VARCHAR2 (240);
   BEGIN
      SELECT conversion_type
        INTO v_curr_conv_code
        FROM gl_daily_conversion_types
       WHERE UPPER (conversion_type) = UPPER (p_curr_conv_code);

      p_error_msg := NULL;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         p_error_msg :=
                  'Curr_Conv_Code not defined in Oracle ' || p_curr_conv_code;
      WHEN TOO_MANY_ROWS
      THEN
         p_error_msg :=
                   'Multiple Curr_Conv_Code defined for ' || p_curr_conv_code;
      WHEN OTHERS
      THEN
         p_error_msg :=
            'Exception while deriving Curr_Conv_Code for '
            || p_curr_conv_code;
   END validate_curr_conv_code;

   PROCEDURE validate_ccid (
      p_ccid        IN       VARCHAR2,
      p_rowid                VARCHAR2,
      p_error_msg   OUT      VARCHAR2
   )
   AS
      v_ccid   VARCHAR2 (240);
   BEGIN
      SELECT code_combination_id
        INTO v_ccid
        FROM gl_code_combinations_kfv
       WHERE code_combination_id = p_ccid AND enabled_flag = 'Y'
                                                                --and char_of_account_id =50328
      ;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         p_error_msg := 'CCID not defined in Oracle ' || p_ccid;
      WHEN TOO_MANY_ROWS
      THEN
         p_error_msg := 'Multiple CCID defined for ' || p_ccid;
      WHEN OTHERS
      THEN
         p_error_msg := 'Exception while deriving CCID for ' || p_ccid;
   END validate_ccid;

--== Derive_Tran_Type
   PROCEDURE derive_tran_type (
      p_resa_cur      IN       xxabrl_inst_resa_ar_int%ROWTYPE,
      p_nature        OUT      VARCHAR2,
      p_tran_type     OUT      VARCHAR2,
      p_description   OUT      VARCHAR2,
      p_error_msg     OUT      VARCHAR2
   )
   AS
      v_roll_up2      VARCHAR2 (240);
      v_nature        VARCHAR2 (240);
      v_tran_type     VARCHAR2 (240);
      v_description   VARCHAR2 (240);
   BEGIN
      print_log ('Derive Tran-Type for ID_TYPE: ' || p_resa_cur.total_id,
                 'N');
      print_log ('Derive Tran-Type for Roll_Up1: '
                 || p_resa_cur.rollup_level_1,
                 'N'
                );

      -- following query will get the ofin transaction type by using the lookup values in ofin
      --and the stage data of rollup levels and resa transaction type and reason code
      BEGIN
         --IF p_Resa_cur.TOTAL_ID in ('IS_SALES','IS_SALES_1','IS_SALES1','IS_SALES2','IS_SALES_2') THEN
         IF p_resa_cur.total_id IN
               ('IS_SALES', 'IS_INTRA', 'IS_INTER', 'ISINTRAVAT',
                'ISINTERCST', 'IS_TAX_VAT', 'ISINTRACST', 'ISINTERVAT',
                'IS_TAX_CST')
         THEN
            ---query only for  ID_TYPE = IS_SALES
            SELECT attribute2, attribute4,                         -- x_Nature
                                          attribute5,           -- x_Tran_Type
                   attribute8                                 -- x_Description
              INTO v_roll_up2, v_nature, v_tran_type,
                   v_description
              FROM fnd_lookup_values_vl
             WHERE SYSDATE BETWEEN NVL (start_date_active, SYSDATE)
                               AND NVL (end_date_active, SYSDATE)
               AND enabled_flag = 'Y'
               AND lookup_type = 'RESA_AR_IS_TOTAL_ID_LKP'
               AND attribute_category = 'RESA_AR_IS_TOTAL_ID_LKP'
               AND UPPER (description) = UPPER (p_resa_cur.total_id)
               --TOTAL_ID
               AND attribute1 = p_resa_cur.rollup_level_1    -- rollup level 1
               AND UPPER (attribute6) = UPPER (p_resa_cur.tran_type)
               -- resa tran_type
               AND UPPER (attribute7) =
                      DECODE (UPPER (attribute6),
                              'SALE', 'ALL',
                              UPPER (p_resa_cur.reason_code)
                             );                            -- resa reason code
         --AND UPPER(ATTRIBUTE7)  = UPPER(p_Resa_cur.REASON_CODE);  -- resa reason code
         ELSE
            SELECT attribute2, attribute4,                         -- x_Nature
                                          attribute5,           -- x_Tran_Type
                   attribute8                                 -- x_Description
              INTO v_roll_up2, v_nature, v_tran_type,
                   v_description
              FROM fnd_lookup_values_vl
             WHERE SYSDATE BETWEEN NVL (start_date_active, SYSDATE)
                               AND NVL (end_date_active, SYSDATE)
               AND enabled_flag = 'Y'
               AND lookup_type = 'RESA_AR_IS_TOTAL_ID_LKP'
               AND attribute_category = 'RESA_AR_IS_TOTAL_ID_LKP'
               AND UPPER (description) = UPPER (p_resa_cur.total_id)
               --TOTAL_ID
               AND attribute1 = p_resa_cur.rollup_level_1    -- rollup level 1
               AND attribute2 =
                      TO_CHAR
                         (p_resa_cur.rollup_level_2)
                                                --added by govind on 21-aug-17
               -- rollup level 1--decode condition added on 14-jun-2012 by Vikash Kumar
               AND UPPER (attribute6) = UPPER (p_resa_cur.tran_type)
               -- resa tran_type
               AND UPPER (attribute7) =
                      DECODE (UPPER (attribute6),
                              'SALE', 'ALL',
                              UPPER (p_resa_cur.reason_code)
                             );                            -- resa reason code
         --AND UPPER(ATTRIBUTE7)  = decode(UPPER(ATTRIBUTE6),'',UPPER(p_Resa_cur.REASON_CODE);  -- resa reason code
         END IF;

           /*
           BEGIN
             SELECT
             --MEANING,
             --DESCRIPTION,
             --ATTRIBUTE1, --<<in where clause
              ATTRIBUTE2, ATTRIBUTE3, ATTRIBUTE4, ATTRIBUTE5
               INTO v_Roll_Up2, v_Nature, v_Tran_Type, v_Description
               FROM FND_LOOKUP_VALUES_VL
              WHERE SYSDATE BETWEEN NVL(START_DATE_ACTIVE, SYSDATE) AND
                    NVL(END_DATE_ACTIVE, SYSDATE)
                AND ENABLED_FLAG = 'Y'
                AND LOOKUP_TYPE = 'RESA_AR_TOTAL_ID_LKP'
                AND ATTRIBUTE_CATEGORY = 'RESA_AR_TOTAL_ID_LKP'
                AND DESCRIPTION = p_Resa_cur.TOTAL_ID
                AND DECODE(ATTRIBUTE1,'All',p_Resa_cur.ROLLUP_LEVEL_1,ATTRIBUTE1) = p_Resa_cur.ROLLUP_LEVEL_1
                AND DECODE(ATTRIBUTE2,'All',p_Resa_cur.ROLLUP_LEVEL_2,ATTRIBUTE2) = p_Resa_cur.ROLLUP_LEVEL_2
         --       AND ATTRIBUTE1 = decode(v_ATTRIBUTE1,'All','All',p_Resa_cur.ROLLUP_LEVEL_1)
         --       AND ATTRIBUTE2 = decode(v_ATTRIBUTE2,'All','All',p_Resa_cur.ROLLUP_LEVEL_2)
         --       AND ATTRIBUTE2 = decode(DESCRIPTION,'TENDER_TOT','All','ROUNDAMT','All','NETSALES','All',p_Resa_cur.ROLLUP_LEVEL_2)
                ;
               */
         print_log ('Derived Nature: ' || v_nature, 'N');
         print_log ('Derived Tran-Type: ' || v_tran_type, 'N');
         print_log ('Derived Desc: ' || v_description, 'N');
         p_nature := v_nature;
         p_tran_type := v_tran_type;
         p_description := v_description;
         RETURN;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            p_error_msg :=
                  'Tran-Type Lookup not defined for '
               || p_resa_cur.total_id
               || '/'
               || p_resa_cur.rollup_level_1;
         WHEN TOO_MANY_ROWS
         THEN
            p_error_msg :=
                  'Multiple Tran-Type Lookup defined for '
               || p_resa_cur.total_id
               || '/'
               || p_resa_cur.rollup_level_1;
         WHEN OTHERS
         THEN
            p_error_msg :=
                  'Exception while deriving Tran-Type Lookup for '
               || p_resa_cur.total_id
               || '/'
               || p_resa_cur.rollup_level_1;
      END;

      print_log (p_error_msg, 'Y');
   END derive_tran_type;

/*
  PROCEDURE Derive_Bank_Dtl(p_Store               IN XXABRL_INST_RESA_AR_INT%ROWTYPE,
                            p_Tran_Type           IN VARCHAR2,
                            x_Bank_Account_Name   OUT VARCHAR2,
                            x_Bank_Account_Number OUT VARCHAR2,
                            x_description         OUT VARCHAR2,
                            x_error_msg           OUT VARCHAR2) AS
    v_Bank_Account_Name   VARCHAR2(240);
    v_Bank_Account_Number VARCHAR2(240);
    v_description         VARCHAR2(240);
  BEGIN
    IF p_Tran_Type IS NOT NULL THEN
    Print_log('Find Bank Details for :'||p_Tran_Type,'Y');
    --Print_log('afetr bank details','Y');
      BEGIN
           SELECT description INTO v_description
                  FROM FND_FLEX_VALUES_VL
                  WHERE UPPER(FLEX_VALUE_MEANING) = UPPER(p_Store.ROLLUP_LEVEL_1)
                  AND flex_value_set_id =
                  (SELECT flex_value_set_id
                  FROM fnd_flex_value_sets
                  WHERE UPPER(flex_value_set_name) = UPPER('RESA Roll up 1'));
                  x_description := v_description;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          x_error_msg := 'Description not Found for Rol_up1' ||
                         p_Store.ROLLUP_LEVEL_1;
        WHEN TOO_MANY_ROWS THEN
          x_error_msg := 'Multiple Description Found for Rol_up1' ||
                         p_Store.ROLLUP_LEVEL_1;
        WHEN OTHERS THEN
          x_error_msg := 'Exception in Description for Rol_up1' ||
                         p_Store.ROLLUP_LEVEL_1||SQLERRM;
      END;
   --   Print_log('starting the getting bank acct name and number' ,'Y');
    BEGIN
      SELECT
      --MEANING,
      --DESCRIPTION,
      --ATTRIBUTE1,
       ATTRIBUTE2, ATTRIBUTE3
        INTO v_Bank_Account_Name, v_Bank_Account_Number
        FROM FND_LOOKUP_VALUES_VL
       WHERE SYSDATE BETWEEN NVL(START_DATE_ACTIVE, SYSDATE) AND
             NVL(END_DATE_ACTIVE, SYSDATE)
         AND ENABLED_FLAG = 'Y'
         AND LOOKUP_TYPE = 'RESA_AR_BANK_LKP'
         AND ATTRIBUTE_CATEGORY = 'RESA_AR_BANK_LKP'
         AND ATTRIBUTE1 = p_Tran_Type
         AND DESCRIPTION = p_Store.ACCOUNT_NUMBER
         AND DECODE(ATTRIBUTE4,NULL,p_Store.ROLLUP_LEVEL_1,ATTRIBUTE4) = p_Store.ROLLUP_LEVEL_1;
         x_Bank_Account_Name := v_Bank_Account_Name;
         x_Bank_Account_Number := v_Bank_Account_Number;
     --   Print_log('Bank account name and account number found....' ,'Y');
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
     -- Print_log('Bank account name and account number not found....' ,'Y');
        x_error_msg := 'Bank Acct Name/Number not Found for ' ||
                       p_Store.ACCOUNT_NUMBER;
      WHEN TOO_MANY_ROWS THEN
     -- Print_log('Bank account name and account number not found....' ,'Y');
        x_error_msg := 'Multiple Bank Acct Name/Number Found for ' ||
                       p_Store.ACCOUNT_NUMBER;
      WHEN OTHERS THEN
     -- Print_log('Bank account name and account number not found....' ,'Y');
        x_error_msg := 'Exception while deriving Bank Acct Name/Number for ' ||
                       p_Store.ACCOUNT_NUMBER;
    END;
    END IF;
    NULL;
  END;
  *//*
  FUNCTION ACCOUNT_SEG_STATUS(P_Seg_Value IN VARCHAR2,
                              P_Seg_Desc  IN VARCHAR2) RETURN VARCHAR2 IS
    V_Count NUMBER := 0;
  BEGIN
    SELECT COUNT(FFVV.Flex_Value)
      INTO V_Count
      FROM FND_FLEX_VALUES_VL FFVV, FND_FLEX_VALUE_SETS FFVS
     WHERE UPPER(FFVS.Flex_Value_Set_Name) = UPPER(P_Seg_Desc)
       AND FFVS.Flex_Value_Set_Id = FFVV.Flex_Value_Set_Id
       AND FFVV.Flex_Value = P_Seg_Value;
    IF V_Count = 1 THEN
      RETURN NULL;
    ELSE
      RETURN 'Invalid Value';
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN 'Invalid Value';
  END ACCOUNT_SEG_STATUS;
  */

   -----------inserting to stage 2 tables
   PROCEDURE insert_resa (p_source_name VARCHAR2, p_ret_code OUT NUMBER)
   AS
      CURSOR cur_ins
      IS
         SELECT tt.*
           FROM xxabrl_inst_resa_ar_int tt
          WHERE cust_rec_acct NOT IN
                                    ('222502', '222501', '222512', '222508')
            -- AND store=1601
            AND interface_flag IN ('V');

      v_interface_line_context        VARCHAR2 (240)  := 'ABRL_RESA_IS';
      v_interface_line_attribute1     VARCHAR2 (240);
      v_interface_line_attribute2     NUMBER          := 0;
      v_batch_source_name             VARCHAR2 (240)  := 'ReSA_IS';
      v_set_of_book_name              VARCHAR2 (240)
                                     := fnd_profile.VALUE ('GL_SET_OF_BKS_ID');
      v_line_type                     VARCHAR2 (240)  := 'LINE';
      v_description                   VARCHAR2 (240)  := NULL;
      --<< from 'Derive_Tran_Type'
      v_currency_code                 VARCHAR2 (240)  := 'INR';    -- currency
      v_amount                        VARCHAR2 (240)  := 0;
      v_transaction_type              VARCHAR2 (240)  := NULL;
      v_trx_date                      DATE            := NULL;
      v_gl_date                       DATE            := NULL;
      v_orig_system_batch_name        VARCHAR2 (240)  := NULL;
      v_store_name                    VARCHAR2 (240)  := NULL;
      v_orig_system_bill_address_id   VARCHAR2 (240)  := NULL;
      v_conversion_type               VARCHAR2 (240)  := 'Spot';
      -- ask for conversion type we have taken value as per the sample file
      v_conversion_date               DATE            := NULL;
      v_conversion_rate               VARCHAR2 (240)  := NULL;
      v_term_id                       VARCHAR2 (240)  := 'IMMEDIATE';
                                                       -- ask for payment term
      --  v_ATTRIBUTE_CATEGORY          VARCHAR2(240) :='ReSA RECEIVABLES INVOICE';  -- attribute category with values from sebastian ABRL_RESA_IS
      v_attribute1_tax                VARCHAR2 (240)  := NULL;
      v_operating_unit                VARCHAR2 (240)  := NULL;
      v_org_id                        VARCHAR2 (240)  := NULL;        --suresh
      v_created_by                    VARCHAR2 (240)
                                              := fnd_profile.VALUE ('USER_ID');
      v_creation_date                 DATE            := SYSDATE;
      v_last_updated_by               NUMBER  := fnd_profile.VALUE ('USER_ID');
      v_last_update_date              DATE            := SYSDATE;
      v_amount_dr                     NUMBER          := 0;
      v_receipt_number                VARCHAR2 (30);
      v_trx_type                      VARCHAR2 (30);
      v_cust_order_number             VARCHAR2 (240);
      v_reference1                    VARCHAR2 (240);
      v_reference2                    VARCHAR2 (240);
      v_reference3                    VARCHAR2 (240);
      x_nature                        VARCHAR2 (240);
      x_tran_type                     VARCHAR2 (240);
      x_description                   VARCHAR2 (240);
      x_error_msg                     VARCHAR2 (240);
      x_bank_account_name             VARCHAR2 (240);
      x_bank_account_number           VARCHAR2 (240);
      x_bnk_err_msg                   VARCHAR2 (240);
      x_tran_err_msg                  VARCHAR2 (240);
      x_vc_nature                     VARCHAR2 (240);
      v_valid_process                 NUMBER          := 0;
      v_invalid_process               NUMBER          := 0;
      v_s_num                         NUMBER          := -1;
      v_ins_exception                 EXCEPTION;
      v_insert_exception              EXCEPTION;
      v_error_msg                     VARCHAR2 (2400);
   BEGIN
      SELECT COUNT (1)
        INTO v_valid_process
        FROM xxabrl_inst_resa_ar_int tt
       --added cust rec acct 222512 and 222508
      WHERE  cust_rec_acct NOT IN ('222502', '222501', '222512', '222508')
         AND TRUNC (interface_date) LIKE SYSDATE
         AND interface_flag IN ('V', 'P');

      SELECT COUNT (1)
        INTO v_invalid_process
        FROM xxabrl_inst_resa_ar_int tt
       --added cust rec acct 222512 and 222508
      WHERE  cust_rec_acct IN ('222502', '222501', '222512', '222508')
         AND TRUNC (interface_date) LIKE SYSDATE
         AND interface_flag IN ('V', 'P');

      print_log ('Valid Records for ReSA Invoice/Receipts: '
                 || v_valid_process,
                 'Y'
                );
      print_log (   'In-Valid Records for ReSA Invoice/Receipts: '
                 || v_invalid_process,
                 'Y'
                );

      IF v_valid_process <> v_invalid_process
      THEN
         RAISE v_ins_exception;
      END IF;

      fnd_file.put_line (fnd_file.LOG, '### ReSA Data-Population Starts ###');
      v_interface_line_attribute2 := 0;
      print_log ('inserting data', 'Y');

      FOR v_cur_ins IN cur_ins
      LOOP
         fnd_file.put_line (fnd_file.LOG,
                            'Inserting (' || v_cur_ins.s_num || ')'
                           );

         BEGIN
            SELECT
                   --TO_CHAR(v_cur_ins.ACCOUNTING_DATE, 'DDMMYY') ||'S'||v_cur_ins.STORE -- << Need to take as date
                   v_cur_ins.tran_seq_no, v_cur_ins.record_number,
                   ROUND (v_cur_ins.amount_cr, 2),
                   ROUND (v_cur_ins.amount_dr, 2),
                   v_cur_ins.accounting_date, v_cur_ins.accounting_date,
                   'ReSA_IS' || TO_CHAR (v_cur_ins.accounting_date, 'DDMMYY'),
                   v_cur_ins.account_number, v_cur_ins.org_code,
                   v_cur_ins.org_unit_id
                                        -- ,v_cur_ins.s_num
            ,      v_cur_ins.cust_order_no,
                   v_cur_ins.reference1, v_cur_ins.reference2,
                   v_cur_ins.reference3
              INTO v_interface_line_attribute1, v_interface_line_attribute2,
                   v_amount,
                   v_amount_dr,
                   v_trx_date, v_gl_date,
                   v_orig_system_batch_name,
                   v_store_name, v_operating_unit,
                   v_org_id, v_cust_order_number,
                   v_reference1, v_reference2,
                   v_reference3
              FROM DUAL;
         EXCEPTION
            WHEN OTHERS
            THEN
               print_log (   'Exception in Data-Selection from cursor: '
                          || SQLERRM,
                          'Y'
                         );
         END;

         print_log ('Roll-Up#1: ' || v_cur_ins.rollup_level_1, 'Y');
         print_log ('Roll-Up#2: ' || v_cur_ins.rollup_level_2, 'Y');
         print_log ('Amount CR: ' || v_amount, 'Y');
         print_log ('Amount DR: ' || v_amount_dr, 'Y');
/*            NOT REQUIRED HERE SINCE ITS TAKEN CARE IN RESA_VALIDATE ROUTINE
--
--    08.01.2015        LE MERGER    MODIFICATION
--
        BEGIN
        IF    V_ORG_ID    =    901        OR
            V_ORG_ID =    861        OR
            V_ORG_ID =    841        OR
            V_ORG_ID =    961        OR
            V_ORG_ID =    801        OR
            V_ORG_ID =    941        OR
            V_ORG_ID =    821        THEN
            V_ORG_ID            =     SELECT DECODE(V_ORG_ID,    '901','1524',
                                                                      '861','1521',
                                                                       '841','1523',
                                                                       '961','1525',
                                                                       '801','1481',
                                                                       '941','1522',
                                                                        '821','1721'     ) FROM DUAL;
        END IF;
        BEGIN
        SELECT SHORT_CODE
                            INTO
                                    V_OPERATING_UNIT
        FROM APPS.HR_OPERATING_UNITS
        WHERE 1=1
        AND ORGANIZATION_ID = V_ORG_ID;
        EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                       FND_FILE.PUT_LINE(FND_FILE.LOG,'OPERATING UNIT MISSING FOR LE Merger ORG_ID   '|| V_ORG_ID);
                  WHEN TOO_MANY_ROWS THEN
                       FND_FILE.PUT_LINE(FND_FILE.LOG,'OPERATING UNIT TOO MANY FOR LE Merger ORG_ID   '|| V_ORG_ID);
                  WHEN OTHERS THEN
                       FND_FILE.PUT_LINE(FND_FILE.LOG,'OPERATING UNIT MISSING ERROR OTHERS LE Merger ORG_ID   '|| V_ORG_ID);
        END;
        END;
--
--    08.01.2015        LE MERGER    MODIFICATION
--
        NOT REQUIRED HERE SINCE ITS TAKEN CARE IN RESA_VALIDATE ROUTINE

*/

         -- this procedure will derive the OFIN transaction type , nature and description
         derive_tran_type (p_resa_cur         => v_cur_ins,
                           p_nature           => x_nature,
                           p_tran_type        => x_tran_type,
                           p_description      => x_description,
                           p_error_msg        => x_tran_err_msg
                          );
         v_transaction_type := NULL;                                -- Mapping
         print_log (':Tran_Type: ' || x_tran_type, 'Y');
         fnd_file.put_line (fnd_file.LOG, ':Line Nature: ' || x_nature);
         print_log (':Derive_Tran_Type Error Msg: ' || x_tran_err_msg, 'Y');

         IF x_nature = 'Transaction' AND (x_tran_err_msg IS NULL)
         THEN
             --Fnd_File.PUT_LINE(fnd_file.LOG, 'As Transaction ('||v_cur_ins.S_NUM||')');
            --Insert invoice
            IF x_tran_type IS NOT NULL
            THEN
               BEGIN
                  SELECT TYPE
                    INTO v_trx_type
                    FROM ra_cust_trx_types_all tran, hr_operating_units ood
                   WHERE UPPER (tran.NAME) = UPPER (x_tran_type)
                     AND ood.organization_id = tran.org_id
                     AND ood.short_code = v_operating_unit;   --    08.01.2015
-- AND ood.short_code = v_cur_ins.ORG_CODE;                --    OLD ORG_CODE VALUE FROM TABLE = APPS.XXABRL_INST_RESA_AR_INT
--   check with the sankara  for CM amount
/*
    IF v_TRX_TYPE = 'CM' THEN
               --v_AMOUNT := v_AMOUNT_DR * -1;
               IF v_AMOUNT >0 THEN
                  v_AMOUNT := v_AMOUNT * -1;               --08/12/2008
               ELSIF v_AMOUNT <0 THEN
                  v_AMOUNT := ABS(v_AMOUNT);
               END IF;
        END IF;
*/
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     fnd_file.put_line (fnd_file.LOG,
                                           'Invalid Transaction Type  '
                                        || '  -  '
                                        || v_operating_unit
                                        || x_tran_type
                                       );
                  WHEN TOO_MANY_ROWS
                  THEN
                     fnd_file.put_line
                              (fnd_file.LOG,
                                  'Multiple definition for Transaction Type '
                               || '  -  '
                               || v_operating_unit
                               || x_tran_type
                              );
                  WHEN OTHERS
                  THEN
                     fnd_file.put_line
                           (fnd_file.LOG,
                               'Exception while validating Transaction Type '
                            || '  -  '
                            || v_operating_unit
                            || x_tran_type
                           );
               END;
            END IF;                     --     IF x_Tran_Type IS NOT NULL THEN

--   Print_log('type : '||x_Nature,'Y');
            BEGIN
               -- Print_log('Inserting Line','Y');
               INSERT INTO xxabrl_inst_resa_ra_lines_all
                           (interface_line_context,
                            interface_line_attribute1,
                            interface_line_attribute2,
                            batch_source_name, sob_id, line_type,
                            description, currency_code, amount,
                            transaction_type, trx_date, gl_date,
                            orig_system_batch_name, customer_name,
                            conversion_type, conversion_date,
                            conversion_rate, term_id, attribute_category,
                            attribute1, attribute2, attribute3,
                            attribute4, org_code, created_by,
                            creation_date, last_updated_by,
                            last_update_date, interfaced_flag, org_id,
                            --NV_CUSTOMER_ID,
                            --NV_CUST_SITE_ID,
                            orig_system_customer_id,
                            orig_system_bill_address_id,
                            orig_system_ship_address_id
                           )
                    VALUES (v_interface_line_context,
                            v_interface_line_attribute1,
                            v_interface_line_attribute2,
                            v_batch_source_name, 'ABRL LEDGER', v_line_type,
                            x_description, v_currency_code, v_amount,
                            x_tran_type, v_trx_date, v_gl_date,
                            v_orig_system_batch_name, v_store_name,
                            v_conversion_type, v_conversion_date,
                            v_conversion_rate, v_term_id, 'ABRL_RESA_IS',
                            v_cust_order_number, v_reference1, v_reference2,
                            v_reference3, v_operating_unit, v_created_by,
                            v_creation_date, v_last_updated_by,
                            v_last_update_date, 'N', v_org_id,
                            v_cur_ins.orig_system_customer_id,
                            v_cur_ins.orig_system_bill_address_id,
                            v_cur_ins.orig_system_ship_address_id
                           );

               UPDATE xxabrl_inst_resa_ar_int
                  SET interface_flag = 'P'
                WHERE record_number = v_cur_ins.record_number;

-- COMMIT;
               print_log ('Line Inserted', 'Y');
            EXCEPTION
               WHEN OTHERS
               THEN
                  fnd_file.put_line
                     (fnd_file.LOG,
                         'Exception in while inserting data for Invoice-Lines: '
                      || SQLERRM
                     );
                  v_error_msg :=
                     v_error_msg
                     || 'Error Raise while inserting data for ReSA';
                  RAISE v_insert_exception;
            END;

            --Insert Distribution
            BEGIN
               -- Print_log('Inserting distribution','Y');
               INSERT INTO xxabrl_inst_resa_ra_dist_all
                           (interface_distribution_id, interface_line_id,
                            interface_line_context,
                            interface_line_attribute1,
                            interface_line_attribute2, account_class,
                            amount, org_id, description,
                            code_combination_id,
/*      SEGMENT1,
          SEGMENT2,
          SEGMENT3,
          SEGMENT4,
          SEGMENT5,
          SEGMENT6,
          SEGMENT7,
          SEGMENT8,                */
                                                created_by,
                            creation_date, last_updated_by,
                            last_update_date, last_update_login,
                            interfaced_flag
                           )
                    VALUES (NULL, NULL,
                            v_interface_line_context,
                            v_interface_line_attribute1,
                            v_interface_line_attribute2, 'REV',
                            v_amount, v_org_id, x_description,
                            v_cur_ins.code_combination_id,
/*      v_cur_ins.segment1,
          v_cur_ins.segment2,
          v_cur_ins.segment3,
          v_cur_ins.segment4,
          v_cur_ins.segment5,
          v_cur_ins.segment6,
          v_cur_ins.segment7,
          v_cur_ins.segment8,                 */
                                                          v_created_by,
                            v_creation_date, v_last_updated_by,
                            v_last_update_date, NULL,
                            'N'
                           );

               UPDATE xxabrl_inst_resa_ar_int
                  SET interface_flag = 'P'
                WHERE record_number = v_cur_ins.record_number;

--    commit;
               print_log ('Line-Dist Inserted', 'Y');
            EXCEPTION
               WHEN OTHERS
               THEN
                  fnd_file.put_line
                     (fnd_file.LOG,
                         'Exception in while inserting data for Distributions: '
                      || SQLERRM
                     );
                  RAISE v_insert_exception;
            END;
         ELSIF x_tran_err_msg IS NOT NULL
         THEN
            v_error_msg :=
                v_error_msg || 'Error Raise while Tran-Deriving for IS ReSA ';
            RAISE v_insert_exception;
         END IF;

         fnd_file.put_line
                   (fnd_file.LOG,
                    '--------------------------------------------------------'
                   );
      END LOOP;

      COMMIT;
   EXCEPTION
      WHEN v_ins_exception
      THEN
         fnd_file.put_line
                (fnd_file.LOG,
                 'Count for Valid_Process / Invalid_Process Records not same'
                );
         p_ret_code := 1;
      WHEN v_insert_exception
      THEN
         fnd_file.put_line (fnd_file.LOG,
                               'Error Raise while inserting data for ReSA'
                            || SQLERRM
                           );
         ROLLBACK;
         p_ret_code := 1;
      WHEN OTHERS
      THEN
         fnd_file.put_line (fnd_file.LOG,
                            'Exception in IS ReSA Insert: ' || SQLERRM
                           );
         p_ret_code := 1;
   END insert_resa;
END xxabrl_inst_resa_ar_conv_pkg; 
/


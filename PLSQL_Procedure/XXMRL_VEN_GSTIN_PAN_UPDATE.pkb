CREATE OR REPLACE PACKAGE BODY APPS.xxmrl_ven_gstin_pan_update
IS
/*=========================================================================================================
||   Program Name    : XXMRL Vendor GSTIN And PAN Updation Program
||   Description : This Program is used for to update the Vendors GSTIN And PAN and Reporing Code and TDS Type
||
||     Version                        Date                             Author                                Modification
||  ~~~~~~~~             ~~~~~~~~~~~~           ~~~~~~~~~~~~~~~~~~          ~~~~~~~~~~~~~~~~~
|| 1.0.0                       18-Dec-2019               Lokesh Poojari
||  ~~~~~~~~             ~~~~~~~~~~~~           ~~~~~~~~~~~~~~~~~~          ~~~~~~~~~~~~~~~~~
||
============================================================================================================*/
   PROCEDURE xxmrl_main_pkg (
      errbuf    OUT      VARCHAR2,
      retcode   OUT      NUMBER,
      p_type    IN       VARCHAR2
   )
   IS
   BEGIN
      IF p_type = 'GSTIN'
      THEN
         xxmrl_vendor_gstin_update;
      END IF;

      IF p_type = 'PAN'
      THEN
         xxmrl_vendor_pan_update;
      END IF;

      IF p_type = 'SEC_PAN'
      THEN
         xxmrl_vendor_sec_pan_update;
      END IF;

      IF p_type = 'REPORTING_CODE'
      THEN
         xxmrl_vendor_rep_code_update;
      END IF;

      IF p_type = 'TDS_TYPE'
      THEN
         xxmrl_vendor_tds_type_update;
      END IF;
   END xxmrl_main_pkg;

   PROCEDURE xxmrl_vendor_gstin_update
   IS
      CURSOR vendor_gstin
      IS
         SELECT DISTINCT aps.segment1 vendor_num,
                                                 --jprl.registration_type_code,
                                                 jprl.party_reg_id,
                         stg.new_gstin
                    FROM apps.ap_suppliers aps,
                         apps.ap_supplier_sites_all apsa,
                         apps.jai_party_regs jpr,
                         apps.jai_party_reg_lines jprl,
                         xxabrl.xxmrl_vendors_gstin_pan_stage stg
                   WHERE 1 = 1
                     AND aps.vendor_id = apsa.vendor_id
                     AND aps.vendor_id = jpr.party_id
                     AND apsa.vendor_site_id = jpr.party_site_id
                     AND jpr.party_reg_id = jprl.party_reg_id
                     AND apsa.vendor_site_id = stg.vendor_site_id    --1097385
                     AND aps.segment1 = stg.vendor_num             --'9712817'
                     AND jpr.party_site_id = stg.vendor_site_id     -- 1097385
                     AND apsa.org_id = stg.org_id                     -- 1481;
                     AND jprl.registration_type_code = 'GSTIN'
                     AND TRUNC (stg.creation_date) = TRUNC (SYSDATE);
   BEGIN
      FOR vendor_gstin_num IN vendor_gstin
      LOOP
         UPDATE apps.jai_party_reg_lines
            SET registration_number = vendor_gstin_num.new_gstin,
                last_update_date = SYSDATE
          WHERE 1 = 1
            AND party_reg_id = vendor_gstin_num.party_reg_id
            AND registration_type_code = 'GSTIN';

         fnd_file.put_line (fnd_file.output,
                            'Successfully Vendor GSTIN Updated'
                           );
      END LOOP;

      COMMIT;
   END xxmrl_vendor_gstin_update;

   PROCEDURE xxmrl_vendor_pan_update
   IS
      CURSOR vendor_pan
      IS
         SELECT DISTINCT aps.segment1 vendor_num,
                                                 --jprl.registration_type_code,
                                                 jprl.party_reg_id,
                         
                         --stg.new_gstin,
                         stg.new_pan
                    FROM apps.ap_suppliers aps,
                         apps.ap_supplier_sites_all apsa,
                         apps.jai_party_regs jpr,
                         apps.jai_party_reg_lines jprl,
                         xxabrl.xxmrl_vendors_gstin_pan_stage stg
                   WHERE 1 = 1
                     AND aps.vendor_id = apsa.vendor_id
                     AND aps.vendor_id = jpr.party_id
                     AND apsa.vendor_site_id = jpr.party_site_id
                     AND jpr.party_reg_id = jprl.party_reg_id
                     AND apsa.vendor_site_id = stg.vendor_site_id    --1097385
                     AND aps.segment1 = stg.vendor_num             --'9712817'
                     AND jpr.party_site_id = stg.vendor_site_id     -- 1097385
                     AND apsa.org_id = stg.org_id                     -- 1481;
                     AND jprl.registration_type_code = 'PAN'
                     --   and (jprl.registration_type_code='PAN' or jprl.sec_registration_type_code='PAN')
                     AND TRUNC (stg.creation_date) = TRUNC (SYSDATE);
   BEGIN
      FOR vendor_pan_num IN vendor_pan
      LOOP
         UPDATE apps.jai_party_reg_lines
            SET registration_number = vendor_pan_num.new_pan,
                --     SECONDARY_REGISTRATION_NUMBER=vendor_pan_num.new_pan,
                last_update_date = SYSDATE
          WHERE 1 = 1
            AND party_reg_id = vendor_pan_num.party_reg_id
            AND registration_type_code = 'PAN';

         -- and (registration_type_code='PAN' or sec_registration_type_code='PAN');
         fnd_file.put_line (fnd_file.output,
                            'Successfully Vendor PAN Updated');
      END LOOP;

      COMMIT;
   END xxmrl_vendor_pan_update;

   PROCEDURE xxmrl_vendor_sec_pan_update
   IS
      CURSOR vendor_pan
      IS
         SELECT DISTINCT aps.segment1 vendor_num,
                                                 --jprl.registration_type_code,
                                                 jprl.party_reg_id,
                         
                         --stg.new_gstin,
                         stg.new_pan
                    FROM apps.ap_suppliers aps,
                         apps.ap_supplier_sites_all apsa,
                         apps.jai_party_regs jpr,
                         apps.jai_party_reg_lines jprl,
                         xxabrl.xxmrl_vendors_gstin_pan_stage stg
                   WHERE 1 = 1
                     AND aps.vendor_id = apsa.vendor_id
                     AND aps.vendor_id = jpr.party_id
                     AND apsa.vendor_site_id = jpr.party_site_id
                     AND jpr.party_reg_id = jprl.party_reg_id
                     AND apsa.vendor_site_id = stg.vendor_site_id    --1097385
                     AND aps.segment1 = stg.vendor_num             --'9712817'
                     AND jpr.party_site_id = stg.vendor_site_id     -- 1097385
                     AND apsa.org_id = stg.org_id                     -- 1481;
                     AND jprl.sec_registration_type_code = 'PAN'
                     --   and (jprl.registration_type_code='PAN' or jprl.sec_registration_type_code='PAN')
                     AND TRUNC (stg.creation_date) = TRUNC (SYSDATE);
   BEGIN
      FOR vendor_pan_num IN vendor_pan
      LOOP
         UPDATE apps.jai_party_reg_lines
            SET               --registration_number = vendor_pan_num.new_pan,
               secondary_registration_number = vendor_pan_num.new_pan,
               last_update_date = SYSDATE
          WHERE 1 = 1
            AND party_reg_id = vendor_pan_num.party_reg_id
            AND sec_registration_type_code = 'PAN';

         -- and (registration_type_code='PAN' or sec_registration_type_code='PAN');
         fnd_file.put_line (fnd_file.output,
                            'Successfully Vendor Secondary PAN Updated'
                           );
      END LOOP;

      COMMIT;
   END xxmrl_vendor_sec_pan_update;

   PROCEDURE xxmrl_vendor_rep_code_update
   IS
      CURSOR vendor_rep_code
      IS
         SELECT DISTINCT aps.segment1 vendor_num,
--jprl.registration_type_code,
                                                 jprl.party_reg_id,
                         stg.new_gstin, stg.new_pan, stg.new_reporting_code
                    FROM apps.ap_suppliers aps,
                         apps.ap_supplier_sites_all apsa,
                         apps.jai_party_regs jpr,
                         apps.jai_party_reg_lines jprl,
                         apps.jai_reporting_associations jra,
                         xxabrl.xxmrl_vendors_gstin_pan_stage stg
                   WHERE 1 = 1
                     AND aps.vendor_id = apsa.vendor_id
                     AND aps.vendor_id = jpr.party_id
                     AND apsa.vendor_site_id = jpr.party_site_id
                     AND jpr.party_reg_id = jprl.party_reg_id
                     AND apsa.vendor_site_id = stg.vendor_site_id    --1097385
                     AND aps.segment1 = stg.vendor_num             --'9712817'
                     AND jpr.party_site_id = stg.vendor_site_id     -- 1097385
                     AND apsa.org_id = stg.org_id                     -- 1481;
                     AND jprl.party_reg_id = jra.entity_id
                     AND jra.reporting_type_name = 'ABRL Trade Classification'
                     --AND jprl.registration_type_code = 'PAN'
                     AND TRUNC (stg.creation_date) = TRUNC (SYSDATE);
   BEGIN
      FOR ven_rep_code IN vendor_rep_code
      LOOP
         UPDATE apps.jai_reporting_associations
            SET reporting_code = ven_rep_code.new_reporting_code,
                reporting_code_description = ven_rep_code.new_reporting_code,
                last_update_date = SYSDATE
          WHERE 1 = 1
            AND entity_id = ven_rep_code.party_reg_id
            AND reporting_type_name = 'ABRL Trade Classification';

         fnd_file.put_line (fnd_file.output,
                            'Successfully Vendor Reporting Code Updated'
                           );
      END LOOP;

      COMMIT;
   END xxmrl_vendor_rep_code_update;

   PROCEDURE xxmrl_vendor_tds_type_update
   IS
      CURSOR vendor_rep_code
      IS
         SELECT DISTINCT aps.segment1 vendor_num,
--jprl.registration_type_code,
                                                 jprl.party_reg_id,
                         stg.new_gstin, stg.new_pan, stg.new_reporting_code
                    FROM apps.ap_suppliers aps,
                         apps.ap_supplier_sites_all apsa,
                         apps.jai_party_regs jpr,
                         apps.jai_party_reg_lines jprl,
                         apps.jai_reporting_associations jra,
                         xxabrl.xxmrl_vendors_gstin_pan_stage stg
                   WHERE 1 = 1
                     AND aps.vendor_id = apsa.vendor_id
                     AND aps.vendor_id = jpr.party_id
                     AND apsa.vendor_site_id = jpr.party_site_id
                     AND jpr.party_reg_id = jprl.party_reg_id
                     AND apsa.vendor_site_id = stg.vendor_site_id    --1097385
                     AND aps.segment1 = stg.vendor_num             --'9712817'
                     AND jpr.party_site_id = stg.vendor_site_id     -- 1097385
                     AND apsa.org_id = stg.org_id                     -- 1481;
                     AND jprl.party_reg_id = jra.entity_id
                     AND jra.reporting_type_name = 'Vendor Type'
                     --AND jprl.registration_type_code = 'PAN'
                     AND TRUNC (stg.creation_date) = TRUNC (SYSDATE);
   BEGIN
      FOR ven_rep_code IN vendor_rep_code
      LOOP
         UPDATE apps.jai_reporting_associations
            SET reporting_code = ven_rep_code.new_reporting_code,
                reporting_code_description = ven_rep_code.new_reporting_code,
                last_update_date = SYSDATE
          WHERE 1 = 1
            AND entity_id = ven_rep_code.party_reg_id
            AND reporting_type_name = 'Vendor Type';

         fnd_file.put_line (fnd_file.output,
                            'Successfully Vendor TDS Type Updated'
                           );
      END LOOP;

      COMMIT;
   END xxmrl_vendor_tds_type_update;
END xxmrl_ven_gstin_pan_update; 
/


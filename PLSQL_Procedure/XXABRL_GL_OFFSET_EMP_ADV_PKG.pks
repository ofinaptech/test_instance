CREATE OR REPLACE PACKAGE APPS.XXABRL_GL_OFFSET_EMP_ADV_PKG IS
 PROCEDURE GL_OFFSET_EMP_ADV_PRINT_old (Errbuf         OUT VARCHAR2,
                             RetCode        OUT NUMBER,
                             P_FROM_GL_DATE IN VARCHAR2,
                             P_TO_GL_DATE    IN VARCHAR2
                             );

  PROCEDURE GL_OFFSET_EMP_ADV_PRINT(Errbuf         OUT VARCHAR2,
                             RetCode        OUT NUMBER,
                             p_org_id in NUMBER,
                             P_FROM_GL_DATE IN VARCHAR2,
                             P_TO_GL_DATE    IN VARCHAR2
                             );                            
  
END XXABRL_GL_OFFSET_EMP_ADV_PKG; 
/


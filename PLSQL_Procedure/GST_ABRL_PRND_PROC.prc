CREATE OR REPLACE PROCEDURE APPS.gst_abrl_prnd_proc (
   errbuf     OUT      VARCHAR2,
   retcode    OUT      VARCHAR2,
   p_org_id   IN       NUMBER
)
IS
BEGIN
      /*
      ========================
   =========================
   =========================
   =========================
   ====
      ||   Procedure Name  : APPS.GST_ABRL_PRND_PROC
      ||   Description :  GST ABRL Process Recovery Pending Report
      ||
      ||    Date                                     Author                                    Modification
      || ~~~~~~~~~~~            ~~~~~~~~~~~~~~~~~~          ~~~~~~~~~~~~~~~~~
      || 09-MAY-2018             Lokesh Poojari                          New Development
      ========================
   =========================
   =========================
   =========================
   ====*/
   BEGIN
      fnd_file.put_line
         (fnd_file.output,
          'TYPE|ORG ID|OU NAME|VENDOR NUM|VENDOR NAME|VENDOR SITE|GL DATE|INVOICE/GRN DATE|RECEIPT/INVOICE NUM|DOC NUM|VENDOR GSTIN|ABRL GSTIN|CGST AMOUNT|SGST AMOUNT|IGST AMOUNT|TOTAL TAX|STATUS'
         );

      FOR prnd_inv IN
         (SELECT TYPE, bb.org_id, bb.NAME, bb.vendor_num, bb.vendor_name,
                 bb.vendor_site, bb.gl_date, bb.invoice_grn_date,
                 bb.receipt_num_invoice_num, bb.doc_sequence_value,
                 bb.vendor_gstin_no, bb.abrl_gstin_no, bb.cgst_amnt,
                 bb.sgst_amnt, bb.igst_amnt,
                 (bb.cgst_amnt + bb.sgst_amnt + bb.igst_amnt
                 ) total_tax_amount,
                 bb.status
            FROM (SELECT TYPE, aa.org_id, aa.NAME, aa.vendor_num,
                         aa.vendor_name, aa.vendor_site, aa.gl_date,
                         aa.invoice_grn_date, aa.receipt_num_invoice_num,
                         aa.doc_sequence_value, aa.vendor_gstin_no,
                         aa.abrl_gstin_no, aa.cgst_amnt, aa.sgst_amnt,
                         aa.igst_amnt, aa.status
                    FROM (SELECT DISTINCT 'GRN' TYPE,
                                          hou.organization_id org_id,
                                          hou.NAME, aps.segment1 vendor_num,
                                          aps.vendor_name,
                                          apsa.vendor_site_code vendor_site,
                                          aia.gl_date,
                                          TRUNC
                                             (rsh.creation_date
                                             ) invoice_grn_date,
                                          rsh.receipt_num
                                                      receipt_num_invoice_num,
                                          aia.doc_sequence_value,
                                          (SELECT DISTINCT registration_number
                                                      FROM apps.jai_party_regs jpr,
                                                           apps.jai_party_reg_lines jprl
                                                     WHERE 1 = 1
                                                       AND jpr.party_reg_id =
                                                              jprl.party_reg_id
                                                       AND jpr.party_id =
                                                                 aps.vendor_id
                                                       --    AND jpr.party_site_id =apsa.vendor_site_id
                                                       AND jpr.org_id =
                                                                   apsa.org_id
                                                       AND jprl.effective_to IS NULL
                                                       AND ROWNUM <= 1
                                                       AND registration_type_code =
                                                                       'GSTIN'
                                                       AND supplier_flag = 'Y'
                                                       AND site_flag = 'Y'
                                                       AND registration_number <>
                                                              'RCMUNREGISTERED'
                                                       AND jpr.party_type_code =
                                                              'THIRD_PARTY_SITE')
                                                              vendor_gstin_no,
                                          (SELECT DISTINCT registration_number
                                                      FROM apps.jai_party_regs jpr,
                                                           apps.jai_party_reg_lines jprl
                                                     WHERE 1 = 1
                                                       AND jpr.party_reg_id =
                                                              jprl.party_reg_id
                                                       AND jpr.org_id =
                                                                   apsa.org_id
                                                       AND jprl.effective_to IS NULL
                                                       AND jprl.registration_type_code =
                                                                       'GSTIN'
                                                       AND party_type_code =
                                                                          'OU'
                                                       AND org_classification_code =
                                                                     'TRADING')
                                                                abrl_gstin_no,
                                          (SELECT NVL
                                                     (SUM
                                                         (DECODE
                                                             (rec_tax_amt_tax_curr,
                                                              0, nrec_tax_amt_trx_curr,
                                                              rec_tax_amt_tax_curr
                                                             )
                                                         ),
                                                      0
                                                     ) tax_amount
                                             FROM apps.jai_tax_lines jtl
                                            WHERE entity_code =
                                                             'RCV_TRANSACTION'
                                              AND tax_rate_code LIKE '%CGST%'
                                              AND tax_rate_code NOT LIKE
                                                                       '%RCM%'
                                                                       AND RECOVERABLE_FLAG='Y'
                                              AND trx_type = 'RECEIVE'
                                              AND jtl.trx_id =
                                                        rsh.shipment_header_id)
                                                                    cgst_amnt,
                                          (SELECT NVL
                                                     (SUM
                                                         (DECODE
                                                             (rec_tax_amt_tax_curr,
                                                              0, nrec_tax_amt_trx_curr,
                                                              rec_tax_amt_tax_curr
                                                             )
                                                         ),
                                                      0
                                                     ) tax_amount
                                             FROM apps.jai_tax_lines jtl
                                            WHERE entity_code =
                                                             'RCV_TRANSACTION'
                                              AND tax_rate_code LIKE '%SGST%'
                                              AND tax_rate_code NOT LIKE
                                                                       '%RCM%'
                                                                       AND RECOVERABLE_FLAG='Y'
                                              AND trx_type = 'RECEIVE'
                                              AND jtl.trx_id =
                                                        rsh.shipment_header_id)
                                                                    sgst_amnt,
                                          (SELECT NVL
                                                     (SUM
                                                         (DECODE
                                                             (rec_tax_amt_tax_curr,
                                                              0, nrec_tax_amt_trx_curr,
                                                              rec_tax_amt_tax_curr
                                                             )
                                                         ),
                                                      0
                                                     ) tax_amount
                                             FROM apps.jai_tax_lines jtl
                                            WHERE entity_code =
                                                             'RCV_TRANSACTION'
                                              AND tax_rate_code LIKE '%IGST%'
                                              AND tax_rate_code NOT LIKE
                                                                       '%RCM%'
                                                                       AND RECOVERABLE_FLAG='Y'
                                              AND trx_type = 'RECEIVE'
                                              AND jtl.trx_id =
                                                        rsh.shipment_header_id)
                                                                    igst_amnt,
                                          (SELECT DISTINCT status
                                                      FROM apps.jai_rgm_recovery_lines
                                                     WHERE entity_code =
                                                              'RCV_TRANSACTION'
                                                       AND ROWNUM <= 1
                                                       AND document_id =
                                                              rsh.shipment_header_id
                                                                                    --  AND UPPER (NVL (process_action, 'RECOVER')) ='RECOVER'
                                                                                       --AND  status = 'PENDING_REC'
                                          ) status
                                     FROM
                                          --apps.jai_rgm_recovery_lines jrr,
                                          apps.rcv_shipment_headers rsh,
                                          apps.rcv_transactions rt,
                                          apps.ap_invoices_all aia,
                                          apps.ap_invoice_lines_all ail,
                                          apps.hr_operating_units hou,
                                          apps.po_headers_all pha,
                                          apps.ap_suppliers aps,
                                          apps.ap_supplier_sites_all apsa
                                    WHERE 1 = 1
                                      AND rsh.shipment_header_id =
                                                         rt.shipment_header_id
                                      AND aia.invoice_id = ail.invoice_id
                                      AND rt.transaction_id =
                                                        ail.rcv_transaction_id
                                      --     AND rsh.shipment_header_id = jrr.document_id
                                      AND rt.po_header_id = pha.po_header_id
                                      AND hou.organization_id = pha.org_id
                                      AND aps.vendor_id = apsa.vendor_id
                                      AND aps.vendor_id = pha.vendor_id
                                      AND apsa.vendor_site_id =
                                                            pha.vendor_site_id
--                             AND aps.vendor_id = rsh.vendor_id
--                             AND apsa.vendor_site_id=rsh.vendor_site_id
                                      AND hou.organization_id = apsa.org_id
                                      --    AND jrr.entity_code = 'RCV_TRANSACTION'
                                      AND rt.transaction_type = 'RECEIVE'
                                      AND TRUNC (rsh.creation_date) >=
                                                                 '01-jul-2017'
                                             -- AND jrr.status <> 'CONFIRMED'
                                      --       AND UPPER (NVL (process_action, 'RECOVER')) = 'RECOVER'
                                        --     AND jrr.status = 'PENDING_REC'
                                             --    AND jrr.status = 'RECOVERED'
                                      AND ap_invoices_pkg.get_approval_status
                                                 (aia.invoice_id,
                                                  aia.invoice_amount,
                                                  aia.payment_status_flag,
                                                  aia.invoice_type_lookup_code
                                                 ) = 'APPROVED'
                                      AND NOT EXISTS (
                                             SELECT 1
                                               FROM apps.rcv_transactions
                                              WHERE shipment_header_id =
                                                         rt.shipment_header_id
                                                --   AND document_id=shipment_header_id
                                                AND transaction_type <>
                                                                     'RECEIVE'
                                                AND transaction_type IN
                                                       ('RETURN TO RECEIVING',
                                                        'RETURN TO VENDOR',
                                                        'CORRECT'))
                          UNION
                          SELECT DISTINCT 'INVOICE' TYPE,
                                          org.organization_id org_id,
                                          org.NAME, aps.segment1 vendor_num,
                                          aps.vendor_name,
                                          apsa.vendor_site_code vendor_site,
                                          aia.gl_date,
                                          aia.invoice_date invoice_grn_date,
                                          
                                          -- processed_date,
                                          aia.invoice_num
                                                      receipt_num_invoice_num,
                                          aia.doc_sequence_value,
                                          (SELECT DISTINCT registration_number
                                                      FROM apps.jai_party_regs jpr,
                                                           apps.jai_party_reg_lines jprl
                                                     WHERE 1 = 1
                                                       AND jpr.party_reg_id =
                                                              jprl.party_reg_id
                                                       AND jpr.party_id =
                                                                 aps.vendor_id
                                                       --        AND jpr.party_site_id =apsa.vendor_site_id
                                                       AND jpr.org_id =
                                                                   apsa.org_id
                                                       AND jprl.effective_to IS NULL
                                                       AND ROWNUM <= 1
                                                       AND registration_type_code =
                                                                       'GSTIN'
                                                       AND supplier_flag = 'Y'
                                                       AND site_flag = 'Y'
                                                       AND registration_number <>
                                                              'RCMUNREGISTERED'
                                                       AND jpr.party_type_code =
                                                              'THIRD_PARTY_SITE')
                                                              vendor_gstin_no,
                                          (SELECT DISTINCT registration_number
                                                      FROM apps.jai_party_regs jpr,
                                                           apps.jai_party_reg_lines jprl
                                                     WHERE 1 = 1
                                                       AND jpr.party_reg_id =
                                                              jprl.party_reg_id
                                                       AND jpr.org_id =
                                                                   apsa.org_id
                                                       AND jprl.effective_to IS NULL
                                                       AND jprl.registration_type_code =
                                                                       'GSTIN'
                                                       AND party_type_code =
                                                                          'OU'
                                                       AND org_classification_code =
                                                                     'TRADING')
                                                                abrl_gstin_no,
                                          (SELECT NVL
                                                     (SUM
                                                         (DECODE
                                                             (rec_tax_amt_tax_curr,
                                                              0, nrec_tax_amt_trx_curr,
                                                              rec_tax_amt_tax_curr
                                                             )
                                                         ),
                                                      0
                                                     ) tax_amount
                                             FROM apps.jai_tax_lines jtl
                                            WHERE entity_code = 'AP_INVOICES'
                                              AND tax_rate_code LIKE '%CGST%'
                                              AND tax_rate_code NOT LIKE
                                                                       '%RCM%'
                                                                       AND RECOVERABLE_FLAG='Y'
                                              AND jtl.trx_id = aia.invoice_id)
                                                                    cgst_amnt,
                                          (SELECT NVL
                                                     (SUM
                                                         (DECODE
                                                             (rec_tax_amt_tax_curr,
                                                              0, nrec_tax_amt_trx_curr,
                                                              rec_tax_amt_tax_curr
                                                             )
                                                         ),
                                                      0
                                                     ) tax_amount
                                             FROM apps.jai_tax_lines jtl
                                            WHERE entity_code = 'AP_INVOICES'
                                              AND tax_rate_code LIKE '%SGST%'
                                              AND tax_rate_code NOT LIKE
                                                                       '%RCM%'
                                                                       AND RECOVERABLE_FLAG='Y'
                                              AND jtl.trx_id = aia.invoice_id)
                                                                    sgst_amnt,
                                          (SELECT NVL
                                                     (SUM
                                                         (DECODE
                                                             (rec_tax_amt_tax_curr,
                                                              0, nrec_tax_amt_trx_curr,
                                                              rec_tax_amt_tax_curr
                                                             )
                                                         ),
                                                      0
                                                     ) tax_amount
                                             FROM apps.jai_tax_lines jtl
                                            WHERE entity_code = 'AP_INVOICES'
                                              AND tax_rate_code LIKE '%IGST%'
                                              AND tax_rate_code NOT LIKE
                                                                       '%RCM%'
                                                                       AND RECOVERABLE_FLAG='Y'
                                              AND jtl.trx_id = aia.invoice_id)
                                                                    igst_amnt,
                                          (SELECT DISTINCT status
                                                      FROM apps.jai_rgm_recovery_lines
                                                     WHERE entity_code =
                                                                 'AP_INVOICES'
                                                       AND ROWNUM <= 1
                                                       AND document_id =
                                                                aia.invoice_id
                                                                              --  AND UPPER (NVL (process_action, 'RECOVER')) ='RECOVER'
                                                                                 --AND  status = 'PENDING_REC'
                                          ) status
--                                 apps.xxabrl_inv_status(aia.invoice_id,
--                                           aia.invoice_amount,
--                                           aia.Payment_status_flag,
--                                           aia.Invoice_type_lookup_code) invoice_status
                          FROM            apps.ap_suppliers aps,
                                          apps.ap_supplier_sites_all apsa,
                                          apps.ap_invoices_all aia,
                                          apps.hr_operating_units org,
                                          --      apps.jai_rgm_recovery_lines jrr,
                                          apps.ap_invoice_lines_all ail
                                    WHERE 1 = 1
                                      AND aps.vendor_id = apsa.vendor_id
                                      AND aia.vendor_id = aps.vendor_id
                                      AND aia.invoice_id = ail.invoice_id
                                      AND aia.vendor_site_id =
                                                           apsa.vendor_site_id
                                      AND ail.inventory_item_id IS NULL
                                      AND ap_invoices_pkg.get_approval_status
                                                 (aia.invoice_id,
                                                  aia.invoice_amount,
                                                  aia.payment_status_flag,
                                                  aia.invoice_type_lookup_code
                                                 ) = 'APPROVED'
                                      AND aia.invoice_type_lookup_code =
                                                                    'STANDARD'
                                      AND EXISTS (
                                             SELECT 1
                                               FROM apps.jai_tax_lines
                                              WHERE entity_code =
                                                                 'AP_INVOICES'
                                                AND trx_id = aia.invoice_id)
                                      AND ail.line_type_lookup_code = 'ITEM'
                                      AND NOT EXISTS (
                                             SELECT 1
                                               FROM apps.ap_invoice_lines_all
                                              WHERE line_source =
                                                                'HEADER MATCH'
                                                AND invoice_id =
                                                                aia.invoice_id)
                                      --      AND aia.SOURCE not in ('RETEK','SMDN')
                                      AND aia.org_id = org.organization_id
                                      AND ail.match_type <> 'ITEM_TO_RECEIPT'
                                      AND ail.rcv_transaction_id IS NULL
                                      AND ail.po_distribution_id IS NULL
                                      AND aia.SOURCE = 'Manual Invoice Entry'
                                      AND aia.gl_date >= '01-jul-2017') aa
                   WHERE 1 = 1
                     AND (   aa.cgst_amnt <> 0
                          OR aa.sgst_amnt <> 0
                          OR aa.igst_amnt <> 0
                         )) bb
           WHERE 1 = 1
             AND (bb.status = 'PENDING_REC' OR bb.status IS NULL)
             --   and bb.DOC_SEQUENCE_VALUE='381169977'
             -- AND bb.receipt_num_invoice_num = '9050015894'
             AND bb.org_id = NVL (p_org_id, bb.org_id))
      LOOP
         fnd_file.put_line (fnd_file.output,
                               prnd_inv.TYPE
                            || '|'
                            || prnd_inv.org_id
                            || '|'
                            || prnd_inv.NAME
                            || '|'
                            || prnd_inv.vendor_num
                            || '|'
                            || prnd_inv.vendor_name
                            || '|'
                            || prnd_inv.vendor_site
                            || '|'
                            || prnd_inv.gl_date
                            || '|'
                            || prnd_inv.invoice_grn_date
                            || '|'
                            || prnd_inv.receipt_num_invoice_num
                            || '|'
                            || prnd_inv.doc_sequence_value
                            || '|'
                            || prnd_inv.vendor_gstin_no
                            || '|'
                            || prnd_inv.abrl_gstin_no
                            || '|'
                            || prnd_inv.cgst_amnt
                            || '|'
                            || prnd_inv.sgst_amnt
                            || '|'
                            || prnd_inv.igst_amnt
                            || '|'
                            || prnd_inv.total_tax_amount
                            || '|'
                            || prnd_inv.status
                           );
      END LOOP;
   END;
END; 
/


CREATE OR REPLACE Package APPS.XXABRL_APMIGR_OPINV_PKG IS
 PROCEDURE INVOICE_VALIDATE(Errbuf 	  	OUT VARCHAR2
				  ,RetCode	  	OUT NUMBER
				  ,P_Org_Id		In  NUMBER
				  ,P_Data_Source	IN  VARCHAR2,
          p_action varchar2);
 PROCEDURE INVOICE_INSERT( P_Org_Id		IN  NUMBER
				  ,P_Data_Source	IN  VARCHAR2
				   );					

END XXABRL_APMIGR_OPINV_PKG;
/


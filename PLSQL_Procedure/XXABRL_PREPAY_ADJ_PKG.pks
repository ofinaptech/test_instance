CREATE OR REPLACE PACKAGE APPS.xxabrl_prepay_adj_pkg
IS
   PROCEDURE xxabrl_main_pkg (errbuf OUT VARCHAR2, retcode OUT NUMBER);

   PROCEDURE xxabrl_prepay_adj_prc;
END; 
/


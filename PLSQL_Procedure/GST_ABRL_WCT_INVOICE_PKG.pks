CREATE OR REPLACE PACKAGE APPS.GST_ABRL_wct_invoice_pkg AS
 PROCEDURE GST_ABRL_wct_inv_proc(ERRBUFF             OUT VARCHAR2,
                                RETCODE             OUT NUMBER,
                                p_vendor_NAME       IN NUMBER,
                                p_vendor_num_from   IN VARCHAR2,
                                p_vendor_num_TO       IN VARCHAR2,
                                p_vendor_site_num   IN VARCHAR2,
                                p_tds_section       IN VARCHAR2,
                                p_inv_date_from     IN VARCHAR2,
                                p_inv_date_to       IN VARCHAR2,
                                p_cancelled_flag    IN VARCHAR2,
                                p_org_id            IN NUMBER
                                );
END  GST_ABRL_wct_invoice_pkg; 
/


CREATE OR REPLACE PACKAGE APPS.xxabrl_gl_codes_refresh
IS
   PROCEDURE xxabrl_main_pkg (
      errbuf    OUT      VARCHAR2,
      retcode   OUT      NUMBER,
      p_type    IN       VARCHAR2
   );

   PROCEDURE xxabrl_ou_sbu_refresh;

   PROCEDURE xxabrl_locations_refresh;

   PROCEDURE xxabrl_cc_refresh;
END xxabrl_gl_codes_refresh; 
/


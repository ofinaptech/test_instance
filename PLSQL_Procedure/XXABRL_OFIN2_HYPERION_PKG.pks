CREATE OR REPLACE PACKAGE APPS.XXABRL_OFIN2_HYPERION_PKG
 IS
 PROCEDURE XXABRL_HYPN_DATA_CALLING(errbuf out varchar2,
                                     retcode out varchar2,
                                     p_period_name varchar2,
                                     p_version_no number,
                                     p_param_data varchar2); 
 PROCEDURE XXABRL_HYPN_SUPER_OPS_DATA(errbuf out varchar2,
                                     retcode out varchar2,
                                     p_period_name varchar2,
                                     p_version_no number,
                                     p_param_data varchar2); 
 PROCEDURE XXABRL_HYPN_HYPER_OPS_DATA(errbuf out varchar2,
                                     retcode out varchar2,
                                     p_period_name varchar2,
                                     p_version_no number,
                                     p_param_data varchar2); 
                                      
 PROCEDURE XXABRL_HYPN_SCM_OPS_DATA(errbuf out varchar2,
                                     retcode out varchar2,
                                     p_period_name varchar2,
                                     p_version_no number,
                                     p_param_data varchar2);
 PROCEDURE XXABRL_HYPN_SUPER_MCD_DATA(errbuf out varchar2,
                                     retcode out varchar2,
                                     p_period_name varchar2,
                                     p_version_no number,
                                     p_param_data varchar2);
 PROCEDURE XXABRL_HYPN_HYPER_MCD_DATA(errbuf out varchar2,
                                     retcode out varchar2,
                                     p_period_name varchar2,
                                     p_version_no number,
                                     p_param_data varchar2);
 PROCEDURE XXABRL_HYPN_SCM_MCD_DATA(errbuf out varchar2,
                                     retcode out varchar2,
                                     p_period_name varchar2,
                                     p_version_no number,
                                     p_param_data varchar2);
 PROCEDURE XXABRL_HYPN_OHDS_MCD_DATA(errbuf out varchar2,
                                     retcode out varchar2,
                                     p_period_name varchar2,
                                     p_version_no number,
                                     p_param_data varchar2);
 PROCEDURE XXABRL_HYPN_OHDS_OPS_DATA(errbuf out varchar2,
                                     retcode out varchar2,
                                     p_period_name varchar2,
                                     p_version_no number,
                                     p_param_data varchar2);
 PROCEDURE XXABRL_HYPN_MKTNG_DATA(errbuf out varchar2,
                                     retcode out varchar2,
                                     p_period_name varchar2,
                                     p_version_no number,
                                     p_param_data varchar2);
 PROCEDURE XXABRL_HYPN_CONSOL_DATA(errbuf out varchar2,
                                     retcode out varchar2,
                                     p_period_name varchar2,
                                     p_version_no number,
                                     p_param_data varchar2);
 PROCEDURE XXABRL_HYPN_VMERCH_DATA(errbuf out varchar2,
                                     retcode out varchar2,
                                     p_period_name varchar2,
                                     p_version_no number,
                                     p_param_data varchar2);
 PROCEDURE XXABRL_HYPN_DATA_VALIDAION(p_period_name varchar2); 
                                      
 END XXABRL_OFIN2_HYPERION_PKG; 
/


CREATE OR REPLACE PROCEDURE APPS.xxabrl_pr_pending_sum_proc (
   errbuf         OUT   VARCHAR2,
   retcode        OUT   NUMBER,
   p_summ_email         VARCHAR2
)
IS
/*
   =========================================================================================================
   ||   Procedure Name  : xxabrl_update_item_eccid_prc
   ||   Description : ABRL PR Pending With Buyer Concurrent Mailer Scheduler
   ||
   ||   Version     Date            Author              Modification
   ||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
     ||   1.0.0      15-Dec-2010  Mitul and Govind      New Development
    ||   1.0.1      17-Dec-2010  Mitul                 if any buyer e-mail address is inactive then script pick next buyer
   ||   1.0.2      27-Dec-2010  Mitul                  Sending Summary Output
   ||   1.0.3      12-Jan-2011  Mitul                   Restict PR Line which are already Cancelled
   ||   1.0.4      28-Feb-2011  Mitul                   Added Mail Address in Summary Mailer  
   ||   1.0.5     28-Dec-2011  Narasimhulu              Cancelled PR's and Finally closed PR's not considered
   =======================================================================================================*/
               stop_program      EXCEPTION;
               v_email_subject   VARCHAR2 (200);
               v_email_list      VARCHAR2 (200);
               v_errmsg          VARCHAR2 (200);
               v_status_code     VARCHAR2 (200);
               mailhost          VARCHAR2 (40)       := 'mail.adityabirla.com';
               crlf              VARCHAR2 (2)        := CHR (13) || CHR (10);
               v_email           VARCHAR2 (16000);
               message_1         LONG;
               message_2         VARCHAR2 (20000);
               mail_conn         UTL_SMTP.connection;
               v_from_mail       VARCHAR2 (50)       := 'OFIN Support';
               --v_to_mail varchar2(60):= 'srinivas.thalla-v@retail.adityabirla.com';
               v_color           VARCHAR2 (50);
               message_summ_1    VARCHAR2 (16000);
               message_summ_2    VARCHAR2 (16000);
               message_summ_3    VARCHAR2 (16000);
BEGIN
       BEGIN
          FOR v_cur IN
             (SELECT   buyer_id, buyer, e_mail, days, days1, days2, days3,
                       SUM (days + days1 + days2 + days3) total
                  FROM (SELECT   buyer_id, buyer, e_mail,
                                 NVL (SUM ("0-7 DAYS"), 0) days,
                                 NVL (SUM ("8-15 DAYS"), 0) days1,
                                 NVL (SUM ("16-30 DAYS"), 0) days2,
                                 NVL (SUM ("> 30 DAYS"), 0) days3
                            FROM (SELECT DISTINCT pol.suggested_buyer_id buyer_id,
                                                     ppf.first_name
                                                  || ' '
                                                  || ppf.last_name buyer,
                                                  ppf.email_address e_mail,
                                                  por.segment1,
                                                  TRUNC
                                                     (por.approved_date
                                                     ) approved_date,
                                                  CASE
                                                     WHEN (  TRUNC (SYSDATE)
                                                           - TRUNC (approved_date)
                                                          ) BETWEEN 0 AND 7
                                                        THEN 1
                                                  END AS "0-7 DAYS",
                                                  CASE
                                                     WHEN (  TRUNC (SYSDATE)
                                                           - TRUNC (approved_date)
                                                          ) BETWEEN 8 AND 15
                                                        THEN 1
                                                  END AS "8-15 DAYS",
                                                  CASE
                                                     WHEN (  TRUNC (SYSDATE)
                                                           - TRUNC (approved_date)
                                                          ) BETWEEN 16 AND 30
                                                        THEN 1
                                                  END AS "16-30 DAYS",
                                                  CASE
                                                     WHEN (  TRUNC (SYSDATE)
                                                           - TRUNC (approved_date)
                                                          ) > 30
                                                        THEN 1
                                                  END AS "> 30 DAYS"
                                             FROM apps.po_requisition_headers_all por,
                                                  apps.po_requisition_lines_all pol,
                                                   apps.po_req_distributions_all prd,
                                                  apps.po_agents pa,
                                                  apps.per_all_people_f ppf
                                            WHERE 1 = 1
                                              AND por.authorization_status =
                                                                        'APPROVED'
                                              AND pa.agent_id =
                                                            pol.suggested_buyer_id
                                              AND pa.end_date_active IS NULL
                                              AND ppf.person_id = pa.agent_id
                                              ---Added by Narasimhulu 29/dec/2011
                                              and pol.REQUISITION_LINE_ID = prd.REQUISITION_LINE_ID
                                              ---Added by Narasimhulu 29/dec/2011
                                              AND SYSDATE
                                                     BETWEEN ppf.effective_start_date
                                                         AND ppf.effective_end_date
                                              AND pol.requisition_header_id =
                                                         por.requisition_header_id
                                             AND NVL(POL.CLOSED_CODE,'1') <> 'FINALLY CLOSED'
                                            and (POL.CANCEL_FLAG='N' OR POL.CANCEL_FLAG IS NULL)
                                            ---Added by Narasimhulu 29/dec/2011
                                            AND POL.PURCHASING_AGENT_ID IS NULL   
                                             AND POL.LINE_LOCATION_ID IS NULL
                                              --AND pol.suggested_buyer_id IN (5503)
                                              --,5503,5506,7952,3200)
                                         AND PRD.DISTRIBUTION_ID NOT IN (
                                            SELECT PDA.REQ_DISTRIBUTION_ID FROM  apps.po_distributions_all pda
                                                WHERE PDA.REQ_DISTRIBUTION_ID IS NOT NULL
                                                    )
                                              ---Added by Narasimhulu 29/dec/2011
                                             /* AND por.requisition_header_id NOT IN (
                                                     SELECT DISTINCT por1.requisition_header_id
                                                                FROM apps.po_requisition_headers_all por1,
                                                                     apps.po_requisition_lines_all pol1,
                                                                     apps.po_req_distributions_all pod1,
                                                                     apps.po_distributions_all pd1
                                                               WHERE 1 = 1
                                                                 AND por1.requisition_header_id =
                                                                        pol1.requisition_header_id
                                                                 AND pol1.requisition_line_id =
                                                                        pod1.requisition_line_id
                                                                 AND pd1.req_distribution_id =
                                                                        pod1.distribution_id
                                                                 AND pol1.suggested_buyer_id =
                                                                        pol.suggested_buyer_id
                                                                 AND por1.requisition_header_id =
                                                                        por.requisition_header_id)*/
                                                                        )
                        GROUP BY buyer_id, buyer, e_mail)
              GROUP BY buyer_id, buyer, e_mail, days, days1, days2, days3
              ORDER BY 2 DESC)
          LOOP
             BEGIN
                BEGIN
                   mail_conn := UTL_SMTP.open_connection (mailhost, 25);
                   UTL_SMTP.helo (mail_conn, mailhost);
                   UTL_SMTP.mail (mail_conn, v_from_mail);
                   
                    utl_smtp.rcpt (mail_conn, v_cur.e_mail); --v_cur.mail
                    utl_smtp.rcpt (mail_conn, 'ajaykumar.singh@retail.adityabirla.com');
                    utl_smtp.rcpt (mail_conn, 'balasubramanian.sk@retail.adityabirla.com');
                   message_2 :=
                         '<html>
                        <body> 
                        <TABLE height=72 cellSpacing=0 cellPadding=0 width=770 border=0>
                           <font face=''Calibri''>
                        Dear '
                      || v_cur.buyer
                      || ',
                        <br/>
                         <br/>
                        <b>
                        Below are the Purchase requisitions pending with you as on '
                      || TO_CHAR (SYSDATE, 'dd-mm-yyyy')
                      || ','
                      || '
                        <b> 
                        <br/>
                        <br/>
                        </font>
                         <table Border=''1'' width=''250'' height=''500''>
                         <font face=''Calibri'' color=''white''>
                        <Tr bgcolor=''green'' >
                        <td ALIGN=''left'' color=''white''><b>Days</b></td>
                        <td ALIGN=''right''><b>Pending PRs</b></td>
                        </font>
                        </tr>
                        <font face=''Calibri''>
                        <Tr>
                        <td><b>Days 00 - 07</b></td>
                        <td ALIGN=''right''><b>'
                      || v_cur.days
                      || '</b></td>
                        </tr>
                        <Tr>
                        <td><b>Days 08 - 15</b></td>
                        <td ALIGN=''right''><b>'
                      || v_cur.days1
                      || '</td>
                        </tr>
                        <Tr>
                        <td><b>Days 16 - 30</b></td>
                        <td ALIGN=''right''><b>'
                      || v_cur.days2
                      || '</b></td>
                        </tr>
                        <Tr>
                        <td><b>>30 Days</b></td>
                        <td ALIGN=''right''><b>'
                      || v_cur.days3
                      || '</b></td>
                        </tr>
                        <Tr>
                        <td><b>Total</b></td>
                        <td ALIGN=''right''><b>'
                      || v_cur.total
                      || '</b></td>
                        </tr>
                        </font>
                        </table>
                              <br/>
                                                    <font face=''Calibri'' color=''black'' bold=''true''>
                        This is auto generated mail by the system, Please do not reply or respond                    .
                        </font>
                          <br/><br/>
                        <font face=''Calibri'' color=''blue'' bold=''true''>
                        <b>Thanks and Regards</b>,
                        <br/>
                        <b>OFIN Support</b>
                        </font>
                        </body>
                        </html>';
                   UTL_SMTP.open_data (mail_conn);
                   UTL_SMTP.write_data (mail_conn,
                                           'Date: '
                                        || TO_CHAR (SYSDATE,
                                                    'DD-MON-YYYY HH24:MI:SS'
                                                   )
                                        || UTL_TCP.crlf
                                       );
                   UTL_SMTP.write_data (mail_conn,
                                        'From: ' || v_from_mail || UTL_TCP.crlf
                                       );
                   UTL_SMTP.write_data (mail_conn,
                                           'To: '
                                        || SUBSTR (v_cur.e_mail,
                                                   0,
                                                   INSTR (v_cur.e_mail, '@') - 1
                                                  )
                                        || UTL_TCP.crlf
                                       );
                   --utl_smtp.write_data(mail_conn, 'Sent: '  || TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') || utl_tcp.crlf);
                   UTL_SMTP.write_data (mail_conn,
                                           'Subject: '
                                        || 'Open Purchase Requisitions for '
                                        || v_cur.buyer
                                        || UTL_TCP.crlf
                                       );
                   UTL_SMTP.write_data (mail_conn,
                                        'Content-Type: text/html' || UTL_TCP.crlf
                                       );
                   UTL_SMTP.write_data (mail_conn, UTL_TCP.crlf || message_2);
                   UTL_SMTP.close_data (mail_conn);
                   UTL_SMTP.quit (mail_conn);
                EXCEPTION
                   WHEN OTHERS
                   THEN
                      fnd_file.put_line (fnd_file.LOG,
                                            'Mail Address Not Valid :->   '
                                         || v_cur.e_mail
                                        );
                      retcode := 1;
                END;
             END;
          END LOOP;
       END;

       ----Summary----
       BEGIN
          FOR v_cur_summ IN
             (SELECT   end_date_active, buyer_id, buyer, e_mail, days, days1,
                       days2, days3, SUM (days + days1 + days2 + days3) total
                  FROM (SELECT   end_date_active, buyer_id, buyer, e_mail,
                                 NVL (SUM ("0-7 DAYS"), 0) days,
                                 NVL (SUM ("8-15 DAYS"), 0) days1,
                                 NVL (SUM ("16-30 DAYS"), 0) days2,
                                 NVL (SUM ("> 30 DAYS"), 0) days3
                            FROM (SELECT DISTINCT pa.end_date_active,
                                                  pol.suggested_buyer_id buyer_id,
                                                     ppf.first_name
                                                  || ' '
                                                  || ppf.last_name buyer,
                                                  ppf.email_address e_mail,
                                                  por.segment1,
                                                  TRUNC
                                                     (por.approved_date
                                                     ) approved_date,
                                                  CASE
                                                     WHEN (  TRUNC (SYSDATE)
                                                           - TRUNC (approved_date)
                                                          ) BETWEEN 0 AND 7
                                                        THEN 1
                                                  END AS "0-7 DAYS",
                                                  CASE
                                                     WHEN (  TRUNC (SYSDATE)
                                                           - TRUNC (approved_date)
                                                          ) BETWEEN 8 AND 15
                                                        THEN 1
                                                  END AS "8-15 DAYS",
                                                  CASE
                                                     WHEN (  TRUNC (SYSDATE)
                                                           - TRUNC (approved_date)
                                                          ) BETWEEN 16 AND 30
                                                        THEN 1
                                                  END AS "16-30 DAYS",
                                                  CASE
                                                     WHEN (  TRUNC (SYSDATE)
                                                           - TRUNC (approved_date)
                                                          ) > 30
                                                        THEN 1
                                                  END AS "> 30 DAYS"
                                             FROM apps.po_requisition_headers_all por,
                                                  apps.po_requisition_lines_all pol,
                                                   apps.po_req_distributions_all prd,
                                                  apps.po_agents pa,
                                                  apps.per_all_people_f ppf
                                            WHERE 1 = 1
                                              AND por.authorization_status =
                                                                        'APPROVED'
                                              AND pa.agent_id =
                                                            pol.suggested_buyer_id
                                              --and pa.end_date_active is null
                                              AND ppf.person_id = pa.agent_id
                                              AND SYSDATE
                                                     BETWEEN ppf.effective_start_date
                                                         AND ppf.effective_end_date
                                              --and pa.END_DATE_ACTIVE i
                                               AND NVL(POL.CLOSED_CODE,'1') <> 'FINALLY CLOSED'
                                            and (POL.CANCEL_FLAG='N' OR POL.CANCEL_FLAG IS NULL)
                                            ---Added by Narasimhulu 29/dec/2011
                                             AND POL.PURCHASING_AGENT_ID IS NULL   
                                             AND POL.LINE_LOCATION_ID IS NULL
                                             ---Added by Narasimhulu 29/dec/2011
                                              AND pol.requisition_header_id =
                                                         por.requisition_header_id
                                               and (POL.CANCEL_FLAG='N' OR POL.CANCEL_FLAG IS NULL)
                                               ---Added by Narasimhulu 29/dec/2011
                                               and pol.REQUISITION_LINE_ID = prd.REQUISITION_LINE_ID
                                               ---Added by Narasimhulu 29/dec/2011
                                              --   and pol.suggested_buyer_id = 5530
                                              ---Added by Narasimhulu 29/dec/2011
                                              AND PRD.DISTRIBUTION_ID NOT IN (
                                            SELECT PDA.REQ_DISTRIBUTION_ID FROM  apps.po_distributions_all pda
                                                WHERE PDA.REQ_DISTRIBUTION_ID IS NOT NULL
                                                    )
                                                    ---Added by Narasimhulu 29/dec/2011
                                            /*  AND por.requisition_header_id NOT IN (
                                                     SELECT DISTINCT por1.requisition_header_id
                                                                FROM apps.po_requisition_headers_all por1,
                                                                     apps.po_requisition_lines_all pol1,
                                                                     apps.po_req_distributions_all pod1,
                                                                     apps.po_distributions_all pd1
                                                               WHERE 1 = 1
                                                                 AND por1.requisition_header_id =
                                                                        pol1.requisition_header_id
                                                                 AND pol1.requisition_line_id =
                                                                        pod1.requisition_line_id
                                                                 AND pd1.req_distribution_id =
                                                                        pod1.distribution_id
                                                                 AND pol1.suggested_buyer_id =
                                                                        pol.suggested_buyer_id
                                                                 AND por1.requisition_header_id =
                                                                        por.requisition_header_id)*/
                                                                        
                                                                        )
                        GROUP BY end_date_active, buyer_id, buyer, e_mail)
              GROUP BY end_date_active,
                       buyer_id,
                       buyer,
                       e_mail,
                       days,
                       days1, 
                       days2,
                       days3
                            -- having sum(days+days1+days2+days3)<>0
             )
          LOOP
             IF v_cur_summ.end_date_active IS NOT NULL
             THEN
                --if v_cur_summ.buyer_id= 5530 then
                v_color := '<font  face=''Calibri'' color=''red''>';
             ELSE
                v_color := '<font face=''Calibri'' color=''black''>';
             END IF;

             message_summ_2 :=
                   message_summ_2
                || '<Tr>'
                || v_color
                || '
                        <td ALIGN=''left''><b>'
                || v_cur_summ.buyer
                || '</b></td>
                        <td ALIGN=''right''><b>'
                || v_cur_summ.days
                || '</b></td>
                        <td ALIGN=''right''><b>'
                || v_cur_summ.days1
                || '</b></td>
                        <td ALIGN=''right''><b>'
                || v_cur_summ.days2
                || '</b></td>
                        <td ALIGN=''right''><b>'
                || v_cur_summ.days3
                || '</b></td>
                        <td ALIGN=''right''><b>'
                || v_cur_summ.total
                || '</b></td>
                        </font>
                        </tr>';
          END LOOP;

          message_summ_1 :=
                '<html>
                        <body> 
                         <font face=''Calibri''>
                        Dear ' || initcap(replace(substr(p_summ_email,0,instr(p_summ_email,'@')-1),'.',' ')) || ','
                        || '<br/>
                         <br/>
                        <b>
                        Below are the Purchase requisitions pending with Buyers as on '
             || TO_CHAR (SYSDATE, 'dd-mm-yyyy')
             || ','
             || '
                        <b> 
                        <br/>
                        <br/>
                        </font>
                         <table Border=''1'' width=''700'' height=''800''>
                         <font face=''Calibri'' color=''white''>
                        <Tr bgcolor=''green'' >
                        <td ALIGN=''left'' color=''white''><b>Buyer</b></td>
                        <td ALIGN=''right''><b>0-7 DAYS</b></td>
                        <td ALIGN=''right''><b>8-15 DAYS</b></td>
                        <td ALIGN=''right''><b>16-30 DAYS</b></td>
                        <td ALIGN=''right''><b>> 30 DAYS</b></td>
                        <td ALIGN=''right''><b> Total</b></td>
                        </font>
                        </tr>';
          message_summ_3 :=
             '</font>
                        </table>
                              <br/>
                                                    <font face=''Calibri'' color=''black'' bold=''true''>
                        This is auto generated mail by the system, Please do not reply or respond                    .
                        </font>
                          <br/>
                          <br/>
                          <font face=''Calibri''  bold=''true''>
                          ***-----The Buyers Highlighted in red color are end dated----**** 
                          </font>
                          <br/>
                          <br/>
                         <font face=''Calibri'' color=''blue'' bold=''true''>
                        <b>Thanks and Regards</b>,
                        <br/>
                        <b>OFIN Support</b>
                        </font>
                        </body>
                        </html>';
          mail_conn := UTL_SMTP.open_connection (mailhost, 25);
          UTL_SMTP.helo (mail_conn, mailhost);
          UTL_SMTP.mail (mail_conn, v_from_mail);
          UTL_SMTP.rcpt (mail_conn, p_summ_email);
                     utl_smtp.rcpt (mail_conn, 'ajaykumar.singh@retail.adityabirla.com');
                     utl_smtp.rcpt (mail_conn, 'balasubramanian.sk@retail.adityabirla.com');
          UTL_SMTP.open_data (mail_conn);
          UTL_SMTP.write_data (mail_conn,
                                  'Date: '
                               || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS')
                               || UTL_TCP.crlf
                              );
          UTL_SMTP.write_data (mail_conn, 'From: ' || v_from_mail || UTL_TCP.crlf);
          UTL_SMTP.write_data (mail_conn,
                                  'To: '
                               || SUBSTR (p_summ_email,
                                          0,
                                          INSTR (p_summ_email, '@') - 1
                                         )
                               || UTL_TCP.crlf
                              );
          --utl_smtp.write_data(mail_conn, 'Sent: '  || TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') || utl_tcp.crlf);
          UTL_SMTP.write_data (mail_conn,
                                  'Subject: '
                               || 'Open Purchase Requisitions with Buyers'
                               || UTL_TCP.crlf
                              );
          UTL_SMTP.write_data (mail_conn,
                               'Content-Type: text/html' || UTL_TCP.crlf
                              );
          UTL_SMTP.write_data (mail_conn,
                                  UTL_TCP.crlf
                               || message_summ_1
                               || message_summ_2
                               || message_summ_3
                              );
          UTL_SMTP.close_data (mail_conn);
          UTL_SMTP.quit (mail_conn);
       EXCEPTION
          WHEN OTHERS
          THEN
             fnd_file.put_line (fnd_file.LOG,
                                'Mail Address is Not Valid :->   ' || p_summ_email
                               );
             retcode := 1;
       END;
END; 
/


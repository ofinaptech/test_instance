CREATE OR REPLACE PACKAGE BODY APPS.xxabrl_prepay_inv_pay_pkg
IS
   PROCEDURE xxabrl_main_pkg (errbuf OUT VARCHAR2, retcode OUT NUMBER)
   IS
   BEGIN
      xxabrl_prepay_inv_prc;
   END;

   PROCEDURE xxabrl_prepay_inv_prc
   IS
   BEGIN
      EXECUTE IMMEDIATE 'truncate table XXABRLIPROC.XXABRL_PREPAY_INV_PAY_STG';

      FOR cur IN
(SELECT povs.vendor_site_code, povs.vendor_site_id, --added on '27-Oct-2015'
       'PREPAYMENT INVOICE' invoice_type,
       REPLACE (api.invoice_num, '^', '//') invoice_num,
                                                        --api.invoice_num,
                                                        api.invoice_date,
       (SELECT TRANSLATE (api.description,
                          'X' || CHR (9) || CHR (10) || CHR (13),
                          'X'
                         )
          FROM DUAL) description,
       apd.dist_code_combination_id ccid, gl.segment3 sbu,
       (SELECT ffvl.description
          FROM apps.fnd_flex_values_vl ffvl
         WHERE ffvl.flex_value_set_id = 1013466
           AND ffvl.flex_value = gl.segment3) sbu_desc,
       gl.segment4 location_code,
       (SELECT ffvl.description
          FROM apps.fnd_flex_values_vl ffvl
         WHERE ffvl.flex_value_set_id = 1013467
           AND ffvl.flex_value = gl.segment4) location_desc,
       gl.segment6 account_code,
       (SELECT ffvl.description
          FROM apps.fnd_flex_values_vl ffvl
         WHERE ffvl.flex_value_set_id = 1013469
           AND ffvl.flex_value = gl.segment6) account_desc,
       api.invoice_currency_code,
       DECODE (api.invoice_type_lookup_code,
               'CREDIT', 0,
               z.amt_val - NVL (api.discount_amount_taken, 0)
              ) dr_val,
       DECODE (api.invoice_type_lookup_code,
               'CREDIT', 0,
               z.amt_val - NVL (api.discount_amount_taken, 0)
              ) cr_val,
       NULL prepay_amt_remaing, TO_CHAR (api.doc_sequence_value) payment_num,
       NULL check_number, NULL check_date, pov.segment1 vendor_num,
       (CASE
           WHEN (SUBSTR (pov.segment1, 1, 1) = '7')
              THEN (SELECT apsa1.attribute14
                      FROM apps.ap_suppliers aps1,
                           apps.ap_supplier_sites_all apsa1
                     WHERE aps1.vendor_id = apsa1.vendor_id
                       AND aps1.segment1 = pov.segment1
                       AND apsa1.vendor_site_id = povs.vendor_site_id)
           ELSE NULL
        END
       ) "ATTRIBUTE_14",
       pov.vendor_name, pov.vendor_type_lookup_code vendor_type,
       apd.po_distribution_id, apil.po_header_id,    
                                                 apil.rcv_transaction_id,       
       api.org_id,                                   
                  hr.NAME operating_unit, aba.batch_name, api.invoice_id,
       (SELECT MAX (accounting_date)
          FROM apps.xxabrl_ap_view_prepays_v apil2
         WHERE apil2.invoice_id = api.invoice_id) pay_accounting_date,
       NULL payment_type_flag, NULL bank_account_name, NULL bank_account_num,
       NULL bank_uti_no, api.invoice_amount, api.amount_paid,
       TRUNC (apd.accounting_date) gl_date, NULL prepay_invoice_id,
       (SELECT NAME
          FROM apps.ap_terms AT, apps.ap_invoices_all ai
         WHERE AT.term_id = ai.terms_id
           AND ai.invoice_id = api.invoice_id) term_name,
       api.pay_group_lookup_code pay_group,
       apil.reference_1 ppi_invoice_number, api.attribute2 grn_number,
       api.attribute3 grn_date, NULL hold_flag
  FROM apps.ap_invoices_all api,
       apps.ap_batches_all aba,
       apps.ap_invoice_lines_all apil,
       apps.ap_invoice_distributions_all apd,
       apps.ap_suppliers pov,
       apps.gl_code_combinations gl,
       --  apps.ap_payment_schedules_all aps,
       apps.hr_operating_units hr,
       apps.ap_supplier_sites_all povs,
       (SELECT   NVL (SUM (apd.amount), 0) amt_val, api.invoice_id
            FROM apps.ap_invoices_all api,
                 apps.ap_invoice_lines_all apil,
                 apps.ap_invoice_distributions_all apd,
                 apps.ap_suppliers pov,
                 apps.ap_supplier_sites_all povs
           WHERE api.invoice_id = apd.invoice_id
             AND apil.invoice_id = api.invoice_id
             AND apil.line_number = apd.invoice_line_number
             AND api.vendor_id = pov.vendor_id
             AND api.invoice_type_lookup_code = 'PREPAYMENT'
             AND api.vendor_site_id = povs.vendor_site_id
        -- AND apd.match_status_flag = 'A'
        -- AND apil.line_type_lookup_code <> 'PREPAY'
        GROUP BY api.invoice_id) z
 WHERE api.invoice_id = z.invoice_id
   AND api.invoice_id = apd.invoice_id
   AND aba.batch_id = api.batch_id
   AND api.org_id = hr.organization_id
   AND gl.code_combination_id = apd.dist_code_combination_id
   AND apil.invoice_id = api.invoice_id
   --and aps.INVOICE_ID=api.INVOICE_ID
   AND apil.line_number = apd.invoice_line_number
   AND apd.ROWID = (SELECT ROWID
                      FROM apps.ap_invoice_distributions_all
                     WHERE ROWNUM = 1 AND invoice_id = apd.invoice_id)
      AND api.vendor_id = pov.vendor_id
      AND api.invoice_type_lookup_code = 'PREPAYMENT'
     --AND apd.match_status_flag = 'A'
     -- and nvl(aps.AMOUNT_REMAINING,0)<>0
      -- and pov.segment1='9700381'
     AND  APD.ACCOUNTING_DATE>= TRUNC(SYSDATE) - 180
     AND (trunc(api.LAST_UPDATE_DATE) >= trunc(sysdate)-1 OR  
                 trunc(apil.LAST_UPDATE_DATE) >= trunc(sysdate)-1  OR  
                 trunc(apd.LAST_UPDATE_DATE) >= trunc(sysdate)-1 ) 
      AND api.vendor_site_id = povs.vendor_site_id
     --AND pov.segment1 = '9700342'
UNION ALL
SELECT  povs.vendor_site_code, povs.vendor_site_id,
       'PREPAYMENT PAYMENT' invoice_type,
       REPLACE (api.invoice_num, '^', '//') invoice_num,
                                                        --api.invoice_num,
                                                        api.invoice_date,       
       --apd.description description,
       (SELECT TRANSLATE (api.description,
                          'X' || CHR (9) || CHR (10) || CHR (13),
                          'X'
                         )
          FROM DUAL) description,
       apd.dist_code_combination_id ccid, gl.segment3,
       (SELECT ffvl.description
          FROM apps.fnd_flex_values_vl ffvl
         WHERE ffvl.flex_value_set_id = 1013466
           AND ffvl.flex_value = gl.segment3) sbu_desc,
       gl.segment4,
       (SELECT ffvl.description
          FROM apps.fnd_flex_values_vl ffvl
         WHERE ffvl.flex_value_set_id = 1013467
           AND ffvl.flex_value = gl.segment4) location_desc,
       gl.segment6,
       (SELECT ffvl.description
          FROM apps.fnd_flex_values_vl ffvl
         WHERE ffvl.flex_value_set_id = 1013469
           AND ffvl.flex_value = gl.segment6) account_desc,
       api.payment_currency_code,
       DECODE (api.invoice_type_lookup_code,
               'CREDIT', DECODE (status_lookup_code, 'VOIDED', 0, 0),
               app.amount
              ) dr_val,
       DECODE (api.invoice_type_lookup_code,
               'CREDIT', DECODE (status_lookup_code,
                                 'VOIDED', app.amount,
                                 ABS (app.amount)
                                ),
               0
              ) cr_val,
       NULL pre_amt_remaing,
       DECODE (api.payment_status_flag,
               'Y', TO_CHAR (apc.doc_sequence_value),
               'P', TO_CHAR (apc.doc_sequence_value),
               TO_CHAR (apc.doc_sequence_value), 'N',
               TO_CHAR (apc.doc_sequence_value)
              ) payment_num,
       DECODE (api.payment_status_flag,
               'Y', TO_CHAR (apc.check_number),
               'P', TO_CHAR (apc.check_number)
              ) check_number,
       apc.check_date, pov.segment1 vendor_num,
       (CASE
           WHEN (SUBSTR (pov.segment1, 1, 1) = '7')
              THEN (SELECT apsa1.attribute14
                      FROM apps.ap_suppliers aps1,
                           apps.ap_supplier_sites_all apsa1
                     WHERE aps1.vendor_id = apsa1.vendor_id
                       AND aps1.segment1 = pov.segment1
                       AND apsa1.vendor_site_id = povs.vendor_site_id)
           ELSE NULL
        END
       ) "ATTRIBUTE_14",
       pov.vendor_name, pov.vendor_type_lookup_code vendor_type,
       apd.po_distribution_id, apil.po_header_id,    
                                                 apil.rcv_transaction_id,       
       api.org_id, hr.NAME operating_unit, aba.batch_name, api.invoice_id,
       app.accounting_date,
       DECODE (apc.payment_type_flag,
               'Q', 'QUICK',
               'M', 'MANUAL',
               'R', 'REFUND'
              ) payment_type,
       apc.bank_account_name, apc.bank_account_num,
       apc.attribute3 bank_uti_no, NULL invoice_amount,
       DECODE (api.invoice_type_lookup_code,
               'PREPAYMENT', api.amount_paid,
               NULL
              ) amount_paid,
       apc.check_date gl_date, NULL prepay_invoice_id,
       (SELECT NAME
          FROM apps.ap_terms AT, apps.ap_invoices_all ai
         WHERE AT.term_id = ai.terms_id
           AND ai.invoice_id = api.invoice_id) term_name,
       api.pay_group_lookup_code pay_group,
       apil.reference_1 ppi_invoice_number, api.attribute2 grn_number,
       api.attribute3 grn_date, NULL hold_flag
  FROM apps.ap_invoices_all api,
       apps.ap_invoice_lines_all apil,
       apps.ap_batches_all aba,
       apps.gl_code_combinations gl,
       apps.ap_invoice_distributions_all apd,
       apps.ap_suppliers pov,
       apps.hr_operating_units hr,
       apps.ap_invoice_payments_all app,
       -- apps.ap_payment_schedules_all aps,
       apps.ap_checks_all apc,
       apps.ap_supplier_sites_all povs
 WHERE api.invoice_id = apd.invoice_id
   AND apil.invoice_id = api.invoice_id
   AND aba.batch_id = api.batch_id
   -- and aps.INVOICE_ID=api.INVOICE_ID
   AND hr.organization_id = api.org_id
   AND gl.code_combination_id = api.accts_pay_code_combination_id
   AND apil.line_number = apd.invoice_line_number
   AND apd.ROWID = (SELECT ROWID
                      FROM apps.ap_invoice_distributions_all
                     WHERE ROWNUM = 1 AND invoice_id = apd.invoice_id)
   AND api.vendor_id = pov.vendor_id
   AND app.invoice_id = api.invoice_id
   AND app.check_id = apc.check_id
   AND apc.status_lookup_code IN
          ('CLEARED', 'NEGOTIABLE', 'VOIDED', 'RECONCILED UNACCOUNTED',
           'RECONCILED', 'CLEARED BUT UNACCOUNTED')
   -- AND apd.match_status_flag = 'A'
   AND api.vendor_site_id = povs.vendor_site_id
   AND api.invoice_type_lookup_code = 'PREPAYMENT'
    AND (trunc(app.LAST_UPDATE_DATE) >= trunc(sysdate)-1   OR trunc(apc.LAST_UPDATE_DATE) >=trunc(sysdate)-1)
       AND  (APD.ACCOUNTING_DATE>= TRUNC(SYSDATE) -180 or apc.check_date>= TRUNC(SYSDATE) -180)
     -- AND pov.segment1 = '9700342'
      )
   
   LOOP
         INSERT INTO XXABRLIPROC.XXABRL_PREPAY_INV_PAY_STG
                     (vendor_site_code, vendor_site_id,
                      invoice_type, invoice_num, invoice_date,
                      description, ccid, sbu, sbu_desc,
                      location_code, location_desc,
                      account_code, account_desc,
                      invoice_currency_code, dr_val, cr_val,
                      prepay_amt_remaing, payment_num,
                      check_number, check_date, vendor_num,
                      attribute_14, vendor_name, vendor_type,
                      po_distribution_id, po_header_id,
                      rcv_transaction_id, org_id,
                      operating_unit, batch_name, invoice_id,
                      pay_accounting_date, payment_type_flag,
                      bank_account_name, bank_account_num,
                      bank_uti_no, invoice_amount, amount_paid,
                      gl_date, prepay_invoice_id, term_name,
                      pay_group, ppi_invoice_number, grn_number,
                      grn_date, hold_flag, process_flag
                     )
              VALUES (cur.vendor_site_code, cur.vendor_site_id,
                      cur.invoice_type, cur.invoice_num, cur.invoice_date,
                      cur.description, cur.ccid, cur.sbu, cur.sbu_desc,
                      cur.location_code, cur.location_desc,
                      cur.account_code, cur.account_desc,
                      cur.invoice_currency_code, cur.dr_val, cur.cr_val,
                      cur.prepay_amt_remaing, cur.payment_num,
                      cur.check_number, cur.check_date, cur.vendor_num,
                      cur.attribute_14, cur.vendor_name, cur.vendor_type,
                      cur.po_distribution_id, cur.po_header_id,
                      cur.rcv_transaction_id, cur.org_id,
                      cur.operating_unit, cur.batch_name, cur.invoice_id,
                      cur.pay_accounting_date, cur.payment_type_flag,
                      cur.bank_account_name, cur.bank_account_num,
                      cur.bank_uti_no, cur.invoice_amount, cur.amount_paid,
                      cur.gl_date, cur.prepay_invoice_id, cur.term_name,
                      cur.pay_group, cur.ppi_invoice_number, cur.grn_number,
                      cur.grn_date, cur.hold_flag, 'Daily'
                     );
        COMMIT;
      END LOOP;
   END;
END; 
/


CREATE OR REPLACE PROCEDURE APPS.xxabrl_recover_tax_prvn_data (
   errbuf           OUT      VARCHAR2,
   retcode          OUT      NUMBER,
   p_from_period    IN       VARCHAR2,
   p_to_period      IN       VARCHAR2,
   p_from_account   IN       VARCHAR2,
   p_to_account     IN       VARCHAR2
)
IS
   v_from_period   VARCHAR2 (20)
                    := NVL (fnd_date.canonical_to_date (p_from_period), NULL);
   v_to_period     VARCHAR2 (20)
                      := NVL (fnd_date.canonical_to_date (p_to_period), NULL);

   CURSOR xx_rec
   IS
      SELECT DISTINCT po.segment1 po_number, po.po_date, rsh.receipt_num,
                      (rt.po_unit_price * rt.quantity) grn_amount,
                      grn_code_comb.grn_ccid,
                      tax_code_comb.entered_cr tax_amount_cr,
                      tax_code_comb.entered_dr tax_amount_dr,
                      tax_code_comb.tax_ccid,
                      (SELECT NAME
                         FROM apps.hr_operating_units
                        WHERE organization_id =
                                            rsh.ship_to_org_id)
                                                              operating_unit,
                      jai_tax.shipment_line_id
                 --     rsh.SHIP_TO_ORG_ID
      FROM            apps.rcv_transactions rt,
                      apps.rcv_shipment_headers rsh,
                      apps.rcv_shipment_lines rsl,
                      (SELECT   SUM (je.tax_amount) tax_amount,
                                je.shipment_line_id, je.tax_id,
                                je.shipment_header_id, b.creation_date
                           FROM apps.jai_rcv_line_taxes je,
                                apps.jai_cmn_taxes_all b
                          WHERE je.tax_id = b.tax_id
                            AND b.tax_type IN
                                   ('Service', 'SERVICE_EDUCATION_CESS',
                                    'SERVICE_SH_EDU_CESS')
                            AND b.mod_cr_percentage = 100
                       GROUP BY je.shipment_line_id,
                                je.shipment_header_id,
                                je.tax_id,
                                b.creation_date) jai_tax,
                      (SELECT kfv.concatenated_segments tax_ccid,
                              jour.shipment_line_id, jour.entered_cr,
                              jour.entered_dr, jour.transaction_date
                         FROM apps.jai_rcv_journal_entries jour,
                              apps.gl_code_combinations_kfv kfv
                        WHERE kfv.code_combination_id =
                                                      jour.code_combination_id
                          --AND kfv.segment6 = '226042'---commented by Govind on 10-jun-16
                          AND kfv.segment6 BETWEEN NVL (p_from_account,
                                                        kfv.segment6
                                                       )
                                               AND NVL (p_to_account,
                                                        kfv.segment6
                                                       )) tax_code_comb,
                      (SELECT kfv.concatenated_segments grn_ccid,
                              rcv_transaction_id
                         FROM apps.rcv_receiving_sub_ledger rsl,
                              apps.gl_code_combinations_kfv kfv
                        WHERE rsl.accounting_line_type = 'Charge'
                          AND rsl.code_combination_id =
                                                       kfv.code_combination_id) grn_code_comb,
                      (SELECT segment1, TRUNC (creation_date) po_date,
                              po_header_id
                         FROM po_headers_all) po
--   jai_rcv_journal_entries jour,gl_code_combinations_kfv  kfv
      WHERE           rt.shipment_header_id = rsh.shipment_header_id
                  AND rt.shipment_line_id = rsl.shipment_line_id
                  AND rsh.shipment_header_id = rsl.shipment_header_id
                  AND rt.shipment_header_id = jai_tax.shipment_header_id
                  AND rt.shipment_line_id = jai_tax.shipment_line_id
                  AND rt.transaction_type = 'DELIVER'
                  AND tax_code_comb.shipment_line_id = rsl.shipment_line_id
                  AND grn_code_comb.rcv_transaction_id = rt.transaction_id
                  AND po.po_header_id = rt.po_header_id
                  -- AND TRUNC (jai_tax.creation_date) BETWEEN '29-jan-14' AND '02-Feb-14'
                  AND TRUNC (tax_code_comb.transaction_date)
                         BETWEEN NVL (v_from_period,
                                      tax_code_comb.transaction_date
                                     )
                             AND NVL (v_to_period,
                                      tax_code_comb.transaction_date
                                     );
--    AND rsh.receipt_num = '9860001583';
BEGIN
   fnd_file.put_line
                    (fnd_file.output,
                     LPAD ('Provision Data Based On Recoverable Service Tax',
                           100
                          )
                    );
   fnd_file.put_line (fnd_file.output,
                      LPAD ('Report Date : ' || SYSDATE, 160));
   fnd_file.put_line (fnd_file.output, 'From Date  ' || v_from_period);
   fnd_file.put_line (fnd_file.output, 'To Date    ' || v_to_period);
   fnd_file.put_line
      (fnd_file.output,
       '-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------'
      );
   fnd_file.put_line (fnd_file.output,
                         LPAD ('Po Number', 14)
                      || LPAD ('Po Date', 14)
                      || LPAD ('Receipt Number', 20)
                      || LPAD ('GRN Amount', 18)
                      || LPAD ('GRN Code Combiantions', 32)
                      || LPAD ('Tax Amount CR', 32)
                      || LPAD ('Tax Amount DR', 21)
                      || LPAD ('Tax Code Code Combiantions', 35)
                      || LPAD ('Operating Unit', 30)
                     );
   fnd_file.put_line (fnd_file.output, '');

   FOR xx_main IN xx_rec
   LOOP
      fnd_file.put_line (fnd_file.output,
                            LPAD (xx_main.po_number, 16)
                         || LPAD (xx_main.po_date, 12)
                         || LPAD (xx_main.receipt_num, 20)
                         || ' '
                         || LPAD (xx_main.grn_amount, 15)
                         || ' '
                         || LPAD (xx_main.grn_ccid, 40)
                         || ' '
                         || LPAD (NVL (xx_main.tax_amount_cr, 0), 20)
                         --|| lpad(nvl(xx_main.tax_amount_cr,0),10)
                         || ' '
                         || LPAD (NVL (xx_main.tax_amount_dr, 0), 20)
                         || ' '
                         || LPAD (xx_main.tax_ccid, 40)
                         || ' '
                         || LPAD (RPAD (xx_main.operating_unit, 30), 40)
                        );
   END LOOP;
END; 
/


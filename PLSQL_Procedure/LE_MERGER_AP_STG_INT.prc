CREATE OR REPLACE PROCEDURE APPS.LE_MERGER_AP_STG_INT (Errbuf OUT VARCHAR2, RetCode OUT NUMBER) IS
    v_ERROR_FLAG   VARCHAR2(1);
    v_errmsg       VARCHAR2(6000);
    v_header_id    NUMBER;
    v_line_id      NUMBER;
    v_errorinvoice BOOLEAN;
    v_errorlines   BOOLEAN;
    v_record_count               NUMBER := 0;
    v_org_id                     NUMBER;
    v_vendor_site_id             NUMBER(15);
    v_vendor_id                  NUMBER(15);
    v_payment_method_lookup_code VARCHAR2(30);

    l_location VARCHAR2(50);

            
    CURSOR cur_inv_hdr IS
      SELECT aps.ROWID,
             aps.INVOICE_NUM,
             aps.INVOICE_TYPE_LOOKUP_CODE,
             aps.INVOICE_DATE,
             aps.VENDOR_NUM,
             aps.TERMS_ID,
             ROUND(aps.INVOICE_AMOUNT, 2) INVOICE_AMOUNT,
             aps.INVOICE_CURRENCY_CODE,
             aps.EXCHANGE_DATE,
             aps.EXCHANGE_RATE_TYPE,
             aps.DESCRIPTION,
             aps.GL_DATE,
             aps.GROUP_ID,
             aps.VENDOR_EMAIL_ADDRESS,
             aps.TERMS_DATE,
             aps.ACCTS_PAY_CODE_CONCATENATED,
             aps.ERROR_FLAG,
             aps.ERROR_MSG,
             APS.VENDOR_SITE_ID,
             APS.ORG_ID,
             APS.VENDOR_ID,
             aps.payment_method_lookup_code,
             aps.SOURCE,
             aps.GOODS_RECEIVED_DATE -- New coloumn added in the  xxabrl.XXABRL_AP_HEADER_STAGE
        FROM apps.XXABRL_AP_HEADER_STAGE aps
    WHERE 1=1
    AND TRUNC(APS.GL_DATE)               = '15-DEC-14'
    AND NVL(APS.ERROR_FLAG, 'N')         = 'V';


    CURSOR cur_inv_line(p_invoice_num VARCHAR2, p_vendor_num NUMBER) IS
    SELECT als.ROWID,
             als.VENDOR_NUM,
             als.VENDOR_SITE_ID,
             als.INVOICE_NUM,
             als.LINE_NUMBER,
             als.LINE_TYPE_LOOKUP_CODE,
             ROUND(als.AMOUNT, 2) AMOUNT,
             als.ACCOUNTING_DATE,
             als.DIST_CODE_CONCATENATED,
             als.DESCRIPTION,
             als.TDS_TAX_NAME,
             als.WCT_TAX_NAME,
             als.SERVICE_TAX,
             als.VAT,
             als.ERROR_FLAG,
             als.ERROR_MSG
        FROM apps.XXABRL_AP_LINE_STAGE als
       WHERE NVL(ERROR_FLAG, 'N') = 'V'
         AND als.invoice_num = p_invoice_num
            ---AND      als.vendor_site_id = p_vendor_site_id
         AND als.vendor_num = p_vendor_num;

  BEGIN


    FOR rec_inv_hdr IN cur_inv_hdr LOOP
      EXIT WHEN cur_inv_hdr%NOTFOUND;
      v_header_id    := NULL;
      v_errorinvoice := NULL;
      v_errmsg       := NULL;
      v_record_count := cur_inv_hdr%ROWCOUNT;

      SELECT ap_invoices_interface_s.NEXTVAL INTO v_header_id FROM dual;

      BEGIN

      FND_FILE.PUT_LINE(FND_FILE.LOG,'INSERTING RECORD FOR  -> '||rec_inv_hdr.INVOICE_NUM||'  '||rec_inv_hdr.ORG_ID);

        INSERT INTO AP_INVOICES_INTERFACE
          (INVOICE_ID,
           INVOICE_NUM,
           INVOICE_TYPE_LOOKUP_CODE,
           INVOICE_DATE,
           VENDOR_ID,
           VENDOR_SITE_ID,
           INVOICE_AMOUNT,
           INVOICE_CURRENCY_CODE,
           --EXCHANGE_RATE,
           EXCHANGE_RATE_TYPE,
           EXCHANGE_DATE,
           TERMS_ID,
           DESCRIPTION,
           --TERMS_NAME,
           LAST_UPDATE_DATE,
           LAST_UPDATED_BY,
           LAST_UPDATE_LOGIN,
           CREATION_DATE,
           CREATED_BY,
           --GROUP_ID ,
           GL_DATE,
           ORG_ID,
           VENDOR_EMAIL_ADDRESS,
           SOURCE,
           TERMS_DATE,
           PAYMENT_METHOD_CODE,
           GOODS_RECEIVED_DATE, -- added by shailesh on 25 oct 2008
           ACCTS_PAY_CODE_CONCATENATED)
        VALUES
          (V_HEADER_ID,
           rec_inv_hdr.INVOICE_NUM,
           rec_inv_hdr.INVOICE_TYPE_LOOKUP_CODE,
           rec_inv_hdr.INVOICE_DATE,
           rec_inv_hdr.vendor_id,
           Rec_inv_hdr.VENDOR_SITE_ID,         -- CHANGE BY SHAILESH ON 25 oct2008 ORIGINAL IS  v_vendor_site_id
           rec_inv_hdr.INVOICE_AMOUNT,
           rec_inv_hdr.INVOICE_CURRENCY_CODE,
           --rec_inv_hdr.EXCHANGE_RATE,
           rec_inv_hdr.EXCHANGE_RATE_TYPE,
           rec_inv_hdr.EXCHANGE_DATE,
           rec_inv_hdr.TERMS_ID,
           rec_inv_hdr.DESCRIPTION,
           --rec_inv_hdr.TERMS_NAME,
           SYSDATE,
           3,
           3,
           SYSDATE,
           3,
           --rec_inv_hdr.GROUP_ID,
           rec_inv_hdr.GL_DATE,
           rec_inv_hdr.ORG_ID,
           rec_inv_hdr.VENDOR_EMAIL_ADDRESS,
           rec_inv_hdr.source,
           rec_inv_hdr.TERMS_DATE,
          -- v_payment_method_lookup_code,
           rec_inv_hdr.PAYMENT_METHOD_LOOKUP_CODE, -- above stmt is changed by shailesh on 20th june 2009
           rec_inv_hdr.GOODS_RECEIVED_DATE, -- added by shailesh on 25 oct 2008
           rec_inv_hdr.ACCTS_PAY_CODE_CONCATENATED);
        COMMIT;
        v_errorinvoice := FALSE;
      EXCEPTION
        WHEN OTHERS THEN
          v_errorinvoice := TRUE;
          v_errmsg       := 'AFTER INSERT INTO AP INVOICES INTERFACE FAILED IN EXCEPTION' ||
                            SUBSTR(SQLERRM, 1, 150);
          FND_FILE.PUT_LINE(FND_FILE.OUTPUT, v_errmsg);
      END;
      IF v_errorinvoice = TRUE THEN
        UPDATE apps.xxabrl_ap_header_stage
           SET ERROR_FLAG = 'E', ERROR_MSG = v_errmsg
         WHERE ROWID = rec_inv_hdr.ROWID;
        --and CURRENT OF cur_inv_hdr;

        UPDATE apps.xxabrl_ap_line_stage
           SET ERROR_FLAG = 'E',
               ERROR_MSG  = 'Header Inserting failed' || v_errmsg
         WHERE invoice_num = rec_inv_hdr.invoice_num;
        --and CURRENT OF cur_inv_hdr;
      ELSE
        UPDATE apps.xxabrl_ap_header_stage
           SET ERROR_FLAG = 'P',
               ERROR_MSG  = 'SUCCESSFULLY INSERTED IN HEADER INTERFACE TABLE'
         WHERE ROWID = rec_inv_hdr.ROWID;
        --and CURRENT OF cur_inv_hdr;
      END IF;

      FOR rec_inv_line IN cur_inv_line(rec_inv_hdr.invoice_num
                                       --,rec_inv_hdr.vendor_site_id
                                      ,
                                       rec_inv_hdr.vendor_num) LOOP
        EXIT WHEN cur_inv_line%NOTFOUND;
        v_errorinvoice := FALSE;
        v_errmsg       := NULL;
        v_line_id      := NULL;

        SELECT ap_invoice_lines_interface_s.NEXTVAL
          INTO v_line_id
          FROM dual;
        BEGIN

          
          BEGIN

            SELECT ffvv.flex_value --,ffvv.description
              INTO l_location
              FROM FND_FLEX_VALUES_VL FFVV, FND_FLEX_VALUE_SETS FFVS
             WHERE FLEX_VALUE_SET_NAME = 'ABRL_GL_Location'
               AND ffvs.FLEX_VALUE_SET_ID = ffvv.FLEX_VALUE_SET_ID
               AND ffvv.FLEX_VALUE = SUBSTR(rec_inv_line.dist_code_concatenated, 12, 7);

          EXCEPTION
            WHEN OTHERS THEN
              l_location := NULL;
          END;

    
          INSERT INTO AP_INVOICE_LINES_INTERFACE
            (INVOICE_ID,
             INVOICE_LINE_ID,
             LINE_NUMBER,
             LINE_TYPE_LOOKUP_CODE,
             AMOUNT,
             --TAX_CODE,
             DIST_CODE_CONCATENATED,
             DESCRIPTION,
             ACCOUNTING_DATE,
             ORG_ID,
             ATTRIBUTE_CATEGORY,
             ATTRIBUTE1, 
             LAST_UPDATED_BY,
             LAST_UPDATE_DATE,
             LAST_UPDATE_LOGIN,
             CREATED_BY,
             CREATION_DATE)
          VALUES
            (V_HEADER_ID,
             V_LINE_ID, --,rec_inv_line.INVOICE_LINE_ID
             rec_inv_line.LINE_NUMBER,
             rec_inv_line.LINE_TYPE_LOOKUP_CODE,
             rec_inv_line.AMOUNT,
             --,rec_inv_line.TAX_CODE
             rec_inv_line.DIST_CODE_CONCATENATED,
             rec_inv_line.DESCRIPTION,
             rec_inv_line.ACCOUNTING_DATE,
             --v_org_id,
             -- ADDED BY SHAILESH ON 11 MAY 2009  ORG ID IS COMMING FROM HEADER
             rec_inv_hdr.ORG_ID,
             'RETEK PAYABLE INVOICE', --ADDED BY SHAILESH
             l_location -- ADDED BY SHAILESH
            ,
             3,
             SYSDATE,
             3,
             3,
             SYSDATE);
          COMMIT;
          v_errorlines := FALSE;
        EXCEPTION
          WHEN OTHERS THEN
            v_errorlines := TRUE;
            v_errmsg     := 'AFTER INSERT INTO AP INVOICE LINES INTERFACE FAILED IN EXCEPTION' ||
                            SUBSTR(SQLERRM, 1, 150);
            FND_FILE.PUT_LINE(FND_FILE.OUTPUT, v_errmsg);
        END;
        IF v_errorlines = TRUE THEN
          UPDATE apps.xxabrl_ap_line_stage
             SET ERROR_FLAG = 'E', ERROR_MSG = v_errmsg
           WHERE ROWID = rec_inv_line.ROWID;
          --and CURRENT OF cur_inv_line;
        ELSE
          UPDATE apps.xxabrl_ap_line_stage
             SET ERROR_FLAG = 'P',
                 ERROR_MSG  = 'SUCCESSFULLY INSERTED IN LINE INTERFACE TABLE'
           WHERE ROWID = rec_inv_line.ROWID;
          -- and CURRENT OF cur_inv_line;
        END IF;
      END LOOP;

             
    END LOOP;



    COMMIT;

  END LE_MERGER_AP_STG_INT;

---        END OF PROCEDURE INSERT_TRANSACTION 
/


CREATE OR REPLACE PACKAGE APPS.GST_ABRL_customer_mst_pkg AS
 PROCEDURE GST_ABRL_cust_mst_proc(ERRBUFF             OUT VARCHAR2,
                                RETCODE             OUT NUMBER,
                                p_org_id               IN NUMBER,
                                p_classification       IN VARCHAR2,
                                p_cust_num_list      IN VARCHAR2,
                                p_cust_num_from IN VARCHAR2,
                                p_cust_num_to IN VARCHAR2,
                                p_store_day_op_from       IN VARCHAR2,
                                p_store_day_op_to        IN VARCHAR2,
                                p_square_feet_from        IN VARCHAR2,
                                p_square_feet_to        IN VARCHAR2,
                                p_status             IN VARCHAR2,
                                p_city               IN VARCHAR2
                                );


END GST_ABRL_customer_mst_pkg; 
/


CREATE OR REPLACE PACKAGE BODY APPS.xxabrl_supp_bank_pmt_yesbank
AS
   PROCEDURE xxabrl_yesbank_payment_supp (
      errbuf        OUT   VARCHAR2,
      retcode       OUT   VARCHAR2,
      p_from_date         VARCHAR2,
      p_to_date           VARCHAR2
   )
   IS
      --TO SELECT BANK (YESBANK BANK)
      CURSOR bank_cur
      IS
         SELECT bankorgprofile.party_id bank_id,
                bankparty.party_name bank_name
           FROM hz_parties bankparty,
                hz_organization_profiles bankorgprofile,
                hz_code_assignments bankca
          WHERE 1 = 1
            AND bankparty.party_id = bankorgprofile.party_id
            AND bankca.owner_table_name = 'HZ_PARTIES'
            AND bankca.owner_table_id = bankparty.party_id
            AND bankca.class_category = 'BANK_INSTITUTION_TYPE'
            AND bankca.class_code IN ('BANK', 'CLEARINGHOUSE')
            -- AND bankparty.party_id = 13526282
            AND UPPER (bankorgprofile.organization_name) IN ('YES BANK');

      -- TO SELECT ALL PAYMENTS RELATED TO YESBANK
      CURSOR payment_cur (p_bank_id NUMBER)
      IS
         SELECT DECODE
                      (aca.payment_type_flag,
                       'A', (SELECT cb.bank_account_num
                               FROM ce_bank_acct_uses_all cbau,
                                    ce_bank_accounts cb
                              WHERE cbau.bank_acct_use_id =
                                                       aca.ce_bank_acct_use_id
                                AND cbau.bank_account_id = cb.bank_account_id),
                       aca.bank_account_num
                      ) bank_account_num,                      --Payment Batch
                
                --aca.bank_account_num,
                cba.bank_account_type, hou.NAME op_name,
                aca.checkrun_name run_identification, fu.user_name,
                apss.attribute10 pay_location, aca.checkrun_name batch_id,
                cbb.city corp_bank_city, cbb.bank_branch_name corp_bank_brnch,
                aps.segment1 sup_no, aca.amount pay_amount,
                aca.currency_code currency,
                DECODE (aca.payment_method_code,
                        'N', 'N',
                        'RTGS', 'R',
                        'CHECK', 'C',
                        'DD', 'D',
                        'IFT', 'B',
                        'A to A', 'A'
                       ) prod_code,
                NVL (aca.anticipated_value_date, aca.check_date) trx_val_date,
                aca.check_number, aca.check_date pay_run_date,
                aca.check_date pay_inst_date, apss.attribute4 supp_acc_no,   --
                apss.attribute2 supp_bank_name,                              --
                apss.attribute9 supp_bank_acc_type,                          --
                DECODE (aca.payment_method_code,
                        'RTGS', apss.attribute8,
                        apss.attribute7
                       ) supp_ifsc_code,                                     --
                NVL (aps.vendor_name_alt, aps.vendor_name) sup_name,
                aps.vendor_name sup_alt_name,
                REPLACE
                   (REPLACE
                       (REPLACE
                           (REPLACE
                               (REPLACE
                                   (REPLACE
                                       (REPLACE
                                           (REPLACE
                                               (REPLACE
                                                   (REPLACE
                                                       (REPLACE
                                                           (REPLACE
                                                               (REPLACE
                                                                   (REPLACE
                                                                       (REPLACE
                                                                           (REPLACE
                                                                               (REPLACE
                                                                                   (REPLACE
                                                                                       (REPLACE
                                                                                           (REPLACE
                                                                                               (REPLACE
                                                                                                   ((   apss.address_line1
                                                                                                     || ':'
                                                                                                     || apss.address_line2
                                                                                                     || ':'
                                                                                                     || apss.address_line3
                                                                                                     || ':'
                                                                                                     || apss.state
                                                                                                     || ':'
                                                                                                     || apss.zip
                                                                                                    ),
                                                                                                    '~',
                                                                                                    ''
                                                                                                   ),
                                                                                                '!',
                                                                                                ''
                                                                                               ),
                                                                                            '@',
                                                                                            ''
                                                                                           ),
                                                                                        '#',
                                                                                        '+'
                                                                                       ),
                                                                                    '$',
                                                                                    ''
                                                                                   ),
                                                                                '%',
                                                                                ''
                                                                               ),
                                                                            '^',
                                                                            ''
                                                                           ),
                                                                        '&',
                                                                        '+'
                                                                       ),
                                                                    '*',
                                                                    ''
                                                                   ),
                                                                '_',
                                                                ''
                                                               ),
                                                            '=',
                                                            ''
                                                           ),
                                                        '{',
                                                        ''
                                                       ),
                                                    '}',
                                                    ''
                                                   ),
                                                '[',
                                                ''
                                               ),
                                            '\',
                                            '/'
                                           ),
                                        '|',
                                        '/'
                                       ),
                                    ';',
                                    ''
                                   ),
                                '"',
                                ''
                               ),
                            '''',
                            ''
                           ),
                        '<',
                        ''
                       ),
                    '>',
                    ''
                   ) site_address,
                apss.vendor_site_code, apss.zip pin_code,
                   apss.attribute11
                || DECODE (apss.attribute12,
                           NULL, apss.attribute12,
                           ';' || apss.attribute12
                          )
                || DECODE (apss.attribute13,
                           NULL, apss.attribute13,
                           ';' || apss.attribute13
                          ) email_id,
                apss.phone, NULL supp_bank_acc_city,
                apss.attribute3 supp_bank_bran,                              --
                                               aca.check_id, aca.org_id,
                aca.payment_id, aca.doc_sequence_value voucher_no,
                apss.vendor_site_id, cba.bank_id, cba.bank_branch_id,
                aca.ce_bank_acct_use_id, aca.party_id, aps.vendor_id,
                aca.created_by, aca.creation_date, aca.last_update_date,
                aca.last_updated_by
           FROM ap_checks_all aca,
                ap_suppliers aps,
                ap_supplier_sites_all apss,
                iby_payments_all ip,
                ce_bank_accounts cba,
                hr_operating_units hou,
                fnd_user fu,
                cebv_bank_branches cbb
          WHERE 1 = 1
            AND aca.vendor_id = aps.vendor_id
            AND aps.vendor_id = apss.vendor_id
            AND aca.vendor_site_id = apss.vendor_site_id
            AND aca.payment_id = ip.payment_id
            AND ip.internal_bank_account_id = cba.bank_account_id
            AND ip.payment_status IN ('FORMATTED', 'ISSUED')
            AND aca.org_id = hou.organization_id
            AND aca.void_date IS NULL
            AND aca.created_by = fu.user_id
            AND cba.bank_id = p_bank_id
            AND cba.bank_branch_id = cbb.bank_branch_id
            AND cba.bank_account_id IN (
                   SELECT lookup_code
                     FROM fnd_lookup_values
                    WHERE lookup_type = 'XXABRL_YES_BANK_H2H_ACCOUNT'
                      AND TO_NUMBER (lookup_code) = cba.bank_account_id
                      AND NVL (aca.anticipated_value_date, aca.check_date) >=
                                                             start_date_active)
            AND UPPER (aca.bank_account_name) = UPPER (cba.bank_account_name)
            AND TRUNC (aca.creation_date)
                   BETWEEN TO_DATE (p_from_date, 'YYYY/MM/DD HH24:MI:SS')
                       AND TO_DATE (p_to_date, 'YYYY/MM/DD HH24:MI:SS')
            --Restrict the Duplicate Payments
            AND NOT EXISTS (SELECT check_id
                              FROM yesbank_payment_table dpt
                             WHERE dpt.check_id = aca.check_id);

--TO SELECT INVOICES AGAINST EACH PAYMENT (ONLY FOR YES BANK)
      CURSOR inv_cur (p_check_id NUMBER)
      IS
         SELECT ac.check_id, ai.invoice_num, ai.invoice_date,
                ai.invoice_amount, ai.invoice_type_lookup_code,
                aip.amount amount_paid
           FROM ap_checks_all ac,
                ap_invoice_payments_all aip,
                ap_invoices_all ai
          WHERE 1 = 1
            AND ac.check_id = p_check_id
            AND ac.check_id = aip.check_id
            AND TRUNC (ac.creation_date)
                   BETWEEN TO_DATE (p_from_date, 'YYYY/MM/DD HH24:MI:SS')
                       AND TO_DATE (p_to_date, 'YYYY/MM/DD HH24:MI:SS')
            AND aip.invoice_id = ai.invoice_id;

      --local Variable Declaration
      p_bank_id         NUMBER;
      p_check_id        NUMBER;
      g_ever_failed     BOOLEAN         := FALSE;
      l_msg             VARCHAR2 (2000);
      l_location_name   VARCHAR2 (240);
      l_prefix          VARCHAR2 (3);
   BEGIN
      errbuf := '';
      retcode := 0;
      fnd_file.put_line (fnd_file.LOG,
                         'Procedure XXABRL_YESBANK_PAYMENT_SUPP Started'
                        );

      FOR v_bank IN bank_cur
      LOOP
         FOR v_payment IN payment_cur (v_bank.bank_id)
         LOOP
            l_msg := NULL;
            fnd_file.put_line (fnd_file.LOG,
                               '  Processing Check ID.. '
                               || v_payment.check_id
                              );
            -- Each time we loop through items
            -- g_ever_failed flag is set to FALSE
            g_ever_failed := FALSE;

            IF v_bank.bank_name IS NULL
            THEN
               g_ever_failed := TRUE;
               l_msg :=
                      'Bank Name is Null' || ',' || 'Bank Name is Mandatory ';
               fnd_file.put_line (fnd_file.LOG,
                                  l_msg || '-' || v_payment.check_number
                                 );
            END IF;

            IF v_payment.bank_account_num IS NULL
            THEN
               g_ever_failed := TRUE;
               l_msg :=
                     'Bank Account Number is Null'
                  || ','
                  || 'Bank Account Number is Mandatory ';
               fnd_file.put_line (fnd_file.LOG,
                                  l_msg || '-' || v_payment.check_number
                                 );
            END IF;

            IF v_payment.op_name IS NULL
            THEN
               g_ever_failed := TRUE;
               l_msg :=
                     'Operating Unit is Null'
                  || ','
                  || 'Operating Unit is Mandatory ';
               fnd_file.put_line (fnd_file.LOG,
                                  l_msg || '-' || v_payment.check_number
                                 );
            END IF;

            IF v_payment.bank_account_type IS NULL
            THEN
               g_ever_failed := TRUE;
               l_msg :=
                     'Corporate Bank Account Type is Null'
                  || ','
                  || 'Corporate Bank Account Type is Mandatory ';
               fnd_file.put_line (fnd_file.LOG,
                                  l_msg || '-' || v_payment.check_number
                                 );
            END IF;

            IF v_payment.sup_name IS NULL
            THEN
               g_ever_failed := TRUE;
               l_msg :=
                     'Supplier Name is Null'
                  || ','
                  || 'Supplier Name is Mandatory ';
               fnd_file.put_line (fnd_file.LOG,
                                  l_msg || '-' || v_payment.check_number
                                 );
            END IF;

            IF v_payment.sup_no IS NULL
            THEN
               g_ever_failed := TRUE;
               l_msg :=
                     'Supplier Number is Null'
                  || ','
                  || 'Supplier Number is Mandatory ';
               fnd_file.put_line (fnd_file.LOG,
                                  l_msg || '-' || v_payment.check_number
                                 );
            END IF;

            IF v_payment.vendor_site_code IS NULL
            THEN
               g_ever_failed := TRUE;
               l_msg :=
                     'Supplier Site Code is Null'
                  || ','
                  || 'Supplier Site Code is Mandatory ';
               fnd_file.put_line (fnd_file.LOG,
                                  l_msg || '-' || v_payment.check_number
                                 );
            END IF;

            IF v_payment.site_address IS NULL
            THEN
               g_ever_failed := TRUE;
               l_msg :=
                     'Supplier Site Address is Null'
                  || ','
                  || 'Supplier Site Address is Mandatory ';
               fnd_file.put_line (fnd_file.LOG,
                                  l_msg || '-' || v_payment.check_number
                                 );
            END IF;

            IF     v_payment.prod_code IN ('R', 'N')
               AND v_payment.supp_bank_name IS NULL
            THEN
               g_ever_failed := TRUE;
               l_msg :=
                     'Supplier Bank Name is Null'
                  || ','
                  || 'Supplier Bank Name is Mandatory ';
               fnd_file.put_line (fnd_file.LOG,
                                  l_msg || '-' || v_payment.check_number
                                 );
            END IF;

            -- Condition for RTGS and NEFT
            IF v_payment.prod_code = 'R' AND (v_payment.pay_amount) < 200000
            THEN
               g_ever_failed := TRUE;
               l_msg :=
                     'Payment Amount is less than 2 Lakh.'
                  || ','
                  || 'For RTGS Payments Amount Should be morethan or equal to 2 Lakh.';
               fnd_file.put_line (fnd_file.LOG,
                                  l_msg || '-' || v_payment.check_number
                                 );
            END IF;

            IF v_payment.check_number IS NULL
            THEN
               g_ever_failed := TRUE;
               l_msg :=
                     'Supplier Check Number is Null'
                  || ','
                  || 'Supplier Check Number is Mandatory ';
               fnd_file.put_line (fnd_file.LOG,
                                  l_msg || '-' || v_payment.check_number
                                 );
            END IF;

            -- Condition for RTGS and NEFT
            IF     v_payment.prod_code IN ('R', 'N')
               AND (   v_payment.supp_ifsc_code IS NULL
                    OR LENGTH (v_payment.supp_ifsc_code) <> 11
                   )
            THEN
               g_ever_failed := TRUE;
               l_msg :=
                     'Supplier IFSC Code is Invalid/Null'
                  || ','
                  || 'Supplier IFSC Code is Mandatory for RTGS and NEFT';
               fnd_file.put_line (fnd_file.LOG,
                                  l_msg || '-' || v_payment.check_number
                                 );
            END IF;

            --Condition for RTGS and NEFT ,IFT
            IF     v_payment.prod_code IN ('R', 'N', 'B')
               AND v_payment.supp_acc_no IS NULL
            THEN
               g_ever_failed := TRUE;
               l_msg :=
                     'Supplier Account No is Null'
                  || ','
                  || 'Supplier Account No is Mandatory ';
               fnd_file.put_line (fnd_file.LOG,
                                  l_msg || '-' || v_payment.check_number
                                 );
            END IF;

            --Condition for RTGS and NEFT
            IF     v_payment.prod_code IN ('R', 'N')
               AND v_payment.supp_bank_acc_type IS NULL
            THEN
               g_ever_failed := TRUE;
               l_msg :=
                     'Supplier Account Type is Null'
                  || ','
                  || 'Supplier Account Type is Mandatory ';
               fnd_file.put_line (fnd_file.LOG,
                                  l_msg || '-' || v_payment.check_number
                                 );
            END IF;

            IF v_payment.pay_amount IS NULL
            THEN
               g_ever_failed := TRUE;
               l_msg :=
                     'Payment Amount is Null'
                  || ','
                  || 'Payment Amount is Mandatory ';
               fnd_file.put_line (fnd_file.LOG,
                                  l_msg || '-' || v_payment.check_number
                                 );
            END IF;

            IF v_payment.prod_code IS NULL
            THEN
               g_ever_failed := TRUE;
               l_msg :=
                     'Payment Method is Null'
                  || ','
                  || 'Payment Method is Mandatory ';
               fnd_file.put_line (fnd_file.LOG,
                                  l_msg || '-' || v_payment.check_number
                                 );
            END IF;

            IF v_payment.pay_run_date IS NULL
            THEN
               g_ever_failed := TRUE;
               l_msg :=
                  'Payment Date is Null' || ','
                  || 'Payment Date is Mandatory ';
               fnd_file.put_line (fnd_file.LOG,
                                  l_msg || '-' || v_payment.check_number
                                 );
            END IF;

            /* OU Codification*/
            l_location_name := NULL;
            l_prefix := NULL;

            IF v_payment.op_name IS NOT NULL
            THEN
               BEGIN
                  SELECT location_name, prefix_vendor_code
                    INTO l_location_name, l_prefix
                    FROM apps.yes_bank_ou_codification
                   WHERE ou_name = v_payment.op_name;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_location_name := NULL;
                     g_ever_failed := TRUE;
                     l_msg :=
                           'OU Codification is Null'
                        || ','
                        || 'OU Codification  is Mandatory ';
               END;
            END IF;

            IF g_ever_failed = FALSE
            THEN
               INSERT INTO yesbank_payment_table
                           (corporate_account_number, corp_bank_name,
                            corp_acc_type, third_party_id,
                            run_identification,
                            user_id, user_dept,
                            dd_payable_location, profit_centre,
                            batch_id, corp_bank_city,
                            corp_bank_branch,
                            vendor_code,
                            transactional_amount, transactional_currency,
                            product_code, transaction_value_date,
                            pay_doc_number, pay_run_date,
                            instruction_date, benefi_acc_num,
                            benefi_bank_name,
                            benefi_acc_type,
                            benefi_ifsc_code, primary_name,
                            secondary_name, vendor_address,
                            vendor_location, vendor_pincode,
                            primary_email, vendor_mobile, benfi_bank_city,
                            benefi_bank_branch, check_id,
                            transaction_status, updated_in_app,
                            attribute_category, attribute1, attribute2,
                            attribute3, attribute4, attribute5,
                            creation_date, created_by,
                            last_update_date,
                            last_updated_by
                           )
                    VALUES (v_payment.bank_account_num, v_bank.bank_name,
                            v_payment.bank_account_type, l_location_name,
                            v_payment.run_identification,
                            v_payment.user_name, NULL,
                            v_payment.pay_location, NULL,
                            v_payment.batch_id, v_payment.corp_bank_city,
                            v_payment.corp_bank_brnch,
                            l_prefix || '' || v_payment.sup_no,
                            v_payment.pay_amount, v_payment.currency,
                            v_payment.prod_code, v_payment.trx_val_date,
                            v_payment.check_number, v_payment.pay_run_date,
                            v_payment.pay_inst_date, v_payment.supp_acc_no,
                            v_payment.supp_bank_name,
                            v_payment.supp_bank_acc_type,
                            v_payment.supp_ifsc_code, v_payment.sup_name,
                            v_payment.sup_alt_name, v_payment.site_address,
                            v_payment.vendor_site_code, v_payment.pin_code,
                            v_payment.email_id, v_payment.phone, NULL,
                            --Supplier Bank City
                            v_payment.supp_bank_bran, v_payment.check_id,
                            'AUTHORIZED', 'N',
                            NULL,                       -- YESBANK Payment DFF
                                 NULL,                          -- Status Code
                                      NULL,               --Status Description
                            NULL,                                 --UTR Number
                                 NULL,                          --check Number
                                      NULL,                --Bank Reference No
                            v_payment.creation_date, v_payment.created_by,
                            v_payment.last_update_date,
                            v_payment.last_updated_by
                           );

               FOR v_inv IN inv_cur (v_payment.check_id)
               LOOP
                  IF v_inv.invoice_num IS NULL
                  THEN
                     g_ever_failed := TRUE;
                     l_msg :=
                           'Invoice Number is Null'
                        || ','
                        || 'Invoice Number is Mandatory ';
                     fnd_file.put_line (fnd_file.LOG,
                                        l_msg || '-' || v_payment.check_number
                                       );
                  END IF;

                  IF v_inv.invoice_amount IS NULL
                  THEN
                     g_ever_failed := TRUE;
                     l_msg :=
                           'Invoice Amount is Null'
                        || ','
                        || 'Invoice Amount is Mandatory ';
                     fnd_file.put_line (fnd_file.LOG,
                                        l_msg || '-' || v_payment.check_number
                                       );
                  END IF;

                  INSERT INTO yesbank_payment_invoice
                              (check_id, invoice_number,
                               invoice_date,
                               pay_type_code,
                               invoice_amount,
                               net_amount
                              )
                       VALUES (v_inv.check_id, v_inv.invoice_num,
                               v_inv.invoice_date,
                               v_inv.invoice_type_lookup_code,
                               v_inv.invoice_amount,
                               TO_CHAR (v_inv.amount_paid)
                              );
               END LOOP;

               COMMIT;
            END IF;
         END LOOP;
      END LOOP;

      COMMIT;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         l_msg := 'No Records Processed';
      WHEN OTHERS
      THEN
         errbuf := 'Other Exceptions ' || SQLERRM;
         retcode := '1';
   END xxabrl_yesbank_payment_supp;

   PROCEDURE xxabrl_update_yesbank_info (
      errbuf    OUT   VARCHAR2,
      retcode   OUT   VARCHAR2
   )
   IS
      /*
          This Procedure will Update the Attribute Status Once YES Bank gives response file
      */  -- To get status from the YES bank
      CURSOR yesbank_rev_cur
      IS
         SELECT dpt.check_id, dpt.pay_doc_number, dpt.attribute_category,
                dpt.attribute1, dpt.attribute2, dpt.attribute3,
                dpt.attribute4, dpt.attribute5, dpt.transaction_status,
                dpt.run_identification, dpt.product_code, dpt.updated_in_app
           FROM apps.yesbank_payment_table dpt
          WHERE dpt.attribute_category IS NOT NULL
            AND dpt.updated_in_app = 'N';

      --AND DPT.check_id = p_check_id;
      l_updated_flag   VARCHAR2 (1) := 'N';
   BEGIN
      l_updated_flag := 'N';

      -- Updating with Statuses whatever bank is provided in Attributes
      FOR upd_chk IN yesbank_rev_cur
      LOOP
         BEGIN
            UPDATE apps.ap_checks_all
               SET attribute_category = upd_chk.attribute_category,
                   attribute1 = upd_chk.attribute1,
                   attribute2 = upd_chk.attribute2
             WHERE check_number = upd_chk.pay_doc_number
               AND check_id = upd_chk.check_id;

            l_updated_flag := 'Y';
         EXCEPTION
            WHEN OTHERS
            THEN
               NULL;
         END;

         IF (upd_chk.attribute1 = 'SUCCESS')
         THEN
            IF upd_chk.product_code = 'R' OR upd_chk.product_code = 'N'
            THEN
               UPDATE apps.ap_checks_all
                  SET attribute_category = upd_chk.attribute_category,
                      attribute3 = upd_chk.attribute3
                WHERE check_number = upd_chk.pay_doc_number
                  AND check_id = upd_chk.check_id;
            ELSIF upd_chk.product_code = 'D' OR upd_chk.product_code = 'C'
            THEN
               UPDATE apps.ap_checks_all
                  SET attribute_category = upd_chk.attribute_category,
                      attribute4 = upd_chk.attribute4
                WHERE check_number = upd_chk.pay_doc_number
                  AND check_id = upd_chk.check_id;
            ELSIF upd_chk.product_code = 'B'
            THEN
               UPDATE apps.ap_checks_all
                  SET attribute_category = upd_chk.attribute_category,
                      attribute5 = upd_chk.attribute5
                WHERE check_number = upd_chk.pay_doc_number
                  AND check_id = upd_chk.check_id;
            END IF;
         END IF;

         IF l_updated_flag = 'Y'
         THEN
            UPDATE apps.yesbank_payment_table dpt
               SET updated_in_app = 'Y'
             WHERE dpt.pay_doc_number = upd_chk.pay_doc_number
               AND dpt.check_id = upd_chk.check_id;
         END IF;
      END LOOP;

      COMMIT;
   END xxabrl_update_yesbank_info;

   PROCEDURE xxabrl_yb_user_rectified_pay (
      errbuf            OUT   VARCHAR2,
      retcode           OUT   VARCHAR2,
      p_from_check_id         NUMBER,
      p_to_check_id           NUMBER
   )
   IS
      -- This Procedure will Update the Rejected Status to AUTHORIZED once Payments are Rectified by User.
      p_bank_id   NUMBER;

      --TO SELECT BANK (YES BANK)
      CURSOR yes_bank_cur
      IS
         SELECT bankorgprofile.party_id bank_id,
                bankparty.party_name bank_name
           FROM hz_parties bankparty,
                hz_organization_profiles bankorgprofile,
                hz_code_assignments bankca
          WHERE 1 = 1
            AND bankparty.party_id = bankorgprofile.party_id
            AND bankca.owner_table_name = 'HZ_PARTIES'
            AND bankca.owner_table_id = bankparty.party_id
            AND bankca.class_category = 'BANK_INSTITUTION_TYPE'
            AND bankca.class_code IN ('BANK', 'CLEARINGHOUSE')
            -- AND bankparty.party_id = 13526282
            AND UPPER (bankorgprofile.organization_name) IN ('YES BANK');

      CURSOR yesbank_rej_cur (p_bank_id NUMBER)
      IS
         SELECT aca.doc_sequence_value voucher_no, aca.check_id,
                aca.check_number, apss.attribute10 pay_location,
                cbb.city corp_bank_city, cbb.bank_branch_name corp_bank_brnch,
                apss.attribute4 supp_acc_no, apss.attribute2 supp_bank_name,
                apss.attribute9 supp_bank_acc_type,
                DECODE (aca.payment_method_code,
                        'RTGS', apss.attribute8,
                        apss.attribute7
                       ) supp_ifsc_code,
                NVL (aps.vendor_name_alt, aps.vendor_name) sup_name,
                aps.vendor_name sup_alt_name,
                   apss.attribute11
                || DECODE (apss.attribute12,
                           NULL, apss.attribute12,
                           ';' || apss.attribute12
                          )
                || DECODE (apss.attribute13,
                           NULL, apss.attribute13,
                           ';' || apss.attribute13
                          ) email_id
           FROM ap_checks_all aca,
                ap_suppliers aps,
                ap_supplier_sites_all apss,
                iby_payments_all ip,
                ce_bank_accounts cba,
                hr_operating_units hou,
                fnd_user fu,
                cebv_bank_branches cbb
          WHERE 1 = 1
            AND aca.vendor_id = aps.vendor_id
            AND aps.vendor_id = apss.vendor_id
            AND aca.vendor_site_id = apss.vendor_site_id
            AND aca.payment_id = ip.payment_id
            AND ip.internal_bank_account_id = cba.bank_account_id
            AND ip.payment_status IN ('FORMATTED', 'ISSUED')
            AND aca.org_id = hou.organization_id
            AND aca.void_date IS NULL
            AND aca.created_by = fu.user_id
            AND cba.bank_id = p_bank_id
            AND cba.bank_branch_id = cbb.bank_branch_id
            AND cba.bank_account_id IN (
                   SELECT lookup_code
                     FROM fnd_lookup_values
                    WHERE lookup_type = 'XXABRL_YES_BANK_H2H_ACCOUNT'
                      AND TO_NUMBER (lookup_code) = cba.bank_account_id
                      AND NVL (aca.anticipated_value_date, aca.check_date) >=
                                                             start_date_active)
            AND UPPER (aca.bank_account_name) = UPPER (cba.bank_account_name)
            AND aca.check_id BETWEEN NVL (p_from_check_id, aca.check_id)
                                 AND NVL (p_to_check_id, aca.check_id);
   BEGIN
      FOR v_yesbank IN yes_bank_cur
      LOOP
         FOR cur_rej_rec IN yesbank_rej_cur (v_yesbank.bank_id)
         LOOP
            UPDATE apps.yesbank_payment_table yesbank_tab
               SET yesbank_tab.transaction_status = 'AUTHORIZED',
                   yesbank_tab.attribute_category = NULL
                                                        --YESBANK Additional Information SET TO NULL
            ,
                   yesbank_tab.attribute1 = NULL                -- Status Code
                                                ,
                   yesbank_tab.attribute2 = NULL          --Status Description
                                                ,
                   yesbank_tab.attribute3 = NULL                  --UTR Number
                                                ,
                   yesbank_tab.attribute4 = NULL                --check Number
                                                ,
                   yesbank_tab.attribute5 = NULL           --Bank Reference No
                                                ,
                   yesbank_tab.benefi_ifsc_code = cur_rej_rec.supp_ifsc_code
                                                                            --NEFT OR RTGS
            ,
                   yesbank_tab.dd_payable_location = cur_rej_rec.pay_location,
                   yesbank_tab.corp_bank_city = cur_rej_rec.corp_bank_city,
                   yesbank_tab.corp_bank_branch = cur_rej_rec.corp_bank_brnch,
                   yesbank_tab.benefi_acc_num = cur_rej_rec.supp_acc_no,
                   yesbank_tab.benefi_bank_name = cur_rej_rec.supp_bank_name,
                   yesbank_tab.benefi_acc_type =
                                                cur_rej_rec.supp_bank_acc_type,
                   yesbank_tab.primary_name = cur_rej_rec.sup_name,
                   yesbank_tab.secondary_name = cur_rej_rec.sup_alt_name,
                   yesbank_tab.primary_email = cur_rej_rec.email_id,
                   yesbank_tab.updated_in_app = 'N'
             WHERE yesbank_tab.attribute1 = 'TRANSACTION REJECTED'
               AND yesbank_tab.check_id = cur_rej_rec.check_id;
         END LOOP;
      END LOOP;

      COMMIT;
   END xxabrl_yb_user_rectified_pay;

   -- New Procedure for Sceduling Purpose.. Except Date Parameter Everything is same.
   PROCEDURE xxabrl_scd_yb_payment_supp (
      errbuf    OUT   VARCHAR2,
      retcode   OUT   VARCHAR2
   )
   IS
      --TO SELECT BANK (YES BANK)
      CURSOR bank_cur
      IS
         SELECT bankorgprofile.party_id bank_id,
                bankparty.party_name bank_name
           FROM hz_parties bankparty,
                hz_organization_profiles bankorgprofile,
                hz_code_assignments bankca
          WHERE 1 = 1
            AND bankparty.party_id = bankorgprofile.party_id
            AND bankca.owner_table_name = 'HZ_PARTIES'
            AND bankca.owner_table_id = bankparty.party_id
            AND bankca.class_category = 'BANK_INSTITUTION_TYPE'
            AND bankca.class_code IN ('BANK', 'CLEARINGHOUSE')
            -- AND bankparty.party_id = 13526282
            AND UPPER (bankorgprofile.organization_name) IN ('YES BANK');

      -- TO SELECT ALL PAYMENTS RELATED TO YESBANK
      CURSOR payment_cur (p_bank_id NUMBER)
      IS
         SELECT DECODE
                      (aca.payment_type_flag,
                       'A', (SELECT cb.bank_account_num
                               FROM ce_bank_acct_uses_all cbau,
                                    ce_bank_accounts cb
                              WHERE cbau.bank_acct_use_id =
                                                       aca.ce_bank_acct_use_id
                                AND cbau.bank_account_id = cb.bank_account_id),
                       aca.bank_account_num
                      ) bank_account_num,                      --Payment Batch
                
                --aca.bank_account_num,
                cba.bank_account_type, hou.NAME op_name,
                aca.checkrun_name run_identification, fu.user_name,
                apss.attribute10 pay_location,                               --
                                              aca.checkrun_name batch_id,
                cbb.city corp_bank_city, cbb.bank_branch_name corp_bank_brnch,
                aps.segment1 sup_no, aca.amount pay_amount,
                aca.currency_code currency,
                DECODE (aca.payment_method_code,
                        'N', 'N',
                        'RTGS', 'R',
                        'CHECK', 'C',
                        'DD', 'D',
                        'IFT', 'B'
                       ) prod_code,
                NVL (aca.anticipated_value_date, aca.check_date) trx_val_date,
                aca.check_number, aca.check_date pay_run_date,
                aca.check_date pay_inst_date, apss.attribute4 supp_acc_no,   --
                apss.attribute2 supp_bank_name,                              --
                apss.attribute9 supp_bank_acc_type,                          --
                DECODE (aca.payment_method_code,
                        'RTGS', apss.attribute8,
                        apss.attribute7
                       ) supp_ifsc_code,                                     --
                NVL (aps.vendor_name_alt, aps.vendor_name) sup_name,
                aps.vendor_name sup_alt_name,
                REPLACE
                   (REPLACE
                       (REPLACE
                           (REPLACE
                               (REPLACE
                                   (REPLACE
                                       (REPLACE
                                           (REPLACE
                                               (REPLACE
                                                   (REPLACE
                                                       (REPLACE
                                                           (REPLACE
                                                               (REPLACE
                                                                   (REPLACE
                                                                       (REPLACE
                                                                           (REPLACE
                                                                               (REPLACE
                                                                                   (REPLACE
                                                                                       (REPLACE
                                                                                           (REPLACE
                                                                                               (REPLACE
                                                                                                   ((   apss.address_line1
                                                                                                     || ':'
                                                                                                     || apss.address_line2
                                                                                                     || ':'
                                                                                                     || apss.address_line3
                                                                                                     || ':'
                                                                                                     || apss.state
                                                                                                     || ':'
                                                                                                     || apss.zip
                                                                                                    ),
                                                                                                    '~',
                                                                                                    ''
                                                                                                   ),
                                                                                                '!',
                                                                                                ''
                                                                                               ),
                                                                                            '@',
                                                                                            ''
                                                                                           ),
                                                                                        '#',
                                                                                        '+'
                                                                                       ),
                                                                                    '$',
                                                                                    ''
                                                                                   ),
                                                                                '%',
                                                                                ''
                                                                               ),
                                                                            '^',
                                                                            ''
                                                                           ),
                                                                        '&',
                                                                        '+'
                                                                       ),
                                                                    '*',
                                                                    ''
                                                                   ),
                                                                '_',
                                                                ''
                                                               ),
                                                            '=',
                                                            ''
                                                           ),
                                                        '{',
                                                        ''
                                                       ),
                                                    '}',
                                                    ''
                                                   ),
                                                '[',
                                                ''
                                               ),
                                            '\',
                                            '/'
                                           ),
                                        '|',
                                        '/'
                                       ),
                                    ';',
                                    ''
                                   ),
                                '"',
                                ''
                               ),
                            '''',
                            ''
                           ),
                        '<',
                        ''
                       ),
                    '>',
                    ''
                   ) site_address,
                apss.vendor_site_code, apss.zip pin_code,
                   apss.attribute11
                || DECODE (apss.attribute12,
                           NULL, apss.attribute12,
                           ';' || apss.attribute12
                          )
                || DECODE (apss.attribute13,
                           NULL, apss.attribute13,
                           ';' || apss.attribute13
                          ) email_id,
                apss.phone, NULL supp_bank_acc_city,
                apss.attribute3 supp_bank_bran,                              --
                                               aca.check_id, aca.org_id,
                aca.payment_id, aca.doc_sequence_value voucher_no,
                apss.vendor_site_id, cba.bank_id, cba.bank_branch_id,
                aca.ce_bank_acct_use_id, aca.party_id, aps.vendor_id,
                aca.created_by, aca.creation_date, aca.last_update_date,
                aca.last_updated_by
           FROM ap_checks_all aca,
                ap_suppliers aps,
                ap_supplier_sites_all apss,
                iby_payments_all ip,
                ce_bank_accounts cba,
                hr_operating_units hou,
                fnd_user fu,
                cebv_bank_branches cbb
          WHERE 1 = 1
            AND aca.vendor_id = aps.vendor_id
            AND aps.vendor_id = apss.vendor_id
            AND aca.vendor_site_id = apss.vendor_site_id
            AND aca.payment_id = ip.payment_id
            AND ip.internal_bank_account_id = cba.bank_account_id
            AND ip.payment_status IN ('FORMATTED', 'ISSUED')
            AND aca.org_id = hou.organization_id
            AND aca.void_date IS NULL
            AND aca.created_by = fu.user_id
            AND cba.bank_id = p_bank_id
            AND cba.bank_branch_id = cbb.bank_branch_id
            AND cba.bank_account_id IN (
                   SELECT lookup_code
                     FROM fnd_lookup_values
                    WHERE lookup_type = 'XXABRL_YES_BANK_H2H_ACCOUNT'
                      AND TO_NUMBER (lookup_code) = cba.bank_account_id
                      AND NVL (aca.anticipated_value_date, aca.check_date) >=
                                                             start_date_active)
            AND UPPER (aca.bank_account_name) = UPPER (cba.bank_account_name)
            AND TRUNC (aca.creation_date) = TRUNC (SYSDATE)
            --Restrict the Duplicate Payments
            AND NOT EXISTS (SELECT check_id
                              FROM yesbank_payment_table dpt
                             WHERE dpt.check_id = aca.check_id);

--TO SELECT INVOICES AGAINST EACH PAYMENT (ONLY FOR YES BANK)
      CURSOR inv_cur (p_check_id NUMBER)
      IS
         SELECT ac.check_id, ai.invoice_num, ai.invoice_date,
                ai.invoice_amount, ai.invoice_type_lookup_code,
                aip.amount amount_paid
           FROM ap_checks_all ac,
                ap_invoice_payments_all aip,
                ap_invoices_all ai
          WHERE 1 = 1
            AND ac.check_id = p_check_id
            AND ac.check_id = aip.check_id
            AND TRUNC (ac.creation_date) = TRUNC (SYSDATE)
            AND aip.invoice_id = ai.invoice_id;

      --local Variable Declaration
      p_bank_id         NUMBER;
      p_check_id        NUMBER;
      g_ever_failed     BOOLEAN         := FALSE;
      l_msg             VARCHAR2 (2000);
      l_location_name   VARCHAR2 (240);
      l_prefix          VARCHAR2 (3);
   BEGIN
      errbuf := '';
      retcode := 0;
      fnd_file.put_line (fnd_file.LOG,
                         'Procedure XXABRL_YESBANK_PAYMENT_SUPP Started'
                        );

      FOR v_bank IN bank_cur
      LOOP
         FOR v_payment IN payment_cur (v_bank.bank_id)
         LOOP
            l_msg := NULL;
            fnd_file.put_line (fnd_file.LOG,
                               '  Processing Check ID.. '
                               || v_payment.check_id
                              );
            -- Each time we loop through items
            -- g_ever_failed flag is set to FALSE
            g_ever_failed := FALSE;

            IF v_bank.bank_name IS NULL
            THEN
               g_ever_failed := TRUE;
               l_msg :=
                      'Bank Name is Null' || ',' || 'Bank Name is Mandatory ';
               fnd_file.put_line (fnd_file.LOG,
                                  l_msg || '-' || v_payment.check_number
                                 );
            END IF;

            IF v_payment.bank_account_num IS NULL
            THEN
               g_ever_failed := TRUE;
               l_msg :=
                     'Bank Account Number is Null'
                  || ','
                  || 'Bank Account Number is Mandatory ';
               fnd_file.put_line (fnd_file.LOG,
                                  l_msg || '-' || v_payment.check_number
                                 );
            END IF;

            IF v_payment.op_name IS NULL
            THEN
               g_ever_failed := TRUE;
               l_msg :=
                     'Operating Unit is Null'
                  || ','
                  || 'Operating Unit is Mandatory ';
               fnd_file.put_line (fnd_file.LOG,
                                  l_msg || '-' || v_payment.check_number
                                 );
            END IF;

            IF v_payment.bank_account_type IS NULL
            THEN
               g_ever_failed := TRUE;
               l_msg :=
                     'Corporate Bank Account Type is Null'
                  || ','
                  || 'Corporate Bank Account Type is Mandatory ';
               fnd_file.put_line (fnd_file.LOG,
                                  l_msg || '-' || v_payment.check_number
                                 );
            END IF;

            IF v_payment.sup_name IS NULL
            THEN
               g_ever_failed := TRUE;
               l_msg :=
                     'Supplier Name is Null'
                  || ','
                  || 'Supplier Name is Mandatory ';
               fnd_file.put_line (fnd_file.LOG,
                                  l_msg || '-' || v_payment.check_number
                                 );
            END IF;

            IF v_payment.sup_no IS NULL
            THEN
               g_ever_failed := TRUE;
               l_msg :=
                     'Supplier Number is Null'
                  || ','
                  || 'Supplier Number is Mandatory ';
               fnd_file.put_line (fnd_file.LOG,
                                  l_msg || '-' || v_payment.check_number
                                 );
            END IF;

            IF v_payment.vendor_site_code IS NULL
            THEN
               g_ever_failed := TRUE;
               l_msg :=
                     'Supplier Site Code is Null'
                  || ','
                  || 'Supplier Site Code is Mandatory ';
               fnd_file.put_line (fnd_file.LOG,
                                  l_msg || '-' || v_payment.check_number
                                 );
            END IF;

            IF v_payment.site_address IS NULL
            THEN
               g_ever_failed := TRUE;
               l_msg :=
                     'Supplier Site Address is Null'
                  || ','
                  || 'Supplier Site Address is Mandatory ';
               fnd_file.put_line (fnd_file.LOG,
                                  l_msg || '-' || v_payment.check_number
                                 );
            END IF;

            IF     v_payment.prod_code IN ('R', 'N')
               AND v_payment.supp_bank_name IS NULL
            THEN
               g_ever_failed := TRUE;
               l_msg :=
                     'Supplier Bank Name is Null'
                  || ','
                  || 'Supplier Bank Name is Mandatory ';       --||'-'||l_msg;
               fnd_file.put_line (fnd_file.LOG,
                                  l_msg || '-' || v_payment.check_number
                                 );
            END IF;

            --Condition for RTGS and NEFT
            IF v_payment.prod_code = 'R' AND (v_payment.pay_amount) < 200000
            THEN
               g_ever_failed := TRUE;
               l_msg :=
                     'Payment Amount is less than 2 Lakh.'
                  || ','
                  || 'For RTGS Payments Amount Should be morethan or equal to 2 Lakh.';
               --||'-'||l_msg;
               fnd_file.put_line (fnd_file.LOG,
                                  l_msg || '-' || v_payment.check_number
                                 );
            END IF;

            IF v_payment.check_number IS NULL
            THEN
               g_ever_failed := TRUE;
               l_msg :=
                     'Supplier Check Number is Null'
                  || ','
                  || 'Supplier Check Number is Mandatory ';
               fnd_file.put_line (fnd_file.LOG,
                                  l_msg || '-' || v_payment.check_number
                                 );
            END IF;

            --Condition for RTGS and NEFT
            IF     v_payment.prod_code IN ('R', 'N')
               AND (   v_payment.supp_ifsc_code IS NULL
                    OR LENGTH (v_payment.supp_ifsc_code) <> 11
                   )
            THEN
               g_ever_failed := TRUE;
               l_msg :=
                     'Supplier IFSC Code is Invalid/Null'
                  || ','
                  || 'Supplier IFSC Code is Mandatory for RTGS and NEFT';
               fnd_file.put_line (fnd_file.LOG,
                                  l_msg || '-' || v_payment.check_number
                                 );
            END IF;

            --Condition for RTGS and NEFT ,IFT
            IF     v_payment.prod_code IN ('R', 'N', 'B')
               AND v_payment.supp_acc_no IS NULL
            THEN
               g_ever_failed := TRUE;
               l_msg :=
                     'Supplier Account No is Null'
                  || ','
                  || 'Supplier Account No is Mandatory ';
               fnd_file.put_line (fnd_file.LOG,
                                  l_msg || '-' || v_payment.check_number
                                 );
            END IF;

            --Condition for RTGS and NEFT
            IF     v_payment.prod_code IN ('R', 'N')
               AND v_payment.supp_bank_acc_type IS NULL
            THEN
               g_ever_failed := TRUE;
               l_msg :=
                     'Supplier Account Type is Null'
                  || ','
                  || 'Supplier Account Type is Mandatory ';
               fnd_file.put_line (fnd_file.LOG,
                                  l_msg || '-' || v_payment.check_number
                                 );
            END IF;

            IF v_payment.pay_amount IS NULL
            THEN
               g_ever_failed := TRUE;
               l_msg :=
                     'Payment Amount is Null'
                  || ','
                  || 'Payment Amount is Mandatory ';
               fnd_file.put_line (fnd_file.LOG,
                                  l_msg || '-' || v_payment.check_number
                                 );
            END IF;

            IF v_payment.prod_code IS NULL
            THEN
               g_ever_failed := TRUE;
               l_msg :=
                     'Payment Method is Null'
                  || ','
                  || 'Payment Method is Mandatory ';
               fnd_file.put_line (fnd_file.LOG,
                                  l_msg || '-' || v_payment.check_number
                                 );
            END IF;

            IF v_payment.pay_run_date IS NULL
            THEN
               g_ever_failed := TRUE;
               l_msg :=
                  'Payment Date is Null' || ','
                  || 'Payment Date is Mandatory ';
               fnd_file.put_line (fnd_file.LOG,
                                  l_msg || '-' || v_payment.check_number
                                 );
            END IF;

            /* OU Codification*/
            l_location_name := NULL;
            l_prefix := NULL;

            IF v_payment.op_name IS NOT NULL
            THEN
               BEGIN
                  SELECT location_name, prefix_vendor_code
                    INTO l_location_name, l_prefix
                    FROM apps.yes_bank_ou_codification
                   WHERE ou_name = v_payment.op_name;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_location_name := NULL;
                     g_ever_failed := TRUE;
                     l_msg :=
                           'OU Codification is Null'
                        || ','
                        || 'OU Codification  is Mandatory ';
               END;
            END IF;

            IF g_ever_failed = FALSE
            THEN
               INSERT INTO yesbank_payment_table
                           (corporate_account_number, corp_bank_name,
                            corp_acc_type, third_party_id,
                            run_identification,
                            user_id, user_dept,
                            dd_payable_location, profit_centre,
                            batch_id, corp_bank_city,
                            corp_bank_branch,
                            --PAY_TYPE_CODE           ,
                            vendor_code,
                            transactional_amount, transactional_currency,
                            product_code, transaction_value_date,
                            pay_doc_number, pay_run_date,
                            instruction_date, benefi_acc_num,
                            benefi_bank_name,
                            benefi_acc_type,
                            benefi_ifsc_code, primary_name,
                            secondary_name, vendor_address,
                            vendor_location, vendor_pincode,
                            primary_email, vendor_mobile, benfi_bank_city,
                            benefi_bank_branch, check_id,
                            transaction_status, updated_in_app,
                            attribute_category, attribute1, attribute2,
                            attribute3, attribute4, attribute5,
                            creation_date, created_by,
                            last_update_date,
                            last_updated_by
                           )
                    VALUES (v_payment.bank_account_num, v_bank.bank_name,
                            v_payment.bank_account_type, l_location_name,
                            v_payment.run_identification,
                            v_payment.user_name, NULL,
                            v_payment.pay_location, NULL,
                            v_payment.batch_id, v_payment.corp_bank_city,
                            v_payment.corp_bank_brnch,
                            l_prefix || '' || v_payment.sup_no,
                            v_payment.pay_amount, v_payment.currency,
                            v_payment.prod_code, v_payment.trx_val_date,
                            v_payment.check_number, v_payment.pay_run_date,
                            v_payment.pay_inst_date, v_payment.supp_acc_no,
                            v_payment.supp_bank_name,
                            v_payment.supp_bank_acc_type,
                            v_payment.supp_ifsc_code, v_payment.sup_name,
                            v_payment.sup_alt_name, v_payment.site_address,
                            v_payment.vendor_site_code, v_payment.pin_code,
                            v_payment.email_id, v_payment.phone, NULL,
                            --Supplier Bank City
                            v_payment.supp_bank_bran, v_payment.check_id,
                            'AUTHORIZED', 'N',
                            NULL,                       -- YESBANK Payment DFF
                                 NULL,                          -- Status Code
                                      NULL,               --Status Description
                            NULL,                                 --UTR Number
                                 NULL,                          --check Number
                                      NULL,                --Bank Reference No
                            v_payment.creation_date, v_payment.created_by,
                            v_payment.last_update_date,
                            v_payment.last_updated_by
                           );

               FOR v_inv IN inv_cur (v_payment.check_id)
               LOOP
                  IF v_inv.invoice_num IS NULL
                  THEN
                     g_ever_failed := TRUE;
                     l_msg :=
                           'Invoice Number is Null'
                        || ','
                        || 'Invoice Number is Mandatory ';
                     fnd_file.put_line (fnd_file.LOG,
                                        l_msg || '-' || v_payment.check_number
                                       );
                  END IF;

                  IF v_inv.invoice_amount IS NULL
                  THEN
                     g_ever_failed := TRUE;
                     l_msg :=
                           'Invoice Amount is Null'
                        || ','
                        || 'Invoice Amount is Mandatory ';
                     fnd_file.put_line (fnd_file.LOG,
                                        l_msg || '-' || v_payment.check_number
                                       );
                  END IF;

                  INSERT INTO yesbank_payment_invoice
                              (check_id, invoice_number,
                               invoice_date,
                               pay_type_code,
                               invoice_amount,
                               net_amount
                              )
                       VALUES (v_inv.check_id, v_inv.invoice_num,
                               v_inv.invoice_date,
                               v_inv.invoice_type_lookup_code,
                               v_inv.invoice_amount,
                               TO_CHAR (v_inv.amount_paid)
                              );
               END LOOP;

               COMMIT;
            END IF;
         END LOOP;
      END LOOP;

      COMMIT;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         l_msg := 'No Records Processed';
      WHEN OTHERS
      THEN
         errbuf := 'Other Exceptions ' || SQLERRM;
         retcode := '1';
   END xxabrl_scd_yb_payment_supp;
END xxabrl_supp_bank_pmt_yesbank; 
/


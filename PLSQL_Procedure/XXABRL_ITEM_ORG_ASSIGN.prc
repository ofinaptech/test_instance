CREATE OR REPLACE PROCEDURE APPS.xxabrl_item_org_assign (
   errbuf    OUT   VARCHAR2,
   retcode   OUT   NUMBER
)
IS
   /* i..Assign Items to New Organizations.*
    ii..after this need to run Standard import program "Item Import" */
   v_req_id   NUMBER;
   le_load    EXCEPTION;

   CURSOR cur_org
   IS
      SELECT   organization_id, NAME
          FROM apps.hr_all_organization_units
         WHERE NAME IN
                  ('ABRL  DL MOMENTS HM OU', 'ABRL AP HM OU',
                   'ABRL AP KPALLY HM OU', 'ABRL AUR HM OU',
                   'ABRL BANGALORE HM DC OU', 'ABRL BNG BT HM OU',
                   'ABRL BNG HM OU', 'ABRL BNG HOSUR HM OU',
                   'ABRL Chandigarh SM OU', 'ABRL BNG JAYANAGAR HM OU',
                   'ABRL BNG JP NAGAR HM OU', 'ABRL BNG Sahakarnagar HM OU',
                   'ABRL BNG VAISHNAVI HM OU', 'ABRL CORP HM OU',
                   'ABRL CORP OFFICE OU', 'ABRL CORP SM OU', 'ABRL DL HM OU',
                   'ABRL ERRAMANZIL HM OU', 'ABRL GJ HM OU', 'ABRL GJ SM OU',
                   'ABRL GOPALAN HM OU', 'ABRL HO OU', 'ABRL HR SM OU',
                   'ABRL HR ZO OU', 'ABRL HR-RJ SM OU', 'ABRL IND HM OU',
                   'ABRL KEMPFORT TSS HM OU', 'ABRL PUDUCHERRY SM OU',
                   'ABRL MADIWALA TSS HM OU', 'ABRL MARTHALLI HM OU',
                   'ABRL MP SM OU', 'ABRL Mumbai SM OU', 'ABRL NASHIK HM OU',
                   'ABRL NCR-DL SM OU', 'ABRL NCR-HR SM OU',
                   'ABRL NCR-UP SM OU', 'ABRL North RDC OU',
                   'ABRL ORCHID BP HM OU', 'ABRL ORCHID HM OU',
                   'ABRL PJ SM OU', 'ABRL PJ-HR SM OU',
                   'ABRL PRIVATE LABEL OU', 'ABRL RAJ SM OU',
                   'ABRL RAJAJINAGAR TSS HM OU', 'ABRL RJ SM OU',
                   'ABRL RJ ZO OU', 'ABRL ROM SM OU',
                   'ABRL SARJAPUR TSS HM OU', 'ABRL THN HM OU',
                   'ABRL VASHI HM OU', 'ABRL WB SM OU', 'ABRL West RDC OU',
                   'Aditya Birla Retail Group', 'AHMEDABAD DC',
                   'Ahmedabad New DC', 'BANGALORE DC', 'BHIWANDI DC',
                   'CALICUT DC', 'CHENNAI DC', 'COIMBATORE DC', 'DELHI DC',
                   'FAB MALL OU', 'GURGAON DC', 'GURGAON JOSH DC',
                   'HAS TWO OU', 'HM KARNATAKA OU', 'HUBLI DC',
                   'HYDERABAD DC', 'JAIPUR DC', 'KOCHI DC', 'KOLKATTA DC',
                   'LUDHIANA DC', 'MANGALORE DC', 'MYSORE HM DC',
                   'PATALGANGA D.C.', 'RKN CORP OFFICE OU', 'SM ANDHRA OU',
                   'SM KARNATAKA OU', 'SM KARNATAKA RDC OU', 'SM KERALA OU',
                   'SM TAMIL NADU OU', 'SM TELANGANA OU', 'TERRA FIRMA OU',
                   'VIJAYAWADA DC', 'VISAKAPATNAM DC', 'WAGHOLI DC',
                   'ZO AP OU')
      ORDER BY 1;

   CURSOR c1
   IS
      SELECT DISTINCT organization_id, segment1
                 FROM apps.mtl_system_items_interface
                WHERE 1 = 1
                  AND organization_id IN (97)
                  AND inventory_item_status_code = 'Active'
                  AND segment1 IN ('202400005');
BEGIN
   FOR v_org IN cur_org
   LOOP
      FOR v_rec IN c1
      LOOP
         INSERT INTO apps.mtl_system_items_interface
                     (segment1, organization_id, process_flag,
                      transaction_type, set_process_id, program_update_date,
                      last_update_date, created_by, creation_date,
                      last_updated_by
                     )
              VALUES (v_rec.segment1, v_org.organization_id, 1,
                      'CREATE', 1, SYSDATE,
                      SYSDATE, 4191, SYSDATE,
                      4191
                     );
      END LOOP;
   END LOOP;

--   UPDATE xxabrl.xxabrl_item_creation
--      SET org_assignment_flag = 'Y'
--    WHERE org_assignment_flag IS NULL;
   COMMIT;
   fnd_file.put_line (fnd_file.LOG, 'Import Items Program started');
--   97, 1, 1, 1, 1, , 1, 1
   v_req_id :=
      fnd_request.submit_request ('INV',
                                  'INCOIN',
                                  '',
                                  '',
                                  FALSE,
                                  97,
                                  1,
                                  1,
                                  1,
                                  1,
                                  NULL,
                                  1,
                                  1
                                 );
   COMMIT;

   IF (v_req_id = 0)
   THEN
      RAISE le_load;
   ELSE
      fnd_file.put_line
                       (fnd_file.LOG,
                           'Submitted Import Items Program with request id :'
                        || TO_CHAR (v_req_id)
                       );
   END IF;
EXCEPTION
   WHEN le_load
   THEN
      retcode := 2;
      errbuf := 'Import Items Program completed with error';
   WHEN OTHERS
   THEN
      fnd_file.put_line (fnd_file.LOG,
                         'error code 2: ' || SQLCODE || ':' || SQLERRM
                        );
END; 
/


CREATE OR REPLACE PACKAGE BODY APPS.XXABRL_INDIA_IT_PKG AS


PROCEDURE XXABRL_INDIA_IT_PROC( errbuf VARCHAR2
                                    ,retcode NUMBER
                                    ,P_DATE_FROM        VARCHAR2 --FA_BOOKS.DATE_PLACED_IN_SERVICE%TYPE
			            ,P_DATE_TO          VARCHAR2 --FA_BOOKS.DATE_PLACED_IN_SERVICE%TYPE
                                    ,P_BOOK_CODE        FA_BOOKS.BOOK_TYPE_CODE%TYPE
--			            ,dummy              number
                                   )
IS

l_full_year      VARCHAR2(20);
l_year           VARCHAR2(10);
l_ass_full_year  VARCHAR2(20);
l_ass_year       VARCHAR2(10);
l_year_end       VARCHAR2(20);

l_total_opening_wdv          NUMBER(30,2);
l_total_more_than_180        NUMBER(30,2);
l_total_less_than_180        NUMBER(30,2);
l_total_deletion             NUMBER(30,2);
l_total_balance              NUMBER(30,2);
l_total_rate_of_deprn        NUMBER(30,2);
l_total_deprn_open_wdv       NUMBER(30,2);
l_total_deprn_more_than_180  NUMBER(30,2);
l_total_deprn_less_than_180  NUMBER(30,2);
l_total_deprn_total          NUMBER(30,2);
l_total_net_balance          NUMBER(30,2);

lc_balance                    NUMBER(30,2) := 0;
lc_total                      NUMBER(30,2) := 0;
lc_net_balance                NUMBER(30,2) := 0;


CURSOR cur_data --(p_fdate DATE, p_tdate DATE)
IS
SELECT 
         DISTINCT 
		  d.start_date
		, d.end_date
		, d.slno
		, dtls.date_placed_in_service
		, a.closing_wdv
		, b.depn_of_assets
		,  b.unplanned_depn
		, (a.closing_wdv-b.depn_of_assets) total_old
		, bk.COST COST  /*a.original_cost cost  code commented by aiyer for the bug #3662680 */
		, bk.date_effective /* Added by aiyer for the bug 3902532*/
		, dtls.asset_id
		, a.block_id
		, bk.transaction_header_id_in
		, a.TYPE particulars
		, a.opening_wdv
		/*   , CASE  WHEN dtls.date_placed_in_service BETWEEN d.start_date AND d.end_date
		THEN bk.COST
		ELSE NULL
		END          greater_than_180
		, CASE  WHEN dtls.date_placed_in_service BETWEEN d.start_date AND d.end_date
		THEN bk.COST
		ELSE NULL
		END less_than_180   */
		,d.end_date-d.start_date No_of_days
		,dtls.date_placed_in_service-d.start_date diff
		,d.end_date-dtls.date_placed_in_service diff_3
		,CASE WHEN dtls.date_placed_in_service-d.start_date >=181
		      THEN bk.COST
		      ELSE 0
		 END   more_than_180
		,CASE WHEN dtls.date_placed_in_service-d.start_date <=181
		      THEN bk.COST
		      ELSE 0
		 END   less_than_180
		,fr.proceeds_of_sale deletion
--		,(a.opening_wdv+ NVL(bk.COST,0)- NVL(fr.proceeds_of_sale,0)) balance
		,a.rate                    individualrate_of_deprn
		,d.rate rate_of_dep
		,(a.opening_wdv*a.rate/100) dep_on_opening_wdv
		,CASE WHEN dtls.date_placed_in_service-d.start_date >=181
		      THEN (bk.COST*(a.rate/100)*(d.rate/100)) 
		      ELSE 0 
		 END more_180_dep
		,CASE WHEN dtls.date_placed_in_service-d.start_date <=181
		      THEN (bk.COST*(a.rate/100)*(d.rate/100)) 
		      ELSE 0 
		 END less_180_dep
--		,(a.opening_wdv*a.rate/100) +  (bk.COST*(a.rate/100)*(d.rate/100))  total
--		, a.opening_wdv - ((a.opening_wdv*a.rate/100) +  (bk.COST*(a.rate/100)*(d.rate/100))) Net_balance
FROM
		 JAI_FA_AST_BLOCKS     a
		,JAI_FA_DEP_BLOCKS     b
		,fa_retirements        fr
		,fa_books              bk
		,JAI_FA_AST_BLOCK_DTLS   dtls
		,JAI_FA_AST_PERIOD_RATES d
WHERE
		    a.block_id = b.block_id         
		AND b.slno =0   
		AND a.opening_wdv >= 0  
		AND a.year_ended = b.year_ended  
		AND a.closing_wdv IS NOT NULL        
		AND a.book_type_code = bk.book_type_code
		AND a.book_type_code = P_BOOK_CODE 
		AND fr.transaction_header_id_in(+) = bk.transaction_header_id_in
		AND bk.asset_id = fr.asset_id(+)
		AND bk.book_type_code= fr.book_type_code(+)	  
		AND bk.asset_id = dtls.asset_id
		AND a.block_id = dtls.block_id 
		AND bk.date_ineffective IS  NULL
		AND bk.transaction_header_id_out IS   NULL
		AND date_of_acquisition BETWEEN d.start_date  AND d.end_date   --02-02-2002 changed from RR to RRRR
		AND d.year_start = TO_DATE(NVL(P_DATE_FROM,d.year_start),'YYYY/MM/DD HH24:MI:SS') 
	--	AND  a.start_date >= TO_DATE(NVL(P_DATE_FROM,a.start_date),'YYYY/MM/DD HH24:MI:SS') 
		AND  date_of_acquisition BETWEEN TO_DATE(NVL(P_DATE_FROM,a.start_date),'YYYY/MM/DD HH24:MI:SS') AND NVL(P_DATE_TO,a.year_ended)  -- added on 5th feb 09
--		AND  TO_DATE(a.start_date,'YYYY/MM/DD HH24:MI:SS') <= TO_DATE(NVL(P_DATE_TO,a.year_ended),'YYYY/MM/DD HH24:MI:SS')  
		ORDER BY   SUBSTR(a.TYPE,1,20),a.rate;


BEGIN



			BEGIN

			SELECT SUBSTR(TO_CHAR(d.year_start,'DD-MON-RRRR'),8,4)
			      ,SUBSTR(SUBSTR(TO_CHAR(d.year_start,'DD-MON-RRRR'),8,4)  + 1,3,4)
			      ,TO_CHAR(d.year_end,'DD-MON-RRRR')   
			       INTO l_full_year 
			           ,l_year
				   ,l_year_end
			FROM JAI_FA_AST_PERIOD_RATES d
			WHERE d.year_start = TO_DATE(NVL(P_DATE_FROM,d.year_start),'YYYY/MM/DD HH24:MI:SS')
			--AND d.start_date = P_DATE_FROM;
			AND d.start_date = TO_DATE(NVL(P_DATE_FROM,d.year_start),'YYYY/MM/DD HH24:MI:SS') ;
			EXCEPTION
			WHEN NO_DATA_FOUND THEN
				Fnd_File.put_line(Fnd_File.LOG,'Exception found No_dat_found in selecting year');
			WHEN TOO_MANY_ROWS THEN
				Fnd_File.put_line(Fnd_File.LOG,'Exception found too_many_rows in selecting year');
			WHEN OTHERS THEN
				Fnd_File.put_line(Fnd_File.LOG,'Others Exception found  in selecting year');
			END;

                     -- Assessment year
			l_ass_full_year := l_full_year +1;
			l_ass_year      := l_year +1;


       Fnd_File.put_line(Fnd_File.OUTPUT,CHR(9)            ||'ABRL India - Income Tax Act Fixed Asset Schedules');
--       Fnd_File.put_line(Fnd_File.OUTPUT,CHR(9)            ||'---------------------------');
       Fnd_File.put_line(Fnd_File.OUTPUT,CHR(9)            ||'                   ');
      Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || 'As on Date:'
                         || CHR (9)
                         || SYSDATE
                         || CHR (9)
                        );
      Fnd_File.put_line (Fnd_File.output, ' ');
      Fnd_File.put_line (Fnd_File.output, 'REPORT PARAMETRS ');
      Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || 'BOOK TYPE CODE:'
                         || CHR (9)
                         || P_BOOK_CODE
                         || CHR (9)
                        );
        Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || 'Start Date:'
                         || CHR (9)
                         || P_DATE_FROM
                         || CHR (9)
                        );

-------------

	l_total_opening_wdv          := 0;
	l_total_more_than_180        := 0;
	l_total_less_than_180        := 0;
	l_total_deletion             := 0;
	l_total_balance              := 0;
	l_total_rate_of_deprn        := 0;
	l_total_deprn_open_wdv       := 0;
	l_total_deprn_more_than_180  := 0;
	l_total_deprn_less_than_180  := 0;
	l_total_deprn_total          := 0;
	l_total_net_balance          := 0;


	Fnd_File.put_line (Fnd_File.output, ' ');
	Fnd_File.put_line (Fnd_File.output, ' ');
        Fnd_File.put_line (Fnd_File.output, 'Previous Year:'||CHR (9)|| l_full_year ||'-'|| l_year );
        Fnd_File.put_line (Fnd_File.output, 'Assessment Year:'||CHR (9)|| l_ass_full_year ||'-'|| l_ass_year );
	Fnd_File.put_line (Fnd_File.output, ' ');
	Fnd_File.put_line (Fnd_File.output, 'Attachment to Clause 14 of Form 3CD');
	Fnd_File.put_line (Fnd_File.output, ' ');
	Fnd_File.put_line (Fnd_File.output, 'Computation of Depreciation as per the Income-tax Act, 1961');
	Fnd_File.put_line (Fnd_File.output, ' ');


--	Fnd_File.put_line (Fnd_File.output,CHR (9)||CHR (9)||'Additions/(Deletions)'|| CHR(9)||'Additions/(Deletions)'||CHR(9)||'Additions/(Deletions)'||CHR(9)||CHR(9)||
--	                     'Depriciation'||CHR(9)||'Depriciation'||CHR(9)||'Depriciation'||CHR(9)||'Depriciation');

       --Printing Output
       Fnd_File.put_line(Fnd_File.OUTPUT,                     'Particulars'||CHR(9)
	                                                   ||'Opening WDV'||CHR(9)
	                                                   ||'Additions More than 180 days'||CHR(9)
							   ||'Additions Less then 180 days'||CHR(9)
							   ||'(Delitions during the year)'||CHR(9)
           						   ||'Balance At ' ||l_year_end||CHR(9)
			           			   ||'Rate of Depriciation'||CHR(9)
							   ||'Deprn on Opening WDV'||CHR(9)
						           ||'Deprn More than 180 days'||CHR(9)
							   ||'Deprn Less than 180 days'||CHR(9)
 							   ||'Total'||CHR(9)
           						   ||'Net Block as on '||l_year_end||CHR(9)
							   );
	Fnd_File.put_line (Fnd_File.output, ' ');

       --Opening and Fetching Cursor Values into local variables.
	   FOR rec_data IN cur_data
	   LOOP

	-- to get balance value
        lc_balance := NVL(rec_data.opening_wdv,0) + NVL(rec_data.more_than_180,0) + NVL(rec_data.less_than_180,0) - NVL(rec_data.deletion,0) ;
	lc_total := NVL(rec_data.dep_on_opening_wdv,0) + NVL(rec_data.more_180_dep,0) + NVL(rec_data.less_180_dep,0) ;
	lc_net_balance := (NVL(lc_balance,0) - (NVL(lc_total,0))) ;

	l_total_opening_wdv          := l_total_opening_wdv+NVL(rec_data.opening_wdv,0) ;
	l_total_more_than_180        := l_total_more_than_180+NVL(rec_data.more_than_180,0) ;
	l_total_less_than_180        := l_total_less_than_180+ NVL(rec_data.less_than_180,0);
	l_total_deletion             := l_total_deletion+ NVL(rec_data.deletion,0);
	l_total_balance              := l_total_balance+ NVL(lc_balance,0);
	l_total_rate_of_deprn        := l_total_rate_of_deprn+ NVL(rec_data.rate_of_dep,0);
	l_total_deprn_open_wdv       := l_total_deprn_open_wdv+NVL(rec_data.dep_on_opening_wdv,0);
	l_total_deprn_more_than_180  := l_total_deprn_more_than_180+NVL(rec_data.more_180_dep,0);
	l_total_deprn_less_than_180  := l_total_deprn_less_than_180+NVL(rec_data.less_180_dep,0);
	l_total_deprn_total          := l_total_deprn_total+NVL(lc_total,0);
	l_total_net_balance          := l_total_net_balance+NVL(lc_net_balance,0);

--		,(a.opening_wdv*a.rate/100) +  (bk.COST*(a.rate/100)*(d.rate/100))  total
--		, a.opening_wdv - ((a.opening_wdv*a.rate/100) +  (bk.COST*(a.rate/100)*(d.rate/100))) Net_balance



	              --Printing Local Variable Values to output.
				  Fnd_File.put_line(Fnd_File.OUTPUT,rec_data.particulars||CHR(9)
	                                                   ||rec_data.opening_wdv||CHR(9)
	                                                   ||rec_data.more_than_180||CHR(9)
							   ||rec_data.less_than_180||CHR(9)
							   ||NVL(rec_data.deletion,0)||CHR(9)           
           						   ||lc_balance||CHR(9)
			           			   ||rec_data.individualrate_of_deprn||CHR(9)
						           ||rec_data.dep_on_opening_wdv||CHR(9)
							   ||rec_data.more_180_dep||CHR(9)
							   ||rec_data.less_180_dep||CHR(9)
           						   ||lc_total||CHR(9)
			           			   ||lc_net_balance||CHR(9)
							   );

	   END LOOP;

			    -- printing Totals for all the column
			    	  Fnd_File.put_line (Fnd_File.output, ' ');
				  Fnd_File.put_line(Fnd_File.OUTPUT,'Total'||CHR(9)
	                                                   ||l_total_opening_wdv||CHR(9)
	                                                   ||l_total_more_than_180||CHR(9)
							   ||l_total_less_than_180||CHR(9)
							   ||l_total_deletion||CHR(9)           
           						   ||l_total_balance||CHR(9)
			           			   ||l_total_rate_of_deprn||CHR(9)
						           ||l_total_deprn_open_wdv||CHR(9)
							   ||l_total_deprn_more_than_180||CHR(9)
							   ||l_total_deprn_less_than_180||CHR(9)
           						   ||l_total_deprn_total||CHR(9)
			           			   ||l_total_net_balance||CHR(9)
							   );


--Fnd_File.put_line (Fnd_File.output,'The cars taken on lease has been capitalised in the books of account of the Company as per Accounting Standard 19 Accounting for Leases');

    END XXABRL_INDIA_IT_PROC;
END XXABRL_INDIA_IT_PKG;
/


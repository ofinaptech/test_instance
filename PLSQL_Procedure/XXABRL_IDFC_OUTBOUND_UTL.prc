CREATE OR REPLACE PROCEDURE APPS.xxabrl_idfc_outbound_utl (
   errbuf    OUT   VARCHAR2,
   retcode   OUT   VARCHAR2
)
AS
   /**********************************************************************************************************************************************
                          WIPRO Infotech Ltd, Mumbai, India
              Name: Supplier Bank Outbound interface for DBS Bank generating file on FTP [H2H Interface betwen ABRL and DBS Bank]
              Change Record:
             =========================================================================================================================
             Version   Date          Author                    Remarks                  Documnet Ref
             =======   ==========   =============             ==================  =====================================================
             1.0.0     09-Jul-2012   Amresh Kumar Chutke      Initial Version
             1.0.1     19-Jul-2012   Amresh Kumar Chutke      Changed cursor logic to pickup transactions which are created 3 days prior
             1.0.2     23-Jul-2012   Amresh Kumar Chutke      Changed Beneficiary/Receiver Name as DBS is unable to pickup  more than 49 characters
             1.0.3     05-Nov-2012   Amresh Kumar Chutke      Added new Bank account for TSRL Payments and Advice Curosr testing
             1.0.4     08-Nov-2012   Amresh Kumar Chutke      Added comma between check_id and check_number to the Payment File to ease the reconciliation process at user end.
                                                              Added Beneficiary Name and Sender Account Number to the Advice file
                                                              Finance Requirement as per Hemant/Rupesh mails
             1.0.5     11-Dec-2012   Amresh Kumar Chutke      TSRL Payments File Generation GO-LIVE
             1.0.6     01-Dec-2015   Dhiresh More             Make changes in payment File Generation - for IDEAL3
    ************************************************************************************************************************************************/

   --Declaring Payment Detail Local Variables
   lv_abrl_pay_count             NUMBER             := 0;
   --initializing counter to 0
   lv_abrl_pay_amount            NUMBER             := 0;
   --initializing amount to 0
   lv_tsrl_pay_count             NUMBER             := 0;
   --initializing counter to 0
   lv_tsrl_pay_amount            NUMBER             := 0;
   --initializing amount to 0
   l_file_p                      UTL_FILE.file_type;
   l_file_p1                     UTL_FILE.file_type;
   l_file_p2                     UTL_FILE.file_type;
   l_pmt_data                    VARCHAR2 (4000);
   l_pmt_data1                   VARCHAR2 (4000);
   v_dbs_neft_seq                VARCHAR2 (50);
   v_dbs_neft_seq1               VARCHAR2 (50);
   v_dbs_neft_tsrl_seq           VARCHAR2 (50);
   v_dbs_neft_tsrl_seq1          VARCHAR2 (50);
   v_dbs_pay_seq                 VARCHAR2 (50);
--Declaring Advice Detail Local Variables
   lv_abrl_adv_count             NUMBER             := 0;
   --initializing counter to 0
   lv_tsrl_adv_count             NUMBER             := 0;
   --initializing counter to 0
   lv_corporate_account_number   NUMBER             := 0;
   l_file_a                      UTL_FILE.file_type;
   l_file_a1                     UTL_FILE.file_type;
   l_adv_data                    VARCHAR2 (4000);
   v_dbs_advice_seq              VARCHAR2 (50);
   v_dbs_advice_tsrl_seq         VARCHAR2 (50);
   acc_no                        VARCHAR2 (2000);

-- Declaring Payment Cursor to pick up only Authorized Payments
   CURSOR cur_dbs_neft (p_acc_no IN VARCHAR2)
   IS
      SELECT   *
          FROM apps.idfc_payment_table
         WHERE transaction_status = 'AUTHORIZED'
           AND TRUNC (creation_date) >= TRUNC (SYSDATE) - 5
           -- this will also pick up transactions which were created 3 days prior
           AND product_code = 'N'
           AND corporate_account_number NOT IN ('811200088576')
           AND corporate_account_number = p_acc_no
      ORDER BY check_id, transaction_value_date;

--Declaring Advice Cursor to pick up invoices which are made against payments
   CURSOR cur_dbs_adv (p_acc_no VARCHAR2)                        --CUR_DBS_ADV
   IS
      SELECT   dnpi.*, dpt.vendor_code, dpt.third_party_id, dpt.primary_name,
               dpt.corporate_account_number, dpt.primary_email
          FROM apps.idfc_payment_invoice dnpi, apps.idfc_payment_table dpt
         WHERE dnpi.check_id = dpt.check_id
           AND TRUNC (creation_date) >= TRUNC (SYSDATE) - 5
           -- this will also pick up transactions which were created 2 days prior
           AND dpt.transaction_status = 'AUTHORIZED'
           AND dpt.product_code = 'N'
           AND dpt.corporate_account_number NOT IN ('811200088576')
           AND dpt.corporate_account_number = p_acc_no
      ORDER BY dnpi.check_id;

   CURSOR cur_adv_pay_acc_no
   IS
      SELECT   dpt.corporate_account_number
          FROM apps.idfc_payment_invoice dnpi, apps.idfc_payment_table dpt
         WHERE dnpi.check_id = dpt.check_id
           AND TRUNC (creation_date) >= TRUNC (SYSDATE) - 5
           -- this will also pick up transactions which were created 2 days prior
           AND dpt.transaction_status = 'AUTHORIZED'
           AND dpt.product_code = 'N'
           AND dpt.corporate_account_number NOT IN ('811200088576')
      GROUP BY dpt.corporate_account_number;

--TSRL Payments and Advice Curosr -- 05 Nov 2012 testing
-- Declaring TSRL Payment Cursor to pick up only Authorized Payments
   CURSOR cur_dbs_tsrl_neft
   IS
      SELECT   *
          FROM apps.idfc_payment_table
         WHERE transaction_status = 'AUTHORIZED'
           AND TRUNC (creation_date) >= TRUNC (SYSDATE) - 5
           -- this will also pick up transactions which were created 2 days prior
           AND product_code = 'N'
           AND corporate_account_number = '811200088576'
      ORDER BY check_id, transaction_value_date;

--Declaring TSRL Advice Cursor to pick up invoices which are made against payments
   CURSOR cur_dbs_tsrl_adv
   IS
      SELECT   dnpi.*, dpt.vendor_code, dpt.third_party_id, dpt.primary_name,
               dpt.corporate_account_number
          FROM apps.idfc_payment_invoice dnpi, apps.idfc_payment_table dpt
         WHERE dnpi.check_id = dpt.check_id
           AND TRUNC (creation_date) >= TRUNC (SYSDATE) - 5
           -- this will also pick up transactions which were created 2 days prior
           AND dpt.transaction_status = 'AUTHORIZED'
           AND dpt.product_code = 'N'
           AND dpt.corporate_account_number = '811200088576'
      ORDER BY dnpi.check_id;

-- End of TSRL Payments and Advice Curosr -- 05 Nov 2012 testing
   CURSOR trx_amount
   IS
      SELECT   COUNT (*) pay_count,
               SUM (transactional_amount) transactional_amount,
               corporate_account_number
--     INTO lv_abrl_pay_count, lv_abrl_pay_amount
      FROM     apps.idfc_payment_table
         WHERE 1 = 1
           AND transaction_status = 'AUTHORIZED'
           AND TRUNC (creation_date) >= TRUNC (SYSDATE) - 5
           -- this will also pick up transactions which were created 2 days prior
           AND product_code = 'N'
           AND corporate_account_number NOT IN ('811200088576')
      GROUP BY corporate_account_number;
BEGIN
   SELECT COUNT (*), SUM (transactional_amount)
     INTO lv_abrl_pay_count, lv_abrl_pay_amount
     FROM apps.idfc_payment_table
    WHERE 1 = 1
      AND transaction_status = 'AUTHORIZED'
      AND TRUNC (creation_date) >= TRUNC (SYSDATE) - 5
      -- this will also pick up transactions which were created 2 days prior
      AND product_code = 'N'
      AND corporate_account_number NOT IN ('811200088576');

   SELECT COUNT (*), SUM (transactional_amount)
     INTO lv_tsrl_pay_count, lv_tsrl_pay_amount
     FROM apps.idfc_payment_table
    WHERE 1 = 1
      AND transaction_status = 'AUTHORIZED'
      AND TRUNC (creation_date) >= TRUNC (SYSDATE) - 5
      -- this will also pick up transactions which were created 2 days prior
      AND product_code = 'N'
      AND corporate_account_number IN ('811200088576');

   -- Generating a Unique Sequence Number to be concatenated in the file name
   SELECT dbs_neft_seq1.NEXTVAL || 'x' || TO_CHAR (SYSDATE, 'ddmmyyyy')
     INTO v_dbs_neft_seq1
     FROM DUAL;

   --TSRL-- Generating a Unique Sequence Number to be concatenated in TSRL file name
   SELECT dbs_neft_tsrl_seq.NEXTVAL || 'x' || TO_CHAR (SYSDATE, 'ddmmyyyy')
     INTO v_dbs_neft_tsrl_seq
     FROM DUAL;

   --TSRL-- Generating a Unique Sequence Number to be concatenated in TSRL file name
   SELECT dbs_neft_tsrl_seq1.NEXTVAL || 'x' || TO_CHAR (SYSDATE, 'ddmmyyyy')
     INTO v_dbs_neft_tsrl_seq1
     FROM DUAL;

   -- Payment File Generation
   IF lv_abrl_pay_count > 0
   --AND LV_CORPORATE_ACCOUNT_NUMBER NOT IN ('811200088576')
   THEN
      FOR xx_main IN trx_amount
      LOOP
         fnd_file.put_line (fnd_file.LOG,
                               'corporate_account_number=>'
                            || xx_main.corporate_account_number
                           );

-------------  con------------
 -- Generating a Unique Sequence Number to be concatenated in the file name
         SELECT   
-- REVERSE (SUBSTR (REVERSE (xx_main.corporate_account_number),
--                                    1,
--                                    4
--                                   )
--                           )
--                || '.'
                  dbs_neft_seq.NEXTVAL
               -- || 'x'
               -- || TO_CHAR (SYSDATE, 'ddmmyyyy')
           INTO v_dbs_neft_seq
           FROM DUAL;

         SELECT xxabrl_dbs_pay_seq.NEXTVAL
           INTO v_dbs_pay_seq
           FROM DUAL;

         SELECT REVERSE (SUBSTR (REVERSE (xx_main.corporate_account_number),
                                 1,
                                 4
                                )
                        )
           INTO acc_no
           FROM DUAL;

         --Creating a file for the below generated payment data
         l_file_p :=
            UTL_FILE.fopen ('IDFC',
                               'PF_ABRL_IDFC_'
                            || acc_no
                            || '_'
                            || TO_CHAR (SYSDATE, 'DDMMYYY')
                            || '_'
                           || v_dbs_neft_seq
                            || '.TXT',
                            'W'
                           );
         UTL_FILE.put_line (l_file_p,
                               'HEADER'
                            || '|'
                            || TO_CHAR (SYSDATE, 'ddmmyyyy')
                            || '|'
                            || 'INADRE01'
                            || '|'
                            || 'ADITYA BIRLA RETAIL LIMITED'
                           );         ----   

--         l_file_p1 :=
--            UTL_FILE.fopen ('/usr/tmp',
--                            'INDBSI02XXXX.' || v_dbs_neft_seq || '.TXT',
--                            'W'
--                           );
--         UTL_FILE.put_line (l_file_p1,
--                               'HEADER'
--                            || '|'
--                            || TO_CHAR (SYSDATE, 'ddmmyyyy')
--                            || '|'
--                            || 'INADRE01'
--                            || '|'
--                            || 'ADITYA BIRLA RETAIL LIMITED'
--                           );         ----  changes made by Dhiresh on 16Nov15
         FOR rec_dbs_neft IN cur_dbs_neft (xx_main.corporate_account_number)
         LOOP
            l_pmt_data :=
                  'PAYMENT'                         -- Orginating Branch Code
               || '|'
               || 'BPY'
               || '|'
               || rec_dbs_neft.corporate_account_number    --Sender account no
               || '|'
               || 'INR'
               || '|'
               || NULL                                               -- Filler
               || '|'
               || 'INR'
               || '|'
--                  || null -- Filler
--                  ||'|'
               || v_dbs_pay_seq                            -- PAYMENT BATCH ID
               || '|'
               || TO_CHAR (rec_dbs_neft.transaction_value_date, 'ddmmyyyy')
               --Payment date
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || REPLACE
                     (REPLACE
                         (REPLACE
                             (REPLACE
                                 (REPLACE
                                     (REPLACE
                                         (REPLACE
                                             (REPLACE
                                                 (REPLACE
                                                     (REPLACE
                                                         (REPLACE
                                                             (REPLACE
                                                                 (REPLACE
                                                                     (REPLACE
                                                                         (REPLACE
                                                                             (REPLACE
                                                                                 (SUBSTR
                                                                                     (rec_dbs_neft.primary_name,
                                                                                      0,
                                                                                      35
                                                                                     ),
                                                                                  '&',
                                                                                  ''
                                                                                 ),
                                                                              '!',
                                                                              ''
                                                                             ),
                                                                          '@',
                                                                          ''
                                                                         ),
                                                                      '#',
                                                                      ''
                                                                     ),
                                                                  '$',
                                                                  ''
                                                                 ),
                                                              '%',
                                                              ''
                                                             ),
                                                          '|',
                                                          ''
                                                         ),
                                                      '*',
                                                      ''
                                                     ),
                                                  '=',
                                                  ''
                                                 ),
                                              '{',
                                              ''
                                             ),
                                          '}',
                                          ''
                                         ),
                                      '[',
                                      ''
                                     ),
                                  ']',
                                  ''
                                 ),
                              '^',
                              ''
                             ),
                          '\',
                          ''
                         ),
                      ',',
                      ''
                     )
               --Receiver name --Changed as DBS is unable to pickup  more than 50 characters
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || 'Bene Code: '
               || rec_dbs_neft.vendor_code
               --null -- Filler -- As per Bala Sir/Rupesh mail dated July 12, 2012 6:48 PM
               || '|'
               || rec_dbs_neft.benefi_acc_num            --Receiver account no
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || rec_dbs_neft.benefi_ifsc_code                    --IFSC Code
               || '|'
               || NULL                                               -- Filler
               || '|'
               || rec_dbs_neft.benefi_bank_name                      -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || TRIM (TO_CHAR (rec_dbs_neft.transactional_amount,
                                 '99999999.99'
                                )
                       )
               --Payment Amount
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || '20'                  -- Transaction code for Vendor Payment
               || '|'
               || NULL                                               -- Filler
               || '|'
               || rec_dbs_neft.pay_doc_number
               || '|'
               || rec_dbs_neft.check_id
               || '|'
               -- Payment Details -- Finance Requirement as per Hemant/Rupesh mails
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL
               --'E'                                --Delivery Mode as E-Mail
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || REPLACE
                     (REPLACE
                         (REPLACE
                             (REPLACE
                                 (REPLACE
                                     (REPLACE
                                         (REPLACE
                                             (REPLACE
                                                 (REPLACE
                                                     (REPLACE
                                                         (REPLACE
                                                             (REPLACE
                                                                 (REPLACE
                                                                     (REPLACE
                                                                         (REPLACE
                                                                             (REPLACE
                                                                                 (SUBSTR
                                                                                     (rec_dbs_neft.primary_name,
                                                                                      0,
                                                                                      35
                                                                                     ),
                                                                                  '&',
                                                                                  ''
                                                                                 ),
                                                                              '!',
                                                                              ''
                                                                             ),
                                                                          '@',
                                                                          ''
                                                                         ),
                                                                      '#',
                                                                      ''
                                                                     ),
                                                                  '$',
                                                                  ''
                                                                 ),
                                                              '%',
                                                              ''
                                                             ),
                                                          '|',
                                                          ''
                                                         ),
                                                      '*',
                                                      ''
                                                     ),
                                                  '=',
                                                  ''
                                                 ),
                                              '{',
                                              ''
                                             ),
                                          '}',
                                          ''
                                         ),
                                      '[',
                                      ''
                                     ),
                                  ']',
                                  ''
                                 ),
                              '^',
                              ''
                             ),
                          '\',
                          ''
                         ),
                      ',',
                      ''
                     )
               --Receiver name --Changed as DBS is unable to pickup  more than 50 characters
               || '|'
               || NULL                                       -- Address Line 1
               || '|'
               || NULL                                        --Address Line 2
               || '|'
               || NULL                                        --Address Line 3
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                 --'idealmumbai@dbs.com'
               --Email -- Only one email address as discussed with Rupesh/Hemanth
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               --  || 'EmailS:'
               || REPLACE (rec_dbs_neft.primary_email, ',', ';');
--PRIMARY_EMAIL -- As discussed with Rupesh on 11MAY2012     -- Advice Details of Transaction
--               || ':EmailE';
--
--                  || 'Bene Code: '||REC_DBS_NEFT.VENDOR_CODE--null -- Filler -- As per Bala Sir/Rupesh mail dated July 12, 2012 6:48 PM
--                  || '|'
--                  || null -- Filler
--                  || '|'
--                  || NULL --REC_DBS_NEFT.REFERENCE_DATE -- Reference date --blank
--                  || '|'
--                  || null --REC_DBS_NEFT.CHECK_ID --blank as discussed with Hemant on 14MAY12 --Reference transaction no --blank  [REPLACE IT WITH CHECK_ID AS THERE IS NO REFERENCE TO PAYMENT ADVICE]
--                  || '|'
--                  || REC_DBS_NEFT.ATTRIBUTE1 --Acknowledgement status -- blank
--                  || '|'
--                  || REC_DBS_NEFT.ATTRIBUTE2 --Rejection code -- blank
--                  || '|'
--                  ||','
--                  || SUBSTR(REC_DBS_NEFT.PRIMARY_NAME,0,49) --Advice name --Changed as DBS is unable to pickup  more than 50 characters
--                  || '|'
--                  || 'Bene Code: '||REC_DBS_NEFT.VENDOR_CODE --SUBSTR(REC_DBS_NEFT.VENDOR_ADDRESS,100,49) --Address Line 3 As per Bala Sir/Rupesh mail dated July 12, 2012 6:48 PM
--                  || '|'
--                  || 'E' --Delivery Mode --For Email Value= E
--                  || '|'
--                  || 'idealmumbai@dbs.com' --Email -- Only one email address as discussed with Rupesh/Hemanth
--                  || '|'
--                  || NULL --Fax -- blank
--                  || '|'
            UTL_FILE.put_line (l_file_p, l_pmt_data);
         --  UTL_FILE.put_line (l_file_p1, l_pmt_data);
         END LOOP;

         UTL_FILE.put_line (l_file_p,
                               'TRAILER'
                            || '|'
                            || xx_main.pay_count
                            || '|'
                            || TRIM (TO_CHAR (xx_main.transactional_amount,
                                              '99999999999999.99'
                                             )
                                    )
                           );
--         UTL_FILE.put_line (l_file_p1,
--                               'TRAILER'
--                            || '|'
--                            || xx_main.pay_count
--                            || '|'
--                            || TRIM (TO_CHAR (xx_main.transactional_amount,
--                                              '99999999999999.99'
--                                             )
--                                    )
--                           );         ----   
         UTL_FILE.fclose (l_file_p);
         -- UTL_FILE.fclose (l_file_p1);
         l_file_p2 :=
            UTL_FILE.fopen ('IDFC',
                               'PF_ABRL_IDFC_'
                            || acc_no
                            || '_'
                            || TO_CHAR (SYSDATE, 'DDMMYYY')
                            || '_'
                            || v_dbs_neft_seq
                            || '.txt.done',
                            'W'
                           );
         UTL_FILE.fclose (l_file_p2);
      END LOOP;

      fnd_file.put_line (fnd_file.LOG, 'End ');
   END IF;

   ---------------------------------------------------------------------------------
-- Payment Advice File Generation
   BEGIN
      SELECT COUNT (*)
        INTO lv_abrl_adv_count
        FROM apps.idfc_payment_invoice dnpi, apps.idfc_payment_table dpt
       WHERE dnpi.check_id = dpt.check_id
         AND TRUNC (creation_date) >= TRUNC (SYSDATE) - 5
         --this will also pick up transactions which were created 2 days prior
         AND dpt.transaction_status = 'AUTHORIZED'
         AND dpt.product_code = 'N'
         AND corporate_account_number NOT IN ('811200088576');

      acc_no := NULL;

      -- Generating a Unique Sequence Number to be concatenated in the file name
--      SELECT dbs_advice_seq.NEXTVAL || 'x' || TO_CHAR (SYSDATE, 'DDMONYYYY')
--        INTO v_dbs_advice_seq
--        FROM DUAL;
      IF lv_abrl_adv_count > 0
      --AND LV_CORPORATE_ACCOUNT_NUMBER NOT IN ('811200088576')
      THEN
         FOR xx_main IN cur_adv_pay_acc_no
         LOOP
            fnd_file.put_line
                         (fnd_file.LOG,
                             'Payment Advice File corporate_account_number=>'
                          || xx_main.corporate_account_number
                         );

            SELECT    REVERSE
                           (SUBSTR (REVERSE (xx_main.corporate_account_number),
                                    1,
                                    4
                                   )
                           )
                   || '.'
                   || dbs_neft_seq.NEXTVAL
                   || 'x'
                   || TO_CHAR (SYSDATE, 'ddmmyyyy')
              INTO v_dbs_advice_seq
              FROM DUAL;

            SELECT REVERSE (SUBSTR (REVERSE (xx_main.corporate_account_number),
                                    1,
                                    4
                                   )
                           )
              INTO acc_no
              FROM DUAL;

            --Creating a file for the below generated payment data
            l_file_a :=
               UTL_FILE.fopen ('IDFC',
                                  'IF_ABRL_IDFC_'
                               || acc_no
                               || '_'
                               || TO_CHAR (SYSDATE, 'DDMMYYY')
                               || '_'
                               || v_dbs_neft_seq
                               || '.TXT',
                               'W'
                              );

            FOR rec_dbs_adv IN cur_dbs_adv (xx_main.corporate_account_number)
            LOOP
               l_adv_data :=
                     rec_dbs_adv.check_id
                  || '|'
                  || rec_dbs_adv.invoice_number
                  || '|'
                  || TO_CHAR (rec_dbs_adv.invoice_date, 'DD-MON-YYYY')
                  || '|'
                  || rec_dbs_adv.invoice_amount
                  || '|'
                  || REPLACE (rec_dbs_adv.third_party_id, 'OU', '')
                  -- 'Location' added as per Bala Sir/Rupesh mail dated July 12, 2012 6:48 PM
                  || '|'
                  || rec_dbs_adv.tax
                  || '|'
                  || rec_dbs_adv.net_amount
                  || '|'
                  || rec_dbs_adv.pay_type_code
                  || '|'
                  || rec_dbs_adv.vendor_code
                  -- 'Bene Code' added as per Bala Sir/Rupesh mail dated July 12, 2012 6:48 PM
                  || '|'
                  || rec_dbs_adv.primary_name
                  -- Added on 08-Nov-2012 as per mail sent from DBS Bank
                  || '|'
                  || rec_dbs_adv.corporate_account_number
                  -- Added on 08-Nov-2012 as per mail sent from DBS Bank
                  || '|'
                  -- Added on 02-Feb-2016 as per mail sent from DBS Bank
                  --|| 'EmailS:'
                  || REPLACE (rec_dbs_adv.primary_email, ',', ';');
--                  || ':EmailE';
               -- Added on 02-Feb-2016 as per mail sent from DBS Bank
               UTL_FILE.put_line (l_file_a, l_adv_data);
            END LOOP;

            UTL_FILE.fclose (l_file_a);
            l_file_a1 :=
               UTL_FILE.fopen ('IDFC',
                                  'IF_ABRL_IDFC_'
                               || acc_no
                               || '_'
                               || TO_CHAR (SYSDATE, 'DDMMYYY')
                               || '_'
                               || v_dbs_neft_seq
                               || '.txt.done',
                               'W'
                              );
            UTL_FILE.fclose (l_file_a1);
         END LOOP;
      END IF;

      /* Updating the DBS Payment Stage Table as the transaction for ABRL have been delivered to DBS Bank*/
      UPDATE apps.idfc_payment_table
         SET transaction_status = 'PROCESSED',
             status_flag = 'Y'
       WHERE 1 = 1
         AND TRUNC (creation_date) >= TRUNC (SYSDATE) - 5
         AND transaction_status = 'AUTHORIZED'
         AND corporate_account_number NOT IN ('811200088576')
         AND product_code = 'N';

      COMMIT;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         UTL_FILE.fclose (l_file_a);
         DBMS_OUTPUT.put_line ('No Data Found');
      WHEN UTL_FILE.invalid_path
      THEN
         UTL_FILE.fclose (l_file_a);
         DBMS_OUTPUT.put_line ('Invalid Path');
      WHEN UTL_FILE.invalid_filehandle
      THEN
         UTL_FILE.fclose (l_file_a);
         DBMS_OUTPUT.put_line ('Invalid File Handle');
      WHEN UTL_FILE.invalid_operation
      THEN
         UTL_FILE.fclose (l_file_a);
         DBMS_OUTPUT.put_line ('Invalid Operation');
      WHEN UTL_FILE.read_error
      THEN
         UTL_FILE.fclose (l_file_a);
         DBMS_OUTPUT.put_line ('Read Error');
      WHEN UTL_FILE.write_error
      THEN
         UTL_FILE.fclose (l_file_a);
         DBMS_OUTPUT.put_line ('Write Error');
      WHEN UTL_FILE.internal_error
      THEN
         UTL_FILE.fclose (l_file_a);
         DBMS_OUTPUT.put_line ('Internal Error');
      WHEN UTL_FILE.file_open
      THEN
         UTL_FILE.fclose (l_file_a);
         DBMS_OUTPUT.put_line ('File is Open');
      WHEN UTL_FILE.invalid_filename
      THEN
         UTL_FILE.fclose (l_file_a);
         DBMS_OUTPUT.put_line ('Invalid File Name');
      WHEN OTHERS
      THEN
         UTL_FILE.fclose (l_file_a);
         DBMS_OUTPUT.put_line ('Unknown Error' || SQLERRM);
   END;
------------------------------------------------------------------------
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      UTL_FILE.fclose (l_file_p);
      DBMS_OUTPUT.put_line ('No Data Found');
   WHEN UTL_FILE.invalid_path
   THEN
      UTL_FILE.fclose (l_file_p);
      DBMS_OUTPUT.put_line ('Invalid Path');
   WHEN UTL_FILE.invalid_filehandle
   THEN
      UTL_FILE.fclose (l_file_p);
      DBMS_OUTPUT.put_line ('Invalid File Handle');
   WHEN UTL_FILE.invalid_operation
   THEN
      UTL_FILE.fclose (l_file_p);
      DBMS_OUTPUT.put_line ('Invalid Operation');
   WHEN UTL_FILE.read_error
   THEN
      UTL_FILE.fclose (l_file_p);
      DBMS_OUTPUT.put_line ('Read Error');
   WHEN UTL_FILE.write_error
   THEN
      UTL_FILE.fclose (l_file_p);
      DBMS_OUTPUT.put_line ('Write Error');
   WHEN UTL_FILE.internal_error
   THEN
      UTL_FILE.fclose (l_file_p);
      DBMS_OUTPUT.put_line ('Internal Error');
   WHEN UTL_FILE.file_open
   THEN
      UTL_FILE.fclose (l_file_p);
      DBMS_OUTPUT.put_line ('File is Open');
   WHEN UTL_FILE.invalid_filename
   THEN
      UTL_FILE.fclose (l_file_p);
      DBMS_OUTPUT.put_line ('Invalid File Name');
   WHEN OTHERS
   THEN
      UTL_FILE.fclose (l_file_p);
      DBMS_OUTPUT.put_line ('Unknown Error' || SQLERRM);
END xxabrl_idfc_outbound_utl; 
/


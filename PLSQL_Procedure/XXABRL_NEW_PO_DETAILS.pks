CREATE OR REPLACE PACKAGE APPS.xxabrl_new_po_details
IS
   PROCEDURE xxabrl_main_pkg (
      errbuf    OUT      VARCHAR2,
      retcode   OUT      NUMBER,
      p_type    IN       VARCHAR2
   );

   PROCEDURE xxabrl_pos_daily_data;

   PROCEDURE xxabrl_pos_lines;

   PROCEDURE xxabrl_pos_loc_dist_det;

   PROCEDURE xxabrl_pos_action_history_det;

   PROCEDURE xxabrl_po_taxes;
END xxabrl_new_po_details; 
/


CREATE OR REPLACE PACKAGE BODY APPS.gst_abrl_wct_invoice_pkg
AS
   PROCEDURE gst_abrl_wct_inv_proc (
      errbuff             OUT      VARCHAR2,
      retcode             OUT      NUMBER,
      p_vendor_name       IN       NUMBER,
      p_vendor_num_from   IN       VARCHAR2,
      p_vendor_num_to     IN       VARCHAR2,
      p_vendor_site_num   IN       VARCHAR2,
      p_tds_section       IN       VARCHAR2,
      p_inv_date_from     IN       VARCHAR2,
      p_inv_date_to       IN       VARCHAR2,
      p_cancelled_flag    IN       VARCHAR2,
      p_org_id            IN       NUMBER
   )
   AS
      CURSOR cur_org_id (cp_resp_id NUMBER)
      IS
         SELECT organization_id
           FROM fnd_profile_option_values fpop,
                fnd_profile_options fpo,
                per_security_organizations_v pso
          WHERE fpop.profile_option_id = fpo.profile_option_id
            AND level_value = cp_resp_id                    --52299 -- resp id
            AND pso.security_profile_id = fpop.profile_option_value
            AND profile_option_name = 'XLA_MO_SECURITY_PROFILE_LEVEL';

-- variable declaration
      query_string             LONG;

      TYPE ref_cur IS REF CURSOR;

      c                        ref_cur;
      cancelled_string         VARCHAR2 (1500);
      approved_string          VARCHAR2 (1500);
      list_ven_string          LONG;
      range_ven_string         LONG;
      ven_site_code            LONG;
      tds_section              LONG;
      range_inv_date_string    LONG;
      order_by_string          LONG;
      list_org_id              LONG;
      para_org_id              LONG;
      list_org_cond            VARCHAR2 (100);
      v_org_id                 NUMBER          := 0;
      v_rec_section_code       LONG;
      inv_count                NUMBER          := 0;
      inv_date_from            DATE;
      inv_date_to              DATE;
      ven_count                NUMBER          := 0;
      m_ou_flag                VARCHAR2 (1)    := 'N';
      v_respid                 NUMBER         := fnd_profile.VALUE ('RESP_ID');
      v_vendor_name_display    LONG;
-- summery parameters declaration
      v_tot_inv_amt            NUMBER;                               --(30,2);
      v_tot_tds_amt            NUMBER;                               --(30,2);
      v_tot_basic              NUMBER;                               --(30,2);
      v_tot_net_amt_due        NUMBER;                               --(30,2);
-- grand total parameter declaration
      v_grnd_tot_inv_amt       NUMBER;                               --(30,2);
      v_grnd_tot_tds_amt       NUMBER;                               --(30,2);
      v_grnd_tot_basic         NUMBER;                               --(30,2);
      v_grnd_tot_net_amt_due   NUMBER;                               --(30,2);

-- columns to display on report
      TYPE c_rec IS RECORD (
         v_org_name              org_organization_definitions.organization_name%TYPE,
         v_vendor_number         po_vendors.segment1%TYPE,
         v_vendor_name           po_vendors.vendor_name%TYPE,
         v_vendor_site_code      po_vendor_sites_all.vendor_site_code%TYPE,
         v_invoice_type          ap_invoices_all.invoice_type_lookup_code%TYPE,
         v_invoice_number        ap_invoices_all.invoice_num%TYPE,
         v_gl_date               ap_invoices_all.gl_date%TYPE,
         v_invoice_amount        ap_invoices_all.invoice_amount%TYPE,
         v_invoice_line_amount   ap_invoice_lines_all.amount%TYPE,
         v_tds_tax_rate          jai_tax_lines_v.tax_rate_percentage%TYPE,
         v_tds_section           jai_ap_tds_invoices.tds_section%TYPE,
         v_tds_tax_name          jai_tax_lines_v.tax_rate_name%TYPE,
         v_tds_amount            jai_ap_tds_invoices.tds_amount%TYPE,
         v_base_amount           NUMBER,                            --(30,2),
         v_net_amount_due        NUMBER,                            --(30,2),
         v_tds_inv_number        ap_invoices_all.invoice_num%TYPE,
        --v_tds_inv_status        VARCHAR2 (1500),
        v_Approval_status VARCHAR2 (1500),
         v_creation_date         DATE,
         v_user_name             VARCHAR2 (1500),
         v_pan_no                VARCHAR2 (50),
         v_voucher_num           ap_invoices_all.doc_sequence_value%TYPE,
         v_po_number             po_headers_all.segment1%TYPE,
         v_po_date               po_headers_all.creation_date%TYPE,
         v_address               VARCHAR2 (300),
         -- V_VAT_Tin_NO             jai_party_reg_lines.registration_number%type
         v_vendor_gstin_no       jai_party_reg_lines.registration_number%TYPE
      );

      v_rec                    c_rec;
      v_group_by               VARCHAR2 (2000);
/*
In the captioned report, the base amount that is picked up is the total invoice amount entered in that particular bill.
In case where there are different line items and different WCT rates attached to each line amount, the base amount is then incorrect,
as it the total invoice amount.  In such cases, the base amount should be the particular line amount.
In case of clarification, kindly let me know.
This modification is urgent, kindly let us know as to when the same can be done.
*/

   -- --
   BEGIN
-- query string for the WCT
      query_string :=
         'select ood.ORGANIZATION_NAME,
           pv.segment1 vendor_number,
         pv.vendor_name vendor_name,
         povs.vendor_site_code vendor_site,
         api.INVOICE_TYPE_LOOKUP_CODE Invoice_type,
         REPLACE(api.invoice_num,CHR(9),'''') invoice_number,
         apida.ACCOUNTING_DATE,
         api.invoice_amount invoice_amount,
         sum(apida.amount) invoice_dist_amount,
          -- JCTA.TAX_RATE,
           jlt.tax_rate_percentage tax_rate,
            jati.tds_section section_code,
       --    JCTA.SECTION_CODE,
         --  JCTA.TAX_NAME,
            jlt.tax_rate_name TAX_NAME,
           decode(api.invoice_amount,0,0,NVL(JATI.TDS_AMOUNT,0)) tds_amount,
            DECODE (api.invoice_amount,
                 0, 0,
                 CEIL (jati.tds_amount
                       / NVL (DECODE (jlt.tax_rate_percentage, 0, 1), 1)
                      )
                ) base_amount,    
           decode(api.invoice_amount,0,0,NVL(api.invoice_amount,0)-NVL(JATI.TDS_AMOUNT,0)) Net_amount_due,
           REPLACE(jati.tds_invoice_num,CHR(9),'''') tds_invoice_num,
       /*    ap_invoices_pkg.get_approval_status
                     (api_tds.invoice_id,
                      api_tds.invoice_amount,
                      api_tds.payment_status_flag,
                      api_tds.invoice_type_lookup_code
                     )  Approval_status */
                     '''' Approval_status,
                     api.CREATION_DATE
                     ,fu.USER_NAME,
                    (SELECT DISTINCT jprl.registration_number
                     FROM apps.jai_party_regs_v jpr,
                          apps.jai_party_reg_lines jprl
                    WHERE 1 = 1
                      AND jpr.party_reg_id = jprl.party_reg_id
                      AND pv.vendor_id = jp.party_id
                      AND jpr.reg_class_code =''THIRD_PARTY''
                      AND jprl.registration_type_code =''PAN''
                      AND jpr.party_id = pv.vendor_id
                      AND jpr.party_site_id = povs.vendor_site_id
                      AND jpr.party_reg_id = jp.party_reg_id) vendor_pan_no,
          api_VN.DOC_SEQUENCE_VALUE VOUCHER_NUM,     
         ph.SEGMENT1 po_number, 
         trunc(ph.CREATION_DATE) PO_DATE, 
    povs.address_LINE1||povs.address_LINE2||povs.address_LINE3||povs.ADDRESS_LINE4||'',''||povs.CITY||'',''||povs.STATE||'',''||povs.ZIP Address,
     (SELECT distinct registration_number
                             FROM apps.jai_party_regs_v jpr,
                                  jai_party_reg_lines jprl
                            WHERE 1 = 1
                              AND jpr.party_reg_id = jprl.party_reg_id
                              AND jpr.party_id = pv.vendor_id
                              AND jpr.party_site_id = povs.vendor_site_id
                              and jpr.PARTY_REG_ID=jp.PARTY_REG_ID
                              AND registration_type_code =''GSTIN''
                              AND jpr.reg_class_code =''THIRD_PARTY'')
                                                              vendor_gstin_no                       
  from  apps.ap_invoices_all api
       ,apps.ap_invoice_lines_all apil
       ,apps.ap_invoices_all api_VN  
       ,apps.po_vendors pv
       ,apps.po_vendor_sites_all povs
       ,apps.ap_invoice_distributions_all apida
       ,apps.JAI_AP_TDS_INVOICES jati
       , apps.org_organization_definitions OOD 
       ,apps.ap_invoices_all api1
      -- ,apps.ap_invoices_all api_tds
      ,apps.fnd_user fu,
       jai_tax_lines_v jlt,
         jai_party_regs jp,
      apps.po_headers_all ph        
      ,apps.po_distributions_all pda  
    where api.invoice_id=apil.invoice_id
    AND apida.invoice_id = api.invoice_id
    AND apida.invoice_line_number = apil.line_number
  --  and jcta.TAX_ID=apida.GLOBAL_ATTRIBUTE2
    AND jati.invoice_id=api.invoice_id
   -- and JATI.TDS_TAX_ID=apida.GLOBAL_ATTRIBUTE2
   AND api.invoice_id = jlt.trx_id
    and pv.vendor_id = povs.vendor_id
    AND    api.vendor_id = pv.vendor_id
    AND    api.vendor_site_id = povs.vendor_site_id
    AND    OOD.ORGANIZATION_ID = api.ORG_ID   
    and    fu.USER_ID =api.CREATED_BY
    AND    apida.parent_invoice_id = api1.invoice_id(+)
    AND    ph.PO_HEADER_ID(+)=pda.PO_HEADER_ID 
    AND    apida.PO_DISTRIBUTION_ID=pda.PO_DISTRIBUTION_ID(+) 
    and    api_VN.invoice_num=jati.tds_invoice_num 
     AND pv.vendor_id = jp.party_id
     AND povs.vendor_site_id = jp.party_site_id';
-- settign where conditions according to parameters
      list_ven_string := 'AND pv.vendor_ID = ' || p_vendor_name || ' ';
      range_ven_string :=
            'AND pv.segment1 between '
         || p_vendor_num_from
         || ' AND '
         || p_vendor_num_to
         || ' ';
      ven_site_code :=
               'AND povs.vendor_site_code  = ''' || p_vendor_site_num || ''' ';
      tds_section := 'AND jati.tds_section = ''' || p_tds_section || ''' ';

      SELECT TO_DATE (p_inv_date_from, 'yyyy/mm/dd:HH24:MI:SS')
        INTO inv_date_from
        FROM DUAL;

      SELECT TO_DATE (p_inv_date_to, 'yyyy/mm/dd:HH24:MI:SS')
        INTO inv_date_to
        FROM DUAL;

      range_inv_date_string :=
            '   AND apida.ACCOUNTING_DATE  BETWEEN '''
         || inv_date_from
         || ''' AND '''
         || inv_date_to
         || ''' ';
      v_group_by :=
         'group by 
    ood.ORGANIZATION_NAME,pv.segment1 ,
         pv.vendor_name ,
         povs.vendor_site_code ,
          PV.VENDOR_ID,
         POVS.VENDOR_SITE_ID,
         api.INVOICE_TYPE_LOOKUP_CODE ,
        REPLACE(api.invoice_num,CHR(9),'''') ,
         apida.ACCOUNTING_DATE,
         jati.tds_amount,
          jlt.tax_rate_name, 
         api.invoice_amount ,
           jati.tds_section, 
           NVL(JATI.TDS_AMOUNT,0) ,
           jlt.tax_rate_percentage ,
           jp.party_id,
            jp.party_reg_id,
          REPLACE(jati.tds_invoice_num,CHR(9),'''') ,
           jati.tds_invoice_id ,
           api.CREATION_DATE,
           fu.USER_NAME,
           api_VN.DOC_SEQUENCE_VALUE , 
           ph.SEGMENT1,
           trunc(ph.CREATION_DATE),
         (povs.address_LINE1||povs.address_LINE2||povs.address_LINE3||povs.ADDRESS_LINE4||'',''||povs.CITY||'',''||povs.STATE||'',''||povs.ZIP)';
          
      order_by_string :=
          'ORDER BY OOD.ORGANIZATION_name, jati.tds_section, pv.VENDOR_NAME';

      IF p_cancelled_flag IS NOT NULL
      THEN
         SELECT    (SELECT DECODE
                              (p_cancelled_flag,
                               'Y', 'and ap_invoices_pkg.get_approval_status
                     (api.invoice_id,
                      api.invoice_amount,
                      api.payment_status_flag,
                      api.invoice_type_lookup_code
                     ) =',
                               'N', 'and ap_invoices_pkg.get_approval_status
                     (api.invoice_id,
                      api.invoice_amount,
                      api.payment_status_flag,
                      api.invoice_type_lookup_code
                     ) <>'
                              )
                      FROM DUAL)
                || ''''
                || 'CANCELLED'
                || ''''
           INTO cancelled_string
           FROM DUAL;
      END IF;

      fnd_file.put_line (fnd_file.LOG,
                         'string: ' || cancelled_string || approved_string
                        );
---------------------------------- ------------------
      fnd_file.put_line (fnd_file.LOG, 'executing query ');

-- making final query according to parameters
      IF p_vendor_name IS NOT NULL
      THEN
         query_string := query_string || list_ven_string;
      END IF;

      IF p_vendor_num_from IS NOT NULL AND p_vendor_num_to IS NOT NULL
      THEN
         query_string := query_string || range_ven_string;
      END IF;

      IF p_vendor_site_num IS NOT NULL
      THEN
         query_string := query_string || ven_site_code;
      END IF;

      IF p_tds_section IS NOT NULL
      THEN
         query_string := query_string || tds_section;
      END IF;

      IF p_inv_date_from IS NOT NULL AND p_inv_date_to IS NOT NULL
      THEN
         query_string := query_string || range_inv_date_string;
      END IF;

--- VALIDATION FOR ORG ID
      IF p_org_id IS NOT NULL
      THEN
         --- this will satisfied if user will select organization in parameter
         list_org_id := ' AND    OOD.ORGANIZATION_ID = ' || p_org_id || '  ';
         fnd_file.put_line (fnd_file.LOG, 'ORG Selected in parameter');
      ELSE
         -- thia condition will execute for those responsibily having all ou access
         list_org_id := '';
         para_org_id := ' AND    OOD.ORGANIZATION_ID in (';
         fnd_file.put_line
              (fnd_file.LOG,
               'Taking ALL ORGS if run for RESP where ALL OU data accessible'
              );

         FOR k IN cur_org_id (v_respid)
         LOOP
            list_org_id := list_org_id || ',' || k.organization_id;
            -- this flag will set if data comes from all ou
            m_ou_flag := 'Y';
         END LOOP;

         -- this will remove the first comma from the in condition
         list_org_id := SUBSTR (list_org_id, 2, LENGTH (list_org_id));
         list_org_id := para_org_id || list_org_id || ') ';

         IF m_ou_flag = 'N'
         THEN
            -- this condition will satisfy if its not multi ou resp.
            SELECT fpop.profile_option_value
              INTO v_org_id
              FROM fnd_profile_option_values fpop, fnd_profile_options fpo
             WHERE fpop.profile_option_id = fpo.profile_option_id
               AND level_value = v_respid                              --51638
               AND profile_option_name = 'ORG_ID';

            fnd_file.put_line
               (fnd_file.LOG,
                'Responsibility don''t have all ou access so taking default OU'
               );
            list_org_id :=
                        ' AND    OOD.ORGANIZATION_ID =   ' || v_org_id || '  ';
         END IF;
      END IF;

      query_string :=
            query_string
         || cancelled_string
         || approved_string
         || list_org_id
         || v_group_by
         || order_by_string;

-- parameter printing
      BEGIN
         SELECT vendor_name
           INTO v_vendor_name_display
           FROM po_vendors
          WHERE vendor_id = p_vendor_name;
                                         -- p_vendor_name is passing vendor id
      EXCEPTION
         WHEN OTHERS
         THEN
            v_vendor_name_display := '';
      END;

      fnd_file.put_line (fnd_file.output, 'ABRL WCT Invoice Report');
      fnd_file.put_line (fnd_file.output, ' ');
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'As on Date'
                         || CHR (9)
                         || SYSDATE
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output, ' ');
      fnd_file.put_line (fnd_file.output, 'REPORT PARAMETRS');
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Vendor Name'
                         || CHR (9)
                         || v_vendor_name_display
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Vendor From'
                         || CHR (9)
                         || p_vendor_num_from
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Vendor To'
                         || CHR (9)
                         || p_vendor_num_to
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Vendor Site Code'
                         || CHR (9)
                         || p_vendor_site_num
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Section Code'
                         || CHR (9)
                         || p_tds_section
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'GL Date From'
                         || CHR (9)
                         || SUBSTR (p_inv_date_from, 1, 11)
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'GL Date To'
                         || CHR (9)
                         || SUBSTR (p_inv_date_to, 1, 11)
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'CANCELLED FLAG'
                         || CHR (9)
                         || p_cancelled_flag
                         || CHR (9)
                        );
-- display labels of the report
      fnd_file.put_line (fnd_file.output, ' ');
      fnd_file.put_line (fnd_file.output, ' ');
      fnd_file.put_line (fnd_file.output,
                            'Operating Unit'
                         || CHR (9)
                         || 'Vendor Number'
                         || CHR (9)
                         || 'Voucher Number'
                         || CHR (9)
                         || 'Vendor Name'
                         || CHR (9)
                         || 'Address'
                         || CHR (9)
                         || 'PAN Number'
                         || CHR (9)
                         ||'GSTIN'
                         ||CHR(9)
                         || 'Vendor Site'
                         || CHR (9)
                         || 'Invoice Type'
                         || CHR (9)
                         || 'PO Number'
                         || CHR (9)
                         || 'PO Date'
                         || CHR (9)
                         || 'Invoice Number'
                         || CHR (9)
                         || 'GL Date'
                         || CHR (9)
                         || 'Invoice Amount'
                         || CHR (9)
                         || 'Invoice Line Amount'
                         || CHR (9)
                         || 'WCT Tax Rate'
                         || CHR (9)
                         || 'Section Code'
                         || CHR (9)
                         || 'WCT Tax Name'
                         || CHR (9)
                         || 'WCT Amount'
                         || CHR (9)
                         || 'Basic'
                         || CHR (9)
                         || 'Net Amount Due'                  --pay group code
                         || CHR (9)
                         || 'WCT Invoice Number'
                         || CHR (9)
                         || 'WCT Invoice Status'
                         || CHR (9)
                         || 'User Name'
                         || CHR (9)
                         || 'Creation date'
                        );
-- set default value 0 to all summery cols.
      v_tot_inv_amt := 0;
      v_tot_tds_amt := 0;
      v_tot_basic := 0;
      v_tot_net_amt_due := 0;
      v_grnd_tot_inv_amt := 0;
      v_grnd_tot_tds_amt := 0;
      v_grnd_tot_basic := 0;
      v_grnd_tot_net_amt_due := 0;
      fnd_file.put_line (fnd_file.LOG, query_string);

---------------------------------- ------------------
--Fnd_File.put_line (Fnd_File.output, 'before opening the cursor ');
      OPEN c FOR query_string;

      LOOP
         FETCH c
          INTO v_rec;

         EXIT WHEN c%NOTFOUND;

          ---------------------------------- ------------------
--Fnd_File.put_line (Fnd_File.output, 'executing ');
         IF NVL (v_rec_section_code, 'X') <> v_rec.v_tds_section
         THEN
            IF inv_count > 0
            THEN
               --print total vnd amt chr(9),
               fnd_file.put_line (fnd_file.output,
                                     'TOTAL'
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || TO_CHAR (v_tot_inv_amt)
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || TO_CHAR (v_tot_tds_amt)
                                  || CHR (9)
                                  || TO_CHAR (v_tot_basic)
                                  || CHR (9)
                                  || TO_CHAR (v_tot_net_amt_due)
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                 );
            END IF;

            v_rec_section_code := v_rec.v_tds_section;
            v_tot_inv_amt := v_rec.v_invoice_amount;
            v_tot_tds_amt := v_rec.v_tds_amount;
            v_tot_basic := v_rec.v_base_amount;
            v_tot_net_amt_due := v_rec.v_net_amount_due;
         ELSE
            v_tot_inv_amt := v_tot_inv_amt + v_rec.v_invoice_amount;
            v_tot_tds_amt := v_tot_tds_amt + v_rec.v_tds_amount;
            v_tot_basic := v_tot_basic + v_rec.v_base_amount;
            v_tot_net_amt_due := v_tot_net_amt_due + v_rec.v_net_amount_due;
         END IF;

         -- printing the data
         ---------------------------------- ------------------
--Fnd_File.put_line (Fnd_File.output, 'printing');
         fnd_file.put_line (fnd_file.output,
                               v_rec.v_org_name
                            || CHR (9)
                            || v_rec.v_vendor_number
                            || CHR (9)
                            || v_rec.v_voucher_num
                            || CHR (9)
                            || v_rec.v_vendor_name
                            || CHR (9)
                            || v_rec.v_address
                            || CHR (9)
                            || v_rec.v_pan_no
                            || CHR (9)
                            || v_rec.v_vendor_gstin_no
                            || CHR (9)
                            || v_rec.v_vendor_site_code
                            || CHR (9)
                            || v_rec.v_invoice_type
                            || CHR (9)
                            || v_rec.v_po_number        
                            || CHR (9)
                            || v_rec.v_po_date         
                            || CHR (9)
                            || v_rec.v_invoice_number
                            || CHR (9)
                            || v_rec.v_gl_date
                            || CHR (9)
                            || TO_CHAR (v_rec.v_invoice_amount)
                            || CHR (9)
                            || TO_CHAR (v_rec.v_invoice_line_amount)
                            || CHR (9)
                            || v_rec.v_tds_tax_rate
                            || CHR (9)
                            || v_rec.v_tds_section
                            || CHR (9)
                            || v_rec.v_tds_tax_name
                            || CHR (9)
                            || TO_CHAR (v_rec.v_tds_amount)
                            || CHR (9)
                            || TO_CHAR (v_rec.v_base_amount)
                            || CHR (9)
                            || TO_CHAR (v_rec.v_net_amount_due)
                            || CHR (9)
                            || v_rec.v_tds_inv_number
                            || CHR (9)
                            || v_rec.v_Approval_status
                            || CHR (9)
                            || v_rec.v_user_name
                            || CHR (9)
                            || v_rec.v_creation_date
                           );
         ven_count := ven_count + 1;
         inv_count := inv_count + 1;
         v_grnd_tot_inv_amt := v_grnd_tot_inv_amt + v_rec.v_invoice_amount;
         v_grnd_tot_tds_amt := v_grnd_tot_tds_amt + v_rec.v_tds_amount;
         v_grnd_tot_basic := v_grnd_tot_basic + v_rec.v_base_amount;
         v_grnd_tot_net_amt_due :=
                               v_grnd_tot_net_amt_due + v_rec.v_net_amount_due;
      END LOOP;

      fnd_file.put_line (fnd_file.output,
                            'TOTAL'
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || TO_CHAR (v_tot_inv_amt)
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || TO_CHAR (v_tot_tds_amt)
                         || CHR (9)
                         || TO_CHAR (v_tot_basic)
                         || CHR (9)
                         || v_tot_net_amt_due
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                        );
      fnd_file.put_line (fnd_file.output,
                            'Grand Total'
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || TO_CHAR (v_grnd_tot_inv_amt)
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || TO_CHAR (v_grnd_tot_tds_amt)
                         || CHR (9)
                         || TO_CHAR (v_grnd_tot_basic)
                         || CHR (9)
                         || TO_CHAR (v_grnd_tot_net_amt_due)
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                        );

      CLOSE c;
---------------------------------- ------------------

   --Fnd_File.put_line (Fnd_File.output, 'amount closing the cursor'||v_grnd_tot_inv_amt );
   END gst_abrl_wct_inv_proc;
END gst_abrl_wct_invoice_pkg; 
/


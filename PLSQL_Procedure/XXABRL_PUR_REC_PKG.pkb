CREATE OR REPLACE PACKAGE BODY APPS.xxabrl_pur_rec_pkg AS



/*
=========================================================================================================
||   Filename   : xxabrl_pur_rec_pkgsql
||   Description : Script is used to Load GL Data (GL Conversion)
||
||   Version     Date            Author              Modification
||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
||   1.0.0        23-mar-2010    Hansraj Kasana      New Development
||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
||
||   Usage : This script is used to upload data in apps.GL_INTERFACE TABLE
||
========================================================================================================*/


 PROCEDURE xxabrl_main_pkg     (ERRBUFF             OUT VARCHAR2,
                                RETCODE             OUT NUMBER,
                                p_Type              IN Varchar2 ,
                                p_gl_from_date      IN Varchar2 ,
                                p_gl_to_date        IN Varchar2 ,
                                p_gl_acct_from  IN Varchar2,
                                p_gl_acct_to  IN Varchar2
                                ) as
begin
   Fnd_File.put_line (Fnd_File.log,p_Type   ||' <-> '||p_gl_from_date  ||' <-> '|| p_gl_to_date);

 if p_type ='R' then
    xxabrl_receiving_proc(p_gl_from_date,p_gl_to_date,p_gl_acct_from,p_gl_acct_to);
 end if;
 
 if p_type ='P' then
    xxabrl_puchasing_proc(p_gl_from_date,p_gl_to_date,p_gl_acct_from,p_gl_acct_to);
 end if;
end xxabrl_main_pkg;
 
 

  
  -- procedure for receiving india
  PROCEDURE xxabrl_receiving_proc(p_gl_from_date               IN Varchar2 ,
                                p_gl_to_date               IN Varchar2,
                                p_gl_acct_from  IN Varchar2,
                                p_gl_acct_to  IN Varchar2)  as
  
  
  cursor cur_rec is
  SELECT     GLB.NAME batch_name, 
       glh.je_source SOURCE, glh.je_category CATEGORY,     
       glh.default_effective_date gl_date,     
       gll.je_line_num line_number,     
       gl.concatenated_segments account_code,     
       (SELECT FFVL.description FROM apps.fnd_flex_values_vl ffvl     
        WHERE ffvl.FLEX_VALUE_SET_ID=1013466     
        AND ffvl.FLEX_VALUE=gl.SEGMENT3) SBU_desc,     
         (SELECT FFVL.description FROM apps.fnd_flex_values_vl ffvl     
        WHERE ffvl.FLEX_VALUE_SET_ID=1013467     
        AND ffvl.FLEX_VALUE=gl.SEGMENT4) Location_desc,     
       (SELECT FFVL.description FROM apps.fnd_flex_values_vl ffvl     
        WHERE ffvl.FLEX_VALUE_SET_ID=1013469     
        AND ffvl.FLEX_VALUE=gl.SEGMENT6) account_desc,     
      GLL.ACCOUNTED_DR dr_amount,     
      GLL.ACCOUNTED_CR cr_amount,     
      gll.description journal_description,     
      NULL customer_name,     
      NULL customer_Number,     
       glh.doc_sequence_value document_number,     
       DECODE (GLB.status,     
               'P', 'Posted',     
               'U', 'Unposted',     
               GLB.status     
              ) batch_status,     
     fu.user_name Created_By,     
     gl.segment3 SBU,     
     gl.segment4 LOCATION,     
     gl.segment6 GL_account,     
     GLH.LEDGER_ID LEDGER_ID,
     poh.segment1 po_number     
     ,pv.segment1 vendor_number 
     ,pvs.vendor_site_code 
     ,rsh.receipt_num  GRN_NO 
    -- ,rsl.ITEM_DESCRIPTION 
     ,rt.po_unit_price*rt.PRIMARY_QUANTITY Amount 
     ,xah.GL_TRANSFER_STATUS_CODE 
FROM     
       apps.gl_je_headerS glh,     
       apps.gl_je_lines gll,     
       apps.gl_code_combinations_kfv gl,     
       apps.fnd_user fu,     
       apps.gl_je_batches GLB     
         ,apps.xla_ae_lines xla 
       ,    apps.gl_import_references glir 
       , APPS.XLA_AE_HEADERS XAH 
       ,xla.xla_transaction_entities XTE 
       ,apps.RCV_TRANSACTIONS RT 
       ,APPS.PO_HEADERS_ALL POH 
       ,apps.po_vendors pv 
       ,apps.po_vendor_sites_all pvs 
       ,apps.rcv_shipment_headers rsh 
    -- ,  apps.rcv_shipment_lines rsl 
WHERE glh.je_header_id = gll.je_header_id     
  AND gl.code_combination_id = gll.code_combination_id     
  AND glh.je_batch_id = GLB.je_batch_id     
  AND gll.created_by=fu.user_id     
  AND UPPER(glh.je_source)  IN ('COST MANAGEMENT')     
    AND gll.je_header_id = glir.je_header_id (+)     
 AND gll.je_line_num = glir.je_line_num (+)     
   AND glir.gl_sl_link_id = xla.gl_sl_link_id (+)     
 AND glir.gl_sl_link_table = xla.gl_sl_link_table (+)     
 AND NVL(xla.accounted_cr,999999)!=0     
   AND NVL(xla.accounted_dr,999999)!=0     
    AND XAH.AE_HEADER_ID=XLA.AE_HEADER_ID     
    AND xte.entity_id=xah.entity_id 
--    AND glb.NAME LIKE 'Cost Management A 1019029 26839535' 
  AND glh.je_category='Receiving'     
AND POH.PO_HEADER_ID=RT.PO_HEADER_ID     
AND XTE.SOURCE_ID_INT_1=TO_NUMBER(rt.TRANSACTION_ID)     
AND rt.vendor_id=pv.vendor_id     
AND rt.vendor_site_id=pvs.vendor_site_id     
AND rsh.shipment_header_id=rt.shipment_header_id
and glh.default_effective_date between nvl(to_date(p_gl_from_date,'yyyy/mm/dd hh24:mi:ss'),glh.default_effective_date)  
and nvl(to_date(p_gl_to_date ,'yyyy/mm/dd hh24:mi:ss'),glh.default_effective_date)
and gl.segment6 between nvl(p_gl_acct_from,gl.segment6)  and  nvl(p_gl_acct_to,gl.segment6);
--and rownum<10;
    
  begin 
  
   Fnd_File.put_line (Fnd_File.output, 'BATCH_NAME'   ||'|'||
                                      'SOURCE'       ||'|'||
                                      'CATEGORY'     ||'|'||
                                      'GL DATE'      ||'|'||
                                      'LINE NUMBER'  ||'|'||
                                      'ACCOUNT CODE' ||'|'||
                                      'SBU DESC'     ||'|'||
                                      'LOCATION DESC'||'|'||
                                      'ACCOUNT DESC' ||'|'||
                                      'DR AMOUNT'    ||'|'||
                                      'CR AMOUNT'    ||'|'||
                                      'JOURNAL DESCRIPTION' ||'|'||
                                      'DOCUMENT NUMBER'     ||'|'||
                                      'BATCH STATUS'        ||'|'||
                                      'CREATED BY'          ||'|'||
                                      'SBU'                 ||'|'||
                                      'LOCATION'            ||'|'||
                                      'GL ACCOUNT'          ||'|'||
                                      'LEDGER ID'           ||'|'||
                                      'PO NUMBER'           ||'|'||
                                      'VENDOR NUMBER'             ||'|'||
                                      'VENDOR SITE CODE'    ||'|'||
                                      'GRN NO'     ||'|'|| 
                                      'AMOUNT'        ||'|'|| 
                                      'GL TRANSFER STATUS CODE');
  
  for i in cur_rec  loop
  

  
        Fnd_File.put_line (Fnd_File.output,I.BATCH_NAME   ||'|'||
                                      I.SOURCE       ||'|'||
                                      I.CATEGORY     ||'|'||
                                      I.GL_DATE      ||'|'||
                                      I.LINE_NUMBER  ||'|'||
                                      I.ACCOUNT_CODE ||'|'||
                                      I.SBU_DESC     ||'|'||
                                      I.LOCATION_DESC||'|'||
                                      I.ACCOUNT_DESC ||'|'||
                                      I.DR_AMOUNT    ||'|'||
                                      I.CR_AMOUNT    ||'|'||
                                      I.JOURNAL_DESCRIPTION ||'|'||
                                      I.DOCUMENT_NUMBER     ||'|'||
                                      I.BATCH_STATUS        ||'|'||
                                      I.CREATED_BY          ||'|'||
                                      I.SBU                 ||'|'||
                                      I.LOCATION            ||'|'||
                                      I.GL_ACCOUNT          ||'|'||
                                      I.LEDGER_ID           ||'|'||
                                      i.PO_NUMBER           ||'|'||
                                      i.VENDOR_NUMBER             ||'|'||
                                      i.VENDOR_SITE_CODE    ||'|'||
                                      i.GRN_NO     ||'|'|| 
                                      i.AMOUNT        ||'|'|| 
                                      i.GL_TRANSFER_STATUS_CODE);
  
   end loop;
    
  
    end  xxabrl_receiving_proc;
    
   -- procedure for purchasing india 
  PROCEDURE xxabrl_puchasing_proc (p_gl_from_date               IN Varchar2 ,
                                p_gl_to_date               IN Varchar2,
                                p_gl_acct_from  IN Varchar2,
                                p_gl_acct_to  IN Varchar2 )as
   
cursor   cur_pur is
SELECT GLB.NAME batch_name,    
       glh.je_source SOURCE, glh.je_category CATEGORY,    
       glh.default_effective_date gl_date,    
       gll.je_line_num line_number,    
       gl.concatenated_segments account_code,    
       (SELECT FFVL.description FROM apps.fnd_flex_values_vl ffvl    
        WHERE ffvl.FLEX_VALUE_SET_ID=1013466    
        AND ffvl.FLEX_VALUE=gl.SEGMENT3) SBU_desc,    
         (SELECT FFVL.description FROM apps.fnd_flex_values_vl ffvl    
        WHERE ffvl.FLEX_VALUE_SET_ID=1013467    
        AND ffvl.FLEX_VALUE=gl.SEGMENT4) Location_desc,    
       (SELECT FFVL.description FROM apps.fnd_flex_values_vl ffvl    
        WHERE ffvl.FLEX_VALUE_SET_ID=1013469    
        AND ffvl.FLEX_VALUE=gl.SEGMENT6) account_desc,    
      GLL.ACCOUNTED_DR dr_amount,    
      GLL.ACCOUNTED_CR cr_amount,    
      gll.description journal_description,    
      NULL customer_name,    
      NULL customer_Number,    
       glh.doc_sequence_value document_number,    
       DECODE (GLB.status,    
               'P', 'Posted',    
               'U', 'Unposted',    
               GLB.status    
              ) batch_status,    
     fu.user_name Created_By,    
     gl.segment3 SBU,    
     gl.segment4 LOCATION,    
     gl.segment6 GL_account,    
     GLH.LEDGER_ID LEDGER_ID    
             ,poh.segment1 po_number    
              ,rt.po_unit_price*rt.PRIMARY_QUANTITY Amount     
               ,pv.segment1 vendor_number     
             ,pvs.vendor_site_code     
             ,rsh.receipt_num  GRN_NO     
FROM    
       apps.gl_je_headerS glh,    
       apps.gl_je_lines gll,    
       apps.gl_code_combinations_kfv gl,    
       apps.fnd_user fu,    
       apps.gl_je_batches GLB    
             --    ,apps.xla_ae_lines xla     
               ,    apps.gl_import_references glir     
               ,apps.rcv_transactions rt    
,apps.po_headers_all poh    
  ,apps.rcv_shipment_headers rsh     
  ,apps.po_vendors pv    
  ,apps.po_vendor_sites_all pvs     
WHERE glh.je_header_id = gll.je_header_id    
  AND gl.code_combination_id = gll.code_combination_id    
  AND glh.je_batch_id = GLB.je_batch_id    
  AND gll.created_by=fu.user_id    
--  AND glb.NAME ='982 Purchasing India A 23547973 3'    
  AND UPPER(glh.je_source) = 'PURCHASING INDIA'    
     AND gll.je_header_id = glir.je_header_id (+)     
 AND gll.je_line_num = glir.je_line_num (+)     
 AND rt.transaction_id=glir.reference_5    
 AND poh.po_header_id=rt.po_header_id    
 AND rsh.shipment_header_id=rt.shipment_header_id     
 AND rt.vendor_id=pv.vendor_id     
AND rt.vendor_site_id=pvs.vendor_site_id
and glh.default_effective_date between nvl(to_date(p_gl_from_date,'yyyy/mm/dd hh24:mi:ss'),glh.default_effective_date)  
and nvl(to_date(p_gl_to_date ,'yyyy/mm/dd hh24:mi:ss'),glh.default_effective_date)
and gl.segment6 between nvl(p_gl_acct_from,gl.segment6)  and  nvl(p_gl_acct_to,gl.segment6);
--and rownum<10 ;

  begin
  Fnd_File.put_line (Fnd_File.output, 'BATCH_NAME'   ||'|'||
                                      'SOURCE'       ||'|'||
                                      'CATEGORY'     ||'|'||
                                      'GL DATE'      ||'|'||
                                      'LINE NUMBER'  ||'|'||
                                      'ACCOUNT CODE' ||'|'||
                                      'SBU DESC'     ||'|'||
                                      'LOCATION DESC'||'|'||
                                      'ACCOUNT DESC' ||'|'||
                                      'DR AMOUNT'    ||'|'||
                                      'CR AMOUNT'    ||'|'||
                                      'JOURNAL DESCRIPTION' ||'|'||
                                      'DOCUMENT NUMBER'     ||'|'||
                                      'BATCH STATUS'        ||'|'||
                                      'CREATED BY'          ||'|'||
                                      'SBU'                 ||'|'||
                                      'LOCATION'            ||'|'||
                                      'GL ACCOUNT'          ||'|'||
                                      'LEDGER ID'           ||'|'||
                                      'PO NUMBER'           ||'|'||
                                      'AMOUNT'              ||'|'||
                                      'VENDOR NUMBER'       ||'|'||
                                      'VENDOR SITE CODE'    ||'|'||
                                      'GRN NO'  );
  
  for i in cur_pur  loop
  
  
  
        Fnd_File.put_line (Fnd_File.output,I.BATCH_NAME   ||'|'||
                                      I.SOURCE       ||'|'||
                                      I.CATEGORY     ||'|'||
                                      I.GL_DATE      ||'|'||
                                      I.LINE_NUMBER  ||'|'||
                                      I.ACCOUNT_CODE ||'|'||
                                      I.SBU_DESC     ||'|'||
                                      I.LOCATION_DESC||'|'||
                                      I.ACCOUNT_DESC ||'|'||
                                      I.DR_AMOUNT    ||'|'||
                                      I.CR_AMOUNT    ||'|'||
                                      I.JOURNAL_DESCRIPTION ||'|'||
                                      I.DOCUMENT_NUMBER     ||'|'||
                                      I.BATCH_STATUS        ||'|'||
                                      I.CREATED_BY          ||'|'||
                                      I.SBU                 ||'|'||
                                      I.LOCATION            ||'|'||
                                      I.GL_ACCOUNT          ||'|'||
                                      I.LEDGER_ID           ||'|'||
                                      I.PO_NUMBER           ||'|'||
                                      I.AMOUNT              ||'|'||
                                      I.VENDOR_NUMBER       ||'|'||
                                      I.VENDOR_SITE_CODE    ||'|'||
                                      I.GRN_NO  );
  
   end loop;
    
  end xxabrl_puchasing_proc;

END  xxabrl_pur_rec_pkg; 
/


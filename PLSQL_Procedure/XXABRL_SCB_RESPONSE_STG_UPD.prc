CREATE OR REPLACE PROCEDURE APPS.XXABRL_SCB_RESPONSE_STG_UPD(ERRBUF  OUT VARCHAR2,
                                                        RETCODE OUT VARCHAR2) AS
 /**********************************************************************************************************************************************
                        WIPRO Infotech Ltd, Mumbai, India

    Name        : ABRL DBS Response Program 
    Description: This Procedure will update the DBS_PAYMENT_TABLE based on the DBS_RETURN_STATUS table (response file sent by the DBS Bank)

    Change Record:
   ========================
=========================
=========================
=========================
======================
   Version   Date          Author                    Remarks                  Documnet Ref
   =======   ==========   =====
========             ==================  ==================
=========================
==========
   1.0.0     17-Jun-2013   Dhiresh More      Initial Version
   1.0.1     04-Jul-2013   Dhiresh More      changes done for get update required value in 
                attribute1 and attribute2 from revers file 
                received from Standard Charterd Bank

          
  ************************************************************************************************************************************************/

    /*V_CHECK_ID    NUMBER;
    V_TRANSACTION_REF_NO  VARCHAR2(25);
    V_STATUS      VARCHAR2(50);
    V_DESCRIPTION VARCHAR2(250);*/
    
-- Response Cursor    
CURSOR CUR_RES IS
            SELECT SRS.* FROM APPS.SCB_RETURN_STATUS SRS, APPS.SCB_PAYMENT_TABLE SPT
            WHERE SRS.CHECK_ID = SPT.CHECK_ID
            AND TRUNC(SRS.INTERFACE_DATE)=TRUNC(SYSDATE)
            AND SRS.INTERFACE_FLAG='N'; 
 BEGIN
 
    FOR REC_RES IN CUR_RES
    LOOP
        BEGIN
         /*SELECT SRS.CHECK_ID,SRS.TRANSACTION_REF_NO,SRS.STATUS,SRS.DESCRIPTION 
            INTO V_CHECK_ID,V_TRANSACTION_REF_NO,V_STATUS,V_DESCRIPTION
            FROM APPS.SCB_RETURN_STATUS SRS
            WHERE TRUNC(SRS.INTERFACE_DATE)=TRUNC(SYSDATE)
                  AND SRS.INTERFACE_FLAG='N';*/
            
            -- Updating Payment Table with the status sent by SCB Bank      
            UPDATE APPS.SCB_PAYMENT_TABLE SPT
            SET SPT.ATTRIBUTE1 = DECODE(REC_RES.STATUS,'PROCESSED','SUCCESS',REC_RES.STATUS)    -- Changes done by Dhiresh on 04-Jul-2013
               ,SPT.ATTRIBUTE2 = DECODE(REC_RES.STATUS,'PROCESSED','SUCCESS',REC_RES.DESCRIPTION)    -- Changes done by Dhiresh on 04-Jul-2013
               ,SPT.ATTRIBUTE3 = REC_RES.TRANSACTION_REF_NO
               ,SPT.ATTRIBUTE_CATEGORY='SCB Additional Information'
               WHERE SPT.CHECK_ID = REC_RES.CHECK_ID
               AND SPT.TRANSACTION_STATUS = 'PROCESSED';
            
            --Updating Return Status table with interface flag 'Y'
            UPDATE  APPS.SCB_RETURN_STATUS SRS
            SET INTERFACE_FLAG = 'Y'
            WHERE SRS.CHECK_ID = REC_RES.CHECK_ID
            AND INTERFACE_FLAG='N';
            
        EXCEPTION
            WHEN OTHERS THEN
            UPDATE APPS.SCB_RETURN_STATUS SRS
            SET INTERFACE_FLAG='N', REMARKS='Unable to Update Stage Table'
            WHERE SRS.CHECK_ID = REC_RES.CHECK_ID;
            
        DBMS_OUTPUT.PUT_LINE('Unable to Update Stage Table' );
        
        END;
             
    END LOOP;
    
 END XXABRL_SCB_RESPONSE_STG_UPD; 
/


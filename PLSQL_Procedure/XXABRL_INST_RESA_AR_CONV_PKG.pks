CREATE OR REPLACE PACKAGE APPS.XXABRL_INST_RESA_AR_CONV_PKG IS
  /*
  =========================================================================================================
  ||   Filename   : XXABRL_RESA_AR_CONV_PKG.sql
  ||   Description : Script is used to mold ReSA data for AR
  ||
  ||   Version     Date            Author              Modification
  ||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
  ||   1.0.0       22-JAN-2010    Shailesh Bharambe      New Development
  ||   1.0.1       30-JAN-2010    Shailesh Bharambe      Changes in the transaction type query change from IS_NET to IS_SALES
       1.0.2       08-mar-2010   Ravi/Mitul             Added customer class code (Intercompany , Intracompany) and transaction type id
       1.0.3        09-mar-2011   Ravi/Mitul             Removed the transaction type id for IS_TAX, IS_PROMO,IS_INTERTAX<IS_INTRATAX . 
  ||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
  ||
  ||
  ========================================================================================================*/
  PROCEDURE MAIN_PROC(x_err_buf    OUT VARCHAR2,
                      x_ret_code   OUT NUMBER,
                      p_action     IN Varchar2,
                      p_debug_flag IN Varchar2);
  PROCEDURE Print_log(p_msg in varchar2, p_debug_flag varchar2);
  PROCEDURE Validate_ReSA(p_debug_flag in varchar2, p_ret_code OUT NUMBER);
  PROCEDURE Insert_ReSA(p_source_name IN varchar2, p_ret_code OUT NUMBER);
 /* PROCEDURE Validate_customer(p_store     in varchar2,
                              p_rowid     varchar2,
                              p_org_id    varchar2,
                              p_error_msg out varchar2);*/
  PROCEDURE Validate_Currency_code(p_curr_code in varchar2,
                                   p_error_msg out varchar2);
  PROCEDURE Validate_Curr_Conv_code(p_curr_conv_code in varchar2,
                                    p_error_msg      out varchar2);
  PROCEDURE Validate_CCID(p_CCID      in varchar2,
                          p_rowid     varchar2,
                          p_error_msg out varchar2);
  PROCEDURE Derive_Tran_Type(p_Resa_cur     IN  xxabrl_inst_resa_ar_int%ROWTYPE,
                             p_Nature       out varchar2,
                             p_Tran_Type    out varchar2,
                             p_Description  out varchar2,
                             p_error_msg    out varchar2 
                             );                          
 /* PROCEDURE Derive_Bank_Dtl( p_Store        IN  XXABRL_RESA_AR_INT%ROWTYPE,
                             p_Tran_Type    IN varchar2,
                             x_Bank_Account_Name  out varchar2,              
                             x_Bank_Account_Number    out varchar2,
                             x_description         out varchar2,
                             x_error_msg    out varchar2
                             );     */                     
/* FUNCTION ACCOUNT_SEG_STATUS(P_Seg_Value IN Varchar2,
                              P_Seg_Desc  IN Varchar2) return Varchar2;*/
  g_Org_id   number := fnd_profile.value('ORG_ID');
  v_set_of_bks_id Number := Fnd_Profile.VALUE('GL_SET_OF_BKS_ID');
END XXABRL_INST_RESA_AR_CONV_PKG; 
/


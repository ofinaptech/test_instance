CREATE OR REPLACE PROCEDURE APPS.XXABRL_YES_RESPONSE_STG_UPD(ERRBUF  OUT VARCHAR2,
                                                        RETCODE OUT VARCHAR2) AS
 /**********************************************************************************************************************************************
                        WIPRO Infotech Ltd, Mumbai, India
    Name        : ABRL DBS Response Program
    Description: This Procedure will update the DBS_PAYMENT_TABLE based on the DBS_RETURN_STATUS table (response file sent by the DBS Bank)
    Change Record:
   =========================================================================================================================
   Version   Date          Author                    Remarks                  Documnet Ref
   =======   ==========   =============             ==================  =====================================================
   1.0.0     09-Apr-2013   Dhiresh More      Initial Version
  ************************************************************************************************************************************************/
    /*V_CHECK_ID    NUMBER;
    V_TRANSACTION_REF_NO  VARCHAR2(25);
    V_STATUS      VARCHAR2(50);
    V_DESCRIPTION VARCHAR2(250);*/
-- Response Cursor
CURSOR CUR_RES IS
            SELECT YRS.* FROM APPS.YESBANK_RETURN_STATUS YRS, APPS.YESBANK_PAYMENT_TABLE YPT
            WHERE YRS.TRANSACTION_REF_NO = YPT.CHECK_ID
            AND TRUNC(YRS.INTERFACE_DATE)=TRUNC(SYSDATE)
            AND YRS.INTERFACE_FLAG='N';
 BEGIN
    FOR REC_RES IN CUR_RES
    LOOP
        BEGIN
         /*SELECT DRS.CHECK_ID,DRS.TRANSACTION_REF_NO,DRS.STATUS,DRS.DESCRIPTION
            INTO V_CHECK_ID,V_TRANSACTION_REF_NO,V_STATUS,V_DESCRIPTION
            FROM APPS.DBS_RETURN_STATUS DRS
            WHERE TRUNC(DRS.INTERFACE_DATE)=TRUNC(SYSDATE)
                  AND DRS.INTERFACE_FLAG='N';*/
            -- Updating Payment Table with the status sent by DBS Bank
            UPDATE APPS.YESBANK_PAYMENT_TABLE YPT
            SET YPT.ATTRIBUTE1 = REC_RES.PAYMENT_STATUS
               ,YPT.ATTRIBUTE2 = REC_RES.REJECTION_REASON
               ,YPT.ATTRIBUTE3 = REC_RES.TRACKING_NUMBER
               ,YPT.ATTRIBUTE_CATEGORY=REC_RES.ATTRIBUTE_CATEGORY
               --,DPT.TRANSACTION_STATUS = 'PROCESSED'
            WHERE YPT.CHECK_ID = REC_RES.TRANSACTION_REF_NO
            AND YPT.TRANSACTION_STATUS = 'PROCESSED';
            --Updating Return Status table with interface flag 'Y'
            UPDATE  APPS.YESBANK_RETURN_STATUS YRS
            SET INTERFACE_FLAG = 'Y'
            WHERE YRS.TRANSACTION_REF_NO = REC_RES.TRANSACTION_REF_NO
            AND INTERFACE_FLAG='N';
        EXCEPTION
            WHEN OTHERS THEN
            UPDATE APPS.YESBANK_RETURN_STATUS YRS
            SET INTERFACE_FLAG='N', Attribute10='Unable to Update Stage Table'
            WHERE YRS.TRANSACTION_REF_NO = REC_RES.TRANSACTION_REF_NO;
        DBMS_OUTPUT.PUT_LINE('Unable to Update Stage Table' );
        END;
    END LOOP;
 END XXABRL_YES_RESPONSE_STG_UPD;
/


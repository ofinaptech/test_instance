CREATE OR REPLACE PROCEDURE APPS.gst_abrl_open_grn_prc (
   errbuf        OUT   VARCHAR2,
   retcode       OUT   NUMBER,
   p_from_date         VARCHAR2,
   p_to_date           VARCHAR2,
   p_org_id            VARCHAR2
)
IS
CURSOR xx_rec
   IS
   select ab.* from 
      (SELECT *
        FROM (SELECT DISTINCT fogrn.receipt_num, fogrn.transaction_type,
                              fogrn.po_num, fogrn.po_date, fogrn.vendor_code,
                              fogrn.vendor_name, fogrn.po_base, fogrn.po_tax,fogrn.po_tax_type,
                                NVL (fogrn.po_base, 0)
                              + NVL (fogrn.po_tax, 0) po_total,
                              fogrn.grn_base,fogrn.CURRENCY_CODE,
                               fogrn.grn_tax,
                                NVL (fogrn.grn_base, 0)
                              - NVL (partial.inv_amt, 0) open_grn_base,
                                NVL (fogrn.grn_tax, 0)
                              - NVL (partial.inv_tax, 0) open_grn_tax,
                                NVL (fogrn.grn_base, 0)
                              - NVL (partial.inv_amt, 0)
                              + NVL (fogrn.grn_tax, 0)
                              - NVL (partial.inv_tax, 0) total_grn,
                              fogrn.ou,
                                NVL (fogrn.grn_base, 0)
                              - NVL (partial.inv_amt, 0)
                              + NVL (fogrn.grn_tax, 0)
                              - NVL (partial.inv_tax, 0) grn_total,
                              fogrn.grn_date,                 --fogrn.hsn_sac,
                                             fogrn.igst_amt, fogrn.sgst_amt,
                              fogrn.cgst_amt,
                                fogrn.GRN_creator_ID, fogrn.accounts,
                                fogrn.GRN_creator_Name
                         FROM (SELECT                       rsh.receipt_num,
                                      rsh.creation_date grn_date,
                                      rt.transaction_type, hou.NAME ou,
                                      pha.segment1 po_num,
                                      pha.creation_date po_date,
                                      asp.segment1 vendor_code,
                                      asp.vendor_name,
                                      (SELECT NVL
                                                 (SUM (  pl.quantity
                                                       * pl.unit_price
                                                      ),
                                                  0
                                                 )
                                         FROM apps.po_headers_all ph,
                                              apps.po_lines_all pl
                                        WHERE 1 = 1
                                          AND ph.po_header_id =
                                                               pl.po_header_id
                                          AND ph.po_header_id =
                                                              pha.po_header_id
                                          AND NVL (pl.cancel_flag, 'N') <> 'Y'
                                          --AND NVL (pl.closed_code, 'OPEN') = 'OPEN'
                                          AND NVL (pl.closed_code, 'OPEN') <>
                                                              'FINALLY CLOSED')
                                                                      po_base,
                                      (SELECT NVL                                                               -------------------
                                                 (SUM
                                                     (DECODE
                                                         (jai.rec_tax_amt_tax_curr,
                                                          '0', jai.nrec_tax_amt_tax_curr,
                                                          jai.rec_tax_amt_tax_curr
                                                         )
                                                     ),
                                                  0
                                                 ) po_tax
                                         FROM apps.jai_tax_lines jai,  jai_tax_types jtt,
                                              apps.po_headers_all ph,
                                              apps.po_lines_all pl
                                        WHERE 1 = 1
                                          AND jai.trx_id = ph.po_header_id
                                          AND jai.trx_line_id = pl.po_line_id
                                          and jai.tax_type_id=jtt.tax_type_id
                                          AND ph.po_header_id =
                                                               pl.po_header_id
                                          AND ph.po_header_id =
                                                              pha.po_header_id
                                          AND NVL (pl.cancel_flag, 'N') <> 'Y'
                                          --  AND NVL (pl.closed_code, 'OPEN') = 'OPEN'
                                          AND NVL (pl.closed_code, 'OPEN') <>
                                                              'FINALLY CLOSED')
                                                                       po_tax,
                                                                          (SELECT TAX_TYPE_NAME
                                         FROM apps.jai_tax_lines jai,  jai_tax_types jtt,
                                              apps.po_headers_all ph,
                                              apps.po_lines_all pl
                                        WHERE 1 = 1 and 
                                          rownum=1
                                          AND jai.trx_id = ph.po_header_id
                                          AND jai.trx_line_id = pl.po_line_id
                                          and jai.tax_type_id=jtt.tax_type_id
                                          AND ph.po_header_id =
                                                               pl.po_header_id
                                          AND ph.po_header_id =
                                                              pha.po_header_id
                                          AND NVL (pl.cancel_flag, 'N') <> 'Y'
                                          --  AND NVL (pl.closed_code, 'OPEN') = 'OPEN'
                                          AND NVL (pl.closed_code, 'OPEN') <>
                                                              'FINALLY CLOSED')po_tax_type,
                                                                    (SELECT NVL
                                                   (SUM (  po_unit_price
                                                         * quantity
                                                        ),
                                                    0
                                                   )
                                           FROM apps.rcv_transactions
                                          WHERE shipment_header_id =
                                                         rt.shipment_header_id
                                            AND transaction_type = 'RECEIVE')
                                      - (SELECT NVL (SUM (  po_unit_price
                                                          * quantity
                                                         ),
                                                     0
                                                    )
                                           FROM apps.rcv_transactions
                                          WHERE shipment_header_id =
                                                         rt.shipment_header_id
                                            AND transaction_type =
                                                            'RETURN TO VENDOR')
                                      + (SELECT NVL (SUM (  po_unit_price
                                                          * quantity
                                                         ),
                                                     0
                                                    )
                                           FROM apps.rcv_transactions
                                          WHERE shipment_header_id =
                                                         rt.shipment_header_id
                                            AND transaction_type IN
                                                                  ('CORRECT')
                                            AND destination_context =
                                                                     'EXPENSE'
                                            AND rt.quantity LIKE '%-%')
                                      + (SELECT NVL (SUM (  po_unit_price
                                                          * quantity
                                                         ),
                                                     0
                                                    )
                                           FROM apps.rcv_transactions
                                          WHERE shipment_header_id =
                                                         rt.shipment_header_id
                                            AND transaction_type IN
                                                                  ('CORRECT')
                                            AND destination_context =
                                                                     'EXPENSE'
                                            AND rt.quantity NOT LIKE '%-%')
                                                                     grn_base, 
                                             rt.currency_code CURRENCY_CODE,                        
                                        (SELECT NVL                                            --------------------------------------------
                                                   (SUM
                                                       (DECODE
                                                           (jai.rec_tax_amt_tax_curr,
                                                            '0', jai.nrec_tax_amt_tax_curr,
                                                            jai.rec_tax_amt_tax_curr
                                                           )
                                                       ),
                                                    0
                                                   ) grn_tax
                                           FROM apps.jai_tax_lines jai,jai_tax_types jtt
                                          WHERE trx_id = rt.shipment_header_id
                                          AND jai.tax_type_id=jtt.tax_type_id
                                          and jtt.tax_type_name <>'%RCM%'
                                            AND trx_type IN ('RECEIVE')
                                            AND entity_code =
                                                             'RCV_TRANSACTION')
                                      - (SELECT NVL
                                                   (SUM
                                                       (DECODE
                                                           (jai.rec_tax_amt_tax_curr,
                                                            '0', jai.nrec_tax_amt_tax_curr,
                                                            jai.rec_tax_amt_tax_curr
                                                           )
                                                       ),
                                                    0
                                                   ) grn_tax
                                           FROM apps.jai_tax_lines jai,jai_tax_types jtt
                                          WHERE trx_id = rt.shipment_header_id
                                          AND jai.tax_type_id=jtt.tax_type_id
                                          and jtt.tax_type_name <>'%RCM%'
                                            AND trx_type IN
                                                         ('RETURN TO VENDOR')
                                            AND entity_code =
                                                             'RCV_TRANSACTION')
                                      + (SELECT NVL
                                                   (SUM
                                                       (DECODE
                                                           (jai.rec_tax_amt_tax_curr,
                                                            '0', jai.nrec_tax_amt_tax_curr,
                                                            jai.rec_tax_amt_tax_curr
                                                           )
                                                       ),
                                                    0
                                                   ) grn_tax
                                           FROM apps.jai_tax_lines jai,jai_tax_types jtt
                                          WHERE trx_id = rt.shipment_header_id
                                          AND jai.tax_type_id=jtt.tax_type_id
                                          and jtt.tax_type_name <>'%RCM%'
                                            AND trx_type IN ('CORRECT')
                                            AND jai.trx_line_quantity LIKE
                                                                          '-%'
                                            AND det_factor_id IN (
                                                   SELECT det_factor_id
                                                     FROM apps.jai_tax_det_factors
                                                    WHERE trx_id =
                                                             rt.shipment_header_id
                                                      AND applied_from_trx_type =
                                                                     'DELIVER'
                                                      AND trx_type = 'CORRECT')
                                            AND entity_code =
                                                             'RCV_TRANSACTION')
                                      + (SELECT NVL
                                                   (SUM
                                                       (DECODE
                                                           (jai.rec_tax_amt_tax_curr,
                                                            '0', jai.nrec_tax_amt_tax_curr,
                                                            jai.rec_tax_amt_tax_curr
                                                           )
                                                       ),
                                                    0
                                                   ) grn_tax
                                           FROM apps.jai_tax_lines jai,jai_tax_types jtt
                                          WHERE trx_id = rt.shipment_header_id
                                           AND jai.tax_type_id=jtt.tax_type_id
--                                           and jtt.tax_type_name <>'%RCM%'
                                            AND trx_type IN ('CORRECT')
                                            AND jai.trx_line_quantity NOT LIKE
                                                                          '-%'
                                            AND det_factor_id IN (
                                                   SELECT det_factor_id
                                                     FROM apps.jai_tax_det_factors
                                                    WHERE trx_id =
                                                             rt.shipment_header_id
                                                      AND applied_from_trx_type =
                                                                     'DELIVER'
                                                      AND trx_type = 'CORRECT')
                                            AND entity_code =
                                                             'RCV_TRANSACTION')
                                                                      grn_tax,              ----------------------------------------
                                        (SELECT NVL
                                                   (SUM
                                                       (DECODE
                                                           (jai.rec_tax_amt_tax_curr,
                                                            '0', jai.nrec_tax_amt_tax_curr,
                                                            jai.rec_tax_amt_tax_curr
                                                           )
                                                       ),
                                                    0
                                                   ) grn_tax
                                           FROM apps.jai_tax_lines jai
                                          WHERE trx_id = rt.shipment_header_id
                                            AND trx_type IN ('RECEIVE')
                                            AND tax_rate_code LIKE '%SGST%'
                                            AND entity_code =
                                                             'RCV_TRANSACTION')
                                      - (SELECT NVL
                                                   (SUM
                                                       (DECODE
                                                           (jai.rec_tax_amt_tax_curr,
                                                            '0', jai.nrec_tax_amt_tax_curr,
                                                            jai.rec_tax_amt_tax_curr
                                                           )
                                                       ),
                                                    0
                                                   ) grn_tax
                                           FROM apps.jai_tax_lines jai
                                          WHERE trx_id = rt.shipment_header_id
                                            AND tax_rate_code LIKE '%SGST%'
                                            AND trx_type IN
                                                         ('RETURN TO VENDOR')
                                            AND entity_code =
                                                             'RCV_TRANSACTION')
                                      + (SELECT NVL
                                                   (SUM
                                                       (DECODE
                                                           (jai.rec_tax_amt_tax_curr,
                                                            '0', jai.nrec_tax_amt_tax_curr,
                                                            jai.rec_tax_amt_tax_curr
                                                           )
                                                       ),
                                                    0
                                                   ) grn_tax
                                           FROM apps.jai_tax_lines jai
                                          WHERE trx_id = rt.shipment_header_id
                                            AND trx_type IN ('CORRECT')
                                            AND tax_rate_code LIKE '%SGST%'
                                            AND jai.trx_line_quantity LIKE
                                                                          '-%'
                                            AND det_factor_id IN (
                                                   SELECT det_factor_id
                                                     FROM apps.jai_tax_det_factors
                                                    WHERE trx_id =
                                                             rt.shipment_header_id
                                                      AND applied_from_trx_type =
                                                                     'DELIVER'
                                                      AND trx_type = 'CORRECT')
                                            AND entity_code =
                                                             'RCV_TRANSACTION')
                                      + (SELECT NVL
                                                   (SUM
                                                       (DECODE
                                                           (jai.rec_tax_amt_tax_curr,
                                                            '0', jai.nrec_tax_amt_tax_curr,
                                                            jai.rec_tax_amt_tax_curr
                                                           )
                                                       ),
                                                    0
                                                   ) grn_tax
                                           FROM apps.jai_tax_lines jai
                                          WHERE trx_id = rt.shipment_header_id
                                            AND trx_type IN ('CORRECT')
                                            AND tax_rate_code LIKE '%SGST%'
                                            AND jai.trx_line_quantity NOT LIKE
                                                                          '-%'
                                            AND det_factor_id IN (
                                                   SELECT det_factor_id
                                                     FROM apps.jai_tax_det_factors
                                                    WHERE trx_id =
                                                             rt.shipment_header_id
                                                      AND applied_from_trx_type =
                                                                     'DELIVER'
                                                      AND trx_type = 'CORRECT')
                                            AND entity_code =
                                                             'RCV_TRANSACTION')
                                                                     sgst_amt,
                                        (SELECT NVL
                                                   (SUM
                                                       (DECODE
                                                           (jai.rec_tax_amt_tax_curr,
                                                            '0', jai.nrec_tax_amt_tax_curr,
                                                            jai.rec_tax_amt_tax_curr
                                                           )
                                                       ),
                                                    0
                                                   ) grn_tax
                                           FROM apps.jai_tax_lines jai
                                          WHERE trx_id = rt.shipment_header_id
                                            AND trx_type IN ('RECEIVE')
                                            AND tax_rate_code LIKE '%CGST%'
                                            AND entity_code =
                                                             'RCV_TRANSACTION')
                                      - (SELECT NVL
                                                   (SUM
                                                       (DECODE
                                                           (jai.rec_tax_amt_tax_curr,
                                                            '0', jai.nrec_tax_amt_tax_curr,
                                                            jai.rec_tax_amt_tax_curr
                                                           )
                                                       ),
                                                    0
                                                   ) grn_tax
                                           FROM apps.jai_tax_lines jai
                                          WHERE trx_id = rt.shipment_header_id
                                            AND tax_rate_code LIKE '%CGST%'
                                            AND trx_type IN
                                                         ('RETURN TO VENDOR')
                                            AND entity_code =
                                                             'RCV_TRANSACTION')
                                      + (SELECT NVL
                                                   (SUM
                                                       (DECODE
                                                           (jai.rec_tax_amt_tax_curr,
                                                            '0', jai.nrec_tax_amt_tax_curr,
                                                            jai.rec_tax_amt_tax_curr
                                                           )
                                                       ),
                                                    0
                                                   ) grn_tax
                                           FROM apps.jai_tax_lines jai
                                          WHERE trx_id = rt.shipment_header_id
                                            AND trx_type IN ('CORRECT')
                                            AND tax_rate_code LIKE '%CGST%'
                                            AND jai.trx_line_quantity LIKE
                                                                          '-%'
                                            AND det_factor_id IN (
                                                   SELECT det_factor_id
                                                     FROM apps.jai_tax_det_factors
                                                    WHERE trx_id =
                                                             rt.shipment_header_id
                                                      AND applied_from_trx_type =
                                                                     'DELIVER'
                                                      AND trx_type = 'CORRECT')
                                            AND entity_code =
                                                             'RCV_TRANSACTION')
                                      + (SELECT NVL
                                                   (SUM
                                                       (DECODE
                                                           (jai.rec_tax_amt_tax_curr,
                                                            '0', jai.nrec_tax_amt_tax_curr,
                                                            jai.rec_tax_amt_tax_curr
                                                           )
                                                       ),
                                                    0
                                                   ) grn_tax
                                           FROM apps.jai_tax_lines jai
                                          WHERE trx_id = rt.shipment_header_id
                                            AND trx_type IN ('CORRECT')
                                            AND tax_rate_code LIKE '%CGST%'
                                            AND jai.trx_line_quantity NOT LIKE
                                                                          '-%'
                                            AND det_factor_id IN (
                                                   SELECT det_factor_id
                                                     FROM apps.jai_tax_det_factors
                                                    WHERE trx_id =
                                                             rt.shipment_header_id
                                                      AND applied_from_trx_type =
                                                                     'DELIVER'
                                                      AND trx_type = 'CORRECT')
                                            AND entity_code =
                                                             'RCV_TRANSACTION')
                                                                     cgst_amt,
                                        (SELECT NVL
                                                   (SUM     
                                                       (DECODE
                                                           (jai.rec_tax_amt_tax_curr,
                                                            '0', jai.nrec_tax_amt_tax_curr,
                                                            jai.rec_tax_amt_tax_curr
                                                           )
                                                       ),
                                                    0
                                                   ) grn_tax
                                           FROM apps.jai_tax_lines jai
                                          WHERE trx_id = rt.shipment_header_id
                                            AND trx_type IN ('RECEIVE')
                                            AND tax_rate_code LIKE '%IGST%'
                                            AND entity_code =
                                                             'RCV_TRANSACTION')
                                      - (SELECT NVL
                                                   (SUM
                                                       (DECODE
                                                           (jai.rec_tax_amt_tax_curr,
                                                            '0', jai.nrec_tax_amt_tax_curr,
                                                            jai.rec_tax_amt_tax_curr
                                                           )
                                                       ),
                                                    0
                                                   ) grn_tax
                                           FROM apps.jai_tax_lines jai
                                          WHERE trx_id = rt.shipment_header_id
                                            AND tax_rate_code LIKE '%IGST%'
                                            AND trx_type IN
                                                         ('RETURN TO VENDOR')
                                            AND entity_code =
                                                             'RCV_TRANSACTION')
                                      + (SELECT NVL
                                                   (SUM
                                                       (DECODE
                                                           (jai.rec_tax_amt_tax_curr,
                                                            '0', jai.nrec_tax_amt_tax_curr,
                                                            jai.rec_tax_amt_tax_curr
                                                           )
                                                       ),
                                                    0
                                                   ) grn_tax
                                           FROM apps.jai_tax_lines jai
                                          WHERE trx_id = rt.shipment_header_id
                                            AND trx_type IN ('CORRECT')
                                            AND tax_rate_code LIKE '%IGST%'
                                            AND jai.trx_line_quantity LIKE
                                                                          '-%'
                                            AND det_factor_id IN (
                                                   SELECT det_factor_id
                                                     FROM apps.jai_tax_det_factors
                                                    WHERE trx_id =
                                                             rt.shipment_header_id
                                                      AND applied_from_trx_type =
                                                                     'DELIVER'
                                                      AND trx_type = 'CORRECT')
                                            AND entity_code =
                                                             'RCV_TRANSACTION')
                                      + (SELECT NVL
                                                   (SUM
                                                       (DECODE
                                                           (jai.rec_tax_amt_tax_curr,
                                                            '0', jai.nrec_tax_amt_tax_curr,
                                                            jai.rec_tax_amt_tax_curr
                                                           )
                                                       ),
                                                    0
                                                   ) grn_tax
                                           FROM apps.jai_tax_lines jai
                                          WHERE trx_id = rt.shipment_header_id
                                            AND trx_type IN ('CORRECT')
                                            AND tax_rate_code LIKE '%IGST%'
                                            AND jai.trx_line_quantity NOT LIKE
                                                                          '-%'
                                            AND det_factor_id IN (
                                                   SELECT det_factor_id
                                                     FROM apps.jai_tax_det_factors
                                                    WHERE trx_id =
                                                             rt.shipment_header_id
                                                      AND applied_from_trx_type =
                                                                     'DELIVER'
                                                      AND trx_type = 'CORRECT')
                                            AND entity_code =
                                                             'RCV_TRANSACTION')
                                                                     igst_amt,
                                                                          (SELECT description
                                              FROM apps.fnd_user
                                             WHERE user_id = rsh.created_by)
                                                              grn_creator_name,
                                           (SELECT user_name
                                              FROM apps.fnd_user
                                             WHERE user_id =
                                                      rsh.created_by)
                                                                 GRN_creator_ID,
                                        gc.concatenated_segments accounts
--                                      xxabrl_hsn_sac_no_grn
--                                              (rsh.shipment_header_id)
--                                                                      hsn_sac
                               FROM   apps.rcv_shipment_headers rsh,
                                      apps.rcv_shipment_lines rsl,
                                      apps.rcv_transactions rt,
                                      apps.po_headers_all pha,
                                      apps.hr_operating_units hou,
                                      apps.ap_suppliers asp,
                                      apps.po_distributions_all pda,
                                      apps.gl_code_combinations_kfv gc
                                WHERE 1 = 1
--           AND TRUNC (rsh.creation_date) >= '17-apr-19'
                             AND pda.po_header_id = pha.po_header_id
                             AND rt.po_header_id = pda.po_header_id
                             AND rt.po_distribution_id =
                                                        pda.po_distribution_id
                             AND pda.accrual_account_id =
                                                        gc.code_combination_id
                             AND gc.segment6 = '361901'
                                  AND TRUNC (rsh.creation_date)
                                         BETWEEN TO_DATE
                                                      (p_from_date,
                                                       'YYYY/MM/DD HH24:MI:SS'
                                                      )
                                             AND TO_DATE
                                                      (p_to_date,
                                                       'YYYY/MM/DD HH24:MI:SS'
                                                      )
--                                  AND rsh.shipment_header_id IN
--                                                       (206520523, 206520524)
                                  AND rsh.shipment_header_id =
                                                        rsl.shipment_header_id
                                  AND rsl.shipment_line_id =
                                                           rt.shipment_line_id
                                  AND rt.transaction_type IN ('RECEIVE')
--                    AND rsh.receipt_num IN ('9820035069')
                                  AND rt.po_header_id = pha.po_header_id
                                  AND pha.org_id = hou.organization_id
                                  AND pha.org_id = NVL (p_org_id, pha.org_id)
                                  AND rt.vendor_id = asp.vendor_id
--                    AND hou.organization_id IN (
--                           SELECT organization_id
--                             FROM (SELECT organization_id
--                                     FROM fnd_profile_option_values fpop,
--                                          fnd_profile_options fpo,
--                                          per_security_organizations_v pso
--                                    WHERE fpop.profile_option_id =
--                                                         fpo.profile_option_id
--                                      AND level_value = (:p_resp_id)
--                                      AND pso.security_profile_id =
--                                                     fpop.profile_option_value
--                                      AND profile_option_name =
--                                               'XLA_MO_SECURITY_PROFILE_LEVEL'
--                                   UNION
--                                   SELECT TO_NUMBER (fpop.profile_option_value)
--                                     FROM fnd_profile_option_values fpop,
--                                          fnd_profile_options fpo
--                                    WHERE fpop.profile_option_id =
--                                                         fpo.profile_option_id
--                                      AND level_value = (:p_resp_id)
--                                      AND profile_option_name = 'ORG_ID'))
--                            AND NOT EXISTS (
--                                   SELECT 1
--                                     FROM apps.ap_invoice_lines_all
--                                    WHERE 1 = 1
--                                      AND rcv_transaction_id =
--                                                             rt.transaction_id)
 ) fogrn,
                              (SELECT   aa.receipt_num,
                                        NVL (aa.inv_amt, 0) inv_amt,
                                        NVL (aa.inv_tax, 0) inv_tax
                                   FROM (   SELECT DISTINCT rsh.receipt_num,
                                                         rt.transaction_type,
                                                         rsh.creation_date
                                                                     grn_date,
                                                         hou.NAME ou,
                                                         pha.segment1 po_num,
                                                         pha.creation_date
                                                                      po_date,
                                                         asp.segment1
                                                                  vendor_code,
                                                         asp.vendor_name,
                                                         (SELECT NVL
                                                                    (SUM
                                                                        (  pl.quantity
                                                                         * pl.unit_price
                                                                        ),
                                                                     0
                                                                    )
                                                            FROM apps.po_headers_all ph,
                                                                 apps.po_lines_all pl
                                                           WHERE 1 = 1
                                                             AND ph.po_header_id =
                                                                    pl.po_header_id
                                                             AND ph.po_header_id =
                                                                    pha.po_header_id
                                                             AND NVL
                                                                    (pl.cancel_flag,
                                                                     'N'
                                                                    ) <> 'Y'
                                                             -- AND NVL (pl.closed_code, 'OPEN') = 'OPEN'
                                                             AND NVL
                                                                    (pl.closed_code,
                                                                     'OPEN'
                                                                    ) <>
                                                                    'FINALLY CLOSED')
                                                                      po_base,
                                                         (SELECT NVL
                                                                    (SUM
                                                                        (DECODE
                                                                            (jai.rec_tax_amt_tax_curr,
                                                                             '0', jai.nrec_tax_amt_tax_curr,
                                                                             jai.rec_tax_amt_tax_curr
                                                                            )
                                                                        ),
                                                                     0
                                                                    )
                                                                    po_tax
                                                            FROM apps.jai_tax_lines jai,
                                                                 apps.po_headers_all ph,
                                                                 apps.po_lines_all pl
                                                           WHERE 1 = 1
                                                             AND jai.trx_id =
                                                                    ph.po_header_id
                                                             AND jai.trx_line_id =
                                                                    pl.po_line_id
                                                             AND ph.po_header_id =
                                                                    pl.po_header_id
                                                             AND ph.po_header_id =
                                                                    pha.po_header_id
                                                             AND pl.po_line_id =
                                                                    rt.po_line_id
                                                             AND NVL
                                                                    (pl.cancel_flag,
                                                                     'N'
                                                                    ) <> 'Y'
                                                             AND NVL
                                                                    (pl.closed_code,
                                                                     'OPEN'
                                                                    ) IN
                                                                    ('OPEN',
                                                                     'CLOSED')
                                                             AND NVL
                                                                    (pl.closed_code,
                                                                     'OPEN'
                                                                    ) <>
                                                                    'FINALLY CLOSED')
                                                                       po_tax,
                                                                         (SELECT jtt.tax_type_name
                                                            FROM apps.jai_tax_lines jai, jai_tax_types jtt,
                                                                 apps.po_headers_all ph,
                                                                 apps.po_lines_all pl
                                                           WHERE 1 = 1
                                                           and rownum=1
                                                           and jai.tax_type_id=jtt.tax_type_id
                                                             AND jai.trx_id =
                                                                    ph.po_header_id
                                                             AND jai.trx_line_id =
                                                                    pl.po_line_id
                                                             AND ph.po_header_id =
                                                                    pl.po_header_id
                                                             AND ph.po_header_id =
                                                                    pha.po_header_id
                                                             AND pl.po_line_id =
                                                                    rt.po_line_id
                                                             AND NVL
                                                                    (pl.cancel_flag,
                                                                     'N'
                                                                    ) <> 'Y'
                                                             AND NVL
                                                                    (pl.closed_code,
                                                                     'OPEN'
                                                                    ) IN
                                                                    ('OPEN',
                                                                     'CLOSED')
                                                             AND NVL
                                                                    (pl.closed_code,
                                                                     'OPEN'
                                                                    ) <>
                                                                    'FINALLY CLOSED')
                                                                       po_tax_type,
                                                           (SELECT NVL
                                                                      (SUM
                                                                          (  po_unit_price
                                                                           * quantity
                                                                          ),
                                                                       0
                                                                      )
                                                              FROM apps.rcv_transactions
                                                             WHERE shipment_header_id =
                                                                      rt.shipment_header_id
                                                               AND transaction_type =
                                                                      'RECEIVE')
                                                         - (SELECT NVL
                                                                      (SUM
                                                                          (  po_unit_price
                                                                           * quantity
                                                                          ),
                                                                       0
                                                                      )
                                                              FROM apps.rcv_transactions
                                                             WHERE shipment_header_id =
                                                                      rt.shipment_header_id
                                                               AND transaction_type =
                                                                      'RETURN TO VENDOR')
                                                         + (SELECT NVL
                                                                      (SUM
                                                                          (  po_unit_price
                                                                           * quantity
                                                                          ),
                                                                       0
                                                                      )
                                                              FROM apps.rcv_transactions
                                                             WHERE shipment_header_id =
                                                                      rt.shipment_header_id
                                                               AND transaction_type IN
                                                                      ('CORRECT')
                                                               AND destination_context =
                                                                      'EXPENSE'
                                                               AND rt.quantity LIKE
                                                                         '%-%')
                                                         + (SELECT NVL
                                                                      (SUM
                                                                          (  po_unit_price
                                                                           * quantity
                                                                          ),
                                                                       0
                                                                      )
                                                              FROM apps.rcv_transactions
                                                             WHERE shipment_header_id =
                                                                      rt.shipment_header_id
                                                               AND transaction_type IN
                                                                      ('CORRECT')
                                                               AND destination_context =
                                                                      'EXPENSE'
                                                               AND rt.quantity NOT LIKE
                                                                         '%-%')
                                                                     grn_base,
                                                                     rt.currency_code CURRENCY_CODE, 
                                                           (SELECT NVL
                                                                      (SUM
                                                                          (DECODE
                                                                              (jai.rec_tax_amt_tax_curr,
                                                                               '0', jai.nrec_tax_amt_tax_curr,
                                                                               jai.rec_tax_amt_tax_curr
                                                                              )
                                                                          ),
                                                                       0
                                                                      )
                                                                      grn_tax
                                                              FROM apps.jai_tax_lines jai
                                                             WHERE trx_id =
                                                                      rt.shipment_header_id
                                                               AND trx_type IN
                                                                      ('RECEIVE')
                                                               AND entity_code =
                                                                      'RCV_TRANSACTION')
                                                         - (SELECT NVL
                                                                      (SUM
                                                                          (DECODE
                                                                              (jai.rec_tax_amt_tax_curr,
                                                                               '0', jai.nrec_tax_amt_tax_curr,
                                                                               jai.rec_tax_amt_tax_curr
                                                                              )
                                                                          ),
                                                                       0
                                                                      )
                                                                      grn_tax
                                                              FROM apps.jai_tax_lines jai
                                                             WHERE trx_id =
                                                                      rt.shipment_header_id
                                                               AND trx_type IN
                                                                      ('RETURN TO VENDOR')
                                                               AND entity_code =
                                                                      'RCV_TRANSACTION')
                                                         + (SELECT NVL
                                                                      (SUM
                                                                          (DECODE
                                                                              (jai.rec_tax_amt_tax_curr,
                                                                               '0', jai.nrec_tax_amt_tax_curr,
                                                                               jai.rec_tax_amt_tax_curr
                                                                              )
                                                                          ),
                                                                       0
                                                                      )
                                                                      grn_tax
                                                              FROM apps.jai_tax_lines jai
                                                             WHERE trx_id =
                                                                      rt.shipment_header_id
                                                               AND trx_type IN
                                                                      ('CORRECT')
                                                               AND det_factor_id IN (
                                                                      SELECT det_factor_id
                                                                        FROM apps.jai_tax_det_factors
                                                                       WHERE trx_id =
                                                                                rt.shipment_header_id
                                                                         AND applied_from_trx_type =
                                                                                'DELIVER'
                                                                         AND trx_type =
                                                                                'CORRECT')
                                                               AND jai.trx_line_quantity LIKE
                                                                          '-%'
                                                               AND entity_code =
                                                                      'RCV_TRANSACTION')
                                                         + (SELECT NVL
                                                                      (SUM
                                                                          (DECODE
                                                                              (jai.rec_tax_amt_tax_curr,
                                                                               '0', jai.nrec_tax_amt_tax_curr,
                                                                               jai.rec_tax_amt_tax_curr
                                                                              )
                                                                          ),
                                                                       0
                                                                      )
                                                                      grn_tax
                                                              FROM apps.jai_tax_lines jai
                                                             WHERE trx_id =
                                                                      rt.shipment_header_id
                                                               AND trx_type IN
                                                                      ('CORRECT')
                                                               AND det_factor_id IN (
                                                                      SELECT det_factor_id
                                                                        FROM apps.jai_tax_det_factors
                                                                       WHERE trx_id =
                                                                                rt.shipment_header_id
                                                                         AND applied_from_trx_type =
                                                                                'DELIVER'
                                                                         AND trx_type =
                                                                                'CORRECT')
                                                               AND jai.trx_line_quantity NOT LIKE
                                                                          '-%'
                                                               AND entity_code =
                                                                      'RCV_TRANSACTION')
                                                                      grn_tax,
                                                           (SELECT NVL
                                                                      (SUM
                                                                          (DECODE
                                                                              (jai.rec_tax_amt_tax_curr,
                                                                               '0', jai.nrec_tax_amt_tax_curr,
                                                                               jai.rec_tax_amt_tax_curr
                                                                              )
                                                                          ),
                                                                       0
                                                                      )
                                                                      grn_tax
                                                              FROM apps.jai_tax_lines jai
                                                             WHERE trx_id =
                                                                      rt.shipment_header_id
                                                               AND trx_type IN
                                                                      ('RECEIVE')
                                                               AND tax_rate_code LIKE
                                                                      '%SGST%'
                                                               AND entity_code =
                                                                      'RCV_TRANSACTION')
                                                         - (SELECT NVL
                                                                      (SUM
                                                                          (DECODE
                                                                              (jai.rec_tax_amt_tax_curr,
                                                                               '0', jai.nrec_tax_amt_tax_curr,
                                                                               jai.rec_tax_amt_tax_curr
                                                                              )
                                                                          ),
                                                                       0
                                                                      )
                                                                      grn_tax
                                                              FROM apps.jai_tax_lines jai
                                                             WHERE trx_id =
                                                                      rt.shipment_header_id
                                                               AND tax_rate_code LIKE
                                                                      '%SGST%'
                                                               AND trx_type IN
                                                                      ('RETURN TO VENDOR')
                                                               AND entity_code =
                                                                      'RCV_TRANSACTION')
                                                         + (SELECT NVL
                                                                      (SUM
                                                                          (DECODE
                                                                              (jai.rec_tax_amt_tax_curr,
                                                                               '0', jai.nrec_tax_amt_tax_curr,
                                                                               jai.rec_tax_amt_tax_curr
                                                                              )
                                                                          ),
                                                                       0
                                                                      )
                                                                      grn_tax
                                                              FROM apps.jai_tax_lines jai
                                                             WHERE trx_id =
                                                                      rt.shipment_header_id
                                                               AND trx_type IN
                                                                      ('CORRECT')
                                                               AND tax_rate_code LIKE
                                                                      '%SGST%'
                                                               AND jai.trx_line_quantity LIKE
                                                                          '-%'
                                                               AND det_factor_id IN (
                                                                      SELECT det_factor_id
                                                                        FROM apps.jai_tax_det_factors
                                                                       WHERE trx_id =
                                                                                rt.shipment_header_id
                                                                         AND applied_from_trx_type =
                                                                                'DELIVER'
                                                                         AND trx_type =
                                                                                'CORRECT')
                                                               AND entity_code =
                                                                      'RCV_TRANSACTION')
                                                         + (SELECT NVL
                                                                      (SUM
                                                                          (DECODE
                                                                              (jai.rec_tax_amt_tax_curr,
                                                                               '0', jai.nrec_tax_amt_tax_curr,
                                                                               jai.rec_tax_amt_tax_curr
                                                                              )
                                                                          ),
                                                                       0
                                                                      )
                                                                      grn_tax
                                                              FROM apps.jai_tax_lines jai
                                                             WHERE trx_id =
                                                                      rt.shipment_header_id
                                                               AND trx_type IN
                                                                      ('CORRECT')
                                                               AND tax_rate_code LIKE
                                                                      '%SGST%'
                                                               AND jai.trx_line_quantity NOT LIKE
                                                                          '-%'
                                                               AND det_factor_id IN (
                                                                      SELECT det_factor_id
                                                                        FROM apps.jai_tax_det_factors
                                                                       WHERE trx_id =
                                                                                rt.shipment_header_id
                                                                         AND applied_from_trx_type =
                                                                                'DELIVER'
                                                                         AND trx_type =
                                                                                'CORRECT')
                                                               AND entity_code =
                                                                      'RCV_TRANSACTION')
                                                                     sgst_amt,
                                                           (SELECT NVL
                                                                      (SUM
                                                                          (DECODE
                                                                              (jai.rec_tax_amt_tax_curr,
                                                                               '0', jai.nrec_tax_amt_tax_curr,
                                                                               jai.rec_tax_amt_tax_curr
                                                                              )
                                                                          ),
                                                                       0
                                                                      )
                                                                      grn_tax
                                                              FROM apps.jai_tax_lines jai
                                                             WHERE trx_id =
                                                                      rt.shipment_header_id
                                                               AND trx_type IN
                                                                      ('RECEIVE')
                                                               AND tax_rate_code LIKE
                                                                      '%CGST%'
                                                               AND entity_code =
                                                                      'RCV_TRANSACTION')
                                                         - (SELECT NVL
                                                                      (SUM
                                                                          (DECODE
                                                                              (jai.rec_tax_amt_tax_curr,
                                                                               '0', jai.nrec_tax_amt_tax_curr,
                                                                               jai.rec_tax_amt_tax_curr
                                                                              )
                                                                          ),
                                                                       0
                                                                      )
                                                                      grn_tax
                                                              FROM apps.jai_tax_lines jai
                                                             WHERE trx_id =
                                                                      rt.shipment_header_id
                                                               AND tax_rate_code LIKE
                                                                      '%CGST%'
                                                               AND trx_type IN
                                                                      ('RETURN TO VENDOR')
                                                               AND entity_code =
                                                                      'RCV_TRANSACTION')
                                                         + (SELECT NVL
                                                                      (SUM
                                                                          (DECODE
                                                                              (jai.rec_tax_amt_tax_curr,
                                                                               '0', jai.nrec_tax_amt_tax_curr,
                                                                               jai.rec_tax_amt_tax_curr
                                                                              )
                                                                          ),
                                                                       0
                                                                      )
                                                                      grn_tax
                                                              FROM apps.jai_tax_lines jai
                                                             WHERE trx_id =
                                                                      rt.shipment_header_id
                                                               AND trx_type IN
                                                                      ('CORRECT')
                                                               AND tax_rate_code LIKE
                                                                      '%CGST%'
                                                               AND jai.trx_line_quantity LIKE
                                                                          '-%'
                                                               AND det_factor_id IN (
                                                                      SELECT det_factor_id
                                                                        FROM apps.jai_tax_det_factors
                                                                       WHERE trx_id =
                                                                                rt.shipment_header_id
                                                                         AND applied_from_trx_type =
                                                                                'DELIVER'
                                                                         AND trx_type =
                                                                                'CORRECT')
                                                               AND entity_code =
                                                                      'RCV_TRANSACTION')
                                                         + (SELECT NVL
                                                                      (SUM
                                                                          (DECODE
                                                                              (jai.rec_tax_amt_tax_curr,
                                                                               '0', jai.nrec_tax_amt_tax_curr,
                                                                               jai.rec_tax_amt_tax_curr
                                                                              )
                                                                          ),
                                                                       0
                                                                      )
                                                                      grn_tax
                                                              FROM apps.jai_tax_lines jai
                                                             WHERE trx_id =
                                                                      rt.shipment_header_id
                                                               AND trx_type IN
                                                                      ('CORRECT')
                                                               AND tax_rate_code LIKE
                                                                      '%CGST%'
                                                               AND jai.trx_line_quantity NOT LIKE
                                                                          '-%'
                                                               AND det_factor_id IN (
                                                                      SELECT det_factor_id
                                                                        FROM apps.jai_tax_det_factors
                                                                       WHERE trx_id =
                                                                                rt.shipment_header_id
                                                                         AND applied_from_trx_type =
                                                                                'DELIVER'
                                                                         AND trx_type =
                                                                                'CORRECT')
                                                               AND entity_code =
                                                                      'RCV_TRANSACTION')
                                                                     cgst_amt,
                                                           (SELECT NVL
                                                                      (SUM
                                                                          (DECODE
                                                                              (jai.rec_tax_amt_tax_curr,
                                                                               '0', jai.nrec_tax_amt_tax_curr,
                                                                               jai.rec_tax_amt_tax_curr
                                                                              )
                                                                          ),
                                                                       0
                                                                      )
                                                                      grn_tax
                                                              FROM apps.jai_tax_lines jai
                                                             WHERE trx_id =
                                                                      rt.shipment_header_id
                                                               AND trx_type IN
                                                                      ('RECEIVE')
                                                               AND tax_rate_code LIKE
                                                                      '%IGST%'
                                                               AND entity_code =
                                                                      'RCV_TRANSACTION')
                                                         - (SELECT NVL
                                                                      (SUM
                                                                          (DECODE
                                                                              (jai.rec_tax_amt_tax_curr,
                                                                               '0', jai.nrec_tax_amt_tax_curr,
                                                                               jai.rec_tax_amt_tax_curr
                                                                              )
                                                                          ),
                                                                       0
                                                                      )
                                                                      grn_tax
                                                              FROM apps.jai_tax_lines jai
                                                             WHERE trx_id =
                                                                      rt.shipment_header_id
                                                               AND tax_rate_code LIKE
                                                                      '%IGST%'
                                                               AND trx_type IN
                                                                      ('RETURN TO VENDOR')
                                                               AND entity_code =
                                                                      'RCV_TRANSACTION')
                                                         + (SELECT NVL
                                                                      (SUM
                                                                          (DECODE
                                                                              (jai.rec_tax_amt_tax_curr,
                                                                               '0', jai.nrec_tax_amt_tax_curr,
                                                                               jai.rec_tax_amt_tax_curr
                                                                              )
                                                                          ),
                                                                       0
                                                                      )
                                                                      grn_tax
                                                              FROM apps.jai_tax_lines jai
                                                             WHERE trx_id =
                                                                      rt.shipment_header_id
                                                               AND trx_type IN
                                                                      ('CORRECT')
                                                               AND tax_rate_code LIKE
                                                                      '%IGST%'
                                                               AND jai.trx_line_quantity LIKE
                                                                          '-%'
                                                               AND det_factor_id IN (
                                                                      SELECT det_factor_id
                                                                        FROM apps.jai_tax_det_factors
                                                                       WHERE trx_id =
                                                                                rt.shipment_header_id
                                                                         AND applied_from_trx_type =
                                                                                'DELIVER'
                                                                         AND trx_type =
                                                                                'CORRECT')
                                                               AND entity_code =
                                                                      'RCV_TRANSACTION')
                                                         + (SELECT NVL
                                                                      (SUM
                                                                          (DECODE
                                                                              (jai.rec_tax_amt_tax_curr,
                                                                               '0', jai.nrec_tax_amt_tax_curr,
                                                                               jai.rec_tax_amt_tax_curr
                                                                              )
                                                                          ),
                                                                       0
                                                                      )
                                                                      grn_tax
                                                              FROM apps.jai_tax_lines jai
                                                             WHERE trx_id =
                                                                      rt.shipment_header_id
                                                               AND trx_type IN
                                                                      ('CORRECT')
                                                               AND tax_rate_code LIKE
                                                                      '%IGST%'
                                                               AND jai.trx_line_quantity NOT LIKE
                                                                          '-%'
                                                               AND det_factor_id IN (
                                                                      SELECT det_factor_id
                                                                        FROM apps.jai_tax_det_factors
                                                                       WHERE trx_id =
                                                                                rt.shipment_header_id
                                                                         AND applied_from_trx_type =
                                                                                'DELIVER'
                                                                         AND trx_type =
                                                                                'CORRECT')
                                                               AND entity_code =
                                                                      'RCV_TRANSACTION')
                                                                     igst_amt,                      
                                                                        (SELECT user_name
                                              FROM apps.fnd_user
                                             WHERE user_id =
                                                      rsh.created_by)
                                                                 cGRN_creator_ID,
                                           (SELECT description
                                              FROM apps.fnd_user
                                             WHERE user_id = rsh.created_by)
                                                              GRN_creator_Name,                                   
--                                                         xxabrl_hsn_sac_no_grn
--                                                            (rsh.shipment_header_id
--                                                            ) hsn_sac,
                                                         rsh.shipment_header_id,
                                                         (SELECT NVL
                                                                    (SUM
                                                                        (jai.line_amt
                                                                        ),
                                                                     0
                                                                    )
                                                            FROM apps.jai_tax_det_factors jai,
                                                                 apps.ap_invoice_lines_all aila
                                                           WHERE jai.applied_to_trx_id =
                                                                    rt.shipment_header_id
                                                             AND jai.entity_code =
                                                                    'AP_INVOICES'
                                                             AND jai.trx_id =
                                                                    aila.invoice_id
                                                             AND aila.cancelled_flag <>
                                                                           'Y'
                                                             AND aila.amount <>
                                                                             0
                                                             AND jai.trx_line_id =
                                                                    aila.line_number
                                                             AND jai.entity_code =
                                                                    'AP_INVOICES')
                                                                      inv_amt,
                                                         (SELECT NVL
                                                                    (SUM
                                                                        (DECODE
                                                                            (jai.rec_tax_amt_tax_curr,
                                                                             '0', jai.nrec_tax_amt_tax_curr,
                                                                             jai.rec_tax_amt_tax_curr
                                                                            )
                                                                        ),
                                                                     0
                                                                    )
                                                            FROM apps.jai_tax_lines jai,
                                                                 apps.ap_invoice_lines_all aila
                                                           WHERE jai.applied_to_trx_id =
                                                                    rt.shipment_header_id
                                                             AND jai.trx_id =
                                                                    aila.invoice_id
                                                             AND jai.trx_line_id =
                                                                    aila.line_number
                                                             AND aila.cancelled_flag <>
                                                                           'Y'
                                                             AND aila.amount <>
                                                                             0
                                                             AND jai.entity_code =
                                                                    'AP_INVOICES')
                                                                      inv_tax,
                                                                      gc.concatenated_segments accounts
                                                    FROM apps.rcv_shipment_headers rsh,
                                                         apps.rcv_shipment_lines rsl,
                                                         apps.rcv_transactions rt,
                                                         apps.po_headers_all pha,
                                                         apps.hr_operating_units hou,
                                                         apps.ap_suppliers asp,
                                                         apps.po_distributions_all pda,
                                                         apps.gl_code_combinations_kfv gc
                                                   WHERE 1 = 1
--                            AND TRUNC (rsh.creation_date) >='12-apr-19'
                                       AND pda.po_header_id = pha.po_header_id
                             AND rt.po_header_id = pda.po_header_id
                             AND rt.po_distribution_id =
                                                        pda.po_distribution_id
                             AND pda.accrual_account_id =
                                                        gc.code_combination_id
                             AND gc.segment6 = '361901'
                                                     AND TRUNC
                                                            (rsh.creation_date)
                                                            BETWEEN TO_DATE
                                                                      (p_from_date,
                                                                       'YYYY/MM/DD HH24:MI:SS'
                                                                      )
                                                                AND TO_DATE
                                                                      (p_to_date,
                                                                       'YYYY/MM/DD HH24:MI:SS'
                                                                      )
--                                                     AND rsh.shipment_header_id IN
--                                                            (206520523,
--                                                             206520524)
                                                     AND rsh.shipment_header_id =
                                                            rsl.shipment_header_id
                                                     AND rsl.shipment_line_id =
                                                            rt.shipment_line_id
                                                     --AND pha.segment1 = '421003056'
                                                     AND rt.transaction_type IN
                                                                  ('RECEIVE')
--                                     AND rsh.receipt_num IN ('9820035069')
                                                     AND rt.po_header_id =
                                                              pha.po_header_id
                                                     AND pha.org_id =
                                                            hou.organization_id
                                                     AND rt.vendor_id =
                                                                 asp.vendor_id
                                                     AND pha.org_id =
                                                            NVL (p_org_id,
                                                                 pha.org_id
                                                                )
--                                     AND hou.organization_id IN (
--                                            SELECT organization_id
--                                              FROM (SELECT organization_id
--                                                      FROM fnd_profile_option_values fpop,
--                                                           fnd_profile_options fpo,
--                                                           per_security_organizations_v pso
--                                                     WHERE fpop.profile_option_id =
--                                                              fpo.profile_option_id
--                                                       AND level_value =
--                                                                 (:p_resp_id
--                                                                 )
--                                                       AND pso.security_profile_id =
--                                                              fpop.profile_option_value
--                                                       AND profile_option_name =
--                                                              'XLA_MO_SECURITY_PROFILE_LEVEL'
--                                                    UNION
--                                                    SELECT TO_NUMBER
--                                                              (fpop.profile_option_value
--                                                              )
--                                                      FROM fnd_profile_option_values fpop,
--                                                           fnd_profile_options fpo
--                                                     WHERE fpop.profile_option_id =
--                                                              fpo.profile_option_id
--                                                       AND level_value =
--                                                                 (:p_resp_id
--                                                                 )
--                                                       AND profile_option_name =
--                                                                      'ORG_ID'))
                                                     AND EXISTS (
                                                            SELECT 1
                                                              FROM apps.ap_invoice_lines_all
                                                             WHERE 1 = 1
                                                               AND rcv_transaction_id =
                                                                      rt.transaction_id)) aa
                                  WHERE 1 = 1
                               GROUP BY aa.receipt_num, aa.inv_amt,
                                        aa.inv_tax) partial
                        WHERE 1 = 1
                          AND fogrn.receipt_num = partial.receipt_num(+)
                          AND NVL (fogrn.grn_base, 0) > 0
                          --AND NVL (fogrn..grn_tax, 0) <> 0
                          AND   ROUND (NVL (fogrn.grn_base, 0))
                              - ROUND (NVL (partial.inv_amt, 0)) > 0)
       WHERE 1 = 1 AND NVL (open_grn_base, 0) >= 0
             AND NVL (open_grn_tax, 0) >= 0)ab
             where receipt_num not in ('9130003995','9110007403','9820039184','9110007969','9850007410','938300001191','9300003692');
             
             
BEGIN
   fnd_file.put_line (fnd_file.output,
                      LPAD (' GST ABRL OPEN GRN Report', 100)
                     );
   fnd_file.put_line (fnd_file.output,
                      LPAD ('Report Date : ' || SYSDATE, 160));
   fnd_file.put_line
      (fnd_file.output,
       '-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------'
      );
   fnd_file.put_line (fnd_file.output,
                         LPAD ('OU', 20)
                      || '|'
                      || LPAD ('GRN_NO', 20)
                      || '|'
                      || LPAD ('GRN_DATE', 20)
                      || '|'
                      || LPAD ('VENDOR_CODE', 20)
                      || '|'
                      || LPAD ('VENDOR_NAME', 20)
                      || '|'
                      || LPAD ('PO_NUM', 20)
                      || '|'
                      || LPAD ('PO_DATE', 20)
                      || '|'
                      || LPAD ('PO_TAX_TYPE', 20)
                      || '|'
                      || LPAD ('CURRENCY_CODE', 20)
                      || '|'
                      || LPAD ('PO_BASE', 20)
                      || '|'
                      || LPAD ('PO_TAX', 20)
                      || '|'
                      || LPAD ('PO_TOTAL', 20)
                      || '|'
                      || LPAD ('GRN_BASE', 20)
                      || '|'
                      || LPAD ('GRN_TAX', 20)
                      || '|'
                      || LPAD ('OPEN_GRN_BASE', 20)
                      || '|'
                      || LPAD ('OPEN_GRN_TAX', 20)
                      || '|'
                      || LPAD ('TOTAL_GRN', 20)
                      ||'|'
                      ||LPAD('GRN_CREATOR_ID',20)
                      ||'|' 
                      ||LPAD('GRN_CREATOR_NAME',20)
                      ||'|'
                      ||LPAD('ACCOUNTS',40)
--                      || '|'
--                      || LPAD ('IGST_AMT', 30)
--                      || '|'
--                      || LPAD ('SGST_AMT', 30)
--                      || '|'
--                      || LPAD ('CGST_AMT', 30)
                     -- || '|'
                      --|| LPAD ('HSN_SAC', 30)
                     );
   fnd_file.put_line (fnd_file.output, '');

   FOR xx_main IN xx_rec
   LOOP
      fnd_file.put_line (fnd_file.output,
                            LPAD (xx_main.ou, 20)
                         || '|'
                         || LPAD (xx_main.receipt_num, 50)
                         || '|'
                         || LPAD (xx_main.grn_date, 20)
                         || '|'
                         || LPAD (xx_main.vendor_code, 20)
                         || '|'
                         || LPAD (xx_main.vendor_name,20)
                         || '|'
                         || LPAD (xx_main.po_num, 20)
                         || '|'
                         || LPAD (xx_main.po_date, 20)
                         || '|'
                         || LPAD (xx_main.po_tax_type, 20)
                         || '|'
                         || LPAD (xx_main.CURRENCY_CODE, 20)
                         || '|'
                         || LPAD (xx_main.po_base, 20)
                         || '|'
                         || LPAD (xx_main.po_tax, 20)
                         || '|'
                         || LPAD (xx_main.po_total, 20)
                         || '|'
                         || LPAD (xx_main.grn_base, 20)
                         || '|'                
                         || LPAD (xx_main.grn_tax, 20)
                         || '|'
                         || LPAD (xx_main.open_grn_base, 20)
                         || '|'
                         || LPAD (xx_main.open_grn_tax, 20)
                         || '|'
                         || LPAD (xx_main.total_grn, 20)
                          || '|'
                         ||LPAD(xx_main.GRN_CREATOR_ID,20)
                         ||'|'
                         ||LPAD(xx_main.GRN_CREATOR_NAME,20)
                         ||'|'
                         ||LPAD(xx_main.ACCOUNTS,40)
--                         || '|'
--                         || LPAD (xx_main.igst_amt, 30)
--                         || '|'
--                         || LPAD (xx_main.sgst_amt, 30)
--                         || '|'
--                         || LPAD (xx_main.cgst_amt, 30)
--                         || '|'
--                         || LPAD (xx_main.hsn_sac, 30)
                        );
      NULL;
   END LOOP;
END;



--select * from apps. gl_code_combinations 
/


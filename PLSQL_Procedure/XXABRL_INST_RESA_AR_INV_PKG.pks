CREATE OR REPLACE Package APPS.XXABRL_INST_RESA_AR_INV_PKG IS
  PROCEDURE INVOICE_VALIDATE(Errbuf         OUT VARCHAR2,
                             RetCode        OUT NUMBER,
                             P_BATCH_Source IN VARCHAR2,
                             p_action       in varchar2,
                             P_Org_Id       In NUMBER
                             );
  PROCEDURE INVOICE_INSERT(P_Org_Id IN NUMBER, P_BATCH_Source IN VARCHAR2,x_ret_code OUT number);
  FUNCTION ACCOUNT_SEG_STATUS(P_Seg_Value IN Varchar2,
                              P_Seg_Desc  IN Varchar2) return Varchar2;
END XXABRL_INST_RESA_AR_INV_PKG; 
/


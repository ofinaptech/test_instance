CREATE OR REPLACE PROCEDURE APPS.xxabrl_upd_sel_inv_prog (
   err_buf           OUT   VARCHAR2,
   ret_code          OUT   NUMBER,
   p_checkrun_name         VARCHAR2
)
IS
/*********************************************************************************************************
                 WIPRO Infotech Ltd, Mumbai, India

 Object Name : XXABRL_UPD_SEL_INV_PROG

 Description : Procedure to Update the AP selected invoices for Payment Batch

  Change Record:
 =========================================================================================================
 Version    Date            Author        Remarks
 =======   ==========     =============  ==========================================
 1.0      18-August-09   Praveen Kumar   New Development
1.1      18-Feb-10       Praveen Kumar   Extra condition is added in Voucher No Validaiton
1.2      13-Dec-19      Lokesh Poojari    Added Extra links and changed the older version table to new version
***********************************************************************************************************/
   v_err_flag           VARCHAR2 (1);
   v_err_msg            VARCHAR2 (3000);
   v_count              NUMBER          := 0;
   v_err_count          NUMBER          := 0;
   l_checkrun_id        NUMBER;
   l_vendor_num         NUMBER;
   l_vendor_site_code   VARCHAR2 (100);
   l_voucher_num        NUMBER;
   l_inovice_num        VARCHAR2 (100);

   CURSOR cur_upd_sel_inv
   IS
      SELECT *
        FROM xxabrl_ap_selected_invoices
       WHERE ok_to_pay_flag = 'N' AND checkrun_name = p_checkrun_name;
---- invoice_id = 612073;
BEGIN
   FOR cur_upd_sel_inv_r1 IN cur_upd_sel_inv
   LOOP
      v_err_flag := NULL;
      v_err_msg := NULL;

      IF cur_upd_sel_inv_r1.checkrun_name IS NOT NULL
      THEN
         --fnd_file.put_line(fnd_file.output,'checkrun_name '||cur_upd_sel_inv_r1.checkrun_name);
         BEGIN
            SELECT DISTINCT asi.checkrun_id
                       INTO l_checkrun_id
                       FROM ap_selected_invoices_all asi,
                            ap_inv_selection_criteria_all aisc
                      WHERE 1 = 1
                        -- AND asi.vendor_id =aisc.vendor_id
                                                         --added on 13-dec-2019
                        AND asi.checkrun_name = aisc.checkrun_name
                        AND asi.vendor_site_id =
                                             cur_upd_sel_inv_r1.vendor_site_id
                        --added on 13-dec-2019
                        AND asi.invoice_id = cur_upd_sel_inv_r1.invoice_id
                                                      ----added on 13-dec-2019
--                        AND aisc.vendor_id =
--                               cur_upd_sel_inv_r1.vendor_id
                                                      ----added on 13-dec-2019
                        AND aisc.checkrun_name =
                                              cur_upd_sel_inv_r1.checkrun_name;

            IF l_checkrun_id IS NULL
            THEN
               fnd_file.put_line
                              (fnd_file.LOG,
                                  'Checkrun is not available for Invoice Id '
                               || cur_upd_sel_inv_r1.invoice_id
                               || ','
                               || 'Vendor No '
                               || cur_upd_sel_inv_r1.vendor_num
                              );
               v_err_msg := 'Checkrun Name is not valid';
               v_err_flag := 'E';
            END IF;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               fnd_file.put_line
                            (fnd_file.LOG,
                                'No data found for Checkrun Name Invoice Id '
                             || cur_upd_sel_inv_r1.invoice_id
                             || ','
                             || 'Vendor No '
                             || cur_upd_sel_inv_r1.vendor_num
                            );
               v_err_msg := 'No data found for Checkrun Name';
               v_err_flag := 'E';
            WHEN TOO_MANY_ROWS
            THEN
               fnd_file.put_line
                      (fnd_file.LOG,
                          'Too many rows found for Checkrun Name Invoice Id '
                       || cur_upd_sel_inv_r1.invoice_id
                       || ','
                       || 'Vendor No '
                       || cur_upd_sel_inv_r1.vendor_num
                      );
               v_err_msg := 'Too many rows found for Checkrun Name';
               v_err_flag := 'E';
            WHEN OTHERS
            THEN
               fnd_file.put_line (fnd_file.LOG,
                                     'Invalid Checkrun Name Invoice Id '
                                  || cur_upd_sel_inv_r1.invoice_id
                                  || ','
                                  || 'Vendor No '
                                  || cur_upd_sel_inv_r1.vendor_num
                                 );
               v_err_msg := 'Invalid Checkrun Name';
               v_err_flag := 'E';
         END;
      ELSE
         fnd_file.put_line (fnd_file.LOG,
                               'Checkrun Name Is Null'
                            || cur_upd_sel_inv_r1.invoice_id
                            || ','
                            || 'Vendor No '
                            || cur_upd_sel_inv_r1.vendor_num
                           );
         v_err_msg := 'Checkrun Name Is Null';
         v_err_flag := 'E';
      END IF;

      IF cur_upd_sel_inv_r1.vendor_num IS NOT NULL
      THEN
         BEGIN
            -- fnd_file.put_line(fnd_file.output,'vendor_num validation'||cur_upd_sel_inv_r1.voucher_num);
            SELECT DISTINCT segment1
                       INTO l_vendor_num
                       FROM ap_suppliers aps, ap_selected_invoices_all asi
                      WHERE aps.vendor_id = asi.vendor_id
                        AND asi.vendor_site_id =
                                             cur_upd_sel_inv_r1.vendor_site_id
                        --added on 13-dec-2019
                        AND asi.invoice_id = cur_upd_sel_inv_r1.invoice_id
                        ----added on 13-dec-2019
                        AND asi.vendor_id = cur_upd_sel_inv_r1.vendor_id
                        ----added on 13-dec-2019
                        AND asi.vendor_num = cur_upd_sel_inv_r1.vendor_num;

            IF l_vendor_num IS NULL
            THEN
               fnd_file.put_line (fnd_file.LOG,
                                     'Vendor is not available for Invoice Id'
                                  || cur_upd_sel_inv_r1.invoice_id
                                  || ','
                                  || 'Vendor No '
                                  || cur_upd_sel_inv_r1.vendor_num
                                 );
               v_err_msg := v_err_msg || ',' || 'Vendor Name is not valid';
               v_err_flag := 'E';
            END IF;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               fnd_file.put_line
                           (fnd_file.LOG,
                               'No data found for Vendor Name for Invoice Id'
                            || cur_upd_sel_inv_r1.invoice_id
                            || ','
                            || 'Vendor No '
                            || cur_upd_sel_inv_r1.vendor_num
                           );
               v_err_msg := 'No data found for Vendor Name ';
               v_err_flag := 'E';
            WHEN TOO_MANY_ROWS
            THEN
               fnd_file.put_line
                     (fnd_file.LOG,
                         'Too many rows found for Vendor Name for Invoice Id'
                      || cur_upd_sel_inv_r1.invoice_id
                      || ','
                      || 'Vendor No '
                      || cur_upd_sel_inv_r1.vendor_num
                     );
               v_err_msg := 'Too many rows found for Vendor Name ';
               v_err_flag := 'E';
            WHEN OTHERS
            THEN
               fnd_file.put_line (fnd_file.LOG,
                                     'Invalid Vendor Name for Invoice Id'
                                  || cur_upd_sel_inv_r1.invoice_id
                                  || ','
                                  || 'Vendor No '
                                  || cur_upd_sel_inv_r1.vendor_num
                                 );
               v_err_msg := v_err_msg || ',' || 'Invalid Venodr Name';
               v_err_flag := 'E';
         END;
      ELSE
         fnd_file.put_line (fnd_file.LOG,
                               'Vendor Name Is Null for Invoice Id'
                            || cur_upd_sel_inv_r1.invoice_id
                            || ','
                            || 'Vendor No '
                            || cur_upd_sel_inv_r1.vendor_num
                           );
         v_err_msg := v_err_msg || ',' || 'Venodr Name Is null';
         v_err_flag := 'E';
      END IF;

      IF cur_upd_sel_inv_r1.vendor_site_code IS NOT NULL
      THEN
         --fnd_file.put_line(fnd_file.output,'vendor_site_code validation'||cur_upd_sel_inv_r1.vendor_site_code);
         BEGIN
            SELECT DISTINCT apsa.vendor_site_code
                       INTO l_vendor_site_code
                       FROM ap_suppliers aps,
                            ap_supplier_sites_all apsa,
                            ap_selected_invoices_all asi
                      WHERE aps.vendor_id = apsa.vendor_id
                        AND apsa.vendor_site_id = asi.vendor_site_id
                        AND aps.vendor_id = cur_upd_sel_inv_r1.vendor_id
                        AND asi.vendor_site_id =
                                             cur_upd_sel_inv_r1.vendor_site_id
                        --added on 13-dec-2019
                        AND asi.invoice_id = cur_upd_sel_inv_r1.invoice_id
                        ----added on 13-dec-2019
                        AND asi.vendor_id = cur_upd_sel_inv_r1.vendor_id
                        ----added on 13-dec-2019
                        AND apsa.vendor_site_id =
                                             cur_upd_sel_inv_r1.vendor_site_id;

            IF l_vendor_site_code IS NULL
            THEN
               fnd_file.put_line
                         (fnd_file.LOG,
                             'Venodr Site Code not available for Invoice id '
                          || cur_upd_sel_inv_r1.invoice_id
                          || ','
                          || 'Vendor No '
                          || cur_upd_sel_inv_r1.vendor_num
                         );
               v_err_msg :=
                           v_err_msg || ',' || 'Vendor Site Code is not valid';
               v_err_flag := 'E';
            END IF;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               fnd_file.put_line
                         (fnd_file.LOG,
                             'No data found for Vendor Site Code Invoice id '
                          || cur_upd_sel_inv_r1.invoice_id
                          || ','
                          || 'Vendor No '
                          || cur_upd_sel_inv_r1.vendor_num
                         );
               v_err_msg :=
                     v_err_msg
                  || ','
                  || 'No data found for Vendor Site Code Invoice id';
               v_err_flag := 'E';
            WHEN TOO_MANY_ROWS
            THEN
               fnd_file.put_line
                   (fnd_file.LOG,
                       'Too many rows found for Vendor Site Code Invoice id '
                    || cur_upd_sel_inv_r1.invoice_id
                    || ','
                    || 'Vendor No '
                    || cur_upd_sel_inv_r1.vendor_num
                   );
               v_err_msg :=
                     v_err_msg
                  || ','
                  || 'Too many rows found for Vendor Site Code Invoice id';
               v_err_flag := 'E';
            WHEN OTHERS
            THEN
               fnd_file.put_line
                               (fnd_file.LOG,
                                   'Invalid Vendor Site Code for Invoice id '
                                || cur_upd_sel_inv_r1.invoice_id
                                || ','
                                || 'Vendor No '
                                || cur_upd_sel_inv_r1.vendor_num
                               );
               v_err_msg := v_err_msg || ',' || 'Invalid Vendor Site Code';
               v_err_flag := 'E';
         END;
      ELSE
         fnd_file.put_line (fnd_file.LOG,
                               'Vendor Site Code is null for Invoice id '
                            || cur_upd_sel_inv_r1.invoice_id
                            || ','
                            || 'Vendor No '
                            || cur_upd_sel_inv_r1.vendor_num
                           );
         v_err_msg := v_err_msg || ',' || 'Vendor Site Code is null';
         v_err_flag := 'E';
      END IF;

      IF cur_upd_sel_inv_r1.voucher_num IS NOT NULL
      THEN
         ----fnd_file.put_line(fnd_file.output,'voucher_num validation'||cur_upd_sel_inv_r1.voucher_num);
         BEGIN
            SELECT asi.voucher_num
              INTO l_voucher_num
              FROM ap_selected_invoices_all asi
             WHERE NVL (asi.voucher_num, -99999) =
                                   NVL (cur_upd_sel_inv_r1.voucher_num,
                                        -99999)
               AND asi.checkrun_id = cur_upd_sel_inv_r1.checkrun_id
               AND asi.vendor_id = cur_upd_sel_inv_r1.vendor_id
               --added on 13-Dec-2019
               AND asi.vendor_site_id = cur_upd_sel_inv_r1.vendor_site_id
               --added on 13-Dec-2019
               AND asi.invoice_id = cur_upd_sel_inv_r1.invoice_id;

            IF l_voucher_num IS NULL
            THEN
               fnd_file.put_line
                             (fnd_file.LOG,
                                 'Voucher No is not available for Invoice id'
                              || cur_upd_sel_inv_r1.invoice_id
                              || ','
                              || 'Vendor No '
                              || cur_upd_sel_inv_r1.vendor_num
                             );
               v_err_msg := v_err_msg || ',' || 'Voucher Number is not valid';
               v_err_flag := 'E';
            END IF;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               fnd_file.put_line (fnd_file.LOG,
                                     'NO data found for Voucher No '
                                  || cur_upd_sel_inv_r1.invoice_id
                                  || ','
                                  || 'Vendor No '
                                  || cur_upd_sel_inv_r1.vendor_num
                                 );
               v_err_msg := v_err_msg || ',' || 'NO data found for Voucher No';
               v_err_flag := 'E';
            WHEN TOO_MANY_ROWS
            THEN
               fnd_file.put_line (fnd_file.LOG,
                                     'Too many rows found for Voucher No '
                                  || cur_upd_sel_inv_r1.invoice_id
                                  || ','
                                  || 'Vendor No '
                                  || cur_upd_sel_inv_r1.vendor_num
                                 );
               v_err_msg :=
                      v_err_msg || ',' || 'Too many rows found for Voucher No';
               v_err_flag := 'E';
            WHEN OTHERS
            THEN
               fnd_file.put_line (fnd_file.LOG,
                                     'Invalid Voucher No '
                                  || cur_upd_sel_inv_r1.invoice_id
                                  || ','
                                  || 'Vendor No '
                                  || cur_upd_sel_inv_r1.vendor_num
                                 );
               v_err_msg := v_err_msg || ',' || 'Invalid Voucher No';
               v_err_flag := 'E';
         END;
      ELSE
         fnd_file.put_line (fnd_file.LOG,
                               'Voucher No is null'
                            || cur_upd_sel_inv_r1.invoice_id
                            || ','
                            || 'Vendor No '
                            || cur_upd_sel_inv_r1.vendor_num
                           );
         v_err_msg := v_err_msg || ',' || 'Voucher No is null';
         v_err_flag := 'E';
      END IF;

      /* IF cur_upd_sel_inv_r1.invoice_num is  not null  then
         dbms_output.put_line('invoice_num'||cur_upd_sel_inv_r1.voucher_num);
         fnd_file.put_line(fnd_file.output,'invoice_num'||cur_upd_sel_inv_r1.voucher_num);
         begin
          SELECT asi.invoice_num into l_inovice_num
            FROM ap_SELECTed_invoices_all asi
          where  asi.voucher_num = cur_upd_sel_inv_r1.voucher_num
            and  asi.invoice_num = cur_upd_sel_inv_r1.invoice_num;

                   if l_inovice_num is null  then
                        v_err_msg:=v_err_msg||','||'Invoice Number is not valid';
                        v_err_flag:='E';
                   end if;
          exception
          when others then
            v_err_msg:=v_err_msg||','||'Invalid Invoice Number';
            v_err_flag:='E';
            end;
        end if;*/
      IF v_err_msg IS NULL AND v_err_flag IS NULL
      THEN
         UPDATE ap_selected_invoices_all asi
            SET asi.ok_to_pay_flag = 'N',
                asi.dont_pay_reason_code = 'USER REMOVED'
          WHERE 1 = 1
            AND invoice_id = cur_upd_sel_inv_r1.invoice_id
            AND vendor_id = cur_upd_sel_inv_r1.vendor_id
            AND vendor_site_id = cur_upd_sel_inv_r1.vendor_site_id
            AND vendor_num = cur_upd_sel_inv_r1.vendor_num;

         fnd_file.put_line (fnd_file.LOG,
                               'Updated for Invoice id '
                            || cur_upd_sel_inv_r1.invoice_id
                            || ','
                            || 'Vendor No '
                            || cur_upd_sel_inv_r1.vendor_num
                           );

         UPDATE xxabrl_ap_selected_invoices asi
            SET process_flag = 'P',
                err_msg = NULL,
                err_flg = NULL
          WHERE 1 = 1
            AND invoice_id = cur_upd_sel_inv_r1.invoice_id
            AND vendor_id = cur_upd_sel_inv_r1.vendor_id
            AND vendor_site_id = cur_upd_sel_inv_r1.vendor_site_id
            AND vendor_num = cur_upd_sel_inv_r1.vendor_num;

         v_count := v_count + 1;
      ELSE
         UPDATE xxabrl_ap_selected_invoices xasi
            SET xasi.err_flg = 'E',
                xasi.err_msg = v_err_msg
          WHERE 1 = 1
            AND invoice_id = cur_upd_sel_inv_r1.invoice_id
            AND vendor_id = cur_upd_sel_inv_r1.vendor_id
            AND vendor_site_id = cur_upd_sel_inv_r1.vendor_site_id
            AND vendor_num = cur_upd_sel_inv_r1.vendor_num;

         v_err_count := v_err_count + 1;
      END IF;
   END LOOP;

   COMMIT;
   fnd_file.put_line (fnd_file.LOG, 'Total No of Updated Records' || v_count);
   fnd_file.put_line (fnd_file.LOG,
                      'Total No of Error Records' || v_err_count);
END xxabrl_upd_sel_inv_prog; 
/


CREATE OR REPLACE PROCEDURE APPS.XXABRL_CANCEL_INVOICE
(errbuf out varchar2,
 retcode out varchar2,
 p_org_id number)
AS

v_invoice_id            NUMBER      ;
v_last_updated_by       VARCHAR2(20);
v_last_update_login     VARCHAR2(20);
v_accounting_date       DATE        ;
v_boolean               BOOLEAN;


v_error_code            VARCHAR2(100);
v_debug_info            VARCHAR2(1000);


v_message_name          VARCHAR2(1000);
v_invoice_amount        NUMBER;
v_base_amount           NUMBER;
v_tax_amount            NUMBER;
v_temp_cancelled_amount NUMBER;
v_cancelled_by          VARCHAR2(1000);
v_cancelled_amount      NUMBER;
v_cancelled_date        DATE;
v_last_update_date      DATE;
v_token                 VARCHAR2(100);
v_orig_prepay_amt       NUMBER;
v_pay_cur_inv_amt       NUMBER;


cursor cur1 is 
select invoice_id,gl_date,invoice_amount from apps.XXABRL_CANCEL_INVOICES_STG CIS,apps.ap_invoices_all AIA,apps.ap_suppliers ass
where CIS.invoice_num=AIA.invoice_num
and AIA.vendor_id=ass.vendor_id
and ass.segment1=CIS.vendor_num
and AIA.org_id=p_org_id;

--- context done ------------ 
--DBMS_OUTPUT.put_line ('Calling API to check whetehr the Invoice is canellable ' );

begin

for I1 IN CUR1
LOOP


v_boolean :=AP_CANCEL_PKG.IS_INVOICE_CANCELLABLE(
                P_invoice_id       =>I1.INVOICE_ID,
                P_error_code       => v_error_code,    
                P_debug_info       => v_debug_info,
                P_calling_sequence => NULL);


IF v_boolean
THEN
fnd_file.put_line (Fnd_File.LOG,'Invoice '||v_invoice_id|| ' is cancellable' );
ELSE
fnd_file.put_line (Fnd_File.LOG,'Invoice '||v_invoice_id|| ' is not cancellable :'|| v_error_code );
END IF;     
 
fnd_file.put_line (Fnd_File.LOG,'Calling API to Cancel Invoice' );


v_boolean := AP_CANCEL_PKG.AP_CANCEL_SINGLE_INVOICE
            (p_invoice_id                 => I1.invoice_id,
             p_last_updated_by            => 4191,
             p_last_update_login          => 4191,
             p_accounting_date            => I1.GL_DATE,
              p_message_name               => v_message_name,
             p_invoice_amount             => v_invoice_amount,
             p_base_amount                => v_base_amount,
             p_temp_cancelled_amount      => v_temp_cancelled_amount,
             p_cancelled_by               => v_cancelled_by,
             p_cancelled_amount           => v_cancelled_amount,
             p_cancelled_date             => v_cancelled_date,
             p_last_update_date           => v_last_update_date,
             p_original_prepayment_amount => v_orig_prepay_amt,
             p_pay_curr_invoice_amount    => v_pay_cur_inv_amt,
             P_Token                      => v_token,
             p_calling_sequence           => NULL
             );


IF v_boolean
THEN
fnd_file.put_line (Fnd_File.LOG,'Successfully Cancelled the Invoice' );
COMMIT;
ELSE
fnd_file.put_line (Fnd_File.LOG,'Failed to Cancel the Invoice' );
ROLLBACK;
END IF;

END LOOP;


END XXABRL_CANCEL_INVOICE; 
/


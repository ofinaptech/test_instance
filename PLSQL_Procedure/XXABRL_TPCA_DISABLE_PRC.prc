CREATE OR REPLACE PROCEDURE APPS.XXABRL_TPCA_DISABLE_PRC(ERRBUF  OUT VARCHAR2,
                                                    RETCODE OUT VARCHAR2) AS


V_GL_ACCESS_SET_ID   NUMBER;
LN_LOADER_REQUEST_ID NUMBER;

CURSOR C_TPCA_DISABLE IS
    SELECT XTPD.COMPILED_VALUE_ATTRIBUTES_D,FFVL.* FROM XXABRL.XXABRL_TPCA_DISABLE XTPD, APPS.FND_FLEX_VALUES_VL FFVL
    WHERE XTPD.FLEX_VALUE = FFVL.FLEX_VALUE
    AND FFVL.FLEX_VALUE_SET_ID=1013469;
    
 BEGIN

       

    FOR R_TPCA_DISABLE IN C_TPCA_DISABLE 
    LOOP

        BEGIN
        
        FND_FLEX_VALUES_PKG.UPDATE_ROW(
                 X_FLEX_VALUE_ID    =>       R_TPCA_DISABLE.FLEX_VALUE_ID
                ,X_ATTRIBUTE_SORT_ORDER    =>       R_TPCA_DISABLE.ATTRIBUTE_SORT_ORDER
                ,X_FLEX_VALUE_SET_ID    =>       R_TPCA_DISABLE.FLEX_VALUE_SET_ID
                ,X_FLEX_VALUE    =>       R_TPCA_DISABLE.FLEX_VALUE
                ,X_ENABLED_FLAG    =>       R_TPCA_DISABLE.ENABLED_FLAG
                ,X_SUMMARY_FLAG    =>       R_TPCA_DISABLE.SUMMARY_FLAG
                ,X_START_DATE_ACTIVE    =>       R_TPCA_DISABLE.START_DATE_ACTIVE
                ,X_END_DATE_ACTIVE    =>       R_TPCA_DISABLE.END_DATE_ACTIVE
                ,X_PARENT_FLEX_VALUE_LOW    =>       R_TPCA_DISABLE.PARENT_FLEX_VALUE_LOW
                ,X_PARENT_FLEX_VALUE_HIGH    =>       R_TPCA_DISABLE.PARENT_FLEX_VALUE_HIGH
                ,X_STRUCTURED_HIERARCHY_LEVEL    =>       R_TPCA_DISABLE.STRUCTURED_HIERARCHY_LEVEL
                ,X_HIERARCHY_LEVEL    =>       R_TPCA_DISABLE.HIERARCHY_LEVEL
                ,X_COMPILED_VALUE_ATTRIBUTES    =>       R_TPCA_DISABLE.COMPILED_VALUE_ATTRIBUTES_D -- Third Party Control Account Disabled
                ,X_VALUE_CATEGORY    =>   R_TPCA_DISABLE.VALUE_CATEGORY
                ,X_ATTRIBUTE1    =>       R_TPCA_DISABLE.ATTRIBUTE1
                ,X_ATTRIBUTE2    =>       R_TPCA_DISABLE.ATTRIBUTE2
                ,X_ATTRIBUTE3    =>       R_TPCA_DISABLE.ATTRIBUTE3
                ,X_ATTRIBUTE4    =>       R_TPCA_DISABLE.ATTRIBUTE4
                ,X_ATTRIBUTE5    =>       R_TPCA_DISABLE.ATTRIBUTE5
                ,X_ATTRIBUTE6    =>       R_TPCA_DISABLE.ATTRIBUTE6
                ,X_ATTRIBUTE7    =>       R_TPCA_DISABLE.ATTRIBUTE7
                ,X_ATTRIBUTE8    =>       R_TPCA_DISABLE.ATTRIBUTE8
                ,X_ATTRIBUTE9    =>       R_TPCA_DISABLE.ATTRIBUTE9
                ,X_ATTRIBUTE10    =>       R_TPCA_DISABLE.ATTRIBUTE10
                ,X_ATTRIBUTE11    =>       R_TPCA_DISABLE.ATTRIBUTE11
                ,X_ATTRIBUTE12    =>       R_TPCA_DISABLE.ATTRIBUTE12
                ,X_ATTRIBUTE13    =>       R_TPCA_DISABLE.ATTRIBUTE13
                ,X_ATTRIBUTE14    =>       R_TPCA_DISABLE.ATTRIBUTE14
                ,X_ATTRIBUTE15    =>       R_TPCA_DISABLE.ATTRIBUTE15
                ,X_ATTRIBUTE16    =>       R_TPCA_DISABLE.ATTRIBUTE16
                ,X_ATTRIBUTE17    =>       R_TPCA_DISABLE.ATTRIBUTE17
                ,X_ATTRIBUTE18    =>       R_TPCA_DISABLE.ATTRIBUTE18
                ,X_ATTRIBUTE19    =>       R_TPCA_DISABLE.ATTRIBUTE19
                ,X_ATTRIBUTE20    =>       R_TPCA_DISABLE.ATTRIBUTE20
                ,X_ATTRIBUTE21    =>       R_TPCA_DISABLE.ATTRIBUTE21
                ,X_ATTRIBUTE22    =>       R_TPCA_DISABLE.ATTRIBUTE22
                ,X_ATTRIBUTE23    =>       R_TPCA_DISABLE.ATTRIBUTE23
                ,X_ATTRIBUTE24    =>       R_TPCA_DISABLE.ATTRIBUTE24
                ,X_ATTRIBUTE25    =>       R_TPCA_DISABLE.ATTRIBUTE25
                ,X_ATTRIBUTE26    =>       R_TPCA_DISABLE.ATTRIBUTE26
                ,X_ATTRIBUTE27    =>       R_TPCA_DISABLE.ATTRIBUTE27
                ,X_ATTRIBUTE28    =>       R_TPCA_DISABLE.ATTRIBUTE28
                ,X_ATTRIBUTE29    =>       R_TPCA_DISABLE.ATTRIBUTE29
                ,X_ATTRIBUTE30    =>       R_TPCA_DISABLE.ATTRIBUTE30
                ,X_ATTRIBUTE31    =>       R_TPCA_DISABLE.ATTRIBUTE31
                ,X_ATTRIBUTE32    =>       R_TPCA_DISABLE.ATTRIBUTE32
                ,X_ATTRIBUTE33    =>       R_TPCA_DISABLE.ATTRIBUTE33
                ,X_ATTRIBUTE34    =>       R_TPCA_DISABLE.ATTRIBUTE34
                ,X_ATTRIBUTE35    =>       R_TPCA_DISABLE.ATTRIBUTE35
                ,X_ATTRIBUTE36    =>       R_TPCA_DISABLE.ATTRIBUTE36
                ,X_ATTRIBUTE37    =>       R_TPCA_DISABLE.ATTRIBUTE37
                ,X_ATTRIBUTE38    =>       R_TPCA_DISABLE.ATTRIBUTE38
                ,X_ATTRIBUTE39    =>       R_TPCA_DISABLE.ATTRIBUTE39
                ,X_ATTRIBUTE40    =>       R_TPCA_DISABLE.ATTRIBUTE40
                ,X_ATTRIBUTE41    =>       R_TPCA_DISABLE.ATTRIBUTE41
                ,X_ATTRIBUTE42    =>       R_TPCA_DISABLE.ATTRIBUTE42
                ,X_ATTRIBUTE43    =>       R_TPCA_DISABLE.ATTRIBUTE43
                ,X_ATTRIBUTE44    =>       R_TPCA_DISABLE.ATTRIBUTE44
                ,X_ATTRIBUTE45    =>       R_TPCA_DISABLE.ATTRIBUTE45
                ,X_ATTRIBUTE46    =>       R_TPCA_DISABLE.ATTRIBUTE46
                ,X_ATTRIBUTE47    =>       R_TPCA_DISABLE.ATTRIBUTE47
                ,X_ATTRIBUTE48    =>       R_TPCA_DISABLE.ATTRIBUTE48
                ,X_ATTRIBUTE49    =>       R_TPCA_DISABLE.ATTRIBUTE49
                ,X_ATTRIBUTE50    =>       R_TPCA_DISABLE.ATTRIBUTE50
                ,X_FLEX_VALUE_MEANING    =>       R_TPCA_DISABLE.FLEX_VALUE_MEANING
                ,X_DESCRIPTION    =>       R_TPCA_DISABLE.DESCRIPTION
                ,X_LAST_UPDATE_DATE    =>  SYSDATE--     R_TPCA_DISABLE.LAST_UPDATE_DATE
                ,X_LAST_UPDATED_BY    =>   '4191' --    R_TPCA_DISABLE.LAST_UPDATED_BY
                ,X_LAST_UPDATE_LOGIN    =>       R_TPCA_DISABLE.LAST_UPDATE_LOGIN 
                    );
         
        UPDATE  XXABRL.XXABRL_TPCA_DISABLE XTPD
        SET XTPD.STATUS_FLAG = 'P',XTPD.LAST_UPDATE_DATE=SYSDATE
        WHERE XTPD.FLEX_VALUE = R_TPCA_DISABLE.FLEX_VALUE;
        
        COMMIT;

        EXCEPTION
                    WHEN OTHERS THEN
                    UPDATE  XXABRL.XXABRL_TPCA_DISABLE XTPD
                    SET XTPD.STATUS_FLAG = 'E',XTPD.LAST_UPDATE_DATE = SYSDATE
                    WHERE XTPD.FLEX_VALUE = R_TPCA_DISABLE.FLEX_VALUE;
                    
                DBMS_OUTPUT.PUT_LINE('Unable to Update Stage Table' );
            
        END;
                 
    END LOOP;
    
    
     V_GL_ACCESS_SET_ID := FND_PROFILE.VALUE('GL_ACCESS_SET_ID');
          
            IF V_GL_ACCESS_SET_ID IS NULL THEN
              FND_FILE.PUT_LINE(FND_FILE.LOG,
                                'GL_ACCESS_SET_ID / Ledger ID Setup Incomplete...');
            ELSE
              FND_FILE.PUT_LINE(FND_FILE.LOG,
                                'GL_ACCESS_SET_ID: ' || V_GL_ACCESS_SET_ID);
            END IF;
    
     BEGIN
          LN_LOADER_REQUEST_ID :=
             fnd_request.submit_request
                                        ('SQLGL', -- application
                                         'GLNSVI', -- program
                                         '', -- description
                                         '',  -- start_time
                                         FALSE, -- sub_request
                                         V_GL_ACCESS_SET_ID,--1120, -- argument1
                                         'Yes' -- argument2
                                        );
         
         COMMIT;
         
    IF (LN_LOADER_REQUEST_ID = 0) THEN
            
              fnd_file.put_line(fnd_file.log,
                                'Request Not Submitted due to "' ||
                                fnd_message.get || '".');
              fnd_file.put_line(FND_FILE.Log,
                                'Concurrent Request For Loading STD. GL IMPORT Generated an error, Submitted with request id ' ||
                                ln_loader_request_id);
            
            ELSE
              fnd_file.put_line(FND_FILE.Log,
                                'Submitted Loader Program with request id : ' ||
                                TO_CHAR(ln_loader_request_id));
            END IF;
          
    EXCEPTION
      WHEN OTHERS
      THEN
         fnd_file.put_line
                (fnd_file.LOG,
                    'Exception while Request submit '
                 || SQLERRM
                );
    END;
                
 END XXABRL_TPCA_DISABLE_PRC; 
/


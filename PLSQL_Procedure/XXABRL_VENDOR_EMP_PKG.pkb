CREATE OR REPLACE PACKAGE BODY APPS.XXABRL_VENDOR_EMP_PKG AS
  PROCEDURE MAIN(Errbuf   OUT VARCHAR2,
                 RetCode  OUT NUMBER,
                 p_action VARCHAR2) IS
    vRetCode NUMBER;
  BEGIN
    --Validate Data
    VALIDATE_VENDOR_INFO(vRetCode);
    /*if vRetCode <> 0 then
    RetCode :=1;
    End if;*/
    --call insert
    IF p_action = 'N' THEN
      XXABRL_INSERT_VENDOR_MIGR_INFO;
    END IF;
  END MAIN;

  PROCEDURE VALIDATE_VENDOR_INFO(RetCode OUT NUMBER) AS
    x_msg              VARCHAR2(2000);
    v_error_hmsg       VARCHAR2(2000);
    v_error_smsg       VARCHAR2(2000);
    v_error_cmsg       VARCHAR2(2000);
    v_error_tmsg       VARCHAR2(2000);
    v_Error_Sup_Count  NUMBER := 0;
    v_Error_site_Count NUMBER := 0;
    v_valid_sup_Count  NUMBER := 0;
    v_valid_site_Count NUMBER := 0;
    X_SHIP_TO_LOC_ID   NUMBER;
    X_BILL_TO_LOC_ID   NUMBER;
    X_TERM_ID          NUMBER;
    X_EMPLOYEE_ID      NUMBER;
    x_pay_group        VARCHAR2(240);
    v_lookup_code      VARCHAR2(240);
    ln_TDS_LOOKUP_CODE NUMBER;

    CURSOR c1 IS
      SELECT ROWID, SUPPLIERS.*
        FROM XXABRL_SUPPLIER_EMP_INT suppliers
       WHERE NVL(SUPPLIERS.Process_Flag, 'N') IN ('N', 'E');
    CURSOR c2(cp_vendor_name VARCHAR2) IS
      SELECT ROWID, sup_sites.*
        FROM XXABRL_SUPPLIER_SITES_EMP_INT sup_sites
       WHERE vendor_name = cp_vendor_name
         AND NVL(sup_sites.Process_Flag, 'N') IN ('N', 'E');

  BEGIN
    FND_FILE.PUT_LINE(FND_FILE.LOG, 'vendor validation start');

    ln_TDS_LOOKUP_CODE := 0;

    FND_FILE.PUT_LINE(FND_FILE.LOG,
                      '#### Validation Start for Tax Info ####');

    /*For c_cur4 in C4 Loop
      if c_cur4.TDS_VENDOR_TYPE_LOOKUP_CODE is null then
        FND_FILE.PUT_LINE(FND_FILE.LOG,
                          'TDS VENDOR TYPE Lookup Code is NULL');

        ln_TDS_LOOKUP_CODE := ln_TDS_LOOKUP_CODE + 1;

      Else

        BEGIN
          --        FND_FILE.PUT_LINE(FND_FILE.LOG, 'Debug C_CUR4');
          SELECT LOOKUP_CODE
            into v_lookup_code
            FROM FND_LOOKUP_VALUES_VL
           WHERE lookup_type = 'JAI_TDS_VENDOR_TYPE'
             and upper(LOOKUP_CODE) =
                 upper(c_cur4.TDS_VENDOR_TYPE_LOOKUP_CODE);
          --        FND_FILE.PUT_LINE(FND_FILE.LOG, 'debug after select');

          --## use rowid

          FND_FILE.PUT_LINE(FND_FILE.LOG,
                            'Valid TDS Vendor Type Lookup Code ' ||
                            upper(c_cur4.TDS_VENDOR_TYPE_LOOKUP_CODE));
          --commit;

        EXCEPTION
          when no_data_found then
            FND_FILE.PUT_LINE(FND_FILE.LOG,
                              'TDS VENDOR TYPE Lookup Code not defined for: ' ||
                              upper(c_cur4.TDS_VENDOR_TYPE_LOOKUP_CODE));
            v_error_tmsg       := v_error_tmsg ||
                                  'TDS VENDOR TYPE Lookup Code not defined for: ' ||
                                  c_cur4.TDS_VENDOR_TYPE_LOOKUP_CODE;
            ln_TDS_LOOKUP_CODE := ln_TDS_LOOKUP_CODE + 1;
          When too_many_rows then
            FND_FILE.PUT_LINE(FND_FILE.LOG,
                              'Too many rows for TDS VENDOR TYPE Lookup Code: ' ||
                              upper(c_cur4.TDS_VENDOR_TYPE_LOOKUP_CODE));
            ln_TDS_LOOKUP_CODE := ln_TDS_LOOKUP_CODE + 1;
            v_error_tmsg       := v_error_tmsg ||
                                  'too many TDS VENDOR TYPE Lookup Codes defined for: ' ||
                                  c_cur4.TDS_VENDOR_TYPE_LOOKUP_CODE;
        END;
      End if;
      v_error_tmsg := NULL;
      --      FND_FILE.PUT_LINE(FND_FILE.LOG, 'after exception');

    End Loop;*/

    FND_FILE.PUT_LINE(FND_FILE.LOG,
                      '@@@ In-Valid/NULL Tax Code count:' ||
                      ln_TDS_LOOKUP_CODE);
    FND_FILE.PUT_LINE(FND_FILE.LOG,
                      '#### Validation ENDS for Tax Info ####');

    FOR c_cur1 IN C1

     LOOP

      FND_FILE.PUT_LINE(FND_FILE.LOG,
                        'Validation Start for Vendor:' ||
                        c_cur1.VENDOR_NAME);

      --VALIDATION FOR VENDOR NAME
      IF c_cur1.VENDOR_NAME IS NULL THEN
        v_error_hmsg := v_error_hmsg || ',' || 'Vendor name is null';
      END IF;

      --VALIDATION FOR VENDOR Number
      IF c_cur1.VENDOR_NUMBER IS NULL THEN
        v_error_hmsg := v_error_hmsg || ',' || 'Vendor Number is null';
      END IF;
      /*
      --VALIDATION FOR Vendor_Name_ALT
         if c_cur1.Vendor_Name_ALT is null then
           v_error_cmsg := v_error_cmsg || ',' || 'Vendor name alter is null';
         end if;
         */

      --Validation for vendor type lookup code
      IF c_cur1.VENDOR_TYPE_LOOKUP_CODE IS NULL THEN
        v_error_hmsg := v_error_hmsg || ',' ||
                        'vendor type lookup code is null';
      ELSE

        validate_vendor_type(c_cur1.VENDOR_TYPE_LOOKUP_CODE, x_msg);
        IF x_msg IS NOT NULL THEN
          v_error_hmsg := v_error_hmsg || ',' || x_msg;
        END IF;
      END IF;
      FND_FILE.PUT_LINE(FND_FILE.LOG, 'vendor terms validation ');

      -- vendor_number validation

      IF c_cur1.vendor_number IS NULL THEN
        v_error_hmsg := v_error_hmsg || ',' || 'vendor number is null';
      ELSE
        validate_vendor_number(c_cur1.vendor_number, x_msg);

        IF x_msg IS NOT NULL THEN
          v_error_hmsg := v_error_hmsg || ',' || x_msg;
        END IF;
      END IF;
        ----- VENDOR PAYMENT METHOD LOOKUP CODE VALIDATION----ADDED BY SRINI---- Start
       IF c_cur1.PAYMENT_METHOD_LOOKUP_CODE IS NULL THEN
          v_error_hmsg := v_error_hmsg || ',' ||
                          'PAYMENT_METHOD_LOOKUP_CODE is null';
        ELSE
          validate_payment_method(c_cur1.PAYMENT_METHOD_LOOKUP_CODE, x_msg);
          IF x_msg IS NOT NULL THEN
            v_error_hmsg := v_error_hmsg || ',' || x_msg;
          END IF;
        END IF;
       ----- VENDOR PAYMENT METHOD LOOKUP CODE VALIDATION----ADDED BY SRINI----  End

      -- DERIVATION OF  EMPLOYEE_ID
      IF c_cur1.VENDOR_NAME IS NOT NULL THEN
        DERIVE_EMPLOYEE_ID(c_cur1.VENDOR_NUMBER, X_EMPLOYEE_ID, X_MSG);
      ELSE
        v_error_hmsg := v_error_hmsg || ',' || X_MSG;
      END IF;
      /*
      -- Validation for GLOBAL_ATTRIBUTE_CATEGORY
      if c_cur1.GLOBAL_ATTRIBUTE_CATEGORY is null then
        v_error_cmsg := v_error_cmsg || ',' ||
                        'GLOBAL_ATTRIBUTE_CATEGORY is null';
      end if;
      */
      /*
        -- Validation for Primary_Vendor_Category
        if c_cur1.Primary_Vendor_Category is null then
          v_error_cmsg := v_error_cmsg || ',' ||
                          'Primary_Vendor_Category is null';
        end if;
      */

      --Updating vendors staging table
      IF v_error_hmsg IS NOT NULL THEN
        FND_FILE.PUT_LINE(FND_FILE.LOG, '***Vendor Invalid');
        UPDATE XXABRL_SUPPLIER_EMP_INT
           SET error_msg = v_error_hmsg, process_flag = 'E'
         WHERE ROWID = c_cur1.ROWID;
        v_Error_Sup_Count := v_Error_Sup_Count + 1;

      ELSE
        FND_FILE.PUT_LINE(FND_FILE.LOG, 'Vendor Valid');
        UPDATE XXABRL_SUPPLIER_EMP_INT
           SET process_flag = 'V',
               error_msg    = NULL,
               EMPLOYEE_ID  = X_EMPLOYEE_ID
         WHERE ROWID = c_cur1.ROWID;
        v_valid_sup_Count := v_valid_Sup_Count + 1;
        COMMIT;
      END IF;

      v_error_hmsg := NULL;
      ---vendor sites loop start
      FOR c_cur2 IN C2(c_cur1.vendor_name) LOOP
        --## vendor site loop
        FND_FILE.PUT_LINE(FND_FILE.LOG,
                          'Site validation Starts ' ||
                          c_cur2.vendor_site_code);
        -- Operating unit validation

        IF c_cur2.OPERATING_UNIT IS NULL THEN
          v_error_smsg := v_error_smsg || ',' || 'OPERATING_UNIT is null';
        ELSE
          validate_operating_unit(c_cur2.OPERATING_UNIT, x_msg);

          IF x_msg IS NOT NULL THEN
            v_error_smsg := v_error_smsg || ',' || x_msg;
          END IF;
        END IF;

        -- vendor number range validation for each vendor type
        FND_FILE.PUT_LINE(FND_FILE.LOG,
                          '****vendor range validation starts');
        IF c_cur2.OPERATING_UNIT LIKE 'ABRL%' THEN
          FND_FILE.PUT_LINE(FND_FILE.LOG,
                            'vendor type lookup code validation ' ||
                            c_cur1.VENDOR_TYPE_LOOKUP_CODE);

          FND_FILE.PUT_LINE(FND_FILE.LOG,
                            'vendor number ' || c_cur1.VENDOR_NUMBER);
          IF c_cur1.VENDOR_TYPE_LOOKUP_CODE = 'EMPLOYEE' AND
             c_cur1.VENDOR_NUMBER NOT BETWEEN 1000000 AND 1899999 THEN
            v_error_smsg := v_error_smsg || ',' ||
                            'Please enter Vendor Number between  1000000  and 1899999';
          END IF;

          IF c_cur1.VENDOR_TYPE_LOOKUP_CODE = 'MERCHANDISE' AND
             ((c_cur1.VENDOR_NUMBER NOT BETWEEN 9600000 AND 9699999) AND
             (c_cur1.VENDOR_NUMBER NOT BETWEEN 2000000 AND 2999999)) THEN
            v_error_smsg := v_error_smsg || ',' ||
                            'Please enter Vendor Number between  2000000  and 2999999 or 9600000 and 9699999';
          END IF;

          IF c_cur1.VENDOR_TYPE_LOOKUP_CODE = 'CONSUMABLES' AND
             c_cur1.VENDOR_NUMBER NOT BETWEEN 50000000 AND 51999999 THEN
            v_error_smsg := v_error_smsg || ',' ||
                            'Please enter Vendor Number between  50000000  and 51999999';
          END IF;

          IF c_cur1.VENDOR_TYPE_LOOKUP_CODE = 'NON MERCHANDISE' AND
             c_cur1.VENDOR_NUMBER NOT BETWEEN '3000000' AND '3999999' THEN
            v_error_smsg := v_error_smsg || ',' ||
                            'Please enter Vendor Number between  3000000  and 3999999';
            FND_FILE.PUT_LINE(FND_FILE.LOG, 'NON MERCHANDISE Error');
          END IF;

          IF c_cur1.VENDOR_TYPE_LOOKUP_CODE = 'SERVICES' AND
             c_cur1.VENDOR_NUMBER NOT BETWEEN 3000000 AND 3999999 THEN
            v_error_smsg := v_error_smsg || ',' ||
                            'Please enter Vendor Number between  3000000  and 3999999';
          END IF;

          IF c_cur1.VENDOR_TYPE_LOOKUP_CODE = 'CAPEX' AND
             c_cur1.VENDOR_NUMBER NOT BETWEEN 4000000 AND 4999999 THEN
            v_error_smsg := v_error_smsg || ',' ||
                            'Please enter Vendor Number between  4000000  and 4999999';
          END IF;

          IF c_cur1.VENDOR_TYPE_LOOKUP_CODE = 'BULK (RPC & FNV)' AND
             c_cur1.VENDOR_NUMBER NOT BETWEEN 7000000 AND 7199999 THEN
            v_error_smsg := v_error_smsg || ',' ||
                            'Please enter Vendor Number between  7000000  and 7199999';
          END IF;

          IF c_cur1.VENDOR_TYPE_LOOKUP_CODE = 'INTERNAL VENDOR' AND
             c_cur2.PAY_GROUP_LOOKUP_CODE = 'DC/RDC' AND
             c_cur1.VENDOR_NUMBER NOT BETWEEN 1000 AND 1999 THEN
            v_error_smsg := v_error_smsg || ',' ||
                            'Please enter Vendor Number between  1000  and 1999';
          END IF;
          IF c_cur1.VENDOR_TYPE_LOOKUP_CODE = 'INTERNAL VENDOR' AND
             c_cur2.PAY_GROUP_LOOKUP_CODE = 'RPC' AND
             c_cur1.VENDOR_NUMBER NOT BETWEEN 2001 AND 1999 THEN
            v_error_smsg := v_error_smsg || ',' ||
                            'Please enter Vendor Number between  2001  and 2999';
          END IF;

          IF c_cur1.VENDOR_TYPE_LOOKUP_CODE = 'INTERNAL VENDOR' AND
             c_cur2.PAY_GROUP_LOOKUP_CODE = 'F Centres' AND
             c_cur1.VENDOR_NUMBER NOT BETWEEN 3001 AND 3999 THEN
            v_error_smsg := v_error_smsg || ',' ||
                            'Please enter Vendor Number between  3001  and 3999';
          END IF;

          IF c_cur1.VENDOR_TYPE_LOOKUP_CODE = 'STATUTORY' AND
             c_cur1.VENDOR_NUMBER NOT BETWEEN 9800000 AND 9899999 THEN
            v_error_smsg := v_error_smsg || ',' ||
                            'Please enter Vendor Number between  9800000  and 9899999';
          END IF;

        ELSIF c_cur2.OPERATING_UNIT LIKE 'TSRL%' THEN

          IF c_cur1.VENDOR_TYPE_LOOKUP_CODE = 'EMPLOYEE' AND
             c_cur1.VENDOR_NUMBER NOT BETWEEN 1900000 AND 1999999 THEN
            v_error_smsg := v_error_smsg || ',' ||
                            'Please enter Vendor Number between  1900000  and 1999999';
          END IF;

          IF c_cur1.VENDOR_TYPE_LOOKUP_CODE = 'TSRL Vendor Codes' AND
             c_cur1.VENDOR_NUMBER NOT BETWEEN 9700000 AND 9799999 THEN
            v_error_smsg := v_error_smsg || ',' ||
                            'Please enter Vendor Number between 9700000   and 9799999';
          END IF;

          IF c_cur1.VENDOR_TYPE_LOOKUP_CODE = 'STATUTORY' AND
             c_cur1.VENDOR_NUMBER NOT BETWEEN 9800000 AND 9899999 THEN
            v_error_smsg := v_error_smsg || ',' ||
                            'Please enter Vendor Number between  9800000  and 9899999';
          END IF;

        END IF;

        --VALIDATION FOR VENDOR NAME
        IF c_cur2.VENDOR_NAME IS NULL THEN
          v_error_smsg := v_error_smsg || ',' || 'Vendor name is null';
        END IF;
        -- vendor_site_code  validation
        IF c_cur2.vendor_site_code IS NULL THEN
          v_error_smsg := v_error_smsg || ',' || 'vendor site code is null';
        ELSE
          validate_vendor_site_code(c_cur2.vendor_site_code,
                                    c_cur2.VENDOR_NAME,
                                    x_msg);
          IF x_msg IS NOT NULL THEN
            v_error_smsg := v_error_smsg || ',' || x_msg;
          END IF;
        END IF;
        /* --VALIDATION FOR ADDRESS LINE1
        if c_cur2.ADDRESS_LINE1 is null then
          v_error_smsg := v_error_smsg || ',' || 'ADDRESS_LINE1 is null';
        end if;
        --VALIDATION FOR ADDRESS LINE2
        if c_cur2.ADDRESS_LINE2 is null then
          v_error_smsg := v_error_smsg || ',' || 'ADDRESS_LINE2 is null';
        end if;
        --VALIDATION FOR CITY
        if c_cur2.city is null then
          v_error_smsg := v_error_smsg || ',' || 'CITY is null';
        end if;
        --VALIDATION FOR STATE
        if c_cur2.state is null then
          v_error_smsg := v_error_smsg || ',' || 'STATE is null';
        end if;
         */
        -- COUNTRY CODE validation
        IF c_cur2.country IS NULL THEN
          v_error_smsg := v_error_smsg || ',' || 'COUNTRY is null';
        ELSE
          validate_country_code(c_cur2.country, x_msg);
          IF x_msg IS NOT NULL THEN
            v_error_smsg := v_error_smsg || ',' || x_msg;
          END IF;
        END IF;

        -- vendor terms validation
        IF c_cur2.terms_name IS NULL THEN
          v_error_smsg := v_error_smsg || ',' || 'Terms name is null';
        ELSE
          validate_terms_name(c_cur2.terms_name, x_msg);
          IF x_msg IS NOT NULL THEN
            v_error_smsg := v_error_smsg || ',' || x_msg;
          END IF;
        END IF;

        -- PAYMENT_METHOD_LOOKUP_CODE validation
        IF c_cur2.PAYMENT_METHOD_LOOKUP_CODE IS NULL THEN
          v_error_smsg := v_error_smsg || ',' ||
                          'PAYMENT_METHOD_LOOKUP_CODE is null';
        ELSE
          validate_payment_method(c_cur2.PAYMENT_METHOD_LOOKUP_CODE, x_msg);
          IF x_msg IS NOT NULL THEN
            v_error_smsg := v_error_smsg || ',' || x_msg;
          END IF;
        END IF;

        -- PAY_GROUP_LOOKUP_CODE validation
        IF c_cur2.PAY_GROUP_LOOKUP_CODE IS NULL THEN
          v_error_smsg := v_error_smsg || ',' ||
                          'PAY_GROUP_LOOKUP_CODE is null';
        ELSE
          validate_pay_group(c_cur2.PAY_GROUP_LOOKUP_CODE,
                             x_pay_group,
                             x_msg);
          IF x_msg IS NOT NULL THEN
            v_error_smsg := v_error_smsg || ',' || x_msg;
          END IF;
        END IF;

        -- DERIVATION OF  SHIP_TO_LOCTION_ID
        IF c_cur2.SHIP_TO_LOCATION_CODE IS NOT NULL THEN
          DERIVE_SHIT_TO_LOCATION_ID(c_cur2.SHIP_TO_LOCATION_CODE,
                                     X_SHIP_TO_LOC_ID,
                                     X_MSG);
          /* ELSE
          v_error_smsg := v_error_smsg || ',' || X_MSG;*/
        END IF;

        -- DERIVATION OF  BILL_TO_LOCTION_ID
        IF c_cur2.BILL_TO_LOCATION_CODE IS NOT NULL THEN
          DERIVE_BILL_TO_LOCATION_ID(c_cur2.BILL_TO_LOCATION_CODE,
                                     X_BILL_TO_LOC_ID,
                                     X_MSG);
          /*ELSE
          v_error_smsg := v_error_smsg || ',' || X_MSG;*/
        END IF;

        -- DERIVATION OF  TERM_ID
        IF c_cur2.TERMS_NAME IS NOT NULL THEN
          DERIVE_TERM_ID(c_cur2.TERMS_NAME, X_TERM_ID, X_MSG);
        ELSE
          v_error_smsg := v_error_smsg || ',' || X_MSG;
        END IF;
        FND_FILE.PUT_LINE(FND_FILE.LOG,
                          'Site Validation Ends for ' ||
                          c_cur2.vendor_site_code);
        --updating vendor site staging table
        /*       if v_error_smsg is not null then
          FND_FILE.PUT_LINE(FND_FILE.LOG, 'vendor site error update');
          Update XXABRL_SUPPLIER_SITES_EMP_INT
             Set error_msg = v_error_smsg, process_flag = 'E'
           Where ROWID = c_cur2.ROWID;
          v_Error_Sup_Count := v_Error_Sup_Count + 1;

        Else
          FND_FILE.PUT_LINE(FND_FILE.LOG, 'vendor site valid update');
          Update XXABRL_SUPPLIER_SITES_EMP_INT
             Set process_flag        = 'V',
                 error_msg           = NULL,
                 BILL_TO_LOCATION_ID = X_BILL_TO_LOC_ID,
                 SHIP_TO_LOCATION_ID = X_SHIP_TO_LOC_ID,
                 TERM_ID             = X_TERM_ID,
                 DERIVED_PAY_GROUP   = X_PAY_GROUP
           Where ROWID = c_cur2.ROWID;
          v_valid_sup_Count := v_valid_Sup_Count + 1;
          Commit;
        End If;
        v_error_smsg := null;*/

        IF /*v_error_hmsg is null and */
         v_error_smsg IS NOT NULL THEN
          FND_FILE.PUT_LINE(FND_FILE.LOG,
                            '***Invalid Sites' || v_error_smsg);

          UPDATE XXABRL_SUPPLIER_SITES_EMP_INT
             SET error_msg = v_error_smsg, process_flag = 'E'
           WHERE ROWID = c_cur2.ROWID;
          v_Error_site_Count := v_Error_site_Count + 1;

          UPDATE XXABRL_SUPPLIER_EMP_INT
             SET error_msg    = 'Error in vendor sites;' || v_error_smsg,
                 process_flag = 'E'
           WHERE ROWID = c_cur1.ROWID;
          --v_Error_Sup_Count := v_Error_Sup_Count + 1;

        ELSE
          FND_FILE.PUT_LINE(FND_FILE.LOG, 'Valid Site');
          UPDATE XXABRL_SUPPLIER_SITES_EMP_INT
             SET process_flag        = 'V',
                 error_msg           = NULL,
                 BILL_TO_LOCATION_ID = X_BILL_TO_LOC_ID,
                 SHIP_TO_LOCATION_ID = X_SHIP_TO_LOC_ID,
                 TERM_ID             = X_TERM_ID,
                 DERIVED_PAY_GROUP   = X_PAY_GROUP
           WHERE ROWID = c_cur2.ROWID;
          v_valid_sup_Count := v_valid_Sup_Count + 1;
        END IF;
        v_error_hmsg := NULL;
        v_error_smsg := NULL;
        ---vendor sites contacts loop start
        /*For c_cur3 in C3(c_cur1.vendor_name, c_cur2.vendor_site_code) loop
          FND_FILE.PUT_LINE(FND_FILE.LOG,
                            'Contact Validation Starts for ' ||
                            c_cur3.VENDOR_NAME);
          -- Operating unit validation
          if c_cur3.OPERATING_UNIT is null then
            v_error_cmsg := v_error_cmsg || ',' || 'OPERATING_UNIT is null';
          end if;
          --VALIDATION FOR VENDOR NAME
          if c_cur3.VENDOR_NAME is null then
            v_error_cmsg := v_error_cmsg || ',' || 'Vendor name is null';
          end if;
          -- vendor_site_code  validation
          if c_cur2.vendor_site_code is null then
            v_error_cmsg := v_error_cmsg || ',' ||
                            'vendor site code is null';
          end if;
          \*    -- first name validation
          if c_cur2.vendor_site_code is not null and
             c_cur3.FIRST_NAME is null then
            v_error_cmsg := v_error_cmsg || ',' || 'first name is null';
          end if;
          *\




          v_error_cmsg := null;
          FND_FILE.PUT_LINE(FND_FILE.LOG,
                            'Contact Validation Ends for ' ||
                            c_cur3.VENDOR_NAME);
        end loop;*/
      END LOOP;
      FND_FILE.PUT_LINE(FND_FILE.LOG,
                        'Validation Ends for Vendor ' || c_cur1.VENDOR_NAME);
      FND_FILE.PUT_LINE(FND_FILE.LOG,
                        '--------------------------------------------------' ||
                        c_cur1.VENDOR_NAME);
    END LOOP;

    FND_FILE.PUT_LINE(FND_FILE.LOG,
                      '@@@Invalid Vendors ' || v_Error_sup_Count);
    FND_FILE.PUT_LINE(FND_FILE.LOG,
                      '@@@Invalid Sites ' || v_Error_site_Count);

    FND_FILE.PUT_LINE(FND_FILE.LOG, '***Validation Procedure Ends***');
  END VALIDATE_VENDOR_INFO;

  -------------------------------------------------------------
  PROCEDURE validate_terms_name(p_terms_name IN VARCHAR2,
                                v_error_smsg OUT VARCHAR2) IS
    v_term_name VARCHAR2(240);
  BEGIN
    SELECT NAME
      INTO v_term_name
      FROM ap_terms_tl
     WHERE UPPER(trim(NAME)) = UPPER(trim(p_terms_name))
       AND SYSDATE BETWEEN NVL(start_date_active, SYSDATE) AND
           NVL(end_date_active, SYSDATE);
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      v_error_smsg := v_error_smsg ||
                      'Payment Terms does not exist in Oracle Financials-->>';
    WHEN TOO_MANY_ROWS THEN
      v_term_name  := NULL;
      v_error_smsg := v_error_smsg || 'Multiple Payment Terms found-->>';
    WHEN OTHERS THEN
      v_term_name  := NULL;
      v_error_smsg := v_error_smsg || 'Invalid Payment Term -->>';
  END Validate_terms_name;

  -----------------------------------------------------------------------------

  PROCEDURE DERIVE_SHIT_TO_LOCATION_ID(P_SHIP_TO_LOC_CODE IN VARCHAR2,
                                       P_SHIP_TO_LOC_ID   OUT NUMBER,
                                       v_error_smsg       OUT VARCHAR2) IS
    v_ship_to_loc_id NUMBER(10);
  BEGIN
    SELECT location_id
      INTO v_ship_to_loc_id
      FROM hr_locations
     WHERE location_code = P_SHIP_TO_LOC_CODE;

    P_SHIP_TO_LOC_ID := v_ship_to_loc_id;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      v_error_smsg := v_error_smsg ||
                      'ship TO location_code does not exist in Oracle Financials-->>';
    WHEN TOO_MANY_ROWS THEN
      v_ship_to_loc_id := NULL;
      v_error_smsg     := v_error_smsg ||
                          'Multiple ship TO location_code found-->>';
    WHEN OTHERS THEN
      v_ship_to_loc_id := NULL;
      v_error_smsg     := v_error_smsg ||
                          'Invalid ship TO location_code -->>';
  END DERIVE_SHIT_TO_LOCATION_ID;

  ----------------------------------------------------------------------------

  PROCEDURE DERIVE_BILL_TO_LOCATION_ID(P_BILL_TO_LOC_CODE IN VARCHAR2,
                                       P_BILL_TO_LOC_ID   OUT NUMBER,
                                       v_error_smsg       OUT VARCHAR2) IS
    v_bill_to_loc_id NUMBER(10);
  BEGIN
    SELECT location_id
      INTO v_bill_to_loc_id
      FROM hr_locations
     WHERE location_code = P_BILL_TO_LOC_CODE;

    P_BILL_TO_LOC_ID := v_bill_to_loc_id;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      v_error_smsg := v_error_smsg ||
                      'Bill TO Location_Code does not exist in Oracle Financials-->>';
    WHEN TOO_MANY_ROWS THEN
      v_bill_to_loc_id := NULL;
      v_error_smsg     := v_error_smsg ||
                          'Multiple location_code found-->>';
    WHEN OTHERS THEN
      v_bill_to_loc_id := NULL;
      v_error_smsg     := v_error_smsg ||
                          'Invalid bill to location_code -->>';
  END DERIVE_BILL_TO_LOCATION_ID;

  -----------------------------------------------------------------------------

  PROCEDURE DERIVE_EMPLOYEE_ID(P_VENDOR_NUMBER IN VARCHAR2,
                               P_EMPLOYEE_ID   OUT NUMBER,
                               v_error_hmsg    OUT VARCHAR2) IS
    V_EMPOYEE_ID NUMBER(10);
  BEGIN
    SELECT person_id
      INTO V_EMPOYEE_ID
      FROM per_all_people_f
     WHERE EMPLOYEE_NUMBER = P_VENDOR_NUMBER
       AND EFFECTIVE_START_DATE <= TRUNC(SYSDATE)
       AND EFFECTIVE_END_DATE >= TRUNC(SYSDATE);

    P_EMPLOYEE_ID := V_EMPOYEE_ID;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      V_EMPOYEE_ID := NULL;

    WHEN TOO_MANY_ROWS THEN
      V_EMPOYEE_ID := NULL;
      v_error_hmsg := v_error_hmsg ||
                      'Multiple EMPLOYERS AS SAME SUPPLIER-->>';
    WHEN OTHERS THEN
      V_EMPOYEE_ID := NULL;
      v_error_hmsg := v_error_hmsg || 'INVALID SUPLLIER NAME -->>';
  END DERIVE_EMPLOYEE_ID;

  ------------------------------------------------------------------------------
  PROCEDURE DERIVE_TERM_ID(P_TERM_NAME  IN VARCHAR2,
                           P_TERM_ID    OUT NUMBER,
                           v_error_smsg OUT VARCHAR2) IS
    v_term_id NUMBER(10);
  BEGIN
    SELECT term_id INTO v_term_id FROM AP_TERMS WHERE NAME = P_TERM_NAME;

    P_TERM_ID := v_term_id;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      v_error_smsg := v_error_smsg ||
                      'TERM NAME does not exist in Oracle Financials-->>';
    WHEN TOO_MANY_ROWS THEN
      v_term_id    := NULL;
      v_error_smsg := v_error_smsg || 'Multiple TERM NAME found-->>';
    WHEN OTHERS THEN
      v_term_id    := NULL;
      v_error_smsg := v_error_smsg || 'Invalid  TERM NAME -->>';
  END DERIVE_TERM_ID;

  -----------------------------------------------------------------------------
  PROCEDURE validate_vendor_number(p_Vendor_Number IN VARCHAR2,
                                   v_error_hmsg    OUT VARCHAR2) IS
    v_data_count NUMBER;
  BEGIN
    SELECT COUNT(segment1)
      INTO v_data_count
      FROM po_vendors
     WHERE segment1 = p_Vendor_Number;

    IF V_Data_Count > 0 THEN
      v_error_hmsg := v_error_hmsg ||
                      ' Duplicate vendor Number exist in the  Table for the Supplier-->>';
    END IF;

    V_Data_Count := 0;

  EXCEPTION
    WHEN OTHERS THEN
      V_Data_Count := 0;
      v_error_hmsg := v_error_hmsg || ' Invalid vendor Number-->>';

  END validate_vendor_number;
  -----------------------------------------------------------------------------------------
  PROCEDURE validate_vendor_site_code(p_vendor_site_code IN VARCHAR2,
                                      p_vendor_name      IN VARCHAR2,
                                      v_error_smsg       OUT VARCHAR2) IS
    v_data_count NUMBER;
  BEGIN
    SELECT COUNT(*)
      INTO v_data_count
      FROM po_vendors pv, po_vendor_sites_all pvs
     WHERE pv.VENDOR_ID = pvs.VENDOR_ID
       AND pvs.vendor_site_code = p_vendor_site_code
       AND pv.VENDOR_NAME = p_vendor_name;

    IF V_Data_Count > 0 THEN
      v_error_smsg := v_error_smsg ||
                      ' Duplicate vendor site code exist in the  Table for the Supplier-->>';
    END IF;

    V_Data_Count := 0;

  EXCEPTION
    WHEN OTHERS THEN
      V_Data_Count := 0;
      v_error_smsg := v_error_smsg || ' Invalid vendor site code-->>';

  END validate_vendor_site_code;
  ---------------------------------------------------------------------------------------------
  PROCEDURE validate_vendor_type(p_vendor_type_lookup_code IN VARCHAR2,
                                 v_error_hmsg              OUT VARCHAR2) IS
    v_vendor_type_lookup_code VARCHAR2(240);

  BEGIN
    SELECT lookup_code
      INTO v_vendor_type_lookup_code
      FROM po_lookup_codes
     WHERE lookup_type = 'VENDOR TYPE'
       AND lookup_code = p_vendor_type_lookup_code
       AND enabled_flag = 'Y'
       AND NVL(inactive_date, SYSDATE + 1) > SYSDATE;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      v_error_hmsg := v_error_hmsg ||
                      'No lookup_code present for the Vendor type lookup code :-->>';
    WHEN TOO_MANY_ROWS THEN
      v_vendor_type_lookup_code := NULL;
      v_error_hmsg              := v_error_hmsg ||
                                   'lookup_code present for the Vendor type lookup code-->>';
    WHEN OTHERS THEN
      v_vendor_type_lookup_code := NULL;
      v_error_hmsg              := v_error_hmsg ||
                                   'Invalid lookup_code -->>';
  END validate_vendor_type;

  --------------------------------------------------------------------------------------------------
  PROCEDURE validate_operating_unit(P_OPERATING_UNIT IN VARCHAR2,
                                    v_error_smsg     OUT VARCHAR2) IS
    V_OPERATING_UNIT VARCHAR2(240);

  BEGIN
    --    fnd_file.PUT_LINE(fnd_file.LOG, 'operating unit:' || P_OPERATING_UNIT);
    SELECT Short_code
      INTO V_OPERATING_UNIT
      FROM HR_Operating_Units
     WHERE NAME = P_OPERATING_UNIT;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      v_error_smsg := v_error_smsg ||
                      'Selected Org Id does not exist in Oracle Financials';
      fnd_file.put_line(fnd_file.output,
                        '........................................................................');

    WHEN TOO_MANY_ROWS THEN
      v_error_smsg := v_error_smsg ||
                      'Multiple Org Id exist in Oracle Financials';
      fnd_file.put_line(fnd_file.output,
                        '........................................................................');

    WHEN OTHERS THEN
      v_error_smsg := v_error_smsg || 'Invalid Org Id Selected ';
      fnd_file.put_line(fnd_file.output,
                        '........................................................................');
  END validate_operating_unit;

  -----------------------------------------------------------------------------------------------
  PROCEDURE validate_pay_group(P_PAY_GROUP  IN VARCHAR2,
                               X_PAY_GROUP  OUT VARCHAR2,
                               v_error_smsg OUT VARCHAR2) IS
    V_PAY_GROUP VARCHAR2(240);
  BEGIN
    SELECT LOOKUP_CODE
      INTO V_PAY_GROUP
      FROM FND_LOOKUP_VALUES_VL
     WHERE lookup_type = 'PAY GROUP'
       AND LOOKUP_CODE = P_PAY_GROUP;

    X_PAY_GROUP := V_PAY_GROUP;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      v_error_smsg := 'Pay Group ' || P_PAY_GROUP ||
                      ' not defined in Oracle';
    WHEN TOO_MANY_ROWS THEN
      v_error_smsg := 'Too many values found for Pay Group ' || P_PAY_GROUP;
  END;
  ---------------------------------------------------------------------------
  PROCEDURE validate_country_code(P_COUNTRY_CODE IN VARCHAR2,
                                  v_error_smsg   OUT VARCHAR2) IS
    V_COUNTRY_CODE VARCHAR2(240);

  BEGIN
    SELECT territory_code
      INTO V_COUNTRY_CODE
      FROM fnd_territories_vl
     WHERE UPPER(territory_code) = UPPER(P_COUNTRY_CODE)
       AND obsolete_flag = 'N';

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      v_error_smsg := v_error_smsg || 'No Country code Present for Country';

    WHEN TOO_MANY_ROWS THEN
      v_error_smsg := v_error_smsg ||
                      'Multiple No Country code Present for Country';

    WHEN OTHERS THEN
      v_error_smsg := v_error_smsg ||
                      'Invalid No Country code Present for Country ';

  END validate_country_code;

  ------------------------------------------------------------------------------------------------
  PROCEDURE validate_payment_method(P_PAY_METHOD_CODE IN VARCHAR2,
                                    v_error_smsg      OUT VARCHAR2) IS
    V_PAY_METHOD_CODE VARCHAR2(240);

  BEGIN
    SELECT Payment_Method_code
      INTO V_PAY_METHOD_CODE
      FROM iby_payment_methods_vl
     WHERE UPPER(Payment_Method_code) = Trim(UPPER(P_PAY_METHOD_CODE));
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      v_error_smsg := v_error_smsg ||
                      'Payment method does not exist in Oracle Financials';

    WHEN TOO_MANY_ROWS THEN
      v_error_smsg := v_error_smsg ||
                      'Multiple Payment method  exist in Oracle Financials';

    WHEN OTHERS THEN
      v_error_smsg := v_error_smsg || 'Invalid Payment method ';

  END validate_payment_method;

  --------------------------------------------------------------------------------------------
  PROCEDURE XXABRL_INSERT_VENDOR_MIGR_INFO IS
    v_user_id NUMBER := FND_PROFILE.VALUE('USER_ID');
    v_resp_id NUMBER := FND_PROFILE.VALUE('RESP_ID');
    v_appl_id NUMBER := FND_PROFILE.VALUE('RESP_APPL_ID');

    CURSOR APS_C1 IS
      SELECT ROWID,
             VENDOR_NAME,
             Vendor_Number,
             -- Vendor_Name_ALT,
             EMPLOYEE_ID,
             VENDOR_TYPE_LOOKUP_CODE,
             /* PRINCIPAL_VENDOR,
                                       LEGAL_STATUS,
                                       CEO_NAME,
                                       CEO_TITLE,*/
             SHIP_TO_LOCATION_CODE,
             BILL_TO_LOCATION_CODE,
             TERMS_NAME,
             START_DATE_ACTIVE,
             END_DATE_ACTIVE,
             SMALL_BUSINESS_FLAG,
             HOLD_FLAG,
             PURCHASING_HOLD_REASON,
             HOLD_ALL_PAYMENTS_FLAG,
             HOLD_FUTURE_PAYMENTS_FLAG,
             -- HOLD_UNMATCHED_INVOICES_FLAG,
             MIN_ORDER_AMOUNT,
             TAX_VERIFICATION_DATE,
             VAT_REGISTRATION_NUM,
             PAYMENT_PRIORITY,
             EXCLUDE_FREIGHT_FROM_DISCOUNT,
             CUSTOMER_NUM,
             PAYMENT_METHOD_LOOKUP_CODE -- added by srini
      -- NAV_VENDOR_NUM
      /*GLOBAL_ATTRIBUTE_CATEGORY,
                         Primary_Vendor_Category,
                         MICR_CODE,
                         NEFT_CODE,
                         RTGS_CODE,
                         APMC_Registration,
                         Drug_License,
                         Liquor_License,
                         MSMED,
                         TAX_REGION,
                         TAX_PAYER_TYPE*/
        FROM XXABRL_SUPPLIER_EMP_INT
       WHERE NVL(PROCESS_FLAG, 'N') = 'V';

    CURSOR APS_C2(CP_VENDOR_NAME VARCHAR2) IS
      SELECT ROWID,
             OPERATING_UNIT,
             VENDOR_NAME,
             VENDOR_SITE_CODE,
             ADDRESS_LINE1,
             ADDRESS_LINE2,
             ADDRESS_LINE3,
             CITY,
             COUNTY,
             STATE,
             COUNTRY,
             ZIP,
             AREA_CODE,
             PHONE,
             FAX,
             FAX_AREA_CODE,
             EMAIL_ADDRESS,
             PURCHASING_SITE_FLAG,
             PAY_SITE_FLAG,
             -- RFQ_ONLY_SITE_FLAG,
             INACTIVE_DATE,
             INVOICE_AMOUNT_LIMIT,
             BILL_TO_LOCATION_CODE,
             SHIP_TO_LOCATION_CODE,
             TERMS_NAME,
             BILL_TO_LOCATION_ID,
             SHIP_TO_LOCATION_ID,
             TERM_ID,
             PAY_GROUP_LOOKUP_CODE,
             PAYMENT_METHOD_LOOKUP_CODE,
             --NAV_VENDOR_NUM, --***
             DERIVED_PAY_GROUP
        FROM XXABRL_SUPPLIER_SITES_EMP_INT
       WHERE VENDOR_NAME = CP_VENDOR_NAME
         AND NVL(PROCESS_FLAG, 'N') = 'V';

    ln_vendor_interface_id      AP_SUPPLIERS_INT.vendor_interface_id%TYPE;
    ln_vendor_site_interface_id NUMBER;
    ln_vendor_site_cont_int_id  NUMBER;
  BEGIN
    FND_FILE.PUT_LINE(FND_FILE.LOG, 'Starting Inserting Vendors');
    FOR lr_vend_rec IN APS_C1 LOOP
      BEGIN
        SELECT AP_SUPPLIERS_INT_S.NEXTVAL
          INTO ln_vendor_interface_id
          FROM DUAL;
      EXCEPTION
        WHEN OTHERS THEN
          FND_FILE.PUT_LINE(FND_FILE.LOG,
                            'Error : Unable to derive vendor_interface_id');
      END;

      FND_FILE.PUT_LINE(FND_FILE.LOG,
                        'Inserting Vendor L1:' || lr_vend_rec.VENDOR_NAME);

      INSERT INTO AP_SUPPLIERS_INT
        (VENDOR_INTERFACE_ID,
         VENDOR_NAME,
         SEGMENT1,
         --Vendor_Name_ALT,
         EMPLOYEE_ID,
         VENDOR_TYPE_LOOKUP_CODE,
         /*  ATTRIBUTE1,
                           ATTRIBUTE2,
                           ATTRIBUTE3,
                           ATTRIBUTE4,*/
         SHIP_TO_LOCATION_CODE,
         BILL_TO_LOCATION_CODE,
         TERMS_NAME,
         START_DATE_ACTIVE,
         END_DATE_ACTIVE,
         SMALL_BUSINESS_FLAG,
         HOLD_FLAG,
         AUTO_CALCULATE_INTEREST_FLAG,
         PURCHASING_HOLD_REASON,
         HOLD_ALL_PAYMENTS_FLAG,
         HOLD_FUTURE_PAYMENTS_FLAG,
         --  HOLD_UNMATCHED_INVOICES_FLAG,
         MIN_ORDER_AMOUNT,
         TAX_VERIFICATION_DATE,
         VAT_REGISTRATION_NUM,
         PAYMENT_PRIORITY,
         EXCLUDE_FREIGHT_FROM_DISCOUNT,
         CUSTOMER_NUM,
         PAYMENT_METHOD_LOOKUP_CODE, --- added by srini
         --GLOBAL_ATTRIBUTE_CATEGORY,
         /* Global_Attribute1,
                           Global_Attribute2,
                           Global_Attribute3,
                           Global_Attribute4,
                           Global_Attribute5,
                           Global_Attribute6,
                           Global_Attribute7,
                           Global_Attribute8,*/
         -- Global_Attribute9,
         CREATION_DATE,
         CREATED_BY)
      VALUES
        (ln_vendor_interface_id,
         lr_vend_rec.VENDOR_NAME,
         lr_vend_rec.Vendor_Number,
         --lr_vend_rec.Vendor_Name_ALT,
         lr_vend_rec.EMPLOYEE_ID,
         lr_vend_rec.VENDOR_TYPE_LOOKUP_CODE,
         /* lr_vend_rec.PRINCIPAL_VENDOR,
                           lr_vend_rec.LEGAL_STATUS,
                           lr_vend_rec.CEO_NAME,
                           lr_vend_rec.CEO_TITLE,*/
         lr_vend_rec.SHIP_TO_LOCATION_CODE,
         lr_vend_rec.BILL_TO_LOCATION_CODE,
         lr_vend_rec.TERMS_NAME,
         lr_vend_rec.START_DATE_ACTIVE,
         lr_vend_rec.END_DATE_ACTIVE,
         lr_vend_rec.SMALL_BUSINESS_FLAG,
         lr_vend_rec.HOLD_FLAG,
         'N',
         lr_vend_rec.PURCHASING_HOLD_REASON,
         lr_vend_rec.HOLD_ALL_PAYMENTS_FLAG,
         lr_vend_rec.HOLD_FUTURE_PAYMENTS_FLAG,
         --  lr_vend_rec.HOLD_UNMATCHED_INVOICES_FLAG,
         lr_vend_rec.MIN_ORDER_AMOUNT,
         lr_vend_rec.TAX_VERIFICATION_DATE,
         lr_vend_rec.VAT_REGISTRATION_NUM,
         lr_vend_rec.PAYMENT_PRIORITY,
         lr_vend_rec.EXCLUDE_FREIGHT_FROM_DISCOUNT,
         lr_vend_rec.CUSTOMER_NUM,
         lr_vend_rec.PAYMENT_METHOD_LOOKUP_CODE, --- added by srini
         -- 'ABRL Supplier Additional Info',
         /*  lr_vend_rec.Primary_Vendor_Category,
                           lr_vend_rec.MICR_CODE,
                           lr_vend_rec.NEFT_CODE,
                           lr_vend_rec.RTGS_CODE,
                           lr_vend_rec.APMC_Registration,
                           lr_vend_rec.Drug_License,
                           lr_vend_rec.Liquor_License,
                           lr_vend_rec.MSMED,
                           lr_vend_rec.TAX_REGION,
                           lr_vend_rec.TAX_PAYER_TYPE,*/
         --lr_vend_rec.NAV_VENDOR_NUM,
         SYSDATE,
         v_user_id);
      FOR lr_Site_rec IN APS_C2(lr_vend_rec.vendor_name) LOOP
        --
        -- We just need to create US Sites in US OU only else we need to create every other site
        -- in all the existing OU except US
        --
        ln_vendor_site_interface_id := NULL;
        BEGIN
          SELECT AP_SUPPLIER_SITES_INT_S.NEXTVAL
            INTO ln_vendor_site_interface_id
            FROM DUAL;
        EXCEPTION
          WHEN OTHERS THEN
            FND_FILE.PUT_LINE(FND_FILE.LOG,
                              'Unable to derive vendor_site_interface_id');
        END;
        FND_FILE.PUT_LINE(FND_FILE.LOG,
                          'Inserting Vendor Site' ||
                          lr_Site_rec.VENDOR_SITE_CODE);
        BEGIN
          INSERT INTO AP_SUPPLIER_SITES_INT
            (VENDOR_INTERFACE_ID,
             VENDOR_SITE_INTERFACE_ID,
             OPERATING_UNIT_NAME,
             --,VENDOR_NAME
             VENDOR_SITE_CODE,
             ADDRESS_LINE1,
             ADDRESS_LINE2,
             ADDRESS_LINE3,
             CITY,
             COUNTY,
             STATE,
             COUNTRY,
             ZIP,
             AREA_CODE,
             PHONE,
             FAX,
             FAX_AREA_CODE,
             --  EMAIL_ADDRESS,
             PURCHASING_SITE_FLAG,
             PAY_SITE_FLAG,
             EXCLUSIVE_PAYMENT_FLAG,
             -- RFQ_ONLY_SITE_FLAG,
             INACTIVE_DATE,
             INVOICE_AMOUNT_LIMIT,
             BILL_TO_LOCATION_CODE,
             SHIP_TO_LOCATION_CODE,
             TERMS_NAME,
             BILL_TO_LOCATION_ID,
             SHIP_TO_LOCATION_ID,
             TERMS_ID,
             PAY_GROUP_LOOKUP_CODE,
             PAYMENT_METHOD_LOOKUP_CODE,
             ATTRIBUTE_CATEGORY,
             --Attribute1, --***
             CREATION_DATE,
             CREATED_BY)
          VALUES
            (ln_vendor_interface_id,
             ln_vendor_site_interface_id,
             lr_Site_rec.OPERATING_UNIT,
             --, lr_Site_rec.VENDOR_NAME
             SUBSTR(lr_Site_rec.VENDOR_SITE_CODE, 1, 15),
             lr_Site_rec.ADDRESS_LINE1,
             lr_Site_rec.ADDRESS_LINE2,
             lr_Site_rec.ADDRESS_LINE3,
             SUBSTR(lr_Site_rec.CITY, 1, 15),
             lr_Site_rec.COUNTY,
             lr_Site_rec.STATE,
             lr_Site_rec.COUNTRY,
             lr_Site_rec.ZIP,
             DECODE(lr_Site_rec.AREA_CODE, 0, NULL, lr_Site_rec.AREA_CODE),
             lr_Site_rec.PHONE,
             --  to_char(decode(lr_Site_rec.PHONE, 0, null, lr_Site_rec.PHONE)),
             DECODE(lr_Site_rec.FAX, 0, NULL, lr_Site_rec.FAX),
             DECODE(lr_Site_rec.FAX_AREA_CODE,
                    0,
                    NULL,
                    lr_Site_rec.FAX_AREA_CODE),
             /*   decode(lr_Site_rec.EMAIL_ADDRESS,
                                            0,
                                            null,
                                         lr_Site_rec.EMAIL_ADDRESS),*/
             UPPER(lr_Site_rec.PURCHASING_SITE_FLAG),
             UPPER(lr_Site_rec.PAY_SITE_FLAG),
             'Y',
             --upper(lr_Site_rec.RFQ_ONLY_SITE_FLAG),
             lr_Site_rec.INACTIVE_DATE,
             lr_Site_rec.INVOICE_AMOUNT_LIMIT,
             lr_Site_rec.BILL_TO_LOCATION_CODE,
             lr_Site_rec.SHIP_TO_LOCATION_CODE,
             lr_Site_rec.TERMS_NAME,
             lr_Site_rec.BILL_TO_LOCATION_ID,
             lr_Site_rec.SHIP_TO_LOCATION_ID,
             lr_Site_rec.TERM_ID,
             UPPER(lr_Site_rec.DERIVED_PAY_GROUP),
             lr_Site_rec.PAYMENT_METHOD_LOOKUP_CODE,
             'Supplier Site Additional Info',
             --***lr_Site_rec.NAV_VENDOR_NUM,
             SYSDATE,
             v_user_id);

        EXCEPTION
          WHEN OTHERS THEN
            FND_FILE.PUT_LINE(FND_FILE.LOG,
                              'error while inserting site for vendor ' ||
                              ln_vendor_interface_id || SQLERRM);
        END;

        /*FOR lr_Site_cont_rec IN APS_C3(lr_vend_rec.vendor_name,
                                       lr_Site_rec.vendor_site_code) LOOP
          --
          -- We just need to create US Sites in US OU only else we need to create every other site
          -- in all the existing OU except US
          --
          ln_vendor_site_cont_int_id := NULL;
          BEGIN
            SELECT PO_VENDOR_CONTACTS_S.nextval
              INTO ln_vendor_site_cont_int_id
              FROM DUAL;
          EXCEPTION
            WHEN OTHERS THEN
              FND_FILE.PUT_LINE(FND_FILE.LOG,
                                'Unable to derive vendor_site_Contact_interface_id');
          END;

          FND_FILE.PUT_LINE(FND_FILE.LOG,
                            'Inserting Site Contact' ||
                            lr_Site_cont_rec.FIRST_NAME);

          INSERT INTO AP_SUP_SITE_CONTACT_INT
            (VENDOR_INTERFACE_ID,
             VENDOR_CONTACT_INTERFACE_ID,
             OPERATING_UNIT_NAME,
             -- VENDOR_NAME  ,
             VENDOR_SITE_CODE,
             -- TITLE,
             FIRST_NAME,
             --   MIDDLE_NAME,
             LAST_NAME,
             EMAIL_ADDRESS,
             AREA_CODE,
             PHONE,
             FAX_AREA_CODE,
             FAX,
             CREATION_DATE,
             CREATED_BY)
          VALUES
            (ln_vendor_interface_id,
             ln_vendor_site_cont_int_id,
             lr_Site_cont_rec.OPERATING_UNIT,
             --    , lr_Site_cont_rec.VENDOR_NAME
             SUBSTR(lr_Site_cont_rec.VENDOR_SITE_CODE, 1, 15),
             --   lr_Site_cont_rec.TITLE,
             lr_Site_cont_rec.FIRST_NAME,
             -- lr_Site_cont_rec.MIDDLE_NAME,
             lr_Site_cont_rec.LAST_NAME,
             lr_Site_cont_rec.EMAIL_ADDRESS,
             lr_Site_cont_rec.PHONE_AREA_CODE,
             lr_Site_cont_rec.PHONE,
             lr_Site_cont_rec.FAX_AREA_CODE,
             lr_Site_cont_rec.FAX,
             SYSDATE,
             v_user_id);

          Commit;
        END LOOP;*/

        UPDATE XXABRL_SUPPLIER_SITES_EMP_INT
           SET process_Flag = 'Y'
         WHERE ROWID = lr_Site_rec.ROWID;
        COMMIT;

      END LOOP;
      UPDATE XXABRL_SUPPLIER_EMP_INT
         SET process_Flag = 'Y'
       WHERE ROWID = lr_vend_rec.ROWID;
      COMMIT;
    END LOOP;
    COMMIT;
  END XXABRL_INSERT_VENDOR_MIGR_INFO;
END XXABRL_VENDOR_EMP_PKG;
/


CREATE OR REPLACE PACKAGE BODY APPS.Xxabrl_gl_offset_pkg
AS
PROCEDURE XXABRL_GL_OFFSET_PROC(ERRBUFF OUT VARCHAR2,
                                RETCODE OUT NUMBER,
                                P_FROM_SBU NUMBER,
                                P_TO_SBU  NUMBER,
                                P_FROM_LOCATION NUMBER,
                                P_TO_LOCATION NUMBER,
                                P_FROM_ACCOUNT NUMBER,
                                P_TO_ACCOUNT NUMBER,
                                P_FROM_GL_DATE date,
                                P_TO_GL_DATE date,
                                P_BATCH_NAME VARCHAR2
                                )
IS

       query_string             LONG;
       order_by_string          LONG;
       batch_string             LONG;
       inv_count                NUMBER;
       sbu_string               LONG;
       location_string          LONG;
       gl_account_string        LONG;
       gl_date_string           LONG;
       closing_balance         NUMBER;
      cr_total                 NUMBER;
      dr_total                 NUMBER;

TYPE ref_cur IS REF CURSOR;

      c                        ref_cur;

      TYPE c_rec IS RECORD (
                            v_batch_name           gl_je_batches.NAME%TYPE,
                            v_SOURCE               gl_je_headers.je_source%TYPE,
                            v_CATEGORY             gl_je_headers.je_category%TYPE,
                            v_gl_date              gl_je_headers.default_effective_date%TYPE,
                            v_line_number          gl_je_lines.je_line_num%TYPE,
                            v_account_code         gl_code_combinations_kfv.concatenated_segments%TYPE,
                            v_SBU_desc             VARCHAR2(250),
                            v_Location_desc        VARCHAR2(250),
                            v_account_desc         VARCHAR2(250),
                            v_dr_amount            gl_je_lines.accounted_dr%TYPE,
                            v_cr_amount            gl_je_lines.accounted_cr%TYPE,
                            v_journal_description  gl_je_lines.description%TYPE,
                            v_Customer_Number      VARCHAR2(250),
                            v_Customer_Name        VARCHAR2(250),
                            v_document_number      gl_je_headers.doc_sequence_value%TYPE,
                            v_batch_status        VARCHAR2(100),
                            v_Created_By          fnd_user.user_name%TYPE
         );

      v_rec                    c_rec;


    V_LEDGER_ID NUMBER;
    V_RESP_ID NUMBER := Fnd_Profile.VALUE('RESP_ID');
    V_LEDGER_COND VARCHAR2(1000);
    BEGIN

  query_string :=
  'SELECT AA.batch_name,
AA.SOURCE,
AA.CATEGORY,
AA.gl_date,
AA.line_number,
AA.account_code,
AA.SBU_desc,
AA.Location_desc,
AA.account_desc,
AA.dr_amount,
AA.cr_amount,
AA.journal_description,
AA.Customer_Number,
AA.Customer_Name,
AA.document_number,
AA.batch_status,
AA.Created_By
FROM
(SELECT GLB.NAME batch_name,
       glh.je_source SOURCE, glh.je_category CATEGORY,
       glh.default_effective_date gl_date,
       gll.je_line_num line_number,
       gl.concatenated_segments account_code,
       (SELECT FFVL.description FROM fnd_flex_values_vl ffvl
        WHERE ffvl.FLEX_VALUE_SET_ID=1013466
        AND ffvl.FLEX_VALUE=gl.SEGMENT3) SBU_desc,
         (SELECT FFVL.description FROM fnd_flex_values_vl ffvl
        WHERE ffvl.FLEX_VALUE_SET_ID=1013467
        AND ffvl.FLEX_VALUE=gl.SEGMENT4) Location_desc,
       (SELECT FFVL.description FROM fnd_flex_values_vl ffvl
        WHERE ffvl.FLEX_VALUE_SET_ID=1013469
        AND ffvl.FLEX_VALUE=gl.SEGMENT6) account_desc,
       decode(xla.accounted_dr,null,decode(xla.accounted_cr,null, gll.accounted_dr),xla.accounted_dr) dr_amount,
       decode(xla.accounted_cr,null,decode(xla.accounted_dr,null, gll.accounted_cr),xla.accounted_cr) cr_amount,
       gll.description journal_description,
       CASE WHEN UPPER(glh.je_source)=''PAYABLES''
            THEN (SELECT po.segment1
                 FROM po_vendors po
                 WHERE po.VENDOR_ID=xla.PARTY_ID
                 AND ROWNUM<2)
            WHEN UPPER(glh.je_source)=''RECEIVABLES''
            THEN (SELECT  ar.account_number
                 FROM hz_parties hzp, hz_cust_accounts ar
                 WHERE ar.party_id = hzp.party_id
                 AND ar.cust_account_id=xla.party_id
                 AND ROWNUM<2 )
        END AS Customer_Number,
        CASE WHEN UPPER(glh.je_source)=''PAYABLES''
            THEN (SELECT po.vendor_name
                 FROM po_vendors po
                 WHERE po.VENDOR_ID=xla.PARTY_ID
                 )
            WHEN UPPER(glh.je_source)=''RECEIVABLES''
            THEN (SELECT  hzp.party_name
                 FROM hz_parties hzp, hz_cust_accounts ar
                 WHERE ar.party_id = hzp.party_id
                 AND ar.cust_account_id=xla.party_id
                 )
         END AS Customer_Name,
       NVL(glir.SUBLEDGER_DOC_SEQUENCE_VALUE,glh.doc_sequence_value) document_number,
       DECODE (GLB.status,
               ''P'', ''Posted'',
               ''U'', ''Unposted'',
               GLB.status
              ) batch_status,
        CASE WHEN UPPER(glh.je_category)=''RECEIPTS''
            THEN (SELECT fu.user_name
                 FROM ar_cash_receipts_all arc,fnd_user fu
                 WHERE arc.DOC_SEQUENCE_VALUE=NVL(glir.SUBLEDGER_DOC_SEQUENCE_VALUE,arc.DOC_SEQUENCE_VALUE)
                 AND arc.PAY_FROM_CUSTOMER=NVL(xla.party_id,arc.PAY_FROM_CUSTOMER)
                 AND arc.created_by=fu.user_id
                 AND arc.TYPE=''CASH''
                 AND ROWNUM<2)
           WHEN UPPER(glh.je_category)=''MISC RECEIPTS''
            THEN (SELECT fu.user_name
                 FROM ar_cash_receipts_all arc,fnd_user fu
                 WHERE arc.DOC_SEQUENCE_VALUE=glir.SUBLEDGER_DOC_SEQUENCE_VALUE
                 AND arc.created_by=fu.user_id
                 AND arc.TYPE=''MISC''
                 AND ROWNUM<2)
           WHEN UPPER(glh.je_category)=''PAYMENTS''
            THEN (SELECT fu.user_name
                 FROM ap_checks_all arc,fnd_user fu
                 WHERE arc.DOC_SEQUENCE_VALUE=glir.SUBLEDGER_DOC_SEQUENCE_VALUE
                 AND arc.vendor_id(+)=xla.party_id
                 AND arc.created_by=fu.user_id
                 --and arc.PAYMENT_TYPE_FLAG=''Q''
                 AND ROWNUM<2)
            WHEN UPPER(glh.je_category)=''RECONCILED PAYMENTS''
            THEN (SELECT fu.user_name
                 FROM ap_checks_all arc,fnd_user fu
                 WHERE arc.DOC_SEQUENCE_VALUE=glir.SUBLEDGER_DOC_SEQUENCE_VALUE
                 AND arc.vendor_id(+)=xla.party_id
                 AND arc.created_by=fu.user_id
                 --and arc.PAYMENT_TYPE_FLAG=''M''
                 AND ROWNUM<2)
            WHEN UPPER(glh.je_category)=''PURCHASE INVOICES''
            THEN (SELECT fu.user_name
                 FROM ap_invoices_all arc,fnd_user fu
                 WHERE arc.DOC_SEQUENCE_VALUE=NVL(glir.SUBLEDGER_DOC_SEQUENCE_VALUE,arc.DOC_SEQUENCE_VALUE)
                 AND arc.vendor_id(+)=xla.party_id
                 AND arc.created_by=fu.user_id
                 AND ROWNUM<2)
           WHEN UPPER(glh.je_category)=''SALES INVOICES''
           THEN (SELECT fu.user_name
                 FROM ra_customer_trx_all arc,fnd_user fu
                 WHERE arc.DOC_SEQUENCE_VALUE=glir.SUBLEDGER_DOC_SEQUENCE_VALUE
                 AND arc.bill_to_customer_id(+)=xla.party_id
                 AND arc.created_by=fu.user_id
                 AND ROWNUM<2)
           WHEN UPPER(glh.je_category)=''CREDIT MEMOS''
           THEN (SELECT fu.user_name
                 FROM fnd_user fu,ra_cust_trx_line_gl_dist_all arc
                 WHERE arc.CODE_COMBINATION_ID=xla.CODE_COMBINATION_ID
                 AND ABS(arc.amount)=NVL(xla.ACCOUNTED_CR,xla.ACCOUNTED_DR)
                 AND arc.created_by=fu.user_id
                 AND ROWNUM<2)
           ELSE (SELECT fu.user_name
                 FROM fnd_user fu
                 WHERE nvl(xla.created_by,gll.created_by)=fu.user_id
                 AND ROWNUM<2)
        END AS Created_By,
       gl.segment3 SBU,
       gl.segment4 LOCATION,
       gl.segment6 GL_account,
            GLH.LEDGER_ID LEDGER_ID
  FROM gl_je_headers glh,
       gl_je_lines gll,
       gl_code_combinations_kfv gl,
       gl_je_batches GLB,
       xla_ae_lines xla,
       gl_import_references glir
 WHERE glh.je_header_id = gll.je_header_id
   AND gll.je_header_id = glir.je_header_id (+)
   AND gll.je_line_num = glir.je_line_num (+)
   AND glir.gl_sl_link_id = xla.gl_sl_link_id (+)
   AND glir.gl_sl_link_table = xla.gl_sl_link_table (+)
   AND gl.code_combination_id = gll.code_combination_id
   AND glh.je_batch_id = GLB.je_batch_id
   AND UPPER(glh.je_source) IN (''PAYABLES'',''RECEIVABLES'')
   AND NVL(xla.accounted_cr,999999)!=0
   AND NVL(xla.accounted_dr,999999)!=0
UNION ALL
SELECT GLB.NAME batch_name,
       glh.je_source SOURCE, glh.je_category CATEGORY,
       glh.default_effective_date gl_date,
       gll.je_line_num line_number,
       gl.concatenated_segments account_code,
       (SELECT FFVL.description FROM fnd_flex_values_vl ffvl
        WHERE ffvl.FLEX_VALUE_SET_ID=1013466
        AND ffvl.FLEX_VALUE=gl.SEGMENT3) SBU_desc,
         (SELECT FFVL.description FROM fnd_flex_values_vl ffvl
        WHERE ffvl.FLEX_VALUE_SET_ID=1013467
        AND ffvl.FLEX_VALUE=gl.SEGMENT4) Location_desc,
       (SELECT FFVL.description FROM fnd_flex_values_vl ffvl
        WHERE ffvl.FLEX_VALUE_SET_ID=1013469
        AND ffvl.FLEX_VALUE=gl.SEGMENT6) account_desc,
      GLL.ACCOUNTED_DR dr_amount,
      GLL.ACCOUNTED_CR cr_amount,
      gll.description journal_description,
      NULL customer_name,
      NULL customer_Number,
       glh.doc_sequence_value document_number,
       DECODE (GLB.status,
               ''P'', ''Posted'',
               ''U'', ''Unposted'',
               GLB.status
              ) batch_status,
     fu.user_name Created_By,
     gl.segment3 SBU,
     gl.segment4 LOCATION,
     gl.segment6 GL_account,
     GLH.LEDGER_ID LEDGER_ID
FROM
       gl_je_headerS glh,
       gl_je_lines gll,
       gl_code_combinations_kfv gl,
       fnd_user fu,
       gl_je_batches GLB
WHERE glh.je_header_id = gll.je_header_id
  AND gl.code_combination_id = gll.code_combination_id
  AND glh.je_batch_id = GLB.je_batch_id
  AND gll.created_by=fu.user_id
--  AND NVL (glh.accrual_rev_status, ''NR'') <> ''R'' 
  AND UPPER(glh.je_source) NOT IN (''PAYABLES'',''RECEIVABLES'')
) AA
WHERE 1=1  ';
 --nvl(xla.accounted_dr dr_amount,
       --nvl(xla.accounted_cr cr_amount,

BEGIN
 SELECT DEFAULT_LEDGER_ID INTO V_LEDGER_ID
     FROM FND_PROFILE_OPTION_VALUES fpop,
      FND_PROFILE_OPTIONS fpo     ,
      gl_access_sets GAS
      WHERE --level_value=51664--cp_resp_id  --52299 -- resp id
      fpop.PROFILE_OPTION_ID =fpo.PROFILE_OPTION_ID
      AND GAS.ACCESS_SET_ID=FPOP.profile_option_value
        AND level_value=V_RESP_ID --51664
      AND PROFILE_OPTION_NAME='GL_ACCESS_SET_ID';

      EXCEPTION
      WHEN OTHERS THEN
      Fnd_File.put_line (Fnd_File.LOG,'NO LEDGER FOUND. ');
      V_LEDGER_ID:=0;

END;



/*

      SELECT * FROM gl_access_sets

            WHERE ACCESS_SET_ID=1080


      SELECT DISTINCT LEDGER_ID FROM GL_JE_HEADERS

      SQL="SELECT name, access_set_id
INTO :visible_option_value, :profile_option_value
FROM gl_access_sets
WHERE enabled_flag = 'Y'
ORDER BY NAME"
COLUMN="NAME(30)"

     AND PROFILE_OPTION_NAME='GL: Data Access Set';

     SELECT * FROM FND_PROFILE_OPTIONS
     WHERE PROFILE_OPTION_NAME='GL_ACCESS_SET_ID';

     FND_PROFILE_OPTIONS fpo
     WHERE fpop.PROFILE_OPTION_ID =fpo.PROFILE_OPTION_ID
     AND level_value=51664--cp_resp_id  --52299 -- resp id
     AND PROFILE_OPTION_NAME='GL: Data Access Set';


     SELECT *
     FROM FND_PROFILE_OPTION_VALUES fpop,
     FND_PROFILE_OPTIONS fpo
     WHERE fpop.PROFILE_OPTION_ID =fpo.PROFILE_OPTION_ID
     AND level_value=51664 --52299 -- resp id
     AND pso.security_profile_id=fpop.PROFILE_OPTION_VALUE
     AND PROFILE_OPTION_NAME='GL_ACCESS_SET_ID';
     */

/*

SELECT * FROM all_tab_columns
WHERE column_name LIKE '%LEDGER%ID%'
AND table_name LIKE 'GL%'

    SELECT * FROM GL_SETS_OF_BOOKS

    GL_JE_HEADERS_V

    SELECT * FROM GL_JE_BATCHES

    SELECT DISTINCT  LEDGER_ID FROM GL_JE_HEADERS_V */

  sbu_string:= ' AND AA.SBU BETWEEN '''||P_FROM_SBU||''' AND '''||P_TO_SBU||'''';
  location_string:=' AND TO_NUMBER(AA.LOCATION) BETWEEN '''||P_FROM_LOCATION||''' AND '''||P_TO_LOCATION||'''';
  gl_account_string:=' AND AA.GL_ACCOUNT BETWEEN '''||P_FROM_ACCOUNT||''' AND '''||P_TO_ACCOUNT||'''';
  gl_date_string:=' AND to_date(to_char(AA.Gl_Date,''DD-MON-YY'')) BETWEEN '''||P_FROM_GL_DATE||''' AND '''||P_TO_GL_DATE||'''';
   batch_string:= ' AND AA.batch_name = '''|| p_batch_name|| '''';
   order_by_string:=' ORDER BY AA.batch_name,AA.SOURCE,AA.CATEGORY,AA.line_number';
   V_LEDGER_COND := ' AND AA.LEDGER_ID = '||V_LEDGER_ID;

  -- ADDING LEDGER ID CONDITION
  query_string:=query_string|| V_LEDGER_COND;

    -- Checking For the from SBU and To SBU Parameter

      IF (P_FROM_SBU IS NULL AND P_TO_SBU IS NULL)
      THEN
         query_string := query_string;
     ELSE
         query_string := query_string ||sbu_string;
      END IF;

      -- Checking For the from LOCATION and To LOCATION Parameter

      IF (P_FROM_LOCATION IS NULL AND P_TO_LOCATION IS NULL)
      THEN
         query_string := query_string;
     ELSE
         query_string := query_string ||location_string;
      END IF;

       -- Checking For the from GL ACCOUNT and To GL ACCOUNT Parameter

      IF (P_FROM_ACCOUNT IS NULL AND P_TO_ACCOUNT IS NULL)
      THEN
         query_string := query_string;
     ELSE
         query_string := query_string ||gl_account_string;
      END IF;

      -- Checking  For Batch name Parameter

         IF(p_batch_name IS NULL) THEN
         query_string:=query_string;
         ELSE
         query_string :=query_string||batch_string;
         END IF;

   -- Checking GL Date Parameter

     IF (P_FROM_GL_DATE IS NOT NULL AND P_TO_GL_DATE IS NOT NULL)
      THEN
         query_string := query_string || gl_date_string;
      ELSIF (P_FROM_GL_DATE IS NULL AND P_TO_GL_DATE IS NULL)
      THEN
         query_string := query_string;
      END IF;



      Fnd_File.put_line (Fnd_File.output,
                         'ABRL General Ledger Offset Report'
                        );
      Fnd_File.put_line (Fnd_File.output, ' ');
      Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'As on Date'
                         || CHR (9)
                         || SYSDATE
                         || CHR (9)
                        );
      Fnd_File.put_line (Fnd_File.output, ' ');
      Fnd_File.put_line (Fnd_File.output, 'REPORT PARAMETRS ');
      Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'From SBU'
                         || CHR (9)
                         || P_FROM_SBU
                         || CHR (9)
                        );

        Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'To SBU'
                         || CHR (9)
                         || P_TO_SBU
                         || CHR (9)
                        );
             Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'From Location'
                         || CHR (9)
                         || P_FROM_LOCATION
                         || CHR (9)
                        );

        Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'To Location'
                         || CHR (9)
                         || P_TO_LOCATION
                         || CHR (9)
                        );

         Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'From GL Account'
                         || CHR (9)
                         || P_FROM_ACCOUNT
                         || CHR (9)
                        );

        Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'To GL Account'
                         || CHR (9)
                         || P_TO_ACCOUNT
                         || CHR (9)
                        );

       Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'From GL Date'
                         || CHR (9)
                         || P_FROM_GL_DATE
                         || CHR (9)
                        );

        Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'To GL Date'
                         || CHR (9)
                         || P_TO_GL_DATE
                         || CHR (9)
                        );

        Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Batch Name'
                         || CHR (9)
                         || p_batch_name
                         || CHR (9)
                        );

   Fnd_File.put_line (Fnd_File.output, ' ');
      Fnd_File.put_line (Fnd_File.output, ' ');
      Fnd_File.put_line (Fnd_File.output,
                           'Batch Name'
                         || CHR (9)
                         || 'Source'
                         || CHR (9)
                         || 'Category'
                         || CHR (9)
                                     || 'GL Date'
                         || CHR (9)
                         || 'Line Number'
                         || CHR (9)
                         || 'Account Code'
                         ||  CHR (9)
                         || 'State SBU'
                         ||  CHR (9)
                         || 'Location'
                         ||  CHR (9)
                         || 'GL Account'
                         || CHR (9)
                         || 'Dr Amount'
                         || CHR (9)
                         || 'Cr Amount'
                         || CHR (9)
                         || 'Journal Description'
                         || CHR (9)
                         || 'Vendor \ Customer Name'
                         || CHR (9)
                         || 'Vendor \ Customer Number'
                         ||  CHR (9)
                         || 'Document Number'
                         ||  CHR (9)
                         || 'Batch Status'
                         ||  CHR (9)
                         || 'Created by (Entry in Subledger)'
                        );

                   query_string:=query_string;

                   Fnd_File.put_line (Fnd_File.LOG,
                         'string: '||query_string
                        );

  inv_count:=0;
  cr_total:=0;
  dr_total:=0;
  --closing_balance:=0;

         OPEN c
       FOR query_string;
      LOOP
         FETCH c
          INTO v_rec;
         EXIT WHEN c%NOTFOUND;


         Fnd_File.put_line (Fnd_File.output,
                             v_rec.v_batch_name
                            || CHR (9)
                            || v_rec.v_SOURCE
                            || CHR (9)
                            || v_rec.v_CATEGORY
                            || CHR (9)
                            || v_rec.v_gl_date
                            || CHR (9)
                            || v_rec.v_line_number
                            || CHR (9)
                            || v_rec.v_account_code
                            || CHR (9)
                            || v_rec.v_SBU_desc
                            || CHR (9)
                            || v_rec.v_Location_desc
                            || CHR (9)
                            || v_rec.v_account_desc
                            || CHR (9)
                            || v_rec.v_dr_amount
                            || CHR (9)
                            || v_rec.v_cr_amount
                            || CHR (9)
                            || v_rec.v_journal_description
                            || CHR (9)
                            || v_rec.v_Customer_Number
                            || CHR (9)
                            ||v_rec.v_Customer_Name
                            || CHR (9)
                            ||v_rec.v_document_number
                            || CHR (9)
                            || v_rec.v_batch_status
                            || CHR (9)
                            || v_rec.v_Created_By
                            );

                   cr_total:=cr_total+NVL(v_rec.v_cr_amount,0);
                   dr_total:=dr_total+NVL(v_rec.v_dr_amount,0);

                   inv_count :=inv_count+1;

END LOOP;

 Fnd_File.put_line (Fnd_File.LOG,
                         'CR Total : '||cr_total
                        );

   Fnd_File.put_line (Fnd_File.LOG,
                         'DR Total : '||dr_total
                        );

     Fnd_File.put_line (Fnd_File.output, ' ');
      Fnd_File.put_line (Fnd_File.output,
                            ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || 'Grand Total'
                         || CHR (9)
                         || dr_total
                         ||  CHR (9)
                         || cr_total
                         );

             closing_balance:= (cr_total)-(dr_total);

    Fnd_File.put_line (Fnd_File.output, ' ');

              Fnd_File.put_line (Fnd_File.output,
                            ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || 'Closing Balance  '
                         || CHR (9)
                         || closing_balance
                         );


CLOSE c;

END Xxabrl_gl_offset_PROC;
END Xxabrl_gl_offset_pkg; 
/


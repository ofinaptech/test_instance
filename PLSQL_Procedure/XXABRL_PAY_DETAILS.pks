CREATE OR REPLACE PACKAGE APPS.xxabrl_pay_details
IS
   PROCEDURE xxabrl_main_pkg (
      errbuf    OUT      VARCHAR2,
      retcode   OUT      NUMBER,
      p_type    IN       VARCHAR2
   );

   PROCEDURE xxabrl_pay_daily_data;

   PROCEDURE xxabrl_pay_dump;
END xxabrl_pay_details; 
/


CREATE OR REPLACE PACKAGE APPS.xxabrl_baan_to_ar_main
IS
   PROCEDURE main_proc (x_err_buf OUT VARCHAR2, x_ret_code OUT NUMBER);

   PROCEDURE baan_intermediate_to_staging (
      x_err_buf    OUT   VARCHAR2,
      x_ret_code   OUT   NUMBER
   );

   PROCEDURE baan_intermediate_to_stag_t (
      x_err_buf    OUT   VARCHAR2,
      x_ret_code   OUT   NUMBER
   );

   PROCEDURE baan_tender (x_err_buf OUT VARCHAR2, x_ret_code OUT NUMBER);
END xxabrl_baan_to_ar_main; 
/


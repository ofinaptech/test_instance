CREATE OR REPLACE PROCEDURE APPS.xxabrl_update_item_eccid_prc (
   errbuf    OUT      VARCHAR2,
   retcode   OUT      VARCHAR2,
   p_item    IN       VARCHAR2
)
IS

   /*
   =========================================================================================================
   ||   Procedure Name  : xxabrl_update_item_eccid_prc
   ||   Description : ABRL ITEM Update Expense Account Code Program
   ||
   ||   Version     Date            Author              Modification
   ||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
   ||   1.0.0      29-sep-2010  Mitul Vanpariya      New Development
   =======================================================================================================*/
BEGIN
   DECLARE
      v_req_id                 NUMBER;
      v_item_type              VARCHAR2 (250)   := NULL;
      v_item_type1             VARCHAR2 (250)   := NULL;
      v_exp_account            NUMBER           := NULL;
      v_enabled_flag           CHAR             := NULL;
      v_end_date_active        DATE             := NULL;
      v_inserted               VARCHAR2 (30000) := NULL;
      v_not_inserted           VARCHAR2 (30000) := NULL;
      v_err_code               VARCHAR2 (250)   := NULL;
      v_err_code1              VARCHAR2 (250)   := NULL;
      i                        INTEGER          := 0;
      j                        INTEGER          := 0;
      v_cc_code                VARCHAR2 (250);
      v_count                  NUMBER           := 0;
      fatal_error_except       EXCEPTION;
      non_fatal_error_except   EXCEPTION;
   BEGIN
      fnd_file.put_line (fnd_file.LOG,
                            'Code Combinations                         '
                         || 'Org Id                '
                        );
      fnd_file.put_line (fnd_file.LOG,
                            '==================                        '
                         || '=======            '
                        );

      SELECT item_type
        INTO v_item_type
        FROM apps.mtl_system_items
       WHERE segment1 = p_item AND organization_id = 97;

      IF v_item_type = 'ABRL OPEX'
      THEN
         FOR inv_rec IN (SELECT *
                           FROM apps.mtl_system_items a
                          WHERE a.segment1 = p_item
                            AND a.organization_id <> 97)
         LOOP
            SELECT COUNT (*)
              INTO v_count
              FROM apps.gl_code_combinations cc2
             WHERE    cc2.segment1
                   || '.'
                   || cc2.segment2
                   || '.'
                   || cc2.segment3
                   || '.'
                   || cc2.segment4
                   || '.'
                   || cc2.segment5
                   || '.'
                   || cc2.segment6
                   || '.'
                   || cc2.segment7
                   || '.'
                   || cc2.segment8 =
                      (SELECT    cc.segment1
                              || '.'
                              || cc.segment2
                              || '.'
                              || cc.segment3
                              || '.'
                              || cc.segment4
                              || '.'
                              || cc.segment5
                              || '.'
                              || (SELECT cc1.segment6
                                    FROM apps.mtl_system_items e,
                                         apps.gl_code_combinations cc1
                                   WHERE e.expense_account =
                                                       cc1.code_combination_id
                                     AND e.segment1 = inv_rec.segment1
                                     AND e.organization_id = 97)
                              || '.'
                              || cc.segment7
                              || '.'
                              || cc.segment8
                         FROM apps.gl_code_combinations cc
                        WHERE cc.code_combination_id = inv_rec.expense_account);

            IF v_count > 0
            THEN
               SELECT code_combination_id, enabled_flag, end_date_active
                 INTO v_exp_account, v_enabled_flag, v_end_date_active
                 FROM apps.gl_code_combinations cc2
                WHERE    cc2.segment1
                      || '.'
                      || cc2.segment2
                      || '.'
                      || cc2.segment3
                      || '.'
                      || cc2.segment4
                      || '.'
                      || cc2.segment5
                      || '.'
                      || cc2.segment6
                      || '.'
                      || cc2.segment7
                      || '.'
                      || cc2.segment8 =
                         (SELECT    cc.segment1
                                 || '.'
                                 || cc.segment2
                                 || '.'
                                 || cc.segment3
                                 || '.'
                                 || cc.segment4
                                 || '.'
                                 || cc.segment5
                                 || '.'
                                 || (SELECT cc1.segment6
                                       FROM apps.mtl_system_items e,
                                            apps.gl_code_combinations cc1
                                      WHERE e.expense_account =
                                                       cc1.code_combination_id
                                        AND e.segment1 = inv_rec.segment1
                                        AND e.organization_id = 97)
                                 || '.'
                                 || cc.segment7
                                 || '.'
                                 || cc.segment8
                            FROM apps.gl_code_combinations cc
                           WHERE cc.code_combination_id =
                                                       inv_rec.expense_account);

               IF     v_exp_account IS NOT NULL
                  AND v_enabled_flag = 'Y'
                  AND v_end_date_active IS NULL
               THEN
                  INSERT INTO mtl_system_items_interface
                              (segment1, organization_id, expense_account,
                               process_flag, transaction_type,
                               set_process_id, last_update_date,
                               program_update_date, last_updated_by)
                     SELECT inv_rec.segment1, inv_rec.organization_id,
                            v_exp_account, 1, 'UPDATE', 1, TRUNC (SYSDATE),
                            TRUNC (SYSDATE), 4191
                       FROM DUAL;

                  COMMIT;

                  SELECT    segment1
                         || '.'
                         || segment2
                         || '.'
                         || segment3
                         || '.'
                         || segment4
                         || '.'
                         || segment5
                         || '.'
                         || segment6
                         || '.'
                         || segment7
                         || '.'
                         || segment8
                    INTO v_cc_code
                    FROM apps.gl_code_combinations
                   WHERE code_combination_id = v_exp_account;

                  v_inserted :=
                        v_inserted
                     || CHR (10)
                     || v_cc_code
                     || '     '
                     || inv_rec.organization_id
                     || '     Code Combinations Updated';
                  i := i + 1;
               ELSE
                  SELECT    cc.segment1
                         || '.'
                         || cc.segment2
                         || '.'
                         || cc.segment3
                         || '.'
                         || cc.segment4
                         || '.'
                         || cc.segment5
                         || '.'
                         || (SELECT cc1.segment6
                               FROM apps.mtl_system_items e,
                                    apps.gl_code_combinations cc1
                              WHERE e.expense_account =
                                                       cc1.code_combination_id
                                AND e.segment1 = inv_rec.segment1
                                AND e.organization_id = 97)
                         || '.'
                         || cc.segment7
                         || '.'
                         || cc.segment8
                    INTO v_err_code
                    FROM apps.gl_code_combinations cc
                   WHERE cc.code_combination_id = inv_rec.expense_account;

                  IF v_enabled_flag <> 'Y' OR v_end_date_active IS NOT NULL
                  THEN
                     v_not_inserted :=
                           v_not_inserted
                        || CHR (10)
                        || v_err_code
                        || '  '
                        || inv_rec.organization_id
                        || '         Code Combinations Expired or Disable Please Check it';
                  END IF;
               END IF;
               
            ELSE
            
             SELECT    cc.segment1
                         || '.'
                         || cc.segment2
                         || '.'
                         || cc.segment3
                         || '.'
                         || cc.segment4
                         || '.'
                         || cc.segment5
                         || '.'
                         || (SELECT cc1.segment6
                               FROM apps.mtl_system_items e,
                                    apps.gl_code_combinations cc1
                              WHERE e.expense_account =
                                                       cc1.code_combination_id
                                AND e.segment1 = inv_rec.segment1
                                AND e.organization_id = 97)
                         || '.'
                         || cc.segment7
                         || '.'
                         || cc.segment8
                    INTO v_err_code
                    FROM apps.gl_code_combinations cc
                   WHERE cc.code_combination_id = inv_rec.expense_account;
               v_not_inserted :=
                     v_not_inserted
                  || CHR (10)
                  || v_err_code
                  || '  '
                  || inv_rec.organization_id
                  || '         Code Combination Not Available';
            END IF;
         END LOOP;

         fnd_file.put_line (fnd_file.LOG, 'Total Records Inserted :  ' || i);
      END IF;

      SELECT item_type
        INTO v_item_type1
        FROM apps.mtl_system_items
       WHERE segment1 = p_item AND organization_id = 97;

      IF v_item_type = 'ABRL CAPEX'
      THEN
         FOR inv_rec1 IN (SELECT *
                            FROM apps.mtl_system_items a
                           WHERE a.segment1 = p_item
                             AND a.organization_id <> 97)
         LOOP
         
         
          SELECT count(*)
              INTO v_count
              FROM apps.gl_code_combinations cc2
             WHERE    cc2.segment1
                   || '.'
                   || cc2.segment2
                   || '.'
                   || cc2.segment3
                   || '.'
                   || cc2.segment4
                   || '.'
                   || cc2.segment5
                   || '.'
                   || cc2.segment6
                   || '.'
                   || cc2.segment7
                   || '.'
                   || cc2.segment8 =
                      (SELECT    cc.segment1
                              || '.'
                              || cc.segment2
                              || '.'
                              || cc.segment3
                              || '.'
                              || cc.segment4
                              || '.'
                              || cc.segment5
                              || '.'
                              || DECODE (cc2.segment1,
                                         '11', '113901',
                                         '31', '113902',
                                         (SELECT cc1.segment6
                                            FROM apps.mtl_system_items e,
                                                 apps.gl_code_combinations cc1
                                           WHERE e.expense_account =
                                                       cc1.code_combination_id
                                             AND e.segment1 =
                                                             inv_rec1.segment1
                                             AND e.organization_id = 97)
                                        )
                              || '.'
                              || cc.segment7
                              || '.'
                              || cc.segment8
                         FROM apps.gl_code_combinations cc
                        WHERE cc.code_combination_id =inv_rec1.expense_account);
                        
                        
             IF v_count > 0 then
             
                                     
            SELECT code_combination_id, enabled_flag, end_date_active
              INTO v_exp_account, v_enabled_flag, v_end_date_active
              FROM apps.gl_code_combinations cc2
             WHERE    cc2.segment1
                   || '.'
                   || cc2.segment2
                   || '.'
                   || cc2.segment3
                   || '.'
                   || cc2.segment4
                   || '.'
                   || cc2.segment5
                   || '.'
                   || cc2.segment6
                   || '.'
                   || cc2.segment7
                   || '.'
                   || cc2.segment8 =
                      (SELECT    cc.segment1
                              || '.'
                              || cc.segment2
                              || '.'
                              || cc.segment3
                              || '.'
                              || cc.segment4
                              || '.'
                              || cc.segment5
                              || '.'
                              || DECODE (cc2.segment1,
                                         '11', '113901',
                                         '31', '113902',
                                         (SELECT cc1.segment6
                                            FROM apps.mtl_system_items e,
                                                 apps.gl_code_combinations cc1
                                           WHERE e.expense_account =
                                                       cc1.code_combination_id
                                             AND e.segment1 =
                                                             inv_rec1.segment1
                                             AND e.organization_id = 97)
                                        )
                              || '.'
                              || cc.segment7
                              || '.'
                              || cc.segment8
                         FROM apps.gl_code_combinations cc
                        WHERE cc.code_combination_id =
                                                      inv_rec1.expense_account);

            IF     v_exp_account IS NOT NULL
               AND v_enabled_flag = 'Y'
               AND v_end_date_active IS NULL
            THEN
               INSERT INTO mtl_system_items_interface
                           (segment1, organization_id, expense_account,
                            process_flag, transaction_type, set_process_id,
                            last_update_date, program_update_date,
                            last_updated_by)
                  SELECT inv_rec1.segment1, inv_rec1.organization_id,
                         v_exp_account, 1, 'UPDATE', 1, TRUNC (SYSDATE),
                         TRUNC (SYSDATE), 4191
                    FROM DUAL;

               COMMIT;

               SELECT    segment1
                      || '.'
                      || segment2
                      || '.'
                      || segment3
                      || '.'
                      || segment4
                      || '.'
                      || segment5
                      || '.'
                      || segment6
                      || '.'
                      || segment7
                      || '.'
                      || segment8
                 INTO v_cc_code
                 FROM apps.gl_code_combinations
                WHERE code_combination_id = v_exp_account;

               v_inserted :=
                     v_inserted
                  || CHR (10)
                  || v_cc_code
                  || '     '
                  || inv_rec1.organization_id
                  || '     Code Combinations Updated';
               j := j + 1;
            ELSE
               SELECT    cc.segment1
                      || '.'
                      || cc.segment2
                      || '.'
                      || cc.segment3
                      || '.'
                      || cc.segment4
                      || '.'
                      || cc.segment5
                      || '.'
                      || DECODE (cc.segment1,
                                 '11', '113901',
                                 '31', '113902',
                                 (SELECT cc1.segment6
                                    FROM apps.mtl_system_items e,
                                         apps.gl_code_combinations cc1
                                   WHERE e.expense_account =
                                                       cc1.code_combination_id
                                     AND e.segment1 = inv_rec1.segment1
                                     AND e.organization_id = 97)
                                )
                      || '.'
                      || cc.segment7
                      || '.'
                      || cc.segment8
                 INTO v_err_code1
                 FROM apps.gl_code_combinations cc
                WHERE cc.code_combination_id = inv_rec1.expense_account;

              

               IF v_enabled_flag <> 'Y' OR v_end_date_active IS NOT NULL
               THEN
                  v_not_inserted :=
                        v_not_inserted
                     || CHR (10)
                     || v_err_code1
                     || '  '
                     || inv_rec1.organization_id
                     || '         Code Combinations Expired or Disable Please Check it';
               END IF;
            END IF;
            
            ELSE
            
             SELECT    cc.segment1
                      || '.'
                      || cc.segment2
                      || '.'
                      || cc.segment3
                      || '.'
                      || cc.segment4
                      || '.'
                      || cc.segment5
                      || '.'
                      || DECODE (cc.segment1,
                                 '11', '113901',
                                 '31', '113902',
                                 (SELECT cc1.segment6
                                    FROM apps.mtl_system_items e,
                                         apps.gl_code_combinations cc1
                                   WHERE e.expense_account =
                                                       cc1.code_combination_id
                                     AND e.segment1 = inv_rec1.segment1
                                     AND e.organization_id = 97)
                                )
                      || '.'
                      || cc.segment7
                      || '.'
                      || cc.segment8
                 INTO v_err_code1
                 FROM apps.gl_code_combinations cc
                WHERE cc.code_combination_id = inv_rec1.expense_account; 
            
               v_not_inserted :=
                     v_not_inserted
                  || CHR (10)
                  || v_err_code1
                  || '  '
                  || inv_rec1.organization_id
                  || '         Code Combination Not Available';
            END IF;
            
         END LOOP;

         fnd_file.put_line (fnd_file.LOG, 'Total Records Inserted :   ' || j);
      END IF;

      fnd_file.put_line (fnd_file.LOG, v_inserted);
      fnd_file.put_line (fnd_file.LOG, v_not_inserted);
      COMMIT;

      IF i <> 0 OR j <> 0
      THEN
         BEGIN
            --apps.Fnd_Global.apps_initialize(0,51144,401);
            v_req_id :=
               fnd_request.submit_request ('INV',
                                           'INCOIN',
                                           'Update Item Open Interface',
                                           SYSDATE,
                                           FALSE,
                                           '97',
                                           '1',
                                           '1',
                                           '1',
                                           '1',
                                           NULL,
                                           2
                                          );
            COMMIT;
         END;
      END IF;

      IF i <> 60 AND v_item_type = 'ABRL OPEX'
      THEN
         retcode := 1;
      END IF;

      IF j <> 60 AND v_item_type = 'ABRL CAPEX'
      THEN
         retcode := 1;
      END IF;
   END;

END;
--   exception
--    when no_data_found then null; 
/


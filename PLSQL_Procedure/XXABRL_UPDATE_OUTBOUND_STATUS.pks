CREATE OR REPLACE PACKAGE APPS.XXABRL_Update_Outbound_Status AS

/* Validation added by Vikash Kumar on 06-July-2012 for Address. As per the requirement 
Address_line1||Address_line2 for any of the sites should not be less than 4 character*/

  PROCEDURE Update_Status(err_buf     OUT VARCHAR2,
                          ret_code    OUT NUMBER,
                          P_PROG_NAME IN VARCHAR2);
  PROCEDURE Err_file_Prog(err_buf     OUT VARCHAR2,
                          ret_code    OUT NUMBER);
END XXABRL_Update_Outbound_Status; 
/


CREATE OR REPLACE PACKAGE BODY APPS.XXABRL_LOC_EXP_PKG IS
  PROCEDURE XXABRL_LOC_EXP_PROC(errbuf        OUT VARCHAR2,
                                retcode       OUT NUMBER,
                                P_FROM_PERIOD IN VARCHAR2,
                                P_TO_PERIOD   IN VARCHAR2,
                                P_FROM_LOCATION IN VARCHAR2,
                                P_TO_LOCATION   IN VARCHAR2,
                                 P_FROM_ACCOUNT   IN VARCHAR2,
                                P_TO_ACCOUNT  IN VARCHAR2,
                                P_STATE_SBU  IN VARCHAR2
                               -- P_LEDGER_ID IN VARCHAR2 
                               ) as
    v_str1particulars  LONG;
    v_str1data         LONG;
    v_strlcodedata     LONG;
    v_str1acct         varchar2(240);
    v_net_db_bal      number(30);
    v_total1          number(30) := 0;
    v_str2data        LONG;
  begin
    Fnd_File.put_line(Fnd_File.OUTPUT, 'ABRL Location wise Expense Report');
    Fnd_File.put_line(Fnd_File.OUTPUT, '');
    Fnd_File.put_line(Fnd_File.OUTPUT,
                      CHR(9) || CHR(9) || CHR(9) || 'As on Date' || CHR(9) ||
                      sysdate || CHR(9));
     Fnd_File.put_line(Fnd_File.OUTPUT,
                      CHR(9) || CHR(9) || CHR(9) || ' Period From' || CHR(9) ||
                      P_FROM_PERIOD || CHR(9));
     Fnd_File.put_line(Fnd_File.OUTPUT,
                      CHR(9) || CHR(9) || CHR(9) || 'Period To' || CHR(9) ||
                      P_TO_PERIOD || CHR(9));
     Fnd_File.put_line(Fnd_File.OUTPUT,
                    CHR(9) || CHR(9) || CHR(9) || 'Location From' || CHR(9) ||
                P_FROM_LOCATION || CHR(9));
     Fnd_File.put_line(Fnd_File.OUTPUT,
                      CHR(9) || CHR(9) || CHR(9) || 'Location To' || CHR(9) ||
                      P_TO_LOCATION || CHR(9));
     Fnd_File.put_line(Fnd_File.OUTPUT,
                      CHR(9) || CHR(9) || CHR(9) || 'Account From' || CHR(9) ||
                      P_FROM_ACCOUNT || CHR(9));
    Fnd_File.put_line(Fnd_File.OUTPUT,
                      CHR(9) || CHR(9) || CHR(9) || 'Account To' || CHR(9) ||
                      P_TO_ACCOUNT || CHR(9));
    Fnd_File.put_line(Fnd_File.OUTPUT,
                      CHR(9) || CHR(9) || CHR(9) || 'State SBU' || CHR(9) ||
                      P_STATE_SBU || CHR(9));
                    
    Fnd_File.put_line(Fnd_File.OUTPUT, '');
    Fnd_File.put_line(Fnd_File.OUTPUT, '');
 begin
      for c1 in (select distinct locations
                   from (SELECT a.description locations,
                                b.description particulars,
                                SUM(gb.period_net_dr - gb.period_net_cr) net_db_bal
                           FROM (SELECT ffvv.flex_value, ffvv.description
                                   FROM fnd_flex_values_vl  ffvv,
                                        fnd_flex_value_sets ffvs
                                  WHERE ffvs.flex_value_set_name =
                                        'ABRL_GL_Location'
                                    AND ffvs.flex_value_set_id =
                                        ffvv.flex_value_set_id) a,
                                (SELECT ffvv.flex_value, ffvv.description
                                   FROM fnd_flex_values_vl  ffvv,
                                        fnd_flex_value_sets ffvs
                                  WHERE ffvs.flex_value_set_name =
                                        'ABRL_GL_Account'
                                    AND ffvs.flex_value_set_id =
                                        ffvv.flex_value_set_id) b,
                                gl_code_combinations gcc,
                                gl_balances gb
                          WHERE  a.flex_value = gcc.segment4
                            AND b.flex_value = gcc.segment6
                            AND gb.code_combination_id =
                                gcc.code_combination_id
                            AND gb.period_name between  P_FROM_PERIOD AND P_TO_PERIOD 
                            AND gcc.segment4 between P_FROM_LOCATION and P_TO_LOCATION 
                            AND gcc.segment6 between P_FROM_ACCOUNT and P_TO_ACCOUNT 
                            AND gcc.segment3 = P_STATE_SBU --AND GB.LEDGER_ID=P_LEDGER_ID               
                          GROUP BY a.description,b.description
                          order by a.description)
                  order by locations) loop
                   v_str1data := v_str1data || CHR(9) || c1.locations;
     
      end loop;
      Fnd_File.put_line(Fnd_File.OUTPUT, v_str1data||CHR(9) ||'Expense wise total');
      for cl in (select distinct particulars
                   from (SELECT a.description locations,
                                b.description particulars,
                                SUM(gb.period_net_dr - gb.period_net_cr) net_db_bal
                           FROM (SELECT ffvv.flex_value, ffvv.description
                                   FROM fnd_flex_values_vl  ffvv,
                                        fnd_flex_value_sets ffvs
                                  WHERE ffvs.flex_value_set_name =
                                        'ABRL_GL_Location'
                                    AND ffvs.flex_value_set_id =
                                        ffvv.flex_value_set_id) a,
                                (SELECT ffvv.flex_value, ffvv.description
                                   FROM fnd_flex_values_vl  ffvv,
                                        fnd_flex_value_sets ffvs
                                  WHERE ffvs.flex_value_set_name =
                                        'ABRL_GL_Account'
                                    AND ffvs.flex_value_set_id =
                                        ffvv.flex_value_set_id) b,
                                gl_code_combinations gcc,
                                gl_balances gb
                          WHERE  a.flex_value = gcc.segment4
                            AND b.flex_value = gcc.segment6
                            AND gb.code_combination_id =
                                gcc.code_combination_id
                            AND gb.period_name between  P_FROM_PERIOD AND P_TO_PERIOD 
                            AND gcc.segment4 between P_FROM_LOCATION and P_TO_LOCATION 
                            AND gcc.segment6 between P_FROM_ACCOUNT and P_TO_ACCOUNT
                            AND gcc.segment3 = P_STATE_SBU -- AND GB.LEDGER_ID=P_LEDGER_ID                
                          GROUP BY a.description,b.description
                          order by b.description)
                  order by particulars) loop
          v_str1particulars := v_str1particulars || CHR(9) || cl.particulars;
        for cp in (select distinct locations
                   from (SELECT a.description locations,
                                b.description particulars,
                                SUM(gb.period_net_dr - gb.period_net_cr) net_db_bal
                           FROM (SELECT ffvv.flex_value, ffvv.description
                                   FROM fnd_flex_values_vl  ffvv,
                                        fnd_flex_value_sets ffvs
                                  WHERE ffvs.flex_value_set_name =
                                        'ABRL_GL_Location'
                                    AND ffvs.flex_value_set_id =
                                        ffvv.flex_value_set_id) a,
                                (SELECT ffvv.flex_value, ffvv.description
                                   FROM fnd_flex_values_vl  ffvv,
                                        fnd_flex_value_sets ffvs
                                  WHERE ffvs.flex_value_set_name =
                                        'ABRL_GL_Account'
                                    AND ffvs.flex_value_set_id =
                                        ffvv.flex_value_set_id) b,
                                gl_code_combinations gcc,
                                gl_balances gb
                          WHERE  a.flex_value = gcc.segment4
                            AND b.flex_value = gcc.segment6
                            AND gb.code_combination_id =
                                gcc.code_combination_id
                            AND gb.period_name between  P_FROM_PERIOD AND P_TO_PERIOD
                            AND gcc.segment4 between P_FROM_LOCATION and P_TO_LOCATION 
                            AND gcc.segment6 between P_FROM_ACCOUNT and P_TO_ACCOUNT 
                            AND gcc.segment3 = P_STATE_SBU -- AND GB.LEDGER_ID=P_LEDGER_ID                
                          GROUP BY a.description,b.description
                          order by a.description)
                  order by locations) loop
          begin
            select net_db_bal
              into v_net_db_bal
              from (SELECT a.description locations,
                           b.description particulars,
                           SUM(gb.period_net_dr - gb.period_net_cr) net_db_bal
                      FROM (SELECT ffvv.flex_value, ffvv.description
                              FROM fnd_flex_values_vl  ffvv,
                                   fnd_flex_value_sets ffvs
                             WHERE ffvs.flex_value_set_name =
                                   'ABRL_GL_Location'
                               AND ffvs.flex_value_set_id =
                                   ffvv.flex_value_set_id) a,
                           (SELECT ffvv.flex_value, ffvv.description
                              FROM fnd_flex_values_vl  ffvv,
                                   fnd_flex_value_sets ffvs
                             WHERE ffvs.flex_value_set_name =
                                   'ABRL_GL_Account'
                               AND ffvs.flex_value_set_id =
                                   ffvv.flex_value_set_id) b,
                           gl_code_combinations gcc,
                           gl_balances gb
                     WHERE a.flex_value = gcc.segment4
                       AND b.flex_value = gcc.segment6
                       AND gb.code_combination_id = gcc.code_combination_id
                       AND gb.period_name between  P_FROM_PERIOD AND P_TO_PERIOD
                       AND gcc.segment4 between P_FROM_LOCATION and P_TO_LOCATION 
                       AND gcc.segment6 between P_FROM_ACCOUNT and P_TO_ACCOUNT 
                      AND gcc.segment3 = P_STATE_SBU -- AND GB.LEDGER_ID=P_LEDGER_ID
                     GROUP BY a.description, b.description
                     order by b.description)
             where particulars = cl.particulars
               and locations = cp.locations;     
             v_str1particulars := v_str1particulars|| CHR(9) ||to_char(v_net_db_bal);
            v_total1   := v_total1 + v_net_db_bal;
          exception
            when no_data_found then
                v_str1particulars:=  v_str1particulars || CHR(9) || ' ';
          end;
        end loop;
          v_str1particulars := SUBSTR(v_str1particulars,2) || CHR(9) || v_total1;
        Fnd_File.put_line(Fnd_File.OUTPUT,   v_str1particulars || CHR(9));
          v_str1particulars := NULL;
        v_total1   := 0;
      end loop;
      v_str2data := to_number(null);
      for c3 in (select SUM(net_db_bal) net_db_bal
              from (SELECT a.description locations,
                           b.description particulars,
                           SUM(gb.period_net_dr - gb.period_net_cr) net_db_bal
                      FROM (SELECT ffvv.flex_value, ffvv.description
                              FROM fnd_flex_values_vl  ffvv,
                                   fnd_flex_value_sets ffvs
                             WHERE ffvs.flex_value_set_name =
                                   'ABRL_GL_Location'
                               AND ffvs.flex_value_set_id =
                                   ffvv.flex_value_set_id) a,
                           (SELECT ffvv.flex_value, ffvv.description
                              FROM fnd_flex_values_vl  ffvv,
                                   fnd_flex_value_sets ffvs
                             WHERE ffvs.flex_value_set_name =
                                   'ABRL_GL_Account'
                               AND ffvs.flex_value_set_id =
                                   ffvv.flex_value_set_id) b,
                           gl_code_combinations gcc,
                           gl_balances gb
                     WHERE a.flex_value = gcc.segment4
                       AND b.flex_value = gcc.segment6
                       AND gb.code_combination_id = gcc.code_combination_id
                        AND gb.period_name between  P_FROM_PERIOD AND P_TO_PERIOD
                            AND gcc.segment4 between P_FROM_LOCATION and P_TO_LOCATION 
                            AND gcc.segment6 between P_FROM_ACCOUNT and P_TO_ACCOUNT 
                           AND gcc.segment3 = P_STATE_SBU  --AND GB.LEDGER_ID=P_LEDGER_ID  
                     GROUP BY a.description, b.description
                     order by b.description)GROUP BY  locations
                  order by locations) loop
      
        v_str2data := v_str2data || CHR(9) || c3.net_db_bal;
      end loop;
      Fnd_File.put_line(Fnd_File.OUTPUT, 'Location wise total'||v_str2data);
    end;
  END XXABRL_LOC_EXP_PROC;
END XXABRL_LOC_EXP_PKG;
/


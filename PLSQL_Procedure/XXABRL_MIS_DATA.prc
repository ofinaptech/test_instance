CREATE OR REPLACE PROCEDURE APPS.xxabrl_mis_data (
   errbuf      OUT      VARCHAR2,
   retcode     OUT      VARCHAR2,
   form_date   IN       VARCHAR2,
   to_date1    IN       VARCHAR2
)
AS
   p_from_run_date     DATE
                     := NVL (fnd_date.canonical_to_date (form_date), SYSDATE);
   p_to_run_date       DATE
                      := NVL (fnd_date.canonical_to_date (to_date1), SYSDATE);
   ap_file             UTL_FILE.file_type;
   g_conc_request_id   NUMBER        := fnd_profile.VALUE ('CONC_REQUEST_ID');

   CURSOR ap_main
   IS
      SELECT '1', povs.vendor_site_code, povs.vendor_site_id,
             api.invoice_type_lookup_code invoice_type,
             REPLACE (api.invoice_num, '^', '//') invoice_num,
             api.invoice_date,
             (SELECT TRANSLATE (api.description,
                                'X' || CHR (9) || CHR (10) || CHR (13),
                                'X'
                               )
                FROM DUAL) description,
             apd.dist_code_combination_id ccid, gl.segment3 sbu,
             (SELECT ffvl.description
                FROM apps.fnd_flex_values_vl ffvl
               WHERE ffvl.flex_value_set_id = 1013466
                 AND ffvl.flex_value = gl.segment3) sbu_desc,
             gl.segment4 location_code,
             (SELECT ffvl.description
                FROM apps.fnd_flex_values_vl ffvl
               WHERE ffvl.flex_value_set_id = 1013467
                 AND ffvl.flex_value = gl.segment4) location_desc,
             gl.segment6 account_code,
             (SELECT ffvl.description
                FROM apps.fnd_flex_values_vl ffvl
               WHERE ffvl.flex_value_set_id = 1013469
                 AND ffvl.flex_value = gl.segment6) account_desc,
             api.invoice_currency_code,
             DECODE (api.invoice_type_lookup_code,
                     'CREDIT', ABS (z.amt_val)
                      - ABS (NVL (api.discount_amount_taken, 0)),
                     0
                    ) dr_val,
             DECODE (api.invoice_type_lookup_code,
                     'CREDIT', 0,
                     z.amt_val - NVL (api.discount_amount_taken, 0)
                    ) cr_val,
             NULL prepay_amt_remaing,
             TO_CHAR (api.doc_sequence_value) payment_num, NULL check_number,
             NULL check_date, pov.segment1 vendor_num,
             (CASE
                 WHEN (SUBSTR (pov.segment1, 1, 1) = '7')
                    THEN (SELECT apsa1.attribute14
                            FROM apps.ap_suppliers aps1,
                                 apps.ap_supplier_sites_all apsa1
                           WHERE aps1.vendor_id = apsa1.vendor_id
                             AND aps1.segment1 = pov.segment1
                             AND apsa1.vendor_site_id = povs.vendor_site_id)
                 ELSE NULL
              END
             ) "ATTRIBUTE_14",
             pov.vendor_name, pov.vendor_type_lookup_code vendor_type,
             apd.po_distribution_id, apil.po_header_id,            
             --added  on '30-sep-2015'
             apil.rcv_transaction_id,
                                     --added  on '30-sep-2015'
                                     api.org_id,
             (SELECT hr.NAME
                FROM apps.hr_operating_units hr
               WHERE hr.organization_id = api.org_id) operating_unit,
             (SELECT aba.batch_name
                FROM apps.ap_batches_all aba
               WHERE aba.batch_id = api.batch_id) batch_name, api.invoice_id,
             NULL pay_accounting_date, NULL payment_type_flag,
             NULL bank_account_name, NULL bank_account_num, NULL bank_uti_no,
             api.invoice_amount, api.amount_paid, z.accounting_date gl_date,
             NULL prepay_invoice_id,
             (SELECT NAME
                FROM ap_terms AT
               WHERE AT.term_id = api.terms_id) term_name,
             api.pay_group_lookup_code pay_group,
             apil.reference_1 ppi_invoice_number, api.attribute2 grn_number,
             api.attribute3 grn_date, NULL hold_flag
        FROM apps.ap_invoices_all api,
             apps.ap_invoice_lines_all apil,
             apps.ap_invoice_distributions_all apd,
             apps.ap_suppliers pov,
             apps.gl_code_combinations gl,
             apps.ap_supplier_sites_all povs,
             (SELECT   NVL (SUM (apd.amount), 0) amt_val, api.invoice_id,
                       apd.accounting_date
                  FROM apps.ap_invoices_all api,
                       apps.ap_invoice_lines_all apil,
                       apps.ap_invoice_distributions_all apd
                 WHERE api.invoice_id = apd.invoice_id
                   AND apil.invoice_id = api.invoice_id
                   AND apil.line_number = apd.invoice_line_number
                   AND TO_DATE (TO_CHAR (apd.accounting_date, 'DD-MON-YY'))
                          BETWEEN p_from_run_date
                              AND p_to_run_date
                   AND api.invoice_type_lookup_code <> 'PREPAYMENT'
                   AND apil.line_type_lookup_code <> 'PREPAY'
              GROUP BY api.invoice_id, apd.accounting_date) z
       WHERE api.invoice_id = z.invoice_id
         AND api.invoice_id = apd.invoice_id
         AND gl.code_combination_id = api.accts_pay_code_combination_id
         AND apil.invoice_id = api.invoice_id
         AND apil.line_number = apd.invoice_line_number
         AND api.vendor_site_id = povs.vendor_site_id
         AND TO_DATE (TO_CHAR (z.accounting_date, 'DD-MON-YY'))
                BETWEEN p_from_run_date
                    AND p_to_run_date
         AND apd.ROWID = (SELECT ROWID
                            FROM apps.ap_invoice_distributions_all
                           WHERE ROWNUM = 1 AND invoice_id = apd.invoice_id)
         AND api.vendor_id = pov.vendor_id
         AND api.invoice_type_lookup_code <> 'PREPAYMENT'
         --    AND pov.VENDOR_TYPE_LOOKUP_CODE='NON MERCHANDISE'
         AND ap_invoices_pkg.get_approval_status (api.invoice_id,
                                                  api.invoice_amount,
                                                  api.payment_status_flag,
                                                  api.invoice_type_lookup_code
                                                 ) IN
                                                    ('APPROVED', 'CANCELLED')
         AND (   (api.invoice_type_lookup_code <> 'DEBIT')
              OR (api.invoice_type_lookup_code = 'DEBIT')
             )
              AND not EXISTS (SELECT invoice_id
                         FROM xxabrl.xxabrl_tsrl_open_invoices a
                         where api.invoice_id=a.invoice_id)
         AND pov.segment1 IN (SELECT segment1
                                FROM xxabrl.xxabrl_mis_mismatch_vendors)
      UNION ALL
      SELECT '2', povs.vendor_site_code, povs.vendor_site_id,
             DECODE (api.invoice_type_lookup_code,
                     'PREPAYMENT', api.invoice_type_lookup_code,
                     'PAYMENT'
                    ) invoice_type,
             REPLACE (api.invoice_num, '^', '//') invoice_num,
             api.invoice_date,
             (SELECT TRANSLATE (api.description,
                                'X' || CHR (9) || CHR (10) || CHR (13),
                                'X'
                               )
                FROM DUAL) description,
             apd.dist_code_combination_id ccid, gl.segment3,
             (SELECT ffvl.description
                FROM apps.fnd_flex_values_vl ffvl
               WHERE ffvl.flex_value_set_id = 1013466
                 AND ffvl.flex_value = gl.segment3) sbu_desc,
             gl.segment4,
             (SELECT ffvl.description
                FROM apps.fnd_flex_values_vl ffvl
               WHERE ffvl.flex_value_set_id = 1013467
                 AND ffvl.flex_value = gl.segment4) location_desc,
             gl.segment6,
             (SELECT ffvl.description
                FROM apps.fnd_flex_values_vl ffvl
               WHERE ffvl.flex_value_set_id = 1013469
                 AND ffvl.flex_value = gl.segment6) account_desc,
             api.payment_currency_code,
             DECODE (api.invoice_type_lookup_code,
                     'CREDIT', DECODE (status_lookup_code, 'VOIDED', 0, 0),
                     app.amount
                    ) dr_val,
             DECODE (api.invoice_type_lookup_code,
                     'CREDIT', DECODE (status_lookup_code,
                                       'VOIDED', app.amount,
                                       ABS (app.amount)
                                      ),
                     0
                    ) cr_val,
             NULL pre_amt_remaing,
             DECODE (api.payment_status_flag,
                     'Y', TO_CHAR (apc.doc_sequence_value),
                     'P', TO_CHAR (apc.doc_sequence_value),
                     TO_CHAR (apc.doc_sequence_value), 'N',
                     TO_CHAR (apc.doc_sequence_value)
                    ) payment_num,
             DECODE (api.payment_status_flag,
                     'Y', TO_CHAR (apc.check_number),
                     'P', TO_CHAR (apc.check_number)
                    ) check_number,
             apc.check_date, pov.segment1 vendor_num,
             (CASE
                 WHEN (SUBSTR (pov.segment1, 1, 1) = '7')
                    THEN (SELECT apsa1.attribute14
                            FROM apps.ap_suppliers aps1,
                                 apps.ap_supplier_sites_all apsa1
                           WHERE aps1.vendor_id = apsa1.vendor_id
                             AND aps1.segment1 = pov.segment1
                             AND apsa1.vendor_site_id = povs.vendor_site_id)
                 ELSE NULL
              END
             ) "ATTRIBUTE_14",
             pov.vendor_name, pov.vendor_type_lookup_code vendor_type,
             apd.po_distribution_id, apil.po_header_id,            
             --added  on '30-sep-2015'
             apil.rcv_transaction_id,
                                     --added  on '30-sep-2015'
                                     api.org_id,      --added on '27-Oct-2015'
             (SELECT hr.NAME
                FROM apps.hr_operating_units hr
               WHERE hr.organization_id = api.org_id) operating_unit,
             (SELECT aba.batch_name
                FROM apps.ap_batches_all aba
               WHERE aba.batch_id = api.batch_id) batch_name, api.invoice_id,
             app.accounting_date,
             DECODE (apc.payment_type_flag,
                     'Q', 'QUICK',
                     'M', 'MANUAL',
                     'R', 'REFUND'
                    ) payment_type,
             apc.bank_account_name, apc.bank_account_num,
             apc.attribute3 bank_uti_no, NULL invoice_amount,
             DECODE (api.invoice_type_lookup_code,
                     'PREPAYMENT', api.amount_paid,
                     NULL
                    ) amount_paid,
             apc.check_date gl_date, NULL prepay_invoice_id, NULL term_name,
             NULL pay_group, NULL ppi_invoice_number, NULL grn_number,
             NULL grn_date, aps.hold_flag
        FROM apps.ap_invoices_all api,
             apps.ap_invoice_lines_all apil,
--            APPS.AP_BATCHES_ALL ABA,
             apps.gl_code_combinations gl,
             apps.ap_invoice_distributions_all apd,
             apps.ap_suppliers pov,
--            apps.hr_operating_units hr,
             apps.ap_invoice_payments_all app,
             apps.ap_payment_schedules_all aps,
             apps.ap_checks_all apc,
             apps.ap_supplier_sites_all povs
       WHERE api.invoice_id = apd.invoice_id
         AND apil.invoice_id = api.invoice_id
         AND aps.invoice_id(+) = api.invoice_id
         AND gl.code_combination_id = api.accts_pay_code_combination_id
         AND apil.line_number = apd.invoice_line_number
         AND TO_DATE (TO_CHAR (app.accounting_date, 'DD-MON-YY'))
                BETWEEN p_from_run_date
                    AND p_to_run_date
         AND apd.ROWID = (SELECT ROWID
                            FROM apps.ap_invoice_distributions_all
                           WHERE ROWNUM = 1 AND invoice_id = apd.invoice_id)
         AND api.vendor_id = pov.vendor_id
         AND app.invoice_id = api.invoice_id
         AND app.check_id = apc.check_id
         AND apc.status_lookup_code IN
                ('CLEARED', 'NEGOTIABLE', 'VOIDED', 'RECONCILED UNACCOUNTED',
                 'RECONCILED', 'CLEARED BUT UNACCOUNTED')
         AND api.vendor_site_id = povs.vendor_site_id
         AND api.invoice_type_lookup_code <> 'PREPAYMENT'
          AND not EXISTS (SELECT invoice_id
                         FROM xxabrl.xxabrl_tsrl_open_invoices a
                         where api.invoice_id=a.invoice_id)
         AND pov.segment1 IN (SELECT segment1
                                FROM xxabrl.xxabrl_mis_mismatch_vendors);
BEGIN
   ap_file :=
      UTL_FILE.fopen ('VENDOR_INVOICE_PORTAL',
                      'MIS_Miss_Match_Data' || g_conc_request_id || '.txt',
                      'w'
                     );
   UTL_FILE.put_line (ap_file,
                         'VENDOR SITE CODE'
                      || '^'
                      || 'VENDOR SITE ID'
                      || '^'
                      || 'INVOICE_TYPE'
                      || '^'
                      || 'INVOICE NUMBER'
                      || '^'
                      || 'INVOICE DATE'
                      || '^'
                      || 'DESCRIPTION'
                      || '^'
                      || 'CCID'
                      || '^'
                      || 'SBU'
                      || '^'
                      || 'SBU DESC'
                      || '^'
                      || 'LOCATION CODE'
                      || '^'
                      || 'LOCATION DESC'
                      || '^'
                      || 'ACCOUNT CODE'
                      || '^'
                      || 'ACCOUNT DESC'
                      || '^'
                      || 'INVOICE CURRENCY CODE'
                      || '^'
                      || 'DR VAL'
                      || '^'
                      || 'CR VAL'
                      || '^'
                      || 'PREPAY AMT REMAING'
                      || '^'
                      || 'DOC NUM'
                      || '^'
                      || 'CHECK NUMBER'
                      || '^'
                      || 'CHECK_DATE'
                      || '^'
                      || 'VENDOR NUMBER'
                      || '^'
                      || 'ATTRIBUTE_14'
                      || '^'
                      || 'VENDOR NAME'
                      || '^'
                      || 'VENDOR TYPE'
                      || '^'
                      || 'PO DISTRIBUTION ID'
                      || '^'
                      || 'PO HEADER ID'
                      || '^'
                      || 'RCV TRANSACTION ID'
                      || '^'
                      || 'ORG ID'
                      || '^'
                      || 'OPERATING UNIT'
                      || '^'
                      || 'BATCH NAME'
                      || '^'
                      || 'INVOICE ID'
                      || '^'
                      || 'PAY ACCOUNTING DATE'
                      || '^'
                      || 'PAYMENT TYPE FLAG'
                      || '^'
                      || 'BANK ACCOUNT NAME'
                      || '^'
                      || 'BANK ACCOUNT NUM'
                      || '^'
                      || 'BANK UTI NO.'
                      || '^'
                      || 'INVOICE AMOUNT'
                      || '^'
                      || 'AMOUNT PAID'
                      || '^'
                      || 'GL DATE'
                      || '^'
                      || 'PREPAY_INVOICE_ID'
                      || '^'
                      || 'TERM NAME'
                      || '^'
                      || 'PAY GROUP'
                      || '^'
                      || 'PPI INVOICE NUM'
                      || '^'
                      || 'GRN NUMBER'
                      || '^'
                      || 'GRN DATE'
                      || '^'
                      || 'HOLD FLAG'
                     );

   FOR ap_rec IN ap_main
   LOOP
      UTL_FILE.put_line (ap_file,
                            ap_rec.vendor_site_code
                         || '^'
                         || ap_rec.vendor_site_id
                         || '^'
                         || ap_rec.invoice_type
                         || '^'
                         || ap_rec.invoice_num
                         || '^'
                         || ap_rec.invoice_date
                         || '^'
                         || ap_rec.description
                         || '^'
                         || ap_rec.ccid
                         || '^'
                         || ap_rec.sbu
                         || '^'
                         || ap_rec.sbu_desc
                         || '^'
                         || ap_rec.location_code
                         || '^'
                         || ap_rec.location_desc
                         || '^'
                         || ap_rec.account_code
                         || '^'
                         || ap_rec.account_desc
                         || '^'
                         || ap_rec.invoice_currency_code
                         || '^'
                         || ap_rec.dr_val
                         || '^'
                         || ap_rec.cr_val
                         || '^'
                         || ap_rec.prepay_amt_remaing
                         || '^'
                         || ap_rec.payment_num
                         || '^'
                         || ap_rec.check_number
                         || '^'
                         || ap_rec.check_date
                         || '^'
                         || ap_rec.vendor_num
                         || '^'
                         || ap_rec.attribute_14
                         || '^'
                         || ap_rec.vendor_name
                         || '^'
                         || ap_rec.vendor_type
                         || '^'
                         || ap_rec.po_distribution_id
                         || '^'
                         || ap_rec.po_header_id
                         || '^'
                         || ap_rec.rcv_transaction_id
                         || '^'
                         || ap_rec.org_id
                         || '^'
                         || ap_rec.operating_unit
                         || '^'
                         || ap_rec.batch_name
                         || '^'
                         || ap_rec.invoice_id
                         || '^'
                         || ap_rec.pay_accounting_date
                         || '^'
                         || ap_rec.payment_type_flag
                         || '^'
                         || ap_rec.bank_account_name
                         || '^'
                         || ap_rec.bank_account_num
                         || '^'
                         || ap_rec.bank_uti_no
                         || '^'
                         || ap_rec.invoice_amount
                         || '^'
                         || ap_rec.amount_paid
                         || '^'
                         || ap_rec.gl_date
                         || '^'
                         || ap_rec.prepay_invoice_id
                         || '^'
                         || ap_rec.term_name
                         || '^'
                         || ap_rec.pay_group
                         || '^'
                         || ap_rec.ppi_invoice_number
                         || '^'
                         || ap_rec.grn_number
                         || '^'
                         || ap_rec.grn_date
                         || '^'
                         || ap_rec.hold_flag           --|| CHR(13) || CHR(10)
                        );
   END LOOP;

   UTL_FILE.fclose (ap_file);
EXCEPTION
   WHEN TOO_MANY_ROWS
   THEN
      DBMS_OUTPUT.put_line ('Too Many Rows');
   WHEN NO_DATA_FOUND
   THEN
      DBMS_OUTPUT.put_line ('No Data Found');
   WHEN UTL_FILE.invalid_path
   THEN
      raise_application_error (-20000, 'utl_file.invalid_path');
   WHEN UTL_FILE.invalid_mode
   THEN
      raise_application_error (-20000, 'utl_file.invalid_mode');
   WHEN UTL_FILE.invalid_filehandle
   THEN
      raise_application_error (-20000, 'utl_file.invalid_filehandle');
   WHEN UTL_FILE.invalid_operation
   THEN
      raise_application_error (-20000, 'utl_file.invalid_operation');
   WHEN UTL_FILE.read_error
   THEN
      raise_application_error (-20001, 'utl_file.read_error');
   WHEN UTL_FILE.write_error
   THEN
      raise_application_error (-20001, 'utl_file.write_error');
   WHEN UTL_FILE.internal_error
   THEN
      raise_application_error (-20001, 'utl_file.internal_error');
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line (SUBSTR (   'Error '
                                    || TO_CHAR (SQLCODE)
                                    || ': '
                                    || SQLERRM,
                                    1,
                                    255
                                   )
                           );
      UTL_FILE.fclose (ap_file);
END; 
/


CREATE OR REPLACE PROCEDURE APPS.gst_abrl_supplier_add_info_xml (
   errbuf         OUT      VARCHAR2,
   retcode        OUT      NUMBER,
   p_run_date_1   IN       VARCHAR2
)
IS
   p_run_date          DATE
                  := NVL (fnd_date.canonical_to_date (p_run_date_1), SYSDATE);
   v_file              UTL_FILE.file_type;
   g_conc_request_id   NUMBER        := fnd_profile.VALUE ('CONC_REQUEST_ID');

   CURSOR supplier_add_info
   IS
      SELECT DISTINCT FINAL.vendor_id, FINAL.vendor_number,
                      FINAL.action_code, FINAL.tax_reg_dtl1,
                      CASE
                         WHEN FINAL.registration_number IS NOT NULL
                         AND LENGTH (FINAL.registration_number) = '15'
                            THEN FINAL.registration_number
                         ELSE NULL
                      END tax_reg_dtl2,
                      CASE
                         WHEN FINAL.registration_number IS NOT NULL
                         AND LENGTH (FINAL.registration_number) = '15'
                            THEN 'GSTIN'
                         ELSE 'RCM'
                      END tax_reg_dtl3,
                      FINAL.tax_reg_dtl4, FINAL.tax_reg_dtl5,
                      FINAL.tax_reg_dtl6, FINAL.vendor_item_category,
                      FINAL.vendor_category, FINAL.pre_mark_ind,
                      FINAL.settlement_code,
                      CASE
                         WHEN FINAL.registration_number IS NOT NULL
                         AND LENGTH (FINAL.registration_number) = '15'
                            THEN '61'
                         ELSE '62'
                      END tax_payer_type,
                      FINAL.principal_vendor, FINAL.parent_vendor
                 FROM (SELECT DISTINCT bb.vendor_id, bb.vendor_number,
                                       bb.action_code, bb.tax_reg_dtl1,
                                       gstin.registration_number,
                                       bb.tax_reg_dtl4, bb.tax_reg_dtl5,
                                       bb.tax_reg_dtl6,
                                       bb.vendor_item_category,
                                       bb.vendor_category, bb.pre_mark_ind,
                                       bb.settlement_code,
                                       bb.principal_vendor, bb.parent_vendor
                                  FROM (SELECT DISTINCT aa.vendor_id,
                                                        aa.vendor_number,
                                                        aa.tax_reg_dtl1,
                                                        aa.vendor_site_id,
                                                        aa.tax_reg_dtl4,
                                                        aa.tax_reg_dtl5,
                                                        aa.tax_reg_dtl6,
                                                        aa.vendor_item_category,
                                                        aa.vendor_category,
                                                        aa.pre_mark_ind,
                                                        aa.action_code,
                                                        aa.settlement_code,
                                                        aa.principal_vendor,
                                                        aa.parent_vendor
                                                   FROM (SELECT DISTINCT pv.vendor_id,
                                                                         pvsa.attribute14
                                                                            vendor_number,
                                                                         pvsa.vendor_site_id,
                                                                         (CASE
                                                                             WHEN TRUNC
                                                                                    (pv.creation_date
                                                                                    )
                                                                                    BETWEEN TRUNC
                                                                                              (p_run_date
                                                                                              )
                                                                                        AND TRUNC
                                                                                              (SYSDATE
                                                                                              )
                                                                                THEN 'A'
                                                                             ELSE 'M'
                                                                          END
                                                                         )
                                                                            action_code,
                                                                         pv.segment1
                                                                            tax_reg_dtl1,
                                                                         jcv.vat_reg_no
                                                                            tax_reg_dtl2,
                                                                         jcv.cst_reg_no
                                                                            tax_reg_dtl3,
                                                                         hzp.attribute3
                                                                            tax_reg_dtl4,
                                                                         ''
                                                                            tax_reg_dtl5,
--                                                                         (SELECT MAX
--                                                                                    (pan_no
--                                                                                    )
--                                                                            FROM apps.jai_ap_tds_vendor_hdrs
--                                                                           WHERE vendor_id =
--                                                                                    pv.vendor_id)
                nvl((SELECT DISTINCT jprl.registration_number
           FROM apps.jai_party_regs jpr,
                apps.jai_party_reg_lines jprl
          WHERE 1 = 1
            AND jpr.party_reg_id = jprl.party_reg_id
            AND jpr.party_type_code = 'THIRD_PARTY_SITE'
            AND jpr.party_id = pv.vendor_id
            AND rownum<=1
            AND jprl.registration_type_code ='PAN'                  
            AND jprl.effective_to IS NULL),
            (SELECT DISTINCT jprl.SECONDARY_REGISTRATION_NUMBER
           FROM apps.jai_party_regs jpr,
                apps.jai_party_reg_lines jprl
          WHERE 1 = 1
            AND jpr.party_reg_id = jprl.party_reg_id
            AND jpr.party_type_code = 'THIRD_PARTY_SITE'
            AND jpr.party_id = pv.vendor_id
            AND rownum<=1
            AND jprl.SEC_REGISTRATION_TYPE_CODE ='PAN'                    
   AND jprl.effective_to IS NULL)) 
                                                                            tax_reg_dtl6,
                                                                         'Item Category - NOT APPLICABLE'
                                                                            -- ||pv.global_attribute1
                                                                         vendor_item_category,
                                                                         'Vendor Category -MERCHANDISE'
                                                                            -- ||pv.vendor_type_lookup_code
                                                                         vendor_category,
                                                                         DECODE
                                                                            (pv.vendor_type_lookup_code,
                                                                             'INTERNAL VENDORS', 'Y',
                                                                             'N'
                                                                            )
                                                                            pre_mark_ind,
                                                                         DECODE
                                                                            (pv.vendor_type_lookup_code,
                                                                             'INTERNAL VENDORS', 'E',
                                                                             'N'
                                                                            )
                                                                            settlement_code,
                                                                         hzp.attribute2
                                                                            tax_payer_type,
                                                                         NVL
                                                                            ((SELECT DISTINCT    (pv1.attribute1
                                                                                                 )
                                                                                              || ':'
                                                                                              || (SELECT segment1
                                                                                                    FROM apps.po_vendors
                                                                                                   WHERE vendor_name =
                                                                                                            pv1.attribute1)
                                                                                         FROM apps.po_vendors pv1
                                                                                        WHERE pv1.vendor_id =
                                                                                                 pv.vendor_id
                                                                                          AND pv1.global_attribute10 =
                                                                                                 'Y'),
                                                                                (pv.segment1
                                                                                )
                                                                             || ':'
                                                                             || (SELECT vendor_name
                                                                                   FROM apps.po_vendors
                                                                                  WHERE segment1 =
                                                                                           pv.segment1)
                                                                            )
                                                                            principal_vendor
                                                                                            --Changed on 28-Feb-2013
                                                                         ,
                                                                         (   pv.segment1
                                                                          || ':'
                                                                          || (SELECT vendor_name
                                                                                FROM apps.po_vendors
                                                                               WHERE segment1 =
                                                                                        pv.segment1)
                                                                         )
                                                                            parent_vendor
                                                                    --Changed on 28-Feb-2013
                                                         FROM            apps.po_vendors pv,
                                                                         apps.po_vendor_sites_all pvsa,
                                                                         apps.jai_cmn_vendor_sites jcv,
                                                                         apps.hz_party_sites hzp,
                                                                         apps.jai_ap_tds_vendor_hdrs hdr,
                                                                         (SELECT DISTINCT jpr.party_id,
                                                                                          jprl.last_update_date
                                                                                     FROM apps.jai_party_regs jpr,
                                                                                          apps.jai_party_reg_lines jprl,
                                                                                          apps.ap_suppliers asp,
                                                                                          apps.ap_supplier_sites_all assa
                                                                                    WHERE 1 =
                                                                                             1
                                                                                      AND jpr.party_reg_id =
                                                                                             jprl.party_reg_id
                                                                                      --AND jpr.party_id = 15355388
                                                                                      AND registration_type_code =
                                                                                             'GSTIN'
                                                                                      AND asp.vendor_id =
                                                                                             assa.vendor_id
                                                                                      AND jprl.effective_to IS NULL
                                                                                      AND asp.vendor_id =
                                                                                             jpr.party_id
                                                                                      AND jpr.party_site_id =
                                                                                             assa.vendor_site_id
                                                                                      AND jpr.org_id =
                                                                                             assa.org_id
                                                                                      AND (   TRUNC
                                                                                                 (asp.last_update_date
                                                                                                 ) >=
                                                                                                 TRUNC
                                                                                                    (p_run_date
                                                                                                    )
                                                                                           OR TRUNC
                                                                                                 (assa.last_update_date
                                                                                                 ) >=
                                                                                                 TRUNC
                                                                                                    (p_run_date
                                                                                                    )
                                                                                           OR (jprl.last_update_date
                                                                                              ) >=
                                                                                                 TRUNC
                                                                                                    (p_run_date
                                                                                                    )
                                                                                          )) site,
                                                                         (SELECT NAME,
                                                                                 organization_id
                                                                            FROM apps.hr_operating_units) hr
                                                                   WHERE 1 = 1
                                                                     AND jcv.vendor_id(+) =
                                                                            pvsa.vendor_id
                                                                     AND jcv.vendor_site_id(+) =
                                                                            pvsa.vendor_site_id
                                                                     AND pv.vendor_id =
                                                                            pvsa.vendor_id
                                                                     AND pv.vendor_id =
                                                                            site.party_id(+)
--                                                      AND pv.segment1 =
--                                                                     '7010307'
--                                                               AND pvsa.attribute14 IN
--                                                                      ('2006937',
--                                                                       '2005136')
                                                                     AND hzp.party_id =
                                                                            pv.party_id
                                                                     AND hzp.location_id =
                                                                            pvsa.location_id
                                                                     AND hr.organization_id =
                                                                            pvsa.org_id
                                                                     AND hdr.vendor_site_id(+) =
                                                                            pvsa.vendor_site_id
                                                                     AND hdr.vendor_id(+) =
                                                                            pvsa.vendor_id
                                                                     AND pvsa.org_id NOT IN
                                                                            (147,
                                                                             801,
                                                                             861,
                                                                             941,
                                                                             841,
                                                                             901,
                                                                             961,
                                                                             821)
                                                                     AND SUBSTR
                                                                            (pv.segment1,
                                                                             1,
                                                                             1
                                                                            ) =
                                                                            '7'
                                                                     AND SUBSTR
                                                                            (pvsa.attribute14,
                                                                             1,
                                                                             1
                                                                            ) =
                                                                            '2'
--                                                               AND pv.segment1 IN
--                                                                      ('7504130',
--                                                                       '7504327',
--                                                                       '7500613',
--                                                                       '7503705',
--                                                                       '7001285')
                                                                     AND (   TRUNC
                                                                                (pv.last_update_date
                                                                                ) >=
                                                                                TRUNC
                                                                                   (p_run_date
                                                                                   )
                                                                          OR TRUNC
                                                                                (pvsa.last_update_date
                                                                                ) >=
                                                                                TRUNC
                                                                                   (p_run_date
                                                                                   )
                                                                          OR TRUNC
                                                                                (site.last_update_date
                                                                                ) >=
                                                                                TRUNC
                                                                                   (p_run_date
                                                                                   )
                                                                         )) aa) bb,
                                       (SELECT DISTINCT asp.vendor_id,
                                                        jprl.registration_number,
                                                        assa.attribute14
                                                   FROM apps.jai_party_regs jpr,
                                                        apps.jai_party_reg_lines jprl,
                                                        apps.ap_suppliers asp,
                                                        apps.ap_supplier_sites_all assa
                                                  WHERE 1 = 1
                                                    AND jpr.party_reg_id =
                                                             jprl.party_reg_id
                                                    AND jpr.party_id =
                                                                 asp.vendor_id
                                                    AND jpr.party_site_id =
                                                           assa.vendor_site_id
                                                    AND assa.attribute14 IN (
                                                           SELECT DISTINCT assa.attribute14
                                                                      FROM apps.ap_suppliers asp,
                                                                           apps.ap_supplier_sites_all assa,
                                                                           apps.jai_party_regs jpr,
                                                                           apps.jai_party_reg_lines jprl
                                                                     WHERE 1 =
                                                                              1
                                                                       AND asp.vendor_id =
                                                                              assa.vendor_id
                                                                       AND jprl.effective_to IS NULL
                                                                       AND jpr.party_id =
                                                                              asp.vendor_id
                                                                       -- AND asp.segment1 = '7504130'
                                                                       AND jpr.party_reg_id =
                                                                              jprl.party_reg_id
                                                                       AND (   TRUNC
                                                                                  (asp.last_update_date
                                                                                  ) >=
                                                                                  TRUNC
                                                                                     (p_run_date
                                                                                     )
                                                                            OR TRUNC
                                                                                  (assa.last_update_date
                                                                                  ) >=
                                                                                  TRUNC
                                                                                     (p_run_date
                                                                                     )
                                                                            OR (jprl.last_update_date
                                                                               ) >=
                                                                                  TRUNC
                                                                                     (p_run_date
                                                                                     )
                                                                           )
                                                                       AND jprl.registration_type_code =
                                                                              'GSTIN'
                                                                                     -- AND asp.segment1 = '7504124'
                                                        )
                                                    AND jprl.effective_to IS NULL
                                                    AND jprl.registration_type_code =
                                                                       'GSTIN') gstin
                                 WHERE bb.vendor_id = gstin.vendor_id(+)
                                   AND bb.vendor_number = gstin.attribute14(+)
                              GROUP BY bb.vendor_id,
                                       vendor_number,
                                       action_code,
                                       tax_reg_dtl1,
                                       gstin.registration_number,
                --  vendor_site_id,
--                tax_reg_dtl2,
--                tax_reg_dtl3,
                                       tax_reg_dtl4,
                                       tax_reg_dtl5,
                                       tax_reg_dtl6,
                                       vendor_item_category,
                                       vendor_category,
                                       pre_mark_ind,
                                       settlement_code,
--                tax_payer_type,
                                       principal_vendor,
                                       parent_vendor) FINAL
                WHERE 1 = 1;
BEGIN
   FOR xx_main IN supplier_add_info
   LOOP
      fnd_file.put_line (fnd_file.LOG, 'Vendor_no' || xx_main.vendor_number);
      v_file :=
         UTL_FILE.fopen ('UNIQUE_VENDOR',
                            'RSupplierB_'
                         || xx_main.vendor_number
                         || '_additional_'
                         || g_conc_request_id
                         || '.xml',
                         'w'
                        );
      UTL_FILE.put_line (v_file, '<?xml version="1.0" encoding="utf-8" ?>');
      UTL_FILE.put_line (v_file, '<Vendor>');
      UTL_FILE.put_line (v_file, '<VendorDesc>');
      UTL_FILE.put_line (v_file,
                         '<supplier>' || xx_main.vendor_number
                         || '</supplier>'
                        );
      UTL_FILE.put_line (v_file,
                            '<action_code>'
                         || xx_main.action_code
                         || '</action_code>'
                        );
      UTL_FILE.put_line (v_file,
                            '<tax_reg_dtl1>'
                         || xx_main.tax_reg_dtl1
                         || '</tax_reg_dtl1>'
                        );
      UTL_FILE.put_line (v_file,
                            '<tax_reg_dtl2>'
                         || xx_main.tax_reg_dtl2
                         || '</tax_reg_dtl2>'
                        );
      UTL_FILE.put_line (v_file,
                            '<tax_reg_dtl3>'
                         || xx_main.tax_reg_dtl3
                         || '</tax_reg_dtl3>'
                        );
      UTL_FILE.put_line (v_file,
                            '<tax_reg_dtl4>'
                         || xx_main.tax_reg_dtl4
                         || '</tax_reg_dtl4>'
                        );
      UTL_FILE.put_line (v_file,
                            '<tax_reg_dtl5>'
                         || xx_main.tax_reg_dtl2
                         || '</tax_reg_dtl5>'
                        );
      UTL_FILE.put_line (v_file,
                            '<tax_reg_dtl6>'
                         || xx_main.tax_reg_dtl6
                         || '</tax_reg_dtl6>'
                        );
      UTL_FILE.put_line (v_file,
                            '<vendor_item_category>'
                         || xx_main.vendor_item_category
                         || '</vendor_item_category>'
                        );
      UTL_FILE.put_line (v_file,
                            '<vendor_category>'
                         || xx_main.vendor_category
                         || '</vendor_category>'
                        );
      UTL_FILE.put_line (v_file,
                            '<pre_mark_ind>'
                         || xx_main.pre_mark_ind
                         || '</pre_mark_ind>'
                        );
      UTL_FILE.put_line (v_file,
                            '<settlement_code>'
                         || xx_main.settlement_code
                         || '</settlement_code>'
                        );
      UTL_FILE.put_line (v_file,
                            '<taxpayer_type>'
                         || xx_main.tax_payer_type
                         || '</taxpayer_type>'
                        );
      UTL_FILE.put_line
                     (v_file,
                         '<principal_vendor>'
                      || xxabrl_supplier_specialchars
                                                     (xx_main.principal_vendor)
                      || '</principal_vendor>'
                     );
      UTL_FILE.put_line (v_file,
                            '<parent_vendor>'
                         || xxabrl_supplier_specialchars
                                                        (xx_main.parent_vendor)
                         || '</parent_vendor>'
                        );
      UTL_FILE.put_line (v_file, '</VendorDesc>');
      UTL_FILE.put_line (v_file, '</Vendor>');
      UTL_FILE.fclose (v_file);
   END LOOP;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      UTL_FILE.fclose (v_file);
      fnd_file.put_line (fnd_file.LOG, 'No Data Found');
   WHEN UTL_FILE.invalid_path
   THEN
      UTL_FILE.fclose (v_file);
      fnd_file.put_line (fnd_file.LOG, 'Invalid Path');
   WHEN UTL_FILE.invalid_filehandle
   THEN
      UTL_FILE.fclose (v_file);
      fnd_file.put_line (fnd_file.LOG, 'Invalid File Handle');
   WHEN UTL_FILE.invalid_operation
   THEN
      UTL_FILE.fclose (v_file);
      fnd_file.put_line (fnd_file.LOG, 'Invalid Operation');
   WHEN UTL_FILE.read_error
   THEN
      UTL_FILE.fclose (v_file);
      fnd_file.put_line (fnd_file.LOG, 'Read Error');
   WHEN UTL_FILE.write_error
   THEN
      UTL_FILE.fclose (v_file);
      fnd_file.put_line (fnd_file.LOG, 'Write Error');
   WHEN UTL_FILE.internal_error
   THEN
      UTL_FILE.fclose (v_file);
      fnd_file.put_line (fnd_file.LOG, 'Internal Error');
   WHEN UTL_FILE.file_open
   THEN
      UTL_FILE.fclose (v_file);
      fnd_file.put_line (fnd_file.LOG, 'File is Open');
   WHEN UTL_FILE.invalid_filename
   THEN
      UTL_FILE.fclose (v_file);
      fnd_file.put_line (fnd_file.LOG, 'Invalid File Name');
   WHEN OTHERS
   THEN
      UTL_FILE.fclose (v_file);
      fnd_file.put_line (fnd_file.LOG, 'Unknown Error' || SQLERRM);
      UTL_FILE.fclose (v_file);
END gst_abrl_supplier_add_info_xml; 
/


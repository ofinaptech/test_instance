CREATE OR REPLACE PACKAGE APPS.XXABRL_SUPP_BANK_PMT_SCB AS
  /**********************************************************************************************************************************************
                        WIPRO Infotech Ltd, Mumbai, India
            Name        : Supplier Bank Outbound interface for SCB Bank [H2H Interface betwen ABRL and SCB Bank]
            Change Record:
           =========================================================================================================================
           Version   Date          Author               Remarks                  Documnet Ref
           =======   ==========   =============        ============================================================================
           1.0.0     13-Jun-2013    Dhiresh More  Initial Version
  ************************************************************************************************************************************************/

PROCEDURE XXABRL_SCBANK_PAYMENT_SUPP ( errbuf     out VARCHAR2
                                  ,      retcode    out VARCHAR2
                                  ,      P_FROM_DATE  VARCHAR2
                                  ,      P_TO_DATE   VARCHAR2   );

--This Procedure will Process all Valid Payments into SCB_PAYMENT_TABLE

PROCEDURE XXABRL_UPDATE_SCB_INFO (  errbuf     out VARCHAR2
                                  ,  retcode    out VARCHAR2);
/*This Program will Update (AP_CHECKS_ALL) the DFF in Payment Screen(SCB Bank Payment Information),
   Once the SCB Bank will Provide
*/

PROCEDURE XXABRL_SCB_USER_RECTIFIED_PAY( errbuf     out VARCHAR2
                                  ,  retcode    out VARCHAR2
                                  ,  p_from_check_id NUMBER
                                  ,  p_to_check_id NUMBER );

/*  This Program will Update the SCB_PAYMENT_TABLE ,
    with all Corrected data by User as well as Transaction Status as
*/

PROCEDURE XXABRL_SCD_SCB_PAYMENT_SUPP( errbuf     out VARCHAR2
                                         ,  retcode    out VARCHAR2);
/*  This Procedure will Process all Valid Payments into SCB_PAYMENT_TABLE thru Schdule Program
*/
END; 
/


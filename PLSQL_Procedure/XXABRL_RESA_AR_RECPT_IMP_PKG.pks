CREATE OR REPLACE Package APPS.XXABRL_RESA_AR_RECPT_IMP_PKG IS
  Procedure RECEIPT_VALIDATE(p_errbuf      OUT VARCHAR2,
                             p_retcode     OUT NUMBER,
                             p_action IN VARCHAR2,
                             P_Data_Source IN VARCHAR2,
                             p_receipt_to_apply IN varchar2,
                             p_apply_all_flag IN varchar2,
                             p_org_id in number
                             );
  PROCEDURE RECEIPT_INSERT(P_Org_Id IN NUMBER, P_Data_Source IN VARCHAR2);
  PROCEDURE RECEIPT_APPLY_CALL(P_Ref_Number IN VARCHAR2);
  PROCEDURE RECEIPT_APPLY_DATA(
                               ARR_R2 IN XXABRL_RESA_AR_RECEIPT_INT%ROWTYPE
                               ,p_Rcpt_Amt_To_Apply Number
                               ,x_Rcpt_Bal_Amt OUT Number
                               ,x_exit_flag OUT varchar2
                               ,x_org_id in number                               
                               );
END XXABRL_RESA_AR_RECPT_IMP_PKG; 
/


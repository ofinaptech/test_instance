CREATE OR REPLACE PACKAGE BODY APPS.gst_abrl_pur_rec_pkg
AS
   PROCEDURE gst_abrl_main_pkg (
      errbuff          OUT      VARCHAR2,
      retcode          OUT      NUMBER,
      p_type           IN       VARCHAR2,
      p_gl_from_date   IN       VARCHAR2,
      p_gl_to_date     IN       VARCHAR2,
      p_gl_acct_from   IN       VARCHAR2,
      p_gl_acct_to     IN       VARCHAR2
   )
   AS
   BEGIN
      fnd_file.put_line (fnd_file.LOG,
                            p_type
                         || ' <-> '
                         || p_gl_from_date
                         || ' <-> '
                         || p_gl_to_date
                        );

      IF p_type = 'F'
      THEN
         xxabrl_financial_proc (p_gl_from_date,
                                p_gl_to_date,
                                p_gl_acct_from,
                                p_gl_acct_to
                               );
      END IF;

      IF p_type = 'C'
      THEN
         xxabrl_costmanagement_proc (p_gl_from_date,
                                     p_gl_to_date,
                                     p_gl_acct_from,
                                     p_gl_acct_to
                                    );
      END IF;
   END gst_abrl_main_pkg;

   -- Procedure for Cost Management Entries
   PROCEDURE xxabrl_costmanagement_proc (
      p_gl_from_date   IN   VARCHAR2,
      p_gl_to_date     IN   VARCHAR2,
      p_gl_acct_from   IN   VARCHAR2,
      p_gl_acct_to     IN   VARCHAR2
   )
   AS
      CURSOR cur_rec
      IS
         SELECT   GLB.NAME batch_name, glh.je_source SOURCE,
                  glh.je_category CATEGORY,
                  glh.default_effective_date gl_date,
                  gll.je_line_num line_number,
                  gl.concatenated_segments account_code,
                  (SELECT ffvl.description
                     FROM apps.fnd_flex_values_vl ffvl
                    WHERE ffvl.flex_value_set_id = 1013466
                      AND ffvl.flex_value = gl.segment3) sbu_desc,
                  (SELECT ffvl.description
                     FROM apps.fnd_flex_values_vl ffvl
                    WHERE ffvl.flex_value_set_id = 1013467
                      AND ffvl.flex_value = gl.segment4) location_desc,
                  (SELECT ffvl.description
                     FROM apps.fnd_flex_values_vl ffvl
                    WHERE ffvl.flex_value_set_id = 1013469
                      AND ffvl.flex_value = gl.segment6) account_desc,
                  gll.accounted_dr dr_amount, gll.accounted_cr cr_amount,
                  gll.description journal_description, NULL customer_name,
                  NULL customer_number,
                  glh.doc_sequence_value document_number,
                  DECODE (GLB.status,
                          'P', 'Posted',
                          'U', 'Unposted',
                          GLB.status
                         ) batch_status,
                  fu.user_name created_by, gl.segment3 sbu,
                  gl.segment4 LOCATION, gl.segment6 gl_account,
                  glh.ledger_id ledger_id, poh.segment1 po_number,
                  pv.segment1 vendor_number, pvs.vendor_site_code,
                  rsh.receipt_num grn_no,
                  rt.po_unit_price * rt.primary_quantity amount,
                  xah.gl_transfer_status_code
             FROM apps.gl_je_headers glh,
                  apps.gl_je_lines gll,
                  apps.gl_code_combinations_kfv gl,
                  apps.fnd_user fu,
                  apps.gl_je_batches GLB,
                  apps.xla_ae_lines xla,
                  apps.gl_import_references glir,
                  apps.xla_ae_headers xah,
                  xla.xla_transaction_entities xte,
                  apps.rcv_transactions rt,
                  apps.po_headers_all poh,
                  apps.po_vendors pv,
                  apps.po_vendor_sites_all pvs,
                  apps.rcv_shipment_headers rsh
            -- ,  apps.rcv_shipment_lines rsl
         WHERE    glh.je_header_id = gll.je_header_id
              AND gl.code_combination_id = gll.code_combination_id
              AND glh.je_batch_id = GLB.je_batch_id
              AND gll.created_by = fu.user_id
              AND UPPER (glh.je_source) IN ('COST MANAGEMENT')
              AND gll.je_header_id = glir.je_header_id(+)
              AND gll.je_line_num = glir.je_line_num(+)
              AND glir.gl_sl_link_id = xla.gl_sl_link_id(+)
              AND glir.gl_sl_link_table = xla.gl_sl_link_table(+)
              AND NVL (xla.accounted_cr, 999999) != 0
              AND NVL (xla.accounted_dr, 999999) != 0
              AND xah.ae_header_id = xla.ae_header_id
              AND xte.entity_id = xah.entity_id
              --AND glh.je_category = 'Receiving'
              AND poh.po_header_id = rt.po_header_id
              AND xte.source_id_int_1 = TO_NUMBER (rt.transaction_id)
              AND rt.vendor_id = pv.vendor_id
              AND rt.vendor_site_id = pvs.vendor_site_id
              AND rsh.shipment_header_id = rt.shipment_header_id
              AND glh.default_effective_date
                     BETWEEN NVL (TO_DATE (p_gl_from_date,
                                           'yyyy/mm/dd hh24:mi:ss'
                                          ),
                                  glh.default_effective_date
                                 )
                         AND NVL (TO_DATE (p_gl_to_date,
                                           'yyyy/mm/dd hh24:mi:ss'
                                          ),
                                  glh.default_effective_date
                                 )
              AND gl.segment6 BETWEEN NVL (p_gl_acct_from, gl.segment6)
                                  AND NVL (p_gl_acct_to, gl.segment6)
         ORDER BY gll.je_line_num;
   BEGIN
      fnd_file.put_line (fnd_file.output,
                            'BATCH_NAME'
                         || '|'
                         || 'SOURCE'
                         || '|'
                         || 'CATEGORY'
                         || '|'
                         || 'GL DATE'
                         || '|'
                         || 'LINE NUMBER'
                         || '|'
                         || 'ACCOUNT CODE'
                         || '|'
                         || 'SBU DESC'
                         || '|'
                         || 'LOCATION DESC'
                         || '|'
                         || 'ACCOUNT DESC'
                         || '|'
                         || 'DR AMOUNT'
                         || '|'
                         || 'CR AMOUNT'
                         || '|'
                         || 'JOURNAL DESCRIPTION'
                         || '|'
                         || 'DOCUMENT NUMBER'
                         || '|'
                         || 'BATCH STATUS'
                         || '|'
                         || 'CREATED BY'
                         || '|'
                         || 'SBU'
                         || '|'
                         || 'LOCATION'
                         || '|'
                         || 'GL ACCOUNT'
                         || '|'
                         || 'LEDGER ID'
                         || '|'
                         || 'PO NUMBER'
                         || '|'
                         || 'VENDOR NUMBER'
                         || '|'
                         || 'VENDOR SITE CODE'
                         || '|'
                         || 'GRN NO'
                         || '|'
                         || 'AMOUNT'
                         || '|'
                         || 'GL TRANSFER STATUS CODE'
                        );

      FOR i IN cur_rec
      LOOP
         fnd_file.put_line (fnd_file.output,
                               i.batch_name
                            || '|'
                            || i.SOURCE
                            || '|'
                            || i.CATEGORY
                            || '|'
                            || i.gl_date
                            || '|'
                            || i.line_number
                            || '|'
                            || i.account_code
                            || '|'
                            || i.sbu_desc
                            || '|'
                            || i.location_desc
                            || '|'
                            || i.account_desc
                            || '|'
                            || i.dr_amount
                            || '|'
                            || i.cr_amount
                            || '|'
                            || i.journal_description
                            || '|'
                            || i.document_number
                            || '|'
                            || i.batch_status
                            || '|'
                            || i.created_by
                            || '|'
                            || i.sbu
                            || '|'
                            || i.LOCATION
                            || '|'
                            || i.gl_account
                            || '|'
                            || i.ledger_id
                            || '|'
                            || i.po_number
                            || '|'
                            || i.vendor_number
                            || '|'
                            || i.vendor_site_code
                            || '|'
                            || i.grn_no
                            || '|'
                            || i.amount
                            || '|'
                            || i.gl_transfer_status_code
                           );
      END LOOP;
   END xxabrl_costmanagement_proc;

   -- procedure for Financial India
   PROCEDURE xxabrl_financial_proc (
      p_gl_from_date   IN   VARCHAR2,
      p_gl_to_date     IN   VARCHAR2,
      p_gl_acct_from   IN   VARCHAR2,
      p_gl_acct_to     IN   VARCHAR2
   )
   AS
      CURSOR cur_pur
      IS
         SELECT DISTINCT gjb.NAME batch_name, gjh.je_source SOURCE,
                         gjh.je_category CATEGORY,
                         gjh.default_effective_date gl_date,
                         gjl.je_line_num line_number,
                         gcc.concatenated_segments account_code,
                         (SELECT ffvl.description
                            FROM apps.fnd_flex_values_vl ffvl
                           WHERE ffvl.flex_value_set_id = 1013466
                             AND ffvl.flex_value = gcc.segment3) sbu_desc,
                         (SELECT ffvl.description
                            FROM apps.fnd_flex_values_vl ffvl
                           WHERE ffvl.flex_value_set_id =
                                                        1013467
                             AND ffvl.flex_value = gcc.segment4)
                                                               location_desc,
                         (SELECT ffvl.description
                            FROM apps.fnd_flex_values_vl ffvl
                           WHERE ffvl.flex_value_set_id =
                                                         1013469
                             AND ffvl.flex_value = gcc.segment6)
                                                                account_desc,
                         gjl.entered_dr dr_amount, gjl.entered_cr cr_amount,
                         gjl.description journal_description,
                         asp.segment1 customer_code,
                         asp.vendor_name customer_name,
                         gjh.doc_sequence_value document_number,
                         DECODE (gjb.status,
                                 'P', 'Posted',
                                 'Unposted'
                                ) batch_status,
                         fu.user_name created_by, gcc.segment3 sbu,
                         gcc.segment4 LOCATION, gcc.segment6 gl_account,
                         gjh.ledger_id, pha.segment1 po_number,
                         rt.quantity * rt.po_unit_price amount,
                         asp.segment1 vendor_number, assa.vendor_site_code,
                         rsh.receipt_num grn_no
                    FROM apps.gl_je_headers gjh,
                         apps.gl_je_lines gjl,
                         apps.gl_import_references gir,
                         apps.jai_tax_journal_entries jai,
                         apps.jai_tax_lines jtl,
                         apps.rcv_shipment_headers rsh,
                         apps.rcv_transactions rt,
                         apps.gl_je_batches gjb,
                         apps.ap_suppliers asp,
                         apps.ap_supplier_sites_all assa,
                         apps.po_headers_all pha,
                         apps.gl_code_combinations_kfv gcc,
                         apps.fnd_user fu
                   WHERE 1 = 1
                     AND gjh.je_source = 'Financials India'
--            AND gjh.je_header_id = 108943790
                     --AND gjh.je_category = 'PO Receiving'
                     AND gjh.je_header_id = gjl.je_header_id
                     AND gjh.je_header_id = gir.je_header_id
                     AND gjh.je_batch_id = gjb.je_batch_id
                     AND gjl.je_line_num = gir.je_line_num
                     AND gjl.reference_3 = jai.tax_det_factor_id
                     AND gjl.reference_4 = jai.tax_line_id
                     AND gjl.reference_5 = jai.tax_dist_id
                     AND jai.tax_line_id = jtl.tax_line_id
                     AND jtl.trx_loc_line_id = rt.transaction_id
                     AND jai.tax_det_factor_id = jtl.det_factor_id
                     AND rsh.shipment_header_id = jtl.trx_id
                     AND rt.shipment_header_id = rsh.shipment_header_id
                     --AND rsh.receipt_num = '9230003285'
                     AND gjl.code_combination_id = gcc.code_combination_id
                     AND rt.po_header_id = pha.po_header_id
                     AND pha.vendor_id = asp.vendor_id
                     AND pha.vendor_site_id = assa.vendor_site_id
                     AND gjh.created_by = fu.user_id
                     AND gjh.default_effective_date
                            BETWEEN NVL (TO_DATE (p_gl_from_date,
                                                  'yyyy/mm/dd hh24:mi:ss'
                                                 ),
                                         gjh.default_effective_date
                                        )
                                AND NVL (TO_DATE (p_gl_to_date,
                                                  'yyyy/mm/dd hh24:mi:ss'
                                                 ),
                                         gjh.default_effective_date
                                        )
                     AND gcc.segment6 BETWEEN NVL (p_gl_acct_from,
                                                   gcc.segment6
                                                  )
                                          AND NVL (p_gl_acct_to, gcc.segment6)
                ORDER BY gjl.je_line_num;
   BEGIN
      fnd_file.put_line (fnd_file.output,
                            'BATCH_NAME'
                         || '|'
                         || 'SOURCE'
                         || '|'
                         || 'CATEGORY'
                         || '|'
                         || 'GL DATE'
                         || '|'
                         || 'LINE NUMBER'
                         || '|'
                         || 'ACCOUNT CODE'
                         || '|'
                         || 'SBU DESC'
                         || '|'
                         || 'LOCATION DESC'
                         || '|'
                         || 'ACCOUNT DESC'
                         || '|'
                         || 'DR AMOUNT'
                         || '|'
                         || 'CR AMOUNT'
                         || '|'
                         || 'JOURNAL DESCRIPTION'
                         || '|'
                         || 'DOCUMENT NUMBER'
                         || '|'
                         || 'BATCH STATUS'
                         || '|'
                         || 'CREATED BY'
                         || '|'
                         || 'SBU'
                         || '|'
                         || 'LOCATION'
                         || '|'
                         || 'GL ACCOUNT'
                         || '|'
                         || 'LEDGER ID'
                         || '|'
                         || 'PO NUMBER'
                         || '|'
                         || 'AMOUNT'
                         || '|'
                         || 'VENDOR NUMBER'
                         || '|'
                         || 'VENDOR SITE CODE'
                         || '|'
                         || 'GRN NO'
                        );

      FOR i IN cur_pur
      LOOP
         fnd_file.put_line (fnd_file.output,
                               i.batch_name
                            || '|'
                            || i.SOURCE
                            || '|'
                            || i.CATEGORY
                            || '|'
                            || i.gl_date
                            || '|'
                            || i.line_number
                            || '|'
                            || i.account_code
                            || '|'
                            || i.sbu_desc
                            || '|'
                            || i.location_desc
                            || '|'
                            || i.account_desc
                            || '|'
                            || i.dr_amount
                            || '|'
                            || i.cr_amount
                            || '|'
                            || i.journal_description
                            || '|'
                            || i.document_number
                            || '|'
                            || i.batch_status
                            || '|'
                            || i.created_by
                            || '|'
                            || i.sbu
                            || '|'
                            || i.LOCATION
                            || '|'
                            || i.gl_account
                            || '|'
                            || i.ledger_id
                            || '|'
                            || i.po_number
                            || '|'
                            || i.amount
                            || '|'
                            || i.vendor_number
                            || '|'
                            || i.vendor_site_code
                            || '|'
                            || i.grn_no
                           );
      END LOOP;
   END xxabrl_financial_proc;
END gst_abrl_pur_rec_pkg; 
/


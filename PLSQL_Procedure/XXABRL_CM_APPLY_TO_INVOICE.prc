CREATE OR REPLACE PROCEDURE APPS.XXABRL_CM_APPLY_TO_INVOICE (
   x_err_buf    OUT      VARCHAR2,
   x_ret_code   OUT      NUMBER,
   p_org_id     IN       NUMBER
)
AS
   v_return_status            VARCHAR2 (1);
   p_count                    NUMBER;
   v_msg_count                NUMBER;
   v_msg_data                 VARCHAR2 (4000);
   v_cm_unapp_rec             ar_cm_api_pub.cm_app_rec_type;
   v_cm_id                    NUMBER                        := 0;
   v_inv_id                   NUMBER                        := 0;
   v_gl_date                  DATE                          := SYSDATE;
   v_context                  VARCHAR2 (2);
   v_out_rec_application_id   NUMBER                        := 0;
   v_amount_applied_from      NUMBER                        := 0;
   v_amount_applied_to        NUMBER                        := 0;
   xx_inv_amt_bal             NUMBER                        := 0;
   xx_cm_amt_bal              NUMBER                        := 0;
   v_org_id                   NUMBER          := fnd_profile.VALUE ('ORG_ID');
   v_user_id                  NUMBER         := fnd_profile.VALUE ('USER_ID');
   v_resp_id                  NUMBER         := fnd_profile.VALUE ('RESP_ID');
   v_creation_sign            VARCHAR2 (10)                 := NULL;

   CURSOR xx_ref_no
   IS
      SELECT   interface_header_attribute1, org_id
          FROM xxabrl.xxabrl_cm_apply_to_inv_stg
         WHERE org_id = p_org_id
           AND NVL (status_flag, 'N') IN ('N', 'E')
         --  AND gl_date <= '30-Apr-16'
          --   AND short_code = 'SM KA OU'
      -- and trx_number in ('620100302949','620100311473' )
      GROUP BY interface_header_attribute1, org_id;

   CURSOR v_main (p_interface_header_attribute1 IN VARCHAR2)
   IS
      SELECT ROWID, s.*
        FROM xxabrl.xxabrl_cm_apply_to_inv_stg s
       WHERE interface_header_attribute1 = p_interface_header_attribute1
         AND NVL (status_flag, 'N') IN ('N', 'E');
BEGIN
   fnd_file.put_line (fnd_file.LOG, 'v_resp_id=>' || v_resp_id);

   BEGIN
      mo_global.init ('AR');
      fnd_global.apps_initialize (user_id           => v_user_id,
                                  resp_id           => v_resp_id,
                                  resp_appl_id      => 222
                                 );
      mo_global.set_policy_context ('S', v_org_id);
      COMMIT;
   END;

   FOR ref_no IN xx_ref_no
   LOOP
      FOR xx_main IN v_main (ref_no.interface_header_attribute1)
      LOOP
         v_return_status := NULL;
         v_out_rec_application_id := NULL;

         BEGIN
            SELECT   SUM (amount_due_remaining), rct.customer_trx_id
                INTO xx_inv_amt_bal, v_inv_id
                FROM apps.ra_customer_trx_all rct,
                     apps.ar_payment_schedules_all ps
               WHERE 1 = 1
                 AND rct.interface_header_attribute1 =
                                           xx_main.interface_header_attribute1
                 AND rct.customer_trx_id = ps.customer_trx_id
                 AND CLASS = 'INV'
                 AND NVL (amount_due_remaining, 0) <> 0
                 AND ROWNUM < 2
            GROUP BY rct.customer_trx_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               xx_inv_amt_bal := 0;
         END;

         BEGIN
            SELECT   SUM (amount_due_remaining), rct.customer_trx_id
                INTO xx_cm_amt_bal, v_cm_id
                FROM apps.ra_customer_trx_all rct,
                     apps.ar_payment_schedules_all ps
               WHERE 1 = 1
                 AND rct.interface_header_attribute1 =
                                           xx_main.interface_header_attribute1
                 AND rct.customer_trx_id = ps.customer_trx_id
                 AND CLASS = 'CM'
                 AND NVL (amount_due_remaining, 0) <> 0
                 AND rct.trx_number = xx_main.trx_number
            GROUP BY rct.customer_trx_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               xx_cm_amt_bal := 0;
         END;

--         IF NVL (xx_cm_amt_bal, 0) = 0 OR NVL (xx_inv_amt_bal, 0) = 0
--         THEN
--            UPDATE xxabrl.xxabrl_cm_apply_to_inv_stg s
--               SET error_msg =
--                         'CM amount_due_remaining=>'
--                      || xx_cm_amt_bal
--                      || ':'
--                      || 'INV amount_due_remaining=>'
--                      || xx_inv_amt_bal,
--                   status_flag = 'E'
--             WHERE s.ROWID = xx_main.ROWID AND trx_number = xx_main.trx_number;
         --ELSE
         v_cm_unapp_rec.cm_customer_trx_id := v_cm_id;
         v_cm_unapp_rec.inv_customer_trx_id := v_inv_id;
         v_cm_unapp_rec.gl_date := v_gl_date;

         IF xx_main.remaining < 0
         THEN
            v_cm_unapp_rec.amount_applied :=
                                            -1 * ROUND (xx_main.remaining, 2);
         ELSIF xx_main.remaining > 0
         THEN
            v_cm_unapp_rec.amount_applied := ROUND (xx_main.remaining, 2);
         END IF;

         IF UPPER (xx_main.trx_type) IN ('STORE FORFEITURE', 'SHORT/EXCESS')
         THEN
            v_cm_unapp_rec.amount_applied :=
                                            -1 * ROUND (xx_main.remaining, 2);
         END IF;

         SELECT creation_sign
           INTO v_creation_sign
           FROM apps.ra_cust_trx_types_all
          WHERE UPPER (NAME) = UPPER (xx_main.trx_type)
            AND org_id = xx_main.org_id;

         IF v_creation_sign = 'A' AND xx_main.remaining > 0
         THEN
            v_cm_unapp_rec.amount_applied :=
                                            -1 * ROUND (xx_main.remaining, 2);
         END IF;

         fnd_file.put_line (fnd_file.LOG,
                               'v_cm_unapp_rec.amount_applied=>'
                            || v_cm_unapp_rec.amount_applied
                           );
         /* Invoking credit memo unapplication api */
         ar_cm_api_pub.apply_on_account
                        (p_api_version                    => 1.0,
                         p_init_msg_list                  => fnd_api.g_false,
                         p_commit                         => fnd_api.g_false,
                         p_cm_app_rec                     => v_cm_unapp_rec,
                         x_return_status                  => v_return_status,
                         x_msg_count                      => v_msg_count,
                         x_msg_data                       => v_msg_data,
                         x_out_rec_application_id         => v_out_rec_application_id,
                         x_acctd_amount_applied_from      => v_amount_applied_from,
                         x_acctd_amount_applied_to        => v_amount_applied_to,
                         p_org_id                         => NULL
                        );
         fnd_file.put_line
             (fnd_file.LOG,
              '--------------------------------------------------------------'
             );
         fnd_file.put_line (fnd_file.LOG,
                            'TRX_NUMBER=>' || xx_main.trx_number);
         fnd_file.put_line (fnd_file.LOG,
                            'RETURN_STATUS=>' || v_return_status);
         fnd_file.put_line (fnd_file.LOG,
                            'REC_APPLICATION_ID=>' || v_out_rec_application_id
                           );
         fnd_file.put_line
             (fnd_file.LOG,
              '--------------------------------------------------------------'
             );

         IF UPPER (v_return_status) = 'S'
         THEN
            UPDATE xxabrl.xxabrl_cm_apply_to_inv_stg s
               SET error_msg = v_out_rec_application_id,
                   amt_apply_from = v_amount_applied_from,
                   amt_apply_to = v_amount_applied_to,
                   status_flag = 'A'
             WHERE ROWID = xx_main.ROWID;
         --AND trx_number = xx_main.trx_number;
         ELSE
            UPDATE xxabrl.xxabrl_cm_apply_to_inv_stg s
               SET error_msg = 'ERROR',
                   status_flag = 'E'
             WHERE ROWID = xx_main.ROWID;
         END IF;

         --DBMS_OUTPUT.put_line ('Message count ' || v_msg_count);
--         IF NVL (v_msg_count, 0) = 0
--         THEN
----            UPDATE xxabrl.xxabrl_cm_apply_to_inv_stg s
----               SET error_msg = NULL,
----                   amt_apply_from = v_amount_applied_from,
----                   amt_apply_to = v_amount_applied_to,
----                   status_flag = 'A'
--             WHERE ROWID = xx_main.ROWID AND trx_number = xx_main.trx_number;
         IF v_msg_count > 0
         THEN
            LOOP
               p_count := p_count + 1;
               v_msg_data :=                              --v_msg_data||':'||
                        fnd_msg_pub.get (fnd_msg_pub.g_next, fnd_api.g_false);

               IF v_msg_data IS NULL
               THEN
                  EXIT;
               END IF;

--                  INSERT INTO   xx_test
--                       VALUES (   v_msg_data
--                               || '=>'
--                               || 'v_cm_id=>'
--                               || v_cm_id
--                               || '=>'
--                               || 'v_inv_id=>'
--                               || v_inv_id);
               fnd_file.put_line (fnd_file.LOG,
                                     'Error Message=>'
                                  || xx_main.trx_number
                                  || '=>'
                                  || p_count
                                  || ' ---'
                                  || v_msg_data
                                 );
            END LOOP;

            fnd_file.put_line
               (fnd_file.LOG,
                '--------------------------------------------------------------'
               );
         END IF;
--         END IF;
      END LOOP;
   END LOOP;

   COMMIT;
END; 
/


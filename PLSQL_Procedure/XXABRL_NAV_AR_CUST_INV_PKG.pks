CREATE OR REPLACE PACKAGE APPS.XXABRL_NAV_AR_CUST_INV_PKG IS
  PROCEDURE CUST_INVOICE_VALIDATE(Errbuf         OUT VARCHAR2,
                             RetCode        OUT NUMBER,
                             /*P_Org_Id       In NUMBER,*/
                             P_BATCH_Source IN VARCHAR2,
                             p_action VARCHAR2,
                             P_GL_DATE VARCHAR2
                             );
  PROCEDURE CUST_INVOICE_INSERT(P_Org_Id IN NUMBER, P_BATCH_Source IN VARCHAR2,P_GL_DATE IN DATE ,x_ret_code OUT NUMBER);
  FUNCTION CUST_ACCOUNT_SEG_STATUS(P_Seg_Value IN VARCHAR2,
                              P_Seg_Desc  IN VARCHAR2) RETURN VARCHAR2;
END XXABRL_NAV_AR_CUST_INV_PKG; 
/


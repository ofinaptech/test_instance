CREATE OR REPLACE PROCEDURE APPS.ABRL_RMS_RMSTOGL_TAB_SCHEDULE 
 /**********************************************************************************************************************************************
                     WIPRO Infotech Ltd, Mumbai, India

   Name        : ABRL RMS to GL table refresh and backup from MV
  
  Change Record:
 =========================================================================================================================
  Version   Date          Author               Remarks                  Documnet Ref
 =======   ==========   =============        ============================================================================
  1.0.0    22-NOV-2010   Sandeep Sulakhe     Initial Version - 1.0.0    
/*******************************************************************************************************/
           (
  errbuf    OUT   VARCHAR2,
  retcode   OUT   VARCHAR2
)
IS
BEGIN
declare
v_cnt number :=0;
v_cnt1 number :=0;
v_cnt2 number :=0;
--v_cnt2 number :=0;
tableName varchar2 (100);
sqlString varchar2 (300);
--sqlString2 varchar2 (300);
--v_tname varchar2(50);
BEGIN

fnd_file.put_line (fnd_file.LOG, '#################################################################################');          

select count(*) into v_cnt from ABRL_RMS_RMSTOGL_TAB;
if v_cnt >=1 then

tableName := 'ABRL_RMS_RMSTOGL_TAB_'||to_char(sysdate,'DDMONYY');
fnd_file.put_line (fnd_file.LOG, ' ');
fnd_file.put_line (fnd_file.LOG, ' ');
fnd_file.put_line (fnd_file.LOG, 'Backup Table Name: '||tableName);
sqlString :='CREATE TABLE '||tableName||' as select * from ABRL_RMS_RMSTOGL_TAB';
--dbms_output.PUT_LINE('Script: '||sqlString );

--select count(*) into v_cnt1 from tableName;

--sqlString2 := --||tableName;
--dbms_output.PUT_LINE(sqlString2);


EXECUTE IMMEDIATE sqlString;
EXECUTE IMMEDIATE 'select count(*) from '|| tableName||' ' into v_cnt1;
fnd_file.put_line (fnd_file.LOG, 'Creating the backup table '||tableName||'..');

fnd_file.put_line (fnd_file.LOG, ' ');
fnd_file.put_line (fnd_file.LOG, 'Row-count of ABRL_RMS_RMSTOGL_TAB :  ' || v_cnt);
fnd_file.put_line (fnd_file.LOG, 'Row-count of '||tableName||' :  ' || v_cnt1);
fnd_file.put_line (fnd_file.LOG, ' ');
--select tname into v_tname from tab
--where tname ='ABRL_RMS_RMSTOGL_TAB_|| to_char(sysdate,'ddmmyy')'

if v_cnt=v_cnt1 then
execute immediate 'truncate table ABRL_RMS_RMSTOGL_TAB';
fnd_file.put_line (fnd_file.LOG, 'Count verified.Truncating table ABRL_RMS_RMSTOGL_TAB... ');
fnd_file.put_line (fnd_file.LOG, ' ');

insert into ABRL_RMS_RMSTOGL_TAB select * from ABRL_RMS_RMSTOGL_MV;
fnd_file.put_line (fnd_file.LOG, 'Inserting the contents of RMS2GL MV into ABRL_RMS_RMSTOGL_TAB.. ');
fnd_file.put_line (fnd_file.LOG, ' ');
fnd_file.put_line (fnd_file.LOG, ' ');

select count(*) into v_cnt2 from ABRL_RMS_RMSTOGL_TAB;
--1479786
fnd_file.put_line (fnd_file.LOG, ' ');
fnd_file.put_line (fnd_file.LOG, '>> Latest row-count of ABRL_RMS_RMSTOGL_TAB:  ' || v_cnt2||' <<');
fnd_file.put_line (fnd_file.LOG, ' ');

else
fnd_file.put_line (fnd_file.LOG, '*** Error code2: ABRL_RMS_RMSTOGL_TAB & '||tableName||' row count not matching. Check!! *** ');

--select count(*) into v_cnt2 from ABRL_RMS_RMSTOGL_TAB;

--end if;
--end if;

--fnd_file.put_line (fnd_file.LOG, 'ABRL_RMS_RMSTOGL_TAB Old row-count :  ' || v_cnt);

--fnd_file.put_line (fnd_file.OUT, 'ABRL_RMS_RMSTOGL_TAB Old Backup row-count :  ' || v_cnt1);
--fnd_file.put_line (fnd_file.OUT, 'ABRL_RMS_RMSTOGL_TAB New row-count :  ' || v_cnt2);

end if;
else 
fnd_file.put_line (fnd_file.LOG, ' ');
fnd_file.put_line (fnd_file.LOG, ' ');
fnd_file.put_line (fnd_file.LOG, '*** Error code 1: ABRL_RMS_RMSTOGL_TAB does not have any rows!Check!! *** ');
end if;
fnd_file.put_line (fnd_file.LOG, ' ');
fnd_file.put_line (fnd_file.LOG, ' ');
fnd_file.put_line (fnd_file.LOG, '#################################################################################');
end;
end; 
/


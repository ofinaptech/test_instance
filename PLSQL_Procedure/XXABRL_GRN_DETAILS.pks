CREATE OR REPLACE PACKAGE APPS.xxabrl_grn_details
IS
   PROCEDURE xxabrl_main_pkg (
      errbuf    OUT      VARCHAR2,
      retcode   OUT      NUMBER,
      p_type    IN       VARCHAR2
   );

   PROCEDURE xxabrl_grn_headers;

   PROCEDURE xxabrl_grn_lines;

   PROCEDURE xxabrl_grn_trans;

   PROCEDURE xxabrl_grn_tax;
END xxabrl_grn_details; 
/


CREATE OR REPLACE PACKAGE BODY APPS.XXABRL_RMS_GL_OCTROI_PKG IS

  /*************************************************************************************************************/
  /* Following Cursor will selects GL data */
  /*************************************************************************************************************/

  gn_conc_req_id NUMBER := fnd_global.conc_request_id; -- To print the concurrent request id
  gn_user_id     NUMBER := fnd_global.user_id;

/*  
        CHANGE BY PRAVEEN KUMAR ON 21-APR-2010
        REASON :- USER_JE_SOURCE_NAME SHOULD BE RETEK . BECAUSE FOR THE USER_JE_SOURCE_NAME OCTROI WE
        PREPARED SEPERATE PROGRAM ACCORDING TO REQUIRMENT.
*/

  CURSOR VAL_CUR IS
    SELECT USER_JE_SOURCE_NAME, ACCOUNTING_DATE, GROUP_ID2
      FROM XXABRL_GL_INTER_STAG
     WHERE 1 = 1
--     AND TRUNC(INTERFACE_DATE) = TRUNC(SYSDATE)
    AND TRUNC(INTERFACE_DATE)  >= TRUNC(SYSDATE)
    AND NVL(INTERFACE_FLAG,'N') IN ('N', 'E')
    GROUP BY USER_JE_SOURCE_NAME, ACCOUNTING_DATE, GROUP_ID2;

--     AND ((TRUNC(ACCOUNTING_DATE) < '01-DEC-14')
--                OR
--         (TRUNC(ACCOUNTING_DATE) > '30-NOV-14' AND SEGMENT3 NOT IN ('752', '754', '772', '775', '773', '812', '792')))
--    USER_JE_SOURCE_NAME='OCTROI'


  CURSOR ABRL_GL_CUR(P_USER_JE_SOURCE_NAME VARCHAR2, P_ACCOUNTING_DATE DATE, P_GROUP_ID NUMBER) IS
    SELECT ROWID, GIS.*
      FROM XXABRL_GL_INTER_STAG GIS
     WHERE 1=1
    AND USER_JE_SOURCE_NAME        = P_USER_JE_SOURCE_NAME
    AND ACCOUNTING_DATE            = P_ACCOUNTING_DATE
    AND GROUP_ID2                = P_GROUP_ID
       AND NVL(INTERFACE_FLAG, 'N')        IN ('N', 'E')
-- AND TRUNC(INTERFACE_DATE) = TRUNC(SYSDATE)
       AND TRUNC(INTERFACE_DATE)        >= TRUNC(SYSDATE);

--     AND ((TRUNC(ACCOUNTING_DATE) < '01-DEC-14')
--                OR
--         (TRUNC(ACCOUNTING_DATE) > '30-NOV-14' AND SEGMENT3 NOT IN ('752', '754', '772', '775', '773', '812', '792')));
-- AND USER_JE_SOURCE_NAME= 'OCTROI'
-- AND ERROR_MESSAGE IS NULL
-- AND INTERFACE_FLAG IS NULL


/*  ERRORS RECORDS ARE PROCESSED ONCE AGAIN FOR RE-VALIDATION */

  CURSOR ABRL_GL_INT_CUR IS
    SELECT rowid, gis.*
      FROM XXABRL_GL_INTER_STAG GIS
     WHERE 1=1
    AND ERROR_MESSAGE IS NULL
        AND INTERFACE_FLAG = 'V'
-- AND TRUNC(INTERFACE_DATE) = TRUNC(SYSDATE)
    AND TRUNC(INTERFACE_DATE) >= TRUNC(SYSDATE);

--     AND ((TRUNC(ACCOUNTING_DATE) < '01-DEC-14')
--                OR
--         (TRUNC(ACCOUNTING_DATE) > '30-NOV-14' AND SEGMENT3 NOT IN ('752', '754', '772', '775', '773', '812', '792')));
--    AND USER_JE_SOURCE_NAME= 'OCTROI'


  /*************************************************************************************************************/
  /* Main Procedure calling */
  /*************************************************************************************************************/

  PROCEDURE MAIN_PROC(x_err_buf OUT VARCHAR2, x_ret_code OUT NUMBER) IS
    ln_val_ins_retcode NUMBER := 0; --variable to return the status
    ln_val_retcode     NUMBER := 0;
    v_tot_cnt          NUMBER := 0;
    v_succ_cnt         NUMBER := 0;
    v_fail_cnt         NUMBER := 0;
    v_user_name        VARchar2(100) := null;
    L_VALIDATE_CNT     NUMBER :=0;
    NV_SEGMENT_COUNT   NUMBER := 0;
    NV_TSRL_MIGRATION  number;
    
  
  BEGIN
    x_ret_code := 0;
    x_err_buf  := NULL;
  
    SELECT USER_NAME
      INTO V_USER_NAME
      FROM FND_USER
     WHERE USER_ID = GN_USER_ID;
  
    UPDATE XXABRL_GL_INTER_STAG SET ENTERED_DR = NULL WHERE ENTERED_DR = 0;
    -- and USER_JE_SOURCE_NAME='OCTROI';
  
    UPDATE XXABRL_GL_INTER_STAG SET ENTERED_CR = NULL WHERE ENTERED_CR = 0;
    -- and USER_JE_SOURCE_NAME='OCTROI';
    COMMIT;
  

--    29TH December 2014      updated by Bala.SK
--    DUE TO Legal Entity Merger of TSRL with ABRL
--    UPDATING OLD VALUES OF SEGMENT1, SEGMENT3 AND LEDGER_ID TO NEW VALUES 

    BEGIN

          NV_TSRL_MIGRATION := 0;

-- 812

          SELECT COUNT(*) INTO NV_TSRL_MIGRATION
          FROM APPS.XXABRL_GL_INTER_STAG
          WHERE 1=1
          AND TRUNC(INTERFACE_DATE)        >= TRUNC(SYSDATE)
          AND TRUNC(ACCOUNTING_DATE)        >= '01-APR-2015'
            AND NVL(INTERFACE_FLAG,'N')        IN ('N', 'E')
          AND SEGMENT3                     = '812';

          IF  NVL(NV_TSRL_MIGRATION,0) > 0   THEN

                        BEGIN
                        
                        UPDATE APPS.XXABRL_GL_INTER_STAG
                        SET 
                SEGMENT1    = '11',
                SEGMENT3        = '635',
                LEDGER_ID        = 2101
            WHERE 1=1
            AND TRUNC(INTERFACE_DATE)        >= TRUNC(SYSDATE)
            AND TRUNC(ACCOUNTING_DATE)    >= '01-APR-2015'
            AND NVL(INTERFACE_FLAG,'N')        IN ('N', 'E')
            AND SEGMENT3                = '812';
                        
                        EXCEPTION
                        WHEN OTHERS THEN
                        FND_FILE.PUT_LINE(Fnd_File.log,  '  LE Merger UPDATE FAILURE FOR = 812 ');
                        x_ret_code := 2;
            ROLLBACK;
                        goto muttal;
                        END;                          
                        
                        COMMIT;
          END IF;

          FND_FILE.PUT_LINE(Fnd_File.log,  '  LE Merger UPDATED FOR = 812 ... ROWS = ' || NV_TSRL_MIGRATION);

          NV_TSRL_MIGRATION := 0;

--    772

          SELECT COUNT(*) INTO NV_TSRL_MIGRATION
          FROM APPS.XXABRL_GL_INTER_STAG
          WHERE 1=1
          AND TRUNC(INTERFACE_DATE)        >= TRUNC(SYSDATE)
          AND TRUNC(ACCOUNTING_DATE)        >= '01-APR-2015'
      AND NVL(INTERFACE_FLAG,'N')        IN ('N', 'E')
          AND SEGMENT3                     = '772';

          IF  NVL(NV_TSRL_MIGRATION,0) > 0   THEN

                        BEGIN
                        
                        UPDATE APPS.XXABRL_GL_INTER_STAG
                        SET 
                SEGMENT1    = '11',
                SEGMENT3        = '620',
                                LEDGER_ID        = 2101
                        WHERE 1=1
            AND TRUNC(INTERFACE_DATE)        >= TRUNC(SYSDATE)
            AND TRUNC(ACCOUNTING_DATE)    >= '01-APR-2015'
            AND NVL(INTERFACE_FLAG,'N')        IN ('N', 'E')
            AND SEGMENT3                = '772';
                        
                        EXCEPTION
                        WHEN OTHERS THEN
                        FND_FILE.PUT_LINE(Fnd_File.log,  '  LE Merger UPDATE FAILURE FOR = 772 ');
                        x_ret_code := 2;
            ROLLBACK;
                        goto muttal;
                        END;                          

          FND_FILE.PUT_LINE(Fnd_File.log,  '  LE Merger UPDATED FOR = 772 ... ROWS = ' || NV_TSRL_MIGRATION);
                        
                        COMMIT;
          END IF;          

          NV_TSRL_MIGRATION := 0;

-- 792

          SELECT COUNT(*) INTO NV_TSRL_MIGRATION
          FROM APPS.XXABRL_GL_INTER_STAG
          WHERE 1=1
          AND TRUNC(INTERFACE_DATE)        >= TRUNC(SYSDATE)
          AND TRUNC(ACCOUNTING_DATE)        >= '01-APR-2015'
      AND NVL(INTERFACE_FLAG,'N')        IN ('N', 'E')
          AND SEGMENT3                     = '792';

          IF  NVL(NV_TSRL_MIGRATION,0) > 0   THEN

                        BEGIN
                        
                        UPDATE APPS.XXABRL_GL_INTER_STAG
                        SET 
                SEGMENT1    = '11',
                SEGMENT3        = '640',
                                LEDGER_ID        = 2101
                        WHERE 1=1
            AND TRUNC(INTERFACE_DATE)        >= TRUNC(SYSDATE)
            AND TRUNC(ACCOUNTING_DATE)    >= '01-APR-2015'
            AND NVL(INTERFACE_FLAG,'N')        IN ('N', 'E')
            AND SEGMENT3                = '792';
                        
                        EXCEPTION
                        WHEN OTHERS THEN
                        FND_FILE.PUT_LINE(Fnd_File.log,  '  LE Merger UPDATE FAILURE FOR = 792 ');
                        x_ret_code := 2;
            ROLLBACK;
                        goto muttal;
                        END;                          

          FND_FILE.PUT_LINE(Fnd_File.log,  '  LE Merger UPDATED FOR = 792 ... ROWS = ' || NV_TSRL_MIGRATION);

                        COMMIT;
          END IF;          

          NV_TSRL_MIGRATION := 0;
-- 752

          SELECT COUNT(*) INTO NV_TSRL_MIGRATION
          FROM APPS.XXABRL_GL_INTER_STAG
          WHERE 1=1
          AND TRUNC(INTERFACE_DATE)        >= TRUNC(SYSDATE)
          AND TRUNC(ACCOUNTING_DATE)        >= '01-APR-2015'
      AND NVL(INTERFACE_FLAG,'N')        IN ('N', 'E')
          AND SEGMENT3                     = '752';

          IF  NVL(NV_TSRL_MIGRATION,0) > 0   THEN

                        BEGIN
                        
                        UPDATE APPS.XXABRL_GL_INTER_STAG
                        SET 
                SEGMENT1    = '11',
                SEGMENT3        = '610',
                                LEDGER_ID        = 2101
                        WHERE 1=1
            AND TRUNC(INTERFACE_DATE)        >= TRUNC(SYSDATE)
            AND TRUNC(ACCOUNTING_DATE)    >= '01-APR-2015'
            AND NVL(INTERFACE_FLAG,'N')        IN ('N', 'E')
            AND SEGMENT3                = '752';
                        
                        EXCEPTION
                        WHEN OTHERS THEN
                        FND_FILE.PUT_LINE(Fnd_File.log,  '  LE Merger UPDATE FAILURE FOR = 752 ');
                        x_ret_code := 2;
            ROLLBACK;
                        goto muttal;
                        END;                          

          FND_FILE.PUT_LINE(Fnd_File.log,  '  LE Merger UPDATED FOR = 752 ... ROWS = ' || NV_TSRL_MIGRATION);

                        COMMIT;
          END IF;          

          NV_TSRL_MIGRATION := 0;
-- 754

          SELECT COUNT(*) INTO NV_TSRL_MIGRATION
          FROM APPS.XXABRL_GL_INTER_STAG
          WHERE 1=1
          AND TRUNC(INTERFACE_DATE)        >= TRUNC(SYSDATE)
          AND TRUNC(ACCOUNTING_DATE)        >= '01-APR-2015'
      AND NVL(INTERFACE_FLAG,'N')        IN ('N', 'E')
          AND SEGMENT3                     = '754';

          IF  NVL(NV_TSRL_MIGRATION,0) > 0   THEN

                        BEGIN
                        
                        UPDATE APPS.XXABRL_GL_INTER_STAG
                        SET 
                SEGMENT1    = '11',
                SEGMENT3        = '615',
                                LEDGER_ID        = 2101
                        WHERE 1=1
            AND TRUNC(INTERFACE_DATE)        >= TRUNC(SYSDATE)
            AND TRUNC(ACCOUNTING_DATE)    >= '01-APR-2015'
            AND NVL(INTERFACE_FLAG,'N')        IN ('N', 'E')
            AND SEGMENT3                = '754';
                        
                        EXCEPTION
                        WHEN OTHERS THEN
                        FND_FILE.PUT_LINE(Fnd_File.log,  '  LE Merger UPDATE FAILURE FOR = 754 ');
                        x_ret_code := 2;
            ROLLBACK;
                        goto muttal;
                        END;                          

          FND_FILE.PUT_LINE(Fnd_File.log,  '  LE Merger UPDATED FOR = 754 ... ROWS = ' || NV_TSRL_MIGRATION);

                        COMMIT;
          END IF;          

          NV_TSRL_MIGRATION := 0;
-- 775

          SELECT COUNT(*) INTO NV_TSRL_MIGRATION
          FROM APPS.XXABRL_GL_INTER_STAG
          WHERE 1=1
          AND TRUNC(INTERFACE_DATE)        >= TRUNC(SYSDATE)
          AND TRUNC(ACCOUNTING_DATE)        >= '01-APR-2015'
      AND NVL(INTERFACE_FLAG,'N')        IN ('N', 'E')
          AND SEGMENT3                     = '775';

          IF  NVL(NV_TSRL_MIGRATION,0) > 0   THEN

                        BEGIN
                        
                        UPDATE APPS.XXABRL_GL_INTER_STAG
                        SET 
                SEGMENT1    = '11',
                SEGMENT3        = '625',
                LEDGER_ID        = 2101
                        WHERE 1=1
            AND TRUNC(INTERFACE_DATE)        >= TRUNC(SYSDATE)
            AND TRUNC(ACCOUNTING_DATE)    >= '01-APR-2015'
            AND NVL(INTERFACE_FLAG,'N')        IN ('N', 'E')
            AND SEGMENT3                = '775';
                        
                        EXCEPTION
                        WHEN OTHERS THEN
                        FND_FILE.PUT_LINE(Fnd_File.log,  '  LE Merger UPDATE FAILURE FOR = 775 ');
                        x_ret_code := 2;
            ROLLBACK;
                        goto muttal;
                        END;                          

          FND_FILE.PUT_LINE(Fnd_File.log,  '  LE Merger UPDATED FOR = 775 ... ROWS = ' || NV_TSRL_MIGRATION);

                        COMMIT;
          END IF;          

    END;
--    29TH December 2014      updated by Bala.SK
--    DUE TO Legal Entity Merger of TSRL with ABRL
--    UPDATING OLD VALUES OF SEGMENT1, SEGMENT3 AND LEDGER_ID TO NEW VALUES 


--    19TH FEB 2015
--

    BEGIN

          NV_TSRL_MIGRATION := 0;


          SELECT COUNT(*) INTO NV_TSRL_MIGRATION
          FROM APPS.XXABRL_GL_INTER_STAG
          WHERE 1=1
--          AND TRUNC(INTERFACE_DATE)        >= '30-JAN-2015'
          AND TRUNC(ACCOUNTING_DATE)       >= '01-APR-2015'
          AND NVL(INTERFACE_FLAG,'N')      IN ('N', 'E')
          AND SEGMENT3                     IN ( '812', '772', '773', '792', '752', '754', '775', '751');


    IF NVL(NV_TSRL_MIGRATION,0)    > 0    THEN

                        FND_FILE.PUT_LINE(Fnd_File.log,  '  LE Merger Old Org to New Org UPDATE FAILURE... Serious ');
                        x_ret_code := 2;
            ROLLBACK;
                        goto muttal;
    END IF;

        END;        
--
--    19TH FEB 2015












      SELECT COUNT(1)
      INTO V_TOT_CNT
      FROM XXABRL_GL_INTER_STAG
     WHERE 1=1
--AND USER_JE_SOURCE_NAME='OCTROI'
        AND INTERFACE_FLAG IS NULL
        AND ERROR_MESSAGE IS NULL
--        AND TRUNC(INTERFACE_DATE) = TRUNC(SYSDATE);
    AND TRUNC(INTERFACE_DATE) >= TRUNC(SYSDATE);
    

    SELECT COUNT(1) INTO L_VALIDATE_CNT
    FROM XXABRL_GL_INTER_STAG
    WHERE 1=1
--AND USER_JE_SOURCE_NAME='OCTROI'    
    AND NVL(INTERFACE_FLAG,'N') IN('N','E')
--    AND TRUNC(INTERFACE_DATE) = TRUNC(SYSDATE);
    AND TRUNC(INTERFACE_DATE) >= TRUNC(SYSDATE);

    
    IF L_VALIDATE_CNT <> 0 THEN
    XXABRL_RMS_GL_OCTROI_PKG.XXABRL_GL_VALID_PROC(x_val_retcode => ln_val_retcode);
    COMMIT;
    END IF; 
    
    BEGIN
    XXABRL_RMS_GL_OCTROI_PKG.XXABRL_GL_INT_PROC(x_val_ins_retcode => ln_val_ins_retcode);
    END;
  
  
    SELECT COUNT(1)
      INTO V_SUCC_CNT
      FROM XXABRL_GL_INTER_STAG
     WHERE 1=1
-- AND USER_JE_SOURCE_NAME='OCTROI'
        AND INTERFACE_FLAG = 'P'
        AND ERROR_MESSAGE IS NULL
--        AND TRUNC(INTERFACE_DATE) = TRUNC(SYSDATE);
    AND TRUNC(INTERFACE_DATE) >= TRUNC(SYSDATE);

      
  SELECT COUNT(1)
      INTO V_FAIL_CNT
      FROM XXABRL_GL_INTER_STAG
     WHERE 1=1
-- AND USER_JE_SOURCE_NAME='OCTROI'
        AND INTERFACE_FLAG = 'E'
--        AND TRUNC(INTERFACE_DATE) = TRUNC(SYSDATE);
    AND TRUNC(INTERFACE_DATE) >= TRUNC(SYSDATE);

       
  
    --  To return the status of the MAIN procedure
  
    IF NVL(ln_val_ins_retcode, 0) = 0 AND NVL(ln_val_retcode, 0) = 0 THEN
      x_ret_code := 0;
    ELSE
      x_ret_code := 1;
    END IF;
    fnd_file.put_line(fnd_file.output,
                      ('----------------------------------------------------------------------------------------'));
    fnd_file.put_line(fnd_file.output,
                      ('                                                                                        '));
    fnd_file.put_line(fnd_file.output,
                      ('  Request id     : ' || gn_conc_req_id ||
                      '                     ' ||
                      TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS')));
    fnd_file.put_line(fnd_file.output,
                      ('  Requester Name : ' || v_user_name));
    fnd_file.put_line(fnd_file.output,
                      ('                                                                                        '));
    fnd_file.put_line(fnd_file.output,
                      ('                              ABRL RMS TO GL                                            '));
    fnd_file.put_line(fnd_file.output,
                      ('                                                                                        '));
    fnd_file.put_line(fnd_file.output,
                      ('                           Run Date: ' ||
                      TO_CHAR(SYSDATE, 'DD/MM/YYYY')));
    fnd_file.put_line(fnd_file.output,
                      ('                                                                                        '));
    fnd_file.put_line(fnd_file.output,
                      ('                                                                                        '));
    fnd_file.put_line(fnd_file.output,
                      ('  Total Number of Records in GL Staging Table            : ' ||
                      v_tot_cnt));
    fnd_file.put_line(fnd_file.output,
                      ('                                                                                        '));
    fnd_file.put_line(fnd_file.output,
                      ('  Total Number of records inserted into interface table  : ' ||
                      v_succ_cnt));
    fnd_file.put_line(fnd_file.output,
                      ('                                                                                        '));
    fnd_file.put_line(fnd_file.output,
                      ('  Total number of records errored                        : ' ||
                      v_fail_cnt));
    fnd_file.put_line(fnd_file.output,
                      ('                                                                                        '));
    fnd_file.put_line(fnd_file.output,
                      ('                                                                                        '));
    fnd_file.put_line(fnd_file.output,
                      ('----------------------------------------------------------------------------------------'));
  

        <<MUTTAL>>
        
            NV_SEGMENT_COUNT :=0;



  EXCEPTION
  
    WHEN OTHERS THEN
      FND_FILE.PUT_LINE(FND_FILE.LOG,
                        'ERROR : MAIN PROCEDURE ' ||
                        SUBSTR(SQLERRM, 1, 300));
      x_ret_code := 2;
    

  END MAIN_PROC;

  /*************************************************************************************************************/
  /* Procedure for Validating Staging table */
  /*************************************************************************************************************/

  PROCEDURE XXABRL_GL_VALID_PROC(x_val_retcode OUT NUMBER) IS
  
    v_ledger_id             NUMBER;
    v_err_msg               VARCHAR2(2000);
    v_err_flag              VARCHAR2(1);
    v_user_je_cat_name      VARCHAR2(25);
    v_user_je_src_name      VARCHAR2(25);
    v_period_name           VARCHAR2(15);
    v_dummy                 VARCHAR2(1);
    v_currency_code         VARCHAR2(20);
    v_conversion_type       VARCHAR2(30);
    v_code_combination_id   NUMBER;
    v_chart_of_accounts_id  NUMBER;
    v_cnt                   NUMBER := 0;
--        v_vat number:=0;
    V_USER_NAME             VARCHAR2(100);
    x_concatenated_segments varchar2(100);
    NV_ERROR_COUNTER        NUMBER;
  
  BEGIN
  
    SELECT USER_NAME
      INTO V_USER_NAME
      FROM FND_USER
     WHERE USER_ID = GN_USER_ID;
  
    fnd_file.put_line(fnd_file.log,
                      ('------------------------------------------------------------------------------                           '));
    fnd_file.put_line(fnd_file.log,
                      ('                                                                                                       '));
    fnd_file.put_line(fnd_file.log,
                      ('  Request id     : ' || gn_conc_req_id ||
                      '                               ' ||
                      TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS')));
    fnd_file.put_line(fnd_file.log, ('  Requester Name : ' || v_user_name));
    fnd_file.put_line(fnd_file.log,
                      ('                                                                                                       '));
    fnd_file.put_line(fnd_file.log,
                      ('                        ABRL RMS TO GL ERROR REPORT                                                      '));
    fnd_file.put_line(fnd_file.log,
                      ('                                                                                                       '));
    fnd_file.put_line(fnd_file.log,
                      ('                           Run Date: ' ||
                      TO_CHAR(SYSDATE, 'DD/MM/YYYY')));
    fnd_file.put_line(fnd_file.log,
                      ('                                                                                                       '));
    fnd_file.put_line(fnd_file.log,
                      ('------------------------------------------------------------------------------                          '));
    fnd_file.put_line(fnd_file.log,
                      ('                                                                                                       '));
    --fnd_file.put_line(fnd_file.log,('                                                                                                       '));
  
    fnd_file.put_line(fnd_file.log,
                      (' SEQ_NUM     ERROR_MESSAGES                                                                            '));
    fnd_file.put_line(fnd_file.log,
                      (' -------     ---------------                                                                              '));
  
    v_err_msg := null;
  
    FOR R_VAL IN VAL_CUR LOOP
    
      FOR ABRL_GL_REC IN ABRL_GL_CUR(R_VAL.USER_JE_SOURCE_NAME,
                                     R_VAL.ACCOUNTING_DATE,
                                     R_VAL.GROUP_ID2) 
      LOOP

        --Initialize Variables
        v_err_flag             := NULL;
        v_err_msg              := NULL;
        v_ledger_id            := NULL;
        v_user_je_cat_name     := NULL;
        v_user_je_src_name     := NULL;
        v_period_name          := NULL;
        v_dummy                := NULL;
        v_currency_code        := NULL;
        v_code_combination_id  := NULL;
        v_chart_of_accounts_id := NULL;
        NV_ERROR_COUNTER       := 0;
      
        /*************************************************************************************************************/
        /* Deriving Ledger_id from Segment3 */
        /*************************************************************************************************************/
        
         BEGIN
         
         V_LEDGER_ID:=2101;
         
         
          IF TRUNC(ABRL_GL_REC.ACCOUNTING_DATE) < '01-APR-2012' THEN

           BEGIN
            --added Location logic extra DATE 05-Aug-2011
               SELECT GLS.LEDGER_ID  
                INTO v_ledger_id
               FROM APPS.GL_LEDGERS GLS,
               APPS.GL_CODE_COMBINATIONS_KFV GCC,
               apps.GL_LEDGER_SEGMENT_VALUES GLSV
                WHERE 
                     GLS.RET_EARN_CODE_COMBINATION_ID = GCC.CODE_COMBINATION_ID
                     AND GCC.SEGMENT1=abrl_gl_rec.segment1
                     AND GLSV.LEDGER_ID = GLS.LEDGER_ID
                     AND GLSV.SEGMENT_VALUE=abrl_gl_rec.segment3;
                --added Location logic extra DATE 05-Aug-2011
                                 
           EXCEPTION
                WHEN NO_DATA_FOUND THEN
                v_err_msg     := v_err_msg ||
                                 ' Invalid Segment3 to derive ledger id ' ||
                                 abrl_gl_rec.segment3;
                v_err_flag    := 'Y';
                x_val_retcode := 1;
                Fnd_File.put_line(Fnd_File.log,
                                  '   ' || abrl_gl_rec.seq_num || '.  ' ||
                                  ' Invalid segment3 (State SBU) to derive Ledger ID : ' ||
                                  abrl_gl_rec.segment3);
                WHEN TOO_MANY_ROWS THEN
                v_err_msg     := v_err_msg || ' Too many rows for ' ||
                                 abrl_gl_rec.segment3;
                v_err_flag    := 'Y';
                x_val_retcode := 1;
                Fnd_File.put_line(Fnd_File.log,
                                  '   ' || abrl_gl_rec.seq_num || '.  ' ||
                                  ' Too many rows segment3 (State SBU) to derive Ledger ID : ' ||
                                  abrl_gl_rec.segment3);
              
                WHEN OTHERS THEN
                v_err_msg     := v_err_msg ||
                                 ' Unexpected while deriving ledger id ' ||
                                 SQLERRM;
                v_err_flag    := 'Y';
                x_val_retcode := 1;
                Fnd_File.put_line(Fnd_File.log,
                                  '   ' || abrl_gl_rec.seq_num || '.  ' ||
                                  ' Unexpected while deriving ledger id ' ||
                                  SQLERRM);
           END;                 
                              
          END IF;
          
        END;
        
         /*************************************************************************************************************/
        /* VAT Accounting Codes are not allowed from 1st July 2017 due to GST                                                                       */ 
        /*************************************************************************************************************/
        
BEGIN

 --   V_VAT := 0;
   
    IF      ABRL_GL_REC.SEGMENT6 = 226106       AND         ABRL_GL_REC.ACCOUNTING_DATE > '30-JUN-17'
    THEN

      v_err_flag := 'Y';
      v_err_msg := 'VAT Regime Accounts are not accepted in GST Regime';

   END IF;
   
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
        /*************************************************************************************************************/
        /* Validation for User Category Name */
        /*************************************************************************************************************/
        BEGIN
          SELECT user_je_category_name
            INTO v_user_je_cat_name
            FROM apps.GL_JE_CATEGORIES
           WHERE UPPER(user_je_category_name) IN
                 (SELECT UPPER(DESCRIPTION)
                    FROM FND_LOOKUP_VALUES
                   WHERE LOOKUP_TYPE = 'ABRL_JV_CATEGORY_DERIVATION'
                     AND LOOKUP_CODE = ABRL_GL_REC.GROUP_ID2);
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            v_err_msg     := ' Mismatch - group id category description with Journal Category ' ||
                             ABRL_GL_REC.GROUP_ID2;
            v_err_flag    := 'Y';
            x_val_retcode := 1;
            FND_FILE.PUT_LINE(Fnd_File.log,
                              '   ' || abrl_gl_rec.seq_num || '.  ' ||
                              ' Mismatch - Group ID category description with Journal Category : ' ||
                              ABRL_GL_REC.GROUP_ID2);
          WHEN OTHERS THEN
            v_err_msg     := ' Unexpected Journal Category Error ' ||
                             SQLERRM;
            v_err_flag    := 'Y';
            x_val_retcode := 1;
            FND_FILE.PUT_LINE(Fnd_File.log,
                              '   ' || abrl_gl_rec.seq_num || '.  ' ||
                              ' Unexpected Journal Category Error ' ||
                              SQLERRM);
        END;
      
        /*************************************************************************************************************/
        /* Validation for Journal Source name */
        /*************************************************************************************************************/
      
        BEGIN
          SELECT user_je_source_name
            INTO v_user_je_src_name
            FROM apps.GL_JE_SOURCES
           WHERE upper(user_je_source_name) =
                 upper(abrl_gl_rec.user_je_source_name);
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            v_err_msg     := ' Invalid Journal Source ' ||
                             abrl_gl_rec.user_je_source_name;
            v_err_flag    := 'Y';
            x_val_retcode := 1;
            FND_FILE.PUT_LINE(Fnd_File.log,
                              '   ' || abrl_gl_rec.seq_num || '.  ' ||
                              ' Invalid Journal Source : ' ||
                              abrl_gl_rec.user_je_source_name);
          WHEN OTHERS THEN
            v_err_msg     := ' Unexpected Journal Source Error ' || SQLERRM;
            v_err_flag    := 'Y';
            x_val_retcode := 1;
            FND_FILE.PUT_LINE(Fnd_File.log,
                              '   ' || abrl_gl_rec.seq_num || '.  ' ||
                              ' Unexpected Journal Source Error ' ||
                              SQLERRM);
        END;
        /*************************************************************************************************************/
        /* Validation for Period Name */
        /*************************************************************************************************************/
      
        BEGIN
          SELECT PERIOD_NAME
            INTO v_period_name
            FROM apps.GL_PERIODS
           WHERE UPPER(period_name) = UPPER(abrl_gl_rec.period_name)
             AND UPPER(period_set_name) = UPPER('ABRL Calendar');
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            v_err_msg     := ' Invalid Period Name ' ||
                             abrl_gl_rec.period_name;
            v_err_flag    := 'Y';
            x_val_retcode := 1;
            FND_FILE.PUT_LINE(Fnd_File.log,
                              '   ' || abrl_gl_rec.seq_num || '.  ' ||
                              ' Invalid Period Name : ' ||
                              abrl_gl_rec.period_name);
          WHEN OTHERS THEN
            v_err_msg     := 'Unexpected Error in Period Name' || SQLERRM;
            v_err_flag    := 'Y';
            x_val_retcode := 1;
            FND_FILE.PUT_LINE(Fnd_File.log,
                              '   ' || abrl_gl_rec.seq_num || '.  ' ||
                              ' Unexpected Error in Period Name ' ||
                              SQLERRM);
        END;
      
        /*************************************************************************************************************/
        /* Validate Entered DR and Entered CR Amount */
        /*************************************************************************************************************/
      
        /*  BEGIN
        
            IF abrl_gl_rec.entered_dr <= 0 THEN
                  v_err_msg  := v_err_msg ||'Entered Debit amount is invalid '||abrl_gl_rec.entered_dr;
                  v_err_flag := 'Y';
                x_val_retcode:=1;
                  FND_FILE.PUT_LINE(Fnd_File.log,'   '||abrl_gl_rec.seq_num||'.  '||' Invalid Debit Amount : '||abrl_gl_rec.entered_dr);
            END IF;
        
            IF abrl_gl_rec.entered_cr <= 0 THEN
                   v_err_msg  := v_err_msg ||'Entered Credit amount is invalid '|| abrl_gl_rec.entered_cr;
                   v_err_flag := 'Y';
                x_val_retcode:=1;
                   FND_FILE.PUT_LINE(Fnd_File.log,'   '||abrl_gl_rec.seq_num||'.  '||' Ivalid Credit Amount : '||abrl_gl_rec.entered_cr);
            END IF;
             EXCEPTION
            WHEN OTHERS THEN
                v_err_msg  := v_err_msg ||' Unexpected DR AND CR Amount Error ';
                v_err_flag := 'Y';
                x_val_retcode:=1;
                  FND_FILE.PUT_LINE(Fnd_File.log,'   '||abrl_gl_rec.seq_num||'.  '||' Unexpected DR AND CR Amount Error '||SQLERRM);
        END;  */
      
        /*************************************************************************************************************/
        /* Validation for Period Name is open or not */
        /*************************************************************************************************************/
      
        BEGIN
          SELECT 'Y'
            INTO v_dummy
            FROM GL_PERIOD_STATUSES
           WHERE UPPER(period_name) = UPPER(abrl_gl_rec.period_name)
             AND (closing_status = 'O' OR closing_status = 'F')
             AND set_of_books_id = v_ledger_id
             AND application_id in
                 (SELECT application_id
                    FROM fnd_application
                   WHERE application_short_name = 'SQLGL');
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            v_err_msg     := v_err_msg || ' Period is not opened ' ||
                             abrl_gl_rec.period_name;
            v_err_flag    := 'Y';
            x_val_retcode := 1;
            FND_FILE.PUT_LINE(Fnd_File.log,
                              '   ' || abrl_gl_rec.seq_num || '.  ' ||
                              ' Period not open : ' ||
                              abrl_gl_rec.period_name);
          WHEN OTHERS THEN
            v_err_msg     := ' Unexpected Error in Period ' || SQLERRM;
            v_err_flag    := 'Y';
            x_val_retcode := 1;
            FND_FILE.PUT_LINE(Fnd_File.log,
                              '   ' || abrl_gl_rec.seq_num || '.  ' ||
                              ' Unexpected Error in Period ' || SQLERRM);
        END;
      
        /*************************************************************************************************************/
        /* Validate currency code */
        /*************************************************************************************************************/
      
        BEGIN
          SELECT currency_code
            INTO v_currency_code
            FROM apps.FND_CURRENCIES
           WHERE UPPER(currency_code) = UPPER(abrl_gl_rec.currency_code);
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            v_err_msg     := v_err_msg || ' Invalid Currency Code ' ||
                             abrl_gl_rec.currency_code;
            v_err_flag    := 'Y';
            x_val_retcode := 1;
            Fnd_File.put_line(Fnd_File.log,
                              '   ' || abrl_gl_rec.seq_num || '.  ' ||
                              ' Invalid Currency Code : ' ||
                              abrl_gl_rec.currency_code);
          WHEN OTHERS THEN
            v_err_msg     := v_err_msg || 'Unexpected Curr Code Error' ||
                             SQLERRM;
            v_err_flag    := 'Y';
            x_val_retcode := 1;
            Fnd_File.put_line(Fnd_File.log,
                              '   ' || abrl_gl_rec.seq_num || '.  ' ||
                              ' Unexpected Curr Code Error' || SQLERRM);
        END;
      
        /*************************************************************************************************************/
        /* Validate USER CURRENCY CONVERSION TYPE */
        /*************************************************************************************************************/
      
        BEGIN
          SELECT CONVERSION_TYPE
            INTO v_conversion_type
            FROM apps.GL_DAILY_CONVERSION_TYPES
           WHERE conversion_type =
                 abrl_gl_rec.user_currency_conversion_type
             AND conversion_type in ('Corporate', 'Spot');
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            v_err_msg     := v_err_msg ||
                             ' Invalid User Currency Conversion Type ' ||
                             abrl_gl_rec.user_currency_conversion_type;
            v_err_flag    := 'Y';
            x_val_retcode := 1;
            Fnd_File.put_line(Fnd_File.log,
                              '   ' || abrl_gl_rec.seq_num || '.  ' ||
                              ' Invalid User Currency Conversion Type : ' ||
                              abrl_gl_rec.user_currency_conversion_type);
          WHEN OTHERS THEN
            v_err_msg     := v_err_msg ||
                             'Unexpected User Currency Conversion Type Error' ||
                             SQLERRM;
            v_err_flag    := 'Y';
            x_val_retcode := 1;
            Fnd_File.put_line(Fnd_File.log,
                              '   ' || abrl_gl_rec.seq_num || '.  ' ||
                              ' Unexpected User Currency Conversion Type Error' ||
                              SQLERRM);
        END;
        
        /*************************************************************************************************************/
        /* Validate segment values */
        /*************************************************************************************************************/
      
      /*   THIS IS SQL TAKING TOO MUCH TIME THAT IS WHY ITS DISCARDED.......BALA SK
      
        BEGIN
          
               SELECT code_combination_id
                   ,chart_of_accounts_id
               INTO v_code_combination_id
                   ,v_chart_of_accounts_id
               FROM GL_CODE_COMBINATIONS_KFV
              WHERE  1=1
                AND concatenated_segments = abrl_gl_rec.segment1 ||'.'||
                                            abrl_gl_rec.segment2 ||'.'||
                                            abrl_gl_rec.segment3 ||'.'||
                                            abrl_gl_rec.segment4 ||'.'||
                                            abrl_gl_rec.segment5 ||'.'||
                                            abrl_gl_rec.segment6 ||'.'||
                                            abrl_gl_rec.segment7 ||'.'||
                                            abrl_gl_rec.segment8
                AND enabled_flag = 'Y';
               EXCEPTION
              WHEN NO_DATA_FOUND THEN
                 v_err_msg := v_err_msg ||' Invalid Code Combination : ' ||abrl_gl_rec.segment1 ||'.'||
                                                                           abrl_gl_rec.segment2 ||'.'||
                                                                           abrl_gl_rec.segment3 ||'.'||
                                                                           abrl_gl_rec.segment4 ||'.'||
                                                                           abrl_gl_rec.segment5 ||'.'||
                                                                           abrl_gl_rec.segment6 ||'.'||
                                                                           abrl_gl_rec.segment7 ||'.'||
                                                                           abrl_gl_rec.segment8 ;
                 v_err_flag := 'Y';
                 x_val_retcode:=1;
                 Fnd_File.put_line(Fnd_File.log,'   '||abrl_gl_rec.seq_num||'.  '||' Invalid Code Combination : '||abrl_gl_rec.segment1 ||'.'||
                                                                              abrl_gl_rec.segment2 ||'.'||
                                                                              abrl_gl_rec.segment3 ||'.'||
                                                                              abrl_gl_rec.segment4 ||'.'||
                                                                              abrl_gl_rec.segment5 ||'.'||
                                                                              abrl_gl_rec.segment6 ||'.'||
                                                                              abrl_gl_rec.segment7 ||'.'||
                                                                              abrl_gl_rec.segment8);
              WHEN OTHERS THEN
                 v_err_msg := v_err_msg ||'Unexpected Code Combination Error'||SQLERRM;
                 v_err_flag := 'Y';
                 x_val_retcode:=1;
                 Fnd_File.put_line(Fnd_File.log,'   '||abrl_gl_rec.seq_num||'.  '||' Unexpected Code Combination Error'||SQLERRM);
                 END;
    
       THIS IS SQL TAKING TOO MUCH TIME THAT IS WHY ITS DISCARDED.......BALA SK     */
       

/*   SEGMENT 1 - 8 VALIDATION CARRIED OUT DIRECTLY WITH THE TABLE .....   BALA SK     29.02.2012   */

        BEGIN

               v_code_combination_id   := NULL;
               x_concatenated_segments := NULL;
               NV_ERROR_COUNTER        := 0;
        
               SELECT code_combination_id INTO v_code_combination_id
               FROM GL_CODE_COMBINATIONS_KFV
               WHERE  1=1
               AND enabled_flag    = 'Y'
               AND segment1     =  abrl_gl_rec.segment1
               AND segment2     =  abrl_gl_rec.segment2
               AND segment3     =  abrl_gl_rec.segment3
               AND segment4     =  abrl_gl_rec.segment4
               AND segment5     =  abrl_gl_rec.segment5
               AND segment6     =  abrl_gl_rec.segment6
               AND segment7     =  abrl_gl_rec.segment7
               AND segment8     =  abrl_gl_rec.segment8;
               
              EXCEPTION
              WHEN NO_DATA_FOUND THEN
                 v_err_msg := v_err_msg ||' Invalid Segments : ' ||abrl_gl_rec.segment1 ||'.'||
                                                                           abrl_gl_rec.segment2 ||'.'||
                                                                           abrl_gl_rec.segment3 ||'.'||
                                                                           abrl_gl_rec.segment4 ||'.'||
                                                                           abrl_gl_rec.segment5 ||'.'||
                                                                           abrl_gl_rec.segment6 ||'.'||
                                                                           abrl_gl_rec.segment7 ||'.'||
                                                                           abrl_gl_rec.segment8 ;
                 v_err_flag := 'Y';
                 x_val_retcode:=1;
                 NV_ERROR_COUNTER:= 1;

                 Fnd_File.put_line(Fnd_File.log,'   '||abrl_gl_rec.seq_num||'.  '||' Invalid Segments: '||abrl_gl_rec.segment1 ||'.'||
                                                                              abrl_gl_rec.segment2 ||'.'||
                                                                              abrl_gl_rec.segment3 ||'.'||
                                                                              abrl_gl_rec.segment4 ||'.'||
                                                                              abrl_gl_rec.segment5 ||'.'||
                                                                              abrl_gl_rec.segment6 ||'.'||
                                                                              abrl_gl_rec.segment7 ||'.'||
                                                                              abrl_gl_rec.segment8);

                    BEGIN
                    
                        v_chart_of_accounts_id  := 50328;
                        x_concatenated_segments := abrl_gl_rec.segment1 || '.' ||
                                     abrl_gl_rec.segment2 || '.' ||
                                     abrl_gl_rec.segment3 || '.' ||
                                     abrl_gl_rec.segment4 || '.' ||
                                     abrl_gl_rec.segment5 || '.' ||
                                     abrl_gl_rec.segment6 || '.' ||
                                     abrl_gl_rec.segment7 || '.' ||
                                     abrl_gl_rec.segment8;
      
                      SELECT fnd_flex_ext.get_ccid('SQLGL',
                                                       'GL#',
                                                       50328,
                                                       TO_CHAR(SYSDATE,
                                                               'YYYY/MM/DD HH24:MI:SS'),
                                                       x_concatenated_segments)
                            INTO v_code_combination_id
                            FROM DUAL;
                        
                          IF v_code_combination_id = 0 THEN
                          
                            v_err_msg     := v_err_msg || ' Invalid Code Combination : ' ||
                                             abrl_gl_rec.segment1 || '.' ||
                                             abrl_gl_rec.segment2 || '.' ||
                                             abrl_gl_rec.segment3 || '.' ||
                                             abrl_gl_rec.segment4 || '.' ||
                                             abrl_gl_rec.segment5 || '.' ||
                                             abrl_gl_rec.segment6 || '.' ||
                                             abrl_gl_rec.segment7 || '.' ||
                                             abrl_gl_rec.segment8;
                            v_err_flag    := 'Y';
                            x_val_retcode := 1;
                            Fnd_File.put_line(Fnd_File.log,
                                              '   ' || abrl_gl_rec.seq_num || '.  ' ||
                                              ' Invalid Code Combination : ' ||
                                              abrl_gl_rec.segment1 || '.' ||
                                              abrl_gl_rec.segment2 || '.' ||
                                              abrl_gl_rec.segment3 || '.' ||
                                              abrl_gl_rec.segment4 || '.' ||
                                              abrl_gl_rec.segment5 || '.' ||
                                              abrl_gl_rec.segment6 || '.' ||
                                              abrl_gl_rec.segment7 || '.' ||
                                              abrl_gl_rec.segment8);
                          
                           ELSE IF NVL(NV_ERROR_COUNTER,0)= 1    THEN
                             v_err_flag := 'N';
                             x_val_retcode:=0;
                                                    
                           END IF;
                          END IF;

                    END;

              WHEN OTHERS THEN
                 v_err_msg := v_err_msg ||'Unexpected Segments Error'||SQLERRM;
                 v_err_flag := 'Y';
                 x_val_retcode:=1;
                 Fnd_File.put_line(Fnd_File.log,'   '||abrl_gl_rec.seq_num||'.  '||' Unexpected Segments Error'||SQLERRM);
             END;


      
        /*************************************************************************************************************/
        /* Validate segment values CCID and file CCID */
        /*************************************************************************************************************/
        /*
               IF abrl_gl_rec.code_combination_id <> NVL(v_code_combination_id,-11111)
               THEN
                    v_err_msg := v_err_msg ||' Invalid code_combination_id value  ' ||abrl_gl_rec.code_combination_id;
                    v_err_flag := 'Y';
                    x_val_retcode:=1;
                    Fnd_File.put_line(Fnd_File.log,'   '||abrl_gl_rec.seq_num||'.  '||' Code combination ID '||abrl_gl_rec.code_combination_id ||' is not valid in APPS ');
               END IF;
        */


       /*************************************************************************************************************/
        /* updating stging table */
        /*************************************************************************************************************/
      
        IF NVL(v_err_flag, 'N') = 'Y' THEN
          BEGIN
          
            UPDATE apps.XXABRL_GL_INTER_STAG
               SET INTERFACE_FLAG = 'E',
                   error_message  = error_message || ' ; ' ||
                                    abrl_gl_rec.seq_num || '.   ' ||
                                    v_err_msg
             WHERE 1 = 1
                  --  and USER_JE_SOURCE_NAME='OCTROI'
               AND ROWID = abrl_gl_rec.ROWID;
          
            FND_FILE.PUT_LINE(Fnd_File.LOG,
                              '   ' || abrl_gl_rec.seq_num || '.  ' ||
                              v_err_msg);
            COMMIT;
          EXCEPTION
            WHEN OTHERS THEN
              FND_FILE.PUT_LINE(Fnd_File.LOG,
                                '   ' || abrl_gl_rec.seq_num || '.  ' ||
                                ' Error in Update XXABRL_GL_INTER_STAG table' ||
                                SQLERRM);
          END;
            
ELSE
            
          BEGIN
          
            UPDATE APPS.XXABRL_GL_INTER_STAG
               SET INTERFACE_FLAG            = 'V',
                   ERROR_MESSAGE             = NULL,
                   LEDGER_ID                 = V_LEDGER_ID,
                   CHART_OF_ACCOUNTS_ID      = V_CHART_OF_ACCOUNTS_ID,
                   USER_JE_CATEGORY_NAME     = V_USER_JE_CAT_NAME,
                   USER_JE_SOURCE_NAME       = V_USER_JE_SRC_NAME,
                   CODE_COMBINATION_ID       = V_CODE_COMBINATION_ID
             WHERE 1 = 1
-- AND USER_JE_SOURCE_NAME='OCTROI'
               AND ROWID = abrl_gl_rec.ROWID;
            COMMIT;
                    
          EXCEPTION
            WHEN OTHERS THEN
              FND_FILE.PUT_LINE(Fnd_File.LOG,
                                '   ' || abrl_gl_rec.seq_num || '.  ' ||
                                ' Error in Update XXABRL_GL_INTER_STAG table' ||
                                SQLERRM);
              x_val_retcode := 1;
          END;
        END IF;
      END LOOP;
    
BEGIN
      
       V_CNT := 0;
      
      SELECT COUNT(GROUP_ID2)
          INTO V_CNT
          FROM XXABRL_GL_INTER_STAG GIS
         WHERE 1=1
-- AND USER_JE_SOURCE_NAME='OCTROI'
           AND USER_JE_SOURCE_NAME = R_VAL.USER_JE_SOURCE_NAME
           AND ACCOUNTING_DATE = R_VAL.ACCOUNTING_DATE
           AND GROUP_ID2 = R_VAL.GROUP_ID2
           AND INTERFACE_FLAG = 'E';
      
        IF V_CNT > 0 THEN
        
          UPDATE XXABRL_GL_INTER_STAG
             SET INTERFACE_FLAG = 'E'
           WHERE 1=1
--  AND USER_JE_SOURCE_NAME='OCTROI'
             AND USER_JE_SOURCE_NAME = R_VAL.USER_JE_SOURCE_NAME
             AND ACCOUNTING_DATE = R_VAL.ACCOUNTING_DATE
             AND GROUP_ID2 = R_VAL.GROUP_ID2
             AND NVL(INTERFACE_FLAG,'N') = 'E';


--             AND ((TRUNC(ACCOUNTING_DATE) < '01-DEC-14')
--                OR
--         (TRUNC(ACCOUNTING_DATE) > '30-NOV-14' AND SEGMENT3 NOT IN ('752', '754', '772', '775', '773', '812', '792')))
--             AND INTERFACE_FLAG  NOT IN ( 'P', 'B');      

--        29th December 2014 
--        MEANS DUE TO RMS DATA ERROR, DATE BY DATE WE ARE TRYING TO LOAD TO AVOID RECONCILIATION BETWEEN
--        STAGE vs INTERFACE  vs  BASE 

        COMMIT;

        END IF;
      
      EXCEPTION
        WHEN OTHERS THEN
          FND_FILE.PUT_LINE(Fnd_File.LOG,
                            ' Error in Update XXABRL_GL_INTER_STAG table' ||
                            SQLERRM);
          x_val_retcode := 1;
      END;
    
    END LOOP;
  
    fnd_file.put_line(fnd_file.log,
                      ('*** Validation for Duplicate Sequence Numbers ***  '));
    fnd_file.put_line(fnd_file.log,
                      ('--------------------------------------------------- '));
  


/*                                 MODIFIED BY BALA.SK ON 12.06.2013
                                    DUE TO TOO MUCH TIME IS CONSUMED ON DUPLICATE SQUENCE NUMBER CHECK OF RMS WHICH IS OF NO USE TO OFIN
                                    IT WAS FOUND THAT THERE WERE 8 DUPLICATE SEQUENCE NUMBERS EXIST ON 
                                                INTERFACE_DATE = '07-FEB-2013' AND    ACCOUNTING_DATE = '25-JAN-2013'
*/
  
    BEGIN
      FOR CUR_DUP IN (SELECT SEQ_NUM FROM XXABRL_GL_INTER_STAG
                       WHERE 
                                        SEQ_NUM IS NOT NULL
                                    AND TRUNC(ACCOUNTING_DATE) >= '01-APR-2015'
                                    AND TRUNC(INTERFACE_DATE)  >= TRUNC(SYSDATE)
-- AND USER_JE_SOURCE_NAME = 'OCTROI'
                       GROUP BY SEQ_NUM
                      HAVING COUNT(*) > 1) 

      LOOP
            BEGIN
            
          UPDATE XXABRL_GL_INTER_STAG GIS
             SET GIS.INTERFACE_FLAG = 'E',
                 GIS.ERROR_MESSAGE  = 'DUPLICATE SEQUENCE NUMBER ; ' ||
                                      GIS.ERROR_MESSAGE
           WHERE 
            GIS.SEQ_NUM = CUR_DUP.SEQ_NUM;
        
        V_ERR_FLAG := 'Y';
        
          fnd_file.put_line(fnd_file.log,
                            'Duplicate Sequence Number in staging: ' ||
                            Cur_Dup.seq_num);
        EXCEPTION
          when no_data_found then
            fnd_file.put_line(fnd_file.log,
                              'Seq# not found for update: ' ||
                              Cur_Dup.seq_num);
          when Others then
            fnd_file.put_line(fnd_file.log,
                              'Error while Updating for duplicate Sequence#: ' ||
                              Cur_Dup.seq_num);
        END;
      END LOOP;
    END;

/*                                 MODIFIED BY BALA.SK ON 12.06.2013
                                    DUE TO TOO MUCH TIME IS CONSUMED ON DUPLICATE SQUENCE NUMBER CHECK OF RMS WHICH IS OF NO USE TO OFIN
                                    IT WAS FOUND THAT THERE WERE 8 DUPLICATE SEQUENCE NUMBERS EXIST ON 
                                                INTERFACE_DATE = '07-FEB-2013' AND    ACCOUNTING_DATE = '25-JAN-2013'
*/



    fnd_file.put_line(fnd_file.log,
                      ('=================
=========================
========= '));
    COMMIT;
  
    fnd_file.put_line(fnd_file.log,
                      ('                                                                                                       '));
  END XXABRL_GL_VALID_PROC;

  /*************************************************************************************************************/
  /* Procedure for Inserting into interface table */
  /*************************************************************************************************************/

  PROCEDURE XXABRL_GL_INT_PROC(x_val_ins_retcode OUT NUMBER) IS
    CURSOR CUR_SUB IS
      SELECT USER_JE_SOURCE_NAME, GROUP_ID
        FROM GL_INTERFACE
       WHERE 1 = 1
         AND DATE_CREATED LIKE SYSDATE
         AND USER_JE_SOURCE_NAME IN ('RETEK', 'OCTROI')
       GROUP BY USER_JE_SOURCE_NAME, GROUP_ID;
  
    v_gl_access_set_id   number;
    v_ledger_id          number;
    v_ledger_name        varchar2(250);
    v_error_cnt          number;
    v_insert_cnt         number;
    v_group_id           number;
    ln_loader_request_id number;
  
    v_set_of_books_id number := fnd_profile.value('GL_SET_OF_BKS_ID');
    v_user_id         number := fnd_profile.value('USER_ID');
    v_resp_id         number := fnd_profile.value('RESP_ID');
    v_appl_id         number := fnd_profile.value('RESP_APPL_ID');
    
 
    -- Cursor added for Data access set ..Sai on 02-Aug-2010. 
    
 CURSOR led_id_cur IS
  select distinct lgr.ledger_id, lgr.name
             -- into v_LEDGER_ID, v_LEDGER_NAME
                from
                gl_access_set_ledgers gas ,
                gl_ledgers lgr,
                gl_interface gli
                where gas.access_set_id =  fnd_profile.value( 'GL_ACCESS_SET_ID')
                and gas.ledger_id = lgr.ledger_id
                and gli.ledger_id = lgr.ledger_id ; 

    
  
  BEGIN
    v_insert_cnt := 0;
    
    FOR R_INT IN ABRL_GL_INT_CUR LOOP
    
      BEGIN
      
        INSERT INTO APPS.GL_INTERFACE
          (status,
           ledger_id,
           accounting_date,
           currency_code,
           date_created,
           created_by,
           actual_flag,
           user_je_category_name,
           user_je_source_name,
           currency_conversion_date,
           user_currency_conversion_type,
           entered_dr,
           entered_cr,
           transaction_date,
           period_name,
           code_combination_id,
           accounted_dr,
           accounted_cr,
           currency_conversion_rate,
           chart_of_accounts_id,
           set_of_books_id,
           reference21,
           reference22,
           reference23,
           reference24,
           reference25,
           attribute1,
           attribute2,
           attribute3,
           attribute4,
           attribute5,
           attribute6,
           group_id)
        VALUES
          (r_int.status,
           r_int.ledger_id,
           r_int.accounting_date,
           r_int.currency_code,
           SYSDATE,
           3,
           r_int.actual_flag,
           r_int.user_je_category_name,
           r_int.user_je_source_name,
           r_int.currency_conversion_date,
           r_int.user_currency_conversion_type,
           r_int.entered_dr,
           r_int.entered_cr,
           r_int.transaction_date,
           r_int.period_name,
           r_int.code_combination_id,
           r_int.entered_dr,
           r_int.entered_cr,
           r_int.currency_conversion_rate,
           r_int.chart_of_accounts_id,
           r_int.ledger_id,
           r_int.reference21,
           r_int.reference22,
           r_int.reference23,
           r_int.reference24,
           r_int.reference25,
           r_int.attribute1,
           r_int.attribute2,
           r_int.attribute3,
           r_int.attribute4,
           r_int.attribute5,
           r_int.attribute6,
           r_int.group_id2);
      
        UPDATE apps.XXABRL_GL_INTER_STAG
           SET INTERFACE_FLAG = 'P'
         WHERE 1 = 1
           AND INTERFACE_FLAG = 'V'
-- AND USER_JE_SOURCE_NAME='OCTROI'
           AND ROWID = r_int.ROWID;
        COMMIT;
        v_INSERT_CNT := v_INSERT_CNT + 1;
      
      EXCEPTION
        WHEN OTHERS THEN
          FND_FILE.PUT_LINE(Fnd_File.LOG,
                            ' Error in Interface Program while inserting the record ' ||
                            SQLERRM);
                            
          x_val_ins_retcode := 1;
      END;
    
    END LOOP;
  
    v_GL_ACCESS_SET_ID := fnd_profile.value('GL_ACCESS_SET_ID');
  
    IF v_GL_ACCESS_SET_ID IS NULL then
      fnd_file.put_line(FND_FILE.Log,
                        'GL_ACCESS_SET_ID / Ledger ID Setup Incomplete...');
    ELSE
      fnd_file.put_line(FND_FILE.Log,
                        'GL_ACCESS_SET_ID: ' || v_GL_ACCESS_SET_ID);

      BEGIN
      
      --Commented Below Query on 03-Aug-2010.
      open led_id_cur;
      fetch led_id_cur
      into v_ledger_id,v_ledger_name;
      close led_id_cur;
            
       /* select lgr.ledger_id, lgr.name
          into v_LEDGER_ID, v_LEDGER_NAME
          from gl_access_set_ledgers gas, gl_ledgers lgr, gl_interface gli
         where gas.access_set_id = v_GL_ACCESS_SET_ID
           and gas.ledger_id = lgr.ledger_id
           and gli.ledger_id = lgr.ledger_id --<<
           and rownum = 1;
        NULL;
      */
    
    FND_FILE.PUT_LINE(FND_FILE.LOG,
                          'LEDGER ID: ' || V_LEDGER_ID ||
                          ';  LEDGER_NAME: ' || V_LEDGER_NAME);
      
        BEGIN
    
    select group_id
            into v_group_id
            from gl_srs_ji_groups_v
           where je_source_name = je_source_name                     --'RETEK'
                --and ledger_id =v_LEDGER_ID
             and ledger_id in
                 (select lgr.ledger_id
                    from gl_access_set_ledgers gas, gl_ledgers lgr
                   where gas.access_set_id = v_GL_ACCESS_SET_ID
                     and gas.ledger_id = lgr.ledger_id)
             and rownum = 1;
          fnd_file.put_line(FND_FILE.Log,
                            'Derived Group ID this Ledger ' || v_group_id);
        EXCEPTION
          when no_data_found then
            fnd_file.put_line(FND_FILE.Log,
                              'No Group ID found from GL Interface for this Ledger ' ||
                              v_LEDGER_ID);
          when others then
            fnd_file.put_line(FND_FILE.Log,
                              'Exception while deriving Group_ID/GL Interface/Ledger ' ||
                              v_LEDGER_ID);
        END;
      
      EXCEPTION
        when no_data_found then
          fnd_file.put_line(FND_FILE.Log,
                            'Ledger ID not found for GL_ACCESS_SET_ID: ' ||
                            v_GL_ACCESS_SET_ID);
        when others then
          fnd_file.put_line(FND_FILE.Log,
                            'Exception ( ' || SQLERRM ||
                            ' )while deriving Ledger ID for : ' ||
                            v_GL_ACCESS_SET_ID);
      END;
    END IF;
  
    IF v_LEDGER_ID IS NOT NULL THEN

-- IF 1=1 THEN

      BEGIN
             v_error_cnt := 0;

        SELECT COUNT(*)
          INTO V_ERROR_CNT
          FROM XXABRL_GL_INTER_STAG
         WHERE 1=1
-- USER_JE_SOURCE_NAME = 'RETEK' 
    AND INTERFACE_FLAG = 'V'
    AND SEQ_NUM IS NOT NULL
        AND LEDGER_ID = V_LEDGER_ID
-- AND TRUNC(INTERFACE_DATE) = TRUNC(SYSDATE);
    AND TRUNC(INTERFACE_DATE) >= TRUNC(SYSDATE);
        
        
        IF v_insert_cnt > 0 and v_group_id is not NULL then
          BEGIN
            fnd_global.apps_initialize(user_id      => v_user_id,
                                       resp_id      => v_resp_id,
                                       resp_appl_id => v_appl_id);
            COMMIT;
          EXCEPTION
            when others then
              fnd_file.put_line(FND_FILE.Log,
                                'Error in App Intialize ' || SQLERRM);
          END;
        
          BEGIN
          
            FOR REQ_SUB in CUR_SUB LOOP
            
                -- New loop added on 03-Aug-2010
                FOR LED_ID IN LED_ID_CUR LOOP
                    
                  IF REQ_SUB.user_je_source_name = 'OCTROI' THEN
                    
                    -- ABRL Ledger                 
                    BEGIN
                    ln_loader_request_id := fnd_request.submit_request('SQLGL',
                                                                         'GLLEZLSRS',
                                                                         '',
                                                                         '',
                                                                         FALSE,
                                                                         1120, --v_gl_access_set_id, -- commented for Migration on 01-Apr-2012 -- Amresh
                                                                         '21', ---Octroi Source..
                                                                         2101,--LED_ID.ledger_id, --ABRL Ledger-- commented for Migration on 01-Apr-2012 -- Amresh
                                                                         101, ---Octroi Group Id Constant
                                                                         'N',
                                                                         'N',
                                                                         'O',
                                                                         CHR(0));
                      COMMIT;
                    EXCEPTION
                      WHEN OTHERS THEN
                        fnd_file.put_line(fnd_file.LOG,
                                          'Exception while Request submit ' ||
                                          SQLERRM);
                                      
                    END; 
                    
                  ELSIF REQ_SUB.user_je_source_name = 'RETEK' THEN
                  
                    -- ABRL Ledger  
                    BEGIN
                      ln_loader_request_id := fnd_request.submit_request('SQLGL',
                                                                         'GLLEZLSRS',
                                                                         '',
                                                                         '',
                                                                         FALSE,
                                                                         1120, --v_gl_access_set_id, -- commented for Migration on 01-Apr-2012 -- Amresh
                                                                         '1', ---RETEK Source..
                                                                         2101, --LED_ID.ledger_id,--TSRL TN Ledger -- commented for Migration on 01-Apr-2012 -- Amresh
                                                                         REQ_SUB.group_id, ---RETEK Group Id Constant
                                                                         'N',
                                                                         'N',
                                                                         'O',
                                                                         CHR(0));
                      COMMIT;
                    EXCEPTION
                      WHEN OTHERS THEN
                        fnd_file.put_line(fnd_file.LOG,
                                          'Exception while Request submit ' ||
                                          SQLERRM);
                    END;
                    
                  END IF;
                
                END LOOP;
              ----
            END LOOP;
            COMMIT;
            IF (ln_loader_request_id = 0) THEN
            
              fnd_file.put_line(fnd_file.log,
                                'Request Not Submitted due to "' ||
                                fnd_message.get || '".');
              fnd_file.put_line(FND_FILE.Log,
                                'Concurrent Request For Loading STD. GL IMPORT Generated an error, Submitted with request id ' ||
                                ln_loader_request_id);
            
            ELSE
              fnd_file.put_line(FND_FILE.Log,
                                'Submitted Loader Program with request id : ' ||
                                TO_CHAR(ln_loader_request_id));
            END IF;
          EXCEPTION
            when others then
              fnd_file.put_line(FND_FILE.Log,
                                'Exception while Request submit ' ||
                                SQLERRM);
          END;
        END IF;
      
      EXCEPTION
        when others then
          fnd_file.put_line(FND_FILE.Log,
                            'Exception while couting Error records');
      END;
    END IF;
  
  END XXABRL_GL_INT_PROC;

  
/*         
      3RD MARCH 2012
          DUE TO ERRONEOUS SEGMENTS DATA FROM  RMS, UPDATION OF SEGMENT2, SEGMENT4, SEGMENT5, SEGMENT7 - SEGMENT8
          BEING CARRIED OUT UNFORTUNATELY TO AVOID STAGE DATA GETTING REJECTED

          BEGIN
          
          NV_SEGMENT_COUNT := 0;
          
          SELECT COUNT(*) INTO NV_SEGMENT_COUNT 
          FROM APPS.XXABRL_GL_INTER_STAG
          WHERE 1=1
          AND  TRUNC(INTERFACE_DATE) = TRUNC(SYSDATE)
            --AND TRUNC(INTERFACE_DATE) IN ('16-FEB-2012', '29-FEB-2012', '14-MAR-2012')
          AND SEGMENT2 = '0';
          
          
              IF  NVL(NV_SEGMENT_COUNT,0) > 0   THEN

                BEGIN
                
                UPDATE APPS.XXABRL_GL_INTER_STAG
                SET
                SEGMENT2 = '000'
                  WHERE 1=1
                  AND  TRUNC(INTERFACE_DATE) = TRUNC(SYSDATE)
                  --  AND TRUNC(INTERFACE_DATE) IN ('16-FEB-2012', '29-FEB-2012', '14-MAR-2012')
                  AND SEGMENT2 = '0';
                
                EXCEPTION
                WHEN OTHERS THEN
                FND_FILE.PUT_LINE(Fnd_File.log,  '  Failed to update SEGMENT2');
                x_ret_code := 2;
                GOTO MUTTAL;
                END;                              

                COMMIT;
              END IF;
        
          NV_SEGMENT_COUNT := 0;
          
          SELECT COUNT(*) INTO NV_SEGMENT_COUNT 
          FROM APPS.XXABRL_GL_INTER_STAG
          WHERE 1=1
          AND  TRUNC(INTERFACE_DATE) = TRUNC(SYSDATE)
          --             AND TRUNC(INTERFACE_DATE) IN ('16-FEB-2012', '29-FEB-2012', '14-MAR-2012')
          AND SEGMENT4 = '0';
          
          
          IF  NVL(NV_SEGMENT_COUNT,0) > 0   THEN

                        BEGIN
                        
                        UPDATE APPS.XXABRL_GL_INTER_STAG
                        SET
                        SEGMENT4 = '0000000'
                          WHERE 1=1
                          AND  TRUNC(INTERFACE_DATE) = TRUNC(SYSDATE)
                          --            AND TRUNC(INTERFACE_DATE) IN ('16-FEB-2012', '29-FEB-2012', '14-MAR-2012')
                          AND SEGMENT4 = '0';
                        
                        EXCEPTION
                        WHEN OTHERS THEN
                        FND_FILE.PUT_LINE(Fnd_File.log,  '  Failed to update SEGMENT4');
                        x_ret_code := 2;
                        GOTO MUTTAL;
                        END;                              

                        COMMIT;                        
                END IF;

          NV_SEGMENT_COUNT := 0;
          
          SELECT COUNT(*) INTO NV_SEGMENT_COUNT 
          FROM APPS.XXABRL_GL_INTER_STAG
          WHERE 1=1
          AND  TRUNC(INTERFACE_DATE) = TRUNC(SYSDATE)
          --            AND TRUNC(INTERFACE_DATE) IN ('16-FEB-2012', '29-FEB-2012', '14-MAR-2012')
          AND SEGMENT5 = '0';
          
          
          IF  NVL(NV_SEGMENT_COUNT,0) > 0   THEN

                        BEGIN
                        
                        UPDATE APPS.XXABRL_GL_INTER_STAG
                        SET
                        SEGMENT5 = '00'
                          WHERE 1=1
                          AND  TRUNC(INTERFACE_DATE) = TRUNC(SYSDATE)
                          --            AND TRUNC(INTERFACE_DATE) IN ('16-FEB-2012', '29-FEB-2012', '14-MAR-2012')
                          AND SEGMENT5 = '0';
                        
                        EXCEPTION
                        WHEN OTHERS THEN
                        FND_FILE.PUT_LINE(Fnd_File.log,  '  Failed to update SEGMENT5');
                        x_ret_code := 2;
                        goto muttal;
                        END;       
                        
                        COMMIT;
                END IF;

          NV_SEGMENT_COUNT := 0;
          
          SELECT COUNT(*) INTO NV_SEGMENT_COUNT 
          FROM APPS.XXABRL_GL_INTER_STAG
          WHERE 1=1
          AND  TRUNC(INTERFACE_DATE) = TRUNC(SYSDATE)
          --            AND TRUNC(INTERFACE_DATE) IN ('16-FEB-2012', '29-FEB-2012', '14-MAR-2012')
          AND SEGMENT7 = '0';
          
          
          IF  NVL(NV_SEGMENT_COUNT,0) > 0   THEN

                        BEGIN
                        
                        UPDATE APPS.XXABRL_GL_INTER_STAG
                        SET
                        SEGMENT7 = '000'
                          WHERE 1=1
                          AND  TRUNC(INTERFACE_DATE) = TRUNC(SYSDATE)
                          --            AND TRUNC(INTERFACE_DATE) IN ('16-FEB-2012', '29-FEB-2012', '14-MAR-2012')
                          AND SEGMENT7 = '0';
                        
                        EXCEPTION
                        WHEN OTHERS THEN
                        FND_FILE.PUT_LINE(Fnd_File.log,  '  Failed to update SEGMENT7');
                        x_ret_code := 2;
                        goto muttal;
                        END;                              
                        
                        COMMIT;
                END IF;

          NV_SEGMENT_COUNT := 0;
          
          SELECT COUNT(*) INTO NV_SEGMENT_COUNT 
          FROM APPS.XXABRL_GL_INTER_STAG
          WHERE 1=1
          AND  TRUNC(INTERFACE_DATE) = TRUNC(SYSDATE)
          --            AND TRUNC(INTERFACE_DATE) IN ('16-FEB-2012', '29-FEB-2012', '14-MAR-2012')
          AND SEGMENT8 = '0';
          
          
          IF  NVL(NV_SEGMENT_COUNT,0) > 0   THEN

                        BEGIN
                        
                        UPDATE APPS.XXABRL_GL_INTER_STAG
                        SET SEGMENT8 = '0000'
                          WHERE 1=1
                          AND  TRUNC(INTERFACE_DATE) = TRUNC(SYSDATE)
                          --            AND TRUNC(INTERFACE_DATE) IN ('16-FEB-2012', '29-FEB-2012', '14-MAR-2012')
                          AND SEGMENT8 = '0';
                        
                        EXCEPTION
                        WHEN OTHERS THEN
                        FND_FILE.PUT_LINE(Fnd_File.log,  '  Failed to update SEGMENT8');
                        x_ret_code := 2;
                        goto muttal;
                        END;                          
                        
                        COMMIT;
                END IF;
          
          END;         
  
      3RD MARCH 2012
          DUE TO ERRONEOUS SEGMENTS DATA FROM  RMS, UPDATION OF SEGMENT2, SEGMENT4, SEGMENT5, SEGMENT7 - SEGMENT8
          BEING CARRIED OUT UNFORTUNATELY TO AVOID STAGE DATA GETTING REJECTED

*/


END XXABRL_RMS_GL_OCTROI_PKG; 
/


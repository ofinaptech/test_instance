CREATE OR REPLACE PACKAGE APPS.xxmrl_ven_gstin_pan_update
IS
   PROCEDURE xxmrl_main_pkg (
      errbuf    OUT      VARCHAR2,
      retcode   OUT      NUMBER,
      p_type    IN       VARCHAR2
   );

   PROCEDURE xxmrl_vendor_gstin_update;

   PROCEDURE xxmrl_vendor_pan_update;
   
   PROCEDURE xxmrl_vendor_sec_pan_update;

   PROCEDURE xxmrl_vendor_rep_code_update;

   PROCEDURE xxmrl_vendor_tds_type_update;

END xxmrl_ven_gstin_pan_update; 
/


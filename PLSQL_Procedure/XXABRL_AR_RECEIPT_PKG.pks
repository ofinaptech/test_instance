CREATE OR REPLACE PACKAGE APPS.XXABRL_AR_RECEIPT_PKG AS
PROCEDURE  XXABRL_AR_RECEIPT_PROC (ERRBUFF OUT VARCHAR2,
                                    RETCODE OUT number,
                                    P_OPERATING_UNIT IN VARCHAR2,                                     
                                    P_FROM_CUSTOMER  VARCHAR2,
                                    P_TO_CUSTOMER   VARCHAR2,
                                    P_LIST_CUSTOMER  VARCHAR2,
                                    P_RECEIPT_FROM_NUM VARCHAR2,
                                    P_RECEIPT_TO_NUM  VARCHAR2,
                                    P_RECEIPT_FROM_DATE  DATE,
                                    P_RECEIPT_TO_DATE DATE,
                                    P_RECEIPT_METHOD VARCHAR2,
                                    P_BANK_ACCOUNT VARCHAR2,
                                    P_RECEIPT_STATUS VARCHAR2                                                                 
                                    ) ;
                                   END XXABRL_AR_RECEIPT_PKG; 
/


CREATE OR REPLACE PACKAGE BODY APPS.GST_ABRL_zerotds_invoice_pkg AS

/*
=========================================================================================================
||   Filename   : XXABRL_GL_INTER_PKG.sql
||   Description : Script is used to Load GL Data (GL Conversion)
||
||   Version     Date            Author              Modification
||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
||   1.0.0        05-may-2013    Amit Kumar      New Development

||   Usage : This script is used to get the zero percentage tds
||
========================================================================================================*/


PROCEDURE GST_ABRL_zerotds_inv_proc(ERRBUFF           OUT VARCHAR2,
                                RETCODE             OUT NUMBER,
                                p_vendor_NAME       IN NUMBER,
                                p_vendor_num_from   IN VARCHAR2,
                                p_vendor_num_to     IN VARCHAR2,
                                p_vendor_site_num   IN VARCHAR2,
                                p_tds_section       IN VARCHAR2,
                                p_inv_date_from     IN VARCHAR2,
                                p_inv_date_to       IN VARCHAR2,
                                p_cancelled_flag    IN VARCHAR2,
                                p_org_id            IN NUMBER
                                ) AS


CURSOR CUR_ORG_ID(cp_resp_id NUMBER) IS

     SELECT ORGANIZATION_ID
     FROM FND_PROFILE_OPTION_VALUES fpop,
     FND_PROFILE_OPTIONS fpo,
     PER_SECURITY_ORGANIZATIONS_V  pso
     WHERE fpop.PROFILE_OPTION_ID =fpo.PROFILE_OPTION_ID
     AND level_value=cp_resp_id  --52299 -- resp id
     AND pso.security_profile_id=fpop.PROFILE_OPTION_VALUE
     AND PROFILE_OPTION_NAME='XLA_MO_SECURITY_PROFILE_LEVEL';

-- variable declaration
query_string             LONG;

TYPE ref_cur IS REF CURSOR;

c                        ref_cur;

cancelled_string         VARCHAR2 (1500);
approved_string          VARCHAR2 (1500);
LIST_VEN_STRING          LONG;
RANGE_VEN_STRING         LONG;
VEN_SITE_CODE            LONG;
TDS_SECTION              LONG;
RANGE_INV_DATE_STRING    LONG;
ORDER_BY_STRING          LONG;
LIST_ORG_ID              LONG;
PARA_ORG_ID              LONG;
LIST_ORG_COND   VARCHAR2(100);
V_ORG_ID NUMBER:=0;
v_rec_section_code             LONG;
inv_count NUMBER:=0;
inv_date_from DATE;
inv_date_to DATE;
VEN_count NUMBER:=0;
m_ou_flag VARCHAR2(1):='N';

   V_RESPID             NUMBER := Fnd_Profile.VALUE('RESP_ID');

v_vendor_name_display LONG;
-- summery parameters declaration

v_tot_inv_amt     NUMBER;--(30,2);
-- grand total parameter declaration
v_grnd_tot_inv_amt     NUMBER;--(30,2);
-- columns to display on report

TYPE c_rec IS RECORD (  v_org_name               org_organization_definitions.organization_name%TYPE,
                        v_vendor_number          po_vendors.segment1%TYPE,
                        v_vendor_name            po_vendors.vendor_name%TYPE,
                        v_vendor_site_code       po_vendor_sites_all.vendor_site_code%TYPE,
                        v_invoice_type           ap_invoices_all.INVOICE_TYPE_LOOKUP_CODE%TYPE,
                        v_invoice_number         ap_invoices_all.invoice_num%TYPE,
                        V_GL_DATE                ap_invoices_all.GL_DATE%TYPE,
                        v_invoice_amount         ap_invoices_all.invoice_amount%TYPE,
                        v_invoice_line_amount    ap_invoice_lines_all.amount%type,
                      --  v_tds_tax_rate           JAI_CMN_TAXES_ALL.TAX_RATE%TYPE,
                       -- v_tds_section            JAI_CMN_TAXES_ALL.SECTION_CODE%TYPE,
                       -- v_tds_tax_name           JAI_CMN_TAXES_ALL.tax_name%TYPE,
                         v_tds_tax_rate           JAI_TAX_RATE_DETAILS_V.RATE_PERCENTAGE%TYPE,
                        v_tds_section              JAI_TAX_RATES_V.TAX_TYPE_CODE%TYPE,
                        v_tds_tax_name          JAI_TAX_RATES_V.TAX_RATE_NAME%TYPE,
                        v_creation_date          date,
                        v_user_name              varchar2(1500),
                        v_pan_no                 VARCHAR2(50),
                        v_tds_vendor_type        varchar2(25)
                        
                          );

v_rec                    c_rec;

v_group_by  varchar2(3000);

-- --  
BEGIN
-- query string for the zero tds

query_string :=
'  select ood.ORGANIZATION_NAME,pv.segment1 vendor_number,
         pv.vendor_name vendor_name,
         povs.vendor_site_code vendor_site,
         api.INVOICE_TYPE_LOOKUP_CODE Invoice_type,
         REPLACE(api.invoice_num,CHR(9),'''') invoice_number,
         apida.ACCOUNTING_DATE,
         api.invoice_amount invoice_amount,
         sum(apida.amount) invoice_dist_amount,
   --     jaict.TAX_RATE,
          jtrd.RATE_PERCENTAGE TAX_RATE,
   --     jaict.SECTION_CODE,
           jtr.TAX_TYPE_CODE  SECTION_CODE,
    --      jaict.TAX_NAME,
            jtr.TAX_RATE_NAME tax_name,
            api.CREATION_DATE
                     ,fu.USER_NAME
                  /*   ,NVL((SELECT JAT.PAN_NO FROM apps.JAI_AP_TDS_VENDOR_HDRS JAT
            WHERE JAT.VENDOR_ID=PV.VENDOR_ID
            AND JAT.VENDOR_SITE_ID = POVS.VENDOR_SITE_ID),NULL) PAN_NO, */
            ,NVL((SELECT jprl.REGISTRATION_NUMBER FROM apps.jai_party_regs_v jpr,
    apps.jai_party_reg_lines jprl
            WHERE 1=1
            --and JAT.VENDOR_ID=PV.VENDOR_ID
            and jpr.PARTY_ID=PV.VENDOR_ID
          --  AND JAT.VENDOR_SITE_ID = POVS.VENDOR_SITE_ID
          AND REGISTRATION_TYPE_CODE=''PAN''
            AND jpr.PARTY_SITE_ID = POVS.VENDOR_SITE_ID),NULL) PAN_NO, 
          --  jaivh.TDS_VENDOR_TYPE_LOOKUP_CODE Vendor_Type
            jtr.TAX_TYPE_CODE Vendor_Type
  from  apps.ap_invoices_all api
       ,apps.ap_invoice_lines_all apil
       ,apps.po_vendors pv
       ,apps.po_vendor_sites_all povs
       ,apps.ap_invoice_distributions_all apida,
   --    ,apps.JAI_CMN_TAXES_ALL jaict
         apps.JAI_TAX_RATES_V jtr,
        apps.org_organization_definitions OOD
       ,apps.ap_invoices_all api1
       ,apps.fnd_user fu,
       -- apps.JAI_AP_TDS_VENDOR_HDRS jaivh,
       apps. JAI_TAX_LINES jtl ,
       apps.JAI_TAX_RATE_DETAILS_V  jtrd
  where api.invoice_id=apil.invoice_id
    AND apida.invoice_id = api.invoice_id
    AND apida.invoice_line_number = apil.line_number
   -- and jaict.TAX_ID=apida.GLOBAL_ATTRIBUTE1
   and jtl.TAX_RATE_ID=apida.invoice_id
   and jtr.tax_rate_id=jtrd.TAX_RATE_ID
    and pv.vendor_id = povs.vendor_id
    AND api.vendor_id = pv.vendor_id
    AND api.vendor_site_id = povs.vendor_site_id
    AND OOD.ORGANIZATION_ID = api.ORG_ID   
    and fu.USER_ID =api.CREATED_BY
    AND apida.parent_invoice_id = api1.invoice_id(+)
--    and jaivh.VENDOR_ID = pv.VENDOR_ID
   AND jtl.PARTY_ID=pv.VENDOR_ID
 --  and jaivh.VENDOR_SITE_ID =povs.VENDOR_SITE_ID
 AND jtl.PARTY_SITE_ID=povs.VENDOR_SITE_ID
--   and jaict.TAX_RATE = 0
  AND jtrd.RATE_PERCENTAGE=0
--   and jaict.TAX_TYPE = ''TDS'' 
   AND jtr.TAX_TYPE_CODE like ''%TDS%'' ';
    
-- setting where conditions according to parameters

LIST_VEN_STRING:= 'AND pv.vendor_ID = '||p_vendor_NAME||' ' ;
RANGE_VEN_STRING:= 'AND pv.segment1 between '||p_vendor_num_from||' AND '||p_vendor_num_to||' ';
VEN_SITE_CODE:='AND povs.vendor_site_code  = '''||p_vendor_site_num||''' ';
TDS_SECTION:='AND jtr.TAX_TYPE_CODE = '''||p_tds_section||''' ';

SELECT TO_DATE(p_inv_date_from,'yyyy/mm/dd:HH24:MI:SS') INTO inv_date_from FROM dual;
SELECT TO_DATE(p_inv_date_to,'yyyy/mm/dd:HH24:MI:SS') INTO inv_date_to FROM dual;

RANGE_INV_DATE_STRING:='AND apida.ACCOUNTING_DATE  BETWEEN '''||inv_date_from||''' AND '''||inv_date_to||''' ';

v_group_by := ' group by 
    ood.ORGANIZATION_NAME,pv.segment1 ,
         pv.vendor_name ,
         povs.vendor_site_code ,
          PV.VENDOR_ID,
         POVS.VENDOR_SITE_ID,
         api.INVOICE_TYPE_LOOKUP_CODE ,
         REPLACE(api.invoice_num,CHR(9),'''') ,
         apida.ACCOUNTING_DATE,
         api.invoice_amount ,
      --   jaict.TAX_RATE,
         jtr.TAX_RATE_NAME ,
      --   jaict.SECTION_CODE,
           jtr.TAX_TYPE_CODE,
        -- jaict.TAX_NAME,
         jtr.TAX_RATE_NAME,
         api.CREATION_DATE,
         fu.USER_NAME ,
         jtrd.RATE_PERCENTAGE,
       --  jaivh.TDS_VENDOR_TYPE_LOOKUP_CODE,
           jtr.TAX_TYPE_CODE ';

ORDER_BY_STRING:='ORDER BY OOD.ORGANIZATION_name ,jtr.TAX_TYPE_CODE, pv.VENDOR_NAME';

IF p_cancelled_flag IS NOT NULL
      THEN
         SELECT    (SELECT DECODE (p_cancelled_flag,
                                   'Y', 'and ap_invoices_pkg.get_approval_status
                     (api.invoice_id,
                      api.invoice_amount,
                      api.payment_status_flag,
                      api.invoice_type_lookup_code
                     ) =',
                                   'N', 'and ap_invoices_pkg.get_approval_status
                     (api.invoice_id,
                      api.invoice_amount,
                      api.payment_status_flag,
                      api.invoice_type_lookup_code
                     ) <>'
                                  )
                      FROM DUAL)
                || ''''
                || 'CANCELLED'
                || ''''
           INTO cancelled_string
           FROM DUAL;
      END IF;

Fnd_File.put_line (Fnd_File.LOG,
                         'string: ' || cancelled_string || approved_string
                        );

---------------------------------- ------------------
Fnd_File.put_line (Fnd_File.LOG, 'executing query ');

-- making final query according to parameters


IF     p_vendor_NAME IS NOT NULL THEN

       query_string := query_string || LIST_VEN_STRING;

END IF;

IF  p_vendor_num_from IS NOT NULL AND p_vendor_num_to IS NOT NULL THEN

       query_string := query_string || RANGE_VEN_STRING   ;

END IF;

IF p_vendor_site_num IS NOT NULL THEN

       query_string :=query_string ||  VEN_SITE_CODE;

END IF;

IF  p_tds_section IS NOT NULL THEN

       query_string :=query_string || TDS_SECTION;

END IF;

IF p_inv_date_from IS NOT NULL  AND p_inv_date_to IS NOT NULL THEN

       query_string :=query_string || RANGE_INV_DATE_STRING;

END IF;



--- VALIDATION FOR ORG ID



 IF p_org_id IS NOT  NULL THEN
   --- this will satisfied if user will select organization in parameter
    LIST_ORG_ID:= ' AND    OOD.ORGANIZATION_ID = ' || p_org_id||'  ';
    Fnd_File.put_line (Fnd_File.LOG,'ORG Selected in parameter');

 ELSE
    -- this condition will execute for those responsibily having all ou access
     LIST_ORG_ID:='';
     PARA_ORG_ID  := ' AND    OOD.ORGANIZATION_ID in (' ;

      Fnd_File.put_line (Fnd_File.LOG,'Taking ALL ORGS if run for RESP where ALL OU data accessible');

     FOR K IN CUR_ORG_ID(V_RESPID) LOOP

         LIST_ORG_ID := LIST_ORG_ID ||','|| K.ORGANIZATION_ID;
         -- this flag will set if data comes from all ou
          m_ou_flag := 'Y';
     END LOOP;

     -- this will remove the first comma from the in condition
     LIST_ORG_ID:=SUBSTR(LIST_ORG_ID,2,LENGTH(LIST_ORG_ID));

     LIST_ORG_ID:=PARA_ORG_ID || LIST_ORG_ID ||') ';

     IF m_ou_flag = 'N' THEN
        -- this condition will satisfy if its not multi ou resp.

        SELECT fpop.PROFILE_OPTION_VALUE
        INTO V_ORG_ID
        FROM FND_PROFILE_OPTION_VALUES fpop,
        FND_PROFILE_OPTIONS fpo
        WHERE fpop.PROFILE_OPTION_ID =fpo.PROFILE_OPTION_ID
        AND level_value=V_RESPID --51638
        AND PROFILE_OPTION_NAME='ORG_ID';

         Fnd_File.put_line (Fnd_File.LOG,'Responsibility don''t have all ou access so taking default OU');

        LIST_ORG_ID := ' AND    OOD.ORGANIZATION_ID =   ' || V_ORG_ID ||'  ';

     END IF;

 END IF;


query_string :=
         query_string || cancelled_string || approved_string || LIST_ORG_ID
         || v_group_by || order_by_string;


-- parameter printing
BEGIN
SELECT vendor_name
INTO v_vendor_name_display
FROM po_vendors
WHERE vendor_id=p_vendor_NAME; -- p_vendor_name is passing vendor id
EXCEPTION WHEN OTHERS THEN
v_vendor_name_display:='';
END;

Fnd_File.put_line (Fnd_File.output,
                         'ABRL Zero % TDS Invoice Report'
                        );
      Fnd_File.put_line (Fnd_File.output, ' ');
      Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'As on Date'
                         || CHR (9)
                         || SYSDATE
                         || CHR (9)
                        );
      Fnd_File.put_line (Fnd_File.output, ' ');
      Fnd_File.put_line (Fnd_File.output, 'REPORT PARAMETRS');
      Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Vendor Name'
                         || CHR (9)
                         ||  v_vendor_name_display
                         || CHR (9)
                        );
      Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Vendor From'
                         || CHR (9)
                         || p_vendor_num_from
                         || CHR (9)
                        );
      Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Vendor To'
                         || CHR (9)
                         || p_vendor_num_to
                         || CHR (9)
                        );
      Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Vendor Site Code'
                         || CHR (9)
                         || p_vendor_site_num
                         || CHR (9)
                        );
      Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'TDS Section Code'
                         || CHR (9)
                         || p_tds_section
                         || CHR (9)
                        );
      Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'GL Date From'
                         || CHR (9)
                         ||  SUBSTR(p_inv_date_from,1,11)
                         || CHR (9)
                        );
      Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'GL Date To'
                         || CHR (9)
                         ||  SUBSTR(p_inv_date_to,1,11)
                         || CHR (9)
                        );
      Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'CANCELLED FLAG'
                         || CHR (9)
                         || p_cancelled_flag
                         || CHR (9)
                        );

-- display labels of the report
      Fnd_File.put_line (Fnd_File.output, ' ');
      Fnd_File.put_line (Fnd_File.output, ' ');
      Fnd_File.put_line (Fnd_File.output,
                           'Operating Unit'
                         || CHR (9)
                         || 'Vendor Number'
                         || CHR (9)
                         || 'Vendor Name'
                         || CHR (9)
                         || 'PAN Number'
                         || CHR (9)
                         ||'TDS Vendor Type'
                         || CHR(9)
                         || 'Vendor Site'
                         || CHR (9)
                         || 'Invoice Type'
                         || CHR (9)
                         || 'Invoice Number'
                         || CHR (9)
                         || 'GL Date'
                         || CHR (9)
                         || 'Invoice Amount'
                         || CHR (9)
                         || 'Invoice Line Amount'
                         || CHR (9)
                                     || 'TDS Tax Rate'
                         || CHR (9)
                                     || 'TDS Section'
                         || CHR (9)
                                     || 'TDS Tax Name'
                         || CHR (9)
                         || 'User Name'
                         || CHR (9)
                         ||'Creation date'
                         );



-- set default value 0 to all summery cols.
v_tot_inv_amt           :=0;

v_grnd_tot_inv_amt      :=0;

Fnd_File.put_line (Fnd_File.LOG,QUERY_STRING);

---------------------------------- ------------------
--Fnd_File.put_line (Fnd_File.output, 'before opening the cursor ');

       OPEN c
       FOR query_string;

      LOOP
         FETCH c
          INTO v_rec;

          EXIT WHEN c%NOTFOUND;


          ---------------------------------- ------------------
--Fnd_File.put_line (Fnd_File.output, 'executing ');
         IF NVL (v_rec_section_code, 'X') <> v_rec.v_tds_section
         THEN
            IF inv_count > 0
            THEN
               --print total vnd amt chr(9),
               Fnd_File.put_line (Fnd_File.output,
                                   'TOTAL'
                                  || CHR (9)
                                  || ' '
                                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || TO_CHAR(v_tot_inv_amt)
                                 );
            END IF;

            v_rec_section_code := v_rec.v_tds_section;


            v_tot_inv_amt      :=v_rec.v_invoice_amount;
            
            ELSE

            v_tot_inv_amt      :=v_tot_inv_amt     +   v_rec.v_invoice_amount;
            
END IF;
         -- printing the data
         ---------------------------------- ------------------
--Fnd_File.put_line (Fnd_File.output, 'printing');
         Fnd_File.put_line (Fnd_File.output,
                               v_rec.v_org_name
                            || CHR (9)
                            ||   v_rec.v_vendor_number
                            || CHR (9)
                            || v_rec.v_vendor_name
                            || CHR (9)
                            || v_rec.v_pan_no
                            || CHR (9)
                            || v_rec.v_tds_vendor_type
                            || CHR (9)
                            || v_rec.v_vendor_site_code
                            || CHR (9)
                            || v_rec.v_invoice_type
                            || CHR (9)
                            || v_rec.v_invoice_number
                            || CHR (9)
                            || v_rec.v_gl_date
                            || CHR (9)
                            || TO_CHAR(v_rec.v_invoice_amount)
                            || CHR (9)
                            || TO_CHAR(v_rec.v_invoice_line_amount)
                            || CHR (9)
                            || v_rec.v_tds_tax_rate
                            || CHR (9)
                            || v_rec.v_tds_section
                            || CHR (9)
                            || v_rec.v_tds_tax_name
                            || CHR (9)
                            || v_rec.v_user_name
                            || CHR (9)
                            || v_rec.v_creation_date
                            );
         VEN_count := VEN_count + 1;
         inv_count := inv_count + 1;

        v_grnd_tot_inv_amt      :=v_grnd_tot_inv_amt     +    v_rec.v_invoice_amount;
       
END LOOP;

        Fnd_File.put_line (Fnd_File.output,
                                     'TOTAL'
                                  || CHR (9)
                                  || ' '
                                           || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                   || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || TO_CHAR(v_tot_inv_amt)
                                );
      /* Fnd_File.put_line (Fnd_File.output,
                                     'Grand Total'
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '--to_char(v_grnd_tot_inv_amt)
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  --|| TO_CHAR(v_grnd_tot_tds_amt)
                                  --|| CHR (9)
                                 --                 || TO_CHAR(v_grnd_tot_basic)
                                 -- || CHR (9)
                                 -- || TO_CHAR(v_grnd_tot_sur)
                                 -- || CHR (9)
                                   --               || TO_CHAR(v_grnd_tot_cess)
                                --  || CHR (9)
                                --                  || TO_CHAR(v_grnd_tot_she_cess)
                                --  || CHR (9)
                                --  || TO_CHAR(v_grnd_tot_net_amt_due)
                                --  || CHR (9)
                                --  || ' '
                                --  || CHR (9)
                               --   || ' '
                                  ); */
    CLOSE c;

---------------------------------- ------------------
--Fnd_File.put_line (Fnd_File.output, 'amount closing the cursor'||v_grnd_tot_inv_amt );
END GST_ABRL_zerotds_inv_proc;
END GST_ABRL_zerotds_invoice_pkg; 
/


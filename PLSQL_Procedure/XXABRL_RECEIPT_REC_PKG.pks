CREATE OR REPLACE PACKAGE APPS.XXABRL_RECEIPT_REC_PKG AS
PROCEDURE XXABRL_RECEIPT_REC_PROC (ERRBUFF OUT VARCHAR2,
                                      RETCODE OUT NUMBER,
									                  --  P_LEDGER IN VARCHAR2,
                                      P_FROM_CUSTOMER IN VARCHAR2,
                                      P_TO_CUSTOMER IN VARCHAR2,
									                    P_LIST_OF_BANKS IN VARCHAR2,
                                      P_RECEIPT_METHOD IN VARCHAR2,
                                      P_RECEIPT_FROM_DATE IN DATE,
                                      P_RECEIPT_TO_DATE IN DATE,
                                      P_GL_FROM_DATE    IN DATE,
                                      P_GL_TO_DATE      IN DATE,
                                     -- P_BANK_ACCOUNT_NUM IN VARCHAR2,
                                      P_RECEIPT_STATUS IN VARCHAR2
                                      )  ;
                                     END XXABRL_RECEIPT_REC_PKG ;
/


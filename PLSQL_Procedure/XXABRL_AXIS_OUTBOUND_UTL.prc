CREATE OR REPLACE PROCEDURE APPS.xxabrl_axis_outbound_utl (
   errbuf    OUT   VARCHAR2,
   retcode   OUT   VARCHAR2
)
AS
   --Declaring Payment Detail Local Variables
   lv_abrl_pay_count             NUMBER             := 0;
   --initializing counter to 0
   lv_abrl_pay_amount            NUMBER             := 0;
   --initializing amount to 0
   lv_tsrl_pay_count             NUMBER             := 0;
   --initializing counter to 0
   lv_tsrl_pay_amount            NUMBER             := 0;
   --initializing amount to 0
   l_file_p                      UTL_FILE.file_type;
   l_file_p1                     UTL_FILE.file_type;
   l_pmt_data                    VARCHAR2 (4000);
   l_pmt_data1                   VARCHAR2 (4000);
   v_dbs_neft_seq                VARCHAR2 (50);
   v_dbs_neft_seq1               VARCHAR2 (50);
   v_dbs_neft_tsrl_seq           VARCHAR2 (50);
   v_dbs_neft_tsrl_seq1          VARCHAR2 (50);
   v_dbs_pay_seq                 VARCHAR2 (50);
--Declaring Advice Detail Local Variables
   lv_abrl_adv_count             NUMBER             := 0;
   --initializing counter to 0
   lv_tsrl_adv_count             NUMBER             := 0;
   --initializing counter to 0
   lv_corporate_account_number   NUMBER             := 0;
   l_file_a                      UTL_FILE.file_type;
   l_adv_data                    VARCHAR2 (6000);
   v_dbs_advice_seq              VARCHAR2 (50);
   v_dbs_advice_tsrl_seq         VARCHAR2 (50);

-- Declaring Payment Cursor to pick up only Authorized Payments
   CURSOR cur_dbs_neft (p_acc_no IN VARCHAR2)
   IS
      SELECT   *
          FROM apps.axis_payment_table
         WHERE transaction_status = 'AUTHORIZED'
           AND TRUNC (creation_date) >= TRUNC (SYSDATE) - 3
           -- this will also pick up transactions which were created 3 days prior
           AND product_code = 'N'
           AND corporate_account_number NOT IN ('811200088576')
           AND corporate_account_number = p_acc_no
      ORDER BY check_id, transaction_value_date;

--Declaring Advice Cursor to pick up invoices which are made against payments
   CURSOR cur_dbs_adv (p_acc_no VARCHAR2)                        --CUR_DBS_ADV
   IS
      SELECT   dnpi.*, dpt.vendor_code, dpt.third_party_id, dpt.primary_name,
               dpt.corporate_account_number,dpt.pay_doc_number
          FROM apps.axis_payment_invoice dnpi, apps.axis_payment_table dpt
         WHERE dnpi.check_id = dpt.check_id
           AND TRUNC (creation_date) >= TRUNC (SYSDATE) - 3
           -- this will also pick up transactions which were created 2 days prior
           AND dpt.transaction_status = 'AUTHORIZED'
           AND dpt.product_code = 'N'
           AND dpt.corporate_account_number NOT IN ('811200088576')
           AND dpt.corporate_account_number = p_acc_no
      ORDER BY dnpi.check_id;

   CURSOR cur_adv_pay_acc_no
   IS
      SELECT   dpt.corporate_account_number
          FROM apps.axis_payment_invoice dnpi, apps.axis_payment_table dpt
         WHERE dnpi.check_id = dpt.check_id
           AND TRUNC (creation_date) >= TRUNC (SYSDATE) - 3
           -- this will also pick up transactions which were created 2 days prior
           AND dpt.transaction_status = 'AUTHORIZED'
           AND dpt.product_code = 'N'
           AND dpt.corporate_account_number NOT IN ('811200088576')
      GROUP BY dpt.corporate_account_number;

--TSRL Payments and Advice Curosr -- 05 Nov 2012 testing
-- Declaring TSRL Payment Cursor to pick up only Authorized Payments
   CURSOR cur_dbs_tsrl_neft
   IS
      SELECT   *
          FROM apps.axis_payment_table
         WHERE transaction_status = 'AUTHORIZED'
           AND TRUNC (creation_date) >= TRUNC (SYSDATE) - 3
           -- this will also pick up transactions which were created 2 days prior
           AND product_code = 'N'
           AND corporate_account_number = '811200088576'
      ORDER BY check_id, transaction_value_date;

--Declaring TSRL Advice Cursor to pick up invoices which are made against payments
   CURSOR cur_dbs_tsrl_adv
   IS
      SELECT   dnpi.*, dpt.vendor_code, dpt.third_party_id, dpt.primary_name,
               dpt.corporate_account_number
          FROM apps.axis_payment_invoice dnpi, apps.axis_payment_table dpt
         WHERE dnpi.check_id = dpt.check_id
           AND TRUNC (creation_date) >= TRUNC (SYSDATE) - 3
           -- this will also pick up transactions which were created 2 days prior
           AND dpt.transaction_status = 'AUTHORIZED'
           AND dpt.product_code = 'N'
           AND dpt.corporate_account_number = '811200088576'
      ORDER BY dnpi.check_id;

-- End of TSRL Payments and Advice Curosr -- 05 Nov 2012 testing
   CURSOR trx_amount
   IS
      SELECT   COUNT (*) pay_count,
               SUM (transactional_amount) transactional_amount,
               corporate_account_number
--     INTO lv_abrl_pay_count, lv_abrl_pay_amount
      FROM     apps.axis_payment_table
         WHERE 1 = 1
           AND transaction_status = 'AUTHORIZED'
           AND TRUNC (creation_date) >= TRUNC (SYSDATE) - 3
           -- this will also pick up transactions which were created 2 days prior
           AND product_code = 'N'
           AND corporate_account_number NOT IN ('811200088576')
      GROUP BY corporate_account_number;
BEGIN
   SELECT COUNT (*), SUM (transactional_amount)
     INTO lv_abrl_pay_count, lv_abrl_pay_amount
     FROM apps.axis_payment_table
    WHERE 1 = 1
      AND transaction_status = 'AUTHORIZED'
      AND TRUNC (creation_date) >= TRUNC (SYSDATE) - 3
      -- this will also pick up transactions which were created 2 days prior
      AND product_code = 'N'
      AND corporate_account_number NOT IN ('811200088576');

   SELECT COUNT (*), SUM (transactional_amount)
     INTO lv_tsrl_pay_count, lv_tsrl_pay_amount
     FROM apps.axis_payment_table
    WHERE 1 = 1
      AND transaction_status = 'AUTHORIZED'
      AND TRUNC (creation_date) >= TRUNC (SYSDATE) - 3
      -- this will also pick up transactions which were created 2 days prior
      AND product_code = 'N'
      AND corporate_account_number IN ('811200088576');

   -- Generating a Unique Sequence Number to be concatenated in the file name
   SELECT dbs_neft_seq1.NEXTVAL || 'x' || TO_CHAR (SYSDATE, 'ddmmyyyy')
     INTO v_dbs_neft_seq1
     FROM DUAL;

   --TSRL-- Generating a Unique Sequence Number to be concatenated in TSRL file name
   SELECT dbs_neft_tsrl_seq.NEXTVAL || 'x' || TO_CHAR (SYSDATE, 'ddmmyyyy')
     INTO v_dbs_neft_tsrl_seq
     FROM DUAL;

   --TSRL-- Generating a Unique Sequence Number to be concatenated in TSRL file name
   SELECT dbs_neft_tsrl_seq1.NEXTVAL || 'x' || TO_CHAR (SYSDATE, 'ddmmyyyy')
     INTO v_dbs_neft_tsrl_seq1
     FROM DUAL;

   -- Payment File Generation
   IF lv_abrl_pay_count > 0
   --AND LV_CORPORATE_ACCOUNT_NUMBER NOT IN ('811200088576')
   THEN
      FOR xx_main IN trx_amount
      LOOP
         fnd_file.put_line (fnd_file.LOG,
                               'corporate_account_number=>'
                            || xx_main.corporate_account_number
                           );

-------------  con------------
 -- Generating a Unique Sequence Number to be concatenated in the file name
         SELECT    REVERSE (SUBSTR (REVERSE (xx_main.corporate_account_number),
                                    1,
                                    4
                                   )
                           )
                || '.'
                || dbs_neft_seq.NEXTVAL
                || 'x'
                || TO_CHAR (SYSDATE, 'ddmmyyyy')
           INTO v_dbs_neft_seq
           FROM DUAL;

         SELECT xxabrl_dbs_pay_seq.NEXTVAL
           INTO v_dbs_pay_seq
           FROM DUAL;

         --Creating a file for the below generated payment data
         l_file_p :=
            UTL_FILE.fopen ('SCBANK',
                               'cphquest2pay1_PT'
                            || TO_CHAR (SYSDATE, 'YYYYMMDD')
                            || TO_CHAR (SYSDATE, 'YY')
                            || TO_CHAR (SYSDATE, 'HH24MISS')
                            || '.TXT',
                            'W'
                           );
         UTL_FILE.put_line (l_file_p,
                               'HEADER'
                            || '|'
                            || TO_CHAR (SYSDATE, 'ddmmyyyy')
                            || '|'
                            || 'INADRE01'
                            || '|'
                            || 'ADITYA BIRLA RETAIL LIMITED'
                           );         ----  changes made by Dhiresh on 16Nov15
         l_file_p1 :=
            UTL_FILE.fopen ('SCBANK',
                            'AXIS_payment_File_Ref' || v_dbs_neft_seq
                            || '.TXT',
                            'W'
                           );
         UTL_FILE.put_line (l_file_p1,
                               'HEADER'
                            || '|'
                            || TO_CHAR (SYSDATE, 'ddmmyyyy')
                            || '|'
                            || 'INADRE01'
                            || '|'
                            || 'ADITYA BIRLA RETAIL LIMITED'
                           );         ----  changes made by Dhiresh on 16Nov15

         FOR rec_dbs_neft IN cur_dbs_neft (xx_main.corporate_account_number)
         LOOP
            fnd_file.put_line (fnd_file.LOG,
                               'Check_id:=>' || rec_dbs_neft.check_id
                              );
            l_pmt_data :=
                  'PAYMENT'                          -- Orginating Branch Code
               || '|'
               || 'BPY'
               || '|'
               || rec_dbs_neft.corporate_account_number    --Sender account no
               || '|'
               || 'INR'
               || '|'
               || NULL                                               -- Filler
               || '|'
               || 'INR'
               || '|'
--                  || null -- Filler
--                  || '|'
               || v_dbs_pay_seq                            -- PAYMENT BATCH ID
               || '|'
               || TO_CHAR (rec_dbs_neft.transaction_value_date, 'ddmmyyyy')
               --Payment date
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || REPLACE
                     (REPLACE
                         (REPLACE
                             (REPLACE
                                 (REPLACE
                                     (REPLACE
                                         (REPLACE
                                             (REPLACE
                                                 (REPLACE
                                                     (REPLACE
                                                         (REPLACE
                                                             (REPLACE
                                                                 (REPLACE
                                                                     (REPLACE
                                                                         (REPLACE
                                                                             (REPLACE
                                                                                 (SUBSTR
                                                                                     (rec_dbs_neft.primary_name,
                                                                                      0,
                                                                                      35
                                                                                     ),
                                                                                  '&',
                                                                                  ''
                                                                                 ),
                                                                              '!',
                                                                              ''
                                                                             ),
                                                                          '@',
                                                                          ''
                                                                         ),
                                                                      '#',
                                                                      ''
                                                                     ),
                                                                  '$',
                                                                  ''
                                                                 ),
                                                              '%',
                                                              ''
                                                             ),
                                                          '^',
                                                          ''
                                                         ),
                                                      '*',
                                                      ''
                                                     ),
                                                  '=',
                                                  ''
                                                 ),
                                              '{',
                                              ''
                                             ),
                                          '}',
                                          ''
                                         ),
                                      '[',
                                      ''
                                     ),
                                  ']',
                                  ''
                                 ),
                              '|',
                              ''
                             ),
                          '\',
                          ''
                         ),
                      ',',
                      ''
                     )
               --Receiver name --Changed as DBS is unable to pickup  more than 50 characters
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || 'Bene Code: '
               || rec_dbs_neft.vendor_code
               --null -- Filler -- As per Bala Sir/Rupesh mail dated July 12, 2012 6:48 PM
               || '|'
               || rec_dbs_neft.benefi_acc_num            --Receiver account no
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || rec_dbs_neft.benefi_ifsc_code                    --IFSC Code
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || TRIM (TO_CHAR (rec_dbs_neft.transactional_amount,
                                 '99999999.99'
                                )
                       )
               --Payment Amount
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || '20'                  -- Transaction code for Vendor Payment
               || '|'
               || NULL                                               -- Filler
               || '|'
               || rec_dbs_neft.pay_doc_number
               || '|'
               || rec_dbs_neft.check_id
               || '|'
               -- Payment Details -- Finance Requirement as per Hemant/Rupesh mails
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || 'E'                                --Delivery Mode as E-Mail
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || REPLACE
                     (REPLACE
                         (REPLACE
                             (REPLACE
                                 (REPLACE
                                     (REPLACE
                                         (REPLACE
                                             (REPLACE
                                                 (REPLACE
                                                     (REPLACE
                                                         (REPLACE
                                                             (REPLACE
                                                                 (REPLACE
                                                                     (REPLACE
                                                                         (REPLACE
                                                                             (REPLACE
                                                                                 (SUBSTR
                                                                                     (rec_dbs_neft.primary_name,
                                                                                      0,
                                                                                      35
                                                                                     ),
                                                                                  '&',
                                                                                  ''
                                                                                 ),
                                                                              '!',
                                                                              ''
                                                                             ),
                                                                          '@',
                                                                          ''
                                                                         ),
                                                                      '#',
                                                                      ''
                                                                     ),
                                                                  '$',
                                                                  ''
                                                                 ),
                                                              '%',
                                                              ''
                                                             ),
                                                          '^',
                                                          ''
                                                         ),
                                                      '*',
                                                      ''
                                                     ),
                                                  '=',
                                                  ''
                                                 ),
                                              '{',
                                              ''
                                             ),
                                          '}',
                                          ''
                                         ),
                                      '[',
                                      ''
                                     ),
                                  ']',
                                  ''
                                 ),
                              '|',
                              ''
                             ),
                          '\',
                          ''
                         ),
                      ',',
                      ''
                     )
               --Receiver name --Changed as DBS is unable to pickup  more than 50 characters
               || '|'
               || NULL                                       -- Address Line 1
               || '|'
               || NULL                                        --Address Line 2
               || '|'
               || NULL                                        --Address Line 3
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || 'xxxx@Axis.com'
               --Email -- Only one email address as discussed with Rupesh/Hemanth
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || NULL                                               -- Filler
               || '|'
               || 'EmailS:'
               || REPLACE (rec_dbs_neft.primary_email, '|', ';')
--PRIMARY_EMAIL -- As discussed with Rupesh on 11MAY2012     -- Advice Details of Transaction
               || ':EmailE';
--
--                  || 'Bene Code: '||REC_DBS_NEFT.VENDOR_CODE--null -- Filler -- As per Bala Sir/Rupesh mail dated July 12, 2012 6:48 PM
--                  || '|'
--                  || null -- Filler
--                  || '|'
--                  || NULL --REC_DBS_NEFT.REFERENCE_DATE -- Reference date --blank
--                  || '|'
--                  || null --REC_DBS_NEFT.CHECK_ID --blank as discussed with Hemant on 14MAY12 --Reference transaction no --blank  [REPLACE IT WITH CHECK_ID AS THERE IS NO REFERENCE TO PAYMENT ADVICE]
--                  || '|'
--                  || REC_DBS_NEFT.ATTRIBUTE1 --Acknowledgement status -- blank
--                  || '|'
--                  || REC_DBS_NEFT.ATTRIBUTE2 --Rejection code -- blank
--                  || '|'
--                  ||'|'
--                  || SUBSTR(REC_DBS_NEFT.PRIMARY_NAME,0,49) --Advice name --Changed as DBS is unable to pickup  more than 50 characters
--                  || '|'
--                  || 'Bene Code: '||REC_DBS_NEFT.VENDOR_CODE --SUBSTR(REC_DBS_NEFT.VENDOR_ADDRESS,100,49) --Address Line 3 As per Bala Sir/Rupesh mail dated July 12, 2012 6:48 PM
--                  || '|'
--                  || 'E' --Delivery Mode --For Email Value= E
--                  || '|'
--                  || 'idealmumbai@dbs.com' --Email -- Only one email address as discussed with Rupesh/Hemanth
--                  || '|'
--                  || NULL --Fax -- blank
--                  || '|'
            UTL_FILE.put_line (l_file_p, l_pmt_data);
            UTL_FILE.put_line (l_file_p1, l_pmt_data);
         END LOOP;

         UTL_FILE.put_line (l_file_p,
                               'TRAILER'
                            || '|'
                            || xx_main.pay_count
                            || '|'
                            || TRIM (TO_CHAR (xx_main.transactional_amount,
                                              '99999999999999.99'
                                             )
                                    )
                           );
         UTL_FILE.put_line (l_file_p1,
                               'TRAILER'
                            || '|'
                            || xx_main.pay_count
                            || '|'
                            || TRIM (TO_CHAR (xx_main.transactional_amount,
                                              '99999999999999.99'
                                             )
                                    )
                           );         ----  changes made by Dhiresh on 16Nov15
         UTL_FILE.fclose (l_file_p);
         UTL_FILE.fclose (l_file_p1);
      END LOOP;

      fnd_file.put_line (fnd_file.LOG, 'End ');
   END IF;

-- Payment Advice File Generation
   BEGIN
      SELECT COUNT (*)
        INTO lv_abrl_adv_count
        FROM apps.axis_payment_invoice dnpi, apps.axis_payment_table dpt
       WHERE dnpi.check_id = dpt.check_id
         AND TRUNC (creation_date) >= TRUNC (SYSDATE) - 3
         --this will also pick up transactions which were created 2 days prior
         AND dpt.transaction_status = 'AUTHORIZED'
         AND dpt.product_code = 'N'
         AND corporate_account_number NOT IN ('811200088576');

      -- Generating a Unique Sequence Number to be concatenated in the file name
--      SELECT dbs_advice_seq.NEXTVAL || 'x' || TO_CHAR (SYSDATE, 'DDMONYYYY')
--        INTO v_dbs_advice_seq
--        FROM DUAL;
      IF lv_abrl_adv_count > 0
      --AND LV_CORPORATE_ACCOUNT_NUMBER NOT IN ('811200088576')
      THEN
         FOR xx_main IN cur_adv_pay_acc_no
         LOOP
            fnd_file.put_line
                         (fnd_file.LOG,
                             'Payment Advice File corporate_account_number=>'
                          || xx_main.corporate_account_number
                         );

            SELECT    REVERSE
                           (SUBSTR (REVERSE (xx_main.corporate_account_number),
                                    1,
                                    4
                                   )
                           )
                   || '.'
                   || dbs_neft_seq.NEXTVAL
                   || 'x'
                   || TO_CHAR (SYSDATE, 'ddmmyyyy')
              INTO v_dbs_advice_seq
              FROM DUAL;

            --Creating a file for the below generated payment data
            l_file_a :=
               UTL_FILE.fopen ('SCBANK',
                                  'cphquest2pay1_PA'
                               || TO_CHAR (SYSDATE, 'YYYYMMDD')
                               || TO_CHAR (SYSDATE, 'YY')
                               || TO_CHAR (SYSDATE, 'HH24MISS')
                               || '.TXT',
                               'W'
                              );

            FOR rec_dbs_adv IN cur_dbs_adv (xx_main.corporate_account_number)
            LOOP
               fnd_file.put_line (fnd_file.LOG,
                                     'Payment Advice check_id=>'
                                  || rec_dbs_adv.check_id
                                 );
               fnd_file.put_line (fnd_file.LOG,
                                     ' rec_dbs_adv.invoice_number=>'
                                  || rec_dbs_adv.invoice_number
                                 );
               fnd_file.put_line (fnd_file.LOG,
                                     'rec_dbs_adv.invoice_date=>'
                                  || rec_dbs_adv.invoice_date
                                 );
--               fnd_file.put_line (fnd_file.LOG,
--                                     ' rec_dbs_adv.invoice_amount=>'
--                                  || rec_dbs_adv.invoice_amount
--                                 );
--               fnd_file.put_line (fnd_file.LOG,
--                                     'rec_dbs_adv.third_party_id=>'
--                                  || rec_dbs_adv.third_party_id
--                                 );
--               fnd_file.put_line (fnd_file.LOG,
--                                  ' rec_dbs_adv.tax=>' || rec_dbs_adv.tax
--                                 );
--               fnd_file.put_line (fnd_file.LOG,
--                                     ' rec_dbs_adv.net_amount=>'
--                                  || rec_dbs_adv.net_amount
--                                 );
--               fnd_file.put_line (fnd_file.LOG,
--                                     ' rec_dbs_adv.pay_type_code=>'
--                                  || rec_dbs_adv.pay_type_code
--                                 );
--               fnd_file.put_line (fnd_file.LOG,
--                                     'rec_dbs_adv.vendor_code=>'
--                                  || rec_dbs_adv.vendor_code
--                                 );
--               fnd_file.put_line (fnd_file.LOG,
--                                     'rec_dbs_adv.primary_name=>'
--                                  || rec_dbs_adv.primary_name
--                                 );
--               fnd_file.put_line (fnd_file.LOG,
--                                     'rec_dbs_adv.corporate_account_number=>'
--                                  || rec_dbs_adv.corporate_account_number
--                                 );
               UTL_FILE.put_line (l_file_a,
                                     rec_dbs_adv.check_id
                                  || '|'
                                  || rec_dbs_adv.invoice_number
                                  || '|'
                                  || rec_dbs_adv.invoice_date
                                  || '|'
                                  || TRIM
                                         (TO_CHAR (rec_dbs_adv.invoice_amount,
                                                   '99999999999999.99'
                                                  )
                                         )
                                  || '|'
                                  || rec_dbs_adv.third_party_id
                                  -- 'Location' added as per Bala Sir/Rupesh mail dated July 12, 2012 6:48 PM
                                  || '|'
                                  || TO_CHAR (rec_dbs_adv.tax, '99999999.99')
                                  || '|'
                                  || rec_dbs_adv.net_amount
                                  || '|'
                                  || rec_dbs_adv.pay_type_code
                                  || '|'
                                  || rec_dbs_adv.vendor_code
                                  -- 'Bene Code' added as per Bala Sir/Rupesh mail dated July 12, 2012 6:48 PM
                                  || '|'
                                  || rec_dbs_adv.primary_name
                                  -- Added on 08-Nov-2012 as per mail sent from DBS Bank
                                  || '|'
                                  || rec_dbs_adv.corporate_account_number
                                  || '|'
                                  || rec_dbs_adv.pay_doc_number
                                 );
            END LOOP;

            UTL_FILE.fclose (l_file_a);
         END LOOP;
      END IF;

      fnd_file.put_line (fnd_file.LOG, 'UPDATE Payment Advise');

      UPDATE apps.axis_payment_table
         SET transaction_status = 'PROCESSED',
             interface_status = 'Y'
       WHERE 1 = 1
         AND TRUNC (creation_date) >= TRUNC (SYSDATE) - 3
         AND transaction_status = 'AUTHORIZED'
         AND corporate_account_number NOT IN ('811200088576')
         AND product_code = 'N';

      COMMIT;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         UTL_FILE.fclose (l_file_a);
         fnd_file.put_line (fnd_file.LOG, 'No Data Found');
      WHEN UTL_FILE.invalid_path
      THEN
         UTL_FILE.fclose (l_file_a);
         fnd_file.put_line (fnd_file.LOG, 'Invalid Path');
      WHEN UTL_FILE.invalid_filehandle
      THEN
         UTL_FILE.fclose (l_file_a);
         fnd_file.put_line (fnd_file.LOG, 'Invalid File Handle');
      WHEN UTL_FILE.invalid_operation
      THEN
         UTL_FILE.fclose (l_file_a);
         fnd_file.put_line (fnd_file.LOG, 'Invalid Operation');
      WHEN UTL_FILE.read_error
      THEN
         UTL_FILE.fclose (l_file_a);
         fnd_file.put_line (fnd_file.LOG, 'Read Error');
      WHEN UTL_FILE.write_error
      THEN
         UTL_FILE.fclose (l_file_a);
         fnd_file.put_line (fnd_file.LOG, 'Write Error');
      WHEN UTL_FILE.internal_error
      THEN
         UTL_FILE.fclose (l_file_a);
         fnd_file.put_line (fnd_file.LOG, 'Internal Error');
      WHEN UTL_FILE.file_open
      THEN
         UTL_FILE.fclose (l_file_a);
         fnd_file.put_line (fnd_file.LOG, 'File is Open');
      WHEN UTL_FILE.invalid_filename
      THEN
         UTL_FILE.fclose (l_file_a);
         fnd_file.put_line (fnd_file.LOG, 'Invalid File Name');
      WHEN OTHERS
      THEN
         UTL_FILE.fclose (l_file_a);
         fnd_file.put_line (fnd_file.LOG,
                            'Unknown Error' || SQLERRM || ':' || SQLCODE
                           );
   END;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      UTL_FILE.fclose (l_file_p);
      fnd_file.put_line (fnd_file.LOG, 'No Data Found');
   WHEN UTL_FILE.invalid_path
   THEN
      UTL_FILE.fclose (l_file_p);
      fnd_file.put_line (fnd_file.LOG, 'Invalid Path');
   WHEN UTL_FILE.invalid_filehandle
   THEN
      UTL_FILE.fclose (l_file_p);
      fnd_file.put_line (fnd_file.LOG, 'Invalid File Handle');
   WHEN UTL_FILE.invalid_operation
   THEN
      UTL_FILE.fclose (l_file_p);
      fnd_file.put_line (fnd_file.LOG, 'Invalid Operation');
   WHEN UTL_FILE.read_error
   THEN
      UTL_FILE.fclose (l_file_p);
      fnd_file.put_line (fnd_file.LOG, 'Read Error');
   WHEN UTL_FILE.write_error
   THEN
      UTL_FILE.fclose (l_file_p);
      fnd_file.put_line (fnd_file.LOG, 'Write Error');
   WHEN UTL_FILE.internal_error
   THEN
      UTL_FILE.fclose (l_file_p);
      fnd_file.put_line (fnd_file.LOG, 'Internal Error');
   WHEN UTL_FILE.file_open
   THEN
      UTL_FILE.fclose (l_file_p);
      fnd_file.put_line (fnd_file.LOG, 'File is Open');
   WHEN UTL_FILE.invalid_filename
   THEN
      UTL_FILE.fclose (l_file_p);
      fnd_file.put_line (fnd_file.LOG, 'Invalid File Name');
   WHEN OTHERS
   THEN
      UTL_FILE.fclose (l_file_p);
      fnd_file.put_line (fnd_file.LOG, 'Unknown Error' || SQLERRM);
END xxabrl_axis_outbound_utl; 
/


CREATE OR REPLACE PROCEDURE APPS.XXABRL_NAVISION_GRN_DATE_DIFF
   (errbuf    OUT      VARCHAR2,
   retcode   OUT      VARCHAR2)
IS
BEGIN

/**********************************************************************************************************************************************
                        WIPRO Infotech Ltd, Mumbai, India

            Name        :GRN Date Exception Report

            Change Record:
           =========================================================================================================================
           Version   Date          Author               Remarks                  Documnet Ref
           =======   ==========   =============        ============================================================================
           1.0.0     13-Jul-2011    Mitul     Initial Version
 **********************************************************************************************************************************************     */
DECLARE
 v_email_subject   VARCHAR2 (200);
   v_email_list      VARCHAR2 (200);
   v_errmsg          VARCHAR2 (200);
   v_status_code     VARCHAR2 (200);
   mailhost          VARCHAR2 (40)       := 'mail.adityabirla.com';
   crlf              VARCHAR2 (2)        := CHR (13) || CHR (10);
   v_email           VARCHAR2 (16000);
   message_1         varchar2(30000);
   message_2         VARCHAR2 (30000);
   message_3         VARCHAR2 (30000);
   mail_conn         UTL_SMTP.connection;
   v_from_mail       VARCHAR2 (50)       := 'OFIN SUPPORT';
   i integer:=0;
   j integer:=0;
    SRC VARCHAR2(100);
OU VARCHAR2(50);
VEN_NUM VARCHAR2(100);
INV_NUM varchar2(1000);
INV_DATE VARCHAR2(150);
INV_AMT VARCHAR2(150);
begin 
 message_1 :='<html>
          <body>
          <font face=''Calibri''>
          Dear '||'All'||',
          <br/>
           <br/>
          <b>
          Below are the Invoices with Multiple GRN with Difference of > 3 days 
                    <br/>
          <br/>
          </font>
           <table Border=''1'' autosize=''true''>
           <font face=''Calibri'' color=''white''>
          <Tr bgcolor=''gray'' >
          <td nowrap=''nowrap'' ALIGN=''left'' color=''white''><b>SOURCE</b></td>
          <td nowrap=''nowrap'' ALIGN=''left''><b>OPERATING_UNIT</b></td>
          <td nowrap=''nowrap'' ALIGN=''left'' color=''white''><b>VENDOR_NUMBER</b></td>
          <td nowrap=''nowrap'' ALIGN=''left''><b>VENDOR DC CODE</b></td>
          <td nowrap=''nowrap'' ALIGN=''left''><b>VENDOR NAME</b></td>
          <td nowrap=''nowrap'' ALIGN=''left''><b>INVOICE_NUM</b></td>
          <td nowrap=''nowrap'' ALIGN=''left''><b>INVOICE_AMOUNT</b></td>
          <td nowrap=''nowrap'' ALIGN=''left''><b>GRN 1 DATE</b></td>
          <td nowrap=''nowrap'' ALIGN=''left''><b>GRN 2 DATE</b></td>
          <td nowrap=''nowrap'' ALIGN=''left''><b>GRN 3 DATE</b></td>
          <td nowrap=''nowrap'' ALIGN=''left''><b>GRN 4 DATE</b></td>
          <td nowrap=''nowrap'' ALIGN=''left''><b>DATE1-DATE2</b></td>
          <td nowrap=''nowrap'' ALIGN=''left''><b>DATE1-DATE3</b></td>
          <td nowrap=''nowrap'' ALIGN=''left''><b>DATE1-DATE4</b></td>
                             </font>
          </tr>
          <font face=''Calibri''>';
       message_3 :='
          </font>
          </table>
              <br/>
            <!--                          <font face=''Calibri'' color=''black'' bold=''true''>
          This is auto generated mail by the system, Please do not reply or respond    
                         .
          </font>
          -->
            <br/><br/>
          <font face=''Calibri'' color=''Lime Green'' bold=''true''>
          <b>Thanks and Regards</b>,
          <br/>
          <b>OFIN Support</b>
          </font>
          </body>
          </html>';
          
for sel_grn in (


SELECT * FROM(
select
SOURCE,
OPERATING_UNIT,
VENDOR_NUMBER,
VENDOR_DC_CODE,
VENDOR_NAME,
INVOICE_NUM,
INVOICE_DATE,
INVOICE_AMOUNT, 
DATE1,DATE2,DATE3,DATE4,
TO_DATE(AA.DATE2)-TO_DATE(AA.DATE1) diff1,
TO_DATE(AA.DATE3)-TO_DATE(AA.DATE1) diff2,
TO_DATE(AA.DATE4)-TO_DATE(AA.DATE1) diff3
from(
select
SOURCE,
OPERATING_UNIT,
VENDOR_NUMBER,
VENDOR_DC_CODE,
ER_VENDOR_NAME VENDOR_NAME,
INVOICE_NUM,
INVOICE_DATE,
INVOICE_AMOUNT,
substr(ATTRIBUTE2,0,instr(ATTRIBUTE2,'|')-1) date1,
substr(ATTRIBUTE2,13,instr(ATTRIBUTE2,'|')-1) date2,
substr(ATTRIBUTE2,25,instr(ATTRIBUTE2,'|')-1) date3,
substr(ATTRIBUTE2,37,instr(ATTRIBUTE2,'|')-1) date4 
from APPS.xxabrl_ap_invoices_int A,APPS.XXABRL_AP_VENDOR_MAP_INT B
where 1=1
AND A.VENDOR_NUMBER=B.ER_VENDOR_NUMBER
AND A.VENDOR_DC_CODE=B.ER_DC_CODE
AND A.ORG_ID =B.OF_ORGANIZATION_ID
and ATTRIBUTE1 is not null
and ATTRIBUTE2 is not null
and SOURCE='NAVISION'
--and TRUNC(A.creation_date) ='04-JUN-2011'
and ATTRIBUTE2 like '%|%'
and TRUNC(A.creation_date) = TRUNC(SYSDATE)
--and invoice_num= 'HM_1001344022'
) AA)BB
WHERE (BB.DIFF1>3 OR BB.DIFF2>3  OR BB.DIFF3>3  )


)






        loop
        
        i:=i+1;
        

        --if (to_date(trim(sel_grn.date1)) - to_date(trim(sel_grn.date2))) >=3 or (to_date(trim(sel_grn.date1)) - to_date(trim(sel_grn.date3))) >=3 then
  
            SRC:= SRC|| sel_grn.invoice_num; 
            OU:=  OU|| sel_grn.invoice_num; 
            VEN_NUM:= VEN_NUM|| sel_grn.invoice_num;
            INV_NUM:= INV_NUM|| sel_grn.invoice_num;
            INV_DATE:=INV_DATE|| sel_grn.invoice_num;
            INV_AMT:= INV_AMT|| sel_grn.invoice_num;
                
        --if (to_date(trim(sel_grn.date2)) - to_date(trim(sel_grn.date1))) >=3 or (to_date(trim(sel_grn.date3)) - to_date(trim(sel_grn.date1))) >=3 then            
            
            
            message_2:= message_2 ||'</tr>
                              <font face=''Calibri''>
                              <Tr>
                              <td nowrap=''nowrap'' ALIGN=''left''>'||sel_grn.SOURCE||'</td>
                              <td nowrap=''nowrap'' ALIGN=''left''>'||sel_grn.OPERATING_UNIT||'</td>
                              <td  nowrap=''nowrap'' ALIGN=''left''>'||sel_grn.VENDOR_NUMBER||'</td>
                              <td nowrap=''nowrap'' ALIGN=''left''>'||sel_grn.VENDOR_DC_CODE||'</td>
                              <td nowrap=''nowrap'' ALIGN=''left''>'||sel_grn.VENDOR_NAME||'</td>
                              <td nowrap=''nowrap'' ALIGN=''left''>'||sel_grn.INVOICE_NUM||'</td>
                              <td nowrap=''nowrap'' ALIGN=''left''>'||sel_grn.INVOICE_AMOUNT||'</td>
                              <td nowrap=''nowrap'' ALIGN=''left''>'||sel_grn.date1||'</td>
                              <td nowrap=''nowrap'' ALIGN=''left''>'||sel_grn.date2||'</td>
                              <td nowrap=''nowrap'' ALIGN=''left''>'||sel_grn.date3||'</td>
                              <td nowrap=''nowrap'' ALIGN=''left''>'||sel_grn.date4||'</td>
                              <td nowrap=''nowrap'' ALIGN=''left''>'||sel_grn.diff1||'</td>
                              <td nowrap=''nowrap'' ALIGN=''left''>'||sel_grn.diff2||'</td>
                              <td nowrap=''nowrap'' ALIGN=''left''>'||sel_grn.diff3||'</td>
                              </tr>';
       -- end if;                   
    
    END LOOP;          
    
    if i>=1 then  
     
    begin
        
         
                    mail_conn := utl_smtp.open_connection (mailhost, 25);
                    utl_smtp.helo (mail_conn, mailhost);
                    utl_smtp.mail (mail_conn, v_from_mail);
                    utl_smtp.rcpt (mail_conn, 'srinivas.thalla-v@retail.adityabirla.com');
                    utl_smtp.rcpt (mail_conn, 'balasubramanian.sk@retail.adityabirla.com');
                    utl_smtp.open_data(mail_conn);
                    UTL_SMTP.write_data(mail_conn, 'Date: ' || TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') || utl_tcp.crlf); 
                    utl_smtp.write_data(mail_conn, 'From: ' || v_from_mail || utl_tcp.crlf);
                    utl_smtp.write_data(mail_conn, 'To: '   || substr('srinivas.thalla-v@retail.adityabirla.com',0,instr('srinivas.thalla-v@retail.adityabirla.com','@')-1) || utl_tcp.crlf);
                    --utl_smtp.write_data(mail_conn, 'Sent: '  || TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') || utl_tcp.crlf);
                    utl_smtp.write_data(mail_conn, 'Subject: ' || 'Invoices with Multiple GRN Exception Mailer' || utl_tcp.crlf);
                    utl_smtp.write_data(mail_conn, 'Content-Type: text/html' || utl_tcp.crlf);
                    utl_smtp.write_data(mail_conn, utl_tcp.crlf || message_1 || message_2 || message_3);
                    utl_smtp.close_data(mail_conn);
                    utl_smtp.quit(mail_conn);  
         
       

   end;
   
   end if;

END;
END XXABRL_NAVISION_GRN_DATE_DIFF; 
/


CREATE OR REPLACE PROCEDURE APPS.xxabrl_ipro_menu_enable (
   errbuf    OUT   VARCHAR2,
   retcode   OUT   NUMBER
)
IS
   lv_rowid   ROWID;

   CURSOR c1
   IS
      SELECT   abc.responsibility_id, application_id, responsibility_name,
               action_id, menu_name, user_menu_name, abc.description,
               abc.last_update_date, rule_type, status, fu.user_name
          FROM xxabrl.xxabrl_iproc_resp_menu_remov abc,
               fnd_user_resp_groups_direct furg,
               fnd_user fu
         WHERE 1 = 1
           AND abc.responsibility_id = furg.responsibility_id
           AND furg.user_id = fu.user_id
           AND fu.end_date IS NULL
           AND furg.end_date IS NULL
           AND fu.user_name = '1000623'
      GROUP BY abc.responsibility_id,
               application_id,
               responsibility_name,
               action_id,
               menu_name,
               user_menu_name,
               abc.description,
               abc.last_update_date,
               rule_type,
               status,
               fu.user_name;
       
BEGIN
   NULL;

   FOR i IN c1
   LOOP
      fnd_resp_functions_pkg.insert_row (lv_rowid,
                                         i.application_id,
                                         i.responsibility_id,
                                         i.action_id,
                                         i.rule_type,
                                         0,
                                         SYSDATE,
                                         0,
                                         SYSDATE,
                                         USERENV ('SESSIONID')
                                        );

      UPDATE xxabrl.xxabrl_iproc_resp_menu_remov
         SET last_update_date = SYSDATE,
             status = 'Inc'
       WHERE application_id = i.application_id
         AND responsibility_id = i.responsibility_id
         AND rule_type = i.rule_type
         AND action_id = i.action_id;
   END LOOP;

   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      fnd_file.put_line (fnd_file.LOG, 'Error in Staging Table');
END; 
/


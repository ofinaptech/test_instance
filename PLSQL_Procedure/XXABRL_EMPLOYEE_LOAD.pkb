CREATE OR REPLACE PACKAGE BODY APPS.xxabrl_employee_load IS

  PROCEDURE create_employee(Errbuf OUT VARCHAR2, RetCode OUT NUMBER) AS

    CURSOR emp IS

      SELECT a.ROWID, HIRE_DARE, LAST_NAME, GENDER,MAIL_ID, EMPLOYEE_NUMBER,a.DOB dob
        FROM xxabrl.xx_hr_emp_stg A
       WHERE NVL(a.PROCESS_FLAG, 'X') IN ('N','E');

    --and trim(prm_token_no)='C1021';
    --and trim(prm_token_no)='62228';

    v_person_type_id          NUMBER;
    v_title                   VARCHAR2(10);
    v_nationality             VARCHAR2(10) := 'IND';
    v_error_code              VARCHAR2(30);
    v_err_message             VARCHAR2(500);
    v_err_flag                VARCHAR2(1) := 'N';
    v_total_processed_records NUMBER := 0;
    v_total_reject_records    NUMBER := 0;
    v_total_load_records      NUMBER := 0;
    --out

    v_person_id                NUMBER;
    v_assignment_id            NUMBER;
    v_per_obj_version_number   NUMBER;
    v_asg_obj_version_number   NUMBER;
    v_per_effective_start_date DATE;
    v_per_effective_end_date   DATE;
    v_full_name                VARCHAR2(300);
    v_per_comment_id           NUMBER;
    v_assignment_sequence      NUMBER;
    v_assignment_number        VARCHAR2(300);
    v_name_combination_warning BOOLEAN;
    v_assign_payroll_warning   BOOLEAN;
    v_orig_hire_warning        BOOLEAN;

    --out for assignment updation

    l_assign_obj_no NUMBER;

    --l_assign_obj_no number;
    l_special_ceiling_step_id    NUMBER;
    l_group_name                 VARCHAR2(500);
    l_effective_start_date       DATE;
    l_effective_end_date         DATE;
    l_org_now_no_manager_warning BOOLEAN;
    l_other_manager_warning      BOOLEAN;
    l_spp_delete_warning         BOOLEAN;
    l_entries_changed_warning    VARCHAR2(400);
    l_tax_dist_changed_warning   BOOLEAN;

  BEGIN

    FOR emp_var IN emp LOOP

	DBMS_OUTPUT.PUT_LINE('DOB ---  '||emp_var.DOB);
      v_err_flag           := 'N';
      v_total_load_records := v_total_load_records + 1;

      --v_prm_token_no:=trim(emp_var.PRM_TOKEN_NO);

      IF emp_var.GENDER = 'M' THEN

        v_title := 'MR.';

      ELSIF emp_var.GENDER = 'F' THEN

        v_title := 'MS.';

      END IF;

      IF emp_var.LAST_NAME IS NULL THEN

        v_err_flag := 'Y';

        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                          ' Employee Name cannot be NULL for Employee :- ' ||
                          emp_var.EMPLOYEE_NUMBER ||
                          '    Employee Name cannot be NULL');
        v_err_message := 'Employee Name cannot be NULL for Employee';
      END IF;

      IF emp_var.EMPLOYEE_NUMBER IS NULL THEN

        v_err_flag := 'Y';
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                          'Employee Number is NULL for  :- ' ||
                          emp_var.LAST_NAME ||
                          '    Employee Number is NULL');
        v_err_message := 'Employee Number is NULL';
      END IF;

      IF EMP_VAR.DOB IS NULL THEN

        v_err_flag := 'Y';
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                          'Date of Birth is NULL for Employee :- ' ||
                          emp_var.EMPLOYEE_NUMBER || '    ');
        v_err_message := 'Date of Birth is NULL';
      END IF;

      IF emp_var.GENDER IS NULL THEN

        v_err_flag := 'Y';
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                          'Employee Gender is NULL for  :- ' ||
                          emp_var.EMPLOYEE_NUMBER || '    Gender is NULL');
        v_err_message := 'Employee Gender is NULL';
      END IF;

      IF v_err_flag = 'N' THEN

        BEGIN

          hr_employee_api.create_employee(p_validate                  => FALSE,
                                          p_hire_date                 => TO_DATE(emp_var.HIRE_DARE,
                                                                                 'DD-MM-rrrr'),
                                          p_business_group_id         => 81,
                                          p_last_name                 => emp_var.LAST_NAME,
                                          p_sex                       => emp_var.GENDER,
                                          p_person_type_id            => 1120,
                                          p_date_of_birth             => emp_var.DOB,
                                          p_email_address             => emp_var.MAIL_ID,
                                          p_employee_number           => emp_var.EMPLOYEE_NUMBER,
                                          p_title                     => v_title,
                                          p_person_id                 => v_person_id,
                                          p_assignment_id             => v_assignment_id,
                                          p_per_object_version_number => v_per_obj_version_number --       out nocopy number
                                         ,
                                          p_asg_object_version_number => v_asg_obj_version_number --       out nocopy number
                                         ,
                                          p_per_effective_start_date  => v_per_effective_start_date --        out nocopy date
                                         ,
                                          p_per_effective_end_date    => v_per_effective_end_date --          out nocopy date
                                         ,
                                          p_full_name                 => v_full_name --                       out nocopy varchar2
                                         ,
                                          p_per_comment_id            => v_per_comment_id --                  out nocopy number
                                         ,
                                          p_assignment_sequence       => v_assignment_sequence --             out nocopy number
                                         ,
                                          p_assignment_number         => v_assignment_number --               out nocopy varchar2
                                         ,
                                          p_name_combination_warning  => v_name_combination_warning --        out nocopy boolean
                                         ,
                                          p_assign_payroll_warning    => v_assign_payroll_warning --          out nocopy boolean
                                         ,
                                          p_orig_hire_warning         => v_orig_hire_warning --               out nocopy boolean
                                          );

          DBMS_OUTPUT.PUT_LINE('PERSON_ID ' || v_person_id);
          DBMS_OUTPUT.PUT_LINE('OVN ' || v_per_obj_version_number);
          DBMS_OUTPUT.PUT_LINE('FULL NAME  ' || v_full_name);
          FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                            'Employee Data Uploaded Successfully for employee :' ||
                            emp_var.EMPLOYEE_NUMBER);

          v_total_processed_records := v_total_processed_records + 1;

          UPDATE xxabrl.xx_hr_emp_stg
             SET PROCESS_FLAG = 'Y'
           WHERE ROWID = emp_var.ROWID;

          COMMIT;

        EXCEPTION
          WHEN OTHERS THEN
            FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                              'ERROR Occure While loading data  thru API ' ||
                              SUBSTR(SQLERRM, 1, 500));


            v_err_message := 'ERROR Occure While loading data  thru API ' ||
                             SUBSTR(SQLERRM, 1, 500);

				DBMS_OUTPUT.PUT_LINE('Error for '||emp_var.LAST_NAME ||' dob '||emp_var.DOB||'Error - '||v_err_message);
				DBMS_OUTPUT.PUT_LINE('hIRE dATE '||TO_DATE(emp_var.HIRE_DARE,'DD-MM-YYYY'));
            UPDATE xxabrl.xx_hr_emp_stg
               SET process_flag = 'E', error_message = v_err_message
             WHERE ROWID = emp_var.ROWID;
            COMMIT;
        END;

      ELSE
        UPDATE xxabrl.xx_hr_emp_stg
           SET process_flag = 'E', error_message = v_err_message
         WHERE ROWID = emp_var.ROWID;
        COMMIT;
      END IF;

    END LOOP;

    v_total_reject_records := v_total_load_records -
                              v_total_processed_records;

    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                      'Total no. employees for process  :' ||
                      v_total_load_records);
    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                      'Total no. Of employees processed :' ||
                      v_total_processed_records);
    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                      'Total no. Of employees Rejected  :' ||
                      v_total_reject_records);

  END create_employee;

END xxabrl_employee_load;
/


CREATE OR REPLACE PACKAGE BODY APPS.XXABRL_AR_TRANS_PKG AS
/*
   Version     date             Name                   Changes  
                --                                  Previous Version
    1.0.1    31-JUL-2010      Sayikrishna            Duplicate Records Restricted..Commented Party_sites
                                                     Order bu GL date added.
*/
PROCEDURE  XXABRL_AR_TRANS_PROC (ERRBUFF OUT VARCHAR2,
                                    RETCODE OUT number,
                                    P_FROM_CUSTOMER  VARCHAR2,
                                    P_TO_CUSTOMER   VARCHAR2,
                                    P_LIST_CUSTOMER  VARCHAR2,
                                    P_TRX_FROM_NUM VARCHAR2,
                                    P_TRX_TO_NUM  VARCHAR2,
                                    P_TRX_FROM_DATE  DATE,
                                    P_TRX_TO_DATE DATE,
                                    P_TRX_TYPE VARCHAR2,
                                    P_TRX_STATUS VARCHAR2                                                                 
                                    ) IS
                                    
                                   
            QUERY_STRING  LONG;
            LIST_CUST_STRING  LONG;
            RANGE_CUST_STRING LONG;
            RANGE_TRX_DATE_STRING LONG;
            TRX_DATE_STRING LONG;
            ORDER_BY_STRING LONG;
            TRX_NUM_STRING LONG;
            TRX_TYPE_STRING LONG;
            TRX_STATUS_STRING LONG;

            v_total_TRX_amt  number(30,2);
            v_grand_total_TRX_amt  number(30,2);
            V_REC_CUSTOMER    VARCHAR(250);
            cust_count        number(15);
 
 
 
 TYPE REF_CUR IS REF CURSOR ;
C REF_CUR;

TYPE C_REC IS RECORD(
V_SOURCE_NAME            RA_CUSTOMER_TRX_PARTIAL_V.BS_BATCH_SOURCE_NAME%TYPE,
V_TRX_NUMBER             RA_CUSTOMER_TRX_PARTIAL_V.TRX_NUMBER%TYPE,
V_DOCUMENT_NUMBER        RA_CUSTOMER_TRX_PARTIAL_V.DOC_SEQUENCE_VALUE%TYPE,
V_TRX_CLASS              AR_LOOKUPS.MEANING%TYPE,
V_TRX_TYPE               RA_CUSTOMER_TRX_PARTIAL_V.CTT_TYPE_NAME%TYPE,
V_TRX_DATE               RA_CUSTOMER_TRX_PARTIAL_V.TRX_DATE%TYPE,
V_GL_DATE                RA_CUSTOMER_TRX_PARTIAL_V.GD_GL_DATE %TYPE ,                        
V_CUSTOMER_NUM           RA_CUSTOMER_TRX_PARTIAL_V.RAC_BILL_TO_CUSTOMER_NUM%TYPE,
V_CUSTOMER_NAME          RA_CUSTOMER_TRX_PARTIAL_V.RAC_BILL_TO_CUSTOMER_NAME%TYPE,
V_AMOUNT                 AR_PAYMENT_SCHEDULES_ALL.AMOUNT_DUE_ORIGINAL%TYPE,
V_TRX_STATUS             AR_LOOKUPS.MEANING%TYPE
);

V_REC C_REC;
 
 BEGIN

 QUERY_STRING:='select 
          rctp.bs_batch_source_name 
         ,rctp.trx_number
         ,rctp.doc_sequence_value
         ,al1.meaning   trxaction_class
         ,rctp.ctt_type_name
         ,rctp.trx_date
         ,rctp.gd_gl_date
         ,rctp.rac_bill_to_customer_num
         ,rctp.rac_bill_to_customer_name
         ,aps.amount_due_original
         ,al2.meaning   transaction_status
     from ra_customer_trx_partial_v rctp
         ,ar_payment_schedules_all aps
         ,hz_cust_accounts_all hca 
         ,hz_parties  hp
         --,hz_party_sites hs -- Commented Sai30JUN10. Party Site Table not required here 
         ,ar_lookups al1
         ,ar_lookups al2
   where rctp.bill_to_customer_id=hca.cust_account_id
   and   rctp.customer_trx_id=aps.customer_trx_id
   and   aps.customer_id=hca.cust_account_id
   and   aps.customer_id=rctp.bill_to_customer_id
   and   hp.party_id=hca.party_id
   and   al1.lookup_code=rctp.ctt_class
   and   al1.lookup_type=''INV/CM''
   and   al2.lookup_code=rctp.STATUS_TRX
   and   al2.lookup_type=''INVOICE_TRX_STATUS''';

      
        LIST_CUST_STRING:= 'AND rctp.RAC_BILL_TO_CUSTOMER_NUM IN ('||P_LIST_CUSTOMER||') ' ;
        RANGE_CUST_STRING:= 'AND rctp.RAC_BILL_TO_CUSTOMER_NUM between '''||P_FROM_CUSTOMER||''' AND '''||P_TO_CUSTOMER||''' ';
        RANGE_TRX_DATE_STRING:='AND rctp.TRX_DATE  BETWEEN '''||P_TRX_FROM_DATE||''' AND '''||P_TRX_TO_DATE||''' ';
        TRX_DATE_STRING:='AND rctp.TRX_DATE =rctp.TRX_DATE ';
        TRX_NUM_STRING:='AND rctp.TRX_NUMBER BETWEEN '''||P_TRX_FROM_NUM||''' AND '''||P_TRX_TO_NUM||''' ';
        TRX_TYPE_STRING:='AND rctp.CTT_TYPE_NAME='''||P_TRX_TYPE||''' ';
        TRX_STATUS_STRING:='AND AL2.MEANING='''||P_TRX_STATUS||''' ';

        ORDER_BY_STRING:='order by rctp.rac_bill_to_customer_name,rctp.gd_gl_date';
      
      
      IF (P_FROM_CUSTOMER IS NULL and P_TO_CUSTOMER IS NULL) AND P_LIST_CUSTOMER IS NOT NULL
       THEN
        QUERY_STRING:= QUERY_STRING || LIST_CUST_STRING;
      ELSIF 
        (P_FROM_CUSTOMER IS NOT NULL and P_TO_CUSTOMER IS NOT NULL) AND P_LIST_CUSTOMER IS NULL 
      THEN
        QUERY_STRING:= QUERY_STRING || RANGE_CUST_STRING;
      ELSIF (P_FROM_CUSTOMER IS NOT NULL and P_TO_CUSTOMER IS NOT NULL)  AND P_LIST_CUSTOMER IS NOT NULL THEN
        QUERY_STRING:= QUERY_STRING || LIST_CUST_STRING;
     END IF;

     IF (P_TRX_FROM_DATE IS NOT NULL and P_TRX_TO_DATE IS NOT NULL) THEN
        QUERY_STRING:= QUERY_STRING ||RANGE_TRX_DATE_STRING;
       ELSIF (P_TRX_FROM_DATE  IS  NULL and P_TRX_TO_DATE IS NULL) THEN
        QUERY_STRING:= QUERY_STRING ||TRX_DATE_STRING;
     END IF;  

     IF P_TRX_TYPE  IS NOT NULL THEN
        QUERY_STRING:= QUERY_STRING ||TRX_TYPE_STRING;
     END IF;
     
    IF P_TRX_FROM_NUM IS NOT NULL and P_TRX_TO_NUM IS NOT NULL THEN
        QUERY_STRING:= QUERY_STRING || TRX_NUM_STRING;  
    END IF;
   
        
    IF P_TRX_STATUS IS NOT NULL THEN
        QUERY_STRING:= QUERY_STRING ||TRX_STATUS_STRING;
    END IF; 
   

         Fnd_File.put_line(Fnd_File.OUTPUT,'ABRL AR TRANSACTION REGISTER REPORT');
         Fnd_File.put_line(Fnd_File.OUTPUT,' ');
        
         Fnd_File.put_line(Fnd_File.OUTPUT,
                      CHR(9) || CHR(9) || CHR(9) || 'As on Date' || CHR(9) ||
                      sysdate || CHR(9));
         Fnd_File.put_line(Fnd_File.OUTPUT,' ');
         Fnd_File.put_line(Fnd_File.OUTPUT,'REPORT PARAMETRS ');               
    
         Fnd_File.put_line(Fnd_File.OUTPUT,
                      CHR(9) || CHR(9) || CHR(9) || ' Customer Low' || CHR(9) ||
                      P_FROM_CUSTOMER|| CHR(9));
         Fnd_File.put_line(Fnd_File.OUTPUT,
                      CHR(9) || CHR(9) || CHR(9) || ' Customer High' || CHR(9) ||
                      P_TO_CUSTOMER|| CHR(9));
         Fnd_File.put_line(Fnd_File.OUTPUT,
                      CHR(9) || CHR(9) || CHR(9) || ' Customer List' || CHR(9) ||
                      P_LIST_CUSTOMER || CHR(9));
         Fnd_File.put_line(Fnd_File.OUTPUT,
                      CHR(9) || CHR(9) || CHR(9) || 'TRX Number Low' || CHR(9) ||
                      P_TRX_FROM_NUM || CHR(9));
         Fnd_File.put_line(Fnd_File.OUTPUT,
                      CHR(9) || CHR(9) || CHR(9) || 'TRX Number High' || CHR(9) ||
                      P_TRX_TO_NUM || CHR(9));
         Fnd_File.put_line(Fnd_File.OUTPUT,
                      CHR(9) || CHR(9) || CHR(9) || 'TRX Date Low' || CHR(9) ||
                      P_TRX_FROM_DATE|| CHR(9));
         Fnd_File.put_line(Fnd_File.OUTPUT,
                      CHR(9) || CHR(9) || CHR(9) || 'TRX Date High' || CHR(9) ||
                      P_TRX_TO_DATE|| CHR(9));
         Fnd_File.put_line(Fnd_File.OUTPUT,
                      CHR(9) || CHR(9) || CHR(9) || 'TRX TYPE' || CHR(9) ||
                     P_TRX_TYPE|| CHR(9));                           
         Fnd_File.put_line(Fnd_File.OUTPUT,
                      CHR(9) || CHR(9) || CHR(9) || 'TRX Status' || CHR(9) ||
                       P_TRX_STATUS || CHR(9));
                       
      
         
         Fnd_File.put_line(Fnd_File.OUTPUT,' ');
         Fnd_File.put_line(Fnd_File.OUTPUT,' ');

         fnd_File.put_line(Fnd_File.OUTPUT,
                          'Source'          || chr(9) ||
                          'Transaction Number'          || chr(9) ||
                          'Document Number'      || chr(9) ||
                          'Transaction Class'        || chr(9) ||
                          'Transaction Type'        || chr(9) ||
                          'Transaction Date'       || chr(9) ||
                          'GL Date'    || CHR(9) ||
                          'Customer Number'         || CHR(9) ||
                          'Customer Name'         || CHR(9) ||
                          'Amount'         || CHR(9) ||
                          'Status'         || CHR(9) 
                          );
            
            
         QUERY_STRING:=QUERY_STRING||ORDER_BY_STRING;

         Fnd_File.put_line(Fnd_File.log,'string: '||QUERY_STRING);
 
        v_total_trx_amt:=0;

 
        v_grand_total_trx_amt:=0;
 
 
        cust_count:=0;
 
     

    OPEN C FOR QUERY_STRING;

          LOOP
                FETCH C INTO V_REC;
                EXIT WHEN c%NOTFOUND;
       
             if nvl(V_REC_CUSTOMER,'X') <> V_REC.V_CUSTOMER_NAME  then
                if cust_count >0 then

                 Fnd_File.put_line(Fnd_File.OUTPUT,
                      ' '  || CHR(9) ||
                      ' '  || CHR(9) ||
                      ' '  || CHR(9) ||
                      ' '  || CHR(9) ||
                      ' '  || CHR(9) ||
                      ' '  || CHR(9) ||
                      ' '  || CHR(9) ||
                      ' '  || CHR(9) ||
                      'Customer Total Amount'||' '|| CHR(9) ||
                    v_total_trx_amt || CHR(9)||
                      ' ' || CHR(9)              
                       );

               
               end if;
                      
                v_rec_customer:= v_rec.v_customer_name;
                    v_total_trx_amt:=v_rec.v_amount ;
                 else
                v_total_trx_amt:=v_total_trx_amt+v_rec.v_amount;

     
             End if;
                  
              
            Fnd_File.put_line(Fnd_File.OUTPUT,
                      V_REC.V_SOURCE_NAME  || CHR(9) ||
                      V_REC.V_TRX_NUMBER  || CHR(9) ||
                      V_REC.V_DOCUMENT_NUMBER|| CHR(9) ||
                      V_REC.V_TRX_CLASS  || CHR(9) ||
                      V_REC.V_TRX_TYPE || CHR(9) ||
                      V_REC.V_TRX_DATE   || CHR(9) ||
                      V_REC.V_GL_DATE  || CHR(9) ||
                      V_REC.V_CUSTOMER_NUM    || CHR(9) ||
                      V_REC.V_CUSTOMER_NAME  || CHR(9)||
                      V_REC.V_AMOUNT || CHR(9)||
                      V_REC.V_TRX_STATUS || CHR(9)              
                       );

               cust_count:=cust_count+1;
             
             v_grand_total_trx_amt:=v_grand_total_trx_amt+v_rec.v_amount;
               
             end loop;
          
               
          Fnd_File.put_line(Fnd_File.OUTPUT,
                      ' '  || CHR(9) ||
                      ' '  || CHR(9) ||
                      ' ' || CHR(9) ||
                      ' '  || CHR(9) ||
                      ' ' || CHR(9) ||
                      ' '   || CHR(9) ||
                      ' '  || CHR(9) ||
                      ' '  || CHR(9)||'Customer Total Amount'||' '|| CHR(9) ||
              v_total_trx_amt || chr(9)||
              ' ' || CHR(9)              
                       );
             
              Fnd_File.put_line(Fnd_File.OUTPUT,
                      ' '  || CHR(9) ||
                      ' '  || CHR(9) ||
                      ' '|| CHR(9) ||
                      ' '  || CHR(9) ||
                      ' ' || CHR(9) ||
                      ' '   || CHR(9) ||
                      ' '  || CHR(9) ||
                      ' '    || CHR(9) ||'Grand Total Amount'||
                       ' '  || CHR(9)||
             v_grand_total_TRX_amt || CHR(9)||
              ' ' || CHR(9)              
                       );
             
                        
    CLOSE C;
               
 END XXABRL_AR_TRANS_PROC;
END XXABRL_AR_TRANS_PKG; 
/


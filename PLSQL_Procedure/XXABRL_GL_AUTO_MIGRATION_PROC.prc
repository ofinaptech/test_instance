CREATE OR REPLACE PROCEDURE APPS.XXABRL_GL_AUTO_MIGRATION_PROC(ERRBUFF OUT VARCHAR2,
                                RETCODE OUT NUMBER,
                                P_FROM_SBU NUMBER,
                                P_TO_SBU  NUMBER,
                                P_FROM_LOCATION NUMBER,
                                P_TO_LOCATION NUMBER,
                                P_FROM_ACCOUNT NUMBER,
                                P_TO_ACCOUNT NUMBER,
                                P_FROM_GL_DATE date,
                                P_TO_GL_DATE date,
                                P_BATCH_NAME VARCHAR2
                                )
IS
/*
   =======================================================================================================
   ||   Procedure Name  : XXABRL_GL_AUTO_MIGRATION_PROC
   ||   Description : ABRL HM Automigration Data Port
   ||
   ||   Version     Date            Author              Modification
   ||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
   ||   1.0.0      31-Jan-2011  Mitul Vanpariya      New Development
   ||   1.0.1      14-Dec-2011  Narasimhulu           Added G/L Date Column                   
   =======================================================================================================*/

       query_string             LONG;
       query_string1            LONG;
       order_by_string          LONG;
       batch_string             LONG;
       inv_count              NUMBER;
       sbu_string               LONG;
       location_string          LONG;
       gl_account_string        LONG;
       gl_date_string           LONG;
       gl_date_string1           LONG;
       closing_balance         NUMBER;
       cr_total                NUMBER;
       dr_total                NUMBER;
       v_dr_amount             NUMBER:=0;
       v_cr_amount             NUMBER:=0;
       v_REQ_CREATED_BY        number:=FND_PROFILE.value('USER_ID');
       V_REQ_ID NUMBER :=fnd_global.conc_request_id;
        L_LTD_GL_DATE          DATE;

TYPE ref_cur IS REF CURSOR;
TYPE ref_cur1 IS REF CURSOR;

      c                        ref_cur;
      c1                       ref_cur1;

      TYPE c_rec IS RECORD (
                            v_batch_name           gl_je_batches.NAME%TYPE,
                            v_SOURCE               gl_je_headers.je_source%TYPE,
                            v_CATEGORY             gl_je_headers.je_category%TYPE,
                            v_gl_date              gl_je_headers.default_effective_date%TYPE,
                            v_line_number          gl_je_lines.je_line_num%TYPE,
                            v_SEGMENT1             gl_code_combinations_kfv.segment1%TYPE,
                            v_SEGMENT2             gl_code_combinations_kfv.segment1%TYPE,
                            v_SEGMENT3             gl_code_combinations_kfv.segment1%TYPE,
                            v_SEGMENT4             gl_code_combinations_kfv.segment1%TYPE,
                            v_SEGMENT5             gl_code_combinations_kfv.segment1%TYPE,
                            v_SEGMENT6             gl_code_combinations_kfv.segment1%TYPE,
                            v_SEGMENT7             gl_code_combinations_kfv.segment1%TYPE,
                            v_SEGMENT8             gl_code_combinations_kfv.segment1%TYPE,
                            v_account_code         gl_code_combinations_kfv.concatenated_segments%TYPE,
                            v_SBU_desc             VARCHAR2(250),
                            v_Location_desc        VARCHAR2(250),
                            v_account_desc         VARCHAR2(250),
                            v_dr_amount            VARCHAR2(20),
                            v_cr_amount            VARCHAR2(20),
                            v_journal_description  gl_je_lines.description%TYPE,
                            v_Customer_Number      VARCHAR2(250),
                            v_Customer_Name        VARCHAR2(250),
                            v_document_number      gl_je_headers.doc_sequence_value%TYPE,
                            v_batch_status        VARCHAR2(100),
                           ou_name               varchar2(240),
                            v_lookup_code           varchar2(20),
                            v_lookup_MEANING        varchar2(20),
                            v_lookup_description    varchar2(50)
--                            v_request_id number,
--                            v_period_form varchar2(50),
--                            v_period_to varchar2(50)
                          );

      TYPE c_rec1 IS RECORD (v_dr_amount            gl_je_lines.accounted_dr%TYPE,
                             v_cr_amount            gl_je_lines.accounted_cr%TYPE                
         );

      v_rec                    c_rec;
      v_rec1                   c_rec1;


    V_LEDGER_ID NUMBER;
    V_RESP_ID NUMBER := Fnd_Profile.VALUE('RESP_ID');
    V_LEDGER_COND VARCHAR2(1000);
    
    BEGIN

  query_string :=
  'SELECT AA.batch_name,
AA.SOURCE,
AA.CATEGORY,
AA.gl_date,
AA.line_number,
       AA.segment1,
       AA.segment2,
       AA.segment3,
       AA.segment4,
       AA.segment5,
       AA.segment6,
       AA.segment7,
       AA.segment8,
AA.account_code,
AA.SBU_desc,
AA.Location_desc,
AA.account_desc,
AA.dr_amount,
AA.cr_amount,
AA.journal_description,
AA.Customer_Number,
AA.Customer_Name,
AA.document_number,
AA.batch_status,
(select NAME from hr_operating_units where to_char(ORGANIZATION_ID) =to_char(aa.org_id)) OU_NAME,
aa.lookup_code,aa.MEANING,aa.description
FROM
(SELECT GLB.NAME batch_name,
       glh.je_source SOURCE, glh.je_category CATEGORY,
       glh.default_effective_date gl_date,
       gll.je_line_num line_number,
       gl.segment1,
       gl.segment2,
       gl.segment3,
       gl.segment4,
       gl.segment5,
       gl.segment6,
       gl.segment7,
       gl.segment8,
       gl.concatenated_segments account_code,
       (SELECT FFVL.description FROM fnd_flex_values_vl ffvl
        WHERE ffvl.FLEX_VALUE_SET_ID=1013466
        AND ffvl.FLEX_VALUE=gl.SEGMENT3) SBU_desc,
         (SELECT FFVL.description FROM fnd_flex_values_vl ffvl
        WHERE ffvl.FLEX_VALUE_SET_ID=1013467
        AND ffvl.FLEX_VALUE=gl.SEGMENT4) Location_desc,
       (SELECT FFVL.description FROM fnd_flex_values_vl ffvl
        WHERE ffvl.FLEX_VALUE_SET_ID=1013469
        AND ffvl.FLEX_VALUE=gl.SEGMENT6) account_desc,
       decode(xla.accounted_dr,null,decode(xla.accounted_cr,null, gll.accounted_dr),xla.accounted_dr) dr_amount,
       decode(xla.accounted_cr,null,decode(xla.accounted_dr,null, gll.accounted_cr),xla.accounted_cr) cr_amount,
       gll.description journal_description,
      CASE WHEN UPPER(glh.je_source)=''PAYABLES''
            THEN (SELECT po.segment1
                 FROM po_vendors po
                 WHERE po.VENDOR_ID=xla.PARTY_ID
                 AND ROWNUM<2)
            WHEN UPPER(glh.je_source)=''RECEIVABLES''
            THEN /*(SELECT  ar.account_number
                 FROM hz_parties hzp, hz_cust_accounts ar
                 WHERE ar.party_id = hzp.party_id
                 AND ar.cust_account_id=xla.party_id
                 AND ROWNUM<2 ) */
                DECODE(glh.je_category ,''Receipts'',(select ACCOUNT_NUMBER  
                                                      from apps.hz_cust_accounts_all hca
                                                          ,apps.hz_parties hp
                                                          ,apps.ar_cash_receipts_all   acra 
                                                          ,xla.xla_transaction_entities xte
                                                          ,apps.xla_ae_headers xah
                                                      where hca.cust_account_id=acra.PAY_FROM_CUSTOMER
                                                        and hca.PARTY_ID=hp.party_id
                                                        and acra.cash_receipt_id=xte.SOURCE_ID_INT_1
                                                        and xah.ENTITY_ID=xte.ENTITY_ID
                                                        and xah.AE_HEADER_ID=xla.AE_HEADER_ID
                                                      ),
                 (SELECT  ar.account_number
                 FROM hz_parties hzp, hz_cust_accounts ar
                 WHERE ar.party_id = hzp.party_id
                 AND ar.cust_account_id=xla.party_id
                 AND ROWNUM<2 ))
        END AS Customer_Number,
        CASE WHEN UPPER(glh.je_source)=''PAYABLES''
            THEN (SELECT po.vendor_name
                 FROM po_vendors po
                 WHERE po.VENDOR_ID=xla.PARTY_ID
                 )
            WHEN UPPER(glh.je_source)=''RECEIVABLES''
            THEN /*(SELECT  hzp.party_name
                 FROM hz_parties hzp, hz_cust_accounts ar
                 WHERE ar.party_id = hzp.party_id
                 AND ar.cust_account_id=xla.party_id
                 )*/
                 decode( glh.je_category ,''Receipts'',(select hp.party_name  
                                                      from hz_cust_accounts_all hca
                                                          ,hz_parties hp
                                                          ,apps.ar_cash_receipts_all   acra 
                                                          ,xla.xla_transaction_entities xte
                                                          ,xla_ae_headers xah
                                                      where hca.cust_account_id=acra.PAY_FROM_CUSTOMER
                                                        and hca.PARTY_ID=hp.party_id
                                                        and acra.cash_receipt_id=xte.SOURCE_ID_INT_1
                                                        and xah.ENTITY_ID=xte.ENTITY_ID
                                                        and xah.AE_HEADER_ID=xla.AE_HEADER_ID
                                                      )
                 ,(SELECT  hzp.party_name
                 FROM hz_parties hzp, hz_cust_accounts ar
                 WHERE ar.party_id = hzp.party_id
                 AND ar.cust_account_id=xla.party_id
                 ))
         END AS Customer_Name,
       /*CASE WHEN UPPER(glh.je_source)=''PAYABLES''
            THEN (SELECT po.segment1
                 FROM po_vendors po
                 WHERE po.VENDOR_ID=xla.PARTY_ID
                 AND ROWNUM<2)
            WHEN UPPER(glh.je_source)=''RECEIVABLES''
            THEN (SELECT  ar.account_number
                 FROM hz_parties hzp, hz_cust_accounts ar
                 WHERE ar.party_id = hzp.party_id
                 AND ar.cust_account_id=xla.party_id
                 AND ROWNUM<2 )
        END AS Customer_Number,
        CASE WHEN UPPER(glh.je_source)=''PAYABLES''
            THEN (SELECT po.vendor_name
                 FROM po_vendors po
                 WHERE po.VENDOR_ID=xla.PARTY_ID
                 )
            WHEN UPPER(glh.je_source)=''RECEIVABLES''
            THEN (SELECT  hzp.party_name
                 FROM hz_parties hzp, hz_cust_accounts ar
                 WHERE ar.party_id = hzp.party_id
                 AND ar.cust_account_id=xla.party_id
                 )
         END AS Customer_Name,*/
      /* NVL(glir.SUBLEDGER_DOC_SEQUENCE_VALUE,glh.doc_sequence_value) document_number,*/
      /*decode( glh.je_category,''Credit Memos'',(select distinct rac.DOC_SEQUENCE_VALUE 
                                    from 
                                    XLA_AE_HEADERS xxla,
                                    RA_CUST_TRX_LINE_GL_DIST_ALL cust,
                                    RA_CUSTOMER_TRX_ALL rac
                                    where
                                    xxla.EVENT_ID=cust.EVENT_ID and
                                    cust.CUSTOMER_TRX_ID=rac.CUSTOMER_TRX_ID and
                                    cust.ORG_ID=rac.ORG_ID and
                                    xxla.AE_HEADER_ID=xla.AE_HEADER_ID),
                              ''Purchase Invoices'',(SELECT XXLA.DOC_SEQUENCE_VALUE FROM           
                                                        XLA_AE_HEADERS xxla
                                                        WHERE
                                                        XXLA.AE_HEADER_ID=XLA.AE_HEADER_ID )                              
                              ,nvl(glir.SUBLEDGER_DOC_SEQUENCE_VALUE,glh.doc_sequence_value)) document_number,*/
                              nvl(decode( glh.je_category,''Credit Memos'',(select distinct rac.DOC_SEQUENCE_VALUE 
                                    from 
                                    XLA_AE_HEADERS xxla,
                                    RA_CUST_TRX_LINE_GL_DIST_ALL cust,
                                    RA_CUSTOMER_TRX_ALL rac
                                    where
                                    xxla.EVENT_ID=cust.EVENT_ID and
                                    cust.CUSTOMER_TRX_ID=rac.CUSTOMER_TRX_ID and
                                    cust.ORG_ID=rac.ORG_ID and
                                    xxla.AE_HEADER_ID=xla.AE_HEADER_ID)
                              ,(SELECT XXLA.DOC_SEQUENCE_VALUE FROM           
                                                        XLA_AE_HEADERS xxla
                                                        WHERE
                                                        XXLA.AE_HEADER_ID=XLA.AE_HEADER_ID )                              
                              ),nvl(glir.SUBLEDGER_DOC_SEQUENCE_VALUE,glh.doc_sequence_value)) document_number,
       DECODE (GLB.status,
               ''P'', ''Posted'',
               ''U'', ''Unposted'',
               GLB.status
              ) batch_status,
         null Created_By,
        gl.segment3 SBU,
       gl.segment4 LOCATION,
       gl.segment6 GL_account,
            GLH.LEDGER_ID LEDGER_ID,gl.CODE_COMBINATION_ID,xla.CREATED_BY CREATED_BYxla ,gll.CREATED_BY CREATED_BYgll,
               (select distinct SECURITY_ID_INT_1 from xla.xla_transaction_entities xlat,xla_ae_headers xlah where 
                xlat.APPLICATION_ID=xla.APPLICATION_ID
   and xlat.ENTITY_ID=xlah.ENTITY_ID 
   and xlah.AE_HEADER_ID=xla.AE_HEADER_ID
   )    org_id,
   lookup.lookup_code,lookup.MEANING,lookup.description
  FROM gl_je_headers glh,
       gl_je_lines gll,
       gl_code_combinations_kfv gl,
       gl_je_batches GLB,
       xla_ae_lines xla,
   --    xla_ae_headers xlah,
       gl_import_references glir,
       (select lookup_code,MEANING,description from FND_LOOKUP_VALUES_VL
 where LOOKUP_TYPE=''ABRL_DIRECT_EXPENSE_CODE''
 and END_DATE_ACTIVE is null
  ) lookup
 WHERE glh.je_header_id = gll.je_header_id
   AND gll.je_header_id = glir.je_header_id (+)
   AND gll.je_line_num = glir.je_line_num (+)
   AND glir.gl_sl_link_id = xla.gl_sl_link_id (+)
   AND glir.gl_sl_link_table = xla.gl_sl_link_table (+)
   AND gl.code_combination_id = gll.code_combination_id
 --  and xla.AE_HEADER_ID=xlah.AE_HEADER_ID
   AND glh.je_batch_id = GLB.je_batch_id
   AND UPPER(glh.je_source) IN (''PAYABLES'',''RECEIVABLES'')
   AND NVL(xla.accounted_cr,999999)!=0
   AND NVL(xla.accounted_dr,999999)!=0
   AND GL.SEGMENT6 =lookup.lookup_code
UNION ALL
SELECT GLB.NAME batch_name,
       glh.je_source SOURCE, glh.je_category CATEGORY,
       glh.default_effective_date gl_date,
       gll.je_line_num line_number,
        gl.segment1,
       gl.segment2,
       gl.segment3,
       gl.segment4,
       gl.segment5,
       gl.segment6,
       gl.segment7,
       gl.segment8,
       gl.concatenated_segments account_code,
       (SELECT FFVL.description FROM fnd_flex_values_vl ffvl
        WHERE ffvl.FLEX_VALUE_SET_ID=1013466
        AND ffvl.FLEX_VALUE=gl.SEGMENT3) SBU_desc,
         (SELECT FFVL.description FROM fnd_flex_values_vl ffvl
        WHERE ffvl.FLEX_VALUE_SET_ID=1013467
        AND ffvl.FLEX_VALUE=gl.SEGMENT4) Location_desc,
       (SELECT FFVL.description FROM fnd_flex_values_vl ffvl
        WHERE ffvl.FLEX_VALUE_SET_ID=1013469
        AND ffvl.FLEX_VALUE=gl.SEGMENT6) account_desc,
      GLL.ACCOUNTED_DR dr_amount,
      GLL.ACCOUNTED_CR cr_amount,
      gll.description journal_description,
      NULL customer_name,
      NULL customer_Number,
       glh.doc_sequence_value document_number,
       DECODE (GLB.status,
               ''P'', ''Posted'',
               ''U'', ''Unposted'',
               GLB.status
              ) batch_status,
     fu.user_name Created_By,
     gl.segment3 SBU,
     gl.segment4 LOCATION,
     gl.segment6 GL_account,
     GLH.LEDGER_ID LEDGER_ID,null,null,null,null,
     lookup.lookup_code,lookup.MEANING,lookup.description
FROM
       gl_je_headerS glh,
       gl_je_lines gll,
       gl_code_combinations_kfv gl,
       fnd_user fu,
       gl_je_batches GLB,
       (select lookup_code,MEANING,description from FND_LOOKUP_VALUES_VL
 where LOOKUP_TYPE=''ABRL_DIRECT_EXPENSE_CODE''
 and END_DATE_ACTIVE is null
  ) lookup
WHERE glh.je_header_id = gll.je_header_id
  AND gl.code_combination_id = gll.code_combination_id
  AND glh.je_batch_id = GLB.je_batch_id
  AND gll.created_by=fu.user_id
  AND UPPER(glh.je_source) NOT IN (''PAYABLES'',''RECEIVABLES'')
  AND GL.SEGMENT6 =lookup.lookup_code
) AA
WHERE 1=1
AND UPPER(AA.SBU_desc) LIKE ''%HM%'' ';

 
BEGIN
 SELECT DEFAULT_LEDGER_ID INTO V_LEDGER_ID
     FROM FND_PROFILE_OPTION_VALUES fpop,
      FND_PROFILE_OPTIONS fpo     ,
      gl_access_sets GAS
      WHERE --level_value=51664--cp_resp_id  --52299 -- resp id
      fpop.PROFILE_OPTION_ID =fpo.PROFILE_OPTION_ID
      AND GAS.ACCESS_SET_ID=FPOP.profile_option_value
        AND level_value=V_RESP_ID --51664
      AND PROFILE_OPTION_NAME='GL_ACCESS_SET_ID';

      EXCEPTION
      WHEN OTHERS THEN
      Fnd_File.put_line (Fnd_File.LOG,'NO LEDGER FOUND. ');
      V_LEDGER_ID:=0;

END;

  sbu_string:= ' AND AA.SBU BETWEEN '''||P_FROM_SBU||''' AND '''||P_TO_SBU||'''';
  location_string:=' AND TO_NUMBER(AA.LOCATION) BETWEEN '''||P_FROM_LOCATION||''' AND '''||P_TO_LOCATION||'''';
  gl_account_string:=' AND AA.GL_ACCOUNT BETWEEN '''||P_FROM_ACCOUNT||''' AND '''||P_TO_ACCOUNT||'''';
  gl_date_string:=' AND to_date(to_char(AA.Gl_Date,''DD-MON-YY'')) BETWEEN '''||P_FROM_GL_DATE||''' AND '''||P_TO_GL_DATE||'''';
  gl_date_string1:=' AND to_date(to_char(AA.Gl_Date,''DD-MON-YY'')) < '''||P_FROM_GL_DATE||'''';
  batch_string:= ' AND AA.batch_name = '''|| p_batch_name|| '''';
  order_by_string:=' ORDER BY AA.batch_name,AA.SOURCE,AA.CATEGORY,AA.line_number';
  V_LEDGER_COND := ' AND AA.LEDGER_ID = '||V_LEDGER_ID;

  -- ADDING LEDGER ID CONDITION
  query_string:=query_string|| V_LEDGER_COND;
  

  -- Checking For the from SBU and To SBU Parameter

      IF (P_FROM_SBU IS NULL AND P_TO_SBU IS NULL)
      THEN
         query_string := query_string;
         query_string1 := query_string1;
      ELSE
         query_string := query_string ||sbu_string;
         query_string1 := query_string1 ||sbu_string;
      END IF;

      -- Checking For the from LOCATION and To LOCATION Parameter

      IF (P_FROM_LOCATION IS NULL AND P_TO_LOCATION IS NULL)
      THEN
         query_string := query_string;
         query_string1 := query_string1;
      ELSE
         query_string := query_string ||location_string;
         query_string1 := query_string1 ||location_string;
      END IF;

     -- Checking For the from GL ACCOUNT and To GL ACCOUNT Parameter

      IF (P_FROM_ACCOUNT IS NULL AND P_TO_ACCOUNT IS NULL)
      THEN
         query_string := query_string;
         query_string1 := query_string1;
      ELSE
         query_string := query_string ||gl_account_string;
         query_string1 := query_string1 ||gl_account_string;
      END IF;

      -- Checking  For Batch name Parameter

      IF(p_batch_name IS NULL) THEN
         query_string:=query_string;
         query_string1:=query_string1;
         ELSE
         query_string :=query_string||batch_string;
         query_string1 :=query_string1||batch_string;
      END IF;

    -- Checking GL Date Parameter

     IF (P_FROM_GL_DATE IS NOT NULL AND P_TO_GL_DATE IS NOT NULL)
      THEN
         query_string := query_string || gl_date_string;
         query_string1 := query_string1 || gl_date_string1;
      ELSIF (P_FROM_GL_DATE IS NULL AND P_TO_GL_DATE IS NULL)
      THEN
         query_string := query_string;
         query_string1:= query_string1;
      END IF;



      
     /*
   Fnd_File.put_line (Fnd_File.output, ' ');
      Fnd_File.put_line (Fnd_File.output, ' ');
      Fnd_File.put_line (Fnd_File.output,
                           'Batch Name'
                         || CHR (9)
                         || 'Source'
                         || CHR (9)
                         || 'Category'
                         || CHR (9)
                                     || 'GL Date'
                         || CHR (9)
                         || 'Line Number'
                         || CHR (9)
                         || 'Account Code'
                         ||  CHR (9)
                         || 'State SBU'
                         ||  CHR (9)
                         || 'Location'
                         ||  CHR (9)
                         || 'GL Account'
                         || CHR (9)
                         || 'Dr Amount'
                         || CHR (9)
                         || 'Cr Amount'
                         || CHR (9)
                         || 'Journal Description'
                         || CHR (9)
                         || 'Vendor \ Customer Name'
                         || CHR (9)
                         || 'Vendor \ Customer Number'
                         ||  CHR (9)
                         || 'Document Number'
                         ||  CHR (9)
                         || 'Batch Status'
                         ||  CHR (9)
                         || 'Created by (Entry in Subledger)'
                         ||  CHR (9)
                         || 'OU Name'
                        );*/

                   query_string:=query_string;

                   Fnd_File.put_line (Fnd_File.LOG,
                         'string: '||query_string
                        );

  inv_count:=0;
  cr_total:=0;
  dr_total:=0; 
  
  /*Modified the G/L date --by Narasimhulu -- 15/12/2011*/
   
  SELECT LAST_DAY(TO_DATE(P_TO_GL_DATE)) INTO L_LTD_GL_DATE FROM DUAL;
    
                                  
         OPEN c
       FOR query_string;
      LOOP
         FETCH c
          INTO v_rec;
         EXIT WHEN c%NOTFOUND;   
         
       -- ravi added 
Insert into XXABRL.xxabrl_gl_auto_mig_data_port 
                 (  BATCH_NAME,       SOURCE,
                    CATEGORY,         GL_DATE,
                    LINE_NUMBER,      SEGMENT1,
                    SEGMENT2,         SEGMENT3,
                    SEGMENT4,         SEGMENT5,
                    SEGMENT6,         SEGMENT7,
                    SEGMENT8,         ACCOUNT_CODE,
                    SBU_DESC,         LOCATION_DESC,
                  ACCOUNT_DESC,     DR_AMOUNT,
                   CR_AMOUNT,        JOURNAL_DESCRIPTION,
                    CUSTOMER_NUMBER,  CUSTOMER_NAME,
                   DOCUMENT_NUMBER,  BATCH_STATUS,
                   OU_NAME,
                  -- CREATED_BY,      
                   LOOKUP_CODE,  
                   LOOKUP_MEANING,
                   LOOKUP_DESC,
                   REQUEST_ID,
                   PERIOD_FROM,
                   PERIOD_TO,
                   REQ_CREATED_BY,
                  REQ_CREATION_DATE,FLAG_STATUS,
                  LD_GL_DATE
)
                 VALUES
                   (v_rec.v_batch_name,                 v_rec.v_SOURCE,
                    v_rec.v_CATEGORY,                   to_date(v_rec.v_gl_date),
                    v_rec.v_line_number,                v_rec.v_SEGMENT1,                 
                    v_rec.v_SEGMENT2,                   v_rec.v_SEGMENT3,
                    v_rec.v_SEGMENT4,                   v_rec.v_SEGMENT5,
                    v_rec.v_SEGMENT6,                   v_rec.v_SEGMENT7,
                    v_rec.v_SEGMENT8,                   v_rec.v_account_code,
                   v_rec.v_SBU_desc,                   v_rec.v_Location_desc,
                   v_rec.v_account_desc,               v_rec.v_dr_amount,
                   v_rec.v_cr_amount,                  v_rec.v_journal_description,
                   v_rec.v_Customer_Number,            v_rec.v_Customer_Name,
                   v_rec.v_document_number,            v_rec.v_batch_status,
                   v_rec.ou_name,
                   -- v_rec.v_Created_By,                 
                    v_rec.v_lookup_code,
                   v_rec.v_lookup_MEANING,
                   v_rec.v_lookup_description,
                   V_REQ_ID,
                   P_FROM_GL_DATE,
                   P_TO_GL_DATE,
                    v_REQ_CREATED_BY,
                   SYSDATE,                            'Y',
                   L_LTD_GL_DATE
                    );
        Commit;
              
        -- ravi added
     /*    Fnd_File.put_line (Fnd_File.output,
                             v_rec.v_batch_name
                            ||','
                            || v_rec.v_SOURCE
                            ||','
                            || v_rec.v_CATEGORY
                            ||','
                            || v_rec.v_gl_date
                            ||','
                            || v_rec.v_line_number
                            ||','
                            || v_rec.v_account_code
                            ||','
                            || v_rec.v_SBU_desc
                            ||','
                            || v_rec.v_Location_desc
                            ||','
                            || v_rec.v_account_desc
                            ||','
                            || v_rec.v_dr_amount
                            ||','
                            || v_rec.v_cr_amount
                            ||','
                            || v_rec.v_journal_description
                            ||','
                            || v_rec.v_Customer_Number
                            ||','
                            ||v_rec.v_Customer_Name
                            ||','
                            ||v_rec.v_document_number
                            ||','
                            || v_rec.v_batch_status
                            ||','
                            || v_rec.v_Created_By
                            ||','
                            || v_rec.ou_name
                            );*/

                   cr_total:=cr_total+NVL(v_rec.v_cr_amount,0);
                   dr_total:=dr_total+NVL(v_rec.v_dr_amount,0);

                   inv_count :=inv_count+1;

END LOOP;

                    cr_total:= nvl(cr_total,0) + nvl(v_cr_amount,0);
                    dr_total:= nvl(dr_total,0) + nvl(v_dr_amount,0);
        




CLOSE c;

FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                        'SEGMENT3'
                        ||','
                        ||'SBU'
                        ||','    
                        ||'SEGMENT4'
                        ||','    
                        ||'LOCATION_DESC'
                        ||','    
                        ||'SEGMENT5'
                        ||','
                        ||'MERCHANDISE'
                        ||','    
                        ||'DIRECT_EXPENSE'
                        ||','    
                        ||'OCTROI_EXPENSE'
                        ||','
                        ||'TM_EXPENSES'
                        ||','    
                        ||'MRECOVERY_EXPENSE'
                        ||','    
                        ||'VISIBILITY_EXPENSE'
                        ||','    
                        ||'COOP_EXPENSE'
                        ||','    
                        ||'G/L_DATE');


FOR SEL_DATA IN (
            SELECT
             segment3,
              (SELECT FFVL.description FROM fnd_flex_values_vl ffvl
                    WHERE ffvl.FLEX_VALUE_SET_ID=1013466
                    AND ffvl.FLEX_VALUE=SEGMENT3) SBU,
             segment4,
              (SELECT FFVL.description FROM fnd_flex_values_vl ffvl
                    WHERE ffvl.FLEX_VALUE_SET_ID=1013467
                    AND ffvl.FLEX_VALUE=SEGMENT4) Location_desc,
             segment5,
             (SELECT FFVL.description FROM fnd_flex_values_vl ffvl
                    WHERE ffvl.FLEX_VALUE_SET_ID=1013468
                    AND ffvl.FLEX_VALUE=SEGMENT5) Merchandise,
             segment6,
             (SELECT FFVL.description FROM fnd_flex_values_vl ffvl
                    WHERE ffvl.FLEX_VALUE_SET_ID=1013469
                    AND ffvl.FLEX_VALUE=SEGMENT6) account_desc,
             SUM(DIRECT_EXPENSE) DIRECT_EXPENSE,
             SUM(OCTROI_EXPENSE) OCTROI_EXPENSE,
             SUM(TM_EXPENSES) TM_EXPENSES,
             SUM(MRECOVERY_EXPENSE) MRECOVERY_EXPENSE,
             SUM(VISIBILITY_EXPENSE) VISIBILITY_EXPENSE,
             SUM(COOP_EXPENSE) COOP_EXPENSE,LD_GL_DATE
              FROM( 
            select
            segment3,segment4,segment5,segment6, 
            DECODE(LOOKUP_DESC,'DIRECT_EXPENSE',AMOUNT,0) DIRECT_EXPENSE,
            DECODE(LOOKUP_DESC,'OCTROI_EXPENSE',AMOUNT,0) OCTROI_EXPENSE,
            DECODE(LOOKUP_DESC,'TM_EXPENSES',AMOUNT,0) TM_EXPENSES,
            DECODE(LOOKUP_DESC,'MRECOVERY_EXPENSE',AMOUNT,0) MRECOVERY_EXPENSE,
            DECODE(LOOKUP_DESC,'VISIBILITY_EXPENSE',AMOUNT,0) VISIBILITY_EXPENSE,
            DECODE(LOOKUP_DESC,'COOP_EXPENSE',AMOUNT,0) COOP_EXPENSE,LD_GL_DATE
            from(
            select 
            segment3,segment4,segment5,segment6,LOOKUP_DESC,nvl(sum(dr_amount),0)-nvl(sum(cr_amount),0) amount,LD_GL_DATE
             FROM 
            XXABRL.xxabrl_gl_auto_mig_data_port
            WHERE REQUEST_ID=V_REQ_ID
             group by segment3,segment4,segment5,segment6,LOOKUP_DESC ,LD_GL_DATE
             )
              )
             GROUP BY  segment3,segment4,segment5,segment6,LD_GL_DATE
             order by 1,2,3,4            
             )
             
             LOOP
             
             
             Fnd_File.put_line (Fnd_File.output,                           
                            SEL_DATA.SEGMENT3  
                            ||','
                            || SEL_DATA.SBU
                            ||','
                            || SEL_DATA.SEGMENT4 
                            ||','
                            || SEL_DATA.LOCATION_DESC
                            ||','
                            || SEL_DATA.SEGMENT5
                            ||','
                            || SEL_DATA.MERCHANDISE
                            ||','
                        --    || SEL_DATA.SEGMENT6
                        --    ||','
                        --    || SEL_DATA.ACCOUNT_DESC
                           -- ||','
                            || SEL_DATA.DIRECT_EXPENSE
                            ||','
                            || SEL_DATA.OCTROI_EXPENSE
                            ||','
                            || SEL_DATA.TM_EXPENSES
                            ||','
                            || SEL_DATA.MRECOVERY_EXPENSE
                            ||','
                            || SEL_DATA.VISIBILITY_EXPENSE
                            ||','
                            || SEL_DATA.COOP_EXPENSE
                            ||','
                             || SEL_DATA.LD_GL_DATE
                            ||','
                            );
             
             END LOOP;
             
             

END XXABRL_GL_AUTO_MIGRATION_PROC; 
/


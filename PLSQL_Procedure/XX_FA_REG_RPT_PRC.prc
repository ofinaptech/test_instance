CREATE OR REPLACE PROCEDURE APPS.xx_fa_reg_rpt_prc
                              ( errbuf VARCHAR2
                               ,retcode NUMBER
                               ,p_book_code        FA_BOOKS.BOOK_TYPE_CODE%TYPE
                               ,p_date_from        VARCHAR2 --FA_BOOKS.DATE_PLACED_IN_SERVICE%TYPE
                               ,p_date_to          VARCHAR2 --FA_BOOKS.DATE_PLACED_IN_SERVICE%TYPE
			       ,dummy              NUMBER
                               ,p_cat_segment1      FA_CATEGORIES.SEGMENT1%TYPE
--                               ,p_cat_segment2      FA_CATEGORIES.SEGMENT2%TYPE
--                               ,p_cat_segment3      FA_CATEGORIES.SEGMENT3%TYPE
                               ,p_location          FA_LOCATIONS.SEGMENT1%TYPE
                              )
AS

--p_from_date DATE;
--p_to_date   DATE;


l_concat_segments  VARCHAR2(500);

CURSOR cur_data --(p_fdate DATE, p_tdate DATE)
IS
/*
SELECT    ad.asset_number ASS_NO
        , ad.current_units QTY
		 ,TO_CHAR(Bk.DATE_PLACED_IN_SERVICE,'DD-MON-YYYY')  DPIS
		 ,ai.invoice_date   INV_DATE ------
		,ad.current_units
		 ,loc.segment1 asset_LOCATION
		 ,cat.segment1 Major_cat
		 ,cat.segment2 min_cat1
		 ,cat.segment3 min_cat2
		 ,ad.attribute_category_code
		 ,bk.original_cost
		 ,bk.COST final_cost
		 ,bk.basic_rate dep_com_act
		 ,bk.deprn_method_code
		 ,jfb.rate DEP_RATE
		 ,ad2.description asset_desc
		 , fr.date_retired, fr.proceeds_of_sale
		 ,bk.ltd_proceeds, bk.ytd_proceeds
		 ,fth.transaction_name
		 ,pv.vendor_name
		 ,ai.po_number
--		 ,ph.creation_date  --------------
		 ,ai.invoice_number
--		 ,fw.end_date   --------------
    FROM fa_asset_history ah,
         fa_books bk,
         fa_categories cat,
         fa_lookups lookups_at,
         fa_lookups lookups_nu,
         fa_lookups lookups_ol,
         fa_lookups lookups_iu,
         fa_lookups lookups_pt,
         fa_lookups lookups_12,
         fa_additions_tl ad2,
         fa_additions_b ad,
		 FA_ASSET_INVOICES ai,
		 fa_distribution_history  fd,
		 FA_LOCATIONS loc,
		 fa_retirements fr -- date retired
		 ,fa_transaction_headers fth
		 ,po_vendors pv
		 ,JAI_FA_AST_BLOCKS jfb
		 ,jai_fa_ast_block_dtls jfbd
--		 ,po_headers_all ph   ------------
--		 ,fa_warranties fw    ----------
   WHERE    ad.asset_id = ad2.asset_id
     AND ad.parent_asset_id IS NULL
     AND ad2.LANGUAGE = USERENV ('LANG')
     AND ah.asset_id = ad.asset_id
     AND ah.date_effective <= SYSDATE
     AND NVL (ah.date_ineffective, SYSDATE + 1) > SYSDATE
     AND ah.category_id = cat.category_id
     AND bk.asset_id = ad.asset_id
     AND bk.book_type_code = P_BOOK_CODE
     AND bk.date_ineffective IS NULL
     AND lookups_at.lookup_code = ad.asset_type
     AND lookups_at.lookup_type = 'ASSET TYPE'
     AND lookups_nu.lookup_code = ad.new_used
     AND lookups_nu.lookup_type = 'NEWUSE'
     AND lookups_ol.lookup_code = ad.owned_leased
     AND lookups_ol.lookup_type = 'OWNLEASE'
     AND lookups_iu.lookup_code = ad.in_use_flag
     AND lookups_iu.lookup_type = 'YESNO'
     AND lookups_pt.lookup_code(+) = ad.property_type_code
     AND lookups_pt.lookup_type(+) = 'PROPERTY TYPE'
     AND lookups_12.lookup_code(+) = ad.property_1245_1250_code
     AND lookups_12.lookup_type(+) = '1245/1250 PROPERTY'
	 AND  ad.asset_id = ai.asset_id(+)   --------- for invoice date
	 AND ad.asset_id = fd.asset_id -- for distribution qty
	 AND fd.location_id = loc.location_id   -- for location
	 AND ad.asset_id = fr.asset_id
	 AND fr.book_type_code=bk.book_type_code
	 AND ad.asset_id = fth.asset_id
	 AND fth.book_type_code=bk.book_type_code
	 AND fr.transaction_header_id_in = fth.transaction_header_id(+)
   AND 	 AI.PO_VENDOR_ID = Pv.VENDOR_ID(+)
   AND AI.DATE_INEFFECTIVE IS NULL
   AND jfb.block_id = jfbd.block_id
-- AND jfbd.asset_id = ad.asset_id    ----------------------
   AND jfbd.BOOK_TYPE_CODE = bk.book_type_code
-- AND ai.PO_NUMBER = ph.segment1  -------------------
-- AND ph.vendor_id = fw.po_vendor_id ---------------------
AND bk.date_placed_in_service BETWEEN TO_DATE(p_date_from,'YYYY/MM/DD HH24:MI:SS') AND TO_DATE(p_date_to,'YYYY/MM/DD HH24:MI:SS')
-----AND ad.attribute_category_code = p_cat_segment1
and cat.category_id = p_cat_segment1
--AND cat.segment1 = p_cat_segment1
--AND cat.segment2 = p_cat_segment2
--AND cat.segment3 = p_cat_segment3
AND loc.segment1 = p_location
order by ad.asset_number; */


SELECT            ad.asset_number ASS_NO
                 ,ad.current_units QTY
		 ,TO_CHAR(Bk.DATE_PLACED_IN_SERVICE,'DD-MON-YYYY')  DPIS
		 ,ai.invoice_date   INV_DATE
		 ,ad.current_units
		 ,loc.segment1 asset_LOCATION
		 ,cat.segment1 Major_cat
		 ,cat.segment2 min_cat1
		 ,cat.segment3 min_cat2
		 ,ad.attribute_category_code
		 ,bk.original_cost
		 ,bk.COST final_cost
		 ,(bk.basic_rate*100) dep_com_act
		 ,bk.deprn_method_code
		-- ,jfb.rate DEP_RATE
		 ,ad2.description asset_desc
		 , fr.date_retired, fr.proceeds_of_sale
---------        ,bk.ltd_proceeds, bk.ytd_proceeds
                 ,fds.DEPRN_RESERVE ltd_value
                 ,fds.YTD_DEPRN ytd_value
		 ,fth.transaction_name
		 ,pv.vendor_name
		 ,ai.po_number
		 ,ph.creation_date
		 ,ai.invoice_number
		 ,fw.end_date
		 ,ap.doc_sequence_value
    FROM fa_asset_history ah,
         fa_books bk,
         fa_categories cat,
         fa_lookups lookups_at,
         fa_lookups lookups_nu,
         fa_lookups lookups_ol,
         fa_lookups lookups_iu,
         fa_lookups lookups_pt,
         fa_lookups lookups_12,
         fa_additions_tl ad2,
         fa_additions_b ad
		 ,FA_ASSET_INVOICES ai
		 ,fa_distribution_history  fd
		 ,FA_LOCATIONS loc
		 ,fa_retirements fr -- date retired
		 ,fa_transaction_headers fth
		 ,po_vendors pv
		-- ,JAI_FA_AST_BLOCKS jfb
		-- ,jai_fa_ast_block_dtls jfbd
		 ,po_headers_all ph
		 ,fa_warranties fw
		 ,FA_DEPRN_SUMMARY fds
		 ,ap_invoices_all ap       	-- added for document seq num/voucher number
   WHERE    ad.asset_id = ad2.asset_id
     AND ad.parent_asset_id IS NULL
     AND ad2.LANGUAGE = USERENV ('LANG')
     AND ah.asset_id = ad.asset_id
     AND ah.date_effective <= SYSDATE
     AND NVL (ah.date_ineffective, SYSDATE + 1) > SYSDATE
     AND ah.category_id = cat.category_id
     AND bk.asset_id = ad.asset_id
     AND bk.book_type_code = P_BOOK_CODE
--     AND bk.date_ineffective IS NULL
     AND lookups_at.lookup_code = ad.asset_type
     AND lookups_at.lookup_type = 'ASSET TYPE'
     AND lookups_nu.lookup_code = ad.new_used
     AND lookups_nu.lookup_type = 'NEWUSE'
     AND lookups_ol.lookup_code = ad.owned_leased
     AND lookups_ol.lookup_type = 'OWNLEASE'
     AND lookups_iu.lookup_code = ad.in_use_flag
     AND lookups_iu.lookup_type = 'YESNO'
     AND lookups_pt.lookup_code(+) = ad.property_type_code
     AND lookups_pt.lookup_type(+) = 'PROPERTY TYPE'
     AND lookups_12.lookup_code(+) = ad.property_1245_1250_code
     AND lookups_12.lookup_type(+) = '1245/1250 PROPERTY'
	 AND  ad.asset_id = ai.asset_id(+)   --------- for invoice date
	 AND ad.asset_id = fd.asset_id -- for distribution qty
	 AND fd.location_id = loc.location_id   -- for location
	 AND bk.asset_id = fr.asset_id(+)
	 AND bk.book_type_code= fr.book_type_code(+)
	 AND ad.asset_id = NVL(fth.asset_id,ad.asset_id)
	 AND NVL(fth.book_type_code,bk.book_type_code)=bk.book_type_code
	 AND fr.transaction_header_id_in = fth.transaction_header_id(+)
	 AND fr.transaction_header_id_in(+) = bk.transaction_header_id_in
         AND AI.PO_VENDOR_ID = Pv.VENDOR_ID(+)
         AND AI.DATE_INEFFECTIVE IS NULL
	-- AND jfb.block_id = jfbd.block_id
	-- AND jfbd.BOOK_TYPE_CODE = bk.book_type_code
         AND ai.PO_NUMBER = ph.segment1(+)
         AND ph.vendor_id = fw.po_vendor_id(+)
-----------     AND bk.date_placed_in_service BETWEEN TO_DATE(P_DATE_FROM,'YYYY/MM/DD HH24:MI:SS') AND TO_DATE(P_DATE_TO,'YYYY/MM/DD HH24:MI:SS')
         AND NVL(TO_DATE(P_DATE_FROM,'YYYY/MM/DD HH24:MI:SS'),bk.date_placed_in_service) <= bk.date_placed_in_service
         AND NVL(TO_DATE(P_DATE_TO,'YYYY/MM/DD HH24:MI:SS'),bk.date_placed_in_service) >= bk.date_placed_in_service
-----------    AND ad.attribute_category_code = p_cat_segment1
         AND cat.category_id = NVL(P_CAT_SEGMENT1,cat.category_id)
--AND cat.segment1 = p_cat_segment1
--AND cat.segment2 = p_cat_segment2
--AND cat.segment3 = p_cat_segment3
         AND loc.segment1 = NVL(P_LOCATION,loc.segment1)
	 AND ad.asset_id = fds.asset_id(+)
      --   AND deprn_source_code = 'DEPRN'
	 		 AND ap.invoice_id(+) = ai.invoice_id  -- added for document seq num/voucher number
		         AND ap.invoice_num(+) = ai.invoice_number -- added for document seq num/voucher number
			 	-- AND ap.org_id = ph.org_id  -- added for document seq num/voucher number
         ORDER BY ad.asset_number;


BEGIN



	BEGIN

		SELECT segment1||'.'|| segment2||'.'|| segment3 INTO l_concat_segments
		FROM fa_categories
		WHERE category_id = p_cat_segment1;
	EXCEPTION
	WHEN OTHERS THEN
		Fnd_File.put_line(Fnd_File.OUTPUT,'Exception found in selecting categories');
	END;

       Fnd_File.put_line(Fnd_File.OUTPUT,CHR(9)            ||'Fixed Asset Register Report');
--       Fnd_File.put_line(Fnd_File.OUTPUT,CHR(9)            ||'---------------------------');
       Fnd_File.put_line(Fnd_File.OUTPUT,CHR(9)            ||'                   ');
      Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'As on Date:'
                         || CHR (9)
                         || SYSDATE
                         || CHR (9)
                        );
      Fnd_File.put_line (Fnd_File.output, ' ');
      Fnd_File.put_line (Fnd_File.output, 'REPORT PARAMETRS ');
      Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'BOOK TYPE CODE:'
                         || CHR (9)
                         || p_book_code
                         || CHR (9)
                        );
        Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Date From:'
                         || CHR (9)
                         || p_date_from
                         || CHR (9)
                        );
      Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || ' Date To:'
                         || CHR (9)
                         || p_date_to
                         || CHR (9)
                        );
        Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Category Segments:'
                         || CHR (9)
                         || l_concat_segments
                         || CHR (9)
                        );
      Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Asset Location:'
                         || CHR (9)
                         || p_location
                         || CHR (9)
                        );

       --Printing Output
       Fnd_File.put_line(Fnd_File.OUTPUT,CHR(9)            ||'Voucher Number'||CHR(9)
	                                                   ||'Invoice Date'||CHR(9)
	                                                   ||'Date in Service'||CHR(9)
							   ||'Asset Number'||CHR(9)
							   ||'Quantity'||CHR(9)
           						   ||'Asset location'||CHR(9)
			           			   ||'Major Category'||CHR(9)
						           ||'Minor Category1'||CHR(9)
							   ||'Minor category2'||CHR(9)
 							   ||'Asset Description'||CHR(9)
           						   ||'Original Cost of Assets'||CHR(9)
			           			   ||'Final Cost of Assets'||CHR(9)
           						   ||'Depreciation Companies Act'||CHR(9)
           						   ||'Overriding Depreciation method'||CHR(9)
           						--   ||'Depreciation Rate- Income Tax'||CHR(9)
           						   ||'Sale of Asset Date'||CHR(9)
							   ||'Realisation Value'||CHR(9)
							   ||'Depreciation charged till Date'||CHR(9)
							   ||'Accumulated Depreciation'||CHR(9)
							   ||'Remarks for sale of Assets'||CHR(9)
							   ||'Vendor Name'||CHR(9)
							   ||'PO Number'||CHR(9)
							   ||'PO Date'||CHR(9)
							   ||'Party Document Number'||CHR(9)
							   ||'Warrent Up to date'||CHR(9)
							   );

     /*  --Printing Output
	   Fnd_File.put_line(Fnd_File.OUTPUT,CHR(9)        ||'---------------'||CHR(9)
	                                                   ||'------------'||CHR(9)
	                                                   ||'--------------'||CHR(9)
							   ||'--------'||CHR(9)
							   ||'--------'||CHR(9)
           						   ||'--------------'||CHR(9)
			           			   ||'--------------'||CHR(9)
						           ||'---------------'||CHR(9)
							   ||'---------------'||CHR(9)
 							   ||'-----------------'||CHR(9)           -- description
           						   ||'-----------------------'||CHR(9)     -- org cost
			           			   ||'--------------------'||CHR(9)         -- final cost
           						   ||'--------------------------'||CHR(9)  -- dca
           						   ||'------------------------------'||CHR(9)  -- odm
           						   ||'-----------------------------'||CHR(9)   --income tax
           						   ||'------------------'||CHR(9)              -- sale of asset date
							   ||'-----------------'||CHR(9)             -- relisation value
							   ||'------------------------------'||CHR(9) -- dc till date
							   ||'------------------------'||CHR(9)    -- acc dep
							   ||'--------------------------'||CHR(9)   -- remakes
							   ||'-----------'||CHR(9)   -- vendor name
							   ||'---------'||CHR(9)   -- po num
							   ||'-------'||CHR(9)  -- po date
							   ||'---------------------'||CHR(9) -- party doc num
							   ||'------------------'||CHR(9)  -- warrenty up to date
							   );  */

       --Opening and Fetching Cursor Values into local variables.
	   FOR rec_data IN cur_data--(p_from_date,p_to_date)
	   LOOP

	              --Printing Local Variable Values to output.
				  Fnd_File.put_line(Fnd_File.OUTPUT,CHR(9) ||rec_data.doc_sequence_value||CHR(9) -- document sequence
	                                                   ||rec_data.INV_DATE||CHR(9)
	                                                   ||rec_data.dpis||CHR(9)
							   ||rec_data.ASS_NO||CHR(9)
							   ||rec_data.QTY||CHR(9)           -- qty
           						   ||rec_data.asset_location||CHR(9)
			           			   ||rec_data.Major_cat||CHR(9)
						           ||rec_data.min_cat1||CHR(9)
							   ||rec_data.min_cat2||CHR(9)
							   ||rec_data.ASSET_desc||CHR(9)
           						   ||rec_data.original_cost||CHR(9)
			           			   ||rec_data.final_cost||CHR(9)
           						   ||rec_data.dep_com_act||CHR(9)
           						   ||rec_data.deprn_method_code||CHR(9)  -- overriding dep method
           						--   ||rec_data.DEP_RATE||CHR(9)
							   ||rec_data.date_retired||CHR(9)
							   ||rec_data.proceeds_of_sale||CHR(9)
							   ||rec_data.ytd_value||CHR(9)     -- dep charged till date
							   ||rec_data.ltd_value||CHR(9)     -- accumulated dep
							   ||rec_data.transaction_name||CHR(9)
							   ||rec_data.vendor_name||CHR(9)
							   ||rec_data.po_number||CHR(9)
							   ||rec_data.creation_date||CHR(9)
							   ||rec_data.invoice_number||CHR(9)
							   ||rec_data.end_date||CHR(9)

							   );




	   END LOOP;
END xx_fa_reg_rpt_prc;
/


CREATE OR REPLACE PROCEDURE APPS.XXABRL_OPEN_GRN_REC_ACCRUAL (
   errbuf          OUT NOCOPY      VARCHAR2,
   retcode         OUT NOCOPY      NUMBER,
   p_from_date    IN               VARCHAR2 DEFAULT NULL,
   p_as_of_date    IN              VARCHAR2 DEFAULT NULL,
   p_sbu_segment   IN              VARCHAR2 DEFAULT NULL
)
IS
   
   /*=================================================================================================================
   
     Report Name  : XXABRL Accrual Expenses Reconciliation Report
     Filename     : XXABRL_OPEN_GRN_REC_ACCRUAL.sql
     
            Version          Name                             Date               Remarks
            ~~~~~~          ~~~~~                            ~~~~~              ~~~~~~~~
            1.0.0            Kumaresan                     31-MAR-2014        New Development
            1.0.1            Kumaresan                     25-APR-2014        Added Manual JV/Spread Sheet (Query)    
                    
   =================================================================================================================*/
     
   v_open_grn_amt    NUMBER;
   v_grn_total_amt   NUMBER;
   v_grn_sum         NUMBER;
   v_from_date       DATE;
   v_as_of_date      DATE;
   v_org_id          NUMBER;
   v_sbu_sgement     VARCHAR2 (10);
   v_sbu             VARCHAR2 (100);
   v_inv_total_amt   NUMBER;   
   v_inv_opn_amt     NUMBER;
   v_grn_full_amt    NUMBER;
   v_grn_full_tax    NUMBER;
   
   v_grn_amt_cost    NUMBER; 
   v_grn_tax_pur     NUMBER;
   v_jv_debit_amt  NUMBER;
   v_jv_credit_amt NUMBER;
   
   v_prv_grnamt  NUMBER;
      --v_grn_amt_cost  v_grn_tax_pur   

      
BEGIN
   v_sbu_sgement := p_sbu_segment;
   v_from_date  := NVL (fnd_date.canonical_to_date (p_from_date), SYSDATE);
   v_as_of_date := NVL (fnd_date.canonical_to_date (p_as_of_date), SYSDATE);


   SELECT   description
       INTO v_sbu
       FROM fnd_flex_values_vl
      WHERE (   ('' IS NULL)
             OR (structured_hierarchy_level IN (
                    SELECT hierarchy_id
                      FROM fnd_flex_hierarchies_vl h
                     WHERE h.flex_value_set_id = 1013466
                       AND h.hierarchy_name LIKE '')
                )
            )
        AND (flex_value = v_sbu_sgement)
        AND (flex_value_set_id = 1013466)
   ORDER BY flex_value;


 IF  V_SBU <> 'N/A'  THEN
   fnd_file.put_line (fnd_file.output, '  ');
   fnd_file.put_line (fnd_file.output, '  ');
   fnd_file.put_line (fnd_file.output, '  ');
   fnd_file.put_line (fnd_file.output,
                      CHR (9) || 'XXABRL Accrual Expenses Reconciliation Report' 
                       || CHR (9)||' '|| CHR (9)||' '|| CHR (9)||' '|| CHR (9)||'Run Date : '
                       || SYSDATE
                       );
   fnd_file.put_line (fnd_file.output, CHR (9) || '                   ');
   fnd_file.put_line (fnd_file.output, ' ');
   fnd_file.put_line (fnd_file.output, 'REPORT PARAMETRS ');
   fnd_file.put_line (fnd_file.output, ' ');
   fnd_file.put_line (fnd_file.output, CHR (9) || ' SBU Name   :   ' || v_sbu);
   fnd_file.put_line (fnd_file.output,
                      CHR (9) || ' From Date  :   ' || v_from_date
                     );
   fnd_file.put_line (fnd_file.output,
                      CHR (9) || ' To Date    :   ' || v_as_of_date
                     );
                     
   fnd_file.put_line (fnd_file.output, '  ');
   fnd_file.put_line (fnd_file.output, '  ');
   fnd_file.put_line (fnd_file.output,
                         SUBSTR (RPAD ('Operating Unit', 25, ' '), 1, 25)
                      || ' '
                      || SUBSTR (LPAD ('PO No   ', 12, ' '), 1, 12)
                      || ' '
                      || SUBSTR (LPAD ('PO Date', 12, ' '), 1, 12)
                      || ' '                      
                      || SUBSTR (LPAD ('PO Amount', 15, ' '), 1, 15)
                      || ' '
                      || SUBSTR (LPAD ('GRN No', 12, ' '), 1, 12)
                      || ' '
                      || SUBSTR (LPAD ('GRN Date', 12, ' '), 1, 12)
                      || ' '
                      || SUBSTR (LPAD ('GRN Amount', 15, ' '), 1, 15)
                      || ' '
                      || SUBSTR (LPAD ('GRN Tax', 15, ' '), 1, 15)
                      || ' '
                      || SUBSTR (LPAD ('GRN Total', 20, ' '), 1, 20)
                      || ' '
                      || SUBSTR (LPAD ('Invoice Amt', 20, ' '), 1, 20)                      
                      || ' '
                      || SUBSTR (LPAD ('GRN Open Bal', 20, ' '), 1, 20)
                      || ' '
                      || SUBSTR (RPAD ('                   Segments',  45, ' '), 1, 45)
                      || ' '
                      || SUBSTR (RPAD ('Supplier Code', 15, ' '), 1, 15)
                      || ' '
                      || SUBSTR (RPAD ('        Supplier Name', 30, ' '), 1, 30)
                     );

                     
   fnd_file.put_line (fnd_file.output, RPAD ('-', ROUND (283 / 1), '-'));
   fnd_file.put_line (fnd_file.output, ' ');

   FOR grn IN
      (
       SELECT podet.operating_unit,
              podet.po_number,
              podet.po_date, 
              podet.supplier_name, 
              rep.receipt_num,
              NVL (trunc(podet.po_amt,2), 0) po_amt,
              NVL (trunc(rep.grn_amt,2 ), 0) grn_amt, 
              NVL (trunc(rep.grn_tax,2 ), 0) grn_tax,
              NVL (trunc(rep.inv_amt,2 ), 0) inv_amt, 
              rep.accounts, 
              podet.grn_date,
              podet.supplier_code
         FROM (SELECT   grn_sum.receipt_num, 
                        SUM (grn_sum.grn_amt) grn_amt,
                     CASE WHEN SUM (grn_sum.grn_amt) <> 0 and xx_jai_grn_only_tax(grn_sum.receipt_num) = 0 THEN
                        (decode (sign(SUM (grn_sum.grn_amt)),'-1',-(xx_grn_only_tax(grn_sum.receipt_num,v_as_of_date)),
                        xx_grn_only_tax(grn_sum.receipt_num,v_as_of_date)) )
                         WHEN SUM (grn_sum.grn_amt) <> 0 and xx_jai_grn_only_tax(grn_sum.receipt_num) < 0 THEN
                        (decode (sign(SUM (grn_sum.grn_amt)),'-1',-(xx_grn_tax_return(grn_sum.receipt_num,v_as_of_date)),
                        xx_grn_tax_return(grn_sum.receipt_num,v_as_of_date)) )                                                
                        else
                        xx_jai_grn_only_tax(grn_sum.receipt_num)                                                
                      END AS grn_tax,                          
                        NVL (NULL, 0) AS inv_amt,
                        MIN (grn_sum.accounts) accounts, 
                        MIN (grn_date) dates
                   FROM (SELECT   grn.receipt_num receipt_num,
                                  (SUM (NVL (TRUNC (grn.grn_amount, 2), 0))
                                  ) grn_amt,
                                  grn.accounts accounts,
                                  grn.creation_date grn_date, grn.org_id
                             FROM (SELECT rsh.receipt_num, 
                                          TRUNC
                                              (rt.creation_date)
                                                                creation_date,
                                          TRUNC (rt.creation_date) trans_date,
                                          rsl.quantity_shipped,
                                          rt.po_unit_price,
                                          (  rt.po_unit_price
                                           * rsl.quantity_shipped
                                          ) AS po_amt,
                                          rt.transaction_id,
                                          rt.organization_id,
                                          rt.transaction_type,
                                          DECODE
                                             (rt.transaction_type,
                                              'RETURN TO RECEIVING', (  (  rt.po_unit_price
                                                                         * rt.quantity
                                                                        )
                                                                      * -1
                                               ),
                                              (rt.po_unit_price * rt.quantity
                                              )
                                             ) AS grn_amount,
                                          pd.org_id,
                                          (SELECT concatenated_segments
                                             FROM apps.gl_code_combinations_kfv
                                            WHERE code_combination_id =  gc.code_combination_id)  accounts
                                     FROM  (select creation_date, 
                                                   po_unit_price, 
                                                   transaction_id, 
                                                   organization_id, 
                                                   transaction_type,
                                                   shipment_line_id,
                                                   po_distribution_id,
                                                   quantity
                                              from po.rcv_transactions ) rt,
                                          (select po_distribution_id,
                                                  accrual_account_id,
                                                  org_id                                                  
                                             from apps.po_distributions_all) pd,
                                          (select code_combination_id,
                                                  segment6,
                                                  segment3
                                             from apps.gl_code_combinations) gc,
                                          (select shipment_line_id,
                                                  shipment_header_id,
                                                  quantity_shipped
                                             from po.rcv_shipment_lines ) rsl,
                                          (select shipment_header_id,
                                                  receipt_num 
                                             from po.rcv_shipment_headers) rsh
                                    WHERE 1 = 1
                                      AND rt.po_distribution_id = pd.po_distribution_id
                                      AND pd.accrual_account_id = gc.code_combination_id
                                      AND rt.shipment_line_id =  rsl.shipment_line_id
                                      AND rsh.shipment_header_id =  rsl.shipment_header_id
                                      AND TRUNC (rt.creation_date) between v_from_date and v_as_of_date                                      
                                      AND gc.segment6 = '361901'
                                      AND gc.segment3 = v_sbu_sgement                                          
                                      AND rt.transaction_type IN
                                             ('RETURN TO RECEIVING',
                                              'RECEIVE')) grn
                         GROUP BY grn.receipt_num,
                                  grn.org_id,
                                  grn.accounts,
                                  grn.creation_date) grn_sum,            
                     ((
                           SELECT tab_grn.receipt_num
                             FROM (SELECT   inv_sum.receipt_num,
                                            SUM (inv_sum.inv_amt) inv_amt
                                       FROM (SELECT   aid.org_id,
                                                      rsh.receipt_num,
                                                      ai.doc_sequence_value,
                                                      DECODE
                                                         (ai.invoice_currency_code,
                                                          'INR', SUM
                                                             (NVL (aid.amount,
                                                                   0
                                                                  )
                                                             ),
                                                          SUM
                                                             (NVL
                                                                 (aid.base_amount,
                                                                  0
                                                                 )
                                                             )
                                                         ) inv_amt,
                                                      aid.accounting_date,
                                                      gc.concatenated_segments  accounts
                                                 FROM (select invoice_id,
                                                              invoice_currency_code,
                                                              doc_sequence_value,
                                                              vendor_id 
                                                         from apps.ap_invoices_all) ai,
                                                      (select dist_code_combination_id,
                                                              accounting_date,
                                                              org_id,
                                                              rcv_transaction_id,
                                                              invoice_id,
                                                              base_amount,
                                                              amount
                                                          from apps.ap_invoice_distributions_all) aid,
                                                      (select vendor_id
                                                         from apps.ap_suppliers) asu,
                                                      (select segment6,
                                                              segment3,
                                                              code_combination_id,
                                                              concatenated_segments
                                                         from apps.gl_code_combinations_kfv) gc,
                                                      (select transaction_id,
                                                              shipment_line_id                                                              
                                                          from po.rcv_transactions) rt,
                                                      (select shipment_header_id,
                                                               shipment_line_id
                                                         from po.rcv_shipment_lines) rsl,
                                                      (select shipment_header_id,
                                                              receipt_num
                                                         from po.rcv_shipment_headers) rsh
                                                WHERE 1 = 1
                                                  AND asu.vendor_id = ai.vendor_id
                                                  AND ai.invoice_id = aid.invoice_id
                                                  AND aid.dist_code_combination_id =  gc.code_combination_id
                                                  AND aid.rcv_transaction_id =  rt.transaction_id(+)
                                                  AND rt.shipment_line_id = rsl.shipment_line_id(+)
                                                  AND rsl.shipment_header_id = rsh.shipment_header_id(+)                                                                                             
                                                  AND gc.segment6 = '361901'
                                                  AND gc.segment3 = v_sbu_sgement                                                      
                                                  AND TRUNC (aid.accounting_date) between v_from_date and v_as_of_date
                                             GROUP BY ai.doc_sequence_value,
                                                      rsh.receipt_num,
                                                      aid.org_id,
                                                      aid.accounting_date,
                                                      ai.invoice_currency_code,
                                                      gc.concatenated_segments) inv_sum
                                   GROUP BY inv_sum.receipt_num) tab_inv,
                                  (SELECT   grn_sum.receipt_num,
                                            SUM (grn_sum.grn_amt) grn_amt
                                       FROM (SELECT   grn.receipt_num
                                                                  receipt_num,
                                                      (SUM
                                                          (NVL
                                                              (TRUNC
                                                                  (grn.grn_amount,
                                                                   2
                                                                  ),
                                                               0
                                                              )
                                                          )
                                                      ) grn_amt,
                                                      grn.accounts accounts,
                                                      grn.creation_date
                                                                     grn_date,
                                                      grn.org_id
                                                 FROM (SELECT rsh.receipt_num,
                                                              TRUNC
                                                                 (rt.creation_date
                                                                 )
                                                                 creation_date,
                                                              TRUNC
                                                                 (rt.creation_date
                                                                 ) trans_date,
                                                              rsl.quantity_shipped,
                                                              rt.po_unit_price,
                                                              (  rt.po_unit_price
                                                               * rsl.quantity_shipped
                                                              ) AS po_amt,
                                                              rt.transaction_id,
                                                              rt.organization_id,
                                                              rt.transaction_type,
                                                              DECODE
                                                                 (rt.transaction_type,
                                                                  'RETURN TO RECEIVING', (  (  rt.po_unit_price
                                                                                             * rt.quantity
                                                                                            )
                                                                                          * -1
                                                                   ),
                                                                  (  rt.po_unit_price
                                                                   * rt.quantity
                                                                  )
                                                                 )
                                                                 AS grn_amount,
                                                              pd.org_id,
                                                              (SELECT concatenated_segments
                                                                 FROM apps.gl_code_combinations_kfv
                                                                WHERE code_combination_id = gc.code_combination_id)    accounts
                                                         FROM  (select creation_date, 
                                                                       po_unit_price, 
                                                                       transaction_id, 
                                                                       organization_id, 
                                                                       transaction_type,
                                                                       shipment_line_id,
                                                                       po_distribution_id,
                                                                       quantity
                                                                  from po.rcv_transactions ) rt,
                                                              (select po_distribution_id,
                                                                      accrual_account_id,
                                                                      org_id                                                  
                                                                 from apps.po_distributions_all) pd,
                                                              (select code_combination_id,
                                                                      segment6,
                                                                      segment3
                                                                 from apps.gl_code_combinations) gc,
                                                              (select shipment_line_id,
                                                                      shipment_header_id,
                                                                      quantity_shipped
                                                                 from po.rcv_shipment_lines ) rsl,
                                                              (select shipment_header_id,
                                                                      receipt_num 
                                                                 from po.rcv_shipment_headers) rsh
                                                        WHERE 1 = 1
                                                          AND rt.po_distribution_id =    pd.po_distribution_id
                                                          AND pd.accrual_account_id =
                                                                 gc.code_combination_id
                                                          AND rt.shipment_line_id =
                                                                 rsl.shipment_line_id
                                                          AND rsh.shipment_header_id =
                                                                 rsl.shipment_header_id
                                                          AND TRUNC (rt.creation_date) between v_from_date and v_as_of_date                                                          
                                                          AND gc.segment6 =  '361901'
                                                          AND gc.segment3 = v_sbu_sgement                                                              
                                                          AND rt.transaction_type IN
                                                                 ('RETURN TO RECEIVING',
                                                                  'RECEIVE')) grn
                                             GROUP BY grn.receipt_num,
                                                      grn.org_id,
                                                      grn.accounts,
                                                      grn.creation_date) grn_sum
                                   GROUP BY grn_sum.receipt_num) tab_grn
                            WHERE tab_grn.receipt_num = tab_inv.receipt_num)) grn_sum_notin
                  WHERE grn_sum.receipt_num = grn_sum_notin.receipt_num(+)
                    and grn_sum_notin.receipt_num is null 
               GROUP BY grn_sum.receipt_num
               UNION ALL
               SELECT tab_grn.receipt_num, 
                      tab_grn.grn_amt,
                     CASE WHEN (tab_grn.grn_amt) <> 0 and xx_jai_grn_only_tax(tab_grn.receipt_num) = 0 THEN
                        ( decode (sign(tab_grn.grn_amt),'-1',-(xx_grn_only_tax(tab_grn.receipt_num,v_as_of_date)),
                        xx_grn_only_tax(tab_grn.receipt_num,v_as_of_date)) )
                        WHEN (tab_grn.grn_amt) <> 0 and xx_jai_grn_only_tax(tab_grn.receipt_num) < 0 THEN
                        ( decode (sign(tab_grn.grn_amt),'-1',-(xx_grn_tax_return(tab_grn.receipt_num,v_as_of_date)),
                        xx_grn_tax_return(tab_grn.receipt_num,v_as_of_date)) )                        
                        else
                        (xx_jai_grn_only_tax(tab_grn.receipt_num))                        
                      END AS grn_tax,
                      tab_inv.inv_amt, 
                      tab_inv.accounts, 
                      tab_inv.dates
                 FROM (SELECT   inv_sum.receipt_num,
                                SUM (inv_sum.inv_amt) inv_amt,
                                MIN (inv_sum.accounts) AS accounts,
                                MIN (inv_sum.dates) AS dates
                           FROM (SELECT   aid.org_id, rsh.receipt_num,
                                          ai.doc_sequence_value,
                                          DECODE
                                             (ai.invoice_currency_code,
                                              'INR', SUM (NVL (aid.amount, 0)),
                                              SUM (NVL (aid.base_amount, 0))
                                             ) inv_amt,
                                          aid.accounting_date dates,
                                          gc.concatenated_segments accounts
                                                 FROM (select invoice_id,
                                                              invoice_currency_code,
                                                              doc_sequence_value,
                                                              vendor_id 
                                                         from apps.ap_invoices_all) ai,
                                                      (select dist_code_combination_id,
                                                              accounting_date,
                                                              org_id,
                                                              rcv_transaction_id,
                                                              invoice_id,
                                                              base_amount,
                                                              amount
                                                          from apps.ap_invoice_distributions_all) aid,
                                                      (select vendor_id
                                                         from apps.ap_suppliers) asu,
                                                      (select segment6,
                                                              segment3,
                                                              code_combination_id,
                                                              concatenated_segments
                                                         from apps.gl_code_combinations_kfv) gc,
                                                      (select transaction_id,
                                                              shipment_line_id                                                              
                                                          from po.rcv_transactions) rt,
                                                      (select shipment_header_id,
                                                               shipment_line_id
                                                         from po.rcv_shipment_lines) rsl,
                                                      (select shipment_header_id,
                                                              receipt_num
                                                         from po.rcv_shipment_headers) rsh
                                    WHERE 1 = 1
                                      AND asu.vendor_id = ai.vendor_id
                                      AND ai.invoice_id = aid.invoice_id
                                      AND aid.dist_code_combination_id =
                                                        gc.code_combination_id
                                      AND aid.rcv_transaction_id = rt.transaction_id(+)
                                      AND rt.shipment_line_id = rsl.shipment_line_id(+)
                                      AND rsl.shipment_header_id = rsh.shipment_header_id(+)                                      
                                      AND gc.segment6 = '361901'
                                      AND gc.segment3 = v_sbu_sgement                                          
                                      AND TRUNC (aid.accounting_date ) between v_from_date and v_as_of_date
                                 GROUP BY ai.doc_sequence_value,
                                          rsh.receipt_num,
                                          aid.org_id,
                                          aid.accounting_date,
                                          ai.invoice_currency_code,
                                          gc.concatenated_segments) inv_sum
                       GROUP BY inv_sum.receipt_num) tab_inv,
                      (SELECT   grn_sum.receipt_num,
                                SUM (grn_sum.grn_amt) grn_amt
                           FROM (SELECT   grn.receipt_num receipt_num,
                                          (SUM (NVL (TRUNC (grn.grn_amount, 2),
                                                     0
                                                    )
                                               )
                                          ) grn_amt,
                                          grn.accounts accounts,
                                          grn.creation_date grn_date,
                                          grn.org_id
                                     FROM (SELECT rsh.receipt_num,
                                                  TRUNC
                                                     (rt.creation_date
                                                     ) creation_date,
                                                  TRUNC
                                                     (rt.creation_date
                                                     ) trans_date,
                                                  rsl.quantity_shipped,
                                                  rt.po_unit_price,
                                                  (  rt.po_unit_price
                                                   * rsl.quantity_shipped
                                                  ) po_amt,
                                                  rt.transaction_id,
                                                  rt.organization_id,
                                                  rt.transaction_type,
                                                  DECODE
                                                     (transaction_type,
                                                      'RETURN TO RECEIVING', (  (  rt.po_unit_price
                                                                                 * rt.quantity
                                                                                )
                                                                              * -1
                                                       ),
                                                      (  rt.po_unit_price
                                                       * rt.quantity
                                                      )
                                                     ) grn_amount,
                                                  pd.org_id,
                                                  (SELECT concatenated_segments
                                                     FROM apps.gl_code_combinations_kfv
                                                    WHERE code_combination_id =
                                                             gc.code_combination_id)
                                                                     accounts
                                             FROM  (select creation_date, 
                                                           po_unit_price, 
                                                           transaction_id, 
                                                           organization_id, 
                                                           transaction_type,
                                                           shipment_line_id,
                                                           po_distribution_id,
                                                           quantity
                                                      from po.rcv_transactions ) rt,
                                                  (select po_distribution_id,
                                                          accrual_account_id,
                                                          org_id                                                  
                                                     from apps.po_distributions_all) pd,
                                                  (select code_combination_id,
                                                          segment6,
                                                          segment3
                                                     from apps.gl_code_combinations) gc,
                                                  (select shipment_line_id,
                                                          shipment_header_id,
                                                          quantity_shipped
                                                     from po.rcv_shipment_lines ) rsl,
                                                  (select shipment_header_id,
                                                          receipt_num 
                                                     from po.rcv_shipment_headers) rsh
                                            WHERE 1 = 1
                                              AND rt.po_distribution_id =
                                                         pd.po_distribution_id
                                              AND pd.accrual_account_id =
                                                        gc.code_combination_id
                                              AND rt.shipment_line_id =
                                                          rsl.shipment_line_id
                                              AND rsh.shipment_header_id =
                                                        rsl.shipment_header_id
                                              AND TRUNC (rt.creation_date) between v_from_date and v_as_of_date                                                 
                                              AND gc.segment6 = '361901'
                                              AND gc.segment3 = v_sbu_sgement                                                  
                                              AND rt.transaction_type IN ('RETURN TO RECEIVING', 'RECEIVE')) grn
                                 GROUP BY grn.receipt_num,
                                          grn.org_id,
                                          grn.accounts,
                                          grn.creation_date) grn_sum
                       GROUP BY grn_sum.receipt_num) tab_grn
                WHERE tab_grn.receipt_num = tab_inv.receipt_num) rep,
              (SELECT DISTINCT hou.NAME operating_unit, ph.segment1 po_number,
                               (SELECT SUM (pl.quantity * pl.unit_price
                                           )
                                  FROM apps.po_lines_all pl
                                 WHERE pl.po_header_id = ph.po_header_id)
                                                                       po_amt,
                               TRUNC (ph.creation_date) po_date,
                               pv.vendor_name supplier_name,
                               rsh.receipt_num receipt_num,
                               pv.segment1 supplier_code,
                               TRUNC (rsh.creation_date) grn_date
                          FROM (select po_header_id,
                                       shipment_header_id                                        
                                  from po.rcv_shipment_lines) rsl,
                               (select shipment_header_id,
                                       creation_date,
                                       receipt_num
                                  from po.rcv_shipment_headers) rsh,
                               (select vendor_id,
                                       vendor_name,
                                       segment1 
                                  from apps.po_vendors) pv,
                               (select organization_id,
                                       NAME 
                                  from apps.hr_operating_units) hou,
                               (select po_header_id,
                                       vendor_id,
                                       org_id,
                                       segment1,
                                       creation_date
                                  from apps.po_headers_all) ph
                         WHERE  rsh.shipment_header_id = rsl.shipment_header_id
                           AND rsl.po_header_id = ph.po_header_id
                           AND hou.organization_id = ph.org_id
                           AND pv.vendor_id = ph.vendor_id) podet
        WHERE podet.receipt_num = rep.receipt_num 
         --and rep.receipt_num = 9820024437 
          )
   LOOP
   
      IF grn.po_amt > 0  THEN 
       
          /*IF (  (NVL (grn.grn_amt, 0) + NVL (grn.grn_tax, 0))
              - (NVL (grn.inv_amt, 0))
             ) <> 0
          THEN*/
          
          
          
              v_inv_opn_amt  := nvl(v_inv_opn_amt,0) + NVL (grn.inv_amt, 0); 
          
             v_open_grn_amt :=
                (  (NVL (grn.grn_amt, 0) + NVL (grn.grn_tax, 0))
                 - (NVL (grn.inv_amt, 0))
                );
             v_grn_total_amt := (NVL (grn.grn_amt, 0) + NVL (grn.grn_tax, 0));
             
             v_grn_full_amt  := nvl(v_grn_full_amt,0) +  nvl(v_grn_total_amt,0);
             
             v_grn_amt_cost  := nvl(v_grn_amt_cost,0) +  NVL (grn.grn_amt, 0);
             
             v_grn_tax_pur   := nvl(v_grn_tax_pur,0) +  NVL (grn.grn_tax, 0);
             
              
                 
             v_grn_sum := NVL (v_grn_sum, 0) + v_open_grn_amt;

            fnd_file.put_line (fnd_file.output,
                             SUBSTR (RPAD (grn.operating_unit, 25, ' '), 1, 25)
                          || ' '
                          || SUBSTR (LPAD (grn.po_number, 12, ' '), 1, 12)
                          || ' '
                          || SUBSTR (LPAD (grn.po_date, 12, ' '), 1, 12)
                          || ' '                      
                          || SUBSTR (LPAD (grn.po_amt, 15, ' '), 1, 15)
                          || ' '
                          || SUBSTR (LPAD (grn.receipt_num, 12, ' '), 1, 12)
                          || ' '
                          || SUBSTR (LPAD (grn.grn_date, 12, ' '), 1, 12)
                          || ' '
                          || SUBSTR (LPAD (grn.grn_amt, 15, ' '), 1, 15)
                          || ' '
                          || SUBSTR (LPAD (grn.grn_tax, 15, ' '), 1, 15)
                          || ' '
                          || SUBSTR (LPAD (v_grn_total_amt, 20, ' '), 1, 20)
                          || ' '
                          || SUBSTR (LPAD (grn.inv_amt, 20, ' '), 1, 20)                      
                          || ' '
                          || SUBSTR (LPAD (v_open_grn_amt, 20, ' '), 1, 20)
                          || '     '
                          || SUBSTR (RPAD (grn.accounts,  45, ' '), 1, 45)
                          || ' '
                          || SUBSTR (RPAD (grn.supplier_code, 15, ' '), 1, 15)
                          || ' '
                          || SUBSTR (RPAD (grn.supplier_name, 30, ' '), 1, 30)
                         );

                               
         -- END IF;
      
      END IF;
      
   END LOOP;

   -- v_grn_full_amt   
   fnd_file.put_line (fnd_file.output, ' ');
   fnd_file.put_line (fnd_file.output, RPAD ('-', ROUND (283 / 1), '-'));
   fnd_file.put_line (fnd_file.output, ' ');
   fnd_file.put_line (fnd_file.output,
                         ' '
                         || LPAD ('Total  ', 130, ' ')  --148 
                         || LPAD (v_grn_full_amt, 15, ' ') 
                         || LPAD (v_inv_opn_amt, 21, ' ')
                         ||LPAD(v_grn_sum,21,' ')    
                    --    || LPAD ('Total  ', 169, ' ') || LPAD (v_inv_opn_amt, 15, ' ')||LPAD(v_grn_sum,21,' ')
                     );
   fnd_file.put_line (fnd_file.output, ' ');
   fnd_file.put_line (fnd_file.output, RPAD ('-', ROUND (283 / 1), '-'));
   

   
   ---------------------------------------------------------------------------
                         --         Manual  
   ---------------------------------------------------------------------------
   
   fnd_file.put_line (fnd_file.output, '  ');
   fnd_file.put_line (fnd_file.output, '  ');   
   fnd_file.put_line (fnd_file.output, '  ');
   fnd_file.put_line (fnd_file.output, '  ');
   fnd_file.put_line (fnd_file.output,
                      CHR (9) || '   Manual Invoices   (  Payables  ) '
                     );

   fnd_file.put_line (fnd_file.output, '  ');
   fnd_file.put_line (fnd_file.output, '  ');   
   fnd_file.put_line (fnd_file.output, '  ');
                                   

   fnd_file.put_line (fnd_file.output,
                         SUBSTR (RPAD ('Operating Unit', 25, ' '), 1, 25)
                      || ' '
                      || SUBSTR (RPAD ('Supplier Name', 30, ' '), 1, 30)
                      || ' '
                      || SUBSTR (LPAD ('Supplier NO', 15, ' '), 1, 15)
                      || ' '
                      || SUBSTR (LPAD ('Invoice Date', 15, ' '), 1, 15)
                      || ' '
                      || SUBSTR (LPAD ('Invoice Amount', 15, ' '), 1, 15)
                      || ' '
                      || SUBSTR (LPAD ('Voucher No', 15, ' '), 1, 15)
                      || ' '                      
                      || LPAD ('Segments              ', 40, ' ')
                     );
   fnd_file.put_line (fnd_file.output, RPAD ('-', ROUND (168 / 1), '-'));
   fnd_file.put_line (fnd_file.output, ' ');
   
      
   
   FOR MANU IN (  SELECT 
                        (SELECT DISTINCT hou.NAME
                         FROM apps.hr_operating_units hou
                          WHERE hou.organization_id = aid.org_id ) operating_unit,
                            asu.vendor_name     supplier_name, 
                            asu.segment1  supplier_number,
                             aid.accounting_date dates,
                             DECODE ( ai.invoice_currency_code,'INR', nvl(SUM (aid.amount),0),
                             SUM (NVL (aid.base_amount, 0))) inv_amt,
                             ai.doc_sequence_value,                        
                             gc.concatenated_segments accounts
                        FROM apps.ap_invoices_all ai,
                             apps.ap_invoice_distributions_all aid,
                             apps.ap_suppliers asu,
                             apps.gl_code_combinations_kfv gc,
                             po.rcv_transactions rt,
                             po.rcv_shipment_lines rsl,
                             po.rcv_shipment_headers rsh
                       WHERE 1 = 1
                         AND asu.vendor_id = ai.vendor_id
                         AND ai.invoice_id = aid.invoice_id
                         AND aid.dist_code_combination_id = gc.code_combination_id
                         AND aid.rcv_transaction_id = rt.transaction_id(+)
                         AND rt.shipment_line_id = rsl.shipment_line_id(+)
                         AND rsl.shipment_header_id = rsh.shipment_header_id(+)
                         AND gc.segment6 = '361901'
                         AND gc.segment3 = v_sbu_sgement
                         AND aid.accounting_date between v_from_date and v_as_of_date
                         AND rsh.receipt_num is null
                         --AND ai.doc_sequence_value IN (363021214,363026906) 802
                    GROUP BY ai.doc_sequence_value,
                             rsh.receipt_num,
                             aid.org_id,
                             aid.accounting_date,
                             ai.invoice_currency_code,
                             gc.concatenated_segments, 
                             asu.vendor_name,asu.segment1
                             )
      LOOP
        
        if manu.inv_amt <> 0  then   
      
         v_inv_total_amt := nvl(v_inv_total_amt,0) + NVL (manu.inv_amt, 0);
      
         
         fnd_file.put_line (fnd_file.output,
                               SUBSTR (RPAD (manu.operating_unit, 25, ' '),
                                       1,
                                       25
                                      )
                            || ' '
                            || SUBSTR (RPAD (manu.supplier_name, 30, ' '),
                                       1,
                                       30
                                      )
                            || ' '
                            || SUBSTR (LPAD (manu.supplier_number, 15, ' '), 1, 15)
                            || ' '
                            || SUBSTR (lPAD (manu.dates, 15, ' '), 1, 15)
                            || ' '
                            || SUBSTR (LPAD (manu.inv_amt, 15, ' '), 1, 15)
                            || ' '
                            || SUBSTR (LPAD (manu.doc_sequence_value, 15, ' '), 1, 15)
                            || ' '
                            || LPAD (manu.accounts, 40, ' ')
                           );
           end if;
          
        END LOOP;
        
   fnd_file.put_line (fnd_file.output, ' ');
   fnd_file.put_line (fnd_file.output, RPAD ('-', ROUND (168 / 1), '-'));
   fnd_file.put_line (fnd_file.output, ' ');
   fnd_file.put_line (fnd_file.output,
                         ' '
                      || LPAD ('Total  ', 88, ' ')
                      || LPAD (v_inv_total_amt, 15, ' ')
                     );
   fnd_file.put_line (fnd_file.output, ' ');
   fnd_file.put_line (fnd_file.output, RPAD ('-', ROUND (168 / 1), '-'));
                                      
  
   fnd_file.put_line (fnd_file.output, '  ');
   fnd_file.put_line (fnd_file.output, '  ');   
   fnd_file.put_line (fnd_file.output, '  ');
   
   
   --------------------------------------------------------------------------
              -- Previous Grn Amount for Current period Invoices
   --------------------------------------------------------------------------   
   
   fnd_file.put_line (fnd_file.output, '  ');
   fnd_file.put_line (fnd_file.output,
                      CHR (9) || '   Previous Grn Amount for Current period Invoices  '
                     );

   fnd_file.put_line (fnd_file.output, '  ');
   fnd_file.put_line (fnd_file.output, '  ');   
   fnd_file.put_line (fnd_file.output, '  ');   
   

   fnd_file.put_line (fnd_file.output,
                      SUBSTR (LPAD ('Receipt No', 15, ' '), 1, 15)
                    || ' '
                    || SUBSTR (LPAD ('Acct Date'    , 15, ' '), 1, 15)
                    || ' '
                    || SUBSTR (LPAD ('Voucher No', 20, ' '), 1, 20)
                    || ' '
                    || SUBSTR (LPAD ('Invoice Amt', 15, ' '), 1, 15)
                    || ' '                    
                    || SUBSTR (RPAD ('SBU', 10, ' '), 1, 10)
                   );
    --fnd_file.put_line (fnd_file.output, ' ');
    fnd_file.put_line (fnd_file.output, RPAD ('-', ROUND (75 / 1), '-'));

   
   FOR PRV_GRN IN( SELECT   MIN (aid.accounting_date) acc_date,
                    MIN (grn.trans_date) grn_date,
                    ai.doc_sequence_value, rsh.receipt_num, 
                    DECODE (ai.invoice_currency_code,
                         'INR', NVL (SUM (aid.amount), 0),
                         SUM (NVL (aid.base_amount, 0))
                        ) inv_amt,gc.segment3
            FROM apps.ap_invoices_all ai,
                 apps.ap_invoice_distributions_all aid,
                 apps.ap_suppliers asu,
                 apps.gl_code_combinations_kfv gc,
                 po.rcv_transactions rt,
                 po.rcv_shipment_lines rsl,
                 po.rcv_shipment_headers rsh,
                 (SELECT rsh.receipt_num, TRUNC (rt.creation_date) trans_date,
                         rsl.quantity_shipped, rt.po_unit_price,
                         (rt.po_unit_price * rsl.quantity_shipped) AS po_amt,
                         rt.transaction_id, rt.organization_id, rt.transaction_type,
                         DECODE (transaction_type,
                                 'RETURN TO RECEIVING', (  (  rt.po_unit_price
                                                            * rsl.quantity_shipped
                                                           )
                                                         * -1),
                                 (rt.po_unit_price * rsl.quantity_shipped
                                 )
                                ) AS grn_amount,
                         DECODE (transaction_type,
                                 'RETURN TO RECEIVING', (jrl.tax_amount * -1),
                                 (jrl.tax_amount
                                 )
                                ) tax_amount
                    FROM po.rcv_transactions rt,
                         po.rcv_shipment_lines rsl,
                         po.rcv_shipment_headers rsh,
                         apps.ap_suppliers asu,
                         apps.org_organization_definitions ood,
                         apps.hr_operating_units ho,
                         ja.jai_rcv_lines jrl
                   WHERE 1 = 1
                     AND rt.shipment_line_id = rsl.shipment_line_id
                     AND rsl.shipment_header_id = rsh.shipment_header_id
                     AND asu.vendor_id = rt.vendor_id
                     AND rt.organization_id = ood.organization_id
                     AND ood.operating_unit = ho.organization_id
                     AND jrl.shipment_header_id = rsh.shipment_header_id
                     AND jrl.shipment_line_id = rsl.shipment_line_id
                     AND TRUNC (rt.creation_date) < v_from_date
                     AND rt.transaction_type IN ('RETURN TO RECEIVING', 'RECEIVE')) grn
           WHERE 1 = 1
             AND asu.vendor_id = ai.vendor_id
             AND ai.invoice_id = aid.invoice_id
             AND aid.dist_code_combination_id = gc.code_combination_id
             AND aid.rcv_transaction_id = rt.transaction_id(+)
             AND rt.shipment_line_id = rsl.shipment_line_id(+)
             AND rsl.shipment_header_id = rsh.shipment_header_id(+)
             AND gc.segment6 = '361901'
             AND aid.accounting_date BETWEEN  v_from_date and v_as_of_date
             AND gc.segment3 = v_sbu_sgement
             --AND  rsh.receipt_num = 9030004931
             AND rt.transaction_id = grn.transaction_id
        GROUP BY ai.doc_sequence_value,
                 rsh.receipt_num,
                 ai.invoice_currency_code,
                 gc.segment3)
  LOOP 
     
     v_prv_grnamt :=  NVL(v_prv_grnamt,0) + nvl(prv_grn.inv_amt,0);      
  
          fnd_file.put_line (fnd_file.output,
                             SUBSTR (LPAD (PRV_GRN.RECEIPT_NUM, 15, ' '), 1, 15)
                            || ' '
                            || SUBSTR (LPAD (PRV_GRN.ACC_DATE    , 15, ' '), 1, 15)
                            || ' '
                            || SUBSTR (LPAD (PRV_GRN.DOC_SEQUENCE_VALUE, 20, ' '), 1, 20)
                            || ' '
                            || SUBSTR (LPAD (PRV_GRN.INV_AMT, 15, ' '), 1, 15)
                            || ' '
                            || SUBSTR (RPAD (PRV_GRN.SEGMENT3, 10, ' '), 1, 10)                            
                           );

  END LOOP;                 
  
  fnd_file.put_line (fnd_file.output, ' ');
       fnd_file.put_line (fnd_file.output, RPAD ('-', ROUND (75 / 1), '-'));
   fnd_file.put_line (fnd_file.output,
                                 ' '
                                 || LPAD ('Total  ', 50, ' ')  
                                 || LPAD (v_prv_grnamt, 17, ' ') 
                             );
   
     fnd_file.put_line (fnd_file.output, ' ');
     fnd_file.put_line (fnd_file.output, RPAD ('-', ROUND (75 / 1), '-'));

   --------------------------------------------------------------------------
                        -- Manual JV /Spread Sheet
   --------------------------------------------------------------------------
   fnd_file.put_line (fnd_file.output, '  ');
   fnd_file.put_line (fnd_file.output, '  ');   
   fnd_file.put_line (fnd_file.output, '  ');
   fnd_file.put_line (fnd_file.output, '  ');
   fnd_file.put_line (fnd_file.output,
                      CHR (9) || '   Manual Invoices JV   (  General Ledger  ) '
                     );

   fnd_file.put_line (fnd_file.output, '  ');
   fnd_file.put_line (fnd_file.output, '  ');   
   fnd_file.put_line (fnd_file.output, '  ');   
   
               
             fnd_file.put_line (fnd_file.output,
                               SUBSTR (RPAD ('Source', 20, ' '),
                                       1,
                                       20
                                      )
                            || ' '
                            || SUBSTR (RPAD ('Category', 20, ' '),
                                       1,
                                       20
                                      )
                            || ' '
                            || SUBSTR (RPAD ('Batch Name', 40, ' '), 1, 40)
                            || ' '
                            || SUBSTR (RPAD ('Journal Desc', 40, ' '), 1, 40)
                            || ' '
                            || SUBSTR (lPAD ('GL Date', 15, ' '), 1, 15)
                            || ' '
                            || SUBSTR (LPAD ('Debit Amt', 15, ' '), 1, 15)
                            || ' '
                            || SUBSTR (LPAD ('Credit Amt', 15, ' '), 1, 15)
                            || ' '
                            || SUBSTR (LPAD ('Voucher No', 20, ' '), 1, 20)
                            || ' '
                            || LPAD ('Segments              ', 40, ' ')
                           );

   
   fnd_file.put_line (fnd_file.output, RPAD ('-', ROUND (235 / 1), '-'));
   fnd_file.put_line (fnd_file.output, '  ');
   
   FOR MANU_JV IN ( SELECT glh.je_source SOURCE,
                       glh.je_category CATEGORY,
                       GLB.NAME batch_name,
                       glh.default_effective_date gl_date, 
                       gll.description journal_description,
                       glh.doc_sequence_value Voucher_No,
                       nvl(trunc(gll.accounted_dr,2),0) dr_amount,
                       nvl(trunc(gll.accounted_cr,2),0) cr_amount, 
                       gl.concatenated_segments Segments        
                  FROM (select je_source ,
                               je_category,
                               default_effective_date,
                               doc_sequence_value,
                               je_header_id,
                               je_batch_id 
                          from apps.gl_je_headers ) glh,
                       (select je_header_id,
                               accounted_dr,
                               accounted_cr, 
                               description,
                               code_combination_id,
                               created_by
                          from apps.gl_je_lines ) gll,
                       (select concatenated_segments,
                               code_combination_id,
                               segment3,
                               segment4,
                               segment6
                          from apps.gl_code_combinations_kfv ) gl,
                       (select user_name , 
                               user_id
                          from apps.fnd_user ) fu,
                       (select NAME,
                               je_batch_id,
                               status
                          from apps.gl_je_batches ) GLB
                 WHERE glh.je_header_id = gll.je_header_id
                   AND gl.code_combination_id = gll.code_combination_id
                   AND glh.je_batch_id = GLB.je_batch_id
                   AND gll.created_by = fu.user_id 
                   AND TO_DATE (TO_CHAR (glh.default_effective_date, 'DD-MON-YY'))
                          BETWEEN v_from_date
                              AND v_as_of_date
                   AND gl.segment6 = '361901'
                   AND glh.je_source IN
                          ('Spreadsheet','Assets','Manual', 'Other', 'AutoCopy', 'Statistical', '1',
                           '21')
                   AND gl.segment3 = v_sbu_sgement
                   AND GLB.status = 'P'
                   --AND glh.doc_sequence_value IN (611001908,612001201) -- 802
                   order by glh.doc_sequence_value
                   )
                   
      LOOP
      
      
        v_jv_debit_amt :=  NVL(v_jv_debit_amt,0) + manu_jv.dr_amount;
        v_jv_credit_amt := NVL(v_jv_credit_amt,0) + manu_jv.cr_amount;
         
                       
          fnd_file.put_line (fnd_file.output,
                               SUBSTR (RPAD (manu_jv.SOURCE, 20, ' '),
                                       1,
                                       20
                                      )
                            || ' '
                            || SUBSTR (RPAD (manu_jv.CATEGORY, 20, ' '),
                                       1,
                                       20
                                      )
                            || ' '
                            || SUBSTR (RPAD (manu_jv.batch_name, 40, ' '), 1, 40)
                            || ' '
                            || SUBSTR (RPAD (manu_jv.journal_description, 40, ' '), 1, 40)
                            || ' '
                            || SUBSTR (LPAD (manu_jv.gl_date, 15, ' '), 1, 15)
                            || ' '
                            || SUBSTR (LPAD (manu_jv.dr_amount, 15, ' '), 1, 15)
                            || ' '
                            || SUBSTR (LPAD (manu_jv.cr_amount, 15, ' '), 1, 15)
                            || ' '
                            || SUBSTR (LPAD (manu_jv.Voucher_No, 20, ' '), 1, 20)
                            || ' '
                            || LPAD (manu_jv.Segments, 40, ' ')
                           );

       END LOOP;

        fnd_file.put_line (fnd_file.output, ' ');
        fnd_file.put_line (fnd_file.output, RPAD ('-', ROUND (235 / 1), '-'));
        fnd_file.put_line (fnd_file.output, ' ');
           
        fnd_file.put_line (fnd_file.output,
                                 ' '
                                 || LPAD ('Total  ', 139, ' ')  --148 
                                 || LPAD (v_jv_debit_amt, 15, ' ') 
                                 || LPAD (v_jv_credit_amt, 16, ' ')
                                 ||LPAD(v_jv_debit_amt - v_jv_credit_amt,22,' ')
                                 || LPAD ('(Dr-Cr)', 10, ' ')
                             );
        fnd_file.put_line (fnd_file.output, ' ');               
        fnd_file.put_line (fnd_file.output, '  ');
         
                         
  ELSE
  
   fnd_file.put_line (fnd_file.output, ' This SBU is not available ');
   
   END IF ;
   
 
EXCEPTION
   WHEN OTHERS
   THEN
      fnd_file.put_line (fnd_file.output,
                         'Exception found in Query ' || SQLERRM
                        );
END; 
/


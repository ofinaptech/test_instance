CREATE OR REPLACE PROCEDURE APPS.gst_abrl_tdsinv_without_cmdm (
   errbuff    OUT      VARCHAR2,
   retcode    OUT      NUMBER,
   p_org_id   IN       NUMBER
)
AS
/*  Version         Date            Name                Remarks
    1.0.0        26-OCT-2010    Sayikrishna         Created for getting those Invoices which are not generated TDS CM
*/
   CURSOR tdsinv_cur
   IS
      (SELECT hou.NAME ou, (SELECT pv.vendor_name
                              FROM apps.po_vendors pv
                             WHERE pv.vendor_id = api.vendor_id) vendor_name,
              api.invoice_id, api.invoice_num, api.invoice_date inv_date,
              api.invoice_amount
         FROM apps.ap_invoices_all api, apps.hr_operating_units hou
        WHERE 1 = 1
          AND api.org_id = NVL(p_org_id, api.org_id)
          AND api.org_id = hou.organization_id
          AND api.cancelled_date IS NULL
          AND NOT EXISTS (
                 SELECT 1
                   FROM apps.jai_ap_tds_invoices aptds
                  WHERE 1 = 1             
                    AND aptds.invoice_id = api.invoice_id
                    and aptds.DM_INVOICE_NUM not like '%CM%'
                    AND aptds.organization_id = hou.organization_id
                    AND aptds.organization_id = api.org_id)
          AND EXISTS (
                 SELECT 1
                   FROM apps.ap_invoice_distributions_all aida,
                        jai_tax_lines jtl      
                  WHERE aida.invoice_id = api.invoice_id
                    AND aida.invoice_id=jtl.trx_id
                       AND aida.global_attribute_category ='JA.IN.APXINWKB.DISTRIBUTIONS'
                    AND aida.global_attribute1 IS NOT NULL
                    AND aida.org_id = api.org_id
                    AND aida.org_id = hou.organization_id));
BEGIN
   fnd_file.put_line (fnd_file.output,
                      'ABRL TDS Invoices not generated CM/DM'
                     );
   fnd_file.put_line (fnd_file.output, ' ');
/*FND_FILE.PUT_LINE (FND_FILE.OUTPUT,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Operating Unit'
                         || CHR (9)
                         ||  P_OPERATING_UNIT
                         || CHR (9)
                        );*
    /* DISPLAY LABELS OF THE REPORT */
   fnd_file.put_line (fnd_file.output, ' ');
   fnd_file.put_line (fnd_file.output,
                         'Operating Unit'
                      || CHR (9)
                      || 'Vendor Name'
                      || CHR (9)
                      || 'Invoice Number'
                      || CHR (9)
                      || 'Invoice Date'
                      || CHR (9)
                      || 'Invoice Amount'
                      || CHR (9)
                     );

   FOR v_rec IN tdsinv_cur
   LOOP
      EXIT WHEN tdsinv_cur%NOTFOUND;
      /* PRINTING THE DATA  */
      fnd_file.put_line (fnd_file.output,
                            v_rec.ou
                         || CHR (9)
                         || v_rec.vendor_name
                         || CHR (9)
                         || v_rec.invoice_num
                         || CHR (9)
                         || v_rec.inv_date
                         || CHR (9)
                         || v_rec.invoice_amount
                        );
   END LOOP;
END gst_abrl_tdsinv_without_cmdm; 
/


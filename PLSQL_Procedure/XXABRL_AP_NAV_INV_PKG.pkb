CREATE OR REPLACE PACKAGE body APPS.xxabrl_ap_nav_inv_pkg
AS
/*****************************************************************************************************************************************
                 WIPRO Infotech Ltd, Mumbai, India

 Object Name : xxabrl_ap_nav_inv_pkg

 Description : procedure to give the invoice details

  Change Record:
 =============================================================================================================
 Version    Date            Author                              Remarks
 =======   ==========     =============                         =========================================================================
 1.0       01-jun-09   Shailesh (Wipro Infotech Ltd)            Initial Version
 1.0.1       04-aug-09   Naresh Kumar (Synergy Web-Tech Ltd.)     commeted the operating unit to get tsrl kl sm data
 1.0.2       08-OCT-09   Naresh Kumar (Synergy Web-Tech Ltd.)     replaced the batch name to source
 1.0.3      10-JUL-2010  Sayikrishna                               Invoice Creation Date Added
*****************************************************************************************************************************************/

PROCEDURE xxabrl_ap_nav_inv_proc(ERRBUFF             OUT VARCHAR2
                                                                     ,RETCODE            OUT NUMBER
                                      ,p_org_id  number
                                      ,Source varchar2
                                      ,p_from_gl_date in varchar2
                                      ,p_to_gl_date in varchar2 ) as


                                     
                                     
                                     


     query_string             LONG;
     v_batch_name varchar2(240);
     order_by_string varchar2(240);
     -- LIST_VEN_STRING          LONG;
     v_ou_name varchar2(75);
      v_gl_date  varchar2(150);
      v_org_id   varchar2(150);

 V_RESPID             NUMBER := Fnd_Profile.VALUE('RESP_ID');


 TYPE ref_cur IS REF CURSOR;

      c                        ref_cur;

      TYPE c_rec IS RECORD (
         v_operating_unit           hr_operating_units.NAME%TYPE,
         v_batch_name               AP_BATCHES_ALL.Batch_Name%TYPE,
         v_vendor_number            po_vendors.segment1%TYPE,
         v_vendor_name              po_vendors.vendor_name%TYPE,
         v_vendor_site              PO_VENDOR_SITES_ALL.vendor_site_code%TYPE,
         v_vocher_num               ap_invoices_all.DOC_SEQUENCE_VALUE%TYPE,
         v_invoice_num              ap_invoices_all.invoice_num%TYPE,
       --  v_invoice_type             VARCHAR2(250),
         v_invoice_currency         ap_invoices_all.invoice_currency_code%type, 
         v_invoice_amount           ap_invoices_all.invoice_amount%TYPE,
         v_amount_paid              ap_invoices_all.amount_paid%TYPE,
         v_invoice_date             ap_invoices_all.invoice_date%TYPE,
         v_inv_creation_date        ap_invoices_all.creation_date%TYPE,
         v_gl_date                  ap_invoices_all.gl_date%TYPE,
         v_Invoice_Type             ap_invoices_all.invoice_type_lookup_code%TYPE,
         v_invoice_description      ap_invoices_all.description%TYPE,
         v_terms_date               ap_invoices_all.terms_date%TYPE,
         v_payment_method_code      ap_invoices_all.payment_method_code%TYPE,
         v_vendor_type              ap_invoices_all.pay_group_lookup_code%TYPE,
         v_Payment_Status           ap_invoices_all.payment_status_flag%TYPE, 
         v_terms_name               ap_terms.NAME%TYPE, 
         v_approval_status          varchar2(30),
         v_GRN_NO                   ap_invoice_lines_all.REFERENCE_1%TYPE
         );

v_rec                    c_rec;



   BEGIN

   query_string :='SELECT ood.organization_name operating_unit, 
       ab.batch_name batch_name,
       pv.segment1 vendor_number, 
       pv.vendor_name vendor_name,
       povs.vendor_site_code vendor_site,
       api.doc_sequence_value voucher_number, 
       api.invoice_num invoice_number,
       api.invoice_currency_code invoice_currency,
       api.invoice_amount invoice_amount, 
       NVL(api.amount_paid,0)   amount_paid,
       api.invoice_date invoice_date,
       api.creation_date inv_creation_date,    
       api.gl_date      gl_date,  
       api.invoice_type_lookup_code Invoice_Type,
       api.description invoice_description, 
       api.terms_date terms_date,
       api.payment_method_code payment_method_code,
       api.pay_group_lookup_code vendor_type,
       api.payment_status_flag Payment_Status,
       AT.NAME terms_name,
       ap_invoices_pkg.get_approval_status
                     (api.invoice_id,
                      api.invoice_amount,
                      api.payment_status_flag,
                      api.invoice_type_lookup_code
                     ) approval_status,
                     ail.REFERENCE_1 GRN_NO
  FROM apps.ap_batches_all ab,
       apps.po_vendors pv,
       apps.po_vendor_sites_all povs,
       apps.ap_invoices_all api,
       apps.ap_terms AT,
       apps.org_organization_definitions ood   ,
       apps.ap_invoice_lines_all ail   
 WHERE pv.vendor_id = povs.vendor_id
   AND ab.batch_id(+) = api.batch_id
   AND ail.invoice_id=api.invoice_id
   AND api.vendor_id = pv.vendor_id
   AND api.terms_id = AT.term_id
   AND ood.organization_id = povs.org_id 
AND api.org_id=ood.organization_id'
;

-- following query commented by naresh to get the tsrl kl sm data.
   --AND ood.organization_name LIKE ''ABRL%''  

order_by_string:= ' order by ood.organization_name,pv.segment1 ,api.gl_date ';

if source is not null then 
--v_batch_name := ' AND ab.BATCH_NAME LIKE '''||source||'%'''; --commented  by naresh on 08-oct-09 
v_batch_name := ' AND api.source like '''||source||''''; -----added  by naresh on 08-oct-09 to get the data for specific source.
else
v_batch_name:='';
end if;

if p_org_id is not null then
v_org_id := ' and  ood.organization_id = '||p_org_id ;
end if;
 

 if p_from_gl_date is not null and p_to_gl_date is not null then
 v_gl_date := ' AND api.gl_date between '''||  TO_DATE(p_from_gl_date,'YYYY/MM/DD HH24:MI:SS')||''' and '''||TO_DATE(p_to_gl_date,'YYYY/MM/DD HH24:MI:SS')||'''';
 end if;


 begin
 select name into v_ou_name from hr_operating_units
 where organization_id=p_org_id;
exception 
when others then 
v_ou_name:='';
end;

      Fnd_File.put_line (Fnd_File.output,
                         'ABRL - AP NAVISION Dump Report'
                        );
      Fnd_File.put_line (Fnd_File.output, ' ');
      Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'As on Date'
                         || CHR (9)
                         || SYSDATE
                         || CHR (9)
                        );
      Fnd_File.put_line (Fnd_File.output, ' ');
      Fnd_File.put_line (Fnd_File.output, 'REPORT PARAMETRS ');
      Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Source'
                         || CHR (9)
                         || source
                         || CHR (9)
                        );
      Fnd_File.put_line (Fnd_File.output, ' ');
      Fnd_File.put_line (Fnd_File.output, 'REPORT PARAMETRS ');
      Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'From GL Date'
                         || CHR (9)
                         || p_from_gl_date
                         || CHR (9)
                        );
      Fnd_File.put_line (Fnd_File.output, ' ');
      Fnd_File.put_line (Fnd_File.output, 'REPORT PARAMETRS ');
      Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'To GL Date'
                         || CHR (9)
                         || p_to_gl_date
                         || CHR (9)
                        );    
 Fnd_File.put_line (Fnd_File.output, ' ');
      Fnd_File.put_line (Fnd_File.output, 'REPORT PARAMETRS ');
      Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Operating Unit'
                         || CHR (9)
                         || v_ou_name
                         || CHR (9)
                        );                                                                      

        

  Fnd_File.put_line (Fnd_File.output, ' ');
      Fnd_File.put_line (Fnd_File.output, ' ');
      Fnd_File.put_line (Fnd_File.output,
                          'Operating Unit'
                         || CHR (9)
                         || 'Batch Name'
                         || CHR (9)
                         || 'Vendor Number'
                         || CHR (9)
                         || 'Vendor Name'
                         || CHR (9)
                         || 'Vendor Site'
                         || CHR (9)
                         || 'Voucher Number'
                         || CHR (9)
                         || 'Invoice Number'   
                         || CHR (9)
                         || 'Invoice Type'
                         || CHR (9)
                         || 'invoice Currency'
                         || CHR (9)
                         || 'Invoice Amount'
                         || CHR (9)
                         ||'Amount Paid'
                         || CHR (9)
                         || 'Invoice Date '
                         || CHR (9)
                         || 'Invoice Creation Date '
                         || CHR (9)
                         || 'GL Date'
                         || CHR (9)
                         || 'Invoice Type'
                         || CHR (9)
                         || 'Invoice Description'
                         || CHR (9)
                         || 'Terms Date'
                         || CHR (9)
                         || 'Payment Method Code'
                         || CHR (9)
                         || 'Vendor Type'
                         || CHR (9)
                         || 'Payment Status'
                         || CHR (9)
                         || 'Terms Name'
                         || CHR (9) 
                         || 'Approval Status'
                         || CHR (9)
                         || 'GRN No'
                         || CHR (9)
                        );

                   query_string:=query_string||v_batch_name||v_gl_date|| v_org_id||order_by_string;

                   Fnd_File.put_line (Fnd_File.LOG,
                         'string: '||query_string
                        );

         OPEN c
       FOR query_string;
      LOOP
         FETCH c
          INTO v_rec;
         EXIT WHEN c%NOTFOUND;

         
         
         
         
         
         Fnd_File.put_line (Fnd_File.output,
                             v_rec.v_operating_unit
                            || CHR (9)
                            || v_rec.v_batch_name  
                            || CHR (9)
                            || v_rec.v_Vendor_Number
                            || CHR (9)
                            || v_rec.v_vendor_name 
                            || CHR (9)
                            || v_rec.v_vendor_site  
                            || CHR (9)
                            || v_rec.v_vocher_num
                            || CHR (9)
                            || v_rec.v_invoice_num
                            || CHR (9)
                            || v_rec.v_invoice_type   
                            || CHR (9)
                            || v_rec.v_invoice_currency
                            || CHR (9)
                            || v_rec.v_invoice_amount
                            || CHR (9)
                            || v_rec.v_amount_paid  
                            || CHR (9)
                            ||v_rec.v_invoice_date
                            || CHR (9)
                            ||v_rec.v_inv_creation_date  --Added on 03-JUL-2010
                            || CHR (9)
                            ||v_rec.v_gl_date  
                            || CHR (9)
                            || v_rec.v_Invoice_Type
                            || CHR (9)
                            || v_rec.v_invoice_description 
                            || CHR (9)
                            || v_rec.v_terms_date
                            || CHR (9)
                            || v_rec.v_payment_method_code
                            || CHR (9)
                            ||v_rec.v_vendor_type 
                             || CHR (9)
                            ||v_rec.v_Payment_Status
                             || CHR (9)
                            ||v_rec.v_terms_name
                             || CHR (9)
                            ||v_rec.v_approval_status 
                             || CHR (9)
                            ||v_rec.v_GRN_NO  
                            );

                            
         
                   --inv_count :=inv_count+1;

END LOOP;

CLOSE c;




end xxabrl_ap_nav_inv_proc;                                     



END   xxabrl_ap_nav_inv_pkg; 
/


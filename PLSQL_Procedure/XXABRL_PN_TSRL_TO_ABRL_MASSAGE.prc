CREATE OR REPLACE PROCEDURE APPS.xxabrl_pn_tsrl_to_abrl_massage (
   errbuf    OUT   VARCHAR2,
   retcode   OUT   VARCHAR2
)
AS
   v_user_id                  NUMBER         := fnd_profile.VALUE ('USER_ID');
   v_org_id                   NUMBER          := fnd_profile.VALUE ('ORG_ID');
   v_retcode                  VARCHAR2 (100);
   v_header_count             NUMBER                                     := 0;
   v_line_count               NUMBER                                     := 0;
   new_vendor_site_id         NUMBER                                     := 0;
   header_errors              NUMBER                                     := 0;
   line_errors                NUMBER                                     := 0;
   new_org_id                 NUMBER;
   bala_old_segments          apps.gl_code_combinations_kfv.concatenated_segments%TYPE;
   bala_new_segments          apps.gl_code_combinations_kfv.concatenated_segments%TYPE;
   bala_code_combination_id   apps.gl_code_combinations_kfv.code_combination_id%TYPE;
   bala_segment3              VARCHAR2 (3);
   new_tax_id                 VARCHAR2 (50);

   CURSOR header_massage (in_org_id NUMBER)
   IS
      SELECT *
        FROM apps.ap_invoices_interface header
       WHERE 1 = 1
         AND header.org_id = in_org_id
         AND header.SOURCE = 'Oracle Property Manager'
         AND TRUNC (header.creation_date) > '31-DEC-14'
         AND NVL (header.attribute15, 'N') = 'N';

   CURSOR line_massage (in_invoice_id NUMBER, in_org_id NUMBER)
   IS
      SELECT *
        FROM apps.ap_invoice_lines_interface line
       WHERE 1 = 1
         AND line.invoice_id = in_invoice_id
         AND line.org_id = in_org_id
         AND NVL (line.attribute15, 'N') = 'N';
--    1
BEGIN
--           V_ORG_ID := 961;                ONLY FOR TESTING

   --    2
   IF    v_org_id = 901
      OR v_org_id = 861
      OR v_org_id = 841
      OR v_org_id = 961
      OR v_org_id = 801
      OR v_org_id = 941
      OR v_org_id = 821
   THEN
      FOR i IN header_massage (v_org_id)
      LOOP
         BEGIN
            new_org_id := 0;
            header_errors := 0;
            v_header_count := 0;

            SELECT DECODE (v_org_id,
                           '901', '1524',
                           '861', '1521',
                           '841', '1523',
                           '961', '1525',
                           '801', '1481',
                           '941', '1522',
                           '821', '1721'
                          )
              INTO new_org_id
              FROM DUAL;

            FOR j IN line_massage (i.invoice_id, i.org_id)
            LOOP
               BEGIN
                  line_errors := 0;
                  v_line_count := 0;
                  bala_old_segments := NULL;
                  bala_segment3 := NULL;
                  bala_new_segments := NULL;
                  bala_code_combination_id := 0;

                  BEGIN
                     SELECT concatenated_segments
                       INTO bala_old_segments
                       FROM apps.gl_code_combinations_kfv
                      WHERE 1 = 1
                        AND code_combination_id = j.dist_code_combination_id
                        AND enabled_flag = 'Y';
                  EXCEPTION
                     WHEN NO_DATA_FOUND
                     THEN
                        fnd_file.put_line
                               (fnd_file.LOG,
                                   ' CCID not defined in Oracle For Old Org '
                                || j.org_id
                                || '  -  '
                                || j.dist_code_combination_id
                               );
                        DBMS_OUTPUT.put_line
                                (   ' CCID NOT DEFINED IN ORACLE FOR OLD ORG '
                                 || j.org_id
                                 || '  -  '
                                 || j.dist_code_combination_id
                                );
                        line_errors := 1;
                     WHEN TOO_MANY_ROWS
                     THEN
                        fnd_file.put_line
                           (fnd_file.LOG,
                               ' Multiple CCID defined in Oracle For Old Org '
                            || j.org_id
                            || '  -  '
                            || j.dist_code_combination_id
                           );
                        DBMS_OUTPUT.put_line
                           (   ' MULTIPLE CCID DEFINED IN ORACLE FOR OLD ORG '
                            || j.org_id
                            || '  -  '
                            || j.dist_code_combination_id
                           );
                        line_errors := 1;
                     WHEN OTHERS
                     THEN
                        fnd_file.put_line
                               (fnd_file.LOG,
                                   ' Exception Others in Oracle For Old Org '
                                || j.org_id
                                || '  -  '
                                || j.dist_code_combination_id
                               );
                        DBMS_OUTPUT.put_line
                                (   ' EXCEPTION OTHERS IN ORACLE FOR OLD ORG '
                                 || j.org_id
                                 || '  -  '
                                 || j.dist_code_combination_id
                                );
                        line_errors := 1;
                  END;

                  SELECT DECODE (SUBSTR (bala_old_segments, 8, 3),
                                 '812', '635',
                                 '772', '620',
                                 '773', '630',
                                 '792', '640',
                                 '752', '610',
                                 '754', '615',
                                 '775', '625',
                                 '751', '645'
                                )
                    INTO bala_segment3
                    FROM DUAL;

                  bala_new_segments :=
                        '11.'
                     || SUBSTR (bala_old_segments, 4, 4)
                     || bala_segment3
                     || SUBSTR (bala_old_segments, 11, 27);

                  IF SUBSTR (bala_new_segments, 8, 3) = '610'
                  THEN
                     new_org_id := 1481;
                  END IF;

                  IF SUBSTR (bala_new_segments, 8, 3) = '615'
                  THEN
                     new_org_id := 1501;
                  END IF;

                  BEGIN
-- 31.000.751.0000000.00.362002.000.0000
                     SELECT code_combination_id
                       INTO bala_code_combination_id
                       FROM apps.gl_code_combinations_kfv
                      WHERE 1 = 1
                        AND segment1 = SUBSTR (bala_new_segments, 1, 2)
                        AND segment2 = SUBSTR (bala_new_segments, 4, 3)
                        AND segment3 = SUBSTR (bala_new_segments, 8, 3)
                        AND segment4 = SUBSTR (bala_new_segments, 12, 7)
                        AND segment5 = SUBSTR (bala_new_segments, 20, 2)
                        AND segment6 = SUBSTR (bala_new_segments, 23, 6)
                        AND segment7 = SUBSTR (bala_new_segments, 30, 3)
                        AND segment8 = SUBSTR (bala_new_segments, 34, 4)
                        AND enabled_flag = 'Y';
                  EXCEPTION
                     WHEN NO_DATA_FOUND
                     THEN
                        fnd_file.put_line
                               (fnd_file.LOG,
                                   ' CCID not defined in Oracle For NEW Org '
                                || bala_new_segments
                               );
                        DBMS_OUTPUT.put_line
                                (   ' CCID NOT DEFINED IN ORACLE FOR NEW ORG '
                                 || new_org_id
                                 || '  -  '
                                 || bala_new_segments
                                );
                        line_errors := 1;
                     WHEN TOO_MANY_ROWS
                     THEN
                        fnd_file.put_line
                           (fnd_file.LOG,
                               ' Multiple CCID defined in Oracle For NEW Org '
                            || bala_new_segments
                           );
                        DBMS_OUTPUT.put_line
                           (   ' MULTIPLE CCID DEFINED IN ORACLE FOR NEW ORG '
                            || new_org_id
                            || '  -  '
                            || bala_new_segments
                           );
                        line_errors := 1;
                     WHEN OTHERS
                     THEN
                        fnd_file.put_line
                               (fnd_file.LOG,
                                   ' Exception Others in Oracle For NEW Org '
                                || bala_new_segments
                               );
                        DBMS_OUTPUT.put_line
                                (   ' EXCEPTION OTHERS IN ORACLE FOR NEW ORG '
                                 || new_org_id
                                 || '  -  '
                                 || bala_new_segments
                                );
                        line_errors := 1;
                  END;

                  fnd_file.put_line (fnd_file.LOG, '==========');
                  fnd_file.put_line (fnd_file.LOG,
                                        ' NEW_ORG = '
                                     || new_org_id
                                     || ' NEW_CCID =  '
                                     || bala_code_combination_id
                                    );
                  fnd_file.put_line (fnd_file.LOG, '==========');
                  DBMS_OUTPUT.put_line (   ' NEW_ORG = '
                                        || new_org_id
                                        || ' NEW_CCID =  '
                                        || bala_code_combination_id
                                       );
                  new_tax_id := NULL;

                  IF     NVL (j.attribute10, 'N') <> 'N'
                     AND NVL (j.attribute10, '0') <> '0'
                  THEN
                     IF SUBSTR (bala_old_segments, 8, 3) = '754'
                     THEN
                        new_tax_id := 74675;
                     ELSE
                        BEGIN
                           SELECT TO_CHAR (tax_id)
                             INTO new_tax_id
                             FROM apps.jai_cmn_taxes_all jtc
                            WHERE 1 = 1
                              AND jtc.tax_type = 'TDS'
                              AND jtc.section_type = 'TDS_SECTION'
                              AND org_id = new_org_id
                              AND tax_name IN (
                                     SELECT tax_name
                                       FROM apps.jai_cmn_taxes_all
                                      WHERE 1 = 1
                                        AND org_id = j.org_id
                                        AND tax_id = j.attribute10);
                        EXCEPTION
                           WHEN NO_DATA_FOUND
                           THEN
                              fnd_file.put_line
                                 (fnd_file.LOG,
                                     ' Tax Name not found for New_Org based on Old_Org and Tax_ID '
                                  || j.org_id
                                  || '  -  '
                                  || j.attribute10
                                  || '  -  '
                                  || new_org_id
                                 );
                              DBMS_OUTPUT.put_line
                                 (   ' TAX NAME NOT DEFINED IN ORACLE FOR NEW ORG '
                                  || j.org_id
                                  || '  -  '
                                  || j.attribute10
                                  || '  -  '
                                  || new_org_id
                                 );
                              line_errors := 1;
                           WHEN TOO_MANY_ROWS
                           THEN
                              fnd_file.put_line
                                 (fnd_file.LOG,
                                     ' Multiple Tax Names defined for New_Org based on Old_Org and Tax_ID '
                                  || j.org_id
                                  || '  -  '
                                  || j.attribute10
                                  || '  -  '
                                  || new_org_id
                                 );
                              DBMS_OUTPUT.put_line
                                 (   ' MULTIPLE TAX NAME DEFINED IN ORACLE FOR NEW ORG '
                                  || j.org_id
                                  || '  -  '
                                  || j.attribute10
                                  || '  -  '
                                  || new_org_id
                                 );
                              line_errors := 1;
                           WHEN OTHERS
                           THEN
                              fnd_file.put_line
                                 (fnd_file.LOG,
                                     ' Exception Others  for New_Org based on Old_Org and Tax_ID '
                                  || j.org_id
                                  || '  -  '
                                  || j.attribute10
                                  || '  -  '
                                  || new_org_id
                                 );
                              DBMS_OUTPUT.put_line
                                 (   ' EXCEPTION OTHERS IN ORACLE FOR NEW ORG '
                                  || j.org_id
                                  || '  -  '
                                  || j.attribute10
                                  || '  -  '
                                  || new_org_id
                                 );
                              line_errors := 1;
                        END;
                     END IF;
                  END IF;

                  DBMS_OUTPUT.put_line (   'LINES :: Old_Segments= '
                                        || bala_old_segments
                                        || ' - New Segments= '
                                        || bala_new_segments
                                        || ' - Old_CCID= '
                                        || j.dist_code_combination_id
                                        || ' - New_CCID= '
                                        || bala_code_combination_id
                                        || ' - Old_Org_ID= '
                                        || i.org_id
                                        || ' - New_Org_ID= '
                                        || new_org_id
                                       );

                  IF NVL (line_errors, 0) = 0
                  THEN
                     BEGIN
                        UPDATE apps.ap_invoice_lines_interface
                           SET dist_code_combination_id =
                                                      bala_code_combination_id,
                               org_id = new_org_id,
                               attribute10 = new_tax_id,
                               attribute15 = 'Y'
                         WHERE 1 = 1
                           AND invoice_id = j.invoice_id
                           AND org_id = j.org_id
                           AND dist_code_combination_id =
                                                    j.dist_code_combination_id;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           fnd_file.put_line
                              (fnd_file.LOG,
                                  ' ERROR UPDATING APPS.AP_INVOICE_LINES_INTERFACE FOR NEW_ORG '
                               || j.invoice_id
                               || '  -  '
                               || j.org_id
                              );
                           DBMS_OUTPUT.put_line
                              (   ' ERROR UPDATING APPS.AP_INVOICE_LINES_INTERFACE FOR NEW_ORG '
                               || j.invoice_id
                               || '  -  '
                               || j.org_id
                              );
                           line_errors := 1;
                     END;
                  END IF;

                  IF NVL (line_errors, 0) = 0
                  THEN
                     v_line_count := NVL (v_line_count, 0) + 1;
                  END IF;
               END;
            END LOOP;

            new_vendor_site_id := 0;

            ---  CONDITION ADDED  TO GETTING NEW VENDOR_SITE_ID
            BEGIN
               SELECT vendor_site_id
                 INTO new_vendor_site_id
                 FROM apps.ap_supplier_sites_all
                WHERE 1 = 1
                  AND vendor_id = i.vendor_id
                  AND org_id = new_org_id
                  AND UPPER (vendor_site_code) =
                                  (SELECT UPPER (a.vendor_site_code)
                                     FROM apps.ap_supplier_sites_all a
                                    WHERE a.vendor_site_id = i.vendor_site_id);
            EXCEPTION
               WHEN OTHERS
               THEN
                  fnd_file.put_line
                     (fnd_file.LOG,
                         ' NEW VENDOR_SITE_ID NOT FOUND DURING HEADER MASSAGE = '
                      || i.vendor_id
                      || '  -  '
                      || new_org_id
                      || '  -  '
                      || i.vendor_site_id
                     );
                  DBMS_OUTPUT.put_line (   ' NEW VENDOR_SITE_ID NOTFOUND = '
                                        || new_org_id
                                        || ' - Invoice ID= '
                                        || i.invoice_id
                                        || ' - Vendor ID= '
                                        || i.vendor_id
                                        || ' - Old Vendor Site ID= '
                                        || i.vendor_site_id
                                       );
                  header_errors := 1;
            END;

---  CONDITION ADDED  TO GETTING NEW VENDOR_SITE_ID
            fnd_file.put_line (fnd_file.LOG, '==========');
            fnd_file.put_line (fnd_file.LOG,
                                  ' INVOICE_ID = '
                               || i.invoice_id
                               || ' ORG_ID =  '
                               || i.org_id
                              );
            fnd_file.put_line (fnd_file.LOG, '==========');
            DBMS_OUTPUT.put_line (   'HEADER = '
                                  || new_org_id
                                  || ' - Invoice ID= '
                                  || i.invoice_id
                                  || ' - '
                                  || i.org_id
                                 );

            IF NVL (header_errors, 0) = 0
            THEN
               BEGIN
                  UPDATE apps.ap_invoices_interface
                     SET vendor_site_id = new_vendor_site_id,
                         org_id = new_org_id,
                         legal_entity_id = 27274,
                         attribute15 = 'Y'
                   WHERE 1 = 1
                     AND invoice_id = i.invoice_id
                     AND org_id = i.org_id;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     fnd_file.put_line
                        (fnd_file.LOG,
                            ' ERROR UPDATING APPS.AP_INVOICES_INTERFACE FOR NEW_ORG '
                         || i.invoice_id
                         || '  -  '
                         || i.org_id
                        );
                     DBMS_OUTPUT.put_line
                        (   ' ERROR UPDATING APPS.AP_INVOICES_INTERFACE FOR NEW_ORG '
                         || i.invoice_id
                         || '  -  '
                         || i.org_id
                        );
                     header_errors := 1;
               END;
            END IF;

            IF NVL (header_errors, 0) = 0
            THEN
               v_header_count := NVL (v_header_count, 0) + 1;

               IF NVL (line_errors, 0) = 0
               THEN
                  COMMIT;
               ELSE
                  ROLLBACK;
               END IF;
            END IF;
         END;
      END LOOP;

      IF NVL (v_header_count, 0) > 0 AND NVL (v_line_count, 0) > 0
      THEN
         v_retcode :=
               ' TSRL OLD-ORG GOT MASSAGED TO NEW-ORG = '
            || v_header_count
            || '  -  '
            || v_line_count;
         fnd_file.put_line (fnd_file.LOG,
                               ' TSRL OLD-ORG GOT MASSAGED TO NEW-ORG = '
                            || v_org_id
                            || '  -  '
                            || v_header_count
                            || '  -  '
                            || v_line_count
                           );
         DBMS_OUTPUT.put_line (   ' TSRL OLD-ORG=  '
                               || v_org_id
                               || ' GOT MASSAGED TO NEW-ORG =  '
                               || new_org_id
                               || '   -  Header Count=  '
                               || v_header_count
                               || '  -  Line Count=  '
                               || v_line_count
                              );
      ELSIF NVL (v_header_count, 0) > 0 OR NVL (v_line_count, 0) > 0
      THEN
         DBMS_OUTPUT.put_line
                          (   ' TSRL OLD-ORG=  '
                           || v_org_id
                           || ' GOT MASSAGED TO NEW-ORG for few Invoices =  '
                           || new_org_id
                           || '   -  Header Count=  '
                           || v_header_count
                           || '  -  Line Count=  '
                           || v_line_count
                          );
      ELSIF NVL (v_header_count, 0) = 0 OR NVL (v_line_count, 0) = 0
      THEN
         DBMS_OUTPUT.put_line (   ' NO TSRL OPERATING UNITS TO MASSAGE=  '
                               || v_org_id
                              );
      END IF;
   ELSE
      v_retcode := ' NO TSRL OPERATING UNITS TO MASSAGE ';
      fnd_file.put_line (fnd_file.LOG,
                         ' NO TSRL OPERATING UNITS TO MASSAGE ' || v_org_id
                        );
      DBMS_OUTPUT.put_line (   ' NO TSRL OPERATING UNITS TO MASSAGE=  '
                            || v_org_id
                           );
   END IF;                                                             --    2

   fnd_file.put_line (fnd_file.LOG, '==========');
   fnd_file.put_line (fnd_file.LOG, ' END OF PROGRAM');
END;                                                                   --    1 
/


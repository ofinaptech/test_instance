CREATE OR REPLACE PACKAGE BODY APPS.XXABRL_SUPP_BANK_PMT_SCB AS
  PROCEDURE XXABRL_SCBANK_PAYMENT_SUPP(errbuf  OUT VARCHAR2,
                                        retcode OUT VARCHAR2 ,
                                        P_FROM_DATE  VARCHAR2 ,
                                        P_TO_DATE   VARCHAR2)
   IS
    --TO SELECT BANK (SCB BANK)
    CURSOR BANK_CUR IS
      SELECT bankorgprofile.party_id bank_id,
             bankparty.party_name    BANK_NAME
        FROM hz_parties               bankparty,
             hz_organization_profiles bankorgprofile,
             hz_code_assignments      bankca
       WHERE 1 = 1
         AND bankparty.party_id = bankorgprofile.party_id
         AND bankca.owner_table_name = 'HZ_PARTIES'
         AND bankca.owner_table_id = bankparty.party_id
         AND bankca.class_category = 'BANK_INSTITUTION_TYPE'
         AND bankca.class_code IN ('BANK', 'CLEARINGHOUSE')
         -- AND bankparty.party_id = 13526282
         AND UPPER(bankorgprofile.organization_name) IN ('STANDARD CHARTERED BANK');

    -- TO SELECT ALL PAYMENTS RELATED TO SCB
    CURSOR PAYMENT_CUR(p_bank_id number) IS
    select
        decode(aca.payment_type_flag, 'A',(select cb.bank_account_num
            from ce_bank_acct_uses_all cbau,ce_bank_accounts cb
            where cbau.bank_acct_use_id =aca.ce_bank_acct_use_id
            and   cbau.bank_account_id = cb.bank_account_id),aca.bank_account_num) bank_account_num,--Payment Batch
        --aca.bank_account_num,
        cba.bank_account_type,
        hou.name op_name,
        aca.checkrun_name run_identification,
        fu.user_name,
        apss.attribute10 pay_location,
        aca.checkrun_name batch_id,
        cbb.city corp_bank_city,
        cbb.bank_branch_name corp_bank_brnch,
        aps.segment1 sup_no,
        aca.amount pay_amount,
        aca.currency_code currency,
        decode(aca.payment_method_code,'N','N','RTGS','R','CHECK','C','DD','D','IFT','B') prod_code,
        nvl(aca.anticipated_value_date,aca.check_date) trx_val_date,
        aca.check_number ,
        aca.check_date pay_run_date,
        aca.check_date pay_inst_date,
        apss.attribute4 supp_acc_no,--
        apss.attribute2 supp_bank_name,--
        apss.attribute9 supp_bank_acc_type,--
        decode(aca.payment_method_code,'RTGS',apss.attribute8,apss.attribute7) supp_ifsc_code,--
        decode(aps.VENDOR_TYPE_LOOKUP_CODE,'NON MERCHANDISE','N','MERCHANDISE','M') vendor_type, -- added by amresh on 07 Aug 2012, requested by Bala Sir/SC Bank
        nvl(aps.vendor_name_alt,aps.vendor_name) sup_name,
        aps.vendor_name sup_alt_name,
         (apss.address_line1 || ';' || apss.address_line2 || ';' ||
             apss.address_line2 || ';' || apss.state || ';' || apss.zip) site_address,
        apss.vendor_site_code,
        apss.zip pin_code,
        apss.attribute11
                        ||decode(apss.attribute12,null,apss.attribute12,','||apss.attribute12)
                        ||decode(apss.attribute13,null,apss.attribute13,','||apss.attribute13) email_id,
        apss.phone ,
        NULL supp_bank_acc_city,
        apss.attribute3 supp_bank_bran,--
        aca.check_id,
        aca.org_id,
        aca.payment_id,
        aca.doc_sequence_value voucher_no,
        apss.vendor_site_id,
        cba.bank_id,
        cba.bank_branch_id,
        aca.ce_bank_acct_use_id,
        aca.party_id,
        aps.vendor_id,
        aca.created_by,
        aca.creation_date,
        aca.last_update_date,
        aca.last_updated_by
        from ap_checks_all         aca,
             ap_suppliers          aps,
             ap_supplier_sites_all apss,
             iby_payments_all      ip,
             ce_bank_accounts      cba,
             hr_operating_units    hou,
             fnd_user fu,
             cebv_bank_branches cbb
       where 1 = 1
         and aca.vendor_id = aps.vendor_id
         and aps.vendor_id = apss.vendor_id
         and aca.vendor_site_id = apss.vendor_site_id
         and aca.payment_id = ip.payment_id
         and ip.internal_bank_account_id = cba.bank_account_id
         and ip.payment_status in ('FORMATTED', 'ISSUED')
         and aca.org_id = hou.organization_id
         and aca.void_date is null
         and aca.created_by = fu.user_id
         and cba.bank_id =p_bank_id
         and cba.bank_branch_id = cbb.bank_branch_id
         and cba.bank_account_id in (SELECT LOOKUP_CODE
                                        from  fnd_lookup_values
                                        where lookup_type ='XXABRL_SCB_BANK_H2H_ACCOUNT'
                                        and   TO_NUMBER(LOOKUP_CODE) =cba.bank_account_id
                                        and nvl(aca.anticipated_value_date,aca.check_date)>=start_date_active
                                        )
         and UPPER(aca.bank_account_name) = UPPER(cba.bank_account_name)
         AND trunc(aca.creation_date) between to_date(p_from_date,'YYYY/MM/DD HH24:MI:SS')
                                          and to_date(p_to_date,'YYYY/MM/DD HH24:MI:SS')
         --Restrict the Duplicate Payments
         and not exists (select check_id
                                    from SCB_PAYMENT_TABLE DPT
                                    where DPT.check_id = aca.check_id) ;

--TO SELECT INVOICES AGAINST EACH PAYMENT (ONLY FOR SCB BANK)
    CURSOR INV_CUR(p_check_id NUMBER ) IS
     select ac.check_id, ai.invoice_num,ai.invoice_date,
            ai.invoice_amount,ai.invoice_type_lookup_code,aiP.AMOUNT AMOUNT_PAID,ai.DESCRIPTION
      from ap_checks_all           ac,
           ap_invoice_payments_all aip,
           ap_invoices_all         ai
     where 1 = 1
       and ac.check_id =p_check_id
       and ac.check_id = aip.check_id
       AND trunc(ac.creation_date) between to_date(p_from_date,'YYYY/MM/DD HH24:MI:SS') and to_date(p_to_date,'YYYY/MM/DD HH24:MI:SS')
       and aip.invoice_id = ai.invoice_id;

    --local Variable Declaration
    p_bank_id           NUMBER;
    p_check_id          NUMBER;
    g_ever_failed       BOOLEAN         := FALSE;
    l_msg               VARCHAR2(2000);
     L_LOCATION_NAME   VARCHAR2(240);
    L_PREFIX           VARCHAR2(3);

  BEGIN
    errbuf  := '';
    retcode := 0;
    fnd_file.put_line (
                   fnd_file.LOG,
                      'Procedure XXABRL_SCBANK_PAYMENT_SUPP Started'
               );

     FOR v_bank IN BANK_CUR LOOP

        FOR v_payment IN PAYMENT_CUR(v_bank.bank_id) LOOP

           l_msg := NULL;
             fnd_file.put_line (
                   fnd_file.LOG,
                      '  Processing Check ID.. '
                   ||v_payment.check_id
                );
                -- Each time we loop through items
                -- g_ever_failed flag is set to FALSE
                g_ever_failed := FALSE;

            IF v_bank.bank_name  IS NULL THEN
                 g_ever_failed := TRUE;
                 l_msg := 'Bank Name is Null'
                       || ','
                                     || 'Bank Name is Mandatory ';
                fnd_file.put_line (
                   fnd_file.LOG,
                      l_msg ||'-'||v_payment.check_number
                );

            END IF;

            IF v_payment.bank_account_num is null then
                 g_ever_failed := TRUE;

             l_msg := 'Bank Account Number is Null'
                       || ','
                                     || 'Bank Account Number is Mandatory ' ;

            fnd_file.put_line (
                   fnd_file.LOG,
                      l_msg ||'-'||v_payment.check_number
                );

            END IF;

             IF v_payment.op_name is null then
                 g_ever_failed := TRUE;

             l_msg := 'Operating Unit is Null'
                       || ','
                                     || 'Operating Unit is Mandatory ' ;

            fnd_file.put_line (
                   fnd_file.LOG,
                      l_msg ||'-'||v_payment.check_number
                );

            END IF;

            IF v_payment.bank_account_type is null then
                 g_ever_failed := TRUE;
             l_msg := 'Corporate Bank Account Type is Null'
                       || ','
                                     || 'Corporate Bank Account Type is Mandatory ' ;

            fnd_file.put_line (
                   fnd_file.LOG,
                      l_msg ||'-'||v_payment.check_number
                );

            END IF;

            if v_payment.sup_name is null then
                 g_ever_failed := TRUE;

             l_msg := 'Supplier Name is Null'
                       || ','
                                     || 'Supplier Name is Mandatory ' ;

            fnd_file.put_line (
                   fnd_file.LOG,
                      l_msg ||'-'||v_payment.check_number
                );

            end if;

              if v_payment.sup_no is null then
                 g_ever_failed := TRUE;
             l_msg := 'Supplier Number is Null'
                       || ','
                                     || 'Supplier Number is Mandatory ' ;

            fnd_file.put_line (
                   fnd_file.LOG,
                      l_msg ||'-'||v_payment.check_number
                );

            end if;

            IF v_payment.vendor_site_code  IS NULL THEN
                 g_ever_failed := TRUE;

                 l_msg := 'Supplier Site Code is Null'
                       || ','
                                     || 'Supplier Site Code is Mandatory ' ;

            fnd_file.put_line (
                   fnd_file.LOG,
                      l_msg ||'-'||v_payment.check_number
                );

            END IF;

            IF v_payment.site_address  IS NULL THEN
                 g_ever_failed := TRUE;

                 l_msg := 'Supplier Site Address is Null'
                       || ','
                                     || 'Supplier Site Address is Mandatory ' ;

            fnd_file.put_line (
                   fnd_file.LOG,
                      l_msg ||'-'||v_payment.check_number
                );

            END IF;

             IF v_payment.prod_code in ('R','N') AND v_payment.supp_bank_name  IS NULL THEN
                 g_ever_failed := TRUE;

                 l_msg := 'Supplier Bank Name is Null'
                       || ','
                                     || 'Supplier Bank Name is Mandatory ' ;

            fnd_file.put_line (
                   fnd_file.LOG,
                      l_msg ||'-'||v_payment.check_number
                );

            END IF;

             -- Condition for RTGS and NEFT
              IF v_payment.prod_code = 'R' AND  (v_payment.pay_amount) < 200000
              THEN
                 g_ever_failed := TRUE;

                 l_msg := 'Payment Amount is less than 2 Lakh.'
                       || ','
                                     || 'For RTGS Payments Amount Should be morethan or equal to 2 Lakh.' ;

            fnd_file.put_line (
                   fnd_file.LOG,
                      l_msg ||'-'||v_payment.check_number
                );

            END IF;

            IF v_payment.check_number  IS NULL THEN
                 g_ever_failed := TRUE;

                 l_msg := 'Supplier Check Number is Null'
                       || ','
                                     || 'Supplier Check Number is Mandatory ' ;

            fnd_file.put_line (
                   fnd_file.LOG,
                      l_msg ||'-'||v_payment.check_number
                );

            END IF;

            -- Condition for RTGS and NEFT
             IF v_payment.prod_code in ('R','N') AND
                (v_payment.supp_ifsc_code  IS NULL or length(v_payment.supp_ifsc_code) <> 11)
             THEN
                 g_ever_failed := TRUE;

                 l_msg := 'Supplier IFSC Code is Invalid/Null'
                       || ','
                                     || 'Supplier IFSC Code is Mandatory for RTGS and NEFT' ;

            fnd_file.put_line (
                   fnd_file.LOG,
                      l_msg ||'-'||v_payment.check_number
                );

            END IF;

            --Condition for RTGS and NEFT ,IFT
            IF v_payment.prod_code in ('R','N','B') AND v_payment.supp_acc_no  IS NULL THEN
                 g_ever_failed := TRUE;

                 l_msg := 'Supplier Account No is Null'
                       || ','
                                     || 'Supplier Account No is Mandatory ' ;

                fnd_file.put_line (
                   fnd_file.LOG,
                      l_msg ||'-'||v_payment.check_number
                );

            END IF;

            --Condition for RTGS and NEFT
             IF v_payment.prod_code in ('R','N') AND v_payment.supp_bank_acc_type  IS NULL THEN
                 g_ever_failed := TRUE;
                 l_msg := 'Supplier Account Type is Null'
                       || ','
                                     || 'Supplier Account Type is Mandatory ';
                fnd_file.put_line (
                   fnd_file.LOG,
                      l_msg ||'-'||v_payment.check_number
                );

            END IF;

            IF v_payment.pay_amount  IS NULL THEN
                 g_ever_failed := TRUE;
                 l_msg := 'Payment Amount is Null'
                       || ','
                                     || 'Payment Amount is Mandatory ' ;
                fnd_file.put_line (
                   fnd_file.LOG,
                      l_msg ||'-'||v_payment.check_number
                );

            END IF;

            IF v_payment.prod_code  IS NULL THEN
                 g_ever_failed := TRUE;

                 l_msg := 'Payment Method is Null'
                       || ','
                                     || 'Payment Method is Mandatory ' ;

                fnd_file.put_line (
                   fnd_file.LOG,
                      l_msg ||'-'||v_payment.check_number
                );

            END IF;

            IF v_payment.pay_run_date  IS NULL THEN
                 g_ever_failed := TRUE;

                 l_msg := 'Payment Date is Null'
                       || ','
                                     || 'Payment Date is Mandatory ';

                fnd_file.put_line (
                   fnd_file.LOG,
                      l_msg ||'-'||v_payment.check_number
                );

            END IF;

            /* OU Codification*/
           L_LOCATION_NAME := NULL;
            L_PREFIX        := NULL;

            IF v_payment.op_name IS NOT NULL THEN

                BEGIN
                SELECT LOCATION_NAME,PREFIX_VENDOR_CODE INTO L_LOCATION_NAME,L_PREFIX
                 FROM APPS.SCB_OU_CODIFICATION
                WHERE OU_NAME = v_payment.op_name;

                EXCEPTION
                WHEN OTHERS THEN
                L_LOCATION_NAME := NULL;
                 g_ever_failed := TRUE;
                 l_msg := 'OU Codification is Null'
                           || ','
                                         || 'OU Codification  is Mandatory ';

                END;

            END IF;

         IF g_ever_failed = FALSE THEN
            INSERT INTO SCB_PAYMENT_TABLE
             (CORPORATE_ACCOUNT_NUMBER  ,
              CORP_BANK_NAME          ,
              CORP_ACC_TYPE           ,
              THIRD_PARTY_ID          ,
              RUN_IDENTIFICATION      ,
              USER_ID                 ,
              USER_DEPT               ,
              DD_PAYABLE_LOCATION     ,
              PROFIT_CENTRE           ,
              BATCH_ID                ,
              CORP_BANK_CITY          ,
              CORP_BANK_BRANCH        ,
              VENDOR_CODE             ,
              TRANSACTIONAL_AMOUNT    ,
              TRANSACTIONAL_CURRENCY  ,
              PRODUCT_CODE            ,
              TRANSACTION_VALUE_DATE  ,
              PAY_DOC_NUMBER          ,
              PAY_RUN_DATE            ,
              INSTRUCTION_DATE        ,
              BENEFI_ACC_NUM          ,
              BENEFI_BANK_NAME        ,
              BENEFI_ACC_TYPE         ,
              BENEFI_IFSC_CODE        ,
              VENDOR_TYPE             ,
              PRIMARY_NAME            ,
              SECONDARY_NAME          ,
              VENDOR_ADDRESS          ,
              VENDOR_LOCATION         ,
              VENDOR_PINCODE          ,
              PRIMARY_EMAIL           ,
              VENDOR_MOBILE           ,
              BENFI_BANK_CITY         ,
              BENEFI_BANK_BRANCH      ,
              CHECK_ID                ,
              TRANSACTION_STATUS      ,
              UPDATED_IN_APP          ,
              ATTRIBUTE_CATEGORY      ,
              ATTRIBUTE1              ,
              ATTRIBUTE2              ,
              ATTRIBUTE3              ,
              ATTRIBUTE4              ,
              ATTRIBUTE5              ,
              CREATION_DATE           ,
              CREATED_BY              ,
              LAST_UPDATE_DATE        ,
              LAST_UPDATED_BY
                       )
            values
              (v_payment.bank_account_num,
               v_bank.bank_name,
               v_payment.bank_account_type,
               L_LOCATION_NAME,
               v_payment.run_identification,
               v_payment.user_name,
               NULL,
               v_payment.pay_location,
               NULL,
               v_payment.batch_id,
               v_payment.corp_bank_city,
               v_payment.corp_bank_brnch,
               L_PREFIX||''||v_payment.sup_no               ,
               v_payment.pay_amount,
               v_payment.currency,
               v_payment.prod_code,
               v_payment.trx_val_date,
               v_payment.check_number,
               v_payment.pay_run_date,
               v_payment.pay_inst_date,
               v_payment.supp_acc_no,
               v_payment.supp_bank_name,
               v_payment.supp_bank_acc_type,
               v_payment.supp_ifsc_code,
               v_payment.vendor_type,
               v_payment.sup_name,
               v_payment.sup_alt_name,
               v_payment.site_address,
               v_payment.vendor_site_code,
               v_payment.pin_code,
               v_payment.email_id,
               v_payment.phone,
               NULL,--Supplier Bank City
               v_payment.supp_bank_bran,
               v_payment.check_id,
               'AUTHORIZED',
               'N',
               NULL, -- SCB Payment DFF
               NULL, -- Status Code
               NULL, --Status Description
               NULL, --UTR Number
               NULL, --check Number
               NULL, --Bank Reference No
               v_payment.creation_date,
               v_payment.created_by,
               v_payment.last_update_date,
               v_payment.last_updated_by
               );

                    FOR v_inv in INV_CUR(v_payment.check_id) LOOP

                         IF v_inv.invoice_num  IS NULL THEN
                             g_ever_failed := TRUE;
                             l_msg := 'Invoice Number is Null'
                                   || ','
                                                 || 'Invoice Number is Mandatory ' ;
                            fnd_file.put_line (
                                   fnd_file.LOG,
                                      l_msg ||'-'||v_payment.check_number
                                );

                        END IF;

                        IF v_inv.invoice_amount  IS NULL THEN
                             g_ever_failed := TRUE;

                             l_msg := 'Invoice Amount is Null'
                                   || ','
                                                 || 'Invoice Amount is Mandatory ' ;

                            fnd_file.put_line (
                                   fnd_file.LOG,
                                      l_msg ||'-'||v_payment.check_number
                                );

                        END IF;

                    INSERT INTO apps.SCB_PAYMENT_INVOICE
                           ( check_id
                           , invoice_number
                           , invoice_date
                           , pay_type_code
                           , invoice_amount
                           , net_amount
                       --    , invoice_desc -- added by Amresh to print in advice file for SC Bank
                           )
                    VALUES ( v_inv.check_id
                            ,v_inv.invoice_num
                            ,v_inv.invoice_date
                            ,v_inv.invoice_type_lookup_code
                            ,v_inv.invoice_amount
                            ,to_char(v_inv.amount_paid)
                        --    ,v_inv.DESCRIPTION -- added by Amresh to print in advice file for SC Bank
                           );

                    END LOOP;

                    COMMIT;
          END IF;

         END LOOP;

   END LOOP;

   COMMIT;

    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        l_msg := 'No Records Processed';
    WHEN OTHERS THEN
        errbuf := 'Other Exceptions '||SQLERRM;
        retcode := '1';

  END XXABRL_SCBANK_PAYMENT_SUPP;

  PROCEDURE XXABRL_UPDATE_SCB_INFO  (errbuf  OUT VARCHAR2,
                                        retcode OUT VARCHAR2) IS
    /*
        This Procedure will Update the Attribute Status Once SCB Bank gives response file
    */
        -- To get status from the SCB bank
   CURSOR SCB_REV_CUR
   IS
    SELECT   DPT.check_id, DPT.pay_doc_number, DPT.attribute_category,
             DPT.attribute1, DPT.attribute2, DPT.attribute3, DPT.attribute4,
             DPT.attribute5, DPT.transaction_status, DPT.run_identification,
             DPT.product_code,DPT.updated_in_app
        FROM APPS.SCB_PAYMENT_TABLE DPT
       WHERE DPT.attribute_category IS NOT NULL
         AND DPT.updated_in_app ='N' ;
         --AND DPT.check_id = p_check_id;

 l_updated_flag  varchar2(1) :='N' ;
    BEGIN
        l_updated_flag  :='N' ;
     -- Updating with Statuses whatever bank is provided in Attributes
       FOR upd_chk IN SCB_REV_CUR
       LOOP

          BEGIN
             UPDATE apps.ap_checks_all
                SET attribute_category = upd_chk.attribute_category,
                    attribute1 = upd_chk.attribute1,
                    attribute2 = upd_chk.attribute2
              WHERE check_number = upd_chk.pay_doc_number
                AND check_id = upd_chk.check_id;
                 l_updated_flag  :='Y' ;

          EXCEPTION
             WHEN OTHERS
             THEN
                NULL;
          END;

          IF (upd_chk.attribute1 = 'SUCCESS') THEN
            IF upd_chk.product_code = 'R' OR upd_chk.product_code ='N'
                THEN
                    UPDATE apps.ap_checks_all
                    SET attribute_category = upd_chk.attribute_category,
                                attribute3 = upd_chk.attribute3
                    WHERE check_number = upd_chk.pay_doc_number
                      AND check_id = upd_chk.check_id;
            ELSIF upd_chk.product_code = 'D' OR upd_chk.product_code ='C'
                THEN
                    UPDATE apps.ap_checks_all
                    SET attribute_category = upd_chk.attribute_category,
                                attribute4 = upd_chk.attribute4
                    WHERE check_number = upd_chk.pay_doc_number
                      AND check_id = upd_chk.check_id;
            ELSIF upd_chk.product_code = 'B'
                THEN
                    UPDATE apps.ap_checks_all
                    SET attribute_category = upd_chk.attribute_category,
                                attribute5 = upd_chk.attribute5
                    WHERE check_number = upd_chk.pay_doc_number
                      AND check_id = upd_chk.check_id;
            END IF;

         END IF;

        IF l_updated_flag  ='Y' THEN
        UPDATE APPS.SCB_PAYMENT_TABLE DPT
        SET UPDATED_IN_APP ='Y'
        WHERE DPT.pay_doc_number = upd_chk.pay_doc_number
              AND DPT.check_id = upd_chk.check_id;

        END IF;

       END LOOP;

       COMMIT;

    END XXABRL_UPDATE_SCB_INFO;
PROCEDURE XXABRL_SCB_USER_RECTIFIED_PAY( errbuf     out VARCHAR2
                                  ,  retcode    out VARCHAR2
                                  ,  p_from_check_id NUMBER
                                  ,  p_to_check_id NUMBER ) IS
    -- This Procedure will Update the Rejected Status to AUTHORIZED once Payments are Rectified by User.
 p_bank_id           NUMBER;
      --TO SELECT BANK (SCB BANK)
    CURSOR SCB_BANK_CUR IS
      SELECT bankorgprofile.party_id bank_id,
             bankparty.party_name    BANK_NAME
        FROM hz_parties               bankparty,
             hz_organization_profiles bankorgprofile,
             hz_code_assignments      bankca
       WHERE 1 = 1
         AND bankparty.party_id = bankorgprofile.party_id
         AND bankca.owner_table_name = 'HZ_PARTIES'
         AND bankca.owner_table_id = bankparty.party_id
         AND bankca.class_category = 'BANK_INSTITUTION_TYPE'
         AND bankca.class_code IN ('BANK', 'CLEARINGHOUSE')
         -- AND bankparty.party_id = 13526282
         AND UPPER(bankorgprofile.organization_name) IN ('STANDARD CHARTERED BANK');

 CURSOR SCB_REJ_CUR(p_bank_id number) IS
  select aca.doc_sequence_value voucher_no,
          aca.check_id,
          aca.check_number ,
          apss.attribute10 pay_location,
          cbb.city corp_bank_city,
          cbb.bank_branch_name corp_bank_brnch,
          apss.attribute4 supp_acc_no,
          apss.attribute2 supp_bank_name,
          apss.attribute9 supp_bank_acc_type,
        decode(aca.payment_method_code,'RTGS',apss.attribute8,apss.attribute7) supp_ifsc_code,
          nvl(aps.vendor_name_alt,aps.vendor_name) sup_name,
          aps.vendor_name sup_alt_name,
          apss.attribute11
                        ||decode(apss.attribute12,null,apss.attribute12,','||apss.attribute12)
                        ||decode(apss.attribute13,null,apss.attribute13,','||apss.attribute13) email_id
        from ap_checks_all         aca,
             ap_suppliers          aps,
             ap_supplier_sites_all apss,
             iby_payments_all      ip,
             ce_bank_accounts      cba,
             hr_operating_units    hou,
             fnd_user fu,
             cebv_bank_branches cbb
       where 1 = 1
         and aca.vendor_id = aps.vendor_id
         and aps.vendor_id = apss.vendor_id
         and aca.vendor_site_id = apss.vendor_site_id
         and aca.payment_id = ip.payment_id
         and ip.internal_bank_account_id = cba.bank_account_id
         and ip.payment_status in ('FORMATTED', 'ISSUED')
         and aca.org_id = hou.organization_id
         and aca.void_date is null
         and aca.created_by = fu.user_id
         and cba.bank_id =p_bank_id
         and cba.bank_branch_id = cbb.bank_branch_id
         and cba.bank_account_id in (SELECT LOOKUP_CODE
                                        from  fnd_lookup_values
                                        where lookup_type ='XXABRL_SCB_BANK_H2H_ACCOUNT'
                                        and   TO_NUMBER(LOOKUP_CODE) =cba.bank_account_id
                                        and nvl(aca.anticipated_value_date,aca.check_date)>=start_date_active
                                        )
         and UPPER(aca.bank_account_name) = UPPER(cba.bank_account_name)
         and aca.check_id BETWEEN nvl(p_from_check_id,aca.check_id) AND NVL(p_to_check_id,aca.check_id) ;

    BEGIN

     FOR V_SCB IN SCB_BANK_CUR LOOP
        FOR cur_rej_rec IN SCB_REJ_CUR(V_SCB.bank_id) LOOP
            UPDATE APPS.SCB_PAYMENT_TABLE SCB_TAB
            SET    SCB_TAB.transaction_status  = 'AUTHORIZED'
                  ,SCB_TAB.ATTRIBUTE_CATEGORY  = NULL --SCB Additional Information SET TO NULL
                  ,SCB_TAB.attribute1          = NULL  -- Status Code
                  ,SCB_TAB.attribute2          = NULL  --Status Description
                  ,SCB_TAB.attribute3          = NULL  --UTR Number
                  ,SCB_TAB.attribute4          = NULL  --check Number
                  ,SCB_TAB.attribute5          = NULL  --Bank Reference No
                  ,SCB_TAB.benefi_ifsc_code    = cur_rej_rec.supp_ifsc_code --NEFT OR RTGS
                  ,SCB_TAB.dd_payable_location = cur_rej_rec.pay_location
                  ,SCB_TAB.corp_bank_city      = cur_rej_rec.corp_bank_city
                  ,SCB_TAB.corp_bank_branch    = cur_rej_rec.corp_bank_brnch
                  ,SCB_TAB.benefi_acc_num      = cur_rej_rec.supp_acc_no
                  ,SCB_TAB.benefi_bank_name    = cur_rej_rec.supp_bank_name
                  ,SCB_TAB.benefi_acc_type     = cur_rej_rec.supp_bank_acc_type
                  ,SCB_TAB.primary_name        = cur_rej_rec.sup_name
                  ,SCB_TAB.secondary_name      = cur_rej_rec.sup_alt_name
                  ,SCB_TAB.primary_email       = cur_rej_rec.email_id
                  ,SCB_TAB.updated_in_app      ='N'
            WHERE  SCB_TAB.attribute1 = 'TRANSACTION REJECTED'
               AND SCB_TAB.check_id = cur_rej_rec.CHECK_ID ;
        END LOOP;

     END LOOP;

         COMMIT;

    END XXABRL_SCB_USER_RECTIFIED_PAY;

  -- New Procedure for Sceduling Purpose.. Except Date Parameter Everything is same.
  PROCEDURE XXABRL_SCD_SCB_PAYMENT_SUPP(errbuf  OUT VARCHAR2,
                                        retcode OUT VARCHAR2
                                        )
   IS
    --TO SELECT BANK (SCB BANK)
    CURSOR BANK_CUR IS
      SELECT bankorgprofile.party_id bank_id,
             bankparty.party_name    BANK_NAME
        FROM hz_parties               bankparty,
             hz_organization_profiles bankorgprofile,
             hz_code_assignments      bankca
       WHERE 1 = 1
         AND bankparty.party_id = bankorgprofile.party_id
         AND bankca.owner_table_name = 'HZ_PARTIES'
         AND bankca.owner_table_id = bankparty.party_id
         AND bankca.class_category = 'BANK_INSTITUTION_TYPE'
         AND bankca.class_code IN ('BANK', 'CLEARINGHOUSE')
         -- AND bankparty.party_id = 13526282
         AND UPPER(bankorgprofile.organization_name) IN ('STANDARD CHARTERED BANK');

    -- TO SELECT ALL PAYMENTS RELATED TO SCB
    CURSOR PAYMENT_CUR(p_bank_id number) IS
    select
        decode(aca.payment_type_flag, 'A',(select cb.bank_account_num
            from ce_bank_acct_uses_all cbau,ce_bank_accounts cb
            where cbau.bank_acct_use_id =aca.ce_bank_acct_use_id
            and   cbau.bank_account_id = cb.bank_account_id),aca.bank_account_num) bank_account_num,--Payment Batch
        --aca.bank_account_num,
        cba.bank_account_type,
        hou.name op_name,
        aca.checkrun_name run_identification,
        fu.user_name,
        apss.attribute10 pay_location,--
        aca.checkrun_name batch_id,
        cbb.city corp_bank_city,
        cbb.bank_branch_name corp_bank_brnch,
        aps.segment1 sup_no,
        aca.amount pay_amount,
        aca.currency_code currency,
        decode(aca.payment_method_code,'N','N','RTGS','R','CHECK','C','DD','D','IFT','B') prod_code,
        nvl(aca.anticipated_value_date,aca.check_date) trx_val_date,
        aca.check_number ,
        aca.check_date pay_run_date,
        aca.check_date pay_inst_date,
        apss.attribute4 supp_acc_no,--
        apss.attribute2 supp_bank_name,--
        apss.attribute9 supp_bank_acc_type,--
        decode(aca.payment_method_code,'RTGS',apss.attribute8,apss.attribute7) supp_ifsc_code,--
        decode(aps.VENDOR_TYPE_LOOKUP_CODE,'NON MERCHANDISE','N','MERCHANDISE','M') vendor_type, -- added by amresh on 07 Aug 2012, requested by Bala Sir/SC Bank
        nvl(aps.vendor_name_alt,aps.vendor_name) sup_name,
        aps.vendor_name sup_alt_name,
         (apss.address_line1 || ';' || apss.address_line2 || ';' ||
             apss.address_line2 || ';' || apss.state || ';' || apss.zip) site_address,
        apss.vendor_site_code,
        apss.zip pin_code,
        apss.attribute11
                        ||decode(apss.attribute12,null,apss.attribute12,','||apss.attribute12)
                        ||decode(apss.attribute13,null,apss.attribute13,','||apss.attribute13) email_id,
        apss.phone ,
        NULL supp_bank_acc_city,
        apss.attribute3 supp_bank_bran,--
        aca.check_id,
        aca.org_id,
        aca.payment_id,
        aca.doc_sequence_value voucher_no,
        apss.vendor_site_id,
        cba.bank_id,
        cba.bank_branch_id,
        aca.ce_bank_acct_use_id,
        aca.party_id,
        aps.vendor_id,
        aca.created_by,
        aca.creation_date,
        aca.last_update_date,
        aca.last_updated_by
        from ap_checks_all         aca,
             ap_suppliers          aps,
             ap_supplier_sites_all apss,
             iby_payments_all      ip,
             ce_bank_accounts      cba,
             hr_operating_units    hou,
             fnd_user fu,
             cebv_bank_branches cbb
       where 1 = 1
         and aca.vendor_id = aps.vendor_id
         and aps.vendor_id = apss.vendor_id
         and aca.vendor_site_id = apss.vendor_site_id
         and aca.payment_id = ip.payment_id
         and ip.internal_bank_account_id = cba.bank_account_id
         and ip.payment_status in ('FORMATTED', 'ISSUED')
         and aca.org_id = hou.organization_id
         and aca.void_date is null
         and aca.created_by = fu.user_id
         and cba.bank_id =p_bank_id
         and cba.bank_branch_id = cbb.bank_branch_id
         and cba.bank_account_id in (SELECT LOOKUP_CODE
                                        from  fnd_lookup_values
                                        where lookup_type ='XXABRL_SCB_BANK_H2H_ACCOUNT'
                                        and   TO_NUMBER(LOOKUP_CODE) =cba.bank_account_id
                                        and nvl(aca.anticipated_value_date,aca.check_date)>=start_date_active
                                        )
         and UPPER(aca.bank_account_name) = UPPER(cba.bank_account_name)
         AND trunc(aca.creation_date) =trunc(sysdate)
         --Restrict the Duplicate Payments
         and not exists (select check_id
                                    from SCB_PAYMENT_TABLE DPT
                                    where DPT.check_id = aca.check_id) ;

--TO SELECT INVOICES AGAINST EACH PAYMENT (ONLY FOR SCB BANK)
    CURSOR INV_CUR(p_check_id NUMBER ) IS
     select ac.check_id, ai.invoice_num,ai.invoice_date,
            ai.invoice_amount,ai.invoice_type_lookup_code,aip.AMOUNT AMOUNT_PAID,ai.DESCRIPTION
      from ap_checks_all           ac,
           ap_invoice_payments_all aip,
           ap_invoices_all         ai
     where 1 = 1
       and ac.check_id =p_check_id
       and ac.check_id = aip.check_id
       AND trunc(ac.creation_date) =trunc(sysdate)
       and aip.invoice_id = ai.invoice_id;

    --local Variable Declaration
    p_bank_id           NUMBER;
    p_check_id          NUMBER;
    g_ever_failed       BOOLEAN         := FALSE;
    l_msg               VARCHAR2(2000);
     L_LOCATION_NAME   VARCHAR2(240);
    L_PREFIX           VARCHAR2(3);

  BEGIN
    errbuf  := '';
       retcode := 0;
    fnd_file.put_line (
                   fnd_file.LOG,
                      'Procedure XXABRL_SCBANK_PAYMENT_SUPP Started'
               );

     FOR v_bank IN BANK_CUR LOOP

        FOR v_payment IN PAYMENT_CUR(v_bank.bank_id) LOOP
           l_msg := NULL;
             fnd_file.put_line (
                   fnd_file.LOG,
                      '  Processing Check ID.. '
                   ||v_payment.check_id
                );

                -- Each time we loop through items
                -- g_ever_failed flag is set to FALSE
                g_ever_failed := FALSE;

            IF v_bank.bank_name  IS NULL THEN
                 g_ever_failed := TRUE;
                 l_msg := 'Bank Name is Null'
                       || ','
                                     || 'Bank Name is Mandatory ';
                fnd_file.put_line (
                   fnd_file.LOG,
                      l_msg ||'-'||v_payment.check_number
                );

            END IF;

            IF v_payment.bank_account_num is null then
                 g_ever_failed := TRUE;

             l_msg := 'Bank Account Number is Null'
                       || ','
                                     || 'Bank Account Number is Mandatory ' ;
            fnd_file.put_line (
                   fnd_file.LOG,
                      l_msg ||'-'||v_payment.check_number
                );
            END IF;

             IF v_payment.op_name is null then
                 g_ever_failed := TRUE;

             l_msg := 'Operating Unit is Null'
                       || ','
                                     || 'Operating Unit is Mandatory ' ;

            fnd_file.put_line (
                   fnd_file.LOG,
                      l_msg ||'-'||v_payment.check_number
                );

            END IF;

            IF v_payment.bank_account_type is null then
                 g_ever_failed := TRUE;
             l_msg := 'Corporate Bank Account Type is Null'
                       || ','
                                     || 'Corporate Bank Account Type is Mandatory ' ;

            fnd_file.put_line (
                   fnd_file.LOG,
                      l_msg ||'-'||v_payment.check_number
                );

            END IF;

            if v_payment.sup_name is null then
                 g_ever_failed := TRUE;
             l_msg := 'Supplier Name is Null'
                       || ','
                                     || 'Supplier Name is Mandatory ' ;

            fnd_file.put_line (
                   fnd_file.LOG,
                      l_msg ||'-'||v_payment.check_number
                );

            end if;

              if v_payment.sup_no is null then
                 g_ever_failed := TRUE;

             l_msg := 'Supplier Number is Null'
                       || ','
                                     || 'Supplier Number is Mandatory ' ;

            fnd_file.put_line (
                   fnd_file.LOG,
                      l_msg ||'-'||v_payment.check_number
                );

            end if;

            IF v_payment.vendor_site_code  IS NULL THEN
                 g_ever_failed := TRUE;
                 l_msg := 'Supplier Site Code is Null'
                       || ','
                                     || 'Supplier Site Code is Mandatory ' ;

            fnd_file.put_line (
                   fnd_file.LOG,
                      l_msg ||'-'||v_payment.check_number
                );

            END IF;

            IF v_payment.site_address  IS NULL THEN
                 g_ever_failed := TRUE;

                 l_msg := 'Supplier Site Address is Null'
                       || ','
                                     || 'Supplier Site Address is Mandatory ' ;

            fnd_file.put_line (
                   fnd_file.LOG,
                      l_msg ||'-'||v_payment.check_number
                );

            END IF;

             IF v_payment.prod_code in ('R','N') AND v_payment.supp_bank_name  IS NULL THEN
                 g_ever_failed := TRUE;
                 l_msg := 'Supplier Bank Name is Null'
                       || ','
                                     || 'Supplier Bank Name is Mandatory ' ;--||'-'||l_msg;

            fnd_file.put_line (
                   fnd_file.LOG,
                      l_msg ||'-'||v_payment.check_number
                );

            END IF;

             --Condition for RTGS and NEFT
              IF v_payment.prod_code = 'R' AND  (v_payment.pay_amount) < 200000
              THEN
                 g_ever_failed := TRUE;

                 l_msg := 'Payment Amount is less than 2 Lakh.'
                       || ','
                                     || 'For RTGS Payments Amount Should be morethan or equal to 2 Lakh.' ;--||'-'||l_msg;

            fnd_file.put_line (
                   fnd_file.LOG,
                      l_msg ||'-'||v_payment.check_number
                );

            END IF;

            IF v_payment.check_number  IS NULL THEN
                 g_ever_failed := TRUE;
                 l_msg := 'Supplier Check Number is Null'
                       || ','
                                     || 'Supplier Check Number is Mandatory ' ;

            fnd_file.put_line (
                   fnd_file.LOG,
                      l_msg ||'-'||v_payment.check_number
                );

            END IF;

            --Condition for RTGS and NEFT
             IF v_payment.prod_code in ('R','N') AND
                (v_payment.supp_ifsc_code  IS NULL or length(v_payment.supp_ifsc_code) <> 11)
             THEN
                 g_ever_failed := TRUE;

                 l_msg := 'Supplier IFSC Code is Invalid/Null'
                       || ','
                                     || 'Supplier IFSC Code is Mandatory for RTGS and NEFT' ;

            fnd_file.put_line (
                   fnd_file.LOG,
                      l_msg ||'-'||v_payment.check_number
                );

            END IF;

            --Condition for RTGS and NEFT ,IFT
            IF v_payment.prod_code in ('R','N','B') AND v_payment.supp_acc_no  IS NULL THEN
                 g_ever_failed := TRUE;

                 l_msg := 'Supplier Account No is Null'
                       || ','
                                     || 'Supplier Account No is Mandatory ' ;

                fnd_file.put_line (
                   fnd_file.LOG,
                      l_msg ||'-'||v_payment.check_number
                );

            END IF;

            --Condition for RTGS and NEFT
             IF v_payment.prod_code in ('R','N') AND v_payment.supp_bank_acc_type  IS NULL THEN
                 g_ever_failed := TRUE;
                 l_msg := 'Supplier Account Type is Null'
                       || ','
                                     || 'Supplier Account Type is Mandatory ';

                fnd_file.put_line (
                   fnd_file.LOG,
                      l_msg ||'-'||v_payment.check_number
                );

            END IF;

            IF v_payment.pay_amount  IS NULL THEN
                 g_ever_failed := TRUE;
                 l_msg := 'Payment Amount is Null'
                       || ','
                                     || 'Payment Amount is Mandatory ' ;

                fnd_file.put_line (
                   fnd_file.LOG,
                      l_msg ||'-'||v_payment.check_number
                );

            END IF;

            IF v_payment.prod_code  IS NULL THEN
                 g_ever_failed := TRUE;
                 l_msg := 'Payment Method is Null'
                       || ','
                                     || 'Payment Method is Mandatory ' ;
                fnd_file.put_line (
                   fnd_file.LOG,
                      l_msg ||'-'||v_payment.check_number
                );

            END IF;

            IF v_payment.pay_run_date  IS NULL THEN
                 g_ever_failed := TRUE;

                 l_msg := 'Payment Date is Null'
                       || ','
                                     || 'Payment Date is Mandatory ';

                fnd_file.put_line (
                   fnd_file.LOG,
                      l_msg ||'-'||v_payment.check_number
                );

            END IF;

            /* OU Codification*/
            L_LOCATION_NAME := NULL;
            L_PREFIX        := NULL;

            IF v_payment.op_name IS NOT NULL THEN

                BEGIN
                SELECT LOCATION_NAME,PREFIX_VENDOR_CODE INTO L_LOCATION_NAME,L_PREFIX
                 FROM APPS.SCB_OU_CODIFICATION
                WHERE OU_NAME = v_payment.op_name;

                EXCEPTION
                WHEN OTHERS THEN
                L_LOCATION_NAME := NULL;
                 g_ever_failed := TRUE;
                 l_msg := 'OU Codification is Null'
                           || ','
                                         || 'OU Codification  is Mandatory ';
                END;

            END IF;

         IF g_ever_failed = FALSE THEN
            insert into SCB_PAYMENT_TABLE
             (CORPORATE_ACCOUNT_NUMBER  ,
              CORP_BANK_NAME          ,
              CORP_ACC_TYPE           ,
              THIRD_PARTY_ID          ,
              RUN_IDENTIFICATION      ,
              USER_ID                 ,
              USER_DEPT               ,
              DD_PAYABLE_LOCATION     ,
              PROFIT_CENTRE           ,
              BATCH_ID                ,
              CORP_BANK_CITY          ,
              CORP_BANK_BRANCH        ,
              --PAY_TYPE_CODE           ,
              VENDOR_CODE             ,
              TRANSACTIONAL_AMOUNT    ,
              TRANSACTIONAL_CURRENCY  ,
              PRODUCT_CODE            ,
              TRANSACTION_VALUE_DATE  ,
              PAY_DOC_NUMBER          ,
              PAY_RUN_DATE            ,
              INSTRUCTION_DATE        ,
              BENEFI_ACC_NUM          ,
              BENEFI_BANK_NAME        ,
              BENEFI_ACC_TYPE         ,
              BENEFI_IFSC_CODE        ,
          --    VENDOR_TYPE             ,
              PRIMARY_NAME            ,
              SECONDARY_NAME          ,
              VENDOR_ADDRESS          ,
              VENDOR_LOCATION         ,
              VENDOR_PINCODE          ,
              PRIMARY_EMAIL           ,
              VENDOR_MOBILE           ,
              BENFI_BANK_CITY         ,
              BENEFI_BANK_BRANCH      ,
              CHECK_ID                ,
              TRANSACTION_STATUS      ,
              UPDATED_IN_APP          ,
              ATTRIBUTE_CATEGORY      ,
              ATTRIBUTE1              ,
              ATTRIBUTE2              ,
              ATTRIBUTE3              ,
              ATTRIBUTE4              ,
              ATTRIBUTE5              ,
              CREATION_DATE           ,
              CREATED_BY              ,
              LAST_UPDATE_DATE        ,
              LAST_UPDATED_BY
                       )
            values
              (v_payment.bank_account_num,
               v_bank.bank_name,
               v_payment.bank_account_type,
               L_LOCATION_NAME,
               v_payment.run_identification,
               v_payment.user_name,
               NULL,
               v_payment.pay_location,
               NULL,
               v_payment.batch_id,
               v_payment.corp_bank_city,
               v_payment.corp_bank_brnch,
               L_PREFIX||''||v_payment.sup_no               ,
               v_payment.pay_amount,
               v_payment.currency,
               v_payment.prod_code,
               v_payment.trx_val_date,
               v_payment.check_number,
               v_payment.pay_run_date,
               v_payment.pay_inst_date,
               v_payment.supp_acc_no,
               v_payment.supp_bank_name,
               v_payment.supp_bank_acc_type,
               v_payment.supp_ifsc_code,
           --    v_payment.vendor_type,
               v_payment.sup_name,
               v_payment.sup_alt_name,
               v_payment.site_address,
               v_payment.vendor_site_code,
               v_payment.pin_code,
               v_payment.email_id,
               v_payment.phone,
               NULL,--Supplier Bank City
               v_payment.supp_bank_bran,
               v_payment.check_id,
               'AUTHORIZED',
               'N',
               NULL, -- SCB Payment DFF
               NULL, -- Status Code
               NULL, --Status Description
               NULL, --UTR Number
               NULL, --check Number
               NULL, --Bank Reference No
               v_payment.creation_date,
               v_payment.created_by,
               v_payment.last_update_date,
               v_payment.last_updated_by
               );

                    FOR v_inv in INV_CUR(v_payment.check_id) LOOP

                         IF v_inv.invoice_num  IS NULL THEN
                             g_ever_failed := TRUE;

                             l_msg := 'Invoice Number is Null'
                                   || ','
                                                 || 'Invoice Number is Mandatory ' ;

                            fnd_file.put_line (
                                   fnd_file.LOG,
                                      l_msg ||'-'||v_payment.check_number
                                );

                        END IF;

                        IF v_inv.invoice_amount  IS NULL THEN
                             g_ever_failed := TRUE;
                             l_msg := 'Invoice Amount is Null'
                                   || ','
                                                 || 'Invoice Amount is Mandatory ' ;

                            fnd_file.put_line (
                                   fnd_file.LOG,
                                      l_msg ||'-'||v_payment.check_number
                                );

                        END IF;

                    INSERT INTO SCB_PAYMENT_INVOICE
                           ( check_id
                           , invoice_number
                           , invoice_date
                           , pay_type_code
                           , invoice_amount
                           , net_amount
                           , invoice_desc -- added by Amresh to print in advice file for SC Bank
                           )
                    VALUES ( v_inv.check_id
                            ,v_inv.invoice_num
                            ,v_inv.invoice_date
                            ,v_inv.invoice_type_lookup_code
                            ,v_inv.invoice_amount
                            ,to_char(v_inv.amount_paid)
                            ,v_inv.DESCRIPTION -- added by Amresh to print in advice file for SC Bank
                           );

                    END LOOP;

                    COMMIT;

          END IF;

         END LOOP;

   END LOOP;

   COMMIT;

    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        l_msg := 'No Records Processed';

    WHEN OTHERS THEN
        errbuf := 'Other Exceptions '||SQLERRM;
        retcode := '1';
  END XXABRL_SCD_SCB_PAYMENT_SUPP;

END XXABRL_SUPP_BANK_PMT_SCB; 
/


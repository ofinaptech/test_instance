CREATE OR REPLACE PACKAGE APPS.XXABRL_RMS_GL_OCTROI_PKG IS
/*

||   Filename   : XXABRL_GL_INTER_PKG.sql
||   Description : Script is used to Load GL Data (GL Conversion)
||
||   Version     Date            Author              Modification
||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
||     1.0.0      12-MAY-2010    Praveen Kumar      New Development
      1.0.1      29-MAY-2010    Praveen/Sai         Existing RMS to GL Program is disabled.
                                                    Filter Condition Octroi is removed and
                                                    passing source of 'RETEK'& 'OCTROI'.
      1.0.2      17-JUL-2010    Sayikrishna         Dynamic CC ID function ,SYSDATE format changed
                                                    reason: some ccid is valid still it showing 0.
      1.0.3      03-AUG-2010    Sayikrishna         Cursor Added to Pick all ledger id's.(led_id_cur)     
      1.0.4      05-AUG-2011    Narasimhulu         Added Logic to capture ledger id based on Location  
      1.0.5      08-AUG-2012    Amresh Kumar        Changed the Journal Import Program submission to pick up only for new ledger                                               
||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
||
||   Usage : This script is used to upload data in apps.GL_INTERFACE TABLE
||

*/

 PROCEDURE XXABRL_GL_VALID_PROC(x_val_retcode OUT NUMBER);
 PROCEDURE XXABRL_GL_INT_PROC(x_val_ins_retcode OUT NUMBER);

 PROCEDURE MAIN_PROC(x_err_buf           OUT VARCHAR2
                    ,x_ret_code          OUT NUMBER);

END XXABRL_RMS_GL_OCTROI_PKG; 
/


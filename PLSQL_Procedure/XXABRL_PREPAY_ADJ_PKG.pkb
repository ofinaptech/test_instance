CREATE OR REPLACE PACKAGE BODY APPS.xxabrl_prepay_adj_pkg
IS
   PROCEDURE xxabrl_main_pkg (errbuf OUT VARCHAR2, retcode OUT NUMBER)
   IS
   BEGIN
      xxabrl_prepay_adj_prc;
   END;

   PROCEDURE xxabrl_prepay_adj_prc
   IS
   BEGIN
      EXECUTE IMMEDIATE 'truncate table XXABRLIPROC.XXABRL_PREPAY_ADJ_STG';

      FOR cur IN
(SELECT   povs.vendor_site_code, 
                 povs.VENDOR_SITE_ID,
               'PREPAYMENT-ADJUSTMENT' INVOICE_TYPE,
            replace (api.invoice_num,'^','//') INVOICE_NUM,    
             api.invoice_date, --api.description description,
              (SELECT  
               TRANSLATE ( api.description, 'X' || CHR (9) || CHR (10) || CHR (13), 'X' )
                from dual) description,             
            apd.dist_code_combination_id ccid,gl.SEGMENT3 SBU ,(select FFVL.description FROM apps.fnd_flex_values_vl ffvl
        WHERE ffvl.FLEX_VALUE_SET_ID=1013466
        AND ffvl.FLEX_VALUE=gl.SEGMENT3
         )SBU_desc,
         gl.segment4 LOCATION_CODE
         ,(SELECT FFVL.description FROM apps.fnd_flex_values_vl ffvl
        WHERE ffvl.FLEX_VALUE_SET_ID=1013467
        AND ffvl.FLEX_VALUE=gl.SEGMENT4) Location_desc,
        gl.segment6 ACCOUNT_CODE,
          (SELECT FFVL.description FROM apps.fnd_flex_values_vl ffvl
        WHERE ffvl.FLEX_VALUE_SET_ID=1013469
        AND ffvl.FLEX_VALUE=gl.SEGMENT6) account_desc,
             api.invoice_currency_code,
            decode(APD.REVERSAL_FLAG ,'N' ,ZZ.AMT,ZZ.AMT*-1) dr_val,
            decode(APD.REVERSAL_FLAG ,'N' ,ZZ.AMT,ZZ.AMT*-1) cr_val,null prepay_amt_remaing ,
            TO_CHAR (api.doc_sequence_value)payment_num,
            /*null pay_accounting_date,*/
            NULL check_number, null check_date , pov.segment1 VENDOR_NUM,
        (case when (substr(pov.segment1,1,1)='7') then (select apsa1.attribute14 from apps.ap_suppliers aps1,
        apps.ap_supplier_sites_all apsa1
        where
        aps1.vendor_id=apsa1.vendor_id
        and aps1.segment1=pov.SEGMENT1
        and apsa1.vendor_site_id=povs.vendor_site_id) else NULL end)"ATTRIBUTE_14",     
             pov.vendor_name,
            pov.vendor_type_lookup_code VENDOR_TYPE, 
            apd.po_distribution_id,
            apil.PO_HEADER_ID,
            apil.RCV_TRANSACTION_ID,
            api.ORG_ID,
            hr.name OPERATING_UNIT , 
            aBA.batch_NAME,
             api.invoice_id,
            (SELECT MAX (accounting_date)
               FROM APPS.xxabrl_ap_view_prepays_v apil2
              WHERE apil2.invoice_id = api.invoice_id) PAY_accounting_date,
            NULL payment_type_flag, NULL bank_account_name,
            NULL bank_account_num,NULL Bank_UTI_no, api.invoice_amount, api.amount_paid,
            trunc(APD.ACCOUNTING_DATE) gl_date,APIL.PREPAY_INVOICE_ID,(select NAME from apps.ap_terms AT,apps.ap_invoices_all ai
            where AT.term_id=AI.TERMS_ID
            and ai.invoice_id=api.invoice_id) TERM_NAME,Api.PAY_GROUP_LOOKUP_CODE PAY_GROUP,
            APIL.REFERENCE_1  PPI_Invoice_Number,
         api.ATTRIBUTE2 GRN_Number,
              Api.ATTRIBUTE3 GRN_Date,null hold_flag
       FROM apps.ap_invoices_all api,
       APPS.AP_BATCHES_ALL ABA ,
            apps.ap_invoice_lines_all apil,
            apps.ap_invoice_distributions_all apd,
            apps.ap_suppliers pov,
            apps.gl_code_combinations gl,
           -- apps.ap_payment_schedules_all aps,
            apps.hr_operating_units hr,
            apps.ap_supplier_sites_all povs,
         ( SELECT   NVL (SUM (apd.amount), 0)*-1 amt, api.invoice_id,apd.accounting_date,APIL.LINE_NUMBER,apd.DISTRIBUTION_LINE_NUMBER
                 FROM apps.ap_invoices_all api,
                      apps.ap_invoice_lines_all apil,
                      apps.ap_invoice_distributions_all apd,
                      apps.ap_suppliers pov,
                      apps.ap_supplier_sites_all povs
                WHERE api.invoice_id = apd.invoice_id
                  AND apil.invoice_id = api.invoice_id
                  AND apil.line_number = apd.invoice_line_number
                  AND api.vendor_id = pov.vendor_id
                  and api.invoice_type_lookup_code <> 'PREPAYMENT'
                  AND api.vendor_site_id = povs.vendor_site_id
                --  AND apd.match_status_flag = 'A'
                  --and api.invoice_id=90526782
                 --- and  apd.REVERSAL_FLAG='N'
                  AND apil.line_type_lookup_code = 'PREPAY'
             GROUP BY api.invoice_id,apd.accounting_date,APIL.LINE_NUMBER,apd.DISTRIBUTION_LINE_NUMBER) ZZ
      WHERE api.invoice_id = zZ.invoice_id
      AND ZZ.LINE_NUMBER=APIL.LINE_NUMBER
      and zz.DISTRIBUTION_LINE_NUMBER=apd.DISTRIBUTION_LINE_NUMBER
        AND api.invoice_id = apd.invoice_id
        AND ABA.BATCH_ID=API.BATCH_ID
        and api.org_id=hr.ORGANIZATION_ID
        and gl.CODE_COMBINATION_ID=api.ACCTS_PAY_CODE_COMBINATION_ID
        AND apil.invoice_id = api.invoice_id
        -- and aps.INVOICE_ID=api.INVOICE_ID
        AND apil.line_number = apd.invoice_line_number
        AND api.vendor_id = pov.vendor_id
      --  AND api.invoice_type_lookup_code <> 'PREPAYMENT'
      --  AND apd.match_status_flag = 'A'
       -- and nvl(aps.AMOUNT_REMAINING,0)<>0
    AND APD.REVERSAL_FLAG='N'
    AND  APD.ACCOUNTING_DATE>= TRUNC(SYSDATE) - 180
    AND (trunc(api.LAST_UPDATE_DATE) >= trunc(sysdate)-1 OR  
                 trunc(apil.LAST_UPDATE_DATE) >= trunc(sysdate)-1  OR  
                 trunc(apd.LAST_UPDATE_DATE) >= trunc(sysdate)-1 ) 
      AND APIL.PREPAY_INVOICE_ID IS NOT NULL
        AND api.vendor_site_id = povs.vendor_site_id
       -- AND pov.segment1=9602508
        )
               
   LOOP
         INSERT INTO XXABRLIPROC.XXABRL_PREPAY_ADJ_STG
                     (vendor_site_code, vendor_site_id,
                      invoice_type, invoice_num, invoice_date,
                      description, ccid, sbu, sbu_desc,
                      location_code, location_desc,
                      account_code, account_desc,
                      invoice_currency_code, dr_val, cr_val,
                      prepay_amt_remaing, payment_num,
                      check_number, check_date, vendor_num,
                      attribute_14, vendor_name, vendor_type,
                      po_distribution_id, po_header_id,
                      rcv_transaction_id, org_id,
                      operating_unit, batch_name, invoice_id,
                      pay_accounting_date, payment_type_flag,
                      bank_account_name, bank_account_num,
                      bank_uti_no, invoice_amount, amount_paid,
                      gl_date, prepay_invoice_id, term_name,
                      pay_group, ppi_invoice_number, grn_number,
                      grn_date, hold_flag, process_flag
                     )
              VALUES (cur.vendor_site_code, cur.vendor_site_id,
                      cur.invoice_type, cur.invoice_num, cur.invoice_date,
                      cur.description, cur.ccid, cur.sbu, cur.sbu_desc,
                      cur.location_code, cur.location_desc,
                      cur.account_code, cur.account_desc,
                      cur.invoice_currency_code, cur.dr_val, cur.cr_val,
                      cur.prepay_amt_remaing, cur.payment_num,
                      cur.check_number, cur.check_date, cur.vendor_num,
                      cur.attribute_14, cur.vendor_name, cur.vendor_type,
                      cur.po_distribution_id, cur.po_header_id,
                      cur.rcv_transaction_id, cur.org_id,
                      cur.operating_unit, cur.batch_name, cur.invoice_id,
                      cur.pay_accounting_date, cur.payment_type_flag,
                      cur.bank_account_name, cur.bank_account_num,
                      cur.bank_uti_no, cur.invoice_amount, cur.amount_paid,
                      cur.gl_date, cur.prepay_invoice_id, cur.term_name,
                      cur.pay_group, cur.ppi_invoice_number, cur.grn_number,
                      cur.grn_date, cur.hold_flag, 'Daily'
                     );
        COMMIT;    
      END LOOP;
   END;
END; 
/


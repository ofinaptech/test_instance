CREATE OR REPLACE PACKAGE APPS.Xxabrl_India_Creditors_pkg
AS
PROCEDURE XXABRL_INDIA_CREDITORS_PROC(ERRBUFF OUT VARCHAR2,
                                      RETCODE OUT NUMBER,
                                      P_OPERATING_UNIT   VARCHAR2,
                                      P_VENDOR_NAME      VARCHAR2,
                                      P_VENDOR_SITE      VARCHAR2,
                                      P_FROM_VENDOR      VARCHAR2,
                                      P_TO_VENDOR         VARCHAR2,
                                      P_FROM_GL_DATE     VARCHAR2,
                                      P_TO_GL_DATE       VARCHAR2,
                                      P_FROM_VOCHER      VARCHAR2,
                                      P_TO_VOCHER        VARCHAR2
                                      );
END Xxabrl_India_Creditors_pkg;
/


CREATE OR REPLACE PROCEDURE APPS.xxabrl_resa_xml_loader (
   errbuf    OUT   VARCHAR2,
   retcode   OUT   NUMBER
)
AS
   query_string        LONG;
   le_load             EXCEPTION;
   gn_error            NUMBER    := 1;
   no_of_files         NUMBER    := 0;
   g_conc_request_id   NUMBER    := fnd_profile.VALUE ('CONC_REQUEST_ID');

   CURSOR xx_get_file
   IS
      SELECT *
        FROM dir_list
       WHERE UPPER (filename) LIKE '%R%';
BEGIN
   get_dir_list
             ('/ebiz/app01/DEV2/apps/apps_st/appl/xxabrl/12.0.0/dat/RESA_AR/AR/');

   BEGIN
      SELECT COUNT (*)
        INTO no_of_files
        FROM dir_list
       WHERE UPPER (filename) LIKE '%R%';
   END;

   fnd_file.put_line (fnd_file.LOG, 'Total files :' || no_of_files);

   FOR xx_file_name IN xx_get_file
   LOOP
      fnd_file.put_line (fnd_file.LOG,
                         'File name :' || xx_file_name.filename);

      BEGIN
         query_string :=
               'INSERT INTO  xxabrl.xxabrl_resa_ar_int 
               (  S_NUM,id_type, rollup_level_1, rollup_level_2, STORE,
                set_of_books_id, currency_code,  SEGMENT1,SEGMENT2,SEGMENT3,SEGMENT4,SEGMENT5,SEGMENT6,SEGMENT7,SEGMENT8,
                amount_dr, amount_cr,
                currency_conversion_type, currency_conversion_date,
                accounting_date, code_combination_id, org_id,
                 CREATION_DATE,INTERFACE_FLAG,RECORD_NUMBER)
      WITH t AS
           (SELECT  XMLTYPE (BFILENAME (''RESA_XML_LOAD'','''
            || xx_file_name.filename
            || '''),
                            NLS_CHARSET_ID (''UTF-8'')
                           ) xmlcol
              FROM DUAL)
      SELECT XXABRL_ReSA_AR_cnv_s.nextval,
             EXTRACTVALUE (VALUE (x), ''DAYTOTAL/TOTAL_ID'') id_type,
             EXTRACTVALUE (VALUE (x), ''DAYTOTAL/ROLLUP_LEVEL_1'') rollup_level_1,
             EXTRACTVALUE (VALUE (x), ''DAYTOTAL/ROLLUP_LEVEL_2'') rollup_level_2,
             EXTRACTVALUE (VALUE (x), ''DAYTOTAL/STORE'') STORE,
             EXTRACTVALUE (VALUE (x), ''DAYTOTAL/SET_OF_BOOKS_ID'') set_of_books_id,
             EXTRACTVALUE (VALUE (x), ''DAYTOTAL/CURRENCY_CODE'') currency_code,
               EXTRACTVALUE (VALUE (x), ''DAYTOTAL/SEGMENT1'') SEGMENT1,
             EXTRACTVALUE (VALUE (x), ''DAYTOTAL/SEGMENT2'') SEGMENT2,
             EXTRACTVALUE (VALUE (x), ''DAYTOTAL/SEGMENT3'') SEGMENT3,
             EXTRACTVALUE (VALUE (x),''DAYTOTAL/SEGMENT4'') SEGMENT4,
             EXTRACTVALUE (VALUE (x), ''DAYTOTAL/SEGMENT5'') SEGMENT5,
             EXTRACTVALUE (VALUE (x), ''DAYTOTAL/SEGMENT6'') SEGMENT6,
             EXTRACTVALUE (VALUE (x), ''DAYTOTAL/SEGMENT7'') SEGMENT7,
             EXTRACTVALUE (VALUE (x), ''DAYTOTAL/SEGMENT8'') SEGMENT8,
             EXTRACTVALUE (VALUE (x), ''DAYTOTAL/DEBIT_AMOUNT'') amount_dr,
             EXTRACTVALUE (VALUE (x), ''DAYTOTAL/CREDIT_AMOUNT'') amount_cr,
             EXTRACTVALUE
                     (VALUE (x),
                      ''DAYTOTAL/CONVERSION_TYPE''
                     ) currency_conversion_type,
             to_date(EXTRACTVALUE
                     (VALUE (x),
                      ''DAYTOTAL/CONVERSION_DATE''
                     ),''YYYY-MM-DD'') currency_conversion_date,
             to_date(EXTRACTVALUE (VALUE (x), ''DAYTOTAL/ACCOUNTING_DATE''),''YYYY-MM-DD'') accounting_date,
             EXTRACTVALUE (VALUE (x),
                          ''DAYTOTAL/CODE_COMBINATION_ID''
                          ) code_combination_id
              ,
             EXTRACTVALUE (VALUE (x), ''DAYTOTAL/ORG_UNIT_ID'') org_id,sysdate,''N'',XXABRL_ReSA_AR_cnv_s.nextval
        FROM t, TABLE (XMLSEQUENCE (EXTRACT (t.xmlcol, ''/RESAData/DAYTOTAL''))) x';
         fnd_file.put_line (fnd_file.LOG, 'query_string =>' || query_string);

         EXECUTE IMMEDIATE query_string;
      EXCEPTION
         WHEN le_load
         THEN
            retcode := gn_error;
            errbuf :=
                  'Error while inserting the record into staging table :'
               || SQLCODE
               || ':'
               || SQLERRM;
      END;
--      BEGIN
--         UTL_FILE.fcopy (src_location       => 'RESA_XML_LOAD',
--                         src_filename       => xx_file_name.filename,
--                         dest_location      => 'RESA_XML_LOAD_ARCH',
--                         dest_filename      =>    xx_file_name.filename
--                                               || '_'
--                                               || g_conc_request_id,
--                         start_line         => 1,
--                         end_line           => NULL
--                        );
--         UTL_FILE.fremove ('RESA_XML_LOAD', xx_file_name.filename);
--      EXCEPTION
--         WHEN le_load
--         THEN
--            retcode := gn_error;
--            errbuf :=
--                  'Error while moving the file from one path to another :'
--               || SQLCODE
--               || ':'
--               || SQLERRM;
--      END;
   END LOOP;

   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      retcode := 2;
      errbuf := 'ERROR : ' || SQLCODE || ':' || SQLERRM;
END; 
/


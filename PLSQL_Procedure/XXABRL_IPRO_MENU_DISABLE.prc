CREATE OR REPLACE PROCEDURE APPS.xxabrl_ipro_menu_disable (
   errbuf    OUT   VARCHAR2,
   retcode   OUT   NUMBER
)
IS
   CURSOR c1
   IS
      SELECT responsibility_id, application_id, responsibility_name,
             action_id, menu_name, user_menu_name, description,
             last_update_date, rule_type, status
        FROM xxabrl.xxabrl_iproc_resp_menu_remov
       WHERE status = 'Inc';
BEGIN
   NULL;

   FOR i IN c1
   LOOP
      fnd_resp_functions_pkg.delete_row (i.application_id,
                                         i.responsibility_id,
                                         i.rule_type,
                                         i.action_id
                                        );

      UPDATE xxabrl.xxabrl_iproc_resp_menu_remov
         SET last_update_date = SYSDATE,
             status = 'Ex'
       WHERE application_id = i.application_id
         AND responsibility_id = i.responsibility_id
         AND rule_type = i.rule_type
         AND action_id = i.action_id;
   END LOOP;

   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      fnd_file.put_line (fnd_file.LOG, 'Error in Staging Tabl');
END; 
/


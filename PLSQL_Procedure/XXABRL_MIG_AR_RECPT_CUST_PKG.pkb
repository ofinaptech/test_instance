CREATE OR REPLACE PACKAGE BODY APPS.xxabrl_mig_ar_recpt_cust_pkg
IS
/**************************************************************************************************************
                 OBJECT INFORMATION

 Object Name : XXABRL_MIG_AR_RECPT_CUST_PKG

 Description : Procedure to  Validate the receipts , inserting the receipts

  Change Record:
 =============================================================================================================
 Version    Date            Author       Remarks
 =======   ==========    =============   =====================================================================
 1.0.1     26-AUG-14      Jeyaprakash     Initial Version

***************************************************************************************************************/
   PROCEDURE cust_receipt_validate (
      p_errbuf             OUT      VARCHAR2,
      p_retcode            OUT      NUMBER,
      p_action             IN       VARCHAR2,
      p_data_source        IN       VARCHAR2,
      p_receipt_to_apply   IN       VARCHAR2,
      p_apply_all_flag     IN       VARCHAR2,
      p_gl_date            IN       VARCHAR2
   )
   AS
      CURSOR arr_c1 (p_operating_unit VARCHAR2, cp_gl_date DATE)
      IS
         SELECT ROWID, data_source, receipt_number, operating_unit,
                receipt_date, deposit_date, gl_date, currency_code,
                exchange_rate_type, exchange_rate, exchange_date,
                NVL (deposited_amount, receipt_amount) receipt_amount,
                er_customer_number, er_dc_code, comments, er_tender_type,
                sales_transaction_number, of_customer_number, receipt_method,
                bank_account_name, bank_account_number, invoice_number,
                amount_applied, org_name, actual_amount, deposited_amount,
                deposited_date, opening_balance, closing_balance,
                net_change_at_store
           FROM xxabrl_ar_mig_receipt_all
          WHERE UPPER (TRIM (data_source)) = UPPER (TRIM (p_data_source))
            AND UPPER (TRIM (NVL (operating_unit, p_operating_unit))) =
                                               UPPER (TRIM (p_operating_unit))
            AND gl_date = NVL (cp_gl_date, gl_date)
            AND NVL (deposited_amount, receipt_amount) <> 0            --ADDED
            AND NVL (interfaced_flag, 'N') IN ('N', 'E')
            AND freeze_flag = 'Y'
            /*    ----- UPDATED BY RAVI AND MITUL NOT TO MOVE THE FREEZED DATA FOR KASM FROM 01-APR-2011
                AND receipt_number NOT IN (
                      SELECT  receipt_number FROM
                      APPS.xxabrl_navi_ar_receipt_stg2
                      WHERE  1=1
                      AND TRUNC(GL_DATE)>'31-MAR-2011'
                      AND operating_unit='TSRL KA SM')
                 ----- UPDATED BY RAVI AND MITUL */-- New Condition added for Resticting Reverse case of Receipts
            AND NOT EXISTS (
                   SELECT 1
                     FROM ar_cash_receipts_all acr
                    WHERE acr.receipt_number =
                                      xxabrl_ar_mig_receipt_all.receipt_number
                      AND status = 'REV'
                      AND acr.reversal_category IS NOT NULL
                      AND acr.reversal_date IS NOT NULL);

      -- Declaring Local Varibules
      v_orgid                    NUMBER        := fnd_profile.VALUE ('ORG_ID');
      v_set_of_bks_id            NUMBER
                                     := fnd_profile.VALUE ('GL_SET_OF_BKS_ID');
      v_user_id                  NUMBER       := fnd_profile.VALUE ('USER_ID');
      v_fun_curr                 VARCHAR2 (10);
      v_currency                 fnd_currencies.currency_code%TYPE;
      v_operating_unit           hr_operating_units.short_code%TYPE;
      v_bank_account_name        ce_bank_accounts.bank_account_name%TYPE;
      v_bank_account_num         ce_bank_accounts.bank_account_num%TYPE;
      v_error_count              NUMBER                                   := 0;
      v_ok_rec_count             NUMBER                                   := 0;
      v_error_message            VARCHAR2 (1000);
      v_record_count             NUMBER                                   := 0;
      v_data_count               NUMBER                                   := 0;
      v_cust_account_id          NUMBER;
      v_customer_number          hz_cust_accounts_all.account_number%TYPE;
      v_invoice_number           VARCHAR2 (20);
      v_amount_applied           NUMBER;
      v_site_use_id              NUMBER;
      v_receipt_method           ar_receipt_methods.NAME%TYPE;
      v_remit_bank_acct_use_id   VARCHAR2 (240);
      v_gl_date                  DATE;
      p_org_id                   NUMBER        := fnd_profile.VALUE ('ORG_ID');
   BEGIN
      fnd_file.put_line (fnd_file.LOG, ' gl date ' || p_gl_date);
      fnd_file.put_line (fnd_file.LOG, ' ececuting select stmt');

      BEGIN
         SELECT TO_DATE (p_gl_date, 'YYYY/MM/DD HH24:MI:SS')
           INTO v_gl_date
           FROM DUAL;

         fnd_file.put_line (fnd_file.LOG, ' executed stmt');
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (fnd_file.LOG, ' Error-> ' || SQLERRM);
      END;

--================================
-- Validation For Operating Unit
--================================
      BEGIN
         SELECT short_code
           INTO v_operating_unit
           FROM hr_operating_units
          WHERE organization_id = p_org_id;

         fnd_file.put_line (fnd_file.output,
                               'OU / Org ID '
                            || v_operating_unit
                            || ' / '
                            || p_org_id
                           );
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            fnd_file.put_line
                       (fnd_file.output,
                        'Selected Org Id does not exist in Oracle Financials'
                       );
            fnd_file.put_line
               (fnd_file.output,
                '........................................................................'
               );
         WHEN TOO_MANY_ROWS
         THEN
            fnd_file.put_line (fnd_file.output,
                               'Multiple Org Id exist in Oracle Financials'
                              );
            fnd_file.put_line
               (fnd_file.output,
                '........................................................................'
               );
         WHEN OTHERS
         THEN
            fnd_file.put_line (fnd_file.output, 'Invalid Org Id Selected ');
            fnd_file.put_line
               (fnd_file.output,
                '........................................................................'
               );
      END;

      fnd_file.put_line (fnd_file.output,
                            'Receipts/Invoice to APPLY '
                         || NVL (p_receipt_to_apply, 'NULL')
                        );

      ----
      -- Update user id in new record
      ----
      UPDATE xxabrl_ar_mig_receipt_all
         SET created_by = v_user_id
       WHERE TRIM (UPPER (data_source)) = TRIM (UPPER (p_data_source))
         AND TRIM (UPPER (operating_unit)) = TRIM (UPPER (v_operating_unit))
         AND NVL (interfaced_flag, 'N') = 'N'
         AND freeze_flag = 'Y';

        -- AND CREATED_BY IS NULL;
      ----
      -- Remove error message before validating
      ----
      UPDATE xxabrl_ar_mig_receipt_all
         SET error_message = NULL
       WHERE TRIM (UPPER (data_source)) = TRIM (UPPER (p_data_source))
         AND TRIM (UPPER (operating_unit)) = TRIM (UPPER (v_operating_unit))
         AND NVL (interfaced_flag, 'N') IN ('N', 'E')
         AND error_message IS NOT NULL
         AND freeze_flag = 'Y';

      COMMIT;
      fnd_file.put_line (fnd_file.output,
                         'Following AR Receipt Information are validating'
                        );
      fnd_file.put_line
         (fnd_file.output,
          '........................................................................'
         );

---- ============================
-- Select functional currency
----============================
      BEGIN
         SELECT currency_code
           INTO v_fun_curr
           FROM gl_sets_of_books
          WHERE set_of_books_id = v_set_of_bks_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            v_fun_curr := NULL;
      END;

      fnd_file.put_line (fnd_file.output,
                         '***--- AR Receipt Validating Starts ---***'
                        );

      FOR arr_r1 IN arr_c1 (v_operating_unit, v_gl_date)
      LOOP
         EXIT WHEN arr_c1%NOTFOUND;
         v_record_count := arr_c1%ROWCOUNT;
         v_error_message := NULL;
         v_data_count := NULL;
         v_cust_account_id := NULL;
         v_site_use_id := NULL;
         v_currency := NULL;
         v_receipt_method := NULL;
         ----
         --  Receipt Number validation
         ---
         fnd_file.put_line
                      (fnd_file.output,
                       '----------------------------------------------------'
                      );
         fnd_file.put_line (fnd_file.output,
                               '### Validating Receipt ('
                            || arr_r1.receipt_number
                            || ')'
                           );

         IF arr_r1.receipt_number IS NULL
         THEN
            v_error_message := v_error_message || 'Receipr Number is Null';
         END IF;

         ----
         --  Receipt Amount validation
         ---
         IF arr_r1.receipt_amount IS NULL
         THEN
            v_error_message := v_error_message || 'Receipr Amount is Null';
         ELSIF arr_r1.receipt_amount <= 0
         THEN
            v_error_message :=
                              v_error_message || 'Receipr Amount is negative';
         END IF;

          ----
          --  Receipt Date validation
          ---
         /* IF ARR_R1.Receipt_Date IS NULL THEN
            V_Error_Message := V_Error_Message || 'Receipr Date is Null';
          END IF;*/   -----------by JAGAN
          ----
          --  Deposite Date validation
          ---
         IF arr_r1.deposit_date IS NULL
         THEN
            v_error_message := v_error_message || 'Deposite Date is Null';
         END IF;

         ----
         --  GL Date validation, should be in AR Open Period
         ---
         IF arr_r1.gl_date IS NULL
         THEN
            v_error_message := v_error_message || 'GL Date is Null';
         ELSE
            BEGIN
               v_data_count := 0;

               SELECT COUNT (gps.period_name)
                 INTO v_data_count
                 FROM gl_period_statuses gps, fnd_application fna
                WHERE fna.application_short_name = 'AR'
                  AND fna.application_id = gps.application_id
                  AND gps.closing_status = 'O'
                  AND gps.set_of_books_id = v_set_of_bks_id
                  AND arr_r1.gl_date BETWEEN gps.start_date AND gps.end_date;

               IF v_data_count = 0
               THEN
                  v_error_message :=
                        v_error_message
                     || 'GL date is not in AR Open Period -->>';
               END IF;
            EXCEPTION
               WHEN OTHERS
               THEN
                  v_error_message :=
                           v_error_message || 'GL Period not open in AR -->>';
            END;
         END IF;

         ----
         --  Currency Code validation
         ---
         IF arr_r1.currency_code IS NULL
         THEN
            v_error_message := v_error_message || 'Currency is null --->>';
         ELSE
            BEGIN
               SELECT currency_code
                 INTO v_currency
                 FROM fnd_currencies
                WHERE UPPER (currency_code) =
                                           TRIM (UPPER (arr_r1.currency_code));

               IF     UPPER (TRIM (v_currency)) <> UPPER (TRIM (v_fun_curr))
                  AND arr_r1.exchange_date IS NULL
               THEN
                  v_error_message :=
                              v_error_message || 'Exchange Date is Null -->>';
               END IF;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  v_error_message :=
                        v_error_message || 'Currency Code Does Not Exist-->>';
               WHEN TOO_MANY_ROWS
               THEN
                  v_error_message :=
                             v_error_message || 'Multiple Currency Found-->>';
               WHEN OTHERS
               THEN
                  v_error_message :=
                                   v_error_message || 'Invalid Currency -->>';
            END;
         END IF;

         ----
         --  Customer Number validation
         ---
         IF arr_r1.er_customer_number IS NULL
         THEN
            v_error_message :=
               v_error_message || 'Feeder System Customer Number is Null-->>';
         END IF;

         IF arr_r1.er_dc_code IS NULL
         THEN
            v_error_message := v_error_message || 'DC Code is Null-->>';
         END IF;

         IF     arr_r1.er_customer_number IS NOT NULL
            AND arr_r1.er_dc_code IS NOT NULL
         THEN
            BEGIN
               v_cust_account_id := NULL;
               v_site_use_id := NULL;

               SELECT UNIQUE hca.account_number, hca.cust_account_id,
                             hcsu.site_use_id
                        INTO v_customer_number, v_cust_account_id,
                             v_site_use_id
                        FROM hz_cust_accounts_all hca,
                             hz_cust_acct_sites_all hcas,
                             hz_party_sites hps,
                             hz_cust_site_uses_all hcsu
                       --,xxabrl_ar_cust_bkacc_map_int xac
               WHERE            /*ER_DC_Code = ARR_R1.ER_DC_Code
                            And ER_Customer_Number = ARR_R1.ER_Customer_Number
                            And hca.Account_Number = xac.OF_Customer_Number
                            And */
                             hca.account_number = arr_r1.er_customer_number
                         AND hca.cust_account_id = hcas.cust_account_id
                         AND hcas.org_id = p_org_id
                         AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id(+)
                         AND hcas.party_site_id = hps.party_site_id
                         --And hps.Party_Site_Number = xac.OF_Customer_Site_Code
                         AND hcsu.site_use_code = 'BILL_TO'
                         AND hcas.bill_to_flag = 'P'
                         AND hcas.status = 'A';

               fnd_file.put_line (fnd_file.output,
                                     'Account Number '
                                  || arr_r1.er_customer_number
                                 );
               fnd_file.put_line (fnd_file.output,
                                  'Cust Acct ID ' || v_cust_account_id
                                 );
               fnd_file.put_line (fnd_file.output,
                                  'Site Use ID ' || v_site_use_id
                                 );
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  v_error_message :=
                        v_error_message
                     || 'Customer Number Or Site in not exist in this Org -->>';
               WHEN TOO_MANY_ROWS
               THEN
                  v_error_message :=
                        v_error_message
                     || 'Multiple Customer Number Or Site found in this Org -->>';
               WHEN OTHERS
               THEN
                  v_error_message :=
                        v_error_message
                     || 'Customer Number Or Site in not exist in this Org -->>';
            END;
         END IF;

         ----
         --  Duplicate Receipt Number check
             ---
         IF arr_r1.receipt_number IS NOT NULL
         THEN
            BEGIN
               v_data_count := 0;

               SELECT COUNT (receipt_number)
                 INTO v_data_count
                 FROM xxabrl_ar_mig_receipt_all
                WHERE TRIM (UPPER (p_data_source)) =
                                                  TRIM (UPPER (p_data_source))
                  AND TRIM (UPPER (operating_unit)) =
                                               TRIM (UPPER (v_operating_unit))
                  AND er_dc_code = arr_r1.er_dc_code
                  AND of_customer_number = arr_r1.er_customer_number
                  AND TRIM (receipt_number) = TRIM (arr_r1.receipt_number)
                  AND ROWID <> arr_r1.ROWID;

               IF v_data_count > 0
               THEN
                  v_error_message :=
                        v_error_message
                     || 'Duplicate Receipt Number exist in Staging table -->>';
               END IF;
            EXCEPTION
               WHEN OTHERS
               THEN
                  v_error_message :=
                            v_error_message || 'Error In Receipt Number -->>';
            END;

            BEGIN
               v_data_count := 0;

               SELECT COUNT (receipt_number)
                 INTO v_data_count
                 FROM ar_cash_receipts_all
                WHERE TRIM (receipt_number) = TRIM (arr_r1.receipt_number)
                  AND pay_from_customer = v_cust_account_id
                  AND org_id = p_org_id;

               IF v_data_count > 0
               THEN
                  v_error_message :=
                        v_error_message
                     || 'Receipt Number exist in Oracle Financials -->>';
               END IF;
            EXCEPTION
               WHEN OTHERS
               THEN
                  v_error_message :=
                            v_error_message || 'Error In Receipt Number -->>';
            END;
         END IF;

             ----
         --  Receipt Method validation
         ---
         /*
         If ARR_R1.ER_Tender_Type is null then
           V_Error_Message := V_Error_Message || 'Tender Type is null --->>';
         Else
           Begin
             V_Receipt_Method   := Null;
             V_Bank_Account_num := Null;
             Select OF_Receipt_Methods, OF_Bank_Account_Name
               Into V_Receipt_Method, V_Bank_Account_Name
               From XXABRL_AR_CUST_BKACC_MAP_INT
              Where ER_DC_Code = ARR_R1.ER_DC_CODE
                And ER_Customer_Number = ARR_R1.ER_Customer_Number
                And ER_Tender_Type = ARR_R1.ER_Tender_Type
                And OF_Organization_id = P_Org_Id;
           Exception
             When no_data_found then
               V_Error_Message := V_Error_Message ||
                                  'Tender Type Mapping does not exist -->>';
             When too_many_rows then
               V_Error_Message := V_Error_Message ||
                                  'Multiple Tender Type found -->>';
             When Others then
               V_Error_Message := V_Error_Message ||
                                  'Invalid Tender Type -->>';
           End;
         End If;
         */
         IF arr_r1.receipt_method IS NOT NULL
         THEN
            BEGIN
               SELECT NAME
                 INTO v_receipt_method
                 FROM ar_receipt_methods
                WHERE UPPER (NAME) = TRIM (UPPER (arr_r1.receipt_method));

               fnd_file.put_line (fnd_file.output,
                                  'V_Receipt_Method ' || v_receipt_method
                                 );
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  v_error_message :=
                        v_error_message
                     || 'Mapped Receipt Method '
                     || v_receipt_method
                     || ' does not exist -->>';
               WHEN TOO_MANY_ROWS
               THEN
                  v_error_message :=
                        v_error_message
                     || 'Multiple '
                     || v_receipt_method
                     || ' Receipt Method found -->>';
               WHEN OTHERS
               THEN
                  v_error_message :=
                        v_error_message
                     || 'Invalid Receipt_Method '
                     || v_receipt_method
                     || '  -->>';
            END;
         END IF;

         ----
         --  Bank Account validation
         ----
         IF v_bank_account_num IS NOT NULL
         THEN
            BEGIN
               SELECT bank_account_num
                 INTO v_bank_account_num
                 FROM ce_bank_accounts
                WHERE TRIM (UPPER (bank_account_name)) =
                                            TRIM (UPPER (v_bank_account_name));
            --And    Org_id    = P_Org_Id;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  v_error_message :=
                        v_error_message
                     || 'Bank Account Number '
                     || v_bank_account_num
                     || ' does not exist -->>';
               WHEN TOO_MANY_ROWS
               THEN
                  v_error_message :=
                        v_error_message
                     || 'Multiple '
                     || v_bank_account_num
                     || ' Bank Account Number found -->>';
               WHEN OTHERS
               THEN
                  v_error_message :=
                        v_error_message
                     || 'Invalid Bank Acount Number '
                     || v_bank_account_num
                     || ' -->>';
            END;
         END IF;

      ----
      --  Get amount to be applied
      ---
--      IF ARR_R1.Sales_Transaction_Number IS NULL THEN
--        V_Error_Message := V_Error_Message ||
--                           'Sales Transaction Number is Null -->>';
--      ELSE
--      NULL;
--      /*
--      Fnd_file.PUT_LINE(fnd_file.output,'Sales_Transaction_Number '||ARR_R1.Sales_Transaction_Number);

         --        Begin
--          V_Invoice_Number := Null;
--          Select Trx_Number
--            Into V_Invoice_Number
--            From Ra_Customer_Trx_All
--           Where Bill_To_Customer_Id = V_Cust_Account_Id
--             And Bill_To_Site_Use_Id = V_site_use_id
--             And Org_Id = P_Org_Id
--             And Interface_Header_Attribute1 =
--                 ARR_R1.Sales_Transaction_Number;
--        EXCEPTION
--          When no_data_found then
--            V_Invoice_Number := Null;
--            V_Error_Message  := V_Error_Message ||
--                                'Sales Transaction Number ' ||
--                                ARR_R1.Sales_Transaction_Number ||
--                                ' does not exist in Current Organization-->>';
--          When too_many_rows then
--            V_Invoice_Number := Null;
--            V_Error_Message  := V_Error_Message ||
--                                'Multiple Sales Transaction Number ' ||
--                                ARR_R1.Sales_Transaction_Number ||
--                                ' Found -->>';
--          When Others then
--            V_Invoice_Number := Null;
--            V_Error_Message  := V_Error_Message ||
--                                'Invalid Sales Transaction Number ' ||
--                                ARR_R1.Sales_Transaction_Number || ' -->>';
--        End;
--        If V_Invoice_Number Is Not Null Then
--          V_Amount_Applied := 0;
--          Begin
--            Select Amount_Due_Remaining
--              Into V_Amount_Applied
--              From ar_payment_schedules_all
--             Where Trx_number = V_Invoice_Number
--               And Customer_id = V_Cust_Account_Id
--               And Customer_site_use_id = V_site_use_id
--               And Invoice_Currency_code = ARR_R1.CURRENCY_CODE
--               And Org_id = P_Org_Id
--               And Class = 'INV';
--            If V_Amount_Applied = 0 Then
--              V_Error_Message := V_Error_Message ||
--                                 'Invoice O/S Amount is Zero, you can not apply this receipt-->>';
--            End If;
--            If V_Amount_Applied > ARR_R1.Receipt_Amount then
--              V_Amount_Applied := ARR_R1.Receipt_Amount;
--            End If;
--          EXCEPTION
--            When no_data_found then
--              V_Error_Message := V_Error_Message || 'Invoice Number ' ||
--                                 V_Invoice_Number ||
--                                 ' does not exist in Current Organization-->>';
--            When too_many_rows then
--              V_Error_Message := V_Error_Message || 'Multiple ' ||
--                                 V_Invoice_Number ||
--                                 ' Invoice Number Found -->>';
--            When Others then
--              V_Error_Message := V_Error_Message ||
--                                 'Invalid Invoice Number ' ||
--                                 V_Invoice_Number || ' -->>';
--          End;
--        End If;*/
--      END IF;

         -- Get Remitt to Account ID
         IF     (arr_r1.bank_account_name IS NOT NULL)
            AND (arr_r1.bank_account_number IS NOT NULL)
            AND (arr_r1.receipt_method IS NOT NULL)
         THEN
            BEGIN
               SELECT   remit_bank_acct_use_id
                   INTO v_remit_bank_acct_use_id
                   FROM ar_receipt_method_accounts_all
                  WHERE remit_bank_acct_use_id IN (
                           SELECT ba.bank_acct_use_id
                             FROM ce_bank_acct_uses_all ba,
                                  ce_bank_accounts cba,
                                  ce_bank_branches_v bb
                            WHERE bb.bank_name LIKE '%'
                              AND bb.bank_branch_name LIKE '%'
                              AND bb.bank_institution_type = 'BANK'
                              AND bb.branch_party_id = cba.bank_branch_id
                              AND cba.bank_account_id = ba.bank_account_id
                              AND cba.account_classification = 'INTERNAL'
                              AND UPPER (cba.bank_account_name) =
                                              UPPER (arr_r1.bank_account_name)
                              --'MH HDFC BANK - 1234' ---added be Naresh on 27-may-10
                              AND cba.bank_account_num =
                                                    arr_r1.bank_account_number
                                                                              -- '45678901234'
                        )
                    AND receipt_method_id =
                           (SELECT receipt_method_id
                              FROM ar_receipt_methods
                             WHERE UPPER (NAME) =
                                                 UPPER (arr_r1.receipt_method))
                                                 and org_id=p_org_id
               ORDER BY remit_bank_acct_use_id;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  v_error_message :=
                        v_error_message
                     || 'Bank_Account Name/Number  '
                     || arr_r1.bank_account_name
                     || ' / '
                     || arr_r1.bank_account_number
                     || ' does not exist in Current Organization-->>';
               WHEN TOO_MANY_ROWS
               THEN
                  v_error_message :=
                        v_error_message
                     || 'Bank_Account Name/Number  '
                     || arr_r1.bank_account_name
                     || ' / '
                     || arr_r1.bank_account_number
                     || ' Multiple declaration-->>';
               WHEN OTHERS
               THEN
                  v_error_message :=
                        v_error_message
                     || 'Exception in Bank_Account Name/Number  '
                     || arr_r1.bank_account_name
                     || ' / '
                     || arr_r1.bank_account_number;
            END;
         ELSE
            v_error_message :=
                  v_error_message
               || 'Receipt_Method/Bank_Account_Name/Acct_Number is NULL';
         END IF;

         IF v_error_message IS NOT NULL
         THEN
            ----
            --  Update error message
            ---
            UPDATE xxabrl_ar_mig_receipt_all
               SET error_message = v_error_message,
                   interfaced_flag = 'E'
             WHERE ROWID = arr_r1.ROWID;

            COMMIT;
            v_error_count := v_error_count + 1;
            fnd_file.put_line (fnd_file.output, v_record_count || '--');
            fnd_file.put_line (fnd_file.output,
                                  arr_r1.receipt_number
                               || '-->'
                               || '-->'
                               || v_error_message
                              );
         ELSE
            ----
            --  Record is valid, update additional required info
            ---
            UPDATE xxabrl_ar_mig_receipt_all
               SET of_customer_number = TRIM (v_customer_number),
                   customer_id = TRIM (v_cust_account_id),
                   customer_site_id = TRIM (v_site_use_id),
                   receipt_method =
                                 TRIM (NVL (v_receipt_method, receipt_method)),
                   bank_account_number =
                                 NVL (v_bank_account_num, bank_account_number),
                   --  Invoice_Number      = V_Invoice_Number,
                   --  Amount_Applied      = V_Amount_Applied,
                   org_id = p_org_id,
                   remit_bank_acc_id = v_remit_bank_acct_use_id,
                   interfaced_flag = 'V'
             WHERE ROWID = arr_r1.ROWID;

            COMMIT;
            v_ok_rec_count := v_ok_rec_count + 1;
            fnd_file.put_line (fnd_file.output, v_record_count || '--');
            fnd_file.put_line (fnd_file.output,
                                  arr_r1.receipt_number
                               || '-->'
                               || '-->Data without error'
                              );
         END IF;
      END LOOP;

      IF v_error_count > 0
      THEN
         p_retcode := 1;
      END IF;

      fnd_file.put_line
         (fnd_file.output,
          '........................................................................'
         );
      fnd_file.put_line (fnd_file.output,
                         'Number of Records with error :' || v_error_count
                        );
      fnd_file.put_line (fnd_file.output,
                         'Number of Valid Records :' || v_ok_rec_count
                        );
      fnd_file.put_line
         (fnd_file.output,
          '........................................................................'
         );

      ----
      -- Calling Receipt Insert prog
      ----
      IF p_action = 'N' AND v_error_count = 0
      THEN
         --Create to Receipt

         -- v_gl_date ADDED BY SHAILESH ON 12 FEB 2009 FOR GL_DATE PARAMETER
         cust_receipt_insert (p_org_id, p_data_source, v_gl_date);
      --######## TEST Code
      --######## TEST Code
      END IF;
   /*IF p_apply_all_flag = 'Y' THEN

       FOR v_Cur_Apply IN (SELECT DISTINCT sales_transaction_number FROM
                            XXABRL_AR_MIG_RECEIPT_ALL
                           WHERE
                           interfaced_flag in('C','EA')
                           --23-03-11 aadded conditoin to pick up receipts per application which are with interfaced_flag  'EA'
                           AND GL_DATE=NVL(v_gl_date,GL_DATE) -- ADDED BY SHAILESH ON 12 FEB 2009 FOR GL DATE PARAMETER
                           AND org_id = Fnd_Profile.VALUE('ORG_ID'))
       LOOP
       -- ,V_GL_DATE ADDED BY SHAILESH ON 12 FEB 2009 FOR GL_DATE
           CUST_RECEIPT_APPLY_CALL(P_Ref_Number => v_Cur_Apply.sales_transaction_number,P_GL_DATE=>V_GL_DATE);
       END LOOP;

    ELS */

   /* IF p_apply_all_flag = 'N' THEN

       IF p_receipt_to_apply IS NOT NULL THEN
        -- ,V_GL_DATE ADDED BY SHAILESH ON 12 FEB 2009 FOR GL_DATE
          CUST_RECEIPT_APPLY_CALL(P_Ref_Number => p_receipt_to_apply,P_GL_DATE=>V_GL_DATE);
        null;
       END IF;  */

   --  END IF;
   END cust_receipt_validate;

--  PROCEDURE CUST_RECEIPT_APPLY_DATA(
--                               ARR_R2 IN  XXABRL_AR_MIG_RECEIPT_ALL%ROWTYPE
--                               ,p_Rcpt_Amt_To_Apply NUMBER
--                               ,x_Rcpt_Bal_Amt OUT NUMBER
--                               ,x_exit_flag OUT VARCHAR2
--                               )
--  AS

   --    CURSOR Cur_Open_Inv(p_Interface_Header_Attribute1 VARCHAR2) IS
--SELECT *
--  FROM
--  (
--  SELECT
--    AMOUNT_DUE_ORIGINAL,
--    AMOUNT_DUE_REMAINING,
--    rcta.TRX_NUMBER,
--    rcta.TRX_DATE
--    FROM
--    Ra_Customer_Trx_ALL rcta
--    ,Ar_Payment_Schedules_All apsa
--    WHERE rcta.customer_trx_id = apsa.customer_trx_id
--    AND apsa.AMOUNT_DUE_REMAINING    <>0
--    AND rcta.Interface_Header_Attribute1 = p_Interface_Header_Attribute1
--    ORDER BY AMOUNT_DUE_ORIGINAL
--    )
--    WHERE ROWNUM = 1;
--/*    select
--    AMOUNT_DUE_ORIGINAL,
--    AMOUNT_DUE_REMAINING
--    from
--    Ra_Customer_Trx_All rcta
--    ,Ar_Payment_Schedules_All apsa
--    where rcta.customer_trx_id = apsa.customer_trx_id
--    and apsa.AMOUNT_DUE_REMAINING    <>0
--    and rcta.Interface_Header_Attribute1 = p_Interface_Header_Attribute1
--    and rownum = 1
--    order by AMOUNT_DUE_ORIGINAL
--    ;*/

   --    l_return_status   VARCHAR2(1);
--    l_msg_count       NUMBER;
--    l_msg_data        VARCHAR2(240);
--    l_count           NUMBER;
--    l_cash_receipt_id NUMBER;
--    l_msg_data_out    VARCHAR2(2000);
--    l_mesg            VARCHAR2(240);
--    p_count           NUMBER;
--    l_msg_index_out   NUMBER := 0;
--    V_Ok_Receipt      NUMBER := 0;
--    V_Error_Receipt   NUMBER := 0;
--    v_Rcpt_Amt_To_Apply NUMBER :=0;
--    v_Open_Inv NUMBER :=0;
--  BEGIN

   --  BEGIN
--    SELECT
--      COUNT(1)
--      INTO v_Open_Inv
--            FROM
--      RA_CUSTOMER_TRX_ALL/*_GT*/ rcta
--      ,Ar_Payment_Schedules_All apsa
--      WHERE rcta.customer_trx_id = apsa.customer_trx_id
--      AND apsa.AMOUNT_DUE_REMAINING    <>0
--      AND rcta.Interface_Header_Attribute1 = ARR_R2.sales_transaction_number;

   --      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'Opn.Inv.Cnt: '||NVL(v_Open_Inv,0));

   --      IF  v_Open_Inv = 0 THEN
--          x_Rcpt_Bal_Amt := p_Rcpt_Amt_To_Apply;
--          x_exit_flag :='Z'; --Force Exit
--      END IF;

   --  EXCEPTION
--  WHEN OTHERS THEN
--  FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'Error while deriving Open Invoice Count ');
--  END;

   --  FOR open_inv IN Cur_Open_Inv(ARR_R2.sales_transaction_number)
--  LOOP

   --      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'Applying Inv# '||open_inv.TRX_NUMBER);

   --      IF open_inv.AMOUNT_DUE_REMAINING < 0 THEN
--      -- Credit Note Invoice -ve (i.e. Amount -100)
--         v_Rcpt_Amt_To_Apply := open_inv.AMOUNT_DUE_ORIGINAL;

   --      ELSIF  open_inv.AMOUNT_DUE_REMAINING >= p_Rcpt_Amt_To_Apply THEN
--      -- Invoice >= Receipt Amt same
--         v_Rcpt_Amt_To_Apply := p_Rcpt_Amt_To_Apply;

   --      ELSIF  (open_inv.AMOUNT_DUE_REMAINING < p_Rcpt_Amt_To_Apply) AND (open_inv.AMOUNT_DUE_REMAINING>0) THEN
--      -- Invoice < Receipt Amt same
--         v_Rcpt_Amt_To_Apply := open_inv.AMOUNT_DUE_REMAINING;

   --      END IF;

   --      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,':Rcpt_Amt_Avail '||p_Rcpt_Amt_To_Apply);
--      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,':Inv_Amt_To_Apply '||v_Rcpt_Amt_To_Apply);

   --      ar_receipt_api_pub.APPLY (
--                p_api_version            => 1.0,
--                p_init_msg_list          => FND_API.G_TRUE,
--                p_commit                 => fnd_api.g_false,
--                p_validation_level       => fnd_api.g_valid_level_full,
--                x_return_status          => l_return_status,
--                x_msg_count              => l_msg_count,
--                x_msg_data               => l_msg_data,
--                ----
--                p_trx_number             => open_inv.TRX_NUMBER,
--                p_receipt_number         => ARR_R2.Receipt_Number,
--                p_amount_applied         => v_Rcpt_Amt_To_Apply, --<<<<<<<<
----                p_amount_applied_from    => ARR_R2.RECEIPT_AMOUNT,
--                p_apply_date             => ARR_R2.deposited_date --open_inv.TRX_DATE
--                /*,
--                p_gl_date                => ARR_R2.Gl_Date*/
--          );

   --          IF l_return_status <> FND_API.G_RET_STS_SUCCESS THEN
--            FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
--                              '***Error in Receipt Apply #: ' || ARR_R2.RECEIPT_NUMBER);
--            IF l_msg_count >= 1 THEN
--              l_msg_data_out := l_msg_data_out || ' ~ ' || l_msg_data;
--              FOR i IN 0 .. l_msg_count LOOP
--                FND_MSG_PUB.GET(p_msg_index     => i,
--                                p_encoded       => 'F',
--                                p_data          => l_msg_data,
--                                p_msg_index_out => l_msg_index_out);
--                l_msg_data_out := l_msg_data_out || ' ~ ' || l_msg_data;
--              END LOOP;
--            END IF;
--            V_Error_Receipt := V_Error_Receipt + 1;
--            -----
--            -- Update error message
--            -----
--            UPDATE  XXABRL_AR_MIG_RECEIPT_ALL
--               SET INTERFACED_FLAG = 'EA', ERROR_MESSAGE = l_msg_data_out    --EA: Error in Apply
--             WHERE Receipt_Number = ARR_R2.Receipt_Number;
--            COMMIT;
--            FND_FILE.PUT_LINE(FND_FILE.OUTPUT, '***API Error: '||l_msg_data_out);

   --          ELSE
--            -----
--            -- Update interfaced flag
--            -----
--            UPDATE  XXABRL_AR_MIG_RECEIPT_ALL
--               SET INTERFACED_FLAG = 'A'     --Successfully APPLIED
--             WHERE Receipt_Number = ARR_R2.Receipt_Number;
--            V_OK_Receipt := V_OK_Receipt + 1;
--            FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
--                              '##Receipt(Inv) Applied Successfully : ' || ARR_R2.RECEIPT_NUMBER||'('||open_inv.TRX_NUMBER||')');

   --            COMMIT;
--          END IF;

   --          x_exit_flag :='N'; --Default

   --          IF open_inv.AMOUNT_DUE_REMAINING < 0 THEN
--          -- Credit Note Invoice -ve (i.e. Amount -100)
--             IF l_return_status = FND_API.G_RET_STS_SUCCESS THEN
--                  x_Rcpt_Bal_Amt := ABS(open_inv.AMOUNT_DUE_ORIGINAL) + p_Rcpt_Amt_To_Apply;
--                  x_exit_flag :='N'; --Force Exit
--             ELSE
--                  x_Rcpt_Bal_Amt := 0; --abs(open_inv.AMOUNT_DUE_ORIGINAL) + p_Rcpt_Amt_To_Apply;
--                  x_exit_flag :='Y'; --Force Exit
--             END IF;

   --          ELSIF  open_inv.AMOUNT_DUE_REMAINING >= p_Rcpt_Amt_To_Apply THEN
--          -- Invoice >= Receipt Amt same
--             x_Rcpt_Bal_Amt := 0;
--             x_exit_flag :='N'; --Force Exit

   --          ELSIF  (open_inv.AMOUNT_DUE_REMAINING < p_Rcpt_Amt_To_Apply) AND (open_inv.AMOUNT_DUE_REMAINING>0) THEN
--          -- Invoice < Receipt Amt same
--             x_Rcpt_Bal_Amt := p_Rcpt_Amt_To_Apply - open_inv.AMOUNT_DUE_REMAINING;
--             x_exit_flag :='N'; --Force Exit
--          END IF;

   --          FND_FILE.PUT_LINE(FND_FILE.OUTPUT,':Rcpt_Amt_Balance '||x_Rcpt_Bal_Amt);

   --      END LOOP;
--  END CUST_RECEIPT_APPLY_DATA;

   --  PROCEDURE CUST_RECEIPT_APPLY_CALL(P_Ref_Number IN VARCHAR2,P_GL_DATE IN DATE)
--  AS

   --  CURSOR Cur_Rcpt(CP_GL_DATE DATE) IS
--  SELECT * FROM
--  XXABRL_AR_MIG_RECEIPT_ALL
--  WHERE
--  interfaced_flag in('C','EA')
--  --23-03-11 aadded conditoin to pick up receipts per application which are with interfaced_flag  'EA'
--  AND GL_DATE = NVL(CP_GL_DATE,GL_DATE)
--  AND sales_transaction_number =P_Ref_Number
--  ORDER BY nvl(deposited_amount,receipt_amount) DESC
--  ; -- Create Cash done

   --  v_Rcpt_Amt_To_Apply NUMBER :=0;
--  v_Rcpt_Bal_Amt NUMBER :=0;
--  v_exit_flag VARCHAR2(2) :='N';
--  BEGIN

   --  FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'::::::::::::::::::::::::::::::::::::::');
--  FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'Sales Transaction Number(i.e. Inv/Recpt Common #): '||P_Ref_Number);

   --  FOR c_Cur_Rcpt IN Cur_Rcpt(P_GL_DATE)
--  LOOP

   --      v_Rcpt_Amt_To_Apply := nvl(c_Cur_Rcpt.deposited_amount,c_Cur_Rcpt.receipt_amount); ---receipt_amount;

   --      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'1. Receipt# (Amount) '||c_Cur_Rcpt.Receipt_Number||' ('||c_Cur_Rcpt.receipt_amount||')');

   --      LOOP

   --          --Rollback here, Get Status Flag per receipt / Commit etc
--        --  CUST_RECEIPT_APPLY_DATA(c_Cur_Rcpt,v_Rcpt_Amt_To_Apply,v_Rcpt_Bal_Amt,v_exit_flag);
--          v_Rcpt_Amt_To_Apply :=v_Rcpt_Bal_Amt;

   --          IF v_exit_flag = 'Z' THEN
--             FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'Opn.Inv.Cnt# Zero');
--          END IF;

   --          EXIT WHEN (v_Rcpt_Bal_Amt <=0 OR v_exit_flag IN('Y','Z'));
--          --Exit when 1=1;

   --      END LOOP;

   --      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'........................................');

   --  END LOOP;

   --  END CUST_RECEIPT_APPLY_CALL;

   -- P_GL_DATE ADDED BY SHAILESH ON 12 FEB 2009 FOR GL_DATE PARAMETER
   PROCEDURE cust_receipt_insert (
      p_org_id        IN   NUMBER,
      p_data_source   IN   VARCHAR2,
      p_gl_date       IN   DATE
   )
   AS
      --
      -- Declaring cursor for selecting valid receipt transaction
      --
       -- CP_GL_DATE ADDED BY SHAILESH ON 12 FEB 2009 FOR GL_DATE PARAMETER
      CURSOR arr_c2 (cp_gl_date DATE)
      IS
         SELECT ROWID, data_source, receipt_number, operating_unit,
                receipt_date, deposit_date, gl_date, currency_code,
                exchange_rate_type, exchange_rate, exchange_date,
                NVL (deposited_amount, receipt_amount) receipt_amount,
                er_customer_number, er_dc_code, comments, er_tender_type,
                sales_transaction_number, of_customer_number, receipt_method,
                bank_account_name, bank_account_number, invoice_number,
                customer_id, customer_site_id, amount_applied,
                remit_bank_acc_id, actual_amount, deposited_amount,
                deposited_date, opening_balance, closing_balance,
                net_change_at_store, old_doc_no, old_gl_date
           FROM xxabrl_ar_mig_receipt_all
          WHERE UPPER (TRIM (data_source)) = UPPER (TRIM (p_data_source))
            AND org_id = p_org_id
            AND NVL (deposited_amount, receipt_amount) <> 0
            -- CP_GL_DATE ADDED BY SHAILESH ON 12 FEB 2009 FOR GL_DATE PARAMETER
            AND gl_date = NVL (cp_gl_date, gl_date)
            AND NVL (interfaced_flag, 'N') IN ('V', 'EC')
            AND freeze_flag = 'Y';

         ----- UPDATED BY RAVI AND MITUL NOT TO MOVE THE FREEZED DATA FOR KASM FROM 01-APR-2011
/*        AND receipt_number NOT IN (
               SELECT  receipt_number FROM
               APPS.xxabrl_navi_ar_receipt_stg2
               WHERE  1=1
               AND TRUNC(GL_DATE)>'31-MAR-2011'
               AND operating_unit='TSRL KA SM');  */
          ----- UPDATED BY RAVI AND MITUL
      v_record_count        NUMBER                                       := 0;
      v_attribute_rec       ar_receipt_api_pub.attribute_rec_type;
      v_attribute_rec_g     ar_receipt_api_pub.global_attribute_rec_type;
      l_return_status       VARCHAR2 (1);
      l_msg_count           NUMBER;
      l_msg_data            VARCHAR2 (240);
      l_count               NUMBER;
      l_cash_receipt_id     NUMBER;
      l_msg_data_out        VARCHAR2 (2000);
      l_mesg                VARCHAR2 (240);
      p_count               NUMBER;
      l_msg_index_out       NUMBER                                       := 0;
      v_ok_receipt          NUMBER                                       := 0;
      v_error_receipt       NUMBER                                       := 0;
      l_receipt_number      NUMBER;
      x_receipt_method_id   NUMBER;
   BEGIN
      BEGIN
         mo_global.set_policy_context ('S', p_org_id);
        -- arp_global.init_global(84);
      --   arp_standard.init_standard(84);
      END;

--      begin
--    fnd_global.APPS_INITIALIZE(user_id=>0,
--                           resp_id=>62181,
--                           resp_appl_id=>222);
--      mo_global.init('AR');
--      end;

      -- l_receipt_number := ar_cash_receipts_s.NEXTVAL;

      -- P_GL_DATE ADDED BY SHAILESH ON 12 FEB 2009 FOR GL_DATE PARAMETER
      FOR arr_r2 IN arr_c2 (p_gl_date)
      LOOP
         EXIT WHEN arr_c2%NOTFOUND;
         v_record_count := arr_c2%ROWCOUNT;
         l_msg_data_out := NULL;

         BEGIN
            SELECT receipt_method_id
              INTO x_receipt_method_id
              FROM apps.ar_receipt_methods s
             WHERE UPPER (NAME) = UPPER (arr_r2.receipt_method);
         END;

--=======================
--  Declaring Record Varibule for Attributes
--==========================
         v_attribute_rec.attribute_category := NULL;
         v_attribute_rec.attribute1 := arr_r2.actual_amount;
         v_attribute_rec.attribute2 := arr_r2.deposited_amount;
         v_attribute_rec.attribute3 := arr_r2.deposited_date;
         v_attribute_rec.attribute4 := arr_r2.opening_balance;
         v_attribute_rec.attribute5 := arr_r2.closing_balance;
         v_attribute_rec.attribute6 := arr_r2.net_change_at_store;
         v_attribute_rec.attribute7 := arr_r2.old_doc_no;
         v_attribute_rec.attribute8 := arr_r2.old_gl_date;
         v_attribute_rec.attribute9 := NULL;
         v_attribute_rec.attribute10 := NULL;
         v_attribute_rec.attribute11 := NULL;
         v_attribute_rec.attribute12 := NULL;
         v_attribute_rec.attribute13 := NULL;
         v_attribute_rec.attribute14 := NULL;
         v_attribute_rec.attribute15 := NULL;
         -- Receipt API to create and Apply recipt
         ----
         fnd_file.put_line (fnd_file.output, 'P_Org_Id : ' || p_org_id);
         fnd_file.put_line (fnd_file.output,
                               'p_currency_code               =>: '
                            || arr_r2.currency_code
                           );
         fnd_file.put_line (fnd_file.output,
                               'p_amount                      =>: '
                            || arr_r2.receipt_amount
                           );
         fnd_file.put_line (fnd_file.output,
                               'p_receipt_number              =>: '
                            || arr_r2.receipt_number
                           );
         --  FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'p_receipt_number              =>: ' || l_receipt_number);
         fnd_file.put_line (fnd_file.output,
                               'p_receipt_date                =>: '
                            || arr_r2.deposited_date
                           );
         fnd_file.put_line (fnd_file.output,
                               'p_gl_date                     =>: '
                            || arr_r2.gl_date
                           );
         fnd_file.put_line (fnd_file.output,
                               'p_customer_id                 =>: '
                            || arr_r2.customer_id
                           );
         fnd_file.put_line (fnd_file.output,
                               'p_customer_site_use_id        =>: '
                            || arr_r2.customer_site_id
                           );
         fnd_file.put_line (fnd_file.output,
                               'p_deposit_date                =>: '
                            || arr_r2.deposit_date
                           );
         fnd_file.put_line (fnd_file.output,
                               'p_receipt_method_name         =>: '
                            || arr_r2.receipt_method
                           );
         fnd_file.put_line (fnd_file.output,
                               'p_remittance_bank_account_num =>: '
                            || arr_r2.bank_account_number
                           );
         fnd_file.put_line (fnd_file.output,
                               'p_receipt_comments            =>: '
                            || arr_r2.comments
                           );
         fnd_file.put_line (fnd_file.output,
                               'p_trx_number                  =>: '
                            || arr_r2.invoice_number
                           );
         fnd_file.put_line (fnd_file.output,
                               'p_amount_applied              =>: '
                            || arr_r2.amount_applied
                           );
         fnd_file.put_line (fnd_file.output,
                               'p_apply_date                  =>: '
                            || arr_r2.gl_date
                           );
         fnd_file.put_line (fnd_file.output,
                               'p_apply_gl_date               =>: '
                            || arr_r2.gl_date
                           );
         fnd_file.put_line (fnd_file.output,
                               'p_cr_id                       =>: '
                            || l_cash_receipt_id
                           );
         fnd_file.put_line (fnd_file.output,
                            'p_org_id                      =>: ' || p_org_id
                           );
         fnd_file.put_line (fnd_file.output,
                               'REMIT_BANK_ACC_ID                     =>: '
                            || arr_r2.remit_bank_acc_id
                           );
         ar_receipt_api_pub.create_cash
                    (p_api_version                     => 1.0,
                     p_init_msg_list                   => fnd_api.g_true,
                     p_commit                          => fnd_api.g_false,
                     p_validation_level                => fnd_api.g_valid_level_full,
                     x_return_status                   => l_return_status,
                     x_msg_count                       => l_msg_count,
                     x_msg_data                        => l_msg_data,
                     p_currency_code                   => arr_r2.currency_code,
                     p_amount                          => arr_r2.receipt_amount,
                     p_receipt_number                  => arr_r2.receipt_number,
                     p_receipt_date                    =>   arr_r2.deposited_date
                                                          - 1,
                     p_cr_id                           => l_cash_receipt_id
                                                                           --OUT
         ,
                    -- p_receipt_method_name             => arr_r2.receipt_method,
                     p_receipt_method_id               => x_receipt_method_id,
                     p_customer_id                     => arr_r2.customer_id
                                                                            --,p_customer_number              => ARR_R2.OF_CUSTOMER_NUMBER --Customer_Id
         ,
                     p_comments                        =>    arr_r2.comments
                                                          || '::'
                                                          || arr_r2.receipt_number
                                                          || '::'
                                                          || arr_r2.old_doc_no
                                                          || '::'
                                                          || arr_r2.old_gl_date
--        ,p_customer_receipt_reference   => SUBSTR(p_rhp_receipt.sender_to_receiver_info,1,30)
         ,
                     p_remittance_bank_account_id      => arr_r2.remit_bank_acc_id,
                     p_attribute_rec                   => v_attribute_rec,
                     p_global_attribute_rec            => v_attribute_rec_g
                    );
         fnd_file.put_line (fnd_file.output,
                               'p_cr_id                       =>: '
                            || l_cash_receipt_id
                           );

         IF l_return_status <> fnd_api.g_ret_sts_success
         THEN
            fnd_file.put_line (fnd_file.output,
                                  'Receipt Number : '
                               || arr_r2.receipt_number
                               || ' Error while creating receipt.'
                              );

            IF l_msg_count >= 1
            THEN
               l_msg_data_out := l_msg_data_out || ' ~ ' || l_msg_data;

               FOR i IN 0 .. l_msg_count
               LOOP
                  fnd_msg_pub.get (p_msg_index          => i,
                                   p_encoded            => 'F',
                                   p_data               => l_msg_data,
                                   p_msg_index_out      => l_msg_index_out
                                  );
                  l_msg_data_out := l_msg_data_out || ' ~ ' || l_msg_data;
               END LOOP;
            END IF;

            v_error_receipt := v_error_receipt + 1;

            -----
            -- Update error message
            -----
            UPDATE xxabrl_ar_mig_receipt_all
               SET interfaced_flag = 'EC',
                   error_message = l_msg_data_out     -- EC: Error in Creation
             WHERE ROWID = arr_r2.ROWID;

            COMMIT;
            fnd_file.put_line (fnd_file.output, l_msg_data_out);
            fnd_file.put_line (fnd_file.output,
                               '-----------------------------------------'
                              );
         ELSE
            -----
            -- Update interfaced flag
            -----
            UPDATE xxabrl_ar_mig_receipt_all
               SET interfaced_flag = 'C',
                   error_message = NULL
             WHERE ROWID = arr_r2.ROWID;

            v_ok_receipt := v_ok_receipt + 1;
            fnd_file.put_line (fnd_file.output,
                                  'Receipt Number : '
                               || arr_r2.receipt_number
                               || '  created successfully.'
                              );
            fnd_file.put_line (fnd_file.output,
                               '-----------------------------------------'
                              );
            COMMIT;
         END IF;
      END LOOP;

      fnd_file.put_line (fnd_file.output, 'Receipt Created :' || v_ok_receipt);
      fnd_file.put_line (fnd_file.output,
                         'Error Receipt d :' || v_error_receipt
                        );
      fnd_file.put_line (fnd_file.output,
                         '-----------------------------------------'
                        );
   END cust_receipt_insert;
END xxabrl_mig_ar_recpt_cust_pkg; 
/


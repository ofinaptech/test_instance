CREATE OR REPLACE PACKAGE APPS.XXABRL_VENDOR_MIGR_V2_PKG AS
  PROCEDURE MAIN(Errbuf   OUT VARCHAR2,
                 RetCode  OUT NUMBER,
                 p_action varchar2);
                 
  PROCEDURE XXABRL_INSERT_VENDOR_MIGR_INFO;
  
  PROCEDURE VALIDATE_VENDOR_INFO(RetCode OUT NUMBER);
  
  PROCEDURE validate_terms_name(p_terms_name IN VARCHAR2,
                                v_error_smsg OUT varchar2);
                                
  PROCEDURE validate_vendor_number(p_Vendor_Number IN VARCHAR2,
                                   v_error_hmsg    OUT varchar2);
                                   
  PROCEDURE validate_vendor_name(p_Vendor_Name IN VARCHAR2,
                                 v_error_hmsg  OUT varchar2);
                                 
  PROCEDURE validate_vendor_site_code(p_vendor_site_code IN VARCHAR2,
                                      p_vendor_name      IN VARCHAR2,
                                      P_OPERATING_UNIT   IN VARCHAR2,
                                      v_error_smsg       OUT varchar2);
                                      
  PROCEDURE validate_vendor_type(p_vendor_type_lookup_code IN VARCHAR2,
                                 v_error_hmsg              OUT varchar2);
                                 
  PROCEDURE validate_operating_unit(P_OPERATING_UNIT IN VARCHAR2,
                                    v_error_smsg     OUT varchar2);
                                    
  PROCEDURE validate_country_code(P_COUNTRY_CODE IN VARCHAR2,
                                  v_error_smsg   OUT varchar2);
                                  
  PROCEDURE validate_payment_method(P_PAY_METHOD_CODE IN VARCHAR2,
                                    v_error_smsg      OUT varchar2);
                                    
  PROCEDURE validate_pay_group(P_PAY_GROUP  IN VARCHAR2,
                               x_pay_group  out varchar2,
                               v_error_smsg OUT varchar2);
                               
  PROCEDURE DERIVE_SHIT_TO_LOCATION_ID(P_SHIP_TO_LOC_CODE IN varchar2,
                                       P_SHIP_TO_LOC_ID   OUT NUMBER,
                                       v_error_smsg       OUT varchar2);
                                       
  PROCEDURE DERIVE_EMPLOYEE_ID(P_VENDOR_NUMBER IN varchar2,
                               P_EMPLOYEE_ID   OUT NUMBER,
                               v_error_hmsg    OUT varchar2);
                               
  PROCEDURE DERIVE_BILL_TO_LOCATION_ID(P_BILL_TO_LOC_CODE IN varchar2,
                                       P_BILL_TO_LOC_ID   OUT NUMBER,
                                       v_error_smsg       OUT varchar2);
                                       
  PROCEDURE DERIVE_TERM_ID(P_TERM_NAME  IN varchar2,
                           P_TERM_ID    OUT NUMBER,
                           v_error_smsg OUT varchar2);
                           
END XXABRL_VENDOR_MIGR_V2_PKG;
/


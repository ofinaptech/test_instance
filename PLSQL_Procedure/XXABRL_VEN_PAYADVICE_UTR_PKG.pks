CREATE OR REPLACE PACKAGE APPS.xxabrl_ven_payadvice_utr_pkg
IS
   PROCEDURE xxabrl_ven_main_utr_pkg (errbuf OUT VARCHAR2, retcode OUT NUMBER);

   PROCEDURE xxabrl_ven_payadvice_utr_prc;
END; 
/


CREATE OR REPLACE PROCEDURE APPS.xxabrl_yesbank_outbound_utl (
   errbuf    OUT   VARCHAR2,
   retcode   OUT   VARCHAR2
)
AS
   /**********************************************************************************************************************************************
                          WIPRO Infotech Ltd, Mumbai, India

              Name: Supplier Bank Outbound interface for YES Bank generating file on FTP [H2H Interface betwen ABRL and YES Bank]

              Change Record:
             =========================================================================================================================
             Version   Date          Author                    Remarks                  Documnet Ref
             =======   ==========   =============             ==================  =====================================================
             1.0.0     24-Apr-2013   Dhiresh More      Initial Version

             1.0.1     29-May-2013   Dhiresh More      chenge query to generate no of files as many account are available
    ************************************************************************************************************************************************/

   --Declaring Payment Detail Local Variables
   lv_abrl_count         NUMBER             := 0;        --initializing  to 0
   lv_tot_abrl_amt       NUMBER             := 0;         --initializing to 0
   lv_tsrl_count         NUMBER             := 0;        --initializing  to 0
   lv_tot_tsrl_amt       NUMBER             := 0;         --initializing to 0
   l_file_p              UTL_FILE.file_type;
   l_file_p1             UTL_FILE.file_type;
   l_pmt_data            VARCHAR2 (4000);
   l_pmt_data1           VARCHAR2 (4000);
   v_yesbank_abrl_seq    VARCHAR2 (50);
   v_yesbank_abrl_seq1   VARCHAR2 (50);
   v_yesbank_tsrl_seq    VARCHAR (50);
   v_yesbank_seq         VARCHAR (50);
   chk_pt                NUMBER             := 0;              -- for testing
--Declaring Advice Detail Local Variables
   lv_abrl_adv_count     NUMBER             := 0; --initializing counter to 0
   lv_tsrl_adv_count     NUMBER             := 0; --initializing counter to 0
   l_file_a              UTL_FILE.file_type;
   l_adv_data            VARCHAR2 (4000);

-- check total account numbers available for H2H payment in Yes Bank
   CURSOR cur_yb_acc_chk
   IS
      SELECT DISTINCT corporate_account_number,
                      DECODE (corporate_account_number,
                              '000181400008423', 'TSRL'
                               || SUBSTR (corporate_account_number, 12, 4),
                                 'ABRL'
                              || SUBSTR (corporate_account_number, 12, 4)
                             ) account_name,
                      DECODE (corporate_account_number,
                              '000181400008423', 'TSRL',
                              'ABRL'
                             ) SOURCE
                 FROM apps.yesbank_payment_table
                WHERE transaction_status = 'AUTHORIZED'
                  AND TRUNC (creation_date) >= TRUNC (SYSDATE) - 5;

   -- this will also pick up transactions which were created 2 days prior

   -- Declaring Payment Cursor to pick up only  ABRL Authorized Payments
   CURSOR cur_abrl_yb_pay (p_account_num VARCHAR2)
   IS
      SELECT   *
          FROM apps.yesbank_payment_table
         WHERE transaction_status = 'AUTHORIZED'
           AND TRUNC (creation_date) >= TRUNC (SYSDATE) - 5
           -- this will also pick up transactions which were created 2 days prior
              --AND PRODUCT_CODE='N'
           AND corporate_account_number = p_account_num
--IN ('000181400007420') -- Changes done by Dhiresh on 28-05-2013 to remove Hardcode account number in query
      ORDER BY check_id, transaction_value_date;

--Declaring Advice Cursor to pick up invoices which are made against ABRL payments
   CURSOR cur_abrl_yb_adv (p_account_num VARCHAR2)
   IS
      SELECT   ypi.*, ypt.primary_name, ypt.vendor_code, ypt.vendor_address,
               ypt.transactional_currency, ypt.third_party_id,
               ypt.primary_email
          FROM apps.yesbank_payment_invoice ypi,
               apps.yesbank_payment_table ypt
         WHERE ypi.check_id = ypt.check_id
           AND TRUNC (ypt.creation_date) >= TRUNC (SYSDATE) - 5
           -- this will also pick up transactions which were created 2 days prior
           AND ypt.transaction_status = 'AUTHORIZED'
           --   AND YPT.PRODUCT_CODE='N' --
           AND ypt.corporate_account_number = p_account_num
--IN ('000181400007420') -- Changes done by Dhiresh on 28-05-2013 to remove Hardcode account number in query
      ORDER BY ypi.check_id;
BEGIN
   FOR rec_yb_acc_chk IN cur_yb_acc_chk
   LOOP
      -- Generating Sequence number for Sequence Table
      IF rec_yb_acc_chk.SOURCE = 'TSRL'
      THEN
         -- Generating a Unique Sequence Number to be concatenated in TSRL payment file name
         SELECT yesbank_tsrl_seq.NEXTVAL || 'x'
                || TO_CHAR (SYSDATE, 'ddmmyyyy')
           INTO v_yesbank_seq
           FROM DUAL;
      ELSE
         -- Generating a Unique Sequence Number to be concatenated in ABRL payment file name
         SELECT yesbank_abrl_seq.NEXTVAL || 'x'
                || TO_CHAR (SYSDATE, 'ddmmyyyy')
           INTO v_yesbank_seq
           FROM DUAL;
      END IF;

      SELECT COUNT (*), SUM (transactional_amount)
        INTO lv_abrl_count, lv_tot_abrl_amt
        FROM apps.yesbank_payment_table
       WHERE 1 = 1
         AND transaction_status = 'AUTHORIZED'
         AND TRUNC (creation_date) >= TRUNC (SYSDATE) - 5
         -- this will also pick up transactions which were created 2 days prior
          --AND PRODUCT_CODE='N'
         AND corporate_account_number =
                                       rec_yb_acc_chk.corporate_account_number;

      --IN ('000181400007420');
      BEGIN
         -- ABRL Payment File Generation
          --ABRL Payment Header
         IF lv_abrl_count > 0
         THEN
            chk_pt := chk_pt + 1;                              --for checking
            fnd_file.put_line (fnd_file.LOG,
                                  'string '
                               || chk_pt
                               || ' :-'
                               || rec_yb_acc_chk.account_name
                               || 'x'
                               || v_yesbank_seq
                               || '.TXT'
                              );                                --for checking
            --Creating a file for the below generated payment data
            l_file_p :=
               UTL_FILE.fopen ('YESBANK',
                                  rec_yb_acc_chk.account_name
                               || 'x'
                               || v_yesbank_seq
                               || '.TXT',
                               'W'
                              );
            l_pmt_data :=
                  'H'                                          -- Header Block
               || TO_CHAR (SYSDATE, 'dd/mm/yyyy')               -- Upload date
               || RPAD (   'ABRL'
                        || REPLACE (TO_CHAR (SYSDATE, 'dd/mm/yyyy'), '/')
                        || 'x'
                        || v_yesbank_seq,
                        20,
                        ' '
                       );                                      --Customer Code
            UTL_FILE.put_line (l_file_p, l_pmt_data);
            UTL_FILE.fclose (l_file_p);
            -- Payment Transaction

            --Creating a file for the below generated payment data
            l_file_p :=
               UTL_FILE.fopen ('YESBANK',
                                  rec_yb_acc_chk.account_name
                               || 'x'
                               || v_yesbank_seq
                               || '.TXT',
                               'A'
                              );

            FOR rec_abrl_yb_pay IN
               cur_abrl_yb_pay (rec_yb_acc_chk.corporate_account_number)
            LOOP
               l_pmt_data :=
                     'D'                                  -- Detail Indicator
                  || RPAD
                        (CASE
                            WHEN rec_abrl_yb_pay.product_code = 'N'
                               THEN 'N06'
                            WHEN rec_abrl_yb_pay.product_code = 'R'
                               THEN 'R41'
                            WHEN rec_abrl_yb_pay.product_code = 'A'
                               THEN 'A'
                         END,
                         3,
                         ' '
                        )                                   -- D2 Message Type
                  || RPAD (rec_abrl_yb_pay.corporate_account_number, 15, ' ')
                  -- YES Bank Debit Account Number
                  || RPAD ('ADITYA BIRLA RETAIL LIMITED', 35, ' ')
                  --Remitter Name
                  || RPAD ('Skyline Icon Building 5th Floor', 35, ' ')
                  -- Address1 of remitter
                  || RPAD ('86 / 92, Andheri Kurla Road', 35, ' ')
                  --Address2 of remitter
                  || RPAD ('Andheri East I Mumbai - 400059.', 35, ' ')
                  --Address3 of remitter
                  || RPAD (rec_abrl_yb_pay.benefi_ifsc_code, 11, ' ')
                  --Beneficiary IFSC Code
                  || RPAD (rec_abrl_yb_pay.benefi_acc_num, 34, ' ')
                  --Beneficiary Account No
                  || RPAD (rec_abrl_yb_pay.primary_name, 35, ' ')
                  --Beneficiary Name
                  || RPAD (rec_abrl_yb_pay.vendor_address, 140, ' ')
                                                       -- Address Line 1,2,3,4
                  --|| RPAD(SUBSTR(REC_ABRL_YB_PAY.VENDOR_ADDRESS,0,35),35,' ') -- Address Line 1
                  --|| RPAD(SUBSTR(REC_ABRL_YB_PAY.VENDOR_ADDRESS,36,35),35,' ') -- Address Line 2
                  --|| RPAD(SUBSTR(REC_ABRL_YB_PAY.VENDOR_ADDRESS,71,35),35,' ') -- Address Line 3
                  --|| RPAD(SUBSTR(REC_ABRL_YB_PAY.VENDOR_ADDRESS,106,35),35,' ') -- Address Line 4
                  || RPAD (rec_abrl_yb_pay.check_id, 16, ' ')
                  -- Transaction Ref No
                  || RPAD (TO_CHAR (SYSDATE, 'dd/mm/yyyy'), 10, ' ')
                  -- Upload date
                  || LPAD (rec_abrl_yb_pay.transactional_amount, 14, '0')
                  -- Amount
                  || RPAD (rec_abrl_yb_pay.pay_doc_number, 27, ' ')
                  -- Sender To Receiver Info
                  || RPAD (' ', 33, ' ')                         -- Add Info 1
                  || RPAD (' ', 33, ' ')                         -- Add Info 2
                  || RPAD (' ', 33, ' ')                         -- Add Info 3
                  || RPAD (' ', 33, ' ');                        -- Add Info 4
               UTL_FILE.put_line (l_file_p, l_pmt_data);
            END LOOP;

            UTL_FILE.fclose (l_file_p);
            -- Payment Footer
            l_file_p :=
               UTL_FILE.fopen ('YESBANK',
                                  rec_yb_acc_chk.account_name
                               || 'x'
                               || v_yesbank_seq
                               || '.TXT',
                               'A'
                              );
            l_pmt_data :=
                  'F'                                          -- Footer Block
               || LPAD (lv_abrl_count, 5, '0')            -- Transaction Count
               || LPAD (lv_tot_abrl_amt, 14, '0');        -- Total file amount
            UTL_FILE.put_line (l_file_p, l_pmt_data);
            UTL_FILE.fclose (l_file_p);
         END IF;

         chk_pt := chk_pt + 1;                                  --for checking
         fnd_file.put_line (fnd_file.LOG,
                               'string '
                            || chk_pt
                            || ' :- LV_ABRL_COUNT - '
                            || lv_abrl_count
                           );                                   --for checking

         -- ABRL Payment Advice File Generation
         BEGIN
            SELECT COUNT (*)
              INTO lv_abrl_adv_count
              FROM apps.yesbank_payment_invoice ypi,
                   apps.yesbank_payment_table ypt
             WHERE ypi.check_id = ypt.check_id
               AND TRUNC (creation_date) >= TRUNC (SYSDATE) - 5
               --this will also pick up transactions which were created 2 days prior
               AND ypt.transaction_status = 'AUTHORIZED'
               --AND YPT.PRODUCT_CODE='N'
               AND ypt.corporate_account_number =
                                       rec_yb_acc_chk.corporate_account_number;

            -- IN ('000181400007420');
            IF lv_abrl_adv_count > 0
            THEN
               --Creating a file for the below generated payment data
               l_file_a :=
                  UTL_FILE.fopen ('YESBANK',
                                     rec_yb_acc_chk.account_name
                                  || 'x'
                                  || v_yesbank_seq
                                  || '_ADV.TXT',
                                  'W'
                                 );

               FOR rec_abrl_yb_adv IN
                  cur_abrl_yb_adv (rec_yb_acc_chk.corporate_account_number)
               LOOP
                  l_adv_data :=
                        RPAD (rec_abrl_yb_adv.check_id, 16, ' ')    -- REF_NO
                     || RPAD (rec_abrl_yb_adv.primary_name, 50, ' ')
                     --Beneficiary Name
                     || RPAD (rec_abrl_yb_adv.vendor_code, 10, ' ')
                     --Beneficiary Code
                     || RPAD (' ', 20, ' ')                       -- Bene Code
                     || RPAD
                           (SUBSTR (rec_abrl_yb_adv.vendor_address, 0, 180),
                            180,
                            ' '
                           )                           -- Address Line 1-2-3-4
                     || RPAD (rec_abrl_yb_adv.transactional_currency, 3, ' ')
                     -- Currency
                     || RPAD (rec_abrl_yb_adv.invoice_number, 50, ' ')
                     -- Invoice No
                     || RPAD
                           (TO_CHAR (rec_abrl_yb_adv.invoice_date,
                                     'DD/MM/YYYY'
                                    ),
                            10,
                            ' '
                           )                                   -- Invoice Date
                     || RPAD (rec_abrl_yb_adv.invoice_amount, 14, ' ')
                     -- Invoice Amount
                     || RPAD (rec_abrl_yb_adv.invoice_amount, 14, ' ')
                     -- Passed Amount
                     || RPAD (NVL (rec_abrl_yb_adv.tax, 0), 14, ' ')    -- Tax
                     || RPAD (rec_abrl_yb_adv.invoice_amount, 14, ' ')
                     -- Net Amount
                     || RPAD (rec_abrl_yb_adv.third_party_id, 60, ' ')
                     -- Location
                     || RPAD (' ', 3, ' ')                     -- Voucher Type
                     || RPAD (' ', 8, ' ')                       -- Voucher No
                     || RPAD (' ', 50, ' ')                 -- Contact Person1
                     || RPAD (REPLACE (rec_abrl_yb_adv.primary_email, ',',
                                       ';'),
                              300,
                              ' '
                             );
--changes made by Dhiresh --|| RPAD(SUBSTR(REC_ABRL_YB_ADV.PRIMARY_EMAIL,1,INSTR(RPAD(REC_ABRL_YB_ADV.PRIMARY_EMAIL,100,' '),';',-1)-1),100,' '); -- CONTACT_EMAIL
                  UTL_FILE.put_line (l_file_a, l_adv_data);
               END LOOP;

               UTL_FILE.fclose (l_file_a);
            END IF;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               UTL_FILE.fclose (l_file_a);
               DBMS_OUTPUT.put_line ('No Data Found');
            WHEN UTL_FILE.invalid_path
            THEN
               UTL_FILE.fclose (l_file_a);
               DBMS_OUTPUT.put_line ('Invalid Path');
            WHEN UTL_FILE.invalid_filehandle
            THEN
               UTL_FILE.fclose (l_file_a);
               DBMS_OUTPUT.put_line ('Invalid File Handle');
            WHEN UTL_FILE.invalid_operation
            THEN
               UTL_FILE.fclose (l_file_a);
               DBMS_OUTPUT.put_line ('Invalid Operation');
            WHEN UTL_FILE.read_error
            THEN
               UTL_FILE.fclose (l_file_a);
               DBMS_OUTPUT.put_line ('Read Error');
            WHEN UTL_FILE.write_error
            THEN
               UTL_FILE.fclose (l_file_a);
               DBMS_OUTPUT.put_line ('Write Error');
            WHEN UTL_FILE.internal_error
            THEN
               UTL_FILE.fclose (l_file_a);
               DBMS_OUTPUT.put_line ('Internal Error');
            WHEN UTL_FILE.file_open
            THEN
               UTL_FILE.fclose (l_file_a);
               DBMS_OUTPUT.put_line ('File is Open');
            WHEN UTL_FILE.invalid_filename
            THEN
               UTL_FILE.fclose (l_file_a);
               DBMS_OUTPUT.put_line ('Invalid File Name');
            WHEN OTHERS
            THEN
               UTL_FILE.fclose (l_file_a);
               DBMS_OUTPUT.put_line ('Unknown Error' || SQLERRM);
         END;

         chk_pt := chk_pt + 1;                                  --for checking
         fnd_file.put_line (fnd_file.LOG,
                               'string '
                            || chk_pt
                            || ' :- LV_ABRL_ADV_COUNT - '
                            || lv_abrl_adv_count
                           );                                   --for checking

         /* Updating the YESBANK Payment Stage Table as the ABRL transaction have been delivered to YES Bank*/
         UPDATE apps.yesbank_payment_table
            SET transaction_status = 'PROCESSED',
                status_flag = 'Y'
          WHERE 1 = 1
            AND transaction_status = 'AUTHORIZED'
            AND TRUNC (creation_date) >= TRUNC (SYSDATE) - 5
            -- AND PRODUCT_CODE='N'
            AND corporate_account_number =
                                       rec_yb_acc_chk.corporate_account_number;

         --IN('000181400007420');
         COMMIT;
      END;
------------------------------------------------------------------------
   END LOOP;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      UTL_FILE.fclose (l_file_p);
      DBMS_OUTPUT.put_line ('No Data Found');
   WHEN UTL_FILE.invalid_path
   THEN
      UTL_FILE.fclose (l_file_p);
      DBMS_OUTPUT.put_line ('Invalid Path');
   WHEN UTL_FILE.invalid_filehandle
   THEN
      UTL_FILE.fclose (l_file_p);
      DBMS_OUTPUT.put_line ('Invalid File Handle');
   WHEN UTL_FILE.invalid_operation
   THEN
      UTL_FILE.fclose (l_file_p);
      DBMS_OUTPUT.put_line ('Invalid Operation');
   WHEN UTL_FILE.read_error
   THEN
      UTL_FILE.fclose (l_file_p);
      DBMS_OUTPUT.put_line ('Read Error');
   WHEN UTL_FILE.write_error
   THEN
      UTL_FILE.fclose (l_file_p);
      DBMS_OUTPUT.put_line ('Write Error');
   WHEN UTL_FILE.internal_error
   THEN
      UTL_FILE.fclose (l_file_p);
      DBMS_OUTPUT.put_line ('Internal Error');
   WHEN UTL_FILE.file_open
   THEN
      UTL_FILE.fclose (l_file_p);
      DBMS_OUTPUT.put_line ('File is Open');
   WHEN UTL_FILE.invalid_filename
   THEN
      UTL_FILE.fclose (l_file_p);
      DBMS_OUTPUT.put_line ('Invalid File Name');
   WHEN OTHERS
   THEN
      UTL_FILE.fclose (l_file_p);
      DBMS_OUTPUT.put_line ('Unknown Error' || SQLERRM);
END xxabrl_yesbank_outbound_utl; 
/


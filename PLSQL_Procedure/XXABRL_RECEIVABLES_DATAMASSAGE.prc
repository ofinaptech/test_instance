CREATE OR REPLACE PROCEDURE APPS.xxabrl_receivables_datamassage (
   x_err_buf    OUT   VARCHAR2,
   x_ret_code   OUT   NUMBER
)
AS
BEGIN
--RECEIPT UPDATATION
   BEGIN
--  SM TN OU
      UPDATE apps.xxabrl_navi_ar_receipt_stg2 a
         SET operating_unit = 'SM TN OU',
             org_name = 'SM TN OU'
       WHERE 1 = 1
         AND TRUNC (gl_date) > '30-NOV-14'
         AND operating_unit = 'TN SM TSRL'
         AND (a.er_customer_number, a.gl_date) IN (
                                      SELECT b.customer_name, b.gl_date
                                        FROM apps.xxabrl_navi_ar_int_line_stg2 b);

--SM KL OU
      UPDATE apps.xxabrl_navi_ar_receipt_stg2 a
         SET operating_unit = 'SM KL OU',
             org_name = 'SM KL OU'
       WHERE 1 = 1
         AND TRUNC (gl_date) > '30-NOV-14'
         AND operating_unit = 'KL SM TSRL'
         AND (a.er_customer_number, a.gl_date) IN (
                                      SELECT b.customer_name, b.gl_date
                                        FROM apps.xxabrl_navi_ar_int_line_stg2 b);

--SM KA OU
      UPDATE apps.xxabrl_navi_ar_receipt_stg2 a
         SET operating_unit = 'SM KA OU',
             org_name = 'SM KA OU'
       WHERE 1 = 1
         AND TRUNC (gl_date) > '30-NOV-14'
         AND operating_unit = 'KA SM TSRL'
         AND (a.er_customer_number, a.gl_date) IN (
                                      SELECT b.customer_name, b.gl_date
                                        FROM apps.xxabrl_navi_ar_int_line_stg2 b);

-- HM KA OU
      UPDATE apps.xxabrl_navi_ar_receipt_stg2 a
         SET operating_unit = 'HM KA OU',
             org_name = 'HM KA OU'
       WHERE 1 = 1
         AND TRUNC (gl_date) > '30-NOV-14'
         AND operating_unit = 'KA HM TSRL'
         AND er_customer_number = '1511329'
         AND (a.er_customer_number, a.gl_date, a.statement_no) IN (
                             SELECT b.customer_name, b.gl_date,
                                    b.statement_no
                               FROM apps.xxabrl_navi_ar_int_line_stg2 b);

--SM AP OU
      UPDATE apps.xxabrl_navi_ar_receipt_stg2 a
         SET operating_unit = 'SM AP OU',
             org_name = 'SM AP OU'
       WHERE 1 = 1
         AND TRUNC (gl_date) > '30-NOV-14'
         AND operating_unit = 'AP SM TSRL'
--AND ER_CUSTOMER_NUMBER='1161604'
         AND (a.er_customer_number, a.gl_date) IN (
                SELECT b.customer_name, b.gl_date
                  FROM apps.xxabrl_navi_ar_int_line_stg2 b
                 WHERE 1 = 1
                   AND TRUNC (gl_date) > '30-NOV-14'
                   AND org_code = 'AP SM TSRL'
                   AND interfaced_flag IN ('N', 'I')
                   AND freeze_flag = 'N'
                   AND (interface_line_attribute1,
                        interface_line_attribute2,
                        customer_name
                       ) IN (
                          SELECT interface_line_attribute1,
                                 interface_line_attribute2, segment4
                            FROM apps.xxabrl_navi_ra_int_dist_stg2
                           WHERE 1 = 1
                             AND org_id = 'AP SM TSRL'
                             AND segment1 = 31
                             AND segment3 = 752));

--      SM TG OU
      UPDATE apps.xxabrl_navi_ar_receipt_stg2 a
         SET operating_unit = 'SM TG OU',
             org_name = 'SM TG OU'
       WHERE 1 = 1
         AND TRUNC (gl_date) > '30-NOV-14'
         AND operating_unit = 'AP SM TSRL'
--AND ER_CUSTOMER_NUMBER='1161604'
         AND (a.er_customer_number, a.gl_date) IN (
                SELECT b.customer_name, b.gl_date
                  FROM apps.xxabrl_navi_ar_int_line_stg2 b
                 WHERE 1 = 1
                   AND TRUNC (gl_date) > '30-NOV-14'
                   AND org_code = 'AP SM TSRL'
                   AND interfaced_flag IN ('N', 'I')
                   AND freeze_flag = 'N'
                   AND (interface_line_attribute1,
                        interface_line_attribute2,
                        customer_name
                       ) IN (
                          SELECT interface_line_attribute1,
                                 interface_line_attribute2, segment4
                            FROM apps.xxabrl_navi_ra_int_dist_stg2
                           WHERE 1 = 1
                             AND org_id = 'AP SM TSRL'
                             AND segment1 = 31
                             AND segment3 = 754));

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         fnd_file.put_line (fnd_file.LOG,
                               'RECEIPT UPDATATION ERROR :-'
                            || SQLERRM
                            || ':'
                            || SQLCODE
                           );
   END;

--  sales and dist updation
   BEGIN
--   HM KA OU
      UPDATE apps.xxabrl_navi_ra_int_dist_stg2
         SET org_id = 'HM KA OU',
             segment1 = '11',
             segment3 = '630'
       WHERE 1 = 1
         AND segment4 = '1511329'
         AND (interface_line_attribute1, interface_line_attribute2) IN (
                SELECT interface_line_attribute1, interface_line_attribute2
                  FROM apps.xxabrl_navi_ar_int_line_stg2 nril
                 WHERE 1 = 1
                   AND UPPER (nril.org_code) = 'KA HM TSRL'
                   AND TRUNC (nril.gl_date) > '30-NOV-14'
                   AND interfaced_flag IN ('N', 'I')
                   AND freeze_flag = 'N');

      UPDATE apps.xxabrl_navi_ar_int_line_stg2
         SET sob_id = 'ABRL Ledger',
             org_code = 'HM KA OU',
             org_id = '1523'
       WHERE 1 = 1
         AND TRUNC (gl_date) > '30-NOV-14'
         AND org_code LIKE 'KA HM TSRL'
         AND interfaced_flag IN ('N', 'I')
         AND freeze_flag = 'N';

--SM KL OU
      UPDATE apps.xxabrl_navi_ra_int_dist_stg2
         SET org_id = 'SM KL OU',
             segment1 = '11',
             segment3 = '635'
       WHERE 1 = 1
         AND org_id = 'KL SM TSRL'
         AND (interface_line_attribute1, interface_line_attribute2) IN (
                SELECT interface_line_attribute1, interface_line_attribute2
                  FROM apps.xxabrl_navi_ar_int_line_stg2 nril
                 WHERE 1 = 1
                   AND UPPER (nril.org_code) = 'KL SM TSRL'
                   AND TRUNC (nril.gl_date) > '30-NOV-14'
                   AND interfaced_flag IN ('N', 'I')
                   AND freeze_flag = 'N');

      UPDATE apps.xxabrl_navi_ar_int_line_stg2
         SET sob_id = 'ABRL Ledger',
             org_code = 'SM KL OU',
             org_id = '1524'
       WHERE 1 = 1
         AND TRUNC (gl_date) > '30-NOV-14'
         AND org_code LIKE 'KL SM TSRL'
         AND interfaced_flag IN ('N', 'I')
         AND freeze_flag = 'N';

--SM KA OU
      UPDATE apps.xxabrl_navi_ra_int_dist_stg2
         SET org_id = 'SM KA OU',
             segment1 = '11',
             segment3 = '620'
       WHERE 1 = 1
         AND org_id = 'KA SM TSRL'
         AND (interface_line_attribute1, interface_line_attribute2) IN (
                SELECT interface_line_attribute1, interface_line_attribute2
                  FROM apps.xxabrl_navi_ar_int_line_stg2 nril
                 WHERE 1 = 1
                   AND UPPER (nril.org_code) = 'KA SM TSRL'
                   AND TRUNC (nril.gl_date) > '30-NOV-14'
                   AND interfaced_flag IN ('N', 'I')
                   AND freeze_flag = 'N');

      UPDATE apps.xxabrl_navi_ar_int_line_stg2
         SET sob_id = 'ABRL Ledger',
             org_code = 'SM KA OU',
             org_id = '1521'
       WHERE 1 = 1
         AND TRUNC (gl_date) > '30-NOV-14'
         AND org_code LIKE 'KA SM TSRL'
         AND interfaced_flag IN ('N', 'I')
         AND freeze_flag = 'N';

--SM TN OU
      UPDATE apps.xxabrl_navi_ra_int_dist_stg2
         SET org_id = 'SM TN OU',
             segment1 = '11',
             segment3 = '640'
       WHERE 1 = 1
         AND org_id = 'TN SM TSRL'
         AND (interface_line_attribute1, interface_line_attribute2) IN (
                SELECT interface_line_attribute1, interface_line_attribute2
                  FROM apps.xxabrl_navi_ar_int_line_stg2 nril
                 WHERE 1 = 1
                   AND UPPER (nril.org_code) = 'TN SM TSRL'
                   AND TRUNC (nril.gl_date) > '30-NOV-14'
                   AND interfaced_flag IN ('N', 'I')
                   AND freeze_flag = 'N');

      UPDATE apps.xxabrl_navi_ar_int_line_stg2
         SET sob_id = 'ABRL Ledger',
             org_code = 'SM TN OU',
             org_id = '1525'
       WHERE 1 = 1
         AND TRUNC (gl_date) > '30-NOV-14'
         AND org_code LIKE 'TN SM TSRL'
         AND interfaced_flag IN ('N', 'I')
         AND freeze_flag = 'N';

--SM AP OU
      UPDATE apps.xxabrl_navi_ra_int_dist_stg2
         SET org_id = 'SM AP OU',
             segment1 = '11',
             segment3 = '610'
       WHERE 1 = 1
         AND org_id = 'AP SM TSRL'
         AND segment1 = 31
         AND segment3 = 752
         AND (interface_line_attribute1,
              interface_line_attribute2,
              segment4
             ) IN (
                SELECT interface_line_attribute1, interface_line_attribute2,
                       customer_name
                  FROM apps.xxabrl_navi_ar_int_line_stg2 nril
                 WHERE 1 = 1
                   AND UPPER (nril.org_code) = 'AP SM TSRL'
                   AND TRUNC (nril.gl_date) > '30-NOV-14'
                   AND interfaced_flag IN ('N', 'I')
                   AND freeze_flag = 'N');

      UPDATE apps.xxabrl_navi_ar_int_line_stg2
         SET sob_id = 'ABRL Ledger',
             org_code = 'SM AP OU',
             org_id = '1481'
       WHERE 1 = 1
         AND TRUNC (gl_date) > '30-NOV-14'
         AND org_code = 'AP SM TSRL'
         AND interfaced_flag IN ('N', 'I')
         AND freeze_flag = 'N'
         AND (interface_line_attribute1,
              interface_line_attribute2,
              customer_name
             ) IN (
                SELECT interface_line_attribute1, interface_line_attribute2,
                       segment4
                  FROM apps.xxabrl_navi_ra_int_dist_stg2
                 WHERE 1 = 1
                   AND org_id = 'SM AP OU'
                   AND segment1 = 11
                   AND segment3 = 610);

--SM TG OU
      UPDATE apps.xxabrl_navi_ra_int_dist_stg2
         SET org_id = 'SM TG OU',
             segment1 = '11',
             segment3 = '615'
       WHERE 1 = 1
         AND org_id = 'AP SM TSRL'
         AND segment1 = 31
         AND segment3 = 754
         AND (interface_line_attribute1,
              interface_line_attribute2,
              segment4
             ) IN (
                SELECT interface_line_attribute1, interface_line_attribute2,
                       customer_name
                  FROM apps.xxabrl_navi_ar_int_line_stg2 nril
                 WHERE 1 = 1
                   AND UPPER (nril.org_code) = 'AP SM TSRL'
                   AND TRUNC (nril.gl_date) > '30-NOV-14'
                   AND interfaced_flag IN ('N', 'I')
                   AND freeze_flag = 'N');

      UPDATE apps.xxabrl_navi_ar_int_line_stg2
         SET sob_id = 'ABRL Ledger',
             org_code = 'SM TG OU',
             org_id = '1501'
       WHERE 1 = 1
         AND TRUNC (gl_date) > '30-NOV-14'
         AND org_code = 'AP SM TSRL'
         AND interfaced_flag IN ('N', 'I')
         AND freeze_flag = 'N'
         AND (interface_line_attribute1,
              interface_line_attribute2,
              customer_name
             ) IN (
                SELECT interface_line_attribute1, interface_line_attribute2,
                       segment4
                  FROM apps.xxabrl_navi_ra_int_dist_stg2
                 WHERE 1 = 1
                   AND org_id = 'SM TG OU'
                   AND segment1 = 11
                   AND segment3 = 615);

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         fnd_file.put_line (fnd_file.LOG,
                            'SALES/ Dist ERROR :-' || SQLERRM || ':'
                            || SQLCODE
                           );
   END;

   ---  NEW BANK UPDATION
   BEGIN
      --HM KA OU
--      UPDATE apps.xxabrl_navi_ar_receipt_stg2 rec
--         SET rec.bank_account_name =
--                (SELECT ba.bank_account_name
----       ba.bank_account_num,
----       ap_use_allowed_flag, ar_use_allowed_flag,
----        hr.NAME
--                 FROM   apps.ce_bank_accounts ba,
--                        apps.ce_bank_branches_v branch,
--                        (SELECT default_legal_context_id, NAME, date_to,
--                                organization_id, short_code
--                           FROM hr_operating_units) hr
--                  WHERE 1 = 1
--                    AND ba.bank_branch_id = branch.pk_id
--                    AND bank_account_num = rec.bank_account_number
--                    AND account_owner_org_id = hr.default_legal_context_id
--                    and ba.end_date is null
----   AND account_owner_org_id = le.legal_entity_id
----   AND UPPER (hr.NAME) LIKE UPPER ('TSRL%')
--                    AND hr.date_to IS NULL
--                    AND hr.short_code = rec.org_name
--                    AND ar_use_allowed_flag = 'Y')
--       WHERE gl_date >= '01-Dec-14'
--         AND org_name = 'HM KA OU'
--         AND interfaced_flag IN ('N', 'I', 'E');
      BEGIN
------SM TG OU
         UPDATE apps.xxabrl_navi_ar_receipt_stg2 rec
            SET rec.bank_account_name =
                   (SELECT ba.bank_account_name
--       ba.bank_account_num,
--       ap_use_allowed_flag, ar_use_allowed_flag,
--        hr.NAME
                    FROM   apps.ce_bank_accounts ba,
                           apps.ce_bank_branches_v branch,
                           (SELECT default_legal_context_id, NAME, date_to,
                                   organization_id, short_code
                              FROM hr_operating_units) hr
                     WHERE 1 = 1
                       AND ba.bank_branch_id = branch.pk_id
                       AND bank_account_num = rec.bank_account_number
                       AND account_owner_org_id = hr.default_legal_context_id
                       AND ba.end_date IS NULL
--   AND account_owner_org_id = le.legal_entity_id
--   AND UPPER (hr.NAME) LIKE UPPER ('TSRL%')
                       AND hr.date_to IS NULL
                       AND hr.short_code = rec.org_name
                       AND ar_use_allowed_flag = 'Y')
          WHERE gl_date >= '01-Dec-14'
            AND org_name = 'SM TG OU'
            AND interfaced_flag IN ('N', 'I', 'E');
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (fnd_file.LOG, 'Updation error in SM TG OU');
      END;

      BEGIN
------SM KL OU
         UPDATE apps.xxabrl_navi_ar_receipt_stg2 rec
            SET rec.bank_account_name =
                   (SELECT ba.bank_account_name
--       ba.bank_account_num,
--       ap_use_allowed_flag, ar_use_allowed_flag,
--        hr.NAME
                    FROM   apps.ce_bank_accounts ba,
                           apps.ce_bank_branches_v branch,
                           (SELECT default_legal_context_id, NAME, date_to,
                                   organization_id, short_code
                              FROM hr_operating_units) hr
                     WHERE 1 = 1
                       AND ba.bank_branch_id = branch.pk_id
                       AND bank_account_num = rec.bank_account_number
                       AND account_owner_org_id = hr.default_legal_context_id
                       AND ba.end_date IS NULL
--   AND account_owner_org_id = le.legal_entity_id
--   AND UPPER (hr.NAME) LIKE UPPER ('TSRL%')
                       AND hr.date_to IS NULL
                       AND hr.short_code = rec.org_name
                       AND ar_use_allowed_flag = 'Y')
          WHERE gl_date >= '01-Dec-14'
            AND org_name = 'SM KL OU'
            AND interfaced_flag IN ('N', 'I', 'E');
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (fnd_file.LOG, 'Updation error in SM KL OU');
      END;

      BEGIN
------SM KA OU
         UPDATE apps.xxabrl_navi_ar_receipt_stg2 rec
            SET rec.bank_account_name =
                   (SELECT ba.bank_account_name
--       ba.bank_account_num,
--       ap_use_allowed_flag, ar_use_allowed_flag,
--        hr.NAME
                    FROM   apps.ce_bank_accounts ba,
                           apps.ce_bank_branches_v branch,
                           (SELECT default_legal_context_id, NAME, date_to,
                                   organization_id, short_code
                              FROM hr_operating_units) hr
                     WHERE 1 = 1
                       AND ba.bank_branch_id = branch.pk_id
                       AND bank_account_num = rec.bank_account_number
                       AND account_owner_org_id = hr.default_legal_context_id
                       AND ba.end_date IS NULL
--   AND account_owner_org_id = le.legal_entity_id
--   AND UPPER (hr.NAME) LIKE UPPER ('TSRL%')
                       AND hr.date_to IS NULL
                       AND hr.short_code = rec.org_name
                       AND ar_use_allowed_flag = 'Y')
          WHERE gl_date >= '01-Dec-14'
            AND org_name = 'SM KA OU'
            AND interfaced_flag IN ('N', 'I', 'E');
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (fnd_file.LOG, 'Updation error in SM KA OU');
      END;

      BEGIN
------SM TN OU
         UPDATE apps.xxabrl_navi_ar_receipt_stg2 rec
            SET rec.bank_account_name =
                   (SELECT ba.bank_account_name
--       ba.bank_account_num,
--       ap_use_allowed_flag, ar_use_allowed_flag,
--        hr.NAME
                    FROM   apps.ce_bank_accounts ba,
                           apps.ce_bank_branches_v branch,
                           (SELECT default_legal_context_id, NAME, date_to,
                                   organization_id, short_code
                              FROM hr_operating_units) hr
                     WHERE 1 = 1
                       AND ba.bank_branch_id = branch.pk_id
                       AND bank_account_num = rec.bank_account_number
                       AND account_owner_org_id = hr.default_legal_context_id
                       AND ba.end_date IS NULL
--   AND account_owner_org_id = le.legal_entity_id
--   AND UPPER (hr.NAME) LIKE UPPER ('TSRL%')
                       AND hr.date_to IS NULL
                       AND hr.short_code = rec.org_name
                       AND ar_use_allowed_flag = 'Y')
          WHERE gl_date >= '01-Dec-14'
            AND org_name = 'SM TN OU'
            AND interfaced_flag IN ('N', 'I', 'E');
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (fnd_file.LOG, 'Updation error in SM TN OU');
      END;

      BEGIN
------SM AP OU
         UPDATE apps.xxabrl_navi_ar_receipt_stg2 rec
            SET rec.bank_account_name =
                   (SELECT ba.bank_account_name
--       ba.bank_account_num,
--       ap_use_allowed_flag, ar_use_allowed_flag,
--        hr.NAME
                    FROM   apps.ce_bank_accounts ba,
                           apps.ce_bank_branches_v branch,
                           (SELECT default_legal_context_id, NAME, date_to,
                                   organization_id, short_code
                              FROM hr_operating_units) hr
                     WHERE 1 = 1
                       AND ba.bank_branch_id = branch.pk_id
                       AND bank_account_num = rec.bank_account_number
                       AND account_owner_org_id = hr.default_legal_context_id
                       AND ba.end_date IS NULL
--   AND account_owner_org_id = le.legal_entity_id
--   AND UPPER (hr.NAME) LIKE UPPER ('TSRL%')
                       AND hr.date_to IS NULL
                       AND hr.short_code = rec.org_name
                       AND ar_use_allowed_flag = 'Y')
          WHERE gl_date >= '01-Dec-14'
            AND org_name = 'SM AP OU'
            AND interfaced_flag IN ('N', 'I', 'E');
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (fnd_file.LOG, 'Updation error in SM AP OU');
      END;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         fnd_file.put_line (fnd_file.LOG,
                               'NEW BANK UPDATION ERROR :-'
                            || SQLERRM
                            || ':'
                            || SQLCODE
                           );
   END;

   fnd_file.put_line (fnd_file.LOG,
                         'LEM MASSAGING SUCCESSFULLY :-'
                      || SQLERRM
                      || ':'
                      || SQLCODE
                     );
EXCEPTION
   WHEN OTHERS
   THEN
      fnd_file.put_line (fnd_file.LOG,
                         'ERROR :-' || SQLERRM || ':' || SQLCODE
                        );
END; 
/


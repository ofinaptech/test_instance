CREATE OR REPLACE PROCEDURE APPS.XXABRL_HPN_GLBALTAB_OVERWRITE (ERRBUF OUT VARCHAR2,
                                                                RETCODE OUT NUMBER,
                                                                P_PERIOD_NAME VARCHAR2,
                                                                P_VERSION_NO NUMBER,
                                                                P_CREATED_BY VARCHAR2)

/****************************************************************************************************************
                 OBJECT INFORMATION

 Object Name : XXABRL_HPN_GLBALTAB_OVERWRITE

 Description : Procedure to Overwrite the existing Version of GL_BALANCE Table with the fresh Data of GL BALANCES

  Change Record:
 ==================================================================================================================
 Version    Date            Author           Remarks
 =======   ==========     =============     ========================================================================
 1.0.0     11-Nov-09      Praveen Kumar     Initial Version
 1.0.1     09-Feb-10      Praveen Kumar     Updated in Update Statement
********************************************************************************************************************/

AS
 V_COUNT NUMBER;
 V_VERSION_NO NUMBER;
 V_TABLE_NAME VARCHAR2(100);
 V_PERIOD_NAME VARCHAR2(50);
 DELETE_STRING LONG;
 CREATE_STRING LONG;
 INSERT_STRING LONG;

 BEGIN

   BEGIN
   
   SELECT TNAME
     INTO V_TABLE_NAME
     FROM XXABRL_GL_BALANCES_TABLE
     WHERE PERIOD = P_PERIOD_NAME
       AND VERSION_NO = P_VERSION_NO;

     EXCEPTION
       WHEN OTHERS THEN
        FND_FILE.PUT_LINE(FND_FILE.LOG,'Table Name does not found for this period and version');
     END;

       

     

     IF V_TABLE_NAME IS NOT NULL THEN       

       DELETE_STRING := 'TRUNCATE TABLE '||V_TABLE_NAME;  
       EXECUTE IMMEDIATE DELETE_STRING; 

       COMMIT;      

       DBMS_OUTPUT.PUT_LINE ('TNAME' || V_TABLE_NAME);
       DBMS_OUTPUT.PUT_LINE ('PERIOD' || P_PERIOD_NAME);

       --CREATE_STRING := 'CREATE TABLE '||V_TABLE_NAME||'AS SELECT * FROM GL_BALANCES';

       

              INSERT_STRING:= 'INSERT INTO '||V_TABLE_NAME||' (
                                                  LEDGER_ID,   
                                                  CODE_COMBINATION_ID,
                                                  CURRENCY_CODE,      
                                                  PERIOD_NAME,        
                                                  ACTUAL_FLAG,        
                                                  LAST_UPDATE_DATE,   
                                                  LAST_UPDATED_BY,    
                                                  BUDGET_VERSION_ID,  
                                                  ENCUMBRANCE_TYPE_ID,
                                                  TRANSLATED_FLAG,    
                                                  REVALUATION_STATUS, 
                                                  PERIOD_TYPE, 
                                                  PERIOD_YEAR, 
                                                  PERIOD_NUM,  
                                                  PERIOD_NET_DR,
                                                  PERIOD_NET_CR,
                                                  PERIOD_TO_DATE_ADB,
                                                  QUARTER_TO_DATE_DR,
                                                  QUARTER_TO_DATE_CR,
                                                  QUARTER_TO_DATE_ADB,
                                                  YEAR_TO_DATE_ADB,
                                                  PROJECT_TO_DATE_DR,
                                                  PROJECT_TO_DATE_CR,
                                                  PROJECT_TO_DATE_ADB,
                                                  BEGIN_BALANCE_DR,
                                                  BEGIN_BALANCE_CR,
                                                  PERIOD_NET_DR_BEQ,
                                                  PERIOD_NET_CR_BEQ,
                                                  BEGIN_BALANCE_DR_BEQ,
                                                  BEGIN_BALANCE_CR_BEQ,
                                                  TEMPLATE_ID,
                                                  ENCUMBRANCE_DOC_ID,
                                                  ENCUMBRANCE_LINE_NUM,
                                                  QUARTER_TO_DATE_DR_BEQ,
                                                  QUARTER_TO_DATE_CR_BEQ,
                                                  PROJECT_TO_DATE_DR_BEQ,
                                                  PROJECT_TO_DATE_CR_BEQ) SELECT * FROM GL_BALANCES';                      
                         
                         EXECUTE IMMEDIATE INSERT_STRING;

          UPDATE XXABRL_GL_BALANCES_TABLE
             SET OVERWRITE_FLAG = 'Y'
                ,INSERT_FLAG = 'N' 
                ,CREATED_BY = P_CREATED_BY
                ,CREATION_DATE = SYSDATE
           WHERE VERSION_NO = P_VERSION_NO             
             AND PERIOD = P_PERIOD_NAME;

          COMMIT;                    

       

                                           COMMIT;  

     END IF;                                      

                                            

 END XXABRL_HPN_GLBALTAB_OVERWRITE; 
/


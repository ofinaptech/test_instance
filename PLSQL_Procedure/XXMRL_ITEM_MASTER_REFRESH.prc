CREATE OR REPLACE PROCEDURE APPS.xxmrl_item_master_refresh (
   errbuf    VARCHAR2,
   errcode   NUMBER
)
AS
/*=========================================================================================================
||   Program Name    : XXMRL Item Master Outbound Refresh
||   Description : This Program is used for to View the Item Master info to users
||
||     Version                        Date                             Author                                Modification
||  ~~~~~~~~             ~~~~~~~~~~~~           ~~~~~~~~~~~~~~~~~~          ~~~~~~~~~~~~~~~~~
|| 11.2.0.4.0                 20-May-2019               Lokesh Poojari
||  ~~~~~~~~             ~~~~~~~~~~~~           ~~~~~~~~~~~~~~~~~~          ~~~~~~~~~~~~~~~~~
||
============================================================================================================*/
BEGIN
   DBMS_SNAPSHOT.REFRESH ('XXMRL_ITEM_MASTER_MV', 'A');
   fnd_file.put_line (fnd_file.LOG, 'Refreshed Successfully');
EXCEPTION
   WHEN OTHERS
   THEN
      fnd_file.put_line (fnd_file.LOG,
                         'Error=>' || SQLCODE || 'ErrorMsg=>' || SQLERRM
                        );
END; 
/


CREATE OR REPLACE PACKAGE BODY APPS.GST_ABRL_supplier_master_pkg
AS
/*
  =========================================================================================================
  ||   Filename   : XXABRL_APNAV_PKG_INV_Interface.sql
  ||   Description : Script is used to mold Navision data for AP
  ||
  ||   Version     Date            Author              Modification
  ||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
  ||   1.0.0       13-jan-2008    Hans Raj Kasana      New Development
  ||   1.0.1       12-feb-2010    Naresh Hasti         added the Navision Vendor number in site summary and detailed level
  ||   1.0.2       02-mar-10     Naresh Hasti          changed the payment methode column, to pick from correct table.
  ||   1.0.3       12-jul-11     Mitul                 Update Site Detail an Summary Query For Site status Active Or Inactive
  ||   1.0.4       21-Aug-12     Dhiresh               Add bank details columns
  |   1.0.5      26-Aug-2021     Vishwa Teja               Added date of creation and date of amendment columns for site detailed ,site summary,header detailed 
  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  ||
  ||
  ========================================================================================================*/ 
   PROCEDURE  GST_ABRL_supplier_master_main (
      errbuff                 OUT   VARCHAR2,
      retcode                 OUT   NUMBER,
      p_report_type                 VARCHAR2,
      p_operating_unit              VARCHAR2,
      p_vendor_type                 VARCHAR2,
      p_from_vendor                 VARCHAR2,
      p_to_vendor                   VARCHAR2,
      p_list_of_vendors             VARCHAR2,
      p_from_terms                  VARCHAR2,
      p_to_terms                    VARCHAR2,
      p_list_of_terms               VARCHAR2,
      p_from_pay_group              VARCHAR2,
      p_to_pay_group                VARCHAR2,
      p_list_of_pay_group           VARCHAR2,
      p_from_prin_vendor            VARCHAR2,
      p_to_prin_vendor              VARCHAR2,
      p_list_of_prin_vendor         VARCHAR2,
      p_term_date                   VARCHAR2,
      p_vendor_category             VARCHAR2,
      p_from_legacy                 VARCHAR2,
      p_to_legacy                   VARCHAR2,
      p_list_of_legacy              VARCHAR2,
      p_status                      VARCHAR2,
      p_resp_id                     VARCHAR2
   )
   IS
   BEGIN
      fnd_file.put_line (fnd_file.output, 'ABRL Supplier Master Report');
      fnd_file.put_line (fnd_file.output, ' ');
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'As on Date'
                         || CHR (9)
                         || SYSDATE
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output, ' ');
      fnd_file.put_line (fnd_file.output, 'REPORT PARAMETRS ');
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Report Type'
                         || CHR (9)
                         || p_report_type
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Operating Unit'
                         || CHR (9)
                         || p_operating_unit
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Vendor Type'
                         || CHR (9)
                         || p_vendor_type
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'From Vendor Number'
                         || CHR (9)
                         || p_from_vendor
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'To Vendor Number'
                         || CHR (9)
                         || p_to_vendor
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Vendor Number List'
                         || CHR (9)
                         || p_list_of_vendors
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'From Payment Terms'
                         || CHR (9)
                         || p_from_terms
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'To Payment Terms'
                         || CHR (9)
                         || p_to_terms
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Payment Terms List'
                         || CHR (9)
                         || p_list_of_terms
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'From Pay Group'
                         || CHR (9)
                         || p_from_pay_group
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'To Pay Group'
                         || CHR (9)
                         || p_to_pay_group
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Pay Group List'
                         || CHR (9)
                         || p_list_of_pay_group
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'From Principal Vendor Number'
                         || CHR (9)
                         || p_from_prin_vendor
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'To Principal Vendor Number'
                         || CHR (9)
                         || p_to_prin_vendor
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Principal Vendor Number List'
                         || CHR (9)
                         || p_list_of_prin_vendor
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Term Date Basis'
                         || CHR (9)
                         || p_term_date
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Primary Vendor Category'
                         || CHR (9)
                         || p_vendor_category
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'From Legacy System Reference'
                         || CHR (9)
                         || p_from_legacy
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'To Legacy System Reference'
                         || CHR (9)
                         || p_to_legacy
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Legacy System Reference List'
                         || CHR (9)
                         || p_list_of_legacy
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Vendor Status'
                         || CHR (9)
                         || p_status
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output, ' ');

      IF p_report_type = 'HEADER'
      THEN
          GST_ABRL_header_proc (p_vendor_type,
                             p_from_vendor,
                             p_to_vendor,
                             p_list_of_vendors,
                             p_from_terms,
                             p_to_terms,
                             p_list_of_terms,
                             p_from_pay_group,
                             p_to_pay_group,
                             p_list_of_pay_group,
                             p_from_prin_vendor,
                             p_to_prin_vendor,
                             p_list_of_prin_vendor,
                             p_vendor_category,
                             p_status
                            );
      ELSIF p_report_type = 'SITE SUMMARY'
      THEN
         GST_ABRL_site_summary_proc (p_vendor_type,
                                   p_operating_unit,
                                   p_from_vendor,
                                   p_to_vendor,
                                   p_list_of_vendors,
                                   p_from_terms,
                                   p_to_terms,
                                   p_list_of_terms,
                                   p_from_pay_group,
                                   p_to_pay_group,
                                   p_list_of_pay_group,
                                   p_from_prin_vendor,
                                   p_to_prin_vendor,
                                   p_list_of_prin_vendor,
                                   p_term_date,
                                   p_vendor_category,
                                   p_from_legacy,
                                   p_list_of_legacy,
                                   p_to_legacy,
                                   p_status,
                                   p_resp_id
                                  );
      ELSE
          GST_ABRL_site_detailed_proc (p_vendor_type,
                                    p_operating_unit,
                                    p_from_vendor,
                                    p_to_vendor,
                                    p_list_of_vendors,
                                    p_from_terms,
                                    p_to_terms,
                                    p_list_of_terms,
                                    p_from_pay_group,
                                    p_to_pay_group,
                                    p_list_of_pay_group,
                                    p_from_prin_vendor,
                                    p_to_prin_vendor,
                                    p_list_of_prin_vendor,
                                    p_term_date,
                                    p_vendor_category,
                                    p_from_legacy,
                                    p_to_legacy,
                                    p_list_of_legacy,
                                    p_status,
                                    p_resp_id
                                   );
      END IF;
   END GST_ABRL_supplier_master_main;

--Header Procedure Layout Starting...
   PROCEDURE GST_ABRL_header_proc (
      p_vendor_type           VARCHAR2,
      p_from_vendor           VARCHAR2,
      p_to_vendor             VARCHAR2,
      p_list_of_vendors       VARCHAR2,
      p_from_terms            VARCHAR2,
      p_to_terms              VARCHAR2,
      p_list_of_terms         VARCHAR2,
      p_from_pay_group        VARCHAR2,
      p_to_pay_group          VARCHAR2,
      p_list_of_pay_group     VARCHAR2,
      p_from_prin_vendor      VARCHAR2,
      p_to_prin_vendor        VARCHAR2,
      p_list_of_prin_vendor   VARCHAR2,
      p_vendor_category       VARCHAR2,
      p_status                VARCHAR2
   )
   IS
      query_string               LONG;
      vendor_type_string         LONG;
      range_ven_string           LONG;
      list_ven_string            LONG;
      range_term_string          LONG;
      list_term_string           LONG;
      range_pay_group_string     LONG;
      list_pay_group_string      LONG;
      range_prin_vendor_string   LONG;
      list_prin_vendor_string    LONG;
      vendor_category_string     LONG;
      status_string              LONG;
      order_by_string            LONG;
      inv_count                  NUMBER;

      TYPE ref_cur IS REF CURSOR;

      c                          ref_cur;

      TYPE c_rec IS RECORD (
         v_vendor_number             ap_suppliers.segment1%TYPE,
         v_vendor_name               ap_suppliers.vendor_name%TYPE,
         v_alternate_vendor_name     ap_suppliers.vendor_name%TYPE,
         v_vendor_type               ap_suppliers.vendor_type_lookup_code%TYPE,
         v_primary_vendor_category   ap_suppliers.global_attribute1%TYPE,
         v_principal_vendor_number  ap_suppliers.segment1%TYPE,
         v_principal_vendor_name     ap_suppliers.vendor_name%TYPE,
         v_legal_status              hz_organization_profiles.legal_status%TYPE,
         v_ceo_name                  hz_organization_profiles.ceo_name%TYPE,
         v_ceo_title                 hz_organization_profiles.ceo_title%TYPE,
         v_status                    VARCHAR2 (20),
         v_pay_group                 po_lookup_codes.displayed_field%TYPE,
         v_payment_terms             ap_terms.NAME%TYPE,
         v_small_business            ap_suppliers.small_business_flag%TYPE,
         v_micr_code                 ap_suppliers.global_attribute2%TYPE,
         v_neft_code                 ap_suppliers.global_attribute3%TYPE,
         v_rtgs_code                 ap_suppliers.global_attribute4%TYPE,
         v_abmc_regisration          ap_suppliers.global_attribute5%TYPE,
         v_drug_license             ap_suppliers.global_attribute6%TYPE,
         v_liquor_license            ap_suppliers.global_attribute7%TYPE,
         v_msmed                     VARCHAR2 (50),
         v_vendor_creation_date       apps.ap_suppliers.CREATION_DATE%TYPE
      );

      v_rec                      c_rec;
   BEGIN
      query_string :=
         'SELECT distinct AA.Vendor_Number,
AA.Vendor_name,
AA.Alternate_Vendor_Name,
AA.Vendor_Type,
AA.Primary_Vendor_Category,
AA.Principal_Vendor_Number,
AA.Principal_Vendor_Name,
AA.LEGAL_STATUS,
AA.CEO_NAME,
AA.CEO_TITLE,
AA.Status,
AA.Pay_group,
AA.Payment_Terms,
AA.Small_Business,
AA.MICR_Code,
AA.NEFT_Code,
AA.RTGS_Code,
AA.ABMC_Regisration,
AA.Drug_License,
AA.Liquor_License,
AA.MSMED,
AA.vendor_creation_date
FROM
(SELECT distinct 
       aps.segment1 Vendor_Number,
       aps.vendor_name Vendor_name,
       aps.VENDOR_NAME_ALT Alternate_Vendor_Name,
       aps.VENDOR_TYPE_LOOKUP_CODE Vendor_Type,
       aps.GLOBAL_ATTRIBUTE1 Primary_Vendor_Category,
       SUP_PARENT.SEGMENT1 Principal_Vendor_Number,
       SUP_PARENT.vendor_name Principal_Vendor_Name,
       hrp.LEGAL_STATUS,
       hrp.CEO_NAME,
       hrp.CEO_TITLE,
       DECODE(SIGN(TRUNC(SYSDATE)-NVL(aps.END_DATE_ACTIVE,TRUNC(SYSDATE))),''0'',''Active'',''-1'',''Active'',''IN Active'') Status,
       po_pay.DISPLAYED_FIELD Pay_group,
       APT.NAME Payment_Terms,
       aps.SMALL_BUSINESS_FLAG Small_Business,
       aps.GLOBAL_ATTRIBUTE2 MICR_Code,
       aps.GLOBAL_ATTRIBUTE3 NEFT_Code,
       aps.GLOBAL_ATTRIBUTE4 RTGS_Code,
       aps.GLOBAL_ATTRIBUTE5 ABMC_Regisration,
       aps.GLOBAL_ATTRIBUTE6 Drug_License,
       aps.GLOBAL_ATTRIBUTE7 Liquor_License,
       DECODE(aps.GLOBAL_ATTRIBUTE3,''N'',''No'',''Y'',''Yes'',APS.GLOBAL_ATTRIBUTE3) MSMED,
       aps.creation_date vendor_creation_date
FROM apps.ap_suppliers aps,
     apps.ap_suppliers SUP_PARENT,
     apps.PO_LOOKUP_CODES po_pay,
     apps.hz_organization_profiles hrp,
     apps.ap_terms APT
WHERE 1=1
     AND aps.PARTY_ID=HRP.PARTY_ID
     AND SUP_PARENT.VENDOR_ID(+)=aps.PARENT_VENDOR_ID
     AND po_pay.LOOKUP_TYPE(+)=''PAY GROUP''
     AND aps.PAY_GROUP_LOOKUP_CODE=po_pay.lookup_code(+)
     AND aps.TERMS_ID=APT.TERM_ID(+)
     )AA
WHERE 1=1';
      list_ven_string :=
                     'AND AA.Vendor_Number IN (' || p_list_of_vendors || ') ';
      range_ven_string :=
            'AND AA.Vendor_Number between '''
         || p_from_vendor
         || ''' AND '''
         || p_to_vendor
         || '''';
      list_term_string :=
                        'AND AA.Payment_Terms IN (' || p_list_of_terms || ') ';
      range_term_string :=
            'AND AA.Payment_Terms between '''
         || p_from_terms
         || ''' AND '''
         || p_to_terms
         || '''';
      vendor_type_string := 'AND AA.Vendor_Type=''' || p_vendor_type || '''';
      list_pay_group_string :=
                         'AND AA.Pay_group IN (' || p_list_of_pay_group || ')';
      range_pay_group_string :=
            'AND AA.Pay_group between '''
         || p_from_pay_group
         || ''' AND '''
         || p_to_pay_group
         || '''';
      list_prin_vendor_string :=
         'AND AA.Principal_Vendor_Number IN (' || p_list_of_prin_vendor || ')';
      range_prin_vendor_string :=
            'AND AA.Principal_Vendor_Number between '''
         || p_from_prin_vendor
         || ''' AND '''
         || p_to_prin_vendor
         || '''';
      vendor_category_string :=
              'AND AA.Primary_Vendor_Category=''' || p_vendor_category || '''';
      status_string := 'AND AA.Status=''' || p_status || '''';
      order_by_string := 'ORDER BY AA.Vendor_Number,AA.Vendor_name';

      -- Checking for Vendor Number Multiple amd Rang Parameter
      IF     (p_from_vendor IS NULL AND p_to_vendor IS NULL)
         AND p_list_of_vendors IS NOT NULL
      THEN
         query_string := query_string || list_ven_string;
      ELSIF     (p_from_vendor IS NOT NULL AND p_to_vendor IS NOT NULL)
            AND p_list_of_vendors IS NULL
      THEN
         query_string := query_string || range_ven_string;
      ELSIF     (p_from_vendor IS NOT NULL AND p_to_vendor IS NOT NULL)
            AND p_list_of_vendors IS NOT NULL
      THEN
         query_string := query_string || list_ven_string;
      END IF;

      -- Checking For Payment Terms Multiple amd Rang Parameter
      IF     (p_from_terms IS NULL AND p_to_terms IS NULL)
         AND p_list_of_terms IS NOT NULL
      THEN
         query_string := query_string || list_term_string;
      ELSIF     (p_from_terms IS NOT NULL AND p_to_terms IS NOT NULL)
            AND p_list_of_terms IS NULL
      THEN
         query_string := query_string || range_term_string;
      ELSIF     (p_from_terms IS NOT NULL AND p_to_terms IS NOT NULL)
            AND p_list_of_terms IS NOT NULL
      THEN
         query_string := query_string || list_term_string;
      END IF;

      -- Checking For Pay Group Multiple amd Rang Parameter
      IF     (p_from_pay_group IS NULL AND p_to_pay_group IS NULL)
         AND p_list_of_pay_group IS NOT NULL
      THEN
         query_string := query_string || list_pay_group_string;
      ELSIF     (p_from_pay_group IS NOT NULL AND p_to_pay_group IS NOT NULL
                )
            AND p_list_of_pay_group IS NULL
      THEN
         query_string := query_string || range_pay_group_string;
      ELSIF     (p_from_pay_group IS NOT NULL AND p_to_pay_group IS NOT NULL
                )
            AND p_list_of_pay_group IS NOT NULL
      THEN
         query_string := query_string || list_pay_group_string;
      END IF;

      -- Checking For Principal Vendor Number Multiple amd Rang Parameter
      IF     (p_from_prin_vendor IS NULL AND p_to_prin_vendor IS NULL)
         AND p_list_of_prin_vendor IS NOT NULL
      THEN
         query_string := query_string || list_prin_vendor_string;
      ELSIF     (    p_from_prin_vendor IS NOT NULL
                 AND p_to_prin_vendor IS NOT NULL
                )
            AND p_list_of_prin_vendor IS NULL
      THEN
         query_string := query_string || range_prin_vendor_string;
      ELSIF     (    p_from_prin_vendor IS NOT NULL
                 AND p_to_prin_vendor IS NOT NULL
                )
            AND p_list_of_prin_vendor IS NOT NULL
      THEN
         query_string := query_string || list_prin_vendor_string;
      END IF;

      -- Checking  For Primary Vendor Category Parameter
      IF (p_vendor_category IS NULL)
      THEN
         query_string := query_string;
      ELSE
         query_string := query_string || vendor_category_string;
      END IF;

      -- Checking  For Vendor Status Parameter
      IF (p_status = 'Active')
      THEN
         query_string := query_string || status_string;
      ELSIF (p_status = 'In Active')
      THEN
         query_string := query_string || status_string;
      ELSE
         query_string := query_string;
      END IF;

      -- Checking  For Vendor Type Parameter
      IF (p_vendor_type IS NULL)
      THEN
         query_string := query_string;
      ELSE
         query_string := query_string || vendor_type_string;
      END IF;

      fnd_file.put_line (fnd_file.output, 'Header Report');
      fnd_file.put_line (fnd_file.output, ' ');
      fnd_file.put_line (fnd_file.output,
                            
                            --'Operating Unit'
                            'Vendor Number'
                         || '^'
                         || 'Vendor Name'
                         || '^'
                         || 'Alternate Vendor Name'
                         || '^'
                         || 'Vendor Type'
                         || '^'
                         || 'Primary Vendor catagory'
                         || '^'
                         || 'Principal Vendor Number'
                         || '^'
                         || 'Principal Vendor Name'
                         || '^'
                         || 'Legal Status'
                         || '^'
                         || 'CEO Name'
                         || '^'
                         || 'CEO Title'
                         || '^'
                         || 'Status'
                         || '^'
                         || 'Pay Group'
                         || '^'
                         || 'Payment Terms'
                         || '^'
                         || 'Small Business'
                         || '^'
                         || 'MICR Code'
                         || '^'
                         || 'NEFT Code'
                         || '^'
                         || 'RTGS Code'
                         || '^'
                         || 'APMC Registration'
                         || '^'
                         || 'Drug License'
                         || '^'
                         || 'Liquor License'
                         || '^'
                         || 'MSMED'
                         || '^'
                         || 'Vendor Creation Date'
                        );
      query_string := query_string || order_by_string;
      fnd_file.put_line (fnd_file.LOG, 'Header string: ' || query_string);
      inv_count := 0;

      OPEN c FOR query_string;

      LOOP
         FETCH c
          INTO v_rec;

         EXIT WHEN c%NOTFOUND;
         fnd_file.put_line (fnd_file.output,
                               v_rec.v_vendor_number
                            || '^'
                            || v_rec.v_vendor_name
                            || '^'
                            || v_rec.v_alternate_vendor_name
                            || '^'
                            || v_rec.v_vendor_type
                            || '^'
                            || v_rec.v_primary_vendor_category
                            || '^'
                            || v_rec.v_principal_vendor_number
                            || '^'
                            || v_rec.v_principal_vendor_name
                            || '^'
                            || v_rec.v_legal_status
                            || '^'
                            || v_rec.v_ceo_name
                            || '^'
                            || v_rec.v_ceo_title
                            || '^'
                            || v_rec.v_status
                            || '^'
                            || v_rec.v_pay_group
                            || '^'
                            || v_rec.v_payment_terms
                            || '^'
                            || v_rec.v_small_business
                            || '^'
                            || v_rec.v_micr_code
                            || '^'
                            || v_rec.v_neft_code
                            || '^'
                            || v_rec.v_rtgs_code
                            || '^'
                            || v_rec.v_abmc_regisration
                            || '^'
                            || v_rec.v_drug_license
                            || '^'
                            || v_rec.v_liquor_license
                            || '^'
                            || v_rec.v_msmed
                            || '^'
                            || to_char(v_rec.v_vendor_creation_date, 'DD-MON-YYYY')
                           );
         inv_count := inv_count + 1;
      END LOOP;

      CLOSE c;
   END GST_ABRL_header_proc;

--Site Summary Procedure Layout Starting...
   PROCEDURE GST_ABRL_site_summary_proc (
      p_vendor_type           VARCHAR2,
      p_operating_unit        VARCHAR2,
      p_from_vendor           VARCHAR2,
      p_to_vendor             VARCHAR2,
      p_list_of_vendors       VARCHAR2,
      p_from_terms            VARCHAR2,
      p_to_terms              VARCHAR2,
      p_list_of_terms         VARCHAR2,
      p_from_pay_group        VARCHAR2,
      p_to_pay_group          VARCHAR2,
      p_list_of_pay_group     VARCHAR2,
      p_from_prin_vendor      VARCHAR2,
      p_to_prin_vendor        VARCHAR2,
      p_list_of_prin_vendor   VARCHAR2,
      p_term_date             VARCHAR2,
      p_vendor_category       VARCHAR2,
      p_from_legacy           VARCHAR2,
      p_to_legacy             VARCHAR2,
      p_list_of_legacy        VARCHAR2,
      p_status                VARCHAR2,
      p_resp_id               VARCHAR2
   )
   IS
      query_string_sum           LONG;
      vendor_type_string         LONG;
      range_ven_string           LONG;
      list_ven_string            LONG;
      range_term_string          LONG;
      list_term_string           LONG;
      range_pay_group_string     LONG;
      list_pay_group_string      LONG;
      range_prin_vendor_string   LONG;
      list_prin_vendor_string    LONG;
      term_date_string           LONG;
      vendor_category_string     LONG;
      status_string              LONG;
      range_legacy_string        LONG;
      list_legacy_string         LONG;
      order_by_string            LONG;
      inv_count1                 NUMBER;
      list_ou_string             LONG;
      list_ledger_wise_ou        LONG;

      TYPE ref_cur1 IS REF CURSOR;

      c_sum                      ref_cur1;

      TYPE c_rec1 IS RECORD (
         v_operating_unit            hr_operating_units.NAME%TYPE,
         v_vendor_number             ap_suppliers.segment1%TYPE,
         v_vendor_name              ap_suppliers.vendor_name%TYPE,
         v_alternate_vendor_name     ap_suppliers.vendor_name_alt%TYPE,
         v_vendor_type               ap_suppliers.vendor_type_lookup_code%TYPE,
         v_primary_vendor_category   ap_suppliers.global_attribute1%TYPE,
         v_principal_vendor_number   ap_suppliers.segment1%TYPE,
         v_site_name                 ap_supplier_sites_all.vendor_site_code%TYPE,
         v_vendor_site_id            ap_supplier_sites_all.vendor_site_id%TYPE,
         v_site_purpose              VARCHAR2 (50),
         v_bill_to_location          hr_locations_all.location_code%TYPE,
         v_ship_to_location          hr_locations_all.location_code%TYPE,
         v_pay_group                 po_lookup_codes.displayed_field%TYPE,
         v_payment_method            iby_external_payees_all.default_payment_method_code%TYPE,
         v_payment_terms             ap_terms.NAME%TYPE,
         v_terms_date                ap_supplier_sites_all.terms_date_basis%TYPE,
         v_pay_date_basis            ap_supplier_sites_all.pay_date_basis_lookup_code%TYPE,
         v_liability_account         gl_code_combinations_kfv.concatenated_segments%TYPE,
         v_prepayment_account        gl_code_combinations_kfv.concatenated_segments%TYPE,
         v_gstin_num                        jai_party_reg_lines_v.REGISTRATION_NUMBER%TYPE,
         v_pan_no                             jai_party_reg_lines_v.registration_number%TYPE,
         v_reporting_code                jai_reporting_associations.reporting_code%TYPE,
         v_tds_vendor_type            jai_reporting_associations.reporting_code_description%TYPE,
         v_status                    VARCHAR2 (30),
         v_navision_code             VARCHAR (240),
         v_organization_id           VARCHAR2 (240),
         v_vendor_creation_date        apps.ap_suppliers.CREATION_DATE%TYPE,
         v_vendor_amendment_date       ap_supplier_sites_all.LAST_UPDATE_DATE%TYPE
         );

      v_rec1                     c_rec1;
   BEGIN
      query_string_sum :=
         'select distinct AA.Operating_Unit,
AA.Vendor_Number,
AA.Vendor_name,
AA.Alternate_Vendor_Name,
AA.Vendor_Type,
AA.Primary_Vendor_Category,
AA.Principal_Vendor_Number,
AA.Site_Name,
AA.vendor_site_id,
AA.Site_Purpose,
AA.Bill_To_Location,
AA.Ship_To_Location,
AA.Pay_group,
AA.Payment_Method,
AA.Payment_Terms,
AA.Terms_Date,
AA.Pay_Date_Basis,
AA.Liability_Account,
AA.Prepayment_Account,
AA.Gstin_Num,
AA.Pan_Num,
AA.Reporting_Code,
AA.Tds_Vendor_type,
AA.Status,
AA.Navision_Code,
AA.organization_id,
AA.vendor_creation_date,
AA.vendor_amendment_date
FROM
(
SELECT distinct hou.NAME Operating_Unit,
       aps.segment1 Vendor_Number,
       aps.vendor_name Vendor_name,
       aps.VENDOR_NAME_ALT Alternate_Vendor_Name,
       aps.VENDOR_TYPE_LOOKUP_CODE Vendor_Type,
       aps.global_attribute1 Primary_Vendor_Category,
       SUP_PARENT.SEGMENT1 Principal_Vendor_Number,
       apsa.VENDOR_SITE_CODE Site_Name,
       apsa.vendor_site_id,
       DECODE(NVL(apsa.PAY_SITE_FLAG,''N'')||'',''||NVL(apsa.RFQ_ONLY_SITE_FLAG,''N'')||'',''||NVL(apsa.PURCHASING_SITE_FLAG,''N''),''Y,Y,Y'',''Payment,Purchasing,RFQ'',
      ''N,Y,N'',''RFQ'',''N,N,Y'',''Purchasing'',''Y,N,N'',''Payment'',''Y,Y,N'',''Payment,RFQ'',''N,Y,Y'',''RFQ,Purchasing'',
       ''Y,N,Y'',''Payment,Purchasing'') Site_Purpose,
       HR_BILL.LOCATION_CODE Bill_To_Location,
       HR_SHIP.LOCATION_CODE Ship_To_Location,
       po_pay.DISPLAYED_FIELD Pay_group,
       iepm.Payment_Method_code Payment_Method,
       APT.NAME Payment_Terms,
       apsa.TERMS_DATE_BASIS Terms_Date,
       apsa.PAY_DATE_BASIS_LOOKUP_CODE Pay_Date_Basis,
       GL_LIA.CONCATENATED_SEGMENTS Liability_Account,
       GL_PRE.CONCATENATED_SEGMENTS Prepayment_Account,
       (SELECT DISTINCT registration_number
                           FROM apps.jai_party_regs jpr,
                                apps.jai_party_reg_lines jprl
                          WHERE 1 = 1
                            AND jpr.party_reg_id = jprl.party_reg_id
                          --  AND jp.party_reg_id = jpr.party_reg_id
                            AND jpr.party_id= aps.vendor_id
                     --       AND jpr.party_site_id = apsa.vendor_site_id
                            --  and jpr.ORG_ID=apsa.ORG_ID
                            AND jprl.registration_type_code = ''GSTIN''
                   --         AND jpr.party_class_code = ''SUPPLIER''
                            and jprl.EFFECTIVE_TO is null
                            and rownum<=1
                                                                 --AND  jpr.REG_CLASS_CODE LIKE ''%THIRD_PARTY%''
               ) gstin_num,
                  (SELECT DISTINCT registration_number
                           FROM apps.jai_party_regs jpr,
                                apps.jai_party_reg_lines jprl
                          WHERE 1 = 1
                            AND jpr.party_reg_id = jprl.party_reg_id
                          ---  AND jp.party_reg_id = jpr.party_reg_id
                            AND jpr.party_id = aps.vendor_id
  -- and apsa.ORG_ID=jpr.ORG_ID
--   AND jpr.party_id=SUP_PARENT.vendor_id
                     --       AND jpr.party_site_id = apsa.vendor_site_id
                            AND jprl.registration_type_code =''PAN''
                        --    AND jpr.party_class_code = ''SUPPLIER''
                            and jprl.EFFECTIVE_TO is null
                            and rownum<=1
                                                                 --  AND  jpr.REG_CLASS_CODE like ''%THIRD_PARTY%''
               ) pan_num,
                        (SELECT DISTINCT jra.reporting_code
           FROM apps.jai_party_regs jpr,
                apps.jai_party_reg_lines jprl,
                apps.jai_reporting_associations jra,
                   apps.jai_regimes jr
          WHERE 1 = 1
            AND jpr.party_reg_id = jprl.party_reg_id
            AND jra.entity_id = jpr.party_reg_id
            AND jpr.party_id= aps.vendor_id
          AND jprl.regime_id = jr.regime_id
       AND jra.regime_id = jr.regime_id
       AND apsa.vendor_site_id=jpr.PARTY_SITE_ID
       AND apsa.org_id=jpr.org_id
             and jprl.EFFECTIVE_TO is null
            and rownum<=1
            AND jra.entity_source_table = ''JAI_PARTY_REGS''
            AND jr.REGIME_CODE=''GST-INDIA''
            AND jpr.supplier_flag = ''Y''
            AND jpr.site_flag = ''Y'')reporting_code,
                (SELECT DISTINCT reporting_code_description
                           FROM apps.jai_reporting_associations jra,
                                apps.jai_party_regs jpr
                          WHERE jpr.party_id = apsa.vendor_id
                            AND jpr.party_site_id = apsa.vendor_site_id
                            AND jpr.org_id = apsa.org_id
                            AND jra.entity_id = jpr.party_reg_id
                            AND rownum<=1
                            AND reporting_type_name = ''Vendor Type''
                            AND entity_source_table = ''JAI_PARTY_REGS'')
                                                              tds_vendor_type,         
       DECODE(SIGN(TRUNC(SYSDATE)-NVL(apsa.INACTIVE_DATE,TRUNC(SYSDATE))),''0'',''Active'',''-1'',''Active'',''IN Active'') Status,
       po_pay.lookup_code,
       apsa.attribute1 Navision_Code,
       hou.organization_id,
       aps.creation_date vendor_creation_date,
       apsa.last_update_date vendor_amendment_date
FROM apps.ap_suppliers aps,
     apps.ap_suppliers SUP_PARENT,
     apps.PO_LOOKUP_CODES po_pay,
     apps.ap_supplier_sites_all apsa,
     apps.IBY_EXTERNAL_PAYEES_ALL  IBY,
     APPS.iby_ext_party_pmt_mthds iepm,
     apps.ap_terms APT,
     apps.hr_locations_all HR_BILL,
     apps.hr_locations_all HR_SHIP,
     apps.hr_operating_units hou,
--    apps.jai_party_regs_v jp,
     apps.gl_code_combinations_kfv GL_LIA,
     apps.gl_code_combinations_kfv GL_PRE
    -- apps.jai_ap_tds_vendor_hdrs JAT
WHERE 1=1
AND aps.vendor_id=apsa.vendor_id
AND SUP_PARENT.VENDOR_ID(+)=aps.PARENT_VENDOR_ID
AND apsa.VENDOR_SITE_ID=IBY.SUPPLIER_SITE_ID(+)
AND IBY.Ext_Payee_id = iepm.ext_pmt_party_id(+)
and iepm.PRIMARY_FLAG(+)=''Y''
AND po_pay.LOOKUP_TYPE(+)=''PAY GROUP''
AND apsa.PAY_GROUP_LOOKUP_CODE=po_pay.lookup_code(+)
--AND aps.vendor_id = jp.party_id(+)
--AND apsa.vendor_site_id(+) = jp.party_site_id
AND apsa.TERMS_ID=APT.TERM_ID(+)
AND apsa.ORG_ID=hou.ORGANIZATION_ID
and hou.ORGANIZATION_ID not in(663,801,821,841,861,881,901,921,941,961,981,83,84,85,86,87,88,89,90,91,92,93,94,95,96)
AND apsa.PREPAY_CODE_COMBINATION_ID=GL_PRE.CODE_COMBINATION_ID(+)
AND apsa.ACCTS_PAY_CODE_COMBINATION_ID=GL_LIA.CODE_COMBINATION_ID(+)
AND apsa.BILL_TO_LOCATION_ID=HR_BILL.LOCATION_ID(+)
AND apsa.SHIP_TO_LOCATION_ID=HR_SHIP.LOCATION_ID(+)
) AA
WHERE 1=1 ';
/*and AA.organization_id IN
(SELECT  ORGANIZATION_ID  FROM
   (
     SELECT ORGANIZATION_ID
     FROM FND_PROFILE_OPTION_VALUES fpop,
     FND_PROFILE_OPTIONS fpo,
     PER_SECURITY_ORGANIZATIONS_V  pso
     WHERE fpop.PROFILE_OPTION_ID =fpo.PROFILE_OPTION_ID
     AND level_value=('||P_RESP_ID||')
     AND pso.security_profile_id=fpop.PROFILE_OPTION_VALUE
     AND PROFILE_OPTION_NAME=''XLA_MO_SECURITY_PROFILE_LEVEL''
     union
     SELECT to_number(fpop.PROFILE_OPTION_VALUE)
     FROM FND_PROFILE_OPTION_VALUES fpop,
     FND_PROFILE_OPTIONS fpo
     WHERE fpop.PROFILE_OPTION_ID =fpo.PROFILE_OPTION_ID
     AND level_value=('||P_RESP_ID||')
     AND PROFILE_OPTION_NAME=''ORG_ID''
   )
  ) ';*/
      list_ledger_wise_ou :=
            ' and AA.organization_id IN
(SELECT  ORGANIZATION_ID  FROM 
   (
     SELECT ORGANIZATION_ID  
     FROM FND_PROFILE_OPTION_VALUES fpop,
     FND_PROFILE_OPTIONS fpo,
     PER_SECURITY_ORGANIZATIONS_V  pso
     WHERE fpop.PROFILE_OPTION_ID =fpo.PROFILE_OPTION_ID
     AND level_value=('
         || p_resp_id
         || ') 
     AND pso.security_profile_id=fpop.PROFILE_OPTION_VALUE
     AND PROFILE_OPTION_NAME=''XLA_MO_SECURITY_PROFILE_LEVEL''
     union
     SELECT to_number(fpop.PROFILE_OPTION_VALUE)
     FROM FND_PROFILE_OPTION_VALUES fpop,
     FND_PROFILE_OPTIONS fpo
     WHERE fpop.PROFILE_OPTION_ID =fpo.PROFILE_OPTION_ID
     AND level_value=('
         || p_resp_id
         || ')  
     AND PROFILE_OPTION_NAME=''ORG_ID''
   ) 
  ) ';
      list_ou_string :=
                     'AND AA.organization_id in (' || p_operating_unit || ') ';
      list_ven_string :=
                      'AND AA.Vendor_Number IN (' || p_list_of_vendors || ') ';
      range_ven_string :=
            'AND AA.Vendor_Number between '''
         || p_from_vendor
         || ''' AND '''
         || p_to_vendor
         || '''';
      list_term_string :=
                        'AND AA.Payment_Terms IN (' || p_list_of_terms || ') ';
      range_term_string :=
            'AND AA.Payment_Terms between '''
         || p_from_terms
         || ''' AND '''
         || p_to_terms
         || '''';
      vendor_type_string := 'AND AA.Vendor_Type=''' || p_vendor_type || '''';
      list_pay_group_string :=
                       'AND AA.lookup_code IN (' || p_list_of_pay_group || ')';
      range_pay_group_string :=
            'AND AA.lookup_code between '''
         || p_from_pay_group
         || ''' AND '''
         || p_to_pay_group
         || '''';
      list_prin_vendor_string :=
         'AND AA.Principal_Vendor_Number, IN (' || p_list_of_prin_vendor
         || ')';
      range_prin_vendor_string :=
            'AND AA.Principal_Vendor_Number between '''
         || p_from_prin_vendor
         || ''' AND '''
         || p_to_prin_vendor
         || '''';
      term_date_string := 'AND AA.Terms_Date=''' || p_term_date || '''';
      vendor_category_string :=
              'AND AA.Primary_Vendor_Category=''' || p_vendor_category || '''';
      status_string := 'AND AA.Status=''' || p_status || '''';
      range_legacy_string :=
            'AND AA.legacy_system between '''
         || p_from_legacy
         || ''' AND '''
         || p_to_legacy
         || '''';
      list_legacy_string :=
                        'AND AA.legacy_system IN (' || p_list_of_legacy || ')';
      order_by_string :=
                  'ORDER BY AA.Operating_Unit,AA.Vendor_Number,AA.Vendor_name';

      IF (p_operating_unit IS NULL)
      THEN
         query_string_sum := query_string_sum || list_ledger_wise_ou;
      ELSE
         query_string_sum := query_string_sum || list_ou_string;
      END IF;

      -- Checking For Vendor Number Multiple amd Rang Parameter
      IF     (p_from_vendor IS NULL AND p_to_vendor IS NULL)
         AND p_list_of_vendors IS NOT NULL
      THEN
         query_string_sum := query_string_sum || list_ven_string;
      ELSIF     (p_from_vendor IS NOT NULL AND p_to_vendor IS NOT NULL)
            AND p_list_of_vendors IS NULL
      THEN
         query_string_sum := query_string_sum || range_ven_string;
      ELSIF     (p_from_vendor IS NOT NULL AND p_to_vendor IS NOT NULL)
            AND p_list_of_vendors IS NOT NULL
      THEN
         query_string_sum := query_string_sum || list_ven_string;
      END IF;

      -- Checking For Payment Terms Multiple amd Rang Parameter
      IF     (p_from_terms IS NULL AND p_to_terms IS NULL)
         AND p_list_of_terms IS NOT NULL
      THEN
         query_string_sum := query_string_sum || list_term_string;
      ELSIF     (p_from_terms IS NOT NULL AND p_to_terms IS NOT NULL)
            AND p_list_of_terms IS NULL
      THEN
         query_string_sum := query_string_sum || range_term_string;
      ELSIF     (p_from_terms IS NOT NULL AND p_to_terms IS NOT NULL)
            AND p_list_of_terms IS NOT NULL
      THEN
         query_string_sum := query_string_sum || list_term_string;
      END IF;

      -- Checking For Pay Group Multiple amd Rang Parameter
      IF     (p_from_pay_group IS NULL AND p_to_pay_group IS NULL)
         AND p_list_of_pay_group IS NOT NULL
      THEN
         query_string_sum := query_string_sum || list_pay_group_string;
      ELSIF     (p_from_pay_group IS NOT NULL AND p_to_pay_group IS NOT NULL
                )
            AND p_list_of_pay_group IS NULL
      THEN
         query_string_sum := query_string_sum || range_pay_group_string;
      ELSIF     (p_from_pay_group IS NOT NULL AND p_to_pay_group IS NOT NULL
                )
            AND p_list_of_pay_group IS NOT NULL
      THEN
         query_string_sum := query_string_sum || list_pay_group_string;
      END IF;

      -- Checking For Principal Vendor Number Multiple amd Rang Parameter
      IF     (p_from_prin_vendor IS NULL AND p_to_prin_vendor IS NULL)
         AND p_list_of_prin_vendor IS NOT NULL
      THEN
         query_string_sum := query_string_sum || list_prin_vendor_string;
      ELSIF     (    p_from_prin_vendor IS NOT NULL
                 AND p_to_prin_vendor IS NOT NULL
                )
            AND p_list_of_prin_vendor IS NULL
      THEN
         query_string_sum := query_string_sum || range_prin_vendor_string;
      ELSIF     (    p_from_prin_vendor IS NOT NULL
                 AND p_to_prin_vendor IS NOT NULL
                )
            AND p_list_of_prin_vendor IS NOT NULL
      THEN
         query_string_sum := query_string_sum || list_prin_vendor_string;
      END IF;

      -- Checking for term Date Basis Parameter
      IF (p_term_date IS NULL)
      THEN
         query_string_sum := query_string_sum;
      ELSE
         query_string_sum := query_string_sum || term_date_string;
      END IF;

      -- Checking  For Primary Vendor Category Parameter
      IF (p_vendor_category IS NULL)
      THEN
         query_string_sum := query_string_sum;
      ELSE
         query_string_sum := query_string_sum || vendor_category_string;
      END IF;

      -- Checking  For Vendor Status Parameter
      IF (p_status = 'Active')
      THEN
         query_string_sum := query_string_sum || status_string;
      ELSIF (p_status = 'In Active')
      THEN
         query_string_sum := query_string_sum || status_string;
      ELSE
         query_string_sum := query_string_sum;
      END IF;

      -- Checking For Legacy System Reference Multiple amd Rang Parameter
      IF     (p_from_legacy IS NULL AND p_to_legacy IS NULL)
         AND p_list_of_legacy IS NOT NULL
      THEN
         query_string_sum := query_string_sum || list_legacy_string;
      ELSIF     (p_from_legacy IS NOT NULL AND p_to_legacy IS NOT NULL)
            AND p_list_of_legacy IS NULL
      THEN
         query_string_sum := query_string_sum || range_legacy_string;
      ELSIF     (p_from_legacy IS NOT NULL AND p_to_legacy IS NOT NULL)
            AND p_list_of_legacy IS NOT NULL
      THEN
         query_string_sum := query_string_sum || list_legacy_string;
      END IF;

      -- Checking  For Vendor Type Parameter
      IF (p_vendor_type IS NULL)
      THEN
         query_string_sum := query_string_sum;
      ELSE
         query_string_sum := query_string_sum || vendor_type_string;
      END IF;

      fnd_file.put_line (fnd_file.output, 'Site Summary');
      fnd_file.put_line (fnd_file.output, ' ');
      fnd_file.put_line (fnd_file.output,
                            'Operating Unit'
                         || '^'
                         || 'Vendor Number'
                         || '^'
                         || 'Vendor Name'
                         || '^'
                         || 'Alternate Vendor Name'
                         || '^'
                         || 'Vendor Type'
                         || '^'
                         || 'Primary Vendor catagory'
                         || '^'
                         || 'Principal Vendor Number'
                         || '^'
                         || 'Site Name'
                         || '^'
                         || 'Vendor Site Id'
                         || '^'
                         || 'Site Purpose'
                         || '^'
                         || 'Bill to Location'
                         || '^'
                         || 'Ship to Location'
                         || '^'
                         || 'Pay Group'
                         || '^'
                         || 'Payment Method'
                         || '^'
                         || 'Payment Terms'
                         || '^'
                         || 'Terms Date'
                         || '^'
                         || 'Pay Date Basis'
                         || '^'
                         || 'Liability Account'
                         || '^'
                         || 'Prepayment Account'
                         || '^'
                         ||'GSTIN Num'
                         || '^'
                         || 'PAN No.'
                         || '^'
                         ||'Reporting Code'
                         ||'^'
                         || 'TDS Vendor Type'
                         || '^' 
                         || 'Status'
                         || '^'
                         || 'Navision_Code'
                         || '^'
                         || 'Vendor Creation Date'
                         || '^'
                         || 'Vendor Amendment Date'
                        );
      query_string_sum := query_string_sum || order_by_string;
      fnd_file.put_line (fnd_file.LOG,
                         'Site Summary string: ' || query_string_sum
                        );
      inv_count1 := 0;

      OPEN c_sum FOR query_string_sum;

      LOOP
         FETCH c_sum
          INTO v_rec1;

         EXIT WHEN c_sum%NOTFOUND;
         fnd_file.put_line (fnd_file.output,
                               v_rec1.v_operating_unit
                            || '^'
                            || v_rec1.v_vendor_number
                            || '^'
                            || v_rec1.v_vendor_name
                            || '^'
                            || v_rec1.v_alternate_vendor_name
                            || '^'
                            || v_rec1.v_vendor_type
                            || '^'
                            || v_rec1.v_primary_vendor_category
                            || '^'
                            || v_rec1.v_principal_vendor_number
                            || '^'
                            || v_rec1.v_site_name
                            || '^'
                            || v_rec1.v_vendor_site_id
                            || '^'
                            || v_rec1.v_site_purpose
                            || '^'
                            || v_rec1.v_bill_to_location
                            || '^'
                            || v_rec1.v_ship_to_location
                            || '^'
                            || v_rec1.v_pay_group
                            || '^'
                            || v_rec1.v_payment_method
                            || '^'
                            || v_rec1.v_payment_terms
                            || '^'
                            || v_rec1.v_terms_date
                            || '^'
                            || v_rec1.v_pay_date_basis
                            || '^'
                            || v_rec1.v_liability_account
                            || '^'
                            || v_rec1.v_prepayment_account
                            || '^'
                            || v_rec1.v_gstin_num  
                            ||'^'
                            || v_rec1.v_pan_no 
                            ||'^'
                            ||v_rec1.v_reporting_code
                             ||'^'
                            ||v_rec1.v_tds_vendor_type
                            || '^'
                            || v_rec1.v_status
                            || '^'
                            || v_rec1.v_navision_code
                            || '^'
                            || to_char(v_rec1.v_vendor_creation_date, 'DD-MON-YYYY')
                            || '^'
                            || to_char(v_rec1.v_vendor_amendment_date, 'DD-MON-YYYY')
                           );
         inv_count1 := inv_count1 + 1;
      END LOOP;

      CLOSE c_sum;
   END GST_ABRL_site_summary_proc;

--Site Detailed Layout Procedure Starting...
   PROCEDURE GST_ABRL_site_detailed_proc (
      p_vendor_type           VARCHAR2,
      p_operating_unit        VARCHAR2,
      p_from_vendor           VARCHAR2,
      p_to_vendor             VARCHAR2,
      p_list_of_vendors       VARCHAR2,
      p_from_terms            VARCHAR2,
      p_to_terms              VARCHAR2,
      p_list_of_terms         VARCHAR2,
      p_from_pay_group        VARCHAR2,
      p_to_pay_group          VARCHAR2,
      p_list_of_pay_group     VARCHAR2,
      p_from_prin_vendor      VARCHAR2,
      p_to_prin_vendor        VARCHAR2,
      p_list_of_prin_vendor   VARCHAR2,
      p_term_date             VARCHAR2,
      p_vendor_category       VARCHAR2,
      p_from_legacy           VARCHAR2,
      p_to_legacy             VARCHAR2,
      p_list_of_legacy        VARCHAR2,
      p_status                VARCHAR2,
      p_resp_id               VARCHAR2
   )
   IS
      query_string_det           LONG;
      order_by_string            LONG;
      range_ven_string           LONG;
      list_ven_string            LONG;
      range_term_string          LONG;
      list_term_string           LONG;
      range_pay_group_string     LONG;
      list_pay_group_string      LONG;
      range_prin_vendor_string   LONG;
      list_prin_vendor_string    LONG;
      term_date_string           LONG;
      vendor_category_string     LONG;
      status_string              LONG;
      list_legacy_string         LONG;
      range_legacy_string        LONG;
      vendor_type_string         LONG;
      inv_count2                 NUMBER;
      list_ou_string             LONG;
      list_ledger_wise_ou        LONG;

      TYPE ref_cur2 IS REF CURSOR;

      c_det                      ref_cur2;

      TYPE c_rec2 IS RECORD (
         v_operating_unit              hr_operating_units.NAME%TYPE,
         v_vendor_number               ap_suppliers.segment1%TYPE,
         v_vendor_name                 ap_suppliers.vendor_name%TYPE,
         v_alternate_vendor_name       ap_suppliers.vendor_name_alt%TYPE,
         v_vendor_type                 ap_suppliers.vendor_type_lookup_code%TYPE,
         v_primary_vendor_category     ap_suppliers.global_attribute1%TYPE,
         v_site_name                   ap_supplier_sites_all.vendor_site_code%TYPE,
         v_vendor_site_id              ap_supplier_sites_all.vendor_site_id%TYPE,
         v_address_line_1              ap_supplier_sites_all.address_line1%TYPE,
         v_address_line_2              ap_supplier_sites_all.address_line2%TYPE,
         v_address_line_3              ap_supplier_sites_all.address_line3%TYPE,
         v_address_line_4              ap_supplier_sites_all.address_line4%TYPE,
         v_city                        ap_supplier_sites_all.city%TYPE,
         v_country                     ap_supplier_sites_all.country%TYPE,
         v_site_purpose                VARCHAR2 (240),
         v_bill_to_location            hr_locations_all.location_code%TYPE,
         v_ship_to_location            hr_locations_all.location_code%TYPE,
         v_invoice_limit               ap_supplier_sites_all.invoice_amount_limit%TYPE,
         v_invoice_tolerance           ap_tolerance_templates.tolerance_name%TYPE,
         v_alternate_pay_site          VARCHAR2 (240),
         v_create_debit_memo_for_rts   ap_supplier_sites_all.create_debit_memo_flag%TYPE,
         v_gapless_invoice_numbering   ap_supplier_sites_all.gapless_inv_num_flag%TYPE,
         v_pay_group                   po_lookup_codes.displayed_field%TYPE,
         v_payment_method              iby_external_payees_all.default_payment_method_code%TYPE,
         v_payment_terms               ap_terms.NAME%TYPE,
         v_terms_date                  ap_supplier_sites_all.terms_date_basis%TYPE,
         v_pay_date_basis              ap_supplier_sites_all.pay_date_basis_lookup_code%TYPE,
         v_payment_priority            ap_supplier_sites_all.payment_priority%TYPE,
         v_payment_hold_all            ap_supplier_sites_all.hold_all_payments_flag%TYPE,
         v_payment_hold_unmatched      ap_supplier_sites_all.hold_unmatched_invoices_flag%TYPE,
         v_payment_hold_unvalidated    ap_supplier_sites_all.hold_future_payments_flag%TYPE,
         v_payment_hold_reason         ap_supplier_sites_all.hold_reason%TYPE,
         v_deduct_bank_charge          VARCHAR2 (240),
         v_always_take_discount        ap_supplier_sites_all.always_take_disc_flag%TYPE,
         v_exclude_freight             ap_supplier_sites_all.exclude_freight_from_discount%TYPE,
         v_fob                         ap_supplier_sites_all.fob_lookup_code%TYPE,
         v_freight_terms               ap_supplier_sites_all.freight_terms_lookup_code%TYPE,
         v_transport_arrangement       ap_supplier_sites_all.shipping_control%TYPE,
         v_liability_account           gl_code_combinations_kfv.concatenated_segments%TYPE,
         v_prepayment_account          gl_code_combinations_kfv.concatenated_segments%TYPE,
         v_bills_payable_account       gl_code_combinations_kfv.concatenated_segments%TYPE,
         v_distribution_set            ap_distribution_sets_all.distribution_set_name%TYPE,
         v_gstin_num                   jai_party_reg_lines.REGISTRATION_NUMBER%TYPE,
         v_tax_region                  hz_party_sites.attribute1%TYPE, 
         v_pan_no                      jai_party_reg_lines.registration_number%TYPE,
         v_reporting_code              jai_reporting_associations.reporting_code%TYPE,
        --  v_tan_no                   jai_party_reg_lines_v.registration_number%TYPE,
          v_tds_vendor_type            jai_reporting_associations.reporting_code_description%TYPE,
         v_contact_name                VARCHAR2 (240),
         v_email_address               VARCHAR2 (240),
         v_phone                       VARCHAR2 (240),
         v_mobile_number               VARCHAR2 (240),
         --   v_Contact_Address          VARCHAR2(240),
         v_status                      VARCHAR (30),
         v_navision_code               VARCHAR (240),
         v_organization_id             VARCHAR2 (240),
         v_bank_name                   ap_supplier_sites_all.attribute2%TYPE,
         v_branch_name                 ap_supplier_sites_all.attribute3%TYPE,
         v_account_num                 ap_supplier_sites_all.attribute4%TYPE,
         v_neft_code                   ap_supplier_sites_all.attribute7%TYPE,
         v_rtgs_code                   ap_supplier_sites_all.attribute8%TYPE,
         v_account_type                ap_supplier_sites_all.attribute9%TYPE,
         v_postal_code                 ap_supplier_sites_all.zip%TYPE,
         v_retek_supplier_number       ap_supplier_sites_all.attribute14%TYPE,
         v_email_address_1             ap_supplier_sites_all.attribute11%TYPE,
         v_email_address_2             ap_supplier_sites_all.attribute12%TYPE,
         v_email_address_3             ap_supplier_sites_all.attribute13%TYPE,
         v_state                       ap_supplier_sites_all.state%TYPE,
         v_MSMED_Type                  ap_supplier_sites_all.global_attribute8%TYPE,
         v_vendor_creation_date        apps.ap_suppliers.CREATION_DATE%TYPE,
         v_vendor_amendment_date       ap_supplier_sites_all.LAST_UPDATE_DATE%TYPE
      );

      v_rec2                     c_rec2;
   BEGIN
      query_string_det :=
         '
SELECT distinct AA.Operating_Unit,
AA.Vendor_Number,
AA.Vendor_name,
AA.Alternate_Vendor_Name,
AA.Vendor_Type,
AA.Primary_Vendor_Category,
AA.Site_Name,
AA.vendor_site_id,
AA.address_line_1,
AA.Address_Line_2,
AA.Address_Line_3,
AA.Address_Line_4,
AA.CITY City,
AA.COUNTRY Country,
AA.Site_Purpose,
AA.Bill_To_Location,
AA.Ship_To_Location,
AA.Invoice_Limit,
AA.Invoice_Tolerance,
AA.Alternate_Pay_Site,
AA.Create_Debit_Memo_For_RTS,
AA.Gapless_Invoice_Numbering,
AA.Pay_group,
AA.Payment_Method,
AA.Payment_Terms,
AA.Terms_Date,
AA.Pay_Date_Basis,
AA.Payment_Priority,
AA.Payment_Hold_All,
AA.Payment_Hold_Unmatched,
AA.Payment_Hold_Unvalidated,
AA.Payment_Hold_reason,
AA.Deduct_Bank_Charge,
AA.Always_Take_Discount,
AA.Exclude_Freight_From_Discount,
AA.FoB,
AA.Freight_Terms,
AA.transport_arrangement,
AA.Liability_Account,
AA.Prepayment_Account,
AA.Bills_Payable_Account,
AA.Distribution_Set,
AA.gstin_num,
AA.tax_region,
AA.pan_num,
AA.reporting_code,
AA.tds_vendor_type,
AA.Contact_Name,
AA.EMAIL_ADDRESS,
AA.Phone_Number,
AA.Mobile_Number,
AA.Status,
AA.Navision_Code,
AA.organization_id,
AA.Bank_Name,
AA.Bank_Branch,
AA.Account_Num,
AA.NEFT_Code,
AA.RTGS_Code,
AA.Account_Type,
AA.postal_code,
AA.Retek_Supplier_Number,
AA.email_address_1,
AA.email_address_2,
AA.email_address_3,aa.state,
AA.MSMED_Type,
AA.vendor_creation_date,
AA.vendor_amendment_date
FROM(
SELECT hou.NAME Operating_Unit,
       aps.segment1 Vendor_Number,
       aps.vendor_name Vendor_name,
       aps.VENDOR_NAME_ALT Alternate_Vendor_Name,
       aps.VENDOR_TYPE_LOOKUP_CODE Vendor_Type,
       aps.GLOBAL_ATTRIBUTE1 Primary_Vendor_Category,
       apsa.VENDOR_SITE_CODE Site_Name,
       apsa.vendor_site_id,
       apsa.address_line1 address_line_1,
       apsa.ADDRESS_LINE2 Address_Line_2,
       apsa.ADDRESS_LINE3 Address_Line_3,
       apsa.ADDRESS_LINE4 Address_Line_4,
       apsa.CITY City,
       apsa.COUNTRY Country,
       DECODE(NVL(apsa.PAY_SITE_FLAG,''N'')||'',''||NVL(apsa.RFQ_ONLY_SITE_FLAG,''N'')||'',''||NVL(apsa.PURCHASING_SITE_FLAG,''N''),''Y,Y,Y'',''Payment,Purchasing,RFQ'',
      ''N,Y,N'',''RFQ'',''N,N,Y'',''Purchasing'',''Y,N,N'',''Payment'',''Y,Y,N'',''Payment,RFQ'',''N,Y,Y'',''RFQ,Purchasing'',
       ''Y,N,Y'',''Payment,Purchasing'') Site_Purpose,
       HR_BILL.LOCATION_CODE Bill_To_Location,
       HR_SHIP.LOCATION_CODE Ship_To_Location,
       apsa.INVOICE_AMOUNT_LIMIT Invoice_Limit,
       APTT.TOLERANCE_NAME Invoice_Tolerance,
       DECODE(apsa.DEFAULT_PAY_SITE_ID,NULL,apsa.VENDOR_SITE_CODE_ALT,apsa.VENDOR_SITE_CODE) Alternate_Pay_Site,
       apsa.CREATE_DEBIT_MEMO_FLAG Create_Debit_Memo_For_RTS,
       apsa.GAPLESS_INV_NUM_FLAG Gapless_Invoice_Numbering,
       po_pay.DISPLAYED_FIELD Pay_group,
       iepm.Payment_Method_code Payment_Method,
       APT.NAME Payment_Terms,
       apsa.TERMS_DATE_BASIS Terms_Date,
       apsa.PAY_DATE_BASIS_LOOKUP_CODE Pay_Date_Basis,
       apsa.PAYMENT_PRIORITY Payment_Priority,
       apsa.HOLD_ALL_PAYMENTS_FLAG Payment_Hold_All,
       apsa.HOLD_UNMATCHED_INVOICES_FLAG Payment_Hold_Unmatched,
       apsa.HOLD_FUTURE_PAYMENTS_FLAG Payment_Hold_Unvalidated,
       apsa.HOLD_REASON Payment_Hold_reason,
       DECODE(NVL(apsa.BANK_CHARGE_BEARER,''N''),''N'',''Supplier/Negotiated'',''S'',''Supplier/Standard'',''I'',''No'',apsa.BANK_CHARGE_BEARER) Deduct_Bank_Charge,
       apsa.ALWAYS_TAKE_DISC_FLAG Always_Take_Discount,
       apsa.EXCLUDE_FREIGHT_FROM_DISCOUNT Exclude_Freight_From_Discount,
       apsa.FOB_LOOKUP_CODE FoB,
       apsa.FREIGHT_TERMS_LOOKUP_CODE Freight_Terms,
       apsa.SHIPPING_CONTROL transport_arrangement,
       GL_LIA.CONCATENATED_SEGMENTS Liability_Account,
       GL_PRE.CONCATENATED_SEGMENTS Prepayment_Account,
       GL_BILL.CONCATENATED_SEGMENTS Bills_Payable_Account,
       APD.DISTRIBUTION_SET_NAME Distribution_Set,
       (SELECT DISTINCT registration_number
                           FROM apps.jai_party_regs  jpr,
                                apps.jai_party_reg_lines jprl
                          WHERE 1 = 1
                            AND jpr.party_reg_id = jprl.party_reg_id
                        --    AND jp.party_reg_id = jpr.party_reg_id
                            AND jpr.party_id= aps.vendor_id
--   AND jpr.party_id=SUP_PARENT.vendor_id
                            AND jpr.party_site_id = apsa.vendor_site_id
                            --  and jpr.ORG_ID=apsa.ORG_ID
                            AND jprl.registration_type_code = ''GSTIN''
                        --    AND jpr.party_class_code = ''SUPPLIER''
                            and jprl.EFFECTIVE_TO is null
                            and rownum<=1
                                                                 --AND  jpr.REG_CLASS_CODE LIKE ''%THIRD_PARTY%''
               ) gstin_num,
                   (SELECT    attribute1  
     FROM   apps.hz_party_sites hps
     where 1=1
     and hps.party_Site_id=apsa.party_Site_id ) tax_region,
                  (SELECT DISTINCT registration_number
                           FROM apps.jai_party_regs jpr,
                                apps.jai_party_reg_lines jprl
                          WHERE 1 = 1
                            AND jpr.party_reg_id = jprl.party_reg_id
                      --      AND jp.party_reg_id = jpr.party_reg_id
                            AND jpr.party_id= aps.vendor_id
  -- and apsa.ORG_ID=jpr.ORG_ID
                    --        AND jpr.party_site_id = apsa.vendor_site_id
                            AND jprl.registration_type_code =''PAN''
                        --    AND jpr.party_class_code = ''SUPPLIER''
                            and jprl.EFFECTIVE_TO is null
                            and rownum<=1
                                                                 --  AND  jpr.REG_CLASS_CODE like ''%THIRD_PARTY%''
               ) pan_num,
            (SELECT DISTINCT jra.reporting_code
           FROM apps.jai_party_regs jpr,
                apps.jai_party_reg_lines jprl,
                apps.jai_reporting_associations jra,
                   apps.jai_regimes jr
          WHERE 1 = 1
            AND jpr.party_reg_id = jprl.party_reg_id
            AND jra.entity_id = jpr.party_reg_id
            AND jpr.party_id= aps.vendor_id
          AND jprl.regime_id = jr.regime_id
       AND jra.regime_id = jr.regime_id
       AND apsa.vendor_site_id=jpr.PARTY_SITE_ID
       AND apsa.org_id=jpr.org_id
             and jprl.EFFECTIVE_TO is null
            and rownum<=1
            AND jra.entity_source_table = ''JAI_PARTY_REGS''
            AND jr.REGIME_CODE=''GST-INDIA''
            AND jpr.supplier_flag = ''Y''
            AND jpr.site_flag = ''Y'')reporting_code,
                (SELECT DISTINCT reporting_code_description
                           FROM apps.jai_reporting_associations jra,
                                apps.jai_party_regs jpr
                          WHERE jpr.party_id = apsa.vendor_id
                            AND jpr.party_site_id = apsa.vendor_site_id
                            AND jpr.org_id = apsa.org_id
                            AND jra.entity_id = jpr.party_reg_id
                            AND rownum<=1
                            AND reporting_type_name = ''Vendor Type''
                            AND entity_source_table = ''JAI_PARTY_REGS'')
                                                              tds_vendor_type,       
       pvc.FIRST_NAME||'' ''||pvc.LAST_NAME Contact_Name,
       apsa.EMAIL_ADDRESS,
       apsa.PHONE Phone_Number,
       apsa.area_code Mobile_Number,
        DECODE(SIGN(TRUNC(SYSDATE)-NVL(apsa.INACTIVE_DATE,TRUNC(SYSDATE))),''0'',''Active'',''-1'',''Active'',''IN Active'') Status,
         po_pay.lookup_code,
         apsa.attribute1 Navision_Code,
         hou.organization_id,
         apsa.ATTRIBUTE2 Bank_Name,  
         apsa.ATTRIBUTE3 Bank_Branch,
         apsa.ATTRIBUTE4 Account_Num,
         apsa.ATTRIBUTE7 NEFT_Code,
         apsa.ATTRIBUTE8 RTGS_Code,
         apsa.ATTRIBUTE9 Account_Type,
         apsa.zip postal_code,
         apsa.ATTRIBUTE14 Retek_Supplier_Number,
         apsa.ATTRIBUTE11 email_address_1,
         apsa.ATTRIBUTE12 email_address_2,
         apsa.ATTRIBUTE13 email_address_3,
         apsa.state,
         apsa.global_attribute8 MSMED_Type,
         aps.creation_date vendor_creation_date,
         apsa.last_update_date vendor_amendment_date
FROM apps.ap_suppliers aps,
     apps.ap_suppliers  SUP_PARENT,
     apps.ap_supplier_sites_all apsa,
     apps.IBY_EXTERNAL_PAYEES_ALL IBY,
     APPS.iby_ext_party_pmt_mthds iepm,
     apps.PO_LOOKUP_CODES po_pay,
     apps.ap_terms APT,
     apps.po_vendor_contacts pvc,
     apps.hr_locations_all HR_BILL,
     apps.hr_locations_all HR_SHIP,
     apps.ap_tolerance_templates APTT,
     apps.gl_code_combinations_kfv GL_LIA,
     apps.gl_code_combinations_kfv GL_PRE,
     apps.gl_code_combinations_kfv GL_BILL,
     apps.ap_distribution_sets_all APD,
     apps.hr_operating_units hou
    -- apps.jai_party_regs  jp
WHERE 1=1
AND aps.vendor_id=apsa.vendor_id
AND SUP_PARENT.VENDOR_ID(+)=aps.PARENT_VENDOR_ID
AND apsa.VENDOR_SITE_ID=IBY.SUPPLIER_SITE_ID(+)
AND IBY.Ext_Payee_id = iepm.ext_pmt_party_id(+)
and iepm.PRIMARY_FLAG(+)=''Y''
AND po_pay.LOOKUP_TYPE(+)=''PAY GROUP''
AND apsa.PAY_GROUP_LOOKUP_CODE=po_pay.lookup_code(+)
--AND aps.vendor_id = jp.party_id(+)
--AND apsa.vendor_site_id(+) = jp.party_site_id
AND apsa.terms_id=APT.term_id(+)
AND apsa.org_id=hou.organization_id
and hou.ORGANIZATION_ID not in(663,801,821,841,861,881,901,921,941,961,981,83,84,85,86,87,88,89,90,91,92,93,94,95,96)
AND apsa.bill_to_location_id=HR_BILL.location_id(+)
AND apsa.ship_to_location_id=HR_SHIP.location_id(+)
AND apsa.tolerance_id=APTT.tolerance_id(+)
AND apsa.prepay_code_combination_id=GL_PRE.code_combination_id(+)
AND apsa.accts_pay_code_combination_id=GL_LIA.code_combination_id(+)
AND apsa.future_dated_payment_ccid=GL_BILL.code_combination_id(+)
AND apsa.distribution_set_id=APD.distribution_set_id(+)
AND apsa.VENDOR_SITE_ID=pvc.VENDOR_SITE_ID(+)
) AA 
WHERE 1=1 ';
/*and AA.organization_id IN
(SELECT  ORGANIZATION_ID  FROM
   (
     SELECT ORGANIZATION_ID
     FROM FND_PROFILE_OPTION_VALUES fpop,
     FND_PROFILE_OPTIONS fpo,
     PER_SECURITY_ORGANIZATIONS_V  pso
     WHERE fpop.PROFILE_OPTION_ID =fpo.PROFILE_OPTION_ID
     AND level_value=('||P_RESP_ID||')
     AND pso.security_profile_id=fpop.PROFILE_OPTION_VALUE
     AND PROFILE_OPTION_NAME=''XLA_MO_SECURITY_PROFILE_LEVEL''
     union
     SELECT to_number(fpop.PROFILE_OPTION_VALUE)
     FROM FND_PROFILE_OPTION_VALUES fpop,
     FND_PROFILE_OPTIONS fpo
     WHERE fpop.PROFILE_OPTION_ID =fpo.PROFILE_OPTION_ID
     AND level_value=('||P_RESP_ID||')
     AND PROFILE_OPTION_NAME=''ORG_ID''
   )
  ) ';*/
------AND AA.Operating_Unit = '''--       || p_operating_unit--      || '''';
      list_ledger_wise_ou :=
            ' and AA.organization_id IN
(SELECT  ORGANIZATION_ID  FROM 
   (
     SELECT ORGANIZATION_ID  
     FROM FND_PROFILE_OPTION_VALUES fpop,
     FND_PROFILE_OPTIONS fpo,
     PER_SECURITY_ORGANIZATIONS_V  pso
     WHERE fpop.PROFILE_OPTION_ID =fpo.PROFILE_OPTION_ID
     AND level_value=('
         || p_resp_id
         || ') 
     AND pso.security_profile_id=fpop.PROFILE_OPTION_VALUE
     AND PROFILE_OPTION_NAME=''XLA_MO_SECURITY_PROFILE_LEVEL''
     union
     SELECT to_number(fpop.PROFILE_OPTION_VALUE)
     FROM FND_PROFILE_OPTION_VALUES fpop,
     FND_PROFILE_OPTIONS fpo
     WHERE fpop.PROFILE_OPTION_ID =fpo.PROFILE_OPTION_ID
     AND level_value=('
         || p_resp_id
         || ')  
     AND PROFILE_OPTION_NAME=''ORG_ID''
   ) 
  ) ';
      list_ou_string :=
                     'AND AA.organization_id in (' || p_operating_unit || ') ';
      list_ven_string :=
                      'AND AA.Vendor_Number IN (' || p_list_of_vendors || ') ';
      range_ven_string :=
            'AND AA.Vendor_Number between '''
         || p_from_vendor
         || ''' AND '''
         || p_to_vendor
         || '''';
      list_term_string :=
                        'AND AA.Payment_Terms IN (' || p_list_of_terms || ') ';
      range_term_string :=
            'AND AA.Payment_Terms between '''
         || p_from_terms
         || ''' AND '''
         || p_to_terms
         || '''';
      vendor_type_string := 'AND AA.Vendor_Type=''' || p_vendor_type || '''';
      list_pay_group_string :=
                       'AND AA.lookup_code IN (' || p_list_of_pay_group || ')';
      range_pay_group_string :=
            'AND AA.lookup_code between '''
         || p_from_pay_group
         || ''' AND '''
         || p_to_pay_group
         || '''';
      list_prin_vendor_string :=
         'AND AA.Principal_Vendor_Number, IN (' || p_list_of_prin_vendor
         || ')';
      range_prin_vendor_string :=
            'AND AA.Principal_Vendor_Number between '''
         || p_from_prin_vendor
         || ''' AND '''
         || p_to_prin_vendor
         || '''';
      term_date_string := 'AND AA.Terms_Date=''' || p_term_date || '''';
      vendor_category_string :=
              'AND AA.Primary_Vendor_Category=''' || p_vendor_category || '''';
      status_string := 'AND AA.Status=''' || p_status || '''';
      range_legacy_string :=
            'AND AA.legacy_system between '''
         || p_from_legacy
         || ''' AND '''
         || p_to_legacy
         || '''';
      list_legacy_string :=
                        'AND AA.legacy_system IN (' || p_list_of_legacy || ')';
      order_by_string :=
                 ' ORDER BY AA.Operating_Unit,AA.Vendor_Number,AA.Vendor_name';

      IF (p_operating_unit IS NULL)
      THEN
         query_string_det := query_string_det || list_ledger_wise_ou;
      ELSE
         query_string_det := query_string_det || list_ou_string;
      END IF;

      IF     (p_from_vendor IS NULL AND p_to_vendor IS NULL)
         AND p_list_of_vendors IS NOT NULL
      THEN
         query_string_det := query_string_det || list_ven_string;
      ELSIF     (p_from_vendor IS NOT NULL AND p_to_vendor IS NOT NULL)
            AND p_list_of_vendors IS NULL
      THEN
         query_string_det := query_string_det || range_ven_string;
      ELSIF     (p_from_vendor IS NOT NULL AND p_to_vendor IS NOT NULL)
            AND p_list_of_vendors IS NOT NULL
      THEN
         query_string_det := query_string_det || list_ven_string;
      END IF;

      -- Checking For Payment Terms Multiple amd Rang Parameter
      IF     (p_from_terms IS NULL AND p_to_terms IS NULL)
         AND p_list_of_terms IS NOT NULL
      THEN
         query_string_det := query_string_det || list_term_string;
      ELSIF     (p_from_terms IS NOT NULL AND p_to_terms IS NOT NULL)
            AND p_list_of_terms IS NULL
      THEN
         query_string_det := query_string_det || range_term_string;
      ELSIF     (p_from_terms IS NOT NULL AND p_to_terms IS NOT NULL)
            AND p_list_of_terms IS NOT NULL
      THEN
         query_string_det := query_string_det || list_term_string;
      END IF;

      -- Checking For Pay Group Multiple amd Rang Parameter
      IF     (p_from_pay_group IS NULL AND p_to_pay_group IS NULL)
         AND p_list_of_pay_group IS NOT NULL
      THEN
         query_string_det := query_string_det || list_pay_group_string;
      ELSIF     (p_from_pay_group IS NOT NULL AND p_to_pay_group IS NOT NULL
                )
            AND p_list_of_pay_group IS NULL
      THEN
         query_string_det := query_string_det || range_pay_group_string;
      ELSIF     (p_from_pay_group IS NOT NULL AND p_to_pay_group IS NOT NULL
                )
            AND p_list_of_pay_group IS NOT NULL
      THEN
         query_string_det := query_string_det || list_pay_group_string;
      END IF;

      -- Checking For Principal Vendor Number Multiple amd Rang Parameter
      IF     (p_from_prin_vendor IS NULL AND p_to_prin_vendor IS NULL)
         AND p_list_of_prin_vendor IS NOT NULL
      THEN
         query_string_det := query_string_det || list_prin_vendor_string;
      ELSIF     (    p_from_prin_vendor IS NOT NULL
                 AND p_to_prin_vendor IS NOT NULL
                )
            AND p_list_of_prin_vendor IS NULL
      THEN
         query_string_det := query_string_det || range_prin_vendor_string;
      ELSIF     (    p_from_prin_vendor IS NOT NULL
                 AND p_to_prin_vendor IS NOT NULL
                )
            AND p_list_of_prin_vendor IS NOT NULL
      THEN
         query_string_det := query_string_det || list_prin_vendor_string;
      END IF;

      -- Checking for term Date Basis Parameter
      IF (p_term_date IS NULL)
      THEN
         query_string_det := query_string_det;
      ELSE
         query_string_det := query_string_det || term_date_string;
      END IF;

      -- Checking  For Primary Vendor Category Parameter
      IF (p_vendor_category IS NULL)
      THEN
         query_string_det := query_string_det;
      ELSE
         query_string_det := query_string_det || vendor_category_string;
      END IF;

      -- Checking  For Vendor Status Parameter
      IF (p_status = 'Active')
      THEN
         query_string_det := query_string_det || status_string;
      ELSIF (p_status = 'In Active')
      THEN
         query_string_det := query_string_det || status_string;
      ELSE
         query_string_det := query_string_det;
      END IF;

      -- Checking For Legacy System Reference Multiple amd Rang Parameter
      IF     (p_from_legacy IS NULL AND p_to_legacy IS NULL)
         AND p_list_of_legacy IS NOT NULL
      THEN
         query_string_det := query_string_det || list_legacy_string;
      ELSIF     (p_from_legacy IS NOT NULL AND p_to_legacy IS NOT NULL)
            AND p_list_of_legacy IS NULL
      THEN
         query_string_det := query_string_det || range_legacy_string;
      ELSIF     (p_from_legacy IS NOT NULL AND p_to_legacy IS NOT NULL)
            AND p_list_of_legacy IS NOT NULL
      THEN
         query_string_det := query_string_det || list_legacy_string;
      END IF;

      -- Checking  For Vendor Type Parameter
      IF (p_vendor_type IS NULL)
      THEN
         query_string_det := query_string_det;
      ELSE
         query_string_det := query_string_det || vendor_type_string;
      END IF;

      fnd_file.put_line (fnd_file.output, 'Site Detailed');
      fnd_file.put_line (fnd_file.output, ' ');
      fnd_file.put_line (fnd_file.output,
                            'Operating Unit'
                         || '^'
                         || 'Vendor Number'
                         || '^'
                         || 'Vendor Name'
                         || '^'
                         || 'Alternate Vendor Name'
                         || '^'
                         || 'Vendor Type'
                         || '^'
                         || 'Primary Vendor catagory'
                         || '^'
                         || 'Site Name'
                         || '^'
                         || 'Vendor site id'
                         || '^'
                         || 'Address Line 1'
                         || '^'
                         || 'Address Line 2'
                         || '^'
                         || 'Address Line 3'
                         || '^'
                         || 'Address Line 4'
                         || '^'
                         || 'City'
                         || '^'
                         || 'State'
                         || '^'
                         || 'Country'
                         || '^'
                         || 'ZIP'
                         || '^'
                         || 'Site Purpose'
                         || '^'
                         || 'Bill to Location'
                         || '^'
                         || 'Ship to Location'
                         || '^'
                         || 'Invoice Limit'
                         || '^'
                         || 'Invoice Tolerance'
                         || '^'
                         || 'Alternate Pay Site'
                         || '^'
                         || 'Create Debit Memo for RTS'
                         || '^'
                         || 'Gapless Invoice Numbering'
                         || '^'
                         || 'Pay Group'
                         || '^'
                         || 'Payment Method'
                         || '^'
                         || 'Payment Terms'
                         || '^'
                         || 'Terms Date'
                         || '^'
                         || 'Pay Date Basis'
                         || '^'
                         || 'Payment Priority'
                         || '^'
                         || 'Payment Hold - All'
                         || '^'
                         || 'Payment Hold - Unmatched'
                         || '^'
                         || 'Payment Hold - Unvalidated'
                         || '^'
                         || 'Payment Hold Reason'
                         || '^'
                         || 'Deduct Bank Charge'
                         || '^'
                         || 'Always Take Discount'
                         || '^'
                         || 'Exclude Freight from Discount'
                         || '^'
                         || 'FoB'
                         || '^'
                         || 'Freight Terms'
                         || '^'
                         || 'Transport Arrangement'
                         || '^'
                         || 'Liability Account'
                         || '^'
                         || 'Prepayment Account'
                         || '^'
                         || 'Bills Payable Account'
                         || '^'
                         || 'Distribution Set'
                         || '^'
                         ||'GSTIN Num'
                         || '^'
                         ||'Tax Region'
                         ||'^'
                         || 'PAN No'
                         ||'^'
                         ||'Reporting Code'
                         ||'^'
                         || 'TDS Vendor Type'
                         || '^' 
                         || 'Contact Name'
                         || '^'
                         || 'E-Mail Address'
                         || '^'
                         || 'Phone Number'
                         || '^'
                         || 'Mobile Number'
                         || '^'
                         || 'Status'
                         || '^'
                         || 'Navision Code'
                         || '^'
                         || 'Vendor Bank Name'
                         || '^'
                         || 'Vendor Branch Name'
                         || '^'
                         || 'Vendor Account Number'
                         || '^'
                         || 'Beneficiary NEFT IFSC Code'
                         || '^'
                         || 'Beneficiary RTGS IFSC Code'
                         || '^'
                         || 'Account Type'
                         || '^'
                         || 'Retek Supplier Number'
                         || '^'
                         || 'Email Address1'
                         || '^'
                         || 'Email Address2'
                         || '^'
                         || 'Email Address3'
                         || '^'
                         || 'Msmed_Type'
                         || '^'
                         || 'Vendor Creation Date'
                         || '^'
                         || 'Vendor Amendment Date'
                        );
      query_string_det := query_string_det || order_by_string;
      fnd_file.put_line (fnd_file.LOG,
                         'Site Detailed string: ' || query_string_det
                        );
      inv_count2 := 0;

      OPEN c_det FOR query_string_det;

      LOOP
         FETCH c_det
          INTO v_rec2;

         EXIT WHEN c_det%NOTFOUND;
         fnd_file.put_line (fnd_file.output,
                               v_rec2.v_operating_unit
                            || '^'
                            || v_rec2.v_vendor_number
                            || '^'
                            || v_rec2.v_vendor_name
                            || '^'
                            || v_rec2.v_alternate_vendor_name
                            || '^'
                            || v_rec2.v_vendor_type
                            || '^'
                            || v_rec2.v_primary_vendor_category
                            || '^'
                            || v_rec2.v_site_name
                            || '^'
                            || v_rec2.v_vendor_site_id
                            || '^'
                            || v_rec2.v_address_line_1
                            || '^'
                            || v_rec2.v_address_line_2
                            || '^'
                            || v_rec2.v_address_line_3
                            || '^'
                            || v_rec2.v_address_line_4
                            || '^'
                            || v_rec2.v_city
                            || '^'
                            || v_rec2.v_state
                            || '^'
                            || v_rec2.v_country
                            || '^'
                            || v_rec2.v_postal_code
                            || '^'
                            || v_rec2.v_site_purpose
                            || '^'
                            || v_rec2.v_bill_to_location
                            || '^'
                            || v_rec2.v_ship_to_location
                            || '^'
                            || v_rec2.v_invoice_limit
                            || '^'
                            || v_rec2.v_invoice_tolerance
                            || '^'
                            || v_rec2.v_alternate_pay_site
                            || '^'
                            || v_rec2.v_create_debit_memo_for_rts
                            || '^'
                            || v_rec2.v_gapless_invoice_numbering
                            || '^'
                            || v_rec2.v_pay_group
                            || '^'
                            || v_rec2.v_payment_method
                            || '^'
                            || v_rec2.v_payment_terms
                            || '^'
                            || v_rec2.v_terms_date
                            || '^'
                            || v_rec2.v_pay_date_basis
                            || '^'
                            || v_rec2.v_payment_priority
                            || '^'
                            || v_rec2.v_payment_hold_all
                            || '^'
                            || v_rec2.v_payment_hold_unmatched
                            || '^'
                            || v_rec2.v_payment_hold_unvalidated
                            || '^'
                            || v_rec2.v_payment_hold_reason
                            || '^'
                            || v_rec2.v_deduct_bank_charge
                            || '^'
                            || v_rec2.v_always_take_discount
                            || '^'
                            || v_rec2.v_exclude_freight
                            || '^'
                            || v_rec2.v_fob
                            || '^'
                            || v_rec2.v_freight_terms
                            || '^'
                            || v_rec2.v_transport_arrangement
                            || '^'
                            || v_rec2.v_liability_account
                            || '^'
                            || v_rec2.v_prepayment_account
                            || '^'
                            || v_rec2.v_bills_payable_account
                            || '^'
                            || v_rec2.v_distribution_set
                            || '^'
                            || v_rec2.v_gstin_num  
                            ||'^'
                            ||v_rec2.v_tax_region
                            ||'^'
                            || v_rec2.v_pan_no 
                            ||'^'
                            ||v_rec2.v_reporting_code
                            ||'^'
                            ||v_rec2.v_tds_vendor_type
                            || '^'
                            || v_rec2.v_contact_name
                            || '^'
                            || v_rec2.v_email_address
                            || '^'
                            || v_rec2.v_phone
                            || '^'
                            || v_rec2.v_mobile_number
                            || '^'
                            || v_rec2.v_status
                            || '^'
                            || v_rec2.v_navision_code
                            || '^'
                            || v_rec2.v_bank_name
                            || '^'
                            || v_rec2.v_branch_name
                            || '^'
                            || v_rec2.v_account_num
                            || '^'
                            || v_rec2.v_neft_code
                            || '^'
                            || v_rec2.v_rtgs_code
                            || '^'
                            || v_rec2.v_account_type
                            || '^'
                            || v_rec2.v_retek_supplier_number
                            || '^'
                            || v_rec2.v_email_address_1
                            || '^'
                            || v_rec2.v_email_address_2
                            || '^'
                            || v_rec2.v_email_address_3
                            || '^'
                            || v_rec2.v_MSMED_Type
                            || '^'
                            || to_char(v_rec2.v_vendor_creation_date, 'DD-MON-YYYY')
                            || '^'
                            || to_char(v_rec2.v_vendor_amendment_date, 'DD-MON-YYYY')
                           );
         inv_count2 := inv_count2 + 1;
       /*
      Fnd_File.put_line (Fnd_File.LOG,
                             'count=: '||inv_count2|| '^' || 'Vendor Number='||v_rec2.v_Vendor_Number
                             || '^' ||'Address 4 = '||v_rec2.v_address_line_3
                            );
      */
      END LOOP;

      CLOSE c_det;
   END GST_ABRL_site_detailed_proc;
END GST_ABRL_supplier_master_pkg; 
/


CREATE OR REPLACE Package APPS.XXABRL_APNAV_PKG_INV_Interface IS
 PROCEDURE INVOICE_VALIDATE(Errbuf           OUT VARCHAR2
                  ,RetCode          OUT NUMBER
                  ,P_Org_Id        In  NUMBER
                  ,P_Data_Source    IN  VARCHAR2);
 PROCEDURE INVOICE_INSERT( P_Org_Id        IN  NUMBER
                  ,P_Data_Source    IN  VARCHAR2
                   );

 FUNCTION ACCOUNT_SEG_STATUS(P_Seg_Value IN Varchar2,
                      P_Seg_Desc  IN Varchar2) return Varchar2;


END XXABRL_APNAV_PKG_INV_Interface; 
/


CREATE OR REPLACE PACKAGE APPS.xxabrl_insert_vendor_v2_pkg
AS
   PROCEDURE main (errbuf OUT VARCHAR2, retcode OUT NUMBER, p_action VARCHAR2);

   PROCEDURE xxabrl_insert_vendor_info;

   PROCEDURE validate_vendor_info (retcode OUT NUMBER);

   PROCEDURE validate_terms_name (
      p_terms_name   IN       VARCHAR2,
      v_error_smsg   OUT      VARCHAR2
   );

  /* PROCEDURE validate_terms_basis (
      p_terms_basis   IN       VARCHAR2,
      v_error_smsg    OUT      VARCHAR2
   );*/ ---Commented for Loading the Merchandise Vendors

   PROCEDURE validate_state_name (
      p_state_name   IN       VARCHAR2,
      v_error_smsg   OUT      VARCHAR2
   );

   PROCEDURE validate_vendor_number (
      p_vendor_number   IN       VARCHAR2,
      v_error_hmsg      OUT      VARCHAR2
   );

   PROCEDURE validate_vendor_name (
      p_vendor_name   IN       VARCHAR2,
      v_error_hmsg    OUT      VARCHAR2
   );

   PROCEDURE validate_vendor_site_code (
      p_vendor_site_code   IN       VARCHAR2,
      p_vendor_name        IN       VARCHAR2,
      p_operating_unit     IN       VARCHAR2,
      v_error_smsg         OUT      VARCHAR2
   );

   PROCEDURE validate_vendor_type (
      p_vendor_type_lookup_code   IN       VARCHAR2,
      v_error_hmsg                OUT      VARCHAR2
   );

   PROCEDURE validate_operating_unit (
      p_operating_unit   IN       VARCHAR2,
      v_error_smsg       OUT      VARCHAR2
   );

   PROCEDURE validate_country_code (
      p_country_code   IN       VARCHAR2,
      v_error_smsg     OUT      VARCHAR2
   );

   PROCEDURE validate_payment_method (
      p_pay_method_code   IN       VARCHAR2,
      v_error_smsg        OUT      VARCHAR2
   );

  /* PROCEDURE validate_pur_site_flag (
      p_operating_unit    IN       VARCHAR2,
      p_vendor_name       IN       VARCHAR2,
      v_error_smsg        OUT      VARCHAR2
   );

   PROCEDURE validate_pay_site_flag (
      p_operating_unit    IN       VARCHAR2,
      p_vendor_name     IN       VARCHAR2,
      v_error_smsg        OUT      VARCHAR2
   );

   PROCEDURE validate_ret_site_flag (
      p_operating_unit    IN       VARCHAR2,
      p_vendor_name     IN       VARCHAR2,
      v_error_smsg        OUT      VARCHAR2
   );

   PROCEDURE validate_ho_site_flag (
      p_operating_unit    IN       VARCHAR2,
      p_vendor_name        IN       VARCHAR2,
      p_pur_site_flag      IN       VARCHAR2 default null,
      p_pay_site_flag      IN       VARCHAR2 default null,
      p_ret_site_flag      IN       VARCHAR2 default null,
      v_error_smsg         OUT      VARCHAR2
   );*/--Commented for Loading the Merchandise Vendors

   PROCEDURE validate_pay_group (
      p_pay_group    IN       VARCHAR2,
      x_pay_group    OUT      VARCHAR2,
      v_error_smsg   OUT      VARCHAR2
   );

   PROCEDURE derive_shit_to_location_id (
      p_ship_to_loc_code   IN       VARCHAR2,
      p_ship_to_loc_id     OUT      NUMBER,
      v_error_smsg         OUT      VARCHAR2
   );

   PROCEDURE derive_employee_id (
      p_vendor_number   IN       VARCHAR2,
      p_employee_id     OUT      NUMBER,
      v_error_hmsg      OUT      VARCHAR2
   );

   PROCEDURE derive_bill_to_location_id (
      p_bill_to_loc_code   IN       VARCHAR2,
      p_bill_to_loc_id     OUT      NUMBER,
      v_error_smsg         OUT      VARCHAR2
   );

   PROCEDURE derive_term_id (
      p_term_name    IN       VARCHAR2,
      p_term_id      OUT      NUMBER,
      v_error_smsg   OUT      VARCHAR2
   );
END xxabrl_insert_vendor_v2_pkg; 
/


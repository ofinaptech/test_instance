CREATE OR REPLACE PROCEDURE APPS.xxabrl_vendor_sites_import (
   errbuf               OUT      VARCHAR2,
   retcode              OUT      NUMBER,
   xx_org_id            IN       NUMBER,
   xx_party_site_flag   IN       VARCHAR2
)
IS
   v_org_name   VARCHAR2 (100);
BEGIN
   BEGIN
      SELECT NAME
        INTO v_org_name
        FROM apps.hr_operating_units
       WHERE organization_id = xx_org_id;
   EXCEPTION
      WHEN OTHERS
      THEN
         fnd_file.put_line (fnd_file.LOG,
                            'error in Org_id :' || SQLCODE || ':' || SQLERRM
                           );
   END;

   BEGIN
      IF UPPER (xx_party_site_flag) = 'N'
      THEN
         --- SItes inserted query-----
         INSERT INTO apps.ap_supplier_sites_int
                     (vendor_site_interface_id, vendor_id, vendor_site_code,
                      address_line1, address_line2, address_line3,
                      address_line4, city, state, country, zip, attribute5,
                      attribute6, area_code, phone, fax_area_code, fax,
                      payment_method_lookup_code, terms_date_basis,
                      terms_name, purchasing_site_flag, pay_site_flag,
                      org_id, operating_unit_name, attribute_category,
                      attribute14, attribute15, location_id, creation_date,
                      attribute8, attribute7, global_attribute2,
                      global_attribute5, global_attribute6,
                      global_attribute7, global_attribute8, attribute1,
                      attribute2, attribute3, attribute4, attribute9,
                      attribute10, attribute11, attribute12, attribute13,
                      vendor_site_code_alt, pay_group_lookup_code,
                      match_option, invoice_currency_code,
                      hold_all_payments_flag, hold_unmatched_invoices_flag,
                      hold_future_payments_flag, hold_reason)
            SELECT apps.ap_supplier_sites_int_s.NEXTVAL, asl.vendor_id,
                   vendor_site_code, RTRIM (address_line1),
                   RTRIM (address_line2), RTRIM (address_line3),
                   RTRIM (address_line4), RTRIM (city), RTRIM (state),
                   RTRIM (country), zip, asl.attribute5, asl.attribute6,
                   area_code, phone, fax_area_code, fax,
                   ieppm.payment_method_code, asl.terms_date_basis,
                   NVL (ap.NAME, 'D999'), purchasing_site_flag,
                   pay_site_flag, xx_org_id, v_org_name,
                   
                   --  change the new ou name and  org_id
                   asl.attribute_category, asl.attribute14, asl.attribute15,
                   hz.location_id, SYSDATE, asl.attribute8, asl.attribute7,
                   asl.global_attribute2, asl.global_attribute5,
                   asl.global_attribute6, asl.global_attribute7,
                   asl.global_attribute8, asl.attribute1, asl.attribute2,
                   asl.attribute3, asl.attribute4, asl.attribute9,
                   asl.attribute10, asl.attribute11, asl.attribute12,
                   asl.attribute13, asl.vendor_site_code_alt,
                   asl.pay_group_lookup_code, asl.match_option,
                   asl.invoice_currency_code, asl.hold_all_payments_flag,
                   asl.hold_unmatched_invoices_flag,
                   asl.hold_future_payments_flag, asl.hold_reason
              FROM apps.ap_suppliers asp,
                   apps.ap_supplier_sites_all asl,
                   apps.hr_operating_units hr,
                   apps.ap_terms ap,
                   apps.hz_party_sites hz,
                   (SELECT supplier_site_id, ext_payee_id
                      FROM apps.iby_external_payees_all) iepa,
                   (SELECT payment_method_code, ext_pmt_party_id,
                           primary_flag
                      FROM apps.iby_ext_party_pmt_mthds) ieppm
             WHERE 1 = 1
--  AND substr(segment1,1,1) ='9'
               AND asl.vendor_id = asp.vendor_id
       --  AND asl.org_id = 1201                              ---  change the ou
      --AND vendor_type_lookup_code = 'NON MERCHANDISE'
--   and asp.segment1 in (  )
               AND asl.vendor_site_id IN (
                                         SELECT vendor_site_id
                                           FROM xxabrl.xxabrl_supplier_sites_stg
                                          WHERE status_flag IS NULL)
               AND asl.org_id = hr.organization_id
               AND ap.term_id(+) = asp.terms_id
               AND hz.location_id(+) = asl.location_id
               AND hz.status(+) = 'A'
               AND hz.party_site_id(+) = asl.party_site_id
               AND iepa.supplier_site_id(+) = asl.vendor_site_id
               AND ieppm.ext_pmt_party_id(+) = iepa.ext_payee_id
               AND ieppm.primary_flag(+) = 'Y'
               AND asp.end_date_active IS NULL
               AND asl.inactive_date IS NULL;
      ELSE
         --- SItes inserted query-----
         INSERT INTO apps.ap_supplier_sites_int
                     (vendor_site_interface_id, vendor_id, vendor_site_code,
                      address_line1, address_line2, address_line3,
                      address_line4, city, state, country, zip, attribute5,
                      attribute6, area_code, phone, fax_area_code, fax,
                      payment_method_lookup_code, terms_date_basis,
                      terms_name, purchasing_site_flag, pay_site_flag,
                      org_id, operating_unit_name, attribute_category,
                      attribute14, attribute15, location_id, creation_date,
                      attribute8, attribute7, global_attribute2,
                      global_attribute5, global_attribute6,
                      global_attribute7, global_attribute8, attribute1,
                      attribute2, attribute3, attribute4, attribute9,
                      attribute10, attribute11, attribute12, attribute13,
                      vendor_site_code_alt, pay_group_lookup_code,
                      match_option, invoice_currency_code,
                      hold_all_payments_flag, hold_unmatched_invoices_flag,
                      hold_future_payments_flag, hold_reason, party_site_id)
            SELECT apps.ap_supplier_sites_int_s.NEXTVAL, asl.vendor_id,
                   vendor_site_code, RTRIM (address_line1),
                   RTRIM (address_line2), RTRIM (address_line3),
                   RTRIM (address_line4), RTRIM (city), RTRIM (state),
                   RTRIM (country), zip, asl.attribute5, asl.attribute6,
                   area_code, phone, fax_area_code, fax,
                   ieppm.payment_method_code, asl.terms_date_basis,
                   NVL (ap.NAME, 'D999'), purchasing_site_flag,
                   pay_site_flag, xx_org_id, v_org_name,
                   
                   --  change the new ou name and  org_id
                   asl.attribute_category, asl.attribute14, asl.attribute15,
                   hz.location_id, SYSDATE, asl.attribute8, asl.attribute7,
                   asl.global_attribute2, asl.global_attribute5,
                   asl.global_attribute6, asl.global_attribute7,
                   asl.global_attribute8, asl.attribute1, asl.attribute2,
                   asl.attribute3, asl.attribute4, asl.attribute9,
                   asl.attribute10, asl.attribute11, asl.attribute12,
                   asl.attribute13, asl.vendor_site_code_alt,
                   asl.pay_group_lookup_code, asl.match_option,
                   asl.invoice_currency_code, asl.hold_all_payments_flag,
                   asl.hold_unmatched_invoices_flag,
                   asl.hold_future_payments_flag, asl.hold_reason,
                   asl.party_site_id
              FROM apps.ap_suppliers asp,
                   apps.ap_supplier_sites_all asl,
                   apps.hr_operating_units hr,
                   apps.ap_terms ap,
                   apps.hz_party_sites hz,
                   (SELECT supplier_site_id, ext_payee_id
                      FROM apps.iby_external_payees_all) iepa,
                   (SELECT payment_method_code, ext_pmt_party_id,
                           primary_flag
                      FROM apps.iby_ext_party_pmt_mthds) ieppm
             WHERE 1 = 1
--  AND substr(segment1,1,1) ='9'
               AND asl.vendor_id = asp.vendor_id
       --  AND asl.org_id = 1201                              ---  change the ou
      --AND vendor_type_lookup_code = 'NON MERCHANDISE'
--   and asp.segment1 in (  )
               AND asl.vendor_site_id IN (
                                         SELECT vendor_site_id
                                           FROM xxabrl.xxabrl_supplier_sites_stg
                                          WHERE status_flag IS NULL)
               AND asl.org_id = hr.organization_id
               AND ap.term_id(+) = asp.terms_id
               AND hz.location_id(+) = asl.location_id
               AND hz.status(+) = 'A'
               AND hz.party_site_id(+) = asl.party_site_id
               AND iepa.supplier_site_id(+) = asl.vendor_site_id
               AND ieppm.ext_pmt_party_id(+) = iepa.ext_payee_id
               AND ieppm.primary_flag(+) = 'Y'
               AND asp.end_date_active IS NULL
               AND asl.inactive_date IS NULL;
      END IF;

      COMMIT;

      UPDATE xxabrl.xxabrl_supplier_sites_stg
         SET status_flag = 'Y'
       WHERE TRUNC (creation_date) = TRUNC (SYSDATE) AND status_flag IS NULL;

      fnd_file.put_line (fnd_file.LOG,
                         ' INSERTED SUCCESSFULLY:' || SQLCODE || ':'
                         || SQLERRM
                        );
      COMMIT;
   END;
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      fnd_file.put_line (fnd_file.LOG,
                         'error in insert :' || SQLCODE || ':' || SQLERRM
                        );
END; 
/


CREATE OR REPLACE PACKAGE BODY APPS.XXABRL_PAYMENT_BATCH_PKG IS
  Procedure Print_log(
                      p_str IN Varchar2, 
                      p_debug_flag in Varchar2
                      )
  AS
  Begin
       if p_debug_flag = 'Y' then
          Fnd_File.PUT_LINE(Fnd_File.LOG,p_str);
       End if;
  End;                      
  

  Procedure MAIN(P_Errbuf      OUT VARCHAR2,
                 P_RetCode     OUT NUMBER,
                 p_vendor_id   IN VARCHAR2,
                 p_start_date  IN Date,
                 p_end_date    IN Date,
                 p_org_code    IN VARCHAR2,
                 p_org_id      IN NUMBER,
                 p_checkrun_id IN VARCHAR2) As
  Begin
  
    LOAD_GLOBAL_TABLE(p_start_date,
                      p_end_date,
                      p_org_id,
                      p_checkrun_id,
                      p_vendor_id);
                      
    DISPLAY_DATA('Y',p_checkrun_id);
  
  End;

  Procedure DISPLAY_DATA (p_debug_flag IN Varchar2, p_checkrun_id IN VARCHAR2) As
    cursor cur_gt is
      select 
      Decode(tt.c_transaction_type,'N',NULL,'R',NULL,tt.C_ADDRESS_LINE1) CF_ADDRESS_LINE1,
      Decode(tt.c_transaction_type,'N',NULL,'R',NULL,tt.C_ADDRESS_LINE2) CF_ADDRESS_LINE2,
      Decode(tt.c_transaction_type,'N',NULL,'R',NULL,tt.C_ADDRESS_LINE3) CF_ADDRESS_LINE3,
      Decode(tt.c_transaction_type,'N',NULL,'R',NULL,tt.C_ADDRESS_LINE4) CF_ADDRESS_LINE4,
      tt.* 
      from XXABRL_PAY_BATCH_DATA_GT tt;
      
    vc_line_str      varchar2(3000);
    CF_BANK_VALID    varchar2(250);
    CF_ACCT_NUM      varchar2(250);
    CF_BRANCH_VALID  varchar2(250);
    CF_EMAIL         varchar2(250);
    CF_MICR_Code     varchar2(250);
    CF_IFC_Code      varchar2(250);
    CF_NEFT_Code     varchar2(250);
    CF_RTGS_Code     varchar2(250);
    CF_Payment1      varchar2(2999);
    CF_Payment2      varchar2(2999);
    CF_Payment3      varchar2(2999);
    CF_Address5      varchar2(2999);
    CF_TRANSACTION_TYPE varchar2(2999);
    
  Begin
  
    fnd_file.put_line(fnd_file.output, gc_header_str);     
    For v_cur_gt in cur_gt Loop
    
    Print_log('------------------------------',p_debug_flag);
    Print_log('',p_debug_flag);    
    Print_log('Start Display_Data for check# '||v_cur_gt.check_id,p_debug_flag);    
    --### Derive Beneficiary Details from DFF 
    -----------------------------------------
    begin
      select pvsa.attribute2, pvsa.attribute3 ,pvsa.attribute4
        INTO CF_BANK_VALID , CF_BRANCH_VALID , CF_ACCT_NUM
        from ap_checks ch, po_vendor_sites_all pvsa
       where ch.vendor_site_id = pvsa.VENDOR_SITE_ID
         and pvsa.ATTRIBUTE_CATEGORY = 'Supplier Site Additional Info'
         and ch.check_id = v_cur_gt.CHECK_ID
         and rownum = 1;

    Exception

    When no_data_found then
         Print_log('DFF-Account_Name not defined for check# '||v_cur_gt.C_CHECK_NUMBER,p_debug_flag);
    When Too_many_rows then
         Print_log('DFF-Multiple Account_Name defined for check# '||v_cur_gt.C_CHECK_NUMBER,p_debug_flag);
    When others then
         Print_log('DFF-Exception in Account_Name for check# '||v_cur_gt.C_CHECK_NUMBER,p_debug_flag);           
    End;
    -----------------------------------------
    -----------------------------------------
    

    --### Derive Beneficiary email id 
    -----------------------------------------
    begin
      select pvc.email_address, pvc.AREA_CODE||' '||pvc.phone
        into CF_EMAIL , CF_Address5
        from apps.ap_checks           ch,
             apps.po_vendor_sites_all pvsa,
             po_vendor_contacts       pvc
       where ch.vendor_site_id = pvsa.VENDOR_SITE_id
         and pvsa.VENDOR_SITE_ID = pvc.VENDOR_SITE_ID
         and ch.check_id = v_cur_gt.CHECK_ID
         and rownum = 1;
    
    Exception
    
      When no_data_found then
        Print_log('DFF-Email not defined for check# ' ||
                  v_cur_gt.C_CHECK_NUMBER,
                  p_debug_flag);
      When Too_many_rows then
        Print_log('DFF-Multiple Email defined for check# ' ||
                  v_cur_gt.C_CHECK_NUMBER,
                  p_debug_flag);
      When others then
        Print_log('DFF-Exception in Email for check# ' ||
                  v_cur_gt.C_CHECK_NUMBER,
                  p_debug_flag);
    End;
    -----------------------------------------
    -----------------------------------------
    
    --### Derive Beneficiary Details from DFF 
    -----------------------------------------
    CF_TRANSACTION_TYPE :=v_cur_gt.C_TRANSACTION_TYPE;
    
    begin
      select pv.global_attribute2, pv.global_attribute3 ,pv.global_attribute4
        INTO CF_MICR_Code , CF_NEFT_Code , CF_RTGS_Code
        from po_vendors pv
       where pv.vendor_id = v_cur_gt.vendor_id
         and pv.GLOBAL_ATTRIBUTE_CATEGORY = 'ABRL Supplier Additional Info'
         and rownum = 1;
         
    Exception

    When no_data_found then
         Print_log('ABRL Supplier Additional Info not defined for check# '||v_cur_gt.C_CHECK_NUMBER,p_debug_flag);
    When Too_many_rows then
         Print_log('Multiple ABRL Supplier Additional Infodefined for check# '||v_cur_gt.C_CHECK_NUMBER,p_debug_flag);
    When others then
         Print_log('Exception ABRL Supplier Additional Info for check# '||v_cur_gt.C_CHECK_NUMBER,p_debug_flag);           
    End;
    
     --NEFT    
     if to_number(v_cur_gt.C_AMOUNT) < 100000 then
        CF_IFC_Code := CF_NEFT_Code;
        select 
               decode(CF_TRANSACTION_TYPE,'D','D','C','C','N') 
               into 
               CF_TRANSACTION_TYPE 
               from dual;
     --RTGS   
     elsif to_number(v_cur_gt.C_AMOUNT) >= 100000 then
        CF_IFC_Code := CF_RTGS_Code;            
        select 
               decode(CF_TRANSACTION_TYPE,'D','D','C','C','R') 
               into 
               CF_TRANSACTION_TYPE 
               from dual;
     End if;  
     
     if CF_TRANSACTION_TYPE in ('C','D') then
        
        CF_ACCT_NUM     :=NULL;
        CF_BANK_VALID   :=NULL;
        CF_BRANCH_VALID :=NULL;
        CF_ACCT_NUM     :=NULL;
     
     Else
         CF_Address5 :=NULL;
         
     End if;
     
     Print_log('CF_IFC_Code: ' ||CF_IFC_Code,p_debug_flag); 
    
    -----------------------------------------
    -----------------------------------------    

    Begin
    
        For v_pay_cur in (
                          select ais.invoice_num,
                                 ais.invoice_date,
                                 (select reference_1
                                    from ap_invoice_lines_all aisl
                                   where aisl.invoice_id = ais.invoice_id
                                   group by reference_1) reference_1
                            from ap_checks                        ch,
                                 ap_lookup_codes                  lk2,
                                 fnd_territories_vl               ft,
                                 po_vendors                       pv,
                                 org_organization_definitions     haou,
                                 ap_invoice_payments              aip,
                                 ap_invoices                      ais,
                                 iby_payment_methods_vl           iby1,
                                 AP.AP_INV_SELECTION_CRITERIA_ALL aisc,
                                 po_vendor_sites_all              pvs
                           where pvs.vendor_site_id = ch.vendor_site_id
                             and ch.payment_type_flag not in ('R')
                             and ch.country = ft.territory_code(+)
                             and lk2.lookup_type = 'CHECK STATE'
                             and lk2.lookup_code = ch.status_lookup_code
                             and aip.invoice_id = ais.invoice_id
                             and ch.check_id =  aip.check_id
                             and ch.org_id = haou.organization_id
                             --and ch.org_id = 141
                             and ch.checkrun_id = NVL(p_checkrun_id, ch.checkrun_id)
                             and ch.vendor_id = NVL(NULL, ch.vendor_id)
                             and ais.payment_status_flag = 'Y'
                             and pv.vendor_id = ch.vendor_id
                             and iby1.payment_method_code(+) = ch.payment_method_code
                             and aisc.checkrun_id = ch.checkrun_id
                             and aip.check_id = v_cur_gt.CHECK_ID
                            )
          Loop
          
              --select decode(v_pay_cur.invoice_num,NULL,CF_Payment1,Decode(length(CF_Payment1),0,v_pay_cur.invoice_num,CF_Payment1||','||v_pay_cur.invoice_num)) into CF_Payment1 from dual;
              --select decode(v_pay_cur.invoice_date,NULL,CF_Payment2,Decode(length(CF_Payment2),0,v_pay_cur.invoice_date,CF_Payment2||','||v_pay_cur.invoice_date)) into CF_Payment2 from dual;
              --select decode(v_pay_cur.reference_1,NULL,CF_Payment3,Decode(length(CF_Payment3),0,v_pay_cur.reference_1,CF_Payment3||','||v_pay_cur.reference_1)) into CF_Payment3 from dual;
          
              select decode(v_pay_cur.invoice_num,NULL,CF_Payment1,CF_Payment1||','||v_pay_cur.invoice_num) into CF_Payment1 from dual;
              select decode(v_pay_cur.invoice_date,NULL,CF_Payment2,CF_Payment2||','||v_pay_cur.invoice_date) into CF_Payment2 from dual;
              select decode(v_pay_cur.reference_1,NULL,CF_Payment3,CF_Payment3||','||v_pay_cur.reference_1) into CF_Payment3 from dual;

              --CF_Payment1 := CF_Payment1||','||v_pay_cur.invoice_num;
              --CF_Payment2 := CF_Payment2||','||v_pay_cur.invoice_date;
              --CF_Payment3 := CF_Payment3||','||v_pay_cur.reference_1;                            
          
          End Loop;
          
          --select decode(length(CF_Payment1),1,NULL,0,NULL,substr(CF_Payment1,2)) into CF_Payment1 from dual;
          --select decode(length(CF_Payment2),1,NULL,0,NULL,substr(CF_Payment2,2)) into CF_Payment2 from dual;
          --select decode(length(CF_Payment3),1,NULL,0,NULL,substr(CF_Payment3,2)) into CF_Payment3 from dual;          
          
          CF_Payment1:= substr(CF_Payment1,2);
          CF_Payment2:= substr(CF_Payment2,2);
          CF_Payment3:= substr(CF_Payment3,2);
          
          --Print_log('Invoice# '||CF_Payment1,p_debug_flag);                        
          --Print_log('Invoice Date# '||CF_Payment2,p_debug_flag);
          --Print_log('Reference# '||CF_Payment3,p_debug_flag);
      Exception
  
      When no_data_found then
           Print_log('ABRL Supplier Additional Info not defined for check# '||v_cur_gt.C_CHECK_NUMBER,p_debug_flag);
      When Too_many_rows then
           Print_log('Multiple ABRL Supplier Additional Infodefined for check# '||v_cur_gt.C_CHECK_NUMBER,p_debug_flag);
      When others then
           Print_log('Exception ABRL Supplier Additional Info for check# '||v_cur_gt.C_CHECK_NUMBER,p_debug_flag);           
      End;
      
      
      Begin
      vc_line_str := CF_TRANSACTION_TYPE || '~' ||    --Transaction Type
                     NULL/*v_cur_gt.C_BEN_CODE*/ || '~' ||                               --Beneficiary Code
                     CF_ACCT_NUM|| '~' || v_cur_gt.                     --Beneficiary Account Number
                     C_AMOUNT || '~' || v_cur_gt.                       --Instrument Amount
                     C_BEN_NAME || '~' || v_cur_gt.                     --Beneficiary Name
                     C_DRAWEE_LOCATION || '~' || v_cur_gt.              --Drawee Location
                     C_CASHIN_CODE || '~' || v_cur_gt.                  --Print Location
                     CF_ADDRESS_LINE1 || '~' || v_cur_gt.                --Bene Address 1
                     CF_ADDRESS_LINE2 || '~' ||                          --Bene Address 2
                     replace(v_cur_gt.CF_ADDRESS_LINE3,chr(0),'') || '~' ||     --Bene Address 3
                     replace(v_cur_gt.CF_ADDRESS_LINE4,chr(0),'') || '~' ||     --Bene Address 4
                     CF_Address5     || '~' ||                                 --Bene Address 5
                     NULL || '~' || v_cur_gt.                   --Instruction Reference Number(Blank)
                     CUST_REF_NUMBER || '~' ||                 --Customer Reference Number
                     CF_Payment1 || '~' ||                                        --Payment details 1           
                     CF_Payment2 || '~' ||                                       --Payment details 2           
                     CF_Payment3 || '~' ||                                       --Payment details 3           
                     NULL        || '~' ||                                --Payment details 4           
                     NULL        || '~' ||                                --Payment details 5           
                     NULL        || '~' ||                                --Payment details 6           
                     NULL        || '~' || v_cur_gt.                               --Payment details 7                                                                                                                    
                     C_CHECK_NUMBER || '~' || v_cur_gt.                 --Cheque Number
                     C_CHECK_DATE || '~' || v_cur_gt.                   --Chq / Trn Date
                     MICR_NUM || '~' ||                                 --MICR Number
                     CF_IFC_Code || '~' ||                              --IFC Code
                     CF_BANK_VALID || '~' ||                                      --Bene Bank Name
                     CF_BRANCH_VALID || '~' ||                                   --Bene Bank Branch Name
                     CF_EMAIL                                           --Beneficiary email id
                     ;
                     
                     Begin
                     
                      update ap_checks 
                             set ATTRIBUTE14 = to_char(sysdate,'DD-MON-YYYY HH24:MI:SS'),
                             ATTRIBUTE15 ='PROCESSED'
                             where CHECK_ID =v_cur_gt.CHECK_ID; 
                     Exception
                     when others then
                          Print_log('Error:Exception while Updating Payment Batch for Check_Id: '||v_cur_gt.CHECK_ID ||' >>'||SQLERRM,p_debug_flag);           
                     End;
      
      Exception
  
      When others then
           Print_log('Error:Exception in string formation: '||SQLERRM,p_debug_flag);           
      End;

      

      fnd_file.put_line(fnd_file.output, nvl(vc_line_str,'NULL-String:vc_line_str'));

      
      vc_line_str       :=NULL;
      CF_BANK_VALID     :=NULL;
      CF_ACCT_NUM       :=NULL;
      CF_BRANCH_VALID   :=NULL;
      CF_EMAIL          :=NULL;
      CF_MICR_Code      :=NULL;
      CF_IFC_Code       :=NULL;
      CF_NEFT_Code      :=NULL;
      CF_RTGS_Code      :=NULL;
      CF_Payment1       :=NULL;
      CF_Payment2       :=NULL;
      CF_Payment3       :=NULL;
      CF_Address5       :=NULL;
      
    
    End Loop;
    commit;
  Exception
  when others then
       fnd_file.put_line(fnd_file.log, 'Error:Exception in Main Block: '||SQLERRM);
  
  End;

  Procedure LOAD_GLOBAL_TABLE( /*P_Errbuf      OUT VARCHAR2,
                                                           P_RetCode     OUT NUMBER,*/p_start_date  IN Date,
                              p_end_date    IN Date,
                              p_org_id      IN NUMBER,
                              p_checkrun_id IN VARCHAR2,
                              p_vendor_id   IN VARCHAR2) AS
  Begin
  
    Begin
      insert into XXABRL_PAY_BATCH_DATA_GT
        select (case ch.payment_method_code
                 when 'CHECK' then
                  'C'
                 when 'EFT' then
                  'S'
                 when 'SEFT' then
                  'S'
                 when 'DD' then
                  'D'
                 when 'NEFT' then
                  'N'
                 when 'RTGS' then
                  'R'
                 when 'PAY ORDER PRINTING' then
                  'H'
                 when 'FUNDS TRANSFERS' then
                  'I'
                 else
                  null
               end) c_transaction_type,
               pvs.vendor_site_code c_ben_code,
               --ieb.BANK_ACCOUNT_NUMBER c_ben_acc_num,
               ch.amount c_amount,
               --* H@N$ 04/12/08       substr(ch.vendor_name, 1, 240) c_ben_name,
               substr(nvl(pv.VENDOR_NAME_ALT,pv.VENDOR_NAME), 1, 240) c_ben_name,
               --*       
               (case ch.payment_method_code
                 when 'CHECK' then
                  --upper(pvs.city)
                  'MUM'
                 when 'SEFT' then
                  upper(pvs.city)
                 when 'DRAFT' then                 
                  --upper(pvs.city)
                  'MUM'
                 when 'NEFT' then
                  upper(pvs.city)
                 else
                  null
               end) c_drawee_location,
               (case ch.payment_method_code
                 when 'CHECK' then
                  '000'
                 when 'DRAFT' then                 
                  '000'
                 else
                  null
               end) c_cashin_code,
               replace(substr(pvs.address_line1, 1, 25), ',', ' ') c_address_line1,
               replace(substr(pvs.address_line2, 1, 25), ',', ' ') c_address_line2,
               replace(substr(pvs.CITY||','||pvs.STATE, 1, 25), ',', ' ') c_address_line3,
               replace(substr(pvs.ZIP, 1, 25), ',', ' ') c_address_line4,
               /*substr(pvs.city, 1, 8) || '-' || substr(pvs.state, 1, 10) || '-' ||
               pvs.area_code*/ NULL c_address_line5,
               ch.doc_sequence_value inst_ref_num,
               pv.segment1 cust_ref_number,
               ch.check_number c_check_number,
               ch.CHECK_ID,
               ch.check_date c_check_date,
               pv.global_attribute2 micr_num,
               pv.global_attribute3 neft_code,
               pv.global_attribute4 rtgs_code,
               --ieb.bank_name  c_bank,
               --ieb.bank_branch_name c_branch_name,
               pvs.email_address c_email_address,
               ch.org_id c_org_id,
               haou.organization_name c_org_name,
               ch.checkrun_name,
               pv.vendor_id,
               0 Invoice_id
          from ap_checks                    ch,
               ap_lookup_codes              lk2,
               fnd_territories_vl           ft,
               po_vendors                   pv,
               org_organization_definitions haou,
               ap_invoice_payments          aip,
               ap_invoices                  ais,
               iby_payment_methods_vl       iby1,
               /*ap_invoice_selection_criteria*/
               AP.AP_INV_SELECTION_CRITERIA_ALL aisc,
               po_vendor_sites_all              pvs
         where
        
         pvs.vendor_site_id = ch.vendor_site_id
         and ch.payment_type_flag not in ('R') --nvl(:p_payment_type,ch.payment_type_flag )
        --and   (ch.check_date) between NVL(:p_start_date,ch.check_date) and NVL(:p_end_date,ch.check_date)
         and to_char(ch.check_date, 'DD-MON-YYYY') between
         NVL(p_start_date, to_char(ch.check_date, 'DD-MON-YYYY')) and
         NVL(p_end_date, to_char(ch.check_date, 'DD-MON-YYYY'))
         and ch.country = ft.territory_code(+)
         and lk2.lookup_type = 'CHECK STATE'
         and lk2.lookup_code = ch.status_lookup_code
         and aip.invoice_id = ais.invoice_id
         and ch.check_id = aip.check_id
         and ch.org_id = haou.organization_id
         and ch.org_id = p_org_id
         and ch.checkrun_id = NVL(p_checkrun_id, ch.checkrun_id)
         and ch.vendor_id = NVL(p_vendor_id, ch.vendor_id)
         and ais.payment_status_flag = 'Y'
         and pv.vendor_id = ch.vendor_id
         and iby1.payment_method_code(+) = ch.payment_method_code
         and aisc.checkrun_id = ch.checkrun_id
         and ch.ATTRIBUTE14 is null
         and ch.ATTRIBUTE15 is null
         Group by (case ch.payment_method_code
                    when 'CHECK' then
                     'C'
                    when 'EFT' then
                     'S'
                    when 'SEFT' then
                     'S'
                    when 'DD' then
                     'D'
                    when 'NEFT' then
                     'N'
                    when 'RTGS' then
                     'R'
                    when 'PAY ORDER PRINTING' then
                     'H'
                    when 'FUNDS TRANSFERS' then
                     'I'
                    else
                     null
                  end),
                  pvs.vendor_site_code,
                  --ieb.bank_account_number,
                  ch.amount,
                  /*          substr(ch.vendor_name, 1, 240),*/
                  substr(nvl(pv.VENDOR_NAME_ALT,pv.VENDOR_NAME), 1, 240),
                  (case ch.payment_method_code
                 when 'CHECK' then
                  --upper(pvs.city)
                  'MUM'
                 when 'SEFT' then
                  upper(pvs.city)
                 when 'DRAFT' then                 
                  --upper(pvs.city)
                  'MUM'
                 when 'NEFT' then
                  upper(pvs.city)
                 else
                  null
               end),
               (case ch.payment_method_code
                 when 'CHECK' then
                  '000'
                 when 'DRAFT' then                 
                  '000'
                 else
                  null
               end),
                  replace(substr(pvs.address_line1, 1, 25), ',', ' '),
                  replace(substr(pvs.address_line2, 1, 25), ',', ' '),
                  replace(substr(pvs.CITY||','||pvs.STATE, 1, 25), ',', ' '),
                  replace(substr(pvs.ZIP, 1, 25), ',', ' '),
                  substr(pvs.city, 1, 8) || '-' || substr(pvs.state, 1, 10) || '-' ||
                  pvs.area_code,
                  --pvs.city||'-'||pvs.state||'-'||pvs.area_code,
                  ch.doc_sequence_value,
                  pv.segment1,
                  ch.check_number,
                  ch.check_id,
                  ch.check_date,
                  pv.global_attribute2,
                  pv.global_attribute3,
                  pv.global_attribute4,
                  --ieb.bank_name,
                  --ieb.bank_branch_name,
                  pvs.email_address,
                  ch.org_id,
                  haou.organization_name,
                  ch.checkrun_name,
                  pv.vendor_id
         order by 14;

    Exception
      when others then
        Fnd_File.PUT_LINE(fnd_file.LOG,
                          'Exception while inserting Data to GT Table: ' ||
                          SQLERRM);
    End;
  End LOAD_GLOBAL_TABLE;
END XXABRL_PAYMENT_BATCH_PKG;
/


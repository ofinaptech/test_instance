CREATE OR REPLACE PACKAGE BODY APPS.JAI_GSTR2A_PKG
/* $Header: JAI_GSTR2A_PKG_PKB.pls 120.0.12010000.5 2018/02/01 05:43:46 aelluru noship $ */
/* ************************************************************************************************
 * Copyright(c)  2017,2017 Oracle Corporation , 2007. All rights reserved.                                   *
 *************************************************************************************************
 *                                                                                               *
 * PROGRAM NAME                                                                                  *
 *  JAI_GSTR2A_PKG_PKB.pls                                                                               *
 *                                                                                               *
 * DESCRIPTION                                                                                   *
 *      .                                                                                        *
 *                                                                                               *
 * USAGE                                                                                         *
 *   To execute       How to Execute                                                             *
 *                                                                                               *
 * PROGRAM LIST                                                                                  *
 * ==============================================================================================*
 * NAME                       DESCRIPTION                                                        *
.* ------------------------   -------------------------------------------------------------------*
 *                                                                                               *
 * DEPENDENCIES                                                                                  *
 *   None                                                                                        *
 *                                                                                               *
 * CALLED BY                                                                                     *
 *                                                                                               *
 *                                                                                               *
 * LAST UPDATE DATE   18-OCT-2017                                                                *
 *   Date the program has been modified for the last time                                        *
 *
 * RESPONSE_CODE                 RESPONSE_DESC                                                   *
 *        U01                    Unmatched First Party Registration                              *
 *        U03                       Unmatched Third Party Registration                           *
 *        UTIN                    Unmatched Tax Invoice Number                                   *
 *        UTID                    Unmatched Tax Invoice Date                                     *
 *        UTAXV                    Unmatched Taxable Value                                       *
 *        MATCH                    Record matched                                                *
 *          UTAXIV        Unmatched Tax invoice Value
 *          UTAXV        Unmatched Taxable value
*          UITAX        Unmatched IGST Tax amount
*          UCTAX        Unmatched CGST Tax amount
*          USTAX        Unmatched SGST Tax amount
*          UCSTAX        Unmatched CESS Tax amount
*
 * HISTORY                                                                                       *
 * ==============================================================================================*
 *                                                                                               *
 * VERSION    DATE        AUTHOR(S)        DESCRIPTION                                           *
 * ------- ----------- ---------------- ---------------------------------------------------------*
 * DRAFT1A       26-AUG-2017         Initial Version                                             *
 *
 *********************************************************************************************** */
AS
   G_PKG_NAME           CONSTANT VARCHAR2 (30) := 'JAI_GSTR2A_PKG';
   G_MODULE_NAME        CONSTANT VARCHAR2 (100) := 'JAI.PLSQL.JAI_GSTR2A_PKG.';
   C_LINES_PER_INSERT   CONSTANT NUMBER := 1000;

   -----------------------------------------
   --Public Variable Declarations
   -----------------------------------------
   --
   --  Define Global  Variables;

   g_start_date                  DATE;
   g_end_date                    DATE;

   L_VERSION_INFO                VARCHAR2 (80) := NULL;
   L_PARAMLIST                   VARCHAR2 (4000);
   L_ERRBUF                      VARCHAR2 (2000);
   L_LENGTH_ERRBUF               NUMBER;
   L_RETCODE                     NUMBER := 0;
   g_created_by                  NUMBER (15);
   g_creation_date               DATE;
   g_last_updated_by             NUMBER (15);
   g_last_update_date            DATE;
   g_last_update_login           NUMBER (15);
   g_first_party_num             VARCHAR2 (50);
   g_third_party_num             VARCHAR2 (50);
   g_period_name                 VARCHAR2 (20);
   g_tax_inv_number              VARCHAR2 (50);
   g_tax_inv_date                DATE;

   -- G_PKG_NAME              CONSTANT VARCHAR2(30) := 'JAI_GSTR2A_PKG';
   --G_MODULE_NAME           CONSTANT VARCHAR2(80) := 'JA.PLSQL.JAI_GSTR2A_PKG.';
   g_current_runtime_level       NUMBER;
   g_level_statement    CONSTANT NUMBER := FND_LOG.LEVEL_STATEMENT;
   g_level_procedure    CONSTANT NUMBER := FND_LOG.LEVEL_PROCEDURE;
   g_level_event        CONSTANT NUMBER := FND_LOG.LEVEL_EVENT;
   g_level_unexpected   CONSTANT NUMBER := FND_LOG.LEVEL_UNEXPECTED;
   g_error_buffer                VARCHAR2 (100);
   g_match_response_desc         VARCHAR2 (30) := 'Record Matched';
   g_unmatch_response_desc       VARCHAR2 (30) := 'Record Unmatched';

   PG_DEBUG                      VARCHAR2 (1)
      := NVL (FND_PROFILE.VALUE ('AFLOG_ENABLED'), 'N');

   TYPE l_sql_statement_tabtype IS TABLE OF VARCHAR2 (32600)
                                      INDEX BY BINARY_INTEGER;


   TYPE gstr2a_rec_type IS RECORD
   (
      gstin_no           VARCHAR2 (200),
      TaxperiodMonthId   NUMBER,
      TaxPeriodYear      NUMBER
   );

   g_gstr2a_rec                  gstr2a_rec_type;

   TYPE b2b_rec_type IS RECORD
   (
      detail_type   VARCHAR2 (200),
      ctin          VARCHAR2 (200),
      cfs           VARCHAR2 (2),
      flag          VARCHAR2 (2),
      chksum        VARCHAR2 (200),
      inum          VARCHAR2 (30),
      idt           VARCHAR2 (200),
      val           NUMBER,
      pos           VARCHAR2 (200),
      rchrg         VARCHAR2 (20),
      updby         VARCHAR2 (20),
      ty_num        NUMBER,
      ty            VARCHAR2 (1),
      hsn_sc        VARCHAR2 (20),
      txval         NUMBER,
      irt           NUMBER,
      iamt          NUMBER,
      crt           NUMBER,
      camt          NUMBER,
      srt           NUMBER,
      samt          NUMBER,
      csrt          NUMBER,
      csamt         NUMBER,
      elg           VARCHAR2 (20)
   );


   TYPE b2ba_rec_type IS RECORD
   (
      detail_type   VARCHAR2 (200),
      ctin          VARCHAR2 (200),
      cfs           VARCHAR2 (2),
      flag          VARCHAR2 (2),
      chksum        VARCHAR2 (200),
      inum          VARCHAR2 (30),
      idt           VARCHAR2 (200),
      oinum         VARCHAR2 (30),
      oidt          VARCHAR2 (200),
      val           NUMBER,
      pos           VARCHAR2 (200),
      rchrg         VARCHAR2 (20),
      updby         VARCHAR2 (20),
      ty_num        NUMBER,
      ty            VARCHAR2 (1),
      hsn_sc        VARCHAR2 (20),
      txval         NUMBER,
      irt           NUMBER,
      iamt          NUMBER,
      crt           NUMBER,
      camt          NUMBER,
      srt           NUMBER,
      samt          NUMBER,
      csrt          NUMBER,
      csamt         NUMBER,
      elg           VARCHAR2 (20)
   );

   TYPE cdn_rec_type IS RECORD
   (
      detail_type   VARCHAR2 (200),
      ctin          VARCHAR2 (200),
      cfs           VARCHAR2 (2),
      flag          VARCHAR2 (2),
      chksum        VARCHAR2 (200),
      ntty          VARCHAR2 (20),
      nt_num        NUMBER,
      nt_dt         VARCHAR2 (200),
      rsn           VARCHAR2 (20),
      inum          VARCHAR2 (30),
      idt           VARCHAR2 (30),
      val           NUMBER,
      irt           NUMBER,
      iamt          NUMBER,
      crt           NUMBER,
      camt          NUMBER,
      srt           NUMBER,
      samt          NUMBER,
      csrt          NUMBER,
      csamt         NUMBER,
      updby         VARCHAR2 (20),
      elg           VARCHAR2 (20),
      rchrg         VARCHAR2 (20)
   );

   TYPE cdna_rec_type IS RECORD
   (
      detail_type   VARCHAR2 (200),
      ctin          VARCHAR2 (200),
      cfs           VARCHAR2 (2),
      flag          VARCHAR2 (2),
      chksum        VARCHAR2 (200),
      ntty          VARCHAR2 (20),
      nt_num        NUMBER,
      nt_dt         VARCHAR2 (200),
      ont_num       NUMBER,
      ont_dt        VARCHAR2 (200),
      rsn           VARCHAR2 (20),
      inum          VARCHAR2 (30),
      idt           VARCHAR2 (30),
      val           NUMBER,
      irt           NUMBER,
      iamt          NUMBER,
      crt           NUMBER,
      camt          NUMBER,
      srt           NUMBER,
      samt          NUMBER,
      csrt          NUMBER,
      csamt         NUMBER,
      updby         VARCHAR2 (20),
      elg           VARCHAR2 (20),
      rchrg         VARCHAR2 (20)
   );

   TYPE isd_rcd_rec_type IS RECORD
   (
      detail_type   VARCHAR2 (200),
      chksum        VARCHAR2 (200),
      gstin_isd     VARCHAR2 (30),
      inum          VARCHAR2 (30),
      idt           VARCHAR2 (30),
      ig_cr         NUMBER,
      cg_cr         NUMBER,
      sg_cr         NUMBER
   );


   TYPE tds_credit_rec_type IS RECORD
   (
      detail_type   VARCHAR2 (200),
      ctin          VARCHAR2 (200),
      chksum        VARCHAR2 (200),
      inum          VARCHAR2 (30),
      idt           VARCHAR2 (30),
      ival          NUMBER,
      pay_dt        VARCHAR2 (30),
      tds_val       NUMBER,
      irt           NUMBER,
      iamt          NUMBER,
      crt           NUMBER,
      camt          NUMBER,
      srt           NUMBER,
      samt          NUMBER,
      csrt          NUMBER,
      csamt         NUMBER
   );

   TYPE tcs_data_rec_type IS RECORD
   (
      detail_type   VARCHAR2 (200),
      chksum        VARCHAR2 (200),
      ctin          VARCHAR2 (200),
      m_id          NUMBER,
      sup_val       NUMBER,
      tx_val        NUMBER,
      irt           NUMBER,
      iamt          NUMBER,
      crt           NUMBER,
      camt          NUMBER,
      srt           NUMBER,
      samt          NUMBER,
      csrt          NUMBER,
      csamt         NUMBER
   );



   TYPE b2b_tbl_type IS TABLE OF b2b_rec_type
                           INDEX BY BINARY_INTEGER;

   TYPE b2ba_tbl_type IS TABLE OF b2ba_rec_type
                            INDEX BY BINARY_INTEGER;

   TYPE cdn_tbl_type IS TABLE OF cdn_rec_type
                           INDEX BY BINARY_INTEGER;

   TYPE cdna_tbl_type IS TABLE OF cdna_rec_type
                            INDEX BY BINARY_INTEGER;

   TYPE isd_rcd_tbl_type IS TABLE OF isd_rcd_rec_type
                               INDEX BY BINARY_INTEGER;

   TYPE tds_credit_tbl_type IS TABLE OF tds_credit_rec_type
                                  INDEX BY BINARY_INTEGER;

   TYPE tcs_data_tbl_type IS TABLE OF tcs_data_rec_type
                                INDEX BY BINARY_INTEGER;



   TYPE NUMBER_tbl_type IS TABLE OF NUMBER
                              INDEX BY BINARY_INTEGER;

   TYPE VARCHAR2_30_tbl_type IS TABLE OF VARCHAR2 (30)
                                   INDEX BY BINARY_INTEGER;

   TYPE VARCHAR2_100_tbl_type IS TABLE OF VARCHAR2 (100)
                                    INDEX BY BINARY_INTEGER;

   /*===========================================================================+
   | PROCEDURE                                                                 |
   |   APPEND_ERRBUF                                                           |
   |                                                                           |
   | DESCRIPTION                                                               |
   |    This procedure appends the input parameter p_msg to the global         |
   |    variable L_ERRBUF which will be returned to the calling concurrent     |
   |    program.                                                               |
   |                                                                           |
   | SCOPE - Public                                                            |
   |                                                                           |
   | NOTES                                                                     |
   |                                                                           |
   | MODIFICATION HISTORY                                                      |
   |                                                                             |
   |                                                                           |
   +===========================================================================*/

   PROCEDURE append_errbuf (p_msg IN VARCHAR2)
   IS
   BEGIN
      IF NVL (LENGTHB (L_ERRBUF), 0) = 0
      THEN
         L_ERRBUF := p_msg;
      ELSIF NVL (LENGTHB (L_ERRBUF), 0) < 2000 - NVL (LENGTHB (p_msg), 0)
      THEN
         L_ERRBUF := L_ERRBUF || ';' || p_msg;
      END IF;

      L_ERRBUF := L_ERRBUF || fnd_global.newline;
   END append_errbuf;


   FUNCTION get_tax_invoice_value (p_trx_id           NUMBER,
                                   p_det_factor_id    NUMBER DEFAULT NULL,
                                   p_item_id          NUMBER DEFAULT NULL,
                                   p_entity_code      VARCHAR2 DEFAULT NULL,
                                   p_trx_type         VARCHAR2 DEFAULT NULL)
      RETURN NUMBER
   IS
      ln_tax_invoice_amt   NUMBER;
   BEGIN
      SELECT ( (SELECT SUM (line_amt)
                  FROM jai_tax_det_factors
                 WHERE trx_id = p_trx_id
                       AND det_factor_id =
                              NVL (p_det_factor_id, det_factor_id)
                       AND entity_code = NVL (p_entity_code, entity_code)
                       AND NVL (trx_type, 1) =
                              NVL (p_trx_type, NVL (trx_type, 1)))
              + (SELECT SUM (NVL (ROUNDED_TAX_AMT_FUN_CURR, 0))
                   FROM jai_tax_lines a
                  WHERE a.trx_id = p_trx_id
                        AND det_factor_id =
                               NVL (p_det_factor_id, det_factor_id)
                        AND NVL (ITEM_ID, 1) =
                               NVL (p_item_id, NVL (ITEM_ID, 1))
                        AND entity_code = NVL (p_entity_code, entity_code)
                        AND NVL (trx_type, 1) =
                               NVL (p_trx_type, NVL (trx_type, 1))))
                Tax_Invoice_Amount
        INTO ln_tax_invoice_amt
        FROM DUAL;

      RETURN ln_tax_invoice_amt;
   EXCEPTION
      WHEN OTHERS
      THEN
         ln_tax_invoice_amt := 0;
         RETURN ln_tax_invoice_amt;
   END;


   FUNCTION get_taxable_amt (p_trx_id           NUMBER,
                             p_det_factor_id    NUMBER DEFAULT NULL,
                             p_item_id          NUMBER DEFAULT NULL,
                             p_entity_code      VARCHAR2 DEFAULT NULL,
                             p_trx_type         VARCHAR2 DEFAULT NULL)
      RETURN NUMBER
   IS
      ln_taxable_amt   NUMBER;
   BEGIN
      SELECT SUM (ROUNDED_TAXABLE_AMT_FUN_CURR) TAXABLE_AMT
        INTO ln_taxable_amt
        FROM (SELECT DISTINCT
                     det_factor_id,
                     NVL (ROUNDED_TAXABLE_AMT_FUN_CURR, 0)
                        ROUNDED_TAXABLE_AMT_FUN_CURR
                FROM jai_tax_lines a
               WHERE     a.trx_id = p_trx_id
                     AND det_factor_id = NVL (p_det_factor_id, det_factor_id)
                     AND NVL (ITEM_ID, 1) = NVL (p_item_id, NVL (ITEM_ID, 1))
                     AND entity_code = NVL (p_entity_code, entity_code)
                     AND NVL (trx_type, 1) =
                            NVL (p_trx_type, NVL (trx_type, 1)));

      RETURN ln_taxable_amt;
   EXCEPTION
      WHEN OTHERS
      THEN
         ln_taxable_amt := 0;
         RETURN ln_taxable_amt;
   END;

   /*
      FUNCTION get_tax_invoice_value (p_trx_id           NUMBER,
                                      p_det_factor_id    NUMBER,
                                      p_item_id          NUMBER)
         RETURN NUMBER
      IS
         ln_tax_invoice_amt   NUMBER;
      BEGIN
         SELECT ( (SELECT SUM (line_amt)
                     FROM jai_tax_det_factors
                    WHERE trx_id = p_trx_id
                          AND det_factor_id =
                                 NVL (p_det_factor_id, det_factor_id))
                 + (SELECT SUM (NVL (ROUNDED_TAX_AMT_FUN_CURR, 0))
                      FROM jai_tax_lines a
                     WHERE a.trx_id = p_trx_id
                           AND det_factor_id =
                                  NVL (p_det_factor_id, det_factor_id)
                           AND NVL (ITEM_ID, 1) =
                                  NVL (p_item_id, NVL (ITEM_ID, 1))))
                   Tax_Invoice_Amount
           INTO ln_tax_invoice_amt
           FROM DUAL;

         RETURN ln_tax_invoice_amt;
      EXCEPTION
         WHEN OTHERS
         THEN
            ln_tax_invoice_amt := 0;
            RETURN ln_tax_invoice_amt;
      END;
   */

   /*
      FUNCTION get_taxable_amt (p_trx_id           NUMBER,
                                p_det_factor_id    NUMBER DEFAULT NULL,
                                p_item_id          NUMBER DEFAULT NULL)
         RETURN NUMBER
      IS
         ln_taxable_amt   NUMBER;
      BEGIN
         SELECT SUM (ROUNDED_TAXABLE_AMT_FUN_CURR) TAXABLE_AMT
           INTO ln_taxable_amt
           FROM (SELECT DISTINCT
                        det_factor_id,
                        NVL (ROUNDED_TAXABLE_AMT_FUN_CURR, 0)
                           ROUNDED_TAXABLE_AMT_FUN_CURR
                   FROM jai_tax_lines a
                  WHERE     a.trx_id = p_trx_id
                        AND det_factor_id = NVL (p_det_factor_id, det_factor_id)
                        AND NVL (ITEM_ID, 1) = NVL (p_item_id, NVL (ITEM_ID, 1)));

         RETURN ln_taxable_amt;
      EXCEPTION
         WHEN OTHERS
         THEN
            ln_taxable_amt := 0;
            RETURN ln_taxable_amt;
      END;
   */

   FUNCTION Party_Valdation (p_fpty_reg_num    VARCHAR2,
                             p_tpty_reg_num    VARCHAR2,
                             p_tax_inv_num     NUMBER,
                             p_tax_inv_date    DATE)
      RETURN VARCHAR2
   IS
      lc_status          VARCHAR2 (1) := 'U';
      lc_response_code   VARCHAR2 (10);
      lc_response_desc   VARCHAR2 (500);
      lv_api_name        VARCHAR2 (50) := 'Party_Valdation';
      ln_error_count     NUMBER := 0;
      le_party_valid     EXCEPTION;
   BEGIN
      g_current_runtime_level := FND_LOG.G_CURRENT_RUNTIME_LEVEL;

      FND_FILE.put_line (which   => FND_FILE.LOG,
                         buff    => '+-------------------------------+');
      FND_FILE.put_line (which => FND_FILE.LOG, buff => '|  Party_Valdation |');
      FND_FILE.put_line (which   => FND_FILE.LOG,
                         buff    => '+-------------------------------+');
      FND_FILE.put_line (which   => FND_FILE.LOG,
                         buff    => '                                 ');

      --initialize
      lc_status := 'U';
      lc_response_code := NULL;
      lc_response_desc := NULL;

      /* FIRST_PARTY validation */
      BEGIN
         SELECT 'M'
           INTO lc_status
           FROM DUAL
          WHERE 1 = 1
                AND EXISTS
                       (SELECT 1
                          FROM jai_party_reg_lines_v a, jai_party_regs_v b
                         WHERE     a.party_reg_id = b.party_reg_id
                               AND a.REG_CLASS_CODE = 'FIRST_PARTY'
                               AND a.registration_number = p_fpty_reg_num);
      EXCEPTION
         WHEN OTHERS
         THEN
            lc_status := 'U';
            lc_response_code := 'U01';
            lc_response_desc := 'Unmatched First Party Registration';
            RAISE le_party_valid;
      END;

      /* THIRD_PARTY validation */
      BEGIN
         SELECT 'M'
           INTO lc_status
           FROM DUAL
          WHERE 1 = 1
                AND EXISTS
                       (SELECT 1
                          FROM jai_party_reg_lines_v a, jai_party_regs_v b
                         WHERE     a.party_reg_id = b.party_reg_id
                               AND a.REG_CLASS_CODE = 'THIRD_PARTY'
                               AND a.registration_number = p_tpty_reg_num);
      EXCEPTION
         WHEN OTHERS
         THEN
            lc_status := 'U';
            lc_response_code := 'U03';
            lc_response_desc := 'Unmatched Third Party Registration';
            RAISE le_party_valid;
      END;

      RETURN lc_status;
   EXCEPTION
      WHEN le_party_valid
      THEN
         fnd_file.put_line (fnd_file.LOG, 'Party Validation Failed....!!!');
         fnd_file.put_line (fnd_file.LOG,
                            'Response Code: ' || lc_response_code);

         IF lc_response_code = 'U01'
         THEN
            update_status (p_fpty_reg_num    => p_fpty_reg_num,
                           p_tprty_reg_num   => NULL,
                           p_tax_inv_num     => p_tax_inv_num,
                           p_tax_inv_date    => p_tax_inv_date,
                           P_status          => lc_status,
                           p_response_code   => lc_response_code,
                           p_response_desc   => lc_response_desc);
         ELSIF lc_response_code = 'U03'
         THEN
            update_status (p_fpty_reg_num    => NULL,
                           p_tprty_reg_num   => p_tpty_reg_num,
                           p_tax_inv_num     => p_tax_inv_num,
                           p_tax_inv_date    => p_tax_inv_date,
                           P_status          => lc_status,
                           p_response_code   => lc_response_code,
                           p_response_desc   => lc_response_desc);
         END IF;

         RETURN lc_status;
      WHEN OTHERS
      THEN
         g_error_buffer := SQLCODE || ': ' || SUBSTR (SQLERRM, 1, 80);
         FND_MESSAGE.SET_NAME ('JA', 'GENERIC_MESSAGE');
         FND_MESSAGE.SET_TOKEN ('GENERIC_TEXT',
                                'Party_Valdation - ' || g_error_buffer);
         FND_MSG_PUB.Add;

         IF (g_level_unexpected >= g_current_runtime_level)
         THEN
            FND_LOG.STRING (g_level_unexpected,
                            G_MODULE_NAME || lv_api_name,
                            'Party_Valdation error : ' || g_error_buffer);
         END IF;

         APPEND_ERRBUF (g_error_buffer);
         lc_status := 'E';
         RETURN lc_status;
   END Party_Valdation;


   --trx details validation
   PROCEDURE validate_tax_inv_dtls (p_fpty_reg_num     VARCHAR2,
                                    p_tprty_reg_num    VARCHAR2,
                                    p_tax_inv_num      VARCHAR2,
                                    p_tax_inv_date     DATE,
                                    p_tax_inv_amt      NUMBER)
   IS
      /*CURSOR lcu_gstin_EBS_trx_match
      IS
           SELECT JGMP_GST.FIRST_PARTY_PRIMARY_REG_NUM,
                  JGMP_GST.THIRD_PARTY_REG_NUM,
                  JGMP_GST.TAX_INVOICE_NUMBER,
                  JGMP_GST.TAX_INVOICE_DATE,
                  JGMP_GST.TAX_INVOICE_VALUE
             FROM JAI_GST_MATCHING_REP_T JGMP_GST
            WHERE     JGMP_GST.RECORD_TYPE = 'EBS'
                  AND JGMP_GST.STATUS ='U'
                  AND JGMP_GST.TAX_INVOICE_NUMBER IS NOT NULL
                  AND JGMP_GST.FIRST_PARTY_PRIMARY_REG_NUM =
                         NVL (g_first_party_num,
                              JGMP_GST.FIRST_PARTY_PRIMARY_REG_NUM)
                  AND TO_CHAR (TRUNC (JGMP_GST.tax_invoice_date), 'MONYYYY') LIKE
                         g_period_name


      --  JGMP_GST.TAXABLE_AMT;

      TYPE lt_gstin_trx_type_t IS TABLE OF lcu_gstin_EBS_trx_match%ROWTYPE
                                     INDEX BY BINARY_INTEGER;



      TYPE lt_tax_inv_num_ty IS TABLE OF JAI_GST_MATCHING_REP_T.TAX_INVOICE_NUMBER%TYPE
                                   INDEX BY BINARY_INTEGER;

      lt_gstin_trx_ttype   lt_gstin_trx_type_t;
      lt_tax_inv_num_type    lt_tax_inv_num_ty;        -- tax`invoice num validation
       lt_tax_inv_date_type   lt_tax_inv_num_ty;        -- tax`invoice date validation
       lt_tax_taxble_val_type lt_tax_inv_num_ty;        -- taxable value validation
       lt_empty_trx_type         lt_tax_inv_num_ty;     -- empty table type */

      lc_valid_count      NUMBER := 0;
      lc_status           VARCHAR2 (1);
      lc_response_code    VARCHAR2 (10);
      lc_response_desc    VARCHAR2 (500);
      le_tax_dtls_valid   EXCEPTION;
      lv_api_name         VARCHAR2 (50) := 'validate_tax_inv_dtls';
   BEGIN

    null;
   END validate_tax_inv_dtls;


   PROCEDURE retry_matching (
   --        p_record_type     VARCHAR2 DEFAULT 'GSTIN',
                             p_tax_inv_num     VARCHAR2,
                             p_tax_inv_date    DATE)
   IS
      ln_valid_match           NUMBER := 0;
      ld_tax_inv_date          DATE := TRUNC (p_tax_inv_date);
      lc_party_validation      VARCHAR2 (1);
      lc_response_code         VARCHAR2 (10);
      lc_first_party_geg_num   VARCHAR2 (50);
      lc_third_party_geg_num   VARCHAR2 (50);
      ln_tax_invoice_amt       NUMBER;
   BEGIN
   null;
   END retry_matching;

   PROCEDURE update_status (p_fpty_reg_num     VARCHAR2 DEFAULT NULL,
                            p_tprty_reg_num    VARCHAR2 DEFAULT NULL,
                            p_tax_inv_num      VARCHAR2 DEFAULT NULL,
                            p_tax_inv_date     DATE DEFAULT NULL,
                            P_status           VARCHAR2,
                            p_response_code    VARCHAR2,
                            p_response_desc    VARCHAR2,
                            -- p_trx_id            NUMBER DEFAULT NULL,
--                            p_tax_line_id      NUMBER DEFAULT NULL         --,
                            p_rate                 NUMBER DEFAULT NULL,
                            p_tax_type            VARCHAR2 DEFAULT NULL
                            )
   IS
      lv_api_name   VARCHAR2 (50) := 'update_status';
--      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      /*      FND_FILE.put_line (which   => FND_FILE.LOG,
                               buff    => '+-------------------------------+');
            FND_FILE.put_line (
               which   => FND_FILE.LOG,
               buff    => '| Report run statistics for update_status |');
            FND_FILE.put_line (which   => FND_FILE.LOG,
                               buff    => '+-------------------------------+');
            FND_FILE.put_line (which   => FND_FILE.LOG,
                               buff    => '                                 ');
      */
       --#dbms_output.put_line('+-------------------------------+');
       --#dbms_output.put_line( 'p_tax_inv_num:- ' || p_tax_inv_num );
       --#dbms_output.put_line( 'P_status:- ' || P_status );
       --#dbms_output.put_line( 'p_response_code:- ' || p_response_code );
       --#dbms_output.put_line( 'p_response_desc:- ' || p_response_desc );
       --#dbms_output.put_line( 'p_rate:- ' || p_rate );
        --#dbms_output.put_line( 'p_tax_type:- ' || p_tax_type );
  null;
   END update_status;

   PROCEDURE Parties_Valdation
   IS

      lc_status          VARCHAR2 (1) := 'U';
      lc_response_code   VARCHAR2 (10);
      lc_response_desc   VARCHAR2 (500);
      lv_api_name        VARCHAR2 (50) := 'Parties_Valdation';
      ln_error_count     NUMBER := 0;
      le_party_valid     EXCEPTION;
   BEGIN
     null;
	 END Parties_Valdation;

   --trx details validation
   PROCEDURE tax_inv_dtls_validate
   IS

         /*  SELECT JGMP_GST.FIRST_PARTY_PRIMARY_REG_NUM,
                  JGMP_GST.THIRD_PARTY_REG_NUM,
                  JGMP_GST.TAX_INVOICE_NUMBER,
                  JGMP_GST.TAX_INVOICE_DATE,
                  JGMP_GST.TAX_INVOICE_VALUE
             --JGMP_GST.TAXABLE_AMT
             FROM JAI_GST_MATCHING_REP_T JGMP_GST
            WHERE     JGMP_GST.RECORD_TYPE = 'GSTIN'
                  AND JGMP_GST.STATUS IS NULL
                  AND JGMP_GST.TAX_INVOICE_NUMBER IS NOT NULL
                  AND JGMP_GST.FIRST_PARTY_PRIMARY_REG_NUM =
                         NVL (g_first_party_num,
                              JGMP_GST.FIRST_PARTY_PRIMARY_REG_NUM)
                  AND TO_CHAR (TRUNC (JGMP_GST.tax_invoice_date), 'MONYYYY') LIKE
                         g_period_name
                GROUP BY JGMP_GST.FIRST_PARTY_PRIMARY_REG_NUM,
                  JGMP_GST.THIRD_PARTY_REG_NUM,
                  JGMP_GST.TAX_INVOICE_NUMBER,
                  JGMP_GST.TAX_INVOICE_DATE,
                  JGMP_GST.TAX_INVOICE_VALUE;
                */
        /* SELECT UPPER (SUBSTR (SECTION_CODE, 1, 3)) SECTION_CODE,
                FIRST_PARTY_PRIMARY_REG_NUM,
                THIRD_PARTY_REG_NUM,
                TAX_INVOICE_NUMBER,
                TAX_INVOICE_DATE,
                TAX_INVOICE_VALUE,
                TAXABLE_AMT,
                HSN_SC,
                TY_NUM,
                irt,
                iamt,
                crt,
                camt,
                srt,
                samt,
                csrt,
                csamt
           FROM JAI_GST_MATCHING_REP_T JGMP_GST
          WHERE JGMP_GST.RECORD_TYPE = 'GSTIN' AND JGMP_GST.STATUS IS NULL
                --  AND JGMP_GST.TAX_INVOICE_NUMBER IN ('AA122','AA12222')
                -- AND IRT IS NOT NULL
                AND JGMP_GST.FIRST_PARTY_PRIMARY_REG_NUM =
                       NVL (g_first_party_num,
                            JGMP_GST.FIRST_PARTY_PRIMARY_REG_NUM)
                AND TO_CHAR (TRUNC (JGMP_GST.tax_invoice_date), 'MONYYYY') LIKE
                       g_period_name;
*/
      --  JGMP_GST.TAXABLE_AMT;

        /* SELECT                             --EBS.FIRST_PARTY_PRIMARY_REG_NUM,
                --           EBS.THIRD_PARTY_REG_NUM,
                --           EBS.TAX_INVOICE_NUMBER,
                --           EBS.TAX_INVOICE_DATE,
                --           EBS.TAXABLE_AMT,
                --           EBS.HSN_SC,
                --  EBS.TY_NUM,
                --         EBS.irt,
                --                        SUM(EBS.iamt) iamt ,
                --             EBS.crt,
                --                            SUM(EBS.camt) camt,
                --                EBS.srt,
                --                           SUM(EBS.samt) camt,
                --                   EBS.csrt,
                --                            SUM(EBS.csamt) camt
                ebs.trx_id,
                tax_line_id,
                hsn_code_id,
                taxable_amt,
                EBS.iamt,
                EBS.camt,
                EBS.samt,
                EBS.csamt
           FROM JAI_GST_MATCHING_REP_T EBS
          WHERE     EBS.RECORD_TYPE = 'EBS'
                AND EBS.STATUS IS NULL
                --                AND EBS.TAX_INVOICE_NUMBER IN ('AA122','AA12222')
                AND EBS.TAX_INVOICE_NUMBER = p_tin
                AND EBS.TAX_INVOICE_DATE = p_tid
                AND EBS.HSN_SC = p_hsn;
                */
      /*GROUP BY
      JGMP_GST.FIRST_PARTY_PRIMARY_REG_NUM,
            JGMP_GST.THIRD_PARTY_REG_NUM,
            JGMP_GST.TAX_INVOICE_NUMBER,
            JGMP_GST.TAX_INVOICE_DATE,
            JGMP_GST.TAXABLE_AMT,
            JGMP_GST.HSN_SC;*/




      /* lt_tax_inv_num_type    lt_tax_inv_num_ty;        -- tax`invoice num validation
       lt_tax_inv_date_type   lt_tax_inv_num_ty;        -- tax`invoice date validation
       lt_tax_taxble_val_type lt_tax_inv_num_ty;        -- taxable value validation
       lt_empty_trx_type         lt_tax_inv_num_ty;     -- empty table type */
      lc_valid_count       NUMBER := 0;
      lc_status            VARCHAR2 (1);
      lc_response_code     VARCHAR2 (10);
      lc_response_desc     VARCHAR2 (500);
      lc_tax_type           VARCHAR2(10);
      ln_trx_id            NUMBER;
      ln_tax_line_id       NUMBER;
      ln_hsn_code_id       NUMBER;
      ln_taxable_amt       NUMBER;
      ln_iamt              NUMBER;
      ln_camt              NUMBER;
      ln_samt              NUMBER;
      ln_csamt             NUMBER;
      le_tax_dtls_valid    EXCEPTION;
      lv_api_name          VARCHAR2 (50) := 'tax_inv_dtls_validate';
   BEGIN
      null;
   END tax_inv_dtls_validate;


   /*===========================================================================+
  | PROCEDURE                                                                 |
  |   un_match_ebs_records()                                                     |
  |                                                                           |
  | DESCRIPTION                                                               |
  |                      |
  |                                                                             |
  |                                                                           |
  | SCOPE - Public                                                            |
  |                                                                           |
  | NOTES                                                                     |
  |                                                                           |
  | MODIFICATION HISTORY                                                      |
  |                                       |
  |                                                                           |
  +===========================================================================*/

   PROCEDURE un_match_ebs_records (
      P_FIRST_PTY_REG_NUM   IN VARCHAR2 DEFAULT NULL,
      P_PERIOD_NAME         IN VARCHAR2)
   IS
      l_count               NUMBER;
      l_start_time          DATE;
      l_end_time            DATE;
      l_total_time          VARCHAR2 (10);
      lv_api_name           VARCHAR2 (50) := 'un_match_ebs_records';
      lc_response_code      JAI_GST_MATCHING_REP_T.response_code%TYPE;


        /*   SELECT JGMP_GST.TAX_INVOICE_NUMBER,
                  JGMP_GST.FIRST_PARTY_PRIMARY_REG_NUM,
                  JGMP_GST.THIRD_PARTY_REG_NUM,
                  TRUNC (JGMP_GST.TAX_INVOICE_DATE) TAX_INVOICE_DATE
             FROM JAI_GST_MATCHING_REP_T JGMP_GST
            WHERE     JGMP_GST.RECORD_TYPE = 'EBS'
                  AND JGMP_GST.STATUS IS NULL
                  AND JGMP_GST.TAX_INVOICE_NUMBER IS NOT NULL
                  AND JGMP_GST.FIRST_PARTY_PRIMARY_REG_NUM =
                         NVL (P_FIRST_PTY_REG_NUM,
                              JGMP_GST.FIRST_PARTY_PRIMARY_REG_NUM)
                  AND TO_CHAR (TRUNC (JGMP_GST.tax_invoice_date), 'MONYYYY') LIKE
                         P_PERIOD_NAME
                  AND NOT EXISTS
                             (SELECT 1
                                FROM JAI_GST_MATCHING_REP_T JGMP_EBZ
                               WHERE JGMP_EBZ.RECORD_TYPE = 'GSTIN'
                                     AND JGMP_EBZ.FIRST_PARTY_PRIMARY_REG_NUM =
                                            JGMP_GST.FIRST_PARTY_PRIMARY_REG_NUM
                                     AND JGMP_EBZ.THIRD_PARTY_REG_NUM =
                                            JGMP_GST.THIRD_PARTY_REG_NUM
                                     AND TRUNC (JGMP_EBZ.tax_invoice_date) =
                                            TRUNC (JGMP_GST.tax_invoice_date)
                                     AND JGMP_EBZ.TAX_INVOICE_NUMBER =
                                            JGMP_GST.TAX_INVOICE_NUMBER
                                     AND JGMP_EBZ.TAX_INVOICE_VALUE =
                                            JGMP_GST.TAX_INVOICE_VALUE)
         -- AND JGMP_EBZ.TAXABLE_AMT =
         --       JGMP_GST.TAXABLE_AMT)
         GROUP BY JGMP_GST.TAX_INVOICE_NUMBER,
                  JGMP_GST.FIRST_PARTY_PRIMARY_REG_NUM,
                  JGMP_GST.THIRD_PARTY_REG_NUM,
                  TRUNC (JGMP_GST.TAX_INVOICE_DATE);
        */



      /* TYPE lt_trx_inv_num_ttype IS TABLE OF JAI_GST_MATCHING_REP_T.TAX_INVOICE_NUMBER%TYPE
                                       INDEX BY BINARY_INTEGER;
       */

   -- lt_trx_inv_num_t   lt_trx_inv_num_ttype;
   BEGIN
     null;
   END un_match_ebs_records;

   /*===========================================================================+
  | PROCEDURE                                                                 |
  |   match_records()                                                     |
  |                                                                           |
  | DESCRIPTION                                                               |
  |    This procedure do the GSTR2A data matcing btwn GSTIN and EBS          |
  |                                                                             |
  |                                                                           |
  | SCOPE - Public                                                            |
  |                                                                           |
  | NOTES                                                                     |
  |                                                                           |
  | MODIFICATION HISTORY                                                      |
  |                                       |
  |                                                                           |
  +===========================================================================*/

   PROCEDURE match_records (P_FIRST_PTY_REG_NUM   IN VARCHAR2 DEFAULT NULL,
                            P_PERIOD_NAME         IN VARCHAR2)
   IS
      l_count            NUMBER;
      l_start_time       DATE;
      l_end_time         DATE;
      l_total_time       VARCHAR2 (10);
      lv_api_name        VARCHAR2 (50) := 'match_records';
      lc_response_code   JAI_GST_MATCHING_REP_T.response_code%TYPE;


   --      lt_trx_inv_num_t   lt_trx_inv_num_ttype;
   BEGIN
   null;
   END match_records;

   PROCEDURE populate_gstr2a_ebs_data (p_trx_id IN NUMBER)
   --   (P_FIRST_PTY_REG_NUM   IN VARCHAR2 DEFAULT NULL,
   --                            P_PERIOD_NAME         IN VARCHAR2)
   IS

   /*
    TYPE lt_ebs_b2b_data IS TABLE OF lcu_b2b_data%ROWTYPE
              INDEX BY BINARY_INTEGER;
           lt_ebs_b2b_ttype   lt_ebs_b2b_data;

    TYPE lt_ebs_cdn_data IS TABLE OF lcu_cdn_data%ROWTYPE
              INDEX BY BINARY_INTEGER;
           lt_ebs_cdn_ttype   lt_ebs_cdn_data;
   */
   BEGIN
      /*OPEN lcu_b2b_data;
      FETCH lcu_b2b_data BULK COLLECT INTO lt_ebs_b2b_ttype;
      CLOSE lcu_b2b_data;

    --  FOR ALL lt_ebs_b2b_ttype.first .. lt_ebs_b2b_ttype.last
    --  INSERT INTO ;

      OPEN lcu_cdn_data;
      FETCH lcu_cdn_data BULK COLLECT INTO lt_ebs_cdn_ttype;
      CLOSE lcu_cdn_data;
      */
null;
   END populate_gstr2a_ebs_data;


   PROCEDURE extract_gstr2a_ebs_data (
      P_FIRST_PTY_REG_NUM   IN VARCHAR2 DEFAULT NULL,
      P_PERIOD_NAME         IN VARCHAR2)
   IS

   BEGIN
   null;
   END extract_gstr2a_ebs_data;



   PROCEDURE match_process_main (
      errbuf                   OUT NOCOPY VARCHAR2,
      retcode                  OUT NOCOPY NUMBER,
      P_FIRST_PTY_REG_NUM   IN     VARCHAR2 DEFAULT NULL,
      P_PERIOD_NAME         IN     VARCHAR2,
	  p_file_type			IN     VARCHAR2 DEFAULT 'CSV')
   IS

   BEGIN
    null;
   END match_process_main;

   PROCEDURE load_file (errbuf                    OUT NOCOPY VARCHAR2,
                        retcode                   OUT NOCOPY VARCHAR2,
                        p_file_location_name   IN            VARCHAR2)
   IS
      l_api_name   CONSTANT VARCHAR2 (30) := 'load_file';
      l_error               EXCEPTION;
      l_request_id          NUMBER;
      l_req_data            VARCHAR2 (255);
      l_data_count          NUMBER;
   BEGIN
      null;
   END load_file;


END JAI_GSTR2A_PKG;
/


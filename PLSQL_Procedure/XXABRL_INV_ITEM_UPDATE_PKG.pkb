CREATE OR REPLACE PACKAGE BODY APPS.XXABRL_INV_ITEM_UPDATE_PKG AS
  PROCEDURE XXABRL_ITEM_MAIN(errbuf   OUT VARCHAR2,
                             retcode  OUT VARCHAR2,
                             P_ACTION IN VARCHAR2) IS
    v_request_id   NUMBER;
    v_request_id_1 NUMBER;
    lv_phase       VARCHAR2(1000);
    lv_status      VARCHAR2(1000);
    lv_dev_phase   VARCHAR2(1000);
    lv_dev_status  VARCHAR2(1000);
    lv_message     VARCHAR2(1000);
    l_phase        VARCHAR2(1000);
    l              BOOLEAN;
  BEGIN
    if P_ACTION = 'Y' then
      XXABRL_ITEM_VALIDATE;
    ELSif P_ACTION = 'N' then
      XXABRL_ITEM_VALIDATE_INSERT;
    ELSif P_ACTION = 'S' then
      fnd_file.put_line(fnd_file.LOG,
                        'Submitting Request for Item creation');
      v_request_id := fnd_request.submit_request('INV' -- Application
                                                ,
                                                 'INCOIN' -- Program short name
                                                ,
                                                 NULL,
                                                 NULL,
                                                 FALSE,
                                                 97 --p_org_id
                                                ,
                                                 1 --p_all_org
                                                ,
                                                 1 --p_val_item_flag-
                                                ,
                                                 1 --p_pro_item_flag
                                                ,
                                                 1 --p_del_rec_flag
                                                ,
                                                 1 --p_xset_id--
                                                ,
                                                 2); --p_run_mode
      COMMIT;
      fnd_file.put_line(fnd_file.LOG,
                        ' Submited concurrent Request ID' || v_request_id);
      /*v_request_id :=FND_REQUEST.SUBMIT_REQUEST
      ( 'INV'
           ,'INCOIN'
           ,'Item Import'
           ,SYSDATE
           ,FALSE
           ,97  --p_ORGANIZATION_ID --org_id
           ,1 --'Yes'  all organizations
           ,1 --'Yes' Validate Items
           ,1 --'Yes' Process Items
           ,2 --'Yes' Delete Processed Rows
        ,100  --Item Set to be processed
        ,1   --CREATE new Items or UPDATE existing Items
      ,CHR(0)
      );*/
      /* COMMIT;
            fnd_file.put_line (fnd_file.LOG,
                               ' Submited concurrent Request ID' || v_request_id
                              );
          fnd_file.put_line(fnd_file.output,'Please see the output of Item Import program request id :'||v_request_id );
          fnd_file.put_line(fnd_file.output,'........................................................................' );
           l :=
               fnd_concurrent.wait_for_request (request_id      => v_request_id,
                                                INTERVAL        => 60,
                                                max_wait        => 0,
                                                phase           => lv_phase,
                                                status          => lv_status,
                                                dev_phase       => lv_dev_phase,
                                                dev_status      => lv_dev_status,
                                                MESSAGE         => lv_message
                                               );
           v_request_id_1 :=
               fnd_request.submit_request
                  ('INV'                                              -- Application
                        ,
                   'INV_ITEM_CAT_ASSIGN_OI'                    -- Program short name
                                           ,
                   NULL,
                   NULL,
                   FALSE,
                   1
      --p_rec_set_id                                                                                                         ,1--p_upload_rec_flag-
                    ,
                   1
                  );                                               --p_del_rec_field
            COMMIT;
            fnd_file.put_line (fnd_file.LOG,
                               ' Submited concurrent Request ID' || v_request_id_1
                              );
         /*  l :=
               fnd_concurrent.wait_for_request (request_id      => v_request_id_1,
                                                INTERVAL        => 60,
                                                max_wait        => 0,
                                                phase           => lv_phase,
                                                status          => lv_status,
                                                dev_phase       => lv_dev_phase,
                                                dev_status      => lv_dev_status,
                                                MESSAGE         => lv_message
                                               );*/
      /*inv_item_catalog_elem_pub.process_item_catalog_grp_recs (errbuf,
       retcode,
       1,
       1,
       1,
       1,
       NULL,
       NULL,
       NULL,
       NULL,
       NULL
      );*/
    end if;
    COMMIT;
  END XXABRL_ITEM_MAIN;
  --------------- Procedure to Validate data -------------------------------------------------------------------
  PROCEDURE XXABRL_ITEM_VALIDATE IS
    CURSOR c_master IS
      SELECT ROWID, CSTM.*
        FROM XXABRL.XXABRL_INV_SYS_ITEM_INTF_CSTM CSTM
       WHERE complete_flag IN ('N', 'E');
    CURSOR c_category IS
      SELECT CATEGORY_SET_NAME, ITEM_CATEGORY
        FROM XXABRL.XXABRL_INV_SYS_ITEM_INTF_CSTM
       WHERE complete_flag IN ('N', 'E');
    CURSOR c_child_organization IS
      SELECT mp.organization_id
        FROM mtl_parameters mp
       WHERE mp.master_organization_id =
             (SELECT mp1.organization_id
                FROM mtl_parameters mp1
               WHERE mp1.ORGANIZATION_CODE = '000')
         AND mp.organization_code IN
          ('982', '901', '902', '903', '904', '905', '906', '907', '043',
              '985', '986', '908', '909', '910', '911', '912',
              '741','742','761','762','763','781','782','801','802','011','012','013',   
              '018','019','020','021','025','026','030','031')
         AND mp.organization_id <> mp.master_organization_id;
    invalid_method EXCEPTION;
    l_err_flag          NUMBER;
    l_name              VARCHAR2(40);
    l_item_count        NUMBER;
    v_error_item_Count  number := 0;
    v_valid_item_Count  number := 0;
    l_organization_id   NUMBER;
    l_template_name     VARCHAR2(40);
    l_template_id       NUMBER;
    l_uom_code          VARCHAR2(40);
    l_category_set_id   NUMBER;
    l_category_id       NUMBER;
    l_category_set_name VARCHAR2(40);
    l_BUYER_ID          NUMBER;
    l_ASSET_CATEGORY_ID NUMBER;
    cnt                 NUMBER;
    lp_get_user_id      NUMBER := FND_PROFILE.VALUE('USER_ID');
    V_Error_Message     Varchar2(1000);
    V_Data_Count NUMBER := 0;
  BEGIN
   /* UPDATE XXABRL_INV_SYS_ITEM_INTF_CSTM
       Set error = 'Duplicate Records In Staging table>>> ', complete_flag = 'E'
     WHERE SEGMENT1 IN (SELECT segment1
                          FROM XXABRL_INV_SYS_ITEM_INTF_CSTM
                         GROUP BY segment1
                        HAVING COUNT(1) > 1) ;
                        COMMIT;*/
     fnd_file.put_line(fnd_file.output,
                             '---------------------------Validations staring------------------------------------');
    FOR i IN c_master LOOP
      l_err_flag      := 1;
      V_Error_Message := NULL;
---------------------------- Validation for Item  Number duplication in Staging Table-----------------------------------------------------
begin
        IF i.segment1 IS NOT NULL 
        then
          BEGIN
            V_Data_Count := 0;
            SELECT COUNT(1)
              INTO V_Data_Count
              FROM XXABRL_INV_SYS_ITEM_INTF_CSTM
             WHERE segment1 = i.segment1;
            IF V_Data_Count > 1 THEN
           V_Error_Message := V_Error_Message ||
                             'Duplicate Records In Staging table>>>  ';
          COMMIT;
            END IF;
            V_Data_Count := 0;
          EXCEPTION
            WHEN OTHERS THEN
              V_Data_Count := 0;
              V_Error_Message := V_Error_Message || 
                              ' Duplicate Records In Staging table>>>';
                              COMMIT;
          END;
          END IF;
          end;
 -----------------------Validate Master Org-------------------------------------------------------------------------------------
      BEGIN
        fnd_file.put_line(fnd_file.output, 'Validating  Master Org:');
        SELECT ORGANIZATION_CODE, organization_id
          INTO l_name, l_organization_id
          FROM mtl_parameters mp
         WHERE mp.ORGANIZATION_CODE = '000';
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          l_err_flag := 2;
          V_Error_Message := V_Error_Message ||
                             'Invalid Master Organization>>  ';
          COMMIT;
      END;
      --------------------------------------Validate Item  In mtl_system_items_b---------------------------------------------------------
      BEGIN
        fnd_file.put_line(fnd_file.output,
                          'Validating  Item: ' || ' ' || i.segment1);
        SELECT COUNT(*)
          INTO l_item_count
          FROM mtl_system_items_b
         WHERE segment1 = i.segment1;
        IF l_item_count > 0 THEN
          l_err_flag := 2;
          V_Error_Message := V_Error_Message || 'Item Already Exists>>>  ';
        END IF;
        COMMIT;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          l_err_flag := 2;
          V_Error_Message := V_Error_Message || 'Invalid Item>>>  ';
          COMMIT;
      END;
      ----------------------------------Validate Template---------------------------------------------------------------------------------------------
      BEGIN
        fnd_file.put_line(fnd_file.output,
                          'Validating  Template: ' || ' ' ||
                          i.template_name);
        SELECT template_name, template_id
          INTO l_template_name, l_template_id
          FROM mtl_item_templates
         WHERE TRIM(TEMPLATE_NAME) = TRIM(i.template_name);
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          l_err_flag := 2;
          V_Error_Message := V_Error_Message ||
                             'Invalid Item Template>>>   ';
          COMMIT;
      END;
      ----------------------------Validate UOM Code------------------------------------------------------------------------------------------------------
      BEGIN
        fnd_file.put_line(fnd_file.output,
                          'Validating  UOM Code: ' || ' ' ||
                          i.primary_uom_code);
        SELECT uom_code
          INTO l_uom_code
          FROM mtl_units_of_measure
         WHERE UPPER(unit_of_measure) = UPPER(i.primary_uom_code)
            OR UPPER(uom_code) = UPPER(i.primary_uom_code);
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          l_err_flag := 2;
          V_Error_Message := V_Error_Message ||
                             'Invalid Unit of Measure>>>   ';
          COMMIT;
      END;
      ---------------------validating Buyer name-----------------------------------------------------------------------------------------------------------
      BEGIN
        fnd_file.put_line(fnd_file.output,
                          'Validating  Buyer Name: ' || ' ' || i.BUYER_NAME);
        --lp_BUYER_ID:=0;
        SELECT agent_id
          INTO l_BUYER_ID
          FROM PO_AGENTS_V
         WHERE 1 = 1
           AND UPPER(agent_name) = UPPER(i.BUYER_NAME);
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          l_err_flag := 2;
          V_Error_Message := V_Error_Message || 'Invalid Buyer Name>>>   ';
        WHEN OTHERS THEN
          DBMS_OUTPUT.PUT_LINE('  Invalid Buyer ' || SQLERRM);
          FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                            'Invalid Buyer Name>>> ' || SQLERRM);
          COMMIT;
      END;
      --------------------------------------------- VALIDATING ASSET CATEGORY---------------------------------------------------------------------------------
      BEGIN
        fnd_file.put_line(fnd_file.output,
                          'Validating  Asset Category: ' || ' ' ||
                          UPPER(i.ASSET_CATEGORY_1) || '.' ||
                          UPPER(i.ASSET_CATEGORY_2) || '.' ||
                          UPPER(i.ASSET_CATEGORY_3));
        IF (i.ASSET_CATEGORY_1 IS NOT NULL) AND
           (i.ASSET_CATEGORY_2 IS NOT NULL) THEN
          SELECT CATEGORY_ID
            INTO l_ASSET_CATEGORY_ID
            FROM FA_CATEGORIES_VL
           WHERE UPPER(segment1) || '.' || UPPER(segment2) || '.' ||
                 UPPER(segment3) = UPPER(i.ASSET_CATEGORY_1) || '.' ||
                 UPPER(i.ASSET_CATEGORY_2) || '.' ||
                 UPPER(i.ASSET_CATEGORY_3);
          IF l_ASSET_CATEGORY_ID IS NULL THEN
            l_err_flag := 2;
            V_Error_Message := V_Error_Message ||
                               'Invalid Asset Category>>>   ' || ' ' ||
                               i.segment1;
            fnd_file.put_line(fnd_file.output,
                              'INVALID ASSET CATEGORY ID>>>:' || ' ' ||
                              i.segment1);
            COMMIT;
          END IF;
        ELSIF
         ((i.ASSET_CATEGORY_1 IS NULL) AND (i.ASSET_CATEGORY_2 IS NOT NULL)) OR
         ((i.ASSET_CATEGORY_1 IS NOT NULL) AND (i.ASSET_CATEGORY_2 IS NULL)) THEN
          V_Error_Message := V_Error_Message ||
                             'Invalid Asset Category>>>   ' || ' ' ||
                             i.segment1;
          fnd_file.put_line(fnd_file.output,
                            'INVALID ASSET CATEGORY ID>>>:' || ' ' ||
                            i.segment1);
          COMMIT;
        ELSE
          l_ASSET_CATEGORY_ID := NULL;
        END IF;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          --l_ASSET_CATEGORY_ID := NULL;
          V_Error_Message := V_Error_Message || 'Error No data found>>>';
        WHEN OTHERS THEN
          DBMS_OUTPUT.PUT_LINE('  Invalid Asset Category>> ' || SQLERRM);
          FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                            'Invalid Asset Category>> ' || SQLERRM);
          COMMIT;
      END;
      --------------------------------END OF VALIDATION FOR ASSET CATEGORY---------------------------------------------------------------------  
      -----------------------------------------Validate category Sets and Category Combinations------------------------------------------------
      -- FOR j1 IN c_category LOOP
      BEGIN
        fnd_file.put_line(fnd_file.output,
                          'Validating  category Set is  ' || ' ' ||
                          i.category_set_name || ' And Item Category is ' ||
                          i.ITEM_CATEGORY);
        SELECT category_set_id, category_set_name, category_id
          INTO l_category_set_id, l_category_set_name, l_category_id
          FROM mtl_category_sets mcs, MTL_CATEGORIES_B_KFV mc
         WHERE mcs.category_set_name = i.category_set_name
           AND mcs.structure_id = mc.structure_id
           AND UPPER(mc.CONCATENATED_SEGMENTS) = UPPER(i.ITEM_CATEGORY);
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          l_err_flag := 2;
          V_Error_Message := V_Error_Message ||
                             'Invalid category set or category combination>>>   ';
          COMMIT;
      END;
      --  END LOOP;
      -----------------------------------------END of Validations for category Sets and Category Combinations---------------------------------------
      -- Where ROWID = c_cur2.ROWID;
      if V_Error_Message is not null then
        FND_FILE.PUT_LINE(FND_FILE.LOG, '***ITEM Invalid');
        Update XXABRL_INV_SYS_ITEM_INTF_CSTM
           Set error = V_Error_Message, complete_flag = 'E'
         Where ROWID = i.ROWID;
        v_error_item_Count := v_error_item_Count + 1;
        fnd_file.put_line(fnd_file.output,
                          '-----------------------------------------------------------------------------------');
        fnd_file.put_line(fnd_file.output, 'Details of Rejected Record:');
        fnd_file.put_line(fnd_file.output,
                          '-----------------------------------------------------------------------------------');
        fnd_file.put_line(fnd_file.output,
                          'Item                  Description                                Error');
        fnd_file.put_line(fnd_file.output,
                          i.segment1 || '        ' || i.ITEM_DESCRIPTION ||
                          '                   ' || V_Error_Message);
        fnd_file.put_line(fnd_file.output,
                          '-----------------------------------------------------------------------------------');
      Else
        FND_FILE.PUT_LINE(FND_FILE.LOG, 'Item Valid');
        Update XXABRL_INV_SYS_ITEM_INTF_CSTM
           Set organization_id   = l_organization_id,
               uom_code          = l_uom_code,
               template_id       = l_template_id,
               BUYER_ID          = l_BUYER_ID,
               ASSET_CATEGORY_ID = l_ASSET_CATEGORY_ID,
               category_set_id   = l_category_set_id,
               category_id       = l_category_id,
               error             = NULL,
               complete_flag     = 'V'
         Where ROWID = i.ROWID;
        v_valid_item_Count := v_valid_item_Count + 1;
        --   v_valid_item_Count := v_valid_item_Count + 1;
        Commit;
      End If;
      V_Error_Message := null;
      FND_FILE.PUT_LINE(FND_FILE.LOG,
                        'Validation Ends for Item ' || i.segment1);
      FND_FILE.PUT_LINE(FND_FILE.LOG,
                        '--------------------------------------------------' ||
                        i.segment1);
    End Loop;
    FND_FILE.PUT_LINE(FND_FILE.LOG,
                      '@@@Invalid Items ' || v_error_item_Count);
    FND_FILE.PUT_LINE(FND_FILE.LOG,
                      '@@@valid Items ' || v_valid_item_Count);
    FND_FILE.PUT_LINE(FND_FILE.LOG, '***Validation Procedure Ends***');
    fnd_file.put_line(fnd_file.output,
                      '-----------------------Validation Procedure Ends-----------------------------------');
  end XXABRL_ITEM_VALIDATE;
  --------------------------------------------------------------------------------------------------------------------------------    
  Procedure XXABRL_ITEM_VALIDATE_INSERT IS
    CURSOR C1 IS
      SELECT ROWID, CSTM.*
        FROM XXABRL.XXABRL_INV_SYS_ITEM_INTF_CSTM CSTM
       WHERE complete_flag = 'V';
    CURSOR C2 IS
      SELECT ROWID, segment1, organization_id, category_set_id, category_id
        FROM XXABRL.XXABRL_INV_SYS_ITEM_INTF_CSTM
       WHERE complete_flag = 'V';
    CURSOR C3 IS
      SELECT ROWID, mp.organization_id
        FROM mtl_parameters mp
       WHERE mp.master_organization_id =
             (SELECT mp1.organization_id
                FROM mtl_parameters mp1
               WHERE mp1.ORGANIZATION_CODE = '000')
         AND mp.organization_code IN
              ('982', '901', '902', '903', '904', '905', '906', '907', '043',
              '985', '986', '908', '909', '910', '911', '912',
              '741','742','761','762','763','781','782','801','802','011','012','013',   
              '018','019','020','021','025','026','030','031')
         AND mp.organization_id <> mp.master_organization_id;
    lp_get_user_id      NUMBER := FND_PROFILE.VALUE('USER_ID');
    l_expense_account   number;
    l_expense_account_c number;
  BEGIN
    fnd_file.put_line(fnd_file.output, 'Starting Inserting item');
    l_expense_account := NULL;
    FOR l_rec IN C1 LOOP
      FND_FILE.PUT_LINE(FND_FILE.LOG,
                        'Inserting Item l_rec:' || l_rec.segment1);
      IF l_rec.ASSET_CATEGORY_ID IS NOT NULL and l_rec.organization_id = 97 THEN
        SELECT code_combination_id
          into l_expense_account
          FROM gl_code_combinations_kfv gcc
         WHERE gcc.concatenated_segments =
               '11.000.982.0000000.00.113901.000.0000';
      elsIF l_rec.ASSET_CATEGORY_ID IS NULL THEN
        l_expense_account := null;
      end if;
      FND_FILE.PUT_LINE(FND_FILE.LOG,
                        'Inserting Expense Account for Master Item :' ||
                        l_rec.segment1 || ' And Expence Account ID :' ||
                        l_expense_account);
      fnd_file.put_line(fnd_file.LOG,
                        'Completed Validation of Data, Inserting in mtl_system_items_interface ');
      INSERT INTO mtl_system_items_interface
        (organization_id,
         --  start_date_active,
         LAST_UPDATE_DATE,
         LAST_UPDATED_BY,
         CREATION_DATE,
         CREATED_BY,
         segment1,
         description,
         primary_uom_code,
         process_flag,
         MTL_TRANSACTIONS_ENABLED_FLAG,
         STOCK_ENABLED_FLAG,
         transaction_type,
         set_process_id,
         template_id,
         attribute14,
         LIST_PRICE_PER_UNIT,
         BUYER_ID,
         INVENTORY_ITEM_STATUS_CODE,
         ASSET_CATEGORY_ID,
         EXPENSE_ACCOUNT)
      VALUES
        (l_rec.organization_id,
         --   SYSDATE,
         SYSDATE,
         lp_get_user_id, --1249, --XXABRL
         SYSDATE,
         lp_get_user_id, --1249,
         l_rec.segment1,
         l_rec.item_description,
         l_rec.uom_code,
         1,
         'N',
         'N',
         'UPDATE',
         1,
         l_rec.template_id,
         'CUSTOM TO INV',
         DECODE(l_rec.LIST_PRICE_PER_UNIT,
                NULL,
                1,
                0,
                1,
                l_rec.LIST_PRICE_PER_UNIT),
         l_rec.BUYER_ID,
         l_rec.ITEM_STATUS,
         l_rec.ASSET_CATEGORY_ID,
         l_expense_account);
      fnd_file.put_line(fnd_file.output,
                        'Inserting item:' || ' ' || l_rec.segment1);
      --------------------------------------Inserting for Child Organization Items---------------------------------------------------------
      FOR p IN c3 LOOP
        IF l_rec.ASSET_CATEGORY_ID IS NOT NULL and p.organization_id = 147 THEN
          SELECT code_combination_id
            into l_expense_account_c
            FROM gl_code_combinations_kfv gcc
           WHERE gcc.concatenated_segments =
                 '11.000.982.0000000.00.113901.000.0000';
        elsIF l_rec.ASSET_CATEGORY_ID IS NOT NULL and
              p.organization_id = 181 THEN
          SELECT code_combination_id
            into l_expense_account_c
            FROM gl_code_combinations_kfv gcc
           WHERE gcc.concatenated_segments =
                 '11.000.982.0000043.00.113901.000.0000';
        elsIF l_rec.ASSET_CATEGORY_ID IS NOT NULL and
              p.organization_id = 481 THEN
          SELECT code_combination_id
            into l_expense_account_c
            FROM gl_code_combinations_kfv gcc
           WHERE gcc.concatenated_segments =
                 '31.000.742.0000011.00.113901.000.0000';
         elsIF l_rec.ASSET_CATEGORY_ID IS NOT NULL and
              p.organization_id = 96 THEN
          SELECT code_combination_id
            into l_expense_account_c
            FROM gl_code_combinations_kfv gcc
           WHERE gcc.concatenated_segments =
                 '31.000.781.0000000.00.113901.000.0000';
         elsIF l_rec.ASSET_CATEGORY_ID IS NOT NULL and
              p.organization_id =503 THEN
          SELECT code_combination_id
            into l_expense_account_c
            FROM gl_code_combinations_kfv gcc
           WHERE gcc.concatenated_segments =
                 '31.000.763.0000021.00.113901.000.0000';
         elsIF l_rec.ASSET_CATEGORY_ID IS NOT NULL and
              p.organization_id = 93 THEN
          SELECT code_combination_id
            into l_expense_account_c
            FROM gl_code_combinations_kfv gcc
           WHERE gcc.concatenated_segments =
                 '31.000.741.0000000.00.113901.000.0000';
        elsIF l_rec.ASSET_CATEGORY_ID IS NOT NULL and
              p.organization_id = 141 THEN
          SELECT code_combination_id
            into l_expense_account_c
            FROM gl_code_combinations_kfv gcc
           WHERE gcc.concatenated_segments =
                 '11.000.102.0000000.00.113901.000.0000';
        elsIF l_rec.ASSET_CATEGORY_ID IS NOT NULL and
              p.organization_id = 142 THEN
          SELECT code_combination_id
            into l_expense_account_c
            FROM gl_code_combinations_kfv gcc
           WHERE gcc.concatenated_segments =
                 '11.000.103.0000000.00.113901.000.0000';
        elsIF l_rec.ASSET_CATEGORY_ID IS NOT NULL and
              p.organization_id = 143 THEN
          SELECT code_combination_id
            into l_expense_account_c
            FROM gl_code_combinations_kfv gcc
           WHERE gcc.concatenated_segments =
                 '11.000.302.0000000.00.113901.000.0000';
        elsIF l_rec.ASSET_CATEGORY_ID IS NOT NULL and
              p.organization_id = 144 THEN
          SELECT code_combination_id
            into l_expense_account_c
            FROM gl_code_combinations_kfv gcc
           WHERE gcc.concatenated_segments =
                 '11.000.562.0000000.00.113901.000.0000';
        elsIF l_rec.ASSET_CATEGORY_ID IS NOT NULL and
              p.organization_id = 145 THEN
          SELECT code_combination_id
            into l_expense_account_c
            FROM gl_code_combinations_kfv gcc
           WHERE gcc.concatenated_segments =
                 '11.000.122.0000000.00.113901.000.0000';
        elsIF l_rec.ASSET_CATEGORY_ID IS NOT NULL and
              p.organization_id = 146 THEN
          SELECT code_combination_id
            into l_expense_account_c
            FROM gl_code_combinations_kfv gcc
           WHERE gcc.concatenated_segments =
                 '11.000.122.0000000.00.113901.000.0000';
        elsIF l_rec.ASSET_CATEGORY_ID IS NOT NULL and
              p.organization_id = 161 THEN
          SELECT code_combination_id
            into l_expense_account_c
            FROM gl_code_combinations_kfv gcc
           WHERE gcc.concatenated_segments =
                 '11.000.382.0000000.00.113901.000.0000';
        elsIF l_rec.ASSET_CATEGORY_ID IS NOT NULL and
              p.organization_id = 201 THEN
          SELECT code_combination_id
            into l_expense_account_c
            FROM gl_code_combinations_kfv gcc
           WHERE gcc.concatenated_segments =
                 '11.000.985.0000000.00.113901.000.0000';
        elsIF l_rec.ASSET_CATEGORY_ID IS NOT NULL and
              p.organization_id = 202 THEN
          SELECT code_combination_id
            into l_expense_account_c
            FROM gl_code_combinations_kfv gcc
           WHERE gcc.concatenated_segments =
                 '11.000.986.0000000.00.113901.000.0000';
        elsIF l_rec.ASSET_CATEGORY_ID IS NOT NULL and
              p.organization_id = 203 THEN
          SELECT code_combination_id
            into l_expense_account_c
            FROM gl_code_combinations_kfv gcc
           WHERE gcc.concatenated_segments =
                 '11.000.342.0000000.00.113901.000.0000';
        elsIF l_rec.ASSET_CATEGORY_ID IS NOT NULL and
              p.organization_id = 221 THEN
          SELECT code_combination_id
            into l_expense_account_c
            FROM gl_code_combinations_kfv gcc
           WHERE gcc.concatenated_segments =
                 '11.000.322.0000000.00.113901.000.0000';
        elsIF l_rec.ASSET_CATEGORY_ID IS NOT NULL and
              p.organization_id = 222 THEN
          SELECT code_combination_id
            into l_expense_account_c
            FROM gl_code_combinations_kfv gcc
           WHERE gcc.concatenated_segments =
                 '11.000.362.0000000.00.113901.000.0000';
        elsIF l_rec.ASSET_CATEGORY_ID IS NOT NULL and
              p.organization_id = 223 THEN
          SELECT code_combination_id
            into l_expense_account_c
            FROM gl_code_combinations_kfv gcc
           WHERE gcc.concatenated_segments =
                 '11.000.342.0000000.00.113901.000.0000';
        elsIF l_rec.ASSET_CATEGORY_ID IS NOT NULL and
              p.organization_id = 224 THEN
          SELECT code_combination_id
            into l_expense_account_c
            FROM gl_code_combinations_kfv gcc
           WHERE gcc.concatenated_segments =
                 '11.000.342.0000000.00.113901.000.0000';
          elsIF l_rec.ASSET_CATEGORY_ID IS NOT NULL and
              p.organization_id = 84 THEN
          SELECT code_combination_id
            into l_expense_account_c
            FROM gl_code_combinations_kfv gcc
           WHERE gcc.concatenated_segments =
                 '31.000.742.0000000.00.113901.000.0000';
           elsIF l_rec.ASSET_CATEGORY_ID IS NOT NULL and --VIJAYAWADA DC
              p.organization_id = 504 THEN
          SELECT code_combination_id
            into l_expense_account_c
            FROM gl_code_combinations_kfv gcc
           WHERE gcc.concatenated_segments =
                 '31.000.742.0000012.00.113901.000.0000';
          elsIF l_rec.ASSET_CATEGORY_ID IS NOT NULL and --VISAKAPATNAM DC
              p.organization_id = 521 THEN
          SELECT code_combination_id
            into l_expense_account_c
            FROM gl_code_combinations_kfv gcc
           WHERE gcc.concatenated_segments =
                 '31.000.742.0000013.00.113901.000.0000';
          elsIF l_rec.ASSET_CATEGORY_ID IS NOT NULL and   --TSRL KA SM OU
              p.organization_id = 87 THEN
          SELECT code_combination_id
            into l_expense_account_c
            FROM gl_code_combinations_kfv gcc
           WHERE gcc.concatenated_segments =
                 '31.000.762.0000000.00.113901.000.0000';
          elsIF l_rec.ASSET_CATEGORY_ID IS NOT NULL and
              p.organization_id = 94 THEN  --TSRL KA ZO OU
          SELECT code_combination_id
            into l_expense_account_c
            FROM gl_code_combinations_kfv gcc
           WHERE gcc.concatenated_segments =
                 '31.000.761.0000000.00.113901.000.0000';
           elsIF l_rec.ASSET_CATEGORY_ID IS NOT NULL and
              p.organization_id = 86 THEN -- TSRL KA HM OU
          SELECT code_combination_id
            into l_expense_account_c
            FROM gl_code_combinations_kfv gcc
           WHERE gcc.concatenated_segments =
                 '31.000.763.0000000.00.113901.000.0000';
           elsIF l_rec.ASSET_CATEGORY_ID IS NOT NULL and
              p.organization_id = 381 THEN --BANGALORE DC
          SELECT code_combination_id
            into l_expense_account_c
            FROM gl_code_combinations_kfv gcc
           WHERE gcc.concatenated_segments =
                 '31.000.762.0000018.00.113901.000.0000';
           elsIF l_rec.ASSET_CATEGORY_ID IS NOT NULL and
              p.organization_id = 461 THEN --HUBLI DC
          SELECT code_combination_id
            into l_expense_account_c
            FROM gl_code_combinations_kfv gcc
           WHERE gcc.concatenated_segments =
                 '31.000.762.0000020.00.113901.000.0000';
           elsIF l_rec.ASSET_CATEGORY_ID IS NOT NULL and
              p.organization_id = 502 THEN --MANGALORE DC
          SELECT code_combination_id
            into l_expense_account_c
            FROM gl_code_combinations_kfv gcc
           WHERE gcc.concatenated_segments =
                 '31.000.762.0000019.00.113901.000.0000';
          elsIF l_rec.ASSET_CATEGORY_ID IS NOT NULL and
              p.organization_id = 89 THEN --TSRL KL SM OU
          SELECT code_combination_id
            into l_expense_account_c
            FROM gl_code_combinations_kfv gcc
           WHERE gcc.concatenated_segments =
                 '31.000.802.0000000.00.113901.000.0000';
          elsIF l_rec.ASSET_CATEGORY_ID IS NOT NULL and
              p.organization_id = 95 THEN --TSRL KL ZO OU
          SELECT code_combination_id
            into l_expense_account_c
            FROM gl_code_combinations_kfv gcc
           WHERE gcc.concatenated_segments =
                 '31.000.801.0000000.00.113901.000.0000';
           elsIF l_rec.ASSET_CATEGORY_ID IS NOT NULL and
              p.organization_id = 401 THEN --CALICUT DC
          SELECT code_combination_id
            into l_expense_account_c
            FROM gl_code_combinations_kfv gcc
           WHERE gcc.concatenated_segments =
                 '31.000.802.0000031.00.113901.000.0000';
                 elsIF l_rec.ASSET_CATEGORY_ID IS NOT NULL and
              p.organization_id = 501 THEN --KOCHI DC
          SELECT code_combination_id
            into l_expense_account_c
            FROM gl_code_combinations_kfv gcc
           WHERE gcc.concatenated_segments =
                 '31.000.802.0000030.00.113901.000.0000';
                 elsIF l_rec.ASSET_CATEGORY_ID IS NOT NULL and
              p.organization_id = 91 THEN --TSRL TN SM OU
          SELECT code_combination_id
            into l_expense_account_c
            FROM gl_code_combinations_kfv gcc
           WHERE gcc.concatenated_segments =
                 '31.000.782.0000000.00.113901.000.0000';
                 elsIF l_rec.ASSET_CATEGORY_ID IS NOT NULL and
              p.organization_id = 421 THEN --CHENNAI DC
          SELECT code_combination_id
            into l_expense_account_c
            FROM gl_code_combinations_kfv gcc
           WHERE gcc.concatenated_segments =
                 '31.000.782.0000025.00.113901.000.0000';
                 elsIF l_rec.ASSET_CATEGORY_ID IS NOT NULL and
              p.organization_id = 441 THEN --COIMBATORE DC
          SELECT code_combination_id
            into l_expense_account_c
            FROM gl_code_combinations_kfv gcc
           WHERE gcc.concatenated_segments =
                 '31.000.782.0000026.00.113901.000.0000';
        elsIF l_rec.ASSET_CATEGORY_ID IS NULL THEN
          l_expense_account := null;
        end if;
        FND_FILE.PUT_LINE(FND_FILE.LOG,
                          'Inserting Expense Account for Child Item :' ||
                          l_rec.segment1 || ' And Expence Account ID :' ||
                          l_expense_account_c || ' For Org ' ||
                          p.organization_id);
        INSERT INTO mtl_system_items_interface
          (organization_id,
           --      start_date_active,
           LAST_UPDATE_DATE,
           LAST_UPDATED_BY,
           CREATION_DATE,
           CREATED_BY,
           segment1,
           description,
           primary_uom_code,
           process_flag,
          MTL_TRANSACTIONS_ENABLED_FLAG,
         STOCK_ENABLED_FLAG,
           transaction_type,
           set_process_id,
           template_id,
           attribute14,
           LIST_PRICE_PER_UNIT,
           BUYER_ID,
           INVENTORY_ITEM_STATUS_CODE,
           EXPENSE_ACCOUNT)
        VALUES
          (p.organization_id,
           -- SYSDATE,
           SYSDATE,
           lp_get_user_id, --1249, --XXABRL
           SYSDATE,
           lp_get_user_id, --1249,
           l_rec.segment1,
           l_rec.item_description,
           l_rec.uom_code,
           1,
           'N',
           'N',
           'UPDATE',
           1,
           l_rec.template_id,
           'CUSTOM TO INV',
           DECODE(l_rec.LIST_PRICE_PER_UNIT,
                  NULL,
                  1,
                  0,
                  1,
                  l_rec.LIST_PRICE_PER_UNIT),
           l_rec.BUYER_ID,
           l_rec.ITEM_STATUS,
           l_expense_account_c);
      END LOOP;
      l_expense_account_c := NULL;
      ------------------------------------ Inserting Categoris---------------------------------------------------------------------------------------
      --  FOR j IN C2 LOOP
      fnd_file.put_line(fnd_file.output,
                        'Inserting Category set:' || ' ' ||
                        l_rec.category_set_id || '  AND Category ID ' ||
                        l_rec.category_id || ' For Item' || l_rec.segment1);
      -- IF ((j.category_set_id IS NOT NULL) AND (j.category_id IS NOT NULL)) THEN
      /*
     INSERT INTO mtl_item_categories_interface
        (item_number,
         organization_id,
         transaction_type,
         category_set_id,
         category_id,
         set_process_id,
         process_flag)
      VALUES
        (l_rec.segment1,
         l_rec.organization_id,
         'UPDATE',
         l_rec.category_set_id,
         l_rec.category_id,
         1,
         1); 
         
     */    
      /*   FOR l IN c_child_organization LOOP
      INSERT INTO mtl_item_categories_interface
                        (item_number, organization_id,
                         transaction_type, category_set_id,
                         category_id, set_process_id, process_flag
                        )
                 VALUES (i.segment1, l.organization_id,
                         'CREATE', l_category_set_id,
                         l_category_id, 1, 1
                        );
                        END LOOP;*/
      -- END IF;
      --   END LOOP;
      UPDATE XXABRL_INV_SYS_ITEM_INTF_CSTM
         SET complete_flag = 'Y'
       Where ROWID = l_rec.ROWID;
      COMMIT;
    END LOOP;
  END XXABRL_ITEM_VALIDATE_INSERT;
END XXABRL_INV_ITEM_UPDATE_PKG; 
/


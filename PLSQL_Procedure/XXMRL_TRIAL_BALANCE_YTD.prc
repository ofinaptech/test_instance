CREATE OR REPLACE PROCEDURE APPS.XXMRL_TRIAL_BALANCE_YTD (
   ERRBUF    OUT VARCHAR2,
   RETCODE   OUT VARCHAR2)
AS
   /**********************************************************************************************************************************************
               Name: Trial Balance Report O/P for YTD
               Change Record:
              =====================
   =========================
   =========================
   =========================
   =========================
              Version         Date               Author                    Remarks                  Documnet Ref
              =======   ==========   ==
   ===========             ==================  ===============
   =========================
   =============
              1.0.0        09-Jul-2020       Lokesh Poojari       Initial Version

    ************************************************************************************************************************************************/

   l_file_p   UTL_FILE.file_type;

   CURSOR cur_Trial
   IS
      SELECT accounts,
             description,
             Debits,
             Credits,
             Debits - Credits Ending_Balance
        FROM (  SELECT gcc.segment6 accounts,
                       (SELECT ffvl.description
                          FROM apps.fnd_flex_values_vl ffvl
                         WHERE ffvl.flex_value_set_id = 1013469
                               AND ffvl.flex_value = gcc.segment6)
                          description,
                       NVL (SUM (gll.accounted_dr), 0) Debits,
                       NVL (SUM (gll.accounted_cr), 0) Credits
                  FROM apps.gl_je_headers glh,
                       apps.gl_je_lines gll,
                       apps.gl_code_combinations gcc
                 WHERE 1 = 1 AND glh.je_header_id = gll.je_header_id
                       --AND gll.effective_date BETWEEN '01-apr-2000' AND '31-jan-2020'
                       AND gcc.segment6 IN
                              ('361905', '362001', '227010', '362008', '362009')
                       AND gcc.CODE_COMBINATION_ID = gll.CODE_COMBINATION_ID
                       -- AND NVL (glh.accrual_rev_status, 'NR') <> 'R'
                       --AND glh.description NOT LIKE '%Reverse%RETEK%'
                       AND gcc.segment1 = '11'
                       AND glh.status = 'P'
                       AND gcc.segment3 NOT IN ('282')
              GROUP BY gcc.segment6) aa;
BEGIN
   --Creating a file for the below generated Trial Balance data
   l_file_p :=
      UTL_FILE.fopen (
         'TRIAL_BALANCE',
            'Trial_Balance_'
         || TO_CHAR (SYSDATE, 'DD')
         || TO_CHAR (SYSDATE, 'MM')
         || TO_CHAR (SYSDATE, 'YY')
         || '.csv',
         'W');
   UTL_FILE.put_line (
      l_file_p,
         'ACCOUNTS'
      || '|'
      || 'DESCRIPTION'
      || '|'
      || 'DEBITS'
      || '|'
      || 'CREDITS'
      || '|'
      || 'ENDING_BALANCE');

   FOR TRIAL_BALANCE IN cur_Trial
   LOOP
      UTL_FILE.put_line (
         l_file_P,
            TRIAL_BALANCE.ACCOUNTS
         || '|'
         || TRIAL_BALANCE.DESCRIPTION
         || '|'
         || TRIAL_BALANCE.DEBITS
         || '|'
         || TRIAL_BALANCE.CREDITS
         || '|'
         || TRIAL_BALANCE.ENDING_BALANCE);
   END LOOP;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      UTL_FILE.fclose (l_file_p);
      DBMS_OUTPUT.PUT_LINE ('No Data Found');
   WHEN UTL_FILE.invalid_path
   THEN
      UTL_FILE.fclose (l_file_p);
      DBMS_OUTPUT.PUT_LINE ('Invalid Path');
   WHEN UTL_FILE.invalid_filehandle
   THEN
      UTL_FILE.fclose (l_file_p);
      DBMS_OUTPUT.PUT_LINE ('Invalid File Handle');
   WHEN UTL_FILE.invalid_operation
   THEN
      UTL_FILE.fclose (l_file_p);
      DBMS_OUTPUT.PUT_LINE ('Invalid Operation');
   WHEN UTL_FILE.read_error
   THEN
      UTL_FILE.fclose (l_file_p);
      DBMS_OUTPUT.PUT_LINE ('Read Error');
   WHEN UTL_FILE.write_error
   THEN
      UTL_FILE.fclose (l_file_p);
      DBMS_OUTPUT.PUT_LINE ('Write Error');
   WHEN UTL_FILE.internal_error
   THEN
      UTL_FILE.fclose (l_file_p);
      DBMS_OUTPUT.PUT_LINE ('Internal Error');
   WHEN UTL_FILE.file_open
   THEN
      UTL_FILE.fclose (l_file_p);
      DBMS_OUTPUT.PUT_LINE ('File is Open');
   WHEN UTL_FILE.invalid_filename
   THEN
      UTL_FILE.fclose (l_file_p);
      DBMS_OUTPUT.PUT_LINE ('Invalid File Name');
   WHEN OTHERS
   THEN
      UTL_FILE.fclose (l_file_P);
      DBMS_OUTPUT.PUT_LINE ('Unknown Error' || SQLERRM);
      UTL_FILE.fclose (l_file_p);
END XXMRL_TRIAL_BALANCE_YTD; 
/


CREATE OR REPLACE PROCEDURE APPS.xxabrl_er_inv_auto_load (
   errbuf    OUT   VARCHAR2,
   retcode   OUT   VARCHAR2
)
IS
BEGIN

/*
   =========================================================================================================
   ||   Procedure Name  : xxabrl_er_inv_auto_load
   ||   Description : XXABRL E-Retail File Load Program
   ||
   ||   Version     Date            Author              Modification
   ||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
   ||   1.0.0      18-Nov-2010  Mitul Vanpariya      New Development
   =======================================================================================================*/
   
   DECLARE
      lfile_cnt         NUMBER;
      v_count           NUMBER;
      v_path            VARCHAR2 (500);
      v_req_id          NUMBER;
      v_req_id1         NUMBER;
      v_p_h_path        VARCHAR2 (500);
      v_p_l_path        VARCHAR2 (500);
      v_status_code     VARCHAR2 (2);
      v_l_status_code   VARCHAR2 (2);
      m                 NUMBER;
      i                 NUMBER         := 0;
   BEGIN
      v_path :='/ebiz/shared_u01/PROD/apps/apps_st/appl/xxabrl/12.0.0/interface/E_Retail/AP/Uploading';
                

      --********** Header File Load *********------------
      FOR flst_hdr IN
         (SELECT   *
              FROM (SELECT *
                      FROM TABLE (xxabrl.dir_list (v_path))) flist
             WHERE flist.file_name LIKE
                          'ER%HDR%' || TO_CHAR (TRUNC (SYSDATE), 'DDMMYY')      || '%'
          ORDER BY 1)
      LOOP
         SELECT COUNT (*)
           INTO lfile_cnt
           FROM (SELECT *
                   FROM TABLE (xxabrl.dir_list (v_path))) flist
          WHERE flist.file_name = REPLACE (flst_hdr.file_name, 'HDR', 'LINE');

         i := i + 1;

         SELECT COUNT (*)
           INTO v_count
           FROM xxabrl.xxabrl_er_file_list
          WHERE file_name = flst_hdr.file_name;

         IF lfile_cnt = 1
         THEN
            IF v_count >= 1
            THEN
               fnd_file.put_line (fnd_file.LOG,
                                     'This File Already Uploaded  :->  '
                                  || flst_hdr.file_name
                                 );
            ELSE
               IF flst_hdr.file_name LIKE '%HDR%'
               THEN
                  v_p_h_path := v_path || '/' || flst_hdr.file_name;

                  BEGIN
                     apps.fnd_global.apps_initialize (0, 52299, 200);
                     v_req_id :=
                        fnd_request.submit_request
                              ('XXABRL',
                               'XXABRL_AP_INVOICE_INT_LOAD',
                               'ABRL Payables Interface Invoice Header Load',
                               SYSDATE,
                               FALSE,
                               v_p_h_path
                              );
                     COMMIT;
                     COMMIT;
                  END;

                  v_p_l_path :=
                        v_path
                     || '/'
                     || REPLACE (flst_hdr.file_name, 'HDR', 'LINE');

                  FOR m IN 1 .. 9999999
                  LOOP
                     SELECT status_code
                       INTO v_status_code
                       FROM fnd_concurrent_requests f
                      WHERE f.request_id = v_req_id;

                     IF v_status_code <> 'I' AND v_status_code <> 'R'
                     THEN
                        EXIT;
                     END IF;
                  END LOOP;

--               fnd_file.put_line (fnd_file.LOG,
--                                     'Request Id'
--                                  || v_req_id
--                                  || ' '
--                                  || v_status_code
--                                 );
                  IF v_status_code = 'C'
                  THEN
                     INSERT INTO xxabrl.xxabrl_er_file_list
                          VALUES (flst_hdr.file_name, flst_hdr.file_size,
                                  flst_hdr.file_type, flst_hdr.last_modified);

                     fnd_file.put_line
                               (fnd_file.LOG,
                                   'This Header File Sucessfully Loaded  :->'
                                || flst_hdr.file_name
                               );

                     BEGIN
                        apps.fnd_global.apps_initialize (0, 52299, 200);
                        v_req_id1 :=
                           fnd_request.submit_request
                               ('XXABRL',
                                'XXABRL_AP_INV_LINES_INT_LOAD',
                                'ABRL Payables Interface Invoice Lines Load',
                                SYSDATE,
                                FALSE,
                                v_p_l_path
                               );
                        COMMIT;

                        FOR m IN 1 .. 9999999
                        LOOP
                           SELECT status_code
                             INTO v_l_status_code
                             FROM fnd_concurrent_requests f
                            WHERE f.request_id = v_req_id1;

                           IF v_l_status_code <> 'I'
                              AND v_l_status_code <> 'R'
                           THEN
                              EXIT;
                           END IF;
                        END LOOP;

                        IF v_l_status_code = 'C'
                        THEN
                           INSERT INTO xxabrl.xxabrl_er_file_list
                                VALUES (REPLACE (flst_hdr.file_name,
                                                 'HDR',
                                                 'LINE'
                                                ),
                                        flst_hdr.file_size,
                                        flst_hdr.file_type,
                                        flst_hdr.last_modified);

                           fnd_file.put_line
                                 (fnd_file.LOG,
                                     'This Line   File Sucessfully Loaded  :->'
                                  || REPLACE (flst_hdr.file_name,
                                              'HDR',
                                              'LINE'
                                             )
                                 );
                           COMMIT;

                           DECLARE
                              v_req_id1   NUMBER;
                           BEGIN
                              apps.fnd_global.apps_initialize (0, 52299, 200);
                              v_req_id1 :=
                                 fnd_request.submit_request
                                    ('XXABRL',
                                     'XXABRL_AP_INV_INT_FILE_MOVE',
                                     'ABRL Payable Invoice Interface File Move',
                                     SYSDATE,
                                     FALSE,
                                     flst_hdr.file_name,
                                     REPLACE (flst_hdr.file_name,
                                              'HDR',
                                              'LINE'
                                             ));
--                                        '/d05/applcrp/CRP/apps/apps_st/appl/xxabrl/12.0.0/interface/E_Retail/AP/Uploading/'
--                                     || flst_hdr.file_name,
--                                        '/d05/applcrp/CRP/apps/apps_st/appl/xxabrl/12.0.0/interface/E_Retail/AP/Uploading/'
--                                     || REPLACE (flst_hdr.file_name,
--                                                 'HDR',
--                                                 'LINE'
--                                                )
--                                    );
                              COMMIT;
                           END;
                        END IF;
                     END;
                  END IF;
               END IF;
            END IF;
         ELSE
            fnd_file.put_line (fnd_file.LOG,
                                  'This File Not Uploaded :->  '
                               || flst_hdr.file_name
                               || CHR (10)
                               || 'Line File Not Found :-> '
                               || REPLACE (flst_hdr.file_name, 'HDR', 'LINE')
                               || CHR (10)
                              );
         END IF;
      END LOOP;

      fnd_file.put_line (fnd_file.LOG, 'Total Header File  :->' || i);
   
   END;
END; 
/


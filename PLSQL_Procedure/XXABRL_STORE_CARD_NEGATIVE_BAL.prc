CREATE OR REPLACE PROCEDURE APPS.xxabrl_store_card_negative_bal (
   errbuf    OUT   VARCHAR2,
   retcode   OUT   NUMBER
)
IS
   CURSOR c1
   IS
      SELECT   SUBSTR (hou.NAME, 0, 4) segment1, ofin.customer_name LOCATION,
               ofin.gl_date, SUM (ofin.amount) balance
          FROM apps.xxabrl_navi_ar_int_line_stg2 ofin,
               apps.hr_operating_units hou
         WHERE 1 = 1
           AND ofin.org_id = hou.organization_id
           AND UPPER (ofin.description) LIKE 'STORE%CARD%'
           AND TRUNC (gl_date) >= '01-Apr-2013'
           AND TRUNC (ofin.creation_date) BETWEEN '01-Apr-2013' --TRUNC (SYSDATE) - 5
                                              AND TRUNC (SYSDATE)
      GROUP BY hou.NAME, ofin.customer_name, ofin.gl_date;

   CURSOR c2
   IS
      SELECT   format, segment1, SUM (NVL (ytd_loc_bal, 0)) bal,
               MAX (gl_date) gl_date
          FROM apps.xxabrl_card_location_bal
      GROUP BY format, segment1;

   v_cnt       NUMBER := 0;
   v_bal       NUMBER := 0;
   v_new_bal   NUMBER := 0;
BEGIN
   FOR i IN c1
   LOOP
      SELECT COUNT (*)
        INTO v_cnt
        FROM apps.xxabrl_card_location_bal
       WHERE store_code = i.LOCATION AND gl_date = i.gl_date;

      IF v_cnt = 0
      THEN
         INSERT INTO apps.xxabrl_card_location_bal
              VALUES ('HM', i.segment1, i.LOCATION, i.gl_date, i.balance);

         COMMIT;
      ELSE
         SELECT ytd_loc_bal
           INTO v_bal
           FROM apps.xxabrl_card_location_bal
          WHERE store_code = i.LOCATION AND gl_date = i.gl_date;

--         v_new_bal := v_bal + i.balance;

         UPDATE apps.xxabrl_card_location_bal
            SET ytd_loc_bal = i.balance
          WHERE store_code = i.LOCATION AND gl_date = i.gl_date;

         COMMIT;
      END IF;
   END LOOP;

   FOR j IN c2
   LOOP
      IF j.bal < 0
      THEN
               INSERT INTO apps.xxabrl_card_company_bal
              VALUES (j.format, j.segment1, j.gl_date, j.bal);

         /*update apps.xxabrl_card_company_bal
         set format = j.format, segment1 = j.segment1,gl_date = j.gl_date,ytd_loc_bal = j.bal;*/
              

         COMMIT;
         fnd_file.put_line (fnd_file.output,
                               'Segment1'
                            || CHR (9)
                            || 'Format'
                            || CHR (9)
                            || 'GL Date'
                            || CHR (9)
                            || 'YTD Company Balance'
                           );
         fnd_file.put_line (fnd_file.output,
                               j.segment1
                            || CHR (9)
                            || j.format
                            || CHR (9)
                            || j.gl_date
                            || CHR (9)
                            || j.bal
                           );
      END IF;
   END LOOP;
END; 
/


CREATE OR REPLACE PACKAGE APPS.Xxabrl_Asset_Trnf_Pkg AS
PROCEDURE xxabrl_asset_trn_proc(ERRBUFF             OUT VARCHAR2,
                                RETCODE             OUT NUMBER,
                                p_book_code       IN VARCHAR2,
                                p_date_from    IN VARCHAR2,
                                p_date_to       IN VARCHAR2
                                );
                                
end Xxabrl_Asset_Trnf_Pkg; 
/


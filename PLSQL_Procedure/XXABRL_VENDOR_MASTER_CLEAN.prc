CREATE OR REPLACE PROCEDURE APPS.xxabrl_vendor_master_clean(errbuf out varchar2,
                                 retcode out varchar2,
                                 P_FROM_DATE IN  VARCHAR2,
                                 P_TO_DATE IN   VARCHAR2)
as


 V_VENDOR_NUM  NUMBER;
V_NAME VARCHAR2(150);
V_SITE_NAME VARCHAR2(100);
V_ORG_ID  NUMBER;
v_from_date      VARCHAR2 (11):= TO_CHAR (TO_DATE (SUBSTR (p_from_date, 1, 10), 'RRRR/MM/DD'),'DD-MON-YYYY');
 v_to_date        VARCHAR2 (11):= TO_CHAR (TO_DATE (SUBSTR (p_to_date, 1, 10), 'RRRR/MM/DD'),'DD-MON-YYYY');

CURSOR CUR2 IS
SELECT xx.vendor_id,XX.VENDOR_SITE_ID,xx.ORG_ID,XX.vendor_site_code,SP.VENDOR_NAME,SP.SEGMENT1
 FROM APPS.AP_SUPPLIER_SITES_ALL XX,APPS.AP_SUPPLIERS SP
WHERE SP.VENDOR_ID=XX.VENDOR_ID
AND (SP.END_DATE_ACTIVE IS NULL OR  SP.END_DATE_ACTIVE>SYSDATE)
AND (XX.INACTIVE_DATE IS NULL OR  XX.INACTIVE_DATE>SYSDATE)
and sp.VENDOR_TYPE_LOOKUP_CODE='MERCHANDISE';
--AND xx.VENDOR_ID=1438;
--AND XX.INACTIVE_DATE IS NULL;
  BEGIN
    FOR R2 IN CUR2
    LOOP
    
    
             BEGIN
                SELECT  DISTINCT XX.SEGMENT1,XX.VENDOR_NAME,YY.VENDOR_SITE_CODE,YY.ORG_ID
                INTO V_VENDOR_NUM,V_NAME,V_SITE_NAME,V_ORG_ID
                FROM APPS.AP_SUPPLIERS XX, APPS.AP_SUPPLIER_SITES_ALL YY,APPS.AP_INVOICES_ALL AIA
                WHERE AIA.VENDOR_ID=XX.VENDOR_ID
                AND AIA.VENDOR_SITE_ID=YY.VENDOR_SITE_ID
                AND XX.VENDOR_ID = YY.VENDOR_ID
                AND AIA.VENDOR_ID=R2.VENDOR_ID
                AND AIA.VENDOR_SITE_ID=R2.VENDOR_SITE_ID
                AND AIA.ORG_ID=R2.ORG_ID
                AND AIA.GL_DATE BETWEEN v_from_date  AND v_to_date ;
                exception
                when no_data_found
                then
                 insert into XXABRL_NON_INV_VENDOR(VENDOR_num,
                             VENDOR_NAME,
                             VENDOR_SITE_CODE,
                             VENDOR_SITE_ID,
                             ORG_ID,
                             vendor_id)
                             values(r2.segment1,
                             R2.VENDOR_name,
                             r2.vendor_site_code,
                             R2.VENDOR_SITE_ID,
                             R2.ORG_ID,
                             r2.vendor_id);

             END;
       -- utl_file.fclose(x_id);
     
    END LOOP;
      
    COMMIT;
 END  ; 
/


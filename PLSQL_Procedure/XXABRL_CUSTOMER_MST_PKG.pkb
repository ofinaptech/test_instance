CREATE OR REPLACE PACKAGE BODY APPS.xxabrl_customer_mst_pkg
AS
   PROCEDURE xxabrl_cust_mst_proc (
      errbuff               OUT      VARCHAR2,
      retcode               OUT      NUMBER,
      p_org_id              IN       NUMBER,
      p_classification      IN       VARCHAR2,
      p_cust_num_list       IN       VARCHAR2,
      p_cust_num_from       IN       VARCHAR2,
      p_cust_num_to         IN       VARCHAR2,
      p_store_day_op_from   IN       VARCHAR2,
      p_store_day_op_to     IN       VARCHAR2,
      p_square_feet_from    IN       VARCHAR2,
      p_square_feet_to      IN       VARCHAR2,
      p_status              IN       VARCHAR2,
      p_city                IN       VARCHAR2
   )
   AS
      query_string                LONG;

      TYPE ref_cur IS REF CURSOR;

      c                           ref_cur;

      TYPE c_rec IS RECORD (
         v_customer_number     hz_cust_accounts.account_number%TYPE,
         v_customer_name       hz_parties.party_name%TYPE,
         v_legacy_reference    hz_cust_accounts.account_name%TYPE,
         v_customer_type       hz_parties.party_type%TYPE,
         v_store_area          VARCHAR2 (75)
                                            --hz_cust_acct_sites_all.TRANSLATED_CUSTOMER_NAME%TYPE
         ,
         v_address1            hz_parties.address1%TYPE,
         v_address2            hz_parties.address2%TYPE,
         v_address3            hz_parties.address3%TYPE,
         v_address4            hz_parties.address4%TYPE,
         v_city                hz_parties.city%TYPE,
         v_state               hz_parties.state%TYPE,
         v_postal_code         hz_parties.postal_code%TYPE
                                                          --,V_COUNTRY              AR_LOOKUPS.MEANING%TYPE
         ,
         v_purpose             hz_cust_site_uses_all.site_use_code%TYPE,
         v_rec_acct            VARCHAR2 (100),
         v_rev_acct            VARCHAR2 (100),
         v_tax_acct            VARCHAR2 (100),
         v_fright_acct         VARCHAR2 (100),
         v_cle_account         VARCHAR2 (100),
         v_unbilled_acct       VARCHAR2 (100),
         v_unearned_acct       VARCHAR2 (100),
         v_payment_term        VARCHAR2 (500),
         v_status              ar_lookups.meaning%TYPE,
         v_uses_location       xxabrl_customer_mv.uses_location%TYPE,
         v_service_tax_regno   xxabrl_customer_mv.service_tax_regno%TYPE,
         v_vat_no              xxabrl_customer_mv.vat_no%TYPE,
         v_cst_no              xxabrl_customer_mv.cst_no%TYPE,
         v_pan_no              xxabrl_customer_mv.pan_no%TYPE,
         ou_name               hr_operating_units.NAME%TYPE
      );

      v_rec                       c_rec;
      string_classification       VARCHAR2 (500);
      string_customer_list        LONG;
      string_customer_range       LONG;
      string_store_day_op_range   LONG;
      string_square_feet_range    LONG;
      string_status               LONG;
      string_city                 LONG;
      string_org                  LONG;
      v_store_date_from           DATE;
      v_store_date_to             DATE;
   BEGIN
      fnd_file.put_line (fnd_file.LOG, 'Start of Program ');
      query_string :=
         '  SELECT    acnt.account_number Customer_number
             ,party.party_name Customer_name
           ,ACNT.ORIG_SYSTEM_REFERENCE LEGACY_REFERENCE
           ,acnt.CUSTOMER_CLASS_CODE  CUSTOMER_TYPE
     -- ,site.TRANSLATED_CUSTOMER_NAME
     ,REPLACE(SUBSTR(site.TRANSLATED_CUSTOMER_NAME,1,INSTR(site.TRANSLATED_CUSTOMER_NAME,''#'')-1)||''/''||SUBSTR(site.TRANSLATED_CUSTOMER_NAME,INSTR(site.TRANSLATED_CUSTOMER_NAME,''#'')+1,LENGTH(site.TRANSLATED_CUSTOMER_NAME)),CHR(9),'''') TRANSLATED_CUSTOMER_NAME
              -- ,uses.site_use_code purpose
                            ,party.ADDRESS1
                  ,party.ADDRESS2
                  ,party.ADDRESS3
                  ,party.ADDRESS4
                 ,party.CITY
           ,party.STATE
                  ,party.POSTAL_CODE
                  --,DECODE(party.COUNTRY,''IN'',''India'','''') Country
               ,uses.site_use_code purpose
               ,GCC_REC.SEGMENT1||''.''||GCC_REC.SEGMENT2||''.''||GCC_REC.SEGMENT3||''.''||GCC_REC.SEGMENT4||''.''||GCC_REC.SEGMENT5||''.''||GCC_REC.SEGMENT6||''.''||GCC_REC.SEGMENT7||''.''||GCC_REC.SEGMENT8 REC_ACCOUNT
               ,GCC_REV.SEGMENT1||''.''||GCC_REV.SEGMENT2||''.''||GCC_REV.SEGMENT3||''.''||GCC_REV.SEGMENT4||''.''||GCC_REV.SEGMENT5||''.''||GCC_REV.SEGMENT6||''.''||GCC_REV.SEGMENT7||''.''||GCC_REV.SEGMENT8 REV_ACCOUNT
               ,GCC_TAX.SEGMENT1||''.''||GCC_TAX.SEGMENT2||''.''||GCC_TAX.SEGMENT3||''.''||GCC_TAX.SEGMENT4||''.''||GCC_TAX.SEGMENT5||''.''||GCC_TAX.SEGMENT6||''.''||GCC_TAX.SEGMENT7||''.''||GCC_TAX.SEGMENT8 TAX_ACCOUNT
               ,GCC_FRI.SEGMENT1||''.''||GCC_FRI.SEGMENT2||''.''||GCC_FRI.SEGMENT3||''.''||GCC_FRI.SEGMENT4||''.''||GCC_FRI.SEGMENT5||''.''||GCC_FRI.SEGMENT6||''.''||GCC_FRI.SEGMENT7||''.''||GCC_FRI.SEGMENT8 FRI_ACCOUNT
               ,GCC_CLE.SEGMENT1||''.''||GCC_CLE.SEGMENT2||''.''||GCC_CLE.SEGMENT3||''.''||GCC_CLE.SEGMENT4||''.''||GCC_CLE.SEGMENT5||''.''||GCC_CLE.SEGMENT6||''.''||GCC_CLE.SEGMENT7||''.''||GCC_CLE.SEGMENT8 CLE_ACCOUNT
               ,GCC_UNB.SEGMENT1||''.''||GCC_UNB.SEGMENT2||''.''||GCC_UNB.SEGMENT3||''.''||GCC_UNB.SEGMENT4||''.''||GCC_UNB.SEGMENT5||''.''||GCC_UNB.SEGMENT6||''.''||GCC_UNB.SEGMENT7||''.''||GCC_UNB.SEGMENT8 UNB_ACCOUNT
               ,GCC_UNE.SEGMENT1||''.''||GCC_UNE.SEGMENT2||''.''||GCC_UNE.SEGMENT3||''.''||GCC_UNE.SEGMENT4||''.''||GCC_UNE.SEGMENT5||''.''||GCC_UNE.SEGMENT6||''.''||GCC_UNE.SEGMENT7||''.''||GCC_UNE.SEGMENT8 UNE_ACCOUNT
         ,rtv.NAME PAYMENT_TERM
               ,LOOK_STATUS.meaning
               ,mv.USES_LOCATION ,
                mv.SERVICE_TAX_REGNO,
                mv.VAT_NO,mv.CST_NO,mv.PAN_NO,hr.name ou_name
          FROM hz_parties party,
               hz_cust_accounts acnt,
               hz_cust_acct_sites_all site,
               hz_cust_site_uses_ALL uses,
                     RA_TERMS_VL  rtv,
               hz_party_sites ps,
               hz_locations loc,
               HZ_SITE_USES_V HSUV,
               GL_CODE_COMBINATIONS GCC_TAX,
               GL_CODE_COMBINATIONS GCC_REC,
               GL_CODE_COMBINATIONS GCC_REV,
               GL_CODE_COMBINATIONS GCC_FRI,
               GL_CODE_COMBINATIONS GCC_CLE,
               GL_CODE_COMBINATIONS GCC_UNB,
               GL_CODE_COMBINATIONS GCC_UNE,
               AR_LOOKUPS LOOK_STATUS,
               org_organization_definitions OOD,
                XXABRL_CUSTOMER_MV mv,
               hr_operating_units hr
         WHERE party.party_id = acnt.party_id
           AND acnt.cust_account_id = site.cust_account_id
           AND site.cust_acct_site_id = uses.cust_acct_site_id
           AND ps.party_site_id = site.party_site_id
           AND loc.location_id = ps.location_id
           AND uses.PAYMENT_TERM_ID=rtv.TERM_ID(+)
           AND HSUV.site_use_id(+)=uses.site_use_id
           AND GCC_TAX.CODE_COMBINATION_ID(+)=HSUV.GL_ID_TAX
           AND GCC_REC.CODE_COMBINATION_ID(+)=HSUV.GL_ID_REC
           AND GCC_REV.CODE_COMBINATION_ID(+)=HSUV.GL_ID_REV
           AND GCC_FRI.CODE_COMBINATION_ID(+)=HSUV.GL_ID_FREIGHT
           AND GCC_CLE.CODE_COMBINATION_ID(+)=HSUV.GL_ID_CLEARING
           AND GCC_UNB.CODE_COMBINATION_ID(+)=HSUV.GL_ID_UNBILLED
           AND GCC_UNE.CODE_COMBINATION_ID(+)=HSUV.GL_ID_UNEARNED
           AND look_status.lookup_type(+) = ''CUSTOMER_STATUS''
       AND     look_status.lookup_code(+) = acnt.status
       AND SITE.ORG_ID=OOD.ORGANIZATION_ID 
        and mv.CUST_ACCOUNT_ID=acnt.CUST_ACCOUNT_ID
       and mv.CUST_ACCT_SITE_ID=site.CUST_ACCT_SITE_ID
       and mv.USE_SITE_ID=uses.SITE_USE_ID
       and hr.organization_id=mv.org_id ';

      --  ,SUBSTR(site.TRANSLATED_CUSTOMER_NAME,1,INSTR(site.TRANSLATED_CUSTOMER_NAME,''#'')-1)  STORE

      --  AND    look_status.lookup_code(+) = NVL(acnt.status, ''A'')
      --AND PARTY.PARTY_NAME LIKE (''B%'')
      IF p_org_id IS NOT NULL
      THEN
         string_org := ' AND site.org_id = ' || p_org_id;
      ELSE
         string_org := ' AND site.org_id =  site.org_id ';
      END IF;

      IF p_classification IS NOT NULL
      THEN
         string_classification :=
            ' AND  acnt.CUSTOMER_CLASS_CODE = ''' || p_classification || '''';
      END IF;

      IF     p_cust_num_list IS NOT NULL
         AND p_cust_num_from IS NULL
         AND p_cust_num_to IS NULL
      THEN
         SELECT    'AND acnt.account_number IN ('
                || ''''
                || REPLACE (p_cust_num_list, ',', ''',''')
                || ''')'
           INTO string_customer_list
           FROM DUAL;
      END IF;

      IF     p_cust_num_list IS NULL
         AND p_cust_num_from IS NOT NULL
         AND p_cust_num_to IS NOT NULL
      THEN
         string_customer_range :=
               ' AND acnt.account_number between '''
            || p_cust_num_from
            || ''' AND '''
            || p_cust_num_to
            || '''';
      END IF;

      IF p_store_day_op_from IS NOT NULL AND p_store_day_op_to IS NOT NULL
      THEN
         SELECT TO_DATE (p_store_day_op_from, 'yyyy/mm/dd:HH24:MI:SS')
           INTO v_store_date_from
           FROM DUAL;

         SELECT TO_DATE (p_store_day_op_to, 'yyyy/mm/dd:HH24:MI:SS')
           INTO v_store_date_to
           FROM DUAL;

         string_store_day_op_range :=
               'and TO_DATE(SUBSTR(site.TRANSLATED_CUSTOMER_NAME,INSTR(site.TRANSLATED_CUSTOMER_NAME,''#'')+1,LENGTH(site.TRANSLATED_CUSTOMER_NAME)),''DD-MM-RRRR'') BETWEEN '''
            || v_store_date_from
            || ''' AND '''
            || v_store_date_to
            || '''';
      END IF;

      IF p_square_feet_from IS NOT NULL AND p_square_feet_to IS NOT NULL
      THEN
         string_square_feet_range :=
               'and replace((SUBSTR(site.TRANSLATED_CUSTOMER_NAME,1,INSTR(site.TRANSLATED_CUSTOMER_NAME,''#'')-1)),'','','''') BETWEEN '''
            || p_square_feet_from
            || ''' and '''
            || p_square_feet_to
            || '''';
      END IF;

      IF p_city IS NOT NULL
      THEN
         string_city :=
                       ' AND upper(party.CITY) = upper(''' || p_city || ''')';
      END IF;

      IF p_status IS NOT NULL
      THEN
         string_status :=
                     '  AND look_status.lookup_code = ''' || p_status || '''';
      END IF;

      query_string :=
            query_string
         || ' '
         || string_classification
         || string_customer_list
         || string_customer_range
         || string_store_day_op_range
         || string_square_feet_range
         || string_city
         || string_status
         || string_org;
      fnd_file.put_line (fnd_file.LOG, query_string);
      DBMS_OUTPUT.put_line (query_string);
      fnd_file.put_line (fnd_file.output, 'ABRL Customer Master Report');
      fnd_file.put_line (fnd_file.output, ' ');
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'As on Date'
                         || CHR (9)
                         || SYSDATE
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output, ' ');
      fnd_file.put_line (fnd_file.output, 'REPORT PARAMETRS');
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Classification'
                         || CHR (9)
                         || p_classification
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Customer Number List'
                         || CHR (9)
                         || p_cust_num_list
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Customer Number From'
                         || CHR (9)
                         || p_cust_num_from
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Customer Number To'
                         || CHR (9)
                         || p_cust_num_to
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Store Day Of Opening From'
                         || CHR (9)
                         || p_store_day_op_from
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Store Day Of Opening To'
                         || CHR (9)
                         || p_store_day_op_to
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Square Feet From'
                         || CHR (9)
                         || p_square_feet_from
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Square Feet From'
                         || CHR (9)
                         || p_square_feet_to
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Customer Status'
                         || CHR (9)
                         || p_status
                         || CHR (9)
                        );
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'City'
                         || CHR (9)
                         || p_city
                         || CHR (9)
                        );
      -- display labels of the report
      fnd_file.put_line (fnd_file.output, ' ');
      fnd_file.put_line (fnd_file.output, ' ');
      fnd_file.put_line (fnd_file.output,
                            'Customer Number'
                         || '^'
                         || 'Customer Name'
                         || '^'
                         || 'Legacy Reference'
                         || '^'
                         || 'Customer Type'
                         || '^'
                         || 'Address1'
                         || '^'
                         || 'Address2'
                         || '^'
                         || 'Address3'
                         || '^'
                         || 'Address4'
                         || '^'
                         || 'City'
                         || '^'
                         || 'State'
                         || '^'
                         || 'Postal Code'
                         || '^'
                         || 'Purpose'
                         || '^'
                         || 'Receivable Account'              --pay group code
                         || '^'
                         || 'Revenue Account'
                         || '^'
                         || 'Tax Account'
                         || '^'
                         || 'Freight Account'
                         || '^'
                         || 'Clearing Account'
                         || '^'
                         || 'Unbilled Account'
                         || '^'
                         || 'Unearned Account'
                         || '^'
                         || 'Payment Terms'
                         || '^'
                         || 'LOCATION'
                         || '^'
                         || 'SERVICE_TAX_REGNO'
                         || '^'
                         || 'VAT_NO'
                         || '^'
                         || 'CST_NO'
                         || '^'
                         || 'PAN_NO'
                         || '^'
                         || 'OU_NAME'
                         || '^'
                         || 'Status'
                         || '^'
                         || 'Store Area/Store DOO'
                        );

      OPEN c FOR query_string;

      LOOP
         FETCH c
          INTO v_rec;

         EXIT WHEN c%NOTFOUND;
         fnd_file.put_line (fnd_file.output,
                               v_rec.v_customer_number
                            || '^'
                            || v_rec.v_customer_name
                            || '^'
                            || TRIM (v_rec.v_legacy_reference)
                            || '^'
                            || TRIM (v_rec.v_customer_type)
                            || '^'
                            || TRIM (v_rec.v_address1)
                            || '^'
                            || TRIM (v_rec.v_address2)
                            || '^'
                            || TRIM (v_rec.v_address3)
                            || '^'
                            || TRIM (v_rec.v_address4)
                            || '^'
                            || v_rec.v_city
                            || '^'
                            || v_rec.v_state
                            || '^'
                            || v_rec.v_postal_code
                            || '^'
                            || v_rec.v_purpose
                            || '^'
                            || v_rec.v_rec_acct
                            || '^'
                            || v_rec.v_rev_acct
                            || '^'
                            || v_rec.v_tax_acct
                            || '^'
                            || v_rec.v_fright_acct
                            || '^'
                            || v_rec.v_cle_account
                            || '^'
                            || v_rec.v_unbilled_acct
                            || '^'
                            || v_rec.v_unearned_acct
                            || '^'
                            || v_rec.v_payment_term
                            || '^'
                            || v_rec.v_uses_location
                            || '^'
                            || v_rec.v_service_tax_regno
                            || '^'
                            || v_rec.v_vat_no
                            || '^'
                            || v_rec.v_cst_no
                            || '^'
                            || v_rec.v_pan_no
                            || '^'
                            || v_rec.ou_name
                            || '^'
                            || v_rec.v_status
                            || '^'
                            || TRIM (REPLACE ((v_rec.v_store_area),
                                              CHR (9),
                                              ''
                                             )
                                    )
                           );
      END LOOP;

      CLOSE c;
   END xxabrl_cust_mst_proc;
END xxabrl_customer_mst_pkg; 
/


CREATE OR REPLACE PACKAGE APPS.xxabrl_vendor_payadvice_pkg
IS
   PROCEDURE xxabrl_vendor_main_pkg (errbuf OUT VARCHAR2, retcode OUT NUMBER);

   PROCEDURE xxabrl_vendor_payadvice_prc;
END; 
/


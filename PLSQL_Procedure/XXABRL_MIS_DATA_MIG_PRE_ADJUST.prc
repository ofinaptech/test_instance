CREATE OR REPLACE PROCEDURE APPS.XXABRL_MIS_DATA_MIG_PRE_ADJUST (errbuf out varchar2,
                                 retcode out varchar2
                                 )
AS
--P_RUN_DATE     VARCHAR2 (11):= TO_CHAR (TO_DATE (SUBSTR (p_from_date, 1, 10), 'RRRR/MM/DD'),'DD-MON-YYYY');
--P_TO_DATE     VARCHAR2 (11):= TO_CHAR (TO_DATE (SUBSTR (p_from_date, 1, 10), 'RRRR/MM/DD'),'DD-MON-YYYY');
V_L_FILE         VARCHAR2(200);
V_FINANCE_YEAR   NUMBER;
L_RUN_DATE    VARCHAR2(250);
CURSOR CUR_PERIOD_NAME1 IS
SELECT   '3', povs.vendor_site_code, 
                 povs.VENDOR_SITE_ID,--added on '27-Oct-2015'
               'PREPAYMENT-ADJUSTMENT' INVOICE_TYPE,
            replace (api.invoice_num,'^','//') INVOICE_NUM,    
--            api.invoice_num,
             api.invoice_date, --api.description description,
              (SELECT  
               TRANSLATE ( api.description, 'X' || CHR (9) || CHR (10) || CHR (13), 'X' )
                from dual) description,             
            apd.dist_code_combination_id ccid,gl.SEGMENT3 SBU ,(select FFVL.description FROM apps.fnd_flex_values_vl ffvl
        WHERE ffvl.FLEX_VALUE_SET_ID=1013466
        AND ffvl.FLEX_VALUE=gl.SEGMENT3
         )SBU_desc,
         gl.segment4 LOCATION_CODE
         ,(SELECT FFVL.description FROM apps.fnd_flex_values_vl ffvl
        WHERE ffvl.FLEX_VALUE_SET_ID=1013467
        AND ffvl.FLEX_VALUE=gl.SEGMENT4) Location_desc,
        gl.segment6 ACCOUNT_CODE,
          (SELECT FFVL.description FROM apps.fnd_flex_values_vl ffvl
        WHERE ffvl.FLEX_VALUE_SET_ID=1013469
        AND ffvl.FLEX_VALUE=gl.SEGMENT6) account_desc,
             api.invoice_currency_code,
            decode(APD.REVERSAL_FLAG ,'N' ,ZZ.AMT,ZZ.AMT*-1) dr_val,
            decode(APD.REVERSAL_FLAG ,'N' ,ZZ.AMT,ZZ.AMT*-1) cr_val,null prepay_amt_remaing ,
            TO_CHAR (api.doc_sequence_value)payment_num,
            /*null pay_accounting_date,*/
            NULL check_number, null check_date , pov.segment1 VENDOR_NUM,
        (case when (substr(pov.segment1,1,1)='7') then (select apsa1.attribute14 from apps.ap_suppliers aps1,
        apps.ap_supplier_sites_all apsa1
        where
        aps1.vendor_id=apsa1.vendor_id
        and aps1.segment1=pov.SEGMENT1
        and apsa1.vendor_site_id=povs.vendor_site_id) else NULL end)"ATTRIBUTE_14",     
             pov.vendor_name,
            pov.vendor_type_lookup_code VENDOR_TYPE, 
            apd.po_distribution_id,
            apil.PO_HEADER_ID,---added on '01-Oct-2015'
            apil.RCV_TRANSACTION_ID,---added on '01-Oct-2015'
            api.ORG_ID,--added on '27-Oct-2015'
            hr.name OPERATING_UNIT , 
            aBA.batch_NAME,
             api.invoice_id,
            (SELECT MAX (accounting_date)
               FROM APPS.xxabrl_ap_view_prepays_v apil2
              WHERE apil2.invoice_id = api.invoice_id) PAY_accounting_date,
            NULL payment_type_flag, NULL bank_account_name,
            NULL bank_account_num,NULL Bank_UTI_no, api.invoice_amount, api.amount_paid,
            trunc(APD.ACCOUNTING_DATE) gl_date,APIL.PREPAY_INVOICE_ID,(select NAME from ap_terms AT,apps.ap_invoices_all ai
            where AT.term_id=AI.TERMS_ID
            and ai.invoice_id=api.invoice_id) TERM_NAME,Api.PAY_GROUP_LOOKUP_CODE PAY_GROUP,
            APIL.REFERENCE_1  PPI_Invoice_Number,
         api.ATTRIBUTE2 GRN_Number,
              Api.ATTRIBUTE3 GRN_Date
       FROM apps.ap_invoices_all api,
       APPS.AP_BATCHES_ALL ABA ,
            apps.ap_invoice_lines_all apil,
            apps.ap_invoice_distributions_all apd,
            apps.ap_suppliers pov,
            apps.gl_code_combinations gl,
           -- apps.ap_payment_schedules_all aps,
            apps.hr_operating_units hr,
            apps.ap_supplier_sites_all povs,
         ( SELECT   NVL (SUM (apd.amount), 0)*-1 amt, api.invoice_id,apd.accounting_date,APIL.LINE_NUMBER,apd.DISTRIBUTION_LINE_NUMBER
                 FROM apps.ap_invoices_all api,
                      apps.ap_invoice_lines_all apil,
                      apps.ap_invoice_distributions_all apd,
                      apps.ap_suppliers pov,
                      apps.ap_supplier_sites_all povs
                WHERE api.invoice_id = apd.invoice_id
                  AND apil.invoice_id = api.invoice_id
                  AND apil.line_number = apd.invoice_line_number
                  AND api.vendor_id = pov.vendor_id
                  and api.invoice_type_lookup_code <> 'PREPAYMENT'
                  AND api.vendor_site_id = povs.vendor_site_id
                --  AND apd.match_status_flag = 'A'
                  --and api.invoice_id=90526782
                 --- and  apd.REVERSAL_FLAG='N'
                  AND apil.line_type_lookup_code = 'PREPAY'
             GROUP BY api.invoice_id,apd.accounting_date,APIL.LINE_NUMBER,apd.DISTRIBUTION_LINE_NUMBER) ZZ
      WHERE api.invoice_id = zZ.invoice_id
      AND ZZ.LINE_NUMBER=APIL.LINE_NUMBER
      and zz.DISTRIBUTION_LINE_NUMBER=apd.DISTRIBUTION_LINE_NUMBER
        AND api.invoice_id = apd.invoice_id
        AND ABA.BATCH_ID=API.BATCH_ID
        and api.org_id=hr.ORGANIZATION_ID
        and gl.CODE_COMBINATION_ID=api.ACCTS_PAY_CODE_COMBINATION_ID
        AND apil.invoice_id = api.invoice_id
        -- and aps.INVOICE_ID=api.INVOICE_ID
        AND apil.line_number = apd.invoice_line_number
        AND api.vendor_id = pov.vendor_id
      --  AND api.invoice_type_lookup_code <> 'PREPAYMENT'
      --  AND apd.match_status_flag = 'A'
       -- and nvl(aps.AMOUNT_REMAINING,0)<>0
         -- and pov.segment1='9700381'
            --and api.invoice_id in ('104974824',101629786,101629804,104659800,104659802,66703510,101371820)
        -- and api.org_ID =801--,801,961,861)
        --  and api.invoice_id='2761899'
          AND APD.REVERSAL_FLAG='N'
--and api.invoice_num in ('100390137','HN-3221257','DN SM 06219/Hyd/Chq/Off/07/12-13')
      AND  APD.ACCOUNTING_DATE>= TRUNC(SYSDATE) - 180
       AND (trunc(api.LAST_UPDATE_DATE) >= trunc(sysdate)-6 OR  
                 trunc(apil.LAST_UPDATE_DATE) >= trunc(sysdate)-6  OR  
                 trunc(apd.LAST_UPDATE_DATE) >= trunc(sysdate)-6 ) 
      AND APIL.PREPAY_INVOICE_ID IS NOT NULL
        AND api.vendor_site_id = povs.vendor_site_id;
        
x_id utl_file.file_type;
BEGIN
    SELECT sysdate INTO L_RUN_DATE FROM DUAL;
    SELECT 'ABRL_OFBIS_creditors'||'_'||'PREPAY-ADJUST'||'-'||'V'||'_'||L_RUN_DATE||'.txt'INTO V_L_FILE FROM dual;-----Updated By Santosh 18-Apr02014
  --SELECT 'XXABRL_HYPN_MKTNG_'||p_period_name||'_DATA'||NVL(TO_CHAR(TO_DATE(sysdate,'DD-MON-YYYY'),'DDMMYY'),TO_CHAR(SYSDATE-3,'DDMMYY'))||'.csv' INTO V_L_FILE FROM dual;
  x_id:=utl_file.fopen('VENDOR_INVOICE_PORTAL',V_L_FILE,'W');
utl_file.put_line(x_id,  'VENDOR SITE CODE'||'^'||
                              'VENDOR SITE ID'||'^'||--added on '27-Oct-2015'
                              'INVOICE_TYPE'||'^'||
                              'INVOICE NUMBER'||'^'||
                              'INVOICE DATE'||'^'||
                              'DESCRIPTION'||'^'||
                              'CCID'||'^'||
                              'SBU'||'^'||
                              'SBU DESC'||'^'||
                              'LOCATION CODE'||'^'||
                              'LOCATION DESC'||'^'||
                              'ACCOUNT CODE'||'^'||
                             -- 'PIAD AMOUNT'||'|'||
                              'ACCOUNT DESC'||'^'||
                              'INVOICE CURRENCY CODE'||'^'||
                              'DR VAL'||'^'||
                              'CR VAL'||'^'||
                              'PREPAY AMT REMAING'||'^'||
                              'DOC NUM'||'^'||
                              'CHECK NUMBER'||'^'||
                              'CHECK_DATE'||'^'||
                              'VENDOR NUMBER'||'^'||
                              'ATTRIBUTE_14'||'^'||   
                              'VENDOR NAME'||'^'||
                              'VENDOR TYPE'||'^'||
                              'PO DISTRIBUTION ID'||'^'||
                               'PO HEADER ID'||'^'||--added on '01-Oct-2015'
                              'RCV TRANSACTION ID'||'^'||--added on '01-Oct-2015'
                              'ORG ID'||'^'||--added on '27-Oct-2015'
                              'OPERATING UNIT'||'^'||
                              'BATCH NAME'||'^'||
                              'INVOICE ID'||'^'||
                              'PAY ACCOUNTING DATE'||'^'||
                              'PAYMENT TYPE FLAG'||'^'||
                              'BANK ACCOUNT NAME'||'^'||
                              'BANK ACCOUNT NUM'||'^'||
                              'BANK UTI NO.'||'^'||
                              'INVOICE AMOUNT'||'^'||
                              'AMOUNT PAID'||'^'||
                              'GL DATE'||'^'||
                              'PREPAY_INVOICE_ID'||'^'||
                            'TERM NAME'||'^'||
                              'PAY GROUP'||'^'||
                              'PPI INVOICE NUM'||'^'||
                              'GRN NUMBER'||'^'||
                              'GRN DATE'||'^'||
                              'HOLD FLAG'
                              );
  --utl_file.put_line(x_id,'VENDOR PAID INVOICES LIST DETAILS................................................->');
--utl_file.put_line(x_id,'VENDOR STANDARD INVOICES LIST........................................................->');
 FOR R IN CUR_PERIOD_NAME1 LOOP
 utl_file.put_line(x_id,   R.vendor_site_code||'^'||
                              R.VENDOR_SITE_ID||'^'||--added on '27-Oct-2015'
                              R.INVOICE_TYPE||'^'||
                              R.INVOICE_NUM||'^'||
                              R.INVOICE_DATE||'^'||
                              R.DESCRIPTION||'^'||
                              R.CCID||'^'||
                              R.SBU||'^'||
                              R.SBU_DESC||'^'||
                              R.LOCATION_CODE||'^'||
                              R.LOCATION_DESC||'^'||
                              R.ACCOUNT_CODE||'^'||
                             -- 'PIAD AMOUNT'||'|'||
                              R.ACCOUNT_DESC||'^'||
                              R.INVOICE_CURRENCY_CODE||'^'||
                              R.DR_VAL||'^'||
                              R.CR_VAL||'^'||
                              R.PREPAY_AMT_REMAING||'^'||
                              R.PAYMENT_NUM||'^'||
                              R.CHECK_NUMBER||'^'||
                              R.CHECK_DATE||'^'||
                              R.VENDOR_NUM||'^'||
                              R.ATTRIBUTE_14||'^'||    
                              R.VENDOR_NAME||'^'||
                              R.VENDOR_TYPE||'^'||
                              R.PO_DISTRIBUTION_ID||'^'||
                              R.PO_HEADER_ID||'^'||--added on '01-Oct-2015'
                              R.RCV_TRANSACTION_ID||'^'||--added on '01-Oct-2015'
                              R.ORG_ID||'^'||--added on '27-Oct-2015'
                              R.OPERATING_UNIT||'^'||
                              R.BATCH_NAME||'^'||
                              R.INVOICE_ID||'^'||
                              R.PAY_ACCOUNTING_DATE||'^'||
                              R.PAYMENT_TYPE_FLAG||'^'||
                              R.BANK_ACCOUNT_NAME||'^'||
                              R.BANK_ACCOUNT_NUM||'^'||
                              R.Bank_UTI_no||'^'||
                              R.INVOICE_AMOUNT||'^'||
                              R.AMOUNT_PAID||'^'||
                              R.GL_DATE||'^'||
                              R.PREPAY_INVOICE_ID||'^'||
                           R.TERM_NAME||'^'||
                              R.PAY_GROUP||'^'||
                              R.PPI_Invoice_Number||'^'||
                              R.GRN_number||'^'||
                              R.GRN_date||'^'||
                              NULL || CHR(13) || CHR(10)
                              );
  end loop;
  --utl_file.put_line(x_id,'cur2.................................->');
  EXCEPTION
 WHEN TOO_MANY_ROWS THEN
    dbms_output.PUT_LINE('Too Many Rows');
 WHEN NO_DATA_FOUND THEN
    dbms_output.PUT_LINE('No Data Found');
 WHEN utl_file.invalid_path THEN
    RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_path');
 WHEN utl_file.invalid_mode THEN
     RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_mode');
 WHEN utl_file.invalid_filehandle THEN
     RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_filehandle');
 WHEN utl_file.invalid_operation THEN
     RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_operation');
 WHEN utl_file.read_error THEN
     RAISE_APPLICATION_ERROR(-20001, 'utl_file.read_error');
 WHEN utl_file.write_error THEN
     RAISE_APPLICATION_ERROR(-20001, 'utl_file.write_error');
 WHEN utl_file.internal_error THEN
     RAISE_APPLICATION_ERROR(-20001, 'utl_file.internal_error');
 WHEN OTHERS THEN
      dbms_output.put_line(SubStr('Error '||TO_CHAR(SQLCODE)||': '||SQLERRM, 1,255));
utl_file.fclose(x_id);
END; 
/


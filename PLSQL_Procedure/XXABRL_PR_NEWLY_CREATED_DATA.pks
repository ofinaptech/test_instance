CREATE OR REPLACE PACKAGE APPS.xxabrl_pr_newly_created_data
IS
   PROCEDURE xxabrl_main_pkg (
      errbuf    OUT      VARCHAR2,
      retcode   OUT      NUMBER,
      p_type    IN       VARCHAR2
   );

   PROCEDURE xxabrl_prs_daily_data;

   PROCEDURE xxabrl_prs_lines_dist_det;

   PROCEDURE xxabrl_prs_action_history_det;
END xxabrl_pr_newly_created_data; 
/


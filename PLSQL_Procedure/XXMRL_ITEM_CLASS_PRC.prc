CREATE OR REPLACE PROCEDURE APPS.xxmrl_item_class_prc (
   errbuf    OUT   VARCHAR2,
   retcode   OUT   VARCHAR2
)
IS
      /*
      ========================
   =========================
   =========================
   =========================
   ====
      || Concurrent Program Name : XXMRL Item Classifications Updated Program
      || Procedure Name                  : APPS.xxmrl_item_class_prc
      || Description                            : Item Classifications Updation
      =============================================================================================================================
      || Version          Date            Author                 Description                       Remarks
      || ~~~~~~        ~~~~~~~~~~~      ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~~~~            ~~~~~~~~~~~~~~~~~~
      || 1.0.0          17-Dec-2019      Lokesh Poojari     Item Classifications Updation       New Development
      =============================================================================================================================
   =========================
   =========================
   =========================
   ====*/
   v_hdr_rec                    apps.jai_item_templ_hdr%ROWTYPE;
   v_template_hdr_id            NUMBER;
   v_dtl_rec                    apps.jai_item_templ_dtls%ROWTYPE;
   v_assoc_rec                  apps.jai_reporting_associations%ROWTYPE;
   v_segment1                   apps.mtl_system_items_b.segment1%TYPE;
   v_reporting_code             apps.jai_reporting_codes.reporting_code%TYPE;
   v_regime_code                apps.jai_regimes.regime_code%TYPE;
   v_reporting_type_code        apps.jai_reporting_types.reporting_type_code%TYPE;
   v_inventory_item_id          jai_item_templ_hdr.inventory_item_id%TYPE;
   v_reporting_association_id   jai_reporting_associations.reporting_association_id%TYPE;
   process_flag                 VARCHAR2 (20);
   error_msg                    VARCHAR2 (4000);
   v_tot_rec                    NUMBER;
   v_err_rec                    NUMBER;
   v_valid_rec                  NUMBER;

   CURSOR c1
   IS
    SELECT stg.ROWID, stg.*
        FROM xxabrl.xxabrl_gst_item_assignment stg
       WHERE NVL (stg.flag, 'N') IN ('N', 'E');

                                         --AND organization_id = 'ABRL CORP HM OU'
   --     AND item_number = '400000083';
   CURSOR c2
   IS
      SELECT DISTINCT stg.migration_type,
                      (SELECT lookup_code
                         FROM apps.fnd_lookup_values
                        WHERE lookup_type =
                                      'JAI_ITEM_CLASS_CD'
                          AND meaning = stg.item_classification)
                                                         item_classification,
                      item_number, attribute_code, attribute_value,
                      msib.inventory_item_id, hou.organization_id,
                      stg.regime_name, stg.reporting_code,
                      stg.reporting_type_name, jr.regime_id,
                      jrc.reporting_code_description, jrt.reporting_type_id,
                      jrc.reporting_code_id
                 FROM xxabrl.xxabrl_gst_item_assignment stg,
                      apps.hr_operating_units hou,
                      apps.jai_reporting_types jrt,
                      apps.mtl_system_items_b msib,
                      apps.jai_reporting_codes jrc,
                      apps.jai_regimes jr
                WHERE 1 = 1
                  AND stg.organization_id = hou.NAME
                  AND stg.reporting_type_name = jrt.reporting_type_name
                  AND NOT EXISTS (
                         SELECT 1
                           FROM apps.jai_item_templ_hdr
                          WHERE 1 = 1
                            AND inventory_item_id = msib.inventory_item_id
                            AND organization_id = hou.organization_id)
                  AND stg.item_number = msib.segment1
                  AND msib.organization_id = hou.organization_id
                  AND stg.regime_name = jr.regime_code
                  AND stg.reporting_code = jrc.reporting_code
                  AND jrc.reporting_code <> jrc.reporting_code_description
                  and jrc.EFFECTIVE_TO is null
                  AND stg.flag = 'V';

BEGIN
   fnd_file.put_line (fnd_file.LOG,
                      'Validations of Item Classification Starts'
                     );
   fnd_file.put_line (fnd_file.LOG,
                      '-----------------------------------------------'
                     );
   process_flag := NULL;
   v_tot_rec := 0;
   v_err_rec := 0;
   v_valid_rec := 0;

   FOR i IN c1
   LOOP
      process_flag := NULL;
      error_msg := NULL;
      fnd_file.put_line (fnd_file.LOG,
                         'Operating Unit :' || '-' || i.organization_id
                        );
      fnd_file.put_line (fnd_file.LOG,
                            'Item :'
                         || '-'
                         || i.item_number
                         || '-'
                         || i.organization_id
                        );

      --Item Validation
      BEGIN
         SELECT segment1
           INTO v_segment1
           FROM apps.mtl_system_items_b msib,
                apps.hr_all_organization_units hou
          WHERE msib.segment1 = i.item_number
            AND msib.organization_id = hou.organization_id
            AND hou.NAME = i.organization_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            process_flag := 'N';
            error_msg :=
                  'Item is not created for the organization'
               || '-'
               || i.item_number
               || '-'
               || i.organization_id;
            fnd_file.put_line (fnd_file.LOG,
                                  'Item is not created for the organization'
                               || '-'
                               || i.item_number
                               || '-'
                               || i.organization_id
                              );
      END;

      fnd_file.put_line (fnd_file.LOG,
                         'Regime Code :' || '-' || i.regime_name);

      --Validation of Regiem Code
      BEGIN
         SELECT regime_code
           INTO v_regime_code
           FROM apps.jai_regimes jr
          WHERE 1 = 1 AND jr.regime_code = i.regime_name;
      EXCEPTION
         WHEN OTHERS
         THEN
            process_flag := 'N';
            error_msg :=
                  'Regime Code is not created for the organization'
               || '-'
               || i.regime_name;
            fnd_file.put_line
                         (fnd_file.LOG,
                             'Regime Code is not created for the organization'
                          || '-'
                          || i.regime_name
                         );
      END;

      --Validation of Reporting Codes
      fnd_file.put_line (fnd_file.LOG,
                         'Reporting Code :' || '-' || i.reporting_code
                        );

      BEGIN
         SELECT reporting_code
           INTO v_reporting_code
           FROM apps.jai_reporting_codes
          WHERE 1 = 1
            AND reporting_code = i.reporting_code
            AND ROWNUM <= 1
            AND reporting_code <> reporting_code_description;
      EXCEPTION
         WHEN OTHERS
         THEN
            process_flag := 'N';
            error_msg :=
                  'Reporting_code is not created for the organization'
               || '-'
               || i.reporting_code;
            fnd_file.put_line
                      (fnd_file.LOG,
                          'Reporting_code is not created for the organization'
                       || '-'
                       || i.reporting_code
                      );
      END;

      fnd_file.put_line (fnd_file.LOG,
                         'Reporting Type name :' || '-'
                         || i.reporting_type_name
                        );

      --validation of Reporting Type
      BEGIN
         SELECT reporting_type_code
           INTO v_reporting_type_code
           FROM apps.jai_reporting_types
          WHERE reporting_type_name = i.reporting_type_name;
      EXCEPTION
         WHEN OTHERS
         THEN
            process_flag := 'N';
            error_msg :=
                  'Reporting_type_name is not created for the organization'
               || '-'
               || i.reporting_type_name;
            fnd_file.put_line
                 (fnd_file.LOG,
                     'Reporting_type_name is not created for the organization'
                  || '-'
                  || i.reporting_type_name
                 );
      END;

      --Validation of Duplicate Item assigned
      BEGIN
         SELECT COUNT (inventory_item_id)
           INTO v_inventory_item_id
           FROM apps.jai_item_templ_hdr jai,
                apps.hr_all_organization_units hou
          WHERE jai.inventory_item_id IN (
                       SELECT DISTINCT inventory_item_id
                                  FROM apps.mtl_system_items_b
                                 WHERE segment1 = i.item_number
                                   AND organization_id = 97)
            AND jai.organization_id = hou.organization_id
            AND hou.NAME = i.organization_id;

         IF v_inventory_item_id > 0
         THEN
            error_msg :=
                  'This items is already assigned to OU'
               || '-'
               || i.organization_id
               || '-'
               || i.item_number;
         END IF;
      EXCEPTION
         WHEN OTHERS
         THEN
            NULL;
      END;

      fnd_file.put_line (fnd_file.LOG,
                         '-----------------------------------------------'
                        );

      IF process_flag IS NOT NULL
      THEN
         UPDATE xxabrl.xxabrl_gst_item_assignment
            SET flag = 'E',
                error = error_msg
          WHERE ROWID = i.ROWID;

         v_err_rec := v_err_rec + 1;
      ELSE
         UPDATE xxabrl.xxabrl_gst_item_assignment
            SET flag = 'V',
                error = error_msg
          WHERE ROWID = i.ROWID;

         v_valid_rec := v_valid_rec + 1;
      END IF;

      v_tot_rec := v_tot_rec + 1;
      COMMIT;
   END LOOP;

   fnd_file.put_line (fnd_file.LOG, 'Total Records :' || v_tot_rec);
   fnd_file.put_line (fnd_file.LOG, 'Total Valid Records :' || v_valid_rec);
   fnd_file.put_line (fnd_file.LOG, 'Total Error Records :' || v_err_rec);
   fnd_file.put_line (fnd_file.LOG, '------------------------------------');
 --  fnd_file.put_line (fnd_file.LOG, 'Inserting into Jai_item_templ_hdr');
   process_flag := NULL;
   error_msg := NULL;

   FOR j IN c2
   LOOP
      process_flag := NULL;
      error_msg := NULL;

      BEGIN
       --  fnd_file.put_line (fnd_file.LOG, '--enter the loop');
         v_hdr_rec.template_hdr_id := jai_item_templ_hdr_s.NEXTVAL;
         v_hdr_rec.template_name := NULL;
         v_hdr_rec.template_desc := NULL;
         v_hdr_rec.entity_id := j.inventory_item_id;
         v_hdr_rec.entity_type_code := 'ITEM';
         v_hdr_rec.item_classification := j.item_classification;
         v_hdr_rec.inventory_item_id := j.inventory_item_id;
         v_hdr_rec.organization_id := j.organization_id;
         v_hdr_rec.creation_date := SYSDATE;
         v_hdr_rec.created_by := 4191;
         v_hdr_rec.last_update_date := SYSDATE;
         v_hdr_rec.last_update_login := USERENV ('SESSIONID');
         v_hdr_rec.last_updated_by := 4191;
         v_hdr_rec.record_type_code := 'DEFINED';

                fnd_file.put_line (fnd_file.LOG, '----------------Hdr Start--------------------');
               fnd_file.put_line (fnd_file.LOG, 'inventory_item_id :' || j.inventory_item_id);
               fnd_file.put_line (fnd_file.LOG, 'item_classification :' || j.item_classification);
               fnd_file.put_line (fnd_file.LOG, ' organization_id :' ||  j.organization_id);
               fnd_file.put_line (fnd_file.LOG, '----------------Hdr End--------------------');


         jai_item_classification_pkg.insert_item_class_hdr
                                                      (p_hdr_rec      => v_hdr_rec);
      --   fnd_file.put_line (fnd_file.LOG,
     --                       'Template Header id :'
     --                       || v_hdr_rec.template_hdr_id
      --                     );

         IF v_hdr_rec.template_hdr_id <> 0
         THEN
            BEGIN
           --    fnd_file.put_line (fnd_file.LOG, 'enter into line');
               v_dtl_rec.template_dtl_id := jai_item_templ_dtls_s.NEXTVAL;
               v_dtl_rec.template_hdr_id := v_hdr_rec.template_hdr_id;
               v_dtl_rec.attribute_code := 'RECOVERABLE';
               v_dtl_rec.attribute_value := TRIM (j.attribute_value);
               v_dtl_rec.regime_id := j.regime_id;
               v_dtl_rec.copied_from_template_id := NULL;
               v_dtl_rec.user_overridden_flag := NULL;
               v_dtl_rec.applicable_flag := 'Y';
               v_dtl_rec.creation_date := SYSDATE;
               v_dtl_rec.created_by := 4191;
               v_dtl_rec.last_update_date := SYSDATE;
               v_dtl_rec.last_update_login := USERENV ('SESSIONID');
               v_dtl_rec.last_updated_by := 4191;
               v_dtl_rec.record_type_code := 'DEFINED';

         fnd_file.put_line (fnd_file.LOG, '----------------------Dtl start--------------');
                       fnd_file.put_line (fnd_file.LOG, 'd template_hdr_id :' || v_hdr_rec.template_hdr_id);
           fnd_file.put_line (fnd_file.LOG, 'd attribute_value :' || j.attribute_value);
           fnd_file.put_line (fnd_file.LOG, 'd regime_id :' || j.regime_id);
           fnd_file.put_line (fnd_file.LOG, '-------------DTL end-----------------------');


               jai_item_classification_pkg.insert_item_class_dtls
                                                      (p_dtl_rec      => v_dtl_rec);
--         --      fnd_file.put_line (fnd_file.LOG,
          --                           'Template detail id :'
          --                        || v_dtl_rec.template_dtl_id
          --                       );
            EXCEPTION
               WHEN OTHERS
               THEN
                  process_flag := 'N';
                  error_msg :=
                        error_msg
                     || '-'
                     || 'Error in Item assignment details for item '
                     || '-'
                     || j.item_number
                     || '-'
                     || j.organization_id
                     || '-'
                     || j.attribute_code
                     || '-'
                     || j.attribute_value;
              /*    fnd_file.put_line
                              (fnd_file.LOG,
                                  error_msg
                               || '-'
                               || 'Error in Item assignment details for item '
                               || '-'
                               || j.item_number
                               || '-'
                               || j.organization_id
                               || '-'
                               || j.attribute_code
                               || '-'
                               || j.attribute_value
                              );*/
            END;
         --/*
            IF     v_hdr_rec.template_hdr_id <> 0
                AND v_dtl_rec.template_dtl_id <> 0
             THEN
                BEGIN
                   v_assoc_rec.reporting_association_id :=
                                                    v_reporting_association_id;
                   v_assoc_rec.reporting_type_name := j.reporting_type_name;
                   v_assoc_rec.reporting_usage := 'LR';
                   v_assoc_rec.reporting_code_description :=
                                                  j.reporting_code_description;
                   v_assoc_rec.reporting_code := j.reporting_code;
                   v_assoc_rec.entity_code := 'ITEM';
                   v_assoc_rec.entity_id := v_hdr_rec.template_hdr_id;
                   --party_reg_id
                   v_assoc_rec.entity_source_table := 'JAI_ITEM_TEMPL_HDR';
                   v_assoc_rec.effective_from := '1-jul-17';
                   v_assoc_rec.effective_to := NULL;
                   v_assoc_rec.creation_date := SYSDATE;
                   v_assoc_rec.created_by := 4191;
                   v_assoc_rec.last_update_date := SYSDATE;
                   v_assoc_rec.last_update_login := USERENV ('SESSIONID');
                   v_assoc_rec.last_updated_by := 4191;
                   v_assoc_rec.record_type_code := 'DEFINED';
                   v_assoc_rec.reporting_code_id := j.reporting_code_id;
                   v_assoc_rec.reporting_type_id := j.reporting_type_id;
                   v_assoc_rec.regime_id := j.regime_id;
                   v_assoc_rec.stl_hdr_id := NULL;

                   fnd_file.put_line (fnd_file.LOG, '----------------------REP start--------------');
                       fnd_file.put_line (fnd_file.LOG, 'Rep template_hdr_id :' || v_hdr_rec.template_hdr_id);
                       fnd_file.put_line (fnd_file.LOG, 'Rep reporting_code_id :' || j.reporting_code_id);
                       fnd_file.put_line (fnd_file.LOG, ' Rep reporting_type_id :' ||  j.reporting_type_id);
                       fnd_file.put_line (fnd_file.LOG, ' Rep  regime_id :' ||   j.regime_id);
                       fnd_file.put_line (fnd_file.LOG, '-------------REP end-----------------------');

                   jai_reporting_pkg.insert_report_assoc
                                                   (p_assoc_rec      => v_assoc_rec);
--
                   DBMS_OUTPUT.put_line (v_assoc_rec.reporting_association_id);
                   COMMIT;
                EXCEPTION
                   WHEN OTHERS
                   THEN
                      process_flag := 'N';
                      error_msg :=
                            error_msg
                         || '-'
                         || 'Error in Item assignment Reporting for item '
                         || '-'
                         || j.item_number
                         || '-'
                         || j.organization_id;
                      DBMS_OUTPUT.put_line
                             (   error_msg
                              || '-'
                              || 'Error in Item assignment Reporting for item '
                              || '-'
                              || j.item_number
                              || '-'
                              || j.organization_id
                             );
                END;
             END IF;
             -- */
         END IF;

         IF     v_hdr_rec.template_hdr_id <> 0
            AND v_dtl_rec.template_dtl_id <> 0
            AND v_assoc_rec.reporting_association_id <> 0
         THEN
            UPDATE xxabrl.xxabrl_gst_item_assignment
               SET flag = 'Y',
                   error = error_msg
             WHERE item_number = j.item_number
               AND organization_id = j.organization_id;
         ELSE
            UPDATE xxabrl.xxabrl_gst_item_assignment
               SET flag = 'E',
                   error = error_msg
             WHERE item_number = j.item_number
               AND organization_id = j.organization_id;
         END IF;
      EXCEPTION
         WHEN OTHERS
         THEN
            process_flag := 'N';
            error_msg :=
                  'Error in Item assignment for item '
               || '-'
               || j.item_number
               || '-'
               || j.organization_id;
           /* fnd_file.put_line (fnd_file.LOG,
                                  'Error in Item assignment for item '
                               || '-'
                               || j.item_number
                               || '-'
                               || j.organization_id
                               || SQLERRM
                              );*/
      END;

      COMMIT;
   END LOOP;
END xxmrl_item_class_prc;
/


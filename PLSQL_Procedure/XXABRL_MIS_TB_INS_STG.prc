CREATE OR REPLACE PROCEDURE APPS.XXABRL_MIS_TB_INS_STG(ERRBUF  OUT VARCHAR2,
                                                    RETCODE OUT VARCHAR2) AS

                                                    
 

/******************************************************************************************************************************************

****
                        WIPRO Infotech Ltd, Mumbai, India

            Name: ABRL MIS TB Loading into Stage

            Change Record:
           =========================================================================================================================
           Version   Date          Author                    Remarks                  Documnet Ref
           =======   ==========   =============             ==================  =====================================================
           1.0.0     22-Nov-2012   Amresh Kumar Chutke      Initial Version
           1.0.1     23-Nov-2012   Amresh Kumar Chutke      Deriving Code Combination ID from GL Code Combinations 
                                                            for exception reporting. 
           1.0.2     4-Feb-2013    Sravan Kumar             Adding Incremental logic 
           1.0.3     18-Feb-2013   Sravan Kumar             Adding variable to take max update date from tb stg table     
           1.0.4     02-Mar-2013   Sravan kumar             Comment delete command 
           1.0.5     04-May-2013   Sravan Kumar             Change the period year to 
           

*******************************************************************************************************************************************

*****/

V_REQUEST_ID NUMBER:=0;
VD_LAST_UPDATE_DATE DATE;---18feb13 added by sravan

CURSOR CUR_MIS_TB (VD_LAST_UPDATE_DATE DATE) IS
    SELECT XMIS.CC_DESCRIPTION
            ,XMIS.MIS_CC_REPORTING_FUNCTION
            ,XMIS.LOCATION_DESCRIPTION
            ,XMIS.MIS_LOCATION_OPERATING_UNIT
            ,XMIS.MIS_LOCATION_SBU
            ,XMIS.MIS_LOCATION_TYPE
            ,XMIS.MIS_LOCATION_ZONE
            ,XMIS.MIS_LOCATION_REGION
            ,XMIS.ACCOUNT_DESCRIPTION
            ,XMIS.MIS_SCH_VI_CLASSIFICATION
            ,XMIS.MIS_SCH_VI_MAIN_GROUP
            ,XMIS.MIS_SCH_VI_SUB_GROUP
            ,XMIS.MIS_SCH_VI_NO
            ,XMIS.MIS_BUDGET_HEAD
            ,XMIS.MIS_BUSINESS_FORMAT
            ,XMIS.MIS_BUSINESS_FORMAT_MAPPING
            ,XMIS.CC_END_DATE
            ,XMIS.CC_START_DATE
            ,XMIS.LOCATION_START_DATE
            ,XMIS.LOCATION_END_DATE
            ,XMIS.ACCOUNT_START_DATE
            ,XMIS.ACCOUNT_END_DATE
            ,GCC.CODE_COMBINATION_ID,GCC.CONCATENATED_SEGMENTS
            ,GCC.SEGMENT1,GCC.SEGMENT2,GCC.SEGMENT3,GCC.SEGMENT4,GCC.SEGMENT5,GCC.SEGMENT6,GCC.SEGMENT7,GCC.SEGMENT8
            ,GCC.START_DATE_ACTIVE,GCC.END_DATE_ACTIVE
            ,GLB.PERIOD_NAME,GLB.PERIOD_YEAR
            ,(GLB.BEGIN_BALANCE_DR - GLB.BEGIN_BALANCE_CR) BEGINING_BAL
            ,(GLB.PERIOD_NET_DR - GLB.PERIOD_NET_CR) PERIOD_NET
            ,(GLB.BEGIN_BALANCE_DR - GLB.BEGIN_BALANCE_CR) + (GLB.PERIOD_NET_DR - GLB.PERIOD_NET_CR) ENDING_BAL
            ,GLB.LAST_UPDATE_DATE,GLB.LAST_UPDATED_BY
    FROM APPS.XXABRL_MIS_V XMIS, APPS.GL_BALANCES GLB,APPS.GL_CODE_COMBINATIONS_KFV GCC
    WHERE  GLB.CODE_COMBINATION_ID = XMIS.Code_combination_id(+)
        AND GLB.CODE_COMBINATION_ID=GCC.CODE_COMBINATION_ID
        AND GLB.PERIOD_YEAR='2014'
        AND GLB.CURRENCY_CODE='INR'
--        AND GLB.PERIOD_NAME='SEP-12'
--        AND XMIS.CCID IN (43477410,35971399,35970399);
        AND GLB.LAST_UPDATE_DATE > VD_LAST_UPDATE_DATE ; 

 BEGIN 
 
    SELECT MAX(REQUEST_ID) INTO V_REQUEST_ID 
    FROM APPS.FND_CONC_REQ_SUMMARY_V
    WHERE TRUNC(ACTUAL_START_DATE) = TRUNC(SYSDATE) 
        AND USER_CONCURRENT_PROGRAM_NAME ='ABRL MIS TB Loading into Stage'
        and CONCURRENT_PROGRAM_ID=269331;
   
  SELECT MAX(LAST_UPDATE_DATE) INTO VD_LAST_UPDATE_DATE 
    FROM XXABRL.XXABRL_MIS_TB_STG;                        
      
  ---DELETE FROM XXABRL.XXABRL_MIS_TB_STG; ---comment on 02mar13 by sravan
   
   ----COMMIT;
  
        
    FOR REC_MIS_TB IN CUR_MIS_TB(VD_LAST_UPDATE_DATE)
    LOOP
 
     INSERT INTO XXABRL_MIS_TB_STG 
        VALUES
            (REC_MIS_TB.CODE_COMBINATION_ID
            ,REC_MIS_TB.CONCATENATED_SEGMENTS
            ,REC_MIS_TB.SEGMENT1
            ,REC_MIS_TB.SEGMENT2
            ,REC_MIS_TB.SEGMENT3
            ,REC_MIS_TB.SEGMENT4
            ,REC_MIS_TB.SEGMENT5
            ,REC_MIS_TB.SEGMENT6
            ,REC_MIS_TB.SEGMENT7
            ,REC_MIS_TB.SEGMENT8
            ,REC_MIS_TB.PERIOD_NAME
            ,REC_MIS_TB.PERIOD_YEAR
            ,REC_MIS_TB.CC_DESCRIPTION
            ,REC_MIS_TB.MIS_CC_REPORTING_FUNCTION
            ,REC_MIS_TB.LOCATION_DESCRIPTION
            ,REC_MIS_TB.MIS_LOCATION_OPERATING_UNIT
            ,REC_MIS_TB.MIS_LOCATION_SBU
            ,REC_MIS_TB.MIS_LOCATION_TYPE
            ,REC_MIS_TB.MIS_LOCATION_ZONE
            ,REC_MIS_TB.MIS_LOCATION_REGION
            ,REC_MIS_TB.ACCOUNT_DESCRIPTION
            ,REC_MIS_TB.MIS_SCH_VI_CLASSIFICATION
            ,REC_MIS_TB.MIS_SCH_VI_MAIN_GROUP
            ,REC_MIS_TB.MIS_SCH_VI_SUB_GROUP
            ,REC_MIS_TB.MIS_SCH_VI_NO
            ,REC_MIS_TB.MIS_BUDGET_HEAD
            ,REC_MIS_TB.MIS_BUSINESS_FORMAT
            ,REC_MIS_TB.MIS_BUSINESS_FORMAT_MAPPING
            ,REC_MIS_TB.BEGINING_BAL
            ,REC_MIS_TB.PERIOD_NET
            ,REC_MIS_TB.ENDING_BAL
            ,REC_MIS_TB.START_DATE_ACTIVE
            ,REC_MIS_TB.END_DATE_ACTIVE
            ,REC_MIS_TB.CC_START_DATE
            ,REC_MIS_TB.CC_END_DATE
            ,REC_MIS_TB.LOCATION_START_DATE
            ,REC_MIS_TB.LOCATION_END_DATE
            ,REC_MIS_TB.ACCOUNT_START_DATE
            ,REC_MIS_TB.ACCOUNT_END_DATE
            ,SYSDATE
            ,'N'
            ,REC_MIS_TB.LAST_UPDATE_DATE
            ,REC_MIS_TB.LAST_UPDATED_BY
            ,V_REQUEST_ID
            ,'N');
           
     END LOOP;

    COMMIT;

                
 EXCEPTION
        WHEN OTHERS THEN
                    
    DBMS_OUTPUT.PUT_LINE('Unable to Update Stage Table' );

 END XXABRL_MIS_TB_INS_STG; 
/


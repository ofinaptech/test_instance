CREATE OR REPLACE PACKAGE APPS.xxabrl_resa_ar_tndr_inv_pkg
IS
/*

                 OBJECT INFORMATION

 Object Name : XXABRL_RESA_AR_TNDR_INV_PKG

 Description : Procedure to Insert sales invoices and distributions to Interface tabels

 Version    Date            Author        Remarks
_________________________
_________________________
_________________________
_________________________
 1.0.1     12-Jun-10    Govindaraj T       Condition added to avoid the inactive user sites
 1.0.2     19-Jul-11    Mitul, Dharmesh     Condition to specifically select  org_code for validation
_________________________
_________________________
_________________________
_________________________

*/
   PROCEDURE invoice_validate (
      errbuf           OUT      VARCHAR2,
      retcode          OUT      NUMBER,
      p_batch_source   IN       VARCHAR2,
      p_action         IN       VARCHAR2,
      p_org_id         IN       NUMBER
   );

   PROCEDURE invoice_insert (
      p_org_id         IN       NUMBER,
      p_batch_source   IN       VARCHAR2,
      x_ret_code       OUT      NUMBER
   );

   FUNCTION account_seg_status (
      p_seg_value   IN   VARCHAR2,
      p_seg_desc    IN   VARCHAR2
   )
      RETURN VARCHAR2;
END xxabrl_resa_ar_tndr_inv_pkg; 
/


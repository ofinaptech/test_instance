CREATE OR REPLACE PROCEDURE APPS.XXABRL_BUDGET_PROC (
   errbuf            OUT      VARCHAR2,
   retcode           OUT      VARCHAR2,
   p_budget_code     IN       VARCHAR2,
   p_budget_amount   IN       VARCHAR2,
   p_budget_month    IN       VARCHAR2,
   p_format          IN       VARCHAR2,
   p_budget_desc     IN       VARCHAR2,
   P_SBU             IN       VARCHAR2
   
)
AS
   CURSOR c1
   IS
      SELECT DISTINCT gl_account
                 FROM xxabrl.xxabrl_budget_report_stage2
                WHERE budget_code = p_budget_code AND MONTH = p_budget_month
                AND SBU = P_SBU;

   CURSOR c2 (p_gl_account VARCHAR2)
   IS
      SELECT DISTINCT LOCATION
                 FROM xxabrl.xxabrl_budget_report_stage2
                WHERE gl_account = p_gl_account AND MONTH = p_budget_month
               AND  SBU = P_SBU;

   v_row_count   NUMBER := 0;
   v_count       NUMBER := 0;
   l_count       NUMBER := 0;
   x_count       NUMBER := 0;
BEGIN
   BEGIN
      SELECT COUNT (*)
        INTO v_row_count
        FROM xxabrl.xxabrl_budget_report_stage2
       WHERE upper(format) = upper(p_format) AND upper(budget_description) = upper(p_budget_desc);
      

      fnd_file.put_line (fnd_file.LOG, v_row_count);

      UPDATE xxabrl.xxabrl_budget_report_stage2
         SET budget_code = p_budget_code
       WHERE format = p_format AND budget_description = p_budget_desc;
       

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         fnd_file.put_line (fnd_file.LOG, 'Error in updating BUDGET CODE');
   END;

   BEGIN
      FOR i1 IN c1
      LOOP
         SELECT COUNT (DISTINCT gl_account)
           INTO v_count
           FROM xxabrl.xxabrl_budget_report_stage2
          WHERE budget_code = p_budget_code AND MONTH = p_budget_month
          AND SBU = P_SBU;
          

         UPDATE xxabrl.xxabrl_budget_report_stage2
            SET budget_amount = p_budget_amount / v_count
          WHERE budget_code = p_budget_code AND MONTH = p_budget_month
          AND SBU=P_SBU;

         SELECT COUNT (DISTINCT LOCATION)
           INTO l_count
           FROM xxabrl.xxabrl_budget_report_stage2
          WHERE gl_account = i1.gl_account
            AND MONTH = p_budget_month
            AND budget_code = p_budget_code
            AND SBU = P_SBU ;

         UPDATE xxabrl.xxabrl_budget_report_stage2
            SET location_budget = budget_amount / l_count
          WHERE gl_account = i1.gl_account
            AND MONTH = p_budget_month
            AND budget_code = p_budget_code
            AND SBU = P_SBU;

         COMMIT;

         FOR i2 IN c2 (i1.gl_account)
         LOOP
            SELECT COUNT (*)
              INTO x_count 
              FROM xxabrl.xxabrl_budget_report_stage2
             WHERE 1 = 1
               AND gl_account = i1.gl_account
               AND LOCATION = i2.LOCATION
               AND MONTH = p_budget_month
               AND budget_code = p_budget_code
               AND SBU = P_SBU;

            UPDATE xxabrl.xxabrl_budget_report_stage2
               SET location_budget_dist = location_budget / x_count
             WHERE LOCATION = i2.LOCATION
               AND gl_account = i1.gl_account
               AND MONTH = p_budget_month
               AND budget_code = p_budget_code
               AND SBU = P_SBU;
         END LOOP;
      END LOOP;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         fnd_file.put_line (fnd_file.LOG, 'Error in updating BUDGET AMOUNT');
   END;
EXCEPTION
   WHEN OTHERS
   THEN
      fnd_file.put_line (fnd_file.LOG, 'Error in updating');
END XXABRL_BUDGET_PROC; 
/


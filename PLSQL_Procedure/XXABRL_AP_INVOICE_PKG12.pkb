CREATE OR REPLACE PACKAGE BODY APPS.XXABRL_AP_INVOICE_PKG12 AS
PROCEDURE XXABRL_AP_INV_PROC12(ERRBUFF             OUT VARCHAR2,
                                RETCODE             OUT NUMBER,
                                P_SOURCE            IN  VARCHAR2,
                                        P_OPERATING_UNIT    IN  VARCHAR2,
                                P_INV_DATE_FROM     IN VARCHAR2,
                                P_INV_DATE_TO       IN VARCHAR2
                                ) AS
          V_STAGELINE_AMOUNT      NUMBER(20,2);
          V_ORACLELINE_AMOUNT     NUMBER(20,2);
          v_LINE_DIFF             NUMBER(20,2);
          v_HEADER_DIFF           NUMBER(20,2);
CURSOR CUR1(CP_SOURCE VARCHAR2,CP_OPERATING_UNIT VARCHAR2,CP_INV_DATE_FROM VARCHAR2,CP_INV_DATE_TO VARCHAR2) IS
     SELECT AII.OPERATING_UNIT, AII.GL_DATE INVOICE_DATE,AII.VENDOR_NUMBER,AII.INVOICE_NUM,
       nvl(AII.INVOICE_AMOUNT,0) STAGING_HEADER_AMOUNT,nvl(AIA.INVOICE_AMOUNT,0) ORACLE_HEADER_AMOUNT
  FROM APPS.XXABRL_AP_INVOICES_INT AII, APPS.AP_INVOICES_ALL AIA
 WHERE
            AII.INVOICE_NUM          = AIA.INVOICE_NUM(+)
   AND AII.VENDOR_ID              = AIA.VENDOR_ID(+)
   AND AII.SOURCE                   = CP_SOURCE
   AND AII.OPERATING_UNIT   = NVL (CP_OPERATING_UNIT, AII.OPERATING_UNIT)
   AND AII.GL_DATE                  BETWEEN NVL (TO_DATE(CP_INV_DATE_FROM,'YYYY/MM/DD HH24:MI:SS'), AII.GL_DATE)
                            AND NVL (TO_DATE(CP_INV_DATE_TO,'YYYY/MM/DD HH24:MI:SS'), AII.GL_DATE);
                            --AND ROWNUM<100;
BEGIN
FND_FILE.PUT_LINE (FND_FILE.OUTPUT,
                         'ABRL AP INVOICE RECONCILATION REPORT'||'('||P_SOURCE||')'
                        );
      FND_FILE.PUT_LINE (FND_FILE.OUTPUT, ' ');
      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'AS ON DATE'
                         || CHR (9)
                         || SYSDATE
                         || CHR (9)
                        );
      FND_FILE.PUT_LINE (FND_FILE.OUTPUT, ' ');
      FND_FILE.PUT_LINE (FND_FILE.OUTPUT, 'REPORT PARAMETRS');
      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'SOURCE'
                         || CHR (9)
                         || P_SOURCE
                         || CHR (9)
                        );
      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'OPERATING UNIT'
                         || CHR (9)
                         ||  P_OPERATING_UNIT
                         || CHR (9)
                        );
      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'INVOICE DATE FROM'
                         || CHR (9)
                         || P_INV_DATE_FROM
                         || CHR (9)
                        );
      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'INVOICE DATE TO'
                         || CHR (9)
                         || P_INV_DATE_TO
                         || CHR (9)
                        );
-------------------------------------- DISPLAY LABELS OF THE REPORT --------------------------------------------
      FND_FILE.PUT_LINE (FND_FILE.OUTPUT, ' ');
      FND_FILE.PUT_LINE (FND_FILE.OUTPUT, ' ');
      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,
                           'OPERATING UNIT'
                         || CHR (9)
                         || 'INVOICE DATE'
                         || CHR (9)
                         || 'VENDOR NUMBER'
                         || CHR (9)
                         || 'INVOICE NUMBER'
                         || CHR (9)
                         || 'STAGING HEADER AMOUNT'
                         || CHR (9)
                         || 'STAGING LINE AMOUNT'
                         || CHR (9)
                         || 'ORACLE HEADER AMOUNT'
                         || CHR (9)
                         || 'ORACLE LINE AMOUNT'
                         || CHR (9)
                         ||'HEADER DIFF'
                         || CHR (9)
                         ||'LINE DIFF'
                         );
FOR REC1 IN CUR1(P_SOURCE,P_OPERATING_UNIT,P_INV_DATE_FROM,P_INV_DATE_TO) LOOP
EXIT WHEN CUR1%NOTFOUND;
BEGIN
V_STAGELINE_AMOUNT :=0;
SELECT NVL(SUM(AILI.AMOUNT),0) INTO V_STAGELINE_AMOUNT FROM APPS.XXABRL_AP_INVOICE_LINES_INT AILI
WHERE AILI.INVOICE_NUM=REC1.INVOICE_NUM
AND AILI.VENDOR_NUMBER=REC1.VENDOR_NUMBER;
END;
BEGIN
V_ORACLELINE_AMOUNT :=0;
SELECT NVL(SUM(AILA.AMOUNT),0) INTO V_ORACLELINE_AMOUNT
FROM APPS.AP_INVOICE_LINES_ALL AILA,APPS.AP_INVOICES_ALL AI,APPS.HR_OPERATING_UNITS HOU
WHERE AI.INVOICE_ID=AILA.INVOICE_ID(+)
AND AI.INVOICE_NUM=REC1.INVOICE_NUM
AND AI.GL_DATE=REC1.INVOICE_DATE
AND AI.ORG_ID=HOU.ORGANIZATION_ID
AND HOU.SHORT_CODE=REC1.OPERATING_UNIT
AND AILA.LINE_TYPE_LOOKUP_CODE<>'PREPAY';
END;
BEGIN
v_HEADER_DIFF :=0;
v_HEADER_DIFF :=REC1.STAGING_HEADER_AMOUNT-REC1.ORACLE_HEADER_AMOUNT;
END;
BEGIN
v_LINE_DIFF :=0;
v_LINE_DIFF :=V_STAGELINE_AMOUNT-V_ORACLELINE_AMOUNT;
END;
-------------------------- PRINTING THE DATA  ----------------------------------------------------------------
          FND_FILE.PUT_LINE (FND_FILE.OUTPUT,
                               REC1.OPERATING_UNIT
                            || CHR (9)
                            ||   REC1.INVOICE_DATE
                            || CHR (9)
                            || REC1.VENDOR_NUMBER
                            || CHR (9)
                            || REC1.INVOICE_NUM
                            || CHR (9)
                            || REC1.STAGING_HEADER_AMOUNT
                            || CHR (9)
                            || V_STAGELINE_AMOUNT
                            || CHR (9)
                            || REC1.ORACLE_HEADER_AMOUNT
                            || CHR (9)
                            || V_ORACLELINE_AMOUNT
                            || CHR (9)
                            ||v_HEADER_DIFF
                            || CHR (9)
                            ||v_LINE_DIFF
                            );
END LOOP;
END XXABRL_AP_INV_PROC12;
END  XXABRL_AP_INVOICE_PKG12; 
/


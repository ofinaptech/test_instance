CREATE OR REPLACE PROCEDURE APPS.XXABRL_TDSINV_WITHOUT_CMDM 
                               (ERRBUFF             OUT VARCHAR2,
                                RETCODE             OUT NUMBER,
                                P_ORG_ID            IN  NUMBER) AS
/*  Version         Date            Name                Remarks                               
    1.0.0        26-OCT-2010    Sayikrishna         Created for getting those Invoices which are not generated TDS CM
*/
CURSOR TDSINV_CUR IS
(
select hou.name ou,
    (select pv.vendor_name from apps.po_vendors pv where pv.vendor_id = Mi.vendor_id) vendor_name,  
       mi.invoice_id,
       mi.invoice_num,
       mi.invoice_date Inv_date,
       mi.invoice_amount
from apps.ap_invoices_all MI
    ,apps.hr_operating_units hou
where 1=1
and mi.org_id =nvl(p_org_id,mi.org_id)
and mi.org_id = hou.organization_id
AND mi.cancelled_date is null
--and TRUNC(mi.CREATION_DATE)>='01-APR-2010'
and not exists (select 1 from apps.jai_ap_tds_invoices cm
where 1=1-- cm.tds_tax_id =jcta.tax_id--19761
and cm.invoice_id =mi.invoice_id
and cm.organization_id =hou.organization_id
and cm.organization_id = mi.org_id
)     
and exists (select 1 from   apps.ap_invoice_distributions_all aida
                        ,   apps.jai_cmn_taxes_all  jcta
                where aida.invoice_id=mi.invoice_id  
                    and aida.global_attribute_category='JA.IN.APXINWKB.DISTRIBUTIONS'    
                    and aida.global_attribute1 is not null --to check TDS applied or not
                    and aida.org_id = mi.org_id
                    and jcta.section_type ='TDS_SECTION'
                    and jcta.tax_id =aida.global_attribute1
                    and aida.org_id =hou.organization_id 
            )
) ;

BEGIN
FND_FILE.PUT_LINE (FND_FILE.OUTPUT,
                         'ABRL TDS Invoices not generated CM/DM'
                        );            
                  FND_FILE.PUT_LINE (FND_FILE.OUTPUT, ' ');
/*FND_FILE.PUT_LINE (FND_FILE.OUTPUT,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Operating Unit'
                         || CHR (9)
                         ||  P_OPERATING_UNIT
                         || CHR (9)
                        );*
    /* DISPLAY LABELS OF THE REPORT */
      FND_FILE.PUT_LINE (FND_FILE.OUTPUT, ' ');
      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,
                         'Operating Unit'
                         || CHR (9)
                         || 'Vendor Name'
                         || CHR (9)
                         || 'Invoice Number'
                         || CHR (9)
                         || 'Invoice Date'
                         || CHR (9)
                         || 'Invoice Amount'
                         || CHR (9)
                         );
FOR V_REC IN TDSINV_CUR LOOP
EXIT WHEN TDSINV_CUR%NOTFOUND;
            /* PRINTING THE DATA  */
          FND_FILE.PUT_LINE (FND_FILE.OUTPUT,
                               V_REC.ou
                            || CHR (9)
                            ||   V_REC.vendor_name
                            || CHR (9)
                            || V_REC.invoice_num
                            || CHR (9)
                            || V_REC.inv_date
                            || CHR (9)
                            || V_REC.invoice_amount
                            );
END LOOP;
END XXABRL_TDSINV_WITHOUT_CMDM ; 
/


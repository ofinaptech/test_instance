CREATE OR REPLACE PACKAGE BODY APPS.xxmrl_gl_ind116_pkg
IS
      /*
      ========================
   =========================
   =========================
   =========================
   ====
      || Concurrent Program Name : XXMRL INDIAS116 To GL Interface Program 
      || Package Name                     : APPS.xxmrl_gl_ind116_pkg
      || Description                            : Processing the INDIAS116 JV's Through GL Interface
      ===============================================================================================================================
      || Version              Date                                 Author                                                                                  Description                                                                          Remarks
      || ~~~~~~~     ~~~~~~~~~~~            ~~~~~~~~~~~~~~~~~~                                      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~                              ~~~~~~~~~~~~~~~~~
      ||  1.0.0             03-Oct-2019                  Lokesh Poojari                                           Processing the INDIAS116 JV's Through GL Interface                        New Development
      ================================================================================================================================
   =========================
   =========================
   =========================
   ====*/
/*************************************************************************************************************/
/* Following Cursor will selects GL data */
/*************************************************************************************************************/
   gn_conc_req_id   NUMBER := fnd_global.conc_request_id;
   -- To print the concurrent request id
   gn_user_id       NUMBER := fnd_global.user_id;

   CURSOR val_cur
   IS
      SELECT   user_je_source_name, accounting_date, group_id2
          FROM xxmrl_jv_inter_stage
         WHERE 1 = 1
           AND TRUNC (interface_date) >= TRUNC (SYSDATE)
           AND NVL (interface_flag, 'N') IN ('N', 'E')
      GROUP BY user_je_source_name, accounting_date, group_id2;

   CURSOR mrl_gl_cur (
      p_user_je_source_name   VARCHAR2,
      p_accounting_date       DATE,
      p_group_id              NUMBER
   )
   IS
      SELECT ROWID, jis.*
        FROM xxmrl_jv_inter_stage jis
       WHERE 1 = 1
         AND user_je_source_name = p_user_je_source_name
         AND accounting_date = p_accounting_date
         AND group_id2 = p_group_id
         AND NVL (interface_flag, 'N') IN ('N', 'E')
         AND TRUNC (interface_date) >= TRUNC (SYSDATE);

   /*  ERRORS RECORDS ARE PROCESSED ONCE AGAIN FOR RE-VALIDATION */
   CURSOR mrl_gl_int_cur
   IS
      SELECT ROWID, jis.*
        FROM xxmrl_jv_inter_stage jis
       WHERE 1 = 1
         AND error_message IS NULL
         AND interface_flag = 'V'
         AND TRUNC (interface_date) >= TRUNC (SYSDATE);

   /*************************************************************************************************************/
/* Main Procedure calling */
/*************************************************************************************************************/
   PROCEDURE main_proc (x_err_buf OUT VARCHAR2, x_ret_code OUT NUMBER)
   IS
      ln_val_ins_retcode   NUMBER         := 0;
      --variable to return the status
      ln_val_retcode       NUMBER         := 0;
      v_tot_cnt            NUMBER         := 0;
      v_succ_cnt           NUMBER         := 0;
      v_fail_cnt           NUMBER         := 0;
      v_user_name          VARCHAR2 (100) := NULL;
      l_validate_cnt       NUMBER         := 0;
      nv_segment_count     NUMBER         := 0;
      nv_tsrl_migration    NUMBER;
   BEGIN
      x_ret_code := 0;
      x_err_buf := NULL;

      SELECT user_name
        INTO v_user_name
        FROM fnd_user
       WHERE user_id = gn_user_id;

      UPDATE xxmrl_jv_inter_stage
         SET entered_dr = NULL
       WHERE entered_dr = 0;

      UPDATE xxmrl_jv_inter_stage
         SET entered_cr = NULL
       WHERE entered_cr = 0;

      COMMIT;

      SELECT COUNT (1)
        INTO v_tot_cnt
        FROM xxmrl_jv_inter_stage
       WHERE 1 = 1
         AND interface_flag IS NULL
         AND error_message IS NULL
         AND TRUNC (interface_date) >= TRUNC (SYSDATE);

      SELECT COUNT (1)
        INTO l_validate_cnt
        FROM xxmrl_jv_inter_stage
       WHERE 1 = 1
         AND NVL (interface_flag, 'N') IN ('N', 'E')
         AND TRUNC (interface_date) >= TRUNC (SYSDATE);

      IF l_validate_cnt <> 0
      THEN
         xxmrl_gl_ind116_pkg.xxmrl_gl_valid_proc
                                             (x_val_retcode      => ln_val_retcode);
         COMMIT;
      END IF;

      BEGIN
         xxmrl_gl_ind116_pkg.xxmrl_gl_int_proc
                                     (x_val_ins_retcode      => ln_val_ins_retcode);
      END;

      SELECT COUNT (1)
        INTO v_succ_cnt
        FROM xxmrl_jv_inter_stage
       WHERE 1 = 1
         AND interface_flag = 'P'
         AND error_message IS NULL
         AND TRUNC (interface_date) >= TRUNC (SYSDATE);

      SELECT COUNT (1)
        INTO v_fail_cnt
        FROM xxmrl_jv_inter_stage
       WHERE 1 = 1
         AND interface_flag = 'E'
         AND TRUNC (interface_date) >= TRUNC (SYSDATE);

      --  To return the status of the MAIN procedure
      IF NVL (ln_val_ins_retcode, 0) = 0 AND NVL (ln_val_retcode, 0) = 0
      THEN
         x_ret_code := 0;
      ELSE
         x_ret_code := 1;
      END IF;

      fnd_file.put_line
         (fnd_file.output,
          ('----------------------------------------------------------------------------------------')
         );
      fnd_file.put_line
         (fnd_file.output,
          ('                                                                                        ')
         );
      fnd_file.put_line (fnd_file.output,
                         (   '  Request id     : '
                          || gn_conc_req_id
                          || '                     '
                          || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS')
                         )
                        );
      fnd_file.put_line (fnd_file.output,
                         ('  Requester Name : ' || v_user_name
                         )
                        );
      fnd_file.put_line
         (fnd_file.output,
          ('                                                                                        ')
         );
      fnd_file.put_line
         (fnd_file.output,
          ('                              MRL INDIAS116 TO GL                                            ')
         );
      fnd_file.put_line
         (fnd_file.output,
          ('                                                                                        ')
         );
      fnd_file.put_line (fnd_file.output,
                         (   '                           Run Date: '
                          || TO_CHAR (SYSDATE, 'DD/MM/YYYY')
                         )
                        );
      fnd_file.put_line
         (fnd_file.output,
          ('                                                                                        ')
         );
      fnd_file.put_line
         (fnd_file.output,
          ('                                                                                        ')
         );
      fnd_file.put_line
            (fnd_file.output,
             (   '  Total Number of Records in GL Staging Table            : '
              || v_tot_cnt
             )
            );
      fnd_file.put_line
         (fnd_file.output,
          ('                                                                                        ')
         );
      fnd_file.put_line
            (fnd_file.output,
             (   '  Total Number of records inserted into interface table  : '
              || v_succ_cnt
             )
            );
      fnd_file.put_line
         (fnd_file.output,
          ('                                                                                        ')
         );
      fnd_file.put_line
            (fnd_file.output,
             (   '  Total number of records errored                        : '
              || v_fail_cnt
             )
            );
      fnd_file.put_line
         (fnd_file.output,
          ('                                                                                        ')
         );
      fnd_file.put_line
         (fnd_file.output,
          ('                                                                                        ')
         );
      fnd_file.put_line
         (fnd_file.output,
          ('----------------------------------------------------------------------------------------')
         );

      nv_segment_count := 0;
   EXCEPTION
      WHEN OTHERS
      THEN
         fnd_file.put_line (fnd_file.LOG,
                               'ERROR : MAIN PROCEDURE '
                            || SUBSTR (SQLERRM, 1, 300)
                           );
         x_ret_code := 2;
   END main_proc;

/*************************************************************************************************************/
/* Procedure for Validating Staging table */
/*************************************************************************************************************/
   PROCEDURE xxmrl_gl_valid_proc (x_val_retcode OUT NUMBER)
   IS
      v_ledger_id               NUMBER;
      v_err_msg                 VARCHAR2 (2000);
      v_err_flag                VARCHAR2 (1);
      v_user_je_cat_name        VARCHAR2 (25);
      v_user_je_src_name        VARCHAR2 (25);
      v_period_name             VARCHAR2 (15);
      v_dummy                   VARCHAR2 (1);
      v_currency_code           VARCHAR2 (20);
      v_conversion_type         VARCHAR2 (30);
      v_code_combination_id     NUMBER;
      v_chart_of_accounts_id    NUMBER;
      v_cnt                     NUMBER          := 0;
--        v_vat number:=0;
      v_user_name               VARCHAR2 (100);
      x_concatenated_segments   VARCHAR2 (100);
      nv_error_counter          NUMBER;
   BEGIN
      SELECT user_name
        INTO v_user_name
        FROM fnd_user
       WHERE user_id = gn_user_id;

      fnd_file.put_line
         (fnd_file.LOG,
          ('------------------------------------------------------------------------------                           ')
         );
      fnd_file.put_line
         (fnd_file.LOG,
          ('                                                                                                       ')
         );
      fnd_file.put_line (fnd_file.LOG,
                         (   '  Request id     : '
                          || gn_conc_req_id
                          || '                               '
                          || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS')
                         )
                        );
      fnd_file.put_line (fnd_file.LOG,
                         ('  Requester Name : ' || v_user_name));
      fnd_file.put_line
         (fnd_file.LOG,
          ('                                                                                                       ')
         );
      fnd_file.put_line
         (fnd_file.LOG,
          ('                        MRL INDIAS116 TO GL ERROR REPORT                                                      ')
         );
      fnd_file.put_line
         (fnd_file.LOG,
          ('                                                                                                       ')
         );
      fnd_file.put_line (fnd_file.LOG,
                         (   '                           Run Date: '
                          || TO_CHAR (SYSDATE, 'DD/MM/YYYY')
                         )
                        );
      fnd_file.put_line
         (fnd_file.LOG,
          ('                                                                                                       ')
         );
      fnd_file.put_line
         (fnd_file.LOG,
          ('------------------------------------------------------------------------------                          ')
         );
      fnd_file.put_line
         (fnd_file.LOG,
          ('                                                                                                       ')
         );
      --fnd_file.put_line(fnd_file.log,('                                                                                                       '));
      fnd_file.put_line
         (fnd_file.LOG,
          (' SEQ_NUM     ERROR_MESSAGES                                                                            ')
         );
      fnd_file.put_line
         (fnd_file.LOG,
          (' -------     ---------------                                                                              ')
         );
      v_err_msg := NULL;

      FOR r_val IN val_cur
      LOOP
         FOR mrl_gl_rec IN mrl_gl_cur (r_val.user_je_source_name,
                                         r_val.accounting_date,
                                         r_val.group_id2
                                        )
         LOOP
            --Initialize Variables
            v_err_flag := NULL;
            v_err_msg := NULL;
            v_ledger_id := NULL;
            v_user_je_cat_name := NULL;
            v_user_je_src_name := NULL;
            v_period_name := NULL;
            v_dummy := NULL;
            v_currency_code := NULL;
            v_code_combination_id := NULL;
            v_chart_of_accounts_id := NULL;
            nv_error_counter := 0;

/*************************************************************************************************************/
/* Deriving Ledger_id from Segment3 */
/*************************************************************************************************************/
            BEGIN
               v_ledger_id := 2101;

               IF TRUNC (mrl_gl_rec.accounting_date) < '01-APR-2019'
               THEN
                  BEGIN
                     SELECT gls.ledger_id
                       INTO v_ledger_id
                       FROM apps.gl_ledgers gls,
                            apps.gl_code_combinations_kfv gcc,
                            apps.gl_ledger_segment_values glsv
                      WHERE gls.ret_earn_code_combination_id =
                                                       gcc.code_combination_id
                        AND gcc.segment1 = mrl_gl_rec.segment1
                        AND glsv.ledger_id = gls.ledger_id
                        AND glsv.segment_value = mrl_gl_rec.segment3;
                  EXCEPTION
                     WHEN NO_DATA_FOUND
                     THEN
                        v_err_msg :=
                              v_err_msg
                           || ' Invalid Segment3 to derive ledger id '
                           || mrl_gl_rec.segment3;
                        v_err_flag := 'Y';
                        x_val_retcode := 1;
                        fnd_file.put_line
                           (fnd_file.LOG,
                               '   '
                            || mrl_gl_rec.seq_num
                            || '.  '
                            || ' Invalid segment3 (State SBU) to derive Ledger ID : '
                            || mrl_gl_rec.segment3
                           );
                     WHEN TOO_MANY_ROWS
                     THEN
                        v_err_msg :=
                              v_err_msg
                           || ' Too many rows for '
                           || mrl_gl_rec.segment3;
                        v_err_flag := 'Y';
                        x_val_retcode := 1;
                        fnd_file.put_line
                           (fnd_file.LOG,
                               '   '
                            || mrl_gl_rec.seq_num
                            || '.  '
                            || ' Too many rows segment3 (State SBU) to derive Ledger ID : '
                            || mrl_gl_rec.segment3
                           );
                     WHEN OTHERS
                     THEN
                        v_err_msg :=
                              v_err_msg
                           || ' Unexpected while deriving ledger id '
                           || SQLERRM;
                        v_err_flag := 'Y';
                        x_val_retcode := 1;
                        fnd_file.put_line
                                   (fnd_file.LOG,
                                       '   '
                                    || mrl_gl_rec.seq_num
                                    || '.  '
                                    || ' Unexpected while deriving ledger id '
                                    || SQLERRM
                                   );
                  END;
               END IF;
            END;

 /*************************************************************************************************************/
/* VAT Accounting Codes are not allowed from 1st July 2017 due to GST                                                                       */
/*************************************************************************************************************/
            BEGIN
               --   V_VAT := 0;
               IF     mrl_gl_rec.segment6 = 226106
                  AND mrl_gl_rec.accounting_date > '30-JUN-17'
               THEN
                  v_err_flag := 'Y';
                  v_err_msg :=
                         'VAT Regime Accounts are not accepted in GST Regime';
               END IF;
            EXCEPTION
               WHEN OTHERS
               THEN
                  NULL;
            END;

/*************************************************************************************************************/
/* Validation for User Category Name */
/*************************************************************************************************************/
            BEGIN
               SELECT user_je_category_name
                 INTO v_user_je_cat_name
                 FROM apps.gl_je_categories
                WHERE UPPER (user_je_category_name) IN (
                         SELECT UPPER (description)
                           FROM fnd_lookup_values
                          WHERE lookup_type = 'MRL_JV_CATEGORY_DERIVATION'
                            AND lookup_code = mrl_gl_rec.group_id2);
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  v_err_msg :=
                        ' Mismatch - group id category description with Journal Category '
                     || mrl_gl_rec.group_id2;
                  v_err_flag := 'Y';
                  x_val_retcode := 1;
                  fnd_file.put_line
                     (fnd_file.LOG,
                         '   '
                      || mrl_gl_rec.seq_num
                      || '.  '
                      || ' Mismatch - Group ID category description with Journal Category : '
                      || mrl_gl_rec.group_id2
                     );
               WHEN OTHERS
               THEN
                  v_err_msg :=
                             ' Unexpected Journal Category Error ' || SQLERRM;
                  v_err_flag := 'Y';
                  x_val_retcode := 1;
                  fnd_file.put_line (fnd_file.LOG,
                                        '   '
                                     || mrl_gl_rec.seq_num
                                     || '.  '
                                     || ' Unexpected Journal Category Error '
                                     || SQLERRM
                                    );
            END;

/*************************************************************************************************************/
/* Validation for Journal Source name */
/*************************************************************************************************************/
            BEGIN
               SELECT user_je_source_name
                 INTO v_user_je_src_name
                 FROM apps.gl_je_sources
                WHERE UPPER (user_je_source_name) =
                                       UPPER (mrl_gl_rec.user_je_source_name);
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  v_err_msg :=
                        ' Invalid Journal Source '
                     || mrl_gl_rec.user_je_source_name;
                  v_err_flag := 'Y';
                  x_val_retcode := 1;
                  fnd_file.put_line (fnd_file.LOG,
                                        '   '
                                     || mrl_gl_rec.seq_num
                                     || '.  '
                                     || ' Invalid Journal Source : '
                                     || mrl_gl_rec.user_je_source_name
                                    );
               WHEN OTHERS
               THEN
                  v_err_msg := ' Unexpected Journal Source Error ' || SQLERRM;
                  v_err_flag := 'Y';
                  x_val_retcode := 1;
                  fnd_file.put_line (fnd_file.LOG,
                                        '   '
                                     || mrl_gl_rec.seq_num
                                     || '.  '
                                     || ' Unexpected Journal Source Error '
                                     || SQLERRM
                                    );
            END;

/*************************************************************************************************************/
/* Validation for Period Name */
/*************************************************************************************************************/
            BEGIN
               SELECT period_name
                 INTO v_period_name
                 FROM apps.gl_periods
                WHERE UPPER (period_name) = UPPER (mrl_gl_rec.period_name)
                  AND UPPER (period_set_name) = UPPER ('ABRL Calendar');
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  v_err_msg :=
                           ' Invalid Period Name ' || mrl_gl_rec.period_name;
                  v_err_flag := 'Y';
                  x_val_retcode := 1;
                  fnd_file.put_line (fnd_file.LOG,
                                        '   '
                                     || mrl_gl_rec.seq_num
                                     || '.  '
                                     || ' Invalid Period Name : '
                                     || mrl_gl_rec.period_name
                                    );
               WHEN OTHERS
               THEN
                  v_err_msg := 'Unexpected Error in Period Name' || SQLERRM;
                  v_err_flag := 'Y';
                  x_val_retcode := 1;
                  fnd_file.put_line (fnd_file.LOG,
                                        '   '
                                     || mrl_gl_rec.seq_num
                                     || '.  '
                                     || ' Unexpected Error in Period Name '
                                     || SQLERRM
                                    );
            END;

            /*************************************************************************************************************/
/* Validation for Period Name is open or not */
/*************************************************************************************************************/
            BEGIN
               SELECT 'Y'
                 INTO v_dummy
                 FROM gl_period_statuses
                WHERE UPPER (period_name) = UPPER (mrl_gl_rec.period_name)
                  AND (closing_status = 'O' OR closing_status = 'F')
                  AND set_of_books_id = v_ledger_id
                  AND application_id IN (
                                        SELECT application_id
                                          FROM fnd_application
                                         WHERE application_short_name =
                                                                       'SQLGL');
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  v_err_msg :=
                        v_err_msg
                     || ' Period is not opened '
                     || mrl_gl_rec.period_name;
                  v_err_flag := 'Y';
                  x_val_retcode := 1;
                  fnd_file.put_line (fnd_file.LOG,
                                        '   '
                                     || mrl_gl_rec.seq_num
                                     || '.  '
                                     || ' Period not open : '
                                     || mrl_gl_rec.period_name
                                    );
               WHEN OTHERS
               THEN
                  v_err_msg := ' Unexpected Error in Period ' || SQLERRM;
                  v_err_flag := 'Y';
                  x_val_retcode := 1;
                  fnd_file.put_line (fnd_file.LOG,
                                        '   '
                                     || mrl_gl_rec.seq_num
                                     || '.  '
                                     || ' Unexpected Error in Period '
                                     || SQLERRM
                                    );
            END;

/*************************************************************************************************************/
/* Validate currency code */
/*************************************************************************************************************/
            BEGIN
               SELECT currency_code
                 INTO v_currency_code
                 FROM apps.fnd_currencies
                WHERE UPPER (currency_code) =
                                             UPPER (mrl_gl_rec.currency_code);
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  v_err_msg :=
                        v_err_msg
                     || ' Invalid Currency Code '
                     || mrl_gl_rec.currency_code;
                  v_err_flag := 'Y';
                  x_val_retcode := 1;
                  fnd_file.put_line (fnd_file.LOG,
                                        '   '
                                     || mrl_gl_rec.seq_num
                                     || '.  '
                                     || ' Invalid Currency Code : '
                                     || mrl_gl_rec.currency_code
                                    );
               WHEN OTHERS
               THEN
                  v_err_msg :=
                         v_err_msg || 'Unexpected Curr Code Error' || SQLERRM;
                  v_err_flag := 'Y';
                  x_val_retcode := 1;
                  fnd_file.put_line (fnd_file.LOG,
                                        '   '
                                     || mrl_gl_rec.seq_num
                                     || '.  '
                                     || ' Unexpected Curr Code Error'
                                     || SQLERRM
                                    );
            END;

/*************************************************************************************************************/
/* Validate USER CURRENCY CONVERSION TYPE */
/*************************************************************************************************************/
            BEGIN
               SELECT conversion_type
                 INTO v_conversion_type
                 FROM apps.gl_daily_conversion_types
                WHERE conversion_type =
                                     mrl_gl_rec.user_currency_conversion_type
                  AND conversion_type IN ('Corporate', 'Spot');
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  v_err_msg :=
                        v_err_msg
                     || ' Invalid User Currency Conversion Type '
                     || mrl_gl_rec.user_currency_conversion_type;
                  v_err_flag := 'Y';
                  x_val_retcode := 1;
                  fnd_file.put_line
                               (fnd_file.LOG,
                                   '   '
                                || mrl_gl_rec.seq_num
                                || '.  '
                                || ' Invalid User Currency Conversion Type : '
                                || mrl_gl_rec.user_currency_conversion_type
                               );
               WHEN OTHERS
               THEN
                  v_err_msg :=
                        v_err_msg
                     || 'Unexpected User Currency Conversion Type Error'
                     || SQLERRM;
                  v_err_flag := 'Y';
                  x_val_retcode := 1;
                  fnd_file.put_line
                         (fnd_file.LOG,
                             '   '
                          || mrl_gl_rec.seq_num
                          || '.  '
                          || ' Unexpected User Currency Conversion Type Error'
                          || SQLERRM
                         );
            END;

/*************************************************************************************************************/
/* Validate segment values */
/*************************************************************************************************************/
            BEGIN
               v_code_combination_id := NULL;
               x_concatenated_segments := NULL;
               nv_error_counter := 0;

               SELECT code_combination_id
                 INTO v_code_combination_id
                 FROM gl_code_combinations_kfv
                WHERE 1 = 1
                  AND end_date_active is null
                  AND segment1 = mrl_gl_rec.segment1
                  AND segment2 = mrl_gl_rec.segment2
                  AND segment3 = mrl_gl_rec.segment3
                  AND segment4 = mrl_gl_rec.segment4
                  AND segment5 = mrl_gl_rec.segment5
                  AND segment6 = mrl_gl_rec.segment6
                  AND segment7 = mrl_gl_rec.segment7
                  AND segment8 = mrl_gl_rec.segment8;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  v_err_msg :=
                        v_err_msg
                     || ' Invalid Segments : '
                     || mrl_gl_rec.segment1
                     || '.'
                     || mrl_gl_rec.segment2
                     || '.'
                     || mrl_gl_rec.segment3
                     || '.'
                     || mrl_gl_rec.segment4
                     || '.'
                     || mrl_gl_rec.segment5
                     || '.'
                     || mrl_gl_rec.segment6
                     || '.'
                     || mrl_gl_rec.segment7
                     || '.'
                     || mrl_gl_rec.segment8;
                  v_err_flag := 'Y';
                  x_val_retcode := 1;
                  nv_error_counter := 1;
                  fnd_file.put_line (fnd_file.LOG,
                                        '   '
                                     || mrl_gl_rec.seq_num
                                     || '.  '
                                     || ' Invalid Segments: '
                                     || mrl_gl_rec.segment1
                                     || '.'
                                     || mrl_gl_rec.segment2
                                     || '.'
                                     || mrl_gl_rec.segment3
                                     || '.'
                                     || mrl_gl_rec.segment4
                                     || '.'
                                     || mrl_gl_rec.segment5
                                     || '.'
                                     || mrl_gl_rec.segment6
                                     || '.'
                                     || mrl_gl_rec.segment7
                                     || '.'
                                     || mrl_gl_rec.segment8
                                    );

                  BEGIN
                     v_chart_of_accounts_id := 50328;
                     x_concatenated_segments :=
                           mrl_gl_rec.segment1
                        || '.'
                        || mrl_gl_rec.segment2
                        || '.'
                        || mrl_gl_rec.segment3
                        || '.'
                        || mrl_gl_rec.segment4
                        || '.'
                        || mrl_gl_rec.segment5
                        || '.'
                        || mrl_gl_rec.segment6
                        || '.'
                        || mrl_gl_rec.segment7
                        || '.'
                        || mrl_gl_rec.segment8;

                     SELECT fnd_flex_ext.get_ccid
                                            ('SQLGL',
                                             'GL#',
                                             50328,
                                             TO_CHAR (SYSDATE,
                                                      'YYYY/MM/DD HH24:MI:SS'
                                                     ),
                                             x_concatenated_segments
                                            )
                       INTO v_code_combination_id
                       FROM DUAL;

                     IF v_code_combination_id = 0
                     THEN
                        v_err_msg :=
                              v_err_msg
                           || ' Invalid Code Combination : '
                           || mrl_gl_rec.segment1
                           || '.'
                           || mrl_gl_rec.segment2
                           || '.'
                           || mrl_gl_rec.segment3
                           || '.'
                           || mrl_gl_rec.segment4
                           || '.'
                           || mrl_gl_rec.segment5
                           || '.'
                           || mrl_gl_rec.segment6
                           || '.'
                           || mrl_gl_rec.segment7
                           || '.'
                           || mrl_gl_rec.segment8;
                        v_err_flag := 'Y';
                        x_val_retcode := 1;
                        fnd_file.put_line (fnd_file.LOG,
                                              '   '
                                           || mrl_gl_rec.seq_num
                                           || '.  '
                                           || ' Invalid Code Combination : '
                                           || mrl_gl_rec.segment1
                                           || '.'
                                           || mrl_gl_rec.segment2
                                           || '.'
                                           || mrl_gl_rec.segment3
                                           || '.'
                                           || mrl_gl_rec.segment4
                                           || '.'
                                           || mrl_gl_rec.segment5
                                           || '.'
                                           || mrl_gl_rec.segment6
                                           || '.'
                                           || mrl_gl_rec.segment7
                                           || '.'
                                           || mrl_gl_rec.segment8
                                          );
                     ELSE
                        IF NVL (nv_error_counter, 0) = 1
                        THEN
                           v_err_flag := 'N';
                           x_val_retcode := 0;
                        END IF;
                     END IF;
                  END;
               WHEN OTHERS
               THEN
                  v_err_msg :=
                          v_err_msg || 'Unexpected Segments Error' || SQLERRM;
                  v_err_flag := 'Y';
                  x_val_retcode := 1;
                  fnd_file.put_line (fnd_file.LOG,
                                        '   '
                                     || mrl_gl_rec.seq_num
                                     || '.  '
                                     || ' Unexpected Segments Error'
                                     || SQLERRM
                                    );
            END;

            /*************************************************************************************************************/
/* updating stging table */
/*************************************************************************************************************/
            IF NVL (v_err_flag, 'N') = 'Y'
            THEN
               BEGIN
                  UPDATE apps.xxmrl_jv_inter_stage
                     SET interface_flag = 'E',
                         error_message =
                               error_message
                            || ' ; '
                            || mrl_gl_rec.seq_num
                            || '.   '
                            || v_err_msg
                   WHERE 1 = 1 AND ROWID = mrl_gl_rec.ROWID;

                  fnd_file.put_line (fnd_file.LOG,
                                        '   '
                                     || mrl_gl_rec.seq_num
                                     || '.  '
                                     || v_err_msg
                                    );
                  COMMIT;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     fnd_file.put_line
                            (fnd_file.LOG,
                                '   '
                             || mrl_gl_rec.seq_num
                             || '.  '
                             || ' Error in Update xxmrl_jv_inter_stage table'
                             || SQLERRM
                            );
               END;
            ELSE
               BEGIN
                  UPDATE apps.xxmrl_jv_inter_stage
                     SET interface_flag = 'V',
                         error_message = NULL,
                         ledger_id = v_ledger_id,
                         chart_of_accounts_id = v_chart_of_accounts_id,
                         user_je_category_name = v_user_je_cat_name,
                         user_je_source_name = v_user_je_src_name,
                         code_combination_id = v_code_combination_id
                   WHERE 1 = 1
-- AND USER_JE_SOURCE_NAME='OCTROI'
                         AND ROWID = mrl_gl_rec.ROWID;

                  COMMIT;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     fnd_file.put_line
                            (fnd_file.LOG,
                                '   '
                             || mrl_gl_rec.seq_num
                             || '.  '
                             || ' Error in Update xxmrl_jv_inter_stage table'
                             || SQLERRM
                            );
                     x_val_retcode := 1;
               END;
            END IF;
         END LOOP;

         BEGIN
            v_cnt := 0;

            SELECT COUNT (group_id2)
              INTO v_cnt
              FROM xxmrl_jv_inter_stage jis
             WHERE 1 = 1
               AND user_je_source_name = r_val.user_je_source_name
               AND accounting_date = r_val.accounting_date
               AND group_id2 = r_val.group_id2
               AND interface_flag = 'E';

            IF v_cnt > 0
            THEN
               UPDATE xxmrl_jv_inter_stage
                  SET interface_flag = 'E'
                WHERE 1 = 1
                  AND user_je_source_name = r_val.user_je_source_name
                  AND accounting_date = r_val.accounting_date
                  AND group_id2 = r_val.group_id2
                  AND NVL (interface_flag, 'N') = 'E';

               COMMIT;
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               fnd_file.put_line
                            (fnd_file.LOG,
                                ' Error in Update xxmrl_jv_inter_stage table'
                             || SQLERRM
                            );
               x_val_retcode := 1;
         END;
      END LOOP;

      fnd_file.put_line
                       (fnd_file.LOG,
                        ('*** Validation for Duplicate Sequence Numbers ***  ')
                       );
      fnd_file.put_line
                      (fnd_file.LOG,
                       ('--------------------------------------------------- ')
                      );

      BEGIN
         FOR cur_dup IN (SELECT   seq_num
                             FROM xxmrl_jv_inter_stage
                            WHERE seq_num IS NOT NULL
                              AND TRUNC (accounting_date) >= '01-APR-2019'
                              AND TRUNC (interface_date) >= TRUNC (SYSDATE)
                         GROUP BY seq_num
                           HAVING COUNT (*) > 1)
         LOOP
            BEGIN
               UPDATE xxmrl_jv_inter_stage jis
                  SET jis.interface_flag = 'E',
                      jis.error_message =
                           'DUPLICATE SEQUENCE NUMBER ; ' || jis.error_message
                WHERE jis.seq_num = cur_dup.seq_num;

               v_err_flag := 'Y';
               fnd_file.put_line (fnd_file.LOG,
                                     'Duplicate Sequence Number in staging: '
                                  || cur_dup.seq_num
                                 );
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  fnd_file.put_line (fnd_file.LOG,
                                        'Seq# not found for update: '
                                     || cur_dup.seq_num
                                    );
               WHEN OTHERS
               THEN
                  fnd_file.put_line
                         (fnd_file.LOG,
                             'Error while Updating for duplicate Sequence#: '
                          || cur_dup.seq_num
                         );
            END;
         END LOOP;
      END;

      fnd_file.put_line
                  (fnd_file.LOG,
                   ('=================
=========================
========= '        )
                  );
      COMMIT;
      fnd_file.put_line
         (fnd_file.LOG,
          ('                                                                                                       ')
         );
   END xxmrl_gl_valid_proc;

/*************************************************************************************************************/
/* Procedure for Inserting into interface table */
/*************************************************************************************************************/
   PROCEDURE xxmrl_gl_int_proc (x_val_ins_retcode OUT NUMBER)
   IS
      CURSOR cur_sub
      IS
         SELECT   user_je_source_name, GROUP_ID
             FROM gl_interface
            WHERE 1 = 1
              AND date_created LIKE SYSDATE
              AND user_je_source_name='INDIAS116'
         GROUP BY user_je_source_name, GROUP_ID;

      v_gl_access_set_id     NUMBER;
      v_ledger_id            NUMBER;
      v_ledger_name          VARCHAR2 (250);
      v_error_cnt            NUMBER;
      v_insert_cnt           NUMBER;
      v_group_id             NUMBER;
      ln_loader_request_id   NUMBER;
      v_set_of_books_id      NUMBER  := fnd_profile.VALUE ('GL_SET_OF_BKS_ID');
      v_user_id              NUMBER         := fnd_profile.VALUE ('USER_ID');
      v_resp_id              NUMBER         := fnd_profile.VALUE ('RESP_ID');
      v_appl_id              NUMBER      := fnd_profile.VALUE ('RESP_APPL_ID');

      -- Cursor added for Data access set
      CURSOR led_id_cur
      IS
         SELECT DISTINCT lgr.ledger_id, lgr.NAME
                    -- into v_LEDGER_ID, v_LEDGER_NAME
         FROM            gl_access_set_ledgers gas,
                         gl_ledgers lgr,
                         gl_interface gli
                   WHERE gas.access_set_id =
                                        fnd_profile.VALUE ('GL_ACCESS_SET_ID')
                     AND gas.ledger_id = lgr.ledger_id
                     AND gli.ledger_id = lgr.ledger_id;
   BEGIN
      v_insert_cnt := 0;

      FOR r_int IN mrl_gl_int_cur
      LOOP
         BEGIN
            INSERT INTO apps.gl_interface
                        (status, ledger_id,
                         accounting_date, currency_code,
                         date_created, created_by, actual_flag,
                         user_je_category_name,
                         user_je_source_name,
                         currency_conversion_date,
                         user_currency_conversion_type,
                         entered_dr, entered_cr,
                         transaction_date, period_name,
                         code_combination_id, accounted_dr,
                         accounted_cr, currency_conversion_rate,
                         chart_of_accounts_id, set_of_books_id,
                         reference10, 
                         attribute1, GROUP_ID
                        )
                 VALUES (r_int.status, r_int.ledger_id,
                         r_int.accounting_date, r_int.currency_code,
                         SYSDATE, 3, r_int.actual_flag,
                         r_int.user_je_category_name,
                         r_int.user_je_source_name,
                         r_int.currency_conversion_date,
                         r_int.user_currency_conversion_type,
                         r_int.entered_dr, r_int.entered_cr,
                         r_int.transaction_date, r_int.period_name,
                         r_int.code_combination_id, r_int.entered_dr,
                         r_int.entered_cr, r_int.currency_conversion_rate,
                         r_int.chart_of_accounts_id, r_int.ledger_id,
                         r_int.reference10, ---Journal Line Description (In GL_JE_LINES Description field )
                         r_int.attribute1, r_int.group_id2
                        );

            UPDATE apps.xxmrl_jv_inter_stage
               SET interface_flag = 'P'
             WHERE 1 = 1 AND interface_flag = 'V' AND ROWID = r_int.ROWID;

            COMMIT;
            v_insert_cnt := v_insert_cnt + 1;
         EXCEPTION
            WHEN OTHERS
            THEN
               fnd_file.put_line
                  (fnd_file.LOG,
                      ' Error in Interface Program while inserting the record '
                   || SQLERRM
                  );
               x_val_ins_retcode := 1;
         END;
      END LOOP;

      v_gl_access_set_id := fnd_profile.VALUE ('GL_ACCESS_SET_ID');

      IF v_gl_access_set_id IS NULL
      THEN
         fnd_file.put_line
                          (fnd_file.LOG,
                           'GL_ACCESS_SET_ID / Ledger ID Setup Incomplete...'
                          );
      ELSE
         fnd_file.put_line (fnd_file.LOG,
                            'GL_ACCESS_SET_ID: ' || v_gl_access_set_id
                           );

         BEGIN
            OPEN led_id_cur;

            FETCH led_id_cur
             INTO v_ledger_id, v_ledger_name;

            CLOSE led_id_cur;

            fnd_file.put_line (fnd_file.LOG,
                                  'LEDGER ID: '
                               || v_ledger_id
                               || ';  LEDGER_NAME: '
                               || v_ledger_name
                              );

            BEGIN
               SELECT GROUP_ID
                 INTO v_group_id
                 FROM gl_srs_ji_groups_v
                WHERE je_source_name = je_source_name            --'INDIAS116'
                  --and ledger_id =v_LEDGER_ID
                  AND ledger_id IN (
                         SELECT lgr.ledger_id
                           FROM gl_access_set_ledgers gas, gl_ledgers lgr
                          WHERE gas.access_set_id = v_gl_access_set_id
                            AND gas.ledger_id = lgr.ledger_id)
                  AND ROWNUM = 1;

               fnd_file.put_line (fnd_file.LOG,
                                  'Derived Group ID this Ledger '
                                  || v_group_id
                                 );
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  fnd_file.put_line
                     (fnd_file.LOG,
                         'No Group ID found from GL Interface for this Ledger '
                      || v_ledger_id
                     );
               WHEN OTHERS
               THEN
                  fnd_file.put_line
                     (fnd_file.LOG,
                         'Exception while deriving Group_ID/GL Interface/Ledger '
                      || v_ledger_id
                     );
            END;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               fnd_file.put_line
                             (fnd_file.LOG,
                                 'Ledger ID not found for GL_ACCESS_SET_ID: '
                              || v_gl_access_set_id
                             );
            WHEN OTHERS
            THEN
               fnd_file.put_line (fnd_file.LOG,
                                     'Exception ( '
                                  || SQLERRM
                                  || ' )while deriving Ledger ID for : '
                                  || v_gl_access_set_id
                                 );
         END;
      END IF;

      IF v_ledger_id IS NOT NULL
      THEN
-- IF 1=1 THEN
         BEGIN
            v_error_cnt := 0;

            SELECT COUNT (*)
              INTO v_error_cnt
              FROM xxmrl_jv_inter_stage
             WHERE 1 = 1
               AND interface_flag = 'V'
               AND seq_num IS NOT NULL
               AND ledger_id = v_ledger_id
               AND TRUNC (interface_date) >= TRUNC (SYSDATE);

            IF v_insert_cnt > 0 AND v_group_id IS NOT NULL
            THEN
               BEGIN
                  fnd_global.apps_initialize (user_id           => v_user_id,
                                              resp_id           => v_resp_id,
                                              resp_appl_id      => v_appl_id
                                             );
                  COMMIT;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     fnd_file.put_line (fnd_file.LOG,
                                        'Error in App Intialize ' || SQLERRM
                                       );
               END;

               BEGIN
                  FOR req_sub IN cur_sub
                  LOOP
                     FOR led_id IN led_id_cur
                     LOOP
                        -- mrl Ledger
                        BEGIN
                           ln_loader_request_id :=
                              fnd_request.submit_request
                                 ('SQLGL',
                                  'GLLEZLSRS',
                                  '',
                                  '',
                                  FALSE,
                                  1120,                  --v_gl_access_set_id,
                                 '121',                  --- INDIAS116   Source..
                                  2101,                    --LED_ID.ledger_id,
                                  req_sub.GROUP_ID, ---INDIAS116 Group Id Constant
                                  'N',
                                  'N',
                                  'O',
                                  CHR (0)
                                 );
                           COMMIT;
                        EXCEPTION
                           WHEN OTHERS
                           THEN
                              fnd_file.put_line
                                        (fnd_file.LOG,
                                            'Exception while Request submit '
                                         || SQLERRM
                                        );
                        END;
                     --    END IF;
                     END LOOP;
                  ----
                  END LOOP;

                  COMMIT;

                  IF (ln_loader_request_id = 0)
                  THEN
                     fnd_file.put_line (fnd_file.LOG,
                                           'Request Not Submitted due to "'
                                        || fnd_message.get
                                        || '".'
                                       );
                     fnd_file.put_line
                        (fnd_file.LOG,
                            'Concurrent Request For Loading STD. GL IMPORT Generated an error, Submitted with request id '
                         || ln_loader_request_id
                        );
                  ELSE
                     fnd_file.put_line
                            (fnd_file.LOG,
                                'Submitted Loader Program with request id : '
                             || TO_CHAR (ln_loader_request_id)
                            );
                  END IF;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     fnd_file.put_line (fnd_file.LOG,
                                           'Exception while Request submit '
                                        || SQLERRM
                                       );
               END;
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               fnd_file.put_line (fnd_file.LOG,
                                  'Exception while couting Error records'
                                 );
         END;
      END IF;
   END xxmrl_gl_int_proc;
END xxmrl_gl_ind116_pkg; 
/


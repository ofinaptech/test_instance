CREATE OR REPLACE PROCEDURE APPS.XXABRL_SUPPLIER_SITE_INACTIVE
 (errbuf   OUT VARCHAR2,
  retcode   OUT varchar2)
  IS


cursor c1 IS
SELECT xx.VENDOR_NUM,pvs.VENDOR_SITE_ID,pvs.org_id FROM  APPS.XXABRL_SUPP_DEACTIVE_SITE  xx,apps.ap_suppliers pv,apps.ap_supplier_sites_all pvs,
hr_operating_units hr
where xx.VENDOR_NUM=pv.segment1
and hr.ORGANIZATION_ID=pvs.ORG_ID
and xx.VENDOR_SITE_CODE=pvs.VENDOR_SITE_CODE
and pvs.VENDOR_ID=pv.VENDOR_ID
and xx.ORG_ID=hr.ORGANIZATION_ID;
--AND PVS.VENDOR_SITE_ID=127043;


l_msg_count NUMBER := 0;
l_msg_data VARCHAR2 (2000) := NULL;
l_checkmsg_data VARCHAR2 (2000) := NULL;
l_msg_data_err VARCHAR2 (2000) := NULL;
l_return_status VARCHAR2 (10) := NULL;
l_vendor_site_rec ap_vendor_pub_pkg.r_vendor_site_rec_type;
l_vendor_site_id ap_supplier_sites.vendor_site_id%TYPE;
l_msg_index_out NUMBER;
BEGIN
--mo_global.init ('SQLAP');
--mo_global.set_policy_context('S','2658');
-- Initialize values of API attributes

for  I in c1
LOOP 

l_vendor_site_id :=I.VENDOR_SITE_ID;
--l_vendor_site_rec.supplier_notif_method := 'EMAIL';
--l_vendor_site_rec.email_address := 'Titaporn_Tuchinda1@mckinsey.com';
--l_vendor_site_rec.remittance_email := 'Titaporn_Tuchinda1@mckinsey.com';
--l_vendor_site_rec.remit_advice_delivery_method := 'EMAIL';
--l_vendor_site_rec.ext_payee_rec.default_pmt_method := 'EFT';
--l_vendor_site_rec.ext_payee_rec.remit_advice_email := 'Titaporn_Tuchinda@mckinsey.com';
l_vendor_site_rec.inactive_date :='7-jun-2012';
l_vendor_site_rec.org_id :=I.ORG_ID;
-- Added by Sreekumar nair on 07-Mar-2012 in order to deactivate a supplier site, if required.
-- API Call for supplier site updation
ap_vendor_pub_pkg.update_vendor_site
(p_api_version => 1.0,
p_init_msg_list => fnd_api.g_true,
p_commit => fnd_api.g_false,
p_validation_level => fnd_api.g_valid_level_full,
x_return_status => l_return_status,
x_msg_count => l_msg_count,
x_msg_data => l_msg_data,
p_vendor_site_rec => l_vendor_site_rec,
p_vendor_site_id => l_vendor_site_id
);

END LOOP;

commit;

EXCEPTION
WHEN OTHERS THEN
dbms_output.put_line(SQLERRM); 

END XXABRL_SUPPLIER_SITE_INACTIVE; 
/


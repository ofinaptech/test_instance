CREATE OR REPLACE PACKAGE APPS.XXABRL_SUPP_BANK_PMT_YESBANK AS
  /**********************************************************************************************************************************************
                        WIPRO Infotech Ltd, Mumbai, India
            Name        : Supplier Bank Outbound interface for YES Bank [H2H Interface betwen ABRL and YES Bank]
            Change Record:
           =========================================================================================================================
           Version   Date          Author               Remarks                  Documnet Ref
           =======   ==========   =============        ============================================================================
           1.0.0     21-May-2012    Amresh Kumar Chutke  Initial Version
  ************************************************************************************************************************************************/

PROCEDURE XXABRL_YESBANK_PAYMENT_SUPP ( errbuf     out VARCHAR2
                                  ,      retcode    out VARCHAR2
                                  ,      P_FROM_DATE  VARCHAR2
                                  ,      P_TO_DATE   VARCHAR2   );

--This Procedure will Process all Valid Payments into YESBANK_PAYMENT_TABLE

PROCEDURE XXABRL_UPDATE_YESBANK_INFO  (  errbuf     out VARCHAR2
                                  ,  retcode    out VARCHAR2);
/*This Program will Update (AP_CHECKS_ALL) the DFF in Payment Screen(YES Bank Payment Information),
   Once the DBS Bank will Provide
*/

PROCEDURE XXABRL_YB_USER_RECTIFIED_PAY( errbuf     out VARCHAR2
                                  ,  retcode    out VARCHAR2
                                  ,  p_from_check_id NUMBER
                                  ,  p_to_check_id NUMBER );

/*  This Program will Update the YESBANK_PAYMENT_TABLE ,
    with all Corrected data by User as well as Transaction Status as
*/

PROCEDURE XXABRL_SCD_YB_PAYMENT_SUPP( errbuf     out VARCHAR2
                                         ,  retcode    out VARCHAR2);
/*  This Procedure will Process all Valid Payments into YESBANK_PAYMENT_TABLE thru Schdule Program
*/
END; 
/


CREATE OR REPLACE procedure APPS.XXABRL_NAV_INVOICE_VALIDATION(errbuf out varchar2,
retcode out varchar2)
as
/*****************************************************************************************
WIPRO Infotech Ltd, Mumbai, India

Object Name : XXABRL_NAV_INVOICE_VALIDATION

Description : Procedure to Validate the Invoices which comes from the NAVISION Source

Change Record:
==========================================================================================
Version Date Author Remarks
======= ========== ============= ==========================
1.0 25-Aug-09 Praveen Kumar Initial Version
1.0.1 12-jan-10 Naresh Hasti Included the RETEK also in the procedure
1.0.2 24-Mar-10 Praveen Kumar Ledger Id paramater in Invoice Validation is commented.
Since invoice Validation program is erroring out.
*******************************************************************************************/
l_request_id NUMBER;
v_resp_id NUMBER := Fnd_Profile.VALUE('RESP_ID');
v_user_id NUMBER := Fnd_Profile.VALUE('USER_ID');
V_RESP_APPL_ID NUMBER := Fnd_Profile.VALUE('RESP_APPL_ID');
v_ledger_id NUMBER;
/*CURSOR CUR_VALIDATION
IS SELECT DISTINCT ORG_ID,BATCH_ID,BATCH_NAME FROM (
SELECT invoice_num
,invoice_id
,api.invoice_amount
,api.payment_status_flag
,api.invoice_type_lookup_code
,aba.batch_name
,aba.batch_id
,api.org_id
,ap_invoices_pkg.get_approval_status
(api.invoice_id
,api.invoice_amount
,api.payment_status_flag
,api.invoice_type_lookup_code
) approval_status
from ap_invoices_all api
,ap_batches_all aba
where api.batch_id = aba.batch_id
and source IN ('NAVISION','RETEK')
and api.creation_date like sysdate
and api.org_id IN (SELECT organization_id
FROM hr_operating_units
WHERE set_of_books_id IN(SELECT default_ledger_id
FROM fnd_profile_option_values fpop,
fnd_profile_options fpo,
gl_access_sets gas
WHERE fpop.profile_option_id = fpo.profile_option_id
AND gas.access_set_id = fpop.profile_option_value
AND level_value = V_RESP_ID
AND profile_option_name = 'GL_ACCESS_SET_ID')))WHERE approval_status = 'NEVER APPROVED';*/
CURSOR CUR_VALIDATION
IS
SELECT DISTINCT org_id, batch_id, batch_name
FROM (SELECT invoice_num, invoice_id, api.invoice_amount,
api.payment_status_flag, api.invoice_type_lookup_code,
aba.batch_name, aba.batch_id, api.org_id,
ap_invoices_pkg.get_approval_status
(api.invoice_id,
api.invoice_amount,
api.payment_status_flag,
api.invoice_type_lookup_code
) approval_status
FROM ap_invoices_all api, ap_batches_all aba
WHERE api.batch_id = aba.batch_id
AND SOURCE IN ('NAVISION','RETEK')
--AND api.creation_date LIKE SYSDATE
AND api.org_id IN (
SELECT organization_id
FROM (SELECT organization_id
FROM fnd_profile_option_values fpop,
fnd_profile_options fpo,
per_security_organizations_v pso
WHERE fpop.profile_option_id = fpo.profile_option_id
AND level_value =V_RESP_ID
AND pso.security_profile_id = fpop.profile_option_value
AND profile_option_name = 'XLA_MO_SECURITY_PROFILE_LEVEL'
UNION
SELECT TO_NUMBER (fpop.profile_option_value)
FROM fnd_profile_option_values fpop,
fnd_profile_options fpo
WHERE fpop.profile_option_id = fpo.profile_option_id
AND level_value = v_resp_id
AND profile_option_name = 'ORG_ID')))
WHERE approval_status = 'NEVER APPROVED';
BEGIN

BEGIN
SELECT default_ledger_id
INTO v_ledger_id
FROM fnd_profile_option_values fpop,
fnd_profile_options fpo ,
gl_access_sets gas
WHERE fpop.profile_option_id =fpo.profile_option_id
AND gas.access_set_id=fpop.profile_option_value
AND level_value=V_RESP_ID
AND profile_option_name='GL_ACCESS_SET_ID';
EXCEPTION
WHEN NO_DATA_FOUND THEN
fnd_file.put_line(fnd_file.log,'Ledger Id not found for this responsibility - '||V_RESP_ID);
WHEN TOO_MANY_ROWS THEN
fnd_file.put_line(fnd_file.log,'Too Many Ledgers found for this responsibility - '||V_RESP_ID);
WHEN OTHERS THEN
fnd_file.put_line(fnd_file.log,'Invalid Ledger for this responsibility - '||V_RESP_ID);
END;
fnd_global.apps_initialize(v_user_id,v_resp_id,V_RESP_APPL_ID);
FOR cur_rec in cur_validation LOOP
BEGIN
l_request_id:= fnd_request.submit_request (
'SQLAP', --Program Application short name
'APPRVL', --Program short name
'',
'',
FALSE,
cur_rec.org_id,--Report parameter Org Id
'All',
cur_rec.batch_id, --Report parameter batch Id
'',
'',
'',
'',
'',
'',
--v_ledger_id,
'N',
1000
);
fnd_file.put_line(fnd_file.output,'Processing for Batch Name - ' ||cur_rec.batch_name);
fnd_file.put_line(fnd_file.output,'Submitted with Request id - ' ||l_request_id);
fnd_file.put_line(fnd_file.output,'org_id - ' ||cur_rec.org_id);
fnd_file.put_line(fnd_file.output,'-------------------------------------------------');
COMMIT;
EXCEPTION
WHEN OTHERS THEN
fnd_file.put_line(fnd_file.output,'Validation Submission fialed for Batch Name -'||cur_rec.batch_name);
END;
END LOOP;
COMMIT;
END XXABRL_NAV_INVOICE_VALIDATION;
/


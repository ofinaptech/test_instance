CREATE OR REPLACE PACKAGE APPS.xxabrl_gl_offset_pkg3
AS
   PROCEDURE xxabrl_gl_offset_proc (
      errbuff           OUT   VARCHAR2,
      retcode           OUT   NUMBER,
      p_from_co               VARCHAR2,
      p_to_co                 VARCHAR2,
      p_from_sbu              NUMBER,
      p_to_sbu                NUMBER,
      p_from_location         NUMBER,
      p_to_location           NUMBER,
      p_from_account          NUMBER,
      p_to_account            NUMBER,
      p_from_gl_date          DATE,
      p_to_gl_date            DATE,
      p_batch_name            VARCHAR2
   );
END xxabrl_gl_offset_pkg3; 
/


CREATE OR REPLACE PROCEDURE APPS.gs_abrl_unqsupp_one_in_one_xml (
   errbuf         OUT      VARCHAR2,
   retcode        OUT      NUMBER,
   p_run_date_1   IN       VARCHAR2
)
AS
   p_run_date               DATE
                  := NVL (fnd_date.canonical_to_date (p_run_date_1), SYSDATE);
   v_file                   UTL_FILE.file_type;
   g_conc_request_id        NUMBER   := fnd_profile.VALUE ('CONC_REQUEST_ID');
   xx_purchase_count        NUMBER;
   xx_pur_tax               NUMBER             := 0;
   xx_purchase_tax_region   NUMBER;

--     count1 number:=0;

   /*Cursor to fetch SUPPLIER RECORD*/
----C1------
   CURSOR xx_supplier_c1
   IS
      SELECT DISTINCT vendor_number, vendor_id, location_id, creation_date,
                      terms
                 FROM apps.xx_uniquesupplier_details_v
                --xx_supplier_details_v
      WHERE           1 = 1
                  AND (   TRUNC (last_update_date_pv) >= TRUNC (p_run_date)
                       OR TRUNC (last_update_date_pvsa) >= TRUNC (p_run_date)
                       OR TRUNC (last_update_date_jep) >= TRUNC (p_run_date)
                       OR TRUNC (last_update_date_jvh) >= TRUNC (p_run_date)
                       OR TRUNC (last_update_date_jcv) >= TRUNC (p_run_date)
                      );

     --AND segment1 IN  ('7004886')
   --  AND vendor_number I('2104491');

   /*Cursor to fetch SUPPPLIER DETAILS RECORS DEPENED ON C1 PARATMETER/*/
   ---C2---
   CURSOR xx_supplier_deatils_c2 (
      p_vendor_number    VARCHAR2,
      p_vendor_id        NUMBER,
      p_location_id      NUMBER,
      pv_creation_date   DATE,
      p_terms_id         VARCHAR2
   )
   IS
      SELECT (CASE
                 WHEN TRUNC (pv_creation_date) BETWEEN TRUNC (p_run_date)
                                                   AND TRUNC (SYSDATE)
                    THEN 'A'
                 ELSE 'M'
              END
             ) action_code,
             xsd.*
        FROM apps.xx_uniquesupplier_details_v xsd ---xx_supplier_details_v xsd
       WHERE xsd.vendor_id = p_vendor_id
         AND xsd.vendor_number = p_vendor_number
         AND terms = p_terms_id
         AND xsd.location_id = p_location_id
         --     AND segment1 IN ('7004886')
         --     AND vendor_number IN('2104491')
         AND ROWNUM < 2;

   /*Cursor to fetch SUPPPLIER ADDERSS DETAILS RECORS DEPENED ON C1 PARATMETER/*/
   CURSOR xx_adderss_deatils_purchase (
      p_vendor_id       NUMBER,
      p_vendor_number   VARCHAR2
   )
   IS
      SELECT DISTINCT module, key_value_1, vendor_id, key_value_2, seq_no,
                      address_type, primary_addr_ind, add_1, add_2, add_3,
                      city, state, country, POST, contact_name, contact_phone,
                      contact_fax, contact_telex, contact_email,
                 --     siteid_status, 
                      oracle_vendor_site_id_1
                 FROM apps.xx_uv_adderss_purchase_v xsad
                WHERE xsad.vendor_id = p_vendor_id
                  AND xsad.key_value_1 = p_vendor_number
             --  AND key_value_1 IN ('2104491')
              -- AND vendor_id in  (15361755)
      ORDER BY        address_type ASC;

   CURSOR xx_adderss_deatils_payment (
      p_vendor_id       NUMBER,
      p_vendor_number   VARCHAR2
   )
   IS
      SELECT DISTINCT module, key_value_1, vendor_id, key_value_2, seq_no,
                      address_type, primary_addr_ind, add_1, add_2, add_3,
                      city, state, country, POST, contact_name, contact_phone,
                      contact_fax, contact_telex, contact_email,
                    --  siteid_status, 
                      oracle_vendor_site_id_1
                 FROM apps.xx_uv_adderss_payment_v xsad
                WHERE xsad.vendor_id = p_vendor_id
                  AND xsad.key_value_1 = p_vendor_number
                  AND xsad.pay_site_flag = 'Y'
               --   AND key_value_1 IN ('2104491')
             --     AND vendor_id in  (15361755)
      ORDER BY        address_type ASC;

      /* cursor to fetch org details depend on c1 parameter  */
--      c4
   CURSOR xx_org_details_payment (
      p_vendor_id       NUMBER,
      p_vendor_number   VARCHAR2,
      p_add1            VARCHAR2,
      p_add2            VARCHAR2
   )
   IS
      SELECT DISTINCT ou_id, oracle_vendor_site_id, siteid_status
                 FROM apps.xx_uv_adderss_payment_v xsad
                WHERE xsad.vendor_id = p_vendor_id
                  AND xsad.key_value_1 = p_vendor_number
                  AND add_1 = p_add1
                  AND NVL (add_2, NULL) = NVL (p_add2, NULL)
                  AND ou_id NOT IN
                               (147, 801, 861, 941, 841, 901, 961, 821) -- old
             --   AND key_value_1 IN ('2104491')
             --   AND vendor_id in (15361755)
      GROUP BY        ou_id, oracle_vendor_site_id, siteid_status;

   CURSOR xx_org_details_purchase (
      p_vendor_id       NUMBER,
      p_vendor_number   VARCHAR2,
      p_add1            VARCHAR2,
      p_add2            VARCHAR2
   )
   IS
      SELECT DISTINCT ou_id, oracle_vendor_site_id, siteid_status
                 FROM apps.xx_uv_adderss_purchase_v xsad
                WHERE xsad.vendor_id = p_vendor_id
                  AND xsad.key_value_1 = p_vendor_number
                  AND add_1 = p_add1
                  AND NVL (add_2, NULL) = NVL (p_add2, NULL)
                  AND ou_id NOT IN
                               (147, 801, 861, 941, 841, 901, 961, 821) -- old
              -- AND key_value_1 IN ('2104491')
             --  AND vendor_id in  (15361755)
      GROUP BY        ou_id, oracle_vendor_site_id, siteid_status;

   --c5
   CURSOR xx_end_active_date (
      p_vendor_number   VARCHAR2,
      p_vendor_id       NUMBER,
      p_terms_id        VARCHAR2,
      p_location_id     NUMBER
   )
   IS
      SELECT DISTINCT xsd.YEAR, xsd.MONTH, xsd.DAY, xsd.HOUR, xsd.MINUTE,
                      xsd.SECOND
                 FROM apps.xx_uniquesupplier_details_v xsd
                --xx_supplier_details_v xsd
      WHERE           xsd.vendor_id = p_vendor_id
                  AND xsd.vendor_number = p_vendor_number
                  AND terms = p_terms_id
                   --  AND segment1 IN  ('7004886')
                  --   AND vendor_number IN ('2104491')
                  AND location_id = p_location_id;

--     c6
   CURSOR xx_dsd_supplier (
      p_vendor_number   VARCHAR2,
      p_vendor_id       NUMBER,
      p_terms_id        VARCHAR2,
      p_location_id     NUMBER
   )
   IS
      SELECT DISTINCT xsd.dsd_supplier_ind
                 FROM apps.xx_uniquesupplier_details_v xsd
                --xx_supplier_details_v xsd
      WHERE           xsd.vendor_id = p_vendor_id
                  AND xsd.vendor_number = p_vendor_number
                  AND terms = p_terms_id
                  -- AND segment1 IN  ('7004886')
                   --AND vendor_number IN ('2104491')
                  AND location_id = p_location_id;
BEGIN
   /*First line of XML data should be <?xml version=?1.0??>*/
--   fnd_file.put_line (fnd_file.output,
--                      '<?xml version="1.0" encoding="utf-8" ?>'
--                     );
--   fnd_file.put_line
--             (fnd_file.output,
--              '<!--
-- Generated by Oracle Reports version 10.1.2.3.0
--  -->'
--             );
--   fnd_file.put_line (fnd_file.output, '<VENDORDESC>');
   FOR main_supplier IN xx_supplier_c1
   LOOP
      fnd_file.put_line (fnd_file.LOG,
                         'Vendor_no' || main_supplier.vendor_number
                        );
--   count1 :=count1+1;
--    fnd_file.put_line (fnd_file.log,'count' || count1);
      v_file :=
         UTL_FILE.fopen ('UNIQUE_VENDOR',
                            'RSupplierA_'
                         || main_supplier.vendor_number
                         || '_'
                         || g_conc_request_id
                         || '.xml',                              --RsupplierA_
                         'w'
                        );

      FOR xx_main_sub IN xx_supplier_deatils_c2 (main_supplier.vendor_number,
                                                 main_supplier.vendor_id,
                                                 main_supplier.location_id,
                                                 main_supplier.creation_date,
                                                 main_supplier.terms
                                                )
      LOOP
         UTL_FILE.put_line (v_file,
                            '<?xml version="1.0" encoding="utf-8" ?>');
         UTL_FILE.put_line (v_file, '<VENDORDESC>');
         UTL_FILE.put_line (v_file, '<VendorHdrDesc>');
         UTL_FILE.put_line (v_file,
                               '<Action_code>'
                            || xx_main_sub.action_code
                            || '</Action_code>'
                           );
         UTL_FILE.put_line (v_file,
                               '<supplier>'
                            || xx_main_sub.vendor_number
                            || '</supplier>'
                           );
         UTL_FILE.put_line
                      (v_file,
                          '<sup_name>'
                       || xxabrl_supplier_specialchars
                                                      (xx_main_sub.vendor_name)
                       || '</sup_name>'
                      );
         UTL_FILE.put_line (v_file,
                               '<contact_name> '
                            || NVL (xx_main_sub.contact_name, 'Null')
                            || '</contact_name> '
                           );
         UTL_FILE.put_line (v_file,
                               '<contact_phone>'
                            || xx_main_sub.contact_phone
                            || '</contact_phone>'
                           );
         UTL_FILE.put_line (v_file,
                               '<contact_fax>'
                            || xx_main_sub.fax
                            || '</contact_fax>'
                           );
         UTL_FILE.put_line (v_file,
                               '<contact_pager>'
                            || xx_main_sub.contact_pager
                            || '</contact_pager>'
                           );
         UTL_FILE.put_line (v_file,
                               '<sup_status>'
                            || xx_main_sub.sup_status
                            || '</sup_status>'
                           );
         UTL_FILE.put_line (v_file,
                            '<qc_ind>' || xx_main_sub.qc_ind || '</qc_ind>'
                           );
         UTL_FILE.put_line (v_file,
                            '<qc_pct>' || xx_main_sub.qc_pct || '</qc_pct>'
                           );
         UTL_FILE.put_line (v_file,
                            '<qc_freq>' || xx_main_sub.qc_freq || '</qc_freq>'
                           );
         UTL_FILE.put_line (v_file,
                            '<vc_ind>' || xx_main_sub.vc_ind || '</vc_ind>'
                           );
         UTL_FILE.put_line (v_file,
                            '<vc_pct>' || xx_main_sub.vc_pct || '</vc_pct>'
                           );
         UTL_FILE.put_line (v_file,
                            '<vc_freq>' || xx_main_sub.vc_freq || '</vc_freq>'
                           );
         UTL_FILE.put_line (v_file,
                               '<currency_code>'
                            || xx_main_sub.currency_code
                            || '</currency_code>'
                           );
         UTL_FILE.put_line (v_file, '<lang>' || xx_main_sub.lang || '</lang>');
         UTL_FILE.put_line (v_file,
                            '<terms>' || xx_main_sub.terms || '</terms>'
                           );
         UTL_FILE.put_line (v_file,
                               '<freight_terms>'
                            || xx_main_sub.freight_terms
                            || '</freight_terms>'
                           );
         UTL_FILE.put_line (v_file,
                               '<ret_allow_ind>'
                            || xx_main_sub.ret_allow_ind
                            || '</ret_allow_ind>'
                           );
         UTL_FILE.put_line (v_file,
                               '<ret_auth_req>'
                            || xx_main_sub.ret_auth_req
                            || '</ret_auth_req>'
                           );
         UTL_FILE.put_line (v_file,
                               '<ret_min_dol_amt>'
                            || xx_main_sub.ret_min_dol_amt
                            || '</ret_min_dol_amt>'
                           );
         UTL_FILE.put_line (v_file,
                               '<ret_courier>'
                            || xx_main_sub.ret_courier
                            || '</ret_courier>'
                           );
         UTL_FILE.put_line (v_file,
                               '<handling_pct>'
                            || xx_main_sub.handling_pct
                            || '</handling_pct>'
                           );
         UTL_FILE.put_line (v_file,
                               '<edi_po_ind>'
                            || xx_main_sub.edi_po_ind
                            || '</edi_po_ind>'
                           );
         UTL_FILE.put_line (v_file,
                               '<edi_po_chg>'
                            || xx_main_sub.edi_po_chg
                            || '</edi_po_chg>'
                           );
         UTL_FILE.put_line (v_file,
                               '<edi_po_confirm>'
                            || xx_main_sub.edi_po_confirm
                            || '</edi_po_confirm>'
                           );
         UTL_FILE.put_line (v_file,
                            '<edi_asn>' || xx_main_sub.edi_asn || '</edi_asn>'
                           );
         UTL_FILE.put_line (v_file,
                               '<edi_sales_rpt_freq>'
                            || xx_main_sub.edi_sales_rpt_freq
                            || '</edi_sales_rpt_freq>'
                           );
         UTL_FILE.put_line (v_file,
                               '<edi_supp_available_ind>'
                            || xx_main_sub.edi_supp_available_ind
                            || '</edi_supp_available_ind>'
                           );
         UTL_FILE.put_line (v_file,
                               '<edi_contract_ind>'
                            || xx_main_sub.edi_contract_ind
                            || '</edi_contract_ind>'
                           );
         UTL_FILE.put_line (v_file,
                               '<edi_invc_ind>'
                            || xx_main_sub.edi_invc_ind
                            || '</edi_invc_ind>'
                           );
         UTL_FILE.put_line (v_file,
                               '<cost_chg_pct_var>'
                            || xx_main_sub.cost_chg_pct_var
                            || '</cost_chg_pct_var>'
                           );
         UTL_FILE.put_line (v_file,
                               '<cost_chg_amt_var>'
                            || xx_main_sub.cost_chg_amt_var
                            || '</cost_chg_amt_var>'
                           );
         UTL_FILE.put_line (v_file,
                               '<replen_approval_ind>'
                            || xx_main_sub.replen_approval_ind
                            || '</replen_approval_ind>'
                           );
         UTL_FILE.put_line (v_file,
                               '<ship_method>'
                            || xx_main_sub.ship_method
                            || '</ship_method>'
                           );
         UTL_FILE.put_line (v_file,
                               '<payment_method>'
                            || xx_main_sub.payment_method
                            || '</payment_method>'
                           );
         UTL_FILE.put_line (v_file,
                               '<contact_telex>'
                            || xx_main_sub.contact_telex
                            || '</contact_telex>'
                           );
         UTL_FILE.put_line (v_file,
                               '<contact_email>'
                            || xx_main_sub.email_address
                            || '</contact_email>'
                           );
         UTL_FILE.put_line (v_file,
                               '<settlement_code>'
                            || xx_main_sub.settlement_code
                            || '</settlement_code>'
                           );
         UTL_FILE.put_line (v_file,
                               '<pre_mark_ind>'
                            || xx_main_sub.pre_mark_ind
                            || '</pre_mark_ind>'
                           );
         UTL_FILE.put_line (v_file,
                               '<auto_appr_invc_ind>'
                            || xx_main_sub.auto_appr_invc_ind
                            || '</auto_appr_invc_ind>'
                           );
         UTL_FILE.put_line (v_file,
                               '<dbt_memo_code>'
                            || xx_main_sub.dbt_memo_code
                            || '</dbt_memo_code>'
                           );
         UTL_FILE.put_line (v_file,
                               '<freight_charge_ind>'
                            || xx_main_sub.freight_charge_ind
                            || '</freight_charge_ind>'
                           );
         UTL_FILE.put_line (v_file,
                               '<auto_appr_dbt_memo_ind>'
                            || xx_main_sub.auto_appr_dbt_memo_ind
                            || '</auto_appr_dbt_memo_ind>'
                           );
         UTL_FILE.put_line (v_file,
                               '<inv_mgmt_lvl>'
                            || xx_main_sub.inv_mgmt_lvl
                            || '</inv_mgmt_lvl>'
                           );
         UTL_FILE.put_line (v_file,
                               '<backorder_ind>'
                            || xx_main_sub.backorder_ind
                            || '</backorder_ind>'
                           );

         SELECT COUNT (xsd.purchasing_site_flag)
           INTO xx_pur_tax
           FROM apps.xx_uniquesupplier_details_v xsd
          ---xx_supplier_details_v xsd
         WHERE  xsd.vendor_id = main_supplier.vendor_id
            AND xsd.vendor_number = main_supplier.vendor_number
            AND purchasing_site_flag = 'Y';

         IF xx_pur_tax > 0
         THEN
            UTL_FILE.put_line
                  (v_file,
                      '<vat_region>'
                   || xxabrl_supplier_tax_region (main_supplier.vendor_id,
                                                  main_supplier.vendor_number,
                                                  'Y'
                                                 )
                   || '</vat_region>'
                  );
         ELSE
            UTL_FILE.put_line (v_file,
                                  '<vat_region>'
                               || xx_main_sub.tax_region
                               || '</vat_region>'
                              );
         END IF;

         UTL_FILE.put_line (v_file,
                               '<taxpayer_type>'
                            || xx_main_sub.tax_payer_type
                            || '</taxpayer_type>'
                           );
         UTL_FILE.put_line (v_file,
                               '<prepay_invc_ind>'
                            || xx_main_sub.prepay_invc_ind
                            || '</prepay_invc_ind>'
                           );
         UTL_FILE.put_line (v_file,
                               '<service_perf_req_ind>'
                            || xx_main_sub.service_perf_req_ind
                            || '</service_perf_req_ind>'
                           );
         UTL_FILE.put_line (v_file,
                               '<invc_pay_loc>'
                            || xx_main_sub.invc_pay_loc
                            || '</invc_pay_loc>'
                           );
         UTL_FILE.put_line (v_file,
                               '<invc_receive_loc>'
                            || xx_main_sub.invc_receive_loc
                            || '</invc_receive_loc>'
                           );
         UTL_FILE.put_line (v_file,
                               '<addinvc_gross_net>'
                            || xx_main_sub.addinvc_gross_net
                            || '</addinvc_gross_net>'
                           );
         UTL_FILE.put_line (v_file,
                               '<delivery_policy>'
                            || xx_main_sub.delivery_policy
                            || '</delivery_policy>'
                           );
         UTL_FILE.put_line (v_file,
                               '<comment_desc>'
                            || xx_main_sub.comment_desc
                            || '</comment_desc>'
                           );
         UTL_FILE.put_line (v_file,
                               '<default_item_lead_time>'
                            || xx_main_sub.default_item_lead_time
                            || '</default_item_lead_time>'
                           );
         UTL_FILE.put_line (v_file,
                               '<duns_number>'
                            || xx_main_sub.duns_number
                            || '</duns_number>'
                           );
         UTL_FILE.put_line (v_file,
                               '<duns_loc>'
                            || xx_main_sub.duns_loc
                            || '</duns_loc>'
                           );
         UTL_FILE.put_line (v_file,
                               '<bracket_costing_ind>'
                            || xx_main_sub.bracket_costing_ind
                            || '</bracket_costing_ind>'
                           );
         UTL_FILE.put_line (v_file,
                               '<vmi_order_status>'
                            || xx_main_sub.vmi_order_status
                            || '</vmi_order_status>'
                           );

         FOR end_date_act IN xx_end_active_date (xx_main_sub.vendor_number,
                                                 xx_main_sub.vendor_id,
                                                 xx_main_sub.terms,
                                                 xx_main_sub.location_id
                                                )
         LOOP
            UTL_FILE.put_line (v_file, '<end_date_active>');
            UTL_FILE.put_line (v_file,
                               '<year>' || end_date_act.YEAR || '</year>'
                              );
            UTL_FILE.put_line (v_file,
                               '<month>' || end_date_act.MONTH || '</month>'
                              );
            UTL_FILE.put_line (v_file,
                               '<day>' || end_date_act.DAY || '</day>');
            UTL_FILE.put_line (v_file,
                               '<hour>' || end_date_act.HOUR || '</hour>'
                              );
            UTL_FILE.put_line (v_file,
                               '<minute>' || end_date_act.MINUTE
                               || '</minute>'
                              );
            UTL_FILE.put_line (v_file,
                               '<second>' || end_date_act.SECOND
                               || '</second>'
                              );
            UTL_FILE.put_line (v_file, '</end_date_active>');
         END LOOP;

         FOR dsd_supplier IN xx_dsd_supplier (xx_main_sub.vendor_number,
                                              xx_main_sub.vendor_id,
                                              xx_main_sub.terms,
                                              xx_main_sub.location_id
                                             )
         LOOP
            UTL_FILE.put_line (v_file,
                                  '<dsd_supplier_ind>'
                               || dsd_supplier.dsd_supplier_ind
                               || '</dsd_supplier_ind>'
                              );
         END LOOP;

         UTL_FILE.put_line (v_file, '</VendorHdrDesc>');

         SELECT   COUNT (purchasing_site_flag)
             INTO xx_purchase_count
             FROM apps.xx_uv_adderss_purchase_v xsad
            WHERE xsad.vendor_id = main_supplier.vendor_id
              AND xsad.key_value_1 = main_supplier.vendor_number
              AND xsad.purchasing_site_flag = 'Y'
         ORDER BY address_type ASC;

         IF xx_purchase_count > 0
         THEN
            FOR addrs IN
               xx_adderss_deatils_purchase (main_supplier.vendor_id,
                                            main_supplier.vendor_number
                                           )
            LOOP
               UTL_FILE.put_line (v_file, '<VendorAddrDesc>');
               UTL_FILE.put_line (v_file,
                                  '<module>' || addrs.module || '</module>'
                                 );
               UTL_FILE.put_line (v_file,
                                     '<key_value_1>'
                                  || addrs.key_value_1
                                  || '</key_value_1>'
                                 );
               UTL_FILE.put_line (v_file,
                                     '<key_value_2>'
                                  || addrs.key_value_2
                                  || '</key_value_2>'
                                 );
               UTL_FILE.put_line (v_file,
                                  '<seq_no>' || addrs.seq_no || '</seq_no>'
                                 );
               UTL_FILE.put_line (v_file,
                                     '<addr_type>'
                                  || addrs.address_type
                                  || '</addr_type>'
                                 );
               UTL_FILE.put_line (v_file,
                                     '<primary_addr_ind>'
                                  || addrs.primary_addr_ind
                                  || '</primary_addr_ind>'
                                 );
               UTL_FILE.put_line (v_file,
                                     '<add_1>'
                                  || xxabrl_supplier_specialchars (addrs.add_1)
                                  || '</add_1>'
                                 );
               UTL_FILE.put_line (v_file,
                                     '<add_2>'
                                  || xxabrl_supplier_specialchars (addrs.add_2)
                                  || '</add_2>'
                                 );
               UTL_FILE.put_line (v_file,
                                  '<add_3>' || addrs.add_3 || '</add_3>'
                                 );
               UTL_FILE.put_line (v_file, '<city>' || addrs.city || '</city>');
               UTL_FILE.put_line (v_file,
                                  '<state>' || addrs.state || '</state>'
                                 );
               UTL_FILE.put_line (v_file,
                                     '<country_id>'
                                  || addrs.country
                                  || '</country_id>'
                                 );
               UTL_FILE.put_line (v_file, '<post>' || addrs.POST || '</post>');
               UTL_FILE.put_line (v_file,
                                     '<contact_name>'
                                  || 'NA'
                                  || '</contact_name>'    --addrs.contact_name
                                 );
               UTL_FILE.put_line (v_file,
                                     '<contact_phone>'
                                  || addrs.contact_phone
                                  || '</contact_phone>'                      --
                                 );
               UTL_FILE.put_line (v_file,
                                     '<contact_telex>'
                                  || addrs.contact_telex
                                  || '</contact_telex>'
                                 );
               UTL_FILE.put_line (v_file,
                                     '<contact_fax>'
                                  || addrs.contact_fax
                                  || '</contact_fax>'
                                 );
               UTL_FILE.put_line (v_file,
                                     '<contact_email>'
                                  || addrs.contact_email
                                  || '</contact_email>'
                                 );
--         UTL_FILE.put_line (v_file,
--                               ' <oracle_vendor_site_id>'
--                            || addrs.oracle_vendor_site_id
--                            || '   </oracle_vendor_site_id>'
--                           );
               UTL_FILE.put_line (v_file,
                                     '<oracle_vendor_site_id>'
                                  || addrs.oracle_vendor_site_id_1
                                  || '</oracle_vendor_site_id>'
                                 );
               UTL_FILE.put_line (v_file, '<OracleOUDesc>');

               FOR oudes IN
                  xx_org_details_purchase (main_supplier.vendor_id,
                                           main_supplier.vendor_number,
                                           addrs.add_1,
                                           addrs.add_2
                                          )
               LOOP
                  UTL_FILE.put_line (v_file, '<OUDtlDesc>');
                  UTL_FILE.put_line (v_file,
                                     '<ou_id>' || oudes.ou_id || '</ou_id>'
                                    );
                  UTL_FILE.put_line (v_file,
                                        '<site_id>'
                                     || oudes.oracle_vendor_site_id
                                     || '</site_id>'
                                    );
                  UTL_FILE.put_line (v_file,
                                        '<siteid_status>'
                                     || oudes.siteid_status
                                     || '</siteid_status>'
                                    );
                  UTL_FILE.put_line (v_file, '</OUDtlDesc>');
               END LOOP;

               UTL_FILE.put_line (v_file, '</OracleOUDesc>');
               UTL_FILE.put_line (v_file, '</VendorAddrDesc>');
            END LOOP;
         ELSE
            FOR addrs IN
               xx_adderss_deatils_payment (main_supplier.vendor_id,
                                           main_supplier.vendor_number
                                          )
            LOOP
               UTL_FILE.put_line (v_file, '<VendorAddrDesc>');
               UTL_FILE.put_line (v_file,
                                  '<module>' || addrs.module || '</module>'
                                 );
               UTL_FILE.put_line (v_file,
                                     '<key_value_1>'
                                  || addrs.key_value_1
                                  || '</key_value_1>'
                                 );
               UTL_FILE.put_line (v_file,
                                     '<key_value_2>'
                                  || addrs.key_value_2
                                  || '</key_value_2>'
                                 );
               UTL_FILE.put_line (v_file,
                                  '<seq_no>' || addrs.seq_no || '</seq_no>'
                                 );
               UTL_FILE.put_line (v_file,
                                     '<addr_type>'
                                  || addrs.address_type
                                  || '</addr_type>'
                                 );
               UTL_FILE.put_line (v_file,
                                     '<primary_addr_ind>'
                                  || addrs.primary_addr_ind
                                  || '</primary_addr_ind>'
                                 );
               UTL_FILE.put_line (v_file,
                                     '<add_1>'
                                  || xxabrl_supplier_specialchars (addrs.add_1)
                                  || '</add_1>'
                                 );
               UTL_FILE.put_line (v_file,
                                     '<add_2>'
                                  || xxabrl_supplier_specialchars (addrs.add_2)
                                  || '</add_2>'
                                 );
               UTL_FILE.put_line (v_file,
                                  '<add_3>' || addrs.add_3 || '</add_3>'
                                 );
               UTL_FILE.put_line (v_file, '<city>' || addrs.city || '</city>');
               UTL_FILE.put_line (v_file,
                                  '<state>' || addrs.state || '</state>'
                                 );
               UTL_FILE.put_line (v_file,
                                     '<country_id>'
                                  || addrs.country
                                  || '</country_id>'
                                 );
               UTL_FILE.put_line (v_file, '<post>' || addrs.POST || '</post>');
               UTL_FILE.put_line (v_file,
                                     '<contact_name>'
                                  || 'NA'
                                  || '</contact_name>'    --addrs.contact_name
                                 );
               UTL_FILE.put_line (v_file,
                                     '<contact_phone>'
                                  || addrs.contact_phone
                                  || '</contact_phone>'                      --
                                 );
               UTL_FILE.put_line (v_file,
                                     '<contact_telex>'
                                  || addrs.contact_telex
                                  || '</contact_telex>'
                                 );
               UTL_FILE.put_line (v_file,
                                     '<contact_fax>'
                                  || addrs.contact_fax
                                  || '</contact_fax>'
                                 );
               UTL_FILE.put_line (v_file,
                                     '<contact_email>'
                                  || addrs.contact_email
                                  || '</contact_email>'
                                 );
--         UTL_FILE.put_line (v_file,
--                               ' <oracle_vendor_site_id>'
--                            || addrs.oracle_vendor_site_id
--                            || '   </oracle_vendor_site_id>'
--                           );
               UTL_FILE.put_line (v_file,
                                     '<oracle_vendor_site_id>'
                                  || addrs.oracle_vendor_site_id_1
                                  || '</oracle_vendor_site_id>'
                                 );
               UTL_FILE.put_line (v_file, '<OracleOUDesc>');

               FOR oudes IN
                  xx_org_details_payment (main_supplier.vendor_id,
                                          main_supplier.vendor_number,
                                          addrs.add_1,
                                          addrs.add_2
                                         )
               LOOP
                  UTL_FILE.put_line (v_file, '<OUDtlDesc>');
                  UTL_FILE.put_line (v_file,
                                     '<ou_id>' || oudes.ou_id || '</ou_id>'
                                    );
                  UTL_FILE.put_line (v_file,
                                        '<site_id>'
                                     || oudes.oracle_vendor_site_id
                                     || '</site_id>'
                                    );
                  UTL_FILE.put_line (v_file,
                                        '<siteid_status>'
                                     || oudes.siteid_status
                                     || '</siteid_status>'
                                    );
                  UTL_FILE.put_line (v_file, '</OUDtlDesc>');
               END LOOP;

               UTL_FILE.put_line (v_file, '</OracleOUDesc>');
               UTL_FILE.put_line (v_file, '</VendorAddrDesc>');
            END LOOP;
         END IF;

         UTL_FILE.put_line (v_file, '</VENDORDESC>');
         xx_purchase_count := NULL;
         xx_pur_tax := NULL;
      END LOOP;

      UTL_FILE.fclose (v_file);
   END LOOP;
--   utl_file.put_line(v_file, '</VENDORDESC>');
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      UTL_FILE.fclose (v_file);
      fnd_file.put_line (fnd_file.LOG, 'No Data Found');
   WHEN UTL_FILE.invalid_path
   THEN
      UTL_FILE.fclose (v_file);
      fnd_file.put_line (fnd_file.LOG, 'Invalid Path');
   WHEN UTL_FILE.invalid_filehandle
   THEN
      UTL_FILE.fclose (v_file);
      fnd_file.put_line (fnd_file.LOG, 'Invalid File Handle');
   WHEN UTL_FILE.invalid_operation
   THEN
      UTL_FILE.fclose (v_file);
      fnd_file.put_line (fnd_file.LOG, 'Invalid Operation');
   WHEN UTL_FILE.read_error
   THEN
      UTL_FILE.fclose (v_file);
      fnd_file.put_line (fnd_file.LOG, 'Read Error');
   WHEN UTL_FILE.write_error
   THEN
      UTL_FILE.fclose (v_file);
      fnd_file.put_line (fnd_file.LOG, 'Write Error');
   WHEN UTL_FILE.internal_error
   THEN
      UTL_FILE.fclose (v_file);
      fnd_file.put_line (fnd_file.LOG, 'Internal Error');
   WHEN UTL_FILE.file_open
   THEN
      UTL_FILE.fclose (v_file);
      fnd_file.put_line (fnd_file.LOG, 'File is Open');
   WHEN UTL_FILE.invalid_filename
   THEN
      UTL_FILE.fclose (v_file);
      fnd_file.put_line (fnd_file.LOG, 'Invalid File Name');
   WHEN OTHERS
   THEN
      UTL_FILE.fclose (v_file);
      fnd_file.put_line (fnd_file.LOG, 'Unknown Error' || SQLERRM);
      UTL_FILE.fclose (v_file);
END gs_abrl_unqsupp_one_in_one_xml; 
/


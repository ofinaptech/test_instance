CREATE OR REPLACE PROCEDURE APPS.XXABRL_AR_NAVISION_SCH
(errbuf   OUT VARCHAR2,
 RetCode  OUT NUMBER) AS
 v_org_id NUMBER;
 V_FILE_PATH  VARCHAR2(100);
  v_Print_log     VARCHAR2(1) :='Y';
  v_errmsg varchar2(100);
   stop_program EXCEPTION;
   v_user_id           NUMBER          := fnd_profile.VALUE ('USER_ID');
      v_resp_id           NUMBER          := fnd_profile.VALUE ('RESP_ID');
      v_appl_id           NUMBER         := fnd_profile.VALUE ('RESP_APPL_ID');
  --- v_errmsg  varchar2(100);
v_req_id number ;
  BEGIN
      v_org_id  := Fnd_Profile.VALUE('ORG_ID');
    Fnd_File.PUT_LINE(Fnd_File.LOG,'Running ORG ID: '||v_org_id);
    BEGIN
            mo_global.set_policy_context ('S', V_ORG_ID);
         END;
         -- we will submit this req through all corp ou
         fnd_global.apps_initialize (user_id           => v_user_id,
                                     resp_id           => v_resp_id,
                                     resp_appl_id      => v_appl_id
                                    );
      IF v_org_id IS NOT NULL THEN
    BEGIN
      SELECT    '/ebiz/shared_u01/abrlftp/ftpdir/interface/Navision/AR_Tender_Reco/'
       || 'NAV_AR_'
       || (SELECT meaning
             FROM fnd_lookup_values_vl
            WHERE SYSDATE BETWEEN NVL (start_date_active, SYSDATE)
                              AND NVL (end_date_active, SYSDATE)
              AND enabled_flag = 'Y'
              AND lookup_type = 'NAVISION_AR_BANK_LKP'
              AND lookup_code =
                       (SELECT short_code
                          FROM hr_operating_units
                         WHERE organization_id =v_org_id))
       || '_L_'
       || TO_CHAR (SYSDATE - 1, 'DDMMYY')
       || '.csv'
       INTO V_FILE_PATH
         FROM DUAL;
             EXCEPTION
           WHEN NO_DATA_FOUND THEN
                v_errmsg := 'Org ID/Organization name not found';
                RAISE stop_program;
           END;
        END IF;
      Fnd_File.PUT_LINE(Fnd_File.LOG,'Stating AR_NAVISION..'|| V_FILE_PATH);
        BEGIN
        v_req_id :=   fnd_request.submit_request('XXABRL',
                                             'XXABRL_NAVI_AR_INT_LINE_STG2', --XXABRL Navision AR Invoice Line Loader Program
                                                '',
                                                '',
                                               FALSE,
                                             V_FILE_PATH);
              IF  v_req_id > 0  THEN
              Fnd_File.PUT_LINE(Fnd_File.LOG,V_FILE_PATH||v_req_id);
              else
              v_errmsg := 'Error:in AR-Line Loader Submit';
                 RAISE stop_program;
              END IF;
            
              EXCEPTION
          WHEN stop_program THEN
        -- Print_log(v_errmsg,v_Print_log);
         RetCode :=1;
          WHEN OTHERS THEN
         fnd_file.put_line(FND_FILE.LOG,v_errmsg);
              fnd_file.put_line(FND_FILE.LOG,
                                'Exception in Navision AR Request-Submit: ' || SQLERRM);
                                
               END;                 
                      
               
        fnd_file.put_line (fnd_file.LOG,
                            'step 2 :- Submitting DISTT LOADER...'
                           );                 
                                
                                
         IF v_org_id IS NOT NULL THEN
    BEGIN
      SELECT    '/ebiz/shared_u01/abrlftp/ftpdir/interface/Navision/AR_Tender_Reco/'
       || 'NAV_AR_'
       || (SELECT meaning
             FROM fnd_lookup_values_vl
            WHERE SYSDATE BETWEEN NVL (start_date_active, SYSDATE)
                              AND NVL (end_date_active, SYSDATE)
              AND enabled_flag = 'Y'
              AND lookup_type = 'NAVISION_AR_BANK_LKP'
              AND lookup_code =
                       (SELECT short_code
                          FROM hr_operating_units
                         WHERE organization_id =v_org_id))
       || '_D_'
       || TO_CHAR (SYSDATE - 1, 'DDMMYY')
       || '.csv'
       INTO V_FILE_PATH
         FROM DUAL;
             EXCEPTION
           WHEN NO_DATA_FOUND THEN
                v_errmsg := 'Org ID/Organization name not found';
                RAISE stop_program;
           END;
        END IF;
        
        Fnd_File.PUT_LINE(Fnd_File.LOG,'Stating AR_NAVISION_DIST_LOADER'|| V_FILE_PATH);
        
         BEGIN
        v_req_id :=   fnd_request.submit_request('XXABRL',
                                             'XXABRL_NAVI_AR_INV_DIST_STG2', --XXABRL Navision AR Invoice Line Loader Program
                                                '',
                                                '',
                                               FALSE,
                                             V_FILE_PATH);
              IF  v_req_id > 0  THEN
              Fnd_File.PUT_LINE(Fnd_File.LOG,V_FILE_PATH||v_req_id);
              else
              v_errmsg := 'Error:in AR-DISTT Loader Submit';
                 RAISE stop_program;
              END IF;
            
              EXCEPTION
          WHEN stop_program THEN
        -- Print_log(v_errmsg,v_Print_log);
         RetCode :=1;
          WHEN OTHERS THEN
         fnd_file.put_line(FND_FILE.LOG,v_errmsg);
              fnd_file.put_line(FND_FILE.LOG,
                                'Exception in Navision AR  DISTT Request-Submit: ' || SQLERRM);
                                
               END;   
               
               
               
                fnd_file.put_line (fnd_file.LOG,
                            'step 2 :- Submitting Receipt Loader...'
                           );                 
                                
                                
         IF v_org_id IS NOT NULL THEN
    BEGIN
      SELECT    '/ebiz/shared_u01/abrlftp/ftpdir/interface/Navision/AR_Tender_Reco/'
       || 'NAV_AR_'
       || (SELECT meaning
             FROM fnd_lookup_values_vl
            WHERE SYSDATE BETWEEN NVL (start_date_active, SYSDATE)
                              AND NVL (end_date_active, SYSDATE)
              AND enabled_flag = 'Y'
              AND lookup_type = 'NAVISION_AR_BANK_LKP'
              AND lookup_code =
                       (SELECT short_code
                          FROM hr_operating_units
                         WHERE organization_id =v_org_id))
       || '_T_'
       || TO_CHAR (SYSDATE - 1, 'DDMMYY')
       || '.csv'
       INTO V_FILE_PATH
         FROM DUAL;
             EXCEPTION
           WHEN NO_DATA_FOUND THEN
                v_errmsg := 'Org ID/Organization name not found';
                RAISE stop_program;
           END;
        END IF;
        
        Fnd_File.PUT_LINE(Fnd_File.LOG,'Stating AR_NAVISION_RECEIPT_LOADER'|| V_FILE_PATH);
        
         BEGIN
        v_req_id :=   fnd_request.submit_request('XXABRL',
                                             'XXABRL_NAVI_AR_RECEIPT_STG2', --XXABRL Navision AR Invoice Line Loader Program
                                                '',
                                                '',
                                               FALSE,
                                             V_FILE_PATH);
              IF  v_req_id > 0  THEN
              Fnd_File.PUT_LINE(Fnd_File.LOG,V_FILE_PATH||v_req_id);
              else
              v_errmsg := 'Error:in AR-RECEIPT Loader Submit';
                 RAISE stop_program;
              END IF;
            
              EXCEPTION
          WHEN stop_program THEN
        -- Print_log(v_errmsg,v_Print_log);
         RetCode :=1;
          WHEN OTHERS THEN
         fnd_file.put_line(FND_FILE.LOG,v_errmsg);
              fnd_file.put_line(FND_FILE.LOG,
                                'Exception in Navision AR  RECEIPT Request-Submit: ' || SQLERRM);
                                
               END;                      
                                
                                                           
   END XXABRL_AR_NAVISION_SCH; 
/


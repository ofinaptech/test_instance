CREATE OR REPLACE PROCEDURE APPS.xxabrl_p2p_scheduler (
   errbuf        OUT      VARCHAR2,
   retcode       OUT      NUMBER,
   x_from_date   IN       VARCHAR2 DEFAULT NULL,
   x_to_date     IN       VARCHAR2 DEFAULT NULL
)
IS
   v_req_id        NUMBER;
   le_load         EXCEPTION;
   lb_wait         BOOLEAN;
   lc_phase        VARCHAR2 (500);
   lc_status       VARCHAR2 (500);
   lc_dev_phase    VARCHAR2 (500);
   lc_dev_status   VARCHAR2 (500);
   lc_message      VARCHAR2 (2000);
   lc_error_flag   VARCHAR2 (1);
   gn_error        NUMBER          := 2;
   l_layout        BOOLEAN;

--   p_from_date     DATE
--               := NVL (fnd_date.canonical_to_date (x_from_date), '01-Apr-12');
--   p_to_date       DATE
--                     := NVL (fnd_date.canonical_to_date (x_to_date), SYSDATE);
   CURSOR xx_rec
   IS
      SELECT *
        FROM apps.hr_operating_units
       WHERE organization_id NOT IN
                (663, 801, 821, 841, 861, 881, 901, 921, 941, 961, 981, 84,
                 86, 87, 89, 91, 93, 94, 95, 96, 1341, 1241, 1261, 1281, 90,
                 92, 83)
         AND NAME NOT LIKE '%TSRL%'
         AND date_to IS NULL;
-- AND organization_id IN (1524, 1525, 741);
BEGIN
   fnd_file.put_line (fnd_file.LOG, 'Procure to Pay Program started');

   FOR xx_org IN xx_rec
   LOOP
      fnd_file.put_line (fnd_file.LOG,
                            'Procure to Pay Program started for org of :'
                         || xx_org.organization_id
                        );
     l_layout :=
         fnd_request.add_layout('XXABRL','XXABRLMAY20T2','en','US', 'EXCEL');
         
        
      v_req_id :=
         fnd_request.submit_request ('XXABRL',
                                     'XXABRLMAY20T2',
                                     '',
                                     '',
                                     FALSE,
                                     xx_org.organization_id,
                                     NULL,
                                     NULL,
                                     NULL,
                                     x_from_date,
                                     x_to_date,
                                     NULL,
                                     NULL,
                                     NULL,
                                     NULL,
                                     NULL,
                                     NULL,
                                     NULL,
                                     NULL,
                                     NULL
                                    );
      COMMIT;

      IF (v_req_id = 0)
      THEN
         RAISE le_load;
      ELSE
         fnd_file.put_line (fnd_file.LOG,
                               'Procure to Pay Program  with request id :'
                            || TO_CHAR (v_req_id)
                           );
      END IF;

   -- +===================================================================+
-- Waiting for the conccurrent request to get completed
-- +===================================================================+
      lb_wait :=
         fnd_concurrent.wait_for_request (request_id      => v_req_id,
                                          INTERVAL        => 10,
                                          max_wait        => 0,
                                          phase           => lc_phase,
                                          status          => lc_status,
                                          dev_phase       => lc_dev_phase,
                                          dev_status      => lc_dev_status,
                                          MESSAGE         => lc_message
                                         );
      -- Do not remove this commit, concurrent program will not start without this
      COMMIT;

    -- +===================================================================+
-- Checking  item import Program status
-- +===================================================================+
      IF    (    (   UPPER (lc_dev_status) = 'WARNING'
                  OR UPPER (lc_dev_status) = 'ERROR'
                 )
             AND UPPER (lc_dev_phase) = 'COMPLETE'
            )
         OR (    (UPPER (lc_status) = 'WARNING' OR UPPER (lc_status) = 'ERROR'
                 )
             AND UPPER (lc_phase) = 'COMPLETED'
            )
      THEN
         RAISE le_load;
      END IF;
   END LOOP;
EXCEPTION
   WHEN le_load
   THEN
      retcode := gn_error;
      errbuf := 'Import Items Program completed with error';
   WHEN OTHERS
   THEN
      fnd_file.put_line (fnd_file.LOG,
                         'error code 2: ' || SQLCODE || ':' || SQLERRM
                        );
      ROLLBACK;
END; 
/


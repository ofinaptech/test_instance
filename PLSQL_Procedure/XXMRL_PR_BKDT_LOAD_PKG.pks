CREATE OR REPLACE PACKAGE APPS.xxmrl_pr_bkdt_load_pkg
IS
   PROCEDURE xxmrl_wait_for_req_prc1 (p_request_id   IN NUMBER,
                                      p_req_name     IN VARCHAR2);

   PROCEDURE xxmrl_po_req_load_prc (errbuf OUT VARCHAR2, retcode OUT NUMBER);
END xxmrl_pr_bkdt_load_pkg; 
/


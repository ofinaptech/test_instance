CREATE OR REPLACE PROCEDURE APPS.xxabrl_dbs_outbound_utl (
   errbuf    OUT   VARCHAR2,
   retcode   OUT   VARCHAR2
)
AS
   /**********************************************************************************************************************************************
                          WIPRO Infotech Ltd, Mumbai, India
              Name: Supplier Bank Outbound interface for DBS Bank generating file on FTP [H2H Interface betwen ABRL and DBS Bank]
              Change Record:
             =========================================================================================================================
             Version   Date          Author                    Remarks                  Documnet Ref
             =======   ==========   =============             ==================  =====================================================
             1.0.0     09-Jul-2012   Amresh Kumar Chutke      Initial Version
             1.0.1     19-Jul-2012   Amresh Kumar Chutke      Changed cursor logic to pickup transactions which are created 3 days prior
             1.0.2     23-Jul-2012   Amresh Kumar Chutke      Changed Beneficiary/Receiver Name as DBS is unable to pickup  more than 49 characters
             1.0.3     05-Nov-2012   Amresh Kumar Chutke      Added new Bank account for TSRL Payments and Advice Curosr testing
             1.0.4     08-Nov-2012   Amresh Kumar Chutke      Added comma between check_id and check_number to the Payment File to ease the reconciliation process at user end.
                                                              Added Beneficiary Name and Sender Account Number to the Advice file
                                                              Finance Requirement as per Hemant/Rupesh mails
             1.0.5     11-Dec-2012   Amresh Kumar Chutke      TSRL Payments File Generation GO-LIVE
             1.0.6     01-Dec-2015   Dhiresh More             Make changes in payment File Generation - for IDEAL3
    ************************************************************************************************************************************************/

   --Declaring Payment Detail Local Variables
   lv_abrl_pay_count             NUMBER             := 0;
   --initializing counter to 0
   lv_abrl_pay_amount            NUMBER             := 0;
   --initializing amount to 0
   lv_tsrl_pay_count             NUMBER             := 0;
   --initializing counter to 0
   lv_tsrl_pay_amount            NUMBER             := 0;
   --initializing amount to 0
   l_file_p                      UTL_FILE.file_type;
   l_file_p1                     UTL_FILE.file_type;
   l_pmt_data                    VARCHAR2 (4000);
   l_pmt_data1                   VARCHAR2 (4000);
   v_dbs_neft_seq                VARCHAR2 (50);
   v_dbs_neft_seq1               VARCHAR2 (50);
   v_dbs_neft_tsrl_seq           VARCHAR2 (50);
   v_dbs_neft_tsrl_seq1          VARCHAR2 (50);
   v_dbs_pay_seq                 VARCHAR2 (50);
--Declaring Advice Detail Local Variables
   lv_abrl_adv_count             NUMBER             := 0;
   --initializing counter to 0
   lv_tsrl_adv_count             NUMBER             := 0;
   --initializing counter to 0
   lv_corporate_account_number   NUMBER             := 0;
   l_file_a                      UTL_FILE.file_type;
   l_adv_data                    VARCHAR2 (4000);
   v_dbs_advice_seq              VARCHAR2 (50);
   v_dbs_advice_tsrl_seq         VARCHAR2 (50);

-- Declaring Payment Cursor to pick up only Authorized Payments
   CURSOR cur_dbs_neft (p_acc_no IN VARCHAR2)
   IS
      SELECT   *
          FROM apps.dbs_payment_table
         WHERE transaction_status = 'AUTHORIZED'
           AND TRUNC (creation_date) >= TRUNC (SYSDATE) - 3
           -- this will also pick up transactions which were created 3 days prior
           AND product_code = 'N'
           AND corporate_account_number NOT IN ('811200088576')
           AND corporate_account_number = p_acc_no
--           AND pay_doc_number IN
--                  ('300001240', '417186', '417376', '417369', '300000912',
--                   '417165', '417208', '417199', '417196', '417162', '417212',
--                   '417210', '417102', '417206', '417163', '200001158',
--                   '417002', '417157', '417158', '417001', '416999',
--                   '200001196', '200001164', '200000997', '416986', '417009',
--                   '416984', '200000857', '200001058', '200001188', '416952',
--                   '416947', '416945', '416973', '416975', '416769', '416805',
--                   '416791', '416724', '416789', '416726', '416734', '416795',
--                   '206534', '206543', '206454', '206533', '206416', '206476',
--                   '206460', '206389', '206367', '206340', '206346', '206243',
--                   '206352', '206244', '417391', '417395', '417396', '417397',
--                   '417398', '417399', '417400', '417403', '417404', '417409',
--                   '417410', '417413', '417434', '417435', '417436', '417437',
--                   '417438', '417439', '417440', '417441', '417442', '417443',
--                   '417444', '417445', '417446', '417447', '417448', '417449',
--                   '417450', '417451', '417452', '417454', '417455', '417458',
--                   '417460', '417461', '417462', '417464', '417467', '417469',
--                   '417470', '417473', '417474', '417475', '206589', '417476',
--                   '417477', '206591', '417478', '206592', '206593', '417480',
--                   '417481', '206595', '206596', '206597', '206598', '417483',
--                   '206599', '417484', '206600', '417485', '206601', '417486',
--                   '417487', '417488', '5011830', '417489', '5011831',
--                   '417490', '417491', '200001217', '417493', '206602',
--                   '206603', '400002501', '417494', '206604', '206605',
--                   '417496', '400002502', '417497', '206606', '400002503',
--                   '400002504', '206607', '400002505', '206608', '400002506',
--                   '400002507', '206609', '400002508', '417498', '206610',
--                   '400002509', '206611', '400002510', '400002511',
--                   '400002512', '400002513', '5011832', '400002514', '417499',
--                   '400002515', '206612', '400002516', '206613', '400002517',
--                   '400002518', '400002519', '400002520', '400002521',
--                   '206614', '5011833', '400002522', '400002523', '206615',
--                   '5011834', '400002524', '417503', '400002525', '5011835',
--                   '206616', '417504', '5011836', '206617', '417505',
--                   '5011837', '206618', '206619', '5011838', '206620',
--                   '5011839', '206621', '206622', '417506', '206623',
--                   '5011840', '5011841', '417510', '417511', '417512',
--                   '417513', '417514', '417516', '417517', '206624', '417519',
--                   '417520', '417522', '417523', '417524', '417525',
--                   '5011843', '5011844', '5011845', '5011846', '417527',
--                   '417528', '417529', '417530', '206625', '417531',
--                   '5011847', '417532', '417534', '417535', '417537',
--                   '206626', '417538', '417539', '206627', '206628', '417542',
--                   '206629', '206630', '417546', '417549', '417550', '417555',
--                   '417557', '417558', '417560', '417563', '206631', '417566',
--                   '417567', '206632', '417568', '206633', '417569', '417570',
--                   '206634', '417571', '206635', '417572', '206636', '417573',
--                   '417574', '417575', '417576', '5011849', '417577',
--                   '417578', '417579', '417580', '417581', '417582', '417583',
--                   '417584', '417585', '417586', '417587', '417588', '417589',
--                   '417590', '417591', '417592', '417593', '5011850',
--                   '206637', '206644', '206641', '206638', '206642', '206640',
--                   '206643', '206645', '206648', '206646', '206639', '206649',
--                   '206647', '206650', '206651', '206652', '206653', '206654',
--                   '206655', '206656', '206657', '206658', '206659', '206660',
--                   '206661')
      ORDER BY check_id, transaction_value_date;

--Declaring Advice Cursor to pick up invoices which are made against payments
   CURSOR cur_dbs_adv (p_acc_no VARCHAR2)                        --CUR_DBS_ADV
   IS
      SELECT   dnpi.*, dpt.vendor_code, dpt.third_party_id, dpt.primary_name,
               dpt.corporate_account_number,dpt.PRIMARY_EMAIL
          FROM apps.dbs_payment_invoice dnpi, apps.dbs_payment_table dpt
         WHERE dnpi.check_id = dpt.check_id
           AND TRUNC (creation_date) >= TRUNC (SYSDATE) - 3
           -- this will also pick up transactions which were created 2 days prior
           AND dpt.transaction_status = 'AUTHORIZED'
           AND dpt.product_code = 'N'
           AND dpt.corporate_account_number NOT IN ('811200088576')
           AND dpt.corporate_account_number = p_acc_no
      ORDER BY dnpi.check_id;

   CURSOR cur_adv_pay_acc_no
   IS
      SELECT   dpt.corporate_account_number
          FROM apps.dbs_payment_invoice dnpi, apps.dbs_payment_table dpt
         WHERE dnpi.check_id = dpt.check_id
           AND TRUNC (creation_date) >= TRUNC (SYSDATE) - 3
           -- this will also pick up transactions which were created 2 days prior
           AND dpt.transaction_status = 'AUTHORIZED'
           AND dpt.product_code = 'N'
           AND dpt.corporate_account_number NOT IN ('811200088576')
      GROUP BY dpt.corporate_account_number;

--TSRL Payments and Advice Curosr -- 05 Nov 2012 testing
-- Declaring TSRL Payment Cursor to pick up only Authorized Payments
   CURSOR cur_dbs_tsrl_neft
   IS
      SELECT   *
          FROM apps.dbs_payment_table
         WHERE transaction_status = 'AUTHORIZED'
           AND TRUNC (creation_date) >= TRUNC (SYSDATE) - 3
           -- this will also pick up transactions which were created 2 days prior
           AND product_code = 'N'
           AND corporate_account_number = '811200088576'
      ORDER BY check_id, transaction_value_date;

--Declaring TSRL Advice Cursor to pick up invoices which are made against payments
   CURSOR cur_dbs_tsrl_adv
   IS
      SELECT   dnpi.*, dpt.vendor_code, dpt.third_party_id, dpt.primary_name,
               dpt.corporate_account_number
          FROM apps.dbs_payment_invoice dnpi, apps.dbs_payment_table dpt
         WHERE dnpi.check_id = dpt.check_id
           AND TRUNC (creation_date) >= TRUNC (SYSDATE) - 3
           -- this will also pick up transactions which were created 2 days prior
           AND dpt.transaction_status = 'AUTHORIZED'
           AND dpt.product_code = 'N'
           AND dpt.corporate_account_number = '811200088576'
      ORDER BY dnpi.check_id;

-- End of TSRL Payments and Advice Curosr -- 05 Nov 2012 testing
   CURSOR trx_amount
   IS
      SELECT   COUNT (*) pay_count,
               SUM (transactional_amount) transactional_amount,
               corporate_account_number
--     INTO lv_abrl_pay_count, lv_abrl_pay_amount
      FROM     apps.dbs_payment_table
         WHERE 1 = 1
           AND transaction_status = 'AUTHORIZED'
           AND TRUNC (creation_date) >= TRUNC (SYSDATE) - 3
           -- this will also pick up transactions which were created 2 days prior
           AND product_code = 'N'
           AND corporate_account_number NOT IN ('811200088576')
      GROUP BY corporate_account_number;
BEGIN
   SELECT COUNT (*), SUM (transactional_amount)
     INTO lv_abrl_pay_count, lv_abrl_pay_amount
     FROM apps.dbs_payment_table
    WHERE 1 = 1
      AND transaction_status = 'AUTHORIZED'
      AND TRUNC (creation_date) >= TRUNC (SYSDATE) - 3
      -- this will also pick up transactions which were created 2 days prior
      AND product_code = 'N'
      AND corporate_account_number NOT IN ('811200088576');

   SELECT COUNT (*), SUM (transactional_amount)
     INTO lv_tsrl_pay_count, lv_tsrl_pay_amount
     FROM apps.dbs_payment_table
    WHERE 1 = 1
      AND transaction_status = 'AUTHORIZED'
      AND TRUNC (creation_date) >= TRUNC (SYSDATE) - 3
      -- this will also pick up transactions which were created 2 days prior
      AND product_code = 'N'
      AND corporate_account_number IN ('811200088576');

   -- Generating a Unique Sequence Number to be concatenated in the file name
   SELECT dbs_neft_seq1.NEXTVAL || 'x' || TO_CHAR (SYSDATE, 'ddmmyyyy')
     INTO v_dbs_neft_seq1
     FROM DUAL;

   --TSRL-- Generating a Unique Sequence Number to be concatenated in TSRL file name
   SELECT dbs_neft_tsrl_seq.NEXTVAL || 'x' || TO_CHAR (SYSDATE, 'ddmmyyyy')
     INTO v_dbs_neft_tsrl_seq
     FROM DUAL;

   --TSRL-- Generating a Unique Sequence Number to be concatenated in TSRL file name
   SELECT dbs_neft_tsrl_seq1.NEXTVAL || 'x' || TO_CHAR (SYSDATE, 'ddmmyyyy')
     INTO v_dbs_neft_tsrl_seq1
     FROM DUAL;

   -- Payment File Generation
   IF lv_abrl_pay_count > 0
   --AND LV_CORPORATE_ACCOUNT_NUMBER NOT IN ('811200088576')
   THEN
      FOR xx_main IN trx_amount
      LOOP
         fnd_file.put_line (fnd_file.LOG,
                               'corporate_account_number=>'
                            || xx_main.corporate_account_number
                           );

-------------  con------------
 -- Generating a Unique Sequence Number to be concatenated in the file name
         SELECT    REVERSE (SUBSTR (REVERSE (xx_main.corporate_account_number),
                                    1,
                                    4
                                   )
                           )
                || '.'
                || dbs_neft_seq.NEXTVAL
                || 'x'
                || TO_CHAR (SYSDATE, 'ddmmyyyy')
           INTO v_dbs_neft_seq
           FROM DUAL;

         SELECT xxabrl_dbs_pay_seq.NEXTVAL
           INTO v_dbs_pay_seq
           FROM DUAL;

         --Creating a file for the below generated payment data
         l_file_p :=
            UTL_FILE.fopen ('DBSBANK',
                               'IDEAL3.FORMAT362.INADRE01.INADRE01.'
                            || v_dbs_neft_seq
--                            || '-'
                            || '.TXT',
                            'W'
                           );
         UTL_FILE.put_line (l_file_p,
                               'HEADER'
                            || ','
                            || TO_CHAR (SYSDATE, 'ddmmyyyy')
                            || ','
                            || 'INADRE01'
                            || ','
                            || 'ADITYA BIRLA RETAIL LIMITED'
                           );         ----  changes made by Dhiresh on 16Nov15
         l_file_p1 :=
            UTL_FILE.fopen ('DBSBANK',
                            'INDBSI02XXXX.' || v_dbs_neft_seq || '.TXT',
                            'W'
                           );
         UTL_FILE.put_line (l_file_p1,
                               'HEADER'
                            || ','
                            || TO_CHAR (SYSDATE, 'ddmmyyyy')
                            || ','
                            || 'INADRE01'
                            || ','
                            || 'ADITYA BIRLA RETAIL LIMITED'
                           );         ----  changes made by Dhiresh on 16Nov15

         FOR rec_dbs_neft IN cur_dbs_neft (xx_main.corporate_account_number)
         LOOP
            l_pmt_data :=
                  'PAYMENT'                         -- Orginating Branch Code
               || ','
               || 'BPY'
               || ','
               || rec_dbs_neft.corporate_account_number    --Sender account no
               || ','
               || 'INR'
               || ','
               || NULL                                               -- Filler
               || ','
               || 'INR'
               || ','
--                  || null -- Filler
--                  || ','
               || v_dbs_pay_seq                            -- PAYMENT BATCH ID
               || ','
               || TO_CHAR (rec_dbs_neft.transaction_value_date, 'ddmmyyyy')
               --Payment date
               || ','
               || NULL                                               -- Filler
               || ','
               || NULL                                               -- Filler
               || ','
               || REPLACE
                     (REPLACE
                         (REPLACE
                             (REPLACE
                                 (REPLACE
                                     (REPLACE
                                         (REPLACE
                                             (REPLACE
                                                 (REPLACE
                                                     (REPLACE
                                                         (REPLACE
                                                             (REPLACE
                                                                 (REPLACE
                                                                     (REPLACE
                                                                         (REPLACE
                                                                             (REPLACE
                                                                                 (SUBSTR
                                                                                     (rec_dbs_neft.primary_name,
                                                                                      0,
                                                                                      35
                                                                                     ),
                                                                                  '&',
                                                                                  ''
                                                                                 ),
                                                                              '!',
                                                                              ''
                                                                             ),
                                                                          '@',
                                                                          ''
                                                                         ),
                                                                      '#',
                                                                      ''
                                                                     ),
                                                                  '$',
                                                                  ''
                                                                 ),
                                                              '%',
                                                              ''
                                                             ),
                                                          '^',
                                                          ''
                                                         ),
                                                      '*',
                                                      ''
                                                     ),
                                                  '=',
                                                  ''
                                                 ),
                                              '{',
                                              ''
                                             ),
                                          '}',
                                          ''
                                         ),
                                      '[',
                                      ''
                                     ),
                                  ']',
                                  ''
                                 ),
                              '|',
                              ''
                             ),
                          '\',
                          ''
                         ),
                      ',',
                      ''
                     )
               --Receiver name --Changed as DBS is unable to pickup  more than 50 characters
               || ','
               || NULL                                               -- Filler
               || ','
               || NULL                                               -- Filler
               || ','
               || NULL                                               -- Filler
               || ','
               || 'Bene Code: '
               || rec_dbs_neft.vendor_code
               --null -- Filler -- As per Bala Sir/Rupesh mail dated July 12, 2012 6:48 PM
               || ','
               || rec_dbs_neft.benefi_acc_num            --Receiver account no
               || ','
               || NULL                                               -- Filler
               || ','
               || NULL                                               -- Filler
               || ','
               || NULL                                               -- Filler
               || ','
               || rec_dbs_neft.benefi_ifsc_code                    --IFSC Code
               || ','
               || NULL                                               -- Filler
               || ','
               || NULL                                               -- Filler
               || ','
               || NULL                                               -- Filler
               || ','
               || NULL                                               -- Filler
               || ','
               || NULL                                               -- Filler
               || ','
               || NULL                                               -- Filler
               || ','
               || NULL                                               -- Filler
               || ','
               || TRIM (TO_CHAR (rec_dbs_neft.transactional_amount,
                                 '99999999.99'
                                )
                       )
               --Payment Amount
               || ','
               || NULL                                               -- Filler
               || ','
               || NULL                                               -- Filler
               || ','
               || NULL                                               -- Filler
               || ','
               || NULL                                               -- Filler
               || ','
               || '20'                  -- Transaction code for Vendor Payment
               || ','
               || NULL                                               -- Filler
               || ','
               || rec_dbs_neft.pay_doc_number
               || ','
               || rec_dbs_neft.check_id
               || ','
               -- Payment Details -- Finance Requirement as per Hemant/Rupesh mails
               || NULL                                               -- Filler
               || ','
               || NULL                                               -- Filler
               || ','
               || NULL                                               -- Filler
               || ','
               || NULL                                               -- Filler
               || ','
               || NULL                                               -- Filler
               || ','
               || NULL                                               -- Filler
               || ','
               || NULL                                               -- Filler
               || ','
               || NULL                                               -- Filler
               || ','
               || NULL
               --'E'                                --Delivery Mode as E-Mail
               || ','
               || NULL                                               -- Filler
               || ','
               || NULL                                               -- Filler
               || ','
               || REPLACE
                     (REPLACE
                         (REPLACE
                             (REPLACE
                                 (REPLACE
                                     (REPLACE
                                         (REPLACE
                                             (REPLACE
                                                 (REPLACE
                                                     (REPLACE
                                                         (REPLACE
                                                             (REPLACE
                                                                 (REPLACE
                                                                     (REPLACE
                                                                         (REPLACE
                                                                             (REPLACE
                                                                                 (SUBSTR
                                                                                     (rec_dbs_neft.primary_name,
                                                                                      0,
                                                                                      35
                                                                                     ),
                                                                                  '&',
                                                                                  ''
                                                                                 ),
                                                                              '!',
                                                                              ''
                                                                             ),
                                                                          '@',
                                                                          ''
                                                                         ),
                                                                      '#',
                                                                      ''
                                                                     ),
                                                                  '$',
                                                                  ''
                                                                 ),
                                                              '%',
                                                              ''
                                                             ),
                                                          '^',
                                                          ''
                                                         ),
                                                      '*',
                                                      ''
                                                     ),
                                                  '=',
                                                  ''
                                                 ),
                                              '{',
                                              ''
                                             ),
                                          '}',
                                          ''
                                         ),
                                      '[',
                                      ''
                                     ),
                                  ']',
                                  ''
                                 ),
                              '|',
                              ''
                             ),
                          '\',
                          ''
                         ),
                      ',',
                      ''
                     )
               --Receiver name --Changed as DBS is unable to pickup  more than 50 characters
               || ','
               || NULL                                       -- Address Line 1
               || ','
               || NULL                                        --Address Line 2
               || ','
               || NULL                                        --Address Line 3
               || ','
               || NULL                                               -- Filler
               || ','
               || NULL                                               -- Filler
               || ','
               || NULL                                 --'idealmumbai@dbs.com'
               --Email -- Only one email address as discussed with Rupesh/Hemanth
               || ','
               || NULL                                               -- Filler
               || ','
               || NULL                                               -- Filler
               || ','
               || NULL                                               -- Filler
               || ','
               || NULL                                               -- Filler
               || ','
               || NULL                                               -- Filler
               || ','
               || NULL                                               -- Filler
               || ','
               || NULL                                               -- Filler
               || ','
               || NULL                                               -- Filler
               || ','
               || NULL                                               -- Filler
               || ','
               || 'EmailS:'
               || REPLACE (rec_dbs_neft.primary_email, ',', ';')
--PRIMARY_EMAIL -- As discussed with Rupesh on 11MAY2012     -- Advice Details of Transaction
               || ':EmailE';
--
--                  || 'Bene Code: '||REC_DBS_NEFT.VENDOR_CODE--null -- Filler -- As per Bala Sir/Rupesh mail dated July 12, 2012 6:48 PM
--                  || '^'
--                  || null -- Filler
--                  || '^'
--                  || NULL --REC_DBS_NEFT.REFERENCE_DATE -- Reference date --blank
--                  || '^'
--                  || null --REC_DBS_NEFT.CHECK_ID --blank as discussed with Hemant on 14MAY12 --Reference transaction no --blank  [REPLACE IT WITH CHECK_ID AS THERE IS NO REFERENCE TO PAYMENT ADVICE]
--                  || '^'
--                  || REC_DBS_NEFT.ATTRIBUTE1 --Acknowledgement status -- blank
--                  || '^'
--                  || REC_DBS_NEFT.ATTRIBUTE2 --Rejection code -- blank
--                  || '^'
--                  ||','
--                  || SUBSTR(REC_DBS_NEFT.PRIMARY_NAME,0,49) --Advice name --Changed as DBS is unable to pickup  more than 50 characters
--                  || '^'
--                  || 'Bene Code: '||REC_DBS_NEFT.VENDOR_CODE --SUBSTR(REC_DBS_NEFT.VENDOR_ADDRESS,100,49) --Address Line 3 As per Bala Sir/Rupesh mail dated July 12, 2012 6:48 PM
--                  || '^'
--                  || 'E' --Delivery Mode --For Email Value= E
--                  || '^'
--                  || 'idealmumbai@dbs.com' --Email -- Only one email address as discussed with Rupesh/Hemanth
--                  || '^'
--                  || NULL --Fax -- blank
--                  || '^'
            UTL_FILE.put_line (l_file_p, l_pmt_data);
            UTL_FILE.put_line (l_file_p1, l_pmt_data);
         END LOOP;

         UTL_FILE.put_line (l_file_p,
                               'TRAILER'
                            || ','
                            || xx_main.pay_count
                            || ','
                            || TRIM (TO_CHAR (xx_main.transactional_amount,
                                              '99999999999999.99'
                                             )
                                    )
                           );
         UTL_FILE.put_line (l_file_p1,
                               'TRAILER'
                            || ','
                            || xx_main.pay_count
                            || ','
                            || TRIM (TO_CHAR (xx_main.transactional_amount,
                                              '99999999999999.99'
                                             )
                                    )
                           );         ----  changes made by Dhiresh on 16Nov15
         UTL_FILE.fclose (l_file_p);
         UTL_FILE.fclose (l_file_p1);
      END LOOP;

      fnd_file.put_line (fnd_file.LOG, 'End ');
   END IF;

   ---------------end con--------------

   ---------------------------------------------------------------------------------
-- Payment File Generation only for DBS Reconciliation with a different file name
  -- IF lv_abrl_pay_count > 0
   --AND LV_CORPORATE_ACCOUNT_NUMBER NOT IN ('811200088576')
   --THEN
------------CON-------------------
--      FOR xx_main IN trx_amount
--      LOOP
--         SELECT    REVERSE (SUBSTR (REVERSE (xx_main.corporate_account_number),
--                                    1,
--                                    4
--                                   )
--                           )
--                || '.'
--                || dbs_neft_seq.curval
--                || 'x'
--                || TO_CHAR (SYSDATE, 'ddmmyyyy')
--           INTO v_dbs_neft_seq
--           FROM DUAL;

   --         --Creating a file for the below generated payment data
--         l_file_p1 :=
--            UTL_FILE.fopen ('DBS_BANK',
--                               'INDBSI02XXXX.'
--                            || v_dbs_neft_seq
--                            || '-'
--                             || '.TXT',
--                            'W'
--                           );
--         UTL_FILE.put_line (l_file_p1,
--                               'HEADER'
--                            || ','
--                            || TO_CHAR (SYSDATE, 'ddmmyyyy')
--                            || ','
--                            || 'INADRE01'
--                            || ','
--                            || 'ADITYA BIRLA RETAIL LIMITED'
--                           );         ----  changes made by Dhiresh on 16Nov15

   --         FOR rec_dbs_neft1 IN cur_dbs_neft (xx_main.corporate_account_number)
--         LOOP
--            l_pmt_data1 :=
--                  'PAYMENT'                         -- Orginating Branch Code
--               || ','
--               || 'BPY'
--               || ','
--               || rec_dbs_neft1.corporate_account_number   --Sender account no
--               || ','
--               || 'INR'
--               || ','
--               || NULL                                               -- Filler
--               || ','
--               || 'INR'
--               || ','
----                  || null -- Filler
----                  || ','
--               || v_dbs_pay_seq                            -- PAYMENT BATCH ID
--               || ','
--               || TO_CHAR (rec_dbs_neft1.transaction_value_date, 'ddmmyyyy')
--               --Payment date
--               || ','
--               || NULL                                               -- Filler
--               || ','
--               || NULL                                               -- Filler
--               || ','
--               || SUBSTR (rec_dbs_neft1.primary_name, 0, 49)
--               --Receiver name --Changed as DBS is unable to pickup  more than 50 characters
--               || ','
--               || NULL                                               -- Filler
--               || ','
--               || NULL                                               -- Filler
--               || ','
--               || NULL                                               -- Filler
--               || ','
--               || 'Bene Code: '
--               || rec_dbs_neft1.vendor_code
--               --null -- Filler -- As per Bala Sir/Rupesh mail dated July 12, 2012 6:48 PM
--               || ','
--               || rec_dbs_neft1.benefi_acc_num           --Receiver account no
--               || ','
--               || NULL                                               -- Filler
--               || ','
--               || NULL                                               -- Filler
--               || ','
--               || NULL                                               -- Filler
--               || ','
--               || rec_dbs_neft1.benefi_ifsc_code                   --IFSC Code
--               || ','
--               || NULL                                               -- Filler
--               || ','
--               || NULL                                               -- Filler
--               || ','
--               || NULL                                               -- Filler
--               || ','
--               || NULL                                               -- Filler
--               || ','
--               || NULL                                               -- Filler
--               || ','
--               || NULL                                               -- Filler
--               || ','
--               || NULL                                               -- Filler
--               || ','
--               || TO_CHAR (rec_dbs_neft1.transactional_amount, '9999999.99')
--               --Payment Amount
--               || ','
--               || NULL                                               -- Filler
--               || ','
--               || NULL                                               -- Filler
--               || ','
--               || NULL                                               -- Filler
--               || ','
--               || NULL                                               -- Filler
--               || ','
--               || '20'                  -- Transaction code for Vendor Payment
--               || ','
--               || NULL                                               -- Filler
--               || ','
--               || rec_dbs_neft1.pay_doc_number
--               || ','
--               || rec_dbs_neft1.check_id
--               -- Payment Details -- Finance Requirement as per Hemant/Rupesh mails
--               || ','
--               || NULL                                               -- Filler
--               || ','
--               || NULL                                               -- Filler
--               || ','
--               || NULL                                               -- Filler
--               || ','
--               || NULL                                               -- Filler
--               || ','
--               || NULL                                               -- Filler
--               || ','
--               || NULL                                               -- Filler
--               || ','
--               || NULL                                               -- Filler
--               || ','
--               || NULL                                               -- Filler
--               || ','
--               || 'E'                                --Delivery Mode as E-Mail
--               || ','
--               || NULL                                               -- Filler
--               || ','
--               || NULL                                               -- Filler
--               || ','
--               || SUBSTR (rec_dbs_neft1.primary_name, 0, 49)
--               --Receiver name --Changed as DBS is unable to pickup  more than 50 characters
--               || ','
--               || NULL                                       -- Address Line 1
--               || ','
--               || NULL                                        --Address Line 2
--               || ','
--               || NULL                                        --Address Line 3
--               || ','
--               || NULL                                               -- Filler
--               || ','
--               || NULL                                               -- Filler
--               || ','
--               || 'idealmumbai@dbs.com'
--               --Email -- Only one email address as discussed with Rupesh/Hemanth
--               || ','
--               || NULL                                               -- Filler
--               || ','
--               || NULL                                               -- Filler
--               || ','
--               || NULL                                               -- Filler
--               || ','
--               || NULL                                               -- Filler
--               || ','
--               || NULL                                               -- Filler
--               || ','
--               || NULL                                               -- Filler
--               || ','
--               || NULL                                               -- Filler
--               || ','
--               || NULL                                               -- Filler
--               || ','
--               || NULL                                               -- Filler
--               || ','
--               || 'EmailS:'
--               || REPLACE (rec_dbs_neft1.primary_email, ',', ';')
----PRIMARY_EMAIL -- As discussed with Rupesh on 11MAY2012     -- Advice Details of Transaction
--               || ':EmailE';
----
----                  || 'Bene Code: '||REC_DBS_NEFT.VENDOR_CODE--null -- Filler -- As per Bala Sir/Rupesh mail dated July 12, 2012 6:48 PM
----                  || '^'
----                  || null -- Filler
----                  || '^'
----                  || NULL --REC_DBS_NEFT.REFERENCE_DATE -- Reference date --blank
----                  || '^'
----                  || null --REC_DBS_NEFT.CHECK_ID --blank as discussed with Hemant on 14MAY12 --Reference transaction no --blank  [REPLACE IT WITH CHECK_ID AS THERE IS NO REFERENCE TO PAYMENT ADVICE]
----                  || '^'
----                  || REC_DBS_NEFT.ATTRIBUTE1 --Acknowledgement status -- blank
----                  || '^'
----                  || REC_DBS_NEFT.ATTRIBUTE2 --Rejection code -- blank
----                  || '^'
----                  ||','
----                  || SUBSTR(REC_DBS_NEFT.PRIMARY_NAME,0,49) --Advice name --Changed as DBS is unable to pickup  more than 50 characters
----                  || '^'
----                  || 'Bene Code: '||REC_DBS_NEFT.VENDOR_CODE --SUBSTR(REC_DBS_NEFT.VENDOR_ADDRESS,100,49) --Address Line 3 As per Bala Sir/Rupesh mail dated July 12, 2012 6:48 PM
----                  || '^'
----                  || 'E' --Delivery Mode --For Email Value= E
----                  || '^'
----                  || 'idealmumbai@dbs.com' --Email -- Only one email address as discussed with Rupesh/Hemanth
----                  || '^'
----                  || NULL --Fax -- blank
----                  || '^'
--            UTL_FILE.put_line (l_file_p1, l_pmt_data1);
--         END LOOP;

   --         UTL_FILE.put_line (l_file_p1,
--                               'TRAILER'
--                            || ','
--                            || xx_main.pay_count
--                            || ','
--                            || TO_CHAR (xx_main.transactional_amount,
--                                        '9999999.99'
--                                       )
--                           );         ----  changes made by Dhiresh on 16Nov15
--         UTL_FILE.fclose (l_file_p1);
--      ----------END CON----
--      END LOOP;
--   END IF;

   ---------------------------------------------------------------------------------
-- Payment Advice File Generation
   BEGIN
      SELECT COUNT (*)
        INTO lv_abrl_adv_count
        FROM apps.dbs_payment_invoice dnpi, apps.dbs_payment_table dpt
       WHERE dnpi.check_id = dpt.check_id
         AND TRUNC (creation_date) >= TRUNC (SYSDATE) - 3
         --this will also pick up transactions which were created 2 days prior
         AND dpt.transaction_status = 'AUTHORIZED'
         AND dpt.product_code = 'N'
         AND corporate_account_number NOT IN ('811200088576');

      -- Generating a Unique Sequence Number to be concatenated in the file name
--      SELECT dbs_advice_seq.NEXTVAL || 'x' || TO_CHAR (SYSDATE, 'DDMONYYYY')
--        INTO v_dbs_advice_seq
--        FROM DUAL;
      IF lv_abrl_adv_count > 0
      --AND LV_CORPORATE_ACCOUNT_NUMBER NOT IN ('811200088576')
      THEN
         FOR xx_main IN cur_adv_pay_acc_no
         LOOP
            fnd_file.put_line
                         (fnd_file.LOG,
                             'Payment Advice File corporate_account_number=>'
                          || xx_main.corporate_account_number
                         );

            SELECT    REVERSE
                           (SUBSTR (REVERSE (xx_main.corporate_account_number),
                                    1,
                                    4
                                   )
                           )
                   || '.'
                   || dbs_neft_seq.NEXTVAL
                   || 'x'
                   || TO_CHAR (SYSDATE, 'ddmmyyyy')
              INTO v_dbs_advice_seq
              FROM DUAL;

            --Creating a file for the below generated payment data
            l_file_a :=
               UTL_FILE.fopen ('DBSBANK',
                               'INDBSI03XXXX.' || v_dbs_advice_seq || '.TXT',
                               'W'
                              );

            FOR rec_dbs_adv IN cur_dbs_adv (xx_main.corporate_account_number)
            LOOP
               l_adv_data :=
                     rec_dbs_adv.check_id
                  || '^'
                  || rec_dbs_adv.invoice_number
                  || '^'
                  || TO_CHAR (rec_dbs_adv.invoice_date, 'DD-MON-YYYY')
                  || '^'
                  || rec_dbs_adv.invoice_amount
                  || '^'
                  || REPLACE (rec_dbs_adv.third_party_id, 'OU', '')
                  -- 'Location' added as per Bala Sir/Rupesh mail dated July 12, 2012 6:48 PM
                  || '^'
                  || rec_dbs_adv.tax
                  || '^'
                  || rec_dbs_adv.net_amount
                  || '^'
                  || rec_dbs_adv.pay_type_code
                  || '^'
                  || rec_dbs_adv.vendor_code
                  -- 'Bene Code' added as per Bala Sir/Rupesh mail dated July 12, 2012 6:48 PM
                  || '^'
                  || rec_dbs_adv.primary_name
                  -- Added on 08-Nov-2012 as per mail sent from DBS Bank
                  || '^'
                  || rec_dbs_adv.corporate_account_number
                  -- Added on 08-Nov-2012 as per mail sent from DBS Bank
                  || '^'
                  -- Added on 02-Feb-2016 as per mail sent from DBS Bank
                  || 'EmailS:'
                  || REPLACE (rec_dbs_adv.primary_email, ',', ';')
                  || ':EmailE';
               -- Added on 02-Feb-2016 as per mail sent from DBS Bank
               UTL_FILE.put_line (l_file_a, l_adv_data);
            END LOOP;

            UTL_FILE.fclose (l_file_a);
         END LOOP;
      END IF;

      /* Updating the DBS Payment Stage Table as the transaction for ABRL have been delivered to DBS Bank*/
      UPDATE apps.dbs_payment_table
         SET transaction_status = 'PROCESSED',
             status_flag = 'Y'
       WHERE 1 = 1
         AND TRUNC (creation_date) >= TRUNC (SYSDATE) - 3
         AND transaction_status = 'AUTHORIZED'
         AND corporate_account_number NOT IN ('811200088576')
         AND product_code = 'N';

      COMMIT;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         UTL_FILE.fclose (l_file_a);
         DBMS_OUTPUT.put_line ('No Data Found');
      WHEN UTL_FILE.invalid_path
      THEN
         UTL_FILE.fclose (l_file_a);
         DBMS_OUTPUT.put_line ('Invalid Path');
      WHEN UTL_FILE.invalid_filehandle
      THEN
         UTL_FILE.fclose (l_file_a);
         DBMS_OUTPUT.put_line ('Invalid File Handle');
      WHEN UTL_FILE.invalid_operation
      THEN
         UTL_FILE.fclose (l_file_a);
         DBMS_OUTPUT.put_line ('Invalid Operation');
      WHEN UTL_FILE.read_error
      THEN
         UTL_FILE.fclose (l_file_a);
         DBMS_OUTPUT.put_line ('Read Error');
      WHEN UTL_FILE.write_error
      THEN
         UTL_FILE.fclose (l_file_a);
         DBMS_OUTPUT.put_line ('Write Error');
      WHEN UTL_FILE.internal_error
      THEN
         UTL_FILE.fclose (l_file_a);
         DBMS_OUTPUT.put_line ('Internal Error');
      WHEN UTL_FILE.file_open
      THEN
         UTL_FILE.fclose (l_file_a);
         DBMS_OUTPUT.put_line ('File is Open');
      WHEN UTL_FILE.invalid_filename
      THEN
         UTL_FILE.fclose (l_file_a);
         DBMS_OUTPUT.put_line ('Invalid File Name');
      WHEN OTHERS
      THEN
         UTL_FILE.fclose (l_file_a);
         DBMS_OUTPUT.put_line ('Unknown Error' || SQLERRM);
   END;

   /* TSRL Payments */
   -- TSRL Payment File Generation
   IF lv_tsrl_pay_count > 0
   --AND LV_CORPORATE_ACCOUNT_NUMBER IN ('811200088576')
   THEN
      --Creating a file for the below generated TSRL Payment data
      l_file_p :=
         UTL_FILE.fopen ('DBSBANK',
                            'IDEAL1.FORMAT132.INADRE01.INTRIN01.'
                         || v_dbs_neft_tsrl_seq
                         || '.TXT',
                         'W'
                        );

      FOR rec_dbs_tsrl_neft IN cur_dbs_tsrl_neft
      LOOP
         l_pmt_data :=
               NULL                                 -- Orginating Branch Code
            || '^'
            || TO_CHAR (rec_dbs_tsrl_neft.transaction_value_date,
                        'dd/mm/yyyy')                            -- Value date
            || '^'
            || 'F'                             --Transaction type --Value= "F"
            || '^'
            || NULL                        -- Originating branch Reference No.
            || '^'
            || 'TRINETHRA SUPERRETAIL PVT LTD'                   --Sender name
            || '^'
            || NULL
-- null as discussed with Hamanth on 14MAY12 --REC_DBS_NEFT.CORP_ACC_TYPE --Sender Account Type
            || '^'
            || rec_dbs_tsrl_neft.corporate_account_number  --Sender account no
            || '^'
            || rec_dbs_tsrl_neft.transactional_amount         --Payment Amount
            || '^'
            || rec_dbs_tsrl_neft.benefi_ifsc_code                  --IFSC Code
            || '^'
            || SUBSTR (rec_dbs_tsrl_neft.primary_name, 0, 49)
            --Receiver name --Changed as DBS is unable to pickup  more than 50 characters
            || '^'
            || NULL                                                  -- Filler
            || '^'
            || NULL                                                  -- Filler
            || '^'
            || 'Bene Code: '
            || rec_dbs_tsrl_neft.vendor_code
            --null -- Filler -- As per Bala Sir/Rupesh mail dated July 12, 2012 6:48 PM
            || '^'
            || NULL                                                  -- Filler
            || '^'
            || rec_dbs_tsrl_neft.benefi_acc_num          --Receiver account no
            || '^'
            || NULL
            --REC_DBS_TSRL_NEFT.REFERENCE_DATE -- Reference date --blank
            || '^'
            || NULL
--REC_DBS_TSRL_NEFT.CHECK_ID --blank as discussed with Hemant on 14MAY12 --Reference transaction no --blank  [REPLACE IT WITH CHECK_ID AS THERE IS NO REFERENCE TO PAYMENT ADVICE]
            || '^'
            || rec_dbs_tsrl_neft.attribute1  --Acknowledgement status -- blank
            || '^'
            || rec_dbs_tsrl_neft.attribute2          --Rejection code -- blank
            || '^'
            || ','
            || rec_dbs_tsrl_neft.pay_doc_number
            || ','
            || rec_dbs_tsrl_neft.check_id
            || ','
            -- Payment Details -- Finance Requirement as per Hemant/Rupesh mails
            || '^'
            || SUBSTR (rec_dbs_tsrl_neft.primary_name, 0, 49)
            --Advice name --Changed as DBS is unable to pickup  more than 50 characters
            || '^'
            || SUBSTR (rec_dbs_tsrl_neft.vendor_address, 0, 49)
            -- Address Line 1
            || '^'
            || SUBSTR (rec_dbs_tsrl_neft.vendor_address, 50, 49)
            --Address Line 2
            || '^'
            || SUBSTR (rec_dbs_tsrl_neft.vendor_address, 100, 49)
            --Address Line 3
            || '^'
            || 'Bene Code: '
            || rec_dbs_tsrl_neft.vendor_code
--SUBSTR(REC_DBS_NEFT.VENDOR_ADDRESS,100,49) --Address Line 3 As per Bala Sir/Rupesh mail dated July 12, 2012 6:48 PM
            || '^'
            || NULL
            --'E'                        --Delivery Mode --For Email Value= E
            || '^'
            || NULL                                    --'idealmumbai@dbs.com'
            --Email -- Only one email address as discussed with Rupesh/Hemanth
            || '^'
            || NULL                                             --Fax -- blank
            || '^'
            || 'EmailS:'
            || REPLACE (rec_dbs_tsrl_neft.primary_email, ',', ';')
--PRIMARY_EMAIL -- As discussed with Rupesh on 11MAY2012     -- Advice Details of Transaction
            || ':EmailE';
         UTL_FILE.put_line (l_file_p, l_pmt_data);
      END LOOP;

      UTL_FILE.fclose (l_file_p);
   END IF;

---------------------------------------------------------------------------------
-- TSRL Payment File Generation only for DBS Reconciliation with a different file name
   IF lv_tsrl_pay_count > 0
   --AND LV_CORPORATE_ACCOUNT_NUMBER  IN ('811200088576')
   THEN
      --Creating a file for the below generated payment data
      l_file_p1 :=
         UTL_FILE.fopen ('DBSBANK',
                         'INDBSI02XXXX.' || v_dbs_neft_tsrl_seq1 || '.TXT',
                         'W'
                        );

      FOR rec_dbs_tsrl_neft1 IN cur_dbs_tsrl_neft
      LOOP
         l_pmt_data1 :=
               NULL                                 -- Orginating Branch Code
            || '^'
            || TO_CHAR
                      (rec_dbs_tsrl_neft1.transaction_value_date,
                       'dd/mm/yyyy')                             -- Value date
            || '^'
            || 'F'                             --Transaction type --Value= "F"
            || '^'
            || NULL                        -- Originating branch Reference No.
            || '^'
            || 'TRINETHRA SUPERRETAIL PVT LTD'                   --Sender name
            || '^'
            || NULL
-- null as discussed with Hamanth on 14MAY12 --REC_DBS_NEFT.CORP_ACC_TYPE --Sender Account Type
            || '^'
            || rec_dbs_tsrl_neft1.corporate_account_number --Sender account no
            || '^'
            || rec_dbs_tsrl_neft1.transactional_amount        --Payment Amount
            || '^'
            || rec_dbs_tsrl_neft1.benefi_ifsc_code                 --IFSC Code
            || '^'
            || SUBSTR (rec_dbs_tsrl_neft1.primary_name, 0, 49)
--Receiver name -- Changed as DBS is unable to pickup  more than 50 characters
            || '^'
            || NULL                                                  -- Filler
            || '^'
            || NULL                                                  -- Filler
            || '^'
            || 'Bene Code: '
            || rec_dbs_tsrl_neft1.vendor_code
            --null -- Filler -- As per Bala Sir/Rupesh mail dated July 12, 2012 6:48 PM
            || '^'
            || NULL                                                  -- Filler
            || '^'
            || rec_dbs_tsrl_neft1.benefi_acc_num         --Receiver account no
            || '^'
            || NULL
            --REC_DBS_TSRL_NEFT1.REFERENCE_DATE -- Reference date --blank
            || '^'
            || NULL
--REC_DBS_TSRL_NEFT1.CHECK_ID --blank as discussed with Hemant on 14MAY12 --Reference transaction no --blank  [REPLACE IT WITH CHECK_ID AS THERE IS NO REFERENCE TO PAYMENT ADVICE]
            || '^'
            || rec_dbs_tsrl_neft1.attribute1 --Acknowledgement status -- blank
            || '^'
            || rec_dbs_tsrl_neft1.attribute2         --Rejection code -- blank
            || '^'
            || ','
            || rec_dbs_tsrl_neft1.pay_doc_number
            || ','
            || rec_dbs_tsrl_neft1.check_id
            || ','
            -- Payment Details -- Finance Requirement as per Hemant/Rupesh mails
            || '^'
            || SUBSTR (rec_dbs_tsrl_neft1.primary_name, 0, 49)
            --Advice name -- Changed as DBS is unable to pickup  more than 50 characters
            || '^'
            || SUBSTR (rec_dbs_tsrl_neft1.vendor_address, 0, 49)
            -- Address Line 1
            || '^'
            || SUBSTR (rec_dbs_tsrl_neft1.vendor_address, 50, 49)
            --Address Line 2
            || '^'
            || SUBSTR (rec_dbs_tsrl_neft1.vendor_address, 100, 49)
            --Address Line 3
            || '^'
            || 'Bene Code: '
            || rec_dbs_tsrl_neft1.vendor_code
--SUBSTR(REC_DBS_NEFT.VENDOR_ADDRESS,100,49) --Address Line 3 As per Bala Sir/Rupesh mail dated July 12, 2012 6:48 PM
            || '^'
            || NULL
            --'E'                        --Delivery Mode --For Email Value= E
            || '^'
            || NULL                                    --'idealmumbai@dbs.com'
            --Email -- Only one email address as discussed with Rupesh/Hemanth
            || '^'
            || NULL                                             --Fax -- blank
            || '^'
            || 'EmailS:'
            || REPLACE (rec_dbs_tsrl_neft1.primary_email, ',', ';')
--PRIMARY_EMAIL -- As discussed with Rupesh on 11MAY2012     -- Advice Details of Transaction
            || ':EmailE';
         UTL_FILE.put_line (l_file_p1, l_pmt_data1);
      END LOOP;

      UTL_FILE.fclose (l_file_p1);
   END IF;

---------------------------------------------------------------------------------
 -- TSRL Payment Advice File Generation
   BEGIN
      SELECT COUNT (*)
        INTO lv_tsrl_adv_count
        FROM apps.dbs_payment_invoice dnpi, apps.dbs_payment_table dpt
       WHERE dnpi.check_id = dpt.check_id
         AND TRUNC (creation_date) >= TRUNC (SYSDATE) - 3
         --this will also pick up transactions which were created 2 days prior
         AND dpt.transaction_status = 'AUTHORIZED'
         AND dpt.product_code = 'N'
         AND corporate_account_number IN ('811200088576');

      -- Generating a Unique Sequence Number to be concatenated in the file name
      SELECT    dbs_advice_tsrl_seq.NEXTVAL
             || 'x'
             || TO_CHAR (SYSDATE, 'DDMONYYYY')
        INTO v_dbs_advice_tsrl_seq
        FROM DUAL;

      IF lv_tsrl_adv_count > 0
      --AND LV_CORPORATE_ACCOUNT_NUMBER IN ('811200088576')
      THEN
         --Creating a file for the below generated TSRL Payment data
         l_file_a :=
            UTL_FILE.fopen ('DBSBANK',
                            'INDBSI02XXXX.' || v_dbs_advice_tsrl_seq || '.TXT',
                            'W'
                           );

         FOR rec_dbs_tsrl_adv IN cur_dbs_tsrl_adv
         LOOP
            l_adv_data :=
                  rec_dbs_tsrl_adv.check_id
               || '^'
               || rec_dbs_tsrl_adv.invoice_number
               || '^'
               || TO_CHAR (rec_dbs_tsrl_adv.invoice_date, 'DD-MON-YYYY')
               || '^'
               || rec_dbs_tsrl_adv.invoice_amount
               || '^'
               || rec_dbs_tsrl_adv.third_party_id
               -- 'Location' added as per Bala Sir/Rupesh mail dated July 12, 2012 6:48 PM
               || '^'
               || rec_dbs_tsrl_adv.tax
               || '^'
               || rec_dbs_tsrl_adv.net_amount
               || '^'
               || rec_dbs_tsrl_adv.pay_type_code
               || '^'
               || rec_dbs_tsrl_adv.vendor_code
               -- 'Bene Code' added as per Bala Sir/Rupesh mail dated July 12, 2012 6:48 PM
               || '^'
               || rec_dbs_tsrl_adv.primary_name
               -- Added on 08-Nov-2012 as per mail sent from DBS Bank
               || '^'
               || rec_dbs_tsrl_adv.corporate_account_number;
            -- Added on 08-Nov-2012 as per mail sent from DBS Bank
            UTL_FILE.put_line (l_file_a, l_adv_data);
         END LOOP;

         UTL_FILE.fclose (l_file_a);
      END IF;

      /* Updating the DBS Payment Stage Table as the transaction for TSRL have been delivered to DBS Bank*/
      UPDATE apps.dbs_payment_table
         SET transaction_status = 'PROCESSED',
             status_flag = 'Y'
       WHERE 1 = 1
         AND TRUNC (creation_date) >= TRUNC (SYSDATE) - 3
         AND transaction_status = 'AUTHORIZED'
         AND corporate_account_number IN ('811200088576')
         AND product_code = 'N';

      COMMIT;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         UTL_FILE.fclose (l_file_a);
         DBMS_OUTPUT.put_line ('No Data Found');
      WHEN UTL_FILE.invalid_path
      THEN
         UTL_FILE.fclose (l_file_a);
         DBMS_OUTPUT.put_line ('Invalid Path');
      WHEN UTL_FILE.invalid_filehandle
      THEN
         UTL_FILE.fclose (l_file_a);
         DBMS_OUTPUT.put_line ('Invalid File Handle');
      WHEN UTL_FILE.invalid_operation
      THEN
         UTL_FILE.fclose (l_file_a);
         DBMS_OUTPUT.put_line ('Invalid Operation');
      WHEN UTL_FILE.read_error
      THEN
         UTL_FILE.fclose (l_file_a);
         DBMS_OUTPUT.put_line ('Read Error');
      WHEN UTL_FILE.write_error
      THEN
         UTL_FILE.fclose (l_file_a);
         DBMS_OUTPUT.put_line ('Write Error');
      WHEN UTL_FILE.internal_error
      THEN
         UTL_FILE.fclose (l_file_a);
         DBMS_OUTPUT.put_line ('Internal Error');
      WHEN UTL_FILE.file_open
      THEN
         UTL_FILE.fclose (l_file_a);
         DBMS_OUTPUT.put_line ('File is Open');
      WHEN UTL_FILE.invalid_filename
      THEN
         UTL_FILE.fclose (l_file_a);
         DBMS_OUTPUT.put_line ('Invalid File Name');
      WHEN OTHERS
      THEN
         UTL_FILE.fclose (l_file_a);
         DBMS_OUTPUT.put_line ('Unknown Error' || SQLERRM);
   END;
/* End of TSRL Payments */

------------------------------------------------------------------------
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      UTL_FILE.fclose (l_file_p);
      DBMS_OUTPUT.put_line ('No Data Found');
   WHEN UTL_FILE.invalid_path
   THEN
      UTL_FILE.fclose (l_file_p);
      DBMS_OUTPUT.put_line ('Invalid Path');
   WHEN UTL_FILE.invalid_filehandle
   THEN
      UTL_FILE.fclose (l_file_p);
      DBMS_OUTPUT.put_line ('Invalid File Handle');
   WHEN UTL_FILE.invalid_operation
   THEN
      UTL_FILE.fclose (l_file_p);
      DBMS_OUTPUT.put_line ('Invalid Operation');
   WHEN UTL_FILE.read_error
   THEN
      UTL_FILE.fclose (l_file_p);
      DBMS_OUTPUT.put_line ('Read Error');
   WHEN UTL_FILE.write_error
   THEN
      UTL_FILE.fclose (l_file_p);
      DBMS_OUTPUT.put_line ('Write Error');
   WHEN UTL_FILE.internal_error
   THEN
      UTL_FILE.fclose (l_file_p);
      DBMS_OUTPUT.put_line ('Internal Error');
   WHEN UTL_FILE.file_open
   THEN
      UTL_FILE.fclose (l_file_p);
      DBMS_OUTPUT.put_line ('File is Open');
   WHEN UTL_FILE.invalid_filename
   THEN
      UTL_FILE.fclose (l_file_p);
      DBMS_OUTPUT.put_line ('Invalid File Name');
   WHEN OTHERS
   THEN
      UTL_FILE.fclose (l_file_p);
      DBMS_OUTPUT.put_line ('Unknown Error' || SQLERRM);
END xxabrl_dbs_outbound_utl; 
/


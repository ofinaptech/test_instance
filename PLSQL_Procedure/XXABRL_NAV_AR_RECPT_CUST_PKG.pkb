CREATE OR REPLACE PACKAGE BODY APPS.xxabrl_nav_ar_recpt_cust_pkg IS
/**************************************************************************************************************
                 OBJECT INFORMATION

 Object Name : XXABRL_NAV_AR_RECPT_CUST_PKG

 Description : Procedure to  Validate the receipts , inserting and applying the receipts 

  Change Record:
 =============================================================================================================
 Version    Date            Author       Remarks
 =======   ==========    =============   =====================================================================
 1.0.1     07-Dec-09    Praveen Kumar     Initial Version
 1.0.2     14-Apr-10   Praveen Kumar      Modified the receipt creation from receipt date to deposited date  
 1.0.3     27-May-10   Naresh Hasti       Added the upper function in the Bank Name validation 
 1.0.4     26-JUL-10   Sayikrishna        New Condition added for Resticting Reverse case iof Receipts
 1.0.5     23-MAR-11   Uma Mahesh        23-03-11 added conditoin to pick up receipts per application which are with interfaced_flag  'EA'
 1.0.6     07-APR-11   RAVI/MITUL        07-04-11 added conditoin not to pick up the freezed data for Ou TSRL KA SM with gl_date > '31-Mar-2011' as instructed from Karnataka finance .
 1.0.7     25-MAY-11   RAVI              COMMENETED THE CONDITION WHICH WAS ADDED IN 1.0.6 VERSION
***************************************************************************************************************/

  PROCEDURE CUST_RECEIPT_VALIDATE(p_errbuf      OUT VARCHAR2,
                             p_retcode     OUT NUMBER,
                             p_action IN VARCHAR2,
                             P_Data_Source IN VARCHAR2,
                             p_receipt_to_apply IN VARCHAR2,
                             p_apply_all_flag IN VARCHAR2,
                             p_gl_date  IN VARCHAR2
                             ) AS
    --
    -- Declaring cursor for validating New and Error receipt transaction
    --  SELECT * FROM XXABRL_NAVI_AR_RECEIPT_STG2
    CURSOR arr_c1(p_operating_unit VARCHAR2,cp_gl_date DATE) IS
      SELECT ROWID
             ,data_source
             ,receipt_number
             ,OPERATING_UNIT
             ,RECEIPT_DATE
             ,deposit_date
             ,gl_date
             ,currency_code
             ,exchange_rate_type
             ,exchange_rate
             ,exchange_date
             ,nvl(deposited_amount,receipt_amount) receipt_amount
             ,er_customer_number
             ,er_dc_code
             ,comments
             ,er_tender_type
             ,sales_transaction_number
             ,of_customer_number
             ,receipt_method
             ,bank_account_name
             ,bank_account_number
             ,invoice_number
             ,amount_applied
             ,org_name
             ,actual_amount
             ,deposited_amount
             ,deposited_date
             ,opening_balance
             ,closing_balance
             ,net_change_at_store
        FROM xxabrl_navi_ar_receipt_stg2
       WHERE UPPER(TRIM(data_source)) = UPPER(TRIM(p_data_source))
         AND UPPER(TRIM(NVL(operating_unit, p_operating_unit))) =
             UPPER(TRIM(p_operating_unit))
         AND gl_date = NVL(cp_gl_date,gl_date)
         AND nvl(deposited_amount,receipt_amount) <> 0 --ADDED     
         AND NVL(interfaced_flag, 'N') IN ('N', 'E')
         AND freeze_flag='Y'
     /*    ----- UPDATED BY RAVI AND MITUL NOT TO MOVE THE FREEZED DATA FOR KASM FROM 01-APR-2011
         AND receipt_number NOT IN (               
               SELECT  receipt_number FROM
               APPS.xxabrl_navi_ar_receipt_stg2
               WHERE  1=1
               AND TRUNC(GL_DATE)>'31-MAR-2011' 
               AND operating_unit='TSRL KA SM')
          ----- UPDATED BY RAVI AND MITUL */       
          -- New Condition added for Resticting Reverse case of Receipts
         and not exists 
                (select 1
                from ar_Cash_receipts_all acr
                where acr.receipt_number=xxabrl_navi_ar_receipt_stg2.RECEIPT_NUMBER
                and STATUS ='REV'
                and acr.reversal_category is not null
                and acr.reversal_date is not null
                ) 
         ;
         -- Declaring Local Varibules 
    v_Orgid             NUMBER := Fnd_Profile.VALUE('ORG_ID');
    v_set_of_bks_id     NUMBER := Fnd_Profile.VALUE('GL_SET_OF_BKS_ID');
    V_user_Id           NUMBER := Fnd_profile.VALUE('USER_ID');
    V_Fun_Curr          VARCHAR2(10);
    v_currency          fnd_currencies.Currency_Code%TYPE;
    V_Operating_Unit    HR_Operating_Units.Short_Code%TYPE;
    V_bank_account_Name CE_bank_accounts.bank_account_name%TYPE;
    V_Bank_Account_num  CE_bank_accounts.bank_account_num%TYPE;
    V_Error_Count       NUMBER := 0;
    v_OK_Rec_count      NUMBER := 0;
    V_Error_Message     VARCHAR2(1000);
    v_record_count      NUMBER := 0;
    V_Data_Count        NUMBER := 0;
    V_Cust_Account_Id   NUMBER;
    V_Customer_Number   Hz_cust_accounts_all.Account_Number%TYPE;
    V_Invoice_Number    VARCHAR2(20);
    V_Amount_Applied    NUMBER;
    V_site_use_id       NUMBER;
    V_Receipt_Method    ar_receipt_methods.NAME%TYPE;
    v_REMIT_BANK_ACCT_USE_ID VARCHAR2(240);
    V_GL_DATE           DATE;  
    P_Org_Id            NUMBER := Fnd_Profile.VALUE('ORG_ID');
  BEGIN

    fnd_file.put_line(fnd_file.LOG, ' gl date '||p_gl_date);  
    fnd_file.put_line(fnd_file.LOG, ' ececuting select stmt');
      
        BEGIN
              SELECT TO_DATE(p_gl_date,'YYYY/MM/DD HH24:MI:SS') INTO v_gl_date FROM dual;

            fnd_file.put_line(fnd_file.LOG, ' executed stmt');
        EXCEPTION
        WHEN OTHERS THEN
         fnd_file.put_line(fnd_file.LOG, ' Error-> '||SQLERRM);
       END;
          --================================
          -- Validation For Operating Unit 
          --================================
             

        BEGIN
          SELECT short_code
            INTO v_operating_unit
            FROM hr_operating_units
           WHERE Organization_Id = P_Org_Id;

            fnd_file.put_line(fnd_file.output,
                              'OU / Org ID '||V_OPERATING_UNIT ||' / '|| P_Org_Id);
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            fnd_file.put_line(fnd_file.output,
                              'Selected Org Id does not exist in Oracle Financials');
            fnd_file.put_line(fnd_file.output,
                              '........................................................................');
          WHEN TOO_MANY_ROWS THEN
            fnd_file.put_line(fnd_file.output,
                              'Multiple Org Id exist in Oracle Financials');
            fnd_file.put_line(fnd_file.output,
                              '........................................................................');
          WHEN OTHERS THEN
            fnd_file.put_line(fnd_file.output, 'Invalid Org Id Selected ');
            fnd_file.put_line(fnd_file.output,
                              '........................................................................');
        END;


    fnd_file.put_line(fnd_file.output, 'Receipts/Invoice to APPLY '||NVL(p_receipt_to_apply,'NULL'));

    ----
    -- Update user id in new record
    ----
    UPDATE xxabrl_navi_ar_receipt_stg2
       SET created_by = v_user_id
     WHERE TRIM(UPPER(data_source)) = TRIM(UPPER(P_Data_Source))
       AND TRIM(UPPER(operating_unit)) = TRIM(UPPER(v_operating_unit))
       AND NVL(interfaced_flag, 'N') = 'N'
       AND Freeze_Flag='Y'; 
      -- AND CREATED_BY IS NULL;
    ----
    -- Remove error message before validating
    ----
    UPDATE xxabrl_navi_ar_receipt_stg2
       SET error_message = NULL
     WHERE TRIM(UPPER(Data_Source)) = TRIM(UPPER(P_Data_Source))
       AND TRIM(UPPER(Operating_Unit)) = TRIM(UPPER(V_Operating_Unit))
       AND NVL(Interfaced_Flag, 'N') IN ('N', 'E')
       AND Error_Message IS NOT NULL
       AND Freeze_Flag='Y';
    COMMIT;
    fnd_file.put_line(fnd_file.output,
                      'Following AR Receipt Information are validating');
    fnd_file.put_line(fnd_file.output,
                      '........................................................................');
    ---- ============================
    -- Select functional currency
    ----============================
            BEGIN
              SELECT Currency_code
                INTO V_Fun_Curr
                FROM GL_SETS_OF_BOOKS
               WHERE set_of_books_id = v_set_of_bks_id;
            EXCEPTION
              WHEN OTHERS THEN
                V_Fun_Curr := NULL;
            END;

    fnd_file.put_line(fnd_file.output,'***--- AR Receipt Validating Starts ---***');

    FOR ARR_R1 IN ARR_C1(V_OPERATING_UNIT,v_gl_date) LOOP
      EXIT WHEN ARR_C1%NOTFOUND;
          v_record_count    := ARR_C1%ROWCOUNT;
          V_Error_Message   := NULL;
          V_Data_Count      := NULL;
          V_Cust_Account_Id := NULL;
          V_site_use_id     := NULL;
          v_currency        := NULL;
          V_Receipt_Method  := NULL;
          ----
          --  Receipt Number validation
          ---
          fnd_file.put_line(fnd_file.output,'----------------------------------------------------');
          fnd_file.put_line(fnd_file.output,'### Validating Receipt ('||ARR_R1.Receipt_Number||')');

          IF ARR_R1.Receipt_Number IS NULL THEN
            V_Error_Message := V_Error_Message || 'Receipr Number is Null';
          END IF;
          ----
          --  Receipt Amount validation
          ---
          IF ARR_R1.Receipt_Amount IS NULL THEN
            V_Error_Message := V_Error_Message || 'Receipr Amount is Null';
          ELSIF ARR_R1.Receipt_Amount <= 0 THEN
            V_Error_Message := V_Error_Message || 'Receipr Amount is negative';
          END IF;
          ----
          --  Receipt Date validation
          ---
         /* IF ARR_R1.Receipt_Date IS NULL THEN
            V_Error_Message := V_Error_Message || 'Receipr Date is Null';
          END IF;*/   -----------by JAGAN
          ----
          --  Deposite Date validation
          ---
          IF ARR_R1.Deposit_Date IS NULL THEN
            V_Error_Message := V_Error_Message || 'Deposite Date is Null';
          END IF;
          ----
          --  GL Date validation, should be in AR Open Period
          ---
          IF ARR_R1.GL_Date IS NULL THEN
            V_Error_Message := V_Error_Message || 'GL Date is Null';
          ELSE
            BEGIN
              V_Data_Count := 0;
              SELECT COUNT(gps.Period_Name)
                INTO V_Data_Count
                FROM gl_period_statuses gps, fnd_application fna
               WHERE fna.application_short_name = 'AR'
                 AND fna.application_id = gps.application_id
                 AND gps.closing_status = 'O'
                 AND gps.set_of_books_id = v_set_of_bks_id
                 AND ARR_R1.GL_date BETWEEN gps.start_date AND gps.end_date;
              IF V_Data_Count = 0 THEN
                V_Error_Message := V_Error_Message ||
                                   'GL date is not in AR Open Period -->>';
              END IF;
            EXCEPTION
              WHEN OTHERS THEN
                V_Error_Message := V_Error_Message ||
                                   'GL Period not open in AR -->>';
            END;
          END IF;
      ----
      --  Currency Code validation
      ---
          IF ARR_R1.Currency_Code IS NULL THEN
            V_Error_Message := V_Error_Message || 'Currency is null --->>';
          ELSE
            BEGIN
              SELECT currency_code
                INTO v_currency
                FROM fnd_currencies
               WHERE UPPER(currency_code) = Trim(UPPER(ARR_R1.Currency_Code));
              IF UPPER(trim(v_currency)) <> UPPER(trim(V_Fun_Curr)) AND
                 ARR_R1.exchange_date IS NULL THEN
                V_Error_Message := V_Error_Message ||
                                   'Exchange Date is Null -->>';
              END IF;
            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                V_Error_Message := V_Error_Message ||
                                   'Currency Code Does Not Exist-->>';
              WHEN TOO_MANY_ROWS THEN
                V_Error_Message := V_Error_Message ||
                                   'Multiple Currency Found-->>';
              WHEN OTHERS THEN
                V_Error_Message := V_Error_Message || 'Invalid Currency -->>';
            END;
          END IF;
          ----
          --  Customer Number validation
          ---
          IF ARR_R1.ER_Customer_Number IS NULL THEN
            V_Error_Message := V_Error_Message ||
                               'Feeder System Customer Number is Null-->>';
          END IF;
          IF ARR_R1.ER_DC_CODE IS NULL THEN
            V_Error_Message := V_Error_Message || 'DC Code is Null-->>';
          END IF;
          IF ARR_R1.ER_Customer_Number IS NOT NULL AND
             ARR_R1.ER_DC_CODE IS NOT NULL THEN
            BEGIN
              V_Cust_Account_Id := NULL;
              V_site_use_id     := NULL;
              SELECT UNIQUE hca.Account_Number,
                     hca.Cust_Account_Id,
                     hcsu.site_use_id
                INTO V_Customer_Number, V_Cust_Account_Id, V_site_use_id
                FROM hz_cust_accounts_all         hca,
                     hz_cust_acct_sites_all       hcas,
                     hz_party_sites               hps,
                     hz_cust_site_uses_all        hcsu
                     --,xxabrl_ar_cust_bkacc_map_int xac
               WHERE /*ER_DC_Code = ARR_R1.ER_DC_Code
                 And ER_Customer_Number = ARR_R1.ER_Customer_Number
                 And hca.Account_Number = xac.OF_Customer_Number
                 And */
                 hca.Account_Number = ARR_R1.ER_Customer_Number
                 AND hca.cust_account_id = hcas.cust_account_id
                 AND hcas.org_id = P_Org_id
                 AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id(+)
                 AND hcas.party_site_id = hps.party_site_id
                 --And hps.Party_Site_Number = xac.OF_Customer_Site_Code
                 AND hcsu.site_use_code = 'BILL_TO'
                 AND hcas.bill_to_flag = 'P'
                 AND hcas.status = 'A';

             Fnd_File.PUT_LINE(fnd_file.OUTPUT,'Account Number '|| ARR_R1.ER_Customer_Number);
             Fnd_File.PUT_LINE(fnd_file.OUTPUT,'Cust Acct ID '|| V_Cust_Account_Id);
             Fnd_File.PUT_LINE(fnd_file.OUTPUT,'Site Use ID '|| V_site_use_id);

        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            V_Error_Message := V_Error_Message ||
                               'Customer Number Or Site in not exist in this Org -->>';
          WHEN TOO_MANY_ROWS THEN
            V_Error_Message := V_Error_Message ||
                               'Multiple Customer Number Or Site found in this Org -->>';
          WHEN OTHERS THEN
            V_Error_Message := V_Error_Message ||
                               'Customer Number Or Site in not exist in this Org -->>';
        END;
      END IF;
      ----
      --  Duplicate Receipt Number check
          ---
          IF ARR_R1.Receipt_Number IS NOT NULL THEN
            BEGIN
              V_Data_Count := 0;
              SELECT COUNT(Receipt_Number)
                INTO V_Data_Count
                FROM xxabrl_navi_ar_receipt_stg2
               WHERE Trim(UPPER(P_Data_Source)) = Trim(UPPER(P_Data_Source))
                 AND Trim(UPPER(Operating_Unit)) =
                     Trim(UPPER(V_Operating_Unit))
                 AND ER_DC_Code = ARR_R1.ER_DC_Code
                 AND OF_Customer_Number = ARR_R1.ER_Customer_Number
                 AND Trim(Receipt_Number) = Trim(ARR_R1.Receipt_Number)
                 AND ROWID <> ARR_R1.ROWID;
              IF V_Data_Count > 0 THEN
                V_Error_Message := V_Error_Message ||
                                   'Duplicate Receipt Number exist in Staging table -->>';
              END IF;
            EXCEPTION
              WHEN OTHERS THEN
                V_Error_Message := V_Error_Message ||
                                   'Error In Receipt Number -->>';
            END;
            BEGIN
              V_Data_Count := 0;
              SELECT COUNT(Receipt_Number)
                INTO V_Data_Count
                FROM ar_cash_receipts_all
               WHERE Trim(Receipt_Number) = Trim(ARR_R1.Receipt_Number)
                 AND Pay_From_Customer = V_Cust_Account_Id
                 AND Org_Id = P_Org_Id;
              IF V_Data_Count > 0 THEN
                V_Error_Message := V_Error_Message ||
                                   'Receipt Number exist in Oracle Financials -->>';
              END IF;
            EXCEPTION
              WHEN OTHERS THEN
                V_Error_Message := V_Error_Message ||
                                   'Error In Receipt Number -->>';
            END;
          END IF;
          ----
      --  Receipt Method validation
      ---
      /*
      If ARR_R1.ER_Tender_Type is null then
        V_Error_Message := V_Error_Message || 'Tender Type is null --->>';
      Else
        Begin
          V_Receipt_Method   := Null;
          V_Bank_Account_num := Null;
          Select OF_Receipt_Methods, OF_Bank_Account_Name
            Into V_Receipt_Method, V_Bank_Account_Name
            From XXABRL_AR_CUST_BKACC_MAP_INT
           Where ER_DC_Code = ARR_R1.ER_DC_CODE
             And ER_Customer_Number = ARR_R1.ER_Customer_Number
             And ER_Tender_Type = ARR_R1.ER_Tender_Type
             And OF_Organization_id = P_Org_Id;
        Exception
          When no_data_found then
            V_Error_Message := V_Error_Message ||
                               'Tender Type Mapping does not exist -->>';
          When too_many_rows then
            V_Error_Message := V_Error_Message ||
                               'Multiple Tender Type found -->>';
          When Others then
            V_Error_Message := V_Error_Message ||
                               'Invalid Tender Type -->>';
        End;
      End If;
      */
      IF ARR_R1.Receipt_Method IS NOT NULL THEN
        BEGIN
          SELECT NAME
            INTO V_Receipt_Method
            FROM ar_receipt_methods
           WHERE UPPER(NAME) = trim(UPPER(ARR_R1.Receipt_Method));

           fnd_file.PUT_LINE(fnd_file.OUTPUT,'V_Receipt_Method ' ||V_Receipt_Method);
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            V_Error_Message := V_Error_Message || 'Mapped Receipt Method ' ||
                               V_Receipt_Method || ' does not exist -->>';
          WHEN TOO_MANY_ROWS THEN
            V_Error_Message := V_Error_Message || 'Multiple ' ||
                               V_Receipt_Method ||
                               ' Receipt Method found -->>';
          WHEN OTHERS THEN
            V_Error_Message := V_Error_Message || 'Invalid Receipt_Method ' ||
                               V_Receipt_Method || '  -->>';
        END;
      END IF;
      ----
      --  Bank Account validation
      ----
      IF V_Bank_Account_num IS NOT NULL THEN
        BEGIN
          SELECT Bank_Account_Num
            INTO V_Bank_Account_num
            FROM Ce_bank_accounts
           WHERE Trim(UPPER(Bank_Account_Name)) =
                 Trim(UPPER(V_Bank_Account_Name));
          --And    Org_id    = P_Org_Id;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            V_Error_Message := V_Error_Message || 'Bank Account Number ' ||
                               V_Bank_Account_num || ' does not exist -->>';
          WHEN TOO_MANY_ROWS THEN
            V_Error_Message := V_Error_Message || 'Multiple ' ||
                               V_Bank_Account_num ||
                               ' Bank Account Number found -->>';
          WHEN OTHERS THEN
            V_Error_Message := V_Error_Message ||
                               'Invalid Bank Acount Number ' ||
                               V_Bank_Account_num || ' -->>';
        END;
      END IF;
      ----
      --  Get amount to be applied
      ---
      IF ARR_R1.Sales_Transaction_Number IS NULL THEN
        V_Error_Message := V_Error_Message ||
                           'Sales Transaction Number is Null -->>';
      ELSE
      NULL;
      /*
      Fnd_file.PUT_LINE(fnd_file.output,'Sales_Transaction_Number '||ARR_R1.Sales_Transaction_Number);

        Begin
          V_Invoice_Number := Null;
          Select Trx_Number
            Into V_Invoice_Number
            From Ra_Customer_Trx_All
           Where Bill_To_Customer_Id = V_Cust_Account_Id
             And Bill_To_Site_Use_Id = V_site_use_id
             And Org_Id = P_Org_Id
             And Interface_Header_Attribute1 =
                 ARR_R1.Sales_Transaction_Number;
        EXCEPTION
          When no_data_found then
            V_Invoice_Number := Null;
            V_Error_Message  := V_Error_Message ||
                                'Sales Transaction Number ' ||
                                ARR_R1.Sales_Transaction_Number ||
                                ' does not exist in Current Organization-->>';
          When too_many_rows then
            V_Invoice_Number := Null;
            V_Error_Message  := V_Error_Message ||
                                'Multiple Sales Transaction Number ' ||
                                ARR_R1.Sales_Transaction_Number ||
                                ' Found -->>';
          When Others then
            V_Invoice_Number := Null;
            V_Error_Message  := V_Error_Message ||
                                'Invalid Sales Transaction Number ' ||
                                ARR_R1.Sales_Transaction_Number || ' -->>';
        End;
        If V_Invoice_Number Is Not Null Then
          V_Amount_Applied := 0;
          Begin
            Select Amount_Due_Remaining
              Into V_Amount_Applied
              From ar_payment_schedules_all
             Where Trx_number = V_Invoice_Number
               And Customer_id = V_Cust_Account_Id
               And Customer_site_use_id = V_site_use_id
               And Invoice_Currency_code = ARR_R1.CURRENCY_CODE
               And Org_id = P_Org_Id
               And Class = 'INV';
            If V_Amount_Applied = 0 Then
              V_Error_Message := V_Error_Message ||
                                 'Invoice O/S Amount is Zero, you can not apply this receipt-->>';
            End If;
            If V_Amount_Applied > ARR_R1.Receipt_Amount then
              V_Amount_Applied := ARR_R1.Receipt_Amount;
            End If;
          EXCEPTION
            When no_data_found then
              V_Error_Message := V_Error_Message || 'Invoice Number ' ||
                                 V_Invoice_Number ||
                                 ' does not exist in Current Organization-->>';
            When too_many_rows then
              V_Error_Message := V_Error_Message || 'Multiple ' ||
                                 V_Invoice_Number ||
                                 ' Invoice Number Found -->>';
            When Others then
              V_Error_Message := V_Error_Message ||
                                 'Invalid Invoice Number ' ||
                                 V_Invoice_Number || ' -->>';
          End;
        End If;*/
      END IF;

      -- Get Remitt to Account ID
      IF (ARR_R1.BANK_ACCOUNT_NAME IS NOT NULL)
         AND (ARR_R1.BANK_ACCOUNT_NUMBER IS NOT NULL)
         AND (ARR_R1.RECEIPT_METHOD IS NOT NULL)
         THEN

      BEGIN
          SELECT REMIT_BANK_ACCT_USE_ID
          INTO v_REMIT_BANK_ACCT_USE_ID
          FROM AR_RECEIPT_METHOD_ACCOUNTS
          WHERE
          remit_bank_acct_use_id IN
                (
                SELECT ba.bank_acct_use_id
                FROM ce_bank_acct_uses  ba,
                ce_bank_accounts   cba,
                ce_bank_branches_v bb
                WHERE bb.bank_name LIKE '%'
                AND bb.bank_branch_name LIKE '%'
                AND bb.bank_institution_type = 'BANK'
                AND bb.branch_party_id = cba.bank_branch_id
                AND cba.bank_account_id = ba.bank_account_id
                AND cba.account_classification = 'INTERNAL'
                AND upper(cba.bank_account_name) = upper(ARR_R1.BANK_ACCOUNT_NAME) --'MH HDFC BANK - 1234' ---added be Naresh on 27-may-10
                AND cba.bank_account_num = ARR_R1.BANK_ACCOUNT_NUMBER-- '45678901234'
                )
          AND RECEIPT_METHOD_ID = (SELECT receipt_method_id
            FROM ar_receipt_methods
           WHERE UPPER(NAME) = UPPER(ARR_R1.RECEIPT_METHOD))
          ORDER BY REMIT_BANK_ACCT_USE_ID;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
              V_Error_Message := V_Error_Message || 'Bank_Account Name/Number  ' ||
                                 ARR_R1.BANK_ACCOUNT_NAME ||' / ' || ARR_R1.BANK_ACCOUNT_NUMBER  ||
                                 ' does not exist in Current Organization-->>';
      WHEN TOO_MANY_ROWS THEN
              V_Error_Message := V_Error_Message || 'Bank_Account Name/Number  ' ||
                                 ARR_R1.BANK_ACCOUNT_NAME ||' / ' || ARR_R1.BANK_ACCOUNT_NUMBER  ||
                                 ' Multiple declaration-->>';
      WHEN OTHERS THEN
              V_Error_Message := V_Error_Message || 'Exception in Bank_Account Name/Number  ' ||
                                 ARR_R1.BANK_ACCOUNT_NAME ||' / ' || ARR_R1.BANK_ACCOUNT_NUMBER;

      END;

      ELSE
          V_Error_Message := V_Error_Message || 'Receipt_Method/Bank_Account_Name/Acct_Number is NULL';
      END IF;

      IF V_Error_Message IS NOT NULL THEN
        ----
        --  Update error message
        ---
        UPDATE xxabrl_navi_ar_receipt_stg2
           SET ERROR_MESSAGE = V_Error_Message, INTERFACED_FLAG = 'E'
         WHERE ROWID = ARR_R1.ROWID;
        COMMIT;
        V_Error_Count := V_Error_Count + 1;
        fnd_file.put_line(fnd_file.output, v_record_count || '--');
        fnd_file.put_line(fnd_file.output,
                          ARR_R1.Receipt_Number || '-->' || '-->' ||
                          V_Error_Message);
      ELSE
        ----
        --  Record is valid, update additional required info
        ---
        UPDATE xxabrl_navi_ar_receipt_stg2
           SET OF_Customer_Number  = Trim(V_Customer_Number),
               customer_id         = Trim(V_Cust_Account_Id),
               customer_site_id    = Trim(V_site_use_id),
               Receipt_Method      = Trim(NVL(V_Receipt_Method,Receipt_Method)),
               Bank_Account_Number = NVL(V_Bank_Account_num,Bank_Account_Number),
               Invoice_Number      = V_Invoice_Number,
               Amount_Applied      = V_Amount_Applied,
               Org_Id              = P_Org_Id,
               REMIT_BANK_ACC_ID   = v_REMIT_BANK_ACCT_USE_ID,
               Interfaced_Flag     = 'V'
         WHERE ROWID = ARR_R1.ROWID;
        COMMIT;
        v_OK_Rec_count := v_OK_Rec_count + 1;
        fnd_file.put_line(fnd_file.output, v_record_count || '--');
        fnd_file.put_line(fnd_file.output,
                          ARR_R1.Receipt_Number || '-->' ||
                          '-->Data without error');
      END IF;
    END LOOP;
    IF V_Error_Count > 0 THEN
      P_RetCode := 1;
    END IF;
    fnd_file.put_line(fnd_file.output,
                      '........................................................................');
    fnd_file.put_line(fnd_file.output,
                      'Number of Records with error :' || V_Error_Count);
    fnd_file.put_line(fnd_file.output,
                      'Number of Valid Records :' || v_OK_Rec_count);
    fnd_file.put_line(fnd_file.output,
                      '........................................................................');
    ----
    -- Calling Receipt Insert prog
    ----
    IF p_action ='N' AND V_Error_Count = 0 THEN

       --Create to Receipt

       -- v_gl_date ADDED BY SHAILESH ON 12 FEB 2009 FOR GL_DATE PARAMETER
       CUST_RECEIPT_INSERT(P_Org_Id, P_Data_Source,v_gl_date);

       --######## TEST Code
       --######## TEST Code

    END IF;

    IF p_apply_all_flag = 'Y' THEN

       FOR v_Cur_Apply IN (SELECT DISTINCT sales_transaction_number FROM
                           XXABRL_NAVI_AR_RECEIPT_STG2
                           WHERE
                           interfaced_flag in('C','EA')
                           --23-03-11 aadded conditoin to pick up receipts per application which are with interfaced_flag  'EA'  
                           AND GL_DATE=NVL(v_gl_date,GL_DATE) -- ADDED BY SHAILESH ON 12 FEB 2009 FOR GL DATE PARAMETER
                           AND org_id = Fnd_Profile.VALUE('ORG_ID'))
       LOOP
       -- ,V_GL_DATE ADDED BY SHAILESH ON 12 FEB 2009 FOR GL_DATE
           CUST_RECEIPT_APPLY_CALL(P_Ref_Number => v_Cur_Apply.sales_transaction_number,P_GL_DATE=>V_GL_DATE);
       END LOOP;

    ELSIF p_apply_all_flag = 'N' THEN

       IF p_receipt_to_apply IS NOT NULL THEN
        -- ,V_GL_DATE ADDED BY SHAILESH ON 12 FEB 2009 FOR GL_DATE
          CUST_RECEIPT_APPLY_CALL(P_Ref_Number => p_receipt_to_apply,P_GL_DATE=>V_GL_DATE);
       END IF;

    END IF;


  END CUST_RECEIPT_VALIDATE;


  PROCEDURE CUST_RECEIPT_APPLY_DATA(
                               ARR_R2 IN xxabrl_navi_ar_receipt_stg2%ROWTYPE
                               ,p_Rcpt_Amt_To_Apply NUMBER
                               ,x_Rcpt_Bal_Amt OUT NUMBER
                               ,x_exit_flag OUT VARCHAR2
                               )
  AS

    CURSOR Cur_Open_Inv(p_Interface_Header_Attribute1 VARCHAR2) IS
SELECT *
  FROM
  (
  SELECT
    AMOUNT_DUE_ORIGINAL,
    AMOUNT_DUE_REMAINING,
    rcta.TRX_NUMBER,
    rcta.TRX_DATE
    FROM
    Ra_Customer_Trx_ALL rcta
    ,Ar_Payment_Schedules_All apsa
    WHERE rcta.customer_trx_id = apsa.customer_trx_id
    AND apsa.AMOUNT_DUE_REMAINING    <>0
    AND rcta.Interface_Header_Attribute1 = p_Interface_Header_Attribute1
    ORDER BY AMOUNT_DUE_ORIGINAL
    )
    WHERE ROWNUM = 1;
/*    select
    AMOUNT_DUE_ORIGINAL,
    AMOUNT_DUE_REMAINING
    from
    Ra_Customer_Trx_All rcta
    ,Ar_Payment_Schedules_All apsa
    where rcta.customer_trx_id = apsa.customer_trx_id
    and apsa.AMOUNT_DUE_REMAINING    <>0
    and rcta.Interface_Header_Attribute1 = p_Interface_Header_Attribute1
    and rownum = 1
    order by AMOUNT_DUE_ORIGINAL
    ;*/

    l_return_status   VARCHAR2(1);
    l_msg_count       NUMBER;
    l_msg_data        VARCHAR2(240);
    l_count           NUMBER;
    l_cash_receipt_id NUMBER;
    l_msg_data_out    VARCHAR2(2000);
    l_mesg            VARCHAR2(240);
    p_count           NUMBER;
    l_msg_index_out   NUMBER := 0;
    V_Ok_Receipt      NUMBER := 0;
    V_Error_Receipt   NUMBER := 0;
    v_Rcpt_Amt_To_Apply NUMBER :=0;
    v_Open_Inv NUMBER :=0;
  BEGIN

  BEGIN
    SELECT
      COUNT(1)
      INTO v_Open_Inv
            FROM
      RA_CUSTOMER_TRX_ALL/*_GT*/ rcta
      ,Ar_Payment_Schedules_All apsa
      WHERE rcta.customer_trx_id = apsa.customer_trx_id
      AND apsa.AMOUNT_DUE_REMAINING    <>0
      AND rcta.Interface_Header_Attribute1 = ARR_R2.sales_transaction_number;

      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'Opn.Inv.Cnt: '||NVL(v_Open_Inv,0));

      IF  v_Open_Inv = 0 THEN
          x_Rcpt_Bal_Amt := p_Rcpt_Amt_To_Apply;
          x_exit_flag :='Z'; --Force Exit
      END IF;

  EXCEPTION
  WHEN OTHERS THEN
  FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'Error while deriving Open Invoice Count ');
  END;



  FOR open_inv IN Cur_Open_Inv(ARR_R2.sales_transaction_number)
  LOOP

      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'Applying Inv# '||open_inv.TRX_NUMBER);

      IF open_inv.AMOUNT_DUE_REMAINING < 0 THEN
      -- Credit Note Invoice -ve (i.e. Amount -100)
         v_Rcpt_Amt_To_Apply := open_inv.AMOUNT_DUE_ORIGINAL;

      ELSIF  open_inv.AMOUNT_DUE_REMAINING >= p_Rcpt_Amt_To_Apply THEN
      -- Invoice >= Receipt Amt same
         v_Rcpt_Amt_To_Apply := p_Rcpt_Amt_To_Apply;

      ELSIF  (open_inv.AMOUNT_DUE_REMAINING < p_Rcpt_Amt_To_Apply) AND (open_inv.AMOUNT_DUE_REMAINING>0) THEN
      -- Invoice < Receipt Amt same
         v_Rcpt_Amt_To_Apply := open_inv.AMOUNT_DUE_REMAINING;

      END IF;

      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,':Rcpt_Amt_Avail '||p_Rcpt_Amt_To_Apply);
      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,':Inv_Amt_To_Apply '||v_Rcpt_Amt_To_Apply);


      ar_receipt_api_pub.APPLY (
                p_api_version            => 1.0,
                p_init_msg_list          => FND_API.G_TRUE,
                p_commit                 => fnd_api.g_false,
                p_validation_level       => fnd_api.g_valid_level_full,
                x_return_status          => l_return_status,
                x_msg_count              => l_msg_count,
                x_msg_data               => l_msg_data,
                ----
                p_trx_number             => open_inv.TRX_NUMBER,
                p_receipt_number         => ARR_R2.Receipt_Number,
                p_amount_applied         => v_Rcpt_Amt_To_Apply, --<<<<<<<<
--                p_amount_applied_from    => ARR_R2.RECEIPT_AMOUNT,
                p_apply_date             => ARR_R2.deposited_date --open_inv.TRX_DATE
                /*,
                p_gl_date                => ARR_R2.Gl_Date*/
          );

          IF l_return_status <> FND_API.G_RET_STS_SUCCESS THEN
            FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                              '***Error in Receipt Apply #: ' || ARR_R2.RECEIPT_NUMBER);
            IF l_msg_count >= 1 THEN
              l_msg_data_out := l_msg_data_out || ' ~ ' || l_msg_data;
              FOR i IN 0 .. l_msg_count LOOP
                FND_MSG_PUB.GET(p_msg_index     => i,
                                p_encoded       => 'F',
                                p_data          => l_msg_data,
                                p_msg_index_out => l_msg_index_out);
                l_msg_data_out := l_msg_data_out || ' ~ ' || l_msg_data;
              END LOOP;
            END IF;
            V_Error_Receipt := V_Error_Receipt + 1;
            -----
            -- Update error message
            -----
            UPDATE xxabrl_navi_ar_receipt_stg2
               SET INTERFACED_FLAG = 'EA', ERROR_MESSAGE = l_msg_data_out    --EA: Error in Apply
             WHERE Receipt_Number = ARR_R2.Receipt_Number;
            COMMIT;
            FND_FILE.PUT_LINE(FND_FILE.OUTPUT, '***API Error: '||l_msg_data_out);

          ELSE
            -----
            -- Update interfaced flag
            -----
            UPDATE xxabrl_navi_ar_receipt_stg2
               SET INTERFACED_FLAG = 'A'     --Successfully APPLIED
             WHERE Receipt_Number = ARR_R2.Receipt_Number;
            V_OK_Receipt := V_OK_Receipt + 1;
            FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                              '##Receipt(Inv) Applied Successfully : ' || ARR_R2.RECEIPT_NUMBER||'('||open_inv.TRX_NUMBER||')');


            COMMIT;
          END IF;

          x_exit_flag :='N'; --Default

          IF open_inv.AMOUNT_DUE_REMAINING < 0 THEN
          -- Credit Note Invoice -ve (i.e. Amount -100)
             IF l_return_status = FND_API.G_RET_STS_SUCCESS THEN
                  x_Rcpt_Bal_Amt := ABS(open_inv.AMOUNT_DUE_ORIGINAL) + p_Rcpt_Amt_To_Apply;
                  x_exit_flag :='N'; --Force Exit
             ELSE
                  x_Rcpt_Bal_Amt := 0; --abs(open_inv.AMOUNT_DUE_ORIGINAL) + p_Rcpt_Amt_To_Apply;
                  x_exit_flag :='Y'; --Force Exit
             END IF;

          ELSIF  open_inv.AMOUNT_DUE_REMAINING >= p_Rcpt_Amt_To_Apply THEN
          -- Invoice >= Receipt Amt same
             x_Rcpt_Bal_Amt := 0;
             x_exit_flag :='N'; --Force Exit

          ELSIF  (open_inv.AMOUNT_DUE_REMAINING < p_Rcpt_Amt_To_Apply) AND (open_inv.AMOUNT_DUE_REMAINING>0) THEN
          -- Invoice < Receipt Amt same
             x_Rcpt_Bal_Amt := p_Rcpt_Amt_To_Apply - open_inv.AMOUNT_DUE_REMAINING;
             x_exit_flag :='N'; --Force Exit
          END IF;

          FND_FILE.PUT_LINE(FND_FILE.OUTPUT,':Rcpt_Amt_Balance '||x_Rcpt_Bal_Amt);

      END LOOP;
  END CUST_RECEIPT_APPLY_DATA;

  PROCEDURE CUST_RECEIPT_APPLY_CALL(P_Ref_Number IN VARCHAR2,P_GL_DATE IN DATE)
  AS

  CURSOR Cur_Rcpt(CP_GL_DATE DATE) IS
  SELECT * FROM
 xxabrl_navi_ar_receipt_stg2
  WHERE
  interfaced_flag in('C','EA')
  --23-03-11 aadded conditoin to pick up receipts per application which are with interfaced_flag  'EA' 
  AND GL_DATE = NVL(CP_GL_DATE,GL_DATE)
  AND sales_transaction_number =P_Ref_Number
  ORDER BY nvl(deposited_amount,receipt_amount) DESC
  ; -- Create Cash done

  v_Rcpt_Amt_To_Apply NUMBER :=0;
  v_Rcpt_Bal_Amt NUMBER :=0;
  v_exit_flag VARCHAR2(2) :='N';
  BEGIN

  FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'::::::::::::::::::::::::::::::::::::::');
  FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'Sales Transaction Number(i.e. Inv/Recpt Common #): '||P_Ref_Number);

  FOR c_Cur_Rcpt IN Cur_Rcpt(P_GL_DATE)
  LOOP

      v_Rcpt_Amt_To_Apply := nvl(c_Cur_Rcpt.deposited_amount,c_Cur_Rcpt.receipt_amount); ---receipt_amount;

      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'1. Receipt# (Amount) '||c_Cur_Rcpt.Receipt_Number||' ('||c_Cur_Rcpt.receipt_amount||')');

      LOOP

          --Rollback here, Get Status Flag per receipt / Commit etc
          CUST_RECEIPT_APPLY_DATA(c_Cur_Rcpt,v_Rcpt_Amt_To_Apply,v_Rcpt_Bal_Amt,v_exit_flag);
          v_Rcpt_Amt_To_Apply :=v_Rcpt_Bal_Amt;

          IF v_exit_flag = 'Z' THEN
             FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'Opn.Inv.Cnt# Zero');
          END IF;

          EXIT WHEN (v_Rcpt_Bal_Amt <=0 OR v_exit_flag IN('Y','Z'));
          --Exit when 1=1;

      END LOOP;

      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'........................................');

  END LOOP;

  END CUST_RECEIPT_APPLY_CALL;

   -- P_GL_DATE ADDED BY SHAILESH ON 12 FEB 2009 FOR GL_DATE PARAMETER
  PROCEDURE CUST_RECEIPT_INSERT(P_Org_Id IN NUMBER, P_Data_Source IN VARCHAR2,P_GL_DATE IN DATE) AS
    --
    -- Declaring cursor for selecting valid receipt transaction
    --
     -- CP_GL_DATE ADDED BY SHAILESH ON 12 FEB 2009 FOR GL_DATE PARAMETER
    CURSOR ARR_C2(CP_GL_DATE DATE) IS
      SELECT ROWID
             ,DATA_SOURCE
             ,RECEIPT_NUMBER
             ,OPERATING_UNIT
             ,RECEIPT_DATE
             ,DEPOSIT_DATE
             ,GL_DATE
             ,CURRENCY_CODE
             ,EXCHANGE_RATE_TYPE
             ,EXCHANGE_RATE
             ,EXCHANGE_DATE
             ,nvl(deposited_amount,Receipt_Amount)receipt_amount
             ,ER_Customer_Number
             ,ER_DC_CODE
             ,COMMENTS
             ,ER_TENDER_TYPE
             ,Sales_Transaction_Number
             ,OF_Customer_Number
             ,Receipt_Method
             ,Bank_Account_Name
             ,Bank_Account_Number
             ,Invoice_Number
             ,Customer_Id
             ,Customer_Site_Id
             ,Amount_Applied
             ,REMIT_BANK_ACC_ID
             ,ACTUAL_AMOUNT
             ,DEPOSITED_AMOUNT
             ,DEPOSITED_DATE
             ,OPENING_BALANCE
             ,CLOSING_BALANCE
             ,NET_CHANGE_AT_STORE
        FROM xxabrl_navi_ar_receipt_stg2 
       WHERE UPPER(trim(DATA_SOURCE)) = UPPER(trim(P_DATA_SOURCE))
         AND Org_Id = P_Org_Id
         AND nvl(deposited_amount,Receipt_Amount) <> 0
          -- CP_GL_DATE ADDED BY SHAILESH ON 12 FEB 2009 FOR GL_DATE PARAMETER
         AND GL_DATE=NVL(CP_GL_DATE,GL_DATE)
         AND NVL(INTERFACED_FLAG, 'N') IN ('V','EC')
         AND FREEZE_FLAG='Y';
         ----- UPDATED BY RAVI AND MITUL NOT TO MOVE THE FREEZED DATA FOR KASM FROM 01-APR-2011
/*        AND receipt_number NOT IN (               
               SELECT  receipt_number FROM
               APPS.xxabrl_navi_ar_receipt_stg2
               WHERE  1=1
               AND TRUNC(GL_DATE)>'31-MAR-2011' 
               AND operating_unit='TSRL KA SM');  */
          ----- UPDATED BY RAVI AND MITUL       
    v_record_count NUMBER := 0;
    v_attribute_rec         AR_RECEIPT_API_PUB.attribute_rec_type;
    v_attribute_rec_g       AR_RECEIPT_API_PUB.global_attribute_rec_type ;
    l_return_status   VARCHAR2(1);
    l_msg_count       NUMBER;
    l_msg_data        VARCHAR2(240);
    l_count           NUMBER;
    l_cash_receipt_id NUMBER;
    l_msg_data_out    VARCHAR2(2000);
    l_mesg            VARCHAR2(240);
    p_count           NUMBER;
    l_msg_index_out   NUMBER := 0;
    V_Ok_Receipt      NUMBER := 0;
    V_Error_Receipt   NUMBER := 0;
  BEGIN

/*    begin
         mo_global.set_policy_context('S',84);
         arp_global.init_global(84);
         arp_standard.init_standard(84);
    end;

*/

  -- P_GL_DATE ADDED BY SHAILESH ON 12 FEB 2009 FOR GL_DATE PARAMETER
    FOR ARR_R2 IN ARR_C2(P_GL_DATE) LOOP
      EXIT WHEN ARR_C2%NOTFOUND;
      v_record_count := ARR_C2%ROWCOUNT;
      l_msg_data_out := NULL;
      --=======================
      --  Declaring Record Varibule for Attributes 
      --==========================
        v_attribute_rec.attribute_category:=NULL; 
        v_attribute_rec.attribute1:=ARR_R2.ACTUAL_AMOUNT;
           v_attribute_rec.attribute2:=ARR_R2.DEPOSITED_AMOUNT;
        v_attribute_rec.attribute3:=ARR_R2.DEPOSITED_DATE;
        v_attribute_rec.attribute4:=ARR_R2.OPENING_BALANCE;
           v_attribute_rec.attribute5:=ARR_R2.CLOSING_BALANCE;
        v_attribute_rec.attribute6:=ARR_R2.NET_CHANGE_AT_STORE;
        v_attribute_rec.attribute7:=NULL;
        v_attribute_rec.attribute8:=NULL;
        v_attribute_rec.attribute9:=NULL;
        v_attribute_rec.attribute10:=NULL;
        v_attribute_rec.attribute11:=NULL;
        v_attribute_rec.attribute12:=NULL;
        v_attribute_rec.attribute13:=NULL;
        v_attribute_rec.attribute14:=NULL;
        v_attribute_rec.attribute15:=NULL;
      ----
      -- Receipt API to create and Apply recipt
      ----
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'P_Org_Id : ' || P_Org_Id);
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'p_currency_code               =>: ' || ARR_R2.CURRENCY_CODE);
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'p_amount                      =>: ' || ARR_R2.RECEIPT_AMOUNT);
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'p_receipt_number              =>: ' || ARR_R2.RECEIPT_NUMBER);
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'p_receipt_date                =>: ' || ARR_R2.DEPOSITED_DATE);
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'p_gl_date                     =>: ' || ARR_R2.GL_DATE);
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'p_customer_id                 =>: ' || ARR_R2.Customer_Id);
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'p_customer_site_use_id        =>: ' || ARR_R2.Customer_Site_Id);
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'p_deposit_date                =>: ' || ARR_R2.DEPOSIT_DATE);
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'p_receipt_method_name         =>: ' || ARR_R2.Receipt_Method);
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'p_remittance_bank_account_num =>: ' || ARR_R2.Bank_Account_Number);
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'p_receipt_comments            =>: ' || ARR_R2.COMMENTS);
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'p_trx_number                  =>: ' || ARR_R2.Invoice_Number);
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'p_amount_applied              =>: ' || ARR_R2.Amount_Applied);
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'p_apply_date                  =>: ' || ARR_R2.GL_DATE);
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'p_apply_gl_date               =>: ' || ARR_R2.GL_DATE);
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'p_cr_id                       =>: ' || l_cash_receipt_id);
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'p_org_id                      =>: ' || P_Org_Id);

     ar_receipt_api_pub.create_cash (
         p_api_version                  => 1.0
        ,p_init_msg_list                => fnd_api.g_true
        ,p_commit                       => fnd_api.g_false
        ,p_validation_level             => fnd_api.g_valid_level_full
        ,x_return_status                => l_return_status
        ,x_msg_count                    => l_msg_count
        ,x_msg_data                     => l_msg_data
        ,p_currency_code                => ARR_R2.CURRENCY_CODE
        ,p_amount                       => ARR_R2.Receipt_Amount
        ,p_receipt_number               => ARR_R2.RECEIPT_NUMBER
        ,p_receipt_date                 => ARR_R2.DEPOSITED_DATE
        ,p_cr_id                        => l_cash_receipt_id  --OUT
        ,p_receipt_method_name          => ARR_R2.Receipt_Method
        ,p_customer_number              => ARR_R2.OF_CUSTOMER_NUMBER --Customer_Id
        ,p_comments                     => ARR_R2.COMMENTS
--        ,p_customer_receipt_reference   => SUBSTR(p_rhp_receipt.sender_to_receiver_info,1,30)
        ,p_remittance_bank_account_id   => ARR_R2.REMIT_BANK_ACC_ID
        ,p_attribute_rec                => v_attribute_rec
        ,p_global_attribute_rec         => v_attribute_rec_g
      );



      IF l_return_status <> FND_API.G_RET_STS_SUCCESS THEN
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                          'Receipt Number : ' || ARR_R2.RECEIPT_NUMBER ||
                          ' Error while creating receipt.');
        IF l_msg_count >= 1 THEN
          l_msg_data_out := l_msg_data_out || ' ~ ' || l_msg_data;
          FOR i IN 0 .. l_msg_count LOOP
            FND_MSG_PUB.GET(p_msg_index     => i,
                            p_encoded       => 'F',
                            p_data          => l_msg_data,
                            p_msg_index_out => l_msg_index_out);
            l_msg_data_out := l_msg_data_out || ' ~ ' || l_msg_data;
          END LOOP;
        END IF;
        V_Error_Receipt := V_Error_Receipt + 1;
        -----
        -- Update error message
        -----
        UPDATE xxabrl_navi_ar_receipt_stg2 
           SET INTERFACED_FLAG = 'EC', ERROR_MESSAGE = l_msg_data_out    -- EC: Error in Creation
         WHERE ROWID = ARR_R2.ROWID;
        COMMIT;
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT, l_msg_data_out);
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                          '-----------------------------------------');
      ELSE
        -----
        -- Update interfaced flag
        -----
        UPDATE xxabrl_navi_ar_receipt_stg2 
           SET INTERFACED_FLAG = 'C'
         WHERE ROWID = ARR_R2.ROWID;
        V_OK_Receipt := V_OK_Receipt + 1;
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                          'Receipt Number : ' || ARR_R2.RECEIPT_NUMBER ||
                          '  created successfully.');
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                          '-----------------------------------------');
        COMMIT;
      END IF;
    END LOOP;
    FND_FILE.PUT_LINE(FND_FILE.OUTPUT, 'Receipt Created :' || V_OK_Receipt);
    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                      'Error Receipt d :' || V_Error_Receipt);
    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                      '-----------------------------------------');
  END CUST_RECEIPT_INSERT;
END xxabrl_nav_ar_recpt_cust_pkg; 
/


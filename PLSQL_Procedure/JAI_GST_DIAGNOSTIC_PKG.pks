CREATE OR REPLACE PACKAGE APPS.jai_gst_diagnostic_pkg AUTHID CURRENT_USER
AS
-- $Header jai_gst_dgeneral_pkg.sql,Version 3 2017/07/14  ALLSHAIK  Exp $
   PROCEDURE il_main (
      errbuf        OUT      VARCHAR2,
      retcode       OUT      VARCHAR2,
      p_diag_type   IN       VARCHAR2,
      p_param_id1   IN       VARCHAR2,
      p_param_id2   IN       VARCHAR2  DEFAULT NULL,
      p_param_id    IN       VARCHAR2  DEFAULT NULL,
      p_param_id3   IN       VARCHAR2, -- SO Header Id
      p_param_id4   IN       VARCHAR2, -- SO Line Id
      p_param_id5   IN       VARCHAR2,
      p_param_id6   IN       VARCHAR2,
      p_param_id7   IN       VARCHAR2,
      p_param_id8   IN       VARCHAR2
   );

   PROCEDURE jai_locaplist (
      errbuf         OUT      VARCHAR2,
      retcode        OUT      VARCHAR2,
      p_param_id1    IN       VARCHAR2,
      p_param_val1   IN       VARCHAR2 DEFAULT 'Invoice_Id',
      p_param_id2    IN       VARCHAR2,
      p_param_val2   IN       VARCHAR2 DEFAULT 'Vendor_Id',
      p_param_id3    IN       VARCHAR2,
      p_param_val3   IN       VARCHAR2,
      p_param_id4    IN       VARCHAR2,
      p_param_val4   IN       VARCHAR2 DEFAULT NULL
   );

   PROCEDURE jai_locporct (
      errbuf         OUT      VARCHAR2,
      retcode        OUT      VARCHAR2,
      p_param_id1    IN       VARCHAR2,
      p_param_val1   IN       VARCHAR2 DEFAULT 'PO_Number',
      p_param_id2    IN       VARCHAR2,
      p_param_val2   IN       VARCHAR2 DEFAULT 'Organization_Id',
      p_param_id3    IN       VARCHAR2,
      p_param_val3   IN       VARCHAR2,
      p_param_id4    IN       VARCHAR2,
      p_param_val4   IN       VARCHAR2 DEFAULT NULL
   );

   PROCEDURE jai_locomlist (
      errbuf         OUT      VARCHAR2,
      retcode        OUT      VARCHAR2,
      p_param_id1    IN       VARCHAR2,
      p_param_val1   IN       VARCHAR2 DEFAULT 'Order Header ID',
      p_param_id2    IN       VARCHAR2,
      p_param_val2   IN       VARCHAR2 DEFAULT 'Org ID',
      p_param_id3    IN       VARCHAR2,
      p_param_val3   IN       VARCHAR2,
      p_param_id4    IN       VARCHAR2,
      p_param_val4   IN       VARCHAR2 DEFAULT 'Order Line ID'
   );

   PROCEDURE jai_locarlist (
      errbuf         OUT      VARCHAR2,
      retcode        OUT      VARCHAR2,
      p_param_id1    IN       VARCHAR2,
      p_param_val1   IN       VARCHAR2 DEFAULT 'Customer Trx ID',
      p_param_id2    IN       VARCHAR2,
      p_param_val2   IN       VARCHAR2 DEFAULT 'Organization_Id',
      p_param_id3    IN       VARCHAR2,
      p_param_val3   IN       VARCHAR2,
      p_param_id4    IN       VARCHAR2,
      p_param_val4   IN       VARCHAR2 DEFAULT NULL
   );

   PROCEDURE jai_locfalist (
      errbuf         OUT      VARCHAR2,
      retcode        OUT      VARCHAR2,
      p_param_id1    IN       VARCHAR2,
      p_param_val1   IN       VARCHAR2 DEFAULT 'Block_id',
      p_param_id2    IN       VARCHAR2,
      p_param_val2   IN       VARCHAR2 DEFAULT 'Start_date',
      p_param_id3    IN       VARCHAR2,
      p_param_val3   IN       VARCHAR2 DEFAULT 'End_Date',
      p_param_id4    IN       VARCHAR2,
      p_param_val4   IN       VARCHAR2 DEFAULT NULL
   );

   PROCEDURE jai_locstatus (
      errbuf         OUT      VARCHAR2,
      retcode        OUT      VARCHAR2,
      p_param_id1    IN       VARCHAR2,
      p_param_val1   IN       VARCHAR2 DEFAULT NULL,
      p_param_id2    IN       VARCHAR2,
      p_param_val2   IN       VARCHAR2 DEFAULT NULL,
      p_param_id3    IN       VARCHAR2,
      p_param_val3   IN       VARCHAR2 DEFAULT NULL,
      p_param_id4    IN       VARCHAR2,
      p_param_val4   IN       VARCHAR2 DEFAULT NULL
   );
END jai_gst_diagnostic_pkg; 
/


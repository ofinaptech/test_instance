CREATE OR REPLACE PACKAGE APPS.XXABRL_INVALID_GRN_DETAIL_PKG is
/* ***********************************************************************************************
  --                              Aditya Birla Retail Limited
  ************************************************************************************************* 
  =========================================================================================================
  ||   Filename     : XXABRL_INVALID_GRN_DETAIL_PKG.sql
  ||   Description : Script is used to get Instances of GRN been done after cut off period of 26th of every month.
  ||
  ||   Version     Date            Author              Modification
  ||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
  ||   1.0.0       28-AUG-2012    Vikash Kumar        New Development
  
  ========================================================================================================*/
PROCEDURE XXABRL_INVALID_GRN_PRC
(                                                      errbuf  out  VARCHAR2, -- is a Out parameter which is used to store error message whenever a program gets into an exception block
                                                       retcode  out  NUMBER,    -- is a Out parameter which is used to record the status of the concurrent request
                                                       P_from_date varchar2
                                                       --P_to_date date
                                                     );
  
end XXABRL_INVALID_GRN_DETAIL_PKG; 
/


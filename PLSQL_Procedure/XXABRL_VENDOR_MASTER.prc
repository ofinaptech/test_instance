CREATE OR REPLACE PROCEDURE APPS.XXABRL_VENDOR_MASTER(errbuf out varchar2,
                                 retcode out varchar2
                                 )
AS
/*********************************************************************************************************
                 WIPRO Infotech Ltd, Mumbai, India

 Object Name : XXABRL_VENDOR_MASTER

 Description : Vendor Master dump for HM 

  Change Record:
 =========================================================================================================
 Version    Date            Author                              Remarks
 =======   ==========     =============                         ==========================================
 1.0      07-Oct-11       Narasimhulu (Wipro Infotech Ltd)      New Development
***********************************************************************************************************/
V_L_FILE         VARCHAR2(200);   
V_FINANCE_YEAR   NUMBER;                                    
CURSOR CUR_PERIOD_NAME IS 
 select 
hr.NAME,
POVS.ORG_ID,
POV.VENDOR_ID,
POVS.VENDOR_SITE_ID,
POV.SEGMENT1 VENDOR_NUMBER,
POV.VENDOR_NAME,
POV.VENDOR_NAME_ALT ALTERNATE_VENDOR_NAME,
POV.VENDOR_TYPE_LOOKUP_CODE VENDOR_TYPE,
POVS.PAY_GROUP_LOOKUP_CODE PRIMARY_VENDOR_CATEGORY,
POVS.VENDOR_SITE_CODE SITE_NAME,
    decode(nvl(povs.pay_site_flag,'N')||','||nvl(povs.rfq_only_site_flag,'N')||','||nvl(povs.purchasing_site_flag,'N'),'Y,Y,Y','Payment,Purchasing,RFQ',
      'N,Y,N','RFQ','N,N,Y','Purchasing','Y,N,N','Payment','Y,Y,N','Payment,RFQ','N,Y,Y','RFQ,Purchasing',
       'Y,N,Y','Payment,Purchasing') site_purpose,
(SELECT iepm.PAYMENT_METHOD_CODE 
FROM apps.iby_external_payees_all iby,apps.iby_ext_party_pmt_mthds iepm
WHERE 
iby.ext_payee_id = iepm.ext_pmt_party_id and
IBY.SUPPLIER_SITE_ID=povs.vendor_site_id and
iepm.primary_flag='Y'
) PAYMENT_METHOD_CODE,
povs.ATTRIBUTE1 Navision_Vendor_Ref,
    povs.ATTRIBUTE2 Vendor_Bank_Name,
    povs.ATTRIBUTE3 Vendor_Branch_Name,
    povs.ATTRIBUTE4 Vendor_Account_Number,
    povs.ATTRIBUTE5 Contact_First_Name,
    povs.ATTRIBUTE6 Contact_Last_Name,
    povs.ATTRIBUTE7 Beneficiary_NEFT_IFSC_Code,
    povs.ATTRIBUTE8 Beneficiary_RTGS_IFSC_Code,
    povs.ATTRIBUTE9 Beneficiary_Account_Type,
    povs.ATTRIBUTE10 Payable_Location,
    povs.ATTRIBUTE11 Beneficiary_Email_ID,
    povs.ATTRIBUTE12 Beneficiary_Email_ID2,
    povs.ATTRIBUTE13 Beneficiary_Email_ID3
from 
apps.po_vendors pov,
apps.po_vendor_sites_all povs,
apps.hr_operating_units hr
where pov.VENDOR_ID=povs.VENDOR_ID
and povs.ORG_ID=hr.ORGANIZATION_ID
AND povs.pay_site_flag='Y'
AND HR.NAME LIKE '%HM%'
AND povs.INACTIVE_DATE IS NULL
AND pov.end_date_active IS NULL;
x_id utl_file.file_type;
BEGIN    
   SELECT 'XXABRL_VENDOR_MASTER'||'_'||'V'||'.txt'INTO V_L_FILE FROM dual;  
  --SELECT 'XXABRL_HYPN_MKTNG_'||p_period_name||'_DATA'||NVL(TO_CHAR(TO_DATE(sysdate,'DD-MON-YYYY'),'DDMMYY'),TO_CHAR(SYSDATE-3,'DDMMYY'))||'.csv' INTO V_L_FILE FROM dual;
  x_id:=utl_file.fopen('VENDOR_MASTER_DUMP',V_L_FILE,'W');
utl_file.put_line(x_id,  'NAME'||'|'|| 
                              'ORG_ID'||'|'||                                 
                              'VENDOR_ID'||'|'||
                              'VENDOR_SITE_ID'||'|'|| 
                              'VENDOR_NUMBER'||'|'|| 
                              'VENDOR_NAME'||'|'||
                              'ALTERNATE_VENDOR_NAME'||'|'||
                              'VENDOR_TYPE'||'|'||
                              'PRIMARY_VENDOR_CATEGORY'||'|'||
                             'SITE_NAME'||'|'||                                 
                              'site_purpose'||'|'||
                              'PAYMENT_METHOD_CODE'||'|'|| 
                              'Navision_Vendor_Ref'||'|'||
                              'Vendor_Bank_Name'||'|'||
                              'Vendor_Branch_Name'||'|'||
                              'Vendor_Account_Number'||'|'||
                              'Contact_First_Name'||'|'||
                              'Contact_Last_Name'||'|'||
                              'Beneficiary_NEFT_IFSC_Code'||'|'||
                              'Beneficiary_RTGS_IFSC_Code'||'|'||
                              'Beneficiary_Account_Type'||'|'||
                              'Payable_Location'||'|'||
                              'Beneficiary_Email_ID'||'|'||
                              'Beneficiary_Email_ID2'||'|'||
                              'Beneficiary_Email_ID3'
                              );
   FOR r IN CUR_PERIOD_NAME LOOP
    utl_file.put_line(x_id,   r.NAME||'|'||
                              r.ORG_ID||'|'||                                 
                              r.VENDOR_ID||'|'||
                              r.VENDOR_SITE_ID||'|'|| 
                              r.VENDOR_NUMBER||'|'|| 
                              r.VENDOR_NAME||'|'||
                              r.ALTERNATE_VENDOR_NAME||'|'||
                              r.VENDOR_TYPE||'|'||
                              r.PRIMARY_VENDOR_CATEGORY||'|'||
                              r.SITE_NAME||'|'||                                 
                              r.site_purpose||'|'||
                              r.PAYMENT_METHOD_CODE||'|'|| 
                              r.Navision_Vendor_Ref||'|'||
                              r.Vendor_Bank_Name||'|'||
                              r.Vendor_Branch_Name||'|'||
                              r.Vendor_Account_Number||'|'||
                              r.Contact_First_Name||'|'||
                              r.Contact_Last_Name||'|'||
                              r.Beneficiary_NEFT_IFSC_Code||'|'||
                              r.Beneficiary_RTGS_IFSC_Code||'|'||
                               r.Beneficiary_Account_Type||'|'||
                              r.Payable_Location||'|'||
                              r.Beneficiary_Email_ID||'|'||
                              r.Beneficiary_Email_ID2||'|'||
                              r.Beneficiary_Email_ID3
                              );
   END LOOP;
EXCEPTION 
 WHEN TOO_MANY_ROWS THEN
    dbms_output.PUT_LINE('Too Many Rows');    
 WHEN NO_DATA_FOUND THEN
    dbms_output.PUT_LINE('No Data Found');    
 WHEN utl_file.invalid_path THEN
    RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_path');
 WHEN utl_file.invalid_mode THEN
     RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_mode');
 WHEN utl_file.invalid_filehandle THEN
     RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_filehandle');
 WHEN utl_file.invalid_operation THEN
     RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_operation');
 WHEN utl_file.read_error THEN
     RAISE_APPLICATION_ERROR(-20001, 'utl_file.read_error');
 WHEN utl_file.write_error THEN
     RAISE_APPLICATION_ERROR(-20001, 'utl_file.write_error');
 WHEN utl_file.internal_error THEN
     RAISE_APPLICATION_ERROR(-20001, 'utl_file.internal_error');
 WHEN OTHERS THEN
      dbms_output.put_line(SubStr('Error '||TO_CHAR(SQLCODE)||': '||SQLERRM, 1,255));             
utl_file.fclose(x_id);
END XXABRL_VENDOR_MASTER; 
/


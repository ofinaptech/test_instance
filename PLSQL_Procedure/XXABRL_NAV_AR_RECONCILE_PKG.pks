CREATE OR REPLACE PACKAGE APPS.XXABRL_NAV_AR_RECONCILE_PKG IS

PROCEDURE XXABRL_NAV_AR_AMT_DIFF_PROC(p_from_trx_date in varchar2,p_to_trx_date  in varchar2,p_org_id in number) ;

 PROCEDURE MAIN_PROC(x_err_buf           OUT VARCHAR2
                      ,x_ret_code          OUT NUMBER
            ,p_from_trx_date in varchar2
            ,p_to_trx_date  in varchar2
            ,p_org_id in number);

            
        

END XXABRL_NAV_AR_RECONCILE_PKG; 
/


CREATE OR REPLACE PACKAGE BODY APPS.xxabrl_pay_rec_pkg
AS
/*********************************************************************************************************
                 OBJECT INFORMATION

 Object Name : Xxabrl_Pay_Rec_Pkg

 Description : Report to Show the status of each and every payment

  Change Record:
 ===========================================================================================================
 Version    Date                                   Author                                                                                           Remarks
 ======   ==========              =============                                                =====================================
  1.1.1      17-Nov-09                Praveen Kumar (Wipro Infotech Ltd)                    Changed the logic to get all the Payment Status
  1.1.2      27-Feb-2020           Lokesh Poojari                                                         Added the extra columns (ou_name,invoice_voucher_num,grn_date,invoice_gl_date,source_name,vendor_type)
1.1.3      15-Jun-2021           Suraj Pawar                                                        Added the extra columns (Goods receiving date,Invoice_date,Credit period,Supplier_site_name)
**************************************************************************************************************/
   PROCEDURE xxabrl_pay_rec_proc (
      errbuff               OUT      VARCHAR2,
      retcode               OUT      NUMBER,
      --  P_LEDGER IN VARCHAR2,
      p_from_supplier       IN       VARCHAR2,
      p_to_supplier         IN       VARCHAR2,
      p_list_of_supp        IN       VARCHAR2,
      p_payment_from_date   IN       DATE,
      p_payment_to_date     IN       DATE,
      p_bank_account_num    IN       VARCHAR2,
      p_reconcile_status    IN       VARCHAR2
   )
   IS
      CURSOR cur_org_id (cp_resp_id NUMBER)
      IS
         SELECT aa.organization_id
           FROM (SELECT organization_id
                   FROM fnd_profile_option_values fpop,
                        fnd_profile_options fpo,
                        per_security_organizations_v pso
                  WHERE fpop.profile_option_id = fpo.profile_option_id
                    AND level_value = cp_resp_id
                    AND pso.security_profile_id = fpop.profile_option_value
                    AND profile_option_name = 'XLA_MO_SECURITY_PROFILE_LEVEL'
                 UNION
                 SELECT TO_NUMBER (fpop.profile_option_value) organization_id
                   FROM fnd_profile_option_values fpop,
                        fnd_profile_options fpo
                  WHERE fpop.profile_option_id = fpo.profile_option_id
                    AND level_value = cp_resp_id
                    AND profile_option_name = 'ORG_ID') aa;

      query_string            LONG;
      ledger_string           LONG;
      range_supp_string       LONG;
      list_supp_string        LONG;
      range_pay_date_string   LONG;
      pay_date_string         LONG;
      bank_account_string     LONG;
      recon_status_string     LONG;
      list_org_id             LONG;
      para_org_id             LONG;
      prev_check_number       ap_checks_all.check_number%TYPE            := '';
      prev_payment_doc_name   ce_payment_documents.payment_document_name%TYPE
                                                                         := '';
      prev_account_num        ap_checks_all.bank_account_num%TYPE        := '';
      v_respid                NUMBER          := fnd_profile.VALUE ('RESP_ID');

      TYPE ref_cur IS REF CURSOR;

      c                       ref_cur;

      TYPE c_rec IS RECORD (
         v_ou_name                   org_organization_definitions.organization_name%TYPE,
         ----added by Lokesh Poojari on '26-Feb-2020'
         v_source_name               ap_invoices_all.SOURCE%TYPE,
         ----added by Lokesh Poojari on '26-Feb-2020'
         v_bank_account              ap_checks_all.bank_account_name%TYPE,
         v_bank_acc_num              ap_checks_all.bank_account_num%TYPE,
         v_vendor_number             ap_suppliers.segment1%TYPE,
         v_vendor_name               ap_suppliers.vendor_name%TYPE,
         v_supplier_type             ap_suppliers.vendor_type_lookup_code%TYPE,
         ----added by Lokesh Poojari on '26-Feb-2020'
         v_pay_group                 ap_invoices_all.pay_group_lookup_code%TYPE,
         v_payment_method            ap_invoices_all.payment_method_code%TYPE,
         v_check_number              ap_checks_all.check_number%TYPE,
         v_payment_document_name     ce_payment_documents.payment_document_name%TYPE,
         v_payment_date              ap_invoice_payments_all.accounting_date%TYPE,
         v_payment_amount            ap_checks_all.amount%TYPE,
         v_invoice_gl_date           ap_invoices_all.gl_date%TYPE,
         ----added by Lokesh Poojari on '26-Feb-2020'
         v_invoice_voucher_num       ap_invoices_all.doc_sequence_value%TYPE,
         ----added by Lokesh Poojari on '26-Feb-2020'
         v_invoice_num               ap_invoices_all.invoice_num%TYPE,
         v_invoice_amount            ap_invoices_all.invoice_amount%TYPE,
         v_recon_status              ap_checks_all.status_lookup_code%TYPE,
         v_reconciliation_document   ap_checks_all.check_voucher_num%TYPE,
         v_bank_debit_date           ap_checks_all.cleared_date%TYPE,
      --   v_grn_date                  rcv_shipment_headers.creation_date%TYPE,
         v_org_id                    ap_invoices_all.org_id%TYPE,
      ----added by Lokesh Poojari on '26-Feb-2020'
      v_goods_received_date   ap_invoices_all.goods_received_date%TYPE,
         v_invoice_date             ap_invoices_all.invoice_date%TYPE,
         v_name                       ap_terms_tl.name%type,                          
         v_vendor_site_code      ap_supplier_sites_all.vendor_site_code%type,
         v_retek_code      ap_supplier_sites_all.ATTRIBUTE14%type
      );

      v_rec                   c_rec;
   BEGIN
      query_string :=
         'SELECT *
  FROM (SELECT DISTINCT ood.organization_name ou_name, ai.SOURCE source_name,
                        ac.bank_account_name bank_account,
                        NVL (ac.bank_account_num,
                             (SELECT bank_account_number
                                FROM apps.cebv_bank_accounts cba
                               WHERE 1 = 1
                                 --CBA.BANK_ACCOUNT_ID = AC.BANK_ACCOUNT_ID
                                 AND cba.bank_account_name =
                                                          ac.bank_account_name
                                 AND ROWNUM = 1)
                            ) account_num,
                        aps.segment1 supplier_number,
                        aps.vendor_name supplier_name,
                        aps.vendor_type_lookup_code supplier_type,
                        ai.pay_group_lookup_code pay_group,
                        ai.payment_method_code payment_method,
                        ac.check_number cheque_number,
                        pd.payment_document_name payment_document_name,
                        aip.accounting_date payment_date,
                        aip.amount payment_amount, ai.gl_date invoice_gl_date,
                        ai.doc_sequence_value invoice_voucher_num,
                        ai.invoice_num vend_invoice_num,
                        ai.invoice_amount invoice_amount,
                        ac.status_lookup_code reco_status,
                        ac.doc_sequence_value voucher_number,
                        ac.cleared_date bank_debit_date,                        
--                        (SELECT DISTINCT TRUNC (rsh.creation_date)
--                                    FROM apps.rcv_shipment_headers rsh,
--                                         apps.rcv_transactions rt
--                                   WHERE 1 = 1
--                                     AND rsh.shipment_header_id =
--                                                         rt.shipment_header_id
--                                     AND rt.transaction_id =
--                                                        ail.rcv_transaction_id)
--                                                                     grn_date,
                                                    ai.org_id organization_id,
                                                    ai.GOODS_RECEIVED_DATE,
                                                    ai.INVOICE_DATE,
                                                    att.NAME ,
                                                    apsa.VENDOR_SITE_CODE,
                                                    apsa.ATTRIBUTE14 
                   FROM apps.ap_invoices_all ai,
                        apps.ap_invoice_lines_all ail,
                        apps.org_organization_definitions ood,
                        apps.ap_checks_all ac,
                        apps.ap_invoice_payments_all aip,
                        apps.ap_suppliers aps,
                        apps.ap_supplier_sites_all apsa,
                        apps.ce_payment_documents pd,
                        apps.ap_terms_tl att
                  WHERE ai.invoice_id = aip.invoice_id
                    AND ai.invoice_id = ail.invoice_id
                    AND aip.check_id = ac.check_id
                    AND aps.vendor_id = apsa.vendor_id
                    AND ai.vendor_id = aps.vendor_id
                    AND apsa.vendor_site_id = ai.vendor_site_id
                    AND ac.payment_document_id = pd.payment_document_id(+)
                    AND aps.vendor_id = ac.vendor_id
                    AND ai.vendor_site_id = apsa.vendor_site_id
                    AND apsa.org_id = ai.org_id
                    AND att.TERM_ID = ai.TERMS_ID
--                    AND ail.line_type_lookup_code = ''ITEM''
                    AND ai.org_id = ood.organization_id) aa
 WHERE 1 = 1';
        /* QUERY_STRING:= 'SELECT * FROM (SELECT
           AC.BANK_ACCOUNT_NAME Bank_Account
          ,AC.BANK_ACCOUNT_NUM   Account_Num
          ,PV.SEGMENT1 Supplier_Number
          ,PV.VENDOR_NAME   Supplier_Name
          ,AI.PAY_GROUP_LOOKUP_CODE Pay_Group
          ,AI.PAYMENT_METHOD_CODE   Payment_Method
          ,PD.PAYMENT_DOCUMENT_NAME
          ,AC.CHECK_NUMBER Cheque_Number
          ,AIP.ACCOUNTING_DATE Payment_Date
          ,AC.AMOUNT Payment_Amount
          ,AI.INVOICE_NUM   Vend_Invoice_Num
          ,AI.INVOICE_AMOUNT Invoice_Amount
          ,AC.STATUS_LOOKUP_CODE Reco_Status
          ,AC.CHECK_VOUCHER_NUM Voucher_Number
          ,AC.CLEARED_DATE Bank_Debit_Date
          FROM  AP_INVOICES_ALL AI
          ,ORG_ORGANIZATION_DEFINITIONS OOD
          ,AP_CHECKS_ALL AC
          ,AP_INVOICE_PAYMENTS_ALL AIP
          ,PO_VENDORS PV
          ,PO_VENDOR_SITES_ALL PVS
          ,CE_STATEMENT_HEADERS CSH
          ,CE_STATEMENT_LINES CSL
          ,CE_PAYMENT_DOCUMENTS PD
          WHERE AI.INVOICE_ID = AIP.INVOICE_ID
          AND TO_CHAR(AC.CHECK_NUMBER) = CSL.BANK_TRX_NUMBER
          AND AIP.CHECK_ID=AC.CHECK_ID
          AND CSH.STATEMENT_HEADER_ID = CSL.STATEMENT_HEADER_ID
          AND PV.VENDOR_ID=PVS.VENDOR_ID
          AND   AI.VENDOR_ID=PV.VENDOR_ID
          AND   PVS.VENDOR_SITE_ID=AI.VENDOR_SITE_ID
          AND   PV.VENDOR_ID=AC.VENDOR_ID
          AND   AI.VENDOR_SITE_ID=PVS.VENDOR_SITE_ID
          AND   PVS.ORG_ID=CSH.ORG_ID
          AND   CSH.ORG_ID=OOD.ORGANIZATION_ID
      UNION ALL
        SELECT
           AC.BANK_ACCOUNT_NAME Bank_Account
          ,AC.BANK_ACCOUNT_NUM   Account_Num
          ,PV.SEGMENT1 Supplier_Number
          ,PV.VENDOR_NAME   Supplier_Name
          ,AI.PAY_GROUP_LOOKUP_CODE Pay_Group
          ,AI.PAYMENT_METHOD_CODE   Payment_Method
          ,AC.CHECK_NUMBER Cheque_Number
          ,
          ,AIP.ACCOUNTING_DATE Payment_Date
          ,AC.AMOUNT Payment_Amount
          ,AI.INVOICE_NUM   Vend_Invoice_Num
          ,AI.INVOICE_AMOUNT Invoice_Amount
          ,AC.STATUS_LOOKUP_CODE Reco_Status
          ,AC.CHECK_VOUCHER_NUM Voucher_Number
          ,AC.CLEARED_DATE Bank_Debit_Date
          FROM  AP_INVOICES_ALL AI
          ,ORG_ORGANIZATION_DEFINITIONS OOD
          ,AP_CHECKS_ALL AC
          ,AP_INVOICE_PAYMENTS_ALL AIP
          ,PO_VENDORS PV
          ,PO_VENDOR_SITES_ALL PVS
        WHERE AI.INVOICE_ID = AIP.INVOICE_ID
         AND NOT EXISTS (SELECT CSL.BANK_TRX_NUMBER
                        FROM CE_STATEMENT_LINES CSL
                            ,CE_STATEMENT_HEADERS CSH
                       WHERE TO_CHAR(AC.CHECK_NUMBER) = CSL.BANK_TRX_NUMBER
                         AND CSH.STATEMENT_HEADER_ID = CSL.STATEMENT_HEADER_ID
                         AND PVS.ORG_ID=CSH.ORG_ID
                         AND CSH.ORG_ID=OOD.ORGANIZATION_ID)
          AND AIP.CHECK_ID=AC.CHECK_ID
          AND PV.VENDOR_ID=PVS.VENDOR_ID
          AND   AI.VENDOR_ID=PV.VENDOR_ID
          AND   PVS.VENDOR_SITE_ID=AI.VENDOR_SITE_ID
          AND   PV.VENDOR_ID=AC.VENDOR_ID
          AND   AI.VENDOR_SITE_ID=PVS.VENDOR_SITE_ID
          AND   PVS.ORG_ID=AI.ORG_ID
          AND   AI.ORG_ID=OOD.ORGANIZATION_ID) AA WHERE 1 = 1'; */
      /*QUERY_STRING:=' SELECT
       AC.BANK_ACCOUNT_NAME Bank_Account
      ,AC.BANK_ACCOUNT_NUM   Account_Num
      ,PV.SEGMENT1 Supplier_Number
      ,PV.VENDOR_NAME   Supplier_Name
      ,AI.PAY_GROUP_LOOKUP_CODE Pay_Group
      ,AI.PAYMENT_METHOD_CODE   Payment_Method
      ,AC.CHECK_NUMBER Cheque_Number
      ,AIP.ACCOUNTING_DATE Payment_Date
      ,AC.AMOUNT Payment_Amount
      ,AI.INVOICE_NUM   Vend_Invoice_Num
      ,AI.INVOICE_AMOUNT Invoice_Amount
      ,CSL.STATUS Reco_Status
      ,AC.CHECK_VOUCHER_NUM
      FROM  AP_INVOICES_ALL AI
      ,ORG_ORGANIZATION_DEFINITIONS OOD
      ,AP_CHECKS_V AC
      ,AP_INVOICE_PAYMENTS_ALL AIP
      ,PO_VENDORS PV
      ,PO_VENDOR_SITES_ALL PVS
      ,CE_STATEMENT_HEADERS CSH
      ,CE_STATEMENT_LINES CSL
      WHERE AI.INVOICE_ID = AIP.INVOICE_ID
      AND TO_CHAR(AC.CHECK_NUMBER) = CSL.BANK_TRX_NUMBER(+)
      AND AIP.CHECK_ID=AC.CHECK_ID
      AND CSH.STATEMENT_HEADER_ID(+) = CSL.STATEMENT_HEADER_ID
      AND PV.VENDOR_ID=PVS.VENDOR_ID
      AND   AI.VENDOR_ID=PV.VENDOR_ID
      AND   PVS.VENDOR_SITE_ID=AI.VENDOR_SITE_ID
      AND   PV.VENDOR_ID=AC.VENDOR_ID
      AND   AI.VENDOR_SITE_ID=PVS.VENDOR_SITE_ID
      AND   PVS.ORG_ID(+)=CSH.ORG_ID
      AND   CSH.ORG_ID=OOD.ORGANIZATION_ID(+) ';*/

      --LEDGER_STRING:='AND OOD.SET_OF_BOOKS_ID ='''||P_LEDGER||''' ';
      range_supp_string :=
            ' AND AA.Supplier_Number BETWEEN '''
         || p_from_supplier
         || ''' AND '''
         || p_to_supplier
         || ''' ';
      list_supp_string :=
                    ' AND AA.Supplier_Number IN ( ' || p_list_of_supp || ' ) ';
      range_pay_date_string :=
            ' AND AA.PAYMENT_DATE BETWEEN '''
         || p_payment_from_date
         || ''' AND '''
         || p_payment_to_date
         || ''' ';
      pay_date_string := ' AND AA.PAYMENT_DATE = AA.PAYMENT_DATE ';
      bank_account_string :=
                       'AND AA.ACCOUNT_NUM =''' || p_bank_account_num || ''' ';
      recon_status_string :=
                      ' AND AA.RECO_STATUS =''' || p_reconcile_status || ''' ';
      list_org_id := '';
      para_org_id := ' AND AA.ORGANIZATION_ID in (';

      FOR k IN cur_org_id (v_respid)
      LOOP
         list_org_id := k.organization_id || ',' || NVL (list_org_id, '0');
      END LOOP;

      list_org_id := para_org_id || list_org_id || ') ';
      /* IF(P_LEDGER IS NOT NULL) THEN
      QUERY_STRING:=QUERY_STRING||LEDGER_STRING;
      ELSIF(P_LEDGER IS NULL) THEN
      QUERY_STRING:=QUERY_STRING;
      END IF;    */

      -- IF(p_operating_unit IS NULL) THEN
      query_string := query_string || list_org_id;

       -- ELSE
      ---  query_string :=query_string||op_string;
       -- END IF;
      IF     (p_from_supplier IS NULL OR p_to_supplier IS NULL)
         AND p_list_of_supp IS NOT NULL
      THEN
         query_string := query_string || list_supp_string;
      ELSIF     (p_from_supplier IS NOT NULL AND p_to_supplier IS NOT NULL)
            AND p_list_of_supp IS NULL
      THEN
         query_string := query_string || range_supp_string;
      ELSIF     (p_from_supplier IS NOT NULL AND p_to_supplier IS NOT NULL)
            AND p_list_of_supp IS NOT NULL
      THEN
         query_string := query_string || list_supp_string;
      END IF;

      IF (p_payment_from_date IS NOT NULL AND p_payment_to_date IS NOT NULL)
      THEN
         query_string := query_string || range_pay_date_string;
      ELSIF (p_payment_from_date IS NULL AND p_payment_to_date IS NULL)
      THEN
         query_string := query_string || pay_date_string;
      END IF;

      IF p_bank_account_num IS NOT NULL
      THEN
         query_string := query_string || bank_account_string;
      ELSIF p_bank_account_num IS NULL
      THEN
         query_string := query_string;
      END IF;

      IF p_reconcile_status IS NOT NULL
      THEN
         query_string := query_string || recon_status_string;
      ELSIF p_reconcile_status IS NULL
      THEN
         query_string := query_string || 'ORDER BY AA.RECO_STATUS DESC ';
      END IF;

      fnd_file.put_line (fnd_file.LOG, 'string: ' || query_string);
      fnd_file.put_line (fnd_file.output,
                         'MRL PAYMENT RECONCILIATION STATUS REPORT'
                        );
      fnd_file.put_line (fnd_file.output, ' ');
      fnd_file.put_line (fnd_file.output, ' ');
      fnd_file.put_line (fnd_file.output, CHR (9) || 'REPORT PARAMETERS ');
      /*  Fnd_File.put_line(Fnd_File.OUTPUT,
        CHR(9)  || 'Ledger ' || CHR(9) ||
      P_LEDGER|| CHR(9));*/
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || 'Bank Account '
                         || CHR (9)
                         || p_bank_account_num
                         || CHR (9)
                        );

      IF     (p_from_supplier IS NULL OR p_to_supplier IS NULL)
         AND p_list_of_supp IS NOT NULL
      THEN
         fnd_file.put_line (fnd_file.output,
                               CHR (9)
                            || 'Supplier List '
                            || CHR (9)
                            || p_list_of_supp
                            || CHR (9)
                           );
      ELSIF     (p_from_supplier IS NOT NULL AND p_to_supplier IS NOT NULL)
            AND p_list_of_supp IS NULL
      THEN
         fnd_file.put_line (fnd_file.output,
                               CHR (9)
                            || 'Supplier From '
                            || CHR (9)
                            || p_from_supplier
                            || CHR (9)
                            || 'Supplier To '
                            || CHR (9)
                            || p_to_supplier
                            || CHR (9)
                           );
      ELSIF     (p_from_supplier IS NOT NULL AND p_to_supplier IS NOT NULL)
            AND p_list_of_supp IS NOT NULL
      THEN
         fnd_file.put_line (fnd_file.output,
                               CHR (9)
                            || 'Supplier List '
                            || CHR (9)
                            || p_list_of_supp
                            || CHR (9)
                           );
      END IF;

      IF (p_payment_from_date IS NOT NULL AND p_payment_to_date IS NOT NULL)
      THEN
         fnd_file.put_line (fnd_file.output,
                               CHR (9)
                            || 'Payment Date From '
                            || p_payment_from_date
                            || CHR (9)
                            || 'Payment Date To '
                            || p_payment_to_date
                            || CHR (9)
                           );
      END IF;

      fnd_file.put_line (fnd_file.output,
                         CHR (9) || 'As On Date' || CHR (9) || SYSDATE
                         || CHR (9)
                        );

      IF p_reconcile_status IS NOT NULL
      THEN
         fnd_file.put_line (fnd_file.output,
                               CHR (9)
                            || 'Reconciliation Status'
                            || CHR (9)
                            || p_reconcile_status
                            || CHR (9)
                           );
      END IF;

      fnd_file.put_line (fnd_file.output, ' ');
      fnd_file.put_line (fnd_file.output, ' ');
      fnd_file.put_line (fnd_file.output,
                            'OU Name'
                         -----added by Lokesh Poojari on '26-Feb-2020'
                         || CHR (9)
                         || 'Source Name'
                         -----added by Lokesh Poojari on '26-Feb-2020'
                         || CHR (9)
                         || 'Bank Account Name'
                         || CHR (9)
                         || 'Bank Account Num'
                         || CHR (9)
                         || 'Retek Code'
                         || CHR (9)
                         || 'Supplier Number'
                         || CHR (9)
                         || 'Supplier Name'
                         || CHR (9)
                         || 'Supplier Site Name'
                         || CHR (9)
                         || 'Credit Period'
                         || CHR (9)
                         || 'Supplier Type'
                         -----added by Lokesh Poojari on '26-Feb-2020'
                         || CHR (9)
                         || 'Pay Group'
                         || CHR (9)
                         || 'Payment Method'
                         || CHR (9)
                         || 'Check Number'
                         || CHR (9)
                         || 'Payment Document Name'
                         || CHR (9)
                         || 'Payment Date'
                         || CHR (9)
                         || 'Payment Amount'
                         || CHR (9)
                         || 'Invoice GL Date'
                         ------added by Lokesh Poojari on '26-Feb-2020'
                         || CHR (9)
                         || 'Invoice Voucher Num'
                         ------added by Lokesh Poojari on '26-Feb-2020'
                         || CHR (9)
                         || 'Supplier Invoice Num'
                         || CHR (9)
                         || 'Invoice Date'
                         || CHR (9)
                         || 'Invoice Amount'
                         || CHR (9)
                         || 'Reconciliation Status'
                         || CHR (9)
                         || 'Voucher Number'
                         || CHR (9)
                         || 'Goods Receing Date'
                         || CHR (9)
                         || 'Bank_Debit_Date'
--                         || CHR (9)
--                         || 'GRN Date'
--                        ------added by Lokesh Poojari on '26-Feb-2020'
                        );

      OPEN c FOR query_string;

      LOOP
         FETCH c
          INTO v_rec;

         EXIT WHEN c%NOTFOUND;
         /*IF prev_check_number = v_rec.V_CHECK_NUMBER and
            Prev_Payment_Doc_Name = v_rec.V_PAYMENT_DOCUMENT_NAME and
            Prev_Account_Num  = v_rec.V_BANK_ACC_NUM then

            Fnd_File.put_line(Fnd_File.OUTPUT,
              v_rec.V_BANK_ACCOUNT || CHR(9) ||
              v_rec.V_BANK_ACC_NUM || CHR(9) ||
              v_rec.V_VENDOR_NUMBER || CHR(9) ||
              v_rec.V_VENDOR_NAME    || CHR(9) ||
              v_rec.V_PAY_GROUP    || CHR(9) ||
              v_rec.V_PAYMENT_METHOD   || CHR(9) ||
              v_rec.V_CHECK_NUMBER  || CHR(9) ||
              v_rec.V_PAYMENT_DOCUMENT_NAME  || CHR(9) ||
              v_rec.V_PAYMENT_DATE || CHR(9) ||
              ''|| CHR(9) ||
              v_rec.V_INVOICE_NUM || CHR(9) ||
              v_rec.V_INVOICE_AMOUNT || CHR(9) ||
              v_rec.V_RECON_STATUS || CHR (9)||
              v_rec.V_RECONCILIATION_DOCUMENT || CHR (9)||
              v_rec.V_BANK_DEBIT_DATE);

            prev_check_number := v_rec.V_CHECK_NUMBER;
            Prev_Payment_Doc_Name := v_rec.V_PAYMENT_DOCUMENT_NAME;
            Prev_Account_Num  := v_rec.V_BANK_ACC_NUM;

         ELSE */
         fnd_file.put_line
             (fnd_file.output,
                 v_rec.v_ou_name -----added by Lokesh Poojari on '26-Feb-2020'
              || CHR (9)
              || v_rec.v_source_name
              -----added by Lokesh Poojari on '26-Feb-2020'
              || CHR (9)
              || v_rec.v_bank_account
              || CHR (9)
              || v_rec.v_bank_acc_num
              || CHR (9)
              || v_rec.v_retek_code
              || CHR (9)
              || v_rec.v_vendor_number
              || CHR (9)
              || v_rec.v_vendor_name
              || CHR (9)
              || v_rec.v_vendor_site_code
              || CHR (9)
              || v_rec.v_name
              || CHR (9)
              || v_rec.v_supplier_type
              -----added by Lokesh Poojari on '26-Feb-2020'
              || CHR (9)
              || v_rec.v_pay_group
              || CHR (9)
              || v_rec.v_payment_method
              || CHR (9)
              || v_rec.v_check_number
              || CHR (9)
              || v_rec.v_payment_document_name
              || CHR (9)
              || v_rec.v_payment_date
              || CHR (9)
              || v_rec.v_payment_amount
              || CHR (9)
              || v_rec.v_invoice_gl_date
              ------added by Lokesh Poojari on '26-Feb-2020'
              || CHR (9)
              || v_rec.v_invoice_voucher_num
              ------added by Lokesh Poojari on '26-Feb-2020'
              || CHR (9)
              || v_rec.v_invoice_num
              || CHR (9)
              || v_rec.v_invoice_date
              || CHR (9)
              || v_rec.v_invoice_amount
              || CHR (9)
              || v_rec.v_recon_status
              || CHR (9)
              || v_rec.v_reconciliation_document
              || CHR (9)
              || v_rec.v_goods_received_date
              || CHR (9)
              || v_rec.v_bank_debit_date
--              || CHR (9)
--              || v_rec.v_grn_date
--             ------added by Lokesh Poojari on '26-Feb-2020'
             );
       /*prev_check_number := v_rec.V_CHECK_NUMBER;
       Prev_Payment_Doc_Name := v_rec.V_PAYMENT_DOCUMENT_NAME;
       Prev_Account_Num  := v_rec.V_BANK_ACC_NUM;
      END IF;*/
      END LOOP;

      CLOSE c;
   END xxabrl_pay_rec_proc;
END xxabrl_pay_rec_pkg;
/


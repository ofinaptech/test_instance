CREATE OR REPLACE PACKAGE APPS.xxabrl_baan_ap_bkdt_load_pkg
IS
   PROCEDURE xxabrl_wait_for_req_prc1 (p_request_id NUMBER, p_req_name VARCHAR2);

   PROCEDURE xxabrl_baan_ap_dt_load_prc (
      errbuf    OUT   VARCHAR2,
      retcode   OUT   NUMBER
   );
   
END xxabrl_baan_ap_bkdt_load_pkg; 
/


CREATE OR REPLACE PACKAGE APPS.XXABRL_SMDN_INV_INTERFACE_N IS
       PROCEDURE debit_note_validate_inv_n(Errbuf       OUT VARCHAR2
                                  ,RetCode      OUT NUMBER);
       PROCEDURE DEBIT_NOTE_INSERT_TRANS_N(p_org_id IN NUMBER,
                             p_vendor_site_id IN NUMBER,
                             p_vendor_id IN NUMBER,
                             p_payment_method_lookup_code IN VARCHAR2);
END XXABRL_SMDN_INV_INTERFACE_N; 
/


CREATE OR REPLACE PACKAGE BODY APPS.xxabrl_baan_to_ar_main
IS
   PROCEDURE main_proc (x_err_buf OUT VARCHAR2, x_ret_code OUT NUMBER)
   IS
      h_total_value                 NUMBER          := 0;
      h_net_tax_amt                 NUMBER          := 0;
      h_net_value                   NUMBER          := 0;
      h_net_promo_disc              NUMBER          := 0;
      h_roundoff_amt                NUMBER          := 0;
      ----
      d_total_value                 NUMBER          := 0;
      d_net_tax_amt                 NUMBER          := 0;
      d_net_value                   NUMBER          := 0;
      d_net_promo_disc              NUMBER          := 0;
      ---
      v_valid_count                 NUMBER          := 0;
      v_invalid_count               NUMBER          := 0;
      v_total_count                 NUMBER          := 0;
      ----
      xx_categoryname               VARCHAR2 (1000);
      xx_abrl_departmentvalue       VARCHAR2 (1000);
      xx_transaction_type           VARCHAR2 (1000);
      xx_gl_id_rev                  NUMBER;
      ------
      v_error_flag                  VARCHAR2 (1)    := NULL;
      v_error_msg                   VARCHAR2 (1000) := NULL;
      v_customer_code               VARCHAR2 (100);
      v_ou_short_code               VARCHAR2 (100);
      v_org_id                      NUMBER;
      v_interface_line_attribute2   NUMBER          := 0;
      v_tax_desc                    VARCHAR2 (100);
      ------
      v_err_buf                     VARCHAR2 (4000);
      v_ret_code                    NUMBER;
      v_segment1                    VARCHAR2 (100)  := '11';
      v_segment2                    VARCHAR2 (100)  := '000';
      v_segment7                    VARCHAR2 (100)  := '000';
      v_segment8                    VARCHAR2 (100)  := '0000';
      v_merchandize                 VARCHAR2 (100);
      v_account                     VARCHAR2 (100);
      v_location                    VARCHAR2 (100);
      v_sbu                         VARCHAR2 (100);
      v_code_combination_id         NUMBER;
      ---------
      xx_err_buf                    VARCHAR2 (2000);
      xx_ret_code                   NUMBER;
      v1_err_buf                    VARCHAR2 (2000);
      v1_ret_code                   NUMBER;
      v_err_bu                      VARCHAR2 (2000);
      v_ret_cod                     NUMBER;
      ---
      tot_tax_amount                NUMBER          := 0;
      tot_promo_disc_amt            NUMBER          := 0;
      tot_net_value                 NUMBER          := 0;
      -----
      tot_item                      NUMBER          := 0;
      tot_ofin_cat_updated          NUMBER          := 0;
      tot_unknown_cat               NUMBER          := 0;
      v_tax_plan                    NUMBER;
      v_tax_account                 VARCHAR2 (200);
      v_tax_merchandize             VARCHAR2 (10);

      CURSOR xx_baan_main_header
      IS
         SELECT DISTINCT org_cd, bill_dt
                    FROM xxabrl.xxabrl_baan_sales_header
                   WHERE NVL (status_flag, 'N') IN ('N', 'T')
                     AND org_cd IN ('009', '006', '007', '005')
                     AND bill_dt >= '1-mar-16'
                GROUP BY org_cd, bill_dt;

      CURSOR xx_baan_main_details
      IS
         SELECT   ROWID, itm_cd, org_cd, bill_dt
             FROM xxabrl.xxabrl_baan_sales_details
            WHERE 1 = 1
              AND status_flag IN ('V', 'E')
              AND org_cd IN ('009', '006', '007', '005')
              AND bill_dt >= '1-mar-16'
         GROUP BY ROWID, itm_cd, org_cd, bill_dt;

      CURSOR xx_baan_store_days
      IS
         SELECT   org_cd, bill_dt
             FROM xxabrl.xxabrl_baan_sales_details
            WHERE status_flag IN ('V', 'E')
              AND org_cd IN ('009', '006', '007', '005')
              AND bill_dt >= '1-mar-16'
         GROUP BY org_cd, bill_dt;

      CURSOR xx_baan_details (p_org_cd VARCHAR2, p_bill_dt DATE)
      IS
         SELECT s.*
           FROM (SELECT   org_cd, bill_dt,
                          ROUND (SUM (NVL (total_amt, 0)), 2) total_amt,
                          ROUND (SUM (NVL (tax_amt, 0)), 2) tax_amt,
                          ROUND (SUM (NVL (promo_disc_amt, 0)),
                                 2
                                ) promo_disc_amt,
                          ROUND (SUM (NVL (net_value, 0)), 2) net_value,
                            ROUND (SUM (NVL (net_value, 0)), 2)
                          + ROUND (SUM (NVL (promo_disc_amt, 0)), 2)
                          - ROUND (SUM (NVL (tax_amt, 0)), 2) gross_sale,
                          ofin_rollup_category
                     FROM xxabrl.xxabrl_baan_sales_details
                    WHERE 1 = 1
                      AND status_flag IN ('V', 'E')
                      AND org_cd = p_org_cd
                      AND bill_dt = p_bill_dt
                 GROUP BY org_cd, bill_dt, ofin_rollup_category) s;

      CURSOR c_net (
         p3_org_cd                 VARCHAR2,
         p3_bill_dt                DATE,
         p3_ofin_rollup_category   VARCHAR2
      )
      IS
         (SELECT   org_cd, bill_dt, ofin_rollup_category,
                     ROUND (SUM (NVL (net_value, 0)), 2)
                   + ROUND (SUM (NVL (promo_disc_amt, 0)), 2)
                   - ROUND (SUM (NVL (tax_amt, 0)), 2) gross_sale
              FROM xxabrl.xxabrl_baan_sales_details
             WHERE org_cd = p3_org_cd
               AND bill_dt = p3_bill_dt
               AND ofin_rollup_category = p3_ofin_rollup_category
          GROUP BY bill_dt, org_cd, ofin_rollup_category);

      CURSOR c_promo (
         p2_org_cd                 VARCHAR2,
         p2_bill_dt                DATE,
         p2_ofin_rollup_category   VARCHAR2
      )
      IS
         (SELECT   org_cd, bill_dt, ofin_rollup_category,
                   ROUND (SUM (NVL (promo_disc_amt, 0)), 2) promo_disc_amt
              FROM xxabrl.xxabrl_baan_sales_details
             WHERE org_cd = p2_org_cd
               AND bill_dt = p2_bill_dt
               AND ofin_rollup_category = p2_ofin_rollup_category
          GROUP BY bill_dt, org_cd, ofin_rollup_category);

      CURSOR c_tax (p1_org_cd VARCHAR2, p1_bill_dt DATE
                                                       --p1_ofin_rollup_category   VARCHAR2
      )
      IS
         (SELECT   org_cd, bill_dt, ROUND (SUM (NVL (tax_amt, 0)), 2) tax_amt,
                   tax_plan
              FROM xxabrl.xxabrl_baan_sales_details
             WHERE org_cd = p1_org_cd
               AND bill_dt = p1_bill_dt
               -- AND ofin_rollup_category = p1_ofin_rollup_category
               AND tax_plan IS NOT NULL
               AND status_flag = 'P'
          GROUP BY tax_plan, org_cd, bill_dt);

      CURSOR c_roundoff (p4_org_cd VARCHAR2, p4_bill_dt DATE)
      IS
         (SELECT   bill_dt, org_cd, SUM (total_value) hdr_tot,
                   SUM (net_value) hdr_net, SUM (net_promo_disc) hdr_promo,
                   SUM (roundoff_amt) hdr_roundoff,
                     SUM (total_value)
                   - SUM (net_value)
                   - SUM (net_promo_disc)
                   - SUM (roundoff_amt) diff,
                     SUM (total_value)
                   - SUM (net_value)
                   - SUM (net_promo_disc) bala_roundoff
              FROM xxabrl.xxabrl_baan_sales_header
             WHERE 1 = 1
               AND org_cd = p4_org_cd
               AND bill_dt = p4_bill_dt
               AND status_flag = 'V'
          GROUP BY bill_dt, org_cd);
   BEGIN
      BEGIN
         fnd_file.put_line (fnd_file.output,
                            '                              ');
         fnd_file.put_line
            (fnd_file.output,
             '                                Start Amount Validation of Headers and Details Store Day Wise                '
            );

         FOR xx_main_rec IN xx_baan_main_header
         LOOP
            v_total_count := v_total_count + 1;

            ------------Checking for Header Amount-------------------
            BEGIN
               SELECT ROUND (SUM (NVL (total_value, 0)), 2),
                      ROUND (SUM (NVL (net_tax_amt, 0)), 2),
                      ROUND (SUM (NVL (net_value, 0)), 2),
                      ROUND (SUM (NVL (net_promo_disc, 0)), 2),
                      ROUND (SUM (NVL (roundoff_amt, 0)), 2)
                 INTO h_total_value,
                      h_net_tax_amt,
                      h_net_value,
                      h_net_promo_disc,
                      h_roundoff_amt
                 FROM xxabrl.xxabrl_baan_sales_header
                WHERE org_cd = xx_main_rec.org_cd  --org_cd is BAAN Store Code
                  AND bill_dt = xx_main_rec.bill_dt;
            EXCEPTION
               WHEN OTHERS
               THEN
                  fnd_file.put_line (fnd_file.LOG,
                                        'Error in Headers amount :=>'
                                     || SQLCODE
                                     || ':'
                                     || SQLERRM
                                    );
                  x_err_buf :=
                        'Error in Header Amount Validation for :=> '
                     || xx_main_rec.org_cd
                     || xx_main_rec.bill_dt;
                  x_ret_code := 1;
            END;

            ------------Checking for Details Amount-------------------
            BEGIN
               SELECT ROUND (SUM (NVL (total_amt, 0)), 2),
                      ROUND (SUM (NVL (tax_amt, 0)), 2),
                      ROUND (SUM (NVL (net_value, 0)), 2),
                      ROUND (SUM (NVL (promo_disc_amt, 0)), 2)
                 INTO d_total_value,
                      d_net_tax_amt,
                      d_net_value,
                      d_net_promo_disc
                 FROM xxabrl.xxabrl_baan_sales_details
                WHERE org_cd = xx_main_rec.org_cd
                  AND bill_dt = xx_main_rec.bill_dt;
            EXCEPTION
               WHEN OTHERS
               THEN
                  fnd_file.put_line (fnd_file.LOG,
                                        'Error in Lines/Details Amount :=>'
                                     || SQLCODE
                                     || ':'
                                     || SQLERRM
                                    );
                  x_err_buf :=
                        'Error in Detail Amount Validation for :=> '
                     || xx_main_rec.org_cd
                     || xx_main_rec.bill_dt;
                  x_ret_code := 1;
            END;

            fnd_file.put_line (fnd_file.output,
                               'Store_No   :=>' || xx_main_rec.org_cd
                              );
            fnd_file.put_line (fnd_file.output,
                               'Bill_Date   :=>' || xx_main_rec.bill_dt
                              );
            fnd_file.put_line (fnd_file.output,
                               '                              '
                              );
            fnd_file.put_line (fnd_file.output,
                               '-------------Headers Amounts------------'
                              );
            fnd_file.put_line (fnd_file.output,
                                  'Header_Total_Amt        :=>'
                               || ' '
                               || h_total_value
                              );
            fnd_file.put_line (fnd_file.output,
                                  'Header_Net_Tax_Amt   :=>'
                               || ' '
                               || h_net_tax_amt
                              );
            fnd_file.put_line (fnd_file.output,
                                  'Header_Net_Value        :=>'
                               || ' '
                               || h_net_value
                              );
            fnd_file.put_line (fnd_file.output,
                                  'Header_Net_Promo_Disc   :=>'
                               || ' '
                               || h_net_promo_disc
                              );
            fnd_file.put_line (fnd_file.output,
                               '                              '
                              );
            fnd_file.put_line
                          (fnd_file.output,
                           '-------------Lines/Details Amounts-------------- '
                          );
            fnd_file.put_line (fnd_file.output,
                                  'Details_Total_Amt         :=>'
                               || ' '
                               || d_total_value
                              );
            fnd_file.put_line (fnd_file.output,
                                  'Details_Net_Tax_Amt   :=>'
                               || ' '
                               || d_net_tax_amt
                              );
            fnd_file.put_line (fnd_file.output,
                                  'Details_Net_Value        :=>'
                               || ' '
                               || d_net_value
                              );
            fnd_file.put_line (fnd_file.output,
                                  'Details_Net_Promo_Disc   :=>'
                               || ' '
                               || d_net_promo_disc
                              );

            IF       (h_total_value - h_net_value - h_net_promo_disc)
                   - (h_roundoff_amt) BETWEEN -5 AND 5
               AND (d_net_value + d_net_promo_disc) - (h_total_value)
                      BETWEEN -8015
                          AND 8015
            THEN
               v_valid_count := v_valid_count + 1;

               UPDATE xxabrl.xxabrl_baan_sales_header
                  SET status_flag = 'V',            ----V stands for Validated
                      error_msg = NULL
                WHERE org_cd = xx_main_rec.org_cd
                  AND bill_dt = xx_main_rec.bill_dt;

               UPDATE xxabrl.xxabrl_baan_sales_details
                  SET status_flag = 'V',
                      error_msg = NULL
                WHERE org_cd = xx_main_rec.org_cd
                  AND bill_dt = xx_main_rec.bill_dt;

               fnd_file.put_line (fnd_file.output,
                                     'Header and Details Matched for  :=>'
                                  || xx_main_rec.org_cd
                                  || '-'
                                  || xx_main_rec.bill_dt
                                 );
            --DBMS_OUTPUT.put_line ('Header and Details Matched for :');
            ELSE
               v_invalid_count := v_invalid_count + 1;

               UPDATE xxabrl.xxabrl_baan_sales_header
                  SET status_flag = 'T',       ---T stands for Amount Mismatch
                      error_msg =
                            'Amount Mismatch for BAAN Store:-'
                         || xx_main_rec.org_cd
                         || ' and Transaction Date:-'
                         || xx_main_rec.bill_dt
                WHERE org_cd = xx_main_rec.org_cd
                  AND bill_dt = xx_main_rec.bill_dt;

               UPDATE xxabrl.xxabrl_baan_sales_details
                  SET status_flag = 'T',
                      error_msg =
                            'Amount Mismatch for BAAN Store:-'
                         || xx_main_rec.org_cd
                         || ' and Transaction Date:-'
                         || xx_main_rec.bill_dt
                WHERE org_cd = xx_main_rec.org_cd
                  AND bill_dt = xx_main_rec.bill_dt;

               fnd_file.put_line (fnd_file.output,
                                     'Header and Details not Matched of :=>'
                                  || xx_main_rec.org_cd
                                  || '-'
                                  || xx_main_rec.bill_dt
                                 );
            --  DBMS_OUTPUT.put_line ('Header and Details Mismatched for :');
            END IF;

            fnd_file.put_line
                          (fnd_file.output,
                           '-------------------------------------------------'
                          );
            COMMIT;
         END LOOP;

         fnd_file.put_line (fnd_file.output, '                              ');
         fnd_file.put_line (fnd_file.output,
                            'Total Count  :=>' || v_total_count
                           );
         fnd_file.put_line (fnd_file.output,
                            'Total Processed Records  :=>' || v_valid_count
                           );
         fnd_file.put_line (fnd_file.output,
                               'Total Error/Mismatched Records :=>   '
                            || v_invalid_count
                           );
         fnd_file.put_line
               (fnd_file.output,
                ' -----End of Amount Validation of Headers and Details-------'
               );
         fnd_file.put_line (fnd_file.output, '                              ');
      EXCEPTION
         WHEN OTHERS
         THEN
            ROLLBACK;
            x_err_buf :=
                  'Error in Amount Validation of Header and Details :--'
               || 'Reason:  '
               || SQLCODE
               || ':'
               || SQLERRM;
            x_ret_code := 1;
      END;

      BEGIN
         fnd_file.put_line (fnd_file.output,
                            '                              ');
         fnd_file.put_line
            (fnd_file.output,
             '                     Updating OFIN_Rollup_Category                '
            );

--         Cursor 2 is, pick up nvl(error_flag,'V') in ('E','V') from polaris sales detail for each STORE DAY [store sales date]
         FOR xx_main_rec1 IN xx_baan_main_details
         LOOP
            ---Getting Transaction type based on Item Master
            tot_item := tot_item + 1;

            BEGIN
               SELECT mp.transaction_type, mp.description,
                      mas.abrl_department
                 INTO xx_transaction_type, xx_categoryname,
                      xx_abrl_departmentvalue
                 FROM xxabrl.xxabrl_transaction_mapping mp,
                      xxabrl.xxabrl_baan_item_master mas
                WHERE UPPER (mp.description(+)) = UPPER (mas.abrl_department)
                  AND mas.item_no = xx_main_rec1.itm_cd;
            EXCEPTION
               WHEN OTHERS
               THEN
                  xx_categoryname := NULL;
                  xx_abrl_departmentvalue := NULL;
                  xx_transaction_type := NULL;
            END;

            IF xx_categoryname IS NULL
            THEN
               tot_unknown_cat := tot_unknown_cat + 1;

               UPDATE xxabrl.xxabrl_baan_sales_details
                  SET ofin_rollup_category = 'UNKNOWN',
                      error_msg =
                            'ABRL_Departmentvalue:'
                         || xx_abrl_departmentvalue
                         || 'Is Not Existed For this Item:=>'
                         || xx_main_rec1.itm_cd
                         || 'in OFIN',
                      abrl_dept_value = xx_abrl_departmentvalue
                WHERE ROWID = xx_main_rec1.ROWID
                  AND itm_cd = xx_main_rec1.itm_cd
                  AND org_cd = xx_main_rec1.org_cd
                  AND bill_dt = xx_main_rec1.bill_dt;
            ELSE
               tot_ofin_cat_updated := tot_ofin_cat_updated + 1;

               UPDATE xxabrl.xxabrl_baan_sales_details
                  SET ofin_rollup_category = xx_abrl_departmentvalue,
                      abrl_dept_value = xx_abrl_departmentvalue
                WHERE ROWID = xx_main_rec1.ROWID
                  AND itm_cd = xx_main_rec1.itm_cd
                  AND org_cd = xx_main_rec1.org_cd
                  AND bill_dt = xx_main_rec1.bill_dt;
            END IF;

            COMMIT;
         END LOOP;

         fnd_file.put_line (fnd_file.output,
                            'Total Item Processed :=>' || tot_item
                           );
         fnd_file.put_line (fnd_file.output,
                               'Total Ofin Category Updated :=>'
                            || tot_ofin_cat_updated
                           );
         fnd_file.put_line (fnd_file.output,
                               'Total Unknown Categories Updated :=>'
                            || tot_unknown_cat
                           );
         fnd_file.put_line (fnd_file.output,
                            '---------End of Category Updation---------'
                           );
         fnd_file.put_line (fnd_file.output,
                            '                                    '
                           );
      EXCEPTION
         WHEN OTHERS
         THEN
            ROLLBACK;
            x_err_buf :=
                  'Error in category updation Reason:=>'
               || SQLCODE
               || '    :   '
               || SQLERRM;
            x_ret_code := 1;
      END;

      BEGIN
         fnd_file.put_line (fnd_file.output,
                            '                              ');
         fnd_file.put_line
            (fnd_file.output,
             '                            Inserting Intermediate Table Process Start                 '
            );
         xx_categoryname := NULL;

         FOR xx_int_main IN xx_baan_store_days
         LOOP
            v_error_flag := 'N';
            v_error_msg := NULL;
            v_customer_code := NULL;
            v_ou_short_code := NULL;
            v_org_id := NULL;

            BEGIN
               SELECT customer_code, ou_short_code, org_id,
                      LOCATION, sbu
                 INTO v_customer_code, v_ou_short_code, v_org_id,
                      v_location, v_sbu
                 FROM xxabrl.xxabrl_baan_location
                WHERE baan_store_code = xx_int_main.org_cd;
            EXCEPTION
               WHEN OTHERS
               THEN
                  v_error_flag := 'E';
                  v_error_msg :=
                        v_error_msg
                     || 'Error in updating customer_name:=>'
                     || xx_int_main.org_cd
                     || ':'
                     || SQLCODE
                     || ':'
                     || SQLERRM;
            END;

            --   v_interface_line_attribute2 := 0;
            FOR xx_int IN xx_baan_details (xx_int_main.org_cd,
                                           xx_int_main.bill_dt
                                          )
            LOOP
               IF v_error_flag = 'N'
               THEN
                  IF NVL (xx_int.gross_sale, 0) > 0
                  THEN
                     fnd_file.put_line
                        (fnd_file.output,
                         '                                   Gross Details / Promo   Amounts                                      '
                        );

                     FOR c1_net IN c_net (xx_int.org_cd,
                                          xx_int.bill_dt,
                                          xx_int.ofin_rollup_category
                                         )
                     LOOP
                        SELECT apps.xxabrl_tender_resa_seq.NEXTVAL
                          INTO v_interface_line_attribute2
                          FROM DUAL;

                        BEGIN
                           v_merchandize := NULL;
                           v_account := NULL;

                           SELECT merchandize, ACCOUNT
                             INTO v_merchandize, v_account
                             FROM xxabrl.xxabrl_transaction_mapping
                            WHERE UPPER (transaction_type) =
                                                  UPPER ('Store Sale Invoice')
                              AND UPPER (description) =
                                           UPPER (c1_net.ofin_rollup_category);
                        EXCEPTION
                           WHEN OTHERS
                           THEN
                              NULL;
--                           v_error_flag := 'E';
--                           v_error_msg :=
--                                 'Error in MERCHANDIZE, ACCOUNT:=>'
--                              || xx_int.sales_trsaction_type
--                              || ':'
--                              || SQLCODE
--                              || ':'
--                              || SQLERRM;
                        END;

                        BEGIN
                           SELECT code_combination_id
                             INTO v_code_combination_id
                             FROM gl_code_combinations
                            WHERE segment1 = v_segment1
                              AND segment2 = v_segment2
                              AND segment3 = v_sbu
                              AND segment4 = v_location
                              AND segment5 = v_merchandize
                              AND segment6 = v_account
                              AND segment7 = v_segment7
                              AND segment8 = v_segment8;
                        EXCEPTION
                           WHEN OTHERS
                           THEN
                              NULL;
                        END;

                        IF v_error_flag = 'N'
                        THEN
                           fnd_file.put_line (fnd_file.output,
                                              'Store :=>' || c1_net.org_cd
                                             );
                           fnd_file.put_line (fnd_file.output,
                                              'Bill Date :=>'
                                              || c1_net.bill_dt
                                             );
                           fnd_file.put_line (fnd_file.output,
                                                 'OFIN Category :=>'
                                              || c1_net.ofin_rollup_category
                                             );
                           fnd_file.put_line (fnd_file.output,
                                                 'Gross Amount :=>'
                                              || c1_net.gross_sale
                                             );
                           fnd_file.put_line (fnd_file.output,
                                              '                              '
                                             );

                           INSERT INTO xxabrl.xxabrl_baan_intermediate_stg
                                       (interface_line_attribute1,
                                        interface_line_attribute2,
                                        description,
                                        amount,
                                        transaction_type, trx_date,
                                        gl_date,
                                        orig_system_batch_name,
                                        customer_name, org_code,
                                        org_id, tender_amount, status_flag,
                                        error_msg, code_combination_id,
                                        segment1, segment2, segment3,
                                        segment4, segment5, segment6,
                                        segment7, segment8
                                       )
                                VALUES (   TO_CHAR (xx_int.bill_dt,
                                                    'ddmmyyyy')
                                        || '-'
                                        || c1_net.org_cd,
                                        v_interface_line_attribute2,
                                        -- xxabrl_tender_resa_seq.NEXTVAL,
                                        c1_net.ofin_rollup_category,
                                        c1_net.gross_sale,
                                        'Store Sale Invoice', c1_net.bill_dt,
                                        c1_net.bill_dt,
                                           'BAAN'
                                        || '-'
                                        || TO_CHAR (c1_net.bill_dt,
                                                    'ddmmyyyy')
                                        || '-'
                                        || c1_net.org_cd,
                                        v_customer_code, v_ou_short_code,
                                        v_org_id, c1_net.gross_sale, 'N',
                                        NULL, v_code_combination_id,
                                        v_segment1, v_segment2, v_sbu,
                                        v_location, v_merchandize, v_account,
                                        v_segment7, v_segment8
                                       );
                        END IF;
                     END LOOP;

--                     fnd_file.put_line (fnd_file.output,
--                                        '           End   Gross  Details           '
--                                       );
                     fnd_file.put_line
                        (fnd_file.output,
                         '                   --------------------------------        '
                        );
                  END IF;

                  IF v_error_flag = 'N' AND NVL (xx_int.promo_disc_amt, 0) > 0
                  THEN
--                     fnd_file.put_line
--                        (fnd_file.output,
--                         '                                 Promo Details                           '
--                        );
                     BEGIN
                        v_code_combination_id := 0;
                        v_merchandize := '00';
                        v_account := '514001';

                        SELECT code_combination_id
                          INTO v_code_combination_id
                          FROM gl_code_combinations
                         WHERE segment1 = v_segment1
                           AND segment2 = v_segment2
                           AND segment3 = v_sbu
                           AND segment4 = v_location
                           AND segment5 = '00'
                           AND segment6 = '514001'
                           AND segment7 = v_segment7
                           AND segment8 = v_segment8;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           NULL;
                     END;

                     IF v_error_flag = 'N'
                     THEN
                        FOR c1_promo IN c_promo (xx_int.org_cd,
                                                 xx_int.bill_dt,
                                                 xx_int.ofin_rollup_category
                                                )
                        LOOP
                           -- v_interface_line_attribute2 :=
                             --                     v_interface_line_attribute2 + 1;
                           BEGIN
                              SELECT apps.xxabrl_tender_resa_seq.NEXTVAL
                                INTO v_interface_line_attribute2
                                FROM DUAL;
                           END;

                           fnd_file.put_line (fnd_file.output,
                                              'Store :=>' || c1_promo.org_cd
                                             );
                           fnd_file.put_line (fnd_file.output,
                                                 'Bill Date :=>'
                                              || c1_promo.bill_dt
                                             );
                           fnd_file.put_line (fnd_file.output,
                                                 'OFIN Category :=>'
                                              || c1_promo.ofin_rollup_category
                                             );
                           fnd_file.put_line (fnd_file.output,
                                                 'Promo Amount :=>'
                                              || c1_promo.promo_disc_amt
                                             );
                           fnd_file.put_line
                                    (fnd_file.output,
                                     '                                       '
                                    );

                           INSERT INTO xxabrl.xxabrl_baan_intermediate_stg
                                       (interface_line_attribute1,
                                        interface_line_attribute2,
                                        description,
                                        amount,
                                        transaction_type, trx_date,
                                        gl_date,
                                        orig_system_batch_name,
                                        customer_name, org_code,
                                        org_id, tender_amount,
                                        status_flag, error_msg,
                                        code_combination_id, segment1,
                                        segment2, segment3, segment4,
                                        segment5, segment6, segment7,
                                        segment8
                                       )
                                VALUES (   TO_CHAR (c1_promo.bill_dt,
                                                    'ddmmyyyy'
                                                   )
                                        || '-'
                                        || c1_promo.org_cd,
                                        v_interface_line_attribute2,
--                                  xxabrl_tender_resa_seq.NEXTVAL,
                                           c1_promo.ofin_rollup_category
                                        || '-'
                                        || 'Discount',
                                        -- xx_int.ofin_rollup_category,
                                        -c1_promo.promo_disc_amt,
                                        'Store Promotion',
                                                          --xx_int.sales_trsaction_type,
                                                          c1_promo.bill_dt,
                                        c1_promo.bill_dt,
                                           'BAAN'
                                        || '-'
                                        || TO_CHAR (c1_promo.bill_dt,
                                                    'ddmmyyyy'
                                                   )
                                        || '-'
                                        || c1_promo.org_cd,
                                        v_customer_code, v_ou_short_code,
                                        v_org_id, -c1_promo.promo_disc_amt,
                                        'N', NULL,
                                        v_code_combination_id, v_segment1,
                                        v_segment2, v_sbu, v_location,
                                        v_merchandize, v_account, v_segment7,
                                        v_segment8
                                       );
                        END LOOP;
                     END IF;

--                     fnd_file.put_line (fnd_file.output,
--                                        '    End Promo Details         '
--                                       );
                     fnd_file.put_line
                        (fnd_file.output,
                         '                     ------------------------            '
                        );
                  END IF;
               END IF;

               -- END IF;
               IF v_error_flag = 'E'
               THEN
                  UPDATE xxabrl.xxabrl_baan_sales_details
                     SET status_flag = 'E',
                         error_msg = v_error_msg
                   WHERE org_cd = xx_int.org_cd
                     AND bill_dt = xx_int.bill_dt
                     AND ofin_rollup_category = xx_int.ofin_rollup_category;
               --AND abrl_dept_value = xx_int.abrl_dept_value;
               ELSE
                  UPDATE xxabrl.xxabrl_baan_sales_details
                     SET status_flag = 'P'
                   --   error_msg = NULL
                  WHERE  org_cd = xx_int.org_cd
                     AND bill_dt = xx_int.bill_dt
                     AND NVL (ofin_rollup_category, 'TSS') =
                                      NVL (xx_int.ofin_rollup_category, 'TSS');
               --AND NVL (abrl_dept_value, 'TSS') =
                            --         NVL (xx_int.abrl_dept_value, 'TSS');
               END IF;

               COMMIT;
            END LOOP;

            IF v_error_flag = 'N'
            THEN
               --    IF NVL (xx_int.tax_amt, 0) > 0
                 --  THEN
               fnd_file.put_line
                  (fnd_file.output,
                   '                                        Tax Details                                 '
                  );

               FOR c1_tax IN c_tax (xx_int_main.org_cd, xx_int_main.bill_dt
                                                                           --xx_int.ofin_rollup_category
                            )
               LOOP
                  BEGIN
                     SELECT pln_name, tax_account, merchandise
                       INTO v_tax_desc, v_tax_account, v_tax_merchandize
                       FROM xxabrl.xxabrl_baan_tax
                      WHERE pln_tax_cd = c1_tax.tax_plan;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        v_error_flag := 'E';
                        v_error_msg :=
                              'tax_account is not existed for the tax plan'
                           || c1_tax.tax_plan;
                  -- NULL;
                  END;

                  BEGIN
                     v_code_combination_id := 0;

                     SELECT code_combination_id
                       INTO v_code_combination_id
                       FROM gl_code_combinations
                      WHERE segment1 = v_segment1
                        AND segment2 = v_segment2
                        AND segment3 = v_sbu
                        AND segment4 = v_location
                        AND segment5 = v_tax_merchandize
                        AND segment6 = v_tax_account
                        AND segment7 = v_segment7
                        AND segment8 = v_segment8;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        NULL;
--                           v_error_flag := 'E';
--                           v_error_msg :=
--                                 v_error_msg
--                              || 'Error in updating CCID=>'
--                              || v_segment1
--                              || ':'
--                              || v_segment2
--                              || ':'
--                              || v_sbu
--                              || ':'
--                              || v_location
--                              || ':'
--                              || v_merchandize
--                              || ':'
--                              || v_account
--                              || ':'
--                              || v_segment7
--                              || ':'
--                              || v_segment8
--                              || ':'
--                              || SQLCODE
--                              || ':'
--                              || SQLERRM;
                  END;

                  IF v_error_flag = 'N' AND NVL (c1_tax.tax_amt, 0) > 0
                  THEN
                     SELECT apps.xxabrl_tender_resa_seq.NEXTVAL
                       INTO v_interface_line_attribute2
                       FROM DUAL;

                     fnd_file.put_line (fnd_file.output,
                                        'Store :=>' || c1_tax.org_cd
                                       );
                     fnd_file.put_line (fnd_file.output,
                                        'Bill Date :=>' || c1_tax.bill_dt
                                       );
                     fnd_file.put_line (fnd_file.output,
                                           'Tax Plan Description :=>'
                                        || v_tax_desc
                                       );
                     fnd_file.put_line (fnd_file.output,
                                        'Tax Amount:-' || c1_tax.tax_amt
                                       );
                     fnd_file.put_line (fnd_file.output, '             ');

                     INSERT INTO xxabrl.xxabrl_baan_intermediate_stg
                                 (interface_line_attribute1,
                                  interface_line_attribute2, description,
                                  amount, transaction_type,
                                  trx_date, gl_date,
                                  orig_system_batch_name,
                                  customer_name, org_code, org_id,
                                  tender_amount, status_flag, error_msg,
                                  code_combination_id, segment1,
                                  segment2, segment3, segment4,
                                  segment5, segment6,
                                  segment7, segment8
                                 )
                          VALUES (   TO_CHAR (c1_tax.bill_dt, 'ddmmyyyy')
                                  || '-'
                                  || c1_tax.org_cd,
                                  --xxabrl_tender_resa_seq.NEXTVAL,
                                  v_interface_line_attribute2, v_tax_desc,
                                  --xx_int.ofin_rollup_category,
                                  --xx_int.total_amt,
                                  c1_tax.tax_amt, 'Store Sale Invoice',
                                  -- xx_int.sales_trsaction_type,
                                  c1_tax.bill_dt, c1_tax.bill_dt,
                                     'BAAN'
                                  || '-'
                                  || TO_CHAR (c1_tax.bill_dt, 'ddmmyyyy')
                                  || '-'
                                  || c1_tax.org_cd,
                                  v_customer_code, v_ou_short_code, v_org_id,
                                  --xx_int.total_amt,
                                  c1_tax.tax_amt, 'N', NULL,
                                  v_code_combination_id, v_segment1,
                                  v_segment2, v_sbu, v_location,
                                  v_tax_merchandize, v_tax_account,
                                  v_segment7, v_segment8
                                 );
                  END IF;
               END LOOP;

--               fnd_file.put_line (fnd_file.output,
--                                  '      End Tax Details        '
--                                 );
               fnd_file.put_line
                  (fnd_file.output,
                   '             -----------------------------               '
                  );
            END IF;

            fnd_file.put_line (fnd_file.output, '            ');
-------- Start for round off Amount---------------------------------------
            fnd_file.put_line
               (fnd_file.output,
                '                               Round off Details                            '
               );

            IF v_error_flag = 'N'
            THEN
               FOR round_off IN c_roundoff (xx_int_main.org_cd,
                                            xx_int_main.bill_dt
                                           )
               LOOP
                  BEGIN
                     v_code_combination_id := 0;
                     v_merchandize := '00';
                     v_account := '415009';

                     SELECT code_combination_id
                       INTO v_code_combination_id
                       FROM gl_code_combinations
                      WHERE segment1 = v_segment1
                        AND segment2 = v_segment2
                        AND segment3 = v_sbu
                        AND segment4 = v_location
                        AND segment5 = '00'
                        AND segment6 = '415009'
                        AND segment7 = v_segment7
                        AND segment8 = v_segment8;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        NULL;
                  END;

                  BEGIN
                     v_merchandize := NULL;
                     v_account := NULL;

                     SELECT merchandize, ACCOUNT
                       INTO v_merchandize, v_account
                       FROM xxabrl.xxabrl_transaction_mapping
                      WHERE UPPER (transaction_type) =
                                                  UPPER ('Store Sale Invoice')
                        AND UPPER (description) = UPPER ('Round off');
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        NULL;
--                           v_error_flag := 'E';
--                           v_error_msg :=
--                                 'Error in MERCHANDIZE, ACCOUNT:=>'
--                              || xx_int.sales_trsaction_type
--                              || ':'
--                              || SQLCODE
--                              || ':'
--                              || SQLERRM;
                  END;

                  IF     v_error_flag = 'N'
                     AND NVL (round_off.bala_roundoff, 0) <> 0
                  THEN
                     SELECT apps.xxabrl_tender_resa_seq.NEXTVAL
                       INTO v_interface_line_attribute2
                       FROM DUAL;

                     fnd_file.put_line (fnd_file.output,
                                        'Store :=>' || round_off.org_cd
                                       );
                     fnd_file.put_line (fnd_file.output,
                                        'Bill Date :=>' || round_off.bill_dt
                                       );
                     fnd_file.put_line (fnd_file.output,
                                           'Round Off Amount :=>'
                                        || round_off.bala_roundoff
                                       );
                     fnd_file.put_line
                        (fnd_file.output,
                         '                            ----------------------         '
                        );

                     INSERT INTO xxabrl.xxabrl_baan_intermediate_stg
                                 (interface_line_attribute1,
                                  interface_line_attribute2, description,
                                  amount,
                                  transaction_type, trx_date,
                                  gl_date,
                                  orig_system_batch_name,
                                  customer_name, org_code, org_id,
                                  tender_amount, status_flag, error_msg,
                                  code_combination_id, segment1,
                                  segment2, segment3, segment4,
                                  segment5, segment6, segment7,
                                  segment8
                                 )
                          VALUES (   TO_CHAR (round_off.bill_dt, 'ddmmyyyy')
                                  || '-'
                                  || round_off.org_cd,
                                  --xxabrl_tender_resa_seq.NEXTVAL,
                                  v_interface_line_attribute2, 'Round off',
                                  --xx_int.ofin_rollup_category,
                                  --xx_int.total_amt,
                                  round_off.bala_roundoff,
                                  'Store Sale Invoice',
                                                       -- xx_int.sales_trsaction_type,
                                                       round_off.bill_dt,
                                  round_off.bill_dt,
                                     'BAAN'
                                  || '-'
                                  || TO_CHAR (round_off.bill_dt, 'ddmmyyyy')
                                  || '-'
                                  || round_off.org_cd,
                                  v_customer_code, v_ou_short_code, v_org_id,
                                  --xx_int.total_amt,
                                  round_off.bala_roundoff, 'N', NULL,
                                  v_code_combination_id, v_segment1,
                                  v_segment2, v_sbu, v_location,
                                  v_merchandize, v_account, v_segment7,
                                  v_segment8
                                 );
                  END IF;
               END LOOP;
            END IF;

-------- end  for round off Amount---------------------------------------
            COMMIT;
            fnd_file.put_line (fnd_file.output, '                         ');
         END LOOP;

         fnd_file.put_line (fnd_file.output,
                            '--Inserting Intermediate Table Process End-'
                           );
      EXCEPTION
         WHEN OTHERS
         THEN
            ROLLBACK;
            x_err_buf :=
                  'Error in inserting into intermediate table:=>'
               || SQLCODE
               || ':'
               || SQLERRM;
            x_ret_code := 1;
      END;
--      BEGIN
--         baan_intermediate_to_staging (v_err_buf, v_ret_code);
--      END;

   --      BEGIN
--         baan_tender (v_err_bu, v_ret_cod);
--         baan_intermediate_to_stag_t (v1_err_buf, v1_ret_code);
--      END;
   END main_proc;

   PROCEDURE baan_intermediate_to_staging (
      x_err_buf    OUT   VARCHAR2,
      x_ret_code   OUT   NUMBER
   )
   IS
      v_insert_exception   EXCEPTION;
      v_total_count        NUMBER          := 0;
      v_valid_count        NUMBER          := 0;
      v_err_flag           VARCHAR2 (20);
      v_error_msg          VARCHAR2 (4000) := NULL;
      v_line_amount        NUMBER          := 0;
      v_dist_amount        NUMBER          := 0;

      CURSOR xx_intermediate_stg2
      IS
         SELECT   ROWID, interface_line_attribute1,
                  interface_line_attribute2, description,
                  ROUND (SUM (NVL (tender_amount, 0)), 2) tender_amount,
                  ROUND (SUM (NVL (amount, 0)), 2) amount, transaction_type,
                  trx_date, gl_date, orig_system_batch_name, customer_name,
                  org_code, org_id, code_combination_id, segment1, segment2,
                  segment3, segment4, segment5, segment6, segment7, segment8
             FROM xxabrl.xxabrl_baan_intermediate_stg
            WHERE 1 = 1 AND status_flag = 'N'
         GROUP BY ROWID,
                  interface_line_attribute1,
                  interface_line_attribute2,
                  description,
                  transaction_type,
                  trx_date,
                  gl_date,
                  orig_system_batch_name,
                  customer_name,
                  org_code,
                  org_id,
                  code_combination_id,
                  segment1,
                  segment2,
                  segment3,
                  segment4,
                  segment5,
                  segment6,
                  segment7,
                  segment8;
   BEGIN
      fnd_file.put_line
               (fnd_file.output,
                '          Intermediate into staging2 Process Start         '
               );

      FOR xx_inter IN xx_intermediate_stg2
      LOOP
         v_err_flag := 'N';
         v_error_msg := NULL;
         v_total_count := v_total_count + 1;
         v_valid_count := v_valid_count + 1;
         --v_line_amount := 0;
         --v_dist_amount := 0;
         fnd_file.put_line (fnd_file.output,
                            'customer_name    =>' || xx_inter.customer_name
                           );
         DBMS_OUTPUT.put_line ('customer_name    =>' || xx_inter.customer_name);
         fnd_file.put_line (fnd_file.output,
                            'gl_date   =>' || xx_inter.gl_date
                           );
         DBMS_OUTPUT.put_line ('gl_date   =>' || xx_inter.gl_date);
         fnd_file.put_line (fnd_file.output,
                               'interface_line_attribute1   =>'
                            || xx_inter.interface_line_attribute1
                           );
         fnd_file.put_line (fnd_file.output,
                               'interface_line_attribute2   =>'
                            || xx_inter.interface_line_attribute2
                           );
         fnd_file.put_line (fnd_file.output,
                            'Tender Amount    =>' || xx_inter.tender_amount
                           );
         fnd_file.put_line (fnd_file.output, ' Amount  =>' || xx_inter.amount);
         fnd_file.put_line (fnd_file.output,
                               'Transaction Type   =>'
                            || xx_inter.transaction_type
                           );
         fnd_file.put_line (fnd_file.output,
                            'Description   =>' || xx_inter.description
                           );
         fnd_file.put_line (fnd_file.output,
                            'Org Code  =>' || xx_inter.org_code
                           );
         fnd_file.put_line (fnd_file.output,
                            '   --------------------------------------      '
                           );

         BEGIN
            INSERT INTO apps.xxabrl_navi_ar_int_line_stg2
                        (interface_line_context, interface_line_attribute1,
                         interface_line_attribute2, batch_source_name,
                         sob_id, line_type, description, currency_code,
                         tender_amount, amount,
                         transaction_type, trx_date,
                         gl_date, orig_system_batch_name,
                         customer_name, orig_system_bill_address_id,
                         conversion_type, conversion_date, conversion_rate,
                         term_id, attribute_category, attribute1,
                         org_code, created_by, creation_date,
                         last_updated_by, last_update_date, interfaced_flag,
                         freeze_flag, org_id, error_message
                        )
                 VALUES ('ABRL_BAAN', xx_inter.interface_line_attribute1,
                         xx_inter.interface_line_attribute2, 'BAAN',
                         'ABRL LEDGER', 'LINE', xx_inter.description, 'INR',
                         xx_inter.tender_amount, xx_inter.amount,
                         xx_inter.transaction_type, xx_inter.trx_date,
                         xx_inter.gl_date, xx_inter.orig_system_batch_name,
                         xx_inter.customer_name, NULL,
                         'Corporate', NULL, NULL,
                         'IMMEDIATE', 'BAAN Receivables Invoice', NULL,
                         xx_inter.org_code, NULL, SYSDATE,
                         NULL, SYSDATE, 'N',
                         'N', xx_inter.org_id, NULL
                        );

            -- BEGIN
            INSERT INTO apps.xxabrl_navi_ra_int_dist_stg2
                        (interface_distribution_id, interface_line_id,
                         interface_line_context, interface_line_attribute1,
                         interface_line_attribute2, account_class,
                         tender_amount,
                         amount, org_id,
                         description, code_combination_id,
                         segment1, segment2,
                         segment3, segment4,
                         segment5, segment6,
                         segment7, segment8, created_by, creation_date,
                         last_updated_by, last_update_date,
                         last_update_login, interfaced_flag, error_message
                        )
                 VALUES (NULL, NULL,
                         'ABRL_BAAN', xx_inter.interface_line_attribute1,
                         xx_inter.interface_line_attribute2, 'REV',
                         ROUND (xx_inter.tender_amount, 2),
                         ROUND (xx_inter.amount, 2), xx_inter.org_code,
                         xx_inter.description,
                                              --------Processing is pending to fetch segments and code combination id
                                              xx_inter.code_combination_id,
                         xx_inter.segment1, xx_inter.segment2,
                         xx_inter.segment3, xx_inter.segment4,
                         xx_inter.segment5, xx_inter.segment6,
                         xx_inter.segment7, xx_inter.segment8,
-----------------------------------------------------------
                         NULL, SYSDATE,
                         NULL, SYSDATE,
                         NULL, 'N', NULL
                        );

            UPDATE xxabrl.xxabrl_baan_intermediate_stg
               SET status_flag = 'Y',
                   error_msg = v_err_flag
             WHERE ROWID = xx_inter.ROWID;
         EXCEPTION
            WHEN OTHERS
            THEN
               ROLLBACK;
               fnd_file.put_line
                  (fnd_file.output,
                      'Error Raise while inserting(Intermediate to staging2(DIST)) data for BAAN'
                   || SQLERRM
                  );
               v_error_msg :=
                     'error in inserting data from intermediate to Stage2(Dist)'
                  || xx_inter.interface_line_attribute1
                  || xx_inter.interface_line_attribute2;
--                  UPDATE xxabrl.xxabrl_baan_intermediate_stg
--                     SET status_flag = 'E',
--                         error_msg = v_error_msg
--                   WHERE ROWID = xx_inter.ROWID;
               --   RAISE v_insert_exception;
         END;

          --  END IF;
         -- fnd_file.put_line (fnd_file.output,
                           --  'Tender Amount ' || xx_inter.tender_amount
                        --    );
         COMMIT;
      END LOOP;

      fnd_file.put_line (fnd_file.output, 'total_count=>' || v_total_count);
      fnd_file.put_line (fnd_file.output, 'Valid Data=>' || v_valid_count);
      fnd_file.put_line
              (fnd_file.output,
               '           Intermediate into staging2 Process End            '
              );
   EXCEPTION
--      WHEN v_insert_exception
--      THEN
--         fnd_file.put_line
--            (fnd_file.LOG,
--                'Error Raise while inserting(Intermediate to staging2) data for BAAN'
--             || SQLERRM
--            );
--         ROLLBACK;
--         x_ret_code := 1;
      WHEN OTHERS
      THEN
         fnd_file.put_line
                   (fnd_file.LOG,
                       'Exception in BAAN Insert(Intermediate to staging2): '
                    || SQLERRM
                   );
         x_ret_code := 1;
   END baan_intermediate_to_staging;

   PROCEDURE baan_tender (x_err_buf OUT VARCHAR2, x_ret_code OUT NUMBER)
   IS
      v_abrl_transaction_type       VARCHAR2 (200);
      v_insert_exception            EXCEPTION;
      v_interface_line_attribute2   NUMBER;
      v_description                 VARCHAR2 (200);
      v_merchandize                 VARCHAR2 (100);
      v_account                     VARCHAR2 (200);
      v_ou_short_code               VARCHAR2 (200);
      v_org_id                      NUMBER;
      v_customer_code               VARCHAR2 (1000);
      v_segment1                    VARCHAR2 (100)  := '11';
      v_segment2                    VARCHAR2 (100)  := '000';
      v_segment7                    VARCHAR2 (100)  := '000';
      v_segment8                    VARCHAR2 (100)  := '0000';
      v_location                    VARCHAR2 (100);
      v_sbu                         VARCHAR2 (100);
      v_code_combination_id         NUMBER          := 0;
      v_amount                      NUMBER;
      v_error_flag                  VARCHAR2 (20);
      v_error_msg                   VARCHAR2 (4000);
      v_total_count                 NUMBER          := 0;
      v_valid_count                 NUMBER          := 0;
      v_invalid_count               NUMBER          := 0;
      v1_err_buf                    VARCHAR2 (4000);
      v1_ret_code                   NUMBER;

      CURSOR xx_tender_data
      IS
         SELECT DISTINCT btd_org_cd, btd_bill_dt, btd_tender_type_cd
                    FROM xxabrl.xxabrl_baan_ar_tender
                   WHERE 1 = 1
                     AND status_flag IS NULL
                     AND btd_org_cd IN ('009', '006', '005', '007')
                     AND btd_bill_dt >= '1-mar-16'
                     -- AND btd_bill_dt > = '27-feb-16'
                     AND UPPER (btd_tender_type_cd) IN
                            ('COU', 'EXC', 'MKT', 'MK2', 'WWS', 'SOD', 'TIC',
                             'PAT', 'BAJ', 'GC', 'GV', 'CCT', 'VGV')
                GROUP BY btd_org_cd, btd_bill_dt, btd_tender_type_cd;
   BEGIN
      fnd_file.put_line (fnd_file.output,
                         '       Inserting Tender data        '
                        );

      --v_interface_line_attribute2 := 0;
      FOR cur_tender IN xx_tender_data
      LOOP
         v_total_count := v_total_count + 1;

         SELECT apps.xxabrl_tender_resa_seq.NEXTVAL
           INTO v_interface_line_attribute2
           FROM DUAL;

         v_error_flag := 'N';
         v_error_msg := NULL;

         BEGIN
            SELECT abrl_transaction_type
              INTO v_abrl_transaction_type
              FROM xxabrl.xxabrl_baan_ar_tender_types
             WHERE TRIM (UPPER (ttm_code)) =
                                  TRIM (UPPER (cur_tender.btd_tender_type_cd))
               AND receipts IS NULL;
         EXCEPTION
            WHEN OTHERS
            THEN
               fnd_file.put_line (fnd_file.LOG,
                                     ' Invalid Transaction type'
                                  || cur_tender.btd_tender_type_cd
                                  || cur_tender.btd_org_cd
                                  || cur_tender.btd_bill_dt
                                 );
               v_error_flag := 'E';
               v_error_msg :=
                     v_error_msg
                  || ' Invalid Transaction type'
                  || cur_tender.btd_tender_type_cd
                  || cur_tender.btd_org_cd
                  || cur_tender.btd_bill_dt;
         END;

         BEGIN
            SELECT NVL (SUM (btd_conv_amt), 0)
              INTO v_amount
              FROM xxabrl.xxabrl_baan_ar_tender
             WHERE btd_tender_type_cd = cur_tender.btd_tender_type_cd
               AND btd_bill_dt = cur_tender.btd_bill_dt
               AND btd_org_cd = cur_tender.btd_org_cd;
         EXCEPTION
            WHEN OTHERS
            THEN
               NULL;
         END;

         BEGIN
            IF UPPER (v_abrl_transaction_type) = 'STORE CARD'
            THEN
               v_amount := (-v_amount);
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               NULL;
         END;

         BEGIN
            SELECT MAX (description), MAX (merchandize), MAX (ACCOUNT)
              INTO v_description, v_merchandize, v_account
              FROM xxabrl.xxabrl_transaction_mapping
             WHERE UPPER (transaction_type) = UPPER (v_abrl_transaction_type);
         EXCEPTION
            WHEN OTHERS
            THEN
               fnd_file.put_line (fnd_file.LOG,
                                     ' Invalid Account Segments '
                                  || v_abrl_transaction_type
                                  || cur_tender.btd_tender_type_cd
                                  || cur_tender.btd_org_cd
                                  || cur_tender.btd_bill_dt
                                 );
               v_error_flag := 'E';
               v_error_msg :=
                     v_error_msg
                  || ' Invalid Account Segments'
                  || v_abrl_transaction_type
                  || cur_tender.btd_tender_type_cd
                  || cur_tender.btd_org_cd
                  || cur_tender.btd_bill_dt;
         END;

         BEGIN
            SELECT ou_short_code, org_id, customer_code, sbu
              INTO v_ou_short_code, v_org_id, v_customer_code, v_sbu
              FROM xxabrl.xxabrl_baan_location
             WHERE baan_store_code = cur_tender.btd_org_cd;
         EXCEPTION
            WHEN OTHERS
            THEN
               -- NULL;
               v_error_flag := 'E';
               v_error_msg :=
                     v_error_msg
                  || ' Invalid Transaction type'
                  || cur_tender.btd_tender_type_cd
                  || cur_tender.btd_org_cd
                  || cur_tender.btd_bill_dt;
         END;

         BEGIN
            SELECT code_combination_id
              INTO v_code_combination_id
              FROM gl_code_combinations
             WHERE segment1 = v_segment1
               AND segment2 = v_segment2
               AND segment3 = v_sbu
               AND segment4 = v_location
               AND segment5 = v_merchandize
               AND segment6 = v_account
               AND segment7 = v_segment7
               AND segment8 = v_segment8;
         EXCEPTION
            WHEN OTHERS
            THEN
               NULL;
--                           v_error_flag := 'E';
--                           v_error_msg :=
--                                 v_error_msg
--                              || 'Error in updating CCID=>'
--                              || v_segment1
--                              || ':'
--                              || v_segment2
--                              || ':'
--                              || v_sbu
--                              || ':'
--                              || v_location
--                              || ':'
--                              || v_merchandize
--                              || ':'
--                              || v_account
--                              || ':'
--                              || v_segment7
--                              || ':'
--                              || v_segment8
--                              || ':'
--                              || SQLCODE
--                              || ':'
--                              || SQLERRM;
         END;

         ----inserting Tender Data into intermediate table----
         BEGIN
            IF v_error_flag = 'N'
            THEN
               v_valid_count := v_valid_count + 1;
               fnd_file.put_line (fnd_file.output,
                                  'BTD_bill_date :=>'
                                  || cur_tender.btd_bill_dt
                                 );
               fnd_file.put_line (fnd_file.output,
                                  'BTD_org_code :=>' || cur_tender.btd_org_cd
                                 );
               fnd_file.put_line (fnd_file.output,
                                     'btd_tender_type_cd :=>'
                                  || cur_tender.btd_tender_type_cd
                                 );
               fnd_file.put_line (fnd_file.output,
                                     'abrl_transaction_type :=>'
                                  || v_abrl_transaction_type
                                 );
               fnd_file.put_line (fnd_file.output,
                                  'Tender Amount :=>' || v_amount
                                 );
               fnd_file.put_line
                             (fnd_file.output,
                              '           --------------------------         '
                             );

               INSERT INTO xxabrl.xxabrl_baan_intermediate_stg
                           (interface_line_attribute1,
                            interface_line_attribute2, description,
                            amount, transaction_type,
                            trx_date, gl_date,
                            orig_system_batch_name,
                            customer_name, org_code, org_id,
                            tender_amount, status_flag, error_msg,
                            code_combination_id, segment1, segment2,
                            segment3, segment4, segment5, segment6,
                            segment7, segment8
                           )
                    VALUES (   TO_CHAR (cur_tender.btd_bill_dt, 'ddmmyyyy')
                            || '-'
                            || cur_tender.btd_org_cd,
                            v_interface_line_attribute2,
                                                        -- xxabrl_tender_resa_seq.NEXTVAL,
                                                        v_description,
                            -v_amount, v_abrl_transaction_type,
                            cur_tender.btd_bill_dt, cur_tender.btd_bill_dt,
                               'BAAN'
                            || '-'
                            || TO_CHAR (cur_tender.btd_bill_dt, 'ddmmyyyy')
                            || '-'
                            || cur_tender.btd_org_cd,
                            v_customer_code, v_ou_short_code, v_org_id,
                            -v_amount, 'N', NULL,
                            v_code_combination_id, v_segment1, v_segment2,
                            v_sbu, v_customer_code, v_merchandize, v_account,
                            v_segment7, v_segment8
                           );

               UPDATE xxabrl.xxabrl_baan_ar_tender
                  SET status_flag = 'T',
                      error_msg = v_error_msg
                WHERE btd_org_cd = cur_tender.btd_org_cd
                  AND btd_bill_dt = cur_tender.btd_bill_dt
                  AND btd_tender_type_cd = cur_tender.btd_tender_type_cd;

               COMMIT;
            ELSE
               v_invalid_count := v_invalid_count + 1;

               UPDATE xxabrl.xxabrl_baan_ar_tender
                  SET status_flag = 'E',                               -- 'E',
                      error_msg = v_error_msg
                WHERE btd_org_cd = cur_tender.btd_org_cd
                  AND btd_bill_dt = cur_tender.btd_bill_dt
                  AND btd_tender_type_cd = cur_tender.btd_tender_type_cd;

               COMMIT;
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               --   ROLLBACK;
               fnd_file.put_line
                  (fnd_file.LOG,
                      'Error Raise while inserting into Intermediate data for BAAN'
                   || SQLERRM
                  );
         --   RAISE v_insert_exception;
         END;
      END LOOP;

      fnd_file.put_line (fnd_file.output, 'Total_count=>' || v_total_count);
      fnd_file.put_line (fnd_file.output, 'Total_Vcount=>' || v_valid_count);
      fnd_file.put_line (fnd_file.output, 'Total_Icount=>' || v_invalid_count);
      fnd_file.put_line (fnd_file.output,
                         '----  Tender  into Intermediate completed  ----'
                        );
--   EXCEPTION
--      WHEN v_insert_exception
--      THEN
--         fnd_file.put_line
--               (fnd_file.LOG,
--                   'Error Raise while inserting(Intermediate ) data for BAAN'
--                || SQLERRM
--               );
--         ROLLBACK;
--         x_ret_code := 1;
--         DBMS_OUTPUT.put_line
--                 (   'Error Raise while inserting(Intermediate) data for BAAN'
--                  || SQLERRM
--                 );
--      WHEN OTHERS
--      THEN
--         fnd_file.put_line (fnd_file.LOG,
--                               'Exception in BAAN Insert(Intermediate): '
--                            || SQLERRM
--                           );
--         x_ret_code := 1;
--         DBMS_OUTPUT.put_line
--                 (   'Error Raise while inserting(Intermediate) data for BAAN'
--                  || SQLERRM
--                 );
   END baan_tender;

   PROCEDURE baan_intermediate_to_stag_t (
      x_err_buf    OUT   VARCHAR2,
      x_ret_code   OUT   NUMBER
   )
   IS
      v_total_count   NUMBER := 0;
      v_valid_count   NUMBER := 0;

      CURSOR xx_intermediate_stg2
      IS
         SELECT   ROWID, interface_line_attribute1,
                  interface_line_attribute2, status_flag, description,
                  SUM (tender_amount) tender_amount, SUM (amount) amount,
                  transaction_type, trx_date, gl_date,
                  orig_system_batch_name, customer_name, org_code, org_id,
                  code_combination_id, segment1, segment2, segment3,
                  segment4, segment5, segment6, segment7, segment8
             FROM xxabrl.xxabrl_baan_intermediate_stg
            WHERE status_flag IN ('N')
              AND UPPER (transaction_type) IN
                     ('COUPON', 'EXCHANGE VOUCHER', 'MARKETING COUPON',
                      'MARKETING DISCOUNT', 'WOMENS WEEK COUPON',
                      'STORE SODEXHO COUPON', 'STORE ACCOR COUPON',
                      'STORE CARD', 'STORE LOYALTY', 'BAJAJ CONSUMER FIN.',
                      'GIFT VOUCHER', 'CLICK AND CARRYTOPAY', 'VIP GV')
         GROUP BY ROWID,
                  interface_line_attribute1,
                  status_flag,
                  interface_line_attribute2,
                  description,
                  transaction_type,
                  trx_date,
                  gl_date,
                  orig_system_batch_name,
                  customer_name,
                  org_code,
                  org_id,
                  code_combination_id,
                  segment1,
                  segment2,
                  segment3,
                  segment4,
                  segment5,
                  segment6,
                  segment7,
                  segment8;
   BEGIN
      fnd_file.put_line
                (fnd_file.output,
                 '----- Tender Intermediate into staging2 Process Start-----'
                );

      FOR xx_inter IN xx_intermediate_stg2
      LOOP
         v_total_count := v_total_count + 1;
         --v_valid_count := v_valid_count + 1;
         fnd_file.put_line (fnd_file.output,
                            'customer_name=>' || xx_inter.customer_name
                           );
         DBMS_OUTPUT.put_line ('customer_name=>' || xx_inter.customer_name);
         fnd_file.put_line (fnd_file.output, 'gl_date=>' || xx_inter.gl_date);
         DBMS_OUTPUT.put_line ('gl_date=>' || xx_inter.gl_date);
         fnd_file.put_line (fnd_file.output,
                               'interface_line_attribute1=>'
                            || xx_inter.interface_line_attribute1
                           );
         DBMS_OUTPUT.put_line (   'interface_line_attribute1=>'
                               || xx_inter.interface_line_attribute1
                              );
         fnd_file.put_line (fnd_file.output,
                               'interface_line_attribute2=>'
                            || xx_inter.interface_line_attribute2
                           );
         fnd_file.put_line (fnd_file.output, 'Amount=>' || xx_inter.amount);
         fnd_file.put_line (fnd_file.output,
                            'Transaction Type=>' || xx_inter.transaction_type
                           );
         DBMS_OUTPUT.put_line (   'interface_line_attribute2=>'
                               || xx_inter.interface_line_attribute2
                              );
         fnd_file.put_line (fnd_file.output,
                            '  ------------------------------       '
                           );

         BEGIN
            INSERT INTO apps.xxabrl_navi_ar_int_line_stg2
                        (interface_line_context, interface_line_attribute1,
                         interface_line_attribute2, batch_source_name,
                         sob_id, line_type, description, currency_code,
                         tender_amount, amount,
                         actual_amount, transaction_type,
                         trx_date, gl_date,
                         orig_system_batch_name,
                         customer_name, orig_system_bill_address_id,
                         conversion_type, conversion_date, conversion_rate,
                         term_id, attribute_category, attribute1,
                         org_code, created_by, creation_date,
                         last_updated_by, last_update_date, interfaced_flag,
                         freeze_flag, org_id, error_message
                        )
                 VALUES ('ABRL_BAAN', xx_inter.interface_line_attribute1,
                         xx_inter.interface_line_attribute2, 'BAAN',
                         'ABRL LEDGER', 'LINE', xx_inter.description, 'INR',
                         xx_inter.tender_amount, xx_inter.amount,
                         xx_inter.amount, xx_inter.transaction_type,
                         xx_inter.trx_date, xx_inter.gl_date,
                         xx_inter.orig_system_batch_name,
                         xx_inter.customer_name, NULL,
                         'Corporate', NULL, NULL,
                         'IMMEDIATE', 'BAAN Receivables Invoice', NULL,
                         xx_inter.org_code, NULL, SYSDATE,
                         NULL, SYSDATE, 'N',
                         'N', xx_inter.org_id, NULL
                        );

            UPDATE xxabrl.xxabrl_baan_intermediate_stg
               SET status_flag = 'Y'
             WHERE ROWID = xx_inter.ROWID;
         EXCEPTION
            WHEN OTHERS
            THEN
               fnd_file.put_line
                  (fnd_file.LOG,
                      'Error Raise while inserting(Intermediate to staging2) data for BAAN'
                   || SQLERRM
                  );
--               UPDATE xxabrl.xxabrl_baan_intermediate_stg
--                  SET status_flag = 'E'
--                WHERE ROWID = xx_inter.ROWID;
               DBMS_OUTPUT.put_line
                  (   'Error Raise while inserting(Intermediate to staging2(LINE)) data for BAAN'
                   || SQLERRM
                  );
         -- ROLLBACK;
          --RAISE v_insert_exception;
         END;

         BEGIN
            INSERT INTO apps.xxabrl_navi_ra_int_dist_stg2
                        (interface_distribution_id, interface_line_id,
                         interface_line_context, interface_line_attribute1,
                         interface_line_attribute2, account_class,
                         tender_amount, amount,
                         org_id, description,
                         code_combination_id, segment1,
                         segment2, segment3,
                         segment4, segment5,
                         segment6, segment7,
                         segment8, created_by, creation_date,
                         last_updated_by, last_update_date,
                         last_update_login, interfaced_flag, error_message
                        )
                 VALUES (NULL, NULL,
                         'ABRL_BAAN', xx_inter.interface_line_attribute1,
                         xx_inter.interface_line_attribute2, 'REV',
                         xx_inter.tender_amount, xx_inter.amount,
                         xx_inter.org_code, xx_inter.description,
                         --------Processing is pending to fetch segments and code combination id
                         xx_inter.code_combination_id, xx_inter.segment1,
                         xx_inter.segment2, xx_inter.segment3,
                         xx_inter.segment4, xx_inter.segment5,
                         xx_inter.segment6, xx_inter.segment7,
                         xx_inter.segment8,
-----------------------------------------------------------
                         NULL, SYSDATE,
                         NULL, SYSDATE,
                         NULL, 'N', NULL
                        );

            UPDATE xxabrl.xxabrl_baan_intermediate_stg
               SET status_flag = 'Y'
             WHERE ROWID = xx_inter.ROWID;

            v_valid_count := v_valid_count + 1;
         EXCEPTION
            WHEN OTHERS
            THEN
               --  ROLLBACK;
               fnd_file.put_line
                  (fnd_file.LOG,
                      'Error Raise while inserting(Intermediate to staging2(DIST)) data for BAAN'
                   || SQLERRM
                  );
--               UPDATE xxabrl.xxabrl_baan_intermediate_stg
--                  SET status_flag = 'E'
--                WHERE ROWID = xx_inter.ROWID;
               DBMS_OUTPUT.put_line
                  (   'Error Raise while inserting(Intermediate to staging2(DIST)) data for BAAN'
                   || SQLERRM
                  );
         --      RAISE v_insert_exception;
         END;

         COMMIT;
      END LOOP;

      fnd_file.put_line (fnd_file.output, 'total_count=>' || v_total_count);
      fnd_file.put_line (fnd_file.output, 'Valid Data=>' || v_valid_count);
      fnd_file.put_line
                       (fnd_file.output,
                        '---- Tender Intermediate into staging2 completed----'
                       );
   EXCEPTION
--      WHEN v_insert_exception
--      THEN
--         fnd_file.put_line
--            (fnd_file.LOG,
--                'Error Raise while inserting(Intermediate to staging2) data for BAAN'
--             || SQLERRM
--            );
        -- ROLLBACK;
       --  x_ret_code := 1;
      --   DBMS_OUTPUT.put_line
        --    (   'Error Raise while inserting(Intermediate to staging2) data for BAAN'
          --   || SQLERRM
          --  );
      WHEN OTHERS
      THEN
         fnd_file.put_line
                   (fnd_file.LOG,
                       'Exception in BAAN Insert(Intermediate to staging2): '
                    || SQLERRM
                   );
         x_ret_code := 1;
         DBMS_OUTPUT.put_line
            (   'Error Raise while inserting(Intermediate to staging2) data for BAAN'
             || SQLERRM
            );
   END baan_intermediate_to_stag_t;
END xxabrl_baan_to_ar_main; 
/


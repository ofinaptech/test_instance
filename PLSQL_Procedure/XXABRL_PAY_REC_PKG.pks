CREATE OR REPLACE PACKAGE APPS.Xxabrl_Pay_Rec_Pkg AS
PROCEDURE XXABRL_PAY_REC_PROC (ERRBUFF OUT VARCHAR2,
                                      RETCODE OUT NUMBER,
                                                      --  P_LEDGER IN VARCHAR2,
                                      P_FROM_SUPPLIER IN VARCHAR2,
                                      P_TO_SUPPLIER  IN VARCHAR2,
                                                        P_LIST_OF_SUPP IN VARCHAR2,
                                      P_PAYMENT_FROM_DATE IN DATE,
                                      P_PAYMENT_TO_DATE IN DATE,
                                      P_BANK_ACCOUNT_NUM IN VARCHAR2,
                                      P_RECONCILE_STATUS IN VARCHAR2
                                      ) ;
                                     END Xxabrl_Pay_Rec_Pkg ; 
/


CREATE OR REPLACE PACKAGE BODY APPS.XXABRL_APMIGR_OPINV_PKG IS

  PROCEDURE INVOICE_VALIDATE(Errbuf        OUT VARCHAR2,
                             RetCode       OUT NUMBER,
                             P_Org_Id      IN NUMBER,
                             P_Data_Source IN VARCHAR2,
                             p_action VARCHAR2
                             ) IS

    CURSOR APT_C1(CP_OPERATING_UNIT VARCHAR2, CP_Data_Source VARCHAR2) IS
      SELECT ROWID,
             OPERATING_UNIT,
             INVOICE_NUM,
             SOURCE,
             INVOICE_TYPE_LOOKUP_CODE,
             INVOICE_DATE,
             VENDOR_NUMBER,
             VENDOR_SITE_CODE,
             PAYMENT_METHOD_LOOKUP_CODE,
             TERMS_NAME,
             INVOICE_AMOUNT,
             INVOICE_CURRENCY_CODE,
             EXCHANGE_RATE,
             EXCHANGE_DATE,
             EXCHANGE_RATE_TYPE,
             DESCRIPTION,
             GL_DATE
        FROM XXABRL_APMIGR_OPINV_INT
       WHERE Trim(UPPER(NVL(SOURCE, CP_Data_Source))) =

             Trim(UPPER(CP_Data_Source))
         AND Trim(UPPER(NVL(OPERATING_UNIT, CP_OPERATING_UNIT))) =

             Trim(UPPER(CP_OPERATING_UNIT))
         AND NVL(INTERFACED_FLAG, 'N') IN ('N', 'E');

    CURSOR APT_C2(CP_INVOICE_NUM VARCHAR2, CP_VENDOR_NUMBER VARCHAR2,  CP_VENDOR_SITE_CODE VARCHAR2, CP_OPERATING_UNIT VARCHAR2, CP_Data_Source VARCHAR2) IS
      SELECT ROWID,
             INVOICE_NUM,
             VENDOR_NUMBER ,
             VENDOR_SITE_CODE,
             LINE_NUMBER,
             LINE_TYPE_LOOKUP_CODE,
             AMOUNT,
             ACCOUNTING_DATE,
             DIST_CODE_CONCATENATED,
             DESCRIPTION,
             TDS_TAX_NAME
        FROM XXABRL_APMIGR_OPINV_LINE_INT
       WHERE Trim(UPPER(SOURCE)) =
             Trim(UPPER(CP_Data_Source))
         AND Trim(UPPER(OPERATING_UNIT)) =
             Trim(UPPER(CP_OPERATING_UNIT))
         AND NVL(INTERFACED_FLAG, 'N') <> 'Y'
         AND INVOICE_NUM =  CP_INVOICE_NUM
         AND VENDOR_NUMBER = CP_VENDOR_NUMBER
         AND VENDOR_SITE_CODE = CP_VENDOR_SITE_CODE
       ORDER BY Line_Number;

    CURSOR APT_C3(CP_OPERATING_UNIT VARCHAR2, CP_Data_Source VARCHAR2) IS
      SELECT TO_CHAR(Creation_Date, 'DD-MON-YYYY') Creation_Date,
             COUNT(Invoice_Num) Rec_Count
        FROM XXABRL_APMIGR_OPINV_INT
       WHERE Trim(UPPER(Operating_Unit)) =
             Trim(UPPER

                  (CP_OPERATING_UNIT))
         AND Trim(UPPER(SOURCE)) =
             Trim(UPPER

                  (CP_Data_Source))
         AND Interfaced_Flag = 'E'
       GROUP BY TO_CHAR(Creation_Date, 'DD-MON-YYYY');

    v_org_id                 NUMBER;
    v_set_of_books_id        NUMBER := FND_PROFILE.VALUE('GL_SET_OF_BKS_ID');
    v_user_id                NUMBER := FND_PROFILE.VALUE('USER_ID');
    v_resp_id                NUMBER := FND_PROFILE.VALUE('RESP_ID');
    v_appl_id                NUMBER := FND_PROFILE.VALUE('RESP_APPL_ID');
    V_OPERATING_UNIT         HR_Operating_Units.Short_Code%TYPE;
    v_error_hmsg             VARCHAR2(4000);
    v_error_lmsg             VARCHAR2(4000);
    v_error_almsg            VARCHAR2(4000);
    V_Fun_Curr               VARCHAR2(15);
    v_currency               VARCHAR2(15);
    v_term_name              ap_terms_tl.NAME%TYPE;
    v_tax_name               VARCHAR2(50);
    v_source_lookup          VARCHAR2(50);
    v_pay_lookup_code        VARCHAR2(50);
    v_Inv_Line_Amt           NUMBER := 0;
    V_DIST_CODE_CONCATENATED  gl_code_combinations_kfv.CONCATENATED_SEGMENTS%TYPE;
    v_vendor_id              po_vendors.vendor_id%TYPE;
    v_vendor_name            po_vendors.vendor_name%TYPE;
    v_vendor_site_name       po_vendor_sites_all.vendor_site_code%TYPE;
    v_vendor_site_id         po_vendor_sites_all.vendor_site_id%TYPE;
    v_record_count           NUMBER := 0;
    v_line_count             NUMBER := 0;
    v_Data_Count             NUMBER;
    v_Code_Combination_id    NUMBER;
    v_Error_Line_Count       NUMBER := 0;
    v_inv_type               VARCHAR2(30);
    v_Error_Rec_count        NUMBER := 0;
    v_OK_Rec_count           NUMBER := 0;
    V_Seg_Status             VARCHAR2(100);
    V_Pay_Site_Flag          VARCHAR2(1);

  BEGIN

    BEGIN
      SELECT NAME
        INTO V_OPERATING_UNIT
        FROM HR_Operating_Units
       WHERE Organization_Id = P_Org_Id;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        fnd_file.put_line(fnd_file.output,
                          'Selected Org Id does not exist in Oracle Financials');
        fnd_file.put_line

        (fnd_file.output,
         '...............................................................

.........');

      WHEN TOO_MANY_ROWS THEN
        fnd_file.put_line

        (fnd_file.output, 'Multiple Org Id exist in Oracle Financials');
        fnd_file.put_line

        (fnd_file.output,
         '...............................................................

.........');

      WHEN OTHERS THEN
        fnd_file.put_line

        (fnd_file.output, 'Invalid Org Id Selected ');
        fnd_file.put_line

        (fnd_file.output,
         '...............................................................

.........');

    END;

    UPDATE XXABRL_APMIGR_OPINV_INT
       SET CREATED_BY = v_user_id
     WHERE Trim(UPPER(SOURCE)) =
           Trim(UPPER (P_Data_Source))
       AND Trim(UPPER(OPERATING_UNIT)) =
           Trim(UPPER(V_OPERATING_UNIT))
       AND NVL(INTERFACED_FLAG, 'N') = 'N'
       AND CREATED_BY IS NULL;

    UPDATE XXABRL_APMIGR_OPINV_LINE_INT
       SET CREATED_BY = v_user_id
     WHERE Trim(UPPER(SOURCE)) =
           Trim(UPPER  (P_Data_Source))
       AND Trim(UPPER(OPERATING_UNIT)) =
           Trim(UPPER (V_OPERATING_UNIT))
       AND NVL(INTERFACED_FLAG, 'N') = 'N'
       AND CREATED_BY IS NULL;

    COMMIT;

    --Deleting previous error message if any--

    UPDATE XXABRL_APMIGR_OPINV_INT
       SET ERROR_MESSAGE = NULL
     WHERE Trim(UPPER(SOURCE)) =
           Trim(UPPER(P_Data_Source))
       AND Trim(UPPER(OPERATING_UNIT)) =
           Trim(UPPER(V_OPERATING_UNIT))
       AND NVL(INTERFACED_FLAG, 'N') IN ('N', 'E')
       AND ERROR_MESSAGE IS NOT NULL;

    UPDATE XXABRL_APMIGR_OPINV_LINE_INT
       SET ERROR_MESSAGE = NULL
     WHERE Trim(UPPER(SOURCE)) =
           Trim(UPPER (P_Data_Source))
       AND Trim(UPPER(OPERATING_UNIT)) =
           Trim(UPPER (V_OPERATING_UNIT))
       AND NVL(INTERFACED_FLAG, 'N') IN ('N', 'E')
       AND ERROR_MESSAGE IS NOT NULL;

    COMMIT;

    --Get Functional Currency

    BEGIN
      SELECT Currency_code
        INTO V_Fun_Curr
        FROM GL_SETS_OF_BOOKS
       WHERE set_of_books_id = v_set_of_books_id;
    EXCEPTION
      WHEN OTHERS THEN
        V_Fun_Curr := NULL;
    END;

    fnd_file.put_line(fnd_file.output, 'Validating Following Invoices');
    fnd_file.put_line

    (fnd_file.output,
     '...............................................................

.........');

    FOR APT_R1 IN APT_C1(Trim(UPPER(V_OPERATING_UNIT)),
                         Trim(UPPER(P_Data_Source))) LOOP
      EXIT WHEN APT_C1%NOTFOUND;
      v_record_count     := APT_C1%ROWCOUNT;
      v_error_hmsg       := NULL;
      v_error_lmsg       := NULL;
      v_vendor_id        := NULL;
      v_vendor_name      := NULL;
      v_vendor_site_name := NULL;
      v_vendor_site_id   := NULL;
      v_org_id           := NULL;
      v_Inv_Line_Amt     := 0;

      fnd_file.put_line(fnd_file.output, 'iNSIDE CURSOR APT_C1');

      --Validation for Invoice Number

      IF APT_R1.invoice_num IS NULL THEN
        v_error_hmsg := 'Invoice Number is Null-->>';
      END IF;

      --Validation for Invoice Date
      IF APT_R1.invoice_date IS NULL THEN
        v_error_hmsg := v_error_hmsg || 'Invoice Date is Null-->>';
      END IF;

      -- Validating for Source

      IF APT_R1.SOURCE IS NULL THEN
        v_error_hmsg := v_error_hmsg || 'Source is null-->';
      ELSE
        BEGIN
          SELECT lookup_code
            INTO v_source_lookup
            FROM ap_lookup_codes
           WHERE lookup_type = 'SOURCE'
             AND UPPER(lookup_code) =
                 Trim(UPPER

                      (APT_R1.SOURCE))
             AND NVL(Enabled_flag, 'N') = 'Y'
             AND SYSDATE BETWEEN NVL

                 (Start_date_Active, SYSDATE) AND
                 NVL(Inactive_date, SYSDATE);
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            v_error_hmsg := v_error_hmsg

                            || APT_R1.SOURCE ||
                            ' Source does not exist in Oracle Financials-->>';
          WHEN TOO_MANY_ROWS THEN
            v_term_name  := NULL;
            v_error_hmsg := v_error_hmsg || APT_R1.SOURCE || '

Multiple SOURCE FOUND-->>';
          WHEN OTHERS THEN

            v_error_hmsg := v_error_hmsg || APT_R1.SOURCE ||
                            ' Wrong Source Code-->>';
        END;
        /* End If;

            -- Validating for DC Location

            If APT_R1.DC_LOCATION is null Then
              v_error_hmsg:=v_error_hmsg||'DC Location is null-->';
            Else

               V_Seg_Status:=Null;

               V_Seg_Status:=ACCOUNT_SEG_STATUS

        (APT_R1.DC_LOCATION,'ABRL_GL_Location');

               If V_Seg_Status is Not Null then
                v_error_lmsg:=v_error_lmsg||'Invalid DC Location

        '||APT_R1.DC_LOCATION||'-->>';
               End If;


            End If;*/

        -- Validating Operating Units

        IF APT_R1.OPERATING_UNIT IS NULL THEN
          v_error_hmsg := v_error_hmsg || 'Operating Unit is Null-->>';
          /*Else
                Begin
                  Select  Organization_Id
                  Into    v_org_id
                  From    HR_Operating_Units
                  Where   Trim(Upper(Short_code)) = Trim

          (Upper(APT_R1.OPERATING_UNIT));

                EXCEPTION
                  when no_data_found then


          v_error_hmsg:=v_error_hmsg||'Operating Unit does not exists-->>';
                  when too_many_rows then


          v_error_hmsg:=v_error_hmsg||'Multiple Operating Unit found-->>';
                            When Others Then


          v_error_hmsg:=v_error_hmsg||'Operating Unit does not exists-->>';

                End;
              */
        END IF;

        IF APT_R1.INVOICE_TYPE_LOOKUP_CODE IS NULL THEN
          v_error_hmsg := v_error_hmsg || 'Invoice Type is Null-->>';
        ELSE
          BEGIN
            SELECT lookup_code
              INTO v_inv_type
              FROM ap_lookup_codes
             WHERE UPPER(lookup_code) =APT_R1.INVOICE_TYPE_LOOKUP_CODE
               AND lookup_type = 'INVOICE TYPE';

          EXCEPTION
            WHEN NO_DATA_FOUND THEN

              v_error_hmsg := v_error_hmsg ||
                              APT_R1.INVOICE_TYPE_LOOKUP_CODE ||
                              ' Invoice Type does

NOT EXISTS-->>';
            WHEN TOO_MANY_ROWS THEN

              v_error_hmsg := v_error_hmsg ||
                              APT_R1.INVOICE_TYPE_LOOKUP_CODE ||
                              ' Multiple Invoice

TYPE FOUND-->>';
            WHEN OTHERS THEN

              v_error_hmsg := v_error_hmsg ||
                              APT_R1.INVOICE_TYPE_LOOKUP_CODE || ' Invalid Invoice

TYPE-->>';
          END;

        END IF;
        --Validation for Supplier Number

        IF APT_R1.vendor_number IS NULL THEN
          v_error_hmsg := v_error_hmsg || 'Vendor Number is Null-->>';

        END IF;

        --Validation for Supplier Site
        IF APT_R1.vendor_SITE_Code IS NULL THEN
          v_error_hmsg := v_error_hmsg || 'Vendor Site Code is Null--

>>';
        END IF;

        -- Validation for Vendor Information
        IF APT_R1.vendor_number IS NOT NULL AND APT_R1.vendor_site_Code IS NOT NULL THEN

          BEGIN
            SELECT PV.Vendor_Id,
                   PV.Vendor_Name,
                   PVS.vendor_site_code,
                   PVS.vendor_site_id
              INTO v_vendor_id
                  ,v_vendor_name
                  , v_vendor_site_name
                   ,v_vendor_site_id
              FROM po_vendor_sites_all     PVS,
                   po_vendors              pv
                   --XXABRL_APMIGR_OPINV_INT XAOI
             WHERE --XAOI.VENDOR_NUMBER =   PV.Segment1
              PV.Segment1=APT_R1.VENDOR_NUMBER
               AND Trim(UPPER(APT_R1.VENDOR_SITE_CODE)) =   Trim(UPPER(PVS.Vendor_site_code))
               AND PV.Vendor_id     = PVS.Vendor_id
               AND PVS.Org_id =    P_Org_Id;

          EXCEPTION
            WHEN NO_DATA_FOUND THEN

              v_error_hmsg       := v_error_hmsg || APT_R1.vendor_site_Code || '-' ||
                                    APT_R1.vendor_number || '- Vendor does not exists-->>';
              v_vendor_id        :=
               NULL;
              v_vendor_name      := NULL;
              v_vendor_site_name := NULL;
              v_vendor_site_id   := NULL;

            WHEN TOO_MANY_ROWS THEN

              v_error_hmsg       := v_error_hmsg || APT_R1.vendor_site_Code || '-' ||
                                    APT_R1.vendor_number || '- Multiple Vendors found-->>';
              v_vendor_id        :=
               NULL;
              v_vendor_name      := NULL;
              v_vendor_site_name := NULL;
              v_vendor_site_id   := NULL;

            WHEN OTHERS THEN

              v_error_hmsg       := v_error_hmsg || APT_R1.vendor_site_Code || '-' ||
                                    APT_R1.vendor_number || '- Vendor does not exists-->>';
              v_vendor_id        :=
               NULL;
              v_vendor_name      := NULL;
              v_vendor_site_name := NULL;
              v_vendor_site_id   := NULL;

          END;
        END IF;

        IF v_vendor_id IS NOT NULL AND v_vendor_site_id IS NOT NULL THEN

          BEGIN
            V_Data_Count := 0;

            SELECT COUNT(pv.Vendor_ID)
              INTO V_Data_Count
              FROM po_vendors pv, po_vendor_sites_all pvs
             WHERE pv.Vendor_Id =           v_vendor_id
               AND pv.Vendor_Id =          pvs.Vendor_Id
               AND pvs.Vendor_Site_Id =           v_vendor_site_id
               AND SYSDATE BETWEEN NVL (pv.Start_date_Active, SYSDATE) AND
                   NVL(pv.End_Date_Active, SYSDATE)
               AND NVL(pv.Enabled_Flag, 'Y') =       'Y'
               AND NVL(pvs.Inactive_Date, SYSDATE) >=  SYSDATE
               AND pvs.Org_id       = P_Org_Id;

            IF V_Data_Count = 0 THEN
              v_error_hmsg := v_error_hmsg || V_vendor_name || 'Vendor Or ' || v_vendor_site_name ||
                              ' Vendor Site is inactive -->>';

            END IF;

            V_Data_Count := 0;

          EXCEPTION
            WHEN OTHERS THEN
              V_Data_Count := 0;

              v_error_hmsg := v_error_hmsg || V_vendor_name ||
                              ' Vendor Or ' || v_vendor_site_name || '

Vendor Site IS inactive -->>';
          END;
        END IF;

        -- Validation for Invoice Number duplication

        IF APT_R1.invoice_num IS NOT NULL AND v_vendor_id IS NOT NULL AND
           v_vendor_site_id IS NOT NULL THEN
          BEGIN
            V_Data_Count := 0;

            SELECT COUNT(Invoice_Num)
              INTO V_Data_Count
              FROM XXABRL_APMIGR_OPINV_INT
             WHERE Trim(UPPER(SOURCE)) =

                   Trim(UPPER(APT_R1.SOURCE))
               AND Trim(UPPER(OPERATING_UNIT)) =

                   Trim(UPPER(APT_R1.OPERATING_UNIT))
               AND VENDOR_NUMBER =

                   APT_R1.VENDOR_NUMBER
               AND VENDOR_SITE_CODE =

                   APT_R1.VENDOR_SITE_CODE
               AND Invoice_Num =

                   APT_R1.invoice_num;

            IF V_Data_Count > 1 THEN
              v_error_hmsg := v_error_hmsg || APT_R1.invoice_num || '

Duplicate Invoice NUMBER exist CURRENT FILE / Stage TABLE FOR THE

Supplier' || V_vendor_name || '-->>';

            END IF;

            V_Data_Count := 0;

          EXCEPTION
            WHEN OTHERS THEN
              V_Data_Count := 0;

              v_error_hmsg := v_error_hmsg || APT_R1.invoice_num ||
                              ' Invalid Invocie Number-->>';

          END;

          BEGIN
            V_Data_Count := 0;

            SELECT COUNT(Invoice_Num)
              INTO V_Data_Count
              FROM Ap_Invoices_all
             WHERE Vendor_id = v_vendor_id
                  --And    Vendor_site_id = v_vendor_site_id
               AND Org_id = p_org_id
               AND Invoice_Num =

                   APT_R1.invoice_num;

            IF V_Data_Count <> 0 THEN
              v_error_hmsg := v_error_hmsg || APT_R1.invoice_num || '

Invoice NUMBER already exist FOR THE Supplier ' ||
                              V_vendor_name || '-->>';

            END IF;

            V_Data_Count := 0;

          EXCEPTION
            WHEN OTHERS THEN
              V_Data_Count := 0;

              v_error_hmsg := v_error_hmsg || APT_R1.invoice_num ||
                              ' Invalid Invocie Number-->>';

          END;
        END IF;

        IF v_vendor_id IS NOT NULL AND v_vendor_site_id IS NOT NULL THEN

          V_Pay_Site_Flag := 'N';

          BEGIN
            SELECT NVL(Pay_Site_Flag, 'N')
              INTO V_Pay_Site_Flag
              FROM po_vendor_sites_all
             WHERE Vendor_id =

                   v_vendor_id
               AND Vendor_site_id = v_vendor_site_id
               AND Org_id = p_org_id;

            IF NVL(V_Pay_Site_Flag, 'N') <> 'Y' THEN

              v_error_hmsg := v_error_hmsg || V_vendor_name || '-' ||
                              v_vendor_site_name ||
                              ' Supplier

site FOR this supplier IS NOT a pay site -->>';
            END IF;
          EXCEPTION
            WHEN OTHERS THEN

              v_error_hmsg    := v_error_hmsg || V_vendor_name || '-' ||
                                 v_vendor_site_name ||
                                 ' Supplier

site FOR this supplier IS NOT a pay site -->>';
              V_Pay_Site_Flag := 'N';
          END;
        END IF;

        -- Validation for Payment Method

        /*BEGIN
              If APT_R1.payment_method_Lookup_Code is not null then

                SELECT  lookup_code
                into    v_pay_lookup_code
                FROM    ap_lookup_codes
                WHERE   lookup_type='PAYMENT METHOD'
                AND     UPPER(lookup_code)=Trim(UPPER

        (APT_R1.payment_method_Lookup_Code))
                And     Nvl(Enabled_flag,'N')='Y'
                And   Sysdate Between nvl

        (Start_date_Active,sysdate) and nvl(Inactive_Date,sysdate);
              End If;
            EXCEPTION
              when no_data_found then
                v_error_hmsg:= v_error_hmsg

        ||APT_R1.payment_method_Lookup_Code|| 'Payment method does not exist in Oracle

        Financials-->>';
              when too_many_rows then
                  v_error_hmsg:= v_error_hmsg ||

        APT_R1.payment_method_Lookup_Code||' Multiple Payment method records found-->>';


                    When  Others Then
                      v_error_hmsg:= v_error_hmsg

        ||APT_R1.payment_method_Lookup_Code||' Wrong Payment method -->>';
            END;*/

        IF v_vendor_site_id IS NOT NULL THEN
          v_pay_lookup_code := NULL;

          BEGIN
            SELECT iepm.Payment_Method_code
              INTO v_pay_lookup_code
              FROM iby_external_payees_all iep,
                   iby_ext_party_pmt_mthds iepm
             WHERE iep.supplier_site_id = v_vendor_site_id
               AND iep.Ext_Payee_id = iepm.ext_pmt_party_id
			   AND primary_flag='Y';

          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              v_error_hmsg := v_error_hmsg ||
                              'Payment

METHOD does NOT exist IN Supplier Site ' ||
                              V_vendor_name || '-' || v_vendor_site_name

                              || '-->>';

              v_pay_lookup_code := APT_R1.payment_method_Lookup_Code;
            WHEN TOO_MANY_ROWS THEN
              v_error_hmsg := v_error_hmsg ||

                              'Multiple Payment method records found ' ||
                              V_vendor_name || '-' || v_vendor_site_name

                              || '-->>';

              v_pay_lookup_code := APT_R1.payment_method_Lookup_Code;
            WHEN OTHERS THEN
              v_error_hmsg := v_error_hmsg || 'Wrong

Payment METHOD ' || V_vendor_name || '-' ||
                              v_vendor_site_name || '-->>';

              v_pay_lookup_code := APT_R1.payment_method_Lookup_Code;
          END;

        END IF;

        -- Validation for Invoice Amount
        IF APT_R1.Invoice_Amount IS NULL THEN
          v_error_hmsg := v_error_hmsg || 'Invoice Amount is Null--

>>';
        END IF;

        -- Validation for Invoice Amount with Transaction Type

        IF UPPER(APT_R1.INVOICE_TYPE_LOOKUP_CODE) = 'STANDARD' AND

           APT_R1.Invoice_Amount < 0 THEN

          v_error_hmsg := v_error_hmsg || 'STANDARD Invoice Amount is

ALWAYS POSITIVE-->>';

        ELSIF UPPER(APT_R1.INVOICE_TYPE_LOOKUP_CODE) = 'CREDIT' AND

              APT_R1.Invoice_Amount >= 0 THEN

          v_error_hmsg := v_error_hmsg || 'CREDIT MEMO Amount is

ALWAYS negative-->>';

        END IF;

        -- Validation for Invoice Currency

        IF APT_R1.Invoice_currency_Code IS NULL THEN
          v_error_hmsg := v_error_hmsg || 'Currency Code is null -->';
        ELSE
          BEGIN
            SELECT currency_code
              INTO v_currency
              FROM fnd_currencies
             WHERE UPPER(currency_code) =
                   Trim(UPPER

                        (APT_R1.Invoice_Currency_Code));

            IF UPPER(trim(v_currency)) <>
               UPPER(trim

                     (V_Fun_Curr)) AND APT_R1.exchange_date IS NULL THEN

              v_error_hmsg := v_error_hmsg || 'Exchange Date is Null -->>';
            END IF;

          EXCEPTION
            WHEN OTHERS THEN
              v_error_hmsg := v_error_hmsg || 'Invalid

Currency -->>';
          END;
        END IF;

        -- Validation for Payment Terms
        /*
            IF APT_R1.Terms_Name IS Not NULL THEN
                v_term_name:=null;
              BEGIN
                  SELECT name
                  into   v_term_name
                  FROM   ap_terms_tl
                  WHERE  upper(trim(name))=upper(trim

        (APT_R1.Terms_Name))
                  and sysdate between nvl

        (start_date_active,sysdate) and nvl(end_date_active,sysdate);
              EXCEPTION
                when no_data_found then
                    v_error_hmsg:= v_error_hmsg || 'Payment Terms

        does not exist in Oracle Financials-->>';
                when too_many_rows then
                    v_term_name:=null;
                    v_error_hmsg:=v_error_hmsg||'Multiple Payment

        Terms found-->>';

                      When  Others Then
                    v_term_name:=null;
                    v_error_hmsg:=v_error_hmsg||'Invalid Payment

        Term -->>';
                    END;
            END IF;
          */

        IF v_vendor_id IS NOT NULL AND v_vendor_site_id IS NOT NULL THEN
          v_term_name := NULL;
          BEGIN
            SELECT NAME
              INTO v_term_name
              FROM Po_Vendor_Sites_all PVS, Ap_terms_tl AT
             WHERE PVS.Vendor_id =

                   v_vendor_id
               AND PVS.Vendor_Site_id =

                   v_vendor_site_id
               AND PVS.Terms_Id =

                   AT.Term_Id
               AND SYSDATE BETWEEN NVL

                   (AT.start_date_active, SYSDATE) AND
                   NVL(AT.end_date_active, SYSDATE);
          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              v_error_hmsg := v_error_hmsg ||
                              'Payment Terms

does NOT DEFINED AT Vendor Site ' ||
                              V_vendor_name || '-' || v_vendor_site_name || '--

>>';
              v_term_name  := NULL;
            WHEN TOO_MANY_ROWS THEN
              v_error_hmsg := v_error_hmsg || 'Multiple

Payment Terms FOUND ' || V_vendor_name || '-' ||
                              v_vendor_site_name || '-->>';
              v_term_name  := NULL;
            WHEN OTHERS THEN
              v_term_name  := NULL;
              v_error_hmsg := v_error_hmsg || 'Invalid

Payment Term -->>';
          END;
        END IF;

        -- Validation for GLdate

        IF APT_R1.GL_date IS NULL THEN

          v_error_hmsg := v_error_hmsg || 'GL Date is Null-->>';

        ELSE
          V_Data_Count := 0;
          BEGIN
            SELECT COUNT(*)
              INTO V_Data_Count
              FROM gl_period_statuses gps, fnd_application fna
             WHERE fna.application_short_name = 'SQLAP'
               AND gps.application_id =

                   fna.application_id
               AND gps.closing_status = 'O'
               AND gps.set_of_books_id = v_set_of_books_id
               AND APT_R1.GL_Date BETWEEN gps.start_date AND gps.end_date;

            IF V_Data_Count = 0 THEN
              v_error_hmsg :=
               v_error_hmsg || APT_R1.GL_Date ||
                              ' GL Date is not in Open Period';
            END IF;
          EXCEPTION
            WHEN OTHERS THEN
              v_error_hmsg := v_error_hmsg || APT_R1.GL_Date ||
                              ' GL Date is not in Open Period-->>';
          END;

        END IF;
        v_Error_Line_Count := 0;
        v_error_almsg      := NULL;

        FOR APT_R2 IN APT_C2(APT_R1.INVOICE_NUM,
                             APT_R1.VENDOR_NUMBER,
                             APT_R1.VENDOR_SITE_CODE,
                             APT_R1.OPERATING_UNIT,
                             APT_R1.SOURCE) LOOP
          EXIT WHEN APT_C2%NOTFOUND;
          fnd_file.put_line(fnd_file.output,'Inside FOR LOOP : APT_C2');

          v_line_count          := APT_C2%ROWCOUNT;
          V_error_lmsg          := NULL;
          v_tax_name            := NULL;
          V_Data_Count          := 0;
          V_Code_Combination_id := NULL;

          -- Validation for Invoice Line Number
          IF APT_R2.LINE_NUMBER IS NULL THEN

            v_error_lmsg := v_error_lmsg ||
                            'Invoice Line Number is Null-->>';

          ELSIF APT_R2.LINE_NUMBER <> v_line_count THEN

            v_error_lmsg := v_error_lmsg ||
                            'Invoice Line Number is not in Sequence-->>';

          END IF;

          -- Validation for Invoice Line Amount

          IF APT_R2.Amount IS NULL THEN

            v_error_lmsg := v_error_lmsg ||
                            'Invoice Line Amount is Null-->>';

         ELSE
        fnd_file.put_line(fnd_file.output,
                      'Line amount:' || APT_R2.Amount);
          fnd_file.put_line(fnd_file.LOG,
                      'Line amount:' || APT_R2.Amount);

          v_Inv_Line_Amt := NVL(v_Inv_Line_Amt, 0) + NVL(APT_R2.Amount, 0);

           fnd_file.put_line(fnd_file.output,
                      'Line amount:' || v_Inv_Line_Amt);
             fnd_file.put_line(fnd_file.LOG,
                      'Line amount:' || v_Inv_Line_Amt);
         END IF;
          -- Validation for Accounting_date

          IF APT_R2.Accounting_date IS NULL THEN

            v_error_lmsg := v_error_lmsg || 'Accounting Date is Null--

>>';

          ELSE

            BEGIN
              SELECT COUNT(*)
                INTO V_Data_Count
                FROM gl_period_statuses gps, fnd_application fna
               WHERE fna.application_short_name = 'SQLAP'
                 AND gps.application_id =

                     fna.application_id
                 AND gps.closing_status = 'O'
                 AND APT_R2.Accounting_Date BETWEEN

                     gps.start_date AND gps.end_date;

              IF V_Data_Count = 0 THEN
                v_error_lmsg := v_error_lmsg || 'Accounting

DATE IS NOT IN OPEN Period';
              END IF;
            EXCEPTION
              WHEN OTHERS THEN
                v_error_lmsg := v_error_lmsg || 'Accounting Date is

NOT IN OPEN Period-->>';
            END;

          END IF;

          IF APT_R2.DIST_CODE_CONCATENATED IS NULL THEN

            v_error_lmsg := v_error_lmsg || 'Charge Account Code is Null

-->>';
            BEGIN
              SELECT code_combination_id
                INTO V_DIST_CODE_CONCATENATED
                FROM gl_code_combinations_kfv
               WHERE TRIM(concatenated_segments) = TRIM
                     (APT_R2.dist_code_concatenated);
            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                v_error_lmsg := 'DIST CODE CONCATENATED VALUE' || ' ' ||

                                APT_R2.dist_code_concatenated || ' ' ||
                                'IS NOT VALID';
              WHEN OTHERS THEN
                v_error_lmsg := 'INVALID DIST CODE CONCATENATED VALUE';
            END;
          END IF;

          BEGIN
            V_Code_Combination_id := NULL;

            SELECT Code_Combination_id
              INTO V_Code_Combination_id
              FROM gl_code_combinations_kfv
             WHERE TRIM(concatenated_segments) =
                   TRIM(APT_R2.dist_code_concatenated);
          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              v_error_lmsg := v_error_lmsg || 'Charge

ACCOUNT ode' || APT_R2.dist_code_concatenated ||
                              ' does not exists-->>';
            WHEN TOO_MANY_ROWS THEN
              v_error_lmsg := v_error_lmsg || 'Multiple

Charge ACCOUNT Code ' ||
                              APT_R2.dist_code_concatenated ||
                              ' exists-->>';
            WHEN OTHERS THEN
              v_error_lmsg := v_error_lmsg || 'Charge

ACCOUNT Code ' ||
                              APT_R2.dist_code_concatenated ||
                              ' does not exists-->>';
          END;
          -- End If;

          --Validation for Invoice Line Type

          IF APT_R2.LINE_TYPE_LOOKUP_CODE IS NULL THEN
          v_error_hmsg := v_error_hmsg || 'Invoice Line Type is Null-->>';
        ELSE
          BEGIN
            SELECT lookup_code
              INTO v_inv_type
              FROM ap_lookup_codes
             WHERE UPPER(lookup_code) =APT_R2.LINE_TYPE_LOOKUP_CODE
               AND lookup_type = 'INVOICE LINE TYPE';

          EXCEPTION
            WHEN NO_DATA_FOUND THEN

              v_error_hmsg := v_error_hmsg ||
                              APT_R2.LINE_TYPE_LOOKUP_CODE ||
                              ' Invoice Line Type does not exists-->>';
            WHEN TOO_MANY_ROWS THEN

              v_error_hmsg := v_error_hmsg ||
                              APT_R2.LINE_TYPE_LOOKUP_CODE ||
                              ' Multiple Invoice line Type found-->>';
            WHEN OTHERS THEN

              v_error_hmsg := v_error_hmsg ||
                              APT_R2.LINE_TYPE_LOOKUP_CODE || ' Invalid Invoice Line Type-->>';
          END;

        END IF;


          -- Validation for Tax Codes
          IF APT_R2.TDS_tax_name IS NOT NULL THEN
            BEGIN
              v_tax_name := NULL;

              SELECT Tax_name
                INTO v_tax_name
                FROM JAI_CMN_TAXES_ALL
               WHERE UPPER(Tax_Name) =
                     Trim(UPPER

                          (APT_R2.TDS_tax_name))
                 AND org_id = P_org_id
                 AND SYSDATE BETWEEN NVL(Start_date, SYSDATE)

                     AND NVL(End_Date, SYSDATE);
            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                v_error_lmsg := v_error_lmsg || 'Tax Name does

NOT EXISTS-->>';
              WHEN TOO_MANY_ROWS THEN
                v_error_lmsg := v_error_lmsg || 'Multiple Tax

NAME EXISTS-->>';
              WHEN OTHERS THEN
                v_error_lmsg := v_error_lmsg || 'Tax Name does

NOT EXISTS-->>';
            END;
          END IF;

          --Updating the error message in table

          IF v_error_lmsg IS NOT NULL THEN

            UPDATE XXABRL_APMIGR_OPINV_LINE_INT
               SET error_message = v_error_lmsg, Interfaced_Flag = 'E'
             WHERE ROWID = APT_R2.ROWID;
            v_Error_Line_Count := v_Error_Line_Count + 1;

          ELSE
            UPDATE XXABRL_APMIGR_OPINV_LINE_INT
               SET DIST_CODE_COMBINATION_ID = V_Code_Combination_id,
                   Org_Id                   = P_Org_Id,
                   TDS_tax_name             = v_tax_name,
                   Interfaced_Flag          = 'V'
             WHERE ROWID = APT_R2.ROWID;

          END IF;
          IF v_error_lmsg IS NOT NULL THEN
            v_error_almsg := v_error_almsg || 'Line No :' || v_line_count || ' ' || v_error_lmsg;
          END IF;
        END LOOP;

        fnd_file.put_line(fnd_file.output,'v_Inv_Line_Amt '||NVL(v_Inv_Line_Amt,0));
        fnd_file.put_line(fnd_file.output,'APT_R1.Invoice_Amount '||NVL(APT_R1.Invoice_Amount,0));

        IF v_Inv_Line_Amt <> APT_R1.Invoice_Amount THEN
          v_error_hmsg := v_error_hmsg || 'Invoice Header and Line Amount is not matching'||v_Inv_Line_Amt ;
        END IF;

        --Updating the error message in table

        IF v_error_hmsg IS NOT NULL THEN

          UPDATE XXABRL_APMIGR_OPINV_INT
             SET error_message = v_error_hmsg, Interfaced_Flag = 'E'
           WHERE ROWID = APT_R1.ROWID;

          v_Error_Rec_count := v_Error_Rec_count + 1;

          COMMIT;

          fnd_file.put_line(fnd_file.output,
                            v_record_count || ' )'

                             || 'Invoice Number : ' || APT_R1.invoice_num || ' ,' ||
                             'Vendor Number :

' || APT_R1.vendor_number);
          fnd_file.put_line(fnd_file.output,
                            'Header Error

:' || v_error_hmsg);

          IF v_error_almsg IS NOT NULL THEN
            fnd_file.put_line(fnd_file.output,
                              'Line Error

:' || v_error_almsg);
          END IF;

        ELSIF v_error_hmsg IS NULL AND v_Error_Line_Count <> 0 THEN

          UPDATE XXABRL_APMIGR_OPINV_INT
             SET error_message   = v_Error_Line_Count || ' Errors in

INTERFACE Lines',
                 Interfaced_Flag = 'E'
           WHERE ROWID = APT_R1.ROWID;

          v_Error_Rec_count := v_Error_Rec_count + 1;

          COMMIT;

          fnd_file.put_line(fnd_file.output,
                            v_record_count || ' )'

                             || 'Invoice Number : ' || APT_R1.invoice_num || ' ,' ||
                             'Vendor Number :

' || APT_R1.vendor_number);
          fnd_file.put_line(fnd_file.output,
                            'Header Error

:' || v_error_hmsg);

          IF v_error_almsg IS NOT NULL THEN
            fnd_file.put_line(fnd_file.output,
                              'Line Error

:' || v_error_almsg);
          END IF;

        ELSE

          UPDATE XXABRL_APMIGR_OPINV_INT
             SET VENDOR_ID = v_vendor_id

                ,
                 VENDOR_SITE_ID = v_vendor_site_id

                ,
                 Org_Id                     = P_Org_Id,
                 TERMS_NAME                 = v_term_name,
                 Payment_Method_Lookup_Code = NVL(v_pay_lookup_code, 'CHECK'),
                 error_message              = NULL,
                 Interfaced_Flag            = 'V'
           WHERE ROWID = APT_R1.ROWID;

          v_OK_Rec_count := v_OK_Rec_count + 1;
          COMMIT;
          fnd_file.put_line(fnd_file.output,
                            v_record_count || ' )'

                             || 'Invoice Number : ' || APT_R1.invoice_num || ' ,' ||
                             'Vendor Number :

' || APT_R1.vendor_number);
          fnd_file.put_line(fnd_file.output,
                            'Status : Valid

Invoice');

        END IF;
      END IF;
    END LOOP;
    IF v_Error_Rec_count > 0 THEN
      retcode := 1;
    END IF;

    fnd_file.put_line

    (fnd_file.output,
     '...............................................................

.........');

    FOR APT_R3 IN APT_C3(Trim(UPPER(V_OPERATING_UNIT)),
                         Trim(UPPER(P_Data_Source))) LOOP
      EXIT WHEN APT_C3%NOTFOUND;
      fnd_file.put_line(fnd_file.output,
                        APT_R3.Creation_Date || ' Records

WITH error :' || APT_R3.Rec_Count);

    END LOOP;

    fnd_file.put_line(fnd_file.output,
                      'Total Number of Records with error

:' || v_Error_Rec_count);
    fnd_file.put_line(fnd_file.output,
                      'Total Number of Valid Records

:' || v_OK_Rec_count);
    fnd_file.put_line

    (fnd_file.output,
     '...............................................................

.........');

    v_source_lookup := P_Data_Source;
    V_Org_Id        := P_Org_Id;
  fnd_file.put_line(fnd_file.output,
                     'Inserting header details:' || P_Data_Source||'-'||P_Org_Id);

   IF p_action = 'N' THEN
    INVOICE_INSERT(V_Org_Id, v_source_lookup);
  END IF;
  END;

  PROCEDURE INVOICE_INSERT(P_Org_Id IN NUMBER, P_Data_Source IN VARCHAR2) IS

    CURSOR APT_C_OP IS
      SELECT UNIQUE OPERATING_UNIT, GL_Date
        FROM XXABRL_APMIGR_OPINV_INT
       WHERE Trim(UPPER(SOURCE)) = Trim(UPPER(P_Data_Source))
         AND Org_Id = P_Org_Id
         AND NVL(INTERFACED_FLAG, 'N') = 'V'
         AND ERROR_MESSAGE IS NULL;

    CURSOR APT_C1(P_OPERATING_UNIT VARCHAR2, P_GL_Date DATE) IS
      SELECT ROWID,
             OPERATING_UNIT,
             INVOICE_NUM,
             SOURCE,
             INVOICE_TYPE_LOOKUP_CODE,
             INVOICE_DATE,
             VENDOR_NUMBER,
             VENDOR_SITE_CODE,
             PAYMENT_METHOD_LOOKUP_CODE,
             TERMS_NAME,
             INVOICE_AMOUNT,
             INVOICE_CURRENCY_CODE,
             EXCHANGE_RATE,
             EXCHANGE_DATE,
             EXCHANGE_RATE_TYPE,
             DESCRIPTION,
             GL_DATE,
             VENDOR_ID,
             VENDOR_SITE_ID,
             Org_Id,
             TERM_ID
        FROM XXABRL_APMIGR_OPINV_INT
       WHERE Trim(UPPER(SOURCE)) = Trim(UPPER(P_Data_Source))
         AND Org_Id = P_Org_Id
         AND NVL(INTERFACED_FLAG, 'N') = 'V'
         AND OPERATING_UNIT = P_OPERATING_UNIT
         AND GL_Date = P_GL_Date
         AND ERROR_MESSAGE IS NULL;
    -- Order by GRN_NUMBER;

    CURSOR APT_C2(P_INVOICE_NUM VARCHAR2, P_VENDOR_NUMBER VARCHAR2, P_VENDOR_SITE_CODE VARCHAR2) IS
      SELECT ROWID,
             INVOICE_NUM,
             VENDOR_NUMBER,
             VENDOR_SITE_CODE,
             LINE_NUMBER,
             LINE_TYPE_LOOKUP_CODE,
             AMOUNT,
             ACCOUNTING_DATE,
             DIST_CODE_COMBINATION_ID,
             DESCRIPTION,
             TDS_TAX_NAME,
             Org_id
        FROM XXABRL_APMIGR_OPINV_LINE_INT
       WHERE Trim(UPPER(SOURCE)) = Trim(UPPER(P_Data_Source))
         AND Org_Id = P_Org_Id
         AND NVL(INTERFACED_FLAG, 'N') = 'V'
         AND ERROR_MESSAGE IS NULL
         AND INVOICE_NUM = P_INVOICE_NUM
         AND VENDOR_NUMBER = P_VENDOR_NUMBER
         AND VENDOR_SITE_CODE = P_VENDOR_SITE_CODE;

    v_invoice_id NUMBER;
    --v_org_id          number;
    v_user_id      NUMBER := FND_PROFILE.VALUE('USER_ID');
    v_resp_id      NUMBER := FND_PROFILE.VALUE('RESP_ID');
    v_appl_id      NUMBER := FND_PROFILE.VALUE('RESP_APPL_ID');
    v_req_id       NUMBER;
    v_record_count NUMBER := 0;
    V_Doc_Category VARCHAR2(50);
    V_Batch_Name   VARCHAR2(50);
    V_Status       VARCHAR2(1);
  BEGIN
    --v_org_id  :=P_Org_Id;
    -----
    -- Inserting data into the Header table AP_INVOICES_INTEFACE
    -----
    fnd_file.put_line(fnd_file.output,
                      'Inserting header details0:' || P_Data_Source||'-'||P_Org_Id);

    FOR APT_R_OP IN APT_C_OP LOOP
      EXIT WHEN APT_C_OP%NOTFOUND;
        fnd_file.put_line(fnd_file.output,
                      'Inserting header details1:' || P_Data_Source||'-'||P_Org_Id);
      FOR APT_R1 IN APT_C1(APT_R_OP.OPERATING_UNIT, APT_R_OP.GL_DATE) LOOP
        EXIT WHEN APT_C1%NOTFOUND;

        fnd_file.put_line(fnd_file.output,
                      'Inserting header details2:' || P_Data_Source||'-'||P_Org_Id);
        v_record_count := APT_C1%ROWCOUNT;

        SELECT ap_invoices_interface_s.NEXTVAL INTO v_invoice_id FROM DUAL;

        V_Doc_Category := NULL;

        IF Trim(UPPER(APT_R1.invoice_type_lookup_code)) = 'STANDARD' THEN
          V_Doc_Category := 'STD INV';
        ELSIF Trim(UPPER(APT_R1.invoice_type_lookup_code)) = 'CREDIT' THEN
          V_Doc_Category := 'CRM INV';
        ELSE
          V_Doc_Category := NULL;
        END IF;
        fnd_file.put_line(fnd_file.output,
                      'Inserting header details3:' || P_Data_Source||'-'||P_Org_Id);
        INSERT INTO ap_invoices_interface
          (INVOICE_ID,
           INVOICE_NUM,
           INVOICE_TYPE_LOOKUP_CODE,
           INVOICE_DATE,
           VENDOR_ID,
           VENDOR_SITE_ID,
           INVOICE_AMOUNT,
           INVOICE_CURRENCY_CODE,
           EXCHANGE_DATE,
           TERMS_NAME,
           DESCRIPTION,
           SOURCE,
           PAYMENT_METHOD_CODE,
           DOC_CATEGORY_CODE,
           GL_DATE,
           ORG_ID,
           TAXATION_COUNTRY,
           CREATED_BY,
           CREATION_DATE,
           LAST_UPDATED_BY,
           LAST_UPDATE_DATE)
        VALUES
          (v_invoice_id,
           Trim(APT_R1.invoice_num),
           Trim(APT_R1.invoice_type_lookup_code),
           Trim(APT_R1.invoice_date),
           Trim(APT_R1.vendor_id),
           Trim (APT_R1.vendor_site_id),
           Trim(APT_R1.Invoice_Amount),
           Trim (APT_R1.Invoice_currency_code),
           Trim(APT_R1.exchange_date),
           Trim (APT_R1.Terms_Name),
           Trim(APT_R1.description),
           Trim (APT_R1.SOURCE),
           Trim( APT_R1.payment_method_Lookup_Code),
           V_Doc_Category,
           Trim(APT_R1.GL_date),
           APT_R1.org_id,
           'IN',
           v_user_id,
           SYSDATE,
           v_user_id,
           SYSDATE);

        COMMIT;

        fnd_file.put_line(fnd_file.output,
                      'Inserted header details:' || P_Data_Source||'-'||P_Org_Id);
        --
        -- Inserting records into Detail table : AP_INVOICE_LINES_INTERFACE
        --

        FOR APT_R2 IN APT_C2(APT_R1.INVOICE_NUM,
                             APT_R1.VENDOR_NUMBER,
                             APT_R1.VENDOR_SITE_CODE) LOOP
          EXIT WHEN APT_C2%NOTFOUND;

          INSERT INTO ap_invoice_lines_interface
            (invoice_id,
             invoice_line_id,
             line_number,

             line_type_lookup_code,
             amount,
             accounting_date,
             DIST_CODE_COMBINATION_ID,
             Description,
             Org_id,
             --Attribute_Category,
             -- Attribute1,
            -- Attribute2,
             created_by,
             creation_date,
             last_updated_by,
             last_update_date)
          VALUES
            (v_invoice_id,
             ap_invoice_lines_interface_s.NEXTVAL,
             APT_R2.LINE_NUMBER,
             Trim (APT_R2.LINE_TYPE_LOOKUP_CODE),
             Trim(APT_R2.AMOUNT),
             Trim (APT_R2.ACCOUNTING_DATE),
             Trim(APT_R2.DIST_CODE_COMBINATION_ID),
             --Trim(NVL(APT_R2.REFERENCE_1,APT_R1.GRN_NUMBER)),
             --  Trim(APT_R2.REFERENCE_2),
             Trim(APT_R2.DESCRIPTION),
             APT_R2.org_id,
             --Trim('invoice'),
            -- Trim(APT_R1.DC_LOCATION),
             -- Trim (APT_R2.TDS_TAX_NAME),
             v_user_id,
             SYSDATE,
             v_user_id,
             SYSDATE);

          UPDATE XXABRL_APMIGR_OPINV_LINE_INT
             SET Interfaced_Flag = 'Y', Interfaced_Date = SYSDATE
           WHERE ROWID = APT_R2.ROWID;

          COMMIT;

        END LOOP;

        UPDATE XXABRL_APMIGR_OPINV_INT
           SET Interfaced_Flag = 'Y', Interfaced_Date = SYSDATE
         WHERE ROWID = APT_R1.ROWID;
        COMMIT;
      END LOOP;

      BEGIN
        V_Batch_Name := NULL;

        /*
            Select  P_Data_Source||'-'||Substr(FFVV.description,1,30)

        ||'-'||TO_CHAR (APT_R_LOC.GL_Date, 'DD-MON-RRRR')
            Into    V_Batch_Name
            From    FND_FLEX_VALUES_VL FFVV,
                    FND_FLEX_VALUE_SETS FFVS
            Where   FFVS.Flex_Value_Set_Name  = 'ABRL_GL_Location'
            And     FFVS.Flex_Value_Set_Id  = FFVV.Flex_Value_Set_Id
            And     FFVV.Flex_Value     = APT_R_LOC.DC_LOCATION;
          */

        SELECT P_Data_Source || '-' || NAME || '-' ||
               TO_CHAR(APT_R_OP.GL_Date, 'DD-MON-RRRR')
          INTO V_Batch_Name
          FROM HR_Operating_Units
         WHERE NAME = APT_R_OP.OPERATING_UNIT;

         EXCEPTION WHEN OTHERS THEN
         V_Batch_Name := P_Data_Source || '-' || TO_CHAR(APT_R_OP.GL_Date, 'DD-MON-RRRR');

      END;

      fnd_file.put_line(fnd_file.output,
                        'Number of Records (Invoices )Inserted in

INTERFACE TABLE :' || v_record_count);
      fnd_file.put_line

      (fnd_file.output,
       '...............................................................

.........');
      -----
    -- Run Payable Open Iterface for Interface data into Oracle Payables
    ----
      IF v_record_count>0 THEN
            fnd_global.apps_initialize (
                  user_id=> v_user_id,
                  resp_id=> v_resp_id,
                  resp_appl_id=> v_appl_id
               );

        COMMIT;

        v_req_id :=fnd_request.submit_request (
                        'SQLAP',
                      'APXIIMPT',
                        'Payables Open Interface Import'||P_Data_Source,
                  NULL,
                        FALSE,
              p_org_id,
                      P_Data_Source, -- Parameter Data source
                        NULL, -- Group id
                    V_Batch_Name,
                        NULL,
                    NULL,
                    NULL,
                    'N',
            'N',
                'N',
            'Y',
                        CHR (0)
                  );
        COMMIT;
        fnd_file.put_line(fnd_file.output,'Please see the output of

    Payables OPEN Invoice Import program request ID :'||v_req_id);
        fnd_file.put_line

    (fnd_file.output,'...............................................................

    .........' );

        LOOP
          BEGIN
            V_Status:=NULL;

            SELECT Status_Code
            INTO   V_Status
            FROM   Fnd_Concurrent_Requests
            WHERE  Request_id=v_req_id;

            IF V_Status IN ('C','D','E','G','X') THEN
                EXIT;
            END IF;

          EXCEPTION
            WHEN OTHERS THEN
                  EXIT;

          END;
        END LOOP;
      END IF;
        END LOOP;

  END;

END XXABRL_APMIGR_OPINV_PKG;
/


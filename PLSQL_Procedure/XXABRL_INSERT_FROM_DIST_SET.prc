CREATE OR REPLACE PROCEDURE APPS.XXABRL_Insert_From_Dist_Set(ERRBUFF OUT VARCHAR2,
                                    RETCODE OUT NUMBER)

     IS
     v_user_id                  NUMBER:=FND_PROFILE.VALUE('USER_ID');                    
     l_inv_dist_id              NUMBER;
      v_cnt number:=0;
      CURSOR CUR_GEN_INV_DIST
          IS 
/*  
    SELECT AIA.INVOICE_ID 
        FROM AP_INVOICES_ALL AIA             
        WHERE AIA.INVOICE_ID IN (SELECT INVOICE_ID FROM AP_INVOICE_LINES_ALL AILA WHERE 
        AILA.GENERATE_DISTS = 'Y')
--  and invoice_id in (516516,516656,515936)
         AND AIA.SOURCE = 'Oracle Property Manager';
*/
-- added by shailesh on 3 june 2009 as for some invoice distribution were loaded for some lines so added extra condition 
-- for not selecting those records which generate distributions
       
       SELECT AIA.INVOICE_ID ,AIA.INVOICE_NUM
       FROM AP_INVOICES_ALL AIA
       WHERE 1=1
       AND AIA.SOURCE = 'Oracle Property Manager'
       AND AIA.INVOICE_ID IN (SELECT INVOICE_ID FROM AP_INVOICE_LINES_ALL AILA 
                                   WHERE 1=1
                                   AND AILA.GENERATE_DISTS = 'Y'
                                   AND (SELECT COUNT(1) FROM AP_INVOICE_LINES_ALL 
                                           WHERE 1=1
                                           AND    INVOICE_ID=AILA.INVOICE_ID
                                                AND GENERATE_DISTS = 'D')  = 0 );
--  AND INVOICE_ID IN (516516,516656,515936)

      CURSOR CUR_INV_LINES(P_INVOICE_ID NUMBER)
      IS
      SELECT * 
        FROM AP_INVOICE_LINES_ALL
       WHERE INVOICE_ID = P_INVOICE_ID; 

     BEGIN
     
     DBMS_OUTPUT.PUT_LINE('Start Of Program');
     
     FOR i IN CUR_GEN_INV_DIST LOOP
  
         DBMS_OUTPUT.PUT_LINE('Loop 1 Invoice Id = '||i.invoice_id);
  
       FOR j IN CUR_INV_LINES(i.invoice_id)  LOOP
  
           DBMS_OUTPUT.PUT_LINE('loop2 Line num = '||j.LINE_NUMBER );
  
         SELECT ap_invoice_distributions_s.NEXTVAL 
         INTO l_inv_dist_id 
         FROM DUAL;

        BEGIN    
     
        INSERT INTO ap_invoice_distributions_all (
                   invoice_id,
                   invoice_line_number,
                   invoice_distribution_id,
                   distribution_line_number,
                   line_type_lookup_code,
                   distribution_class,
                   dist_match_type,
                   org_id,
                   dist_code_combination_id,
                   accounting_date,
                   period_name,
            accrual_posted_flag,
                   cash_posted_flag,                   
                   posted_flag,
                   set_of_books_id,
                   amount,
                   match_status_flag,
                   reversal_flag,
                   cancellation_flag,
                   final_match_flag,
                   assets_addition_flag,
                   assets_tracking_flag,
                   global_attribute_category,
                   global_attribute1,
                   created_by,
                   creation_date,
                   last_updated_by,
                   last_update_date,
                   last_update_login,
                   program_application_id,
                   program_id,
                   program_update_date,
                   request_id
           )
       VALUES (
                   j.invoice_id,                  -- invoice_id
                   j.line_number,                 -- invoice_line_number
                   l_inv_dist_id,          -- invoice_distribution_id
                   1,                     -- distribution_line_number
                   j.line_type_lookup_code,       -- line_type_lookup_code
                   'PERMANENT',                   -- distribution_class
                   'NOT_MATCHED',                 -- dist_match_type
                   j.org_id,                   -- l_org_id
                   J.default_dist_ccid,             -- dist_code_combination_id
                   J.accounting_date,           -- accounting_date
                   J.period_name,              -- period_name
                   'N',                           -- accrual_posted_flag
                   'N',                           -- cash_posted_flag
                   'N',                           -- posted_flag
                   j.set_of_books_id,             -- set_of_books_id
                   j.amount,                      -- amount
                   NULL,                          -- match_status_flag
                   'N',                           -- reversal_flag
                   'N',                           -- cancellation_flag
                   NULL,                          -- final_match_flag
                   'U',
                   'N',                           --assets_addition_flag
                   'JA.IN.APXINWKB.DISTRIBUTIONS',                    --j.global_attribute_category,   -- global_attribute_category
                   j.attribute10,                                             --j.global_attribute1,           -- global_attribute1
                   v_user_id,                                                 -- created_by
                   SYSDATE,                                               -- creation_date
                   NVL(v_user_id,3591),                                         -- last_updated_by
                   SYSDATE,                                               -- last_update_date
                   v_user_id,                     -- last_update_login
                   NULL,                          -- program_application_id
                   NULL,                          -- program_id
                   NULL,                          -- program_update_date
                   NULL                           -- request_id
            );
    
    EXCEPTION
    WHEN OTHERS THEN
                  DBMS_OUTPUT.PUT_LINE('  INSERT FAILURE OF AP_INVOICE_DISTRIBUTIONS_ALL FROM AP_INVOICE_LINES_ALL  ' );
        END;


  ----------------------------------------------------------------------------
  -- Update generate distributions flag in invoice line 
  ----------------------------------------------------------------------------

    BEGIN
  
      DBMS_OUTPUT.PUT_LINE('updating the lines table' );
  
      UPDATE AP_INVOICE_LINES_ALL
         SET GENERATE_DISTS = 'D'
         WHERE 1=1
         AND INVOICE_ID    = J.INVOICE_ID
         AND LINE_NUMBER    = J.LINE_NUMBER;

    EXCEPTION
    WHEN NO_DATA_FOUND THEN
              DBMS_OUTPUT.PUT_LINE('UPDATE FAILURE OF AP_INVOICE_LINES_ALL DURING GENERATE_DISTS COLUMN TO D' );
    WHEN TOO_MANY_ROWS THEN
            DBMS_OUTPUT.PUT_LINE('UPDATE FAILURE OF AP_INVOICE_LINES_ALL DURING GENERATE_DISTS COLUMN TO D' );
    WHEN OTHERS THEN
              DBMS_OUTPUT.PUT_LINE('UPDATE FAILURE OF AP_INVOICE_LINES_ALL DURING GENERATE_DISTS COLUMN TO D' );
    END;
  
     END LOOP;
  
     v_cnt:=v_cnt+1;
     fnd_file.put_line(fnd_file.output,'Distribution Generated for Invoice :- '||i.invoice_num);
  
END LOOP;
  
  fnd_file.put_line(fnd_file.output,'Total Processed Records :- '||v_cnt);
  
  DBMS_OUTPUT.PUT_LINE('end of loop and commit');
  
COMMIT;
  
END XXABRL_insert_from_dist_set; 
/


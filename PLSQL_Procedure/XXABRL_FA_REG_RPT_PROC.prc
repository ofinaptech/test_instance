CREATE OR REPLACE PROCEDURE APPS.XXABRL_FA_REG_RPT_PROC
                              ( errbuf VARCHAR2
                               ,retcode NUMBER
                               ,p_book_code        FA_BOOKS.BOOK_TYPE_CODE%TYPE
                               --,p_period_name_from   fa_deprn_periods.PERIOD_COUNTER%type
                               ,p_period_name_to   fa_deprn_periods.PERIOD_COUNTER%type
                               ,dummy              NUMBER
                             ,p_cat_segment1      FA_CATEGORIES.SEGMENT1%TYPE
                            ,p_location          FA_LOCATIONS.SEGMENT1%TYPE
                                                          )
AS
 /*
  =========================================================================================================
  ||   Filename   : XXABRL_FA_REG_RPT_PROC.sql
  ||   Description : Script is used to mold Asset Register Data
  ||
  ||   Version     Date            Author              Modification
  ||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
  ||   1.0.0       05-OCT-2009    Naresh Hasti         New Development
  ||   1.0.1       30-oct-09      Naresh Hasti         Asset gl code column is taken from gl_code_combinations table.
  ||   1.0.2       25-nov-09      Naresh Hasti         Added the condition to pick the retaired asset in the report.
  ||   1.0.3       13-jul-11      mitul         Added the assign Qty Field
  ||   1.0.4       01-mar-2012    Vikash Kumar   Added six fields final_cost_assigned,deprn_assigned,deprn_till_date_assigned,accumulated_dep_assigned ,
                                                     opening_acc_dep,opening_acc_dep_assigned
  ||   1.0.5      04-apr-2012     Vikash Kumar          Added four fields Old_asset_number,Old_asset_original_cost,  Year, project_number( These fields added for taking reference in TSRL Migration
  ||   1.0.6      28-sep-2012     Vikash Kumar          Added   Book Type Code,change the logic for BOOK_TYPE_CODE parameter(new valueset attached XXABRL_FA_BOOKS instead of ABRL_BOOK_TYPE_CODE)
  ||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
  ========================================================================================================*/
l_concat_segments  VARCHAR2(500);
v_period_name varchar2(15);
CURSOR cur_data
IS
SELECT        bk.BOOK_TYPE_CODE,
TO_CHAR(Bk.DATE_PLACED_IN_SERVICE,'DD-MON-YYYY')  DPIS
,fm.attribute17 old_asset_no
                 ,ad.asset_number ASS_NO
                 ,FD.UNITS_ASSIGNED
                 ,ad.current_units QTY
                    ,loc.segment1 asset_LOCATION
                  ,fak.SEGMENT1 year
                  ,fak.SEGMENT2 project_number
                  ,cat.segment1 Major_cat
         ,cat.segment2 min_cat1
         ,cat.segment3 min_cat2
         ,ad2.description asset_desc
         ,fm.attribute18 old_asset_original_cost
         ,bk.original_cost
         ,bk.COST final_cost
         ,fr.nbv_retired --retired_cost-retired_dpr_cost
         ,gcck.CONCATENATED_SEGMENTS asset_gl_code
         ,gcck1.CONCATENATED_SEGMENTS dep_exp_gl_code
         ,gcck2.CONCATENATED_SEGMENTS dep_reserve_gl_code
         ,(bk.basic_rate*100) dep_com_act
         ,bk.deprn_method_code
            ,fr.date_retired
         ,fr.proceeds_of_sale,
         fdp.PERIOD_NAME
           , (CASE WHEN FDS.PERIOD_COUNTER < P_PERIOD_NAME_TO THEN 0
          ELSE fds.DEPRN_AMOUNT
          END) DEPRN_AMOUNT
         ,decode(XXABRL_GET_YTD_DEPRN(fd.ASSET_ID,p_book_code,p_period_name_to),0,0,-1,fds.YTD_DEPRN) ytd_value
         ,fds.DEPRN_RESERVE ltd_value
         ,fth.transaction_name
         ,fm.REVIEWER_COMMENTS,
         round(((bk.COST/ad.current_units)*FD.UNITS_ASSIGNED),2) final_cost_assigned,
         round((((CASE WHEN FDS.PERIOD_COUNTER < P_PERIOD_NAME_TO THEN 0
          ELSE fds.DEPRN_AMOUNT
          END)/ad.current_units)*FD.UNITS_ASSIGNED),2) deprn_assigned,
           round(((decode(XXABRL_GET_YTD_DEPRN(fd.ASSET_ID,p_book_code,p_period_name_to),0,0,-1,fds.YTD_DEPRN)/ad.current_units)*FD.UNITS_ASSIGNED),2) deprn_till_date_assigned,
          round(((fds.DEPRN_RESERVE/ad.current_units)*FD.UNITS_ASSIGNED),2) accumulated_dep_assigned,
          round((fds.DEPRN_RESERVE-decode(XXABRL_GET_YTD_DEPRN(fd.ASSET_ID,p_book_code,p_period_name_to),0,0,-1,fds.YTD_DEPRN)),2) opening_acc_dep,
         round((((fds.DEPRN_RESERVE/ad.current_units)*FD.UNITS_ASSIGNED)-((decode(XXABRL_GET_YTD_DEPRN(fd.ASSET_ID,p_book_code,p_period_name_to),0,0,-1,fds.YTD_DEPRN)/ad.current_units)*FD.UNITS_ASSIGNED)),2) opening_acc_dep_assigned
  FROM apps.fa_asset_history ah,
         apps.fa_books bk,
         apps.fa_categories cat,
         apps.fa_lookups lookups_at,
         apps.fa_lookups lookups_nu,
         apps.fa_lookups lookups_ol,
         apps.fa_lookups lookups_iu,
         apps.fa_lookups lookups_pt,
         apps.fa_lookups lookups_12,
         apps.fa_additions_tl ad2,
         apps.fa_additions_b ad
         --,FA_ASSET_INVOICES ai
         ,apps.fa_distribution_history  fd
         ,apps.FA_LOCATIONS loc
         ,apps.fa_retirements fr
         ,apps.fa_transaction_headers fth
         ,apps.FA_DEPRN_SUMMARY fds
         ,apps.fa_deprn_periods fdp
         ,apps.fa_books_bas bkprev
        ,apps.fa_mass_additions  fm
         ,apps.fa_distribution_accounts fda
         ,apps.gl_code_combinations_kfv  gcck
         ,apps.gl_code_combinations_kfv  gcck1
         ,apps.gl_code_combinations_kfv  gcck2
         ,apps.FA_ASSET_KEYWORDS fak
    WHERE   1=1
    and  ad.asset_id = ad2.asset_id
     AND ad.parent_asset_id IS NULL
     AND ad2.LANGUAGE = USERENV ('LANG')
     AND ah.asset_id = ad.asset_id
     AND ah.date_effective <= SYSDATE
     AND NVL (ah.date_ineffective, SYSDATE + 1) > SYSDATE
     AND ah.category_id = cat.category_id
     AND bk.asset_id = ad.asset_id
     AND bk.book_type_code  like ''||'%'||P_BOOK_CODE||'%'||''
     AND lookups_at.lookup_code = ad.asset_type
     AND lookups_at.lookup_type = 'ASSET TYPE'
     AND lookups_nu.lookup_code = ad.new_used
     AND lookups_nu.lookup_type = 'NEWUSE'
     AND lookups_ol.lookup_code = ad.owned_leased
     AND lookups_ol.lookup_type = 'OWNLEASE'
     AND lookups_iu.lookup_code = ad.in_use_flag
     AND lookups_iu.lookup_type = 'YESNO'
     AND lookups_pt.lookup_code(+) = ad.property_type_code
     AND lookups_pt.lookup_type(+) = 'PROPERTY TYPE'
     AND lookups_12.lookup_code(+) = ad.property_1245_1250_code
     AND lookups_12.lookup_type(+) = '1245/1250 PROPERTY'
     --AND  ad.asset_id = ai.asset_id(+)   --------- for invoice date
     AND ad.asset_id = fd.asset_id -- for distribution qty
     and ad.ASSET_NUMBER=fm.ASSET_NUMBER(+)
     and fm.FIXED_ASSETS_UNITS(+) <> 0
     AND fd.location_id = loc.location_id   -- for location
     AND bk.asset_id = fr.asset_id(+)
     AND bk.book_type_code= fr.book_type_code(+)
     AND ad.asset_id = NVL(fth.asset_id,ad.asset_id)
     AND NVL(fth.book_type_code,bk.book_type_code)=bk.book_type_code
     AND fr.transaction_header_id_in = fth.transaction_header_id(+)
     AND fr.transaction_header_id_in(+) = bk.transaction_header_id_in
     --and fr.RETIREMENT_PRORATE_CONVENTION(+) not like  'ABRL RETMT' ---retired asset could not come
     and fdp.BOOK_TYPE_CODE=fds.BOOK_TYPE_CODE
     and fdp.PERIOD_COUNTER=fds.PERIOD_COUNTER
     and fds.deprn_run_date in
       (select max(deprn_run_date)
        from apps.FA_DEPRN_SUMMARY fds1
        where fds1.ASSET_ID=fds.ASSET_ID)
        and (    bkprev.transaction_header_id_out(+) = bk.transaction_header_id_in
           --AND NVL (bkprev.COST, bk.COST - 1) != bk.COST
          )
    AND fd.DATE_INEFFECTIVE IS NULL
  and bk.DATE_INEFFECTIVE IS NULL
    AND ad.asset_id = fds.asset_id(+)
   and fd.DISTRIBUTION_ID=fda.DISTRIBUTION_ID
    and fda.ASSET_COST_ACCOUNT_CCID =gcck.CODE_COMBINATION_ID
    and fda.DEPRN_EXPENSE_ACCOUNT_CCID =gcck1.CODE_COMBINATION_ID
    and fda.DEPRN_RESERVE_ACCOUNT_CCID=gcck2.CODE_COMBINATION_ID
    --and fdp.PERIOD_COUNTER BETWEEN nvl(:P_PERIOD_NAME_FROM,fdp.PERIOD_COUNTER) AND nvl(:P_PERIOD_NAME_TO,fdp.PERIOD_COUNTER)
    and fdp.PERIOD_COUNTER <=P_PERIOD_NAME_TO
  AND cat.category_id = NVL(P_CAT_SEGMENT1,cat.category_id)
   AND loc.segment1 = NVL(P_LOCATION,loc.segment1)
   and ad.ASSET_KEY_CCID=fm.ASSET_KEY_CCID(+)  ---added on 13-mar-2012, because duplicate assets were showing----
      and ad.ASSET_KEY_CCID=fak.CODE_COMBINATION_ID
   order by ad.ASSET_NUMBER;
BEGIN
   begin
     SELECT  PERIOD_NAME into v_period_name
     FROM APPS.FA_DEPRN_PERIODS
     where PERIOD_COUNTER=p_period_name_to;
     EXCEPTION
       WHEN OTHERS THEN
     v_period_name :=null;
  end;
BEGIN
        SELECT segment1||'.'|| segment2||'.'|| segment3 INTO l_concat_segments
        FROM fa_categories
        WHERE category_id = p_cat_segment1;
    EXCEPTION
    WHEN OTHERS THEN
        Fnd_File.put_line(Fnd_File.OUTPUT,'Exception found in selecting categories');
    END;
    Fnd_File.put_line(Fnd_File.OUTPUT,CHR(9)            ||'XXABRL Fixed Asset Register Report');
    Fnd_File.put_line(Fnd_File.OUTPUT,CHR(9)            ||'                   ');
    Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'As on Date:'
                         || CHR (9)
                         || SYSDATE
                         || CHR (9)
                        );
      Fnd_File.put_line (Fnd_File.output, ' ');
      Fnd_File.put_line (Fnd_File.output, 'REPORT PARAMETRS ');
      Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'BOOK TYPE CODE:'
                         || CHR (9)
                         || p_book_code
                         || CHR (9)
                        );
    Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Period Name:'
                         || CHR (9)
                         || v_period_name
                         || CHR (9)
                        );
  Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Category Segments:'
                         || CHR (9)
                         || l_concat_segments
                         || CHR (9)
                        );
      Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Asset Location:'
                         || CHR (9)
                         || p_location
                         || CHR (9)
                        );
               Fnd_File.put_line(Fnd_File.OUTPUT,CHR(9) ||'Book Type Code'||CHR(9)
       ||'Date in Service'||CHR(9)
                               ||'Old Asset Number'||CHR(9)
                               ||'Asset Number'||CHR(9)
                               ||'Quantity'||CHR(9)
                               ||'Assigned Quantity'||CHR(9)
                                      ||'Asset location'||CHR(9)
                                      ||'Year'||CHR(9)
                                      ||'Project Number'||CHR(9)
                                      ||'Major Category'||CHR(9)
                                   ||'Minor Category1'||CHR(9)
                               ||'Minor category2'||CHR(9)
                                ||'Asset Description'||CHR(9)
                                ||'Original Cost of Old Assets'||CHR(9)
                                      ||'Original Cost of Assets'||CHR(9)
                                      ||'Final Cost of Assets'||CHR(9)
                                      ||'Final Cost Assigned'||CHR(9)
                                      ||'Depreciation charged for Month'||CHR(9)
                                      ||'Deprn for the Month Assigned'||CHR(9)
                                      ||'Depreciation charged till Date'||CHR(9)
                                      ||'Deprn charged till Date Assigned'||CHR(9)
                                      ||'Accumulated Depreciation'||CHR(9)
                                      ||'Accumulated Depreciation Assigned'||CHR(9)
                                      ||'Opening Acc Depreciation'||CHR(9)
                                      ||'Opening Acc Depreciation Assigned'||CHR(9)
                                   ||'WDV Cost of Retired Asset'||CHR(9)
                                   ||'Asset GL Code'||CHR(9)
                                   ||'Dep.Exp GL Code'||CHR(9)
                                   ||'Dep Reserve  GL Code'||CHR(9)
                                      ||'Depreciation Companies Act'||CHR(9)
                                      ||'Overriding Depreciation method'||CHR(9)
                                                 ||'Sale of Asset Date'||CHR(9)
                               ||'Realisation Value'||CHR(9)
                               ||'Remarks for sale of Assets'||CHR(9)
                 ||'REVIEWER COMMENTS'||CHR(9)
                               );
                FOR rec_data IN cur_data
       LOOP
                  --Printing Local Variable Values to output.
                  Fnd_File.put_line(Fnd_File.OUTPUT,CHR(9) ||rec_data.BOOK_TYPE_CODE||CHR(9)
                  ||rec_data.dpis||CHR(9)
                  ||rec_data.old_asset_no||CHR(9)
                               ||rec_data.ASS_NO||CHR(9)
                               ||rec_data.QTY||CHR(9)           -- qty
                                       ||rec_data.UNITS_ASSIGNED||CHR(9)
                                      ||rec_data.asset_location||CHR(9)
                                      ||rec_data.year||CHR(9)
                                      ||rec_data.project_number||CHR(9)
                                      ||rec_data.Major_cat||CHR(9)
                                   ||rec_data.min_cat1||CHR(9)
                               ||rec_data.min_cat2||CHR(9)
                               ||rec_data.asset_desc||CHR(9)
                               ||rec_data.old_asset_original_cost||CHR(9)
                                      ||rec_data.original_cost||CHR(9)
                                      ||rec_data.final_cost||CHR(9)
                                      ||rec_data.final_cost_assigned||CHR(9)
                                      ||rec_data.deprn_amount||CHR(9)
                                      ||rec_data.deprn_assigned||CHR(9)
                                      ||rec_data.ytd_value||CHR(9)     -- dep charged till date
                                      ||rec_data.deprn_till_date_assigned||CHR(9)
                                      ||rec_data.ltd_value||CHR(9)     -- accumulated dep
                                      ||rec_data.accumulated_dep_assigned||CHR(9)
                                      ||rec_data.opening_acc_dep||CHR(9)
                                      ||rec_data.opening_acc_dep_assigned||CHR(9)
                                   ||rec_data.nbv_retired||CHR(9)
                                   ||rec_data.asset_gl_code||CHR(9)
                                   ||rec_data.dep_exp_gl_code||CHR(9)
                                   ||rec_data.dep_reserve_gl_code||CHR(9)
                                      ||rec_data.dep_com_act||CHR(9)
                                      ||rec_data.deprn_method_code||CHR(9)
                                      ||rec_data.date_retired||CHR(9)
                               ||rec_data.proceeds_of_sale||CHR(9)
                               ||rec_data.transaction_name||CHR(9)
                 ||rec_data.REVIEWER_COMMENTS||CHR(9)
                               );
       END LOOP;
END XXABRL_FA_REG_RPT_PROC; 
/


CREATE OR REPLACE PACKAGE BODY APPS.XXABRL_MAPPING_MAILER IS
---
PROCEDURE XXABRL_VENDOR_MAP_MAILER ( errbuf     out VARCHAR2
                                  ,      retcode    out VARCHAR2
                                   ) as
--This Procedure will Send all Vendor Mapping Error
/*===============================================================================

||   Procedure Name  : XXABRL_MAPPING_MAILER
    Description : XXABRL Vendor Mapping Mailer
   
     Version     Date            Author              Modification
    ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
     1.0.0      24-Dec-2010  Mitul and  Ravi      New Development    
     1.0.1      18-may-2011   Ravi                Added Dhiresh More and comented ritesh kapu mail id's 
     1.0.2      14-oct-2011  Dhiresh More         Remove name of Siddhesh from mailing list and include 'HM gopalan' Location in HM mailing list     

===============================================================================*/
 stop_program      EXCEPTION;
   v_email_subject   VARCHAR2 (200);
   v_email_list      VARCHAR2 (200);
   v_errmsg          VARCHAR2 (200);
   v_status_code     VARCHAR2 (200);
   mailhost          VARCHAR2 (40)       := 'mail.adityabirla.com';
   crlf              VARCHAR2 (2)        := CHR (13) || CHR (10);
   v_email           VARCHAR2 (16000);
   message_1         varchar2(20000);
   hm_message_2         VARCHAR2 (20000);
   sm_message_2         VARCHAR2 (20000);
   message_3         VARCHAR2 (20000);
   mail_conn         UTL_SMTP.connection;
   v_from_mail       VARCHAR2 (50)       := 'naresh.hasti-v@retail.adityabirla.com';
   i integer:=0;
   j integer:=0;
--v_to_mail varchar2(60):= 'srinivas.thalla-v@retail.adityabirla.com';
    begin
  BEGIN
   FOR vndr_hm_mail IN (   
   SELECT DISTINCT a.operating_unit, 
                decode(a.operating_unit,'ABRL GOPAL','ABRL GOPALHM',a.operating_unit) OU, --(code change by Dhiresh to include ABRL Gopal in HM mailer list)
                a.vendor_number,
                a.vendor_dc_code, a.error_message
     FROM apps.xxabrl_ap_invoices_int a
     WHERE SOURCE = 'NAVISION'
           AND TRUNC (gl_date) BETWEEN '01-APR-11'
           AND TRUNC (SYSDATE)
           AND interfaced_flag = 'E'
           AND error_message LIKE '%Vendor%does%'
           --and operating_unit like 'ABRL%'
           -- AND operating_unit LIKE '%HM%'
           --group by operating_unit
           ORDER BY        a.operating_unit, a.vendor_number                        
                        )
               LOOP
                       if vndr_hm_mail.OU like '%HM%' THEN
                       hm_message_2:= hm_message_2 ||'</tr>
                              <font face=''Calibri''>
                              <Tr>
                              <td ALIGN=''left''>'||vndr_hm_mail.OPERATING_UNIT||'</td>
                              <td ALIGN=''left''>'||vndr_hm_mail.VENDOR_NUMBER||'</td>
                              <td ALIGN=''left''>'||vndr_hm_mail.VENDOR_DC_CODE||'</td>
                              <td ALIGN=''left''>'||vndr_hm_mail.ERROR_MESSAGE||'</td>
                              </tr>';
                              i:=i+1;
                       END IF;
                               if vndr_hm_mail.OU not like '%HM%' THEN
                               sm_message_2:= sm_message_2 ||'</tr>
                                      <font face=''Calibri''>
                                      <Tr>
                                      <td ALIGN=''left''>'||vndr_hm_mail.OPERATING_UNIT||'</td>
                                      <td ALIGN=''left''>'||vndr_hm_mail.VENDOR_NUMBER||'</td>
                                      <td ALIGN=''left''>'||vndr_hm_mail.VENDOR_DC_CODE||'</td>
                                      <td ALIGN=''left''>'||vndr_hm_mail.ERROR_MESSAGE||'</td>
                                      </tr>';
                                      j:=j+1;
                               END IF;
               end loop;
      BEGIN
      message_1 :='<html>
          <body>
          <font face=''Calibri''>
          Dear '||'All'||',
          <br/>
           <br/>
          <b>
          Below are the Vendor Mapping Error List '||to_char(sysdate,'dd-mm-yyyy')||','||'
                    <br/>
          <br/>
          </font>
           <table Border=''1'' autosize=''true''>
           <font face=''Calibri'' color=''white''>
          <Tr bgcolor=''gray'' >
          <td ALIGN=''left'' color=''white''><b>OPERATING UNIT</b></td>
          <td ALIGN=''left''><b>VENDOR NUMBER</b></td>
          <td ALIGN=''left'' color=''white''><b>VENDOR DC CODE</b></td>
          <td ALIGN=''left''><b>ERROR MESSAGE</b></td>
                              </font>
          </tr>
          <font face=''Calibri''>';
       message_3 :='
          </font>
          </table>
              <br/>
            <!--                          <font face=''Calibri'' color=''black'' bold=''true''>
          This is auto generated mail by the system, Please do not reply or respond    
          <br/>
          
          Please provide the mapping to the below email id
          <br/>
          naresh.hasti-v@retail.adityabirla.com
                          .
          </font>
          -->
            <br/><br/>
          <font face=''Calibri'' color=''Lime Green'' bold=''true''>
          <b>Thanks and Regards</b>,
          <br/>
          <b>OFIN Support</b>
          </font>
          </body>
          </html>';
          
         
       ----Hyper Market ----------
       
       if i>=1 then
       
         mail_conn := UTL_SMTP.open_connection (mailhost, 25);
         UTL_SMTP.helo (mail_conn, mailhost);
         UTL_SMTP.mail (mail_conn, v_from_mail);
         
         UTL_SMTP.rcpt (mail_conn, 'sushma.samal@retail.adityabirla.com');
         UTL_SMTP.rcpt (mail_conn, 'dinesh.chauhan@retail.adityabirla.com');
         UTL_SMTP.rcpt (mail_conn, 'abrl-mum.cogsupport@retail.adityabirla.com');
         UTL_SMTP.rcpt (mail_conn, 'shalini.pal@retail.adityabirla.com');
         UTL_SMTP.rcpt (mail_conn, 'balasubramanian.sk@retail.adityabirla.com');
         UTL_SMTP.rcpt (mail_conn, 'neilmani.sahu@retail.adityabirla.com');
         UTL_SMTP.rcpt (mail_conn, 'vidhya.srinivasan@retail.adityabirla.com');
         UTL_SMTP.rcpt (mail_conn, 'shiv.murti@retail.adityabirla.com');
         UTL_SMTP.rcpt (mail_conn, 'rajukumar.dokania@retail.adityabirla.com');
         UTL_SMTP.rcpt (mail_conn, 'alpana.monani@retail.adityabirla.com');
         UTL_SMTP.rcpt (mail_conn, 'kishore.ch@retail.adityabirla.com');
         UTL_SMTP.rcpt (mail_conn, 'naresh.hasti-v@retail.adityabirla.com');
         UTL_SMTP.rcpt (mail_conn, 'suresh.ramisetti@retail.adityabirla.com');
         UTL_SMTP.rcpt (mail_conn, 'dhiresh.more@retail.adityabirla.com');
             
         UTL_SMTP.open_data (mail_conn);
         UTL_SMTP.write_data (mail_conn,
                                 'Date: '
                              || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS')
                              || UTL_TCP.crlf
                             );
         UTL_SMTP.write_data (mail_conn,
                              'From: ' || v_from_mail || UTL_TCP.crlf
                             );
        
          utl_smtp.write_data(mail_conn, 'To: ' || 'sushma.samal@retail.adityabirla.com'||';'||'dinesh.chauhan@retail.adityabirla.com'||';'||'abrl-mum.cogsupport@retail.adityabirla.com'|| utl_tcp.crlf);        
          utl_smtp.write_data(mail_conn, 'CC: '   || 'shalini.pal@retail.adityabirla.com'||';'||'balasubramanian.sk@retail.adityabirla.com'||';'||'neilmani.sahu@retail.adityabirla.com'||';'||'vidhya.srinivasan@retail.adityabirla.com'||';'||'shiv.murti@retail.adityabirla.com'||';'||'rajukumar.dokania@retail.adityabirla.com'||';'||'alpana.monani@retail.adityabirla.com'||';'||'kishore.ch@retail.adityabirla.com'||';'||'suresh.ramisetti@retail.adityabirla.com'||';'||'naresh.hasti-v@retail.adityabirla.com'||';'||'dhiresh.more@retail.adityabirla.com'|| utl_tcp.crlf);         
         --utl_smtp.write_data(mail_conn, 'Sent: '  || TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') || utl_tcp.crlf);
         UTL_SMTP.write_data (mail_conn,
                              'Subject: ' || 'Vendor Mapping HM' || UTL_TCP.crlf
                             );
         UTL_SMTP.write_data (mail_conn,
                              'Content-Type: text/html' || UTL_TCP.crlf
                             );
         UTL_SMTP.write_data (mail_conn, UTL_TCP.crlf || message_1 || hm_message_2 || message_3);
         UTL_SMTP.close_data (mail_conn);
         UTL_SMTP.quit (mail_conn);
      /* EXCEPTION WHEN OTHERS THEN
       fnd_file.put_line (fnd_file.LOG, 'Mail Address Not Valid :  ' || 'naresh.hasti-v@retail.adityabirla.com');
       --retcode:=1;
       end;*/
       end if;
       
       ---Super Market-----
       if j>=1 then
       mail_conn := UTL_SMTP.open_connection (mailhost, 25);
         UTL_SMTP.helo (mail_conn, mailhost);
         UTL_SMTP.mail (mail_conn, v_from_mail);
          UTL_SMTP.rcpt (mail_conn, 'srinivas.thalla-v@retail.adityabirla.com');
        UTL_SMTP.rcpt (mail_conn, 'abrl-mum.cogsupport@retail.adityabirla.com');
         UTL_SMTP.rcpt (mail_conn, 'pravin.gupta@retail.adityabirla.com');
         --UTL_SMTP.rcpt (mail_conn, 'ritesh.kapur@adityabirla.com');
         UTL_SMTP.rcpt (mail_conn, 'balasubramanian.sk@retail.adityabirla.com');
         UTL_SMTP.rcpt (mail_conn, 'shalini.pal@retail.adityabirla.com');
         UTL_SMTP.rcpt (mail_conn, 'sachin.dedhia@retail.adityabirla.com');
         --UTL_SMTP.rcpt (mail_conn, 'tarit.ghosal@retail.adityabirla.com');
         UTL_SMTP.rcpt (mail_conn, 'naresh.hasti-v@retail.adityabirla.com');
         UTL_SMTP.rcpt (mail_conn, 'suresh.ramisetti@retail.adityabirla.com');
        UTL_SMTP.rcpt (mail_conn, 'dhiresh.more@retail.adityabirla.com');
                
         UTL_SMTP.open_data (mail_conn);
         UTL_SMTP.write_data (mail_conn,
                                 'Date: '
                              || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS')
                              || UTL_TCP.crlf
                             );
         UTL_SMTP.write_data (mail_conn,
                              'From: ' || v_from_mail || UTL_TCP.crlf
                             );
        
         utl_smtp.write_data(mail_conn, 'To: ' || 'abrl-mum.cogsupport@retail.adityabirla.com'||utl_tcp.crlf);  
        utl_smtp.write_data(mail_conn, 'CC: '   ||'pravin.gupta@retail.adityabirla.com'||';'||'dhiresh.more@retail.adityabirla.com'||';'||'balasubramanian.sk@retail.adityabirla.com'||';'||'shalini.pal@retail.adityabirla.com'||';'||'sachin.dedhia@retail.adityabirla.com'||';'||'naresh.hasti-v@retail.adityabirla.com'||';'||'suresh.ramisetti@retail.adityabirla.com'|| utl_tcp.crlf);          
         --utl_smtp.write_data(mail_conn, 'Sent: '  || TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') || utl_tcp.crlf);
         UTL_SMTP.write_data (mail_conn,
                              'Subject: ' || 'Vendor Mapping SM' || UTL_TCP.crlf
                             );
         UTL_SMTP.write_data (mail_conn,
                              'Content-Type: text/html' || UTL_TCP.crlf
                             );
         UTL_SMTP.write_data (mail_conn, UTL_TCP.crlf || message_1 || sm_message_2 || message_3);
         UTL_SMTP.close_data (mail_conn);
         UTL_SMTP.quit (mail_conn);
       end if;
      
      EXCEPTION
          WHEN OTHERS
          THEN
             fnd_file.put_line (fnd_file.LOG,
                                'Mail Address Not Valid '
                               );
             retcode := 1;
       END;

  

    end;
   
    end XXABRL_VENDOR_MAP_MAILER;


PROCEDURE XXABRL_CUSTOMER_MAP_MAILER (  errbuf     out VARCHAR2
                                  ,  retcode    out VARCHAR2) as
--This Procedure will Send all Vendor Mapping Error
/*===============================================================================

||   Procedure Name  : XXABRL_MAPPING_MAILER
    Description : XXABRL Customer Mapping Mailer
   
     Version     Date            Author              Modification
    ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
     1.0.0      24-Dec-2010  Mitul and  Ravi      New Development    

===============================================================================*/

    stop_program      EXCEPTION;
   v_email_subject   VARCHAR2 (200);
   v_email_list      VARCHAR2 (200);
   v_errmsg          VARCHAR2 (200);
   v_status_code     VARCHAR2 (200);
   mailhost          VARCHAR2 (40)       := 'mail.adityabirla.com';
   crlf              VARCHAR2 (2)        := CHR (13) || CHR (10);
   v_email           VARCHAR2 (16000);
   message_1         varchar2(20000);
   message_2         VARCHAR2 (20000);
   message_3         VARCHAR2 (20000);
   mail_conn         UTL_SMTP.connection;
   v_from_mail       VARCHAR2 (50)       := 'naresh.hasti-v@retail.adityabirla.com';
   i integer:=0;
   j integer:=0;
--v_to_mail varchar2(60):= 'srinivas.thalla-v@retail.adityabirla.com';
begin
    BEGIN
        FOR cust_mail IN (    
                       select distinct a.ORG_CODE,a.CUSTOMER_NAME Customer_number,a.ERROR_MESSAGE
                     from apps.XXABRL_NAVI_RA_INTIS_LINES_ALL a
                    where --INTERFACE_LINE_ATTRIBUTE1 like '%PSINV0910/000102%'
                    org_code like 'ABRL%'
                    --AND TRUNC(GL_DATE)='27-AUG-09'
                    and a.customer_name not in ('MAHARASTRA','RAJ')
                    and error_message like '%No Mapping for customer%'
                    order by a.ORG_CODE,a.CUSTOMER_NAME                                  
                        )
               LOOP
                message_2:= message_2 ||'</tr>
                      <font face=''Calibri''>
                      <Tr>
                      <td ALIGN=''left''>'||cust_mail.ORG_CODE||'</td>
                      <td ALIGN=''left''>'||cust_mail.CUSTOMER_NUMBER||'</td>
                      <td ALIGN=''left''>'||cust_mail.ERROR_MESSAGE||'</td>
                      </tr>';
                      i:=i+1;
              
               end loop;
      BEGIN
      message_1 :='<html>
          <body>
          <font face=''Calibri''>
          Dear '||'All'||',
          <br/>
           <br/>
          <b>
          Below are the Customer Mapping Error List '||to_char(sysdate,'dd-mm-yyyy')||','||'
                    <br/>
          <br/>
          </font>
           <table Border=''1'' autosize=''true''>
           <font face=''Calibri'' color=''white''>
          <Tr bgcolor=''gray'' >
          <td ALIGN=''left'' color=''white''><b>Organization Code</b></td>
          <td ALIGN=''left''><b>Customer Number</b></td>
          <td ALIGN=''left'' color=''white''><b>Error Message</b></td>
           </font>
          </tr>
          <font face=''Calibri''>';
       message_3 :='
          </font>
          </table>
                <br/>
                 <!--                       <font face=''Calibri'' color=''black'' bold=''true''>
          This is auto generated mail by the system, Please do not reply or respond                    .
          </font>
          --> 
            <br/><br/>
          <font face=''Calibri'' color=''blue'' bold=''true''>
          <b>Thanks and Regards</b>,
          <br/>
          <b>OFIN Support</b>
          </font>
          </body>
          </html>';
          
         
       ----Hyper Market ----------
       
       if i>=1 then
       
         mail_conn := UTL_SMTP.open_connection (mailhost, 25);
         UTL_SMTP.helo (mail_conn, mailhost);
         UTL_SMTP.mail (mail_conn, v_from_mail);
         UTL_SMTP.rcpt (mail_conn, 'abrl-mum.cogsupport@retail.adityabirla.com');
         UTL_SMTP.rcpt (mail_conn, 'pravin.gupta@retail.adityabirla.com');
         --UTL_SMTP.rcpt (mail_conn, 'ritesh.kapur@adityabirla.com');
         UTL_SMTP.rcpt (mail_conn, 'balasubramanian.sk@retail.adityabirla.com');
         UTL_SMTP.rcpt (mail_conn, 'shalini.pal@retail.adityabirla.com');
         UTL_SMTP.rcpt (mail_conn, 'sachin.dedhia@retail.adityabirla.com');
         --UTL_SMTP.rcpt (mail_conn, 'tarit.ghosal@retail.adityabirla.com');
         --UTL_SMTP.rcpt (mail_conn, 'komal.kaul@retail.adityabirla.com');
         UTL_SMTP.rcpt (mail_conn, 'naresh.hasti-v@retail.adityabirla.com');
         UTL_SMTP.rcpt (mail_conn, 'suresh.ramisetti@retail.adityabirla.com');
         UTL_SMTP.rcpt (mail_conn, 'dhiresh.more@retail.adityabirla.com');
                     
         UTL_SMTP.open_data (mail_conn);
         UTL_SMTP.write_data (mail_conn,
                                 'Date: '
                              || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS')
                              || UTL_TCP.crlf
                             );
         UTL_SMTP.write_data (mail_conn,
                              'From: ' || v_from_mail || UTL_TCP.crlf
                             );
       UTL_SMTP.write_data
                  (mail_conn,
                    'To: '
                  || SUBSTR
                        ('abrl-mum.cogsupport@retail.adityabirla.com',
                            0,
                               INSTR
                                    ('abrl-mum.cogsupport@retail.adityabirla.com',
                                     '@'
                                   )
                            - 1
                            )
                   || UTL_TCP.crlf
                  );
        utl_smtp.write_data(mail_conn, 'CC: '   ||'pravin.gupta@retail.adityabirla.com'||';'||'dhiresh.more@retail.adityabirla.com'||';'||'balasubramanian.sk@retail.adityabirla.com'||';'||'shalini.pal@retail.adityabirla.com'||';'||'sachin.dedhia@retail.adityabirla.com'||';'||'naresh.hasti-v@retail.adityabirla.com'||';'||'suresh.ramisetti@retail.adityabirla.com'||utl_tcp.crlf);
         --utl_smtp.write_data(mail_conn, 'Sent: '  || TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') || utl_tcp.crlf);
         UTL_SMTP.write_data (mail_conn,
                              'Subject: ' || 'Customer Mapping' || UTL_TCP.crlf
                             );
         UTL_SMTP.write_data (mail_conn,
                              'Content-Type: text/html' || UTL_TCP.crlf
                             );
         UTL_SMTP.write_data (mail_conn, UTL_TCP.crlf || message_1 || message_2 || message_3);
         UTL_SMTP.close_data (mail_conn);
         UTL_SMTP.quit (mail_conn);
      /* EXCEPTION WHEN OTHERS THEN
       fnd_file.put_line (fnd_file.LOG, 'Mail Address Not Valid :  ' || 'naresh.hasti-v@retail.adityabirla.com');
       --retcode:=1;
       end;*/
       end if;
       

  
        EXCEPTION
          WHEN OTHERS
          THEN
             fnd_file.put_line (fnd_file.LOG,
                                'Mail Address Not Valid '
                               );
             retcode := 1;
       END;

    end;
    end XXABRL_CUSTOMER_MAP_MAILER;

end XXABRL_MAPPING_MAILER; 
/


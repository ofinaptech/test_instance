CREATE OR REPLACE PACKAGE APPS.xxmrl_tax_inv_bkdt_load_pkg
IS
   PROCEDURE xxmrl_wait_for_req_prc2 (p_request_id NUMBER, p_req_name VARCHAR2);

   PROCEDURE xxmrl_tax_inv_dt_load_prc(
      errbuf    OUT   VARCHAR2,
      retcode   OUT   NUMBER
   );
   
END xxmrl_tax_inv_bkdt_load_pkg; 
/


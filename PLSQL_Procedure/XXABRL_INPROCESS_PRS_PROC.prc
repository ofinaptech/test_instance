CREATE OR REPLACE procedure APPS.xxabrl_inprocess_prs_proc( errbuf         OUT   VARCHAR2,
   retcode        OUT   NUMBER) as
/*
   =========================================================================================================
   ||   Procedure Name  : xxabrl_inprocess_prs_proc
   ||   Description : ABRL In-Process Requisitions Concurrent Mailer Scheduler
   ||
   ||   Version     Date            Author              Modification
   ||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
     ||   1.0.0      29-Jun-2011  Uma Mahesh and Mitul      New Development
     ||   1.0.1     13-jul-2011  Uma Mahesh                Added E-mail id
    
   =======================================================================================================*/   
   stop_program      EXCEPTION;
   v_email_subject   VARCHAR2 (200);
   v_email_list      VARCHAR2 (200);
   v_errmsg          VARCHAR2 (200);
   v_status_code     VARCHAR2 (200);
   mailhost varchar2 (40) :='mail.adityabirla.com'; 
   crlf varchar2(2)       := CHR (13) || CHR (10);
   v_email varchar2(16000);
   message_1 LONG;
   message_2 varchar2(20000);
   mail_conn utl_smtp.connection;
   v_from_mail varchar2(50):='OFIN Support';
   --v_to_mail varchar2(60):= 'srinivas.thalla-v@retail.adityabirla.com';
   v_color varchar2(20);
   text_message long;
   text_message1  varchar2(16000);
   text_message2  varchar2(16000);
   text_message3  varchar2(16000);

begin

for sel_main in (SELECT  distinct  a.PERSON_ID,a.EMAIL_ADDRESS,a.last_name
          FROM apps.per_all_people_f a ,
      apps.po_requisition_headers_all porh,
      apps.fnd_user fu,
      apps.hr_operating_units hou
         WHERE 1=1 
         and a.person_id = porh.preparer_id
         and hou.ORGANIZATION_ID=porh.ORG_ID
         and sysdate between a.effective_start_date and a.effective_end_date
         and fu.EMPLOYEE_ID = a.PERSON_ID
         and fu.END_DATE is null
         and upper(porh.AUTHORIZATION_STATUS) ='IN PROCESS'
         and trunc(porh.CREATION_DATE)  between '1-apr-2010' and trunc(sysdate)
         --and porh.SEGMENT1 IN ('912000213','622000235')
       -- and  a.FULL_NAME in ('Ravi Bahl, Mr.','Varadkar, Mr. Ninad','Balaji, Rekha')
         and a.attribute1=512) 
         
         
         
loop


       text_message1 :=
       '<html>
                        <body>
                        <font face=''Calibri''>
                       Dear ' 
                       ||SEL_main.LAST_NAME 
                       || ',
                         <br/>
                        <br/>
                        <b>Below are the In-Process Purchase Requisitions as on ' 
                        || TO_CHAR (SYSDATE, 'dd-mm-yyyy')||'<br/><br/>
                                                                        
                        <TABLE height=72 cellSpacing=0 cellPadding=0 width=770 border=1>
                          <Tr bgcolor =''Gray''>
                        <font  face=''Calibri''>
                       <b><td>Operating Unit</td>
                       <td>PR Number</td>
                       <td>PR Creator</td>
                       <td>Status</td>
                       <td>Creation Date</td></b></tr>';
                       
      text_message2:=        
      '</table>
                              <br/>                                                    
                        </body>
                        </html>';
                        
                        
                              
                        



FOR SEL_PR IN (
SELECT   hou.NAME,
         porh.SEGMENT1 PR_Number,
         a.full_name PR_Creator,
         porh.AUTHORIZATION_STATUS Status,
         trunc(porh.creation_date) creation_date,
         a.EMAIL_ADDRESS
          FROM apps.per_all_people_f a ,
      apps.po_requisition_headers_all porh,
      apps.fnd_user fu,
      apps.hr_operating_units hou
         WHERE 1=1
         and a.person_id = porh.preparer_id
         and hou.ORGANIZATION_ID=porh.ORG_ID
         and sysdate between a.effective_start_date and a.effective_end_date
         and fu.EMPLOYEE_ID = a.PERSON_ID
         and fu.END_DATE is null
         and upper(porh.AUTHORIZATION_STATUS) ='IN PROCESS'
         and trunc(porh.CREATION_DATE)  between '1-apr-2010' and trunc(sysdate)
         and a.PERSON_ID=sel_main.person_id
         --and porh.SEGMENT1 IN ('912000213','622000235')
        --and  a.FULL_NAME in ('Ravi Bahl, Mr.')--,'Varadkar, Mr. Ninad')
         and a.attribute1=512         
        order by 1,5 )      
      
      
                       
    
      loop
         text_message :=text_message||
                         '<Tr>
                       <td>'||SEL_PR.NAME||'</td>
                       <td>'||SEL_PR.PR_Number||'</td>
                       <td>'||SEL_PR.PR_Creator||'</td>
                       <td>'||SEL_PR.status||'</td>
                       <td>'||SEL_PR.creation_date||'</td>
                        </tr></br>' ;                  
                        
       
   end loop;         
     
    text_message3 := '<font face=''Calibri'' color=''blue'' bold=''true''>
               This is auto generated mail by the system, Please do not reply or respond                    .
                        </font>
                          <br/><br/>
                        <font face=''Calibri'' color=''blue'' bold=''true''>
                        <b>Thanks and Regards</b>,
                        <br/>
                        <b>OFIN Support</b>
                        </font>';
   
       mail_conn := utl_smtp.open_connection (mailhost, 25);
                    utl_smtp.helo (mail_conn, mailhost);
                    utl_smtp.mail (mail_conn, v_from_mail);
                 --   utl_smtp.rcpt (mail_conn, 'srinivas.thalla-v@retail.adityabirla.com');
                    utl_smtp.rcpt (mail_conn,sel_main.EMAIL_ADDRESS);
                    utl_smtp.rcpt (mail_conn,'raja.gorla@retail.adityabirla.com');
                    utl_smtp.rcpt (mail_conn,'ajaykumar.singh@retail.adityabirla.com');
                    utl_smtp.open_data(mail_conn);
                    UTL_SMTP.write_data(mail_conn, 'Date: ' || TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') || utl_tcp.crlf); 
                    utl_smtp.write_data(mail_conn, 'From: ' || v_from_mail || utl_tcp.crlf);
                    utl_smtp.write_data(mail_conn, 'To: '   || substr('raja.gorla@retail.adityabirla.com',0,instr('raja.gorla@retail.adityabirla.com','@')-1) || utl_tcp.crlf);
                    utl_smtp.write_data(mail_conn, 'To: '   || substr('ajaykumar.singh@retail.adityabirla.com',0,instr('ajaykumar.singh@retail.adityabirla.com','@')-1) || utl_tcp.crlf);
                    --utl_smtp.write_data(mail_conn, 'Sent: '  || TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') || utl_tcp.crlf);
                    utl_smtp.write_data(mail_conn, 'Subject: ' || 'In-Process Purchase Requisitions' || utl_tcp.crlf);
                    utl_smtp.write_data(mail_conn, 'Content-Type: text/html' || utl_tcp.crlf);
                    utl_smtp.write_data(mail_conn, utl_tcp.crlf || text_message1 || text_message || text_message2||text_message3 );
                    utl_smtp.close_data(mail_conn);
                    utl_smtp.quit(mail_conn);  
                    
                   
                    text_message  :=null;
                    
   
                 
end loop; 


                   
                                     
end; 
/


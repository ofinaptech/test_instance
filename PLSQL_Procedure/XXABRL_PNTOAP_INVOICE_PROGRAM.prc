CREATE OR REPLACE PROCEDURE APPS.xxabrl_pntoap_invoice_program (
   errbuf    OUT   VARCHAR2,
   retcode   OUT   VARCHAR2
)
AS
/*****************************************************************************************************************************************
                 OBJECT INFORMATION

 Object Name : XXABRL_PNTOAP_INVOICE_PROGRAM

 Description : Procedure to Calculate Service Tax and TDS on Property Manager Invoices in interface level

  Change Record:
 =================================================
=========================
=========================
==========
 Version    Date            Author                              Remarks
 =======   ==========     =====
========                         =================================
=========================
===============
 1.1.0     01-August-09   Praveen Kumar (Wipro Infotech Ltd)    Voucher Number Generation Code Commented for errored in interface level
 1.1.1     01-November-09 Praveen Kumar (Wipro Infotech Ltd)   Rent starting from 1st day of month and ending on middle of the month
                                                               Rent starting from middle of the month and ending on last day of the month
 1.1.2     28-may-2010   Praveen /SaiKrishna (Wipro Infotech Ltd)   Updating Loction and Cc in Codecombination.

*****************************************************************************************************************************************/
   v_invoice_id                 ap_invoice_lines_interface.invoice_id%TYPE;
   v_invoice_line_num           ap_invoice_lines_interface.invoice_line_id%TYPE;
   v_line_num                   ap_invoice_lines_interface.line_number%TYPE;
   v_amount                     ap_invoice_lines_interface.amount%TYPE;
   v_description                ap_invoice_lines_interface.description%TYPE;
   v_user_id                    NUMBER       := fnd_profile.VALUE ('USER_ID');
   v_org_id                     NUMBER        := fnd_profile.VALUE ('ORG_ID');
   v_tax_rate                   NUMBER;
   v_total                      NUMBER;
   v_total1                     NUMBER;
   v_cat_pre                    NUMBER;
   v_count                      NUMBER;
   v_dist_code_combination_id   NUMBER;
   v_rent_cc_id                 NUMBER;
   v_tot_line_amount            NUMBER;
   v_temp                       NUMBER;
   v_segment1                   VARCHAR2 (15);
   v_segment2                   VARCHAR2 (15);
   v_segment3                   VARCHAR2 (15);
   v_segment4                   VARCHAR2 (15);
   v_segment5                   VARCHAR2 (15);
   v_segment6                   VARCHAR2 (15);
   v_segment7                   VARCHAR2 (15);
   v_segment8                   VARCHAR2 (15);
   v_cc_id                      NUMBER                                   := 0;
   v_cc_id1                     NUMBER                                   := 0;
   v_chart_of_accounts_id       NUMBER;
   x_concatenated_segments      VARCHAR2 (100);
   v_err_msg                    VARCHAR2 (2000);
   v_err_flag                   VARCHAR2 (1);
   v_return_code                VARCHAR2 (1);

/**********************************************************************************
       ---Cursor for getting the Lease Details along with payment terms-----
 **********************************************************************************/
   CURSOR cur_pmnt_term
   IS
      SELECT    pla.lease_num
             || ''
             || ppta.vendor_id
             || ''
             || ppsa.schedule_date lease_vendor,
             ppta.vendor_id, ppsa.schedule_date invoice_date,
             ppta.attribute1 service_tax_type, ppta.attribute2 tax_flag,
             ppta.attribute4 cc_id, ppta.attribute5 tds, pla.lease_num,
             TO_CHAR (ppsa.schedule_date, 'DD-MON-YY'),
             ppta.payment_purpose_code purpose, jctca.tax_category_name,
             ppsa.payment_schedule_id, ppia.ap_invoice_num,
                                                           --Newly Added Customization
                                                           ppta.attribute6,
             ppta.attribute7
        FROM pn_payment_terms_all ppta,
             pn_leases_all pla,
             pn_payment_schedules_all ppsa,
             jai_cmn_tax_ctgs_all jctca,
             pn_payment_items_all ppia
       WHERE 1 = 1
         AND pla.lease_id = ppta.lease_id
         AND ppsa.lease_id = pla.lease_id
         AND jctca.tax_category_name(+) = ppta.attribute1
         AND UPPER (ppta.payment_purpose_code) IN ('RENT', 'CAM')
         AND pla.lease_class_code IN ('DIRECT', 'PAY')
         AND ppia.payment_schedule_id = ppsa.payment_schedule_id
         AND jctca.org_id(+) = v_org_id
         AND NVL (ppsa.attribute1, 'U') <> 'Y'
         AND ppsa.transferred_by_user_id IS NOT NULL
         AND ppsa.schedule_date BETWEEN xxabrl_fday_ofmonth (ppta.start_date)
                                    AND ppta.end_date
         AND UPPER (ppsa.payment_status_lookup_code) = 'APPROVED'
         AND ppta.vendor_id =
                (SELECT vendor_id
                   FROM jai_cmn_vendor_sites jcvs
                  WHERE jcvs.vendor_id = ppta.vendor_id
                    AND vendor_site_id = ppta.vendor_site_id
                    AND service_tax_regno IS NOT NULL)
         AND ppia.ap_invoice_num IS NOT NULL;

/**********************************************************************************
       ---Cursor for getting Service Tax Rates and TDS for Lease -----
 **********************************************************************************/
   CURSOR cur_lease_vendor (
      cp_invoice_num    VARCHAR2,
      cp_lease_num      VARCHAR2,
      cp_invoice_date   DATE,
      cp_vendor_id      VARCHAR2
   )
   IS
      SELECT   ppta.payment_purpose_code purpose, ppta.vendor_id vendor_id,
               ppta.vendor_site_id vendor_site_id,
               aii.invoice_amount actual_amount,
               
               --NVL (ppta.actual_amount, ppta.estimated_amount) actual_amount,
               ppta.term_template_id term_temp_id,
               ppta.attribute1 service_tax_type, ppta.attribute2,
               ppta.attribute5 tds, jctcl.tax_id, jctca.tax_category_id,
               jcta.tax_rate, jcta.tax_type, jcta.tax_account_id,
               ppia.ap_invoice_num, ppsa.schedule_date invoice_date,
               pla.lease_num, ppta.attribute6, ppta.attribute7,
                  pla.lease_num
               || ''
               || ppta.vendor_id
               || ''
               || ppsa.schedule_date lease_vendor
          FROM pn_payment_terms_all ppta,
               pn_payment_schedules_all ppsa,
               jai_cmn_tax_ctgs_all jctca,
               jai_cmn_tax_ctg_lines jctcl,
               jai_cmn_taxes_all jcta,
               pn_leases_all pla,
               pn_payment_items_all ppia,
               ap_invoices_interface aii
         WHERE 1 = 1
           AND pla.lease_id = ppta.lease_id
           AND ppsa.lease_id = pla.lease_id
           AND ppia.payment_schedule_id = ppsa.payment_schedule_id
           AND jctca.tax_category_name = ppta.attribute1
           AND jctca.tax_category_id = jctcl.tax_category_id
           AND jctcl.tax_id = jcta.tax_id
           AND jctca.org_id = jcta.org_id
           AND jctca.org_id = v_org_id
           AND aii.invoice_num = ppia.ap_invoice_num
           AND aii.status IS NULL                             --<> 'PROCESSED'
           AND ppsa.schedule_date BETWEEN xxabrl_fday_ofmonth (ppta.start_date)
                                      AND ppta.end_date
           AND ppia.ap_invoice_num = cp_invoice_num
           AND pla.lease_num = cp_lease_num
           AND ppsa.schedule_date = cp_invoice_date
           AND ppta.vendor_id = cp_vendor_id
      ORDER BY ppta.payment_term_id, jcta.tax_rate DESC;
BEGIN
   v_amount := 0;
   v_tax_rate := 0;
   v_total := 0;
   v_err_flag := 'N';

   -- 1ST LOOP
   --
   FOR i IN cur_pmnt_term
   LOOP
      v_err_flag := 'N';

--    1
--
      IF i.service_tax_type IS NOT NULL
      THEN
         BEGIN
            SELECT COUNT (*)
              INTO v_count
              FROM ap_invoice_lines_interface aili, ap_invoices_interface aii
             WHERE aii.invoice_id = aili.invoice_id
               AND aii.vendor_id = i.vendor_id
               AND aii.SOURCE = 'Oracle Property Manager'
               AND aii.invoice_date = i.invoice_date
               AND aii.invoice_num = i.ap_invoice_num;
         END;

         BEGIN
            SELECT dist_code_combination_id
              INTO v_rent_cc_id
              FROM ap_invoice_lines_interface aili, ap_invoices_interface aii
             WHERE aii.invoice_id = aili.invoice_id
               AND aii.vendor_id = i.vendor_id
               AND aii.SOURCE = 'Oracle Property Manager'
               AND aii.invoice_num = i.ap_invoice_num
               AND aii.invoice_date = i.invoice_date;
         EXCEPTION
            WHEN OTHERS
            THEN
               fnd_file.put_line
                  (fnd_file.LOG,
                      'ERROR TO FIND THE CODE COMBINATION ID FOR THE SERVICE TAX TYPE IS RENT '
                   || i.vendor_id
                   || '  '
                   || i.ap_invoice_num
                   || '  '
                   || i.invoice_date
                  );
               v_err_flag := 'Y';
               GOTO muttal;
         END;

--    2
--
         IF v_count = 1
         THEN
            fnd_file.put_line (fnd_file.LOG,
                                  'Here V Count is=>'
                               || v_count
                               || 'LEASE VENDOR  '
                               || i.lease_vendor
                              );
            fnd_file.put_line (fnd_file.LOG,
                                  i.vendor_id
                               || '  '
                               || i.ap_invoice_num
                               || '  '
                               || i.invoice_date
                              );

-- ADDED BY SHAILESH ON 8 MAY 09. FOR TDS CODE DEFAULT IN RECORDS THE TAX CATEGORY NULL SO ITS NOT
-- GOING TO BELOW LOOP SO TO GET INVOICE ID WE WROTE SAME QUERY HERE.
            BEGIN
               fnd_file.put_line (fnd_file.LOG,
                                  ' START OF LOOP1 ' || i.lease_vendor
                                 );

               SELECT aili.invoice_id, aili.invoice_line_id,
                      aili.line_number, aili.dist_code_combination_id,
                      aili.description, aili.org_id
                 INTO v_invoice_id, v_invoice_line_num,
                      v_line_num, v_dist_code_combination_id,
                      v_description, v_org_id
                 FROM ap_invoice_lines_interface aili,
                      ap_invoices_interface aii
                WHERE aii.invoice_id = aili.invoice_id
                  AND aii.SOURCE = 'Oracle Property Manager'
                  AND aii.invoice_num = i.ap_invoice_num
                  AND aii.invoice_date = i.invoice_date
                  AND aii.vendor_id = i.vendor_id
                  AND invoice_line_id =
                                     (SELECT MAX (invoice_line_id)
                                        FROM ap_invoice_lines_interface aili
                                       WHERE aili.invoice_id = aii.invoice_id);

               fnd_file.put_line (fnd_file.LOG,
                                  ' v_invoice_id => ' || v_invoice_id
                                 );
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  fnd_file.put_line
                     (fnd_file.LOG,
                         'Selected Lease details does not exist in AP interface table '
                      || v_invoice_id
                     );
                  fnd_file.put_line
                     (fnd_file.LOG,
                      '...........................................................'
                     );
                  v_err_flag := 'Y';
                  GOTO muttal;
               WHEN TOO_MANY_ROWS
               THEN
                  fnd_file.put_line
                     (fnd_file.LOG,
                      'Tax Information already inserted into interface tables '
                     );
                  fnd_file.put_line
                        (fnd_file.LOG,
                         '...................................................'
                        );
                  v_err_flag := 'Y';
                  GOTO muttal;
               WHEN OTHERS
               THEN
                  fnd_file.put_line (fnd_file.LOG, 'Lease details not found');
                  fnd_file.put_line (fnd_file.LOG, '....................');
                  v_err_flag := 'Y';
                  GOTO muttal;
            END;

--    2ND LOOP
--
            FOR j IN cur_lease_vendor (i.ap_invoice_num,
                                       i.lease_num,
                                       i.invoice_date,
                                       i.vendor_id
                                      )
            LOOP
               BEGIN
                  fnd_file.put_line (fnd_file.LOG,
                                     ' START OF LOOP ' || i.lease_vendor
                                    );
                  fnd_file.put_line (fnd_file.LOG, 'v_org_id=>' || v_org_id);
                  fnd_file.put_line (fnd_file.LOG,
                                     'i.ap_invoice_num=>' || i.ap_invoice_num
                                    );
                  fnd_file.put_line (fnd_file.LOG,
                                     'i.lease_num=>' || i.lease_num
                                    );
                  fnd_file.put_line (fnd_file.LOG,
                                     'i.invoice_date=>' || i.invoice_date
                                    );
                  fnd_file.put_line (fnd_file.LOG,
                                     'i.vendor_id=>' || i.vendor_id
                                    );

                  SELECT aili.invoice_id, aili.invoice_line_id,
                         aili.line_number, aili.dist_code_combination_id,
                         aili.description, aili.org_id
                    INTO v_invoice_id, v_invoice_line_num,
                         v_line_num, v_dist_code_combination_id,
                         v_description, v_org_id
                    FROM ap_invoice_lines_interface aili,
                         ap_invoices_interface aii
                   WHERE aii.invoice_id = aili.invoice_id
                     AND aii.SOURCE = 'Oracle Property Manager'
                     AND aii.invoice_num = j.ap_invoice_num
                     AND aii.invoice_date = j.invoice_date
                     AND aii.vendor_id = j.vendor_id
                     AND aili.invoice_line_id =
                                     (SELECT MAX (invoice_line_id)
                                        FROM ap_invoice_lines_interface aili
                                       WHERE aili.invoice_id = aii.invoice_id);

--
                  fnd_file.put_line
                     (fnd_file.output,
                         'FOR INVOICE ID WHICH IS GOING TO GENERATE SERVICE TAX AND TDS - '
                      || v_invoice_id
                     );
--
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     fnd_file.put_line
                        (fnd_file.LOG,
                            'Selected Lease details does not exist in AP interface table'
                         || v_invoice_id
                        );
                     fnd_file.put_line
                        (fnd_file.LOG,
                         '...................................................'
                        );
                     v_err_flag := 'Y';
                     GOTO muttal;
                  WHEN TOO_MANY_ROWS
                  THEN
                     fnd_file.put_line
                        (fnd_file.LOG,
                         'Tax Information already inserted into interface tables '
                        );
                     fnd_file.put_line
                        (fnd_file.LOG,
                         '...................................................'
                        );
                     v_err_flag := 'Y';
                     GOTO muttal;
                  WHEN OTHERS
                  THEN
                     fnd_file.put_line (fnd_file.LOG,
                                        'Lease details not found'
                                       );
                     fnd_file.put_line
                        (fnd_file.LOG,
                         '...................................................'
                        );
                     v_err_flag := 'Y';
                     GOTO muttal;
               END;

--
--
               IF UPPER (j.tax_type) = 'SERVICE'
               THEN
                  fnd_file.put_line (fnd_file.LOG,
                                     'j.tax_type=>' || j.tax_type
                                    );
                  v_amount := j.actual_amount;
                  v_tax_rate := j.tax_rate;
                  v_cat_pre := j.tax_category_id;
                  v_total := (v_amount) * (v_tax_rate / 100);
                  v_temp := v_total;
                  v_line_num := v_line_num + 1;

--
--
                  BEGIN
                     SELECT gcc.segment1, gcc.segment2, gcc.segment3,
                            gcc.segment4, gcc.segment5, gcc.segment6,
                            gcc.segment7, gcc.segment8
                       INTO v_segment1, v_segment2, v_segment3,
                            v_segment4, v_segment5, v_segment6,
                            v_segment7, v_segment8
                       FROM gl_code_combinations gcc
                      WHERE 1 = 1
                        AND gcc.code_combination_id = j.tax_account_id;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        fnd_file.put_line (fnd_file.LOG,
                                              'Segments are not found '
                                           || j.tax_account_id
                                          );
                        fnd_file.put_line (fnd_file.LOG,
                                           '....................'
                                          );
                        v_err_flag := 'Y';
                        GOTO muttal;
                  END;

                  BEGIN
                     BEGIN
                        SELECT gc.code_combination_id
                          INTO v_cc_id
                          FROM gl_code_combinations gc
                         WHERE gc.segment1 = v_segment1
                           AND gc.segment2 = j.attribute7
                           AND gc.segment3 = v_segment3
                           AND gc.segment4 = j.attribute6
                           AND gc.segment5 = v_segment5
                           AND gc.segment6 = v_segment6
                           AND gc.segment7 = v_segment7
                           AND gc.segment8 = v_segment8;

                        fnd_file.put_line (fnd_file.LOG,
                                           'v_cc_id=>' || v_cc_id
                                          );
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           v_cc_id := NULL;
                     END;

                     IF NVL (v_cc_id, 0) = 0
                     THEN
                        x_concatenated_segments :=
                              v_segment1
                           || '.'
                           || j.attribute7
                           || '.'
                           || v_segment3
                           || '.'
                           || j.attribute6
                           || '.'
                           || v_segment5
                           || '.'
                           || v_segment6
                           || '.'
                           || v_segment7
                           || '.'
                           || v_segment8;
                        v_chart_of_accounts_id := 50328;
                        fnd_file.put_line (fnd_file.LOG,
                                              'x_concatenated_segments=>'
                                           || x_concatenated_segments
                                          );

                        SELECT fnd_flex_ext.get_ccid ('SQLGL',
                                                      'GL#',
                                                      50328,
                                                      TO_CHAR (SYSDATE,
                                                               'DD-MON-YYYY'
                                                              ),
                                                      x_concatenated_segments
                                                     )
                          INTO v_cc_id1
                          FROM DUAL;

                        fnd_file.put_line (fnd_file.LOG,
                                           ' v_cc_id1=>' || v_cc_id1
                                          );

                        IF NVL (v_cc_id1, 0) = 0
                        THEN
                           v_err_msg :=
                                 v_err_msg
                              || ' Invalid Code Combination : '
                              || v_segment1
                              || '.'
                              || j.attribute7
                              || '.'
                              || v_segment3
                              || '.'
                              || j.attribute6
                              || '.'
                              || v_segment5
                              || '.'
                              || v_segment6
                              || '.'
                              || v_segment7
                              || '.'
                              || v_segment8;
                           fnd_file.put_line (fnd_file.LOG,
                                              ' v_cc_id1=>' || v_err_msg
                                             );
                           v_err_flag := 'Y';
                           GOTO muttal;
                           fnd_file.put_line
                                            (fnd_file.LOG,
                                                '   '
                                             || '.  '
                                             || ' Invalid Code Combination : '
                                             || v_segment1
                                             || '.'
                                             || j.attribute7
                                             || '.'
                                             || v_segment3
                                             || '.'
                                             || j.attribute6
                                             || '.'
                                             || v_segment5
                                             || '.'
                                             || v_segment6
                                             || '.'
                                             || v_segment7
                                             || '.'
                                             || v_segment8
                                            );
                        END IF;
                     END IF;
                  END;

                  fnd_file.put_line (fnd_file.LOG, 'inserting');

                  -- dbms_output.put_line ( 'DIST:' || v_cc_id);
                  INSERT INTO ap_invoice_lines_interface
                              (invoice_id,
                               invoice_line_id,
                               line_number, line_type_lookup_code,
                               amount, dist_code_combination_id,
                               description, amount_includes_tax_flag, org_id,
                               created_by, creation_date, last_updated_by,
                               last_update_date
                              )
                       VALUES (v_invoice_id,
                               ap_invoice_lines_interface_s.NEXTVAL,
                               v_line_num, 'MISCELLANEOUS',
                               ROUND (v_total, 2), NVL (v_cc_id, v_cc_id1),
                               -- j.tax_account_id,
                               v_description, 'S', v_org_id,
                               v_user_id, SYSDATE, v_user_id,
                               SYSDATE
                              );

                  fnd_file.put_line
                     (fnd_file.output,
                         'Service Tax line inserted for the invoice ID  Amount => '
                      || v_total
                     );
--
               ELSE --    Within 2nd loop    IF UPPER (j.tax_type) = 'SERVICE'
--
                  v_amount := v_total;
                  v_tax_rate := j.tax_rate;
                  v_cat_pre := j.tax_category_id;
                  v_total1 := (v_amount) * (v_tax_rate / 100);
                  v_temp := v_temp + v_total1;
                  v_line_num := v_line_num + 1;
--    FEB'15
                  fnd_file.put_line (fnd_file.LOG, 'v_cc_id=>' || v_cc_id);

                  IF NVL (v_cc_id, 0) = 0
                  THEN
                     v_err_flag := 'Y';
                     GOTO muttal;
                  ELSE
--    FEB'15
                     INSERT INTO ap_invoice_lines_interface
                                 (invoice_id,
                                  invoice_line_id,
                                  line_number, line_type_lookup_code,
                                  amount, dist_code_combination_id,
                                  description, amount_includes_tax_flag,
                                  org_id, created_by, creation_date,
                                  last_updated_by, last_update_date
                                 )
                          VALUES (v_invoice_id,
                                  ap_invoice_lines_interface_s.NEXTVAL,
                                  v_line_num, 'MISCELLANEOUS',
                                  ROUND (v_total1, 2),
                                                      --j.tax_account_id,
                                                      v_cc_id,
                                  v_description, 'S',
                                  v_org_id, v_user_id, SYSDATE,
                                  v_user_id, SYSDATE
                                 );

                     fnd_file.put_line
                        (fnd_file.output,
                            'SERVICE TAX LINE INSERTED FOR THE INVOICE ID AMOUNT => '
                         || v_total1
                        );
--
                  END IF;
--
               END IF;
--
            END LOOP;

--
--
            IF i.tax_flag = 'YES' AND i.tax_category_name IS NOT NULL
            THEN
               v_line_num := v_line_num + 1;

               INSERT INTO ap_invoice_lines_interface
                           (invoice_id,
                            invoice_line_id,
                            line_number, line_type_lookup_code, amount,
                            dist_code_combination_id, description,
                            amount_includes_tax_flag, org_id, created_by,
                            creation_date, last_updated_by, last_update_date
                           )
                    VALUES (v_invoice_id,
                            ap_invoice_lines_interface_s.NEXTVAL,
                            v_line_num, 'MISCELLANEOUS', ROUND (-v_temp, 2),
                            i.cc_id, v_description,
                            'S', v_org_id, v_user_id,
                            SYSDATE, v_user_id, SYSDATE
                           );

               fnd_file.put_line
                  (fnd_file.output,
                      'SERVICE TAX LINE INSERTED FOR FOR SERVICE TAX IS ON HOLD AMOUNT => '
                   || -v_temp
                  );
            END IF;

            COMMIT;

            BEGIN
               SELECT SUM (amount)
                 INTO v_tot_line_amount
                 FROM ap_invoice_lines_interface
                WHERE invoice_id = v_invoice_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  NULL;
            END;

            UPDATE ap_invoices_interface aii
               SET aii.invoice_amount = v_tot_line_amount
             ----aii.doc_category_code = 'STD INV' ----Voucher No code Commented since its giving the error DOC CAT NOT REQUIRED
            WHERE  aii.invoice_id = v_invoice_id
               AND aii.SOURCE = 'Oracle Property Manager';

            fnd_file.put_line
                  (fnd_file.output,
                      'Invoice Header amount is updated with the  Amount =>  '
                   || v_tot_line_amount
                  );

            UPDATE pn_payment_schedules_all ppsa
               SET ppsa.attribute1 = 'Y'
             WHERE payment_schedule_id = i.payment_schedule_id;

            fnd_file.put_line (fnd_file.LOG, ' Attribute 10 updation ');

            IF i.purpose = 'CAM'
            THEN
               fnd_file.put_line (fnd_file.LOG,
                                     ' CAM CONDITION FOR ATTRIBUTE10  '
                                  || v_invoice_id
                                 );

               UPDATE ap_invoice_lines_interface aili
                  SET aili.attribute10 = i.tds
                WHERE invoice_id = v_invoice_id
                  AND line_type_lookup_code = 'ITEM';

               fnd_file.put_line (fnd_file.output,
                                  'TDS updated  with ' || i.tds
                                 );
--ADDED BY PRAVEEN FOR CALCULATING THE TDS ONLY ON BASE AMOUNT -- 22-JUN-09
            ELSIF i.purpose = 'RENT'
            THEN
               fnd_file.put_line (fnd_file.LOG,
                                     ' RENT CONDITION FOR ATTRIBUTE10  '
                                  || v_invoice_id
                                 );

               UPDATE ap_invoice_lines_interface aili
                  SET aili.attribute10 = i.tds
                WHERE invoice_id = v_invoice_id
                  AND line_type_lookup_code = 'ITEM';

               fnd_file.put_line (fnd_file.output,
                                  'TDS updated  with ' || i.tds
                                 );
            END IF;

            IF i.tax_category_name = 'RENT'
            THEN
               UPDATE ap_invoice_lines_interface aili
                  SET dist_code_combination_id = v_rent_cc_id
                WHERE invoice_id = v_invoice_id AND aili.amount > 0;

               fnd_file.put_line (fnd_file.output,
                                  'Invoice Updated with ' || v_rent_cc_id
                                 );
            END IF;

            COMMIT;
         ELSE
            fnd_file.put_line
               (fnd_file.output,
                '.............................................................'
               );
            fnd_file.put_line
               (fnd_file.output,
                   ' Tax Information already inserted into interface tables for lease no - vendor no '
                || ' '
                || i.lease_vendor
               );
            fnd_file.put_line
               (fnd_file.output,
                '.............................................................'
               );
         END IF;
      ELSE
         BEGIN
            SELECT COUNT (*)
              INTO v_count
              FROM ap_invoice_lines_interface aili, ap_invoices_interface aii
             WHERE aii.invoice_id = aili.invoice_id
               AND aii.vendor_id = i.vendor_id
               AND aii.SOURCE = 'Oracle Property Manager'
               AND aii.invoice_date = i.invoice_date
               AND aii.invoice_num = i.ap_invoice_num;
         END;

         IF v_count = 1
         THEN
            fnd_file.put_line (fnd_file.LOG,
                                  'Here V1 Count is=>'
                               || v_count
                               || 'LEASE VENDOR  '
                               || i.lease_vendor
                              );

            BEGIN
               fnd_file.put_line (fnd_file.LOG,
                                  'START OF LOOP2 ' || i.lease_vendor
                                 );

               SELECT aili.invoice_id, aili.invoice_line_id,
                      aili.line_number, aili.dist_code_combination_id,
                      aili.description, aili.org_id
                 INTO v_invoice_id, v_invoice_line_num,
                      v_line_num, v_dist_code_combination_id,
                      v_description, v_org_id
                 FROM ap_invoice_lines_interface aili,
                      ap_invoices_interface aii
                WHERE aii.invoice_id = aili.invoice_id
                  AND aii.SOURCE = 'Oracle Property Manager'
                  AND aii.invoice_num = i.ap_invoice_num
                  AND aii.invoice_date = i.invoice_date
                  AND aii.vendor_id = i.vendor_id
                  AND invoice_line_id =
                         (SELECT MAX (invoice_line_id)
                            FROM ap_invoice_lines_interface aili
                           WHERE aili.invoice_id = aii.invoice_id
                             AND aii.invoice_num = i.ap_invoice_num);

               fnd_file.put_line (fnd_file.LOG,
                                  ' v_invoice_id => ' || v_invoice_id
                                 );
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  fnd_file.put_line
                     (fnd_file.LOG,
                         'Selected Lease details does not exist in AP interface table'
                      || v_invoice_id
                     );
                  fnd_file.put_line
                        (fnd_file.LOG,
                         '...................................................'
                        );
                  v_err_flag := 'Y';
                  GOTO muttal;
               WHEN TOO_MANY_ROWS
               THEN
                  fnd_file.put_line
                     (fnd_file.LOG,
                      ' Tax Information already inserted into interface tables '
                     );
                  fnd_file.put_line
                        (fnd_file.LOG,
                         '...................................................'
                        );
                  v_err_flag := 'Y';
                  GOTO muttal;
               WHEN OTHERS
               THEN
                  fnd_file.put_line (fnd_file.LOG,
                                     'Lease details not found' || v_invoice_id
                                    );
                  fnd_file.put_line
                        (fnd_file.LOG,
                         '...................................................'
                        );
                  v_err_flag := 'Y';
                  GOTO muttal;
            END;
         END IF;

         UPDATE ap_invoice_lines_interface aili
            SET aili.attribute10 = i.tds
          WHERE invoice_id = v_invoice_id AND line_type_lookup_code = 'ITEM';

         fnd_file.put_line
            (fnd_file.output,
                'TDS Updated for Invoice ID where service tax is not avialable - '
             || v_invoice_id
            );

/*
        UPDATE ap_invoices_interface aii
        SET aii.doc_category_code = 'STD INV'
        WHERE
            aii.invoice_id        = v_invoice_id
        AND    aii.source            = 'Oracle Property Manager';

*/ ----Voucher No code Commented since its giving the error DOC CAT NOT REQUIRED
         UPDATE pn_payment_schedules_all ppsa
            SET ppsa.attribute1 = 'Y'
          WHERE payment_schedule_id = i.payment_schedule_id;
      END IF;
   END LOOP;

   <<muttal>>
   fnd_file.put_line (fnd_file.LOG,
                      '...................................................'
                     );

--
   IF NVL (v_err_flag, 'N') <> 'Y'
   THEN
--
      COMMIT;
      retcode := 0;
   ELSE
      fnd_file.put_line
         (fnd_file.LOG,
          'Property Manager Program Aborted due to issues -- Check the OU that you are running from --    or    -- as Usual Pass onto OFIN Support'
         );
      fnd_file.put_line
         (fnd_file.LOG,
          'Property Manager Program Aborted due to issues -- Check the OU that you are running from --    or    -- as Usual Pass onto OFIN Support'
         );
      fnd_file.put_line
         (fnd_file.LOG,
          'Property Manager Program Aborted due to issues -- Check the OU that you are running from --    or    -- as Usual Pass onto OFIN Support'
         );
      fnd_file.put_line (fnd_file.LOG,
                         '...................................................'
                        );
      retcode := 2;
      ROLLBACK;
--
   END IF;
END xxabrl_pntoap_invoice_program; 
/


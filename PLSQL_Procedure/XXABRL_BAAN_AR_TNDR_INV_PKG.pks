CREATE OR REPLACE PACKAGE APPS.xxabrl_BAAN_ar_tndr_inv_pkg
IS
 
   PROCEDURE invoice_validate (
      errbuf           OUT      VARCHAR2,
      retcode          OUT      NUMBER,
      p_batch_source   IN       VARCHAR2,
      p_action         IN       VARCHAR2,
      p_org_id         IN       NUMBER
   );

   PROCEDURE invoice_insert (
      p_org_id         IN       NUMBER,
      p_batch_source   IN       VARCHAR2,
      x_ret_code       OUT      NUMBER
   );

   FUNCTION account_seg_status (
      p_seg_value   IN   VARCHAR2,
      p_seg_desc    IN   VARCHAR2
   )
      RETURN VARCHAR2;
END xxabrl_BAAN_ar_tndr_inv_pkg; 
/


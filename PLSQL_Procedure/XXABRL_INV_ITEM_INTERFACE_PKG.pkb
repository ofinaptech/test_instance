CREATE OR REPLACE PACKAGE BODY APPS.xxabrl_inv_item_interface_pkg
AS
   PROCEDURE xxabrl_item_main (
      errbuf     OUT      VARCHAR2,
      retcode    OUT      VARCHAR2,
      p_action   IN       VARCHAR2
   )
   IS
      v_request_id     NUMBER;
      v_request_id_1   NUMBER;
      lv_phase         VARCHAR2 (1000);
      lv_status        VARCHAR2 (1000);
      lv_dev_phase     VARCHAR2 (1000);
      lv_dev_status    VARCHAR2 (1000);
      lv_message       VARCHAR2 (1000);
      l_phase          VARCHAR2 (1000);
      l                BOOLEAN;
   BEGIN
      IF p_action = 'Y'
      THEN
         xxabrl_item_validate;
      ELSIF p_action = 'N'
      THEN
         xxabrl_item_validate_insert;
      ELSIF p_action = 'S'
      THEN
         fnd_file.put_line (fnd_file.LOG,
                            'Submitting Request for Item creation'
                           );
         v_request_id :=
            fnd_request.submit_request ('INV'                   -- application
                                             ,
                                        'INCOIN'         -- program short name
                                                ,
                                        NULL,
                                        NULL,
                                        FALSE,
                                        97                          --p_org_id
                                          ,
                                        1                          --p_all_org
                                         ,
                                        1                   --p_val_item_flag-
                                         ,
                                        1                    --p_pro_item_flag
                                         ,
                                        1                     --p_del_rec_flag
                                         ,
                                        1                        --p_xset_id--
                                         ,
                                        1
                                       );                         --p_run_mode
         COMMIT;
         fnd_file.put_line (fnd_file.LOG,
                            ' Submited concurrent Request ID' || v_request_id
                           );
      END IF;

      COMMIT;
   END xxabrl_item_main;

   --------------- procedure to validate data -------------------------------------------------------------------
   PROCEDURE xxabrl_item_validate
   IS
      CURSOR c_master
      IS
         SELECT ROWID, cstm.*
           FROM xxabrl.xxabrl_inv_sys_item_intf_cstm cstm
          WHERE complete_flag IN ('N', 'E');

      CURSOR c_category
      IS
         SELECT category_set_name, item_category
           FROM xxabrl.xxabrl_inv_sys_item_intf_cstm
          WHERE complete_flag IN ('N', 'E');

      CURSOR c_child_organization
      IS
         SELECT mp.organization_id
           FROM mtl_parameters mp
          WHERE mp.master_organization_id =
                                       (SELECT mp1.organization_id
                                          FROM mtl_parameters mp1
                                         WHERE mp1.organization_code = '000')
            AND mp.organization_id <> mp.master_organization_id;

      invalid_method        EXCEPTION;
      l_err_flag            NUMBER;
      l_name                VARCHAR2 (40);
      l_item_count          NUMBER;
      v_error_item_count    NUMBER  := 0;
      v_valid_item_count    NUMBER  := 0;
      l_organization_id     NUMBER;
      l_template_name       VARCHAR2 (40);
      l_template_id         NUMBER;
      l_uom_code            VARCHAR2 (40);
      l_category_set_id     NUMBER;
      l_category_id         NUMBER;
      l_category_set_name   VARCHAR2 (40);
      l_buyer_id            NUMBER;
      l_asset_category_id   NUMBER;
      cnt                   NUMBER;
      lp_get_user_id        NUMBER   := fnd_profile.VALUE ('USER_ID');
      v_error_message       VARCHAR2 (1000);
      v_data_count          NUMBER                    := 0;
      v_segments            fnd_flex_ext.segmentarray;
   BEGIN
      /* update xxabrl_inv_sys_item_intf_cstm
          set error = 'duplicate records in staging table>>> ', complete_flag = 'e'
        where segment1 in (select segment1
                             from xxabrl_inv_sys_item_intf_cstm
                            group by segment1
                           having count(1) > 1) ;
                           commit;*/
      fnd_file.put_line
         (fnd_file.output,
          '---------------------------Validations staring------------------------------------'
         );

      FOR i IN c_master
      LOOP
         l_err_flag := 1;
         v_error_message := NULL;

        ---------------------------- validation for item  number duplication in staging table-----------------------------------------------------
         BEGIN
            IF i.segment1 IS NOT NULL
            THEN
               BEGIN
                  v_data_count := 0;

                  SELECT COUNT (1)
                    INTO v_data_count
                    FROM xxabrl_inv_sys_item_intf_cstm
                   WHERE segment1 = i.segment1;

                  IF v_data_count > 1
                  THEN
                     v_error_message :=
                           v_error_message
                        || 'Duplicate Records In Staging table>>>  ';
                     COMMIT;
                  END IF;

                  v_data_count := 0;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     v_data_count := 0;
                     v_error_message :=
                           v_error_message
                        || ' Duplicate Records In Staging table>>>';
                     COMMIT;
               END;
            END IF;
         END;

        -----------------------validate master org-------------------------------------------------------------------------------------
         BEGIN
            fnd_file.put_line (fnd_file.output, 'Validating  Master Org:');

            SELECT organization_code, organization_id
              INTO l_name, l_organization_id
              FROM mtl_parameters mp
             WHERE mp.organization_code = '000';
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               l_err_flag := 2;
               v_error_message :=
                         v_error_message || 'Invalid Master Organization>>  ';
               COMMIT;
         END;

         --------------------------------------validate item  in mtl_system_items_b---------------------------------------------------------
         BEGIN
            fnd_file.put_line (fnd_file.output,
                               'Validating  Item: ' || ' ' || i.segment1
                              );

            SELECT COUNT (*)
              INTO l_item_count
              FROM mtl_system_items_b
             WHERE segment1 = i.segment1;

            IF l_item_count > 0
            THEN
               l_err_flag := 2;
               v_error_message :=
                                v_error_message || 'Item Already Exists>>>  ';
            END IF;

            COMMIT;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               l_err_flag := 2;
               v_error_message := v_error_message || 'Invalid Item>>>  ';
               COMMIT;
         END;

        ----------------------------------validate template---------------------------------------------------------------------------------------------
         BEGIN
            fnd_file.put_line (fnd_file.output,
                                  'Validating  Template: '
                               || ' '
                               || i.template_name
                              );

            SELECT template_name, template_id
              INTO l_template_name, l_template_id
              FROM mtl_item_templates
             WHERE TRIM (template_name) = TRIM (i.template_name);
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               l_err_flag := 2;
               v_error_message :=
                             v_error_message || 'Invalid Item Template>>>   ';
               COMMIT;
         END;

        ----------------------------validate uom code------------------------------------------------------------------------------------------------------
         BEGIN
            fnd_file.put_line (fnd_file.output,
                                  'Validating  UOM Code: '
                               || ' '
                               || i.primary_uom_code
                              );

            SELECT uom_code
              INTO l_uom_code
              FROM mtl_units_of_measure
             WHERE UPPER (unit_of_measure) = UPPER (i.primary_uom_code)
                OR UPPER (uom_code) = UPPER (i.primary_uom_code);
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               l_err_flag := 2;
               v_error_message :=
                           v_error_message || 'Invalid Unit of Measure>>>   ';
               COMMIT;
         END;

    ---------------------validating buyer name-----------------------------------------------------------------------------------------------------------
        /*begin
          fnd_file.put_line(fnd_file.output,
                            'validating  buyer name: ' || ' ' || i.buyer_name);
          --lp_buyer_id:=0;
          select agent_id
            into l_buyer_id
            from po_agents_v
           where 1 = 1
             and upper(agent_name) = upper(i.buyer_name);
        exception
          when no_data_found then
            l_err_flag := 2;
            v_error_message := v_error_message || 'invalid buyer name>>>   ';
          when others then
            dbms_output.put_line('  invalid buyer ' || sqlerrm);
            fnd_file.put_line(fnd_file.output,
                              'invalid buyer name>>> ' || sqlerrm);
            commit;
        end;*/
        --------------------------------------------- validating asset category---------------------------------------------------------------------------------
         BEGIN
            fnd_file.put_line (fnd_file.output,
                                  'Validating  Asset Category: '
                               || ' '
                               || UPPER (i.asset_category_1)
                               || '.'
                               || UPPER (i.asset_category_2)
                               || '.'
                               || UPPER (i.asset_category_3)
                              );

            IF     (i.asset_category_1 IS NOT NULL)
               AND (i.asset_category_2 IS NOT NULL)
            THEN
               SELECT category_id
                 INTO l_asset_category_id
                 FROM fa_categories_vl
                WHERE    UPPER (segment1)
                      || '.'
                      || UPPER (segment2)
                      || '.'
                      || UPPER (segment3) =
                            UPPER (i.asset_category_1)
                         || '.'
                         || UPPER (i.asset_category_2)
                         || '.'
                         || UPPER (i.asset_category_3);

               IF l_asset_category_id IS NULL
               THEN
                  l_err_flag := 2;
                  v_error_message :=
                        v_error_message
                     || 'Invalid Asset Category>>>   '
                     || ' '
                     || i.segment1;
                  fnd_file.put_line (fnd_file.output,
                                        'INVALID ASSET CATEGORY ID>>>:'
                                     || ' '
                                     || i.segment1
                                    );
                  COMMIT;
               END IF;
            ELSIF    (    (i.asset_category_1 IS NULL)
                      AND (i.asset_category_2 IS NOT NULL)
                     )
                  OR (    (i.asset_category_1 IS NOT NULL)
                      AND (i.asset_category_2 IS NULL)
                     )
            THEN
               v_error_message :=
                     v_error_message
                  || 'Invalid Asset Category>>>   '
                  || ' '
                  || i.segment1;
               fnd_file.put_line (fnd_file.output,
                                     'INVALID ASSET CATEGORY ID>>>:'
                                  || ' '
                                  || i.segment1
                                 );
               COMMIT;
            ELSE
               l_asset_category_id := NULL;
            END IF;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               --l_asset_category_id := null;
               v_error_message := v_error_message || 'Error No data found>>>';
            WHEN OTHERS
            THEN
               DBMS_OUTPUT.put_line ('  Invalid Asset Category>> ' || SQLERRM);
               fnd_file.put_line (fnd_file.output,
                                  'Invalid Asset Category>> ' || SQLERRM
                                 );
               COMMIT;
         END;

         --------------------------------end of validation for asset category---------------------------------------------------------------------
         -----------------------------------------validate category sets and category combinations------------------------------------------------
         -- for j1 in c_category loop
         BEGIN
            fnd_file.put_line (fnd_file.output,
                                  'Validating  category Set is  '
                               || ' '
                               || i.category_set_name
                               || ' And Item Category is '
                               || i.item_category
                              );

            SELECT category_set_id, category_set_name, category_id
              INTO l_category_set_id, l_category_set_name, l_category_id
              FROM mtl_category_sets mcs, mtl_categories_b_kfv mc
             WHERE mcs.category_set_name = i.category_set_name
               AND mcs.structure_id = mc.structure_id
               AND UPPER (mc.concatenated_segments) = UPPER (i.item_category);
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               l_err_flag := 2;
               v_error_message :=
                     v_error_message
                  || 'Invalid category set or category combination>>>   ';
               COMMIT;
         END;

         --  end loop;
         -----------------------------------------end of validations for category sets and category combinations---------------------------------------
         -- where rowid = c_cur2.rowid;
         IF v_error_message IS NOT NULL
         THEN
            fnd_file.put_line (fnd_file.LOG, '***ITEM Invalid');

            UPDATE xxabrl_inv_sys_item_intf_cstm
               SET error = v_error_message,
                   complete_flag = 'E'
             WHERE ROWID = i.ROWID;

            v_error_item_count := v_error_item_count + 1;
            fnd_file.put_line
               (fnd_file.output,
                '-----------------------------------------------------------------------------------'
               );
            fnd_file.put_line (fnd_file.output, 'Details of Rejected Record:');
            fnd_file.put_line
               (fnd_file.output,
                '-----------------------------------------------------------------------------------'
               );
            fnd_file.put_line
               (fnd_file.output,
                'Item                  Description                                Error'
               );
            fnd_file.put_line (fnd_file.output,
                                  i.segment1
                               || '        '
                               || i.item_description
                               || '                   '
                               || v_error_message
                              );
            fnd_file.put_line
               (fnd_file.output,
                '-----------------------------------------------------------------------------------'
               );
         ELSE
            fnd_file.put_line (fnd_file.LOG, 'Item Valid');

            UPDATE xxabrl_inv_sys_item_intf_cstm
               SET organization_id = l_organization_id,
                   uom_code = l_uom_code,
                   template_id = l_template_id,
                   buyer_id = l_buyer_id,
                   asset_category_id = l_asset_category_id,
                   category_set_id = l_category_set_id,
                   category_id = l_category_id,
                   error = NULL,
                   complete_flag = 'V'
             WHERE ROWID = i.ROWID;

            v_valid_item_count := v_valid_item_count + 1;
            --   v_valid_item_count := v_valid_item_count + 1;
            COMMIT;
         END IF;

         v_error_message := NULL;
         fnd_file.put_line (fnd_file.LOG,
                            'Validation Ends for Item ' || i.segment1
                           );
         fnd_file.put_line
                      (fnd_file.LOG,
                          '--------------------------------------------------'
                       || i.segment1
                      );
      END LOOP;

      fnd_file.put_line (fnd_file.LOG,
                         '@@@Invalid Items ' || v_error_item_count
                        );
      fnd_file.put_line (fnd_file.LOG,
                         '@@@valid Items ' || v_valid_item_count);
      fnd_file.put_line (fnd_file.LOG, '***Validation Procedure Ends***');
      fnd_file.put_line
         (fnd_file.output,
          '-----------------------Validation Procedure Ends-----------------------------------'
         );
   END xxabrl_item_validate;

        --------------------------------------------------------------------------------------------------------------------------------
   PROCEDURE xxabrl_item_validate_insert
   IS
      CURSOR c1
      IS
         SELECT ROWID, cstm.*
           FROM xxabrl.xxabrl_inv_sys_item_intf_cstm cstm
          WHERE complete_flag = 'V';

      CURSOR c2
      IS
         SELECT ROWID, segment1, organization_id, category_set_id,
                category_id
           FROM xxabrl.xxabrl_inv_sys_item_intf_cstm
          WHERE complete_flag = 'V';

      CURSOR c3
      IS
         SELECT ROWID, mp.organization_id
           FROM mtl_parameters mp
          WHERE mp.master_organization_id =
                                       (SELECT mp1.organization_id
                                          FROM mtl_parameters mp1
                                         WHERE mp1.organization_code = '000')
            AND mp.organization_id <> mp.master_organization_id;

      CURSOR c4 (p_cat_name VARCHAR2)
      IS
         SELECT category_set_id, category_set_name, category_id
           --  into l_category_set_id, l_category_set_name, l_category_id
         FROM   mtl_category_sets mcs, mtl_categories_b_kfv mc
          WHERE 1 = 1            --mcs.category_set_name = i.category_set_name
            AND mcs.structure_id = mc.structure_id
            AND UPPER (mc.concatenated_segments) = UPPER (p_cat_name);

      lp_get_user_id        NUMBER            := fnd_profile.VALUE ('USER_ID');
      l_expense_account     NUMBER;
      l_expense_account_c   NUMBER;
      v_segments            fnd_flex_ext.segmentarray;
      l_struct_num          NUMBER;
      v_acc_ccid            NUMBER;
      ln_segment1           VARCHAR2 (10);
      ln_segment2           VARCHAR2 (10);
      ln_segment3           VARCHAR2 (10);
      ln_segment4           VARCHAR2 (10);
      ln_segment5           VARCHAR2 (10);
      ln_segment6           VARCHAR2 (10);
      ln_segment7           VARCHAR2 (10);
      ln_segment8           VARCHAR2 (10);
      return_value          BOOLEAN;
      v_tsrl_invorg         NUMBER;
      v_expense_account     VARCHAR2 (10);
   BEGIN
      fnd_file.put_line (fnd_file.output, 'Starting Inserting item');
      l_expense_account := NULL;

      FOR l_rec IN c1
      LOOP
         fnd_file.put_line (fnd_file.LOG,
                            'Inserting Item l_rec:' || l_rec.segment1
                           );

         BEGIN
            SELECT sob.chart_of_accounts_id
              INTO l_struct_num
              FROM gl_sets_of_books sob
             WHERE NAME = 'ABRL Ledger' AND ROWNUM = 1;
         EXCEPTION
            WHEN OTHERS
            THEN
               fnd_file.put_line
                             (fnd_file.LOG,
                                 'Others Exception Chart of Accounts query  '
                              || SQLCODE
                              || SQLERRM
                             );
         END;

         BEGIN
            SELECT code.segment1, code.segment2, code.segment3,
                   code.segment4, code.segment5, code.segment6,
                   code.segment7, code.segment8
              INTO ln_segment1, ln_segment2, ln_segment3,
                   ln_segment4, ln_segment5, ln_segment6,
                   ln_segment7, ln_segment8
              FROM mtl_parameters par, gl_code_combinations code
             WHERE organization_id = l_rec.organization_id
               AND par.expense_account = code.code_combination_id;

            --exception
              -- when no_data_found
               ---then
            IF l_rec.asset_category_1 IS NOT NULL
            THEN
               BEGIN
                  BEGIN
                     SELECT tsrl.organization_id
                       INTO v_tsrl_invorg
                       FROM xxabrl_tsrl_invorgs tsrl
                      WHERE 1 = 1
                        AND tsrl.organization_id = l_rec.organization_id;
                  EXCEPTION
                     WHEN NO_DATA_FOUND
                     THEN
                        v_tsrl_invorg := 0;
                  END;

                  IF v_tsrl_invorg = 0
                  THEN
                     v_expense_account := l_rec.expense_account;
                  ELSE
                     v_expense_account := '113902';
                  END IF;
               END;
            ELSE
               v_expense_account := l_rec.expense_account;
            END IF;

            v_segments (1) := ln_segment1;
            v_segments (2) := ln_segment2;
            v_segments (3) := ln_segment3;
            v_segments (4) := ln_segment4;
            v_segments (5) := ln_segment5;
            v_segments (6) := v_expense_account;
                                       --l_rec.expense_account;---ln_segment6;
            v_segments (7) := ln_segment7;
            v_segments (8) := ln_segment8;
            DBMS_OUTPUT.put_line ('INSIDE NO DATA FOUND');
            fnd_file.put_line (fnd_file.LOG,
                               'INSIDE NO DATA FOUND for master');
            fnd_file.put_line (fnd_file.LOG,
                                  'SEGMENT1 for master:'
                               || ln_segment1
                               || 'SEGMENT2for master :'
                               || ln_segment2
                               || 'SEGMENT3 for master :'
                               || ln_segment3
                               || 'SEGMENT4 for master:'
                               || ln_segment4
                               || 'SEGMENT5 for master:'
                               || ln_segment5
                               || 'SEGMENT6 for master:'
                               || ln_segment6
                               || 'SEGMENT7 for master:'
                               || ln_segment7
                               || 'SEGMENT8 for master:'
                               || ln_segment8
                              );
            return_value :=
               fnd_flex_ext.get_combination_id
                                           (application_short_name      => 'SQLGL',
                                            key_flex_code               => 'GL#',
                                            structure_number            => l_struct_num,
                                            validation_date             => SYSDATE,
                                            n_segments                  => 8,
                                            segments                    => v_segments,
                                            combination_id              => v_acc_ccid,
                                            data_set                    => -1
                                           );
            fnd_file.put_line (fnd_file.LOG,
                               'L_STRUCT_NUM for master: ' || l_struct_num
                              );
            fnd_file.put_line (fnd_file.LOG,
                               'New CCID Generated for master: ' || v_acc_ccid
                              );
         END;

         /*if l_rec.asset_category_id is not null and l_rec.organization_id = 97 then
           select code_combination_id
             into l_expense_account
             from gl_code_combinations_kfv gcc
            where gcc.concatenated_segments =
                  '11.000.982.0000000.00.618901.000.0000';
         elsif l_rec.asset_category_id is null then
           l_expense_account := null;
         end if;
         fnd_file.put_line(fnd_file.log,
                           'inserting expense account for master item :' ||
                           l_rec.segment1 || ' and expence account id :' ||
                           l_expense_account);
         fnd_file.put_line(fnd_file.log,
                           'completed validation of data, inserting in mtl_system_items_interface ');*/
         INSERT INTO mtl_system_items_interface
                     (organization_id,
                                      --  start_date_active,
                                      last_update_date, last_updated_by,
                      creation_date, created_by, segment1,
                      description, primary_uom_code, process_flag,
                      transaction_type, set_process_id, template_id,
                      attribute14,
                      list_price_per_unit,
                      buyer_id, inventory_item_status_code,
                      asset_category_id, expense_account
                     )
              VALUES (l_rec.organization_id,
                                            --   sysdate,
                                            SYSDATE, lp_get_user_id,
                                                              --1249, --xxabrl
                      SYSDATE, lp_get_user_id,                         --1249,
                                              l_rec.segment1,
                      l_rec.item_description, l_rec.uom_code, 1,
                      'CREATE', 1, l_rec.template_id,
                      'CUSTOM TO INV',
                      DECODE (l_rec.list_price_per_unit,
                              NULL, 1,
                              0, 1,
                              l_rec.list_price_per_unit
                             ),
                      l_rec.buyer_id, l_rec.item_status,
                      l_rec.asset_category_id, v_acc_ccid
                     );

         fnd_file.put_line (fnd_file.output,
                            'Inserting item:' || ' ' || l_rec.segment1
                           );

         --------------------------------------inserting for child organization items---------------------------------------------------------
         FOR p IN c3
         LOOP
            BEGIN
               ln_segment1 := NULL;
               ln_segment2 := NULL;
               ln_segment3 := NULL;
               ln_segment4 := NULL;
               ln_segment5 := NULL;
               ln_segment6 := NULL;
               ln_segment7 := NULL;
               ln_segment8 := NULL;
               v_acc_ccid := 0;

               SELECT code.segment1, code.segment2, code.segment3,
                      code.segment4, code.segment5, code.segment6,
                      code.segment7, code.segment8
                 INTO ln_segment1, ln_segment2, ln_segment3,
                      ln_segment4, ln_segment5, ln_segment6,
                      ln_segment7, ln_segment8
                 FROM mtl_parameters par, gl_code_combinations code
                WHERE organization_id = p.organization_id
                  AND par.expense_account = code.code_combination_id;

               --exception
                 -- when no_data_found
                  ---then
               IF l_rec.asset_category_1 IS NOT NULL
               THEN
                  BEGIN
                     BEGIN
                        SELECT tsrl.organization_id
                          INTO v_tsrl_invorg
                          FROM xxabrl_tsrl_invorgs tsrl
                         WHERE 1 = 1
                           AND tsrl.organization_id = p.organization_id;
                     EXCEPTION
                        WHEN NO_DATA_FOUND
                        THEN
                           v_tsrl_invorg := 0;
                     END;

                     IF v_tsrl_invorg = 0
                     THEN
                        v_expense_account := l_rec.expense_account;
                     ELSE
                        v_expense_account := '113902';
                     END IF;
                  END;
               ELSE
                  v_expense_account := l_rec.expense_account;
               END IF;

               v_segments (1) := ln_segment1;
               v_segments (2) := ln_segment2;
               v_segments (3) := ln_segment3;
               v_segments (4) := ln_segment4;
               v_segments (5) := ln_segment5;
               v_segments (6) := v_expense_account;
                                       --l_rec.expense_account;---ln_segment6;
               v_segments (7) := ln_segment7;
               v_segments (8) := ln_segment8;
               DBMS_OUTPUT.put_line ('INSIDE NO DATA FOUND');
               fnd_file.put_line (fnd_file.LOG, 'INSIDE NO DATA FOUND');
               fnd_file.put_line (fnd_file.LOG,
                                     'SEGMENT1 :'
                                  || ln_segment1
                                  || 'SEGMENT2 :'
                                  || ln_segment2
                                  || 'SEGMENT3 :'
                                  || ln_segment3
                                  || 'SEGMENT4 :'
                                  || ln_segment4
                                  || 'SEGMENT5 :'
                                  || ln_segment5
                                  || 'SEGMENT6 :'
                                  || ln_segment6
                                  || 'SEGMENT7 :'
                                  || ln_segment7
                                  || 'SEGMENT8 :'
                                  || ln_segment8
                                 );
               return_value :=
                  fnd_flex_ext.get_combination_id
                                           (application_short_name      => 'SQLGL',
                                            key_flex_code               => 'GL#',
                                            structure_number            => l_struct_num,
                                            validation_date             => SYSDATE,
                                            n_segments                  => 8,
                                            segments                    => v_segments,
                                            combination_id              => v_acc_ccid,
                                            data_set                    => -1
                                           );
               fnd_file.put_line (fnd_file.LOG,
                                  'L_STRUCT_NUM : ' || l_struct_num
                                 );
               fnd_file.put_line (fnd_file.LOG,
                                  'New CCID Generated : ' || v_acc_ccid
                                 );
            END;

            IF return_value
            THEN
               fnd_file.put_line
                             (fnd_file.LOG,
                                 'Inserting Expense Account for Child Item :'
                              || l_rec.segment1
                              || ' And Expence Account ID :'
                              || l_expense_account_c
                              || ' For Org '
                              || p.organization_id
                             );

               INSERT INTO mtl_system_items_interface
                           (organization_id,
                                            --      start_date_active,
                                            last_update_date,
                            last_updated_by, creation_date, created_by,
                            segment1, description,
                            primary_uom_code, process_flag, transaction_type,
                            set_process_id, template_id, attribute14,
                            list_price_per_unit,
                            buyer_id, inventory_item_status_code,
                            expense_account
                           )
                    VALUES (p.organization_id,
                                              -- sysdate,
                                              SYSDATE,
                            lp_get_user_id,                   --1249, --xxabrl
                                           SYSDATE, lp_get_user_id,    --1249,
                            l_rec.segment1, l_rec.item_description,
                            l_rec.uom_code, 1, 'CREATE',
                            1, l_rec.template_id, 'CUSTOM TO INV',
                            DECODE (l_rec.list_price_per_unit,
                                    NULL, 1,
                                    0, 1,
                                    l_rec.list_price_per_unit
                                   ),
                            l_rec.buyer_id, l_rec.item_status,
                            v_acc_ccid
                           ----l_expense_account_c
                           );
            ELSE
               fnd_file.put_line (fnd_file.LOG,
                                     'This is not a valid code combination:'
                                  || ln_segment1
                                  || '.'
                                  || ln_segment2
                                  || '.'
                                  || ln_segment3
                                  || '.'
                                  || ln_segment4
                                  || '.''.'
                                  || ln_segment5
                                  || '.'
                                  || ln_segment6
                                  || '.'
                                  || ln_segment7
                                  || '.'
                                  || ln_segment8
                                 );
            END IF;
         END LOOP;

         l_expense_account_c := NULL;
        ------------------------------------ inserting categoris---------------------------------------------------------------------------------------
        --  for j in c2 loop
         fnd_file.put_line (fnd_file.output,
                               'Inserting Category set:'
                            || ' '
                            || l_rec.category_set_id
                            || '  AND Category ID '
                            || l_rec.category_id
                            || ' For Item'
                            || l_rec.segment1
                           );

         -- if ((j.category_set_id is not null) and (j.category_id is not null)) then
         FOR j IN c4 (l_rec.item_category)
         LOOP
            INSERT INTO mtl_item_categories_interface
                        (item_number, organization_id, transaction_type,
                         category_set_id, category_id, set_process_id,
                         process_flag
                        )
                 VALUES (l_rec.segment1, l_rec.organization_id, 'CREATE',
                         j.category_set_id, j.category_id, 1,
                         1
                        );

            /*   for l in c_child_organization loop
            insert into mtl_item_categories_interface
                              (item_number, organization_id,
                               transaction_type, category_set_id,
                               category_id, set_process_id, process_flag
                              )
                       values (i.segment1, l.organization_id,
                               'create', l_category_set_id,
                               l_category_id, 1, 1
                              );
                              end loop;*/
            -- end if;
            fnd_file.put_line (fnd_file.output,
                                  'Inserting Category set in loop :'
                               || ' '
                               || l_rec.category_set_id
                               || '  AND Category ID '
                               || l_rec.category_id
                               || ' For Item'
                               || l_rec.segment1
                              );
         END LOOP;

         UPDATE xxabrl_inv_sys_item_intf_cstm
            SET complete_flag = 'Y'
          WHERE ROWID = l_rec.ROWID;

         COMMIT;
      END LOOP;
   END xxabrl_item_validate_insert;
END xxabrl_inv_item_interface_pkg; 
/


CREATE OR REPLACE PACKAGE BODY APPS.XXABRL_AP_NAVISION_REC_PKG AS
/*****************************************************************************************************************************************
                 Synergy Web-Tech Ltd, Mumbai, India

 Object Name : xxabrl_ap_nav_inv_pkg

 Description : procedure to give the invoice details

  Change Record:
 =============================================================================================================
 Version    Date            Author                              Remarks
 =======   ==========     =============                         =========================================================================
 1.0.0       01-jul-09   Naresh Kumar (Synergy Web-Tech Ltd.)             Initial Version
 1.0.1       22-sep-09   Naresh Kumar (Synergy Web-Tech Ltd.)     added the outer join to pick rejected records.
 1.0.2       11-dec-09   Naresh Kumar (Synergy Web-Tech Ltd.)     added the error_level column in the report
 1.0.3       11-AUG-10   Govindraj T  (Wipro Limited)             added the logic to avoid the invoices, which has been moved to base already in interface table and staging interface date null to avoid the interfaced invoices
*****************************************************************************************************************************************/
PROCEDURE XXABRL_AP_NAVISION_REC_PROC (ERRBUFF             OUT VARCHAR2,
                                RETCODE             OUT NUMBER,
                                P_SOURCE            IN  VARCHAR2,
                                P_OPERATING_UNIT    IN  VARCHAR2,
                                P_INV_DATE_FROM     IN VARCHAR2,
                                P_INV_DATE_TO       IN VARCHAR2
                                ) AS
CURSOR CUR1(CP_SOURCE VARCHAR2,CP_OPERATING_UNIT VARCHAR2,CP_INV_DATE_FROM VARCHAR2,CP_INV_DATE_TO VARCHAR2) IS
select  SOURCE,
        OPERATING_UNIT, 
        GL_DATE, 
        VENDOR_NUM, 
        INVOICE_NUM, 
        HEADER_ERROR,
        LINE_ERROR,
        GRN_NUMBER,
        ERROR_LEVEL
from
(SELECT   DISTINCT AII.SOURCE, 
                   AII.OPERATING_UNIT, 
                   AII.GL_DATE , 
                   AII.VENDOR_NUMBER VENDOR_NUM, 
                   AII.INVOICE_NUM,
                   AII.ERROR_MESSAGE HEADER_ERROR,
                   AILI.ERROR_MESSAGE LINE_ERROR,
                   AII.GRN_NUMBER,
                   'Stage' ERROR_LEVEL
 FROM APPS.XXABRL_AP_INVOICES_INT AII, APPS.XXABRL_AP_INVOICE_LINES_INT AILI,apps.hr_operating_units hrou
 WHERE
            AII.INVOICE_NUM   = AILI.INVOICE_NUM(+)
   AND AII.VENDOR_NUMBER      = AILI.VENDOR_NUMBER(+)
   and aii.OPERATING_UNIT=hrou.SHORT_CODE(+)
   AND (AII.INTERFACED_FLAG   = 'E' OR AILI.INTERFACED_FLAG = 'E')
   AND AII.INTERFACED_DATE IS NULL
   AND AII.SOURCE             = CP_SOURCE
   AND hrou.ORGANIZATION_ID             = NVL (CP_OPERATING_UNIT,hrou.ORGANIZATION_ID )
   AND AII.GL_DATE            BETWEEN NVL(TO_DATE(CP_INV_DATE_FROM,'YYYY/MM/DD HH24:MI:SS'), AII.GL_DATE)
  AND NVL(TO_DATE(CP_INV_DATE_TO,'YYYY/MM/DD HH24:MI:SS'), AII.GL_DATE)
UNION ALL
select    DISTINCT APII.SOURCE, 
          HRU.SHORT_CODE OPERATING_UNIT, 
          APII.GL_DATE ,
          pov.segment1 VENDOR_NUM, 
          APII.INVOICE_NUM, 
          APIR.REJECT_LOOKUP_CODE HEADER_ERROR,
          NULL LINE_ERROR,
          apili.REFERENCE_1 GRN_NUMBER,
          'Interface' ERROR_LEVEL
from apps.ap_invoices_interface apii
             ,apps.ap_invoice_lines_interface apili
             ,apps.AP_INTERFACE_REJECTIONS apir
            ,apps.po_vendors pov
             ,APPS.HR_OPERATING_UNITS HRU
where apii.invoice_id=apir.PARENT_ID(+) --- added by Naresh on 23-seg-09
and apii.invoice_id=apili.invoice_id(+)
AND  HRU.ORGANIZATION_ID=APII.ORG_ID
and   apii.status='REJECTED'
and pov.VENDOR_ID=apii.VENDOR_ID
AND APII.SOURCE             = CP_SOURCE
AND APII.ORG_ID     = NVL (CP_OPERATING_UNIT, APII.ORG_ID)
AND APII.GL_DATE            BETWEEN NVL(TO_DATE(CP_INV_DATE_FROM,'YYYY/MM/DD HH24:MI:SS'), APII.GL_DATE)
AND NVL(TO_DATE(CP_INV_DATE_TO,'YYYY/MM/DD HH24:MI:SS'), APII.GL_DATE)
AND NOT EXISTS
            (SELECT 1
            FROM APPS.AP_INVOICES_ALL
            WHERE 1=1
            AND VENDOR_ID =  APII.VENDOR_ID
            AND INVOICE_NUM = APII.INVOICE_NUM
            AND INVOICE_AMOUNT = APII.INVOICE_AMOUNT) )
ORDER BY
SOURCE,OPERATING_UNIT,GL_DATE,VENDOR_NUM,INVOICE_NUM,HEADER_ERROR,LINE_ERROR;
BEGIN
FND_FILE.PUT_LINE (FND_FILE.OUTPUT,
                         'ABRL AP NAVISION ERROR REPORT'||'('||P_SOURCE||')'
                        );
      FND_FILE.PUT_LINE (FND_FILE.OUTPUT, ' ');
      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'AS ON DATE'
                         || CHR (9)
                         || SYSDATE
                         || CHR (9)
                        );
      FND_FILE.PUT_LINE (FND_FILE.OUTPUT, ' ');
      FND_FILE.PUT_LINE (FND_FILE.OUTPUT, 'REPORT PARAMETRS');
      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'SOURCE'
                         || CHR (9)
                         || P_SOURCE
                         || CHR (9)
                        );
      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'OPERATING UNIT'
                         || CHR (9)
                         ||  P_OPERATING_UNIT
                         || CHR (9)
                        );
      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'INVOICE DATE FROM'
                         || CHR (9)
                         || P_INV_DATE_FROM
                         || CHR (9)
                        );
      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'INVOICE DATE TO'
                         || CHR (9)
                         || P_INV_DATE_TO
                         || CHR (9)
                        );
-------------------------------------- DISPLAY LABELS OF THE REPORT --------------------------------------------
      FND_FILE.PUT_LINE (FND_FILE.OUTPUT, ' ');
      FND_FILE.PUT_LINE (FND_FILE.OUTPUT, ' ');
      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,
                           'SOURCE'
                         || CHR (9)
                         || 'OPERATING UNIT'
                         || CHR (9)
                         || 'GL DATE'
                         || CHR (9)
                         || 'VENDOR NUMBER'
                         || CHR (9)
                         || 'INVOICE NUMBER'
                         || CHR (9)
                         || 'HEADER ERROR'
                         || CHR (9)
                         || 'LINE ERROR'
                         ||CHR (9)
                         || 'GRN NUMBER'
                         ||CHR (9)
                         || 'ERROR_LEVEL'
                         );
FOR REC1 IN CUR1(P_SOURCE,P_OPERATING_UNIT,P_INV_DATE_FROM,P_INV_DATE_TO) LOOP
EXIT WHEN CUR1%NOTFOUND;
-------------------------- PRINTING THE DATA  ----------------------------------------------------------------
          FND_FILE.PUT_LINE (FND_FILE.OUTPUT,
                               REC1.SOURCE
                            || CHR (9)
                            ||   REC1.OPERATING_UNIT
                            || CHR (9)
                            || REC1.GL_DATE
                            || CHR (9)
                            || REC1.VENDOR_NUM
                            || CHR (9)
                            || REC1.INVOICE_NUM
                            || CHR (9)
                            || REC1.HEADER_ERROR
                            || CHR (9)
                            || REC1.LINE_ERROR
                            || CHR (9)
                            || REC1.GRN_NUMBER
                            || CHR (9)
                            || REC1.ERROR_LEVEL
                            );
END LOOP;
END XXABRL_AP_NAVISION_REC_PROC;
END  XXABRL_AP_NAVISION_REC_PKG; 
/


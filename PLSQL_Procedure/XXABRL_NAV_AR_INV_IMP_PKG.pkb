CREATE OR REPLACE PACKAGE BODY APPS.XXABRL_NAV_AR_INV_IMP_PKG IS
  
  /*
  =========================================================================================================
  ||   Filename   : XXABRL_RESA_AR_CONV_PKG.sql
  ||   Description : Script is used to mold ReSA data for AR
  ||
  ||   Version     Date            Author              Modification
  ||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
  ||   1.0.0       13-MON-2008    Hans Raj Kasana      New Development
  ||   1.0.1       15-SEP-2009    Shailesh Bharambe    Change Request
  ||   1.0.2       19-SEP-2009    Shailesh Bharambe    Minor Modification  
  ||   1.0.2.1     19-SEP-2009    Shailesh Bharambe    Minor Modification
  ||   1.0.3       24-sep-2009    Shailesh Bharambe    Org_id parameter added so it will take org_id from profile or we can also pass the value for org_id
  ||   1.0.4       19-dec-09      Naresh Hasti         added the line amount and distribution amount validation validation procedure
  ||   1.0.5       10-feb-10      Naresh Hasti         added the codition to pick the active transaction type in transaction type validation
  ||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
  ========================================================================================================*/

  
  
  PROCEDURE INVOICE_VALIDATE(errbuf         OUT VARCHAR2,
                             retcode        OUT NUMBER,
                             -- added by shailesh on 24 sep 2009 
                             P_Org_Id       In NUMBER,
                             P_BATCH_Source IN VARCHAR2,
                             p_action VARCHAR2,
                             P_GL_DATE VARCHAR2
                             ) IS
    --
    -- Declaring cursor 1
    --

    CURSOR ART_C1_Validate(CP_ORG_CODE VARCHAR2, CP_Batch_Source VARCHAR2,CP_GL_DATE DATE) IS
      SELECT BATCH_SOURCE_NAME,
             ORG_CODE,
             INTERFACE_LINE_CONTEXT,
             INTERFACE_LINE_ATTRIBUTE1 INTERFACE_LINE_ATTRIBUTE1,
             NV_CUSTOMER_ID,
             NV_CUST_SITE_ID
        FROM XXABRL_NAVI_RA_INT_LINES_ALL
       WHERE /*NVL(BATCH_SOURCE_NAME, CP_Batch_Source) = CP_Batch_Source
               And*/
               NVL(ORG_CODE, CP_ORG_CODE) = CP_ORG_CODE
               AND GL_DATE = NVL(CP_GL_DATE,GL_DATE) AND
       NVL(INTERFACED_FLAG, 'N') IN ('N', 'E')
       GROUP BY BATCH_SOURCE_NAME,
                ORG_CODE,
                INTERFACE_LINE_CONTEXT,
                INTERFACE_LINE_ATTRIBUTE1,
                NV_CUSTOMER_ID,
                NV_CUST_SITE_ID;

    CURSOR ART_C2(CP_BATCH_SOURCE_NAME VARCHAR2, CP_ORG_CODE VARCHAR2, CP_INTERFACE_LINE_ATTRIBUTE1 VARCHAR2, CP_NV_CUSTOMER_NUMBER VARCHAR2, CP_NV_CUST_SITE_ID VARCHAR2,CP_GL_DATE DATE) IS
      SELECT ROWID,
             BATCH_SOURCE_NAME,
             CUSTOMER_NAME,
             ORG_CODE,
             INTERFACE_LINE_CONTEXT,
             INTERFACE_LINE_ATTRIBUTE1 INTERFACE_LINE_ATTRIBUTE1,
             INTERFACE_LINE_ATTRIBUTE2 INTERFACE_LINE_ATTRIBUTE2,
             LINE_TYPE,
             TRX_DATE,
             GL_DATE,
             AMOUNT,
             CURRENCY_CODE,
             CONVERSION_TYPE,
             CONVERSION_RATE,
             CONVERSION_DATE,
             TRANSACTION_TYPE,
             DESCRIPTION,
             TERM_ID,
             NV_CUSTOMER_ID,
             NV_CUST_SITE_ID,
             ATTRIBUTE1,
             ATTRIBUTE_CATEGORY,
             -- added by shailesh on 15 sept
             statementno
        FROM XXABRL_NAVI_RA_INT_LINES_ALL
       WHERE NVL(BATCH_SOURCE_NAME, CP_BATCH_SOURCE_NAME) =
             CP_BATCH_SOURCE_NAME
         AND NVL(ORG_CODE, CP_ORG_CODE) = CP_ORG_CODE
         AND INTERFACE_LINE_ATTRIBUTE1 = CP_INTERFACE_LINE_ATTRIBUTE1
         AND GL_DATE = NVL(CP_GL_DATE,GL_DATE)
         /*And NVL(NV_CUSTOMER_ID, CP_NV_CUSTOMER_NUMBER) =
             CP_NV_CUSTOMER_NUMBER
         And NVL(NV_CUST_SITE_ID, CP_NV_CUST_SITE_ID) = CP_NV_CUST_SITE_ID*/
         AND NVL(INTERFACED_FLAG, 'N') IN ('N', 'E')
       ORDER BY INTERFACE_LINE_ATTRIBUTE1, INTERFACE_LINE_ATTRIBUTE2;



    CURSOR ART_C3(CP_BATCH_SOURCE_NAME VARCHAR2, CP_ORG_CODE VARCHAR2, CP_INTERFACE_LINE_ATTRIBUTE1 VARCHAR2, CP_INTERFACE_LINE_ATTRIBUTE2 VARCHAR2, CP_NV_CUSTOMER_NUMBER VARCHAR2, CP_NV_CUST_SITE_ID VARCHAR2) IS
      SELECT ROWID,
             --             BATCH_SOURCE_NAME,
             ORG_ID,
             INTERFACE_LINE_CONTEXT,
             INTERFACE_LINE_ATTRIBUTE1 INTERFACE_LINE_ATTRIBUTE1,
             INTERFACE_LINE_ATTRIBUTE2 INTERFACE_LINE_ATTRIBUTE2,
             ACCOUNT_CLASS,
             AMOUNT,
             SEGMENT1 CO,
             SEGMENT2 CC,
             SEGMENT3 STATE_SBU,
             SEGMENT4 LOCATION,
             SEGMENT5 MERCHANDISE,
             SEGMENT6 ACCOUNT,
             SEGMENT7 INTERCOMPANY,
             SEGMENT8 FUTURE,
             RECORD_NUMBER
        FROM XXABRL_NAVI_RA_INT_DIST_ALL
       WHERE /*NVL(BATCH_SOURCE_NAME, CP_BATCH_SOURCE_NAME) =
                   CP_BATCH_SOURCE_NAME
               And */
       /*NVL(ORG_ID, CP_ORG_CODE) = CP_ORG_CODE
       And */INTERFACE_LINE_ATTRIBUTE1 = CP_INTERFACE_LINE_ATTRIBUTE1
       AND INTERFACE_LINE_ATTRIBUTE2 = CP_INTERFACE_LINE_ATTRIBUTE2
      /*         And NVL(NV_CUSTOMER_NUMBER, CP_NV_CUSTOMER_NUMBER) =
                   CP_NV_CUSTOMER_NUMBER
               And NVL(NV_CUST_SITE_ID, CP_NV_CUST_SITE_ID) = CP_NV_CUST_SITE_ID*/
       --And NVL(INTERFACED_FLAG, 'N') in ('N', 'E')
       ;
       
    --v_Orgid               NUMBER := Fnd_Profile.VALUE('ORG_ID');
    v_set_of_bks_id       NUMBER := Fnd_Profile.VALUE('GL_SET_OF_BKS_ID');
    V_user_Id             NUMBER := Fnd_profile.VALUE('USER_ID');
    ORG_CODE              hr_operating_units.Short_Code%TYPE;
    V_Batch_Source        RA_BATCH_SOURCES_ALL.NAME%TYPE;
    V_Fun_Curr            VARCHAR2(10);
    V_Error_Count         NUMBER := 0;
    v_OK_Rec_count        NUMBER := 0;
    V_Error_Hmsg          VARCHAR2(1000);
    V_Error_Lmsg          VARCHAR2(1000);
    v_record_count        NUMBER := 0;
    V_Data_Count          NUMBER := 0;
    v_line_count          NUMBER := 0;
    v_currency            fnd_currencies.currency_code%TYPE;
    v_tax_name            VARCHAR2(50);
    V_CC_id               NUMBER;
    V_State_Sbu           gl_code_combinations.Segment3%TYPE;
    v_Code_Combination_id NUMBER;
    V_CUST_TRX_TYPE_ID    NUMBER;
    V_Gl_ID_REV           NUMBER;
    V_GL_ID_REC           NUMBER;
    V_Cust_Account_Id     NUMBER;
    V_cust_site_id        NUMBER;
    --V_TERM_ID      RA_TERMS.Name%Type;
    V_Term_id     NUMBER;
    V_UOM_Name    VARCHAR2(20);
    V_Description VARCHAR2(240);
    V_Seg_Status  VARCHAR2(100);
    V_GL_DATE DATE;
    
    -- commented by shailesh on 24 sep 2009 as the profile values will come through parameter
   -- P_Org_Id NUMBER:=Fnd_Profile.VALUE('ORG_ID');
    --P_BATCH_Source varchar2(50);
    v_cust_Acct_site_id NUMBER;
    vn_structure_id NUMBER;
    p_gl_id_rev VARCHAR2(300);
    x_gl_rev_acct_id NUMBER;
    v_Conc_Segments VARCHAR2(250);
    v_TRX_TYPE VARCHAR2(15);
    v_ATTRIBUTE_CATEGORY    VARCHAR2(240);
   
  BEGIN
  
  --- code for the checking the line amount and distribution amount matching 
  
  
    BEGIN
fnd_file.put_line(fnd_file.log,'Start');
 fnd_file.put_line(fnd_file.LOG,'P_BATCH_Source: '||NVL(P_BATCH_Source,'NULL'));
 
  fnd_file.put_line(fnd_file.log,'org_id -> '||P_Org_Id);
    --P_BATCH_Source := 'ABRL NAVISION TO AR';
fnd_file.put_line(fnd_file.log,'P_GL_DATE: '||P_GL_DATE);

       -- ADDED BY SHAILESH ON 11 FEB 2009 ADDING PARAMETER AS GL DATE TO RUN ONLY SPECIFIC DATE DATA
    SELECT TO_DATE(P_GL_DATE,'yyyy/mm/dd:HH24:MI:SS') INTO V_GL_DATE FROM dual;

    fnd_file.put_line(fnd_file.log,'gl date: '||V_GL_DATE);


    -- added by shailesh on 24 sep 2009 
    -- passing of org_id from parameter 
    -- parameter will fetch the org_id from profile value. 
    -- the logic is implemented for in one file multiple ou data will come so according to process
    -- we are passing only sysdate - 3 gl_date to rocess other gl_date data we have to submit it through backend
        
 


    SELECT short_code
        INTO ORG_CODE
         FROM hr_operating_units
         WHERE organization_id = P_Org_Id;

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        fnd_file.put_line(fnd_file.output,
                          'Selected Org Id does not exist in Oracle Financials');
        fnd_file.put_line(fnd_file.output,
                          '........................................................................');
      WHEN TOO_MANY_ROWS THEN
        fnd_file.put_line(fnd_file.output,
                          'Multiple Org Id exist in Oracle Financials');
        fnd_file.put_line(fnd_file.output,
                          '........................................................................');
      WHEN OTHERS THEN
        fnd_file.put_line(fnd_file.output, 'Invalid Org Id Selected ');
        fnd_file.put_line(fnd_file.output,
                          '........................................................................');
    END;

    UPDATE XXABRL_NAVI_RA_INT_LINES_ALL
       SET CREATED_BY = v_user_id,
       BATCH_SOURCE_NAME = INITCAP(BATCH_SOURCE_NAME),
       TRANSACTION_TYPE = INITCAP(TRANSACTION_TYPE);

    UPDATE XXABRL_NAVI_RA_INT_LINES_ALL
       SET CREATED_BY = v_user_id
     WHERE Trim(UPPER(BATCH_SOURCE_NAME)) = Trim(UPPER(P_BATCH_Source))
       AND Trim(UPPER(ORG_CODE)) = Trim(UPPER(ORG_CODE))
       AND NVL(INTERFACED_FLAG, 'N') = 'N'
       AND CREATED_BY IS NULL;
    UPDATE XXABRL_NAVI_RA_INT_DIST_ALL
       SET CREATED_BY = v_user_id
     WHERE /*Trim(Upper(BATCH_SOURCE_NAME)) = Trim(Upper(P_BATCH_Source))
           And */
     Trim(UPPER(ORG_CODE)) = Trim(UPPER(ORG_CODE))
     AND NVL(INTERFACED_FLAG, 'N') = 'N'
     AND CREATED_BY IS NULL;
    COMMIT;
    --Deleting previous error message if any--
    UPDATE XXABRL_NAVI_RA_INT_LINES_ALL
       SET ERROR_MESSAGE = NULL
     WHERE Trim(UPPER(BATCH_SOURCE_NAME)) = Trim(UPPER(P_BATCH_Source))
       AND Trim(UPPER(ORG_CODE)) = Trim(UPPER(ORG_CODE))
       AND NVL(INTERFACED_FLAG, 'N') IN ('N', 'E')
       AND ERROR_MESSAGE IS NOT NULL;
    UPDATE XXABRL_NAVI_RA_INT_DIST_ALL
       SET ERROR_MESSAGE = NULL
     WHERE /*Trim(Upper(BATCH_SOURCE_NAME)) = Trim(Upper(P_BATCH_Source))
           And */
     Trim(UPPER(ORG_CODE)) = Trim(UPPER(ORG_CODE))
     AND NVL(INTERFACED_FLAG, 'N') IN ('N', 'E')
     AND ERROR_MESSAGE IS NOT NULL;
    COMMIT;

    BEGIN
      SELECT Currency_code
        INTO V_Fun_Curr
        FROM GL_SETS_OF_BOOKS
       WHERE set_of_books_id = v_set_of_bks_id;
       fnd_file.put_line(fnd_file.output,'GL SOB_ID :' ||v_set_of_bks_id);
    EXCEPTION
      WHEN OTHERS THEN
        V_Fun_Curr := NULL;
        fnd_file.put_line(fnd_file.output,'Exception in Currency Code');
    END;


    ------------------------------------------CALLING amount validation procedure  added by Naresh on 19-dec-09----------
    
    fnd_file.put_line(fnd_file.output,'-------------------------------------------------');
    fnd_file.put_line(fnd_file.output,'****AR Transaction amount Validating Log**** ');
    
    
    AMOUNT_VALIDATION (ORG_CODE,P_BATCH_Source,V_GL_DATE);
    
    fnd_file.put_line(fnd_file.output,'-------------------------------------------------');
    fnd_file.put_line(fnd_file.output,'****end the amount validation Program****');
    
    -------------------end amount validation procedure-------------------------

    
    
    --************************************************************************************
    --**  Additional Loop to Update Customer Site-id and Tran-ID.                        *
    --**  After updating base table, updated data will be used in Re-Call of same loop   *
    --************************************************************************************
/**/    fnd_file.put_line(fnd_file.output,'-------------------------------------------------');
/**/    fnd_file.put_line(fnd_file.output,'***Running update Script for Cust-Site-ID / Tran-ID');

/**/    FOR ART_R1 IN ART_C1_Validate(ORG_CODE, P_BATCH_Source,V_GL_DATE ) LOOP
/**/      EXIT WHEN ART_C1_Validate%NOTFOUND;
/**/
/**/      FOR ART_R2 IN ART_C2(ART_R1.BATCH_SOURCE_NAME,
/**/                           ART_R1.ORG_CODE,
/**/                           ART_R1.INTERFACE_LINE_ATTRIBUTE1,
/**/                           ART_R1.NV_CUSTOMER_ID,
/**/                           ART_R1.NV_CUST_SITE_ID,
                               V_GL_DATE ) LOOP
/**/        EXIT WHEN ART_C2%NOTFOUND;
/**/
/**/        V_Error_Hmsg      := Null;
/**/        V_Data_Count      := NULL;
/**/        V_CC_id           := Null;
/**/        V_Cust_Account_Id := NULL;
/**/        V_cust_site_id    := Null;
/**/        V_Term_id         := NULL;
/**/        V_Description     := Null;
/**/
/**/        V_Error_Hmsg :=NULL;
/**/
/**/        --#######################################
/**/        -- Validating / Updating Customer ID
/**/        --#######################################
/**/        IF ART_R2.CUSTOMER_NAME IS NOT NULL THEN
/**/          BEGIN
/**/            SELECT
/**/            cust_account_id into v_CUST_ACCOUNT_ID
/**/            FROM
/**/            hz_cust_accounts hca
/**/            /*, hz_parties hp */
/**/            where
/**/            hca.account_number = ART_R2.CUSTOMER_NAME
/**/            /*upper(hp.party_name) = upper(ART_R2.CUSTOMER_NAME)*/
/**/            /*and hp.party_id = hca.party_id*/
/**/            ;
/**/
/**/            Begin
/**/            UPDATE XXABRL_NAVI_RA_INT_LINES_ALL int_ln
/**/            set int_ln.NV_CUSTOMER_ID = v_CUST_ACCOUNT_ID
/**/            WHERE ROWID = ART_R2.ROWID;
/**/                  BEGIN
/**/                          SELECT  hcas.cust_Acct_site_id INTO v_cust_Acct_site_id
/**/                     From hz_cust_accounts_all         hca,
/**/                          hz_cust_acct_sites_all       hcas,
/**/                          hz_party_sites               hps,
/**/                          hz_cust_site_uses_all        hcsu
/**/                    Where hca.cust_account_id = v_CUST_ACCOUNT_ID
/**/                      AND hca.cust_account_id = hcas.cust_account_id
/**/                      And hcas.org_id = P_Org_id
/**/                      AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id(+)
/**/                      And hcas.party_site_id = hps.party_site_id
/**/        --              And hps.Party_Site_Number = xac.OF_Customer_Site_Code
/**/                      And hcsu.site_use_code = 'BILL_TO'
/**/                      AND hcas.bill_to_flag = 'P'
/**/                      And hcas.status = 'A';
/**/
/**/                      BEGIN
/**/                        UPDATE XXABRL_NAVI_RA_INT_LINES_ALL int_ln
/**/                        set int_ln.NV_CUST_SITE_ID = v_cust_Acct_site_id
/**/                        WHERE ROWID = ART_R2.ROWID;
/**/
/**/                      EXCEPTION
/**/                      when others then
/**/                      fnd_file.put_line(fnd_file.output,'Exception while updating Customer Site '||ART_R2.CUSTOMER_NAME);
/**/                      END;
/**/
/**/
/**/                  EXCEPTION
/**/                      when NO_DATA_FOUND then
/**/                      V_Error_Hmsg := V_Error_Hmsg || 'Bill_TO Site not Found for '||ART_R2.CUSTOMER_NAME ||' -->>';
/**/                      when too_many_rows then
/**/                      V_Error_Hmsg := V_Error_Hmsg || 'Multiple Bill_TO Site Found for '||ART_R2.CUSTOMER_NAME ||' -->>';
/**/                  End;
/**/
/**/
/**/            EXCEPTION
/**/                when others then
/**/                fnd_file.put_line(fnd_file.output,'Exception while updating Customer Id '||ART_R2.CUSTOMER_NAME);
/**/            END;
/**/
/**/          EXCEPTION
/**/          WHEN NO_DATA_FOUND THEN
/**/          V_Error_Hmsg := V_Error_Hmsg || 'CUSTOMER '||ART_R2.CUSTOMER_NAME || '  not defined-->>';
/**/          WHEN TOO_MANY_ROWS THEN
/**/          V_Error_Hmsg := V_Error_Hmsg || 'Multiple CUSTOMER  defined-->>';
/**/          END;
/**/
/**/        ELSE
/**/            V_Error_Hmsg := V_Error_Hmsg || 'Customer Name Type is Blank-->>';
/**/        END IF;
/**/
/**/        --#######################################
/**/        -- Validating / Updating Transaction Type/ID
/**/        --#######################################
/**/        if ART_R2.TRANSACTION_TYPE is not NULL then
/**/          BEGIN
/**/            select
/**/            CUST_TRX_TYPE_ID, TYPE
/**/            into v_CUST_TRX_TYPE_ID ,v_TRX_TYPE
/**/            FROM ra_cust_trx_types_all
/**/            where
/**/            UPPER(NAME) = UPPER(ART_R2.TRANSACTION_TYPE)
                AND END_DATE is null
/**/            and org_id = P_Org_Id
/**/            ;
/**/
/**/            BEGIN
/**/            if v_TRX_TYPE = 'CM' then
/**/              UPDATE XXABRL_NAVI_RA_INT_LINES_ALL int_ln
/**/              set
/**/              int_ln.CUST_TRX_TYPE_ID = v_CUST_TRX_TYPE_ID
/**/              ,Term_id_new =NULL
/**/              ,Term_id     =NULL
/**/              where rowid = ART_R2.rowid;
/**/            ELSE
/**/              update XXABRL_NAVI_RA_INT_LINES_ALL int_ln
/**/              SET int_ln.CUST_TRX_TYPE_ID = v_CUST_TRX_TYPE_ID
/**/              where rowid = ART_R2.rowid;
/**/            END IF;
/**/
/**/            EXCEPTION
/**/                when others then
/**/                fnd_file.put_line(fnd_file.output,'Exception while updating Transaction Id '||ART_R2.TRANSACTION_TYPE);
/**/            END;
/**/
/**/          EXCEPTION
/**/          WHEN NO_DATA_FOUND THEN
/**/               V_Error_Hmsg := V_Error_Hmsg || 'Transaction Type not defined-->>';
/**/          WHEN TOO_MANY_ROWS THEN
/**/               V_Error_Hmsg := V_Error_Hmsg || 'Multiple Transaction Type defined-->>';
/**/          WHEN OTHERS THEN
/**/               V_Error_Hmsg := V_Error_Hmsg || 'Exception in Transaction Type Validation-->>';
/**/          END;
/**/
/**/        ELSE
/**/            V_Error_Hmsg := V_Error_Hmsg || 'Transaction Type is Blank-->>';
/**/        END IF;
/**/        END LOOP;
/**/    END LOOP;
    --************************************************************************************
    --**  Additional Loop to Update Customer Site-id and Tran-ID.                        *
    --**  After updating base table, updated data will be used in Re-Call of same loop   *
    --************************************************************************************
    --************************************************************************************Ends....

    fnd_file.put_line(fnd_file.output,
                      '          ### AR Transaction Information Validating Log ###');
    fnd_file.put_line(fnd_file.output,
                      '........................................................................');


    FOR ART_R1 IN ART_C1_Validate(ORG_CODE, P_BATCH_Source,V_GL_DATE) LOOP
      EXIT WHEN ART_C1_Validate%NOTFOUND;

      v_record_count := ART_C1_Validate%ROWCOUNT;
      fnd_file.put_line(fnd_file.output,'Batch:'||P_BATCH_Source||' / Org_Code: '||ORG_CODE);

      FOR ART_R2 IN ART_C2(ART_R1.BATCH_SOURCE_NAME,
                           ART_R1.ORG_CODE,
                           ART_R1.INTERFACE_LINE_ATTRIBUTE1,
                           ART_R1.NV_CUSTOMER_ID,
                           ART_R1.NV_CUST_SITE_ID,
                           V_GL_DATE) LOOP
        EXIT WHEN ART_C2%NOTFOUND;
        V_Error_Hmsg      := NULL;
        V_Data_Count      := NULL;
        V_CC_id           := NULL;
        V_Cust_Account_Id := NULL;
        V_cust_site_id    := NULL;
        V_Term_id         := NULL;
        V_Description     := NULL;

        fnd_file.put_line(fnd_file.output,'----------------------');
        fnd_file.put_line(fnd_file.output,'Line# '||ART_R2.INTERFACE_LINE_ATTRIBUTE1||'('||ART_R2.INTERFACE_LINE_ATTRIBUTE2||')');


        V_Error_Hmsg :=NULL;


        --#######################################
        -- Validating Bacth Source
        --#######################################
        IF ART_R2.BATCH_SOURCE_NAME IS NULL THEN
          V_Error_Hmsg := V_Error_Hmsg || 'Batch Source is Null-->>';
        ELSE
          V_Batch_Source := NULL;
          BEGIN
            SELECT NAME
              INTO V_Batch_Source
              FROM RA_BATCH_SOURCES_ALL
             WHERE NAME = ART_R2.BATCH_SOURCE_NAME
               AND Org_id = P_Org_ID
               AND Status = 'A'
               AND Batch_Source_Type = 'FOREIGN'
               AND SYSDATE BETWEEN NVL(Start_Date, SYSDATE) AND
                   NVL(End_Date, SYSDATE);
          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              V_Error_Hmsg := V_Error_Hmsg ||
                              'Batch Source does not exist in Oracle Financials-->>';
            WHEN TOO_MANY_ROWS THEN
              V_Batch_Source := NULL;
              V_Error_Hmsg   := V_Error_Hmsg ||
                                'Multiple Batch Source found-->>';
            WHEN OTHERS THEN
              V_Error_Hmsg := V_Error_Hmsg || 'Wrong Batch Source Code-->>';
          END;
        END IF;

        --#######################################
        -- Validating ORG Code
        --#######################################

        IF ART_R2.ORG_CODE IS NULL THEN
          V_Error_Hmsg := V_Error_Hmsg || 'Operating Unit is Null-->>';
        END IF;

        --#######################################
        -- Validating LINE Type
        --#######################################
        IF ART_R2.LINE_TYPE IS NULL THEN
          V_Error_Hmsg := V_Error_Hmsg || 'Line Type is Null-->>';
        ELSIF UPPER(ART_R2.LINE_TYPE) <> 'LINE' THEN
          V_Error_Hmsg := V_Error_Hmsg || 'Invalid Line Type-->>';
        END IF;

        --#######################################
        -- Validating TRX Date / GL Period
        --#######################################

        IF ART_R2.TRX_DATE IS NULL THEN
          V_Error_Hmsg := V_Error_Hmsg || 'Transaction Date is null-->>';
        END IF;
        IF ART_R2.GL_DATE IS NULL THEN
          V_Error_Hmsg := V_Error_Hmsg || 'GL Date is null-->>';
        ELSE
          BEGIN
            V_Data_Count := NULL;
            SELECT COUNT(gps.Period_Name)
              INTO V_Data_Count
              FROM gl_period_statuses gps, fnd_application fna
             WHERE fna.application_short_name = 'AR'
               AND fna.application_id = gps.application_id
               AND gps.closing_status = 'O'
               AND gps.set_of_books_id = v_set_of_bks_id
               AND ART_R2.GL_DATE BETWEEN gps.start_date AND gps.end_date;

      --- sdded for testing
         Fnd_file.PUT_LINE(fnd_file.LOG,'Interface Line attr  '||ART_R1.INTERFACE_LINE_ATTRIBUTE1);
                Fnd_file.PUT_LINE(fnd_file.LOG,'Set of Book id '||v_set_of_bks_id);
            Fnd_file.PUT_LINE(fnd_file.LOG,'count '||V_Data_Count);
               Fnd_file.PUT_LINE(fnd_file.LOG,'--------------------------------------------------');
      --- remove it after testing
            IF V_Data_Count = 0 THEN
              V_Error_Hmsg := V_Error_Hmsg ||
                              'GL date is not in AR Open Period -->>';
            END IF;
          EXCEPTION
            WHEN OTHERS THEN
              V_Error_Hmsg := V_Error_Hmsg ||
                              'GL Period not open in AR -->>';
          END;
        END IF;
        --#######################################
        -- Validating Invoice Amount
        --#######################################

        IF ART_R2.AMOUNT IS NULL THEN
          V_Error_Hmsg := V_Error_Hmsg || 'Invoice Amount is null-->>';
        END IF;
        --#######################################
        -- Validating Currency Code
        --#######################################

        IF ART_R2.Currency_Code IS NULL THEN
          v_error_Hmsg := v_error_Hmsg || 'Currency Code is null -->';
        ELSE
          BEGIN
            SELECT currency_code
              INTO v_currency
              FROM fnd_currencies
             WHERE UPPER(currency_code) = Trim(UPPER(ART_R2.Currency_Code));
            IF UPPER(trim(v_currency)) <> UPPER(trim(V_Fun_Curr)) AND
               ART_R2.CONVERSION_DATE IS NULL THEN
              v_error_hmsg := v_error_hmsg || 'Exchange Date is Null -->>';
            END IF;
          EXCEPTION
            WHEN OTHERS THEN
              v_error_hmsg := v_error_hmsg || 'Invalid Currency -->>';
          END;
        END IF;
        --#######################################
        -- Validating Transaction Type
        --#######################################

        IF ART_R2.TRANSACTION_TYPE IS NULL THEN
          v_error_hmsg := v_error_hmsg || 'Transaction Type Is Null -->>';
        ELSE
          BEGIN
            V_CUST_TRX_TYPE_ID := NULL;
            SELECT CUST_TRX_TYPE_ID
              INTO V_CUST_TRX_TYPE_ID
              FROM RA_CUST_TRX_TYPES_ALL
             WHERE UPPER(NAME) = UPPER(Trim(ART_R2.TRANSACTION_TYPE))
               AND END_DATE is null
               AND org_id = P_Org_Id;
            IF V_CUST_TRX_TYPE_ID IS NULL THEN
              v_error_hmsg := v_error_hmsg ||
                              'Invalid TRANSACTION TYPE  -->>';
            END IF;
          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              V_Error_Hmsg := V_Error_Hmsg ||
                              'Transaction Type does not exist in Oracle Financials-->>';
            WHEN TOO_MANY_ROWS THEN
              V_Error_Hmsg := V_Error_Hmsg ||
                              'Multiple Transaction Type found-->>';
            WHEN OTHERS THEN
              v_error_hmsg := v_error_hmsg ||
                              'Invalid Transaction Type  -->>';
          END;
        END IF;
        --#######################################
        -- Validating Payment Term
        --#######################################
        IF ART_R2.TERM_ID IS NOT NULL THEN
          v_term_id := NULL;
          BEGIN

          IF UPPER(ART_R2.TERM_ID) = 'CREDIT NOTE' THEN
             v_term_id:=NULL;
          ELSE
            SELECT Term_id
              INTO v_term_id
              FROM RA_TERMS
             WHERE UPPER(NAME) = UPPER(trim(ART_R2.TERM_ID))
               AND SYSDATE BETWEEN NVL(start_date_active, SYSDATE) AND
                   NVL(end_date_active, SYSDATE);
          END IF;
          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              v_error_hmsg := v_error_hmsg ||
                              'Payment Terms does not exist in Oracle Financials-->>';
            WHEN TOO_MANY_ROWS THEN
              v_term_id    := NULL;
              v_error_hmsg := v_error_hmsg ||
                              'Multiple Payment Terms found-->>';
            WHEN OTHERS THEN
              v_term_id    := NULL;
              v_error_hmsg := v_error_hmsg || 'Invalid Payment Term -->>';
          END;
        END IF;
        --###########################################
        -- Validating Customer ID / Customer Site ID
        --###########################################

        IF ART_R2.NV_CUSTOMER_ID IS NULL THEN
          --v_error_hmsg := v_error_hmsg ||'E-Retail Customer / Store Code Is Null -->>';
          NULL;
        END IF;
        IF ART_R2.NV_CUST_SITE_ID IS NULL THEN
          --v_error_hmsg := v_error_hmsg || 'E-Retail DC Code Is Null -->>';
          NULL;
        END IF;
        IF ART_R2.NV_CUSTOMER_ID IS NOT NULL AND
           ART_R2.NV_CUST_SITE_ID IS NOT NULL THEN
          BEGIN
            V_Cust_Account_Id := ART_R2.NV_CUSTOMER_ID;
            V_cust_site_id    := ART_R2.NV_CUST_SITE_ID;
            /*Select Unique hca.Cust_Account_Id, hcas.cust_Acct_site_id
             Into V_Cust_Account_Id, V_cust_site_id
             From hz_cust_accounts_all         hca,
                  hz_cust_acct_sites_all       hcas,
                  hz_party_sites               hps,
                  hz_cust_site_uses_all        hcsu,
                  xxabrl_ar_cust_bkacc_map_int xac
            Where NV_CUST_SITE_ID = ART_R2.NV_CUST_SITE_ID
              And NV_CUSTOMER_NUMBER = ART_R2.NV_CUSTOMER_NUMBER
              And hca.Account_Number = xac.OF_Customer_Number
              And hca.cust_account_id = hcas.cust_account_id
              And hcas.org_id = P_Org_id
              And hcas.cust_acct_site_id = hcsu.cust_acct_site_id(+)
              And hcas.party_site_id = hps.party_site_id
              And hps.Party_Site_Number = xac.OF_Customer_Site_Code
              And hcsu.site_use_code = 'BILL_TO'
              And hcas.bill_to_flag = 'P'
              And hcas.status = 'A';*/
          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              v_error_hmsg := v_error_hmsg ||
                              'Customer Number Or Site in not exist in this Org -->>';
            WHEN TOO_MANY_ROWS THEN
              v_error_hmsg := v_error_hmsg ||
                              'Multiple Customer Number Or Site found in this Org -->>';
            WHEN OTHERS THEN
              v_error_hmsg := v_error_hmsg ||
                              'Customer Number Or Site in not exist in this Org -->>';
          END;
        END IF;

        --#######################################
        -- Validating Tax Code - Context Value
        --#######################################
        v_ATTRIBUTE_CATEGORY :=NULL;
        IF ART_R2.ATTRIBUTE_CATEGORY IS NOT NULL THEN

          BEGIN
            SELECT DESCRIPTIVE_FLEX_CONTEXT_CODE
              INTO v_ATTRIBUTE_CATEGORY
              FROM FND_DESCR_FLEX_CONTEXTS_VL
             WHERE UPPER(DESCRIPTIVE_FLEX_CONTEXT_CODE) =
                   UPPER(ART_R2.ATTRIBUTE_CATEGORY)
               AND (DESCRIPTIVE_FLEXFIELD_NAME LIKE 'RA_CUSTOMER_TRX_LINES')
               AND enabled_flag = 'Y'
               ;

               BEGIN
                   UPDATE XXABRL_NAVI_RA_INT_LINES_ALL
                   SET ATTRIBUTE_CATEGORY   = v_ATTRIBUTE_CATEGORY
                   WHERE ROWID = ART_R2.ROWID;
               EXCEPTION
               WHEN OTHERS THEN
                   v_error_hmsg := v_error_hmsg ||
                      'Exception while updating derived Tax ATTRIBUTE_CATEGORY for:'||Trim(UPPER(ART_R2.ATTRIBUTE_CATEGORY))||' -->>';
               END;
          EXCEPTION
          WHEN NO_DATA_FOUND THEN
                v_error_hmsg := v_error_hmsg ||
                                'Tax ATTRIBUTE_CATEGORY does not exists for:'||Trim(UPPER(ART_R2.ATTRIBUTE_CATEGORY))||' -->>';
          WHEN TOO_MANY_ROWS THEN
                v_error_hmsg := v_error_hmsg ||
                                'Too many rows defined for Tax ATTRIBUTE_CATEGORY:'||Trim(UPPER(ART_R2.ATTRIBUTE_CATEGORY))||' -->>';
          WHEN OTHERS THEN
                v_error_hmsg := v_error_hmsg ||
                                'Exception while deriving Tax ATTRIBUTE_CATEGORY:'||Trim(UPPER(ART_R2.ATTRIBUTE_CATEGORY))||' -->>'||SQLERRM;
          END;

        END IF;


        --#######################################
        -- Validating Tax Code
        --#######################################

        IF ART_R2.ATTRIBUTE1 IS NOT NULL THEN
          BEGIN
            SELECT Tax_name
              INTO v_tax_name
              FROM JAI_CMN_TAXES_ALL
             WHERE UPPER(Tax_Name) = Trim(UPPER(ART_R2.ATTRIBUTE1))
               AND org_id = P_org_id
               AND SYSDATE BETWEEN NVL(Start_date, SYSDATE) AND
                   NVL(End_Date, SYSDATE);
          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              v_error_hmsg := v_error_hmsg ||
                              'Tax Code does not exists for:'||Trim(UPPER(ART_R2.ATTRIBUTE1))||' -->>';
            WHEN TOO_MANY_ROWS THEN
              v_error_hmsg := v_error_hmsg ||
                              'Tax Code does not exists for:'||Trim(UPPER(ART_R2.ATTRIBUTE1))||' -->>';
            WHEN OTHERS THEN
              v_error_hmsg := v_error_hmsg ||
                              'Exception for Tax Code :'||Trim(UPPER(ART_R2.ATTRIBUTE1))||' -->>';
          END;
        END IF;
        --#######################################
        -- Validating Distribution Segments
        --#######################################

--        Fnd_file.PUT_LINE(fnd_file.Output,'---Validation for Segments--');
--        Fnd_file.PUT_LINE(fnd_file.Output,'ART_R2.INTERFACE_LINE_ATTRIBUTE1 '||ART_R2.INTERFACE_LINE_ATTRIBUTE1);
--        Fnd_file.PUT_LINE(fnd_file.Output,'ART_R2.INTERFACE_LINE_ATTRIBUTE2 '||ART_R2.INTERFACE_LINE_ATTRIBUTE2);
--        Fnd_file.PUT_LINE(fnd_file.Output,'ART_R2.NV_CUSTOMER_ID '||ART_R2.NV_CUSTOMER_ID);
--        Fnd_file.PUT_LINE(fnd_file.Output,'ART_R2.NV_CUST_SITE_ID '||ART_R2.NV_CUST_SITE_ID);

        FOR ART_R3 IN ART_C3(ART_R2.BATCH_SOURCE_NAME,
                             ART_R2.ORG_CODE,
                             ART_R2.INTERFACE_LINE_ATTRIBUTE1,
                             ART_R2.INTERFACE_LINE_ATTRIBUTE2,
                             ART_R2.NV_CUSTOMER_ID,
                             ART_R2.NV_CUST_SITE_ID) LOOP
          EXIT WHEN ART_C3%NOTFOUND;
          V_Error_Lmsg          := NULL;
          V_Code_Combination_id := NULL;



          IF ART_R3.AMOUNT IS NULL THEN
            V_Error_Lmsg := 'Invoice Account Line Amount is null-->>';
          ELSIF ART_R3.AMOUNT <> ART_R2.AMOUNT THEN
            V_Error_Lmsg := 'Invoice Line Amount is not equal to Account Line Amount-->>';
          END IF;

          IF ART_R3.CO IS NOT NULL THEN
            --  V_Error_Lmsg:=V_Error_Lmsg||'Accounting Segment Company Value is Null-->>';
            --Else
            V_Seg_Status := NULL;
            V_Seg_Status := ACCOUNT_SEG_STATUS(ART_R3.CO, 'ABRL_GL_CO');
            IF V_Seg_Status IS NOT NULL THEN
              V_Error_Lmsg := V_Error_Lmsg || 'Invalid Company Segment ' ||
                              ART_R3.CO || '-->>';
            END IF;
          END IF;

          -- Validation for CC Seg
          IF ART_R3.CC IS NOT NULL THEN
            --  V_Error_Lmsg:=V_Error_Lmsg||'Accounting Segment Cost Center Value is Null-->>';
            --Else
            V_Seg_Status := NULL;
            V_Seg_Status := ACCOUNT_SEG_STATUS(ART_R3.CC, 'ABRL_GL_CC');
            IF V_Seg_Status IS NOT NULL THEN
              V_Error_Lmsg := V_Error_Lmsg ||
                              'Invalid Cost Center Segment ' || ART_R3.CC ||
                              '-->>';
            END IF;
          END IF;
          -- Validation for Location Seg
          IF ART_R3.LOCATION IS NOT NULL THEN
            -- V_Error_Lmsg:=V_Error_Lmsg||'Accounting Segment Location Value is Null-->>';
            --Else
            V_Seg_Status := NULL;
            V_Seg_Status := ACCOUNT_SEG_STATUS(ART_R3.LOCATION,
                                               'ABRL_GL_Location');
            IF V_Seg_Status IS NOT NULL THEN
              V_Error_Lmsg := V_Error_Lmsg || 'Invalid Location Segment ' ||
                              ART_R3.LOCATION || '-->>';
            END IF;
          END IF;
          -- Validation for Merchandise Seg
          IF ART_R3.Merchandise IS NOT NULL THEN
            --  V_Error_Lmsg:=V_Error_Lmsg||'Accounting Segment Merchandise Value is Null-->>';
            --Else
            V_Seg_Status := NULL;
            V_Seg_Status := ACCOUNT_SEG_STATUS(ART_R3.Merchandise,
                                               'ABRL_GL_Merchandize');
            IF V_Seg_Status IS NOT NULL THEN
              V_Error_Lmsg := V_Error_Lmsg ||
                              'Invalid Merchandise Segment ' ||
                              ART_R3.Merchandise || '-->>';
            END IF;
          END IF;
          -- Validation for Account Seg
          IF ART_R3.ACCOUNT IS NOT NULL THEN
            --  V_Error_Lmsg:=V_Error_Lmsg||'Accounting Segment Natural Account Value is Null-->>';
            --Else
            V_Seg_Status := NULL;
            V_Seg_Status := ACCOUNT_SEG_STATUS(ART_R3.ACCOUNT,
                                               'ABRL_GL_Account');
            IF V_Seg_Status IS NOT NULL THEN
              V_Error_Lmsg := V_Error_Lmsg || 'Invalid Account Segment ' ||
                              ART_R3.ACCOUNT || '-->>';
            END IF;
          END IF;
          -- Validation for Intercompany Seg
          IF ART_R3.Intercompany IS NOT NULL THEN
            --  V_Error_Lmsg:=V_Error_Lmsg||'Accounting Segment Intercompany Value is Null-->>';
            --Else
            V_Seg_Status := NULL;
            V_Seg_Status := ACCOUNT_SEG_STATUS(ART_R3.Intercompany,
                                               'ABRL_GL_STATE_SBU');
            IF V_Seg_Status IS NOT NULL THEN
              V_Error_Lmsg := V_Error_Lmsg ||
                              'Invalid Intercompany Segment ' ||
                              ART_R3.Intercompany || '-->>';
            END IF;
          END IF;
          -- Validation for Future Seg
          IF ART_R3.Future IS NOT NULL THEN
            --  V_Error_Lmsg:=V_Error_Lmsg||'Accounting Segment Future Value is Null-->>';
            --Else
            V_Seg_Status := NULL;
            V_Seg_Status := ACCOUNT_SEG_STATUS(ART_R3.Future,
                                               'ABRL_GL_Future');
            IF V_Seg_Status IS NOT NULL THEN
              V_Error_Lmsg := V_Error_Lmsg || 'Invalid Future Segment ' ||
                              ART_R3.Future || '-->>';
            END IF;
          END IF;




          IF V_Cust_Account_Id IS NOT NULL AND V_cust_site_id IS NOT NULL THEN
            V_State_Sbu := NULL;
            BEGIN
              SELECT gcc.segment3
                INTO V_State_Sbu
                FROM gl_code_combinations   gcc,
                     hz_cust_acct_sites_all hcas,
                     hz_cust_site_uses_all  hcsu
               WHERE hcas.cust_account_id = V_Cust_Account_Id
               -- added by shailesh on 24 sep 2009 as the org_id  is taking from parameter.
                 AND hcas.org_id = p_org_id --v_Orgid
                 AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id(+)
                 AND hcas.cust_Acct_site_id = V_cust_site_id
                 AND hcsu.gl_id_rev = gcc.Code_Combination_Id;

                 --fnd_file.put_line(Fnd_file.OUTPUT,'Derived SBU: '||V_State_Sbu);

            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                V_Error_Lmsg := V_Error_Lmsg ||
                                'Accounting Code missing at Customer ' ||
                                ART_R2.CUSTOMER_NAME || ' Site  -->>';
                V_State_Sbu  := NULL;
              WHEN TOO_MANY_ROWS THEN
                V_Error_Lmsg := V_Error_Lmsg ||
                                'Multiple Charge Account Code exists for ' ||
                                ART_R2.CUSTOMER_NAME || ' Site-->>';
                V_State_Sbu  := NULL;
              WHEN OTHERS THEN
                V_Error_Lmsg := V_Error_Lmsg ||
                                'Accounting Code missing at Vendor Site ' ||
                                ART_R2.CUSTOMER_NAME || ' Site-->>';
                V_State_Sbu  := NULL;
            END;
          END IF;

          IF V_State_Sbu IS NULL THEN
          Fnd_File.PUT_LINE(fnd_File.OUTPUT,'Error SBU: Customer not found for location segment'||V_Error_Lmsg);
          END IF;

          IF ART_R3.CO IS NOT NULL AND ART_R3.CC IS NOT NULL AND
             ART_R3.LOCATION IS NOT NULL AND ART_R3.ACCOUNT IS NOT NULL THEN

            BEGIN
              V_Code_Combination_id := NULL;

                SELECT id_flex_num
                INTO   vn_structure_id
                FROM   FND_ID_FLEX_STRUCTURES_VL
                WHERE  UPPER(id_flex_structure_code) = 'ABRL_ACCOUNTING_FLEX'
                AND    id_flex_code                  = 'GL#' ;

              p_gl_id_rev :=  ART_R3.CO ||'.'|| ART_R3.CC ||'.'|| V_State_Sbu ||'.'|| ART_R3.LOCATION ||'.'|| ART_R3.Merchandise ||'.'|| ART_R3.ACCOUNT ||'.'|| ART_R3.Intercompany ||'.'|| ART_R3.Future;

              --Fnd_file.PUT_LINE(fnd_file.OUTPUT,'Generating CCID for :'||p_gl_id_rev);

                  x_gl_rev_acct_id := fnd_flex_ext.get_ccid (
                              application_short_name => 'SQLGL',
                              key_flex_code          => 'GL#',
                              structure_number       => vn_structure_id, -- 50308,
                              validation_date        => TO_CHAR(SYSDATE,'DD-MON-YYYY'),
                              concatenated_segments  => p_gl_id_rev );

              IF x_gl_rev_acct_id <=0 THEN
                 --Fnd_File.Put_Line(fnd_File.OUTPUT,'Error creating GL Revenue Acct:' ||fnd_flex_ext.get_message|| p_gl_id_rev);
                 V_Error_Lmsg := V_Error_Lmsg ||'Error creating GL Revenue Acct:' ||fnd_flex_ext.get_message||' '|| p_gl_id_rev;
              ELSE
                 --Fnd_File.Put_Line(fnd_File.OUTPUT,'Newly Generated CCID:' ||x_gl_rev_acct_id);
                  BEGIN
/*                      select segment1||'.'||segment2||'.'||segment3||'.'||segment4||'.'||segment5||'.'||segment6||'.'||segment7||'.'||segment8
                      into v_Conc_Segments
                      from
                      XXABRL_NAVI_RA_INT_DIST_ALL
                      where record_number = ART_R3.record_number;

                      Fnd_File.Put_Line(fnd_File.OUTPUT,'Concatenated Segments in Dist Staging:' ||v_Conc_Segments);

                      Update XXABRL_NAVI_RA_INT_DIST_ALL
                      set Code_Combination_Id = x_gl_rev_acct_id
                      where segment1||'.'||segment2||'.'||segment3||'.'||segment4||'.'||segment5||'.'||segment6||'.'||segment7||'.'||segment8 = ART_R3.record_number
                      --where int_dist.INTERFACE_LINE_ATTRIBUTE1 = ART_R3.INTERFACE_LINE_ATTRIBUTE1
                      --and int_dist.INTERFACE_LINE_ATTRIBUTE2 = ART_R3.INTERFACE_LINE_ATTRIBUTE2
                      ;
*/

                      --Fnd_File.Put_Line(fnd_File.OUTPUT,'Derived CCID :' ||x_gl_rev_acct_id);

                      /*INSERT into
                      XXABRL_AR_SEGMENTS_CCIDS
                      (
                      CONC_SEGMENTS,
                      CCID ,
                      RECORD_NUMBER)
                      Values
                      (
                      p_gl_id_rev,
                      x_gl_rev_acct_id,
                      ART_R3.record_number
                      );
                      Commit;

                      Update XXABRL_NAVI_RA_INT_DIST_ALL t1
                      set t1.Code_Combination_Id =
                      (select CCID
                      from XXABRL_AR_SEGMENTS_CCIDS t2
                      where t2.record_number = t1.record_number
                      and rownum =1
                      );*/
                      NULL;

                  EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                       Fnd_File.Put_Line(fnd_File.OUTPUT,'Concatenated Segments in Dist Staging Not Found');
                  WHEN OTHERS THEN
                       Fnd_File.Put_Line(fnd_File.OUTPUT,'Error while updating CCID for '||SQLERRM ||'>>>'|| p_gl_id_rev);
                  END;

              END IF;

                        -- ADDED BY SHAILESH ON 27-FEB-09 ADDING CODE_COMBINATION_ID LOGIN IN DISTRIBUTION

              SELECT Code_Combination_id
                INTO V_Code_Combination_id
                FROM GL_CODE_COMBINATIONS
               WHERE Segment1 = ART_R3.CO
                 AND Segment2 = ART_R3.CC
                 AND Segment3 = V_State_Sbu
                 AND Segment4 = ART_R3.LOCATION
                 AND Segment5 = ART_R3.Merchandise
                 AND Segment6 = ART_R3.ACCOUNT
                 AND Segment7 = ART_R3.Intercompany
                 AND Segment8 = ART_R3.Future;

            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                V_Error_Lmsg := V_Error_Lmsg ||
                                'ABRL_ACCOUNTING_FLEX does not exists-->>';
              WHEN TOO_MANY_ROWS THEN
                V_Error_Lmsg := V_Error_Lmsg ||
                                'Multiple ABRL_ACCOUNTING_FLEX exists-->>';
              WHEN OTHERS THEN
                V_Error_Lmsg := V_Error_Lmsg ||
                                'Exception in ABRL_ACCOUNTING_FLEX >>'||SQLERRM;
            END;
          END IF;
          --Updating the error message in table
          IF V_Error_Lmsg IS NOT NULL THEN
            UPDATE XXABRL_NAVI_RA_INT_DIST_ALL
               SET Error_message = V_Error_Lmsg, Interfaced_Flag = 'E'
             WHERE ROWID = ART_R3.ROWID;
             fnd_file.put_line(fnd_file.output,'Error in Dist: '||V_Error_Lmsg);
            COMMIT;
          ELSE
            UPDATE XXABRL_NAVI_RA_INT_DIST_ALL
               SET CODE_COMBINATION_ID = V_Code_Combination_id,
                   Org_Id              = P_Org_Id,
                   --                   Customer_Id              = V_Cust_Account_Id,
                   --                   Customer_Site_Id         = V_cust_site_id,
                   Interfaced_Flag = 'V'
             WHERE ROWID = ART_R3.ROWID;
             fnd_file.put_line(fnd_file.output,'Distribution Valid');
            COMMIT;
          END IF;
        END LOOP;

        IF V_Error_Hmsg IS NOT NULL THEN
          UPDATE XXABRL_NAVI_RA_INT_LINES_ALL
             SET Error_message = V_Error_Hmsg, Interfaced_Flag = 'E'
           WHERE ROWID = ART_R2.ROWID;
           fnd_file.put_line(fnd_file.output,'Error in Line: '||V_Error_Hmsg);
          COMMIT;
          fnd_file.put_line(fnd_file.output,
                            'Invoice Line Number       :' ||
                            ART_R2.INTERFACE_LINE_ATTRIBUTE2);
          fnd_file.put_line(fnd_file.output,
                            'Line Record Error         :' || V_Error_Hmsg);
          V_Error_Count := V_Error_Count + 1;
          IF V_Error_Lmsg IS NOT NULL THEN
            fnd_file.put_line(fnd_file.output,
                              'Distribution Record Error :' || V_Error_Lmsg);
          END IF;
        ELSIF V_Error_Hmsg IS NULL AND V_Error_Lmsg IS NOT NULL THEN
          UPDATE XXABRL_NAVI_RA_INT_LINES_ALL
             SET Error_message   = 'Error In Invoice Accounting Record '||V_Error_Lmsg,
                 Interfaced_Flag = 'E'
           WHERE ROWID = ART_R2.ROWID;
          COMMIT;
          V_Error_Count := V_Error_Count + 1;
          fnd_file.put_line(fnd_file.output,
                            'Invoice Line Number       :' ||
                            ART_R2.INTERFACE_LINE_ATTRIBUTE2);
          fnd_file.put_line(fnd_file.output,
                            'Distribution Error :' || V_Error_Lmsg);
        ELSE
          UPDATE XXABRL_NAVI_RA_INT_LINES_ALL
             SET NV_Customer_Id      = V_Cust_Account_Id,
                 NV_Cust_Site_Id = V_cust_site_id,
                 Org_id = P_Org_Id,
                 TERM_ID_NEW  = v_term_id,
                 CUST_TRX_TYPE_ID = V_CUST_TRX_TYPE_ID,
                 Interfaced_Flag = 'V'
           WHERE ROWID = ART_R2.ROWID;
           fnd_file.put_line(fnd_file.output,'Line Valid');
          COMMIT;
          v_OK_Rec_count := v_OK_Rec_count + 1;

        END IF;
      END LOOP;
        BEGIN
          V_Data_Count := 0;
          SELECT  COUNT(INTERFACE_LINE_ATTRIBUTE1)
          INTO    V_Data_Count
          FROM    XXABRL_NAVI_RA_INT_LINES_ALL
          WHERE   BATCH_SOURCE_NAME      = ART_R1.BATCH_SOURCE_NAME
          AND     ORG_CODE         = ART_R1.ORG_CODE
          AND   INTERFACE_LINE_CONTEXT     = ART_R1.INTERFACE_LINE_CONTEXT
          AND   INTERFACE_LINE_ATTRIBUTE1  = ART_R1.INTERFACE_LINE_ATTRIBUTE1
          AND   NV_CUSTOMER_ID     = ART_R1.NV_CUSTOMER_ID
          AND   NV_CUST_SITE_ID         = ART_R1.NV_CUST_SITE_ID
          AND   ERROR_MESSAGE      IS NOT NULL;
          IF V_Data_Count >0 THEN
            UPDATE  XXABRL_NAVI_RA_INT_LINES_ALL
            SET     ERROR_MESSAGE      = 'Error In Invoice Lines>>>' ||ERROR_MESSAGE
            WHERE   BATCH_SOURCE_NAME      = ART_R1.BATCH_SOURCE_NAME
            AND     ORG_CODE         = ART_R1.ORG_CODE
            AND   INTERFACE_LINE_CONTEXT     = ART_R1.INTERFACE_LINE_CONTEXT
            AND   INTERFACE_LINE_ATTRIBUTE1  = ART_R1.INTERFACE_LINE_ATTRIBUTE1
            AND   NV_CUSTOMER_ID     = ART_R1.NV_CUSTOMER_ID
            AND   NV_CUST_SITE_ID         = ART_R1.NV_CUST_SITE_ID
            AND   ERROR_MESSAGE      IS NOT NULL;
          END IF;
        EXCEPTION
          WHEN OTHERS THEN
            V_Data_Count:=0;
        END;

    END LOOP;
    fnd_file.put_line(fnd_file.output,
                      '........................................................................');
    fnd_file.put_line(fnd_file.output,
                      'Number of Valid Records    :' || v_OK_Rec_count);
    fnd_file.put_line(fnd_file.output,
                      'Number of Error Records  :' || V_Error_Count);
    fnd_file.put_line(fnd_file.output,
                      '........................................................................');
    IF V_Error_Count >0 THEN
       RetCode :=1;
    END IF;

    IF p_action ='N' AND V_Error_Count =0 THEN
       fnd_file.put_line(fnd_file.output,'--Inserting Data to Interface Tables--');
     --  fnd_file.put_line(fnd_file.output,'gl date: '||NVL(to_char(V_GL_DATE),'NULL'));
       --fnd_file.put_line(fnd_file.output,'RetCode: '||NVL(RetCode,'NULL'));
       fnd_file.put_line(fnd_file.output,'P_ORG_ID: ' ||NVL(TO_CHAR(P_Org_Id),'NULL'));
       fnd_file.put_line(fnd_file.output,'P_BATCH_Source: '||NVL(P_BATCH_Source,'NULL'));

-- ADDED BY SHAILESH 29 FEB 2009 GL_DATE PARAMETER
       INVOICE_INSERT(P_Org_Id, P_BATCH_Source,V_GL_DATE,RetCode);
    END IF;

  END INVOICE_VALIDATE;



    ----------------------------Amount Validation  added by Naresh on 19-dec-09 -----------------------------------------
  
  PROCEDURE    AMOUNT_VALIDATION ( P_ORG_CODE       In VARCHAR2,
                                   p_batch_source   IN   VARCHAR2,
                                   p_gl_date        IN     DATE
                                 )
    IS
       CURSOR c2 (
          cp_org_code            VARCHAR2,
          cp_batch_source_name   VARCHAR2,
          cp_gl_date             DATE
          
       )
       IS
          SELECT   nril.org_code, gl_date, customer_name
              FROM apps.xxabrl_navi_ra_int_lines_all nril
             WHERE NVL (interfaced_flag, 'N') in ('N','T')
               AND NVL (INTERFACE_LINE_CONTEXT, cp_batch_source_name) =
                                                              cp_batch_source_name
               AND NVL (org_code, cp_org_code) = cp_org_code
               AND gl_date = NVL (cp_gl_date, gl_date)
               --and gl_date =nvl(TO_CHAR (cp_gl_date, 'yyyy/mm/dd hh24:mi:ss'),gl_date)
          GROUP BY nril.org_code, gl_date, customer_name;

       v_line_amount   NUMBER := 0;
       v_dist_amount   NUMBER := 0;
             
          BEGIN
    
             
         
    
       FOR r2 IN c2 (P_ORG_CODE, p_batch_source, p_gl_date)
       LOOP
          EXIT WHEN c2%NOTFOUND;

            --V_LINE_AMOUNT NUMBER :=NULL;
            --V_DIST_AMOUNT NUMBER :=NULL;
          BEGIN
             SELECT SUM (NVL (nril.amount, 0))
               INTO v_line_amount
               FROM apps.xxabrl_navi_ra_int_lines_all nril
              WHERE nril.org_code = r2.org_code
                AND nril.gl_date = r2.gl_date
                AND nril.customer_name = r2.customer_name
                AND NRIL.INTERFACED_FLAG in ('N','T');
          EXCEPTION
             WHEN OTHERS
             THEN
                v_line_amount := 0;
          END;

          BEGIN
             SELECT SUM (NVL (TO_NUMBER (nrid.amount), 0))
               INTO v_dist_amount
               FROM apps.xxabrl_navi_ra_int_dist_all nrid
              WHERE (interface_line_attribute1, interface_line_attribute2) IN (
                       SELECT interface_line_attribute1,
                              interface_line_attribute2
                         FROM apps.xxabrl_navi_ra_int_lines_all nril
                        WHERE nril.org_code = r2.org_code
                          AND nril.gl_date = r2.gl_date
                          AND nril.customer_name = r2.customer_name)
                          AND nrid.interfaced_flag in ('N','T');
          EXCEPTION
             WHEN OTHERS
             THEN
                v_dist_amount := 0;
          END;

         

          IF v_line_amount <> v_dist_amount
          THEN
             UPDATE apps.xxabrl_navi_ra_int_lines_all nril
                SET nril.interfaced_flag = 'T'
              WHERE nril.org_code = r2.org_code
                AND nril.gl_date = r2.gl_date
                AND nril.customer_name = r2.customer_name
                AND nril.interfaced_flag in ('N','T') ;

             UPDATE apps.xxabrl_navi_ra_int_dist_all nrid
                SET nrid.interfaced_flag = 'T'
              WHERE (interface_line_attribute1, interface_line_attribute2) IN (
                       SELECT interface_line_attribute1,
                              interface_line_attribute2
                         FROM apps.xxabrl_navi_ra_int_lines_all nril
                        WHERE nril.org_code = r2.org_code
                          AND nril.gl_date = r2.gl_date
                          AND nril.customer_name = r2.customer_name)
                          AND nrid.interfaced_flag in ('N','T');

             
                      fnd_file.put_line (fnd_file.output,
                                   'Line Amount:'
                                || v_line_amount
                                || ' Dist Amount:'
                                || v_dist_amount
                                ||' does not match for this customer '
                                || r2.customer_name
                                || ' - '
                                || r2.gl_date
                               );
             
            fnd_file.put_line (fnd_file.output,' ');
            
            else
            
            UPDATE apps.xxabrl_navi_ra_int_lines_all nril
                SET nril.interfaced_flag = 'N'
              WHERE nril.org_code = r2.org_code
                AND nril.gl_date = r2.gl_date
                AND nril.customer_name = r2.customer_name
                AND nril.interfaced_flag in ('N','T') ;

             UPDATE apps.xxabrl_navi_ra_int_dist_all nrid
                SET nrid.interfaced_flag = 'N'
              WHERE (interface_line_attribute1, interface_line_attribute2) IN (
                       SELECT interface_line_attribute1,
                              interface_line_attribute2
                         FROM apps.xxabrl_navi_ra_int_lines_all nril
                        WHERE nril.org_code = r2.org_code
                          AND nril.gl_date = r2.gl_date
                          AND nril.customer_name = r2.customer_name)
                          AND nrid.interfaced_flag in ('N','T');
            
          END IF;

             COMMIT; 
             
       END LOOP;
    END amount_validation;

 ----------------------------End of Amount Validation--------------------------------------

  -- ADDED BY SHAILESH 29 FEB 2009 GL_DATE PARAMETER
  PROCEDURE INVOICE_INSERT(P_Org_Id IN NUMBER, P_BATCH_Source IN VARCHAR2,P_GL_DATE IN DATE ,x_ret_code OUT NUMBER) IS

/*
         updated by shailesh on 28-jan-09 in the 'where clause of cursor art_C1
         Reason - To load data only to specific OU and parallely we can load more than 1 OU at a time
       BATCH_SOURCE_NAME = P_BATCH_Source
         And ORG_CODE = P_Org_Id
*/


    -- ADDED BY SHAILESH 29 FEB 2009 GL_DATE PARAMETER
    CURSOR ART_C1(CP_ORG_ID NUMBER, CP_BATCH_SOURCE_NAME VARCHAR2,CP_GL_DATE DATE) IS
      SELECT ROWID,
             BATCH_SOURCE_NAME,
             ORG_ID,
             INTERFACE_LINE_CONTEXT,
             INTERFACE_LINE_ATTRIBUTE1 INTERFACE_LINE_ATTRIBUTE1,
             INTERFACE_LINE_ATTRIBUTE2 INTERFACE_LINE_ATTRIBUTE2,
             LINE_TYPE,
             TRX_DATE,
             GL_DATE,
             AMOUNT,
             CURRENCY_CODE,
             CONVERSION_TYPE,
             CONVERSION_RATE,
             CONVERSION_DATE,
             TRANSACTION_TYPE,
             DESCRIPTION,     --COMMENTS,
             CUST_TRX_TYPE_ID,
             NV_CUSTOMER_ID,
             NV_CUST_SITE_ID,
             ATTRIBUTE1,
             ATTRIBUTE_CATEGORY,
         -- added by shailesh on 19 sept 09 
         -- added 2 cols server name and statement number 
            statementno,
                   
             --CUSTOMER_ID,
             --CUSTOMER_SITE_ID,
             TERM_ID_NEW TERM_ID /*,
                   CUST_TRX_TYPE_ID*/
        FROM XXABRL_NAVI_RA_INT_LINES_ALL RNLV
       WHERE
       UPPER(INTERFACE_LINE_CONTEXT) = UPPER(CP_BATCH_SOURCE_NAME)
         AND ORG_ID = CP_ORG_ID
         AND NVL(INTERFACED_FLAG, 'N') = 'V'
         AND RNLV.GL_DATE = NVL(CP_GL_DATE,RNLV.GL_DATE)
          AND NOT EXISTS
      (SELECT INTERFACE_LINE_ATTRIBUTE1
                FROM XXABRL_NAVI_RA_INT_LINES_ALL RNLU
               WHERE RNLU.BATCH_SOURCE_NAME = RNLV.BATCH_SOURCE_NAME
                 AND RNLU.ORG_CODE = RNLV.ORG_CODE
                 AND RNLU.NV_CUSTOMER_ID = RNLV.NV_CUSTOMER_ID
                 AND RNLU.NV_CUST_SITE_ID = RNLV.NV_CUST_SITE_ID
                 AND RNLU.INTERFACE_LINE_ATTRIBUTE1 =
                     RNLV.INTERFACE_LINE_ATTRIBUTE1
                 AND NVL(RNLU.INTERFACED_FLAG, 'N') IN ('N', 'E'))
       ORDER BY /*ORG_CODE,*/ BATCH_SOURCE_NAME,
                INTERFACE_LINE_CONTEXT,
                INTERFACE_LINE_ATTRIBUTE1,
                INTERFACE_LINE_ATTRIBUTE2 /*,
                      CUSTOMER_ID,
                      CUSTOMER_SITE_ID*/
      ;

    CURSOR ART_C2(CP_BATCH_SOURCE_NAME VARCHAR2, CP_ORG_ID NUMBER, CP_INTERFACE_LINE_ATTRIBUTE1 VARCHAR2, CP_INTERFACE_LINE_ATTRIBUTE2 VARCHAR2, CP_CUSTOMER_ID NUMBER, CP_CUSTOMER_SITE_ID NUMBER) IS
      SELECT ROWID,
                   --BATCH_SOURCE_NAME,
                   --ORG_CODE,
                   INTERFACE_LINE_CONTEXT,
                   INTERFACE_LINE_ATTRIBUTE1 INTERFACE_LINE_ATTRIBUTE1,
                   INTERFACE_LINE_ATTRIBUTE2 INTERFACE_LINE_ATTRIBUTE2,
                   ACCOUNT_CLASS,
                   AMOUNT,
                   SEGMENT1 CO,
                   SEGMENT2 CC,
                   SEGMENT3 STATE_SBU,
                   SEGMENT4 LOCATION,
                   SEGMENT5 MERCHANDISE,
                   SEGMENT6 ACCOUNT,
                   SEGMENT7 INTERCOMPANY,
                   SEGMENT8 FUTURE,
                   ORG_ID,
                   CODE_COMBINATION_ID
        FROM XXABRL_NAVI_RA_INT_DIST_ALL
       WHERE /*BATCH_SOURCE_NAME = CP_BATCH_SOURCE_NAME
               And */
       ORG_ID = CP_ORG_ID AND
       INTERFACE_LINE_ATTRIBUTE1 = CP_INTERFACE_LINE_ATTRIBUTE1
       AND INTERFACE_LINE_ATTRIBUTE2 = CP_INTERFACE_LINE_ATTRIBUTE2
/*       And CUSTOMER_ID = CP_CUSTOMER_ID
               And CUSTOMER_SITE_ID = CP_CUSTOMER_SITE_ID
*/       AND
       NVL(INTERFACED_FLAG, 'N') IN ('V');


    v_set_of_bks_id NUMBER := Fnd_Profile.VALUE('GL_SET_OF_BKS_ID');
    v_user_id       NUMBER := FND_PROFILE.VALUE('USER_ID');
    v_resp_id       NUMBER := FND_PROFILE.VALUE('RESP_ID');
    v_appl_id       NUMBER := FND_PROFILE.VALUE('RESP_APPL_ID');
    v_req_id        NUMBER;
    v_record_count  NUMBER := 0;
    v_error_msg VARCHAR2(999);
    v_stmtno1 varchar2(30);
    v_stmtno2 varchar2(30);
       e_insert_int EXCEPTION;
  BEGIN

    --XXABRL_UPDATE_ARINVDIST_CCID; --External Procedure to update CCIDs

    fnd_file.put_line(fnd_file.output,
                      '........................................................................');
    fnd_file.put_line(fnd_file.output,
                      'Insert Records Into Interface Table');
    fnd_file.put_line(fnd_file.output,'........................................................................');
    --fnd_file.put_line(fnd_file.output,'P_ORG_ID: ' ||nvl(P_ORG_ID,'NULL'));
    --fnd_file.put_line(fnd_file.output,'P_BATCH_Source: '||nvl(P_BATCH_Source,'NULL'));



    FOR ART_R1 IN ART_C1(TO_CHAR(P_ORG_ID), P_BATCH_Source,P_GL_DATE) LOOP
      EXIT WHEN ART_C1%NOTFOUND;
      v_record_count := ART_C1%ROWCOUNT;

      fnd_file.put_line(fnd_file.output,' ');
      fnd_file.put_line(fnd_file.output,'------------------------------------------------------------------------');
      fnd_file.put_line(fnd_file.output,'Inserting AR Line: '||Trim(ART_R1.INTERFACE_LINE_ATTRIBUTE1)||' ( '||Trim(ART_R1.INTERFACE_LINE_ATTRIBUTE2)||' )');

      BEGIN

    
      
      INSERT INTO ra_interface_lines_all
        (--interface_line_id, --@Navisite
         interface_line_context,
         interface_line_attribute1,
         interface_line_attribute2,
         -- added by shailesh on 19 sept 09 
         -- added 2 cols server name and statement number 
       --  ATTRIBUTE_category,
         ATTRIBUTE3,
         batch_source_name,
         set_of_books_id,
         line_type,
         description,
         currency_code,
         amount,
         cust_trx_type_id,
         trx_date,
         gl_date,
         orig_system_batch_name,
         orig_system_bill_customer_ID,
         orig_system_bill_address_ID,
         conversion_type,
         conversion_date,
         conversion_rate,
         term_id,
         comments,
         attribute_category,
         attribute1,
         org_id,
         created_by,
         creation_date,
         last_updated_by,
         last_update_date)
      VALUES
        (--XXABRL_AR_CNV_S.NEXTVAL,
         Trim(ART_R1.INTERFACE_LINE_CONTEXT),
         Trim(ART_R1.INTERFACE_LINE_ATTRIBUTE1),
         Trim(ART_R1.INTERFACE_LINE_ATTRIBUTE2),
       --  'ABRL_NAVISION',
           trim(ART_R1.statementno),
         Trim(ART_R1.BATCH_SOURCE_NAME),
         v_set_of_bks_id,
         Trim(ART_R1.LINE_TYPE),
         Trim(ART_R1.DESCRIPTION),
         Trim(ART_R1.CURRENCY_CODE),
         Trim(ART_R1.AMOUNT),
         Trim(ART_R1.CUST_TRX_TYPE_ID),
         Trim(ART_R1.TRX_DATE),
         ART_R1.GL_DATE,
         Trim(ART_R1.BATCH_SOURCE_NAME) || SYSDATE,
         Trim(ART_R1.NV_CUSTOMER_ID),
         Trim(ART_R1.NV_CUST_SITE_ID),
         Trim(NVL(ART_R1.CONVERSION_TYPE, 'Corporate')),
         Trim(ART_R1.CONVERSION_DATE),
         Trim(ART_R1.CONVERSION_RATE),
         Trim(ART_R1.TERM_ID),
         NULL,                        --Trim(ART_R1.DESCRIPTION) ,Trim(ART_R1.COMMENTS),
         Trim(ART_R1.ATTRIBUTE_CATEGORY), --'E-RETAIL RECEIVABLES INVOICE'
         Trim(ART_R1.ATTRIBUTE1), --Tax
         Trim(ART_R1.ORG_ID),
         V_user_Id,
         SYSDATE,
         V_user_Id,
         SYSDATE);
      UPDATE XXABRL_NAVI_RA_INT_LINES_ALL
         SET INTERFACED_FLAG = 'Y', INTERFACED_DATE = SYSDATE
       WHERE ROWID = ART_R1.ROWID;

       fnd_file.put_line(fnd_file.output,'Line Inserted Successfully');

       EXCEPTION
       WHEN OTHERS THEN
       v_error_msg := 'Error-Rollback: Exception in Invoice Line insert to Interface: '||SQLERRM;
       RAISE e_insert_int;
       END;
      --Commit;

        fnd_file.put_line(fnd_file.output,'After 1 commit');

      FOR ART_R2 IN ART_C2(ART_R1.BATCH_SOURCE_NAME,
                           ART_R1.ORG_ID,
                           ART_R1.INTERFACE_LINE_ATTRIBUTE1,
                           ART_R1.INTERFACE_LINE_ATTRIBUTE2,
                           ART_R1.NV_CUSTOMER_ID,
                           ART_R1.NV_CUST_SITE_ID
                           ) LOOP

        EXIT WHEN ART_C2%NOTFOUND;

        fnd_file.put_line(fnd_file.output,'Inserting AR Line Distribution: '||Trim(ART_R1.INTERFACE_LINE_ATTRIBUTE1)||' ( '||Trim(ART_R1.INTERFACE_LINE_ATTRIBUTE2)||' )');

        BEGIN
          INSERT INTO ra_interface_distributions_all
            (interface_line_context,
             interface_line_attribute1,
             interface_line_attribute2,
             account_class,
             amount,
             -- ADDED BY SHAILESH ON 27 FEB 09 ADDITION OF CODE COMBINATION ID
             code_combination_id,
--             code_combination_id,
             org_id,
             created_by,
             creation_date,
             last_updated_by,
             last_update_date,
             PERCENT
          --   segment1,
          --   segment2,
          --   segment3,
          --   segment4,
          --   segment5,
         --    segment6,
         --    segment7,
        --     segment8
             )
          VALUES
            (Trim(ART_R2.INTERFACE_LINE_CONTEXT),
             Trim(ART_R2.INTERFACE_LINE_ATTRIBUTE1),
             Trim(ART_R2.INTERFACE_LINE_ATTRIBUTE2),
             Trim(ART_R2.ACCOUNT_CLASS),
             Trim(ART_R2.AMOUNT),
                        -- ADDED BY SHAILESH ON 27 FEB 09 ADDITION OF CODE COMBINATION ID
             Trim(ART_R2.CODE_COMBINATION_ID),
             --Trim(ART_R2.CODE_COMBINATION_ID),
             Trim(ART_R2.ORG_ID),
             V_user_Id,
             SYSDATE,
             V_user_Id,
             SYSDATE,
             100
           --  ART_R2.CO,
           --  ART_R2.CC,
           --  ART_R2.STATE_SBU,
          --   ART_R2.LOCATION,
          --   ART_R2.MERCHANDISE,
         --    ART_R2.ACCOUNT,
          --   ART_R2.INTERCOMPANY,
          --   ART_R2.FUTURE
             );
        --End If;
        UPDATE XXABRL_NAVI_RA_INT_DIST_ALL
           SET INTERFACED_FLAG = 'Y', INTERFACED_DATE = SYSDATE
         WHERE ROWID = ART_R2.ROWID;

         fnd_file.put_line(fnd_file.output,'AR Line Distribution Inserted Successfully');
       EXCEPTION
       WHEN OTHERS THEN
       v_error_msg := 'Error-Rollback: Exception in Invoice Line-Distribution insert: '||SQLERRM;
       RAISE e_insert_int;
       END;

        --- ADDED BY SHAILESH ON 5TH FEB 2009
        COMMIT;

      END LOOP;
    END LOOP;
    COMMIT;
    fnd_file.put_line(fnd_file.output,
                      'Number of Records (Invoices Lines/Dist )Inserted in Interface Table :' ||
                      v_record_count);
    fnd_file.put_line(fnd_file.output,
                      '........................................................................');
    -----
    -- Run Auto Invoice to upload data into Oracle Receivables
    ----
    /*If v_record_count > 0 Then
      fnd_global.apps_initialize(user_id      => v_user_id,
                                 resp_id      => v_resp_id,
                                 resp_appl_id => v_appl_id);
      Commit;
      v_req_id := fnd_request.submit_request('AR',
                                             'RAXTRX',
                                             'Autoinvoice Import Program' ||
                                             P_BATCH_Source,
                                             NULL,
                                             FALSE,
                                             'MAIN',
                                             'T',
                                             1021,
                                             P_Batch_Source, -- Parameter Data source
                                             Sysdate,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             'N',
                                             'Y',
                                             NULL,
                                             p_org_id,
                                             CHR(0));
      Commit;
      fnd_file.put_line(fnd_file.output,
                        'Please see the output of Payables OPEN Invoice Import program request id :' ||
                        v_req_id);
      fnd_file.put_line(fnd_file.output,
                        '........................................................................');
    End If;*/
  EXCEPTION
  WHEN e_insert_int THEN
       fnd_file.put_line(fnd_file.output,v_error_msg);
       fnd_file.put_line(fnd_file.output,'*** Insert process Rollbacked, No data inserted to Interface');
       ROLLBACK;
       x_ret_code :=1;
  END INVOICE_INSERT;

  FUNCTION ACCOUNT_SEG_STATUS(P_Seg_Value IN VARCHAR2,
                              P_Seg_Desc  IN VARCHAR2) RETURN VARCHAR2 IS
    V_Count NUMBER := 0;
  BEGIN
    SELECT COUNT(FFVV.Flex_Value)
      INTO V_Count
      FROM FND_FLEX_VALUES_VL FFVV, FND_FLEX_VALUE_SETS FFVS
     WHERE UPPER(FFVS.Flex_Value_Set_Name) = UPPER(P_Seg_Desc)
       AND FFVS.Flex_Value_Set_Id = FFVV.Flex_Value_Set_Id
       AND FFVV.Flex_Value = P_Seg_Value;
    IF V_Count = 1 THEN
      RETURN NULL;
    ELSE
      RETURN 'Invalid Value';
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN 'Invalid Value';
  END ACCOUNT_SEG_STATUS;
END XXABRL_NAV_AR_INV_IMP_PKG; 
/


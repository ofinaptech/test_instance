CREATE OR REPLACE PROCEDURE APPS.xxabrl_payment_advise_rep2 (
   errbuf    OUT   VARCHAR2,
   retcode   OUT   VARCHAR2
)
AS
   v_req_id                        NUMBER;
 --  v_transaction_value_date_from   DATE;
  -- v_transaction_value_date_to     DATE;
BEGIN
  /* BEGIN
      SELECT to_char(LAST_DAY (ADD_MONTHS (TRUNC (SYSDATE), -1)) + 1,'YYYY/MM/DD HH24:MI:SS')
        INTO v_transaction_value_date_from
        FROM DUAL;
   END;

   BEGIN
      SELECT to_char(TRUNC (SYSDATE) - 1, 'YYYY/MM/DD HH24:MI:SS') 
        INTO v_transaction_value_date_to
        FROM DUAL;
   END; */   

   v_req_id :=
      fnd_request.submit_request ('XXABRL',
                                  'XXABRL_PAYMENT_ADVISE_REP1',
                                  '',
                                  '',
                                  FALSE,
                                 to_char(LAST_DAY (ADD_MONTHS (TRUNC (SYSDATE), -1)) + 1,'YYYY/MM/DD HH24:MI:SS'),
                                   to_char(TRUNC (SYSDATE) - 1, 'YYYY/MM/DD HH24:MI:SS'),
                                  '',
                                  ''
                                 );
   COMMIT;
   DBMS_OUTPUT.put_line ('Request_id:' || v_req_id);
   DBMS_OUTPUT.put_line (SUBSTR ('Error ' || TO_CHAR (SQLCODE) || ': '
                                 || SQLERRM,
                                 1,
                                 255
                                )
                        );
END; 
/


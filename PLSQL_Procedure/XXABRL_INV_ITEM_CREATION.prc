CREATE OR REPLACE PROCEDURE APPS.XXABRL_INV_ITEM_CREATION (
   errbuf    OUT   VARCHAR2,
   retcode   OUT   NUMBER
)
IS
   error_flag           VARCHAR2 (1)    := 'N';
   xx_temp_id           NUMBER;
   xx_category_id       NUMBER;
   xx_temp_name         VARCHAR2 (100);
   xx_asset_cat_id      NUMBER;
   xx_uom               VARCHAR2 (100);
   xx_buyer_id          NUMBER;
   xx_expense_ccid      NUMBER;
   v_req_id             NUMBER;
   v_req_id1            NUMBER;
   xx_category_set_id   NUMBER;
   old_category_id      NUMBER;
   xx_item_no           VARCHAR2 (1000);
   le_load              EXCEPTION;
   lb_wait              BOOLEAN;
   lc_phase             VARCHAR2 (500);
   lc_status            VARCHAR2 (500);
   lc_dev_phase         VARCHAR2 (500);
   lc_dev_status        VARCHAR2 (500);
   lc_message           VARCHAR2 (2000);
   lc_error_flag        VARCHAR2 (1);
   gn_error             NUMBER          := 2;

   CURSOR xx_item
   IS
      SELECT ROWID, s.*
        FROM xxabrl.xxabrl_item_creation s
       WHERE 1 = 1 AND (status IS NULL or status='E');

   -- and item_code='70015';         --AND item_code = '100000041';
   CURSOR xx_item_catagory_assign
   IS
      SELECT ROWID, s.*
        FROM xxabrl.xxabrl_item_creation s
       WHERE 1 = 1 AND status = 'Y' AND cat_assign_status IS NULL;
BEGIN
--
   FOR xx_main IN xx_item
   LOOP
      fnd_file.put_line (fnd_file.LOG, 'item : ' || xx_main.item_code);

      -- checkimg template
      BEGIN
         SELECT template_id, template_name
           INTO xx_temp_id, xx_temp_name
           FROM mtl_item_templates
          WHERE template_name = xx_main.TEMPLATE;
      EXCEPTION
         WHEN OTHERS
         THEN
            error_flag := 'Y';
      END;

      fnd_file.put_line (fnd_file.LOG, 'template_name : ' || xx_temp_name);

      IF UPPER (xx_temp_name) = 'ABRL CAPEX'
      THEN
         BEGIN
            SELECT category_id
              INTO xx_asset_cat_id
              FROM fa_categories_b
             WHERE segment1 = xx_main.major_category
               AND segment2 = xx_main.minor_category1
               AND segment3 = xx_main.minor_category2;
         EXCEPTION
            WHEN OTHERS
            THEN
               error_flag := 'Y';
         END;
      END IF;

-- checking  unit_of_measure
      BEGIN
         SELECT unit_of_measure
           INTO xx_uom
           FROM mtl_units_of_measure_vl
          WHERE UPPER (uom_code) = UPPER (xx_main.uom);
      EXCEPTION
         WHEN OTHERS
         THEN
            NULL;
      END;

      fnd_file.put_line (fnd_file.LOG, 'unit_of_measure: ' || xx_uom);

-- checking buyer name
      BEGIN
         SELECT agent_id
           INTO xx_buyer_id
           FROM po_agents_v
          WHERE agent_name = xx_main.buyer_name;
      EXCEPTION
         WHEN OTHERS
         THEN
            NULL;
      END;

      -- cheecking gl_code
      BEGIN
         SELECT code_combination_id
           INTO xx_expense_ccid
           FROM gl_code_combinations
          WHERE code_combination_id = xx_main.gl_code;
      EXCEPTION
         WHEN OTHERS
         THEN
            error_flag := 'Y';
      END;

      fnd_file.put_line (fnd_file.LOG,
                         'code_combination_id: ' || xx_expense_ccid
                        );

      IF error_flag = 'N'
      THEN
         fnd_file.put_line (fnd_file.LOG,
                            'inserting mtl_system_items_interface'
                           );

         INSERT INTO mtl_system_items_interface
                     (process_flag, set_process_id, transaction_type,
                      organization_id, segment1, description, template_id,
                      asset_category_id, list_price_per_unit,
                      primary_unit_of_measure, buyer_id,
                      expense_account, creation_date
                     )
              VALUES (1, 1, 'CREATE',
                      97, xx_main.item_code, xx_main.description, xx_temp_id,
                      NVL (xx_asset_cat_id, NULL), xx_main.price,
                      xx_uom, NVL (xx_buyer_id, NULL),
                      NVL (xx_expense_ccid, NULL), SYSDATE
                     );

         UPDATE xxabrl.xxabrl_item_creation
            SET status = 'Y'
          WHERE ROWID = xx_main.ROWID;

         COMMIT;
      ELSE
         fnd_file.put_line (fnd_file.LOG,
                            'error in item : ' || xx_main.item_code
                           );
         fnd_file.put_line (fnd_file.LOG,
                            'error code 1: ' || SQLCODE || ':' || SQLERRM
                           );

         UPDATE xxabrl.xxabrl_item_creation
            SET status = 'E'
          WHERE ROWID = xx_main.ROWID;

         COMMIT;
      END IF;

      error_flag := 'N';
      fnd_file.put_line
         (fnd_file.LOG,
          '-----------------------------------------------------------------------------------'
         );
   END LOOP;

   fnd_file.put_line (fnd_file.LOG, 'Import Items Program started');
--   97, 1, 1, 1, 1, , 1, 1
   v_req_id :=
      fnd_request.submit_request ('INV',
                                  'INCOIN',
                                  '',
                                  '',
                                  FALSE,
                                  97,
                                  1,
                                  1,
                                  1,
                                  1,
                                  NULL,
                                  1,
                                  1
                                 );
   COMMIT;

   IF (v_req_id = 0)
   THEN
      RAISE le_load;
   ELSE
      fnd_file.put_line
                       (fnd_file.LOG,
                           'Submitted Import Items Program with request id :'
                        || TO_CHAR (v_req_id)
                       );
   END IF;

   -- +===================================================================+
-- Waiting for the conccurrent request to get completed
-- +===================================================================+
   lb_wait :=
      fnd_concurrent.wait_for_request (request_id      => v_req_id,
                                       INTERVAL        => 10,
                                       max_wait        => 0,
                                       phase           => lc_phase,
                                       status          => lc_status,
                                       dev_phase       => lc_dev_phase,
                                       dev_status      => lc_dev_status,
                                       MESSAGE         => lc_message
                                      );
   -- Do not remove this commit, concurrent program will not start without this
   COMMIT;

    -- +===================================================================+
-- Checking  item import Program status
-- +===================================================================+
   IF    (    (   UPPER (lc_dev_status) = 'WARNING'
               OR UPPER (lc_dev_status) = 'ERROR'
              )
          AND UPPER (lc_dev_phase) = 'COMPLETE'
         )
      OR (    (UPPER (lc_status) = 'WARNING' OR UPPER (lc_status) = 'ERROR')
          AND UPPER (lc_phase) = 'COMPLETED'
         )
   THEN
      lc_error_flag := 'Y';
   END IF;

   IF lc_error_flag = 'Y'
   THEN
      RAISE le_load;
   ELSE
      BEGIN
         FOR xx_cat IN xx_item_catagory_assign
         LOOP
            -- checking MAJOR_CATEGORY_MINOR_CATEGORY
            BEGIN
               SELECT category_id
                 INTO xx_category_id
                 FROM mtl_categories_b_kfv
                WHERE UPPER (concatenated_segments) =
                                  UPPER (xx_cat.major_category_minor_category);
            EXCEPTION
               WHEN OTHERS
               THEN
                  NULL;
            END;

            SELECT segment1, cat.category_set_id, cat.category_id
              INTO xx_item_no, xx_category_set_id, old_category_id
              FROM mtl_system_items mtl, mtl_item_categories cat
             WHERE mtl.inventory_item_id = cat.inventory_item_id
               AND mtl.organization_id = cat.organization_id
               AND mtl.organization_id = 97          -- this should not change
               AND mtl.segment1 = xx_cat.item_code;

            INSERT INTO mtl_item_categories_interface
                        (category_set_id, category_id,
                         last_update_date, last_updated_by, creation_date,
                         created_by,
                                    --LAST_UPDATE_LOGIN
                                    organization_id, process_flag,
                         item_number, old_category_id, transaction_type,
                         set_process_id
                        )
                 VALUES (xx_category_set_id, NVL (xx_category_id, NULL),
                         SYSDATE, 4191, SYSDATE,
                         4191,
                              --4191      ,
                         97, 1,
                         xx_item_no, old_category_id, 'UPDATE',
                         420
                        );

            UPDATE xxabrl.xxabrl_item_creation
               SET cat_assign_status = 'Y'
             WHERE ROWID = xx_cat.ROWID;

            COMMIT;
         END LOOP;

         fnd_file.put_line
                    (fnd_file.LOG,
                     'Item Category Assignment Open Interface Program started'
                    );
         v_req_id1 :=
            fnd_request.submit_request ('INV',
                                        'INV_ITEM_CAT_ASSIGN_OI',
                                        '',
                                        '',
                                        FALSE,
                                        420,
                                        1,
                                        1
                                       );
         COMMIT;
         fnd_file.put_line
            (fnd_file.LOG,
                'Submitted Item Category Assignment Open Interface Program with request id :'
             || TO_CHAR (v_req_id1)
            );
         fnd_file.put_line
            (fnd_file.LOG,
             '-----------------------------------------------------------------------------------'
            );
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (fnd_file.LOG,
                                  'error in mtl_item_categories_interface : '
                               || SQLCODE
                               || ':'
                               || SQLERRM
                              );
            ROLLBACK;
      END;
   END IF;

   COMMIT;
EXCEPTION
   WHEN le_load
   THEN
      retcode := gn_error;
      errbuf := 'Import Items Program completed with error';
   WHEN OTHERS
   THEN
      fnd_file.put_line (fnd_file.LOG,
                         'error code 2: ' || SQLCODE || ':' || SQLERRM
                        );
      ROLLBACK;
END; 
/


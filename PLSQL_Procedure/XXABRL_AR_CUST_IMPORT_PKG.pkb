CREATE OR REPLACE PACKAGE BODY APPS.XXABRL_AR_CUST_IMPORT_PKG AS


--	gc_created_by_module	VARCHAR2(50):= 'XXABRL_TCA'; 
	gc_created_by_module	VARCHAR2(50):= 'HZ_CPUI'; 
	gc_application_id	NUMBER := FND_PROFILE.VALUE('RESP_APPL_ID');
	gn_global_request_id	NUMBER := NVL(fnd_global.conc_request_id,1);
	gn_user_id		NUMBER := NVL(fnd_global.user_id,1);
	gn_org_id		NUMBER := fnd_profile.VALUE('ORG_ID');
	gn_request_id		NUMBER := fnd_global.conc_request_id;
	gc_debug_flag		VARCHAR2(1);

PROCEDURE printlog(MSG_TEXT IN VARCHAR2, MSG_FLAG VARCHAR2)
IS
BEGIN
if MSG_FLAG = 'Y' then
	FND_FILE.PUT_LINE (FND_FILE.LOG,MSG_TEXT);
	NULL;
End if;

END;

PROCEDURE create_customer_account(
 p_customer        IN XXABRL_AR_CUSTOMER_INT%ROWTYPE
,x_party_id        OUT NUMBER
,x_cust_account_id OUT NUMBER
,x_cust_status     OUT VARCHAR2
,x_error_msg       OUT VARCHAR2
) 
IS
    lc_cust_account_rec     hz_cust_account_v2pub.cust_account_rec_type;
    lc_organization_rec     hz_party_v2pub.organization_rec_type;
    lc_customer_profile_rec hz_customer_profile_v2pub.customer_profile_rec_type;
    lc_create_person_rec    hz_party_v2pub.person_rec_type;

    lc_return_status    VARCHAR2(10);
    lc_msg_data         VARCHAR2(2000);
    lc_customer_flag    VARCHAR2(6);
    ln_cust_acct_count  NUMBER;
    ln_msg_count        NUMBER;
    ln_msg_index_out    NUMBER;
    ln_account_number   NUMBER;
    ln_party_number     NUMBER;
    ln_profile_id       NUMBER;
    lc_generate_cust_no VARCHAR2(5);
    lc_tax_status	VARCHAR2(1);

    E_EXIT		EXCEPTION;
BEGIN

	printlog('Check whether the customer account exist for the given customer name and customer account name' ,gc_debug_flag);
	printlog('OrigSysAddressRef#: '||p_customer.orig_system_address_ref || ' ,CustomerName: '||p_customer.customer_name ,gc_debug_flag);

	BEGIN
		SELECT  hca.cust_account_id
		       ,hca.party_id
		INTO    x_cust_account_id
		       ,x_party_id
		FROM    HZ_CUST_ACCOUNTS HCA
		       ,HZ_PARTIES HP
		WHERE   hp.party_id   = hca.party_id
		AND     hp.party_name = NVL(p_customer.customer_name, p_customer.PERSON_FIRST_NAME || ' ' || p_customer.PERSON_LAST_NAME)
		AND     NVL(hca.orig_system_reference, 'X') =  NVL(p_customer.orig_system_customer_ref, 'X');

		printlog('Account already exists; x_cust_account_id = ' ||x_cust_account_id|| ' ,x_party_id = '||x_party_id ,gc_debug_flag);
		lc_customer_flag := 'UPDATE';
		x_cust_status    := '0'; --TRUE;

		UPDATE XXABRL_AR_CUSTOMER_INT
		SET    new_party_id        = x_party_id
		      ,new_cust_account_id = x_cust_account_id
		WHERE  orig_system_customer_ref = p_customer.orig_system_customer_ref 
		AND    record_number            = p_customer.record_number;

	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			lc_customer_flag := 'NEW';
			printlog('Create a new customer account'  ,gc_debug_flag);

		WHEN OTHERS THEN
			lc_customer_flag := 'ERROR';
			x_error_msg := 'This account exists in Oracle multiple times! Exit Account creation' || SQLERRM;
			RAISE E_EXIT;
	END;			

	IF lc_customer_flag = 'NEW' THEN
      ---------------------------------------------------------
      -- Mandatory parameter: account_number
      -- if GENERATE_CUSTOMER_NUMBER of AR_SYSTEM_PARAMTERS is ON
      -- not required to pass the value.
      -----------------------------------------------------------

		SELECT generate_customer_number
		INTO lc_generate_cust_no
		FROM apps.ar_system_parameters
		WHERE ROWNUM =1;

		printlog('lc_generate_cust_no = '||lc_generate_cust_no  ,gc_debug_flag);

		IF lc_generate_cust_no = 'Y' THEN
			lc_cust_account_rec.account_number := NULL;
		ELSE
			lc_cust_account_rec.account_number := p_customer.customer_number;
		END IF; --by H@N$
		printlog('Acct# = '||lc_cust_account_rec.account_number  ,gc_debug_flag);      

      --*** For MSE - CRP1 it will be from extract file, later manual, as per mail

		SELECT decode(upper(p_customer.customer_type),
			    'EXTERNAL',
			    'R',
			    'INTERNAL',
			    'I',
			    NULL)
		INTO lc_cust_account_rec.customer_type
		FROM dual;
      --External : R, Internal : I

		printlog('Customer TYpe: '||p_customer.customer_type,gc_debug_flag);      

		lc_cust_account_rec.account_name          := p_customer.customer_name; 
		lc_cust_account_rec.customer_class_code   := p_customer.customer_class_code;
		lc_cust_account_rec.orig_system_reference := p_customer.orig_system_customer_ref;

		lc_cust_account_rec.attribute1 := p_customer.residential;
		lc_cust_account_rec.attribute2 := p_customer.referred_by;
		lc_cust_account_rec.attribute3 := p_customer.welcome_packet;
		lc_cust_account_rec.attribute4 := p_customer.derived_tc_sent; 
		lc_cust_account_rec.attribute5 := p_customer.no_paper_mail;
		lc_cust_account_rec.attribute6 := p_customer.no_email;
		lc_cust_account_rec.attribute7 := p_customer.derived_business_type;

		lc_cust_account_rec.created_by_module := gc_created_by_module;

		lc_organization_rec.organization_name := p_customer.customer_name;
		lc_organization_rec.created_by_module := gc_created_by_module;
		
		/*
		IF (p_customer.cust_tax_reference IS NOT NULL) AND (UPPER(p_customer.country) = 'CANADA')  THEN --added this logic on 13-JUN-2008
			lc_organization_rec.tax_reference := p_customer.cust_tax_reference;
		END IF;
		*/

		lc_customer_profile_rec.override_terms := 'Y';--required for AR invoices
		lc_customer_profile_rec.created_by_module := gc_created_by_module;
		lc_customer_profile_rec.application_id    := gc_application_id;

		lc_create_person_rec.person_first_name := p_customer.person_first_name;
		lc_create_person_rec.person_last_name  := p_customer.person_last_name;
		lc_create_person_rec.created_by_module := gc_created_by_module;
		lc_create_person_rec.application_id    := gc_application_id;

		IF p_customer.person_flag = 'Y' THEN
			printlog('Create Person type Party',gc_debug_flag);      
			HZ_CUST_ACCOUNT_V2PUB.create_cust_account(p_init_msg_list    => 'T',
								  p_cust_account_rec => lc_cust_account_rec,
								  p_person_rec       => lc_create_person_rec,
								  /*p_organization_rec     => lc_organization_rec,*/
								  p_customer_profile_rec => lc_customer_profile_rec,
								  p_create_profile_amt   => 'T',
								  x_cust_account_id      => x_cust_account_id,
								  x_account_number       => ln_account_number,
								  x_party_id             => x_party_id,
								  x_party_number         => ln_party_number,
								  x_profile_id           => ln_profile_id,
								  x_return_status        => lc_return_status,
								  x_msg_count            => ln_msg_count,
								  x_msg_data             => lc_msg_data);
		ELSE

		printlog('Create organization type party',gc_debug_flag);  
		HZ_CUST_ACCOUNT_V2PUB.create_cust_account(p_init_msg_list    => 'T',
							  p_cust_account_rec => lc_cust_account_rec,
							  /*p_person_rec       => lc_create_person_rec,*/
							  p_organization_rec     => lc_organization_rec,
							  p_customer_profile_rec => lc_customer_profile_rec,
							  p_create_profile_amt   => 'T',
							  x_cust_account_id      => x_cust_account_id,
							  x_account_number       => ln_account_number,
							  x_party_id             => x_party_id,
							  x_party_number         => ln_party_number,
							  x_profile_id           => ln_profile_id,
							  x_return_status        => lc_return_status,
							  x_msg_count            => ln_msg_count,
							  x_msg_data             => lc_msg_data);
		END IF;

		IF (lc_return_status <> 'S') THEN
			fnd_msg_pub.get(p_msg_index     => FND_MSG_PUB.G_LAST,
					p_encoded       => 'F',
					p_data          => lc_msg_data,
					p_msg_index_out => ln_msg_index_out);

			x_error_msg := 'API Error in create_cust_account: ' || p_customer.customer_name || '  ' || SUBSTR(lc_msg_data, 1, 200);
			x_cust_status := '1'; --FALSE;

		ELSIF (lc_return_status = 'S') THEN

			printlog('Customer:' || p_customer.customer_name || ' ' || ' created Successfully; cust_account_id = ' ||x_cust_account_id|| ' , party_id = '||x_party_id ,gc_debug_flag);
			x_cust_status     := '0'; --TRUE;

			UPDATE XXABRL_AR_CUSTOMER_INT
			SET    new_party_id        = x_party_id
			      ,new_cust_account_id = x_cust_account_id
			WHERE  orig_system_customer_ref = p_customer.orig_system_customer_ref 
			AND    record_number            = p_customer.record_number;

		END IF;

	ELSE
		NULL;      --Update the existing Customer / customer already exists
	END IF; --New Create / Update
	
	/*
	IF (x_cust_status = '0') AND (x_error_msg IS NULL) 
	     AND (p_customer.cust_tax_reference IS NOT NULL) AND (UPPER(p_customer.country) = 'UNITED STATES' ) THEN ----added this logic on 13-JUN-2008
		printlog('Call set_party_tax_profile for UNITED STATES',gc_debug_flag);
		set_party_tax_profile(
			 p_customer  => p_customer
			,p_party_id  => x_party_id
			,x_status    => lc_tax_status
			,x_error_msg => x_error_msg);

		x_cust_status := lc_tax_status;
	END IF;
	*/

EXCEPTION
	WHEN E_EXIT THEN
		printlog(x_error_msg, gc_debug_flag);
		x_cust_status := '1'; --FALSE

	WHEN OTHERS THEN
		x_error_msg := 'Unexpected Error in create_customer_account:' || SQLERRM;
		printlog(x_error_msg, gc_debug_flag);
		x_cust_status := '1'; --FALSE

END create_customer_account;


PROCEDURE create_customer_location(
 p_customer    IN XXABRL_AR_CUSTOMER_INT%ROWTYPE
,p_party_id    IN NUMBER
,x_location_id OUT NUMBER
,x_loc_status  OUT VARCHAR2
,x_error_msg   OUT VARCHAR2
) 
IS
	lc_location_rec  hz_location_v2pub.location_rec_type;
	lc_return_status VARCHAR2(10);
	lc_country       VARCHAR2(60);
	lc_site_flag     VARCHAR2(6);
	lc_msg_data      VARCHAR2(2000);
	lc_county        VARCHAR2(60);

	ln_msg_count     NUMBER;
	ln_msg_index_out NUMBER;

	ln_loc_count     NUMBER;
	E_EXIT		EXCEPTION;
BEGIN
	/*
	BEGIN

	
		SELECT county
		INTO   lc_county
		FROM   XXABRL_AR_CUST_COUNTY_INT
		WHERE  zip_code = p_customer.postal_code
		AND    ROWNUM   = 1;
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			lc_county := NULL;
		WHEN OTHERS THEN
			x_error_msg := 'Error in selecting county from staging table, exit site creation : ' || SQLERRM;
			RAISE E_EXIT;
	END;
	*/

	BEGIN
		SELECT location_id
		INTO   x_location_id
		FROM   HZ_LOCATIONS
		WHERE  TRIM(address1)              = p_customer.address1
		AND    TRIM(NVL(address2, 'X'))    = NVL(p_customer.address2, 'X')
		AND    TRIM(NVL(address3, 'X'))    = NVL(p_customer.address3, 'X')
		AND    TRIM(NVL(city, 'X'))        = NVL(p_customer.city, 'X')
		AND    TRIM(NVL(state, 'X'))       = NVL(p_customer.state, 'X')
		AND    TRIM(NVL(COUNTY, 'X'))      = NVL(NVL(p_customer.county,lc_county), 'X')
				--county logic changed after discussing with Tarun on 11-JUN-2008
		AND    TRIM(NVL(postal_code, 'X')) = NVL(p_customer.postal_code, 'X')
		AND    TRIM(orig_system_reference) = p_customer.orig_system_address_ref;

		printlog('Location already exists; location_id = ' || x_location_id,gc_debug_flag);
		lc_site_flag := 'UPDATE';
		x_loc_status := '0'; --TRUE;

		UPDATE XXABRL_AR_CUSTOMER_INT
		SET    new_location_id          = x_location_id
		WHERE  orig_system_customer_ref = p_customer.orig_system_customer_ref 
		AND    record_number            = p_customer.record_number;

	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			lc_site_flag := 'NEW';
			printlog('Create a new location'  ,gc_debug_flag);

		WHEN OTHERS THEN
			lc_site_flag := 'ERROR';
			x_error_msg := 'This location exists in Oracle multiple times! Exit Site creation' || SQLERRM;
			RAISE E_EXIT;
			
	END;

        IF lc_site_flag = 'NEW' THEN

		lc_location_rec.address1          := p_customer.address1; -- mandatory
		lc_location_rec.address2          := p_customer.address2;
		lc_location_rec.address3          := p_customer.address3;
		lc_location_rec.address4          := p_customer.address4;
		lc_location_rec.county            := NVL(p_customer.county,lc_county);  -- discussed with Hansraj; 
		--county logic changed after discussing with Tarun on 11-JUN-2008
		lc_location_rec.country           := p_customer.country_code; -- mandatory
		lc_location_rec.city              := p_customer.city;
		lc_location_rec.state             := p_customer.state;
		lc_location_rec.postal_code       := p_customer.postal_code;
		lc_location_rec.created_by_module := gc_created_by_module;
		lc_location_rec.application_id    := gc_application_id;
		lc_location_rec.province          := p_customer.province;
		lc_location_rec.orig_system_reference := p_customer.orig_system_address_ref; --added by H@N$  on 06-aug-07

-- Commented as per discussion with Tarun after UAT2 load; 25-JUN-2008
/*		IF UPPER(p_customer.country) = 'CANADA' THEN
			lc_location_rec.province := p_customer.state;
		END IF;        --As per mail*/

		HZ_LOCATION_V2PUB.create_location(p_init_msg_list => 'T',
					    p_location_rec  => lc_location_rec,
					    x_location_id   => x_location_id,
					    x_return_status => lc_return_status,
					    x_msg_count     => ln_msg_count,
					    x_msg_data      => lc_msg_data);
		--
		IF (lc_return_status <> 'S') THEN
			fnd_msg_pub.get(p_msg_index     => FND_MSG_PUB.G_LAST,
				    p_encoded       => 'F',
				    p_data          => lc_msg_data,
				    p_msg_index_out => ln_msg_index_out);

			x_error_msg := 'API Error in create_location: ' || SUBSTR(lc_msg_data, 1, 200); 

			x_loc_status := '1'; --FALSE
		ELSE
			printlog('create location successful; location_id : ' || x_location_id,gc_debug_flag);
			x_loc_status  := '0'; -- TRUE;

			UPDATE XXABRL_AR_CUSTOMER_INT
			SET    new_location_id          = x_location_id
			WHERE  orig_system_customer_ref = p_customer.orig_system_customer_ref 
			AND    record_number            = p_customer.record_number;
		END IF;
	ELSE
		NULL; --location already exists
        END IF;

EXCEPTION
	WHEN E_EXIT THEN
		printlog(x_error_msg, gc_debug_flag);
		x_loc_status := '1'; --FALSE
	WHEN OTHERS THEN
		x_error_msg := 'Unknown Error in create_customer_location:' || SQLERRM;
		printlog( x_error_msg, gc_debug_flag);
		x_loc_status := '1';--FALSE
END create_customer_location;


PROCEDURE create_party_site(
 p_customer          IN XXABRL_AR_CUSTOMER_INT%ROWTYPE
,p_party_id          IN NUMBER
,p_location_id       IN NUMBER
,x_party_site_id     OUT NUMBER
,x_party_site_status OUT VARCHAR2
,x_error_msg         OUT VARCHAR2
) 
IS
	lc_party_site_rec hz_party_site_v2pub.party_site_rec_type;

	ln_msg_count         NUMBER;
	ln_msg_index_out     NUMBER;
	ln_party_site_number NUMBER;
	ln_party_site_count  NUMBER;

	lc_return_status   VARCHAR2(10);
	lc_msg_data        VARCHAR2(2000);
	lc_party_site_flag VARCHAR2(6);
	E_EXIT		   EXCEPTION;

BEGIN
	BEGIN
		SELECT party_site_id
		INTO   x_party_site_id
		FROM   HZ_PARTY_SITES
		WHERE  party_id = p_party_id
		AND    location_id = p_location_id;

		printlog('Party Site already exists; party_site_id = ' || x_party_site_id, gc_debug_flag);
		lc_party_site_flag := 'UPDATE';
		x_party_site_status := '0'; --TRUE;

		UPDATE XXABRL_AR_CUSTOMER_INT
		SET    new_party_site_id        = x_party_site_id
		WHERE  orig_system_customer_ref = p_customer.orig_system_customer_ref 
		AND    record_number            = p_customer.record_number;

	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			lc_party_site_flag := 'NEW';
			printlog('Create a new Party Site' , gc_debug_flag);

		WHEN OTHERS THEN
			lc_party_site_flag := 'ERROR';
			x_error_msg := 'This party site exists in Oracle multiple times! Exit site creation' || SQLERRM;
			RAISE E_EXIT;
			
	END;

	IF lc_party_site_flag = 'NEW' THEN

		lc_party_site_rec.party_id    := p_party_id; 
		lc_party_site_rec.location_id := p_location_id; 
		lc_party_site_rec.addressee   := p_customer.addressee;

		lc_party_site_rec.party_site_name       := p_customer.site_name;
		lc_party_site_rec.created_by_module     := gc_created_by_module;
		lc_party_site_rec.application_id        := gc_application_id;
		--lc_party_site_rec.orig_system_reference := p_customer.orig_system_customer_ref);

		HZ_PARTY_SITE_V2PUB.create_party_site(p_init_msg_list     => 'T',
						    p_party_site_rec    => lc_party_site_rec,
						    x_party_site_id     => x_party_site_id,
						    x_party_site_number => ln_party_site_number,
						    x_return_status     => lc_return_status,
						    x_msg_count         => ln_msg_count,
						    x_msg_data          => lc_msg_data);

		IF (lc_return_status <> 'S') THEN
			fnd_msg_pub.get(p_msg_index     => FND_MSG_PUB.G_LAST,
					p_encoded       => 'F',
					p_data          => lc_msg_data,
					p_msg_index_out => ln_msg_index_out);

			x_error_msg := 'API Error in create_party_site: ' || SUBSTR(lc_msg_data, 1, 200); 

			x_party_site_status := '1'; --FALSE;
		ELSE
			x_party_site_status := '0'; --TRUE;
			printlog('Created party site id: ' || x_party_site_id, gc_debug_flag);

			UPDATE XXABRL_AR_CUSTOMER_INT
			SET    new_party_site_id        = x_party_site_id
			WHERE  orig_system_customer_ref = p_customer.orig_system_customer_ref 
			AND    record_number            = p_customer.record_number;

		END IF;
    ELSE
	NULL; --party site already exists
    END IF;
EXCEPTION
	WHEN E_EXIT THEN

		printlog(x_error_msg, gc_debug_flag);
		x_party_site_status := '1'; --FALSE

	WHEN OTHERS THEN

		x_error_msg := 'Unexpected Error in create_party_site: ' || SQLERRM;
		printlog(x_error_msg, gc_debug_flag);
		x_party_site_status := '1'; --FALSE

END create_party_site;

PROCEDURE create_customer_acct_site ( 
 p_customer          IN XXABRL_AR_CUSTOMER_INT%ROWTYPE
,p_cust_account_id   IN NUMBER
,p_party_site_id     IN NUMBER
,x_cust_acct_site_id OUT NUMBER
,x_cust_site_status  OUT VARCHAR2
,x_error_msg         OUT VARCHAR2
) 
IS
	lc_cust_acct_site_rec hz_cust_account_site_v2pub.cust_acct_site_rec_type;
	lc_return_status      VARCHAR2(10);
	ln_msg_count          NUMBER;
	lc_msg_data           VARCHAR2(2000);
	ln_msg_index_out      NUMBER;
	ln_cust_site_count    NUMBER;
	lc_cust_site_flag     VARCHAR2(10);
	E_EXIT		      EXCEPTION;
BEGIN

	BEGIN
		SELECT cust_acct_site_id
	        INTO   x_cust_acct_site_id
		FROM   HZ_CUST_ACCT_SITES_ALL
		WHERE  cust_account_id = p_cust_account_id
		AND    party_site_id   = p_party_site_id
		AND    org_id          = p_customer.org_id;

		printlog('CustAcctSite already exists as cust_acct_site_id = ' || x_cust_acct_site_id, gc_debug_flag);
		lc_cust_site_flag  := 'UPDATE';
		x_cust_site_status := '0'; 

		UPDATE XXABRL_AR_CUSTOMER_INT
		SET    new_cust_acct_site_id    = x_cust_acct_site_id
		WHERE  orig_system_customer_ref = p_customer.orig_system_customer_ref 
		AND    record_number            = p_customer.record_number;

	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			lc_cust_site_flag := 'NEW';
			printlog('Create a new Party Site' , gc_debug_flag);

		WHEN TOO_MANY_ROWS THEN
			lc_cust_site_flag := 'ERROR';
			x_error_msg := 'This CustAcctSite exists in Oracle multiple times! Exit Site creation' || SQLERRM;
			RAISE E_EXIT;
			
	END;

	IF lc_cust_site_flag = 'NEW' THEN

		lc_cust_acct_site_rec.cust_account_id := p_cust_account_id; 
		lc_cust_acct_site_rec.party_site_id   := p_party_site_id; 
		--lc_cust_acct_site_rec.customer_category_code  :=  p_customer.customer_category_code;
		lc_cust_acct_site_rec.created_by_module := gc_created_by_module;
		lc_cust_acct_site_rec.application_id    := gc_application_id;
		lc_cust_acct_site_rec.org_id            := p_customer.org_id;
    
    if  (p_customer.STORE_AREA is not null) and (p_customer.STORE_DOO is not null) then
        lc_cust_acct_site_rec.translated_customer_name := p_customer.STORE_AREA ||'#'||p_customer.STORE_DOO;  --H@N$/Yogesh/05-nov-08
    End If;    

		lc_cust_acct_site_rec.orig_system_reference :=  p_customer.orig_system_address_ref;
		--lc_cust_acct_site_rec.orig_system_reference :=  p_customer.orig_system_customer_ref;

		HZ_CUST_ACCOUNT_SITE_V2PUB.create_cust_acct_site(p_init_msg_list    => 'T',
							       p_cust_acct_site_rec => lc_cust_acct_site_rec,
							       x_cust_acct_site_id  => x_cust_acct_site_id,
							       x_return_status      => lc_return_status,
							       x_msg_count          => ln_msg_count,
							       x_msg_data           => lc_msg_data);
		IF (lc_return_status <> 'S') THEN
			fnd_msg_pub.get(p_msg_index     => FND_MSG_PUB.G_LAST,
					p_encoded       => 'F',
					p_data          => lc_msg_data,
					p_msg_index_out => ln_msg_index_out);

			x_cust_site_status := '1'; --FALSE;

			x_error_msg := 'API Error in create_cust_acct_site: ' || SUBSTR(lc_msg_data, 1, 200); 
		ELSE
			x_cust_site_status := '0'; --TRUE;
			printlog('cust_acct_site_id : ' || x_cust_acct_site_id, gc_debug_flag);

			UPDATE XXABRL_AR_CUSTOMER_INT
			SET    new_cust_acct_site_id    = x_cust_acct_site_id
			WHERE  orig_system_customer_ref = p_customer.orig_system_customer_ref 
			AND    record_number            = p_customer.record_number;

		END IF;
	ELSE
		NULL;  --cust acct site already exists
	END IF;

EXCEPTION
	WHEN E_EXIT THEN

		printlog(x_error_msg, gc_debug_flag);
		x_cust_site_status := '1'; --FALSE

	WHEN OTHERS THEN

		x_error_msg := 'Unknown Error in create_customer_acct_site:' || SQLERRM;
		printlog( x_error_msg, gc_debug_flag);
		x_cust_site_status := '1'; --FALSE

END create_customer_acct_site;

	
PROCEDURE create_customer_site_use (
 p_customer              IN XXABRL_AR_CUSTOMER_INT%ROWTYPE
,p_profile_class_id      IN NUMBER
,p_cust_acct_site_id     IN NUMBER
,p_cust_account_id       IN NUMBER
,p_party_id              IN NUMBER
,p_party_site_id         IN NUMBER
,x_site_use_id          OUT NUMBER
,x_cust_site_use_status OUT VARCHAR2
,x_error_msg            OUT VARCHAR2
) 
IS
	lc_cust_site_use_rec         hz_cust_account_site_v2pub.cust_site_use_rec_type;
	lc_customer_site_profile_rec hz_customer_profile_v2pub.customer_profile_rec_type;
	lc_cust_profile_amt_rec      hz_customer_profile_v2pub.cust_profile_amt_rec_type;
	--  lc_customer_profile_rec      hz_customer_profile_v2pub.customer_profile_rec_type;

	--lc_cust_profile_data        XXABRL_AR_CUST_PROFILE_INT%ROWTYPE;
	lc_cust_acct_profile_amt_id NUMBER;
	lc_return_status            VARCHAR2(10);
	ln_msg_count                NUMBER;
	lc_msg_data                 VARCHAR2(2000);
	ln_msg_index_out            NUMBER;
	ln_cust_account_profile_id  NUMBER;
	p_location                  NUMBER; -- H@N$
	lc_cust_site_use_status     VARCHAR2(240);

	E_EXIT			    EXCEPTION;

BEGIN

	BEGIN
		SELECT hcsua.site_use_id
		INTO   x_site_use_id	     
		FROM   HZ_CUST_ACCOUNTS       HCA
		      ,HZ_CUST_ACCT_SITES_ALL HCAS
		      ,HZ_CUST_SITE_USES_ALL  HCSUA
		WHERE hca.cust_account_id        = p_cust_account_id
		AND   hcas.cust_account_id       = hca.cust_account_id
		AND   hcsua.cust_acct_site_id    = hcas.cust_acct_site_id
		AND   hcsua.site_use_code        = p_customer.site_use_code
		AND   hcas.orig_system_reference = p_customer.orig_system_address_ref; --shipra

		printlog('CustAcctSiteUse already exists for site_use_id = ' || x_site_use_id, gc_debug_flag);
		x_cust_site_use_status := '0'; --TRUE;
		lc_cust_site_use_status := 'UPDATE';

		UPDATE XXABRL_AR_CUSTOMER_INT
		SET    new_site_use_id          = x_site_use_id
		WHERE  orig_system_customer_ref = p_customer.orig_system_customer_ref 
		AND    record_number            = p_customer.record_number;

	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			lc_cust_site_use_status := 'NEW';
			printlog('Create a new Site Use' , gc_debug_flag);

		WHEN TOO_MANY_ROWS THEN
			lc_cust_site_use_status := 'ERROR';
			x_error_msg := 'This CustAcctSiteUse exists in Oracle multiple times! Exit Site creation';
			RAISE E_EXIT;
			
	END;

	BEGIN
		SELECT location_id     --<<Abrl
		INTO p_location
		FROM hz_party_sites
		WHERE party_site_id = p_party_site_id;
		
		printlog('p_location:'||p_location, gc_debug_flag);
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			printlog('Error in create_customer_site_use:Location Not Found' || SQLERRM, gc_debug_flag);
		WHEN OTHERS THEN
			printlog('Error in create_customer_site_use:' || SQLERRM, gc_debug_flag);
	END;


	IF  lc_cust_site_use_status = 'NEW' THEN

		lc_cust_site_use_rec.gl_id_rev             := p_customer.gl_rev_acct_id;
		lc_cust_site_use_rec.gl_id_freight         := p_customer.gl_freight_acct_id;
		lc_cust_site_use_rec.gl_id_rec             := p_customer.gl_rec_acct_id; --added on 20-JUN-08
		lc_cust_site_use_rec.gl_id_tax             := p_customer.gl_tax_acct_id; --added on 20-JUN-08
		lc_cust_site_use_rec.gl_id_clearing        := p_customer.gl_clearing_acct_id; --added on 20-JUN-08
		lc_cust_site_use_rec.gl_id_unbilled        := p_customer.gl_unbilled_acct_id; --added on 20-JUN-08
		lc_cust_site_use_rec.gl_id_unearned        := p_customer.gl_unearned_acct_id; --added on 20-JUN-08
		lc_cust_site_use_rec.cust_acct_site_id     := p_cust_acct_site_id;
		lc_cust_site_use_rec.site_use_code         := p_customer.site_use_code;
		lc_cust_site_use_rec.created_by_module     := gc_created_by_module;
	--lc_cust_site_use_rec.orig_system_reference := TRIM(p_customer.orig_system_address_ref); --by H@N$ on 30-may-07 address1;------orig_system_address_ref;
--		lc_cust_site_use_rec.fob_point             := NULL; 
--		lc_cust_site_use_rec.freight_term          := NULL; 
		lc_cust_site_use_rec.ship_via              := p_customer.derived_ship_via_code; 

		IF UPPER(p_customer.country_code) <> 'US' THEN --H@N$ May 21-Leslie Mail
		    lc_cust_site_use_rec.tax_reference     := NULL;
		ELSE
		    lc_cust_site_use_rec.tax_reference     := p_customer.cust_tax_reference;
		END IF;

		lc_cust_site_use_rec.payment_term_id       := p_customer.payment_term_id;
		lc_cust_site_use_rec.primary_salesrep_id   := p_customer.salesrep_id; --l_salesrep_id;
		lc_cust_site_use_rec.location              := p_customer.city; --<<abrl 16/11/08 H@N$/Sankara ; earlier p_location to populate
		lc_cust_site_use_rec.org_id                := p_customer.org_id;
--		lc_cust_site_use_rec.order_type_id         := NULL;
		lc_cust_site_use_rec.application_id        := gc_application_id;
		lc_cust_site_use_rec.warehouse_id          := p_customer.warehouse_id;
		lc_cust_site_use_rec.primary_flag          := p_customer.primary_site_use_flag;

		IF p_customer.site_use_code = 'BILL_TO' THEN
      -- Create Profile class only if site is BILL_TO

      
			printlog('This is a BILL_TO site' , gc_debug_flag);

			/*
			BEGIN

				SELECT  *
				INTO   lc_cust_profile_data
				FROM (
					SELECT *
					FROM   XXABRL_AR_CUST_PROFILE_INT 
					WHERE  UPPER(orig_system_address_ref)  = UPPER(p_customer.orig_system_address_ref)
					AND    UPPER(orig_system_customer_ref) = UPPER(p_customer.orig_system_customer_ref)
					UNION
					SELECT *
					FROM   XXABRL_AR_CUST_PROFILE_INT 
					WHERE  UPPER(orig_system_address_ref) = UPPER(p_customer.orig_system_address_ref)
			--		AND    UPPER(orig_system_customer_ref) = UPPER(p_sys_cust_ref);
					);

			EXCEPTION
				WHEN NO_DATA_FOUND THEN
					x_error_msg := 'Profile not defined for this AddressRef#: ' || p_customer.orig_system_address_ref;
					RAISE E_EXIT;
			END;

			*/

			lc_customer_site_profile_rec.profile_class_id  := p_profile_class_id;
			--lc_customer_site_profile_rec.credit_checking   := lc_cust_profile_data.credit_checking;
			lc_customer_site_profile_rec.status            := 'A';
			lc_customer_site_profile_rec.override_terms    := 'Y'; --For AR INVOICES
			lc_customer_site_profile_rec.credit_classification := p_customer.credit_class_code;
			lc_customer_site_profile_rec.created_by_module := gc_created_by_module;
			lc_customer_site_profile_rec.application_id    := gc_application_id;
			lc_customer_site_profile_rec.cust_account_id   := p_cust_account_id;
			lc_customer_site_profile_rec.party_id          := p_party_id;

			--lc_customer_site_profile_rec.attribute1      := lc_cust_profile_data.prov_expires; --PROVISIONAL CREDIT EXPIRES
			--lc_customer_site_profile_rec.attribute2      := lc_cust_profile_data.credit_app; --CREDIT APPLICATION ON FILE
			--lc_customer_site_profile_rec.send_statements := lc_cust_profile_data.statements;
			lc_customer_site_profile_rec.standard_terms  := p_customer.payment_term_id;
			--lc_customer_site_profile_rec.last_credit_review_date   := to_date(lc_cust_profile_data.next_credit_review, 'MM-DD-YYYY');

			/*
			IF NVL(lc_cust_profile_data.statements, 'N') = 'N' THEN
				
				lc_customer_site_profile_rec.send_statements := 'N';
				lc_customer_site_profile_rec.statement_cycle_id := NULL; 
				lc_customer_site_profile_rec.credit_balance_statements := NULL;

			ELSIF lc_cust_profile_data.statements = 'Y' THEN
				
				lc_customer_site_profile_rec.send_statements := 'Y';

				SELECT statement_cycle_id
				INTO   lc_customer_site_profile_rec.statement_cycle_id
				FROM   AR_STATEMENT_CYCLES
				WHERE  UPPER(name) = 'MONTHLY';
				--lc_customer_site_profile_rec.statement_cycle_id := 2; 
				lc_customer_site_profile_rec.credit_balance_statements := lc_cust_profile_data.credit_balance_statement;
			END IF;
			*/
--			lc_customer_site_profile_rec.credit_balance_statements := 'N'; 

			printlog('Call api create_cust_site_use' , gc_debug_flag);

			HZ_CUST_ACCOUNT_SITE_V2PUB.create_cust_site_use(p_init_msg_list        => 'T',
								      p_cust_site_use_rec    => lc_cust_site_use_rec,
								      p_customer_profile_rec => lc_customer_site_profile_rec,
								      p_create_profile       => 'T', --'T'
								      p_create_profile_amt   => 'T', --lc_cust_profile_amt_rec,
								      x_site_use_id          => x_site_use_id,
								      x_return_status        => lc_return_status,
								      x_msg_count            => ln_msg_count,
								      x_msg_data             => lc_msg_data);
			IF (lc_return_status <> 'S') THEN

				fnd_msg_pub.get(p_msg_index     => FND_MSG_PUB.G_LAST,
						p_encoded       => 'F',
						p_data          => lc_msg_data,
						p_msg_index_out => ln_msg_index_out);

				x_error_msg := 'API Error in BILL-TO create_cust_site_use: ' || SUBSTR(lc_msg_data, 1, 200);
				
				x_cust_site_use_status := '1'; --FALSE

			ELSIF (lc_return_status = 'S') THEN
				
				printlog('API status of create_cust_site_use for BILL_TO:' || lc_return_status, gc_debug_flag);
				printlog('Success in create_cust_site_use for BILL_TO:' || x_site_use_id, gc_debug_flag);
        x_cust_site_use_status := '0'; --TRUE
		--Create Site level profile amount
/*
				BEGIN
					SELECT cust_account_profile_id
					INTO   ln_cust_account_profile_id
					FROM   HZ_CUSTOMER_PROFILES HCP
					WHERE  HCP.site_use_id     = x_site_use_id
					AND    HCP.cust_account_id = p_cust_account_id;

				EXCEPTION
					WHEN NO_DATA_FOUND then
						x_error_msg := 'Profile not created for AddressRef#:' || p_customer.orig_system_address_ref;
						RAISE E_EXIT;
				END;

        
--				lc_cust_profile_amt_rec.currency_code           := lc_cust_profile_data.currency_code;
--				lc_cust_profile_amt_rec.trx_credit_limit        := lc_cust_profile_data.trx_credit_limit;
--				lc_cust_profile_amt_rec.overall_credit_limit    := lc_cust_profile_data.overall_credit_limit;
				lc_cust_profile_amt_rec.cust_account_id         := p_cust_account_id;
				lc_cust_profile_amt_rec.created_by_module       := gc_created_by_module;
				lc_cust_profile_amt_rec.application_id          := gc_application_id;
--				lc_cust_profile_amt_rec.cust_account_profile_id := ln_cust_account_profile_id; --For profile:DEFAULT
				lc_cust_profile_amt_rec.site_use_id             := x_site_use_id;
--				lc_cust_profile_amt_rec.attribute1              := lc_cust_profile_data.eh_insured;

				HZ_CUSTOMER_PROFILE_V2PUB.create_cust_profile_amt(p_init_msg_list            => 'F',
										  p_check_foreign_key        => 'T',
										  p_cust_profile_amt_rec     => lc_cust_profile_amt_rec,
										  x_cust_acct_profile_amt_id => lc_cust_acct_profile_amt_id,
										  x_return_status            => lc_return_status,
										  x_msg_count                => ln_msg_count,
										  x_msg_data                 => lc_msg_data);
				IF (lc_return_status <> 'S') THEN

					fnd_msg_pub.get(p_msg_index     => FND_MSG_PUB.G_LAST,
						  p_encoded       => 'F',
						  p_data          => lc_msg_data,
						  p_msg_index_out => ln_msg_index_out);

					x_error_msg := 'API Error in BILL-TO create_cust_profile_amt: ' || SUBSTR(lc_msg_data, 1, 200); 

					x_cust_site_use_status := '1'; --FALSE

				ELSE
					printlog('Created profile amount for BILL_TO site; profile amount id: ' || lc_cust_acct_profile_amt_id, gc_debug_flag);
					x_cust_site_use_status := '0'; --TRUE;

					UPDATE XXABRL_AR_CUSTOMER_INT
					SET    new_site_use_id          = x_site_use_id
					      ,new_site_profile_amt_id  = lc_cust_acct_profile_amt_id
					WHERE  orig_system_customer_ref = p_customer.orig_system_customer_ref 
					AND    record_number            = p_customer.record_number;

				END IF;*/
       
			END IF;
      printlog('Just before SHIP_TO site use', gc_debug_flag);
      
		ELSIF p_customer.site_use_code = 'SHIP_TO' THEN

			lc_cust_site_use_rec.gl_id_rev      := null;
			lc_cust_site_use_rec.gl_id_freight  := null;
			lc_cust_site_use_rec.gl_id_rec      := null;
			lc_cust_site_use_rec.gl_id_tax      := null;
			lc_cust_site_use_rec.gl_id_clearing := null;
			lc_cust_site_use_rec.gl_id_unbilled := null;
			lc_cust_site_use_rec.gl_id_unearned := null;

			HZ_CUST_ACCOUNT_SITE_V2PUB.create_cust_site_use(p_init_msg_list        => 'T',
									p_cust_site_use_rec    => lc_cust_site_use_rec,
									p_customer_profile_rec => lc_customer_site_profile_rec,
									p_create_profile       => 'F',
									p_create_profile_amt   => 'F',
									x_site_use_id          => x_site_use_id,
									x_return_status        => lc_return_status,
									x_msg_count            => ln_msg_count,
									x_msg_data             => lc_msg_data);
			IF (lc_return_status <> 'S') THEN

				fnd_msg_pub.get ( p_msg_index  => FND_MSG_PUB.G_LAST,
					  p_encoded       => 'F',
					  p_data          => lc_msg_data,
					  p_msg_index_out => ln_msg_index_out);

				x_error_msg := 'API Error in SHIP_TO create_cust_site_use: ' || SUBSTR(lc_msg_data, 1, 200); 

				x_cust_site_use_status := '1';  --FALSE

			ELSIF (lc_return_status = 'S') THEN

				printlog('Created create_cust_site_use for SHIP_TO site:' || x_site_use_id,  gc_debug_flag);

				x_cust_site_use_status := '0'; --TRUE;

				UPDATE XXABRL_AR_CUSTOMER_INT
				SET    new_site_use_id          = x_site_use_id
--				      ,new_site_profile_amt_id  = lc_cust_acct_profile_amt_id
				WHERE  orig_system_customer_ref = p_customer.orig_system_customer_ref 
				AND    record_number            = p_customer.record_number;
			END IF;
		END IF;
	ELSE
		NULL; --site use already exists
    printlog('Site Use already exists',  gc_debug_flag);
	END IF;

EXCEPTION
	WHEN E_EXIT THEN

		printlog(x_error_msg, gc_debug_flag);
		x_cust_site_use_status := '1'; --FALSE

	WHEN OTHERS THEN

		x_error_msg := 'Unexpected Error in create_customer_site_use: ' || SQLERRM;
		printlog(x_error_msg, gc_debug_flag);
		x_cust_site_use_status := '1'; --FALSE

END create_customer_site_use;






  --Main procedure to Import Customer
  PROCEDURE main(
 retbuf          OUT VARCHAR2
,retcode         OUT NUMBER
,p_action	 IN VARCHAR2
,p_debug_flag     IN VARCHAR2) 
IS

	CURSOR cur_customer_acct
	IS
		SELECT *
		FROM XXABRL_AR_CUSTOMER_INT
		WHERE status_flag IN ('V')
		ORDER BY customer_name, site_use_code;

	CURSOR cur_customer_site
	IS
		SELECT *
		FROM  XXABRL_AR_CUSTOMER_INT
		WHERE status_flag IN ('V')
		AND   new_party_id        IS NOT NULL
		AND   new_cust_Account_id IS NOT NULL
		ORDER BY customer_name, site_use_code;
/*
	CURSOR cur_cust_site_contact
	IS
		SELECT  b.* -- this is for contacts at the site level 
		FROM   XXABRL_AR_CUSTOMER_INT a
		      ,XXABRL_AR_CUST_CONTACT_INT b
		WHERE a.status_flag = 'S'
		AND   new_party_id        IS NOT NULL
		AND   new_cust_Account_id IS NOT NULL
		AND   a.orig_system_address_ref  = b.orig_system_address_ref
		AND   a.orig_system_customer_ref = b.orig_system_customer_ref
		AND   b.status_flag IN ('N')
	UNION
		SELECT distinct  b.* -- this is for contacts at the account level , 20-JUN-2008
		FROM   XXABRL_AR_CUSTOMER_INT a
		      ,XXABRL_AR_CUST_CONTACT_INT b
		WHERE a.status_flag = 'S'
		AND   new_party_id        IS NOT NULL
		AND   new_cust_Account_id IS NOT NULL
		AND   b.orig_system_address_ref  IS NULL
		AND   a.orig_system_customer_ref = b.orig_system_customer_ref
		AND   b.status_flag IN ('N');
--		ORDER BY b.orig_system_customer_ref, b.orig_system_address_ref;
*/
	ln_cust_account_id      NUMBER;
	ln_location_id          NUMBER;
	ln_party_id             NUMBER;
	ln_party_site_id        NUMBER;
	ln_cust_acct_site_id    NUMBER;
	ln_site_use_id          NUMBER;
	ln_profile_class_id     NUMBER;
	ln_new_party_id          NUMBER;
	ln_new_cust_account_id   NUMBER;
	ln_new_party_site_id     NUMBER;
	ln_new_cust_acct_site_id NUMBER;
	lc_person_flag          VARCHAR2(1);
	lc_customer_name        VARCHAR2(240);
	ln_contact_party_id     NUMBER;
	ln_party_rel_id         NUMBER;
	ln_org_contact_id       NUMBER;
	ln_cust_acct_role_id    NUMBER;
	ln_responsibility_id    NUMBER;
	lc_org_code             VARCHAR2(240);
	lc_url                  VARCHAR2(240);
	lc_cust_status          VARCHAR2(1);
	lc_loc_status           VARCHAR2(1);
	lc_party_site_status    VARCHAR2(1);
	lc_cust_site_status     VARCHAR2(1);
	lc_cust_site_use_status VARCHAR2(1);
	lc_site_cont_status     VARCHAR2(1);
	lc_relate_status        VARCHAR2(1);
	lc_error_msg		VARCHAR2(4000);

	ln_total_acct_cnt	NUMBER := 0;
	ln_error_acct_cnt	NUMBER := 0;
	ln_success_acct_cnt	NUMBER := 0;
	ln_total_site_cnt	NUMBER := 0;
	ln_error_site_cnt	NUMBER := 0;
	ln_success_site_cnt	NUMBER := 0;
	ln_total_contact_cnt	NUMBER := 0;
	ln_error_contact_cnt	NUMBER := 0;
	ln_success_contact_cnt	NUMBER := 0;
	ln_commit_cnt           NUMBER := 0;

	E_EXIT			EXCEPTION;
	E_SITE_EXIT		EXCEPTION;
	E_CONTACT_EXIT          EXCEPTION;
  BEGIN
	printlog('Main program begins here', gc_debug_flag);

	gc_debug_flag := p_debug_flag;
	
	printlog('Valdiation begins here; Debug Flag Status: ', gc_debug_flag);

	if upper(p_action) = 'Y' then
		validate_customer_data (lc_error_msg);
		printlog('Running for Validate Only ', gc_debug_flag);
		RAISE E_EXIT; 

	else
		validate_customer_data (lc_error_msg);
	End if;
	
	IF lc_error_msg IS NOT NULL THEN
		printlog('Un-expected Error In Validation', gc_debug_flag);
		RAISE E_EXIT;

	END IF;

	printlog('Start creating Customer Accounts', gc_debug_flag);
	ln_commit_cnt := 0;
	FOR  lc_acct IN cur_customer_acct LOOP
	BEGIN

		ln_commit_cnt := ln_commit_cnt + 1;
		IF ln_commit_cnt >= 100 THEN
			COMMIT;
			ln_commit_cnt := 0;
		END IF;

		SAVEPOINT ACCT;
		ln_total_acct_cnt := ln_total_acct_cnt + 1;
		create_customer_account ( p_customer        => lc_acct,
					  x_party_id        => ln_party_id,
					  x_cust_account_id => ln_cust_account_id,
					  x_cust_status     => lc_cust_status,
					  x_error_msg       => lc_error_msg);

	        printlog(' Flag cust_status : ' || lc_cust_status, gc_debug_flag);

		IF lc_cust_status = '1' THEN
			printlog(' -------ACCOUNT ERROR-----------', gc_debug_flag);
			printlog(' ', gc_debug_flag);

			ROLLBACK TO ACCT;
			ln_error_acct_cnt := ln_error_acct_cnt + 1;

			UPDATE XXABRL_AR_CUSTOMER_INT 
			SET    status_flag = 'E'
			      ,error_msg   = lc_error_msg
			WHERE orig_system_address_ref = lc_acct.orig_system_address_ref
			AND   record_number           = lc_acct.record_number;		

		ELSIF lc_cust_status = '0' THEN

			ln_success_acct_cnt := ln_success_acct_cnt + 1;
			printlog(' -------ACCOUNT CREATION SUCCESSFUL-----------', gc_debug_flag);
			printlog(' ', gc_debug_flag);

		END IF;

	EXCEPTION

		WHEN OTHERS THEN

			lc_error_msg := 'Error in account creation! Exit program'||SQLERRM;
			RAISE E_EXIT;

	END;
	END LOOP;

        printlog(' Total Customer Accounts    =  ' || ln_total_acct_cnt, gc_debug_flag);
        printlog(' Customer Accounts Success  =  ' || ln_success_acct_cnt, gc_debug_flag);
        printlog(' Customer Accounts In Error =  ' || ln_error_acct_cnt, gc_debug_flag);
	printlog('- - - - - - - - - - - - - - - - - - - - - - - - - - - - -  ', gc_debug_flag);
	printlog(' ', gc_debug_flag);
	COMMIT;

	ln_commit_cnt := 0;
	FOR lc_site IN cur_customer_site LOOP
	BEGIN
		ln_commit_cnt := ln_commit_cnt + 1;
		IF ln_commit_cnt >= 100 THEN
			COMMIT;
			ln_commit_cnt := 0;
		END IF;

		printlog('OrigSysAddressRef#: '||lc_site.orig_system_address_ref || ' ,CustomerName: '||lc_site.customer_name ,gc_debug_flag);
		SAVEPOINT SITE;

		ln_total_site_cnt    := ln_total_site_cnt + 1;
		ln_Location_id       := NULL;
		ln_party_site_id     := NULL;
		ln_cust_acct_site_id := NULL;
		ln_site_use_id       := NULL;

		create_customer_location ( p_customer    => lc_site,
					   p_party_id    => lc_site.new_party_id,
					   x_location_id => ln_location_id,
					   x_loc_status  => lc_loc_status,
					   x_error_msg   => lc_error_msg);

		printlog('Location Flag : ' || lc_loc_status, gc_debug_flag);

		IF (lc_loc_status = '0') AND (lc_error_msg IS NULL) THEN
			create_party_site ( p_customer          => lc_site,
					    p_party_id          => lc_site.new_party_id,
					    p_location_id       => ln_location_id,
				 	    x_party_site_id     => ln_party_site_id,
					    x_party_site_status => lc_party_site_status,
					    x_error_msg         => lc_error_msg);

		ELSE
      printlog(' -------SITE ERROR #1126', gc_debug_flag);    
			RAISE E_SITE_EXIT;
		END IF;

		IF (lc_party_site_status = '0') AND (lc_error_msg IS NULL) THEN
			create_customer_acct_site (  p_customer          => lc_site,
						     p_cust_account_id   => lc_site.new_cust_account_id,
						     p_party_site_id     => ln_party_site_id,
						     x_cust_acct_site_id => ln_cust_acct_site_id,
						     x_cust_site_status  => lc_cust_site_status,
						     x_error_msg         => lc_error_msg);
		ELSE
      printlog(' -------SITE ERROR #1137', gc_debug_flag);
			RAISE E_SITE_EXIT;
		END IF;

		IF (lc_cust_site_status = '0') AND (lc_error_msg IS NULL) THEN

			create_customer_site_use ( p_customer             => lc_site,
						   p_profile_class_id     => ln_profile_class_id, --NULL
						   p_cust_acct_site_id    => ln_cust_acct_site_id,
						   p_cust_account_id      => lc_site.new_cust_account_id,
						   p_party_id             => lc_site.new_party_id,
						   p_party_site_id        => ln_party_site_id,
						   x_site_use_id          => ln_site_use_id,
						   x_cust_site_use_status => lc_cust_site_use_status,
						   x_error_msg            => lc_error_msg);
		ELSE
      printlog(' -------SITE ERROR #1153', gc_debug_flag);
			RAISE E_SITE_EXIT;
		END IF;

		IF (lc_cust_site_use_status = '0')  AND (lc_error_msg IS NULL) THEN
			printlog(' -------SITE CREATED SUCCESSFULLY-----------', gc_debug_flag);
			printlog(' ', gc_debug_flag);
			ln_success_site_cnt := ln_success_site_cnt + 1;

			UPDATE XXABRL_AR_CUSTOMER_INT 
			SET     status_flag      = 'S'
			       ,error_msg        = NULL
			       ,last_updated_by  = gn_user_id
			       ,created_by       = gn_user_id
			       ,request_id       = gn_request_id
			       ,last_update_date = sysdate
			WHERE orig_system_address_ref = lc_site.orig_system_address_ref
			AND   record_number           = lc_site.record_number;

			printlog(' -------Inserting Additional Info (VAT/CST etc)-----------', gc_debug_flag);      
      
      /*BEGIN
      insert into 
      JAI_CMN_CUS_ADDRESSES
      (SERVICE_TAX_REGNO,
      VAT_REG_NO,
      CST_REG_NO,
      PAN_NO,
      TAN_NO)
      values
      (
      lc_site.SERVICE_TAX_REGNO,
      lc_site.VAT_REG_NO,
      lc_site.CST_REG_NO,
      lc_site.PAN_NO,
      lc_site.TAN_NO);
      
      commit;
      EXCEPTION
      when others then
			printlog('Error while inserting Additional Info :'||lc_site.Customer_name, gc_debug_flag);            
      END;*/
      
      		

		ELSE
      printlog(' -------SITE ERROR #1173', gc_debug_flag);
      printlog(lc_error_msg, gc_debug_flag);
      
			RAISE E_SITE_EXIT;
		END IF;
		
	EXCEPTION
		WHEN E_SITE_EXIT THEN
			printlog('Site Error: ' || lc_error_msg, gc_debug_flag);
			printlog(' -------SITE ERROR-----------', gc_debug_flag);
			printlog(' ', gc_debug_flag);
			ROLLBACK TO SITE;

			ln_error_site_cnt := ln_error_site_cnt + 1;			

			UPDATE XXABRL_AR_CUSTOMER_INT 
			SET    status_flag = 'E'
			      ,error_msg   = lc_error_msg
			       ,last_updated_by  = gn_user_id
			       ,created_by       = gn_user_id
			       ,request_id       = gn_request_id
			       ,last_update_date = sysdate
			WHERE orig_system_address_ref = lc_site.orig_system_address_ref
			AND   record_number           = lc_site.record_number;		

		WHEN OTHERS THEN

			lc_error_msg := 'Error in Site creation! Exit program'||SQLERRM;
			RAISE E_EXIT;
	END;
	END LOOP;

        printlog(' Total Sites Count          =  ' || ln_total_site_cnt, gc_debug_flag);
        printlog(' Sites Successfully Created =  ' || ln_success_site_cnt, gc_debug_flag);
        printlog(' Sites In Error             =  ' || ln_error_site_cnt, gc_debug_flag);
	printlog('- - - - - - - - - - - - - - - - - - - - - - - - - - - - -  ', gc_debug_flag);
	printlog(' ', gc_debug_flag);
	COMMIT;

	ln_commit_cnt := 0;
/*
	FOR lc_contact IN cur_cust_site_contact LOOP
	BEGIN
		ln_commit_cnt := ln_commit_cnt + 1;
		IF ln_commit_cnt >= 100 THEN
			COMMIT;
			ln_commit_cnt := 0;
		END IF;

		lc_site_cont_status := '0';
		SAVEPOINT CONTACT;

		ln_total_contact_cnt := ln_total_contact_cnt + 1;
		ln_contact_party_id  := NULL;
		ln_party_rel_id      := NULL;
		ln_org_contact_id    := NULL;
		ln_cust_acct_role_id := NULL;
		ln_responsibility_id := NULL;

		printlog('Create contact for OrigSysAddressRef#: '||lc_contact.orig_system_address_ref || ' ,CustomerName: '||lc_customer_name ,gc_debug_flag);
		printlog('First Middle Last Name: '|| lc_contact.contact_first_name ||' '|| lc_contact.contact_middle_name || ' '|| lc_contact.contact_last_name ,gc_debug_flag);

		BEGIN
			SELECT org_code, url, new_party_id, new_cust_account_id, new_party_site_id, new_cust_acct_site_id, person_flag, customer_name
			INTO   lc_org_code, lc_url, ln_new_party_id, ln_new_cust_account_id, ln_new_party_site_id, ln_new_cust_acct_site_id, lc_person_flag, lc_customer_name
			FROM   XXABRL_AR_CUSTOMER_INT 
			WHERE  status_flag = 'S'
			AND    new_party_id        IS NOT NULL
			AND    new_cust_account_id IS NOT NULL
			AND    orig_system_address_ref  = lc_contact.orig_system_address_ref
			AND    orig_system_customer_ref = lc_contact.orig_system_customer_ref;
		EXCEPTION
			WHEN NO_DATA_FOUND THEN

				SELECT distinct org_code, url, new_party_id, new_cust_account_id, new_party_site_id, new_cust_acct_site_id, person_flag, customer_name
				INTO   lc_org_code, lc_url, ln_new_party_id, ln_new_cust_account_id, ln_new_party_site_id, ln_new_cust_acct_site_id, lc_person_flag, lc_customer_name
				-- this is for contacts at the account level , 20-JUN-2008
				FROM   XXABRL_AR_CUSTOMER_INT a
				WHERE status_flag = 'S'
				AND   new_party_id        IS NOT NULL
				AND   new_cust_Account_id IS NOT NULL
		--		AND   orig_system_address_ref  IS NULL
				AND   orig_system_customer_ref = lc_contact.orig_system_customer_ref
				AND   ROWNUM = 1 ; -- this returnn multiple rows for bill_to and ship_to


		END;

		IF  (lc_contact.contact_first_name IS NOT NULL) OR (lc_contact.contact_last_name IS NOT NULL) THEN
			create_cont_party_at_site ( p_contact          => lc_contact
						   ,x_contact_party_id => ln_contact_party_id
						   ,x_site_cont_status => lc_site_cont_status
						   ,x_error_msg        => lc_error_msg);

			printlog('Party Contact at Site Flag : ' || lc_site_cont_status, gc_debug_flag);

			IF (lc_site_cont_status = '0') AND (lc_error_msg IS NULL) THEN
				create_org_contact( p_contact            => lc_contact
						   ,p_contact_party_id   => ln_contact_party_id
						   ,p_new_party_id       => ln_new_party_id
						   ,p_person_flag        => lc_person_flag
						   ,x_party_rel_id       => ln_party_rel_id
						   ,x_org_contact_id     => ln_org_contact_id
						   ,x_contact_status     => lc_site_cont_status
						   ,x_error_msg          => lc_error_msg);
			ELSE
				RAISE E_CONTACT_EXIT;
			END IF;

			printlog('Org Contact Relationship Flag : ' || lc_site_cont_status, gc_debug_flag);

			IF (lc_site_cont_status = '0') AND (lc_error_msg IS NULL) THEN
				IF lc_contact.orig_system_address_ref IS NULL THEN -- added on 20-JUN-08, to create at account level only
					ln_new_cust_acct_site_id := NULL;
				END IF;
				create_cust_account_role( p_contact      => lc_contact
						   ,p_org_contact_id     => ln_org_contact_id
						   ,p_cust_account_id    => ln_new_cust_account_id
						   ,p_cust_acct_site_id  => ln_new_cust_acct_site_id
						   ,x_cust_acct_role_id  => ln_cust_acct_role_id
						   ,x_contact_status     => lc_site_cont_status
						   ,x_error_msg          => lc_error_msg);
			ELSE
				RAISE E_CONTACT_EXIT;
			END IF;

			printlog('Party Contact Role at Site Flag : ' || lc_site_cont_status, gc_debug_flag);

			IF (lc_site_cont_status = '0') AND (lc_error_msg IS NULL) THEN --AND (lc_org_code = 'RECOU') THEN  commented on 20-JUN-08
				create_role_responsibility (
				 p_contact              => lc_contact
				,p_cust_acct_role_id    => ln_cust_acct_role_id
				,x_responsibility_id    => ln_responsibility_id
				,x_contact_status       => lc_site_cont_status
				,x_error_msg            => lc_error_msg);		
			ELSIF ((lc_site_cont_status <> '0') OR (lc_error_msg IS NOT NULL)) THEN --AND (lc_org_code = 'RECOU') THEN 
				RAISE E_CONTACT_EXIT;
			END IF;

			printlog('Role Responsibility Flag : ' || lc_site_cont_status, gc_debug_flag);
		END IF;

		printlog('Before creating contact point : error_msg : ' || lc_error_msg, gc_debug_flag);
		IF (lc_site_cont_status = '0') AND (lc_error_msg IS NULL) THEN
			IF lc_contact.orig_system_address_ref IS NULL THEN -- added on 20-JUN-08, to create at account level only
				ln_new_party_site_id := NULL;
			END IF;

			create_contact_point (   p_contact          => lc_contact
						,p_url              => lc_url
						,p_party_site_id    => ln_new_party_site_id
						,p_org_contact_id   => ln_org_contact_id
						,x_contact_status   => lc_site_cont_status
						,x_error_msg        => lc_error_msg);
		ELSE
			RAISE E_CONTACT_EXIT;
		END IF;

		printlog('Contact Point at Site Flag : ' || lc_site_cont_status, gc_debug_flag);

		IF (lc_site_cont_status = '0')  AND (lc_error_msg IS NULL) THEN
			printlog(' -------CONTACT AT SITE CREATED SUCCESSFULLY-----------', gc_debug_flag);
			printlog(' ', gc_debug_flag);
			ln_success_contact_cnt := ln_success_contact_cnt + 1;

			UPDATE XXABRL_AR_CUST_CONTACT_INT 
			SET     status_flag      = 'S'
			       ,error_msg        = NULL
			       ,last_updated_by  = gn_user_id
			       ,created_by       = gn_user_id
			       ,request_id       = gn_request_id
			       ,last_update_date = sysdate
			WHERE --orig_system_address_ref = lc_contact.orig_system_address_ref
			   record_number           = lc_contact.record_number;		

		ELSE
--			printlog('Raising after success : '|| lc_error_msg, gc_debug_flag);
			RAISE E_CONTACT_EXIT;
		END IF;
		
		ln_commit_cnt := ln_commit_cnt + 1;
		IF ln_commit_cnt >= 100 THEN
			COMMIT;
			ln_commit_cnt := 0;
		END IF;

	EXCEPTION
		WHEN E_CONTACT_EXIT THEN
			printlog('Error creating site level Contact : ' || lc_error_msg, gc_debug_flag);
			printlog(' -------PARTY CONTACT AT SITE ERROR-----------', gc_debug_flag);
			printlog(' ', gc_debug_flag);
			ROLLBACK TO CONTACT;

			ln_error_contact_cnt := ln_error_contact_cnt + 1;			

			UPDATE XXABRL_AR_CUST_CONTACT_INT 
			SET    status_flag      = 'E'
			      ,error_msg        = lc_error_msg
			      ,last_updated_by  = gn_user_id
			      ,created_by       = gn_user_id
			      ,request_id       = gn_request_id
			      ,last_update_date = sysdate
			WHERE orig_system_address_ref = lc_contact.orig_system_address_ref
			AND   record_number           = lc_contact.record_number;		

		WHEN NO_DATA_FOUND THEN

			lc_error_msg := 'No site / account found for this contact! Error record';
--			RAISE E_EXIT;
			printlog('Error creating site level Contact : ' || lc_error_msg, gc_debug_flag);
			printlog(' -------PARTY CONTACT AT SITE ERROR-----------', gc_debug_flag);
			printlog(' ', gc_debug_flag);
			ROLLBACK TO CONTACT;

			ln_error_contact_cnt := ln_error_contact_cnt + 1;			

			UPDATE XXABRL_AR_CUST_CONTACT_INT 
			SET    status_flag      = 'E'
			      ,error_msg        = lc_error_msg
			      ,last_updated_by  = gn_user_id
			      ,created_by       = gn_user_id
			      ,request_id       = gn_request_id
			      ,last_update_date = sysdate
			WHERE orig_system_address_ref = lc_contact.orig_system_address_ref
			AND   record_number           = lc_contact.record_number;		

		WHEN OTHERS THEN

			lc_error_msg := 'Error in Contact creation at site level! Exit program'||SQLERRM;
			RAISE E_EXIT;
	END;
	END LOOP;

        printlog(' Total Contacts Count          =  ' || ln_total_contact_cnt, gc_debug_flag);
        printlog(' Contacts Successfully Created =  ' || ln_success_contact_cnt, gc_debug_flag);
        printlog(' Contacts In Error             =  ' || ln_error_contact_cnt, gc_debug_flag);
	printlog('- - - - - - - - - - - - - - - - - - - - - - - - - - - - -  ', gc_debug_flag);
	printlog(' ', gc_debug_flag);
	COMMIT;


	printlog('Create account relationships ', gc_debug_flag);
	create_acct_relation( 
		 x_relate_status   => lc_relate_status
		,x_error_msg       => lc_error_msg);
	IF lc_error_msg IS NOT NULL OR lc_relate_status = '1' THEN
		RAISE E_EXIT;
	END IF;
*/
	COMMIT;
	

	printlog('Main program ends here', gc_debug_flag);
EXCEPTION
	WHEN E_EXIT THEN
		printlog( lc_error_msg, gc_debug_flag);	
	WHEN OTHERS THEN
		printlog('Error in main:' || SQLERRM, gc_debug_flag);
END main;

PROCEDURE validate_customer_data(
 x_error_msg  OUT VARCHAR2) 
AS

	CURSOR cur_validate
	IS
		SELECT *
		FROM XXABRL_AR_CUSTOMER_INT
		WHERE status_flag IN ('N','E') --org_id = p_org_id
		ORDER BY record_number;
	
	ln_org_id                  NUMBER;
	lc_customer_name           VARCHAR2(240);
	ln_loop_cnt	           NUMBER := 0;
	ln_valid_cnt	           NUMBER := 0;
	ln_total_cnt	           NUMBER := 0;
	ln_error_cnt	           NUMBER := 0;
	lc_country_code	           VARCHAR2(240);
	ln_salesrep_id             NUMBER;
	lc_business_type           VARCHAR2(240);
	lc_credit_class_code       VARCHAR2(240);
	ln_payment_term_id         NUMBER;
	lc_ship_via_code           VARCHAR(240);
	lc_error_msg               VARCHAR2(4000);
	lc_error                   VARCHAR2(4000);
	ln_gl_rev_acct_id          NUMBER;
	ln_gl_freight_acct_id      NUMBER;
	ln_gl_rec_acct_id	   NUMBER;
	ln_gl_tax_acct_id	   NUMBER;
	ln_gl_clearing_acct_id     NUMBER;
	ln_gl_unbilled_acct_id     NUMBER;
	ln_gl_unearned_acct_id     NUMBER;
	ln_warehouse_id		   NUMBER;

BEGIN
	
	printlog('Begin validate_customer_data', gc_debug_flag);

	UPDATE XXABRL_AR_CUSTOMER_INT
	SET    residential = 'Y'
	WHERE  residential = 'T';

	UPDATE XXABRL_AR_CUSTOMER_INT
	SET    residential = 'N'
	WHERE  residential = 'F';


	UPDATE XXABRL_AR_CUSTOMER_INT
	SET    person_flag = 'Y'
	WHERE  person_flag = 'YY';
   
	UPDATE XXABRL_AR_CUSTOMER_INT
	SET    site_use_code = 'SHIP_TO'
	WHERE  site_use_code = 'SHIPTO';

	UPDATE XXABRL_AR_CUSTOMER_INT
	SET    site_use_code = 'BILL_TO'
	WHERE  site_use_code = 'BILLTO';
/*
	UPDATE XXABRL_AR_CUST_PROFILE_INT
	SET    site_use_code = 'SHIP_TO'
	WHERE  site_use_code = 'SHIPTO';

	UPDATE XXABRL_AR_CUST_PROFILE_INT
	SET    credit_class = 'Moderate Risk'
	WHERE  credit_class = 'MEDIUM RISK';


	UPDATE XXABRL_AR_CUST_PROFILE_INT
	SET    site_use_code = 'BILL_TO'
	WHERE  site_use_code = 'BILLTO';

	UPDATE XXABRL_AR_CUST_CONTACT_INT
	SET    role = 'BILL TO'
	WHERE  role = 'BILLTO';

	UPDATE XXABRL_AR_CUST_CONTACT_INT
	SET    role = 'SHIP TO'
	WHERE  role = 'SHIPTO';
*/
	COMMIT;

--  UPDATE ar_lookups SET enabled_flag = 'Y' WHERE Meaning = 'Fax';

/*
	UPDATE XXABRL_AR_CUST_PROFILE_INT
	SET    credit_app = 'Y'
	WHERE  credit_app = 'T';

	UPDATE XXABRL_AR_CUST_PROFILE_INT
	SET    credit_app = 'N'
	WHERE  credit_app = 'F';

	UPDATE XXABRL_AR_CUST_PROFILE_INT
	SET    statements = 'Y'
	WHERE  statements = 'T';
	UPDATE XXABRL_AR_CUST_PROFILE_INT
	SET statements = 'N'
	WHERE statements = 'F';

	UPDATE XXABRL_AR_CUST_CONTACT_INT
	SET PRIMARY_TELEPHONE_FLAG = 'Y'
	WHERE trim(PRIMARY_TELEPHONE_FLAG) = 'YES';

	UPDATE XXABRL_AR_CUSTOMER_INT
	SET    derived_tc_sent = TO_CHAR(tc_sent, 'YYYY/MM/DD HH24:MI:SS')
	WHERE  tc_sent IS NOT NULL;

*/
--To Remove NULL AND Trim

	UPDATE XXABRL_AR_CUSTOMER_INT
	SET 
         orig_system_customer_ref = DECODE(UPPER(orig_system_customer_ref),
                                           'NULL',
                                           NULL,
                                           orig_system_customer_ref),
         site_use_code            = DECODE(UPPER(site_use_code),
                                           'NULL',
                                           NULL,
                                           site_use_code),
         orig_system_address_ref  = DECODE(UPPER(orig_system_address_ref),
                                           'NULL',
                                           NULL,
                                           orig_system_address_ref),
         customer_name            = DECODE(UPPER(customer_name),
                                           'NULL',
                                           NULL,
                                           customer_name),
         customer_number          = DECODE(UPPER(customer_number),
                                           'NULL',
                                           NULL,
                                           customer_number),
         customer_status          = DECODE(UPPER(customer_status),
                                           'NULL',
                                           NULL,
                                           customer_status),
         customer_type            = DECODE(UPPER(customer_type),
                                           'NULL',
                                           NULL,
                                           customer_type),
         orig_system_parent_ref   = DECODE(UPPER(orig_system_parent_ref),
                                           'NULL',
                                           NULL,
                                           orig_system_parent_ref),
         primary_site_use_flag    = DECODE(UPPER(primary_site_use_flag),
                                           'NULL',
                                           NULL,
                                           primary_site_use_flag),
         address1                 = DECODE(UPPER(address1),
                                           'NULL',
                                           NULL,
                                           address1),
         address2                 = DECODE(UPPER(address2),
                                           'NULL',
                                           NULL,
                                           address2),
         address3                 = DECODE(UPPER(address3),
                                           'NULL',
                                           NULL,
                                           address3),
         city                     = DECODE(UPPER(city),
                                           'NULL',
                                           NULL,
                                           city),
         state                    = DECODE(UPPER(state),
                                           'NULL',
                                           NULL,
                                           state),
         province                 = DECODE(UPPER(province),
                                           'NULL',
                                           NULL,
                                           province),
         county                   = DECODE(UPPER(county),
                                           'NULL',
                                           NULL,
                                           county),
         postal_code              = DECODE(UPPER(postal_code),
                                           'NULL',
                                           NULL,
                                           postal_code),
         country                  = DECODE(UPPER(country),
                                           'NULL',
                                           NULL,
                                           country),
         last_updated_by          = DECODE(UPPER(last_updated_by),
                                           'NULL',
                                           NULL,
                                           last_updated_by),
         last_update_date         = DECODE(UPPER(last_update_date),
                                           'NULL',
                                           NULL,
                                           last_update_date),
         created_by               = DECODE(UPPER(created_by),
                                           'NULL',
                                           NULL,
                                           created_by),
         creation_date            = DECODE(UPPER(creation_date),
                                           'NULL',
                                           NULL,
                                           creation_date),
         last_update_login        = DECODE(UPPER(last_update_login),
                                           'NULL',
                                           NULL,
                                           last_update_login),
         url                      = DECODE(UPPER(url),
                                           'NULL',
                                           NULL,
                                           url),
         org_id                   = DECODE(UPPER(org_id),
                                           'NULL',
                                           NULL,
                                           org_id),
         person_flag              = DECODE(UPPER(person_flag),
                                           'NULL',
                                           NULL,
                                           person_flag),
         person_first_name        = DECODE(UPPER(person_first_name),
                                           'NULL',
                                           NULL,
                                           person_first_name),
         person_last_name         = DECODE(UPPER(person_last_name),
                                           'NULL',
                                           NULL,
                                           person_last_name),
         site_name                = DECODE(UPPER(site_name),
                                           'NULL',
                                           NULL,
                                           site_name),
         addressee                = DECODE(UPPER(addressee),
                                           'NULL',
                                           NULL,
                                           addressee),
         salesperson              = DECODE(UPPER(salesperson),
                                           'NULL',
                                           NULL,
                                           salesperson),
         warehouse                = DECODE(UPPER(warehouse),
                                           'NULL',
                                           NULL,
                                           warehouse),
         record_number            = DECODE(UPPER(record_number),
                                           'NULL',
                                           NULL,
                                           record_number),
         residential              = DECODE(UPPER(residential),
                                           'NULL',
                                           NULL,
                                           residential),
         custclass                = DECODE(UPPER(custclass),
                                           'NULL',
                                           NULL,
                                           custclass),
         org_code                 = DECODE(UPPER(org_code),
                                           'NULL',
                                           NULL,
                                           org_code),
         welcome_packet           = DECODE(UPPER(welcome_packet),
                                           'NULL',
                                           NULL,
                                           welcome_packet),
         business_type            = DECODE(UPPER(business_type),
                                           'NULL',
                                           NULL,
                                           business_type),
         cust_tax_reference       = DECODE(UPPER(cust_tax_reference),
                                           'NULL',
                                           NULL,
                                           cust_tax_reference),
         referred_by              = DECODE(UPPER(referred_by),
                                           'NULL',
                                           NULL,
                                           referred_by),
         tc_sent                  = DECODE(UPPER(tc_sent),
                                           'NULL',
                                           NULL,
                                           tc_sent),
         no_paper_mail            = DECODE(UPPER(no_paper_mail),
                                           'NULL',
                                           NULL,
                                           no_paper_mail),
         no_email                 = DECODE(UPPER(no_email),
                                           'NULL',
                                           NULL,
                                           no_email),
         customer_class_code      = DECODE(UPPER(customer_class_code),
                                           'NULL',
                                           NULL,
                                           customer_class_code),
         cust_ship_via_code       = DECODE(UPPER(cust_ship_via_code),
                                           'NULL',
                                           NULL,
                                           cust_ship_via_code),
         gl_id_rev                = DECODE(UPPER(gl_id_rev),
                                           'NULL',
                                           NULL,
                                           gl_id_rev),
         derived_tc_sent                 = DECODE(UPPER(derived_tc_sent),
                                           'NULL',
                                           NULL,
                                           derived_tc_sent);


	COMMIT;

--UPDATE XXABRL_AR_CUST_PROFILE_INT
/*

	UPDATE XXABRL_AR_CUST_PROFILE_INT
	SET 
         orig_system_customer_ref      = DECODE(UPPER(orig_system_customer_ref),
                                                'NULL',
                                                NULL,
                                                orig_system_customer_ref),
         orig_system_address_ref       = DECODE(UPPER(orig_system_address_ref),
                                                'NULL',
                                                NULL,
                                                orig_system_address_ref),
         credit_checking               = DECODE(UPPER(credit_checking),
                                                'NULL',
                                                NULL,
                                                credit_checking),
         statements                    = DECODE(UPPER(statements),
                                                'NULL',
                                                NULL,
                                                statements),
         standard_term_name            = DECODE(UPPER(standard_term_name),
                                                'NULL',
                                                NULL,
                                                standard_term_name),
         currency_code                 = DECODE(UPPER(currency_code),
                                                'NULL',
                                                NULL,
                                                currency_code),
         overall_credit_limit          = DECODE(UPPER(overall_credit_limit),
                                                'NULL',
                                                NULL,
                                                overall_credit_limit),
         trx_credit_limit              = DECODE(UPPER(trx_credit_limit),
                                                'NULL',
                                                NULL,
                                                trx_credit_limit),
         site_use_code                 = DECODE(UPPER(site_use_code),
                                                'NULL',
                                                NULL,
                                                site_use_code),
         eh_insured                    = DECODE(UPPER(eh_insured),
                                                'NULL',
                                                NULL,
                                                eh_insured),
         prov_expires                  = DECODE(UPPER(prov_expires),
                                                'NULL',
                                                NULL,
                                                prov_expires),
         credit_app                    = DECODE(UPPER(credit_app),
                                                'NULL',
                                                NULL,
                                                credit_app),
         org_code                      = DECODE(UPPER(org_code),
                                                'NULL',
                                                NULL,
                                                org_code),
         next_credit_review            = DECODE(UPPER(next_credit_review),
                                                'NULL',
                                                NULL,
                                                next_credit_review),
         credit_balance_statement      = DECODE(UPPER(credit_balance_statement),
                                                'NULL',
                                                NULL,
                                                credit_balance_statement),
         credit_class                  = DECODE(UPPER(credit_class),
                                                'NULL',
                                                NULL,
                                                credit_class);

	COMMIT;

--UPDATE XXABRL_AR_CUST_CONTACT_INT


	UPDATE XXABRL_AR_CUST_CONTACT_INT
	SET 
         orig_system_customer_ref   = DECODE(UPPER(orig_system_customer_ref),
                                             'NULL',
                                             NULL,
                                             orig_system_customer_ref),
         orig_system_address_ref    = DECODE(UPPER(orig_system_address_ref),
                                             'NULL',
                                             NULL,
                                             orig_system_address_ref),
         contact_first_name         = DECODE(UPPER(contact_first_name),
                                             'NULL',
                                             NULL,
                                             contact_first_name),
         contact_last_name          = DECODE(UPPER(contact_last_name),
                                             'NULL',
                                             NULL,
                                             contact_last_name),
         contact_job_title          = DECODE(UPPER(contact_job_title),
                                             'NULL',
                                             NULL,
                                             contact_job_title),
         tel1                       = DECODE(UPPER(tel1),
                                             'NULL',
                                             NULL,
                                             tel1),
         tel2                       = DECODE(UPPER(tel2),
                                             'NULL',
                                             NULL,
                                             tel2),
         fax                        = DECODE(UPPER(fax),
                                             'NULL',
                                             NULL,
                                             fax),
         mobile_number               = DECODE(UPPER(mobile_number),
                                             'NULL',
                                             NULL,
                                             mobile_number),
         pager_no                   = DECODE(UPPER(pager_no),
                                             'NULL',
                                             NULL,
                                             pager_no),
         extn_tel1                  = DECODE(UPPER(extn_tel1),
                                             'NULL',
                                             NULL,
                                             extn_tel1),
         extn_tel2                  = DECODE(UPPER(extn_tel2),
                                             'NULL',
                                             NULL,
                                             extn_tel2),
         telephone_type             = DECODE(UPPER(telephone_type),
                                             'NULL',
                                             NULL,
                                             telephone_type),
         telephone_area_code        = DECODE(UPPER(telephone_area_code),
                                             'NULL',
                                             NULL,
                                             telephone_area_code),
         email_address              = DECODE(UPPER(email_address),
                                             'NULL',
                                             NULL,
                                             email_address),
         org_code                   = DECODE(UPPER(org_code),
                                             'NULL',
                                             NULL,
                                             org_code),
         phone_country_code         = DECODE(UPPER(phone_country_code),
                                             'NULL',
                                             NULL,
                                             phone_country_code),
         contact_middle_name        = DECODE(UPPER(contact_middle_name),
                                             'NULL',
                                             NULL,
                                             contact_middle_name),
         primary_telephone_flag     = DECODE(UPPER(primary_telephone_flag),
                                             'NULL',
                                             NULL,
                                             primary_telephone_flag);
--commented as per Kapil's suggestion,24-JUN-2008; error out such records during valdiation.
/*         primary_contact_flag     = DECODE(UPPER(primary_contact_flag),
                                             '??',
                                             NULL,
                                             primary_contact_flag),
         role                     = DECODE(UPPER(role),
                                             '??',
                                             NULL,
                                             role); */

	COMMIT;
/*
	UPDATE  XXABRL_ar_cust_contact_int 
	SET     status_flag = 'E'
		,error_msg  = error_msg ||'; The customer reference does not exist in the customer master'
	WHERE  orig_system_customer_ref NOT IN (SELECT orig_system_customer_ref FROM XXABRL_ar_customer_int)
	AND    orig_system_address_ref IS NULL
	AND    status_flag NOT IN ('E','S'); 
*/

/*	UPDATE  XXABRL_ar_cust_contact_int 
	SET     status_flag = 'E'
		,error_msg  = 'First name and last name, both NULL;No contact point data is available for telephone/email/fax/mobile/pager'
	WHERE  contact_first_name IS  NULL 
	AND    contact_last_name  IS  NULL 
	AND    tel1 IS NULL 
	AND    tel2 IS NULL 
	AND    email_address IS NULL 
	AND    fax IS NULL 
	AND    pager_no IS NULL 
	AND    mobile_number IS NULL
	AND    status_flag NOT IN ('E','S'); */
/*
	UPDATE  XXABRL_ar_cust_contact_int 
	SET     status_flag = 'E'
		,error_msg  = error_msg ||';Primary contact and role cannot be created without a contact person'
	WHERE  contact_first_name   IS NULL 
	AND    contact_last_name    IS NULL 
	AND    primary_contact_flag IS NOT NULL 
	AND    role                 IS NOT NULL   
	AND    status_flag NOT IN ('E','S'); 

--added as per Kapil's suggestion,24-JUN-2008; error out such records during valdiation.
	UPDATE  XXABRL_ar_cust_contact_int 
	SET     status_flag = 'E'
		,error_msg  = error_msg ||';Primary contact flag / role have invalid data'
	WHERE  (primary_contact_flag NOT IN ('Y','N') 
	OR     UPPER(role)                 NOT IN ('BILL TO', 'SHIP TO')   )
	AND    status_flag NOT IN ('E','S'); 

	UPDATE  XXABRL_ar_cust_contact_int 
	SET     status_flag = 'E'
		,error_msg  = error_msg ||';Org Code missing in staging table'
	WHERE  nvl(org_code,'X') = 'X'; 

	UPDATE  XXABRL_ar_cust_profile_int 
	SET     status_flag = 'E'
		,error_msg  = error_msg ||';Org Code missing in staging table'
	WHERE  nvl(org_code,'X') = 'X'; 

	printlog('After update of staging tables; Start FOR Loop', gc_debug_flag);	
*/
	FOR lc_validate IN cur_validate LOOP
	BEGIN
		printlog('Record Number = '|| lc_validate.record_number || ' ,AddressRef#: ' || lc_validate.orig_system_address_ref , gc_debug_flag);
		ln_total_cnt := ln_total_cnt + 1;

		lc_error		  := NULL;
		lc_error_msg		  := NULL;
		ln_org_id		  := NULL;
		ln_salesrep_id		  := NULL;
		lc_country_code		  := NULL;
		lc_customer_name	  := NULL;
		lc_business_type	  := NULL;
		lc_credit_class_code	  := NULL;
		ln_payment_term_id	  := NULL;
		lc_ship_via_code	  := NULL;
		ln_gl_rev_acct_id	  := NULL;
		ln_gl_freight_acct_id	  := NULL; 
		ln_gl_rec_acct_id	  := NULL;
		ln_gl_tax_acct_id	  := NULL;
		ln_gl_clearing_acct_id    := NULL;
		ln_gl_unbilled_acct_id    := NULL;
		ln_gl_unearned_acct_id    := NULL;
		ln_warehouse_id		  := NULL;

		ln_org_id := get_org_id( p_org_code  => lc_validate.org_code
		                        ,x_error_msg => lc_error_msg);
                                                        
		IF ln_org_id = -1 or lc_error_msg IS NOT NULL THEN
			lc_error := lc_error || '; '|| lc_error_msg;
		END IF;
		printlog('Derived data: org_id = '|| ln_org_id , gc_debug_flag);	
    
		IF (lc_validate.country IS NULL) OR (lc_validate.address1 IS NULL) THEN
			lc_error := lc_error||'; '|| 'Location: Address1 OR Country is NULL';
			printlog('Location: Address1 OR Country is NULL', gc_debug_flag);	
		END IF;


		--Customer Name shd not be NULL for Organization
		IF (lc_validate.customer_name IS NULL) AND (NVL(lc_validate.person_flag, 'N') = 'N') THEN
			lc_error := lc_error||'; '|| 'For Party_type = ORGANIZATION, customer_name column is NULL';
			printlog('For Party_type = ORGANIZATION, customer_name column is NULL', gc_debug_flag);	
/*		ELSIF lc_validate.customer_name IS NOT NULL THEN
		--Customer Name shd not be existing in Oracle already
--			lc_customer_name := NVL(lc_validate.customer_name,lc_validate.person_first_name || ' ' ||lc_validate.person_last_name);
			
			check_customer_exists( lc_validate.customer_name, lc_error_msg);
			IF  lc_error_msg IS NOT NULL THEN
				lc_error := lc_error ||'; '|| lc_error_msg;
				printlog(lc_error_msg, gc_debug_flag);
			END IF; */
		END IF;
		
		-- Last Name shd not be NULL for Person
		IF (lc_validate.person_first_name IS NULL) AND (lc_validate.person_last_name IS NULL) AND (NVL(lc_validate.person_flag, 'N') = 'Y') THEN
			lc_error := lc_error||'; '|| 'For Party_type = PERSON, First and Last_name column is NULL';
			printlog('For Party_type = PERSON, First and Last_name column is NULL', gc_debug_flag);	
/*		ELSIF lc_validate.person_last_name IS NOT NULL THEN
		--Customer Name shd not be existing in Oracle already
--			lc_customer_name := NVL(lc_validate.customer_name,lc_validate.person_first_name || ' ' ||lc_validate.person_last_name);
			
			check_customer_exists( lc_validate.person_first_name || ' ' || lc_validate.person_last_name, lc_error_msg);
			IF  lc_error_msg IS NOT NULL THEN
				lc_error := lc_error ||'; '|| lc_error_msg;
				printlog(lc_error_msg, gc_debug_flag);
			END IF;
*/
		END IF;

		--Validate CCID for GL REV accounts
--20-JUN-08, all GL accounts to be derived for AEE and REC
		IF lc_validate.site_use_code = 'BILL_TO'  THEN --AND  lc_validate.org_code <> 'RECOU' THEN
			get_gl_rev_acct_id(lc_validate.org_code,lc_validate.REVENUE_ACCOUNT, ln_gl_rev_acct_id, lc_error_msg);
			IF  lc_error_msg IS NOT NULL THEN
				lc_error := lc_error ||'; '|| lc_error_msg;
				printlog(lc_error_msg, gc_debug_flag);
			END IF;

			--Validate CCID for GL Freight account
			
			get_gl_freight_acct_id(lc_validate.org_code,lc_validate.FREIGHT_ACCOUNT, ln_gl_freight_acct_id, lc_error_msg);
			IF  lc_error_msg IS NOT NULL THEN
				lc_error := lc_error ||'; '|| lc_error_msg;
				printlog(lc_error_msg, gc_debug_flag);
			END IF;

			--Validate CCID for GL Receivable account , added on 20-JUN-08

			get_gl_rec_acct_id(lc_validate.org_code, lc_validate.RECEIVABLE_ACCOUNT, ln_gl_rec_acct_id, lc_error_msg);
			IF  lc_error_msg IS NOT NULL THEN
				lc_error := lc_error ||'; '|| lc_error_msg;
				printlog(lc_error_msg, gc_debug_flag);
			END IF;

			--Validate CCID for GL Tax account , added on 20-JUN-08

			get_gl_tax_acct_id(lc_validate.org_code, lc_validate.TAX_ACCOUNT, ln_gl_tax_acct_id, lc_error_msg);
			IF  lc_error_msg IS NOT NULL THEN
				lc_error := lc_error ||'; '|| lc_error_msg;
				printlog(lc_error_msg, gc_debug_flag);
			END IF;

			--Validate CCID for GL Clearing account , added on 20-JUN-08

			get_gl_clearing_acct_id(lc_validate.org_code, lc_validate.CLEARING_ACCOUNT, ln_gl_clearing_acct_id, lc_error_msg);
			IF  lc_error_msg IS NOT NULL THEN
				lc_error := lc_error ||'; '|| lc_error_msg;
				printlog(lc_error_msg, gc_debug_flag);
			END IF;
			--Validate CCID for GL Unbilled account , added on 20-JUN-08

			get_gl_unbilled_acct_id(lc_validate.org_code, lc_validate.UNBILLED_ACCOUNT, ln_gl_unbilled_acct_id, lc_error_msg);
			IF  lc_error_msg IS NOT NULL THEN
				lc_error := lc_error ||'; '|| lc_error_msg;
				printlog(lc_error_msg, gc_debug_flag);
			END IF;
			--Validate CCID for GL Unearned account , added on 20-JUN-08

			get_gl_unearned_acct_id(lc_validate.org_code, lc_validate.UNEARNED_ACCOUNT, ln_gl_unearned_acct_id, lc_error_msg);
			IF  lc_error_msg IS NOT NULL THEN
				lc_error := lc_error ||'; '|| lc_error_msg;
				printlog(lc_error_msg, gc_debug_flag);
			END IF;

		END IF;	

		--Validate SHIP_VIA
		IF lc_validate.cust_ship_via_code IS NOT NULL THEN
			get_ship_via_code(lc_validate.cust_ship_via_code, lc_ship_via_code, lc_error_msg);		
			IF  lc_error_msg IS NOT NULL THEN
				lc_error := lc_error ||'; '|| lc_error_msg;
				printlog(lc_error_msg, gc_debug_flag);
			END IF;
		END IF;			
			
		--Validate Sales Rep
		IF lc_validate.salesperson IS NOT NULL THEN
			get_salesrep_id(lc_validate.salesperson, ln_salesrep_id, lc_error_msg);
			IF  lc_error_msg IS NOT NULL THEN
				lc_error := lc_error ||'; '|| lc_error_msg;
				printlog(lc_error_msg, gc_debug_flag);
			END IF;
		END IF;

		--Validate Payment Terms
		IF lc_validate.site_use_code = 'BILL_TO' THEN
			get_payment_term(lc_validate.orig_system_customer_ref, lc_validate.orig_system_address_ref, ln_payment_term_id, lc_error_msg);
			IF  lc_error_msg IS NOT NULL THEN
				lc_error := lc_error ||'; '|| lc_error_msg;
				printlog(lc_error_msg, gc_debug_flag);
			END IF;
		END IF;
		
		--Validate Cust Credit Class
		get_credit_class(lc_validate.orig_system_customer_ref,lc_validate.orig_system_address_ref, lc_credit_class_code, lc_error_msg);
		IF  lc_error_msg IS NOT NULL THEN
			lc_error := lc_error ||'; '|| lc_error_msg;
			printlog(lc_error_msg, gc_debug_flag);
		END IF;
													 
		--Validate Country
		get_country_code(lc_validate.country, lc_country_code, lc_error_msg);
		IF  lc_error_msg IS NOT NULL THEN
			lc_error := lc_error ||'; '|| lc_error_msg;
			printlog(lc_error_msg, gc_debug_flag);
		END IF;

    validate_address_ref(lc_validate.ORIG_SYSTEM_ADDRESS_REF,lc_error_msg);
		IF  lc_error_msg IS NOT NULL THEN
			lc_error := lc_error ||'; '|| lc_error_msg;
			printlog(lc_error_msg, gc_debug_flag);
		END IF;



		
		--Validate Customer Number
--		validate_cust_number(lc_validate.customer_number, lc_error_msg);
/*		IF  lc_error_msg IS NOT NULL THEN
			lc_error := lc_error ||'; '|| lc_error_msg;
			printlog(lc_error_msg, gc_debug_flag);
		END IF;
*/
		--Validate Business Type

/*		IF (ln_org_id IS NOT NULL) AND (lc_validate.business_type IS NOT NULL) THEN -- validate only for AEE

			get_business_type(lc_validate.business_type, lc_business_type, lc_error_msg);
			
			IF  lc_error_msg IS NOT NULL THEN
				lc_error := lc_error ||'; '|| lc_error_msg;
				printlog(lc_error_msg, gc_debug_flag);
			END IF;

		END IF;

		IF (ln_org_id IS NOT NULL) AND (lc_validate.warehouse IS NOT NULL) THEN 

			ln_warehouse_id := get_warehouse_id ( lc_validate.warehouse, ln_org_id, lc_error_msg);

			IF  lc_error_msg IS NOT NULL THEN
				lc_error := lc_error ||'; '|| lc_error_msg;
				printlog(lc_error_msg, gc_debug_flag);
			END IF;
		END IF;
*/

		IF (lc_validate.org_code IS NULL) THEN 
			lc_error_msg := 'Org Code missing in staging table';
			lc_error := lc_error ||'; '|| lc_error_msg;
			printlog(lc_error_msg, gc_debug_flag);
		END IF;

		printlog('Error for this record '||lc_error, gc_debug_flag);
		
		IF lc_error IS NOT NULL THEN

			UPDATE XXABRL_AR_CUSTOMER_INT 
			SET    status_flag       = 'E'
			       ,error_msg        = lc_error
			       ,last_updated_by  = gn_user_id
			       ,created_by       = gn_user_id
			       ,request_id       = gn_request_id
			       ,last_update_date = sysdate
			WHERE record_number = lc_validate.record_number;
			ln_error_cnt := ln_error_cnt + 1;
			printlog('-----------RECORD INVALID--------', gc_debug_flag);

		ELSIF lc_error IS NULL THEN

			UPDATE XXABRL_AR_CUSTOMER_INT 
			SET     status_flag           = 'V'
			       ,error_msg             = NULL
			       ,org_id                = ln_org_id
			       ,country_code          = lc_country_code
			       ,salesrep_id           = ln_salesrep_id
			       ,derived_business_type = lc_business_type
			       ,credit_class_code     = lc_credit_class_code
			       ,payment_term_id       = ln_payment_term_id
			       ,derived_ship_via_code = lc_ship_via_code
			       ,gl_rev_acct_id        = ln_gl_rev_acct_id
			       ,gl_freight_acct_id    = ln_gl_freight_acct_id
			       ,gl_rec_acct_id        = ln_gl_rec_acct_id
			       ,gl_tax_acct_id        = ln_gl_tax_acct_id
			       ,gl_clearing_acct_id   = ln_gl_clearing_acct_id
			       ,gl_unbilled_acct_id   = ln_gl_unbilled_acct_id
			       ,gl_unearned_acct_id   = ln_gl_unearned_acct_id
			       ,warehouse_id          = ln_warehouse_id
			       ,last_updated_by       = gn_user_id
			       ,created_by            = gn_user_id
			       ,request_id            = gn_request_id
			       ,last_update_date      = SYSDATE
			WHERE record_number = lc_validate.record_number;
			ln_valid_cnt := ln_valid_cnt + 1;
			printlog('-----------RECORD VALIDATED--------', gc_debug_flag);

		END IF;
		
		printlog('', gc_debug_flag);		

		ln_loop_cnt := ln_loop_cnt + 1;

		IF ln_loop_cnt > 100 THEN
			ln_loop_cnt :=0;
			COMMIT;
		END IF;
	END;
	END LOOP;

        printlog(' Total Records in Customer Master staging table =  ' || ln_total_cnt, gc_debug_flag);
        printlog(' Valid   Record Count =  ' || ln_valid_cnt, gc_debug_flag);
        printlog(' INValid Record Count =  ' || ln_error_cnt, gc_debug_flag);
	printlog('- - - - - - - - - - - - - - - - - - - - - - - - - - - - -  ', gc_debug_flag);
	printlog(' ', gc_debug_flag);
	COMMIT;

	
EXCEPTION
	WHEN OTHERS THEN
		x_error_msg := 'Error in MAIN validation program' || SQLERRM;
		printlog( x_error_msg, gc_debug_flag);
END validate_customer_data;

PROCEDURE get_country_code (
 p_country       IN VARCHAR2
,x_country_code OUT VARCHAR2
,x_error_msg    OUT VARCHAR2
)
AS

BEGIN
	SELECT territory_code
	INTO   x_country_code
	FROM   FND_TERRITORIES_TL
	WHERE  UPPER(territory_short_name) = UPPER(p_country);
EXCEPTION
	WHEN NO_DATA_FOUND THEN
		x_error_msg := 'Country code:' || p_country || ' not defined in Oracle';

	WHEN OTHERS THEN
		x_error_msg := 'Exception in get_country_code for  :' || p_country;
END get_country_code;


PROCEDURE validate_address_ref(
 p_orig_system_address_ref  IN  VARCHAR2
,x_error_msg	 OUT  VARCHAR2)AS
v_orig_system_reference varchar2(200);
Begin
		SELECT orig_system_reference
		INTO   v_orig_system_reference
		FROM   HZ_LOCATIONS
		WHERE  TRIM(orig_system_reference) = p_orig_system_address_ref
    and rownum =1;
    x_error_msg :='Orig System Address Ref already exists: '||p_orig_system_address_ref;
Exception
When no_data_found then
NULL;

End validate_address_ref;
	
PROCEDURE get_credit_class(
 p_sys_cust_ref         IN VARCHAR2
,p_sys_addr_ref         IN VARCHAR2
,x_credit_class_code  OUT VARCHAR2
,x_error_msg          OUT VARCHAR2
)
AS
	lc_credit_class          VARCHAR2(240);

BEGIN
	printlog( 'Get credit class for Address Ref: '|| p_sys_addr_ref ||'  Customer Ref: '|| p_sys_cust_ref , gc_debug_flag);

	NULL;
	/*
	SELECT credit_class 
	INTO   lc_credit_class
	FROM   XXABRL_AR_CUST_PROFILE_INT 
	WHERE  UPPER(orig_system_address_ref) = UPPER(p_sys_addr_ref)
	AND    UPPER(orig_system_customer_ref) = UPPER(p_sys_cust_ref);
	*/

	IF lc_credit_class IS NOT NULL THEN
	BEGIN
		SELECT UPPER(TRIM(lookup_code))
		INTO   x_credit_class_code
		FROM   AR_LOOKUPS
		WHERE  lookup_type    = 'AR_CMGT_CREDIT_CLASSIFICATION'
		AND    UPPER(meaning) = UPPER(lc_credit_class)
		AND    ROWNUM = 1;
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			x_error_msg := 'Credit Class: '|| lc_credit_class || ' not defined in Oracle';
	END;	
	END IF;
EXCEPTION
	WHEN NO_DATA_FOUND THEN
		x_error_msg := NULL;
	WHEN OTHERS THEN
		x_error_msg := 'Unexpected error in get_credit_class: ' || SQLERRM;
		printlog( x_error_msg , gc_debug_flag);
END get_credit_class;

	
PROCEDURE get_payment_term(
 p_sys_cust_ref         IN VARCHAR2
,p_sys_addr_ref         IN VARCHAR2
,x_payment_term_id OUT NUMBER
,x_error_msg       OUT VARCHAR2)
AS
	lc_term    VARCHAR2(200);
	ln_term_id NUMBER;
BEGIN
	printlog( 'Get payment term for Address Ref: '|| p_sys_addr_ref ||'  Customer Ref: '|| p_sys_cust_ref , gc_debug_flag);
	/*
	SELECT  standard_term_name
	INTO   lc_term
	FROM (
		SELECT standard_term_name 
		FROM   XXABRL_AR_CUST_PROFILE_INT 
		WHERE  UPPER(orig_system_address_ref) = UPPER(p_sys_addr_ref)
		AND    UPPER(orig_system_customer_ref) = UPPER(p_sys_cust_ref)
		UNION
		SELECT standard_term_name 
		FROM   XXABRL_AR_CUST_PROFILE_INT 
		WHERE  UPPER(orig_system_address_ref) = UPPER(p_sys_addr_ref)
--		AND    UPPER(orig_system_customer_ref) = UPPER(p_sys_cust_ref);
		);

	*/
	--lc_term := '1N30'; --Code Change

	IF lc_term IS NULL THEN
		
		x_error_msg := NULL; --'Payment term is NULL for address_ref:'|| p_sys_add_ref;
	
	ELSIF lc_term IS NOT NULL THEN
	
	BEGIN

		printlog( 'Term from stg table '|| lc_term , gc_debug_flag);

		SELECT term_id
		INTO   x_payment_term_id
		FROM   RA_TERMS
		WHERE  UPPER(TRIM(name))             = UPPER(lc_term)
		AND    nvl(end_Date_active,sysdate) >= sysdate;

	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			x_error_msg := 'Payment term ' || lc_term || ' not defined in Oracle';
	END;
	
	END IF;

EXCEPTION 
	WHEN NO_DATA_FOUND THEN
		
		x_error_msg := 'Payment term not given in extract for BILL_TO address_ref:'|| p_sys_addr_ref;

	WHEN OTHERS THEN
		
		x_error_msg := 'Unexpected error in get_payment_term: ' || SQLERRM;
		printlog( x_error_msg , gc_debug_flag);

END get_payment_term;
	
PROCEDURE get_salesrep_id(
 p_salesperson   IN VARCHAR2
,x_sales_rep_id OUT NUMBER
,x_error_msg    OUT VARCHAR2
)
AS

BEGIN

	SELECT JRS.salesrep_id
	INTO   x_sales_rep_id
	FROM   JTF_RS_SALESREPS      JRS
	WHERE  UPPER(TRIM(JRS.name)) = UPPER(p_salesperson);
	
EXCEPTION
	WHEN NO_DATA_FOUND THEN
		BEGIN
			SELECT JRS.salesrep_id
			INTO   x_sales_rep_id
			FROM   JTF_RS_SALESREPS      JRS
			      ,JTF_RS_DEFRESOURCES_V JRD
			WHERE  UPPER(JRD.resource_name) = UPPER(p_salesperson)
			AND    JRD.resource_id = JRS.resource_id;
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				x_error_msg := 'Sales person ' ||p_salesperson ||' is not defined in Oracle' ;
		END;
	WHEN OTHERS THEN
		x_error_msg := 'Unexpected error in get_salesrep_id ' || SQLERRM;
		printlog( x_error_msg , gc_debug_flag);
END get_salesrep_id;



PROCEDURE get_ship_via_code(
 p_ship_via          IN VARCHAR2
,x_ship_method_code OUT VARCHAR2
,x_error_msg        OUT VARCHAR2
)
AS
	lc_ship_method_code VARCHAR2(100);
BEGIN

	SELECT WCS.ship_method_code
	INTO   x_ship_method_code
	FROM   WSH_CARRIERS_V         WC,
	       WSH_CARRIER_SERVICES_V WCS,
	       FND_LOOKUP_VALUES      FL
	WHERE WC.carrier_id = WCS.carrier_id
	AND   UPPER(WCS.service_level) = UPPER(FL.lookup_code)
	AND   UPPER(FL.lookup_type) = 'WSH_SERVICE_LEVELS'
	AND   FL.enabled_flag = 'Y'
	AND   UPPER(WC.freight_code) = UPPER(p_ship_via)
	AND   ROWNUM = 1;

EXCEPTION
	WHEN NO_DATA_FOUND THEN
		x_error_msg := 'SHIP-VIA / Ship Method: ' || p_ship_via || ' is not defined in Oracle ';
	WHEN OTHERS THEN
		x_error_msg := 'Unexpected error in get_ship_via_code: ' || SQLERRM;
		printlog( x_error_msg , gc_debug_flag);
END get_ship_via_code;


PROCEDURE validate_location(
 p_customer   IN XXABRL_AR_CUSTOMER_INT%ROWTYPE
,x_error_msg OUT VARCHAR2) 
AS
	ln_rec_count NUMBER := NULL;
BEGIN
	SELECT location_id
	INTO   ln_rec_count
	FROM   HZ_LOCATIONS
	WHERE  UPPER(TRIM(address1)) = UPPER(p_customer.address1)
	AND    UPPER(TRIM(country))  = UPPER(p_customer.country);

	IF ln_rec_count IS NOT NULL THEN
		x_error_msg := 'Location for : ' || p_customer.orig_system_address_ref || ' already exists';
	END IF;

EXCEPTION
	WHEN NO_DATA_FOUND THEN
		x_error_msg := NULL;
	WHEN OTHERS THEN
		x_error_msg := 'Unexpected error in validate_location: ' || SQLERRM;
		printlog( x_error_msg , gc_debug_flag);
END validate_location;

PROCEDURE get_gl_rev_acct_id(
 p_org_code        IN VARCHAR2
,p_gl_id_rev       IN VARCHAR2
,x_gl_rev_acct_id OUT NUMBER
,x_error_msg      OUT VARCHAR2) 
AS
	vn_structure_id    NUMBER;
BEGIN
	SELECT id_flex_num
	INTO   vn_structure_id
	FROM   FND_ID_FLEX_STRUCTURES_VL
	WHERE  UPPER(id_flex_structure_code) = 'ABRL_ACCOUNTING_FLEX'
	AND    id_flex_code                  = 'GL#' ;


		x_gl_rev_acct_id := fnd_flex_ext.get_ccid (
					application_short_name => 'SQLGL',
					key_flex_code          => 'GL#',
					structure_number       => vn_structure_id, -- 50308,
					validation_date        => to_char(SYSDATE,'DD-MON-YYYY'),
					concatenated_segments  => p_gl_id_rev );

	IF x_gl_rev_acct_id <= 0 THEN
		x_error_msg := 'Error creating GL Revenue Acct:' ||fnd_flex_ext.get_message;
	ELSE
		printlog( 'Derived GL Revenue account id : '|| x_gl_rev_acct_id || ' for OU: '|| p_org_code , gc_debug_flag);
	END IF;

EXCEPTION
	WHEN OTHERS THEN
		x_error_msg := 'Unexpected error in get_gl_rev_acct_id: ' || SQLERRM;
		printlog( x_error_msg , gc_debug_flag);
		x_gl_rev_acct_id := 0;
END get_gl_rev_acct_id;

PROCEDURE get_gl_freight_acct_id(
 p_org_code            IN VARCHAR2
,p_gl_id_rev           IN VARCHAR2
,x_gl_freight_acct_id OUT NUMBER
,x_error_msg          OUT VARCHAR2)
AS

	vn_structure_id      NUMBER;
	lc_freight_code      VARCHAR2(100);

BEGIN

	SELECT id_flex_num
	INTO   vn_structure_id
	FROM   FND_ID_FLEX_STRUCTURES_VL
	WHERE  UPPER(id_flex_structure_code) = 'ABRL_ACCOUNTING_FLEX'
	AND    id_flex_code                  = 'GL#' ;



		x_gl_freight_acct_id := fnd_flex_ext.get_ccid (
					application_short_name => 'SQLGL',
					key_flex_code          => 'GL#',
					structure_number       => vn_structure_id, --50308,
					validation_date        => to_char(SYSDATE,'DD-MON-YYYY'),
					concatenated_segments  => p_gl_id_rev);


	IF x_gl_freight_acct_id <= 0 THEN
		x_error_msg := 'Error creating GL Freight Acct:' ||fnd_flex_ext.get_message;
	ELSE
		printlog( 'Derived GL Freight account id : '|| x_gl_freight_acct_id || ' for OU: '|| p_org_code , gc_debug_flag);
	END IF;
EXCEPTION
	WHEN OTHERS THEN
		x_error_msg := 'Unexpected error in get_gl_freight_acct_id: ' || SQLERRM;
		printlog( x_error_msg , gc_debug_flag);
		x_gl_freight_acct_id := 0;
END get_gl_freight_acct_id;

PROCEDURE get_gl_rec_acct_id(
 p_org_code        IN VARCHAR2
,p_gl_id_rev       IN VARCHAR2
,x_gl_rec_acct_id OUT NUMBER
,x_error_msg      OUT VARCHAR2) 
AS
	vn_structure_id    NUMBER;
BEGIN
	SELECT id_flex_num
	INTO   vn_structure_id
	FROM   FND_ID_FLEX_STRUCTURES_VL
	WHERE  UPPER(id_flex_structure_code) = 'ABRL_ACCOUNTING_FLEX'
	AND    id_flex_code                  = 'GL#' ;

		x_gl_rec_acct_id := fnd_flex_ext.get_ccid (
					application_short_name => 'SQLGL',
					key_flex_code          => 'GL#',
					structure_number       => vn_structure_id, -- 50308,
					validation_date        => to_char(SYSDATE,'DD-MON-YYYY'),
					concatenated_segments  => p_gl_id_rev);

	IF x_gl_rec_acct_id <= 0 THEN
		x_error_msg := 'Error creating GL Receivable Acct:' ||fnd_flex_ext.get_message;
	ELSE
		printlog( 'Derived GL Receivable account id : '|| x_gl_rec_acct_id || ' for OU: '|| p_org_code , gc_debug_flag);
	END IF;

EXCEPTION
	WHEN OTHERS THEN
		x_error_msg := 'Unexpected error in get_gl_rec_acct_id: ' || SQLERRM;
		printlog( x_error_msg , gc_debug_flag);
		x_gl_rec_acct_id := 0;
END get_gl_rec_acct_id;

PROCEDURE get_gl_tax_acct_id(
 p_org_code        IN VARCHAR2
,p_gl_id_rev       IN VARCHAR2
,x_gl_tax_acct_id OUT NUMBER
,x_error_msg      OUT VARCHAR2) 
AS
	vn_structure_id    NUMBER;
BEGIN
	SELECT id_flex_num
	INTO   vn_structure_id
	FROM   FND_ID_FLEX_STRUCTURES_VL
	WHERE  UPPER(id_flex_structure_code) = 'ABRL_ACCOUNTING_FLEX'
	AND    id_flex_code                  = 'GL#' ;

		x_gl_tax_acct_id := fnd_flex_ext.get_ccid (
					application_short_name => 'SQLGL',
					key_flex_code          => 'GL#',
					structure_number       => vn_structure_id, -- 50308,
					validation_date        => to_char(SYSDATE,'DD-MON-YYYY'),
					concatenated_segments  => p_gl_id_rev);

	IF x_gl_tax_acct_id <= 0 THEN
		x_error_msg := 'Error creating GL Tax Acct:' ||fnd_flex_ext.get_message;
	ELSE
		printlog( 'Derived GL Tax account id : '|| x_gl_tax_acct_id || ' for OU: '|| p_org_code , gc_debug_flag);
	END IF;

EXCEPTION
	WHEN OTHERS THEN
		x_error_msg := 'Unexpected error in get_gl_tax_acct_id: ' || SQLERRM;
		printlog( x_error_msg , gc_debug_flag);
		x_gl_tax_acct_id := 0;
END get_gl_tax_acct_id;

PROCEDURE get_gl_clearing_acct_id(
 p_org_code             IN VARCHAR2
,p_gl_id_rev            IN VARCHAR2
,x_gl_clearing_acct_id OUT NUMBER
,x_error_msg           OUT VARCHAR2) 
AS
	vn_structure_id    NUMBER;
BEGIN
	SELECT id_flex_num
	INTO   vn_structure_id
	FROM   FND_ID_FLEX_STRUCTURES_VL
	WHERE  UPPER(id_flex_structure_code) = 'ABRL_ACCOUNTING_FLEX'
	AND    id_flex_code                  = 'GL#' ;

		x_gl_clearing_acct_id := fnd_flex_ext.get_ccid (
					application_short_name => 'SQLGL',
					key_flex_code          => 'GL#',
					structure_number       => vn_structure_id, -- 50308,
					validation_date        => to_char(SYSDATE,'DD-MON-YYYY'),
					concatenated_segments  => p_gl_id_rev);

	IF x_gl_clearing_acct_id <= 0 THEN
		x_error_msg := 'Error creating GL Clearing Acct:' ||fnd_flex_ext.get_message;
	ELSE
		printlog( 'Derived GL Clearing account id : '|| x_gl_clearing_acct_id || ' for OU: '|| p_org_code , gc_debug_flag);
	END IF;

EXCEPTION
	WHEN OTHERS THEN
		x_error_msg := 'Unexpected error in get_gl_clearing_acct_id: ' || SQLERRM;
		printlog( x_error_msg , gc_debug_flag);
		x_gl_clearing_acct_id := 0;
END get_gl_clearing_acct_id;

PROCEDURE get_gl_unbilled_acct_id(
 p_org_code             IN VARCHAR2
,p_gl_id_rev            IN VARCHAR2
,x_gl_unbilled_acct_id OUT NUMBER
,x_error_msg           OUT VARCHAR2) 
AS
	vn_structure_id    NUMBER;
BEGIN
	SELECT id_flex_num
	INTO   vn_structure_id
	FROM   FND_ID_FLEX_STRUCTURES_VL
	WHERE  UPPER(id_flex_structure_code) = 'ABRL_ACCOUNTING_FLEX'
	AND    id_flex_code                  = 'GL#' ;

		x_gl_unbilled_acct_id := fnd_flex_ext.get_ccid (
					application_short_name => 'SQLGL',
					key_flex_code          => 'GL#',
					structure_number       => vn_structure_id, -- 50308,
					validation_date        => to_char(SYSDATE,'DD-MON-YYYY'),
					concatenated_segments  => p_gl_id_rev);

	IF x_gl_unbilled_acct_id <= 0 THEN
		x_error_msg := 'Error creating GL Unbilled Acct:' ||fnd_flex_ext.get_message;
	ELSE
		printlog( 'Derived GL Unbilled account id : '|| x_gl_unbilled_acct_id || ' for OU: '|| p_org_code , gc_debug_flag);
	END IF;

EXCEPTION
	WHEN OTHERS THEN
		x_error_msg := 'Unexpected error in get_gl_unbilled_acct_id: ' || SQLERRM;
		printlog( x_error_msg , gc_debug_flag);
		x_gl_unbilled_acct_id := 0;
END get_gl_unbilled_acct_id;

PROCEDURE get_gl_unearned_acct_id(
 p_org_code             IN VARCHAR2
,p_gl_id_rev            IN VARCHAR2
,x_gl_unearned_acct_id OUT NUMBER
,x_error_msg           OUT VARCHAR2) 
AS
	vn_structure_id    NUMBER;
BEGIN
	SELECT id_flex_num
	INTO   vn_structure_id
	FROM   FND_ID_FLEX_STRUCTURES_VL
	WHERE  UPPER(id_flex_structure_code) = 'ABRL_ACCOUNTING_FLEX'
	AND    id_flex_code                  = 'GL#' ;

		x_gl_unearned_acct_id := fnd_flex_ext.get_ccid (
					application_short_name => 'SQLGL',
					key_flex_code          => 'GL#',
					structure_number       => vn_structure_id, -- 50308,
					validation_date        => to_char(SYSDATE,'DD-MON-YYYY'),
					concatenated_segments  => p_gl_id_rev);

	IF x_gl_unearned_acct_id <= 0 THEN
		x_error_msg := 'Error creating GL Unearned Acct:' ||fnd_flex_ext.get_message;
	ELSE
		printlog( 'Derived GL Unearned account id : '|| x_gl_unearned_acct_id || ' for OU: '|| p_org_code , gc_debug_flag);
	END IF;

EXCEPTION
	WHEN OTHERS THEN
		x_error_msg := 'Unexpected error in get_gl_unearned_acct_id: ' || SQLERRM;
		printlog( x_error_msg , gc_debug_flag);
		x_gl_unearned_acct_id := 0;
END get_gl_unearned_acct_id;

PROCEDURE check_customer_exists(
 p_cust_name    IN VARCHAR2
,x_error_msg   OUT VARCHAR2)
AS
	ln_rec_count NUMBER := NULL;
BEGIN
	SELECT hp.party_id
	INTO   ln_rec_count
	FROM   HZ_CUST_ACCOUNTS HCA
	      ,HZ_PARTIES       HP
	WHERE  UPPER(party_name)   = UPPER(p_cust_name)
	AND    HCA.party_id = hp.party_id;

	x_error_msg := 'Customer/Person: ' || p_cust_name || ' already exists in Oracle';

EXCEPTION
	WHEN NO_DATA_FOUND THEN
		printlog( 'Customer/Person: ' || p_cust_name || ' is New; valid record', gc_debug_flag);
		x_error_msg := NULL; 
	WHEN OTHERS THEN
		x_error_msg := 'Unexpected error in check_customer_exists: ' || SQLERRM;
END check_customer_exists;

PROCEDURE validate_cust_number(
 p_cust_number     IN VARCHAR2
,x_error_msg      OUT VARCHAR2
) 
AS
	ln_rec_count VARCHAR2(30) := NULL;
BEGIN

	SELECT hcs.account_number
	INTO   ln_rec_count
	FROM   HZ_CUST_ACCOUNTS HCS
	WHERE TRIM(HCS.account_number) = p_cust_number;

	IF ln_rec_count IS NOT NULL THEN
		x_error_msg := 'Customer Account Number: ' || p_cust_number ||' already exists in Oracle';
	END IF;
EXCEPTION
	WHEN NO_DATA_FOUND THEN
		x_error_msg := NULL;
	WHEN OTHERS THEN
		x_error_msg := 'Unexpected error in validate_cust_number: ' || SQLERRM;
END validate_cust_number;

PROCEDURE get_business_type(
 p_business_type IN VARCHAR2
,x_business_type OUT VARCHAR2
,x_error_msg	 OUT  VARCHAR2
)
AS

BEGIN

	SELECT flex_value
	INTO   x_business_type
	FROM   FND_FLEX_VALUES_VL
	WHERE  flex_value_set_id = (SELECT flex_value_set_id
				FROM  FND_FLEX_VALUE_SETS
				WHERE --description         = 'Customer Business Type'
				flex_value_set_name = 'MSENERGY_AEE BUSINESS TYPE')
	AND    UPPER(TRIM(flex_value_meaning)) = UPPER(p_business_type)
	AND    ROWNUM = 1;

EXCEPTION
	WHEN NO_DATA_FOUND THEN
		x_error_msg := 'Business Type: '|| p_business_type || ' is not defined in Oracle';
	WHEN OTHERS THEN
		x_error_msg := 'Unexpected error in get_business_type: ' || SQLERRM;
END get_business_type;


FUNCTION get_org_id(
 p_org_code       IN  VARCHAR2
,x_error_msg	 OUT  VARCHAR2)
RETURN NUMBER
IS
	ln_org_id   NUMBER;

BEGIN

	SELECT organization_id
	INTO   ln_org_id
	FROM   HR_OPERATING_UNITS
	WHERE  upper(NAME) = UPPER(p_org_code);

	RETURN ln_org_id;
EXCEPTION
	WHEN no_data_found THEN
		x_error_msg := 'Org_code: '||p_org_code||' is not defined in Oracle ';
		RETURN -1;
	WHEN OTHERS THEN
		x_error_msg := 'Unexpected error in get_org_id: ' || SQLERRM;
		printlog( x_error_msg, gc_debug_flag);
		RETURN -1;
END get_org_id;

FUNCTION get_warehouse_id (
 p_warehouse      IN  VARCHAR2
,p_org_id         IN  VARCHAR2
,x_error_msg	 OUT  VARCHAR2
)
RETURN NUMBER
IS
	ln_warehouse_id   NUMBER;
BEGIN

	printlog('Warehouse_code: '|| p_warehouse, gc_debug_flag);

	SELECT organization_id
	INTO   ln_warehouse_id
	FROM   ORG_ORGANIZATION_DEFINITIONS OOD
	WHERE  OOD.operating_unit    = p_org_id
	AND    UPPER(OOD.organization_code) = UPPER(p_warehouse);

	printlog('Warehouse_id: ' || ln_warehouse_id, gc_debug_flag);

	RETURN ln_warehouse_id;
EXCEPTION
	WHEN NO_DATA_FOUND THEN
		x_error_msg := 'Warehouse: '||p_warehouse  || ' mapping not defined in Oracle';
		RETURN -1;
	WHEN OTHERS THEN
		x_error_msg := 'Unexpected error in get_warehouse_id: ' || SQLERRM;
		printlog( x_error_msg, gc_debug_flag);
		RETURN -1;
END get_warehouse_id;

END XXABRL_AR_CUST_IMPORT_PKG;
/


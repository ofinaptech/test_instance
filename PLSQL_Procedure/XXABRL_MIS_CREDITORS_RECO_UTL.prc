CREATE OR REPLACE PROCEDURE APPS.XXABRL_MIS_CREDITORS_RECO_UTL(ERRBUF  OUT VARCHAR2,
                                                                RETCODE OUT VARCHAR2,
                                                                P_FROM_DATE Varchar2 ,
                                                                P_TO_DATE Varchar2
                                                   ) AS
                                                   /*
/**********************************************************************************************************************************************
             Name: MIS Creditors Reconciliation Program
            Change Record:
           =====================
=========================
=========================
=========================
=========================
           Version   Date          Author                    Remarks                  Documnet Ref
           =======   ==========   ==
===========             ==================  ===============
=========================
=============
           1.0.0    11-Feb-2014    AMIT                     Initial Version
                                                            
  ************************************************************************************************************************************************/
--Declaring MIS Creditors Detail Local Variables
l_file_p            UTL_FILE.file_type;
--l_pmt_data          VARCHAR2 (4000);
P_L_FILE     VARCHAR2 (11):= TO_CHAR (TO_DATE (SUBSTR (P_FROM_DATE, 1, 10), 'RRRR/mm/dd'),'MONYYYY');
P_F_DATE     VARCHAR2 (11):= TO_CHAR (TO_DATE (SUBSTR (P_FROM_DATE, 1, 10), 'RRRR/mm/dd'),'DD-MON-YYYY');
P_T_DATE     VARCHAR2 (11):= TO_CHAR (TO_DATE (SUBSTR (P_TO_DATE, 1, 10), 'RRRR/mm/dd'),'DD-MON-YYYY');

CURSOR CUR_MIS_CREDITORS_RECO
      IS
      SELECT 
AA.Customer_Number Customer_Number,
AA.Document_number Document_Number,
--AA.Customer_Name,
SUM(NVL(AA.dr_amount,0)) Dr_Amount,
SUM(NVL(AA.cr_amount,0)) Cr_Amount,
SUM(NVL(AA.cr_amount,0))-SUM(NVL(AA.dr_amount,0)) Amount_Due
FROM
(SELECT GLB.NAME batch_name,
       glh.je_source SOURCE, glh.je_category CATEGORY,
       glh.default_effective_date gl_date,
       gll.je_line_num line_number,
       gl.concatenated_segments account_code,
       (SELECT FFVL.description FROM apps.fnd_flex_values_vl ffvl,apps.gl_code_combinations_kfv gl
        WHERE ffvl.FLEX_VALUE_SET_ID=1013464
        AND ffvl.FLEX_VALUE=gl.SEGMENT1) CO_DESC,
       (SELECT FFVL.description FROM apps.fnd_flex_values_vl ffvl
        WHERE ffvl.FLEX_VALUE_SET_ID=1013466
        AND ffvl.FLEX_VALUE=gl.SEGMENT3) SBU_desc,
         (SELECT FFVL.description FROM apps.fnd_flex_values_vl ffvl
        WHERE ffvl.FLEX_VALUE_SET_ID=1013467
        AND ffvl.FLEX_VALUE=gl.SEGMENT4) Location_desc,
       (SELECT FFVL.description FROM apps.fnd_flex_values_vl ffvl
        WHERE ffvl.FLEX_VALUE_SET_ID=1013469
        AND ffvl.FLEX_VALUE=gl.SEGMENT6) account_desc,
       decode(xla.accounted_dr,null,decode(xla.accounted_cr,null, gll.accounted_dr),xla.accounted_dr) dr_amount,
       decode(xla.accounted_cr,null,decode(xla.accounted_dr,null, gll.accounted_cr),xla.accounted_cr) cr_amount,
       gll.description journal_description,
      CASE WHEN UPPER(glh.je_source)='PAYABLES'
            THEN (SELECT po.segment1
                 FROM apps.po_vendors po
                 WHERE po.VENDOR_ID=xla.PARTY_ID
                 AND ROWNUM<2)
            WHEN UPPER(glh.je_source)='RECEIVABLES'
            THEN 
                DECODE(glh.je_category ,'Receipts',(select ACCOUNT_NUMBER  
                                                      from apps.hz_cust_accounts_all hca
                                                          ,apps.hz_parties hp
                                                          ,apps.ar_cash_receipts_all   acra 
                                                          ,xla.xla_transaction_entities xte
                                                          ,apps.xla_ae_headers xah
                                                      where hca.cust_account_id=acra.PAY_FROM_CUSTOMER
                                                        and hca.PARTY_ID
=hp.party_id
                                                        and acra.cash_receipt_id=xte.SOURCE_ID_INT_1
                                                        and xah.ENTITY_ID
=xte.ENTITY_ID
                                                        and xah.AE_HEADER_ID=xla.AE_HEADER_ID
                                                      ),
                 (SELECT  ar.account_number
                 FROM apps.hz_parties hzp, apps.hz_cust_accounts ar
                 WHERE ar.party_id = hzp.party_id
                 AND ar.cust_account_id=xla.party_id
                 AND ROWNUM<2 ))
        END AS Customer_Number,
        CASE WHEN UPPER(glh.je_source)='PAYABLES'
            THEN (SELECT po.vendor_name
                 FROM apps.po_vendors po
                 WHERE po.VENDOR_ID=xla.PARTY_ID
                 )
            WHEN UPPER(glh.je_source)='RECEIVABLES'
            THEN 
                 decode( glh.je_category ,'Receipts',(select hp.party_name  
                                                      from apps.hz_cust_accounts_all hca
                                                          ,apps.hz_parties hp
                                                          ,apps.ar_cash_receipts_all   acra 
                                                          ,xla.xla_transaction_entities xte
                                                          ,apps.xla_ae_headers xah
                                                      where hca.cust_account_id=acra.PAY_FROM_CUSTOMER
                                                        and hca.PARTY_ID=hp.party_id
                                                        and acra.cash_receipt_id=xte.SOURCE_ID_INT_1
                                                        and xah.ENTITY_ID=xte.ENTITY_ID
                                                        and xah.AE_HEADER_ID=xla.AE_HEADER_ID
                                                      )
                 ,(SELECT  hzp.party_name
                 FROM apps.hz_parties hzp, apps.hz_cust_accounts ar
                 WHERE ar.party_id = hzp.party_id
                 AND ar.cust_account_id=xla.party_id
                 ))
         END AS Customer_Name,
                       NVL
                          (DECODE
                              (glh.je_category,
                               'Credit Memos', (SELECT rac.doc_sequence_value
                                                  FROM apps.ra_customer_trx_all rac
                                                 WHERE EXISTS (
                                                          SELECT 1
                                                            FROM apps.ra_cust_trx_line_gl_dist_all cust,
                                                                 apps.xla_ae_headers xxla
                                                           WHERE xxla.event_id =
                                                                    cust.event_id
                                                             AND cust.customer_trx_id =
                                                                    rac.customer_trx_id
                                                             AND cust.org_id =
                                                                    rac.org_id
                                                             AND xxla.ae_header_id =
                                                                    xla.ae_header_id)),
                               (SELECT xxla.doc_sequence_value
                                  FROM apps.xla_ae_headers xxla
                                 WHERE xxla.ae_header_id = xla.ae_header_id)
                              ),
                           NVL (glir.subledger_doc_sequence_value,
                                glh.doc_sequence_value
                               )
                          ) document_number,
       DECODE (GLB.status,
               'P', 'Posted',
               'U', 'Unposted',
               GLB.status
              ) batch_status,
         null Created_By,
         GL.SEGMENT1 CO,
        gl.segment3 SBU,
       gl.segment4 LOCATION,
       gl.segment6 GL_account,
            GLH.LEDGER_ID LEDGER_ID,gl.CODE_COMBINATION_ID,xla.CREATED_BY CREATED_BYxla ,gll.CREATED_BY CREATED_BYgll,
               (select distinct SECURITY_ID_INT_1 from xla.xla_transaction_entities xlat,apps.xla_ae_headers xlah where 
                xlat.APPLICATION_ID=xla.APPLICATION_ID
   and xlat.ENTITY_ID=xlah.ENTITY_ID 
   and xlah.AE_HEADER_ID=xla.AE_HEADER_ID
   )    org_id
  FROM apps.gl_je_headers glh,
       apps.gl_je_lines gll,
       apps.gl_code_combinations_kfv gl,
       apps.gl_je_batches GLB,
       apps.xla_ae_lines xla,
   --    xla_ae_headers xlah,
       apps.gl_import_references glir
 WHERE glh.je_header_id = gll.je_header_id
   AND gll.je_header_id = glir.je_header_id (+)
   AND gll.je_line_num = glir.je_line_num (+)
   AND glir.gl_sl_link_id = xla.gl_sl_link_id (+)
   AND glir.gl_sl_link_table = xla.gl_sl_link_table (+)
   AND gl.code_combination_id = gll.code_combination_id
 --  and xla.AE_HEADER_ID=xlah.AE_HEADER_ID
   AND glh.je_batch_id = GLB.je_batch_id
   AND UPPER(glh.je_source) IN ('PAYABLES','RECEIVABLES')
   AND NVL(xla.accounted_cr,999999)!=0
   AND NVL(xla.accounted_dr,999999)!=0
UNION ALL
                SELECT GLB.NAME batch_name, glh.je_source SOURCE,
                       glh.je_category CATEGORY,
                       glh.default_effective_date gl_date,
                       gll.je_line_num line_number,
                       gl.concatenated_segments account_code,
                       (SELECT ffvl.description
                          FROM apps.fnd_flex_values_vl ffvl,
                               apps.gl_code_combinations_kfv gl
                         WHERE ffvl.flex_value_set_id = 1013464
                           AND ffvl.flex_value = gl.segment1) co_desc,
                       (SELECT ffvl.description
                          FROM apps.fnd_flex_values_vl ffvl
                         WHERE ffvl.flex_value_set_id = 1013466
                           AND ffvl.flex_value = gl.segment3) sbu_desc,
                       (SELECT ffvl.description
                          FROM apps.fnd_flex_values_vl ffvl
                         WHERE ffvl.flex_value_set_id = 1013467
                           AND ffvl.flex_value = gl.segment4) location_desc,
                       (SELECT ffvl.description
                          FROM apps.fnd_flex_values_vl ffvl
                         WHERE ffvl.flex_value_set_id = 1013469
                           AND ffvl.flex_value = gl.segment6) account_desc,
                       gll.accounted_dr dr_amount, gll.accounted_cr cr_amount,
                       gll.description journal_description,
                       NULL customer_name, NULL customer_number,
                       glh.doc_sequence_value document_number,
                       DECODE (GLB.status,
                               'P', 'Posted',
                               'U', 'Unposted',
                               GLB.status
                              ) batch_status,
                       fu.user_name created_by, gl.segment1 co,
                       gl.segment3 sbu, gl.segment4 LOCATION,
                       gl.segment6 gl_account, glh.ledger_id ledger_id, NULL,
                       NULL, NULL, NULL
                  FROM (SELECT je_source, je_category, default_effective_date,
                               doc_sequence_value, ledger_id, je_header_id,
                               je_batch_id
                          FROM apps.gl_je_headers) glh,
                       (SELECT je_line_num, accounted_dr, accounted_cr,
                               description, je_header_id, code_combination_id,
                               created_by
                          FROM apps.gl_je_lines) gll,
                       (SELECT concatenated_segments, segment1, segment3,
                               segment4, segment6, code_combination_id
                          FROM apps.gl_code_combinations_kfv) gl,
                       (SELECT user_name, user_id
                          FROM apps.fnd_user) fu,
                       (SELECT NAME, status, je_batch_id
                          FROM apps.gl_je_batches) GLB
                 WHERE glh.je_header_id = gll.je_header_id
                   AND gl.code_combination_id = gll.code_combination_id
                   AND glh.je_batch_id = GLB.je_batch_id
                   AND gll.created_by = fu.user_id
                   AND UPPER (glh.je_source) NOT IN
                                                  ('PAYABLES', 'RECEIVABLES')) aa
         WHERE 1 = 1
           AND aa.ledger_id = 2101
           --and AA.CO = '31'
           AND aa.SOURCE = 'Payables'
           --AND AA.SBU in ('752')
           AND aa.customer_number IS NOT NULL
           AND aa.gl_account IN
                  ('362001', '362002', '362003', '362004', '362006', '362007','362009')
           AND TRUNC (aa.gl_date) BETWEEN p_f_date AND p_t_date
      --AND aa.customer_number = 9702602
      GROUP BY aa.customer_number, aa.document_number, aa.org_id;
BEGIN
   --Creating a file for the below generated MIS Creditors data
   l_file_p :=
      UTL_FILE.fopen ('VENDOR_INVOICE_PORTAL',
                         'RECO_ABRL_SCREDITORSMIS_DOC_WISE'  /*The File name has been changed by Mr.Lokesh on 03-Jul-2014(Previous File Name:'ABRL_OFBIS_creditors_Reconciliation_v4')*/
                      || '_'
                      || p_l_file
                      || '-'
                      || to_char(sysdate,'DDMMYYYY')
                      || '.TXT',
                      'W'
                     );
   /*l_file_p :=
      UTL_FILE.fopen ('VENDOR_INVOICE_PORTAL',
                         'ABRL_OFBIS_creditors_Reconciliation'
                      || '_'
                      || p_l_file
                      || '-'
                      || SYSDATE
                      || '.TXT',
                      'W'
                     );
                     */
   -- l_PMT_data:='Customer_Number^Document_Number^Dr_Amount^Cr_Amount^Amount_Due';
   UTL_FILE.put_line (l_file_p,
                         'Customer_Number'
                      || '^'
                      || 'Document_Number'
                      || '^'
                      || 'Dr_Amount'
                      || '^'
                      || 'Cr_Amount'
                      || '^'
                      || 'Amount_Due'
                     );

   FOR mis_creditors IN cur_mis_creditors_reco
   LOOP
      UTL_FILE.put_line (l_file_p,
                            mis_creditors.customer_number
                         || '^'
                         || mis_creditors.document_number
                         || '^'
                         || mis_creditors.dr_amount
                         || '^'
                         || mis_creditors.cr_amount
                         || '^'
                         || mis_creditors.amount_due
                        );
   -- UTL_FILE.put_line (l_file_p, l_pmt_data);
   END LOOP;
--UTL_FILE.fclose (l_file_p);
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      UTL_FILE.fclose (l_file_p);
      DBMS_OUTPUT.put_line ('No Data Found');
   WHEN UTL_FILE.invalid_path
   THEN
      UTL_FILE.fclose (l_file_p);
      DBMS_OUTPUT.put_line ('Invalid Path');
   WHEN UTL_FILE.invalid_filehandle
   THEN
      UTL_FILE.fclose (l_file_p);
      DBMS_OUTPUT.put_line ('Invalid File Handle');
   WHEN UTL_FILE.invalid_operation
   THEN
      UTL_FILE.fclose (l_file_p);
      DBMS_OUTPUT.put_line ('Invalid Operation');
   WHEN UTL_FILE.read_error
   THEN
      UTL_FILE.fclose (l_file_p);
      DBMS_OUTPUT.put_line ('Read Error');
   WHEN UTL_FILE.write_error
   THEN
      UTL_FILE.fclose (l_file_p);
      DBMS_OUTPUT.put_line ('Write Error');
   WHEN UTL_FILE.internal_error
   THEN
      UTL_FILE.fclose (l_file_p);
      DBMS_OUTPUT.put_line ('Internal Error');
   WHEN UTL_FILE.file_open
   THEN
      UTL_FILE.fclose (l_file_p);
      DBMS_OUTPUT.put_line ('File is Open');
   WHEN UTL_FILE.invalid_filename
   THEN
      UTL_FILE.fclose (l_file_p);
      DBMS_OUTPUT.put_line ('Invalid File Name');
   WHEN OTHERS
   THEN
      UTL_FILE.fclose (l_file_p);
      DBMS_OUTPUT.put_line ('Unknown Error' || SQLERRM);
END xxabrl_mis_creditors_reco_utl; 
/


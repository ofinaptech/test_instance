CREATE OR REPLACE PACKAGE APPS.XXABRL_ASSET_OPEN_BAL_DATA_PKG IS
 /**********************************************************************************************************************************************
                        WIPRO Infotech Ltd, Mumbai, India

            Name: XXABRL Asset Load Program Package

            Change Record: -- Created by Amresh
           =========================================================================================================================
           Version   Date          Author                    Remarks                  Documnet Ref
           =======   ==========   =============             ==================  =====================================================
           1.0.0                                          Initial Version
           1.0.1     07-Sep-2012   Amresh Kumar Chutke    Added two columns ATTRIBUTE17 and ATTRIBUTE18 
                                                          to the cursors CUR_VALIDATE and CUR_INSERT, so that it will insert
                                                          the records in the mass additions base table. 
           1.0.2     07-Sep-2012   Amresh Kumar Chutke    Enhanced the performance of the package
                                                           by adding a condition which will only pick up current
                                                           dated records based on creation_date  
           1.0.3     12-Sep-2012   Amresh Kumar Chutke    Added one column ATTRIBUTE19 to store Old Mass Addition ID for Migration Purpose.         
 
            Usage : To validate, update asset records into stage table and insert into mass additions base table.   
  ************************************************************************************************************************************************/

PROCEDURE XXABRL_ASSET_LOAD(ERRBUFF             OUT VARCHAR2,
                                RETCODE             OUT NUMBER,
                                P_PRG_FLAG   IN VARCHAR2);

PROCEDURE     XXABRL_ASSET_DATA_VAL_proc;
PROCEDURE     XXABRL_ASSET_DATA_INS_proc;
END XXABRL_ASSET_OPEN_BAL_DATA_PKG; 
/


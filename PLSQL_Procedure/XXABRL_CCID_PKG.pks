CREATE OR REPLACE PACKAGE APPS.XXABRL_CCID_PKG AS
PROCEDURE XXABRL_CCID_PROC (ERRBUFF OUT VARCHAR2,
                                    RETCODE OUT number, 
                                    P_FROM_SEGMENT1     IN VARCHAR2,
                                    P_TO_SEGMENT1     IN VARCHAR2,
                                   P_FROM_SEGMENT2     IN VARCHAR2,
                                   P_TO_SEGMENT2     IN VARCHAR2,
                                   P_FROM_SEGMENT3     IN VARCHAR2,
                                   P_TO_SEGMENT3     IN VARCHAR2,
                                   P_FROM_SEGMENT4     IN VARCHAR2,
                                   P_TO_SEGMENT4     IN VARCHAR2,
                                   P_FROM_SEGMENT5     IN VARCHAR2,
                                   P_TO_SEGMENT5     IN VARCHAR2,
                                   P_FROM_SEGMENT6     IN VARCHAR2,
                                   P_TO_SEGMENT6     IN VARCHAR2,
                                   P_FROM_SEGMENT7     IN VARCHAR2,
                                   P_TO_SEGMENT7    IN VARCHAR2,
                                   P_FROM_SEGMENT8     IN VARCHAR2,  
                                   P_TO_SEGMENT8     IN VARCHAR2,                            
                                    P_FROM_UPDATE IN VARCHAR2,
                                    P_TO_UPDATE IN VARCHAR2,                                 
                                    P_ENABLED_FLAG IN CHAR                                    
                                    );
                                   END XXABRL_CCID_PKG;
/


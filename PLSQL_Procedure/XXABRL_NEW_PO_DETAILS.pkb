CREATE OR REPLACE PACKAGE BODY APPS.xxabrl_new_po_details
IS
   PROCEDURE xxabrl_main_pkg (
      errbuf    OUT      VARCHAR2,
      retcode   OUT      NUMBER,
      p_type    IN       VARCHAR2
   )
   IS
   BEGIN
      IF p_type = 'H'
      THEN
         xxabrl_pos_daily_data;
      END IF;

      IF p_type = 'L'
      THEN
         xxabrl_pos_lines;
      END IF;

      IF p_type = 'LD'
      THEN
         xxabrl_pos_loc_dist_det;
      END IF;

      IF p_type = 'AH'
      THEN
         xxabrl_pos_action_history_det;
      END IF;

      IF p_type = 'T'
      THEN
         xxabrl_po_taxes;
      END IF;
   END xxabrl_main_pkg;

   PROCEDURE xxabrl_pos_daily_data
   IS
   BEGIN
      EXECUTE IMMEDIATE 'truncate table xxabrliproc.xxabrl_po_headers_det';

      FOR buf IN
         (SELECT DISTINCT pha.segment1 po_num, pha.po_header_id,
                          pha.agent_id,
                          (SELECT full_name
                             FROM apps.per_all_people_f
                            WHERE person_id = pha.agent_id
                              AND ROWNUM <= 1) buyer,
                          pha.org_id,
                          (SELECT NAME
                             FROM apps.hr_operating_units
                            WHERE organization_id = pha.org_id)
                                                              operating_unit,
                          pha.authorization_status, pha.type_lookup_code,
                          pha.enabled_flag, pha.last_update_date,
                          pha.last_updated_by,
                          (SELECT user_name
                             FROM apps.fnd_user
                            WHERE user_id =
                                        pha.last_updated_by)
                                                           last_updated_code,
                          (SELECT description
                             FROM apps.fnd_user
                            WHERE user_id =
                                        pha.last_updated_by)
                                                           last_updated_name,
                          pha.creation_date, pha.created_by,
                          (SELECT user_name
                             FROM apps.fnd_user
                            WHERE user_id = pha.created_by) created_code,
                          (SELECT description
                             FROM apps.fnd_user
                            WHERE user_id = pha.created_by) created_by_name,
                          pha.vendor_id,
                          (SELECT segment1
                             FROM apps.ap_suppliers
                            WHERE vendor_id = pha.vendor_id) vendor_code,
                          (SELECT vendor_name
                             FROM apps.ap_suppliers
                            WHERE vendor_id = pha.vendor_id) vendor_name,
                          pha.vendor_site_id,
                          (SELECT b.vendor_site_code
                             FROM apps.ap_supplier_sites_all b
                            WHERE pha.vendor_site_id = b.vendor_site_id)
                                                            vendor_site_code,
                          (SELECT vendor_contact_id
                             FROM apps.ap_supplier_contacts
                            WHERE vendor_site_id =
                                         pha.vendor_site_id)
                                                           vendor_contact_id,
                          pha.ship_to_location_id,
                          (SELECT location_code
                             FROM apps.hr_locations
                            WHERE location_id =
                                     pha.ship_to_location_id)
                                                          ship_location_code,
                          pha.bill_to_location_id,
                          (SELECT location_code
                             FROM apps.hr_locations
                            WHERE location_id =
                                     pha.bill_to_location_id)
                                                       bill_to_location_code,
                          pha.terms_id,
                          (SELECT TYPE
                             FROM apps.ap_terms
                            WHERE term_id = pha.terms_id) term_name,
                          pha.fob_lookup_code, pha.freight_terms_lookup_code,
                          pha.currency_code, pha.revision_num,
                          pha.revised_date, pha.approved_flag,
                          (SELECT MAX (action_date)
                             FROM apps.po_action_history
                            WHERE object_id =
                                         pha.po_header_id
                              AND action_code = 'APPROVE')
                                                         final_approved_date,
                          pha.note_to_authorizer, pha.note_to_vendor,
                          pha.note_to_receiver, pha.print_count,
                          pha.printed_date, pha.comments, pha.closed_date,
                          pha.user_hold_flag, pha.approval_required_flag,
                          pha.cancel_flag, pha.firm_status_lookup_code,
                          pha.frozen_flag, pha.supply_agreement_flag,
                          pha.attribute1 attribute1_insurance,
                          pha.attribute2 attribute2_warranty,
                          pha.attribute3 attribute3_contact_name,
                          pha.attribute4 attribute4_contact_phone,
                          pha.attribute5 attribute5_liquidated_damage,
                          pha.attribute6 attribute6_retention,
                          pha.attribute7 attribute7_vat_regis_number,
                          pha.attribute8 attribute8_cst_number,
                          pha.attribute9 attribute9_old_po_number,
                          pha.attribute10 attribute10_tin_number,
                          pha.attribute11 attribute11_budget_code_po,
                          pha.attribute12 attribute12_po_expiry_date,
                          pha.attribute13 attribute13_crf_amount,
                          pha.attribute14 crf_no,
                          pha.attribute15 attribute15_old_po_app_date,
                          pha.global_attribute1, pha.global_attribute2,
                          pha.global_attribute3, pha.global_attribute4,
                          pha.global_attribute5, pha.global_attribute6,
                          pha.global_attribute7, pha.global_attribute8,
                          pha.global_attribute9, pha.global_attribute10,
                          pha.global_attribute11, pha.global_attribute12,
                          pha.global_attribute13, pha.global_attribute14,
                          pha.global_attribute15, pha.global_attribute16,
                          pha.global_attribute17, pha.global_attribute18,
                          pha.global_attribute19, pha.global_attribute20,
                          NVL (pha.closed_code, 'OPEN') closed_code,
                          pha.change_requested_by, pha.change_summary,
                          pha.document_creation_method,
                          pha.interface_source_code, pha.reference_num,
                          pha.submit_date
                     FROM apps.po_headers_all pha,
                          apps.po_lines_all pla,
                          apps.po_line_locations_all plla,
                          apps.po_distributions_all pda
                    WHERE 1 = 1
                      AND pha.po_header_id = pla.po_header_id
                      AND pla.po_line_id = plla.po_line_id
                      AND plla.line_location_id = pda.line_location_id
                      AND (   TRUNC (pha.creation_date) = TRUNC (SYSDATE)
                           OR TRUNC (pha.creation_date) = TRUNC (SYSDATE - 1)
                          )
            --          AND TRUNC (pha.creation_date) >=  '01-jul-17'
                 --AND pha.po_header_id = 103544966
          ORDER BY        pha.po_header_id)
      LOOP
         INSERT INTO xxabrliproc.xxabrl_po_headers_det
                     (po_num, po_header_id, agent_id, buyer,
                      org_id, operating_unit,
                      authorization_status, type_lookup_code,
                      enabled_flag, last_update_date,
                      last_updated_by, last_updated_code,
                      last_updated_name, creation_date,
                      created_by, craeted_code, created_by_name,
                      vendor_id, vendor_code, vendor_name,
                      vendor_site_id, vendor_site_code,
                      vendor_contact_id, ship_to_location_id,
                      ship_location_code, bill_to_location_id,
                      bill_to_location_code, terms_id,
                      term_name, fob_lookup_code,
                      freight_terms_lookup_code, currency_code,
                      revision_num, revised_date, approved_flag,
                      final_approved_date, note_to_authorizer,
                      note_to_vendor, note_to_receiver,
                      print_count, printed_date, comments,
                      closed_date, user_hold_flag,
                      approval_required_flag, cancel_flag,
                      firm_status_lookup_code, frozen_flag,
                      supply_agreement_flag, attribute1_insurance,
                      attribute2_warranty, attribute3_contact_name,
                      attribute4_contact_phone,
                      attribute5_liquidated_damage,
                      attribute6_retention,
                      attribute7_vat_regis_number,
                      attribute8_cst_number,
                      attribute9_old_po_number,
                      attribute10_tin_number,
                      attribute11_budget_code_po,
                      attribute12_po_expiry_date,
                      attribute13_crf_amount, crf_no,
                      attribute15_old_po_app_date,
                      global_attribute1, global_attribute2,
                      global_attribute3, global_attribute4,
                      global_attribute5, global_attribute6,
                      global_attribute7, global_attribute8,
                      global_attribute9, global_attribute10,
                      global_attribute11, global_attribute12,
                      global_attribute13, global_attribute14,
                      global_attribute15, global_attribute16,
                      global_attribute17, global_attribute18,
                      global_attribute19, global_attribute20,
                      closed_code, change_requested_by,
                      change_summary, document_creation_method,
                      interface_source_code, reference_num,
                      submit_date, process_flag
                     )
              VALUES (buf.po_num, buf.po_header_id, buf.agent_id, buf.buyer,
                      buf.org_id, buf.operating_unit,
                      buf.authorization_status, buf.type_lookup_code,
                      buf.enabled_flag, buf.last_update_date,
                      buf.last_updated_by, buf.last_updated_code,
                      buf.last_updated_name, buf.creation_date,
                      buf.created_by, buf.created_code, buf.created_by_name,
                      buf.vendor_id, buf.vendor_code, buf.vendor_name,
                      buf.vendor_site_id, buf.vendor_site_code,
                      buf.vendor_contact_id, buf.ship_to_location_id,
                      buf.ship_location_code, buf.bill_to_location_id,
                      buf.bill_to_location_code, buf.terms_id,
                      buf.term_name, buf.fob_lookup_code,
                      buf.freight_terms_lookup_code, buf.currency_code,
                      buf.revision_num, buf.revised_date, buf.approved_flag,
                      buf.final_approved_date, buf.note_to_authorizer,
                      buf.note_to_vendor, buf.note_to_receiver,
                      buf.print_count, buf.printed_date, buf.comments,
                      buf.closed_date, buf.user_hold_flag,
                      buf.approval_required_flag, buf.cancel_flag,
                      buf.firm_status_lookup_code, buf.frozen_flag,
                      buf.supply_agreement_flag, buf.attribute1_insurance,
                      buf.attribute2_warranty, buf.attribute3_contact_name,
                      buf.attribute4_contact_phone,
                      buf.attribute5_liquidated_damage,
                      buf.attribute6_retention,
                      buf.attribute7_vat_regis_number,
                      buf.attribute8_cst_number,
                      buf.attribute9_old_po_number,
                      buf.attribute10_tin_number,
                      buf.attribute11_budget_code_po,
                      buf.attribute12_po_expiry_date,
                      buf.attribute13_crf_amount, buf.crf_no,
                      buf.attribute15_old_po_app_date,
                      buf.global_attribute1, buf.global_attribute2,
                      buf.global_attribute3, buf.global_attribute4,
                      buf.global_attribute5, buf.global_attribute6,
                      buf.global_attribute7, buf.global_attribute8,
                      buf.global_attribute9, buf.global_attribute10,
                      buf.global_attribute11, buf.global_attribute12,
                      buf.global_attribute13, buf.global_attribute14,
                      buf.global_attribute15, buf.global_attribute16,
                      buf.global_attribute17, buf.global_attribute18,
                      buf.global_attribute19, buf.global_attribute20,
                      buf.closed_code, buf.change_requested_by,
                      buf.change_summary, buf.document_creation_method,
                      buf.interface_source_code, buf.reference_num,
                      buf.submit_date, 'D'
                     );

         COMMIT;
      END LOOP;
   END xxabrl_pos_daily_data;

   PROCEDURE xxabrl_pos_lines
   IS
   BEGIN
      EXECUTE IMMEDIATE 'truncate table xxabrliproc.xxabrl_po_lines_det';

      FOR buf IN
         (SELECT DISTINCT pla.po_line_id, pla.po_header_id,
                          pla.last_update_date, pla.last_updated_by,
                          (SELECT user_name
                             FROM apps.fnd_user
                            WHERE user_id =
                                        pla.last_updated_by)
                                                           last_updated_code,
                          (SELECT description
                             FROM apps.fnd_user
                            WHERE user_id =
                                        pla.last_updated_by)
                                                           last_updated_name,
                          pla.line_type_id,
                          (SELECT line_type
                             FROM apps.po_line_types
                            WHERE line_type_id =
                                              pla.line_type_id)
                                                              line_type_code,
                          pla.line_num, pla.creation_date, pla.created_by,
                          (SELECT user_name
                             FROM apps.fnd_user
                            WHERE user_id = pla.created_by) created_code,
                          (SELECT user_name || '-'
                                  || description
                             FROM apps.fnd_user
                            WHERE user_id = pla.created_by) created_name,
                          pla.item_id,
                          (SELECT segment1
                             FROM apps.mtl_system_items_b
                            WHERE organization_id = 97
                              AND inventory_item_id = pla.item_id) item_code,
                          pla.item_description, pla.category_id,
                          (SELECT segment1 || '.' || segment2
                             FROM apps.mtl_categories_vl
                            WHERE category_id = pla.category_id)
                                                               category_name,
                          pla.unit_meas_lookup_code, pla.quantity_committed,
                          pla.committed_amount, pla.list_price_per_unit,
                          pla.unit_price, pla.quantity,
                          pla.unit_price * pla.quantity po_base_amount,
                          pla.note_to_vendor, pla.closed_flag,
                          pla.cancel_flag, pla.cancelled_by,
                          (SELECT    user_name
                                  || '-'
                                  || description
                             FROM apps.fnd_user
                            WHERE user_id = pla.cancelled_by)
                                                           cancelled_by_name,
                          pla.cancel_date, pla.cancel_reason,
                          pla.capital_expense_flag, pla.attribute1,
                          pla.attribute2, pla.attribute3, pla.attribute4,
                          pla.attribute5, pla.attribute6, pla.attribute7,
                          pla.attribute8, pla.attribute9, pla.attribute10,
                          pla.reference_num, pla.attribute11,
                          pla.attribute12, pla.attribute13, pla.attribute14,
                          pla.attribute15, pla.closed_code, pla.closed_date,
                          pla.closed_reason, pla.closed_by,
                          (SELECT user_name || '-' || description
                             FROM apps.fnd_user
                            WHERE user_id = pla.closed_by) closed_name,
                          pla.org_id, pla.retroactive_date,
                          pla.base_unit_price
                     FROM apps.po_headers_all pha,
                          apps.po_lines_all pla,
                          apps.po_line_locations_all plla,
                          apps.po_distributions_all pda
                    WHERE 1 = 1
                      AND pha.po_header_id = pla.po_header_id
                      AND pla.po_line_id = plla.po_line_id
                      AND plla.line_location_id = pda.line_location_id
                  --    AND TRUNC (pha.creation_date) >= '01-jul-17'
                      AND (   TRUNC (pla.creation_date) = TRUNC (SYSDATE)
                           OR TRUNC (pla.creation_date) = TRUNC (SYSDATE - 1)
                          )
                 --   AND pha.po_header_id = 103544966
          ORDER BY        pla.po_header_id, pla.po_line_id, pla.line_num)
      LOOP
         INSERT INTO xxabrliproc.xxabrl_po_lines_det
                     (po_line_id, po_header_id,
                      last_update_date, last_updated_by,
                      last_updated_code, last_updated_name,
                      line_type_id, line_type_code, line_num,
                      creation_date, created_by, craeted_code,
                      created_name, item_id, item_code,
                      item_description, category_id,
                      category_name, unit_meas_lookup_code,
                      quantity_committed, committed_amount,
                      list_price_per_unit, unit_price, quantity,
                      po_base_amount, note_to_vendor,
                      closed_flag, cancel_flag, cancelled_by,
                      cancelled_by_name, cancel_date,
                      cancel_reason, capital_expense_flag,
                      attribute1, attribute2, attribute3,
                      attribute4, attribute5, attribute6,
                      attribute7, attribute8, attribute9,
                      attribute10, reference_num, attribute11,
                      attribute12, attribute13, attribute14,
                      attribute15, closed_code, closed_date,
                      closed_reason, closed_by, closed_name,
                      org_id, retroactive_date, base_unit_price,
                      process_flag
                     )
              VALUES (buf.po_line_id, buf.po_header_id,
                      buf.last_update_date, buf.last_updated_by,
                      buf.last_updated_code, buf.last_updated_name,
                      buf.line_type_id, buf.line_type_code, buf.line_num,
                      buf.creation_date, buf.created_by, buf.created_code,
                      buf.created_name, buf.item_id, buf.item_code,
                      buf.item_description, buf.category_id,
                      buf.category_name, buf.unit_meas_lookup_code,
                      buf.quantity_committed, buf.committed_amount,
                      buf.list_price_per_unit, buf.unit_price, buf.quantity,
                      buf.po_base_amount, buf.note_to_vendor,
                      buf.closed_flag, buf.cancel_flag, buf.cancelled_by,
                      buf.cancelled_by_name, buf.cancel_date,
                      buf.cancel_reason, buf.capital_expense_flag,
                      buf.attribute1, buf.attribute2, buf.attribute3,
                      buf.attribute4, buf.attribute5, buf.attribute6,
                      buf.attribute7, buf.attribute8, buf.attribute9,
                      buf.attribute10, buf.reference_num, buf.attribute11,
                      buf.attribute12, buf.attribute13, buf.attribute14,
                      buf.attribute15, buf.closed_code, buf.closed_date,
                      buf.closed_reason, buf.closed_by, buf.closed_name,
                      buf.org_id, buf.retroactive_date, buf.base_unit_price,
                      'D'
                     );

         COMMIT;
      END LOOP;
   END xxabrl_pos_lines;

   PROCEDURE xxabrl_pos_loc_dist_det
   IS
   BEGIN
      EXECUTE IMMEDIATE 'truncate table xxabrliproc.xxabrl_po_locatins_dist';

      FOR buf IN
         (SELECT DISTINCT plla.line_location_id, plla.po_header_id,
                          plla.po_line_id, plla.last_update_date,
                          plla.last_updated_by,
                          (SELECT user_name
                             FROM apps.fnd_user
                            WHERE user_id =
                                       plla.last_updated_by)
                                                           last_updated_code,
                          (SELECT description
                             FROM apps.fnd_user
                            WHERE user_id =
                                       plla.last_updated_by)
                                                           last_updated_name,
                          plla.creation_date, plla.created_by,
                          (SELECT user_name
                             FROM apps.fnd_user
                            WHERE user_id = plla.created_by) created_code,
                          (SELECT description
                             FROM apps.fnd_user
                            WHERE user_id = plla.created_by) created_by_name,
                          plla.quantity, plla.quantity_received,
                          plla.quantity_accepted, plla.quantity_rejected,
                          plla.quantity_billed, plla.quantity_cancelled,
                          plla.unit_meas_lookup_code,
                          plla.ship_to_location_id,
                          (SELECT location_code
                             FROM apps.hr_locations
                            WHERE location_id =
                                     plla.ship_to_location_id)
                                                          ship_location_code,
                          plla.need_by_date, plla.promised_date,
                          plla.last_accept_date, plla.price_override,
                          plla.approved_flag, plla.approved_date,
                          plla.closed_flag, plla.cancel_flag,
                          plla.cancelled_by,
                          (SELECT description
                             FROM apps.fnd_user
                            WHERE user_id = plla.cancelled_by)
                                                              cancelled_name,
                          plla.cancel_date, plla.cancel_reason,
                          plla.attribute2, plla.attribute3, plla.attribute4,
                          plla.attribute5, plla.attribute6, plla.attribute7,
                          plla.attribute8, plla.attribute9, plla.attribute10,
                          plla.unit_of_measure_class, plla.attribute11,
                          plla.attribute12, plla.attribute13,
                          plla.attribute14, plla.attribute15,
                          plla.ship_to_organization_id,
                          (SELECT    organization_code
                                  || '-'
                                  || organization_name
                             FROM apps.org_organization_definitions
                            WHERE organization_id = ship_to_organization_id)
                                                               inventory_org,
                          plla.shipment_num, plla.closed_code,
                          plla.closed_reason, plla.closed_date,
                          plla.closed_by,
                          (SELECT user_name || '-' || description
                             FROM apps.fnd_user
                            WHERE user_id = plla.closed_by) closed_name,
                          plla.org_id, plla.quantity_shipped,
                          plla.amount_received, plla.amount_billed,
                          plla.amount_cancelled, plla.amount_rejected,
                          plla.amount_accepted,
                          plla.closed_for_receiving_date,
                          plla.closed_for_invoice_date,
                          pda.po_distribution_id, pda.distribution_num,
                          pda.set_of_books_id, pda.quantity_ordered,
                          pda.quantity_delivered, pda.req_distribution_id,
                          pda.deliver_to_location_id,
                          (SELECT location_code
                             FROM apps.hr_locations
                            WHERE location_id =
                                     pda.deliver_to_location_id)
                                                       deliver_location_code,
                          pda.deliver_to_person_id, pda.rate_date,
                          pda.destination_organization_id,
                          pda.code_combination_id,
                          (SELECT concatenated_segments
                             FROM apps.gl_code_combinations_kfv
                            WHERE code_combination_id =
                                                       pda.code_combination_id)
                                                charge_concatenated_segments,
                          (SELECT b.description
                             FROM apps.gl_code_combinations_kfv a,
                                  apps.fnd_flex_values_vl b
                            WHERE a.code_combination_id =
                                                       pda.code_combination_id
                              AND a.segment2 = b.flex_value
                              AND b.flex_value_set_id = 1013465) dep,
                          (SELECT b.description
                             FROM apps.gl_code_combinations_kfv a,
                                  apps.fnd_flex_values_vl b
                            WHERE a.code_combination_id =
                                                       pda.code_combination_id
                              AND a.segment4 = b.flex_value
                              AND b.flex_value_set_id = 1013467) LOCATION,
                          (SELECT b.description
                             FROM apps.gl_code_combinations_kfv a,
                                  apps.fnd_flex_values_vl b
                            WHERE a.code_combination_id =
                                                       pda.code_combination_id
                              AND a.segment6 = b.flex_value
                              AND b.flex_value_set_id = 1013469) ACCOUNT,
                          pda.variance_account_id,
                          (SELECT concatenated_segments
                             FROM apps.gl_code_combinations_kfv
                            WHERE code_combination_id =
                                                       pda.variance_account_id)
                                              variance_concatenated_segments,
                          pda.accrual_account_id,
                          (SELECT concatenated_segments
                             FROM apps.gl_code_combinations_kfv
                            WHERE code_combination_id = pda.accrual_account_id)
                                               accural_concatenated_segments
                     FROM apps.po_line_locations_all plla,
                          apps.po_headers_all pha,
                          apps.po_lines_all pla,
                          apps.po_distributions_all pda
                    WHERE 1 = 1
                      AND pha.po_header_id = pla.po_header_id
                      AND pla.po_line_id = plla.po_line_id
                      AND plla.line_location_id = pda.line_location_id
                      AND (   TRUNC (plla.creation_date) = TRUNC (SYSDATE)
                           OR TRUNC (plla.creation_date) = TRUNC (SYSDATE - 1)
                          )
                 -- AND pha.po_header_id = 103544966
     --                 AND TRUNC (pha.creation_date) >= '01-jul-17'
                 ORDER BY pda.distribution_num,
                          plla.po_header_id,
                          plla.po_line_id,
                          plla.line_location_id)
      LOOP
         INSERT INTO xxabrliproc.xxabrl_po_locatins_dist
                     (line_location_id, po_header_id,
                      po_line_id, last_update_date,
                      last_updated_by, last_updated_code,
                      last_updated_name, creation_date,
                      created_by, craeted_code, created_by_name,
                      quantity, quantity_received,
                      quantity_accepted, quantity_rejected,
                      quantity_billed, quantity_cancelled,
                      unit_meas_lookup_code, ship_to_location_id,
                      ship_location_code, need_by_date,
                      promised_date, last_accept_date,
                      price_override, approved_flag,
                      approved_date, closed_flag, cancel_flag,
                      cancelled_by, cancelled_name, cancel_date,
                      cancel_reason, attribute2, attribute3,
                      attribute4, attribute5, attribute6,
                      attribute7, attribute8, attribute9,
                      attribute10, unit_of_measure_class,
                      attribute11, attribute12, attribute13,
                      attribute14, attribute15,
                      ship_to_organization_id, inventory_org,
                      shipment_num, closed_code, closed_reason,
                      closed_date, closed_by, closed_name,
                      org_id, quantity_shipped, amount_received,
                      amount_billed, amount_cancelled,
                      amount_rejected, amount_accepted,
                      closed_for_receiving_date,
                      closed_for_invoice_date, po_distribution_id,
                      distribution_num, set_of_books_id,
                      quantity_ordered, quantity_delivered,
                      req_distribution_id, deliver_to_location_id,
                      deliver_location_code, deliver_to_person_id,
                      rate_date, destination_organization_id,
                      code_combination_id,
                      charge_concatenated_segments, dep,
                      LOCATION, ACCOUNT, variance_account_id,
                      variance_concatenated_segments,
                      accrual_account_id,
                      accural_concatenated_segments, process_flag
                     )
              VALUES (buf.line_location_id, buf.po_header_id,
                      buf.po_line_id, buf.last_update_date,
                      buf.last_updated_by, buf.last_updated_code,
                      buf.last_updated_name, buf.creation_date,
                      buf.created_by, buf.created_code, buf.created_by_name,
                      buf.quantity, buf.quantity_received,
                      buf.quantity_accepted, buf.quantity_rejected,
                      buf.quantity_billed, buf.quantity_cancelled,
                      buf.unit_meas_lookup_code, buf.ship_to_location_id,
                      buf.ship_location_code, buf.need_by_date,
                      buf.promised_date, buf.last_accept_date,
                      buf.price_override, buf.approved_flag,
                      buf.approved_date, buf.closed_flag, buf.cancel_flag,
                      buf.cancelled_by, buf.cancelled_name, buf.cancel_date,
                      buf.cancel_reason, buf.attribute2, buf.attribute3,
                      buf.attribute4, buf.attribute5, buf.attribute6,
                      buf.attribute7, buf.attribute8, buf.attribute9,
                      buf.attribute10, buf.unit_of_measure_class,
                      buf.attribute11, buf.attribute12, buf.attribute13,
                      buf.attribute14, buf.attribute15,
                      buf.ship_to_organization_id, buf.inventory_org,
                      buf.shipment_num, buf.closed_code, buf.closed_reason,
                      buf.closed_date, buf.closed_by, buf.closed_name,
                      buf.org_id, buf.quantity_shipped, buf.amount_received,
                      buf.amount_billed, buf.amount_cancelled,
                      buf.amount_rejected, buf.amount_accepted,
                      buf.closed_for_receiving_date,
                      buf.closed_for_invoice_date, buf.po_distribution_id,
                      buf.distribution_num, buf.set_of_books_id,
                      buf.quantity_ordered, buf.quantity_delivered,
                      buf.req_distribution_id, buf.deliver_to_location_id,
                      buf.deliver_location_code, buf.deliver_to_person_id,
                      buf.rate_date, buf.destination_organization_id,
                      buf.code_combination_id,
                      buf.charge_concatenated_segments, buf.dep,
                      buf.LOCATION, buf.ACCOUNT, buf.variance_account_id,
                      buf.variance_concatenated_segments,
                      buf.accrual_account_id,
                      buf.accural_concatenated_segments, 'D'
                     );

         COMMIT;
      END LOOP;
   END xxabrl_pos_loc_dist_det;

   PROCEDURE xxabrl_pos_action_history_det
   IS
   BEGIN
      EXECUTE IMMEDIATE 'truncate table xxabrliproc.xxabrl_po_action_details';

      FOR buf IN
         (SELECT DISTINCT pah.object_id requisition_line_id, pah.object_id,
                          pah.object_type_code, pah.object_sub_type_code,
                          pah.sequence_num, pah.last_update_date,
                          pah.last_updated_by, pah.creation_date,
                          pah.created_by, action_code, action_date,
                          employee_id,
                          (SELECT full_name
                             FROM apps.per_all_people_f
                            WHERE person_id = pah.employee_id
                              AND ROWNUM <= 1) approval_name,
                          pah.note
                     FROM apps.po_action_history pah,
                          apps.po_headers_all pha
                    WHERE 1 = 1
                 --     AND TRUNC (pha.creation_date) >= '01-jul-17'
                    and (   TRUNC (pha.creation_date) = TRUNC (SYSDATE)
                           OR TRUNC (pha.creation_date) = TRUNC (SYSDATE - 1)
                          )
                      AND pah.object_id = pha.po_header_id
                 -- AND pha.po_header_id = 88214966
          ORDER BY        pah.object_id, pah.sequence_num)
      LOOP
         INSERT INTO xxabrliproc.xxabrl_po_action_details
                     (object_id, object_type_code,
                      object_sub_type_code, sequence_num,
                      last_update_date, last_updated_by,
                      creation_date, created_by, action_code,
                      action_date, employee_id, approval_name,
                      note, process_flag
                     )
              VALUES (buf.object_id, buf.object_type_code,
                      buf.object_sub_type_code, buf.sequence_num,
                      buf.last_update_date, buf.last_updated_by,
                      buf.creation_date, buf.created_by, buf.action_code,
                      buf.action_date, buf.employee_id, buf.approval_name,
                      buf.note, 'D'
                     );

         COMMIT;
      END LOOP;
   END xxabrl_pos_action_history_det;

   PROCEDURE xxabrl_po_taxes
   IS
   BEGIN
      EXECUTE IMMEDIATE 'truncate table xxabrliproc.xxabrl_po_tax_details';

      FOR buf IN
         (SELECT DISTINCT pha.po_header_id, 
                                jtl.trx_line_id po_line_id,
                               jtl.trx_loc_line_id line_location_id, 
                               jtl.precedence_1,
                               jtl.precedence_2,
                               jtl.precedence_3,
                               jtl.precedence_4,
                               jtl.precedence_5,
                               jtl.trx_id tax_id,
                               jtl.tax_line_num tax_line_no,
                NVL (SUM (DECODE (jtl.rec_tax_amt_tax_curr,
                                  '0', jtl.nrec_tax_amt_tax_curr,
                                  jtl.rec_tax_amt_tax_curr
                                 )
                         ),
                     0
                    ) tax_amount,
                jtl.tax_rate_code tax_name, 
                jtl.tax_rate_percentage tax_rate,
                jta.tax_account_id, 
                pha.org_id,
                 gcc.concatenated_segments tax_concatenated_segments
           FROM apps.po_headers_all pha,
                apps.jai_tax_lines jtl,
                apps.jai_tax_accounts jta,
                apps.gl_code_combinations_kfv gcc
          WHERE 1 = 1
            AND pha.po_header_id = jtl.trx_id
                      AND ( TRUNC (pha.creation_date) = TRUNC (SYSDATE)
                           OR TRUNC (pha.creation_date) = TRUNC (SYSDATE - 1)
                          )
            AND jtl.tax_type_id = jta.tax_account_entity_id
            AND jta.interim_recovery_ccid = gcc.code_combination_id
            AND pha.org_id = jta.org_id
            AND tax_account_entity_code = 'TAX_TYPE'
            AND jtl.entity_code = 'PURCHASE_ORDER'
           -- AND TRUNC (pha.creation_date) >= '01-jul-17'
           -- AND pha.segment1 = '615200013621'
       GROUP BY pha.po_header_id,
                jtl.trx_line_id,
                jtl.trx_loc_line_id,
                jtl.precedence_1,
                jtl.precedence_2,
                jtl.precedence_3,
                jtl.precedence_4,
               jtl.precedence_5,
                jtl.tax_rate_code,
                jtl.tax_rate_percentage,
                gcc.concatenated_segments,
                jtl.trx_id,
                jtl.tax_line_num,
                jtl.tax_rate_code,
                jta.tax_account_id,
                pha.org_id
       ORDER BY pha.po_header_id,
                jtl.trx_line_id,
                jtl.trx_loc_line_id,
                jtl.precedence_1,
                jtl.precedence_2,
                jtl.precedence_3,
                jtl.precedence_4,
               jtl.precedence_5,
                jtl.tax_rate_code,
                jtl.tax_rate_percentage,
                jtl.trx_id,
                jtl.tax_line_num,
                jtl.tax_rate_code,
                jta.tax_account_id,
                pha.org_id)
      LOOP
         INSERT INTO xxabrliproc.xxabrl_po_tax_details
                     (po_header_id, po_line_id,
                      line_location_id, tax_id, tax_line_no,
                      tax_amount, tax_name, tax_rate,
                      tax_account_id, org_id,
                      tax_concatenated_segments, process_flag
                     )
              VALUES (buf.po_header_id, buf.po_line_id,
                      buf.line_location_id, buf.tax_id, buf.tax_line_no,
                      buf.tax_amount, buf.tax_name, buf.tax_rate,
                      buf.tax_account_id, buf.org_id,
                      buf.tax_concatenated_segments, 'D'
                     );

         COMMIT;
      END LOOP;
   END xxabrl_po_taxes;
END xxabrl_new_po_details; 
/


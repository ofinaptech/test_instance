CREATE OR REPLACE PROCEDURE APPS.xxmrl_gl_acc_analysis_out (
   errbuf    OUT VARCHAR2,
   retcode   OUT NUMBER--   p_from_gl_date       DATE,
                       --   p_to_gl_date         DATE
   )
AS
   /*
     =========================================================================================================
     ||   Filename   : xxmrl_gl_acc_analysis_out
     ||   Description : Script is used to get GL account Analysis Report data for perticular Accounts
     ||
     ||   Version                      Date                         Author                                   Modification
     ||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
     ||   1.0.0                 22-Aug-2020                     Lokesh Poojari                     New Development
     ||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
     ========================================================================================================*/

   gl_file             UTL_FILE.file_type;
   g_conc_request_id   NUMBER := fnd_profile.VALUE ('CONC_REQUEST_ID');

   CURSOR gl_out
   IS
      SELECT AA.batch_name,
             AA.SOURCE,
             AA.CATEGORY,
             AA.gl_date,
             AA.line_number,
             AA.account_code,
             AA.SBU_desc,
             AA.Location_desc,
             AA.account_desc,
             AA.dr_amount,
             AA.cr_amount,
             AA.journal_description,
             AA.Customer_Number,
             AA.Customer_Name,
             AA.document_number,
             AA.batch_status,
             AA.Created_By,
             (SELECT NAME
                FROM hr_operating_units
               WHERE ORGANIZATION_ID = aa.org_id)
                OU_NAME,
             AA.ae_header_id,
             AA.application_id
        FROM (SELECT GLB.NAME batch_name,
                     glh.je_source SOURCE,
                     glh.je_category CATEGORY,
                     glh.default_effective_date gl_date,
                     gll.je_line_num line_number,
                     gl.concatenated_segments account_code,
                     (SELECT FFVL.description
                        FROM fnd_flex_values_vl ffvl
                       WHERE ffvl.FLEX_VALUE_SET_ID = 1013466
                             AND ffvl.FLEX_VALUE = gl.SEGMENT3)
                        SBU_desc,
                     (SELECT FFVL.description
                        FROM fnd_flex_values_vl ffvl
                       WHERE ffvl.FLEX_VALUE_SET_ID = 1013467
                             AND ffvl.FLEX_VALUE = gl.SEGMENT4)
                        Location_desc,
                     (SELECT FFVL.description
                        FROM fnd_flex_values_vl ffvl
                       WHERE ffvl.FLEX_VALUE_SET_ID = 1013469
                             AND ffvl.FLEX_VALUE = gl.SEGMENT6)
                        account_desc,
                     DECODE (
                        xla.accounted_dr,
                        NULL, DECODE (xla.accounted_cr,
                                      NULL, gll.accounted_dr),
                        xla.accounted_dr)
                        dr_amount,
                     DECODE (
                        xla.accounted_cr,
                        NULL, DECODE (xla.accounted_dr,
                                      NULL, gll.accounted_cr),
                        xla.accounted_cr)
                        cr_amount,
                     gll.description journal_description,
                     CASE
                        WHEN UPPER (glh.je_source) = 'PAYABLES'
                        THEN
                           (SELECT po.segment1
                              FROM po_vendors po
                             WHERE po.VENDOR_ID = xla.PARTY_ID AND ROWNUM < 2)
                        WHEN UPPER (glh.je_source) = 'RECEIVABLES'
                        THEN /*(SELECT  ar.account_number
                             FROM hz_parties hzp, hz_cust_accounts ar
                             WHERE ar.party_id = hzp.party_id
                             AND ar.cust_account_id=xla.party_id
                             AND ROWNUM<2 ) */
                           DECODE (
                              glh.je_category,
                              'Receipts', (SELECT ACCOUNT_NUMBER
                                             FROM apps.hz_cust_accounts_all hca,
                                                  apps.hz_parties hp,
                                                  apps.ar_cash_receipts_all acra,
                                                  xla.xla_transaction_entities xte,
                                                  apps.xla_ae_headers xah
                                            WHERE hca.cust_account_id =
                                                     acra.PAY_FROM_CUSTOMER
                                                  AND hca.PARTY_ID =
                                                         hp.party_id
                                                  AND acra.cash_receipt_id =
                                                         xte.SOURCE_ID_INT_1
                                                  AND xah.ENTITY_ID =
                                                         xte.ENTITY_ID
                                                  AND xah.AE_HEADER_ID =
                                                         xla.AE_HEADER_ID),
                              (SELECT ar.account_number
                                 FROM hz_parties hzp, hz_cust_accounts ar
                                WHERE     ar.party_id = hzp.party_id
                                      AND ar.cust_account_id = xla.party_id
                                      AND ROWNUM < 2))
                     END
                        AS Customer_Number,
                     CASE
                        WHEN UPPER (glh.je_source) = 'PAYABLES'
                        THEN
                           (SELECT po.vendor_name
                              FROM po_vendors po
                             WHERE po.VENDOR_ID = xla.PARTY_ID)
                        WHEN UPPER (glh.je_source) = 'RECEIVABLES'
                        THEN /*(SELECT  hzp.party_name
                             FROM hz_parties hzp, hz_cust_accounts ar
                             WHERE ar.party_id = hzp.party_id
                             AND ar.cust_account_id=xla.party_id
                             )*/
                           DECODE (
                              glh.je_category,
                              'Receipts', (SELECT hp.party_name
                                             FROM hz_cust_accounts_all hca,
                                                  hz_parties hp,
                                                  apps.ar_cash_receipts_all acra,
                                                  xla.xla_transaction_entities xte,
                                                  xla_ae_headers xah
                                            WHERE hca.cust_account_id =
                                                     acra.PAY_FROM_CUSTOMER
                                                  AND hca.PARTY_ID =
                                                         hp.party_id
                                                  AND acra.cash_receipt_id =
                                                         xte.SOURCE_ID_INT_1
                                                  AND xah.ENTITY_ID =
                                                         xte.ENTITY_ID
                                                  AND xah.AE_HEADER_ID =
                                                         xla.AE_HEADER_ID),
                              (SELECT hzp.party_name
                                 FROM hz_parties hzp, hz_cust_accounts ar
                                WHERE ar.party_id = hzp.party_id
                                      AND ar.cust_account_id = xla.party_id))
                     END
                        AS Customer_Name,
                     NVL (
                        DECODE (
                           glh.je_category,
                           'Credit Memos', (SELECT DISTINCT
                                                   rac.DOC_SEQUENCE_VALUE
                                              FROM XLA_AE_HEADERS xxla,
                                                   RA_CUST_TRX_LINE_GL_DIST_ALL cust,
                                                   RA_CUSTOMER_TRX_ALL rac
                                             WHERE xxla.EVENT_ID =
                                                      cust.EVENT_ID
                                                   AND cust.CUSTOMER_TRX_ID =
                                                          rac.CUSTOMER_TRX_ID
                                                   AND cust.ORG_ID =
                                                          rac.ORG_ID
                                                   AND xxla.AE_HEADER_ID =
                                                          xla.AE_HEADER_ID),
                           (SELECT XXLA.DOC_SEQUENCE_VALUE
                              FROM XLA_AE_HEADERS xxla
                             WHERE XXLA.AE_HEADER_ID = XLA.AE_HEADER_ID)),
                        NVL (glir.SUBLEDGER_DOC_SEQUENCE_VALUE,
                             glh.doc_sequence_value))
                        document_number,
                     DECODE (GLB.status,
                             'P', 'Posted',
                             'U', 'Unposted',
                             GLB.status)
                        batch_status,
                     NULL Created_By,
                     gl.segment1,
                     gl.segment3 SBU,
                     gl.segment4 LOCATION,
                     gl.segment6 GL_account,
                     GLH.LEDGER_ID LEDGER_ID,
                     gl.CODE_COMBINATION_ID,
                     xla.CREATED_BY CREATED_BYxla,
                     gll.CREATED_BY CREATED_BYgll,
                     (SELECT DISTINCT SECURITY_ID_INT_1
                        FROM xla.xla_transaction_entities xlat,
                             xla_ae_headers xlah
                       WHERE     xlat.APPLICATION_ID = xla.APPLICATION_ID
                             AND xlat.ENTITY_ID = xlah.ENTITY_ID
                             AND xlah.AE_HEADER_ID = xla.AE_HEADER_ID)
                        org_id,
                     xla.AE_HEADER_ID,
                     xla.APPLICATION_ID
                FROM gl_je_headers glh,
                     gl_je_lines gll,
                     gl_code_combinations_kfv gl,
                     gl_je_batches GLB,
                     xla_ae_lines xla,
                     --    xla_ae_headers xlah,
                     gl_import_references glir
               WHERE     glh.je_header_id = gll.je_header_id
                     AND gll.je_header_id = glir.je_header_id(+)
                     AND gll.je_line_num = glir.je_line_num(+)
                     AND glir.gl_sl_link_id = xla.gl_sl_link_id(+)
                     AND glir.gl_sl_link_table = xla.gl_sl_link_table(+)
                     AND gl.code_combination_id = gll.code_combination_id
                     --                     AND TRUNC (glh.POSTED_DATE) BETWEEN p_from_gl_date
                     --                                                     AND p_to_gl_date
                     AND TRUNC (glh.POSTED_DATE) = TRUNC (SYSDATE) - 1
                     --  and xla.AE_HEADER_ID=xlah.AE_HEADER_ID
                     AND glh.je_batch_id = GLB.je_batch_id
                     AND UPPER (glh.je_source) IN ('PAYABLES', 'RECEIVABLES')
                     AND gl.segment6 IN
                            (227001, 227010, 362001, 362008, 362009)
                     AND gl.segment1 = '11'
                     AND glh.status = 'P'
                     AND gl.segment3 NOT IN ('282')
                     AND NVL (xla.accounted_cr, 999999) != 0
                     AND NVL (xla.accounted_dr, 999999) != 0
              UNION ALL
              SELECT GLB.NAME batch_name,
                     glh.je_source SOURCE,
                     glh.je_category CATEGORY,
                     glh.default_effective_date gl_date,
                     gll.je_line_num line_number,
                     gl.concatenated_segments account_code,
                     (SELECT FFVL.description
                        FROM fnd_flex_values_vl ffvl
                       WHERE ffvl.FLEX_VALUE_SET_ID = 1013466
                             AND ffvl.FLEX_VALUE = gl.SEGMENT3)
                        SBU_desc,
                     (SELECT FFVL.description
                        FROM fnd_flex_values_vl ffvl
                       WHERE ffvl.FLEX_VALUE_SET_ID = 1013467
                             AND ffvl.FLEX_VALUE = gl.SEGMENT4)
                        Location_desc,
                     (SELECT FFVL.description
                        FROM fnd_flex_values_vl ffvl
                       WHERE ffvl.FLEX_VALUE_SET_ID = 1013469
                             AND ffvl.FLEX_VALUE = gl.SEGMENT6)
                        account_desc,
                     GLL.ACCOUNTED_DR dr_amount,
                     GLL.ACCOUNTED_CR cr_amount,
                     gll.description journal_description,
                     NULL customer_name,
                     NULL customer_Number,
                     glh.doc_sequence_value document_number,
                     DECODE (GLB.status,
                             'P', 'Posted',
                             'U', 'Unposted',
                             GLB.status)
                        batch_status,
                     fu.user_name Created_By,
                     gl.segment1,
                     gl.segment3 SBU,
                     gl.segment4 LOCATION,
                     gl.segment6 GL_account,
                     GLH.LEDGER_ID LEDGER_ID,
                     NULL,
                     NULL,
                     NULL,
                     NULL,
                     0,
                     0
                FROM gl_je_headerS glh,
                     gl_je_lines gll,
                     gl_code_combinations_kfv gl,
                     fnd_user fu,
                     gl_je_batches GLB
               WHERE     glh.je_header_id = gll.je_header_id
                     AND gl.code_combination_id = gll.code_combination_id
                     AND glh.je_batch_id = GLB.je_batch_id
                     AND gll.created_by = fu.user_id
                     AND gl.segment6 IN
                            (227001, 227010, 362001, 362008, 362009)
                     AND gl.segment1 = '11'
                     AND glh.status = 'P'
                     AND gl.segment3 NOT IN ('282')
                     --                     AND TRUNC (glh.POSTED_DATE) BETWEEN p_from_gl_date
                     --                                                     AND p_to_gl_date
                     AND TRUNC (glh.POSTED_DATE) = TRUNC (SYSDATE) - 1
                     -- AND NVL (glh.accrual_rev_status, ''NR'') <> ''R''
                     AND UPPER (glh.je_source) NOT IN
                            ('PAYABLES', 'RECEIVABLES')) AA;
BEGIN
   gl_file :=
      UTL_FILE.fopen (
         'TRIAL_BALANCE',
            'XXMRL_GL_ACCOUNT_ANALYSIS_REPORT_'
         || TO_CHAR (SYSDATE, 'DD')
         || TO_CHAR (SYSDATE, 'MM')
         || TO_CHAR (SYSDATE, 'YY')
         || '.csv',
         'W');
   UTL_FILE.put_line (
      gl_file,
         'Batch Name'
      || '|'
      || 'Source'
      || '|'
      || 'Category'
      || '|'
      || 'GL Date'
      || '|'
      || 'Line Number'
      || '|'
      || 'Account Code'
      || '|'
      || 'State SBU'
      || '|'
      || 'Location'
      || '|'
      || 'GL Account'
      || '|'
      || 'Dr Amount'
      || '|'
      || 'Cr Amount'
      || '|'
      || 'Journal Description'
      || '|'
      || 'Vendor \ Customer Name'
      || '|'
      || 'Vendor \ Customer Number'
      || '|'
      || 'Document Number'
      || '|'
      || 'Batch Status'
      || '|'
      || 'Created by (Entry in Subledger)'
      || '|'
      || '(Trx\Invoice)Date'
      || '|'
      || '(Deposit\Payment)Date'
      || '|'
      || 'OU_NAME');

   FOR gl_main IN gl_out
   LOOP
      UTL_FILE.put_line (
         gl_file,
            gl_main.batch_name
         || '|'
         || gl_main.SOURCE
         || '|'
         || gl_main.CATEGORY
         || '|'
         || gl_main.gl_date
         || '|'
         || gl_main.line_number
         || '|'
         || gl_main.account_code
         || '|'
         || gl_main.sbu_desc
         || '|'
         || gl_main.location_desc
         || '|'
         || gl_main.account_desc
         || '|'
         || gl_main.dr_amount
         || '|'
         || gl_main.cr_amount
         || '|'
         || gl_main.journal_description
         || '|'
         || gl_main.Customer_Number
         || '|'
         || gl_main.Customer_Name
         || '|'
         || gl_main.Document_Number
         || '|'
         || gl_main.batch_status
         || '|'
         || gl_main.created_by
         || '|'
         || NULL
         || '|'
         || NULL
         || '|'
         || gl_main.ou_name);
   END LOOP;

   UTL_FILE.fclose (gl_file);
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      UTL_FILE.fclose (gl_file);
      fnd_file.put_line (fnd_file.LOG, 'No Data Found');
   WHEN UTL_FILE.invalid_path
   THEN
      UTL_FILE.fclose (gl_file);
      fnd_file.put_line (fnd_file.LOG, 'Invalid Path');
   WHEN UTL_FILE.invalid_filehandle
   THEN
      UTL_FILE.fclose (gl_file);
      fnd_file.put_line (fnd_file.LOG, 'Invalid File Handle');
   WHEN UTL_FILE.invalid_operation
   THEN
      UTL_FILE.fclose (gl_file);
      fnd_file.put_line (fnd_file.LOG, 'Invalid Operation');
   WHEN UTL_FILE.read_error
   THEN
      UTL_FILE.fclose (gl_file);
      fnd_file.put_line (fnd_file.LOG, 'Read Error');
   WHEN UTL_FILE.write_error
   THEN
      UTL_FILE.fclose (gl_file);
      fnd_file.put_line (fnd_file.LOG, 'Write Error');
   WHEN UTL_FILE.internal_error
   THEN
      UTL_FILE.fclose (gl_file);
      fnd_file.put_line (fnd_file.LOG, 'Internal Error');
   WHEN UTL_FILE.file_open
   THEN
      UTL_FILE.fclose (gl_file);
      fnd_file.put_line (fnd_file.LOG, 'File is Open');
   WHEN UTL_FILE.invalid_filename
   THEN
      UTL_FILE.fclose (gl_file);
      fnd_file.put_line (fnd_file.LOG, 'Invalid File Name');
   WHEN OTHERS
   THEN
      UTL_FILE.fclose (gl_file);
      fnd_file.put_line (fnd_file.LOG, 'Unknown Error' || SQLERRM);
      UTL_FILE.fclose (gl_file);
END; 
/


CREATE OR REPLACE PROCEDURE APPS.GST_ABRL_AP_OFF_SET_SUM_PROC (
   errbuf        OUT      VARCHAR2,
   retcode       OUT      VARCHAR2,
   p_org_id      IN       NUMBER,
   p_from_date   IN       VARCHAR2,
   p_to_date     IN       VARCHAR2
)
IS
BEGIN
/*
   =======================================================================================================
   ||   Procedure Name  : GST_ABRL_AP_OFF_SET_SUM_PROC
   ||   Description : ABRL Payables Invoice Register Summary
   ||
   ||   Version     Date            Author              Modification
   ||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
   ||   1.0.0      31-Jan-2011  Mitul Vanpariya             New Development
   ||   1.0.1      1-Feb-2011   Mitul Vanpariya             Update invoice description
   ||   1.0.2      5-Mar-2011   Amrith Dhanraj              Added vendor and beneficiary details
   ||   1.0.3      12-Mar-2011   Amrith Dhanraj           Changed Layout (Producation Movement - Mitul)
   ||   1.0.4      21-Mar-2011   Mitul                            Added Goods Recevied Date Colunm
   ||   1.0.5       17-Dec-2011   Narasimhulu              Added GRN Date,GRN Number columns
   ||   1.0.6       17-Mar-2012   Amresh Chutke          Added Payment Date column
   ||                 18-Jan-2016    Lokesh                       Added Pan Number Column
   =======================================================================================================*/
   DECLARE
      v_loc                VARCHAR2 (10);
      v_grn_date           VARCHAR2 (2000);
      v_invoice_due_date   DATE;
      v_receipt_num        VARCHAR2 (2000);
      v_pay_method         VARCHAR2 (100);
      l_grn_number         VARCHAR2 (240);
      l_grn_date           DATE;
      l_cx_grn_num         VARCHAR2 (240);
      l_temp1              VARCHAR2 (2000);
      l_cx_grn_date        DATE;
      l_temp               VARCHAR2 (2000);
   BEGIN
      fnd_file.put_line
         (fnd_file.output,
          'OU Name}Invoice No}Invoice Date}Voucher No}Invoice Due Date}GRN Number}GRN Date}Goods Received Date}GL Date}Invoice Type}Invoice Amount}Amount Paid}Amount Remaining}Liability Account}Location Of Distribution Code}State}Credit Days}Source}Invoice Description}Payment Method}Payment Date}Vendor No}Vendor Name}Alt Vendor Name}Vendor Type}Primary Vendor Category}Vendor Site}Vendor Bank Name}Vendor Branch Name}Vendor Account Number}Beneficiary NEFT IFSC Code}Beneficiary RTGS IFSC Code}Beneficiary Account Type}Beneficiary Email ID 1-3}Beneficiary Email ID 4-6}Beneficiary Email ID 7-9}GSTIN}PAN');

      FOR sel_inv_reg IN
         (SELECT distinct ai.invoice_num, ai.description, ai.invoice_date, ai.gl_date,
                 ai.invoice_type_lookup_code, ai.invoice_amount,
                 ai.amount_paid,
                 (NVL (ai.invoice_amount, 0) - NVL (ai.amount_paid, 0)
                 ) amount_remaining,
                 gcc.concatenated_segments cc, hou.NAME, pv.vendor_name,
                 pv.segment1 vendor_no, pv.vendor_name_alt,
                 pv.vendor_type_lookup_code vendor_type,
                 DECODE (UPPER (pv.global_attribute1),
                         'NOT APPLICABLE', NULL,
                         pv.global_attribute1
                        ) prmy_vendor_category,
                 ai.doc_sequence_value voucher_no, pvs.vendor_site_code,
                 pvs.attribute2 vendor_bank_name,
                 pvs.attribute3 vendor_branch_name,
                 pvs.attribute4 vendor_account_number,
                 pvs.attribute7 beneficiary_neft_ifsc_code,
                 pvs.attribute8 beneficiary_rtgs_ifsc_code,
                 pvs.attribute9 beneficiary_account_type,
                 pvs.attribute11 beneficiary_email_id,
                 pvs.attribute12 beneficiary_email_id2,
                 pvs.attribute13 beneficiary_email_id3,
                 pvs.STATE,
                 ai.payment_method_code payment_method,
                 aip.accounting_date payment_date
                        -- to display paid, unpaid and partially paid payments
                                                 ,
                 ai.vendor_id, ai.vendor_site_id, ai.terms_id,
                 (SELECT term.description
                    FROM ap_terms term
                   WHERE term_id = ai.terms_id
                   and rownum<=1) tender_cre_days,
                  (SELECT distinct jprl.registration_number
                             FROM apps.jai_party_regs jpr,
                                  apps.jai_party_reg_lines jprl
                            WHERE 1 = 1
                              AND jpr.party_reg_id = jprl.party_reg_id
                              AND rownum<=1
                              AND jprl.effective_to IS NULL
                              AND jprl.registration_type_code ='PAN'
                              AND jpr.party_id = pv.vendor_id
                              )
                                                                vendor_pan_no, 
        (SELECT distinct registration_number
                             FROM apps.jai_party_regs jpr,
                                  jai_party_reg_lines jprl
                            WHERE 1 = 1
                              AND jpr.party_reg_id = jprl.party_reg_id
                              AND jpr.party_id = pv.vendor_id
                              AND rownum<=1
                              AND registration_type_code = 'GSTIN'
                              AND jprl.effective_to IS NULL
                              )
                                                              vendor_gstin_no,                                                              
                 ai.invoice_id, ai.org_id, ai.SOURCE source_name,
                 ai.goods_received_date
            FROM ap_invoices_all ai,
                 ap_invoice_payments_all aip,
                 gl_code_combinations_kfv gcc,
                 hr_operating_units hou,
                 ap_suppliers pv,
                 ap_supplier_sites_all pvs
           WHERE 1 = 1
             AND aip.invoice_id(+) =
                    ai.invoice_id
              -- condition to display paid, unpaid and partially paid payments
             AND TRUNC (ai.gl_date) BETWEEN TO_DATE (p_from_date,
                                                     'YYYY/MM/DD HH24:MI:SS'
                                                    )
                                        AND TO_DATE (p_to_date,
                                                     'YYYY/MM/DD HH24:MI:SS'
                                                    )
             AND ai.accts_pay_code_combination_id = gcc.code_combination_id
             AND hou.organization_id = ai.org_id
             AND ai.org_id = NVL (p_org_id, ai.org_id)
             AND ai.vendor_id = pv.vendor_id
             AND ai.cancelled_date IS NULL
             --and ai.ORG_ID=621
             --and ai.INVOICE_NUM='MLR/06-11-12'
             AND hou.ORGANIZATION_ID not in (663,801,821,841,861,881,901,921,941,961,981,83,84,85,86,87,88,89,90,91,92,93,94,95,96)
             AND pv.vendor_id = pvs.vendor_id
             AND pvs.vendor_id = ai.vendor_id
             --and ai.invoice_id =2086192
             AND ai.vendor_site_id = pvs.vendor_site_id
             AND ap_invoices_pkg.get_approval_status
                                                  (ai.invoice_id,
                                                   ai.invoice_amount,
                                                   ai.payment_status_flag,
                                                   ai.invoice_type_lookup_code
                                                  ) IN
                                  ('APPROVED', 'FULL', 'UNPAID', 'AVAILABLE'))
      LOOP
         v_grn_date := NULL;
         v_receipt_num := NULL;

         BEGIN
            SELECT DISTINCT TO_CHAR (TO_NUMBER (gc.segment4))
                       INTO v_loc
                       FROM ap_invoice_distributions_all dist,
                            gl_code_combinations gc
                      WHERE 1 = 1
                        AND dist.invoice_id = sel_inv_reg.invoice_id
                        AND dist.org_id = sel_inv_reg.org_id
                        AND dist.invoice_line_number = 1
                        AND dist.dist_code_combination_id =
                                                        gc.code_combination_id
                        AND ROWNUM = 1;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               v_loc := 0;
         END;

         l_grn_date := NULL;
         l_grn_number := NULL;
         l_temp := NULL;
         l_temp1 := NULL;

         IF UPPER (sel_inv_reg.source_name) <> UPPER ('NAVISION')
         THEN
---Cursor grn date
            DECLARE
               CURSOR cx
               IS
                  SELECT DISTINCT rsh.creation_date
                             FROM rcv_shipment_headers rsh,
                                  rcv_shipment_lines rsl,
                                  rcv_transactions rt,
                                  ap_invoice_lines_all ail
                            WHERE rsh.shipment_header_id =
                                                        rsl.shipment_header_id
                              AND rt.shipment_header_id =
                                                        rsh.shipment_header_id
                              AND rt.shipment_line_id = rsl.shipment_line_id
                              AND rt.transaction_id = ail.rcv_transaction_id
                              AND rt.organization_id = rsh.ship_to_org_id
                              AND ail.invoice_id = sel_inv_reg.invoice_id;
            BEGIN
               OPEN cx;

               LOOP
                  FETCH cx
                   INTO l_cx_grn_date;

                  EXIT WHEN cx%NOTFOUND;

                  IF l_temp IS NULL
                  THEN
                     l_temp := l_cx_grn_date;
                  ELSE
                     l_temp := l_temp || ';' || l_cx_grn_date;
                  END IF;
               END LOOP;

               CLOSE cx;
            END;

---Cursor grn Number
            DECLARE
               CURSOR cx
               IS
                  SELECT DISTINCT rsh.receipt_num
                             FROM rcv_shipment_headers rsh,
                                  rcv_shipment_lines rsl,
                                  rcv_transactions rt,
                                  ap_invoice_lines_all ail
                            WHERE rsh.shipment_header_id =
                                                        rsl.shipment_header_id
                              AND rt.shipment_header_id =
                                                        rsh.shipment_header_id
                              AND rt.shipment_line_id = rsl.shipment_line_id
                              AND rt.transaction_id = ail.rcv_transaction_id
                              AND rt.organization_id = rsh.ship_to_org_id
                              AND ail.invoice_id = sel_inv_reg.invoice_id
                                                                         --and rsh.creation_date=v_grn_date
               ;
            BEGIN
               OPEN cx;

               LOOP
                  FETCH cx
                   INTO l_cx_grn_num;

                  EXIT WHEN cx%NOTFOUND;

                  IF l_temp1 IS NULL
                  THEN
                     l_temp1 := l_cx_grn_num;
                  ELSE
                     l_temp1 := l_temp1 || ';' || l_cx_grn_num;
                  END IF;
               END LOOP;

               CLOSE cx;
            END;

            v_grn_date := l_temp;
            v_receipt_num := l_temp1;
         END IF;

         IF UPPER (sel_inv_reg.source_name) = UPPER ('NAVISION')
         THEN
            BEGIN
               SELECT DISTINCT ail.reference_1, aia.attribute3
                          INTO l_grn_number, l_grn_date
                          FROM apps.ap_invoices_all aia,
                               ap_invoice_lines_all ail
                         WHERE aia.invoice_num = sel_inv_reg.invoice_num
                           AND aia.vendor_id = sel_inv_reg.vendor_id
                           AND aia.vendor_site_id = sel_inv_reg.vendor_site_id
                           AND ail.invoice_id = aia.invoice_id
                           AND ail.line_type_lookup_code = 'ITEM'
                           AND ROWNUM = 1;
/*SELECT DISTINCT XAII.GRN_NUMBER,XAII.ATTRIBUTE2 INTO L_GRN_NUMBER,L_GRN_DATE
 FROM XXABRL_AP_INVOICES_INT XAII
WHERE XAII.INVOICE_NUM= SEL_INV_REG.INVOICE_NUM
AND XAII.VENDOR_ID =SEL_INV_REG.VENDOR_ID
AND XAII.VENDOR_SITE_ID=SEL_INV_REG.vendor_site_id;*/
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_grn_number := NULL;
                  l_grn_date := NULL;
            END;

            v_grn_date := l_grn_date;
            v_receipt_num := l_grn_number;
         END IF;

         BEGIN
            SELECT payment_method_name
              INTO v_pay_method
              FROM iby_payment_methods_tl
             WHERE 1 = 1
               AND LANGUAGE = 'US'
               AND source_lang = 'US'
               AND payment_method_code = sel_inv_reg.payment_method;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               v_pay_method := NULL;
         END;

         BEGIN
            SELECT MAX (due_date)
              INTO v_invoice_due_date
              FROM ap_payment_schedules_all aps
             WHERE aps.invoice_id = sel_inv_reg.invoice_id;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               v_invoice_due_date := NULL;
         END;

         fnd_file.put_line (fnd_file.output,
                               sel_inv_reg.NAME
                            || '}'
                            || sel_inv_reg.invoice_num
                            || '}'
                            || sel_inv_reg.invoice_date
                            || '}'
                            || sel_inv_reg.voucher_no
                            || '}'
                            || v_invoice_due_date
                            || '}'
                            || v_receipt_num
                            || '}'
                            || v_grn_date
                            || '}'
                            || sel_inv_reg.goods_received_date
                            || '}'
                            || sel_inv_reg.gl_date
                            || '}'
                            || sel_inv_reg.invoice_type_lookup_code
                            || '}'
                            || sel_inv_reg.invoice_amount
                            || '}'
                            || sel_inv_reg.amount_paid
                            || '}'
                            || sel_inv_reg.amount_remaining
                            || '}'
                            || sel_inv_reg.cc
                            || '}'
                            || v_loc
                            || '}'
                            ||sel_inv_reg.State
                            ||'}'
                            || sel_inv_reg.tender_cre_days
                            || '}'
                            || sel_inv_reg.source_name
                            || '}'
                            || sel_inv_reg.description
                            || '}'
                            || v_pay_method
                            || '}'
                            || sel_inv_reg.payment_date
                            || '}'
                            || sel_inv_reg.vendor_no
                            || '}'
                            || sel_inv_reg.vendor_name
                            || '}'
                            || sel_inv_reg.vendor_name_alt
                            || '}'
                            || sel_inv_reg.vendor_type
                            || '}'
                            || sel_inv_reg.prmy_vendor_category
                            || '}'
                            || sel_inv_reg.vendor_site_code
                            || '}'
                            || sel_inv_reg.vendor_bank_name
                            || '}'
                            || sel_inv_reg.vendor_branch_name
                            || '}'
                            || sel_inv_reg.vendor_account_number
                            || '}'
                            || sel_inv_reg.beneficiary_neft_ifsc_code
                            || '}'
                            || sel_inv_reg.beneficiary_rtgs_ifsc_code
                            || '}'
                            || sel_inv_reg.beneficiary_account_type
                            || '}'
                            || sel_inv_reg.beneficiary_email_id
                            || '}'
                            || sel_inv_reg.beneficiary_email_id2
                            || '}'
                            || sel_inv_reg.beneficiary_email_id3
                            ||'}'
                            ||sel_inv_reg.vendor_gstin_no
                            ||'}'
                            ||sel_inv_reg.vendor_pan_no 
                           );
      END LOOP;
   END;
END; 
/


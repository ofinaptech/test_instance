CREATE OR REPLACE PACKAGE APPS.xxabrl_pur_rec_pkg AS
 PROCEDURE xxabrl_main_pkg     (ERRBUFF             OUT VARCHAR2,
                                RETCODE            OUT NUMBER,
                                p_Type               IN Varchar2 ,
                                p_gl_from_date               IN Varchar2 ,
                                p_gl_to_date               IN Varchar2 ,
                                p_gl_acct_from  IN Varchar2,
                                p_gl_acct_to  IN Varchar2
                                );

 PROCEDURE xxabrl_puchasing_proc(p_gl_from_date               IN Varchar2 ,
                                p_gl_to_date               IN Varchar2 ,
                                p_gl_acct_from  IN Varchar2,
                                p_gl_acct_to  IN Varchar2);
  PROCEDURE xxabrl_receiving_proc(p_gl_from_date               IN Varchar2 ,
                                p_gl_to_date               IN Varchar2 ,
                                p_gl_acct_from  IN Varchar2,
                                p_gl_acct_to  IN Varchar2);                                
END  xxabrl_pur_rec_pkg;


----------------------------------------------------------------------------------- 
/


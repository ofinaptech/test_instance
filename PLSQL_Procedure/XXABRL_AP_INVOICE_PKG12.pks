CREATE OR REPLACE PACKAGE APPS.xxabrl_ap_invoice_pkg12 AS
PROCEDURE xxabrl_ap_inv_proc12(ERRBUFF             OUT VARCHAR2,
                                RETCODE             OUT NUMBER,
                                p_source            IN  VARCHAR2,
                                p_operating_unit    IN  VARCHAR2,
                                p_inv_date_from     IN VARCHAR2,
                                p_inv_date_to       IN VARCHAR2
                                );
end xxabrl_ap_invoice_pkg12; 
/


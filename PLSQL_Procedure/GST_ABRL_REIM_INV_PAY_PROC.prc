CREATE OR REPLACE PROCEDURE APPS.gst_abrl_reim_inv_pay_proc (
   errbuf        OUT      VARCHAR2,
   retcode       OUT      NUMBER,
   p_from_date   IN       VARCHAR2
)
AS
   ap_file             UTL_FILE.file_type;
   g_conc_request_id   NUMBER        := fnd_profile.VALUE ('CONC_REQUEST_ID');
   p_run_date          DATE
                   := NVL (fnd_date.canonical_to_date (p_from_date), SYSDATE);

/*=============================================================
   ||   Procedure Name  : APPS.GST_ABRL_REIM_INV_PAY_PROC
   ||   Description          : GST ABRL ReIM Invoices Payment Status Report
   ||
   ||        Date            Author             Modification
   ||      ~~~~~~~~        ~~~~~~~~~~~        ~~~~~~~~~~~~~~~~~
   ||      12-Feb-2019    Lokesh Poojari       New Development
==============================================================*/
   CURSOR reim_inv_pay
   IS
      SELECT DISTINCT aia.invoice_date,
                      REPLACE (aia.invoice_num, '^', '//') invoice_num,
                      SUBSTR (aia.description, 11, 33) po_num,
                      aps.segment1 ofin_vendor_code,
                      apsa.attribute14 retek_vendor_code,
                      aia.amount_paid payment_amount,
                      TO_CHAR (apc.check_date,
                               'dd-mm-yyyy hh:mm'
                              ) payment_date,
                      NULL prepay_invoice_num, NULL original_inv_amount
                 FROM apps.ap_invoices_all aia,
                      apps.ap_invoice_payments_all aip,
                      apps.ap_checks_all apc,
                      apps.ap_suppliers aps,
                      apps.ap_supplier_sites_all apsa
                WHERE 1 = 1
                  AND aia.invoice_id = aip.invoice_id
                  AND aip.check_id = apc.check_id
                  AND aps.vendor_id = apsa.vendor_id
                  AND aps.vendor_id = aia.vendor_id
                  AND aia.vendor_site_id = apsa.vendor_site_id
                  --  AND aia.gl_date BETWEEN '10-dec-2018' AND '15-dec-2018'
                  AND (   TRUNC (aip.last_update_date) >= p_run_date
                       OR TRUNC (apc.last_update_date) >= p_run_date
                      )
                  AND aia.payment_status_flag = 'Y'
                  AND aia.SOURCE = 'RETEK'
      UNION
      SELECT DISTINCT ai.invoice_date,
                      REPLACE (ai.invoice_num, '^', '//') invoice_num,
                      SUBSTR (ai.description, 11, 33) po_num,
                      aps.segment1 ofin_vendor_code,
                      apsa.attribute14 retek_vendor_code,
                        (-1)
                      * (ail.amount - NVL (ail.included_tax_amount, 0))
                                                               payment_amount,
                      TO_CHAR (aca.check_date,
                               'dd-mm-yyyy hh:mm'
                              ) payment_date,
                      ai2.invoice_num prepay_invoice_num,
                      ai.amount_paid original_inv_amount
                 FROM apps.ap_invoices_all ai,
                      apps.ap_invoices_all ai2,
                      apps.ap_invoice_lines_all ail,
                      apps.ap_suppliers aps,
                      apps.ap_supplier_sites_all apsa,
                      apps.ap_invoice_payments_all aip,
                      apps.ap_checks_all aca
                WHERE ai.invoice_id = ail.invoice_id
                  AND ai2.invoice_id = ail.prepay_invoice_id
--AND   ail.amount                       < 0
                  AND NVL (ail.discarded_flag, 'N') <> 'Y'
                  AND ai.payment_status_flag = 'Y'
                  AND ail.line_type_lookup_code = 'PREPAY'
                  AND ai.vendor_id = aps.vendor_id
                  AND aps.vendor_id = apsa.vendor_id
                  AND ai.vendor_id = aps.vendor_id
                  AND ai.vendor_site_id = apsa.vendor_site_id
                  AND ai.invoice_type_lookup_code NOT IN
                                            ('PREPAYMENT', 'CREDIT', 'DEBIT')
                  AND aip.invoice_id = ai2.invoice_id
                  AND aca.check_id = aip.check_id
                  --AND ai.gl_date BETWEEN '10-dec-2018' AND '15-dec-2018'
                  AND (   TRUNC (aip.last_update_date) >= p_run_date
                       OR TRUNC (aca.last_update_date) >= p_run_date
                      )
--and ai.invoice_num='11363/17-18'
                  AND ai.SOURCE = 'RETEK';
BEGIN
   ap_file :=
      UTL_FILE.fopen ('/Project%20Gati/Invoice%20Tracker',
                      'GST_ABRL_REIM_INV_PAY_' || g_conc_request_id || '.txt',
                      'w'
                     );
   UTL_FILE.put_line (ap_file,
                         'INVOICE_DATE'
                      || '^'
                      || 'INVOICE_NUM'
                      || '^'
                      || 'PO_NUM'
                      || '^'
                      || 'OFIN_VENDOR_CODE'
                      || '^'
                      || 'RETEK_VENDOR_CODE'
                      || '^'
                      || 'PAYMENT_AMOUNT'
                      || '^'
                      || 'PAYMENT_DATE'
                      || '^'
                      || 'PREPAY_INV_NUM'
                      || '^'
                      || 'ORIGINAL_INV_AMOUNT'
                     );

   FOR ap_reim_inv_pay IN reim_inv_pay
   LOOP
      UTL_FILE.put_line (ap_file,
                            ap_reim_inv_pay.invoice_date
                         || '^'
                         || ap_reim_inv_pay.invoice_num
                         || '^'
                         || ap_reim_inv_pay.po_num
                         || '^'
                         || ap_reim_inv_pay.ofin_vendor_code
                         || '^'
                         || ap_reim_inv_pay.retek_vendor_code
                         || '^'
                         || ap_reim_inv_pay.payment_amount
                         || '^'
                         || ap_reim_inv_pay.payment_date
                         || '^'
                         || ap_reim_inv_pay.prepay_invoice_num
                         || '^'
                         || ap_reim_inv_pay.original_inv_amount
                         || '^'
                        );
   END LOOP;

   UTL_FILE.fclose (ap_file);
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      UTL_FILE.fclose (ap_file);
      fnd_file.put_line (fnd_file.LOG, 'No Data Found');
   WHEN UTL_FILE.invalid_path
   THEN
      UTL_FILE.fclose (ap_file);
      fnd_file.put_line (fnd_file.LOG, 'Invalid Path');
   WHEN UTL_FILE.invalid_filehandle
   THEN
      UTL_FILE.fclose (ap_file);
      fnd_file.put_line (fnd_file.LOG, 'Invalid File Handle');
   WHEN UTL_FILE.invalid_operation
   THEN
      UTL_FILE.fclose (ap_file);
      fnd_file.put_line (fnd_file.LOG, 'Invalid Operation');
   WHEN UTL_FILE.read_error
   THEN
      UTL_FILE.fclose (ap_file);
      fnd_file.put_line (fnd_file.LOG, 'Read Error');
   WHEN UTL_FILE.write_error
   THEN
      UTL_FILE.fclose (ap_file);
      fnd_file.put_line (fnd_file.LOG, 'Write Error');
   WHEN UTL_FILE.internal_error
   THEN
      UTL_FILE.fclose (ap_file);
      fnd_file.put_line (fnd_file.LOG, 'Internal Error');
   WHEN UTL_FILE.file_open
   THEN
      UTL_FILE.fclose (ap_file);
      fnd_file.put_line (fnd_file.LOG, 'File is Open');
   WHEN UTL_FILE.invalid_filename
   THEN
      UTL_FILE.fclose (ap_file);
      fnd_file.put_line (fnd_file.LOG, 'Invalid File Name');
   WHEN OTHERS
   THEN
      UTL_FILE.fclose (ap_file);
      fnd_file.put_line (fnd_file.LOG, 'Unknown Error' || SQLERRM);
      UTL_FILE.fclose (ap_file);
END; 
/


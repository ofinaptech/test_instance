CREATE OR REPLACE PROCEDURE APPS.XXABRL_BANK_REV_FEED_OUTBOUND(ERRBUF  OUT VARCHAR2,
                                                    RETCODE OUT VARCHAR2) AS


 /**********************************************************************************************************************************************
                        WIPRO Infotech Ltd, Mumbai, India

            Program Name: ABRL MIS H2H Reverse Feed Outbound Program
            Description: Supplier Bank Outbound interface for AXIS-DBS-ANZ Banks generating file for incorporating the same in Vendor Invoice Portal

            Change Record:
           =====================
=========================
=========================
=========================
=========================
           Version   Date          Author                    Remarks                  Documnet Ref
           =======   ==========   ==
===========             ==================  ===============
=========================
=============
           1.0.0     09-Jul-2012   Amresh Kumar Chutke      Initial Version
           1.0.1     04-Mar-2014   Dhiresh More             entire report change on basis of view 
  ************************************************************************************************************************************************/
--Declaring Payment Detail Local Variables
LV_REC_COUNT        NUMBER:=0; --initializing counter to 0 
V_BANK_REV_FEED     VARCHAR2(50);
l_file_p            UTL_FILE.file_type;
l_file_p1           UTL_FILE.file_type;
l_pmt_data          VARCHAR2 (4000);
l_pmt_data1         VARCHAR2 (4000);
-- Declaring Payment Cursor to pick up only Authorized Payments ALL Banks
 
 CURSOR CUR_BANK_REV_FEED
    IS
    SELECT * FROM APPS.ABRL_H2H_BANK_REVERS_FEED_OUT;
 
 BEGIN
 
            SELECT  COUNT(*) 
            INTO LV_REC_COUNT
            FROM APPS.ABRL_H2H_BANK_REVERS_FEED_OUT;
 
               -- Generating a Unique Sequence Number to be concatenated with the Bank file
              SELECT BANK_REV_FEED_SEQ.NEXTVAL||'x'||to_char(SYSDATE,'DDMMYYYY') INTO V_BANK_REV_FEED
              FROM DUAL;
-- Reverse Feed File Generation for AXIS Bank
           IF LV_REC_COUNT >0  THEN

                --DBSBANKMIS
            l_file_p := UTL_FILE.fopen ('DBSBANKMIS', 'ABRL_BANK_REVERSE_FEED.'||V_BANK_REV_FEED||'.TXT', 'W');


            FOR REC_BANK_REV_FEED IN CUR_BANK_REV_FEED
            LOOP
               l_pmt_data :=
                        REC_BANK_REV_FEED.DOC_SEQUENCE_VALUE
                    || '^'
                    ||    REC_BANK_REV_FEED.VENDOR_NAME
                    || '^'
                    ||    REC_BANK_REV_FEED.VENDOR_CODE
                    || '^'
                    ||    REC_BANK_REV_FEED.CHECK_ID
                    || '^'
                    ||    REC_BANK_REV_FEED.CHECK_NUMBER
                    || '^'
                    ||    REC_BANK_REV_FEED.TRANSACTION_VALUE_DATE
                    || '^'
                    ||    REC_BANK_REV_FEED.TRANSACTIONAL_AMOUNT
                    || '^'
                    ||    REC_BANK_REV_FEED.BENEFI_ACC_NUM
                    || '^'
                    ||    REC_BANK_REV_FEED.BENEFI_IFSC_CODE
                    || '^'
                    ||    REC_BANK_REV_FEED.ATTRIBUTE3
                    || '^'
                    ||    REC_BANK_REV_FEED.ATTRIBUTE5
                    || '^'
                    ||    REC_BANK_REV_FEED.INVOICE_NUMBER
                    || '^'
                    ||    REC_BANK_REV_FEED.INVOICE_AMOUNT
                    || '^'
                    ||    REC_BANK_REV_FEED.INVOICE_DATE
                    || '^'
                    ||    REC_BANK_REV_FEED.TAX
                    || '^'
                    ||    REC_BANK_REV_FEED.NET_AMOUNT
                    || '^'
                    ||    REC_BANK_REV_FEED.PAY_TYPE_CODE
                    || '^'
                    ||    REC_BANK_REV_FEED.SHORT_CODE;
                   -- || '^'
                   -- ||    NULL ;

               UTL_FILE.put_line (l_file_p, l_pmt_data);
-- Updating the Banks Payment Table

             IF REC_BANK_REV_FEED.CORP_BANK_NAME = 'AXIS Bank Ltd'
             THEN
               UPDATE APPS.AXIS_PAYMENT_TABLE APT
                 SET INTERFACE_STATUS='Y'
                 WHERE 1=1
                   AND TRUNC(APT.TRANSACTION_VALUE_DATE)>='31-MAR-2012'
                   AND APT.TRANSACTION_STATUS<>'AUTHORIZED'
                   AND APT.ATTRIBUTE_CATEGORY IS NOT NULL
                   AND APT.INTERFACE_STATUS IS NULL
                   AND APT.CHECK_ID=REC_BANK_REV_FEED.CHECK_ID;
                   
                  ELSE IF REC_BANK_REV_FEED.CORP_BANK_NAME = 'DBS BANK LTD'
                  THEN
                   UPDATE APPS.DBS_PAYMENT_TABLE APT
                     SET INTERFACE_STATUS='Y'
                     WHERE 1=1
                       AND TRUNC(APT.TRANSACTION_VALUE_DATE)>='31-MAR-2012'
                       AND APT.TRANSACTION_STATUS<>'AUTHORIZED'
                       AND APT.ATTRIBUTE_CATEGORY IS NOT NULL
                       AND APT.INTERFACE_STATUS IS NULL
                       AND APT.CHECK_ID=REC_BANK_REV_FEED.CHECK_ID;
                   
                      ELSE IF REC_BANK_REV_FEED.CORP_BANK_NAME = 'ANZ Bank'
                      THEN
                       UPDATE APPS.ANZ_PAYMENT_TABLE APT
                         SET INTERFACE_STATUS='Y'
                         WHERE 1=1
                           AND TRUNC(APT.TRANSACTION_VALUE_DATE)>='31-MAR-2012'
                           AND APT.TRANSACTION_STATUS<>'AUTHORIZED'
                           AND APT.ATTRIBUTE_CATEGORY IS NOT NULL
                           AND APT.INTERFACE_STATUS IS NULL
                           AND APT.CHECK_ID=REC_BANK_REV_FEED.CHECK_ID;
                   
                          ELSE IF REC_BANK_REV_FEED.CORP_BANK_NAME = 'Standard Chartered Bank'
                          THEN
                           UPDATE APPS.SCB_PAYMENT_TABLE APT
                             SET INTERFACE_STATUS='Y'
                             WHERE 1=1
                               AND TRUNC(APT.TRANSACTION_VALUE_DATE)>='31-MAR-2012'
                               AND APT.TRANSACTION_STATUS<>'AUTHORIZED'
                               AND APT.ATTRIBUTE_CATEGORY IS NOT NULL
                               AND APT.INTERFACE_STATUS IS NULL
                               AND APT.CHECK_ID=REC_BANK_REV_FEED.CHECK_ID;
                   
                              ELSE IF REC_BANK_REV_FEED.CORP_BANK_NAME = 'YES BANK'
                              THEN
                               UPDATE APPS.YESBANK_PAYMENT_TABLE APT
                                 SET INTERFACE_STATUS='Y'
                                 WHERE 1=1
                                   AND TRUNC(APT.TRANSACTION_VALUE_DATE)>='31-MAR-2012'
                                   AND APT.TRANSACTION_STATUS<>'AUTHORIZED'
                                   AND APT.ATTRIBUTE_CATEGORY IS NOT NULL
                                   AND APT.INTERFACE_STATUS IS NULL
                                   AND APT.CHECK_ID=REC_BANK_REV_FEED.CHECK_ID;     
                              END IF;
                          END IF;     
                      END IF;
                  END IF;
              END IF;
                COMMIT;

            END LOOP;

            UTL_FILE.fclose (l_file_p);

            END IF;
 -- Reverse Feed File Generation for DBS Bank
    
 EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               UTL_FILE.fclose (l_file_p);
               DBMS_OUTPUT.PUT_LINE( 'No Data Found');
            WHEN UTL_FILE.invalid_path
            THEN
               UTL_FILE.fclose (l_file_p);
               DBMS_OUTPUT.PUT_LINE( 'Invalid Path');
            WHEN UTL_FILE.invalid_filehandle
            THEN
               UTL_FILE.fclose (l_file_p);
               DBMS_OUTPUT.PUT_LINE( 'Invalid File Handle');
            WHEN UTL_FILE.invalid_operation
            THEN
               UTL_FILE.fclose (l_file_p);
               DBMS_OUTPUT.PUT_LINE( 'Invalid Operation');
            WHEN UTL_FILE.read_error
            THEN
               UTL_FILE.fclose (l_file_p);
               DBMS_OUTPUT.PUT_LINE( 'Read Error');
            WHEN UTL_FILE.write_error
            THEN
               UTL_FILE.fclose (l_file_p);
               DBMS_OUTPUT.PUT_LINE( 'Write Error');
            WHEN UTL_FILE.internal_error
            THEN
               UTL_FILE.fclose (l_file_p);
               DBMS_OUTPUT.PUT_LINE( 'Internal Error');
            WHEN UTL_FILE.file_open
            THEN
               UTL_FILE.fclose (l_file_p);
               DBMS_OUTPUT.PUT_LINE( 'File is Open');
            WHEN UTL_FILE.invalid_filename
            THEN
               UTL_FILE.fclose (l_file_p);
               DBMS_OUTPUT.PUT_LINE( 'Invalid File Name');
            WHEN OTHERS
            THEN
               UTL_FILE.fclose (l_file_p);
               DBMS_OUTPUT.PUT_LINE( 'Unknown Error' || SQLERRM);
END XXABRL_BANK_REV_FEED_OUTBOUND; 
/


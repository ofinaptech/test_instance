CREATE OR REPLACE PROCEDURE APPS.XXABRL_HYPN_DET_REP_CALLING(ERRBUF OUT VARCHAR2,
                                                     RETCODE OUT VARCHAR2,
                                                     APPL_NAME IN VARCHAR2,                                                     
                                                     PERIOD_NAME IN VARCHAR2,
                                                     VERSION_NO IN VARCHAR
                                                     )
AS   

/*
  =========================================================================================================
  ||   Filename   : XXABRL_HYPN_DET_REP_CALLING.sql
  ||   Description : Script is used to call the Hyperion Detailed Reports
  ||
  ||   Version     Date            Author              Modification
  ||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
  ||   1.0.0       28-APR-2010    Naresh Hasti           New Development
  ||   ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
  ||
  ||
  ========================================================================================================*/


  v_result BOOLEAN;  
  v_request_id NUMBER;
  v_user_id                    NUMBER := fnd_profile.VALUE ('USER_ID');
  v_resp_id                    NUMBER := fnd_profile.VALUE ('RESP_ID');                                               
  v_resp_appl_id               NUMBER := fnd_profile.VALUE ('RESP_APPL_ID');
  V_TEMPALTE_CODE              VARCHAR2(50); 

 BEGIN

 IF APPL_NAME='SUPER_OPS' THEN 
 
    V_TEMPALTE_CODE:= 'XXABRLHYPSUPOPSDET';
 ELSIF APPL_NAME='HYPER_OPS' THEN
  
 V_TEMPALTE_CODE:= 'XXABRLHYPHYPEROPSDET';
 ELSIF  APPL_NAME='OHDS_OPS' THEN 
 
    V_TEMPALTE_CODE:= 'XXABRLHYPOHDSOPSDET';
 ELSIF  APPL_NAME='SCM_OPS' THEN 
 
    V_TEMPALTE_CODE:= 'XXABRLHYPSCMOPSDET';
 ELSIF  APPL_NAME='SCM_MCD' THEN 
 
    V_TEMPALTE_CODE:= 'XXABRLHYPSCMMCDDET';
 ELSIF  APPL_NAME='SUPER_MCD' THEN 
 
    V_TEMPALTE_CODE:= 'XXABRLHYPSUPMCDDET';
 ELSIF  APPL_NAME='HYPER_MCD' THEN 
 
    V_TEMPALTE_CODE:= 'XXABRLHYPHYPERMCDDET';
 ELSIF APPL_NAME='V_MERCH' THEN 
 
    V_TEMPALTE_CODE:= 'XXABRLHYPVMERCHDET';
 ELSIF  APPL_NAME='CONSOL' THEN 
 
    V_TEMPALTE_CODE:= 'XXABRLHYPCONSOLDET';
    ELSIF  APPL_NAME='OHDS_MCD' THEN 
 
    V_TEMPALTE_CODE:= 'XXABRLHYPOHDSMCDDET';
    ELSIF  APPL_NAME='MARKETING' THEN 
 
    V_TEMPALTE_CODE:= 'XXABRLHYPMKTNGDET';
    
 END IF;
    
              
     --FND_GLOBAL.APPS_INITIALIZE(0,53661,101);    
     FND_GLOBAL.APPS_INITIALIZE(v_user_id,v_resp_id,v_resp_appl_id );
       
  v_result := FND_REQUEST.add_layout(template_appl_name => 'XXABRL',
                                    template_code     => V_TEMPALTE_CODE,
                                    template_language => 'US',
                                    template_territory => NULL,
                                    output_format     => 'EXCEL'
                                  );   
  --DBMS_OUTPUT.PUT_LINE(' Result Id :'||v_result);                                   

  IF v_result THEN
       v_request_id := FND_REQUEST.submit_request(
                                             application => 'XXABRL',
                                             program     => V_TEMPALTE_CODE,                                             
                                             start_time  => SYSDATE,
                                             sub_request => FALSE,
                                             argument1   => PERIOD_NAME,
                                             argument2   => version_no
                                               );
     IF v_request_id > 0 THEN
       DBMS_OUTPUT.PUT_LINE('Report Submitted. Request Id :'||v_request_id);       
         COMMIT;
     ELSE
       DBMS_OUTPUT.PUT_LINE('Error while submitting Report request');
     END IF;
  ELSE
     DBMS_OUTPUT.PUT_LINE('Could not locate the Template');
  END IF;

  END; 
/


CREATE OR REPLACE PACKAGE BODY APPS.XXABRL_SMDN_INV_INTERFACE IS
/***********************************************************************************************************
                 OBJECT INFORMATION

 Object Name : XXABRL_SMDN_INV_INTERFACE

 Description : Migration of Invoices from REIM to Oracle Payables

  Change Record:
 =================================================
=========================
====================
 Version    Date            Author                              Remarks
 =======   ==========     =====
========                         =================================
===========
 1.0.0     31-Dec-2010     Mitul (Wipro Infotech Ltd)    Loadin Super Market Debit Note
 1.0.1     01-Apr-2011     Mitul (Wipro Infotech Ltd)    ADDED VENDOR SITE CODE COLUMN
 1.0.2     26-Apr-2014      Dhiresh More                 Changes done for Unique Vendor
***********************************************************************************************************
*/


  /*======================================Variable Declaration ===
=========================
================*/

  PROCEDURE DEBIT_NOTE_VALIDATE_INVOICES(Errbuf OUT VARCHAR2, RetCode OUT NUMBER) IS
    v_invoice_num                VARCHAR2(50) := NULL;
    v_count                      NUMBER := NULL;
    v_data_count                 NUMBER;
    v_inactive_site              NUMBER;
    v_error_flag                 VARCHAR2(1) := NULL;
    v_error_line_flag            VARCHAR2(1) := NULL;
    v_hdr_err_msg                VARCHAR2(1000) := NULL;
    v_line_err_msg               VARCHAR2(1000) := NULL;
    v_error_almsg                VARCHAR2(4000) := NULL;
    v_vendor_id                  NUMBER(15) := NULL;
    v_vendor_site_id             NUMBER(15) := NULL;
    v_vendor_site_code           VARCHAR2(30) := NULL;
    v_invoice_currency_code      VARCHAR2(30) := NULL;
    v_payment_method_lookup_code VARCHAR2(30) := NULL;
    v_dist_code_concatenated     VARCHAR2(300) := NULL;

    -- following variavble created by shailesh on 25 OCT 2008 for storing the default ACCOUNTING SEGMENT VALUES FOR VENDOR
    l_dist_code_concatenated VARCHAR2(300) := NULL;
    l_nat_acct               VARCHAR2(10);
    -- will store the terms_id from the vendor master see the validation for details
    l_terms_id NUMBER;
    -- CAPTURE THE LOCATION FROM DIST_CODE_COMBINITION (SEGMENT4) VALUE
    L_LOCATION VARCHAR2(10);

    --rec_inv_hdr.vendor_num                         VARCHAR2(50)        := NULL;
    v_check_flag            VARCHAR2(1) := NULL;
    v_fun_curr              VARCHAR2(15) := NULL;
    v_flag_count            NUMBER := 0;
    v_set_of_books_id       NUMBER := FND_PROFILE.VALUE('GL_SET_OF_BKS_ID');
    v_pay_lookup_code       VARCHAR2(50) := NULL;
    v_term_name             ap_terms_tl.NAME%TYPE;
    v_error_head_count      NUMBER := 0;
    v_success_record_count  NUMBER := 0;
    v_error_line_count      NUMBER := 0;
    v_line_type_lookup_code VARCHAR2(25) := NULL;
    v_inv_line_amt          NUMBER := 0;
    --v_user_id                                  NUMBER              :=FND_PROFILE.value('USER_ID');
    --v_resp_id                                  NUMBER              :=FND_PROFILE.value('RESP_ID');
    --v_appl_id                                  NUMBER              :=FND_PROFILE.value('RESP_APPL_ID');
    v_inv_type            VARCHAR2(30) := NULL;
    v_total_error_records NUMBER := 0;
    v_line_count          NUMBER := 0;
    v_total_records       NUMBER := 0;
    v_sup_status          VARCHAR2(1) := NULL;

    v_dummy_flag                 VARCHAR2(1) := NULL;
    v_purchasing_site_flag       VARCHAR2(1) := NULL;
    v_rfq_only_site_flag         VARCHAR2(1) := NULL;
    p_org_id                     NUMBER := NULL;
    p_vendor_site_id             NUMBER(15) := NULL;
    p_vendor_id                  NUMBER(15) := NULL;
    p_payment_method_lookup_code VARCHAR2(30) := NULL;
    p_attribute14                VARCHAR2(150) := NULL;
    v_pay_site_flag              VARCHAR2(1) := NULL;
    X_pay_site_flag              VARCHAR2(1) := NULL;
    v_pay_count                  NUMBER;
    v_rfq_count                  NUMBER;
    v_pur_count                  NUMBER;
    v_ho_count                   NUMBER;
    v_org_cnt                    number:=0;
    /*=============================Declaration of 'INVOICES' cursor ==================================================*/


    CURSOR cur_inv_hdr IS

      SELECT aps.ROWID,
             aps.INVOICE_NUM,
             aps.INVOICE_TYPE_LOOKUP_CODE,
             aps.INVOICE_DATE,
             aps.VENDOR_NUM,
             aps.TERMS_ID,
             ROUND(aps.INVOICE_AMOUNT, 2) INVOICE_AMOUNT,
             aps.INVOICE_CURRENCY_CODE,
             aps.EXCHANGE_DATE,
             aps.EXCHANGE_RATE_TYPE,
             aps.DESCRIPTION,
             aps.GL_DATE,
             aps.GROUP_ID,
             aps.VENDOR_SITE_CODE,
             aps.TERMS_DATE,
             aps.ACCTS_PAY_CODE_CONCATENATED,
             aps.ERROR_FLAG,
             aps.ERROR_MSG,
             aps.GOODS_RECEIVED_DATE, -- New coloumn added in the  xxabrl.XXABRL_AP_SMDN_HEADER_STAGE
             aps.ORG_ID,
             aps.attribute14          -- New coloumn added in the  xxabrl.XXABRL_AP_SMDN_HEADER_STAGE  
        FROM apps.XXABRL_AP_SMDN_HEADER_STAGE aps
       WHERE 1=1
       and   aps.GL_DATE        >=   trunc(sysdate)-180  ---added on 06-Apr-2016
       and NVL(aps.error_flag, 'N') IN ('N', 'E');
    --AND     APS.INVOICE_NUM IN ('INV_10563');


    ------------------------ Start  new cursor added for updating vendor site code in stage table  for unique vendor -----------dt 26Feb2014 -- Dhiresh

    CURSOR cur_inv_hdr_chk IS

    select aps.ROWID,
           aps.INVOICE_NUM,
           aps.VENDOR_NUM,
           aps.ORG_ID,
           aps.ATTRIBUTE14  from apps.XXABRL_AP_SMDN_HEADER_STAGE aps
       WHERE NVL(aps.error_flag, 'N') IN ('N', 'E')
       and   aps.GL_DATE        >=   trunc(sysdate)-180  ---added on 06-Apr-2016
       and aps.attribute14 is not null;
       
------------------------  End new cursor added for updating vendor site code in stage table  for unique vendor -----------dt 26Feb2014 -- Dhiresh


    CURSOR cur_inv_line(p_inv_num VARCHAR2,
    --p_vendor_site_id NUMBER,
    p_vendor_num NUMBER) IS
      SELECT als.ROWID,
             als.VENDOR_NUM,
             als.VENDOR_SITE_ID,
             als.INVOICE_NUM,
             als.LINE_NUMBER,
             als.LINE_TYPE_LOOKUP_CODE,
             als.AMOUNT AMOUNT,
             als.ACCOUNTING_DATE,
             als.DIST_CODE_CONCATENATED,
             als.DESCRIPTION,
             als.SAC_CODE,     --added on 09-Aug-2017
             als.TDS_TAX_NAME,
             als.WCT_TAX_NAME,
             als.SERVICE_TAX,
             als.VAT
        FROM apps.XXABRL_SMDN_AP_LINE_STAGE als
       WHERE NVL(als.error_flag, 'N') <> 'Y' --in('N','E')
         and  als.ACCOUNTING_DATE  >= trunc(sysdate)-180  ---added on 06-Apr-2016
         AND als.invoice_num = p_inv_num
         AND als.vendor_num = p_vendor_num;



    CURSOR cur_check_flags(P_INV_NUM VARCHAR2,p_vendor_num VARCHAR2,CP_ORG_ID NUMBER) IS
         SELECT asups.org_id,
             asups.vendor_site_id,
             asups.vendor_id,
             asups.payment_method_lookup_code,
             asups.pay_site_flag,
             asups.PURCHASING_SITE_FLAG,
             asups.RFQ_ONLY_SITE_FLAG,
             asups.attribute14          
        FROM ap_suppliers asup, ap_supplier_sites_all asups,apps.XXABRL_AP_SMDN_HEADER_STAGE STG
       WHERE asup.vendor_id = asups.vendor_id
      --  AND ASUPS.VENDOR_ID=STG.VENDOR_ID
        AND UPPER(ASUPS.VENDOR_SITE_CODE)=UPPER(STG.VENDOR_SITE_CODE)
        AND ASUP.segment1 = p_vendor_num
        AND STG.INVOICE_NUM= P_INV_NUM
        AND NVL(ASUPS.attribute14,1) = NVL(STG.attribute14,1)   --- New condition added by Dhiresh for unique vendor dt. 26Feb14
        AND ASUPS.ORG_ID = CP_ORG_ID
        AND ASUPS.PAY_SITE_FLAG='Y' 
        AND NVL(TRUNC(asups.INACTIVE_DATE), trunc(SYSDATE)) >= TRUNC(SYSDATE);
         

  BEGIN


    -- Error Report header
    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                      '                             AP Invoice Interface Validation Report');
    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                      'Run Date      : ' ||
                      TO_CHAR(TRUNC(SYSDATE), 'DD-MON-RRRR'));
    FND_FILE.PUT_LINE(FND_FILE.OUTPUT, CHR(10));
    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                      '-------------------------------------------------------------------------------------------------');
    -- FND_FILE.PUT_LINE (FND_FILE.OUTPUT,CHR(10));
    --FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'File Date     : '||TO_CHAR(c_file_rec.file_date,'DD-MON-RRRR:HH24:MI:SS'));
    --  FND_FILE.PUT_LINE (FND_FILE.OUTPUT,CHR(10));
    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                      RPAD('Vendor No', 20) || RPAD('Invoice No', 20) ||
                      RPAD('Error Message', 400));

    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                      'SET OF BOOK ID  ->  ' || v_set_of_books_id);

    SELECT COUNT(INVOICE_NUM)
      INTO v_total_records
      FROM apps.XXABRL_AP_SMDN_HEADER_STAGE
     WHERE error_flag IN ('N', 'E');

    --Get Functional Currency
    BEGIN
      SELECT Currency_code
        INTO v_fun_curr
        FROM GL_SETS_OF_BOOKS
       WHERE set_of_books_id = v_set_of_books_id;

    EXCEPTION
      WHEN OTHERS THEN
        v_fun_curr := NULL;
    END;
    
    -------------------------  code added by Dhiresh on 26Feb2014 for unique vendor  ---------------------------
    
                -------------Start new code added for updating vendor site code in stage table  for unique vendor -----------dt 26Feb2014 -- Dhiresh        
    BEGIN
    
        FOR recch_inv_hdr IN cur_inv_hdr_chk Loop
          EXIT WHEN cur_inv_hdr_chk%NOTFOUND;
          FND_FILE.PUT_LINE(FND_FILE.log,recch_inv_hdr.vendor_num);
            FND_FILE.PUT_LINE(FND_FILE.log,recch_inv_hdr.ORG_ID);
              FND_FILE.PUT_LINE(FND_FILE.log,recch_inv_hdr.attribute14);
          IF recch_inv_hdr.attribute14 is not null then
             update apps.XXABRL_AP_SMDN_HEADER_STAGE set vendor_site_code = (select asups.vendor_site_code from
                                             ap_suppliers asup, ap_supplier_sites_all asups
                                             where asup.vendor_id = asups.vendor_id 
                                             and ASUP.segment1=recch_inv_hdr.vendor_num
                                             and ASUPS.ORG_ID=recch_inv_hdr.ORG_ID
                                             and ASUPS.ATTRIBUTE14=recch_inv_hdr.attribute14
                                             AND ASUPS.PAY_SITE_FLAG='Y' 
                                             AND NVL(TRUNC(asups.INACTIVE_DATE), trunc(SYSDATE)) >= TRUNC(SYSDATE))  
             where rowid=recch_inv_hdr.rowid;
             
          
          END IF;
          
        
        END Loop;
    
    END;
     
            -------------End new code added for updating vendor site code in stage table  for unique vendor -----------dt 26Feb2014 -- Dhiresh
    
    
    FOR rec_inv_hdr IN cur_inv_hdr LOOP
      EXIT WHEN cur_inv_hdr%NOTFOUND;
      v_error_flag                 := NULL;
      v_hdr_err_msg                := NULL;
      v_vendor_id                  := NULL;
      v_vendor_site_id             := NULL;
      v_invoice_currency_code      := NULL;
      v_payment_method_lookup_code := NULL;
      -- rec_inv_hdr.vendor_num                        := NULL;
      v_check_flag      := 'N';
      v_pay_lookup_code := NULL;
      --v_error_head_count                   := 0;
      v_inv_line_amt := 0;
      v_inv_type     := NULL;
      --v_success_record_count                := 0;
      v_sup_status := 'N';
      --    p_org_id                               := NULL;
      --    p_vendor_site_id                       := NULL;
      --    p_vendor_id                            := NULL;
      --    p_payment_method_lookup_code           := NULL;
      --    v_pay_site_flag                        := NULL;
      v_flag_count := 0;
      v_pay_count  := 0;
      v_rfq_count  := 0;
      v_pur_count  := 0;
      v_ho_count   := 0;

      
      
        --- org id validation added by shailesh on 22 june 2009    
     begin
 
        FND_FILE.PUT_LINE(FND_FILE.log,'Validation for org id -> '||rec_inv_hdr.org_id);
      
      select count(organization_id)
      into v_org_cnt
      from apps.hr_operating_units hou
      where hou.organization_id=rec_inv_hdr.org_id;
      
     ---- we need to skip full validation in this case if org_id is null 

             
      exception 
      
      when no_data_found then
       v_error_flag  := 'Y';
         v_hdr_err_msg := 'ORG is Null or ORG not available';
       FND_FILE.PUT_LINE(FND_FILE.log,'ORG is Null or ORG not available-> '||v_error_flag);
      when others then 
       v_error_flag  := 'Y';
         v_hdr_err_msg := 'ORG is Null or ORG not available';
       FND_FILE.PUT_LINE(FND_FILE.log,'Error in ORG ID -> '||v_error_flag);
      
      end ;
      
      
     if v_org_cnt = 0 then
        v_error_flag := 'Y';
        v_hdr_err_msg := 'ORG is Null or ORG not available';
        p_vendor_site_id             := null;
        p_vendor_id                  := null;
        p_payment_method_lookup_code := null;
        FND_FILE.PUT_LINE(FND_FILE.log,'Invalid Org ID -> '||v_error_flag);
     end if;
     
      
    --    FND_FILE.PUT_LINE(FND_FILE.log,'Error Flag -> '||v_error_flag);
      
      
      
      
      IF rec_inv_hdr.vendor_num IS NULL THEN
        DBMS_OUTPUT.PUT_LINE('NO SUPPLIER FOR INVOICE NUMBER');
      ELSE

      FND_FILE.PUT_LINE(FND_FILE.log,'ORG ID PARAMETER --> cur_inv_hdr 2ND FOR LOOP --> '||rec_inv_hdr.ORG_ID);
      
      
       
      END IF;
                 

      /*======================
== Check for suppliers status==============
=========================
=============*/

      IF rec_inv_hdr.vendor_num IS NOT NULL THEN
        BEGIN
          SELECT 'Y'
            INTO v_sup_status
            FROM ap_suppliers
           WHERE segment1 = rec_inv_hdr.vendor_num
             AND SYSDATE BETWEEN NVL(start_date_active, SYSDATE) AND
                 NVL(end_date_active, SYSDATE); -- added by lalit DT 27-JUN-2008
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            v_error_flag  := 'Y';
            v_hdr_err_msg := 'INVALID SUPPLIER';
            FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                              RPAD(NVL(rec_inv_hdr.vendor_num, ' '), 20) ||
                              RPAD(rec_inv_hdr.INVOICE_NUM, 20) ||
                              v_hdr_err_msg);
        END;
      END IF;

      IF v_sup_status = 'Y' THEN
      FOR rec_check_flags IN cur_check_flags(rec_inv_hdr.INVOICE_NUM,rec_inv_hdr.vendor_num,rec_inv_hdr.ORG_ID) LOOP
          EXIT WHEN cur_check_flags%NOTFOUND;
            
          
            
            IF NVL(rec_check_flags.pay_site_flag, 'N') = 'Y' THEN
      
                  p_org_id                     := rec_check_flags.org_id;
                  p_vendor_site_id             := rec_check_flags.vendor_site_id;
                  p_vendor_id                  := rec_check_flags.vendor_id;
                  p_payment_method_lookup_code := rec_check_flags.payment_method_lookup_code;
                  
            END IF;
                        --Mitul   END IF;
       end loop;
         
       
        
        -- added by shailesh on 24 june 2009 as vendor site id is not available in ofin for some vendor.
        if p_vendor_site_id is null then
        
          
             
        
                SELECT 
                COUNT(*) INTO X_pay_site_flag             
                FROM ap_suppliers asup, ap_supplier_sites_all asups,apps.XXABRL_AP_SMDN_HEADER_STAGE STG
                WHERE asup.vendor_id = asups.vendor_id
                --  AND ASUPS.VENDOR_ID=STG.VENDOR_ID
                AND UPPER(ASUPS.VENDOR_SITE_CODE)=UPPER(STG.VENDOR_SITE_CODE)
                AND asup.segment1 = rec_inv_hdr.vendor_num
                AND STG.INVOICE_NUM= rec_inv_hdr.INVOICE_NUM
                AND ASUPS.ORG_ID = rec_inv_hdr.ORG_ID
                --AND ASUPS.PAY_SITE_FLAG='Y' 
                AND NVL(TRUNC(asups.INACTIVE_DATE), trunc(SYSDATE)) >= TRUNC(SYSDATE);
        
        
        IF X_pay_site_flag=1 then
        
            v_error_flag  := 'Y';
                    
                      v_hdr_err_msg :=  v_hdr_err_msg||rec_inv_hdr.vendor_site_code|| ' VENDOR SITE Not Define as a PAY Site '||rec_inv_hdr.vendor_num ||'  ';
                      FND_FILE.PUT_LINE(FND_FILE.log,'ORG is Null or ORG not available-> '||v_error_flag);
                      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                                        RPAD(NVL(rec_inv_hdr.vendor_num, ' '), 20) ||
                                        RPAD(rec_inv_hdr.INVOICE_NUM, 20) ||
                                        v_hdr_err_msg);
        
        ELSE
        
         v_error_flag  := 'Y';
                    
                      v_hdr_err_msg :=  v_hdr_err_msg||'-'|| ' VENDOR SITE Not Found for vendor '||rec_inv_hdr.vendor_num ||'  ';
                      FND_FILE.PUT_LINE(FND_FILE.log,'ORG is Null or ORG not available-> '||v_error_flag);
                      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                                        RPAD(NVL(rec_inv_hdr.vendor_num, ' '), 20) ||
                                        RPAD(rec_inv_hdr.INVOICE_NUM, 20) ||
                                        v_hdr_err_msg);                             
                
                                        
        
            end if;

        end if;
        
        
      END IF;

      

      /*======================
== Validation for Invoice Type Lookup Code==================================
==================*/

      IF rec_inv_hdr.invoice_type_lookup_code IS NULL THEN
        v_hdr_err_msg := v_hdr_err_msg || 'INVOICE TYPE IS NULL';
      ELSE
        BEGIN
          SELECT lookup_code
            INTO v_inv_type
            FROM ap_lookup_codes
           WHERE UPPER(lookup_code) =
                 UPPER(rec_inv_hdr.invoice_type_lookup_code)
             AND lookup_type = 'INVOICE TYPE';
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            v_error_flag  := 'Y';
            v_hdr_err_msg := 'INVOICE TYPE DOES NOT EXISTS';
            FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                              RPAD(NVL(rec_inv_hdr.vendor_num, ' '), 20) ||
                              RPAD(rec_inv_hdr.INVOICE_NUM, 20) ||
                              v_hdr_err_msg);
          WHEN TOO_MANY_ROWS THEN
            v_error_flag  := 'Y';
            v_hdr_err_msg := 'MULTIPLE INVOICE TYPE FOUND';
          WHEN OTHERS THEN
            v_error_flag  := 'Y';
            v_hdr_err_msg := 'INVALID INVOICE TYPE';
            FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                              RPAD(NVL(rec_inv_hdr.vendor_num, ' '), 20) ||
                              RPAD(rec_inv_hdr.INVOICE_NUM, 20) ||
                              v_hdr_err_msg);
        END;
      END IF;

      /*======================
======Check Invoice Number Already exists and Invoice Number Duplication======================
==============*/

      IF rec_inv_hdr.invoice_num IS NOT NULL AND v_sup_status = 'Y' THEN
        BEGIN
          v_data_count := 0;
          SELECT COUNT(invoice_num)
            INTO v_data_count
            FROM ap_invoices_all
           WHERE invoice_num = rec_inv_hdr.invoice_num
             AND vendor_id = p_vendor_id
             -- added by shailesh on 20 june 2009 as org id comming from CSV file to staging table. 
             --AND org_id = p_org_id;
             AND org_id = rec_inv_hdr.org_id;

          IF v_data_count <> 0 THEN
            v_error_flag  := 'Y';
            v_hdr_err_msg := 'INVOICE NUMBER (' || ' ' ||
                             rec_inv_hdr.invoice_num || ' ' ||
                             ') ALREADY EXISTS FOR SUPPLIER' || ' ' ||
                             rec_inv_hdr.vendor_num;
            FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                              RPAD(NVL(rec_inv_hdr.vendor_num, ' '), 20) ||
                              RPAD(rec_inv_hdr.invoice_num, 20) ||
                              v_hdr_err_msg);
          END IF;
          v_data_count := 0;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            v_error_flag  := 'Y';
            v_hdr_err_msg := 'INVOICE NUMBER DOES NOT EXISTS';
            FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                              RPAD(NVL(rec_inv_hdr.vendor_num, ' '), 20) ||
                              RPAD(rec_inv_hdr.invoice_num, 20) ||
                              v_hdr_err_msg);
          WHEN OTHERS THEN
            v_error_flag  := 'Y';
            v_hdr_err_msg := SQLERRM;
        END;
        BEGIN
          v_data_count := 0;
          SELECT COUNT(invoice_num)
            INTO v_data_count
            FROM XXABRL_AP_SMDN_HEADER_STAGE
           WHERE invoice_num = rec_inv_hdr.invoice_num
             AND vendor_num = rec_inv_hdr.vendor_num;

          IF v_data_count > 1 THEN
            v_error_flag  := 'Y';
            v_hdr_err_msg := 'DUPLICATE INVOICE NUMBER.THIS INVOICE ALREADY EXISTS FOR' ||
                             CHR(10) || RPAD(' ', 20) || '' ||
                             RPAD(' ', 20) || 'SUPPLIER ' || ' ' ||
                             rec_inv_hdr.vendor_num || ' ' ||
                             'AND SUPPLIER SITE' || ' ' ||
                             v_vendor_site_code;
            FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                              RPAD(NVL(rec_inv_hdr.vendor_num, ' '), 20) ||
                              RPAD(rec_inv_hdr.invoice_num, 20) ||
                              v_hdr_err_msg);
          END IF;
          v_data_count := 0;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            v_error_flag  := 'Y';
            v_hdr_err_msg := 'INVOICE NUMBER DOES NOT EXISTS';
            FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                              RPAD(NVL(rec_inv_hdr.vendor_num, ' '), 20) ||
                              RPAD(rec_inv_hdr.invoice_num, 20) ||
                              v_hdr_err_msg);
          WHEN OTHERS THEN
            v_error_flag  := 'Y';
            v_hdr_err_msg := v_hdr_err_msg || ' ' || SQLERRM;
        END;
        --   ELSE
        --         v_error_flag := 'Y';
        --         v_hdr_err_msg := v_hdr_err_msg ||' '|| 'INVOICE NUMBER SHOULD NOT BE NULL';
        --         FND_FILE.PUT_LINE (FND_FILE.OUTPUT,v_hdr_err_msg);
      END IF;

      /*============================Validate for Invoice Amount with Transaction Type====================================*/

      IF UPPER(rec_inv_hdr.invoice_type_lookup_code) = 'STANDARD' AND
         rec_inv_hdr.invoice_amount < 0 THEN
        v_error_flag  := 'Y';
        v_hdr_err_msg := 'INVOICE AMOUNT CAN NOT BE NEGATIVE FOR STANDARD INVOICE';
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                          RPAD(NVL(rec_inv_hdr.vendor_num, ' '), 20) ||
                          RPAD(rec_inv_hdr.invoice_num, 20) ||
                          v_hdr_err_msg);
      ELSIF UPPER(rec_inv_hdr.invoice_type_lookup_code) = 'CREDIT' AND
            rec_inv_hdr.invoice_amount >= 0 THEN
        v_error_flag  := 'Y';
        v_hdr_err_msg := 'INVOICE AMOUNT CAN NOT BE POSITIVE FOR CREDIT MEMO';
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                          RPAD(NVL(rec_inv_hdr.vendor_num, ' '), 20) ||
                          RPAD(rec_inv_hdr.invoice_num, 20) ||
                          v_hdr_err_msg);
      END IF;

      /*============================Validate the currency_code in invoice header table====================================*/

      IF rec_inv_hdr.invoice_currency_code IS NOT NULL THEN
        BEGIN
          SELECT currency_code
            INTO v_invoice_currency_code
            FROM fnd_currencies
           WHERE UPPER(currency_code) =
                 TRIM(UPPER(rec_inv_hdr.invoice_currency_code));

          IF UPPER(TRIM(v_invoice_currency_code)) <>
             UPPER(trim(v_fun_curr)) AND rec_inv_hdr.exchange_date IS NULL THEN
            v_error_flag  := 'Y';
            v_hdr_err_msg := 'EXCHANGE DATE IS NULL';
            FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                              RPAD(NVL(rec_inv_hdr.vendor_num, ' '), 20) ||
                              RPAD(rec_inv_hdr.invoice_num, 20) ||
                              v_hdr_err_msg);
          END IF;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            v_error_flag  := 'Y';
            v_hdr_err_msg := rec_inv_hdr.INVOICE_CURRENCY_CODE || ' _ ' ||
                             ' INVOICE_CURRENCY_CODE is not valid';
            FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                              RPAD(NVL(rec_inv_hdr.vendor_num, ' '), 20) ||
                              RPAD(rec_inv_hdr.invoice_num, 20) ||
                              v_hdr_err_msg);
        END;
      ELSE
        v_error_flag  := 'Y';
        v_hdr_err_msg := 'INVOICE CURRENCY CODE SHOULD NOT BE NULL';
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                          RPAD(NVL(rec_inv_hdr.vendor_num, ' '), 20) ||
                          RPAD(rec_inv_hdr.invoice_num, 20) ||
                          v_hdr_err_msg);
      END IF;

      /*============================Validate the payment_method_lookup_code in invoice header ==================================*/

     
      --- Terms_id is taken from vendor master

      BEGIN

        SELECT NAME, apt.term_id
          INTO v_term_name, l_terms_id
          FROM ap_terms_tl apt, po_vendor_sites_all pvsa
         WHERE pvsa.terms_id = apt.term_id
           AND pvsa.vendor_id = p_vendor_id
           AND pvsa.vendor_site_id = p_vendor_site_id
           AND NVL(apt.Enabled_flag, 'N') = 'Y'
           AND SYSDATE BETWEEN NVL(apt.start_date_active, SYSDATE) AND
               NVL(apt.end_date_active, SYSDATE);

      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          l_terms_id := NULL;

        WHEN TOO_MANY_ROWS THEN
          v_error_flag  := 'Y';
          v_hdr_err_msg := 'MULTIPLE TERM IDS FOUND';
          FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                            RPAD(NVL(rec_inv_hdr.vendor_num, ' '), 20) ||
                            RPAD(rec_inv_hdr.INVOICE_NUM, 20) ||
                            v_hdr_err_msg);
        WHEN OTHERS THEN
          v_error_flag  := 'Y';
          v_hdr_err_msg := ' TERM ID';
          FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                            RPAD(NVL(rec_inv_hdr.vendor_num, ' '), 20) ||
                            RPAD(rec_inv_hdr.INVOICE_NUM, 20) ||
                            v_hdr_err_msg);
      END;

      --   apps.XXABRL_AP_SMDN_HEADER_STAGE  l_dist_code_concatenated

      /*============================Validation for  ACCTS_PAY_CODE_CONCATENATED ==================================*/

      -- New validation for the dist code concatenated  added by shailesh on 25 OCT 2008
      -- DIST CODE CONCATENATED SEGMENTS - Default accounting segments from vendor master table
      -- it will pick up the only segm,ent6 (natural acct0 value and update the acct code concatenated from the hdr table.

      BEGIN

        SELECT SUBSTR(rec_inv_hdr.ACCTS_PAY_CODE_CONCATENATED, 1, 22) ||
               GCC.SEGMENT6 ||
               SUBSTR(rec_inv_hdr.ACCTS_PAY_CODE_CONCATENATED, 29, 9)
          INTO l_dist_code_concatenated
          FROM PO_VENDORS           PV,
               PO_VENDOR_SITES_ALL  PVSA,
               GL_CODE_COMBINATIONS GCC
         WHERE PV.VENDOR_ID = PVSA.VENDOR_ID
           AND GCC.CODE_COMBINATION_ID = PVSA.ACCTS_PAY_CODE_COMBINATION_ID
           AND PV.vendor_id = p_vendor_id
           AND PVSA.VENDOR_SITE_ID = p_vendor_site_id;

      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          v_error_line_flag := 'Y';
          v_line_err_msg    := 'ACCOUNTING SEGMENTS NOT MAPPED WITH VENDOR MASTER';

        --FND_FILE.PUT_LINE (FND_FILE.OUTPUT,RPAD(rec_inv_line.vendor_num, 20)
        --                                      || RPAD(rec_inv_line.invoice_num, 20)
        --                                     || v_line_err_msg);
        WHEN OTHERS THEN
          v_error_line_flag := 'Y';
          v_line_err_msg    := 'INVALID DIST CODE CONCATENATED VALUE';
          --FND_FILE.PUT_LINE (FND_FILE.OUTPUT,v_line_err_msg);

      END;

      /*============================Validation for GLdate ==================================*/

      BEGIN
        SELECT
        set_of_books_id INTO v_set_of_books_id
        FROM org_organization_definitions
        WHERE organization_id = rec_inv_hdr.org_id;

        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'SOB-ID: '||v_set_of_books_id);

      EXCEPTION
        WHEN NO_DATA_FOUND THEN
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'SOB not defined for org id: '||rec_inv_hdr.org_id);
        WHEN TOO_MANY_ROWS THEN
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'Multiple SOB defined for org id: '||rec_inv_hdr.org_id);
        WHEN OTHERS THEN
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'Exception while deriving SOB for org id: '||rec_inv_hdr.org_id);
      END;



      IF rec_inv_hdr.gl_date IS NULL THEN
        v_error_flag  := 'Y';
        v_hdr_err_msg := 'GL DATE IS NULL';
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT, v_hdr_err_msg);
      ELSE
        v_data_count := 0;
        BEGIN

          SELECT COUNT(*)
            INTO v_data_count
            FROM gl_period_statuses gps, fnd_application fna
           WHERE fna.application_short_name = 'SQLAP'
             AND gps.application_id = fna.application_id
             AND gps.closing_status = 'O'
             AND gps.set_of_books_id = v_set_of_books_id
             AND TRUNC(NVL(rec_inv_hdr.gl_date, SYSDATE)) BETWEEN
                 gps.start_date AND gps.end_date;
          --   AND     TO_DATE('08-Oct-08','DD-Mon-YYYY')) between gps.start_date and gps.end_date

          --   And     trunc(nvl(TO_DATE('08-Oct-08','DD-Mon-YYYY',sysdate)) between gps.start_date and gps.end_date;

          IF v_data_count = 0 THEN
            v_error_flag  := 'Y';
            v_hdr_err_msg := 'GL DATE IS NOT IN OPEN PERIOD';
            FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                              RPAD(NVL(rec_inv_hdr.vendor_num, ' '), 20) ||
                              RPAD(rec_inv_hdr.INVOICE_NUM, 20) ||
                              v_hdr_err_msg);
          END IF;
        EXCEPTION
          WHEN OTHERS THEN
            v_error_flag  := 'Y';
            v_hdr_err_msg := 'GL DATE IS NOT IN OPEN PERIOD';
            FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                              RPAD(NVL(rec_inv_hdr.vendor_num, ' '), 20) ||
                              RPAD(rec_inv_hdr.INVOICE_NUM, 20) ||
                              v_hdr_err_msg);
        END;
      END IF;

      --DBMS_OUTPUT.PUT_LINE('outsite if part' || ' ' || v_error_flag || ' ' || 'value for ');

      v_error_line_count := 0;
      v_error_almsg      := NULL;

      FOR rec_inv_line IN cur_inv_line(rec_inv_hdr.invoice_num 
                                       ---rec_inv_hdr.vendor_site_id
                                      ,
                                       rec_inv_hdr.vendor_num) LOOP
        v_line_count             := cur_inv_line%ROWCOUNT;
        v_dist_code_concatenated := NULL;
        v_error_line_flag        := NULL;
        v_line_err_msg           := NULL;
        v_line_type_lookup_code  := NULL;
        v_data_count             := 0;

        -- IF rec_inv_line.line_type_lookup_code IS NOT NULL THEN
        --    SELECT line_type_lookup_code
        --    INTO   v_line_type_lookup_code
        --    FROM   apps.XXABRL_SMDN_AP_LINE_STAGE
        --    WHERE  invoice_num := rec_inv_line.invoice_num
        --    AND    line_number := rec_inv_line.line_number
        --    AND    line_type_lookup_code := rec_inv_line.line_type_lookup_code;

        IF rec_inv_line.line_type_lookup_code <> 'ITEM' THEN
          v_line_type_lookup_code := 'MISCELLANEOUS';
        ELSE
          v_line_type_lookup_code := 'ITEM';
        END IF;
        -- END IF;

        /*============================Validation for Accounting date ================================
==*/

        IF rec_inv_line.accounting_date IS NULL THEN
          v_error_line_flag := 'Y';
          v_line_err_msg    := 'ACCOUNTING DATE IS NULL';
          --FND_FILE.PUT_LINE (FND_FILE.OUTPUT,v_line_err_msg);
        ELSE
          BEGIN
            SELECT COUNT(*)
              INTO v_data_count
              FROM gl_period_statuses gps, fnd_application fna
             WHERE fna.application_short_name = 'SQLAP'
               AND gps.application_id = fna.application_id
               AND gps.closing_status = 'O'
               AND gps.set_of_books_id = v_set_of_books_id
               AND TRUNC(NVL(rec_inv_line.accounting_date, SYSDATE)) BETWEEN
                   gps.start_date AND gps.end_date;

            IF v_data_count = 0 THEN
              v_error_line_flag := 'Y';
              v_line_err_msg    := 'ACCOUNTING DATE IS NOT IN OPEN PERIOD' ||
                                   RPAD(' ', 20) || RPAD(' ', 20) ||
                                   CHR(10) || RPAD(' ', 20) ||
                                   RPAD(' ', 20);
              --FND_FILE.PUT_LINE (FND_FILE.OUTPUT,v_line_err_msg);
            END IF;
          EXCEPTION
            WHEN OTHERS THEN
              v_error_line_flag := 'Y';
              v_line_err_msg    := 'ACCOUNTING DATE IS NOT IN OPEN PERIOD';
              --  FND_FILE.PUT_LINE (FND_FILE.OUTPUT,v_line_err_msg);
          END;
        END IF;

        /*============================Validation for Invoice Line Amount=====================================*/

        IF rec_inv_line.amount IS NULL THEN
          v_error_line_flag := 'Y';
          v_line_err_msg    := 'INVOICE LINE AMOUNT IS NULL' ||
                               RPAD(' ', 20) || RPAD(' ', 20) || CHR(10) ||
                               RPAD(' ', 20) || RPAD(' ', 20);
          FND_FILE.PUT_LINE (FND_FILE.OUTPUT,v_line_err_msg);
        END IF;

        FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'rec_inv_line.Amount: ' ||NVL(rec_inv_line.Amount, 0));

        v_inv_line_amt := NVL(v_inv_line_amt, 0) +
                          NVL(rec_inv_line.Amount, 0);
        --v_inv_line_amt :=  ROUND( v_inv_line_amt,2);

        /*============================Validate the concatenated_segments in invoice Lines=====================================*/
        -- Following validation of concatenated segments removed by shailesh on 25 OCt 2008.
        -- As system will take default accounting segment value from vendor master.

        IF rec_inv_line.dist_code_concatenated IS NULL THEN
          v_error_line_flag := 'Y';
          v_line_err_msg    := 'DIST CODE CONCATENATED VALUE IS NULL' ||
                               RPAD(' ', 20) || RPAD(' ', 20) || CHR(10) ||
                               RPAD(' ', 20) || RPAD(' ', 20);
          --FND_FILE.PUT_LINE (FND_FILE.OUTPUT,v_line_err_msg);
        ELSE

          BEGIN

/*            As its Taking Too Much Time, removed by Bala.SK

            SELECT code_combination_id
              INTO v_dist_code_concatenated
              FROM gl_code_combinations_kfv
             WHERE TRIM(concatenated_segments) =
                   TRIM(rec_inv_line.dist_code_concatenated);


    SELECT CODE_COMBINATION_ID    INTO        V_DIST_CODE_CONCATENATED
    FROM GL_CODE_COMBINATIONS_KFV
    WHERE 1=1
    AND SUBSTR(CONCATENATED_SEGMENTS,1,2)  = 
        SUBSTR(rec_inv_line.dist_code_concatenated,1,2)
    AND SUBSTR(CONCATENATED_SEGMENTS,4,3)  = 
        SUBSTR(rec_inv_line.dist_code_concatenated,4,3)
    AND SUBSTR(CONCATENATED_SEGMENTS,8,3)  = 
        SUBSTR(rec_inv_line.dist_code_concatenated,8,3)
    AND SUBSTR(CONCATENATED_SEGMENTS,12,7) = 
        SUBSTR(rec_inv_line.dist_code_concatenated,12,7)
    AND SUBSTR(CONCATENATED_SEGMENTS,20,2) = 
        SUBSTR(rec_inv_line.dist_code_concatenated,20,2)
    AND SUBSTR(CONCATENATED_SEGMENTS,23,6) = 
        SUBSTR(rec_inv_line.dist_code_concatenated,23,6)
    AND SUBSTR(CONCATENATED_SEGMENTS,30,3) = 
        SUBSTR(rec_inv_line.dist_code_concatenated,30,3)
    AND SUBSTR(CONCATENATED_SEGMENTS,34,4) = 
        SUBSTR(rec_inv_line.dist_code_concatenated,34,4)

*/


    SELECT CODE_COMBINATION_ID    INTO        V_DIST_CODE_CONCATENATED
    FROM GL_CODE_COMBINATIONS_KFV
    WHERE 1=1
    AND SEGMENT1 = SUBSTR(rec_inv_line.dist_code_concatenated,1,2)
    AND SEGMENT2 = SUBSTR(rec_inv_line.dist_code_concatenated,4,3)
    AND SEGMENT3 = SUBSTR(rec_inv_line.dist_code_concatenated,8,3)
    AND SEGMENT4 = SUBSTR(rec_inv_line.dist_code_concatenated,12,7)
    AND SEGMENT5 = SUBSTR(rec_inv_line.dist_code_concatenated,20,2)
    AND SEGMENT6 = SUBSTR(rec_inv_line.dist_code_concatenated,23,6)
    AND SEGMENT7 = SUBSTR(rec_inv_line.dist_code_concatenated,30,3)
    AND SEGMENT8 = SUBSTR(rec_inv_line.dist_code_concatenated,34,4);

          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              v_error_line_flag := 'Y';
              v_line_err_msg    := 'DIST CODE CONCATENATED VALUE' || ' ' ||
                                   rec_inv_line.dist_code_concatenated || ' ' ||
                                   'IS NOT VALID';
              --FND_FILE.PUT_LINE (FND_FILE.OUTPUT,RPAD(rec_inv_line.vendor_num, 20)
            --                                      || RPAD(rec_inv_line.invoice_num, 20)
            --                                     || v_line_err_msg);
            WHEN OTHERS THEN
              v_error_line_flag := 'Y';
              v_line_err_msg    := 'INVALID DIST CODE CONCATENATED VALUE';
              --FND_FILE.PUT_LINE (FND_FILE.OUTPUT,v_line_err_msg);
          END;
        END IF;

       
              FND_FILE.PUT_LINE(FND_FILE.log,'Sss -line  Error flag '||v_error_line_flag);
        
        /*====================================================Updating Lines Staging============================================*/
        IF NVL(v_error_line_flag, 'N') = 'Y' THEN

          --DBMS_OUTPUT.PUT_LINE('Updating Lines Staging' || ' ' || v_error_flag || ' ' || 'value for ');
          
         -- FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'SHAILESH - updating line staging error ');
          
          UPDATE apps.XXABRL_SMDN_AP_LINE_STAGE
             SET ERROR_FLAG = 'E', ERROR_MSG = v_line_err_msg
           WHERE ROWID = rec_inv_line.ROWID;
          COMMIT;

          --                  UPDATE  apps.XXABRL_AP_SMDN_HEADER_STAGE
          --                  SET   ERROR_FLAG ='E'
          --                   ,ERROR_MSG = 'Line validation failed'|| v_line_err_msg
          --                  WHERE ROWID = rec_inv_hdr.ROWID
          --               AND invoice_num=rec_inv_line.invoice_num;

          v_error_line_count := v_error_line_count + 1;
        ELSE
        
   --      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'SHAILESH - updating line staging NOT IN error ');
        
          UPDATE apps.XXABRL_SMDN_AP_LINE_STAGE
             SET ERROR_FLAG            = 'V',
                 line_type_lookup_code = v_line_type_lookup_code,
                 ERROR_MSG             = NULL
           WHERE ROWID = rec_inv_line.ROWID;

          COMMIT;
    
        END IF;

        IF v_line_err_msg IS NOT NULL THEN
          v_error_almsg := v_error_almsg || 'LINE NO :' ||
                           TO_CHAR(v_line_count) || ' ' || v_line_err_msg;
        END IF;

      END LOOP;


      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'LINE AMOUNT   ->  '||v_inv_line_amt);
      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'HDR AMOUNT   ->  '||rec_inv_hdr.invoice_amount);


      IF (ROUND(v_inv_line_amt, 2) - ROUND(rec_inv_hdr.invoice_amount, 2)) BETWEEN -1 AND 1 THEN  --H@N$/Sankara/01-11-08
--      IF ROUND(v_inv_line_amt, 2) <> ROUND(rec_inv_hdr.invoice_amount, 2) THEN
        NULL;
      ELSE
        v_error_flag  := 'Y';
        v_hdr_err_msg := 'INVOICE HEADER AND LINE AMOUNT IS NOT MATCHING';
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                          RPAD(NVL(rec_inv_hdr.vendor_num, ' '), 20) ||
                          RPAD(rec_inv_hdr.invoice_num, 20) ||
                          v_hdr_err_msg);
      END IF;

       FND_FILE.PUT_LINE(FND_FILE.log,'Sss - hdr Error flag '||v_error_flag);
      /*====================================================Updating  Header Staging============================================*/
      IF NVL(v_error_flag, 'N') = 'Y' THEN

      
      
       --   FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'SHAILESH - updating HDR staging error ');
        
        UPDATE apps.XXABRL_AP_SMDN_HEADER_STAGE
           SET ERROR_FLAG = 'E', ERROR_MSG = v_hdr_err_msg
         WHERE ROWID = rec_inv_hdr.ROWID;
        COMMIT;
        
                  --- added by Gonvind on 25 june 2009 
           p_vendor_site_id             := null;
        --              UPDATE  apps.XXABRL_SMDN_AP_LINE_STAGE
        --              SET     ERROR_FLAG ='E',ERROR_MSG = 'Header validation failed'|| v_hdr_err_msg
        --              WHERE   1=1--ROWID = rec_inv_line.ROWID
        --             AND     invoice_num = rec_inv_hdr.invoice_num;

        v_error_head_count := v_error_head_count + 1;

        IF v_error_almsg IS NOT NULL THEN
          FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                            RPAD(NVL(rec_inv_hdr.vendor_num, ' '), 20) ||
                            RPAD(rec_inv_hdr.invoice_num, 20) ||
                            'LINE ERROR :' || v_error_almsg);
        END IF;

      ELSIF v_hdr_err_msg IS NULL AND v_error_line_count <> 0 THEN

     --     FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'SHAILESH - updating HDR staging error ');
     
          
      
        UPDATE apps.XXABRL_AP_SMDN_HEADER_STAGE
           SET ERROR_FLAG = 'E',
               ERROR_MSG  = v_error_line_count || 'ERROR IN LINES'
         WHERE ROWID = rec_inv_hdr.ROWID;
                   --- added by Gonvind on 25 june 2009 
           p_vendor_site_id             := null;
        IF v_error_almsg IS NOT NULL THEN
          FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                            RPAD(NVL(rec_inv_hdr.vendor_num, ' '), 20) ||
                            RPAD(rec_inv_hdr.invoice_num, 20) ||
                            'LINE ERROR :' || v_error_almsg);
        END IF;
      ELSE
        --DBMS_OUTPUT.PUT_LINE('inseide else part' || ' ' || v_error_flag || ' ' || 'value for ');
        -- As insert program is running only for the one vendor site for all vendors in stg table so we include the cloumn vendor site id in hdr stg table
        -- and it will store the site id for respective vendor
        -- it will also store the terms id if it is available in vendor master
        
        
          
       --   FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'SHAILESH - updating HDR staging NOT IN error payment method lookup code ' ||  p_payment_method_lookup_code);
          
        UPDATE apps.XXABRL_AP_SMDN_HEADER_STAGE
           SET ERROR_FLAG = 'V',
               ---vendor_site_id=v_vendor_site_id,
               vendor_num                  = rec_inv_hdr.vendor_num,
               VENDOR_SITE_ID              = p_VENDOR_SITE_ID,
               TERMS_ID                    = l_terms_id,
               --ORG_ID                      = P_ORG_ID, --H@N$
               VENDOR_ID                   = p_vendor_id,
               PAYMENT_METHOD_LOOKUP_CODE     =  p_payment_method_lookup_code,
               ACCTS_PAY_CODE_CONCATENATED = l_dist_code_concatenated,
               ERROR_MSG                   = NULL
         WHERE ROWID = rec_inv_hdr.ROWID;
        ----IF YOU WANT TO INSERT THE DATA FOR MULTI ORG THEN CREATE COLUMN ORG_ID IN  apps.XXABRL_AP_SMDN_HEADER_STAGE TABLE
        --AND  PUT THIS STMT IN ABOVE UPDATE CONDITION  ORG_ID=P_ORG_ID
        -- ALSO INCLUDE THIS COLOUMN IN INSERT STMT WHILE INSERTING TO INTERFACE TABLE FOR HDR
                  
                  
        COMMIT;
        --- added by Gonvind on 25 june 2009 
           p_vendor_site_id             := null;

        --Retek Matched Invoices

        v_success_record_count := v_success_record_count + 1;

        --DBMS_OUTPUT.PUT_LINE('before end loop' || ' ' || v_success_record_count || ' ' || 'value for ');

      END IF;
   
    END LOOP;
    
       FND_FILE.PUT_LINE(FND_FILE.LOG,'ORG ID --> INSERT TRANSACTION PASS ORG_ID -> '||P_org_id);    
 
    DEBIT_NOTE_INSERT_TRANSACTION(p_org_id,
                       p_vendor_site_id,
                       p_vendor_id,
                       p_payment_method_lookup_code);

    --IF NVL(v_line_err_msg,'N') <> NULL THEN
    --   v_error_head_count := v_error_line_count +1 ;
    --END IF;

    --v_error_head_count := v_error_line_count +1;

    
    IF v_error_head_count > 0 THEN
      retcode := 1;
    END IF;

    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                      '-------------------------------------------------------------------------------------------------');
    FND_FILE.PUT_LINE(FND_FILE.OUTPUT, CHR(10));
    FND_FILE.PUT_LINE(FND_FILE.OUTPUT, CHR(10));
    FND_FILE.PUT_LINE(FND_FILE.OUTPUT, CHR(10));
    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                      'TOTAL NUMBER OF INVOICES FETCHED :' ||
                      v_total_records);
    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                      'TOTAL NUMBER OF RECORDS WITH ERROR :' ||
                      v_error_head_count);
    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                      'TOTAL NUMBER OF INVOICES PROCESSED :' ||
                      v_success_record_count);
    --DBMS_OUTPUT.PUT_LINE('TOTAL NUMBER OF RECORDS WITH ERROR :'|| v_error_head_count);
    --DBMS_OUTPUT.PUT_LINE('TOTAL NUMBER OF INVOICES PROCESSED      :'|| v_success_record_count);

    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                      '........................................................................');
  END DEBIT_NOTE_VALIDATE_INVOICES;
  /*=================================End of VALIDATE_INVOICES for all tables===========================================*/

  /*=================================Procedure INSERT_TRANSACTION ======================================================*/

  PROCEDURE DEBIT_NOTE_INSERT_TRANSACTION(p_org_id                     IN NUMBER,
                               p_vendor_site_id             IN NUMBER,
                               p_vendor_id                  IN NUMBER,
                               p_payment_method_lookup_code IN VARCHAR2) IS
    v_ERROR_FLAG   VARCHAR2(1);
    v_errmsg       VARCHAR2(6000);
    v_header_id    NUMBER;
    v_line_id      NUMBER;
    v_errorinvoice BOOLEAN;
    v_errorlines   BOOLEAN;
    --v_org_id                                NUMBER :=NULL;-- := 82;
    --v_user_id                                  NUMBER     :=FND_PROFILE.value('USER_ID');
    --v_resp_id                                  NUMBER     :=FND_PROFILE.value('RESP_ID');
    --v_appl_id                                  NUMBER     :=FND_PROFILE.value('RESP_APPL_ID');
    --v_req_id                                    NUMBER;
    v_record_count               NUMBER := 0;
    v_org_id                     NUMBER;
    v_vendor_site_id             NUMBER(15);
    v_vendor_id                  NUMBER(15);
    v_payment_method_lookup_code VARCHAR2(30);

    -- added by shailesh for storing the location of the retake payable invoice
    l_location VARCHAR2(50);

    /*
       CURSOR   cur_inv_hdr IS
          SELECT    rowid,aps.*
       FROM        apps.XXABRL_AP_SMDN_HEADER_STAGE aps
       WHERE       NVL(ERROR_FLAG,'N') = 'V';
    */

    -- Added by shailesh on 25 OCT 2008
    -- Above commented cursor replaced by new cursor
    CURSOR cur_inv_hdr IS

      SELECT aps.ROWID,
             aps.INVOICE_NUM,
             aps.INVOICE_TYPE_LOOKUP_CODE,
             aps.INVOICE_DATE,
             aps.VENDOR_NUM,
             aps.TERMS_ID,
             ROUND(aps.INVOICE_AMOUNT, 2) INVOICE_AMOUNT,
             aps.INVOICE_CURRENCY_CODE,
             aps.EXCHANGE_DATE,
             aps.EXCHANGE_RATE_TYPE,
             aps.DESCRIPTION,
             aps.GL_DATE,
             aps.GROUP_ID,
             aps.VENDOR_SITE_CODE,
             aps.TERMS_DATE,
             aps.ACCTS_PAY_CODE_CONCATENATED,
             aps.ERROR_FLAG,
             aps.ERROR_MSG,
             APS.VENDOR_SITE_ID,
             APS.ORG_ID,
             APS.VENDOR_ID,
             aps.payment_method_lookup_code,
             aps.GOODS_RECEIVED_DATE -- New coloumn added in the  xxabrl.XXABRL_AP_SMDN_HEADER_STAGE
        FROM apps.XXABRL_AP_SMDN_HEADER_STAGE aps
       WHERE 1=1
       and   aps.GL_DATE        >=   trunc(sysdate)-180  ---added on 06-Apr-2016
       and NVL(ERROR_FLAG, 'N') = 'V';

    /*
    CURSOR   cur_inv_line(p_invoice_num VARCHAR2,
                          p_vendor_num NUMBER) IS
     SELECT   rowid,als.*
     FROM     apps.XXABRL_SMDN_AP_LINE_STAGE  als
     WHERE    NVL(ERROR_FLAG,'N') = 'V'
     AND      als.invoice_num = p_invoice_num
    ---AND      als.vendor_site_id = p_vendor_site_id
    AND      als.vendor_num = p_vendor_num;
    */

    -- Added by shailesh on 25 OCT 2008
    -- Above commented cursor replaced by new cursor
    CURSOR cur_inv_line(p_invoice_num VARCHAR2, p_vendor_num NUMBER) IS

      SELECT als.ROWID,
             als.VENDOR_NUM,
             als.VENDOR_SITE_ID,
             als.INVOICE_NUM,
             als.LINE_NUMBER,
             als.LINE_TYPE_LOOKUP_CODE,
             ROUND(als.AMOUNT, 2) AMOUNT,
             als.ACCOUNTING_DATE,
             als.DIST_CODE_CONCATENATED,
             als.DESCRIPTION,
             als.SAC_CODE,
             als.TDS_TAX_NAME,
             als.WCT_TAX_NAME,
             als.SERVICE_TAX,
             als.VAT,
             als.ERROR_FLAG,
             als.ERROR_MSG
        FROM apps.XXABRL_SMDN_AP_LINE_STAGE als
       WHERE NVL(ERROR_FLAG, 'N') = 'V'
       and  als.ACCOUNTING_DATE  >= trunc(sysdate)-180  ---added on 06-Apr-2016
         AND als.invoice_num = p_invoice_num
            ---AND      als.vendor_site_id = p_vendor_site_id
         AND als.vendor_num = p_vendor_num;

  BEGIN

--   commented by shailesh on 20th june 2009
    --v_org_id                     := p_org_id;
    --v_vendor_site_id             := p_vendor_site_id;
    --v_vendor_id                  := p_vendor_id;
    --v_payment_method_lookup_code := p_payment_method_lookup_code;

    FOR rec_inv_hdr IN cur_inv_hdr LOOP
      EXIT WHEN cur_inv_hdr%NOTFOUND;
      v_header_id    := NULL;
      v_errorinvoice := NULL;
      v_errmsg       := NULL;
      v_record_count := cur_inv_hdr%ROWCOUNT;
      --v_payment_method_lookup_code          := NULL;
      --v_org_id                              := NULL;
      --v_vendor_id                           := NULL;

      --           SELECT asr.vendor_id,assa.org_id,assa.payment_method_lookup_code
      --           INTO   v_vendor_id,v_org_id,v_payment_method_lookup_code
      --           FROM   ap_suppliers asr,ap_supplier_sites_all assa
      --           WHERE  asr.vendor_id=assa.vendor_id
      --           AND    asr.segment1=rec_inv_hdr.vendor_num --'9800012'--'987676'--'9800012'
      --           AND       assa.VENDOR_SITE_ID=rec_inv_hdr.vendor_site_id
      --           AND    org_id = fnd_profile.value('org_id');

      SELECT ap_invoices_interface_s.NEXTVAL INTO v_header_id FROM dual;

      BEGIN

      FND_FILE.PUT_LINE(FND_FILE.LOG,'INSERTING RECORD FOR  -> '||rec_inv_hdr.INVOICE_NUM||'  '||rec_inv_hdr.ORG_ID);    
    
      
      
        INSERT INTO AP_INVOICES_INTERFACE
          (INVOICE_ID,
           INVOICE_NUM,
           INVOICE_TYPE_LOOKUP_CODE,
           INVOICE_DATE,
           VENDOR_ID,
           VENDOR_SITE_ID,
           INVOICE_AMOUNT,
           INVOICE_CURRENCY_CODE
           --,EXCHANGE_RATE
          ,
           EXCHANGE_RATE_TYPE,
           EXCHANGE_DATE,
           TERMS_ID,
           DESCRIPTION
           --,TERMS_NAME
          ,
           LAST_UPDATE_DATE,
           LAST_UPDATED_BY,
           LAST_UPDATE_LOGIN,
           CREATION_DATE,
           CREATED_BY
           --,GROUP_ID
          ,
           GL_DATE,
           ORG_ID,
           VENDOR_EMAIL_ADDRESS,
           SOURCE,
           TERMS_DATE,
           PAYMENT_METHOD_CODE,
           GOODS_RECEIVED_DATE -- added by shailesh on 25 oct 2008
          ,
           ACCTS_PAY_CODE_CONCATENATED)
        VALUES
          (V_HEADER_ID,
           rec_inv_hdr.INVOICE_NUM,
           rec_inv_hdr.INVOICE_TYPE_LOOKUP_CODE,
           rec_inv_hdr.INVOICE_DATE,
           rec_inv_hdr.vendor_id,
           Rec_inv_hdr.VENDOR_SITE_ID -- CHANGE BY SHAILESH ON 25 oct2008 ORIGINAL IS  v_vendor_site_id
          ,
           rec_inv_hdr.INVOICE_AMOUNT,
           rec_inv_hdr.INVOICE_CURRENCY_CODE
           --,rec_inv_hdr.EXCHANGE_RATE
          ,
           rec_inv_hdr.EXCHANGE_RATE_TYPE,
           rec_inv_hdr.EXCHANGE_DATE,
           rec_inv_hdr.TERMS_ID,
           rec_inv_hdr.DESCRIPTION
           --,rec_inv_hdr.TERMS_NAME
          ,
           SYSDATE,
           3,
           3,
           SYSDATE,
           3
           --,rec_inv_hdr.GROUP_ID
          ,
           rec_inv_hdr.GL_DATE,
           rec_inv_hdr.ORG_ID,
           rec_inv_hdr.VENDOR_SITE_CODE,
           'SMDN',
           rec_inv_hdr.TERMS_DATE,
          -- v_payment_method_lookup_code,
           rec_inv_hdr.PAYMENT_METHOD_LOOKUP_CODE, -- above stmt is changed by shailesh on 20th june 2009
           rec_inv_hdr.GOODS_RECEIVED_DATE -- added by shailesh on 25 oct 2008
          ,
           rec_inv_hdr.ACCTS_PAY_CODE_CONCATENATED);
        COMMIT;
        v_errorinvoice := FALSE;
      EXCEPTION
        WHEN OTHERS THEN
          v_errorinvoice := TRUE;
          v_errmsg       := 'AFTER INSERT INTO AP INVOICES INTERFACE FAILED IN EXCEPTION' ||
                            SUBSTR(SQLERRM, 1, 150);
          FND_FILE.PUT_LINE(FND_FILE.OUTPUT, v_errmsg);
      END;
      IF v_errorinvoice = TRUE THEN
        UPDATE apps.XXABRL_AP_SMDN_HEADER_STAGE
           SET ERROR_FLAG = 'E', ERROR_MSG = v_errmsg
         WHERE ROWID = rec_inv_hdr.ROWID;
        --and CURRENT OF cur_inv_hdr;

        UPDATE apps.XXABRL_SMDN_AP_LINE_STAGE
           SET ERROR_FLAG = 'E',
               ERROR_MSG  = 'Header Inserting failed' || v_errmsg
         WHERE invoice_num = rec_inv_hdr.invoice_num;
        --and CURRENT OF cur_inv_hdr;
      ELSE
        UPDATE apps.XXABRL_AP_SMDN_HEADER_STAGE
           SET ERROR_FLAG = 'P',
               ERROR_MSG  = 'SUCCESSFULLY INSERTED IN HEADER INTERFACE TABLE'
         WHERE ROWID = rec_inv_hdr.ROWID;
        --and CURRENT OF cur_inv_hdr;
      END IF;

      FOR rec_inv_line IN cur_inv_line(rec_inv_hdr.invoice_num
                                       --,rec_inv_hdr.vendor_site_id
                                      ,
                                       rec_inv_hdr.vendor_num) LOOP
        EXIT WHEN cur_inv_line%NOTFOUND;
        v_errorinvoice := FALSE;
        v_errmsg       := NULL;
        v_line_id      := NULL;

        SELECT ap_invoice_lines_interface_s.NEXTVAL
          INTO v_line_id
          FROM dual;
        BEGIN

          -- following code addted by shailesh on 26 OCT 2008
          -- It will extract the segment4 from dist_code_combinition and  get the location  for the line level dff

          BEGIN

            SELECT ffvv.flex_value --,ffvv.description
              INTO l_location
              FROM FND_FLEX_VALUES_VL FFVV, FND_FLEX_VALUE_SETS FFVS
             WHERE FLEX_VALUE_SET_NAME = 'ABRL_GL_Location'
               AND ffvs.FLEX_VALUE_SET_ID = ffvv.FLEX_VALUE_SET_ID
               AND ffvv.FLEX_VALUE =
                   SUBSTR(rec_inv_line.dist_code_concatenated, 12, 7);

          EXCEPTION
            WHEN OTHERS THEN
              l_location := NULL;
          END;

          /*
          SELECT * FROM AP_INVOICES_INTERFACE
          WHERE INVOICE_NUM='INV_9983'

          SELECT * FROM AP_INVOICE_LINES_INTERFACE
          WHERE  INVOICE_ID=700042
          */

          /*
          select * from AP_INVOICES_INTERFACE
          where invoice_num in ('INV_9983','INV_10373')


          select * from AP_INVOICE_LINES_INTERFACE
          where invoice_id= 700041 --700042
          */
          INSERT INTO AP_INVOICE_LINES_INTERFACE
            (INVOICE_ID,
             INVOICE_LINE_ID,
             LINE_NUMBER,
             LINE_TYPE_LOOKUP_CODE,
             AMOUNT
             --,TAX_CODE
            ,
             DIST_CODE_CONCATENATED,
             DESCRIPTION,
             ATTRIBUTE11, --ADDED ON 09-AUG-2017
             ACCOUNTING_DATE,
             ORG_ID,
             ATTRIBUTE_CATEGORY --ADDED BY LOKESH
            ,
             ATTRIBUTE1 --ADDED BY SHAILESH
            ,
             LAST_UPDATED_BY,
             LAST_UPDATE_DATE,
             LAST_UPDATE_LOGIN,
             CREATED_BY,
             CREATION_DATE)
          VALUES
            (V_HEADER_ID,
             V_LINE_ID --,rec_inv_line.INVOICE_LINE_ID
            ,
             rec_inv_line.LINE_NUMBER,
             rec_inv_line.LINE_TYPE_LOOKUP_CODE,
             rec_inv_line.AMOUNT
             --,rec_inv_line.TAX_CODE
            ,
             rec_inv_line.DIST_CODE_CONCATENATED,
             rec_inv_line.DESCRIPTION,
             rec_inv_line.SAC_CODE,  --ADDED BY LOKESH
             rec_inv_line.ACCOUNTING_DATE,
             --v_org_id, 
             -- ADDED BY SHAILESH ON 11 MAY 2009  ORG ID IS COMMING FROM HEADER
             rec_inv_hdr.ORG_ID,
             'SMDN DEBIT NOTE  SAC CODE' --ADDED BY LOKESH
            ,
             l_location -- ADDED BY SHAILESH
            ,
             3,
             SYSDATE,
             3,
             3,
             SYSDATE);
          COMMIT;
          v_errorlines := FALSE;
        EXCEPTION
          WHEN OTHERS THEN
            v_errorlines := TRUE;
            v_errmsg     := 'AFTER INSERT INTO AP INVOICE LINES INTERFACE FAILED IN EXCEPTION' ||
                            SUBSTR(SQLERRM, 1, 150);
            FND_FILE.PUT_LINE(FND_FILE.OUTPUT, v_errmsg);
        END;
        IF v_errorlines = TRUE THEN
          UPDATE apps.XXABRL_SMDN_AP_LINE_STAGE
             SET ERROR_FLAG = 'E', ERROR_MSG = v_errmsg
           WHERE ROWID = rec_inv_line.ROWID;
          --and CURRENT OF cur_inv_line;
        ELSE
          UPDATE apps.XXABRL_SMDN_AP_LINE_STAGE
             SET ERROR_FLAG = 'P',
                 ERROR_MSG  = 'SUCCESSFULLY INSERTED IN LINE INTERFACE TABLE'
           WHERE ROWID = rec_inv_line.ROWID;
          -- and CURRENT OF cur_inv_line;
        END IF;
      END LOOP;

    -----
    -- RUN PAYABLE OPEN ITERFACE FOR INTERFACE DATA INTO ORACLE PAYABLES
    ----
    /*      If v_record_count>0 Then
                      FND_GLOBAL.APPS_INITIALIZE (
                            user_id=> v_user_id,
                            resp_id=> v_resp_id,
                            resp_appl_id=> v_appl_id
                         );
                   COMMIT;
                   v_req_id :=fnd_request.submit_request (
                                  'SQLAP',
                                'APXIIMPT',
                                 'Payables Open Interface Import'||P_Data_Source,
                            NULL,
                                  FALSE,
                         p_org_id,
                                P_Data_Source, -- Parameter Data source
                                 NULL, -- Group id
                              V_Batch_Name,
                                 NULL,
                              NULL,
                              NULL,
                              'N',
                        'N',
                           'N',
                        'Y',
                                 CHR (0)
                            );
                   Commit;
                   fnd_file.put_line(fnd_file.output,'Please see the output of Payables OPEN Invoice Import program request id :'||v_req_id);
                   fnd_file.put_line(fnd_file.output,'........................................................................' );
                   Loop
                      Begin
                         V_Status:=Null;
                         Select Status_Code
                         Into     V_Status
                         from      Fnd_Concurrent_Requests
                         where  Request_id=v_req_id;
                         If V_Status in ('C','D','E','G','X') then
                               Exit;
                         End If;
                      EXCEPTION
                         When Others Then
                               Exit;
                      End;
                   End Loop;
             End If;*/
    END LOOP;
    -- Added on 21-jun-2008
    
    ---- commented by shailesh on 22 june 2009 for temp testing.
 --   DELETE FROM XXABRL_AP_SMDN_HEADER_STAGE WHERE ERROR_FLAG = 'P';
 --   DELETE FROM XXABRL_SMDN_AP_LINE_STAGE WHERE ERROR_FLAG = 'P';
    --
    COMMIT;
  END DEBIT_NOTE_INSERT_TRANSACTION;
  /*=================================END OF PROCEDURE INSERT_TRANSACTION===================================================*/
END XXABRL_SMDN_INV_INTERFACE; 
/


CREATE OR REPLACE PACKAGE APPS.XXABRL_AP_SEL_INV_PKG
 IS
 PROCEDURE XXABRL_EXP_SEL_INV_PROG(errbuf out varchar2,
                                     retcode out varchar2,
                                     p_checkrun_name varchar2); 
 
 END XXABRL_AP_SEL_INV_PKG; 
/


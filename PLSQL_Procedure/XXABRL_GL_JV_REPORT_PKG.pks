CREATE OR REPLACE PACKAGE APPS.xxabrl_gl_jv_report_pkg
AS
PROCEDURE xxabrl_gl_jv_report_proc(ERRBUFF OUT VARCHAR2,
                                RETCODE OUT NUMBER,
                               P_SOURCE   VARCHAR2,
                               P_FROM_SBU NUMBER,
                               P_TO_SBU  NUMBER,
                               -- P_FROM_LOCATION NUMBER,
                               -- P_TO_LOCATION NUMBER,
                              P_FROM_ACCOUNT NUMBER,
                              P_TO_ACCOUNT NUMBER,
                              P_FROM_GL_DATE varchar2,
                              P_TO_GL_DATE varchar2,
                              P_FROM_DOC_NO NUMBER,
                              P_TO_DOC_NO   NUMBER
                              );
                              end xxabrl_gl_jv_report_pkg; 
/


CREATE OR REPLACE PROCEDURE APPS.XXABRL_MIS_DATA_MIGRATION_MER(errbuf out varchar2,
                                 retcode out varchar2,
                                 P_from_DATE IN  VARCHAR2
                                 )
AS
P_RUN_DATE     VARCHAR2 (11):= TO_CHAR (TO_DATE (SUBSTR (p_from_date, 1, 10), 'RRRR/MM/DD'),'DD-MON-YYYY');
--P_TO_DATE     VARCHAR2 (11):= TO_CHAR (TO_DATE (SUBSTR (p_from_date, 1, 10), 'RRRR/MM/DD'),'DD-MON-YYYY');
V_L_FILE         VARCHAR2(200);
V_FINANCE_YEAR   NUMBER;
L_RUN_DATE    VARCHAR2(250);

CURSOR CUR_PERIOD_NAME1 IS
SELECT  '1', 
        povs.vendor_site_code, 
        povs.VENDOR_SITE_ID,--added on '27-Oct-2015'
        api.invoice_type_lookup_code INVOICE_TYPE,
        replace (api.invoice_num,'^','//') INVOICE_NUM, 
        api.invoice_date, 
       (SELECT  
       TRANSLATE ( api.description, 'X' || CHR (9) || CHR (10) || CHR (13), 'X' )
        from dual) description,              
        apd.dist_code_combination_id ccid,
        gl.SEGMENT3 SBU ,
        (select FFVL.description FROM apps.fnd_flex_values_vl ffvl
            WHERE ffvl.FLEX_VALUE_SET_ID=1013466
            AND ffvl.FLEX_VALUE=gl.SEGMENT3 )SBU_desc,
        gl.segment4 LOCATION_CODE,
        (SELECT FFVL.description FROM apps.fnd_flex_values_vl ffvl
            WHERE ffvl.FLEX_VALUE_SET_ID=1013467
            AND ffvl.FLEX_VALUE=gl.SEGMENT4) Location_desc,
        gl.segment6 ACCOUNT_CODE,
        (SELECT FFVL.description FROM apps.fnd_flex_values_vl ffvl
            WHERE ffvl.FLEX_VALUE_SET_ID=1013469
            AND ffvl.FLEX_VALUE=gl.SEGMENT6) account_desc,
        api.invoice_currency_code,
        DECODE (api.invoice_type_lookup_code,
                    'CREDIT', ABS (z.amt_val)
                     - ABS (NVL (api.discount_amount_taken, 0)),0 ) dr_val,
        DECODE (api.invoice_type_lookup_code,
                    'CREDIT', 0,
                    z.amt_val - NVL (api.discount_amount_taken, 0) ) cr_val,
        null prepay_amt_remaing ,
        TO_CHAR (api.doc_sequence_value)payment_num,
            /*null pay_accounting_date,*/
        NULL check_number, 
        null check_date , 
        pov.segment1 VENDOR_NUM,
        (case when (substr(pov.segment1,1,1)='7') then (select apsa1.attribute14 from apps.ap_suppliers aps1,
        apps.ap_supplier_sites_all apsa1
        where
        aps1.vendor_id=apsa1.vendor_id
        and aps1.segment1=pov.SEGMENT1
        and apsa1.vendor_site_id=povs.vendor_site_id) else NULL end)"ATTRIBUTE_14",
        pov.vendor_name,
        pov.vendor_type_lookup_code VENDOR_TYPE, 
        apd.po_distribution_id,
        apil.PO_HEADER_ID,--added  on '30-sep-2015'
        apil.RCV_TRANSACTION_ID, --added  on '30-sep-2015'
        api.ORG_ID,--added on '27-Oct-2015'
        (select hr.name from apps.hr_operating_units hr where hr.organization_id=api.org_id)  OPERATING_UNIT , 
        (select ABA.batch_NAME from apps.ap_batches_all ABA where aba.batch_id=api.batch_id) BATCH_NAME,   
        api.invoice_id,
        null PAY_accounting_date,
        NULL payment_type_flag, 
        NULL bank_account_name,
        NULL bank_account_num,
        NULL Bank_UTI_no, 
        api.invoice_amount, 
        api.amount_paid,
        z.accounting_date GL_DATE,
        NULL PREPAY_INVOICE_ID,
     --   (select NAME from ap_terms AT,apps.ap_invoices_all ai
     --       where AT.term_id=AI.TERMS_ID
     --       and ai.invoice_id=api.invoice_id) TERM_NAME,
       (select NAME from ap_terms AT where AT.term_id=api.terms_id) TERM_NAME,          
        Api.PAY_GROUP_LOOKUP_CODE PAY_GROUP,
        APIL.REFERENCE_1  PPI_Invoice_Number,
        api.ATTRIBUTE2 GRN_Number,
        Api.ATTRIBUTE3 GRN_Date,
        null Hold_flag
FROM    apps.ap_invoices_all api,
     --   APPS.AP_BATCHES_ALL ABA ,
        apps.ap_invoice_lines_all apil,
        apps.ap_invoice_distributions_all apd,
        --apps.ap_invoice_distributions_all apd1,
        apps.ap_suppliers pov,
        apps.gl_code_combinations gl,
        -- apps.ap_payment_schedules_all aps,
     --   apps.hr_operating_units hr,
        apps.ap_supplier_sites_all povs,
        (
        SELECT   NVL (SUM (apd.amount), 0) amt_val, api.invoice_id,apd.ACCOUNTING_DATE
                 FROM apps.ap_invoices_all api,
                      apps.ap_invoice_lines_all apil,
                      apps.ap_invoice_distributions_all apd
--                      apps.ap_suppliers pov,
--                      apps.ap_supplier_sites_all povs
                WHERE api.invoice_id = apd.invoice_id
                  AND apil.invoice_id = api.invoice_id
                  AND apil.line_number = apd.invoice_line_number
--                  AND api.vendor_id = pov.vendor_id
--                  AND api.vendor_site_id = povs.vendor_site_id
               --   AND apd.match_status_flag = 'A'
                  and apd.ACCOUNTING_DATE>= TRUNC(SYSDATE) - 180
                  and api.invoice_type_lookup_code <> 'PREPAYMENT'
                  AND apil.line_type_lookup_code <> 'PREPAY'
             GROUP BY api.invoice_id,apd.ACCOUNTING_DATE
             ) z
  WHERE     api.invoice_id = z.invoice_id
            AND api.invoice_id = apd.invoice_id
         --   AND ABA.BATCH_ID(+)=API.BATCH_ID
         --   and api.org_id=hr.ORGANIZATION_ID
            -- and apd.invoice_id=apd1.invoice_id
            AND gl.CODE_COMBINATION_ID=api.ACCTS_PAY_CODE_COMBINATION_ID
            AND apil.invoice_id = api.invoice_id
            --and aps.INVOICE_ID=api.INVOICE_ID
            AND apil.line_number = apd.invoice_line_number
            AND api.vendor_site_id = povs.vendor_site_id
            AND apd.ACCOUNTING_DATE>=trunc(sysdate)-180
            -- and api.invoice_id =58378410
            AND apd.ROWID =
               (SELECT ROWID
                  FROM APPS.ap_invoice_distributions_all
                 WHERE ROWNUM = 1
                   AND invoice_id = apd.invoice_id)                
            AND api.vendor_id = pov.vendor_id
            AND api.invoice_type_lookup_code <> 'PREPAYMENT'
              AND pov.VENDOR_TYPE_LOOKUP_CODE <> 'NON MERCHANDISE'
            AND ap_invoices_pkg.get_approval_status
                     (api.invoice_id,
                      api.invoice_amount,
                      api.payment_status_flag,
                      api.invoice_type_lookup_code
                     ) in ('APPROVED','CANCELLED')
            -- AND apd.match_status_flag = 'A'
            -- and nvl(aps.AMOUNT_REMAINING,0)<>0
            --  and apd.ACCOUNTING_DATE<'1-apr-2012'
            --  and api.invoice_num='DN_DELKN_AUG'11_475'
            -- and pov.segment1='9700381'
            --and api.org_id=801--in ('104974824',101629786,101629804,104659800,104659802,66703510,101371820)
            --  and api.org_ID =901--,801,961,861)
            --AND apd.line_type_lookup_code  IN ( 'ITEM','MISCELLANEOUS')
            --and api.invoice_id ='96751701'
            AND (trunc(api.LAST_UPDATE_DATE) >=P_RUN_DATE OR  
                 trunc(apiL.LAST_UPDATE_DATE) >= P_RUN_DATE  OR  
                 trunc(apD.LAST_UPDATE_DATE) >= P_RUN_DATE )  
            AND (   (api.invoice_type_lookup_code <> 'DEBIT')
                OR (api.invoice_type_lookup_code = 'DEBIT') )
UNION ALL
   SELECT   '2', 
            povs.vendor_site_code, 
            povs.VENDOR_SITE_ID,
            DECODE(API.INVOICE_TYPE_LOOKUP_CODE,'PREPAYMENT',API.INVOICE_TYPE_LOOKUP_CODE,'PAYMENT') INVOICE_TYPE,
            replace(api.invoice_num,'^','//') INVOICE_NUM, 
            api.invoice_date,
            (SELECT  
             TRANSLATE ( api.description, 'X' || CHR (9) || CHR (10) || CHR (13), 'X' )
                 from dual) description,              
            --apd.description description,
            apd.dist_code_combination_id ccid,
            gl.SEGMENT3 ,
            (select FFVL.description FROM apps.fnd_flex_values_vl ffvl
                WHERE ffvl.FLEX_VALUE_SET_ID=1013466
                AND ffvl.FLEX_VALUE=gl.SEGMENT3)SBU_desc,
            gl.segment4
         ,  (SELECT FFVL.description FROM apps.fnd_flex_values_vl ffvl
                WHERE ffvl.FLEX_VALUE_SET_ID=1013467
                AND ffvl.FLEX_VALUE=gl.SEGMENT4) Location_desc,
            gl.segment6,
            (SELECT FFVL.description FROM apps.fnd_flex_values_vl ffvl
                WHERE ffvl.FLEX_VALUE_SET_ID=1013469
                AND ffvl.FLEX_VALUE=gl.SEGMENT6) account_desc,
            api.payment_currency_code,
            DECODE (api.invoice_type_lookup_code,
                    'CREDIT', DECODE (status_lookup_code, 'VOIDED', 0, 0),
                    app.amount) dr_val,
            DECODE (api.invoice_type_lookup_code,
                    'CREDIT', DECODE (status_lookup_code,
                                      'VOIDED', app.amount,
                                      ABS (app.amount)
                                     ),0 ) cr_val, 
            null pre_amt_remaing,
            DECODE (api.payment_status_flag,
                    'Y', TO_CHAR (apc.doc_sequence_value),
                    'P', TO_CHAR (apc.doc_sequence_value),
                    TO_CHAR (apc.doc_sequence_value),'N',TO_CHAR (apc.doc_sequence_value)
                   ) payment_num,
           /* DECODE (api.payment_status_flag,
                    'Y', TO_CHAR (app.accounting_date, 'dd-MON-yyyy'),
                    'P', TO_CHAR (app.accounting_date, 'dd-MON-yyyy')
                   ) pay_accounting_date,*/
            DECODE (api.payment_status_flag,
                    'Y', TO_CHAR (apc.check_number),
                    'P', TO_CHAR (apc.check_number)
                   ) check_number,
            apc.check_date ,
            pov.segment1 VENDOR_NUM, 
        (case when (substr(pov.segment1,1,1)='7') then (select apsa1.attribute14 from apps.ap_suppliers aps1,
        apps.ap_supplier_sites_all apsa1
        where
        aps1.vendor_id=apsa1.vendor_id
        and aps1.segment1=pov.SEGMENT1
        and apsa1.vendor_site_id=povs.vendor_site_id) else NULL end)"ATTRIBUTE_14",
            pov.vendor_name, 
            pov.vendor_type_lookup_code VENDOR_TYPE,
            apd.po_distribution_id,
            apil.PO_HEADER_ID,    --added  on '30-sep-2015'
            apil.RCV_TRANSACTION_ID, --added  on '30-sep-2015'
            api.ORG_ID,
        (select hr.name from apps.hr_operating_units hr where hr.organization_id=api.org_id)  OPERATING_UNIT , 
        (select ABA.batch_NAME from apps.ap_batches_all ABA where aba.batch_id=api.batch_id) BATCH_NAME,   
--            hr.SHORT_CODE OPERATING_UNIT ,
--            aBA.batch_NAME, 
            api.invoice_id,
            app.accounting_date, 
            decode(apc.payment_type_flag,'Q','QUICK','M', 'MANUAL','R','REFUND') PAYMENT_TYPE, 
            apc.bank_account_name,
            apc.bank_account_num,
            apc.attribute3 Bank_UTI_no, 
            NULL invoice_amount, 
            decode(api.INVOICE_TYPE_LOOKUP_CODE,'PREPAYMENT',API.AMOUNT_PAID,NULL) AMOUNT_PAID ,
            APC.CHECK_DATE gl_date,
            NULL PREPAY_INVOICE_ID,
            NULL TERM_NAME,
            NULL PAY_GROUP,
            NULL PPI_Invoice_Number,
            NULL GRN_Number, 
            NULL GRN_DATE,APS.HOLD_FLAG
 FROM       APPS.ap_invoices_all api,
            APPS.ap_invoice_lines_all apil,
--            APPS.AP_BATCHES_ALL ABA,
            apps.gl_code_combinations gl,
            APPS.ap_invoice_distributions_all apd,
            APPS.AP_SUPPLIERS pov,
--            apps.hr_operating_units hr,
            APPS.ap_invoice_payments_all app,
            apps.ap_payment_schedules_all aps,
            APPS.ap_checks_all apc,
            APPS.AP_SUPPLIER_sites_all povs
 WHERE      api.invoice_id = apd.invoice_id
            AND apil.invoice_id = api.invoice_id
--            AND ABA.BATCH_ID(+)=API.BATCH_ID
            and aps.INVOICE_ID(+)=api.INVOICE_ID
--            and hr.ORGANIZATION_ID=api.ORG_ID
            and gl.CODE_COMBINATION_ID=api.ACCTS_PAY_CODE_COMBINATION_ID
            AND apil.line_number = apd.invoice_line_number
            AND  APP.ACCOUNTING_DATE >= TRUNC(SYSDATE) - 180
            AND apd.ROWID =
               (SELECT ROWID
                  FROM APPS.ap_invoice_distributions_all
                 WHERE ROWNUM = 1
                   AND invoice_id = apd.invoice_id)
                  -- AND match_status_flag = 'A')
            AND api.vendor_id = pov.vendor_id
            AND app.invoice_id = api.invoice_id
            AND app.check_id = apc.check_id
            AND apc.status_lookup_code IN
               ('CLEARED', 'NEGOTIABLE', 'VOIDED', 'RECONCILED UNACCOUNTED',
                'RECONCILED', 'CLEARED BUT UNACCOUNTED')
            --AND apd.match_status_flag = 'A'
            AND api.vendor_site_id = povs.vendor_site_id
            AND api.invoice_type_lookup_code <> 'PREPAYMENT'
            AND pov.VENDOR_TYPE_LOOKUP_CODE <> 'NON MERCHANDISE'
            -- and nvl(aps.AMOUNT_REMAINING,0)<>0
            -- api.invoice_id='78135429'
            -- and api.org_id=146
            -- and api.org_ID =901--,801,961,861)
            --and api.invoice_id in ('104974824',101629786,101629804,104659800,104659802,66703510,101371820)
            --and api.invoice_num in ('100390137','HN-3221257','DN SM 06219/Hyd/Chq/Off/07/12-13')
            AND (trunc(app.LAST_UPDATE_DATE) >= P_RUN_DATE   OR trunc(apc.LAST_UPDATE_DATE) >= P_RUN_DATE);

x_id utl_file.file_type;
BEGIN

SELECT P_RUN_DATE INTO L_RUN_DATE 
FROM DUAL; 

   SELECT 'ABRL_OFBIS_creditors'||'_'||'V'||'_'||'Merchandise'||'_'||L_RUN_DATE||'.txt'INTO V_L_FILE FROM dual;
  --SELECT 'XXABRL_HYPN_MKTNG_'||p_period_name||'_DATA'||NVL(TO_CHAR(TO_DATE(sysdate,'DD-MON-YYYY'),'DDMMYY'),TO_CHAR(SYSDATE-3,'DDMMYY'))||'.csv' INTO V_L_FILE FROM dual;
  x_id:=utl_file.fopen('VENDOR_INVOICE_PORTAL',V_L_FILE,'W');
utl_file.put_line(x_id,  'VENDOR SITE CODE'||'^'||
                              'VENDOR SITE ID'||'^'||--added on '27-Oct-2015'
                              'INVOICE_TYPE'||'^'||
                              'INVOICE NUMBER'||'^'||
                              'INVOICE DATE'||'^'||
                              'DESCRIPTION'||'^'||
                              'CCID'||'^'||
                              'SBU'||'^'||
                              'SBU DESC'||'^'||
                              'LOCATION CODE'||'^'||
                              'LOCATION DESC'||'^'||
                              'ACCOUNT CODE'||'^'||
                             -- 'PIAD AMOUNT'||'|'||
                              'ACCOUNT DESC'||'^'||
                              'INVOICE CURRENCY CODE'||'^'||
                              'DR VAL'||'^'||
                              'CR VAL'||'^'||
                              'PREPAY AMT REMAING'||'^'||
                              'DOC NUM'||'^'||
                              'CHECK NUMBER'||'^'||
                              'CHECK_DATE'||'^'||
                              'VENDOR NUMBER'||'^'||
                              'ATTRIBUTE_14'||'^'||    
                              'VENDOR NAME'||'^'||
                              'VENDOR TYPE'||'^'||
                              'PO DISTRIBUTION ID'||'^'||
                              'PO HEADER ID'||'^'||--added on '30-Sep-2015'
                              'RCV TRANSACTION ID'||'^'||--added on '30-Sep-2015'
                              'ORG ID'||'^'||--added on '27-Oct-2015'
                              'OPERATING UNIT'||'^'||
                              'BATCH NAME'||'^'||
                              'INVOICE ID'||'^'||
                              'PAY ACCOUNTING DATE'||'^'||
                              'PAYMENT TYPE FLAG'||'^'||
                              'BANK ACCOUNT NAME'||'^'||
                              'BANK ACCOUNT NUM'||'^'||
                              'BANK UTI NO.'||'^'||
                              'INVOICE AMOUNT'||'^'||
                              'AMOUNT PAID'||'^'||
                              'GL DATE'||'^'||
                              'PREPAY_INVOICE_ID'||'^'||
                              'TERM NAME'||'^'||
                              'PAY GROUP'||'^'||
                              'PPI INVOICE NUM'||'^'||
                              'GRN NUMBER'||'^'||
                              'GRN DATE'||'^'||
                              'HOLD FLAG'
                              );
  --utl_file.put_line(x_id,'VENDOR PAID INVOICES LIST DETAILS................................................->');

--utl_file.put_line(x_id,'VENDOR STANDARD INVOICES LIST........................................................->');
 FOR R IN CUR_PERIOD_NAME1 LOOP

 utl_file.put_line(x_id,      R.vendor_site_code||'^'||
                              R.VENDOR_SITE_ID ||'^'||
                              R.INVOICE_TYPE||'^'||
                              R.INVOICE_NUM||'^'||                           
                              R.INVOICE_DATE||'^'||
                              R.DESCRIPTION||'^'||
                              R.CCID||'^'||
                              R.SBU||'^'||
                              R.SBU_DESC||'^'||
                              R.LOCATION_CODE||'^'||
                              R.LOCATION_DESC||'^'||
                              R.ACCOUNT_CODE||'^'||
                             -- 'PIAD AMOUNT'||'|'||
                              R.ACCOUNT_DESC||'^'||
                              R.INVOICE_CURRENCY_CODE||'^'||
                              R.DR_VAL||'^'||
                              R.CR_VAL||'^'||
                              R.PREPAY_AMT_REMAING||'^'||
                              R.PAYMENT_NUM||'^'||
                              R.CHECK_NUMBER||'^'||
                              R.CHECK_DATE||'^'||
                              R.VENDOR_NUM||'^'||                              
                              R.ATTRIBUTE_14||'^'||                             
                              R.VENDOR_NAME||'^'||
                              R.VENDOR_TYPE||'^'||
                              R.PO_DISTRIBUTION_ID||'^'||
                              R.PO_HEADER_ID||'^'||--added on '30-Sep-2015'
                              R.RCV_TRANSACTION_ID||'^'||--added on '30-Sep-2015'
                              R.ORG_ID||'^'||
                              R.OPERATING_UNIT||'^'||
                              R.BATCH_NAME||'^'||
                              R.INVOICE_ID||'^'||
                              R.PAY_ACCOUNTING_DATE||'^'||
                              R.PAYMENT_TYPE_FLAG||'^'||
                              R.BANK_ACCOUNT_NAME||'^'||
                              R.BANK_ACCOUNT_NUM||'^'||
                              R.Bank_UTI_no||'^'||
                              R.INVOICE_AMOUNT||'^'||
                              R.AMOUNT_PAID||'^'||
                              R.GL_DATE ||'^'||
                              R.PREPAY_INVOICE_ID||'^'||
                              R.TERM_NAME||'^'||
                              R.PAY_GROUP||'^'||
                              R.PPI_Invoice_Number||'^'||
                              R.GRN_number||'^'||
                              R.GRN_date||'^'||
                              R.HOLD_FLAG--|| CHR(13) || CHR(10)
                              );

  end loop;
  --utl_file.put_line(x_id,'cur2.................................->');

  EXCEPTION
 WHEN TOO_MANY_ROWS THEN
    dbms_output.PUT_LINE('Too Many Rows');
 WHEN NO_DATA_FOUND THEN
    dbms_output.PUT_LINE('No Data Found');
 WHEN utl_file.invalid_path THEN
    RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_path');
 WHEN utl_file.invalid_mode THEN
     RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_mode');
 WHEN utl_file.invalid_filehandle THEN
     RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_filehandle');
 WHEN utl_file.invalid_operation THEN
     RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_operation');
 WHEN utl_file.read_error THEN
     RAISE_APPLICATION_ERROR(-20001, 'utl_file.read_error');
 WHEN utl_file.write_error THEN
     RAISE_APPLICATION_ERROR(-20001, 'utl_file.write_error');
 WHEN utl_file.internal_error THEN
     RAISE_APPLICATION_ERROR(-20001, 'utl_file.internal_error');
 WHEN OTHERS THEN
      dbms_output.put_line(SubStr('Error '||TO_CHAR(SQLCODE)||': '||SQLERRM, 1,255));
utl_file.fclose(x_id);
END; 
/


CREATE OR REPLACE PACKAGE BODY APPS.gst_abrl_nav_ar_bkdt_load_pkg
IS
      /*
      ========================
   =========================
   =========================
   =========================
   ======
      ||   Filename   : XXABRL_RESA_AR_CONV_PKG.sql
      ||   Description : Script is used to mold ReSA data for AR
      ||
      ||   Version     Date            Author              Modification
      ||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
      ||   1.0.0      29-sep-2009   Shailesh Bharambe      New Development
      ||   1.0.1      05-oct-2009   Shailesh Bharambe     Addition of Receipt logic outside the for loop
      ||   1.0.2      06-oct-2009   Shailesh Bharambe     Addition of the created receipt for applying .
      ||   1.0.3      09-oct-2009   Shailesh Bharambe     Addition of org contect and apps initialization in receipt submission conc prog. addition of interface flag with errored records also in cursor
      ||   1.0.4      12-oct-2009   Naresh Hasti          Addition of gl_date in APPS.XXABRL_NAVI_AR_INT_LINE_STG2 clause
      ||   1.0.5      21-oct-2009   Shailesh Bharambe     Addition of ReSa to AR Logic and ReIM To AP for All OU data load for Auto Invoices.
      ||   1.0.6      22-jan-2010   Shailesh Bharambe     Addition of Institutional ReSa to AR Logic.
      ||   1.0.7      09-Apr-2010   Praveen Kumar         Modified ReSA Procedure to schedule the data from Tender Form to Ofin
      ||   1.0.8      26-May-2010   Praveen Kumar/Naresh  GL Date condition is removed in ReSA Auto Invoice Conversion Program
      ||   1.0.9      27-May-2010   Naresh                added the sysdate with time stamp(24 hours) in ReSA Auto Invoice Conversion Program
      ||   1.1.0      21-Sep-2010   Mitul                 Edit XXABRL_REIM_AP_DT_LOAD_PRC  Submit Payble open inteface request For KL SM Responsbility
      ||   1.1.1      30-APR-2011   Ravi                  Edit XABRL_IS_RESA_AR_DT_LOAD_PRC Submit  request For KL SM Responsbility and TNSM
      ||   1.1.2      13-May-2011   Mitul  and Ravi       FOR AR restrict ('TSRL TN SM','TSRL KL SM','TSRL AP SM').
      ||   1.1.3      30-May-2011   Mitul  and Ravi       Edit XXABRL_REIM_AP_DT_LOAD_PRC  Submit Payble open inteface request For AP SM Responsbility
      ||   1.1.4      10-Oct-2011   Nijam                 Edit XXABRL_REIM_AP_DT_LOAD_PRC  Submit Payble open inteface request For KA SM Responsbility
      ||   1.1.5       27-Aug-2012     Dhiresh           New Org_ID define.
     ||   1.1.6      25-Oct-2013    Dhiresh               Edit XXABRL_REIM_AP_DT_LOAD_PRC  Submit Payble open inteface request For all TSRL Responsbility
   ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
      ========================
   =========================
   =========================
   =========================
   =====*/
   PROCEDURE xxabrl_wait_for_req_prc (
      p_request_id   IN   NUMBER,
      p_req_name     IN   VARCHAR2
   )
   AS
      l_sup_phase           VARCHAR2 (100);
      l_sup_status          VARCHAR2 (100);
      l_sup_phase1          VARCHAR2 (100);
      l_sup_status1         VARCHAR2 (100);
      l_sup_errbuff         VARCHAR2 (500);
      l_sup_b_call_status   BOOLEAN;
   BEGIN
      IF p_request_id = 0
      THEN
         fnd_file.put_line
                        (fnd_file.LOG,
                         'Error in Invoice Validation AND Conversion Program'
                        );
      ELSE
         COMMIT;
         l_sup_b_call_status :=
            fnd_concurrent.wait_for_request (request_id      => p_request_id,
                                             INTERVAL        => 5,
                                             max_wait        => 0,
                                             phase           => l_sup_phase,
                                             status          => l_sup_status,
                                             dev_phase       => l_sup_phase1,
                                             dev_status      => l_sup_status1,
                                             MESSAGE         => l_sup_errbuff
                                            );

         --
         IF l_sup_b_call_status
         THEN
            --
            IF (l_sup_phase1 = 'COMPLETE' AND l_sup_status1 = 'NORMAL')
            THEN
               fnd_file.put_line (fnd_file.LOG,
                                     p_req_name
                                  || TO_CHAR (p_request_id)
                                  || 'Completed Succesfully'
                                 );
            ELSE
               fnd_file.put_line (fnd_file.LOG,
                                     p_req_name
                                  || TO_CHAR (p_request_id)
                                  || ' Error'
                                 );
            END IF;
         --
         ELSE
            fnd_file.put_line (fnd_file.LOG,
                                  p_req_name
                               || TO_CHAR (p_request_id)
                               || ' Not Completed'
                              );
         END IF;
      END IF;
   END xxabrl_wait_for_req_prc;

-- XXABRL Navision AR Back Date Data Load
   PROCEDURE xxabrl_bak_dt_load_prc (errbuf OUT VARCHAR2, retcode OUT NUMBER)
   IS
-- cursor to select the transaction (status = N) from staging table.
      CURSOR cur_bkdt
      IS
         SELECT DISTINCT gl_date, org_code
                    FROM apps.xxabrl_navi_ra_int_lines_all
                   WHERE interfaced_flag IN ('V', 'N', 'E');

--and TRUNC(gl_DATE) between '10-sep-2009' and '10-sep-2009';
 --,'10-sep-2009');

      ---test req    28909109

      -- cursor to select the receipt (status = N)  from staging table.
      CURSOR cur_bkdt_rcpt
      IS
         SELECT DISTINCT gl_date, operating_unit
                    FROM apps.xxabrl_navi_ar_receipt_int
                   WHERE interfaced_flag IN ('N', 'C', 'V', 'EC', 'E');

--and TRUNC(gl_DATE) =  '10-sep-2009';-- and '08-sep-2009';
-- added by shailesh on 06 oct 2009
-- in above query we added the receipts which are validated, created and ready to apply
      v_req_id            NUMBER;
      v_req_id1           NUMBER;
      v_org_id            NUMBER;
      v_org_name          VARCHAR2 (100);
      v_batch_source_id   NUMBER;
      v_errmsg            VARCHAR2 (500);
      conc_request_id     NUMBER;
      success             BOOLEAN;
      v_user_id           NUMBER         := fnd_profile.VALUE ('USER_ID');
      v_resp_id           NUMBER         := fnd_profile.VALUE ('RESP_ID');
      v_appl_id           NUMBER        := fnd_profile.VALUE ('RESP_APPL_ID');
   BEGIN
      fnd_file.put_line (fnd_file.LOG,
                         'Back Dated AR Data Uploading starting...'
                        );

      FOR i IN cur_bkdt
      LOOP
         BEGIN
            SELECT organization_id, NAME
              INTO v_org_id, v_org_name
              FROM apps.hr_operating_units
             WHERE short_code = i.org_code;
         EXCEPTION
            WHEN OTHERS
            THEN
               v_org_id := 0;
               fnd_file.put_line (fnd_file.LOG,
                                     'gl date -> '
                                  || i.gl_date
                                  || ' , '
                                  || i.org_code
                                  || ' ORG NOT FOUND'
                                 );
         END;

         IF v_org_id <> 0
         THEN
            fnd_file.put_line (fnd_file.output,
                                  ' OPERATING UNIT => '
                               || v_org_name
                               || 'GL DATE => '
                               || i.gl_date
                              );

            BEGIN
               mo_global.set_policy_context ('M', v_org_id);
            END;

            -- we will submit this req through all corp ou
            fnd_global.apps_initialize (user_id           => v_user_id,
                                        resp_id           => v_resp_id,
                                        resp_appl_id      => v_appl_id
                                       );
            --AR Validation Program (Validation and Interface)
            fnd_file.put_line
                (fnd_file.LOG,
                    'step 1 :- Submitting Navision AR Validation/Interface...'
                 || i.gl_date
                 || '  '
                 || v_org_name
                );
            v_req_id :=
               fnd_request.submit_request ('XXABRL',
                                           'XXABRL_NAV_AR_INV_IMP_PKG',
                                           '',
                                           '',
                                           FALSE,
                                           v_org_id,
                                           'ABRL_NAVISION',
                                           'N',
                                           TO_CHAR (i.gl_date,
                                                    'yyyy/mm/dd hh24:mi:ss'
                                                   )
                                          );
            fnd_file.put_line
                          (fnd_file.output,
                              ' Navision AR Validation/Interface Request id :'
                           || v_req_id
                          );
            --- following procedure calls the wait for request api this will stop the control of loop till
            -- above request got completed
            xxabrl_wait_for_req_prc (v_req_id,
                                     'Navision AR Validation/Interface'
                                    );

----
            BEGIN
               SELECT batch_source_id
                 INTO v_batch_source_id
                 FROM ra_batch_sources_all
                WHERE batch_source_type = 'FOREIGN'
                  AND status = 'A'
                  AND NAME = 'Navision'
                  AND org_id = v_org_id;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  v_errmsg := 'Navision batch source not found';
               WHEN TOO_MANY_ROWS
               THEN
                  v_errmsg := 'Multiple Navision batch source found';
               WHEN OTHERS
               THEN
                  v_errmsg := 'Exception in Navision batch source ';
            END;

            fnd_file.put_line (fnd_file.LOG,
                                  'step 2 :- Submitting Autoinvoice Master...'
                               || i.gl_date
                               || '  '
                               || v_org_name
                              );
            v_req_id :=
               fnd_request.submit_request ('AR',
                                           'RAXTRX',
                                           '',
                                           '',
                                           FALSE,
                                           'MAIN',
                                           'T',
                                           v_batch_source_id,
                                           'Navision',
                                           TO_CHAR (i.gl_date,
                                                    'yyyy/mm/dd hh24:mi:ss'
                                                   ),
                                           NULL,
                                           NULL,      -- Parameter Data source
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL,
                                           TO_CHAR (i.gl_date,
                                                    'yyyy/mm/dd hh24:mi:ss'
                                                   ),
                                           -- GL_DATE FROM
                                           TO_CHAR (i.gl_date,
                                                    'yyyy/mm/dd hh24:mi:ss'
                                                   ),
                                           -- GL_DATE TO
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL,
                                           'N',
                                           'Y',
                                           NULL,
                                           v_org_id
                                          );
            fnd_file.put_line (fnd_file.output,
                                  ' Auotoinvoice Master Program Request id :'
                               || v_req_id
                              );
            --- following procedure calls the wait for request api this will stop the control of loop till
            -- above request got completed
            xxabrl_wait_for_req_prc (v_req_id, 'Autoinvoice Program');
---==========================
         END IF;
      END LOOP;

      v_org_id := 0;

      -- starting the pending receipt conversion data
      FOR j IN cur_bkdt_rcpt
      LOOP
         BEGIN
            SELECT organization_id, NAME
              INTO v_org_id, v_org_name
              FROM apps.hr_operating_units
             WHERE short_code = j.operating_unit;
         EXCEPTION
            WHEN OTHERS
            THEN
               v_org_id := 0;
               fnd_file.put_line (fnd_file.LOG,
                                     'gl date -> '
                                  || j.gl_date
                                  || ' , '
                                  || j.operating_unit
                                  || ' ORG NOT FOUND'
                                 );
         END;

         -- added by shailesh on 9th oct 2009
         BEGIN
            mo_global.set_policy_context ('M', v_org_id);
         END;

         -- we will submit this req through all corp ou
         fnd_global.apps_initialize (user_id           => v_user_id,
                                     resp_id           => v_resp_id,
                                     resp_appl_id      => v_appl_id
                                    );
          -- navision ar receipt validation and conversion program
          /*fnd_file.put_line(FND_FILE.LOG, 'step 3 :- Submitting Navision AR Receipts Validation/Conversion...'||j.GL_DATE ||'  '||V_ORG_NAME);

                 v_req_id :=  fnd_request.submit_request ('XXABRL',
                                                        'XXABRL_NAV_AR_RECPT_IMP_PKG',
                                                        '',
                                                        '',
                                                        FALSE,
                                                        'Y',
                                                        'ABRL_NAVISION',
                                                        NULL,
                                                        'N',
                                                        to_char(j.GL_DATE,'yyyy/mm/dd hh24:mi:ss'),
                                                        v_org_id
                                                        );


         fnd_file.put_line(fnd_file.output,' Navision AR Receipts Validation/Conversion Request id :'||v_req_id);
         */
                 --- following procedure calls the wait for request api this will stop the control of loop till
                 -- above request got completed
         xxabrl_wait_for_req_prc (v_req_id,
                                  'Navision AR Receipts Validation/Conversion'
                                 );
         v_req_id :=
            fnd_request.submit_request ('XXABRL',
                                        'XXABRL_NAV_AR_RECPT_IMP_PKG',
                                        '',
                                        '',
                                        FALSE,
                                        'N',
                                        'ABRL_NAVISION',
                                        NULL,
                                        'Y',
                                        TO_CHAR (j.gl_date,
                                                 'yyyy/mm/dd hh24:mi:ss'
                                                ),
                                        v_org_id
                                       );
         fnd_file.put_line (fnd_file.output,
                               ' Navision AR Receipts Apply Request id :'
                            || v_req_id
                           );
         --- following procedure calls the wait for request api this will stop the control of loop till
         -- above request got completed
         xxabrl_wait_for_req_prc (v_req_id,
                                  'Navision AR Receipts Apply program'
                                 );
      END LOOP;
   END xxabrl_bak_dt_load_prc;

--- following code added by shailesh on 21 oct 2009
-- program for the Resa to Ar to load all ou data as well as back date data.
-- ReSA To AR Data Load program for All OU
   PROCEDURE xxabrl_resa_ar_dt_load_prc (
      errbuf    OUT   VARCHAR2,
      retcode   OUT   NUMBER
   )
   IS
      CURSOR cur_resa_ar
      IS
         SELECT DISTINCT org_id
                    FROM apps.xxabrl_navi_ar_int_line_stg2
                   WHERE 1 = 1
                     AND TRUNC (gl_date) > '31-MAR-14'
                     AND interface_line_context = 'ABRL_RESA'
                     AND interfaced_flag IN ('N', 'E', 'V')
                     AND freeze_flag = 'Y'
                     AND (    org_id IS NOT NULL
                          AND org_id NOT IN
                                    (901, 801, 961, 861, 84, 87, 89, 91, 161)
                         );

      CURSOR cur_resa_ar_rcpt
      IS
         SELECT DISTINCT org_name
                    FROM apps.xxabrl_navi_ar_receipt_stg2
                   WHERE 1 = 1
                     AND TRUNC (gl_date) > '31-MAR-14'
                     AND data_source = 'ABRL_RESA'
                     AND interfaced_flag IN ('N', 'E', 'V', 'EC', 'C')
                     AND freeze_flag = 'Y'
                     AND org_id NOT IN
                                    (901, 801, 961, 861, 84, 87, 89, 91, 161);

      v_errmsg             VARCHAR2 (500);
      v_batch_source_id    NUMBER;
      v_user_id            NUMBER          := fnd_profile.VALUE ('USER_ID');
      v_resp_id            NUMBER          := fnd_profile.VALUE ('RESP_ID');
      v_appl_id            NUMBER        := fnd_profile.VALUE ('RESP_APPL_ID');
      v_req_id             NUMBER;
      v_org_name           VARCHAR2 (100);
      v_org_id             NUMBER;
      stop_program         EXCEPTION;
      l_phase              VARCHAR2 (100);
      l_status             VARCHAR2 (100);
      l_dev_phase          VARCHAR2 (100);
      l_dev_status         VARCHAR2 (100);
      l_message            VARCHAR2 (1000);
      v_req_status         BOOLEAN;
      xx_interface_count   NUMBER          := 0;
   BEGIN
      FOR i IN cur_resa_ar
      LOOP
         BEGIN
            mo_global.set_policy_context ('S', i.org_id);
         END;

         -- we will submit this req through all corp ou
         fnd_global.apps_initialize (user_id           => v_user_id,
                                     resp_id           => v_resp_id,
                                     resp_appl_id      => v_appl_id
                                    );

         --ReSA To AR Validation Program (Validation and Interface)
         BEGIN
            SELECT NAME
              INTO v_org_name
              FROM hr_operating_units
             WHERE organization_id = i.org_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               v_org_name := 'No Org_found for ' || i.org_id;
         END;

         fnd_file.put_line
             (fnd_file.LOG,
              'step 1 :- Submitting ReSa To AR  Invoice Conversion Program...'
             );
         v_req_id :=
            fnd_request.submit_request ('XXABRL',
                                        'XXABRL_RESA_AR_TNDR_INV_PKG',
                                        
                                        ---'XXABRL_RESA_AR_INV_IMP_PKG',
                                        '',
                                        '',
                                        FALSE,
                                        'ReSA',
                                        'N',
                                        i.org_id
                                       );
         fnd_file.put_line
                      (fnd_file.LOG,
                          'ReSa To AR  Invoice Conversion Program Request Id '
                       || v_req_id
                      );
         -- Wait for request for the invoice conversion program
         xxabrl_wait_for_req_prc (v_req_id,
                                  'ReSa To AR  Invoice Conversion Program'
                                 );
         fnd_file.put_line (fnd_file.LOG, 'starting status program');
         --- IF STATUS IS WARNING THEN STOP PROG
         --- RAISE stop_program;
         v_req_status :=
            fnd_concurrent.get_request_status (request_id      => v_req_id,
                                               phase           => l_phase
                                                                         --OUT NOCOPY varchar2,
            ,
                                               status          => l_status
                                                                          --OUT NOCOPY varchar2,
            ,
                                               dev_phase       => l_dev_phase
                                                                             --OUT NOCOPY varchar2,
            ,
                                               dev_status      => l_dev_status
                                                                              --OUT NOCOPY varchar2,
            ,
                                               MESSAGE         => l_message
                                              );        --OUT NOCOPY varchar2)
         COMMIT;
         fnd_file.put_line (fnd_file.LOG, 'l_phase' || l_phase);
         fnd_file.put_line (fnd_file.LOG, 'l_status' || l_status);
         fnd_file.put_line (fnd_file.LOG, 'l_dev_phase' || l_dev_phase);
         fnd_file.put_line (fnd_file.LOG, 'l_dev_status' || l_dev_status);
         fnd_file.put_line (fnd_file.LOG, 'l_message' || l_message);

         IF l_status = 'Warning'
         THEN
            -- RAISE stop_program;
            fnd_file.put_line (fnd_file.LOG,
                               'Error Occured in Invoce Conversion Program'
                              );
            EXIT;
         END IF;

         IF l_status = 'Error'
         THEN
            --   RAISE stop_program;
            fnd_file.put_line (fnd_file.LOG,
                               'Error Occured in Invoce Conversion Program'
                              );
            EXIT;
         END IF;

         ----- submitting autoinvoice program

         --*****************************
         fnd_file.put_line (fnd_file.LOG,
                            'step 2 :- Submitting Autoinvoice Master...'
                           );

         /*   v_req_id := fnd_request.submit_request('AR',
                                                    'RAXMTR',
                                                    '',
                                                    '',
                                                    FALSE,
                                                    1,
                                                    NULL,
                                                    'ReSA',
                                                    SYSDATE,
                                                    NULL,
                                                    NULL, -- Parameter Data source
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    null,--to_char(i.GL_DATE,'yyyy/mm/dd hh24:mi:ss'), -- GL_DATE FROM
                                                    null,--to_char(i.GL_DATE,'yyyy/mm/dd hh24:mi:ss'), -- GL_DATE TO
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                  --  'N',
                                                    'Y',
                                                    NULL,
                                                   i.org_id
                                                  );
                                                    */

         /*
         select * from ar_ai_batch_source_v
         where batch_source_id=1139
              */
         BEGIN
            SELECT batch_source_id
              INTO v_batch_source_id
              FROM ra_batch_sources_all
             WHERE batch_source_type = 'FOREIGN'
               AND status = 'A'
               AND NAME = 'ReSA'
               AND org_id = i.org_id;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               v_errmsg := 'ReSA batch source not found';
            WHEN TOO_MANY_ROWS
            THEN
               v_errmsg := 'Multiple ReSa batch source found';
            WHEN OTHERS
            THEN
               v_errmsg := 'Exception in ReSA batch source ';
         END;

         fnd_file.put_line (fnd_file.LOG, v_errmsg);
--         SELECT COUNT (*)
--           INTO xx_interface_count
--           FROM apps.ra_interface_lines_all
--          WHERE org_id = i.org_id;

         --         IF NVL (xx_interface_count, 0) > 5000
--         THEN
--            UPDATE apps.ra_interface_lines_all
--               SET interface_status = 'P'
--             WHERE org_id = i.org_id;

         --            UPDATE apps.ra_interface_distributions_all
--               SET interface_status = 'P'
--             WHERE org_id = i.org_id;

         --            COMMIT;

         --            FOR xx_rec IN (SELECT DISTINCT a.org_id
--                                      FROM apps.ra_interface_lines_all a,
--                                           apps.ra_interface_errors_all b
--                                     WHERE a.org_id = i.org_id
--                                       AND a.interface_line_id = b.interface_line_id(+)
--                                       AND b.MESSAGE_TEXT(+) IS NULL)
--            LOOP
--               EXIT WHEN SQL%NOTFOUND;

         --               UPDATE apps.ra_interface_lines_all
--                  SET interface_line_id = NULL,
--                      customer_trx_id = NULL,
--                      request_id = NULL,
--                      link_to_line_id = NULL,
--                      interface_status = NULL
--                WHERE org_id = i.org_id AND ROWNUM <= 5000;

         --               UPDATE apps.ra_interface_distributions_all
--                  SET interface_line_id = NULL,
--                      request_id = NULL,
--                      interface_status = NULL
--                WHERE org_id = i.org_id AND ROWNUM <= 5000;

         --               -- first 5000  rows updated
--               COMMIT;
--               v_req_id :=
--                  fnd_request.submit_request
--                              (application      => 'AR',
--                               program          => 'RAXMTR',
--                               description      => 'Autoinvoice Master Program',
--                               start_time       => TO_CHAR
--                                                      (SYSDATE,
--                                                       'DD/MM/YYYY HH24:MI:SS'
--                                                      ),
--                               sub_request      => FALSE,
--                               argument1        => '1',
--                               argument2        => i.org_id,
--                               argument3        => v_batch_source_id,
--                               argument4        => 'ReSA',
--                               argument5        => TO_CHAR
--                                                      (SYSDATE,
--                                                       'DD/MM/YYYY HH24:MI:SS'
--                                                      ),
--                               argument6        => '',
--                               argument7        => '',
--                               argument8        => '',
--                               argument9        => '',
--                               argument10       => '',
--                               argument11       => '',
--                               argument12       => '',
--                               argument13       => '',
--                               argument14       => '',
--                               argument15       => '',
--                               argument16       => '',
--                               argument17       => '',
--                               argument18       => '',
--                               argument19       => '',
--                               argument20       => '',
--                               argument21       => '',
--                               argument22       => '',
--                               argument23       => '',
--                               argument24       => '',
--                               argument25       => '',
--                               argument26       => 'Y',
--                               argument27       => ''
--                              );
--               COMMIT;
--               fnd_file.put_line
--                               (fnd_file.output,
--                                   ' Auotoinvoice Master Program Request id :'
--                                || v_req_id
--                               );
--               --- following procedure calls the wait for request api this will stop the control of loop till
--               -- above request got completed
--               xxabrl_wait_for_req_prc (v_req_id, 'Autoinvoice Program');
--            END LOOP;
--         ELSE
         v_req_id :=
            fnd_request.submit_request
                              (application      => 'AR',
                               program          => 'RAXMTR',
                               description      => 'Autoinvoice Master Program',
                               start_time       => TO_CHAR
                                                      (SYSDATE,
                                                       'DD/MM/YYYY HH24:MI:SS'
                                                      ),
                               sub_request      => FALSE,
                               argument1        => '1',
                               argument2        => i.org_id,
                               argument3        => v_batch_source_id,
                               argument4        => 'ReSA',
                               argument5        => TO_CHAR
                                                      (SYSDATE,
                                                       'DD/MM/YYYY HH24:MI:SS'
                                                      ),
                               argument6        => '',
                               argument7        => '',
                               argument8        => '',
                               argument9        => '',
                               argument10       => '',
                               argument11       => '',
                               argument12       => '',
                               argument13       => '',
                               argument14       => '',
                               argument15       => '',
                               argument16       => '',
                               argument17       => '',
                               argument18       => '',
                               argument19       => '',
                               argument20       => '',
                               argument21       => '',
                               argument22       => '',
                               argument23       => '',
                               argument24       => '',
                               argument25       => '',
                               argument26       => 'Y',
                               argument27       => ''
                              );
         COMMIT;
         fnd_file.put_line (fnd_file.output,
                               ' Auotoinvoice Master Program Request id :'
                            || v_req_id
                           );
         --- following procedure calls the wait for request api this will stop the control of loop till
         -- above request got completed
         xxabrl_wait_for_req_prc (v_req_id, 'Autoinvoice Program');
--         END IF;
--*****************************
      END LOOP;

      -- code for submitting the receipt data
      FOR j IN cur_resa_ar_rcpt
      LOOP
         BEGIN
            SELECT organization_id, NAME
              INTO v_org_id, v_org_name
              FROM apps.hr_operating_units
             WHERE short_code = j.org_name;
         EXCEPTION
            WHEN OTHERS
            THEN
               v_org_id := 0;
               fnd_file.put_line (fnd_file.LOG,
                                  'No Org found for ' || j.org_name
                                 );
         END;

         BEGIN
            mo_global.set_policy_context ('M', v_org_id);
         END;

         -- we will submit this req through all corp ou
         fnd_global.apps_initialize (user_id           => v_user_id,
                                     resp_id           => v_resp_id,
                                     resp_appl_id      => v_appl_id
                                    );
         -- ReSA ar receipt validation and conversion program
         fnd_file.put_line
            (fnd_file.LOG,
                'step 3 :- Submitting ReSA AR Receipts Conversion Program for  '
             || v_org_name
            );
         v_req_id :=
            fnd_request.submit_request
                               ('XXABRL',
                                LTRIM (RTRIM ('XXABRL_RESA_AR_RECPT_TNDR_PKG')),
                                
                                ----'XXABRL_RESA_AR_RECPT_IMP_PKG','XXABRL_RESA_AR_RECPT_IMP_PKG',
                                '',
                                '',
                                FALSE,
                                'N',
                                'ABRL_RESA',
                                NULL,
                                'Y',
                                v_org_id
                               );
         COMMIT;
         fnd_file.put_line
                       (fnd_file.output,
                           ' ReSA AR Receipts Conversion Program Request id :'
                        || v_req_id
                       );
         xxabrl_wait_for_req_prc (v_req_id,
                                  'ReSA AR Receipts Conversion Program'
                                 );
      END LOOP;
   END xxabrl_resa_ar_dt_load_prc;

-----
-- Payable Open Iterface program for the ReIM To AP to load all Oracle Payables Invoces
----

   -- Code Modify by Dhiresh dt. 27/08/2012
--after TSRL Migration ORG_ID changed , which now updated in this code
   PROCEDURE xxabrl_reim_ap_dt_load_prc (
      errbuf    OUT   VARCHAR2,
      retcode   OUT   NUMBER
   )
   IS
      CURSOR cur_ap_data
      IS
         SELECT DISTINCT org_id
                    FROM ap_invoices_interface
                   WHERE SOURCE = 'RETEK'
                     AND status IS NULL
                     AND org_id NOT IN
                            (801, 861, 901, 961, 841, 821, 881, 921, 981, 941,
                             84, 87, 89, 91, 86, 93, 94, 95, 96, 663);

      v_org_name     VARCHAR2 (30);
      v_batch_name   VARCHAR2 (100);
      v_req_id       NUMBER;
      v_user_id      NUMBER         := fnd_profile.VALUE ('USER_ID');
      v_resp_id      NUMBER         := fnd_profile.VALUE ('RESP_ID');
      v_appl_id      NUMBER         := fnd_profile.VALUE ('RESP_APPL_ID');
   BEGIN
      FOR i IN cur_ap_data
      LOOP
         BEGIN
            SELECT short_code
              INTO v_org_name
              FROM apps.hr_operating_units
             WHERE organization_id = i.org_id;

            v_batch_name :=
                      'ReIM Invoices/' || v_org_name || '/' || TRUNC (SYSDATE);
         EXCEPTION
            WHEN OTHERS
            THEN
               v_org_name := '';
         END;

         BEGIN
            mo_global.set_policy_context ('M', i.org_id);
         END;

         -- we will submit this req through all corp ou
         fnd_global.apps_initialize (user_id           => v_user_id,
                                     resp_id           => v_resp_id,
                                     resp_appl_id      => v_appl_id
                                    );
         -----
         -- Run Payable Open Interface for Interface data into Oracle Payables
         ----
         fnd_file.put_line (fnd_file.output,
                            'Submitting  Payable Open Iterface:'
                           );
         v_req_id :=
            fnd_request.submit_request ('SQLAP',
                                        'APXIIMPT',
                                        '',
                                        NULL,
                                        FALSE,
                                        i.org_id,
                                        'RETEK',      -- Parameter Data source
                                        NULL,                      -- Group id
                                        v_batch_name,
                                        NULL,
                                        NULL,
                                        TRUNC (SYSDATE),
                                        'N',
                                        'N',
                                        'N',
                                        'Y',
                                        CHR (0)
                                       );
         COMMIT;
         fnd_file.put_line (fnd_file.output,
                            ' Payable Open Iterface Request id :' || v_req_id
                           );
         xxabrl_wait_for_req_prc (v_req_id, 'Payable Open Iterface');
      END LOOP;

      -- ADDED BY MITUL
      -- MODIFY BY DHIRESH  dt 25-oct-2013
      BEGIN
         FOR sel_org IN (SELECT DISTINCT org_id
                                    FROM ap_invoices_interface
                                   WHERE SOURCE = 'RETEK'
                                     AND org_id IN
                                            (1524, 1521, 1523, 1525, 1481,
                                             1501, 1522, 1721))
         LOOP
            SELECT short_code,
                   DECODE (organization_id,
                           1524, 62428,
                           1521, 62429,
                           1522, 62424,
                           1525, 62427,
                           1501, 62426,
                           1481, 62423,
                           1721, 62512,
                           1523, 62425
                          ) resp_id,
                   200 appl_id
              INTO v_org_name,
                   v_resp_id,
                   v_appl_id
              FROM apps.hr_operating_units
             WHERE organization_id = sel_org.org_id;

            v_batch_name :=
                      'ReIM Invoices/' || v_org_name || '/' || TRUNC (SYSDATE);
            fnd_global.apps_initialize (user_id           => v_user_id,
                                        resp_id           => v_resp_id,
                                        resp_appl_id      => v_appl_id
                                       );
            v_req_id :=
               fnd_request.submit_request ('SQLAP',
                                           'APXIIMPT',
                                           '',
                                           NULL,
                                           FALSE,
                                           sel_org.org_id,
                                           'RETEK',   -- Parameter Data source
                                           NULL,                   -- Group id
                                           v_batch_name,
                                           NULL,
                                           NULL,
                                           TRUNC (SYSDATE),
                                           'N',
                                           'N',
                                           'N',
                                           'Y',
                                           CHR (0)
                                          );
            fnd_file.put_line (fnd_file.output,
                                  ' Payable Open Iterface Request id :'
                               || v_req_id
                              );
            xxabrl_wait_for_req_prc (v_req_id, 'Payable Open Iterface');
            COMMIT;
         END LOOP;
      END;
   END xxabrl_reim_ap_dt_load_prc;

   --ADDED BY MITUL

   --*******************************************************************************************
-- ABRL Institutional Sales RESA To AR
--*******************************************************************************************

   --- following code added by shailesh on 21 oct 2009
-- program for the Resa to Ar to load all ou data as well as back date data.
-- ReSA To AR Data Load program for All OU
   PROCEDURE xxabrl_is_resa_ar_dt_load_prc (
      errbuf    OUT   VARCHAR2,
      retcode   OUT   NUMBER
   )
   IS
      CURSOR cur_resa_ar
      IS
         SELECT DISTINCT org_id, gl_date                          --,org_code
                    FROM xxabrl_inst_resa_ra_lines_all
                   WHERE interfaced_flag = 'N'
                     AND org_id NOT IN (84, 87, 89, 91, 663);

      --AND org_code ='ABRLNCRUP'  AND gl_date = '10-Nov-2009'  ;
      v_errmsg            VARCHAR2 (500);
      v_batch_source_id   NUMBER;
      v_user_id           NUMBER          := fnd_profile.VALUE ('USER_ID');
      v_resp_id           NUMBER          := fnd_profile.VALUE ('RESP_ID');
      v_appl_id           NUMBER         := fnd_profile.VALUE ('RESP_APPL_ID');
      v_req_id            NUMBER;
      v_org_name          VARCHAR2 (100);
      v_org_id            NUMBER;
      stop_program        EXCEPTION;
      l_phase             VARCHAR2 (100);
      l_status            VARCHAR2 (100);
      l_dev_phase         VARCHAR2 (100);
      l_dev_status        VARCHAR2 (100);
      l_message           VARCHAR2 (1000);
      v_req_status        BOOLEAN;
   BEGIN
      fnd_file.put_line
            (fnd_file.LOG,
             'Started the loading of the Institutional Sales Data for all OU'
            );

      FOR i IN cur_resa_ar
      LOOP
         IF cur_resa_ar%NOTFOUND
         THEN
            fnd_file.put_line (fnd_file.LOG, 'No Data Found to Load');
         END IF;

         v_errmsg := '';

         BEGIN
            mo_global.set_policy_context ('S', i.org_id);
         END;

         -- we will submit this req through all corp ou
         fnd_global.apps_initialize (user_id           => v_user_id,
                                     resp_id           => v_resp_id,
                                     resp_appl_id      => v_appl_id
                                    );

         --ReSA To AR Validation Program (Validation and Interface)
         BEGIN
            SELECT NAME
              INTO v_org_name
              FROM hr_operating_units
             WHERE organization_id = i.org_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               v_org_name := 'No Org_found for ' || i.org_id;
         END;

         fnd_file.put_line
            (fnd_file.LOG,
             'step 1 :- Submitting ABRL Institutional Sales ReSA To AR Validate Program...'
            );
         v_req_id :=
            fnd_request.submit_request ('XXABRL',
                                        'XXABRL_INST_RESA_AR_INV_PKG',
                                        '',
                                        '',
                                        FALSE,
                                        'ReSA_IS',
                                        'N',
                                        i.org_id
                                       );
         fnd_file.put_line
            (fnd_file.LOG,
                'ABRL Institutional Sales ReSA To AR Validate Program Request Id '
             || v_req_id
            );
         -- Wait for request for the invoice conversion program
         xxabrl_wait_for_req_prc
                       (v_req_id,
                        'ABRL Institutional Sales ReSA To AR Validate Program'
                       );
         fnd_file.put_line (fnd_file.LOG, 'starting status program');
         --- IF STATUS IS WARNING THEN STOP PROG
         --- RAISE stop_program;
         v_req_status :=
            fnd_concurrent.get_request_status (request_id      => v_req_id,
                                               phase           => l_phase
                                                                         --OUT NOCOPY varchar2,
            ,
                                               status          => l_status
                                                                          --OUT NOCOPY varchar2,
            ,
                                               dev_phase       => l_dev_phase
                                                                             --OUT NOCOPY varchar2,
            ,
                                               dev_status      => l_dev_status
                                                                              --OUT NOCOPY varchar2,
            ,
                                               MESSAGE         => l_message
                                              );        --OUT NOCOPY varchar2)
         COMMIT;
         fnd_file.put_line (fnd_file.LOG, 'l_phase' || l_phase);
         fnd_file.put_line (fnd_file.LOG, 'l_status' || l_status);
         fnd_file.put_line (fnd_file.LOG, 'l_dev_phase' || l_dev_phase);
         fnd_file.put_line (fnd_file.LOG, 'l_dev_status' || l_dev_status);
         fnd_file.put_line (fnd_file.LOG, 'l_message' || l_message);

         IF l_status = 'Warning'
         THEN
            -- RAISE stop_program;
            fnd_file.put_line
               (fnd_file.LOG,
                'Error Occured in ABRL Institutional Sales ReSA To AR Validate Program'
               );
            EXIT;
         END IF;

         IF l_status = 'Error'
         THEN
            --   RAISE stop_program;
            fnd_file.put_line
               (fnd_file.LOG,
                'Error Occured in ABRL Institutional Sales ReSA To AR Validate Program'
               );
            EXIT;
         END IF;

         ----- submitting autoinvoice program

         --*****************************
         fnd_file.put_line (fnd_file.LOG,
                            'step 2 :- Submitting Autoinvoice Master...'
                           );

         BEGIN
            SELECT batch_source_id
              INTO v_batch_source_id
              FROM ra_batch_sources_all
             WHERE batch_source_type = 'FOREIGN'
               AND status = 'A'
               AND NAME = 'ReSA_IS'
               AND org_id = i.org_id;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               v_errmsg := 'ReSA batch source not found';
            WHEN TOO_MANY_ROWS
            THEN
               v_errmsg := 'Multiple ReSa batch source found';
            WHEN OTHERS
            THEN
               v_errmsg := 'Exception in ReSA batch source ';
         END;

         fnd_file.put_line (fnd_file.LOG, v_errmsg);

         IF v_errmsg IS NULL
         THEN
            v_req_id :=
               fnd_request.submit_request
                              (application      => 'AR',
                               program          => 'RAXMTR',
                               description      => 'Autoinvoice Master Program',
                               start_time       => TO_CHAR
                                                        (SYSDATE,
                                                         'DD/MM/YYYY HH:MI:SS'
                                                        ),
                               sub_request      => FALSE,
                               argument1        => '1',
                               argument2        => i.org_id,
                               argument3        => v_batch_source_id,
                               argument4        => 'ReSA_IS',
                               argument5        => TO_CHAR
                                                      (i.gl_date,
                                                       'yyyy/mm/dd hh24:mi:ss'
                                                      ),
                               argument6        => '',
                               argument7        => '',
                               argument8        => '',
                               argument9        => '',
                               argument10       => '',
                               argument11       => '',
                               argument12       => '',
                               argument13       => '',
                               argument14       => '',
                               argument15       => '',
                               argument16       => '',
                               argument17       => '',
                               argument18       => '',
                               argument19       => '',
                               argument20       => '',
                               argument21       => '',
                               argument22       => '',
                               argument23       => '',
                               argument24       => '',
                               argument25       => '',
                               argument26       => 'Y',
                               argument27       => ''
                              );
            COMMIT;
            fnd_file.put_line (fnd_file.output,
                                  ' Auotoinvoice Master Program Request id :'
                               || v_req_id
                              );
            --- following procedure calls the wait for request api this will stop the control of loop till
            -- above request got completed
            xxabrl_wait_for_req_prc (v_req_id, 'Autoinvoice Program');
         END IF;
--**********************************************************************************
      END LOOP;

      --- added by Ravi-------------

      -- 30.06.2014        not required for old TSRL Org Id's  ... to be deleted from the code by 2nd week Jul 2014
      BEGIN
         FOR sel_org_1 IN (SELECT DISTINCT org_id, gl_date        --,org_code
                                      FROM xxabrl_inst_resa_ra_lines_all
                                     WHERE interfaced_flag = 'N'
                                       AND org_id IN (84, 87, 89, 91, 663))
         LOOP
            BEGIN
               SELECT NAME
                 INTO v_org_name
                 FROM hr_operating_units
                WHERE organization_id = sel_org_1.org_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  v_org_name := 'No Org_found for ' || sel_org_1.org_id;
            END;

            IF sel_org_1.org_id = 89
            THEN
               fnd_global.apps_initialize (4191, 50539, 222);
               fnd_file.put_line
                  (fnd_file.LOG,
                   'step 1 :- Submitting ABRL Institutional Sales ReSA To AR Validate Program...'
                  );
               v_req_id :=
                  fnd_request.submit_request ('XXABRL',
                                              'XXABRL_INST_RESA_AR_INV_PKG',
                                              '',
                                              '',
                                              FALSE,
                                              'ReSA_IS',
                                              'N',
                                              sel_org_1.org_id
                                             );
               fnd_file.put_line
                  (fnd_file.LOG,
                      'ABRL Institutional Sales ReSA To AR Validate Program Request Id '
                   || v_req_id
                  );
               -- Wait for request for the invoice conversion program
               xxabrl_wait_for_req_prc
                       (v_req_id,
                        'ABRL Institutional Sales ReSA To AR Validate Program'
                       );
               fnd_file.put_line (fnd_file.LOG, 'starting status program');
               --- IF STATUS IS WARNING THEN STOP PROG
               --- RAISE stop_program;
               v_req_status :=
                  fnd_concurrent.get_request_status
                                                  (request_id      => v_req_id,
                                                   phase           => l_phase
                                                                             --OUT NOCOPY varchar2,
                  ,
                                                   status          => l_status
                                                                              --OUT NOCOPY varchar2,
                  ,
                                                   dev_phase       => l_dev_phase
                                                                                 --OUT NOCOPY varchar2,
                  ,
                                                   dev_status      => l_dev_status
                                                                                  --OUT NOCOPY varchar2,
                  ,
                                                   MESSAGE         => l_message
                                                  );    --OUT NOCOPY varchar2)
               COMMIT;
               fnd_file.put_line (fnd_file.LOG, 'l_phase' || l_phase);
               fnd_file.put_line (fnd_file.LOG, 'l_status' || l_status);
               fnd_file.put_line (fnd_file.LOG, 'l_dev_phase' || l_dev_phase);
               fnd_file.put_line (fnd_file.LOG,
                                  'l_dev_status' || l_dev_status);
               fnd_file.put_line (fnd_file.LOG, 'l_message' || l_message);

               IF l_status = 'Warning'
               THEN
                  -- RAISE stop_program;
                  fnd_file.put_line
                     (fnd_file.LOG,
                      'Error Occured in ABRL Institutional Sales ReSA To AR Validate Program'
                     );
                  EXIT;
               END IF;

               IF l_status = 'Error'
               THEN
                  --   RAISE stop_program;
                  fnd_file.put_line
                     (fnd_file.LOG,
                      'Error Occured in ABRL Institutional Sales ReSA To AR Validate Program'
                     );
                  EXIT;
               END IF;

               ----- submitting autoinvoice program

               --*****************************
               fnd_file.put_line (fnd_file.LOG,
                                  'step 2 :- Submitting Autoinvoice Master...'
                                 );

               BEGIN
                  SELECT batch_source_id
                    INTO v_batch_source_id
                    FROM ra_batch_sources_all
                   WHERE batch_source_type = 'FOREIGN'
                     AND status = 'A'
                     AND NAME = 'ReSA_IS'
                     AND org_id = sel_org_1.org_id;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     v_errmsg := 'ReSA batch source not found';
                  WHEN TOO_MANY_ROWS
                  THEN
                     v_errmsg := 'Multiple ReSa batch source found';
                  WHEN OTHERS
                  THEN
                     v_errmsg := 'Exception in ReSA batch source ';
               END;

               fnd_file.put_line (fnd_file.LOG, v_errmsg);

               IF v_errmsg IS NULL
               THEN
                  v_req_id :=
                     fnd_request.submit_request
                              (application      => 'AR',
                               program          => 'RAXMTR',
                               description      => 'Autoinvoice Master Program',
                               start_time       => TO_CHAR
                                                        (SYSDATE,
                                                         'DD/MM/YYYY HH:MI:SS'
                                                        ),
                               sub_request      => FALSE,
                               argument1        => '1',
                               argument2        => sel_org_1.org_id,
                               argument3        => v_batch_source_id,
                               argument4        => 'ReSA_IS',
                               argument5        => TO_CHAR
                                                      (sel_org_1.gl_date,
                                                       'yyyy/mm/dd hh24:mi:ss'
                                                      ),
                               argument6        => '',
                               argument7        => '',
                               argument8        => '',
                               argument9        => '',
                               argument10       => '',
                               argument11       => '',
                               argument12       => '',
                               argument13       => '',
                               argument14       => '',
                               argument15       => '',
                               argument16       => '',
                               argument17       => '',
                               argument18       => '',
                               argument19       => '',
                               argument20       => '',
                               argument21       => '',
                               argument22       => '',
                               argument23       => '',
                               argument24       => '',
                               argument25       => '',
                               argument26       => 'Y',
                               argument27       => ''
                              );
                  COMMIT;
                  fnd_file.put_line
                               (fnd_file.output,
                                   ' Auotoinvoice Master Program Request id :'
                                || v_req_id
                               );
                  --- following procedure calls the wait for request api this will stop the control of loop till
                  -- above request got completed
                  xxabrl_wait_for_req_prc (v_req_id, 'Autoinvoice Program');
               END IF;
            END IF;

--**********************************************************************************
            IF sel_org_1.org_id = 91
            THEN
               fnd_global.apps_initialize (4191, 50540, 222);
               fnd_file.put_line
                  (fnd_file.LOG,
                   'step 1 :- Submitting ABRL Institutional Sales ReSA To AR Validate Program...'
                  );
               v_req_id :=
                  fnd_request.submit_request ('XXABRL',
                                              'XXABRL_INST_RESA_AR_INV_PKG',
                                              '',
                                              '',
                                              FALSE,
                                              'ReSA_IS',
                                              'N',
                                              sel_org_1.org_id
                                             );
               fnd_file.put_line
                  (fnd_file.LOG,
                      'ABRL Institutional Sales ReSA To AR Validate Program Request Id '
                   || v_req_id
                  );
               -- Wait for request for the invoice conversion program
               xxabrl_wait_for_req_prc
                       (v_req_id,
                        'ABRL Institutional Sales ReSA To AR Validate Program'
                       );
               fnd_file.put_line (fnd_file.LOG, 'starting status program');
               --- IF STATUS IS WARNING THEN STOP PROG
               --- RAISE stop_program;
               v_req_status :=
                  fnd_concurrent.get_request_status
                                                  (request_id      => v_req_id,
                                                   phase           => l_phase
                                                                             --OUT NOCOPY varchar2,
                  ,
                                                   status          => l_status
                                                                              --OUT NOCOPY varchar2,
                  ,
                                                   dev_phase       => l_dev_phase
                                                                                 --OUT NOCOPY varchar2,
                  ,
                                                   dev_status      => l_dev_status
                                                                                  --OUT NOCOPY varchar2,
                  ,
                                                   MESSAGE         => l_message
                                                  );    --OUT NOCOPY varchar2)
               COMMIT;
               fnd_file.put_line (fnd_file.LOG, 'l_phase' || l_phase);
               fnd_file.put_line (fnd_file.LOG, 'l_status' || l_status);
               fnd_file.put_line (fnd_file.LOG, 'l_dev_phase' || l_dev_phase);
               fnd_file.put_line (fnd_file.LOG,
                                  'l_dev_status' || l_dev_status);
               fnd_file.put_line (fnd_file.LOG, 'l_message' || l_message);

               IF l_status = 'Warning'
               THEN
                  -- RAISE stop_program;
                  fnd_file.put_line
                     (fnd_file.LOG,
                      'Error Occured in ABRL Institutional Sales ReSA To AR Validate Program'
                     );
                  EXIT;
               END IF;

               IF l_status = 'Error'
               THEN
                  --   RAISE stop_program;
                  fnd_file.put_line
                     (fnd_file.LOG,
                      'Error Occured in ABRL Institutional Sales ReSA To AR Validate Program'
                     );
                  EXIT;
               END IF;

               ----- submitting autoinvoice program

               --*****************************
               fnd_file.put_line (fnd_file.LOG,
                                  'step 2 :- Submitting Autoinvoice Master...'
                                 );

               BEGIN
                  SELECT batch_source_id
                    INTO v_batch_source_id
                    FROM ra_batch_sources_all
                   WHERE batch_source_type = 'FOREIGN'
                     AND status = 'A'
                     AND NAME = 'ReSA_IS'
                     AND org_id = sel_org_1.org_id;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     v_errmsg := 'ReSA batch source not found';
                  WHEN TOO_MANY_ROWS
                  THEN
                     v_errmsg := 'Multiple ReSa batch source found';
                  WHEN OTHERS
                  THEN
                     v_errmsg := 'Exception in ReSA batch source ';
               END;

               fnd_file.put_line (fnd_file.LOG, v_errmsg);

               IF v_errmsg IS NULL
               THEN
                  v_req_id :=
                     fnd_request.submit_request
                              (application      => 'AR',
                               program          => 'RAXMTR',
                               description      => 'Autoinvoice Master Program',
                               start_time       => TO_CHAR
                                                        (SYSDATE,
                                                         'DD/MM/YYYY HH:MI:SS'
                                                        ),
                               sub_request      => FALSE,
                               argument1        => '1',
                               argument2        => sel_org_1.org_id,
                               argument3        => v_batch_source_id,
                               argument4        => 'ReSA_IS',
                               argument5        => TO_CHAR
                                                      (sel_org_1.gl_date,
                                                       'yyyy/mm/dd hh24:mi:ss'
                                                      ),
                               argument6        => '',
                               argument7        => '',
                               argument8        => '',
                               argument9        => '',
                               argument10       => '',
                               argument11       => '',
                               argument12       => '',
                               argument13       => '',
                               argument14       => '',
                               argument15       => '',
                               argument16       => '',
                               argument17       => '',
                               argument18       => '',
                               argument19       => '',
                               argument20       => '',
                               argument21       => '',
                               argument22       => '',
                               argument23       => '',
                               argument24       => '',
                               argument25       => '',
                               argument26       => 'Y',
                               argument27       => ''
                              );
                  COMMIT;
                  fnd_file.put_line
                               (fnd_file.output,
                                   ' Auotoinvoice Master Program Request id :'
                                || v_req_id
                               );
                  --- following procedure calls the wait for request api this will stop the control of loop till
                  -- above request got completed
                  xxabrl_wait_for_req_prc (v_req_id, 'Autoinvoice Program');
               END IF;
            END IF;
         END LOOP;
      END;
   END xxabrl_is_resa_ar_dt_load_prc;

   PROCEDURE xxabrl_resa_ar_bef_freezing (
      errbuf    OUT   VARCHAR2,
      retcode   OUT   NUMBER
   )
   IS
      CURSOR cur_resa_ar
      IS
         SELECT DISTINCT ril.org_id
                    FROM apps.ra_interface_lines_all ril,
                         apps.ra_cust_trx_types_all rcta
                   WHERE TRUNC (gl_date) >= '1-jan-18'
                     AND (    ril.org_id IS NOT NULL
                          AND ril.org_id NOT IN
                                    (901, 801, 961, 861, 84, 87, 89, 91, 161)
                         )
                     AND ril.cust_trx_type_id = rcta.cust_trx_type_id
                     AND UPPER (rcta.NAME) IN
                                    ('STORE PROMOTION', 'STORE SALE INVOICE');

      v_errmsg             VARCHAR2 (500);
      v_batch_source_id    NUMBER;
      v_user_id            NUMBER          := fnd_profile.VALUE ('USER_ID');
      v_resp_id            NUMBER          := fnd_profile.VALUE ('RESP_ID');
      v_appl_id            NUMBER        := fnd_profile.VALUE ('RESP_APPL_ID');
      v_req_id             NUMBER;
      v_org_name           VARCHAR2 (100);
      v_org_id             NUMBER;
      stop_program         EXCEPTION;
      l_phase              VARCHAR2 (100);
      l_status             VARCHAR2 (100);
      l_dev_phase          VARCHAR2 (100);
      l_dev_status         VARCHAR2 (100);
      l_message            VARCHAR2 (1000);
      v_req_status         BOOLEAN;
      xx_interface_count   NUMBER          := 0;
   BEGIN
      FOR i IN cur_resa_ar
      LOOP
         BEGIN
            mo_global.set_policy_context ('S', i.org_id);
         END;

         -- we will submit this req through all corp ou
         fnd_global.apps_initialize (user_id           => v_user_id,
                                     resp_id           => v_resp_id,
                                     resp_appl_id      => v_appl_id
                                    );

         --ReSA To AR Validation Program (Validation and Interface)
         BEGIN
            SELECT NAME
              INTO v_org_name
              FROM hr_operating_units
             WHERE organization_id = i.org_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               v_org_name := 'No Org_found for ' || i.org_id;
         END;

--         fnd_file.put_line
--                  (fnd_file.LOG,
--                   'step 1 :- ABRL ReSA AR- Tender Before Freezing Program...'
--                  );
--         v_req_id :=
--            fnd_request.submit_request ('XXABRL',
--                                        'XXABRL_RESA_AR_TND_BEF_FRZ',
--
--                                        ---'XXABRL_RESA_AR_INV_IMP_PKG',
--                                        '',
--                                        '',
--                                        FALSE,
--                                        'ReSA',
--                                        'N',
--                                        i.org_id
--                                       );
--         fnd_file.put_line
--                (fnd_file.LOG,
--                    'ABRL ReSA AR- Tender Before Freezing Program Request Id '
--                 || v_req_id
--                );
--         -- Wait for request for the invoice conversion program
--         xxabrl_wait_for_req_prc
--                               (v_req_id,
--                                'ABRL ReSA AR- Tender Before Freezing Program'
--                               );
--         fnd_file.put_line (fnd_file.LOG, 'starting status program');
--         --- IF STATUS IS WARNING THEN STOP PROG
--         --- RAISE stop_program;
--         v_req_status :=
--            fnd_concurrent.get_request_status (request_id      => v_req_id,
--                                               phase           => l_phase
--                                                                         --OUT NOCOPY varchar2,
--            ,
--                                               status          => l_status
--                                                                          --OUT NOCOPY varchar2,
--            ,
--                                               dev_phase       => l_dev_phase
--                                                                             --OUT NOCOPY varchar2,
--            ,
--                                               dev_status      => l_dev_status
--                                                                              --OUT NOCOPY varchar2,
--            ,
--                                               MESSAGE         => l_message
--                                              );        --OUT NOCOPY varchar2)
--         COMMIT;
--         fnd_file.put_line (fnd_file.LOG, 'l_phase' || l_phase);
--         fnd_file.put_line (fnd_file.LOG, 'l_status' || l_status);
--         fnd_file.put_line (fnd_file.LOG, 'l_dev_phase' || l_dev_phase);
--         fnd_file.put_line (fnd_file.LOG, 'l_dev_status' || l_dev_status);
--         fnd_file.put_line (fnd_file.LOG, 'l_message' || l_message);

         --         IF l_status = 'Warning'
--         THEN
--            -- RAISE stop_program;
--            fnd_file.put_line (fnd_file.LOG,
--                               'Error Occured in Invoce Conversion Program'
--                              );
--            EXIT;
--         END IF;

         --         IF l_status = 'Error'
--         THEN
--            --   RAISE stop_program;
--            fnd_file.put_line (fnd_file.LOG,
--                               'Error Occured in Invoce Conversion Program'
--                              );
--            EXIT;
--         END IF;

         ----- submitting autoinvoice program
         fnd_file.put_line (fnd_file.LOG,
                            'step 2 :- Submitting Autoinvoice Master...'
                           );

         BEGIN
            SELECT batch_source_id
              INTO v_batch_source_id
              FROM ra_batch_sources_all
             WHERE batch_source_type = 'FOREIGN'
               AND status = 'A'
               AND NAME = 'ReSA'
               AND org_id = i.org_id;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               v_errmsg := 'ReSA batch source not found';
            WHEN TOO_MANY_ROWS
            THEN
               v_errmsg := 'Multiple ReSa batch source found';
            WHEN OTHERS
            THEN
               v_errmsg := 'Exception in ReSA batch source ';
         END;

         fnd_file.put_line (fnd_file.LOG, v_errmsg);
         v_req_id :=
            fnd_request.submit_request
                              (application      => 'AR',
                               program          => 'RAXMTR',
                               description      => 'Autoinvoice Master Program',
                               start_time       => TO_CHAR
                                                      (SYSDATE,
                                                       'DD/MM/YYYY HH24:MI:SS'
                                                      ),
                               sub_request      => FALSE,
                               argument1        => '1',
                               argument2        => i.org_id,
                               argument3        => v_batch_source_id,
                               argument4        => 'ReSA',
                               argument5        => TO_CHAR
                                                      (SYSDATE,
                                                       'DD/MM/YYYY HH24:MI:SS'
                                                      ),
                               argument6        => '',
                               argument7        => '',
                               argument8        => '',
                               argument9        => '',
                               argument10       => '',
                               argument11       => '',
                               argument12       => '',
                               argument13       => '',
                               argument14       => '',
                               argument15       => '',
                               argument16       => '',
                               argument17       => '',
                               argument18       => '',
                               argument19       => '',
                               argument20       => '',
                               argument21       => '',
                               argument22       => '',
                               argument23       => '',
                               argument24       => '',
                               argument25       => '',
                               argument26       => 'Y',
                               argument27       => ''
                              );
         COMMIT;
         fnd_file.put_line (fnd_file.output,
                               ' Auotoinvoice Master Program Request id :'
                            || v_req_id
                           );
         --- following procedure calls the wait for request api this will stop the control of loop till
         -- above request got completed
         xxabrl_wait_for_req_prc (v_req_id, 'Autoinvoice Program');
--         END IF;
--*****************************
      END LOOP;
   -- code for submitting the receipt data
   END;
END gst_abrl_nav_ar_bkdt_load_pkg; 
/


CREATE OR REPLACE PACKAGE BODY APPS.xxabrl_baan_to_ar_tender
IS
   PROCEDURE main_proc (x_err_buf OUT VARCHAR2, x_ret_code OUT NUMBER)
   IS
      xx_total_tender_amt      NUMBER          := 0;
      xx_total_sales_amt       NUMBER          := 0;
      diff_amt                 NUMBER          := 0;
      xx_sales_ret_amt         NUMBER;
      final_amt                NUMBER          := 0;
      v_insert_exception       EXCEPTION;
      xx_ou_short_code         VARCHAR2 (100);
      xx_customer_code         VARCHAR2 (100);
      error_msg                VARCHAR2 (3000);
      --   v_ofin_receipt_method    VARCHAR2 (1000);
      v_receipt_amt            NUMBER;
      v_tot_cc_mtd             NUMBER;
      exit_count               NUMBER          := 0;
      v_receipt_dev            NUMBER          := 0;
      v_ofin_receipt_method1   VARCHAR2 (1000);
      v_ofin_bank_name         VARCHAR2 (2000);
      v_ofin_bank_code         VARCHAR2 (2000);

      CURSOR xx_main
      IS
         SELECT   btd_org_cd, btd_bill_dt
             FROM xxabrl.xxabrl_baan_ar_tender a,
                  xxabrl.xxabrl_baan_ar_tender_types b
            WHERE NVL (status_flag, 'N') IN
                                          ('N', 'E') --AND btd_org_cd <> '007'
              --    and BTD_BILL_DT ='31-Dec-15'
             -- AND btd_org_cd = '009'
              -- AND btd_bill_dt = '03-Jan-16'
              AND a.btd_tender_type_cd = b.ttm_code
              AND b.receipts = 'Y'
         GROUP BY btd_org_cd, btd_bill_dt;

      CURSOR xx_tender (p_store_no VARCHAR2, p_store_day DATE)
      IS
         SELECT   a.btd_org_cd, a.btd_bill_dt, a.btd_tender_type_cd,
                  SUM (a.btd_conv_amt) btd_conv_amt
             FROM xxabrl.xxabrl_baan_ar_tender a
            WHERE 1 = 1
              AND a.btd_org_cd = p_store_no
              AND a.btd_bill_dt = p_store_day
              AND btd_tender_type_cd IN ('CA', 'AMX')
         GROUP BY a.btd_org_cd, a.btd_bill_dt, a.btd_tender_type_cd;

      CURSOR xx_tender1 (p_store_no VARCHAR2, p_store_day DATE)
      IS
         SELECT   a.btd_org_cd, a.btd_bill_dt,
                  SUM (a.btd_conv_amt) btd_conv_amt
             FROM xxabrl.xxabrl_baan_ar_tender a
            WHERE 1 = 1
              AND a.btd_org_cd = p_store_no
              AND a.btd_bill_dt = p_store_day
              AND btd_tender_type_cd IN ('CC', 'DC')
         GROUP BY a.btd_org_cd, a.btd_bill_dt;
   BEGIN
      FOR xx_store_days IN xx_main
      LOOP
         xx_total_tender_amt := 0;
         xx_total_sales_amt := 0;
         diff_amt := 0;
         xx_sales_ret_amt := 0;
         final_amt := 0;

         BEGIN
            SELECT NVL (SUM (btd_conv_amt), 0)
              INTO xx_total_tender_amt
              FROM xxabrl.xxabrl_baan_ar_tender a
             WHERE a.btd_org_cd = xx_store_days.btd_org_cd
               AND a.btd_bill_dt = xx_store_days.btd_bill_dt;
         EXCEPTION
            WHEN OTHERS
            THEN
               error_msg :=
                     error_msg
                  || 'Error in calculating Tender Amount :=>'
                  || SQLCODE
                  || ':'
                  || SQLERRM;
               RAISE v_insert_exception;
         END;

         /* BEGIN
             SELECT NVL (SUM (tender_amount), 0)
               INTO xx_total_sales_amt
               FROM apps.xxabrl_navi_ar_int_line_stg2 a,
                    xxabrl.xxabrl_baan_location b
              WHERE 1 = 1
                AND a.customer_name = b.customer_code
                AND b.baan_store_code = xx_store_days.btd_org_cd
                AND a.gl_date = xx_store_days.btd_bill_dt;
          EXCEPTION
             WHEN OTHERS
             THEN
                error_msg :=
                      error_msg
                   || 'Error in calculating Sales Amount :=>'
                   || SQLCODE
                   || ':'
                   || SQLERRM;
                RAISE v_insert_exception;
          END;*/
         BEGIN
            SELECT ROUND (SUM (net_value))
              INTO xx_total_sales_amt
              FROM xxabrl.xxabrl_baan_sales_details
             WHERE org_cd = xx_store_days.btd_org_cd
               AND bill_dt = xx_store_days.btd_bill_dt;
         EXCEPTION
            WHEN OTHERS
            THEN
               error_msg :=
                     error_msg
                  || 'Error in calculating Tender Amount :=>'
                  || SQLCODE
                  || ':'
                  || SQLERRM;
               RAISE v_insert_exception;
         END;

         diff_amt :=
              NVL (ROUND (xx_total_sales_amt) - ROUND (xx_total_tender_amt),
                   0);
         fnd_file.put_line (fnd_file.LOG, 'diff_amt=>' || diff_amt);

         IF NVL (diff_amt, 0) <> 0
         THEN
            NULL;
--            BEGIN
--               SELECT NVL (SUM (net_value), 0)
--                 INTO xx_sales_ret_amt
--                 FROM xxabrl.xxabrl_baan_sales_return_det
--                WHERE org_cd = xx_store_days.btd_org_cd
--                  AND ret_doc_dt = xx_store_days.btd_bill_dt;
--            EXCEPTION
--               WHEN OTHERS
--               THEN
--                  NULL;
--            END;
         END IF;

         fnd_file.put_line (fnd_file.LOG,
                            'xx_sales_ret_amt=>' || xx_sales_ret_amt
                           );

         IF NVL (diff_amt, 0) < 0
         THEN
            final_amt := NVL (ROUND (diff_amt + xx_sales_ret_amt), 0);
            fnd_file.put_line (fnd_file.LOG, 'final_amt=>' || final_amt);
         ELSE
            final_amt := NVL (ROUND (diff_amt - xx_sales_ret_amt), 0);
            fnd_file.put_line (fnd_file.LOG, 'final_amt=>' || final_amt);
         END IF;

--         IF     (ROUND (xx_total_tender_amt) - ROUND (xx_total_sales_amt))
--                   BETWEEN -1
--                       AND 1
--            AND final_amt BETWEEN -1 AND 1
--         THEN
         BEGIN
            FOR xx_rec IN xx_tender (xx_store_days.btd_org_cd,
                                     xx_store_days.btd_bill_dt
                                    )
            LOOP
               -- v_ofin_receipt_method := NULL;
               BEGIN
                  SELECT ou_short_code, customer_code
                    INTO xx_ou_short_code, xx_customer_code
                    FROM xxabrl.xxabrl_baan_location
                   WHERE baan_store_code = xx_rec.btd_org_cd;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     NULL;
               END;

--            BEGIN
--               SELECT DISTINCT ofin_receipt_method
--                          INTO v_ofin_receipt_method1
--                          FROM xxabrl.xxabrl_baan_ar_tender_types
--                         WHERE ttm_code = xx_rec.btd_tender_type_cd;
--            END;
               BEGIN
                  SELECT ofin_receipt_method, ofin_bank_name,
                         ofin_bank_code
                    INTO v_ofin_receipt_method1, v_ofin_bank_name,
                         v_ofin_bank_code
                    FROM xxabrl.xxabrl_baan_ar_tender_types
                   WHERE ttm_code = xx_rec.btd_tender_type_cd;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     error_msg :=
                           error_msg
                        || 'Error to find the receipt method:=>'
                        || SQLCODE
                        || ':'
                        || SQLERRM;
                     RAISE v_insert_exception;
               END;

               fnd_file.put_line (fnd_file.LOG,
                                     'receipt_no=>'
                                  || SUBSTR (   TO_CHAR (xx_rec.btd_bill_dt,
                                                         'ddmmyyyy'
                                                        )
                                             || '-'
                                             || xx_rec.btd_org_cd
                                             || '-'
                                             || v_ofin_receipt_method1,
                                             1,
                                             33
                                            )
                                 );

               BEGIN
                  INSERT INTO apps.xxabrl_navi_ar_receipt_stg2
                              (data_source,
                               receipt_number,
                               operating_unit, receipt_date,
                               deposit_date, gl_date,
                               currency_code, exchange_rate_type,
                               exchange_rate, exchange_date, tender_amount,
                               receipt_amount,
                               er_customer_number,   comments,
                               er_tender_type,
                               sales_transaction_number,
                               of_customer_number, customer_id,
                               customer_site_id, receipt_method,
                               bank_account_name, bank_account_number,
                               invoice_number, amount_applied, org_name,
                               created_by, creation_date, interfaced_flag,
                               freeze_flag, error_message,er_dc_code 
                              )
                       VALUES ('ABRL_BAAN',
                               REGEXP_REPLACE
                                     (SUBSTR (   TO_CHAR (xx_rec.btd_bill_dt,
                                                          'ddmmyyyy'
                                                         )
                                              || '-'
                                              || xx_rec.btd_org_cd
                                              || '-'
                                              || v_ofin_receipt_method1,
                                              1,
                                              30
                                             ),
                                      '[[:space:]]*',
                                      ''
                                     ),
                               xx_ou_short_code, xx_rec.btd_bill_dt,
                               xx_rec.btd_bill_dt, xx_rec.btd_bill_dt,
                               'INR', NULL,
                               NULL, NULL, ROUND (xx_rec.btd_conv_amt, 2),
                               ROUND (xx_rec.btd_conv_amt, 2),
                               xx_customer_code,   'COMMENTS',
                               v_ofin_receipt_method1,
                                  TO_CHAR (xx_rec.btd_bill_dt, 'ddmmyyyy')
                               || '-'
                               || xx_rec.btd_org_cd,
                               xx_customer_code, NULL,
                               NULL, v_ofin_receipt_method1,
                               v_ofin_bank_name, v_ofin_bank_code,
                               NULL, 0, xx_ou_short_code,
                               NULL, SYSDATE, 'N',
                               'N', NULL,xx_ou_short_code
                              );
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     error_msg :=
                           error_msg
                        || 'Error in inserting(Cash Collection) receipts :=>'
                        || SQLCODE
                        || ':'
                        || SQLERRM;
                     RAISE v_insert_exception;
               END;

               UPDATE xxabrl.xxabrl_baan_ar_tender
                  SET status_flag = 'R'
                -- error_msg = 'round-off=>' || final_amt
               WHERE  btd_tender_type_cd = xx_rec.btd_tender_type_cd
                  AND btd_org_cd = xx_rec.btd_org_cd
                  AND btd_bill_dt = xx_rec.btd_bill_dt;
            END LOOP;
         EXCEPTION
            WHEN OTHERS
            THEN
               error_msg :=
                     error_msg
                  || 'Error in inserting(Cash Collection) receipts :=>'
                  || SQLCODE
                  || ':'
                  || SQLERRM;
               RAISE v_insert_exception;
         END;

         BEGIN
            FOR xx_rec IN xx_tender1 (xx_store_days.btd_org_cd,
                                      xx_store_days.btd_bill_dt
                                     )
            LOOP
               --v_ofin_receipt_method := NULL;
               BEGIN
                  SELECT ou_short_code, customer_code
                    INTO xx_ou_short_code, xx_customer_code
                    FROM xxabrl.xxabrl_baan_location
                   WHERE baan_store_code = xx_rec.btd_org_cd;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     NULL;
               END;

               v_receipt_amt := 0;
               exit_count := 0;
               v_receipt_dev := 0;
               v_tot_cc_mtd := 0;

               FOR xx_rec_md IN (SELECT ofin_receipt_method, ofin_bank_name,
                                        ofin_bank_code
                                   FROM xxabrl.xxabrl_baan_ar_tender_types
                                  WHERE ttm_code = 'CC')
               LOOP
                  BEGIN
                     SELECT COUNT (1)
                       INTO v_tot_cc_mtd
                       FROM xxabrl.xxabrl_baan_ar_tender_types
                      WHERE ttm_code = 'CC';
                  END;

                  fnd_file.put_line (fnd_file.LOG,
                                     'v_tot_cc_mtd=>' || v_tot_cc_mtd
                                    );
                  v_receipt_amt := xx_rec.btd_conv_amt / v_tot_cc_mtd;
                  fnd_file.put_line (fnd_file.LOG,
                                     'v_receipt_amt=>' || v_receipt_amt
                                    );
                  v_receipt_dev := v_receipt_amt + v_receipt_dev;
                  fnd_file.put_line (fnd_file.LOG,
                                     'v_receipt_dev=>' || v_receipt_dev
                                    );

                  IF exit_count = v_tot_cc_mtd
                  THEN
                     v_receipt_amt := xx_rec.btd_conv_amt - v_receipt_dev;
                  END IF;

                  fnd_file.put_line (fnd_file.LOG,
                                        'receipt_no1=>'
                                     || xx_rec.btd_bill_dt
                                     || '-'
                                     || xx_rec.btd_org_cd
                                     || '-'
                                     || xx_rec_md.ofin_bank_name
                                    );

                  BEGIN
                     INSERT INTO apps.xxabrl_navi_ar_receipt_stg2
                                 (data_source,
                                  receipt_number,
                                  operating_unit, receipt_date,
                                  deposit_date, gl_date,
                                  currency_code, exchange_rate_type,
                                  exchange_rate, exchange_date,
                                  tender_amount,
                                  receipt_amount,
                                  er_customer_number, er_dc_code, comments,
                                  er_tender_type,
                                  sales_transaction_number,
                                  of_customer_number, customer_id,
                                  customer_site_id, receipt_method,
                                  bank_account_name,
                                  bank_account_number, invoice_number,
                                  amount_applied, org_name, created_by,
                                  creation_date, interfaced_flag,
                                  freeze_flag, error_message
                                 )
                          VALUES ('ABRL_BAAN',
                                   REGEXP_REPLACE
                                     (SUBSTR (   TO_CHAR (xx_rec.btd_bill_dt,
                                                      'ddmmyyyy'
                                                     )
                                          || '-'
                                          || xx_rec.btd_org_cd
                                          || '-'
--                                         || SUBSTR
--                                               (xx_rec_md.ofin_receipt_method,
--                                                1,
--                                                5
--                                               )
--                                         || '-'
                                          || xx_rec_md.ofin_bank_name,
                                          1,
                                          30
                                         ),
                                      '[[:space:]]*',
                                      ''
                                     ),
                                  xx_ou_short_code, xx_rec.btd_bill_dt,
                                  xx_rec.btd_bill_dt, xx_rec.btd_bill_dt,
                                  'INR', NULL,
                                  NULL, NULL,
                                  ROUND (v_receipt_amt, 2),
                                  ROUND (v_receipt_amt, 2),
                                  xx_customer_code, xx_ou_short_code, 'COMMENTS',
                                  xx_rec_md.ofin_receipt_method,
                                     TO_CHAR (xx_rec.btd_bill_dt, 'ddmmyyyy')
                                  || '-'
                                  || xx_rec.btd_org_cd,
                                  xx_customer_code, NULL,
                                  NULL, xx_rec_md.ofin_receipt_method,
                                  xx_rec_md.ofin_bank_name,
                                  xx_rec_md.ofin_bank_code, NULL,
                                  0, xx_ou_short_code, NULL,
                                  SYSDATE, 'N',
                                  'N', NULL
                                 );
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        error_msg :=
                              error_msg
                           || 'Error in inserting(Credit card/Debit Card) receipts :=>'
                           || SQLCODE
                           || ':'
                           || SQLERRM;
                        RAISE v_insert_exception;
                  END;

                  exit_count := exit_count + 1;
                  fnd_file.put_line (fnd_file.LOG,
                                     ' exit_count=>' || exit_count
                                    );
               END LOOP;

               UPDATE xxabrl.xxabrl_baan_ar_tender
                  SET status_flag = 'R'
                -- error_msg = 'round-off=>' || final_amt
               WHERE  btd_tender_type_cd IN ('DC', 'CC')
                  AND btd_org_cd = xx_rec.btd_org_cd
                  AND btd_bill_dt = xx_rec.btd_bill_dt;
            END LOOP;
         EXCEPTION
            WHEN OTHERS
            THEN
               error_msg :=
                     error_msg
                  || 'Error in inserting(Cash Collection) receipts :=>'
                  || SQLCODE
                  || ':'
                  || SQLERRM;
               RAISE v_insert_exception;
         END;
      END LOOP;

      COMMIT;
   EXCEPTION
      WHEN v_insert_exception
      THEN
         ROLLBACK;
         x_err_buf := error_msg;
         fnd_file.put_line (fnd_file.LOG, 'x_err_buf=>' || x_err_buf);
         x_ret_code := 2;
      WHEN OTHERS
      THEN
         x_err_buf := SQLCODE || ':' || SQLERRM;
         x_ret_code := 2;
   END;
END xxabrl_baan_to_ar_tender; 
/


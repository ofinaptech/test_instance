CREATE OR REPLACE PACKAGE BODY APPS.xxabrl_baan_ar_tndr_inv_pkg
IS
   PROCEDURE invoice_validate (
      errbuf           OUT      VARCHAR2,
      retcode          OUT      NUMBER,
      /*P_Org_Id       In NUMBER,*/
      p_batch_source   IN       VARCHAR2,
      p_action         IN       VARCHAR2,
      p_org_id         IN       NUMBER
   )
   IS
      --
      -- Declaring cursor 1
      --
      CURSOR art_c1_validate (cp_org_code VARCHAR2, cp_batch_source VARCHAR2)
      IS
         SELECT   batch_source_name, org_code, interface_line_context,
                  interface_line_attribute1 interface_line_attribute1,
                  nv_customer_id, nv_cust_site_id
             FROM xxabrl_navi_ar_int_line_stg2
            WHERE 1 = 1
              AND TRUNC (gl_date) >= '01-Nov-15'
              --AND customer_name = '1503185'
              AND UPPER (interface_line_context) = 'ABRL_BAAN'
              AND NVL (org_code, cp_org_code) = cp_org_code
              AND NVL (interfaced_flag, 'N') IN ('N', 'E')
              AND freeze_flag = 'Y'
         GROUP BY batch_source_name,
                  org_code,
                  interface_line_context,
                  interface_line_attribute1,
                  nv_customer_id,
                  nv_cust_site_id;

-- AND NVL(BATCH_SOURCE_NAME, CP_Batch_Source) = CP_Batch_Source
-- AND CUSTOMER_NAME = 6501391---:CONTROL_BLK.CUSTOMER_NAME
-- AND GL_DATE = '06-JAN-2010'
      CURSOR art_c2 (
         cp_batch_source_name           VARCHAR2,
         cp_org_code                    VARCHAR2,
         cp_interface_line_attribute1   VARCHAR2,
         cp_nv_customer_number          VARCHAR2,
         cp_nv_cust_site_id             VARCHAR2
      )
      IS
         SELECT   ROWID, batch_source_name, customer_name, org_code,
                  interface_line_context,
                  interface_line_attribute1 interface_line_attribute1,
                  interface_line_attribute2 interface_line_attribute2,
                  line_type, trx_date, gl_date, amount, currency_code,
                  conversion_type, conversion_rate, conversion_date,
                  transaction_type, description, term_id, nv_customer_id,
                  nv_cust_site_id, attribute1, attribute_category,
                  NVL (actual_amount, amount) actual_amount, deposited_amount,
                  deposited_date, opening_balance, closing_balance,
                  net_change_at_store
             FROM xxabrl_navi_ar_int_line_stg2
            WHERE TRUNC (gl_date) > '31-MAR-15'
              AND NVL (org_code, cp_org_code) = cp_org_code
              AND UPPER (interface_line_context) = 'ABRL_BAAN'
              AND interface_line_attribute1 = cp_interface_line_attribute1
              AND NVL (interfaced_flag, 'N') IN ('N', 'E')
              AND freeze_flag = 'Y'
         ORDER BY interface_line_attribute1, interface_line_attribute2;

-- NVL(BATCH_SOURCE_NAME, CP_BATCH_SOURCE_NAME) = CP_BATCH_SOURCE_NAME
-- AND NVL(NV_CUSTOMER_ID, CP_NV_CUSTOMER_NUMBER) = CP_NV_CUSTOMER_NUMBER
-- AND NVL(NV_CUST_SITE_ID, CP_NV_CUST_SITE_ID) = CP_NV_CUST_SITE_ID\
      CURSOR art_c3 (
         cp_batch_source_name           VARCHAR2,
         cp_org_code                    VARCHAR2,
         cp_interface_line_attribute1   VARCHAR2,
         cp_interface_line_attribute2   VARCHAR2,
         cp_nv_customer_number          VARCHAR2,
         cp_nv_cust_site_id             VARCHAR2
      )
      IS
         SELECT ROWID,
-- BATCH_SOURCE_NAME,
                      org_id, interface_line_context,
                interface_line_attribute1 interface_line_attribute1,
                interface_line_attribute2 interface_line_attribute2,
                account_class, amount, segment1 co, segment2 cc,
                segment3 state_sbu, segment4 LOCATION, segment5 merchandise,
                segment6 ACCOUNT, segment7 intercompany, segment8 future,
                record_number
           FROM xxabrl.xxabrl_navi_ra_int_dist_stg2
          WHERE interface_line_attribute1 = cp_interface_line_attribute1
            AND interface_line_attribute2 = cp_interface_line_attribute2;

-- NVL(BATCH_SOURCE_NAME, CP_BATCH_SOURCE_NAME)         = CP_BATCH_SOURCE_NAME
-- AND NVL(ORG_ID, CP_ORG_CODE)                             = CP_ORG_CODE
-- And NVL(NV_CUSTOMER_NUMBER, CP_NV_CUSTOMER_NUMBER) = CP_NV_CUSTOMER_NUMBER
-- And NVL(NV_CUST_SITE_ID, CP_NV_CUST_SITE_ID)  = CP_NV_CUST_SITE_ID*/
-- And NVL(INTERFACED_FLAG, 'N') in ('N', 'E')

      -- v_Orgid               Number := Fnd_Profile.VALUE('ORG_ID');
      v_set_of_bks_id         NUMBER := fnd_profile.VALUE ('GL_SET_OF_BKS_ID');
      v_user_id               NUMBER          := fnd_profile.VALUE ('USER_ID');
      org_code                hr_operating_units.short_code%TYPE;
      v_batch_source          ra_batch_sources_all.NAME%TYPE;
      v_fun_curr              VARCHAR2 (10);
      v_error_count           NUMBER                               := 0;
      v_ok_rec_count          NUMBER                               := 0;
      v_error_hmsg            VARCHAR2 (1000);
      v_error_lmsg            VARCHAR2 (1000);
      v_record_count          NUMBER                               := 0;
      v_data_count            NUMBER                               := 0;
      v_line_count            NUMBER                               := 0;
      v_currency              fnd_currencies.currency_code%TYPE;
      v_tax_name              VARCHAR2 (50);
      v_cc_id                 NUMBER;
      v_state_sbu             gl_code_combinations.segment3%TYPE;
      v_code_combination_id   NUMBER;
      v_cust_trx_type_id      NUMBER;
      v_gl_id_rev             NUMBER;
      v_gl_id_rec             NUMBER;
      v_cust_account_id       NUMBER;
      v_cust_site_id          NUMBER;
      --V_TERM_ID      RA_TERMS.Name%Type;
      v_term_id               NUMBER;
      v_uom_name              VARCHAR2 (20);
      v_description           VARCHAR2 (240);
      v_seg_status            VARCHAR2 (100);
      -- P_Org_Id number;
       --P_BATCH_Source varchar2(50);
      v_cust_acct_site_id     NUMBER;
      vn_structure_id         NUMBER;
      p_gl_id_rev             VARCHAR2 (300);
      x_gl_rev_acct_id        NUMBER;
      v_conc_segments         VARCHAR2 (250);
      v_trx_type              VARCHAR2 (15);
      v_attribute_category    VARCHAR2 (240);
   BEGIN
      BEGIN
         --P_Org_Id := Fnd_Profile.VALUE('ORG_ID');
         --P_BATCH_Source := 'ABRL NAVISION TO AR';
         SELECT short_code
           INTO org_code
           FROM hr_operating_units
          WHERE organization_id = p_org_id;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            fnd_file.put_line
                       (fnd_file.output,
                        'Selected Org Id does not exist in Oracle Financials'
                       );
            fnd_file.put_line
               (fnd_file.output,
                '........................................................................'
               );
         WHEN TOO_MANY_ROWS
         THEN
            fnd_file.put_line (fnd_file.output,
                               'Multiple Org Id exist in Oracle Financials'
                              );
            fnd_file.put_line
               (fnd_file.output,
                '........................................................................'
               );
         WHEN OTHERS
         THEN
            fnd_file.put_line (fnd_file.output, 'Invalid Org Id Selected ');
            fnd_file.put_line
               (fnd_file.output,
                '........................................................................'
               );
      END;

/*        30.06.2014

   UPDATE  XXABRL_NAVI_AR_INT_LINE_STG2
       Set CREATED_BY = v_user_id,
       BATCH_SOURCE_NAME = BATCH_SOURCE_NAME,
       TRANSACTION_TYPE = initcap(TRANSACTION_TYPE)
       WHERE UPPER(INTERFACE_LINE_CONTEXT)  ='ABRL_RESA';

    UPDATE XXABRL_NAVI_AR_INT_LINE_STG2
       Set CREATED_BY = v_user_id
     Where Trim(Upper(BATCH_SOURCE_NAME)) = Trim(Upper(P_BATCH_Source))
       And Trim(Upper(ORG_CODE)) = Trim(Upper(ORG_CODE))
       And NVL(INTERFACED_FLAG, 'N') = 'N'
       AND FREEZE_FLAG='Y'
       AND UPPER(INTERFACE_LINE_CONTEXT)  ='ABRL_RESA'
       And CREATED_BY is Null;

    UPDATE XXABRL.XXABRL_NAVI_RA_INT_DIST_STG2
       Set CREATED_BY = v_user_id
     Where
     -- Trim(Upper(BATCH_SOURCE_NAME)) = Trim(Upper(P_BATCH_Source))
     Trim(Upper(ORG_CODE)) = Trim(Upper(ORG_CODE))
     And NVL(INTERFACED_FLAG, 'N') = 'N'
     And CREATED_BY is Null;
    Commit;
    --Deleting previous error message if any--

    UPDATE XXABRL_NAVI_AR_INT_LINE_STG2
       SET ERROR_MESSAGE = Null
     Where Trim(Upper(BATCH_SOURCE_NAME)) = Trim(Upper(P_BATCH_Source))
       And Trim(Upper(ORG_CODE)) = Trim(Upper(ORG_CODE))
       And NVL(INTERFACED_FLAG, 'N') in ('N', 'E')
       AND FREEZE_FLAG='Y'
       AND UPPER(INTERFACE_LINE_CONTEXT)  ='ABRL_RESA'
       And ERROR_MESSAGE is not null;

    UPDATE XXABRL.XXABRL_NAVI_RA_INT_DIST_STG2
       SET ERROR_MESSAGE = Null
     Where
     -- Trim(Upper(BATCH_SOURCE_NAME)) = Trim(Upper(P_BATCH_Source))
     Trim(Upper(ORG_CODE)) = Trim(Upper(ORG_CODE))
     And NVL(INTERFACED_FLAG, 'N') in ('N', 'E')
     And ERROR_MESSAGE is not null;
    Commit;

30.06.2014            */

      --        30.06.2014        DELETING PREVIOUS ERROR MESSAGE IF ANY
      UPDATE xxabrl_navi_ar_int_line_stg2
         SET error_message = NULL
       WHERE 1 = 1
         AND TRUNC (gl_date) > '31-MAR-15'
         AND UPPER (interface_line_context) = 'ABRL_BAAN'
         AND TRIM (UPPER (org_code)) = TRIM (UPPER (org_code))
         AND NVL (interfaced_flag, 'N') IN ('N', 'E')
         AND freeze_flag = 'Y';

-- AND ERROR_MESSAGE                             IS NOT NULL
      UPDATE xxabrl.xxabrl_navi_ra_int_dist_stg2
         SET error_message = NULL
       WHERE 1 = 1
         AND NVL (interfaced_flag, 'N') IN ('N', 'E')
         AND UPPER (interface_line_context) = 'ABRL_BAAN';

-- And    ERROR_MESSAGE             is not null;
      COMMIT;

--    ====================
--    Validation for Currency Code
--    ==========================
      BEGIN
         SELECT currency_code
           INTO v_fun_curr
           FROM gl_sets_of_books
          WHERE set_of_books_id = v_set_of_bks_id;

         fnd_file.put_line (fnd_file.output, 'GL SOB_ID :' || v_set_of_bks_id);
      EXCEPTION
         WHEN OTHERS
         THEN
            v_fun_curr := NULL;
            fnd_file.put_line (fnd_file.output, 'Exception in Currency Code');
      END;

    --************************************************************************************
    --**  Additional Loop to Update Customer Site-id and Tran-ID.                        *
    --**  After updating base table, updated data will be used in Re-Call of same loop   *
    --************************************************************************************
/**/
      fnd_file.put_line (fnd_file.output,
                         '-------------------------------------------------'
                        );
/**/
      fnd_file.put_line (fnd_file.output,
                         '***Running update Script for Cust-Site-ID / Tran-ID'
                        );

/**/
      FOR art_r1 IN art_c1_validate (org_code, p_batch_source)
      LOOP
/**/
         EXIT WHEN art_c1_validate%NOTFOUND;

/**/
/**/
         FOR art_r2 IN art_c2 (art_r1.batch_source_name,
/**/
                               art_r1.org_code,
/**/
                               art_r1.interface_line_attribute1,
/**/
                               art_r1.nv_customer_id,
/**/
                               art_r1.nv_cust_site_id
                              )
         LOOP
/**/
            EXIT WHEN art_c2%NOTFOUND;
/**/
/**/
            v_error_hmsg := NULL;
/**/
            v_data_count := NULL;
/**/
            v_cc_id := NULL;
/**/
            v_cust_account_id := NULL;
/**/
            v_cust_site_id := NULL;
/**/
            v_term_id := NULL;
/**/
            v_description := NULL;
/**/
/**/
            v_error_hmsg := NULL;

/**/
/**/        --#######################################
/**/        -- Validating / Updating Customer ID
/**/        --#######################################
/**/
            IF art_r2.customer_name IS NOT NULL
            THEN
/**/
               BEGIN
/**/
                  SELECT
/**/
                         cust_account_id
                    INTO v_cust_account_id
/**/
                  FROM
/**/
                         hz_cust_accounts hca
/**/            /*, hz_parties hp */
/**/
                  WHERE
/**/
                         hca.account_number = art_r2.customer_name
/**/            /*upper(hp.party_name) = upper(ART_R2.CUSTOMER_NAME)*/
/**/            /*and hp.party_id = hca.party_id*/
/**/
                  ;

/**/
/**/
                  BEGIN
/**/
                     UPDATE xxabrl_navi_ar_int_line_stg2 int_ln
/**/
                     SET int_ln.nv_customer_id = v_cust_account_id
/**/
                     WHERE  ROWID = art_r2.ROWID
                        AND UPPER (interface_line_context) = 'ABRL_BAAN';

/**/
                     BEGIN
/**/
                        SELECT hcas.cust_acct_site_id
                          INTO v_cust_acct_site_id
/**/
                        FROM   hz_cust_accounts_all hca,
/**/
                               hz_cust_acct_sites_all hcas,
/**/
                               hz_party_sites hps,
/**/
                               hz_cust_site_uses_all hcsu
/**/
                        WHERE  hca.cust_account_id = v_cust_account_id
/**/
                           AND hca.cust_account_id = hcas.cust_account_id
/**/
                           AND hcas.org_id = p_org_id
/**/
                           AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id(+)
/**/
                           AND hcas.party_site_id = hps.party_site_id
/**/        --              And hps.Party_Site_Number = xac.OF_Customer_Site_Code
/**/
                           AND hcsu.site_use_code = 'BILL_TO'
/**/
                           AND hcas.bill_to_flag = 'P'
/**/
                           AND hcas.status = 'A'
                           AND hcsu.status = 'A';

/**/
/**/
                        BEGIN
/**/
                           UPDATE xxabrl_navi_ar_int_line_stg2 int_ln
/**/
                           SET int_ln.nv_cust_site_id = v_cust_acct_site_id
/**/
                           WHERE  ROWID = art_r2.ROWID
                              AND UPPER (interface_line_context) = 'ABRL_BAAN';
/**/
/**/
                        EXCEPTION
/**/
                           WHEN OTHERS
                           THEN
/**/
                              fnd_file.put_line
                                 (fnd_file.output,
                                     'Exception while updating Customer Site '
                                  || art_r2.customer_name
                                 );
/**/
                        END;
/**/
/**/
/**/
                     EXCEPTION
/**/
                        WHEN NO_DATA_FOUND
                        THEN
/**/
                           v_error_hmsg :=
                                 v_error_hmsg
                              || 'Bill_TO Site not Found for '
                              || art_r2.customer_name
                              || ' -->>';
/**/
                        WHEN TOO_MANY_ROWS
                        THEN
/**/
                           v_error_hmsg :=
                                 v_error_hmsg
                              || 'Multiple Bill_TO Site Found for '
                              || art_r2.customer_name
                              || ' -->>';
/**/
                     END;
/**/
/**/
/**/
                  EXCEPTION
/**/
                     WHEN OTHERS
                     THEN
/**/
                        fnd_file.put_line
                                  (fnd_file.output,
                                      'Exception while updating Customer Id '
                                   || art_r2.customer_name
                                  );
/**/
                  END;
/**/
/**/
               EXCEPTION
/**/
                  WHEN NO_DATA_FOUND
                  THEN
/**/
                     v_error_hmsg :=
                           v_error_hmsg
                        || 'CUSTOMER '
                        || art_r2.customer_name
                        || '  not defined-->>';
/**/
                  WHEN TOO_MANY_ROWS
                  THEN
/**/
                     v_error_hmsg :=
                             v_error_hmsg || 'Multiple CUSTOMER  defined-->>';
/**/
               END;
/**/
/**/
            ELSE
/**/
               v_error_hmsg :=
                            v_error_hmsg || 'Customer Name Type is Blank-->>';
/**/
            END IF;

/**/
/**/        --#######################################
/**/        -- Validating / Updating Transaction Type/ID
/**/        --#######################################
/**/
            IF art_r2.transaction_type IS NOT NULL
            THEN
/**/
               BEGIN
/**/
                  SELECT
/**/
                         cust_trx_type_id, TYPE
/**/
                  INTO   v_cust_trx_type_id, v_trx_type
/**/
                  FROM   ra_cust_trx_types_all
/**/
                  WHERE
/**/
                         UPPER (NAME) = UPPER (art_r2.transaction_type)
/**/
                     AND org_id = p_org_id
/**/
                  ;

/**/
/**/
                  BEGIN
/**/
                     IF v_trx_type = 'CM'
                     THEN
/**/
                        UPDATE xxabrl_navi_ar_int_line_stg2 int_ln
/**/
                        SET
/**/
                            int_ln.cust_trx_type_id = v_cust_trx_type_id
/**/
                        ,
                            term_id_new = NULL
/**/
                        ,
                            term_id = NULL
/**/
                        WHERE  ROWID = art_r2.ROWID
                           AND UPPER (interface_line_context) = 'ABRL_BAAN';
/**/
                     ELSE
/**/
                        UPDATE xxabrl_navi_ar_int_line_stg2 int_ln
/**/
                        SET int_ln.cust_trx_type_id = v_cust_trx_type_id
/**/
                        WHERE  ROWID = art_r2.ROWID;
/**/
                     END IF;
/**/
/**/
                  EXCEPTION
/**/
                     WHEN OTHERS
                     THEN
/**/
                        fnd_file.put_line
                               (fnd_file.output,
                                   'Exception while updating Transaction Id '
                                || art_r2.transaction_type
                               );
/**/
                  END;
/**/
/**/
               EXCEPTION
/**/
                  WHEN NO_DATA_FOUND
                  THEN
/**/
                     v_error_hmsg :=
                           v_error_hmsg || 'Transaction Type not defined-->>';
/**/
                  WHEN TOO_MANY_ROWS
                  THEN
/**/
                     v_error_hmsg :=
                           v_error_hmsg
                        || 'Multiple Transaction Type defined-->>';
/**/
                  WHEN OTHERS
                  THEN
                     v_error_hmsg :=
                           v_error_hmsg
                        || 'Exception in Transaction Type Validation-->>';
               END;
            ELSE
               v_error_hmsg :=
                              v_error_hmsg || 'Transaction Type is Blank-->>';
            END IF;
         END LOOP;
      END LOOP;

--************************************************************************************
--**  Additional Loop to Update Customer Site-id and Tran-ID.                        *
--**  After updating base table, updated data will be used in Re-Call of same loop   *
--************************************************************************************
--************************************************************************************Ends....
      fnd_file.put_line
                (fnd_file.output,
                 '          ### AR Transaction Information Validating Log ###'
                );
      fnd_file.put_line
         (fnd_file.output,
          '........................................................................'
         );

      FOR art_r1 IN art_c1_validate (org_code, p_batch_source)
      LOOP
         EXIT WHEN art_c1_validate%NOTFOUND;
         v_record_count := art_c1_validate%ROWCOUNT;
         fnd_file.put_line (fnd_file.output,
                               'Batch:'
                            || p_batch_source
                            || ' / Org_Code: '
                            || org_code
                           );

         FOR art_r2 IN art_c2 (art_r1.batch_source_name,
                               art_r1.org_code,
                               art_r1.interface_line_attribute1,
                               art_r1.nv_customer_id,
                               art_r1.nv_cust_site_id
                              )
         LOOP
            EXIT WHEN art_c2%NOTFOUND;
            v_error_hmsg := NULL;
            v_data_count := NULL;
            v_cc_id := NULL;
            v_cust_account_id := NULL;
            v_cust_site_id := NULL;
            v_term_id := NULL;
            v_description := NULL;
            fnd_file.put_line (fnd_file.output, '----------------------');
            fnd_file.put_line (fnd_file.output,
                                  'Line# '
                               || art_r2.interface_line_attribute1
                               || '('
                               || art_r2.interface_line_attribute2
                               || ')'
                              );
            v_error_hmsg := NULL;

--#######################################
-- Validating Bacth Source
--#######################################
            IF art_r2.batch_source_name IS NULL
            THEN
               v_error_hmsg := v_error_hmsg || 'Batch Source is Null-->>';
            ELSE
               v_batch_source := NULL;

               BEGIN
                  SELECT NAME
                    INTO v_batch_source
                    FROM ra_batch_sources_all
                   WHERE UPPER (NAME) = UPPER (art_r2.batch_source_name)
                     AND org_id = p_org_id
                     AND status = 'A'
                     AND batch_source_type = 'FOREIGN'
                     AND SYSDATE BETWEEN NVL (start_date, SYSDATE)
                                     AND NVL (end_date, SYSDATE);
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     v_error_hmsg :=
                           v_error_hmsg
                        || 'Batch Source does not exist in Oracle Financials-->>';
                  WHEN TOO_MANY_ROWS
                  THEN
                     v_batch_source := NULL;
                     v_error_hmsg :=
                            v_error_hmsg || 'Multiple Batch Source found-->>';
                  WHEN OTHERS
                  THEN
                     v_error_hmsg :=
                                v_error_hmsg || 'Wrong Batch Source Code-->>';
               END;
            END IF;

--#######################################
-- Validating ORG Code
--#######################################
            IF art_r2.org_code IS NULL
            THEN
               v_error_hmsg := v_error_hmsg || 'Operating Unit is Null-->>';
            END IF;

--#######################################
-- Validating LINE Type
--#######################################
            IF art_r2.line_type IS NULL
            THEN
               v_error_hmsg := v_error_hmsg || 'Line Type is Null-->>';
            ELSIF UPPER (art_r2.line_type) <> 'LINE'
            THEN
               v_error_hmsg := v_error_hmsg || 'Invalid Line Type-->>';
            END IF;

--#######################################
-- Validating TRX Date / GL Period
--#######################################
            IF art_r2.trx_date IS NULL
            THEN
               v_error_hmsg := v_error_hmsg || 'Transaction Date is null-->>';
            END IF;

            IF art_r2.gl_date IS NULL
            THEN
               v_error_hmsg := v_error_hmsg || 'GL Date is null-->>';
            ELSE
               BEGIN
                  v_data_count := NULL;

                  SELECT COUNT (gps.period_name)
                    INTO v_data_count
                    FROM gl_period_statuses gps, fnd_application fna
                   WHERE fna.application_short_name = 'AR'
                     AND fna.application_id = gps.application_id
                     AND gps.closing_status = 'O'
                     AND gps.set_of_books_id = v_set_of_bks_id
                     AND art_r2.gl_date BETWEEN gps.start_date AND gps.end_date;

                  IF v_data_count = 0
                  THEN
                     v_error_hmsg :=
                           v_error_hmsg
                        || 'GL date is not in AR Open Period -->>';
                  END IF;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     v_error_hmsg :=
                              v_error_hmsg || 'GL Period not open in AR -->>';
               END;
            END IF;

--#######################################
-- Validating Invoice Amount
--#######################################
            IF art_r2.actual_amount IS NULL
            THEN
               v_error_hmsg := v_error_hmsg || 'Invoice Amount is null-->>';
            END IF;

--#######################################
-- Validating Currency Code
--#######################################
            IF art_r2.currency_code IS NULL
            THEN
               v_error_hmsg := v_error_hmsg || 'Currency Code is null -->';
            ELSE
               BEGIN
                  SELECT currency_code
                    INTO v_currency
                    FROM fnd_currencies
                   WHERE UPPER (currency_code) =
                                           TRIM (UPPER (art_r2.currency_code));

                  IF     UPPER (TRIM (v_currency)) <>
                                                     UPPER (TRIM (v_fun_curr))
                     AND art_r2.conversion_date IS NULL
                  THEN
                     v_error_hmsg :=
                                 v_error_hmsg || 'Exchange Date is Null -->>';
                  END IF;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     v_error_hmsg := v_error_hmsg || 'Invalid Currency -->>';
               END;
            END IF;

--#######################################
-- Validating Transaction Type
--#######################################
            IF art_r2.transaction_type IS NULL
            THEN
               v_error_hmsg :=
                              v_error_hmsg || 'Transaction Type Is Null -->>';
            ELSE
               BEGIN
                  v_cust_trx_type_id := NULL;

                  SELECT cust_trx_type_id
                    INTO v_cust_trx_type_id
                    FROM ra_cust_trx_types_all
                   WHERE UPPER (NAME) = UPPER (TRIM (art_r2.transaction_type))
                     AND org_id = p_org_id;

                  IF v_cust_trx_type_id IS NULL
                  THEN
                     v_error_hmsg :=
                             v_error_hmsg || 'Invalid TRANSACTION TYPE  -->>';
                  END IF;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     v_error_hmsg :=
                           v_error_hmsg
                        || 'Transaction Type does not exist in Oracle Financials-->>';
                  WHEN TOO_MANY_ROWS
                  THEN
                     v_error_hmsg :=
                        v_error_hmsg || 'Multiple Transaction Type found-->>';
                  WHEN OTHERS
                  THEN
                     v_error_hmsg :=
                             v_error_hmsg || 'Invalid Transaction Type  -->>';
               END;
            END IF;

--#######################################
-- Validating Payment Term
--#######################################
            IF art_r2.term_id IS NOT NULL
            THEN
               v_term_id := NULL;

               BEGIN
                  IF UPPER (art_r2.term_id) = 'CREDIT NOTE'
                  THEN
                     v_term_id := NULL;
                  ELSE
                     SELECT term_id
                       INTO v_term_id
                       FROM ra_terms
                      WHERE UPPER (NAME) = UPPER (TRIM (art_r2.term_id))
                        AND SYSDATE BETWEEN NVL (start_date_active, SYSDATE)
                                        AND NVL (end_date_active, SYSDATE);
                  END IF;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     v_error_hmsg :=
                           v_error_hmsg
                        || 'Payment Terms does not exist in Oracle Financials-->>';
                  WHEN TOO_MANY_ROWS
                  THEN
                     v_term_id := NULL;
                     v_error_hmsg :=
                           v_error_hmsg || 'Multiple Payment Terms found-->>';
                  WHEN OTHERS
                  THEN
                     v_term_id := NULL;
                     v_error_hmsg :=
                                  v_error_hmsg || 'Invalid Payment Term -->>';
               END;
            END IF;

--###########################################
-- Validating Customer ID / Customer Site ID
--###########################################
            IF art_r2.nv_customer_id IS NULL
            THEN
               v_error_hmsg :=
                     v_error_hmsg
                  || 'E-Retail Customer / Store Code Is Null -->>';
            END IF;

            IF art_r2.nv_cust_site_id IS NULL
            THEN
               v_error_hmsg :=
                              v_error_hmsg || 'E-Retail DC Code Is Null -->>';
            END IF;

            IF     art_r2.nv_customer_id IS NOT NULL
               AND art_r2.nv_cust_site_id IS NOT NULL
            THEN
               BEGIN
                  v_cust_account_id := art_r2.nv_customer_id;
                  v_cust_site_id := art_r2.nv_cust_site_id;
                           /*Select Unique hca.Cust_Account_Id, hcas.cust_Acct_site_id
                            Into V_Cust_Account_Id, V_cust_site_id
                            From hz_cust_accounts_all         hca,
                                 hz_cust_acct_sites_all       hcas,
                                 hz_party_sites               hps,
                                 hz_cust_site_uses_all        hcsu,
                                 xxabrl_ar_cust_bkacc_map_int xac
                           Where NV_CUST_SITE_ID = ART_R2.NV_CUST_SITE_ID
                             And NV_CUSTOMER_NUMBER = ART_R2.NV_CUSTOMER_NUMBER
                             And hca.Account_Number = xac.OF_Customer_Number
                             And hca.cust_account_id = hcas.cust_account_id
                             And hcas.org_id = P_Org_id
                             And hcas.cust_acct_site_id = hcsu.cust_acct_site
               _id(+)
                             And hcas.party_site_id = hps.party_site_id
                             And hps.Party_Site_Number = xac.OF_Customer_Site_Code
                             And hcsu.site_use_code = 'BILL_TO'
                             And hcas.bill_to_flag = 'P'
                             And hcas.status = 'A';*/
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     v_error_hmsg :=
                           v_error_hmsg
                        || 'Customer Number Or Site in not exist in this Org -->>';
                  WHEN TOO_MANY_ROWS
                  THEN
                     v_error_hmsg :=
                           v_error_hmsg
                        || 'Multiple Customer Number Or Site found in this Org -->>';
                  WHEN OTHERS
                  THEN
                     v_error_hmsg :=
                           v_error_hmsg
                        || 'Customer Number Or Site in not exist in this Org -->>';
               END;
            END IF;

--#######################################
-- Validating Tax Code - Context Value
--#######################################
            v_attribute_category := NULL;

            IF art_r2.attribute_category IS NOT NULL
            THEN
               BEGIN
                  SELECT descriptive_flex_context_code
                    INTO v_attribute_category
                    FROM fnd_descr_flex_contexts_vl
                   WHERE UPPER (descriptive_flex_context_code) =
                                             UPPER (art_r2.attribute_category)
                     AND (descriptive_flexfield_name LIKE
                                                       'RA_CUSTOMER_TRX_LINES'
                         )
                     AND enabled_flag = 'Y';

                  BEGIN
                     UPDATE xxabrl_navi_ar_int_line_stg2
                        SET attribute_category = v_attribute_category
                      WHERE ROWID = art_r2.ROWID
                        AND UPPER (interface_line_context) = 'ABRL_BAAN';
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        v_error_hmsg :=
                              v_error_hmsg
                           || 'Exception while updating derived Tax ATTRIBUTE_CATEGORY for:'
                           || TRIM (UPPER (art_r2.attribute_category))
                           || ' -->>';
                  END;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     v_error_hmsg :=
                           v_error_hmsg
                        || 'Tax ATTRIBUTE_CATEGORY does not exists for:'
                        || TRIM (UPPER (art_r2.attribute_category))
                        || ' -->>';
                  WHEN TOO_MANY_ROWS
                  THEN
                     v_error_hmsg :=
                           v_error_hmsg
                        || 'Too many rows defined for Tax ATTRIBUTE_CATEGORY:'
                        || TRIM (UPPER (art_r2.attribute_category))
                        || ' -->>';
                  WHEN OTHERS
                  THEN
                     v_error_hmsg :=
                           v_error_hmsg
                        || 'Exception while deriving Tax ATTRIBUTE_CATEGORY:'
                        || TRIM (UPPER (art_r2.attribute_category))
                        || ' -->>'
                        || SQLERRM;
               END;
            END IF;

--#######################################
-- Validating Tax Code
--#######################################
            IF art_r2.attribute1 IS NOT NULL
            THEN
               BEGIN
                  SELECT tax_name
                    INTO v_tax_name
                    FROM jai_cmn_taxes_all
                   WHERE UPPER (tax_name) = TRIM (UPPER (art_r2.attribute1))
                     AND org_id = p_org_id
                     AND SYSDATE BETWEEN NVL (start_date, SYSDATE)
                                     AND NVL (end_date, SYSDATE);
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     v_error_hmsg :=
                           v_error_hmsg
                        || 'Tax Code does not exists for:'
                        || TRIM (UPPER (art_r2.attribute1))
                        || ' -->>';
                  WHEN TOO_MANY_ROWS
                  THEN
                     v_error_hmsg :=
                           v_error_hmsg
                        || 'Tax Code does not exists for:'
                        || TRIM (UPPER (art_r2.attribute1))
                        || ' -->>';
                  WHEN OTHERS
                  THEN
                     v_error_hmsg :=
                           v_error_hmsg
                        || 'Exception for Tax Code :'
                        || TRIM (UPPER (art_r2.attribute1))
                        || ' -->>';
               END;
            END IF;

--#######################################
-- Validating Distribution Segments
--#######################################
            fnd_file.put_line (fnd_file.output,
                               '---Validation for Segments--');
            fnd_file.put_line (fnd_file.output,
                                  'ART_R2.INTERFACE_LINE_ATTRIBUTE1 '
                               || art_r2.interface_line_attribute1
                              );
            fnd_file.put_line (fnd_file.output,
                                  'ART_R2.INTERFACE_LINE_ATTRIBUTE2 '
                               || art_r2.interface_line_attribute2
                              );

--        Fnd_file.PUT_LINE(fnd_file.Output,'ART_R2.NV_CUSTOMER_ID '||ART_R2.NV_CUSTOMER_ID);
--        Fnd_file.PUT_LINE(fnd_file.Output,'ART_R2.NV_CUST_SITE_ID '||ART_R2.NV_CUST_SITE_ID);
            FOR art_r3 IN art_c3 (art_r2.batch_source_name,
                                  art_r2.org_code,
                                  art_r2.interface_line_attribute1,
                                  art_r2.interface_line_attribute2,
                                  art_r2.nv_customer_id,
                                  art_r2.nv_cust_site_id
                                 )
            LOOP
               EXIT WHEN art_c3%NOTFOUND;
               v_error_lmsg := NULL;
               v_code_combination_id := NULL;
               fnd_file.put_line (fnd_file.output,
                                  'ART_R2.company ' || art_r3.co
                                 );
               fnd_file.put_line (fnd_file.output,
                                  'ART_R2.company ' || art_r3.cc
                                 );

               IF art_r3.amount IS NULL
               THEN
                  v_error_lmsg := 'Invoice Account Line Amount is null-->>';
               ELSIF art_r3.amount <> art_r2.amount
               THEN
                  v_error_lmsg :=
                     'Invoice Line Amount is not equal to Account Line Amount-->>';
               END IF;

               IF art_r3.co IS NOT NULL
               THEN
                  --  V_Error_Lmsg:=V_Error_Lmsg||'Accounting Segment Company Value is Null-->>';
                  --Else
                  v_seg_status := NULL;
                  v_seg_status :=
                     account_seg_status (art_r3.co,
                                         LTRIM (RTRIM ('ABRL_GL_CO'))
                                        );

                  IF v_seg_status IS NOT NULL
                  THEN
                     v_error_lmsg :=
                           v_error_lmsg
                        || 'Invalid Company Segment '
                        || art_r3.co
                        || '-->>';
                  END IF;
               END IF;

               -- Validation for CC Seg
               IF art_r3.cc IS NOT NULL
               THEN
                  --  V_Error_Lmsg:=V_Error_Lmsg||'Accounting Segment Cost Center Value is Null-->>';
                  --Else
                  v_seg_status := NULL;
                  v_seg_status :=
                     account_seg_status (art_r3.cc,
                                         LTRIM (RTRIM ('ABRL_GL_CC'))
                                        );

                  IF v_seg_status IS NOT NULL
                  THEN
                     v_error_lmsg :=
                           v_error_lmsg
                        || 'Invalid Cost Center Segment '
                        || art_r3.cc
                        || '-->>';
                  END IF;
               END IF;

               -- Validation for Location Seg
               IF art_r3.LOCATION IS NOT NULL
               THEN
                  -- V_Error_Lmsg:=V_Error_Lmsg||'Accounting Segment Location Value is Null-->>';
                  --Else
                  v_seg_status := NULL;
                  v_seg_status :=
                     account_seg_status (art_r3.LOCATION,
                                         LTRIM (RTRIM ('ABRL_GL_Location'))
                                        );

                  IF v_seg_status IS NOT NULL
                  THEN
                     v_error_lmsg :=
                           v_error_lmsg
                        || 'Invalid Location Segment '
                        || art_r3.LOCATION
                        || '-->>';
                  END IF;
               END IF;

               -- Validation for Merchandise Seg
               IF art_r3.merchandise IS NOT NULL
               THEN
                  --  V_Error_Lmsg:=V_Error_Lmsg||'Accounting Segment Merchandise Value is Null-->>';
                  --Else
                  v_seg_status := NULL;
                  v_seg_status :=
                     account_seg_status (art_r3.merchandise,
                                         LTRIM (RTRIM ('ABRL_GL_Merchandize'))
                                        );

                  IF v_seg_status IS NOT NULL
                  THEN
                     v_error_lmsg :=
                           v_error_lmsg
                        || 'Invalid Merchandise Segment '
                        || art_r3.merchandise
                        || '-->>';
                  END IF;
               END IF;

               -- Validation for Account Seg
               IF art_r3.ACCOUNT IS NOT NULL
               THEN
                  --  V_Error_Lmsg:=V_Error_Lmsg||'Accounting Segment Natural Account Value is Null-->>';
                  --Else
                  v_seg_status := NULL;
                  v_seg_status :=
                     account_seg_status (art_r3.ACCOUNT,
                                         LTRIM (RTRIM ('ABRL_GL_Account'))
                                        );

                  IF v_seg_status IS NOT NULL
                  THEN
                     v_error_lmsg :=
                           v_error_lmsg
                        || 'Invalid Account Segment '
                        || art_r3.ACCOUNT
                        || '-->>';
                  END IF;
               END IF;

               -- Validation for Intercompany Seg
               IF art_r3.intercompany IS NOT NULL
               THEN
                  --  V_Error_Lmsg:=V_Error_Lmsg||'Accounting Segment Intercompany Value is Null-->>';
                  --Else
                  v_seg_status := NULL;
                  v_seg_status :=
                     account_seg_status (art_r3.intercompany,
                                         LTRIM (RTRIM ('ABRL_GL_STATE_SBU'))
                                        );

                  IF v_seg_status IS NOT NULL
                  THEN
                     v_error_lmsg :=
                           v_error_lmsg
                        || 'Invalid Intercompany Segment '
                        || art_r3.intercompany
                        || '-->>';
                  END IF;
               END IF;

               -- Validation for Future Seg
               IF art_r3.future IS NOT NULL
               THEN
                  --  V_Error_Lmsg:=V_Error_Lmsg||'Accounting Segment Future Value is Null-->>';
                  --Else
                  v_seg_status := NULL;
                  v_seg_status :=
                     account_seg_status (art_r3.future,
                                         LTRIM (RTRIM ('ABRL_GL_Future'))
                                        );

                  IF v_seg_status IS NOT NULL
                  THEN
                     v_error_lmsg :=
                           v_error_lmsg
                        || 'Invalid Future Segment '
                        || art_r3.future
                        || '-->>';
                  END IF;
               END IF;

               IF v_cust_account_id IS NOT NULL AND v_cust_site_id IS NOT NULL
               THEN
                  v_state_sbu := NULL;

                  BEGIN
                     SELECT gcc.segment3
                       INTO v_state_sbu
                       FROM gl_code_combinations gcc,
                            hz_cust_acct_sites_all hcas,
                            hz_cust_site_uses_all hcsu
                      WHERE hcas.cust_account_id = v_cust_account_id
                        -- added by shailesh on 12 nov 2008 passing the org id from parameter
                        AND hcas.org_id = p_org_id
                        AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id(+)
                        AND hcas.cust_acct_site_id = v_cust_site_id
                        AND hcsu.gl_id_rev = gcc.code_combination_id
                        AND hcas.status = 'A'
                        AND hcsu.status = 'A';
                  --fnd_file.put_line(Fnd_file.OUTPUT,'Derived SBU: '||V_State_Sbu);
                  EXCEPTION
                     WHEN NO_DATA_FOUND
                     THEN
                        v_error_lmsg :=
                              v_error_lmsg
                           || 'Accounting Code missing at Customer '
                           || art_r2.customer_name
                           || ' Site  -->>';
                        v_state_sbu := NULL;
                     WHEN TOO_MANY_ROWS
                     THEN
                        v_error_lmsg :=
                              v_error_lmsg
                           || 'Multiple Charge Account Code exists for '
                           || art_r2.customer_name
                           || ' Site-->>';
                        v_state_sbu := NULL;
                     WHEN OTHERS
                     THEN
                        v_error_lmsg :=
                              v_error_lmsg
                           || 'Accounting Code missing at Vendor Site '
                           || art_r2.customer_name
                           || ' Site-->>';
                        v_state_sbu := NULL;
                  END;
               END IF;

               --Fnd_File.PUT_LINE(fnd_File.OUTPUT,'Error SBU: '||V_Error_Lmsg);
               IF     art_r3.co IS NOT NULL
                  AND art_r3.cc IS NOT NULL
                  AND art_r3.LOCATION IS NOT NULL
                  AND art_r3.ACCOUNT IS NOT NULL
               THEN
                  BEGIN
                     v_code_combination_id := NULL;

                     SELECT id_flex_num
                       INTO vn_structure_id
                       FROM fnd_id_flex_structures_vl
                      WHERE UPPER (id_flex_structure_code) =
                                                        'ABRL_ACCOUNTING_FLEX'
                        AND id_flex_code = 'GL#';

                     p_gl_id_rev :=
                           art_r3.co
                        || '.'
                        || art_r3.cc
                        || '.'
                        || v_state_sbu
                        || '.'
                        || art_r3.LOCATION
                        || '.'
                        || art_r3.merchandise
                        || '.'
                        || art_r3.ACCOUNT
                        || '.'
                        || art_r3.intercompany
                        || '.'
                        || art_r3.future;
                     --Fnd_file.PUT_LINE(fnd_file.OUTPUT,'Generating CCID for :'||p_gl_id_rev);
                     x_gl_rev_acct_id :=
                        fnd_flex_ext.get_ccid
                                   (application_short_name      => 'SQLGL',
                                    key_flex_code               => 'GL#',
                                    structure_number            => vn_structure_id,
                                    -- 50308,
                                    validation_date             => TO_CHAR
                                                                      (SYSDATE,
                                                                       'DD-MON-YYYY'
                                                                      ),
                                    concatenated_segments       => p_gl_id_rev
                                   );

                     IF x_gl_rev_acct_id <= 0
                     THEN
                        fnd_file.put_line
                                        (fnd_file.output,
                                            'Error creating GL Revenue Acct:'
                                         || fnd_flex_ext.GET_MESSAGE
                                         || p_gl_id_rev
                                        );
                        v_error_lmsg :=
                              v_error_lmsg
                           || 'Error creating GL Revenue Acct:'
                           || fnd_flex_ext.GET_MESSAGE;
                     ELSE
                        --Fnd_File.Put_Line(fnd_File.OUTPUT,'Newly Generated CCID:' ||x_gl_rev_acct_id);
                        BEGIN
/*                      select segment1||'.'||segment2||'.'||segment3||'.'||segment4||'.'||segment5||'.'||segment6||'.'||segment7||'.'||segment8
                      into v_Conc_Segments
                      from
                      XXABRL_RESA_RA_INT_DIST_ALL
                      where record_number = ART_R3.record_number;
                      Fnd_File.Put_Line(fnd_File.OUTPUT,'Concatenated Segments in Dist Staging:' ||v_Conc_Segments);
                      Update XXABRL_RESA_RA_INT_DIST_ALL
                      set Code_Combination_Id = x_gl_rev_acct_id
                      where segment1||'.'||segment2||'.'||segment3||'.'||segment4||'.'||segment5||'.'||segment6||'.'||segment7||'.'||segment8 = ART
_R3.record_number
                      --where int_dist.INTERFACE_LINE_ATTRIBUTE1 = ART_R3.INTERFACE_LINE_ATTRIBUTE1
                      --and int_dist.INTERFACE_LINE_ATTRIBUTE2 = ART_R3.INTERFACE_LINE_ATTRIBUTE2
                      ;
*/

                           --Fnd_File.Put_Line(fnd_File.OUTPUT,'Derived CCID :' ||x_gl_rev_acct_id);
/*
                      INSERT into
                      XXABRL_AR_SEGMENTS_CCIDS
                      (
                      CONC_SEGMENTS,
                      CCID ,
                      RECORD_NUMBER)
                      Values
                      (
                      p_gl_id_rev,
                      x_gl_rev_acct_id,
                      ART_R3.record_number
                      );
                      Commit;
                      Update XXABRL_RESA_RA_INT_DIST_ALL t1
                      set t1.Code_Combination_Id =
                      (select CCID
                      from XXABRL_AR_SEGMENTS_CCIDS t2
                      where t2.record_number = t1.record_number
                      and rownum =1
                      );
*/
                           NULL;
                        EXCEPTION
                           WHEN NO_DATA_FOUND
                           THEN
                              fnd_file.put_line
                                 (fnd_file.output,
                                  'Concatenated Segments in Dist Staging Not Found'
                                 );
                           WHEN OTHERS
                           THEN
                              fnd_file.put_line
                                         (fnd_file.output,
                                             'Error while updating CCID for '
                                          || SQLERRM
                                          || '>>>'
                                          || p_gl_id_rev
                                         );
                        END;
                     END IF;
/*              Select Code_Combination_id
                Into V_Code_Combination_id
                From GL_CODE_COMBINATIONS
               where Segment1 = ART_R3.CO
                 And Segment2 = ART_R3.CC
                 And Segment3 = V_State_Sbu
                 And Segment4 = ART_R3.Location
                 And Segment5 = ART_R3.Merchandise
                 And Segment6 = ART_R3.Account
                 And Segment7 = ART_R3.Intercompany
                 And Segment8 = ART_R3.Future;*/
                  EXCEPTION
                     WHEN NO_DATA_FOUND
                     THEN
                        v_error_lmsg :=
                              v_error_lmsg
                           || 'ABRL_ACCOUNTING_FLEX does not exists-->>';
                     WHEN TOO_MANY_ROWS
                     THEN
                        v_error_lmsg :=
                              v_error_lmsg
                           || 'Multiple ABRL_ACCOUNTING_FLEX exists-->>';
                     WHEN OTHERS
                     THEN
                        v_error_lmsg :=
                              v_error_lmsg
                           || 'Exception in ABRL_ACCOUNTING_FLEX >>'
                           || SQLERRM;
                  END;
               END IF;

               --Updating the error message in table
               IF v_error_lmsg IS NOT NULL
               THEN
                  UPDATE xxabrl.xxabrl_navi_ra_int_dist_stg2
                     SET error_message = v_error_lmsg,
                         interfaced_flag = 'E'
                   WHERE ROWID = art_r3.ROWID;

                  fnd_file.put_line (fnd_file.output,
                                     'Error in Dist: ' || v_error_lmsg
                                    );
                  COMMIT;
               ELSE
                  UPDATE xxabrl.xxabrl_navi_ra_int_dist_stg2
                     SET code_combination_id = v_code_combination_id,
                         org_id = p_org_id,
                         --                   Customer_Id              = V_Cust_Account_Id,
                         --                   Customer_Site_Id         = V_cust_site_id,
                         interfaced_flag = 'V'
                   WHERE ROWID = art_r3.ROWID;

                  fnd_file.put_line (fnd_file.output, 'Distribution Valid');
                  COMMIT;
               END IF;
            END LOOP;

            IF v_error_hmsg IS NOT NULL
            THEN
               UPDATE xxabrl_navi_ar_int_line_stg2
                  SET error_message = v_error_hmsg,
                      interfaced_flag = 'E'
                WHERE ROWID = art_r2.ROWID
                  AND UPPER (interface_line_context) = 'ABRL_BAAN';

               fnd_file.put_line (fnd_file.output,
                                  'Error in Line: ' || v_error_hmsg
                                 );
               COMMIT;
               fnd_file.put_line (fnd_file.output,
                                     'Invoice Line Number       :'
                                  || art_r2.interface_line_attribute2
                                 );
               fnd_file.put_line (fnd_file.output,
                                  'Line Record Error         :'
                                  || v_error_hmsg
                                 );
               v_error_count := v_error_count + 1;

               IF v_error_lmsg IS NOT NULL
               THEN
                  fnd_file.put_line (fnd_file.output,
                                        'Distribution Record Error :'
                                     || v_error_lmsg
                                    );
               END IF;
            ELSIF v_error_hmsg IS NULL AND v_error_lmsg IS NOT NULL
            THEN
               UPDATE xxabrl_navi_ar_int_line_stg2
                  SET error_message =
                         'Error In Invoice Accounting Record ' || v_error_lmsg,
                      interfaced_flag = 'E'
                WHERE ROWID = art_r2.ROWID
                  AND UPPER (interface_line_context) = 'ABRL_BAAN';

               COMMIT;
               v_error_count := v_error_count + 1;
               fnd_file.put_line (fnd_file.output,
                                     'Invoice Line Number       :'
                                  || art_r2.interface_line_attribute2
                                 );
               fnd_file.put_line (fnd_file.output,
                                  'Distribution Error :' || v_error_lmsg
                                 );
            ELSE
               UPDATE xxabrl_navi_ar_int_line_stg2
                  SET nv_customer_id = v_cust_account_id,
                      nv_cust_site_id = v_cust_site_id,
                      org_id = p_org_id,
                      term_id_new = v_term_id,
                      cust_trx_type_id = v_cust_trx_type_id,
                      interfaced_flag = 'V'
                WHERE ROWID = art_r2.ROWID
                  AND UPPER (interface_line_context) = 'ABRL_BAAN';

               fnd_file.put_line (fnd_file.output, 'Line Valid');
               COMMIT;
               v_ok_rec_count := v_ok_rec_count + 1;
            END IF;
         END LOOP;

         BEGIN
            v_data_count := 0;

            SELECT COUNT (interface_line_attribute1)
              INTO v_data_count
              FROM xxabrl_navi_ar_int_line_stg2
             WHERE batch_source_name = art_r1.batch_source_name
               AND org_code = art_r1.org_code
               AND interface_line_context = art_r1.interface_line_context
               AND interface_line_attribute1 =
                                              art_r1.interface_line_attribute1
               AND nv_customer_id = art_r1.nv_customer_id
               AND nv_cust_site_id = art_r1.nv_cust_site_id
               AND error_message IS NOT NULL
               AND UPPER (interface_line_context) = 'ABRL_BAAN';

            IF v_data_count > 0
            THEN
               UPDATE xxabrl_navi_ar_int_line_stg2
                  SET error_message =
                                  'Error In Invoice Lines>>>' || error_message
                WHERE batch_source_name = art_r1.batch_source_name
                  AND org_code = art_r1.org_code
                  AND interface_line_context = art_r1.interface_line_context
                  AND interface_line_attribute1 =
                                              art_r1.interface_line_attribute1
                  AND nv_customer_id = art_r1.nv_customer_id
                  AND nv_cust_site_id = art_r1.nv_cust_site_id
                  AND error_message IS NOT NULL
                  AND UPPER (interface_line_context) = 'ABRL_BAAN';
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               v_data_count := 0;
         END;
      END LOOP;

      fnd_file.put_line
         (fnd_file.output,
          '........................................................................'
         );
      fnd_file.put_line (fnd_file.output,
                         'Number of Valid Records    :' || v_ok_rec_count
                        );
      fnd_file.put_line (fnd_file.output,
                         'Number of Error Records  :' || v_error_count
                        );
      fnd_file.put_line
         (fnd_file.output,
          '........................................................................'
         );

      IF v_error_count > 0
      THEN
         retcode := 1;
      END IF;

      fnd_file.put_line (fnd_file.LOG,
                         'P_BATCH_Source: ' || NVL (p_batch_source, 'NULL')
                        );
      fnd_file.put_line (fnd_file.LOG,
                         'P_ORG_ID: ' || NVL (TO_CHAR (p_org_id), 'NULL')
                        );

      IF p_action = 'N' AND v_error_count = 0
      THEN
         fnd_file.put_line (fnd_file.output,
                            '--Inserting Data to Interface Tables--'
                           );
         fnd_file.put_line (fnd_file.output,
                            'P_ORG_ID: ' || NVL (TO_CHAR (p_org_id), 'NULL')
                           );
         fnd_file.put_line (fnd_file.output,
                            'P_BATCH_Source: ' || NVL (p_batch_source, 'NULL')
                           );
         invoice_insert (p_org_id, p_batch_source, retcode);
         fnd_file.put_line (fnd_file.LOG,
                            'P_BATCH_Source: ' || NVL (p_batch_source, 'NULL')
                           );
         fnd_file.put_line (fnd_file.LOG,
                            'P_ORG_ID: ' || NVL (TO_CHAR (p_org_id), 'NULL')
                           );
      END IF;
   END invoice_validate;

   PROCEDURE invoice_insert (
      p_org_id         IN       NUMBER,
      p_batch_source   IN       VARCHAR2,
      x_ret_code       OUT      NUMBER
   )
   IS
      CURSOR art_c1 (cp_org_id NUMBER, cp_batch_source_name VARCHAR2)
      IS
         SELECT   ROWID, batch_source_name, org_id, interface_line_context,
                  interface_line_attribute1 interface_line_attribute1,
                  interface_line_attribute2 interface_line_attribute2,
                  line_type, trx_date, gl_date,
                  NVL (actual_amount, amount) amount, currency_code,
                  conversion_type, conversion_rate, conversion_date,
                  transaction_type, description,                  --COMMENTS,
                                                cust_trx_type_id,
                  nv_customer_id, nv_cust_site_id, attribute1,
                  attribute_category,
                                     --CUSTOMER_ID,
                                     --CUSTOMER_SITE_ID,
                                     term_id_new term_id,                 /*,
                                                           CUST_TRX_TYPE_ID*/
                  NVL (actual_amount, amount) actual_amount,
                  deposited_amount, deposited_date, opening_balance,
                  closing_balance, net_change_at_store
             FROM xxabrl_navi_ar_int_line_stg2 rnlv
            WHERE                  /*BATCH_SOURCE_NAME = CP_BATCH_SOURCE_NAME
                               And ORG_CODE = CP_ORG_ID
                               And */
                                   -- ADDED BY MITUL AND DHARMESH
                  UPPER (org_code) = (SELECT UPPER (short_code)
                                        FROM hr_operating_units
                                       WHERE organization_id = cp_org_id)
              AND
                  -- ADDED BY MITUL AND DHARMESH   version = 1.0.2
                  NVL (interfaced_flag, 'N') = 'V'
              AND UPPER (interface_line_context) = 'ABRL_BAAN'
--         AND CUSTOMER_NAME = 6501391---:CONTROL_BLK.CUSTOMER_NAME ---
--                      AND GL_DATE = '06-JAN-2010'-----
              AND NOT EXISTS (
                     SELECT interface_line_attribute1
                       FROM xxabrl_navi_ar_int_line_stg2 rnlu
                      WHERE rnlu.batch_source_name = rnlv.batch_source_name
                        AND rnlu.org_code = rnlv.org_code
                        AND rnlu.nv_customer_id = rnlv.nv_customer_id
                        AND rnlu.nv_cust_site_id = rnlv.nv_cust_site_id
                        AND rnlu.interface_line_attribute1 =
                                                rnlv.interface_line_attribute1
                        AND NVL (rnlu.interfaced_flag, 'N') IN ('N', 'E')
                        AND UPPER (interface_line_context) = 'ABRL_BAAN'
                        AND freeze_flag = 'Y')
         ORDER BY /*ORG_CODE,*/ batch_source_name,
                   interface_line_context,
                   interface_line_attribute1,
                   interface_line_attribute2                               /*,
                                                       CUSTOMER_ID,
                                                       CUSTOMER_SITE_ID*/
                                            ;

      CURSOR art_c2 (
         cp_batch_source_name           VARCHAR2,
         cp_org_id                      NUMBER,
         cp_interface_line_attribute1   VARCHAR2,
         cp_interface_line_attribute2   VARCHAR2,
         cp_customer_id                 NUMBER,
         cp_customer_site_id            NUMBER
      )
      IS
         SELECT ROWID,
                      --BATCH_SOURCE_NAME,
                      --ORG_CODE,
                      interface_line_context,
                interface_line_attribute1 interface_line_attribute1,
                interface_line_attribute2 interface_line_attribute2,
                account_class, amount, segment1 co, segment2 cc,
                segment3 state_sbu, segment4 LOCATION, segment5 merchandise,
                segment6 ACCOUNT, segment7 intercompany, segment8 future,
                org_id, code_combination_id
           FROM xxabrl.xxabrl_navi_ra_int_dist_stg2
          WHERE                     /*BATCH_SOURCE_NAME = CP_BATCH_SOURCE_NAME
                                      And */
                org_id = cp_org_id
            AND interface_line_attribute1 = cp_interface_line_attribute1
            AND interface_line_attribute2 = cp_interface_line_attribute2
/*       And CUSTOMER_ID = CP_CUSTOMER_ID
               And CUSTOMER_SITE_ID = CP_CUSTOMER_SITE_ID
*/
            AND NVL (interfaced_flag, 'N') IN ('V');

      v_set_of_bks_id   NUMBER       := fnd_profile.VALUE ('GL_SET_OF_BKS_ID');
      v_user_id         NUMBER         := fnd_profile.VALUE ('USER_ID');
      v_resp_id         NUMBER         := fnd_profile.VALUE ('RESP_ID');
      v_appl_id         NUMBER         := fnd_profile.VALUE ('RESP_APPL_ID');
      v_req_id          NUMBER;
      v_record_count    NUMBER         := 0;
      v_error_msg       VARCHAR2 (999);
      e_insert_int      EXCEPTION;
   BEGIN
      --XXABRL_UPDATE_ARINVDIST_CCID; --External Procedure to update CCIDs
      fnd_file.put_line
         (fnd_file.output,
          '........................................................................'
         );
      fnd_file.put_line (fnd_file.output,
                         'Insert Records Into Interface Table'
                        );
      fnd_file.put_line
         (fnd_file.output,
          '........................................................................'
         );

      --fnd_file.put_line(fnd_file.output,'P_ORG_ID: ' ||nvl(P_ORG_ID,'NULL'));
      --fnd_file.put_line(fnd_file.output,'P_BATCH_Source: '||nvl(P_BATCH_Source,'NULL'));
      FOR art_r1 IN art_c1 (p_org_id, p_batch_source)
      LOOP    ----FOR ART_R1 IN ART_C1(to_char(P_ORG_ID), P_BATCH_Source) LOOP
         EXIT WHEN art_c1%NOTFOUND;
         v_record_count := art_c1%ROWCOUNT;
         fnd_file.put_line (fnd_file.output, ' Testing ');
         fnd_file.put_line (fnd_file.output, ' ');
         fnd_file.put_line
            (fnd_file.output,
             '------------------------------------------------------------------------'
            );
         fnd_file.put_line (fnd_file.output,
                               'Inserting AR Line: '
                            || TRIM (art_r1.interface_line_attribute1)
                            || ' ( '
                            || TRIM (art_r1.interface_line_attribute2)
                            || ' )'
                           );

         BEGIN
            INSERT INTO ra_interface_lines_all
                        (                    --interface_line_id, --@Navisite
                         interface_line_context,
                         interface_line_attribute1,
                         interface_line_attribute2,
                         batch_source_name, set_of_books_id,
                         line_type, description,
                         currency_code, amount,
                         cust_trx_type_id,
                         trx_date, gl_date,
                         orig_system_batch_name,
                         orig_system_bill_customer_id,
                         orig_system_bill_address_id,
                         conversion_type,
                         conversion_date,
                         conversion_rate,
                         term_id, comments,
                         attribute_category,
                         attribute1,
                         attribute2,
                         attribute3,
                         attribute4,
                         attribute5,
                         attribute6,
                         attribute7,
                         org_id, created_by, creation_date,
                         last_updated_by, last_update_date
                        )
                 VALUES (                           --XXABRL_AR_CNV_S.NEXTVAL,
                         TRIM (art_r1.interface_line_context),
                         TRIM (art_r1.interface_line_attribute1),
                         TRIM (art_r1.interface_line_attribute2),
                         TRIM (art_r1.batch_source_name), v_set_of_bks_id,
                         TRIM (art_r1.line_type), TRIM (art_r1.description),
                         TRIM (art_r1.currency_code), TRIM (art_r1.amount),
                         TRIM (art_r1.cust_trx_type_id),
                         TRIM (art_r1.trx_date), art_r1.gl_date,
                         TRIM (art_r1.batch_source_name) || SYSDATE,
                         TRIM (art_r1.nv_customer_id),
                         TRIM (art_r1.nv_cust_site_id),
                         TRIM (NVL (art_r1.conversion_type, 'Corporate')),
                         TRIM (art_r1.conversion_date),
                         TRIM (art_r1.conversion_rate),
                         TRIM (art_r1.term_id), NULL,
                         --Trim(ART_R1.DESCRIPTION) ,Trim(ART_R1.COMMENTS),
                         TRIM (art_r1.attribute_category),
                         --'E-RETAIL RECEIVABLES INVOICE'
                         TRIM (art_r1.attribute1),                       --Tax
                         TRIM (art_r1.actual_amount),          --ACTUAL_AMOUNT
                         TRIM (art_r1.deposited_amount),    --DEPOSITED_AMOUNT
                         TRIM (art_r1.deposited_date),        --DEPOSITED_DATE
                         TRIM (art_r1.opening_balance),      --OPENING_BALANCE
                         TRIM (art_r1.closing_balance),      --CLOSING_BALANCE
                         TRIM (art_r1.net_change_at_store),
                         --NET_CHANGE_AT_STORE
                         TRIM (art_r1.org_id), v_user_id, SYSDATE,
                         v_user_id, SYSDATE
                        );

            UPDATE xxabrl_navi_ar_int_line_stg2
               SET interfaced_flag = 'Y',
                   interfaced_date = SYSDATE
             WHERE ROWID = art_r1.ROWID
               AND UPPER (interface_line_context) = 'ABRL_BAAN';

            fnd_file.put_line (fnd_file.output, 'Line Inserted Successfully');
         EXCEPTION
            WHEN OTHERS
            THEN
               v_error_msg :=
                     'Error-Rollback: Exception in Invoice Line insert to Interface: '
                  || SQLERRM;
               RAISE e_insert_int;
         END;

         --Commit;
         fnd_file.put_line (fnd_file.output, 'After 1 commit');
         fnd_file.put_line (fnd_file.output, art_r1.batch_source_name);
         fnd_file.put_line (fnd_file.output, art_r1.org_id);
         fnd_file.put_line (fnd_file.output, art_r1.interface_line_attribute1);
         fnd_file.put_line (fnd_file.output, art_r1.interface_line_attribute2);
         fnd_file.put_line (fnd_file.output, art_r1.nv_customer_id);
         fnd_file.put_line (fnd_file.output, art_r1.nv_cust_site_id);

         FOR art_r2 IN art_c2 (art_r1.batch_source_name,
                               art_r1.org_id,
                               art_r1.interface_line_attribute1,
                               art_r1.interface_line_attribute2,
                               art_r1.nv_customer_id,
                               art_r1.nv_cust_site_id
                              )
         LOOP
            EXIT WHEN art_c2%NOTFOUND;
            fnd_file.put_line (fnd_file.output,
                                  'Inserting AR Line Distribution: '
                               || TRIM (art_r1.interface_line_attribute1)
                               || ' ( '
                               || TRIM (art_r1.interface_line_attribute2)
                               || ' )'
                              );

            BEGIN
               INSERT INTO ra_interface_distributions_all
                           (interface_line_context,
                            interface_line_attribute1,
                            interface_line_attribute2,
                            account_class,
                            amount,
--             code_combination_id,
                                   org_id,
                            created_by, creation_date, last_updated_by,
                            last_update_date, PERCENT, segment1, segment2,
                            segment3, segment4,
                            segment5, segment6,
                            segment7, segment8
                           )
                    VALUES (TRIM (art_r2.interface_line_context),
                            TRIM (art_r2.interface_line_attribute1),
                            TRIM (art_r2.interface_line_attribute2),
                            TRIM (art_r2.account_class),
                            TRIM (art_r2.amount),
--             Trim(ART_R2.CODE_COMBINATION_ID),
                                                 TRIM (art_r2.org_id),
                            v_user_id, SYSDATE, v_user_id,
                            SYSDATE, 100, art_r2.co, art_r2.cc,
                            art_r2.state_sbu, art_r2.LOCATION,
                            art_r2.merchandise, art_r2.ACCOUNT,
                            art_r2.intercompany, art_r2.future
                           );

               --End If;
               UPDATE xxabrl.xxabrl_navi_ra_int_dist_stg2
                  SET interfaced_flag = 'Y',
                      interfaced_date = SYSDATE
                WHERE ROWID = art_r2.ROWID;

               fnd_file.put_line (fnd_file.output,
                                  'AR Line Distribution Inserted Successfully'
                                 );
            EXCEPTION
               WHEN OTHERS
               THEN
                  v_error_msg :=
                        'Error-Rollback: Exception in Invoice Line-Distribution insert: '
                     || SQLERRM;
                  RAISE e_insert_int;
            END;
         --Commit;
         END LOOP;
      END LOOP;

      COMMIT;
      fnd_file.put_line
         (fnd_file.output,
             'Number of Records (Invoices Lines/Dist )Inserted in Interface Table :'
          || v_record_count
         );
      fnd_file.put_line
         (fnd_file.output,
          '........................................................................'
         );
   -----
   -- Run Auto Invoice to upload data into Oracle Receivables
   ----
   /*If v_record_count > 0 Then
     fnd_global.apps_initialize(user_id      => v_user_id,
                                resp_id      => v_resp_id,
                                resp_appl_id => v_appl_id);
     Commit;
     v_req_id := fnd_request.submit_request('AR',
                                            'RAXTRX',
                                            'Autoinvoice Import Program' ||
                                            P_BATCH_Source,
                                            NULL,
                                            FALSE,
                                            'MAIN',
                                            'T',
                                            1021,
                                            P_Batch_Source, -- Parameter Data source
                                            Sysdate,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            'N',
                                            'Y',
                                            NULL,
                                            p_org_id,
                                            CHR(0));
     Commit;
     fnd_file.put_line(fnd_file.output,
                       'Please see the output of Payables OPEN Invoice Import program request id :' ||
                       v_req_id);
     fnd_file.put_line(fnd_file.output,
                       '........................................................................');
   End If;*/
   EXCEPTION
      WHEN e_insert_int
      THEN
         fnd_file.put_line (fnd_file.output, v_error_msg);
         fnd_file.put_line
              (fnd_file.output,
               '*** Insert process Rollbacked, No data inserted to Interface'
              );
         ROLLBACK;
         x_ret_code := 1;
   END invoice_insert;

   FUNCTION account_seg_status (p_seg_value IN VARCHAR2, p_seg_desc IN VARCHAR2)
      RETURN VARCHAR2
   IS
      v_count   NUMBER := 0;
   BEGIN
      SELECT COUNT (ffvv.flex_value)
        INTO v_count
        FROM fnd_flex_values_vl ffvv, fnd_flex_value_sets ffvs
       WHERE UPPER (ffvs.flex_value_set_name) = UPPER (p_seg_desc)
         AND ffvs.flex_value_set_id = ffvv.flex_value_set_id
         AND ffvv.flex_value = p_seg_value;

      IF v_count = 1
      THEN
         RETURN NULL;
      ELSE
         RETURN 'Invalid Value';
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 'Invalid Value';
   END account_seg_status;
END xxabrl_baan_ar_tndr_inv_pkg; 
/


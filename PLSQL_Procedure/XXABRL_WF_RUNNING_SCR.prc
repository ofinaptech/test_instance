CREATE OR REPLACE PROCEDURE APPS.xxabrl_wf_running_scr (
   p_retcode   NUMBER,
   p_errbuf    VARCHAR2
)
IS
   m_mailerid           fnd_svc_components.component_id%TYPE;
   v_component_status   fnd_svc_components.component_status%TYPE;
   v_retcode            NUMBER;
   v_errbuf             VARCHAR2 (4000);
BEGIN
   SELECT component_id, component_status
     INTO m_mailerid, v_component_status
     FROM fnd_svc_components
    WHERE component_name = 'Workflow Notification Mailer';

   IF v_component_status <> 'RUNNING'
   THEN
      fnd_svc_component.start_component (m_mailerid, v_retcode, v_errbuf);
      COMMIT;
   END IF;

   fnd_file.put_line (fnd_file.output, 'Component id' || ' ' || m_mailerid);
   fnd_file.put_line (fnd_file.output,
                       'Component Status' || ' ' || v_component_status
                      );
EXCEPTION
   WHEN OTHERS
   THEN
      fnd_file.put_line (fnd_file.LOG, 'ERROR in Component Status');
END; 
/


CREATE OR REPLACE package body APPS.XXABRL_NAV_AR_RECONCILE_PKG  is


/*
=========================================================================================================
||   Filename   : XXABRL_GL_INTER_PKG.sql
||   Description : Script is used to Load GL Data (GL Conversion)
||
||   Version     Date            Author              Modification
||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
||   1.0.0        23-sep-2008    SHAILESH BHARAMBE      New Development
||   1.0.1        23-sep-2008    SHAILESH BHARAMBE      New Development
||   1.0.2        24-sep-09      Naresh Hasti           Added the total for last OU   
||   1.0.3        10-dec-09      Naresh Hasti           Added the Dist Amout                     
||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
||
||   Usage : This script is used to upload data in apps.GL_INTERFACE TABLE
||
========================================================================================================*/


 PROCEDURE MAIN_PROC(x_err_buf           OUT VARCHAR2
                      ,x_ret_code          OUT NUMBER
            ,p_from_trx_date in varchar2
            ,p_to_trx_date  in varchar2
            ,p_org_id in number) is
begin 

--- execute the given report procedure here 
XXABRL_NAV_AR_AMT_DIFF_PROC(p_from_trx_date,p_to_trx_date,p_org_id);
end MAIN_PROC;  


 PROCEDURE XXABRL_NAV_AR_AMT_DIFF_PROC(p_from_trx_date in varchar2, p_to_trx_date varchar2,p_org_id in number) is
 
 cursor cur_amt_diff(cp_from_trx_date varchar2,cp_to_trx_date varchar2,cp_org_id number) is
 SELECT 
DATA.name ,
DATA.TRX_DATE ,
DATA.customer_name,
DATA.TRX_COUNT,
DATA.trx_amt,
DATA.dist_amt,
DATA.receipt_amt ,
DATA.trx_amt - DATA.dist_amt  LD_DIFF,
DATA.trx_amt - DATA.receipt_amt  LR_DIFF
FROM
(
SELECT hou.name,GL_DATE,TRX_DATE,customer_name,COUNT(ril.Interface_line_attribute1) TRX_COUNT ,SUM(ril.amount) trx_amt,
NVL(SUM(to_number(rid.amount)),0) dist_amt,
(SELECT NVL(SUM(receipt_amount),0) rec_amt FROM  apps. XXABRL_NAVI_AR_RECEIPT_INT 
WHERE sales_transaction_number = ril.Interface_line_attribute1
 )  receipt_amt
FROM apps.XXABRL_NAVI_RA_INT_LINES_ALL ril
,apps.XXABRL_NAVI_RA_INT_DIST_ALL RID
, hr_operating_units hou
WHERE trx_date BETWEEN  TO_DATE(cp_from_trx_date,'yyyy/mm/dd hh24:mi:ss') AND TO_DATE(cp_to_trx_date,'yyyy/mm/dd hh24:mi:ss')
AND ril.org_code=hou.short_code
AND RIL.INTERFACE_LINE_ATTRIBUTE1=RID.INTERFACE_LINE_ATTRIBUTE1(+)
AND RIL.INTERFACE_LINE_ATTRIBUTE2=RID.INTERFACE_LINE_ATTRIBUTE2(+)
AND hou.organization_id = nvl(cp_org_id,hou.organization_id)
GROUP BY  hou.name,GL_DATE,TRX_DATE,customer_name,ril.Interface_line_attribute1
) DATA
ORDER BY DATA.name,DATA.customer_name,DATA.TRX_DATE;



v_store_sum number:=0;
v_org_sum number :=0;
v_store_ld_sum number:=0;
v_org_ld_sum number :=0;
v_store_num number:=0;
v_org_name varchar2(100):='';
v_cnt number:=0; 
 begin

 fnd_file.put_line(fnd_file.output,'XXABRL Transaction and Receipt Amount difference'); 
 Fnd_File.put_line (Fnd_File.output, ' ');
      Fnd_File.put_line (Fnd_File.output, ' ');
      Fnd_File.put_line (Fnd_File.output,
                           'Operating Unit'
                         || CHR (9)
                                     || 'TRX Date'
                         || CHR (9)
                         || 'Store Number'
                         || CHR (9)
                         || 'Transaction Count'
                         || CHR (9)
                         || 'Line Amount'
                         || CHR (9)
                         || 'Dist Amount'
                         || CHR (9)
                         || 'Receipt Amount'
                         || CHR (9)
                         || 'Line and Dist Diff'
                         || CHR (9)
                         || 'Line and Receipt Diff'
                        );
 
 for i in cur_amt_diff(p_from_trx_date, p_to_trx_date,p_org_id)  loop
 /*
 
 
 
     v_store_sum number:=0;
v_org_sum number :=0;
v_store_num number:=0;
v_org_name varchar2(100):='';

   */

   
   if v_cnt = 0 then 
     v_store_num := i.customer_name;
     v_org_name  := i.name;
     end if;
     
     if i.customer_name <> v_store_num  then
     
      Fnd_File.put_line (Fnd_File.output,
                           ''
                         || CHR (9)
                                     || ''
                         || CHR (9)
                         || ''
                         || CHR (9)
                         || ''
                         || CHR (9)
                         || ''
                         || CHR (9)
                         || ''
                         || CHR (9)
                         || 'Total:'
                          || CHR (9)
                         || v_store_ld_sum
                         || CHR (9)
                         || v_store_sum
                        ); 
                         v_store_sum := 0;
                         v_store_ld_sum :=0;
     end if;
     
      if v_org_name  <> i.name  then
      
      Fnd_File.put_line (Fnd_File.output,
                           ''
                         || CHR (9)
                                     || ''
                         || CHR (9)
                         || ''
                         || CHR (9)
                         || ''
                         || CHR (9)
                                     || ''
                         || CHR (9)
                         || ''
                         || CHR (9)
                         || 'Total:'
                         || CHR (9)
                         || v_org_ld_sum
                         || CHR (9)
                         || v_org_sum
                        ); 
                        v_org_sum := 0;
                        v_org_ld_sum :=0;
     end if;
 
      
     Fnd_File.put_line (Fnd_File.output,
                           i.name
                         || CHR (9)
                                     || i.TRX_DATE 
                         || CHR (9)
                         || i.customer_name
                         || CHR (9)
                         || i.TRX_COUNT
                         || CHR (9)
                         || i.trx_amt
                         || CHR (9)
                         || i.dist_amt
                         || CHR (9)
                         || i.receipt_amt
                         || CHR (9)
                         ||i.LD_DIFF
                         || CHR (9)
                         ||i.LR_DIFF
                        );
                 
                             
                        
     v_store_sum   :=   v_store_sum  +  i.LR_DIFF;
     v_org_sum     :=   v_org_sum    +  i.LR_DIFF;
     
     v_store_ld_sum   :=   v_store_ld_sum  +  i.LD_DIFF;
     v_org_ld_sum     :=   v_org_ld_sum    +  i.LD_DIFF;
     
                           
     v_store_num := i.customer_name;
     v_org_name  := i.name;
     v_cnt :=v_cnt +1;
 
 end loop;
 
 Fnd_File.put_line (Fnd_File.output,
                           ''
                         || CHR (9)
                                     || ''
                         || CHR (9)
                         || ''
                         || CHR (9)
                         || ''
                         || CHR (9)
                                     || ''
                         || CHR (9)
                         || ''
                         || CHR (9)
                         || 'Total:'
                         || CHR (9)
                         || v_store_ld_sum
                         || CHR (9)
                         || v_store_sum
                        ); 
                         v_store_sum := 0;
                         v_store_ld_sum :=0;
                         
            Fnd_File.put_line (Fnd_File.output,
                           ''
                         || CHR (9)
                                     || ''
                         || CHR (9)
                         || ''
                         || CHR (9)
                         || ''
                         || CHR (9)
                                     || ''
                         || CHR (9)
                         || ''
                         || CHR (9)
                         || 'Total:'
                         || CHR (9)
                         || v_org_ld_sum
                         || CHR (9)
                         || v_org_sum
                        ); 
                        v_org_sum := 0;             
                        v_org_ld_sum :=0;
 
 end XXABRL_NAV_AR_AMT_DIFF_PROC;  
            

end XXABRL_NAV_AR_RECONCILE_PKG ; 
/


CREATE OR REPLACE PACKAGE BODY APPS.xxabrl_new_grn_details
IS
   PROCEDURE xxabrl_main_pkg (
      errbuf    OUT      VARCHAR2,
      retcode   OUT      NUMBER,
      p_type    IN       VARCHAR2
   )
   IS
   BEGIN
      IF p_type = 'H'
      THEN
         xxabrl_grn_headers;
      END IF;

      IF p_type = 'L'
      THEN
         xxabrl_grn_lines;
      END IF;

      IF p_type = 'T'
      THEN
         xxabrl_grn_trans;
      END IF;

      IF p_type = 'Tax'
      THEN
         xxabrl_grn_tax;
      END IF;
   END xxabrl_main_pkg;

   PROCEDURE xxabrl_grn_headers
   IS
   BEGIN
      EXECUTE IMMEDIATE 'truncate table  xxabrliproc.xxabrl_grn_header';

      FOR buf IN
         (SELECT DISTINCT rsh.shipment_header_id, rsh.last_update_date,
                          rsh.last_updated_by,
                          (SELECT    user_name
                                  || '-'
                                  || description
                             FROM apps.fnd_user
                            WHERE user_id = rsh.last_updated_by)
                                                        last_updated_by_name,
                          rsh.creation_date, rsh.created_by,
                          (SELECT    user_name
                                  || '-'
                                  || description
                             FROM apps.fnd_user
                            WHERE user_id = rsh.created_by) created_by_name,
                          rsh.vendor_id,
                          (SELECT segment1
                             FROM apps.ap_suppliers
                            WHERE vendor_id = rsh.vendor_id) vendor_code,
                          (SELECT vendor_name
                             FROM apps.ap_suppliers
                            WHERE vendor_id = rsh.vendor_id) vendor_name,
                          rsh.vendor_site_id,
                          (SELECT vendor_site_code
                             FROM apps.ap_supplier_sites_all
                            WHERE vendor_site_id =
                                          rsh.vendor_site_id)
                                                            vendor_site_code,
                          rsh.organization_id,
                          (SELECT NAME
                             FROM apps.hr_all_organization_units
                            WHERE organization_id = rsh.organization_id)
                                                          inventory_org_name,
                          rsh.receipt_num, rsh.packing_slip, rsh.employee_id,
                          (SELECT employee_number || '-'
                                  || full_name
                             FROM apps.per_all_people_f
                            WHERE person_id = rsh.employee_id
                              AND ROWNUM <= 1) receiver,
                          rsh.waybill_airbill_num, rsh.comments,
                          rsh.attribute1 supplier_invoice,
                          rsh.attribute2 freight_invoice,
                          rsh.attribute3 supp_inv_date,
                          rsh.attribute4 location_of_shipment,
                          rsh.attribute5 location_of_receipt,
                          rsh.attribute6 old_grn_no,
                          rsh.attribute7 old_grn_date, rsh.attribute8,
                          rsh.attribute9, rsh.attribute10, rsh.attribute11,
                          rsh.attribute12, rsh.attribute13, rsh.attribute14,
                          rsh.attribute15, rsh.ship_to_org_id,
                          (SELECT NAME
                             FROM apps.hr_operating_units
                            WHERE organization_id =
                                              ship_to_org_id)
                                                            ship_to_org_code
                     FROM apps.rcv_shipment_headers rsh
                    WHERE (   TRUNC (rsh.creation_date) = TRUNC (SYSDATE)
                           OR TRUNC (rsh.creation_date) = TRUNC (SYSDATE - 1)
                          )
                 ORDER BY rsh.shipment_header_id)
      LOOP
         INSERT INTO xxabrliproc.xxabrl_grn_header
                     (shipment_header_id, last_update_date,
                      last_updated_by, last_updated_by_name,
                      creation_date, created_by,
                      created_by_name, vendor_id, vendor_code,
                      vendor_name, vendor_site_id,
                      vendor_site_code, organization_id,
                      inventory_org_name, receipt_num,
                      packing_slip, employee_id, receiver,
                      waybill_airbill_num, comments,
                      supplier_invoice, freight_invoice,
                      supp_inv_date, location_of_shipment,
                      location_of_receipt, old_grn_no,
                      old_grn_date, attribute8, attribute9,
                      attribute10, attribute11, attribute12,
                      attribute13, attribute14, attribute15,
                      ship_to_org_id, ship_to_org_code, process_flag
                     )
              VALUES (buf.shipment_header_id, buf.last_update_date,
                      buf.last_updated_by, buf.last_updated_by_name,
                      buf.creation_date, buf.created_by,
                      buf.created_by_name, buf.vendor_id, buf.vendor_code,
                      buf.vendor_name, buf.vendor_site_id,
                      buf.vendor_site_code, buf.organization_id,
                      buf.inventory_org_name, buf.receipt_num,
                      buf.packing_slip, buf.employee_id, buf.receiver,
                      buf.waybill_airbill_num, buf.comments,
                      buf.supplier_invoice, buf.freight_invoice,
                      buf.supp_inv_date, buf.location_of_shipment,
                      buf.location_of_receipt, buf.old_grn_no,
                      buf.old_grn_date, buf.attribute8, buf.attribute9,
                      buf.attribute10, buf.attribute11, buf.attribute12,
                      buf.attribute13, buf.attribute14, buf.attribute15,
                      buf.ship_to_org_id, buf.ship_to_org_code, 'N'
                     );

         COMMIT;
      END LOOP;
   END xxabrl_grn_headers;

   PROCEDURE xxabrl_grn_lines
   IS
   BEGIN
      EXECUTE IMMEDIATE 'truncate table xxabrliproc.xxabrl_grn_lines ';

      FOR buf IN
         (SELECT DISTINCT rsl.shipment_line_id, rsl.last_update_date,
                          rsl.last_updated_by,
                          (SELECT    user_name
                                  || '-'
                                  || description
                             FROM apps.fnd_user
                            WHERE user_id = rsl.last_updated_by)
                                                        last_updated_by_name,
                          rsl.creation_date, rsl.created_by,
                          (SELECT    user_name
                                  || '-'
                                  || description
                             FROM apps.fnd_user
                            WHERE user_id = rsl.created_by) created_by_name,
                          rsl.shipment_header_id, rsl.line_num,
                          rsl.category_id,
                          (SELECT    segment1
                                  || '.'
                                  || segment2
                                  || '.'
                                  || segment3
                             FROM apps.mtl_categories
                            WHERE category_id = rsl.category_id)
                                                               category_code,
                          rsl.quantity_shipped, rsl.quantity_received,
                          rsl.unit_of_measure, rsl.item_id,
                          rsl.item_description,
                          (SELECT segment1
                             FROM apps.mtl_system_items_b
                            WHERE inventory_item_id = rsl.item_id
                              AND organization_id = 97) item_code,
                          rsl.po_header_id, rsl.po_line_id,
                          rsl.po_line_location_id, rsl.po_distribution_id,
                          rsl.deliver_to_person_id,
                          (SELECT    employee_number
                                  || '-'
                                  || full_name
                             FROM apps.per_all_people_f
                            WHERE person_id = rsl.deliver_to_person_id
                              AND ROWNUM <= 1) deliver_to_person,
                          rsl.employee_id,
                          (SELECT employee_number || '-'
                                  || full_name
                             FROM apps.per_all_people_f
                            WHERE person_id = rsl.employee_id
                              AND ROWNUM <= 1) receiver,
                          rsl.to_organization_id,
                          (SELECT NAME
                             FROM apps.hr_operating_units
                            WHERE organization_id =
                                           rsl.to_organization_id)
                                                                 to_org_code,
                          rsl.deliver_to_location_id,
                          (SELECT location_code
                             FROM apps.hr_locations
                            WHERE location_id =
                                     rsl.deliver_to_location_id)
                                                    deliver_to_location_code,
                          rsl.comments, rsl.primary_unit_of_measure,
                          rsl.ship_to_location_id,
                          (SELECT location_code
                             FROM apps.hr_locations
                            WHERE location_id =
                                     rsl.ship_to_location_id)
                                                       ship_to_location_code,
                          rsl.secondary_quantity_received,
                          rsl.amount_received, rsl.attribute10,
                          rsl.attribute11
                     FROM apps.rcv_shipment_lines rsl
                    WHERE 1 = 1
                      AND (   TRUNC (rsl.creation_date) = TRUNC (SYSDATE)
                           OR TRUNC (rsl.creation_date) = TRUNC (SYSDATE - 1)
                          ))
--           ORDER BY        rsl.requisition_line_id,
--                          rsl.shipment_header_id,
--                          rsl.line_num)
      LOOP
         INSERT INTO xxabrliproc.xxabrl_grn_lines
                     (shipment_line_id, last_update_date,
                      last_updated_by, last_updated_by_name,
                      creation_date, created_by,
                      created_by_name, shipment_header_id,
                      line_num, category_id, category_code,
                      quantity_shipped, quantity_received,
                      unit_of_measure, item_id,
                      item_description, item_code, po_header_id,
                      po_line_id, po_line_location_id,
                      po_distribution_id, deliver_to_person_id,
                      deliver_to_person, employee_id, receiver,
                      to_organization_id, to_org_code,
                      deliver_to_location_id,
                      deliver_to_location_code, comments,
                      primary_unit_of_measure, ship_to_location_id,
                      ship_to_location_code,
                      secondary_quantity_received, amount_received,
                      attribute10, attribute11, process_flag
                     )
              VALUES (buf.shipment_line_id, buf.last_update_date,
                      buf.last_updated_by, buf.last_updated_by_name,
                      buf.creation_date, buf.created_by,
                      buf.created_by_name, buf.shipment_header_id,
                      buf.line_num, buf.category_id, buf.category_code,
                      buf.quantity_shipped, buf.quantity_received,
                      buf.unit_of_measure, buf.item_id,
                      buf.item_description, buf.item_code, buf.po_header_id,
                      buf.po_line_id, buf.po_line_location_id,
                      buf.po_distribution_id, buf.deliver_to_person_id,
                      buf.deliver_to_person, buf.employee_id, buf.receiver,
                      buf.to_organization_id, buf.to_org_code,
                      buf.deliver_to_location_id,
                      buf.deliver_to_location_code, buf.comments,
                      buf.primary_unit_of_measure, buf.ship_to_location_id,
                      buf.ship_to_location_code,
                      buf.secondary_quantity_received, buf.amount_received,
                      buf.attribute10, buf.attribute11, 'N'
                     );

         COMMIT;
      END LOOP;
   END xxabrl_grn_lines;

   PROCEDURE xxabrl_grn_trans
   IS
   BEGIN
      EXECUTE IMMEDIATE 'truncate table xxabrliproc.xxabrl_grn_transac';

      FOR buf IN
         (SELECT   rt.transaction_id, rt.last_update_date,
                   rt.last_updated_by,
                   (SELECT user_name || '-'
                           || description
                      FROM apps.fnd_user
                     WHERE user_id = rt.last_updated_by)
                                                        last_updated_by_name,
                   rt.creation_date, rt.created_by,
                   (SELECT user_name || '-' || description
                      FROM apps.fnd_user
                     WHERE user_id = rt.created_by) created_by_name,
                   rt.transaction_type, rt.transaction_date, rt.quantity,
                   rt.unit_of_measure, rt.shipment_header_id,
                   rt.shipment_line_id, rt.user_entered_flag,
                   rt.destination_type_code, rt.primary_quantity,
                   rt.employee_id,
                   (SELECT employee_number || '-' || full_name
                      FROM apps.per_all_people_f
                     WHERE person_id = rt.employee_id
                           AND ROWNUM <= 1) receiver,
                   rt.po_header_id, rt.po_line_id, rt.po_line_location_id,
                   rt.po_distribution_id, rt.po_unit_price, rt.currency_code,
                   rt.deliver_to_person_id,
                   (SELECT    employee_number
                           || '-'
                           || full_name
                      FROM apps.per_all_people_f
                     WHERE person_id = rt.deliver_to_person_id AND ROWNUM <= 1)
                                                           deliver_to_person,
                   rt.deliver_to_location_id,
                   (SELECT location_code
                      FROM apps.hr_locations
                     WHERE location_id =
                              rt.deliver_to_location_id)
                                                    deliver_to_location_code,
                   rt.vendor_id,
                   (SELECT segment1 || '-' || vendor_name
                      FROM apps.ap_suppliers
                     WHERE vendor_id = rt.vendor_id) vendor_name,
                   rt.vendor_site_id,
                   (SELECT vendor_site_code
                      FROM apps.ap_supplier_sites_all
                     WHERE vendor_site_id =
                                           rt.vendor_site_id)
                                                            vendor_site_code,
                   rt.organization_id,
                   (SELECT NAME
                      FROM apps.hr_operating_units
                     WHERE organization_id = rt.organization_id) org_code,
                   rt.location_id,
                   (SELECT location_code
                      FROM apps.hr_locations
                     WHERE location_id = rt.location_id) location_code,
                   rt.comments, rt.quantity_billed, rt.amount_billed,
                   rt.attribute1, rt.attribute2, rt.attribute3,
                   rt.attribute4, rt.attribute5, rt.attribute6,
                   rt.attribute7, rt.attribute8, rt.attribute9,
                   rt.attribute10 shortage_excess,
                   rt.attribute11 invoice_qty, rt.attribute12,
                   rt.attribute13, rt.attribute14, rt.attribute15
              FROM apps.rcv_transactions rt
             WHERE 1 = 1
               AND (   TRUNC (rt.creation_date) = TRUNC (SYSDATE)
                    OR TRUNC (rt.creation_date) = TRUNC (SYSDATE - 1)
                   )
--   AND rt.shipment_line_id = 13697916
          ORDER BY rt.shipment_line_id, rt.transaction_id)
      LOOP
         INSERT INTO xxabrliproc.xxabrl_grn_transac
                     (transaction_id, last_update_date,
                      last_updated_by, last_updated_by_name,
                      creation_date, created_by,
                      created_by_name, transaction_type,
                      transaction_date, quantity,
                      unit_of_measure, shipment_header_id,
                      shipment_line_id, user_entered_flag,
                      destination_type_code, primary_quantity,
                      employee_id, receiver, po_header_id,
                      po_line_id, po_line_location_id,
                      po_distribution_id, po_unit_price,
                      currency_code, deliver_to_person_id,
                      deliver_to_person, deliver_to_location_id,
                      deliver_to_location_code, vendor_id,
                      vendor_name, vendor_site_id,
                      vendor_site_code, organization_id,
                      org_code, location_id, location_code,
                      comments, quantity_billed, amount_billed,
                      attribute1, attribute2, attribute3,
                      attribute4, attribute5, attribute6,
                      attribute7, attribute8, attribute9,
                      shortage_excess, invoice_qty, attribute12,
                      attribute13, attribute14, attribute15, process_flag
                     )
              VALUES (buf.transaction_id, buf.last_update_date,
                      buf.last_updated_by, buf.last_updated_by_name,
                      buf.creation_date, buf.created_by,
                      buf.created_by_name, buf.transaction_type,
                      buf.transaction_date, buf.quantity,
                      buf.unit_of_measure, buf.shipment_header_id,
                      buf.shipment_line_id, buf.user_entered_flag,
                      buf.destination_type_code, buf.primary_quantity,
                      buf.employee_id, buf.receiver, buf.po_header_id,
                      buf.po_line_id, buf.po_line_location_id,
                      buf.po_distribution_id, buf.po_unit_price,
                      buf.currency_code, buf.deliver_to_person_id,
                      buf.deliver_to_person, buf.deliver_to_location_id,
                      buf.deliver_to_location_code, buf.vendor_id,
                      buf.vendor_name, buf.vendor_site_id,
                      buf.vendor_site_code, buf.organization_id,
                      buf.org_code, buf.location_id, buf.location_code,
                      buf.comments, buf.quantity_billed, buf.amount_billed,
                      buf.attribute1, buf.attribute2, buf.attribute3,
                      buf.attribute4, buf.attribute5, buf.attribute6,
                      buf.attribute7, buf.attribute8, buf.attribute9,
                      buf.shortage_excess, buf.invoice_qty, buf.attribute12,
                      buf.attribute13, buf.attribute14, buf.attribute15, 'N'
                     );

         COMMIT;
      END LOOP;
   END xxabrl_grn_trans;

   PROCEDURE xxabrl_grn_tax
   IS
   BEGIN
      EXECUTE IMMEDIATE 'truncate table xxabrliproc.xxabrl_grn_taxes';

      FOR buf IN
         (SELECT DISTINCT rsh.shipment_header_id,
                          jtl.trx_line_id shipment_line_id,
                          jtl.trx_loc_line_id transaction_id,
                          jtl.trx_id tax_id, jtl.tax_rate_code tax_name,
                          jtc.tax_category_desc tax_descr,
                          jtt.tax_type_name tax_type, NULL modvat_flag,
                          jtl.party_id vendor_id,
                          (SELECT segment1
                             FROM apps.ap_suppliers
                            WHERE vendor_id = jtl.party_id) vendor_code,
                          (SELECT vendor_name
                             FROM apps.ap_suppliers
                            WHERE vendor_id = jtl.party_id) vendor_name,
                          jtl.tax_line_num tax_line_no,
                          jtl.trx_currency_code currency,
                          jtl.tax_rate_percentage tax_rate,
                          NVL
                             (SUM (DECODE (jtl.rec_tax_amt_tax_curr,
                                           '0', jtl.nrec_tax_amt_tax_curr,
                                           jtl.rec_tax_amt_tax_curr
                                          )
                                  ),
                              0
                             ) tax_amount,
                          jtl.creation_date, jtl.created_by,
                          (SELECT    user_name
                                  || '-'
                                  || description
                             FROM apps.fnd_user
                            WHERE user_id = jtl.created_by) created_by_name,
                          jtl.last_updated_by,
                          (SELECT    user_name
                                  || '-'
                                  || description
                             FROM apps.fnd_user
                            WHERE user_id = jtl.last_updated_by)
                                                        last_updated_by_name,
                          gcc.concatenated_segments tax_account_segments
                     FROM apps.rcv_shipment_headers rsh,
                          apps.jai_tax_lines jtl,
                          apps.jai_tax_categories jtc,
                          apps.jai_tax_category_lines jtcl,
                          apps.jai_tax_types jtt,
                          apps.jai_tax_rates jtr,
                          apps.gl_code_combinations_kfv gcc,
                          apps.jai_tax_accounts jta
                    WHERE 1 = 1
                      AND rsh.shipment_header_id = jtl.trx_id
                      AND (   TRUNC (rsh.creation_date) = TRUNC (SYSDATE)
                           OR TRUNC (rsh.creation_date) = TRUNC (SYSDATE - 1)
                          )
                      AND gcc.code_combination_id = jta.interim_recovery_ccid
                      AND jtc.tax_category_id = jtcl.tax_category_id
                      AND jtcl.tax_type_id = jtt.tax_type_id
                      AND jtr.tax_rate_id = jtl.tax_rate_id
                      AND jtcl.tax_rate_id = jtr.tax_rate_id
                      AND rsh.organization_id = jta.org_id
                      AND jtl.tax_type_id = jta.tax_account_entity_id
                      AND tax_account_entity_code = 'TAX_TYPE'
                      AND jtl.entity_code = 'RCV_TRANSACTION'
                    --AND rsh.receipt_num = '937300001971'
                 --   AND TRUNC (rsh.creation_date) >= '01-nov-2018'
          GROUP BY        rsh.shipment_header_id,
                          jtl.trx_line_id,
                          jtl.trx_loc_line_id,
                          jtl.trx_id,
                          jtl.tax_rate_code,
                          jtc.tax_category_desc,
                          jtt.tax_type_name,
                          jtl.party_id,
                          jtl.tax_line_num,
                          jtl.trx_currency_code,
                          jtl.tax_rate_percentage,
                          jtl.creation_date,
                          jtl.created_by,
                          jtl.last_updated_by,
                          gcc.concatenated_segments
                 ORDER BY rsh.shipment_header_id,
                          jtl.trx_line_id,
                          jtl.tax_line_num)
      LOOP
         INSERT INTO xxabrliproc.xxabrl_grn_taxes
                     (shipment_header_id, shipment_line_id,
                      transaction_id, tax_id, tax_name,
                      tax_descr, tax_type, modvat_flag,
                      vendor_id, vendor_code, vendor_name,
                      tax_line_no, currency, tax_rate,
                      tax_amount, creation_date, created_by,
                      created_by_name, last_updated_by,
                      last_updated_by_name, tax_account_segments,
                      process_flag
                     )
              VALUES (buf.shipment_header_id, buf.shipment_line_id,
                      buf.transaction_id, buf.tax_id, buf.tax_name,
                      buf.tax_descr, buf.tax_type, buf.modvat_flag,
                      buf.vendor_id, buf.vendor_code, buf.vendor_name,
                      buf.tax_line_no, buf.currency, buf.tax_rate,
                      buf.tax_amount, buf.creation_date, buf.created_by,
                      buf.created_by_name, buf.last_updated_by,
                      buf.last_updated_by_name, buf.tax_account_segments,
                      'N'
                     );

         COMMIT;
      END LOOP;
   END xxabrl_grn_tax;
END xxabrl_new_grn_details; 
/


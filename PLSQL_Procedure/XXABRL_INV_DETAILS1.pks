CREATE OR REPLACE PACKAGE APPS.xxabrl_inv_details1
IS
   PROCEDURE xxabrl_main_pkg (
      errbuf    OUT      VARCHAR2,
      retcode   OUT      NUMBER,
      p_type    IN       VARCHAR2
   );
   PROCEDURE xxabrl_inv_daily_data;
   PROCEDURE xxabrl_inv_dump;
END xxabrl_inv_details1; 
/


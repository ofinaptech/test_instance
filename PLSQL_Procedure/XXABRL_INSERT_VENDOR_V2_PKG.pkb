CREATE OR REPLACE PACKAGE BODY APPS.XXABRL_INSERT_VENDOR_V2_PKG AS

/*
  =========================================================================================================
  ||   Filename   : XXABRL_INSERT_VENDOR_V2_PKG.sql
  ||   Description : Script is used to mold supplier migration to OFIN.
  ||
  ||   Version     Date            Author              Modification
  ||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
  ||   1.0.0        13-MON-2008    Hans Raj Kasana      New Development
  ||   1.0.1       04-sep-09    Praveen Bonthala    commented the PUY,PAY,HO,RET site flags and term date basis
  ||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
  ||
  ||
  ========================================================================================================*/


  PROCEDURE MAIN(Errbuf   OUT VARCHAR2,
                 RetCode  OUT NUMBER,
                 p_action varchar2) IS
    vRetCode Number;
  BEGIN
    --Validate Data
    VALIDATE_VENDOR_INFO(vRetCode);
    /*if vRetCode <> 0 then
    RetCode :=1;
    End if;*/
    --call insert
    if p_action = 'N' then
      XXABRL_INSERT_VENDOR_INFO;
    end if;
  END MAIN;

  PROCEDURE VALIDATE_VENDOR_INFO(RetCode OUT NUMBER) AS
    x_msg              varchar2(2000);
    v_error_hmsg       varchar2(2000);
    v_error_smsg       varchar2(2000);
    v_error_cmsg       varchar2(2000);
    v_error_tmsg       varchar2(2000);
    v_Error_Sup_Count  number;
    v_Error_site_Count number;
    v_valid_sup_Count  number;
    v_valid_site_Count number;
    X_SHIP_TO_LOC_ID   NUMBER;
    X_BILL_TO_LOC_ID   NUMBER;
    X_TERM_ID          NUMBER;
    X_EMPLOYEE_ID      NUMBER;
    x_pay_group        varchar2(240);
    v_lookup_code      varchar2(240);
    ln_TDS_LOOKUP_CODE number;

    v_vendor_id      Number;
    v_vendor_site_id Number;

    /*Cursor c1 is
    select ROWID, SUPPLIERS.*
      FROM XXABRL_SUPPLIER_INT suppliers
     where NVL(SUPPLIERS.Process_Flag, 'N') in ('N', 'E');*/
    Cursor c2 is
      select ROWID, sup_sites.*
        FROM XXABRL_SUPPLIER_SITES_INT sup_sites
       where NVL(sup_sites.Process_Flag, 'N') in ('N','E');
    Cursor c3 is
      select ROWID, sup_conts.*
        FROM XXABRL_SUP_SITE_CONT_INT sup_conts
       where NVL(sup_conts.Process_Flag, 'N') in ('N', 'E');

    /*Cursor c4 is
      select *
        from XXABRL_SUPPLIER_TAXINFO_INT
       where process_flag in ('N', 'E');*/

  BEGIN
    FND_FILE.PUT_LINE(FND_FILE.LOG, 'vendor validation start');

    /*ln_TDS_LOOKUP_CODE := 0;

    FND_FILE.PUT_LINE(FND_FILE.LOG,
                      '#### Validation Start for Tax Info ####');

    For c_cur4 in C4 Loop
      if c_cur4.TDS_VENDOR_TYPE_LOOKUP_CODE is null then
        FND_FILE.PUT_LINE(FND_FILE.LOG,
                          'TDS VENDOR TYPE Lookup Code is NULL');

        ln_TDS_LOOKUP_CODE := ln_TDS_LOOKUP_CODE + 1;

      Else

        BEGIN
          --        FND_FILE.PUT_LINE(FND_FILE.LOG, 'Debug C_CUR4');
          SELECT LOOKUP_CODE
            into v_lookup_code
            FROM FND_LOOKUP_VALUES_VL
           WHERE lookup_type = 'JAI_TDS_VENDOR_TYPE'
             and upper(LOOKUP_CODE) =
                 upper(c_cur4.TDS_VENDOR_TYPE_LOOKUP_CODE);
          --        FND_FILE.PUT_LINE(FND_FILE.LOG, 'debug after select');

          update XXABRL_SUPPLIER_TAXINFO_INT sti
             set DVD_TDS_VEND_TYP_LOKUP_CODE = v_lookup_code,
                 process_flag                = 'V'
           where sti.sup_name = c_cur4.sup_name
             and sti.sup_site = c_cur4.sup_site;
          --## use rowid

          FND_FILE.PUT_LINE(FND_FILE.LOG,
                            'Valid TDS Vendor Type Lookup Code ' ||
                            upper(c_cur4.TDS_VENDOR_TYPE_LOOKUP_CODE));
          --commit;

        EXCEPTION
          when no_data_found then
            FND_FILE.PUT_LINE(FND_FILE.LOG,
                              'TDS VENDOR TYPE Lookup Code not defined for: ' ||
                              upper(c_cur4.TDS_VENDOR_TYPE_LOOKUP_CODE));
            v_error_tmsg       := v_error_tmsg ||
                                  'TDS VENDOR TYPE Lookup Code not defined for: ' ||
                                  c_cur4.TDS_VENDOR_TYPE_LOOKUP_CODE;
            ln_TDS_LOOKUP_CODE := ln_TDS_LOOKUP_CODE + 1;
          When too_many_rows then
            FND_FILE.PUT_LINE(FND_FILE.LOG,
                              'Too many rows for TDS VENDOR TYPE Lookup Code: ' ||
                              upper(c_cur4.TDS_VENDOR_TYPE_LOOKUP_CODE));
            ln_TDS_LOOKUP_CODE := ln_TDS_LOOKUP_CODE + 1;
            v_error_tmsg       := v_error_tmsg ||
                                  'too many TDS VENDOR TYPE Lookup Codes defined for: ' ||
                                  c_cur4.TDS_VENDOR_TYPE_LOOKUP_CODE;
        END;
        if v_error_tmsg is not null then
          update XXABRL_SUPPLIER_TAXINFO_INT sti
             set process_flag = 'E', ERROR_MSG = v_error_tmsg
           where sti.sup_name = c_cur4.sup_name
             and sti.sup_site = c_cur4.sup_site;

        end if;
      End if;
      v_error_tmsg := NULL;
      --      FND_FILE.PUT_LINE(FND_FILE.LOG, 'after exception');

    End Loop;

    FND_FILE.PUT_LINE(FND_FILE.LOG,
                      '@@@ In-Valid/NULL Tax Code count:' ||
                      ln_TDS_LOOKUP_CODE);
    FND_FILE.PUT_LINE(FND_FILE.LOG,
                      '#### Validation ENDS for Tax Info ####');
  */
    --==*/
    For c_cur2 in C2 loop
      FND_FILE.PUT_LINE(FND_FILE.LOG,
                        'Site validation Starts ' ||
                        c_cur2.vendor_site_code);

      -- Operating unit validation

      if c_cur2.OPERATING_UNIT is null then
        v_error_smsg := v_error_smsg || ',' || 'OPERATING_UNIT is null';
      else
        validate_operating_unit(c_cur2.OPERATING_UNIT, x_msg);

        if x_msg is not null then
          v_error_smsg := v_error_smsg || ',' || x_msg;
        End if;
      end if;
      -- vendor number range validation for each vendor type
      FND_FILE.PUT_LINE(FND_FILE.LOG, '****vendor range validation starts');

      --VALIDATION FOR VENDOR NAME
      if c_cur2.VENDOR_NAME is null then
        v_error_smsg := v_error_smsg || ',' || 'Vendor name is null';
      else

        Begin

          select vendor_id
            into v_vendor_id
            from po_vendors
           where upper(vendor_name) = upper(c_cur2.VENDOR_NAME);

        Exception

          when no_data_found then
            v_error_smsg := v_error_smsg || ',' || 'vendor not found';

          when too_many_rows then
            v_error_smsg := v_error_smsg || ',' || 'Too Many Vendors Exist';

          when others then
            v_error_smsg := v_error_smsg || ',' ||
                            'Exception in Vendor Name: ' || SQLERRM;

        End;

      end if;
      -- vendor_site_code  validation
      if c_cur2.vendor_site_code is null then
        v_error_smsg := v_error_smsg || ',' || 'vendor site code is null';
      else
        validate_vendor_site_code(c_cur2.vendor_site_code,
                                    c_cur2.VENDOR_NAME,
                                    c_cur2.OPERATING_UNIT,
                                    x_msg);
        if x_msg is not null then
          v_error_smsg := v_error_smsg || ',' || x_msg;
        End if;
      end if;
      --VALIDATION FOR ADDRESS LINE1
      if c_cur2.ADDRESS_LINE1 is null then
        v_error_smsg := v_error_smsg || ',' || 'ADDRESS_LINE1 is null';
      end if;
      /* --VALIDATION FOR ADDRESS LINE2
      if c_cur2.ADDRESS_LINE2 is null then
        v_error_smsg := v_error_smsg || ',' || 'ADDRESS_LINE2 is null';
      end if;*/
      /*  --VALIDATION FOR CITY
      if c_cur2.city is null then
        v_error_smsg := v_error_smsg || ',' || 'CITY is null';
      end if;*/
      --VALIDATION FOR STATE
      if c_cur2.state is null then
       v_error_smsg := v_error_smsg || ',' || 'STATE is null';
       else
       validate_state_name(c_cur2.state,x_msg);
       if x_msg is not null then
          v_error_smsg := v_error_smsg || ',' || x_msg;
        End if;
      end if;
      -- COUNTRY CODE validation
      if c_cur2.country is null then
        v_error_smsg := v_error_smsg || ',' || 'COUNTRY is null';
      else
        validate_country_code(c_cur2.country, x_msg);
        if x_msg is not null then
          v_error_smsg := v_error_smsg || ',' || x_msg;
        End if;
      end if;
      -- vendor terms validation
      if c_cur2.terms_name is null then
        v_error_smsg := v_error_smsg || ',' || 'terms name is null';
      else
        validate_terms_name(c_cur2.terms_name, x_msg);
        if x_msg is not null then
          v_error_smsg := v_error_smsg || ',' || x_msg;
        End if;
      end if;

      -- PAYMENT_METHOD_LOOKUP_CODE validation
      if c_cur2.PAYMENT_METHOD_LOOKUP_CODE is null then
        v_error_smsg := v_error_smsg || ',' ||
                        'PAYMENT_METHOD_LOOKUP_CODE is null';
      else
        validate_payment_method(c_cur2.PAYMENT_METHOD_LOOKUP_CODE, x_msg);
        if x_msg is not null then
          v_error_smsg := v_error_smsg || ',' || x_msg;
        End if;
      end if;

      -- PAY_GROUP_LOOKUP_CODE validation
      if c_cur2.PAY_GROUP_LOOKUP_CODE is null then
        v_error_smsg := v_error_smsg || ',' ||
                        'PAY_GROUP_LOOKUP_CODE is null';
      else
        validate_pay_group(c_cur2.PAY_GROUP_LOOKUP_CODE,
                           x_pay_group,
                           x_msg);
        if x_msg is not null then
          v_error_smsg := v_error_smsg || ',' || x_msg;
        End if;
      end if;

      -- DERIVATION OF  SHIP_TO_LOCTION_ID
      if c_cur2.SHIP_TO_LOCATION_CODE is not null then
        DERIVE_SHIT_TO_LOCATION_ID(c_cur2.SHIP_TO_LOCATION_CODE,
                                   X_SHIP_TO_LOC_ID,
                                   X_MSG);
        if X_MSG is not null then
          v_error_smsg := v_error_smsg || ',' || X_MSG;
        end if;
      ELSE
        v_error_smsg := v_error_smsg || 'SHIP TO location code is null';
      end if;

      -- DERIVATION OF  BILL_TO_LOCTION_ID
      if c_cur2.BILL_TO_LOCATION_CODE is not null then
        DERIVE_BILL_TO_LOCATION_ID(c_cur2.BILL_TO_LOCATION_CODE,
                                   X_BILL_TO_LOC_ID,
                                   X_MSG);

        if X_MSG is not null then
          v_error_smsg := v_error_smsg || ',' || X_MSG;
        end if;
      ELSE
        v_error_smsg := v_error_smsg || ',' ||
                        'BILL TO location code is null';
      end if;

      -- DERIVATION OF  TERM_ID
      if c_cur2.TERMS_NAME is not null then
        DERIVE_TERM_ID(c_cur2.TERMS_NAME, X_TERM_ID, X_MSG);
      ELSE
        v_error_smsg := v_error_smsg || ',' || X_MSG;
      end if;

     ---State Name Validation
       if c_cur2.vendor_site_code is null then
        v_error_smsg := v_error_smsg || ',' || 'vendor site code is null';
      else
        validate_vendor_site_code(c_cur2.vendor_site_code,
                                    c_cur2.VENDOR_NAME,
                                    c_cur2.OPERATING_UNIT,
                                    x_msg);
        if x_msg is not null then
          v_error_smsg := v_error_smsg || ',' || x_msg;
        End if;
      end if;

      /*---PUR Site Flag Validation
       if c_cur2.vendor_site_code like 'PUR%' then
        validate_pur_site_flag(c_cur2.operating_unit,
                               c_cur2.vendor_name,
                               x_msg);
        if x_msg is not null then
          v_error_smsg := v_error_smsg || ',' || x_msg;
        End if;
      end if;

       ---PAY Site Flag Validation
       if c_cur2.vendor_site_code like 'PAY%' then
        validate_pay_site_flag(c_cur2.operating_unit,
                               c_cur2.vendor_name,
                               x_msg);
        if x_msg is not null then
          v_error_smsg := v_error_smsg || ',' || x_msg;
        End if;
      end if;

       ---RET Site Flag Validation
       if c_cur2.vendor_site_code like 'RET%' then
        validate_ret_site_flag(c_cur2.operating_unit,
                               c_cur2.vendor_name,
                               x_msg);
        if x_msg is not null then
          v_error_smsg := v_error_smsg || ',' || x_msg;
        End if;
      end if;

       ---HO Site Flag Validation
       if c_cur2.vendor_site_code like 'HO%' then
        validate_ho_site_flag( c_cur2.operating_unit,
                               c_cur2.vendor_name,
                               c_cur2.purchasing_site_flag,
                               c_cur2.pay_site_flag,
                               c_cur2.rfq_only_site_flag,
                               x_msg);
        if x_msg is not null then
          v_error_smsg := v_error_smsg || ',' || x_msg;
        End if;
      end if;*/ ---Commented for loading the Merchandise Vendors 



      --Term Date Basis Validation

     /* if c_cur2.TERM_DATE_BASIS is not null then
         validate_terms_basis(c_cur2.term_date_basis, x_msg);
         if x_msg is not null then
         v_error_smsg := v_error_smsg || ',' || X_MSG;
         end if;
      end if;*/--Commented to load Merchandise Vendors

      --Validation for Tax Region
      if c_cur2.state is null then
       v_error_smsg := v_error_smsg || ',' || 'STATE is null';
       else
       validate_state_name(c_cur2.state,x_msg);
       if x_msg is not null then
          v_error_smsg := v_error_smsg || ',' || x_msg;
        End if;
      end if;


      --updating vendor site staging table
      if v_error_smsg is not null then
        FND_FILE.PUT_LINE(FND_FILE.LOG, '***Invalid Sites' || v_error_smsg);
        Update XXABRL_SUPPLIER_SITES_INT
           Set error_msg = v_error_smsg, process_flag = 'E'
         Where ROWID = c_cur2.ROWID;
        v_Error_site_Count := v_Error_Site_Count + 1;

      Else
        FND_FILE.PUT_LINE(FND_FILE.LOG, 'Valid Site');

        Update XXABRL_SUPPLIER_SITES_INT
           Set process_flag        = 'V',
               error_msg           = NULL,
               BILL_TO_LOCATION_ID = X_BILL_TO_LOC_ID,
               SHIP_TO_LOCATION_ID = X_SHIP_TO_LOC_ID,
               TERM_ID             = X_TERM_ID,
               DERIVED_PAY_GROUP   = X_PAY_GROUP,
               Vendor_id           = nvl(v_vendor_id,-1)
         Where ROWID = c_cur2.ROWID;
        v_valid_sup_Count := v_valid_Sup_Count + 1;
        Commit;
      End If;
      v_error_hmsg := null;
      v_error_smsg := null;
      v_vendor_id :=null;

    ---vendor sites contacts loop start

    end loop;

    For c_cur3 in C3 loop

      FND_FILE.PUT_LINE(FND_FILE.LOG,
                        'Contact Validation Starts for ' ||
                        c_cur3.VENDOR_NAME);
      -- Vendor Site code validation
      if c_cur3.vendor_site_code is null then
        v_error_cmsg := v_error_cmsg || ',' || 'Vendor Site Code is null';
      else

        Begin
    /*      select vendor_site_id
            into v_vendor_site_id
            from po_vendor_sites_all
           where vendor_site_code = c_cur3.vendor_site_code
           and vendor_id =(select vendor_id from po_vendors
           where upper(vendor_name) =upper(c_cur3.vendor_name)) ;*/


           select vendor_site_id
            into v_vendor_site_id
            from po_vendor_sites_all pvs
            ,org_organization_definitions ood
           where pvs.vendor_site_code =c_cur3.vendor_site_code
           and pvs.vendor_id  in (select vendor_id from po_vendors
           where upper(vendor_name) =upper(c_cur3.vendor_name))
           and  ood.ORGANIZATION_NAME=c_cur3.OPERATING_UNIT
           and ood.operating_unit=pvs.org_id;

          Begin

            select vendor_id
              into v_vendor_id
              from po_vendor_sites_all
             where vendor_site_id = v_vendor_site_id
             and vendor_id =(select vendor_id from po_vendors where upper(vendor_name) =upper(c_cur3.vendor_name)) ;



          Exception
            when no_data_found then
              v_error_cmsg := v_error_cmsg || ',' ||
                              'Site/Vendor not found ||' ||
                              c_cur3.vendor_site_code;

            when too_many_rows then
              v_error_cmsg := v_error_cmsg || ',' ||
                              'Too Many Site/Vendor ' ||
                              c_cur3.vendor_site_code;

            when others then
              v_error_cmsg := v_error_cmsg || ',' ||
                              'Exception in Site/Vendor: ' || SQLERRM;

          End;

        Exception
          when no_data_found then
            v_error_cmsg := v_error_cmsg || ',' || 'Site not found ||' ||
                            c_cur3.vendor_site_code||'('||c_cur3.vendor_name||')';

          when too_many_rows then
            v_error_cmsg := v_error_cmsg || ',' ||
                            'Too Many Vendors Sites(Vendor) ' ||
                            c_cur3.vendor_site_code||'('||c_cur3.vendor_name||')';

          when others then
            v_error_cmsg := v_error_cmsg || ',' ||
                            'Exception in Vendor Site: ' || SQLERRM;

        End;

      end if;

      -- Operating unit validation
      if c_cur3.OPERATING_UNIT is null then
        v_error_cmsg := v_error_cmsg || ',' || 'OPERATING_UNIT is null';
      end if;
      --VALIDATION FOR VENDOR NAME
      if c_cur3.VENDOR_NAME is null then
        v_error_cmsg := v_error_cmsg || ',' || 'Vendor name is null';
      end if;

      -- last name validation
      if c_cur3.LAST_NAME is null then
        v_error_cmsg := v_error_cmsg || ',' || 'Last name is null';
      end if;
      --updating vendor site contact staging table
      if v_error_cmsg is not null then
        FND_FILE.PUT_LINE(FND_FILE.LOG, '***Invalid Contact');
        Update XXABRL_SUP_SITE_CONT_INT
           Set error_msg = v_error_cmsg, process_flag = 'E'
         Where ROWID = c_cur3.ROWID;

        -- v_Error_Sup_Count := v_Error_Sup_Count + 1;

        Commit;

      Else
        FND_FILE.PUT_LINE(FND_FILE.LOG, 'Contact Valid');
        Update XXABRL_SUP_SITE_CONT_INT
           Set error_msg = NULL,
           process_flag = 'V',
           vendor_id = v_vendor_id,
           vendor_site_id = v_vendor_site_id
         Where ROWID = c_cur3.ROWID;
        v_valid_sup_Count := v_valid_Sup_Count + 1;
      End If;
      v_error_cmsg := null;
      FND_FILE.PUT_LINE(FND_FILE.LOG,
                        'Contact Validation Ends for ' ||
                        c_cur3.VENDOR_NAME);
    end loop;

    --==

    FND_FILE.PUT_LINE(FND_FILE.LOG,
                      '@@@Invalid Vendors ' || v_Error_sup_Count);
    FND_FILE.PUT_LINE(FND_FILE.LOG,
                      '@@@Invalid Sites ' || v_Error_site_Count);

    FND_FILE.PUT_LINE(FND_FILE.LOG, '***Validation Procedure Ends***');
  end VALIDATE_VENDOR_INFO;

  -------------------------------------------------------------
  PROCEDURE validate_terms_name(p_terms_name IN VARCHAR2,
                                v_error_smsg OUT varchar2) IS
    v_term_name varchar2(240);
  BEGIN
    SELECT name
      into v_term_name
      FROM ap_terms_tl
     WHERE upper(trim(name)) = upper(trim(p_terms_name))
       and sysdate between nvl(start_date_active, sysdate) and
           nvl(end_date_active, sysdate);
  EXCEPTION
    when no_data_found then
      v_error_smsg := v_error_smsg ||
                      'Payment Terms does not exist in Oracle Financials-->>';
    when too_many_rows then
      v_term_name  := null;
      v_error_smsg := v_error_smsg || 'Multiple Payment Terms found-->>';
    When Others Then
      v_term_name  := null;
      v_error_smsg := v_error_smsg || 'Invalid Payment Term -->>';
  END Validate_terms_name;

  -----------------------------------------------------------------------------

  -------------------commented  by Praveen on 04 sep 09------------------------------------------
  /*PROCEDURE validate_terms_basis(p_terms_basis IN VARCHAR2,
                                 v_error_smsg OUT varchar2) IS
    v_term_basis varchar2(240);
  BEGIN
      SELECT meaning
      into v_term_basis
      FROM fnd_lookup_values
     WHERE lookup_type = 'TERMS DATE BASIS'
      AND  meaning=p_terms_basis;

  EXCEPTION
    when no_data_found then
      v_error_smsg := v_error_smsg ||
                      'Term Date Basis does not exist in Oracle Financials-->>';
    when too_many_rows then
      v_term_basis  := null;
      v_error_smsg := v_error_smsg || 'Multiple Terms Date Basis found-->>';
    When Others Then
      v_term_basis  := null;
      v_error_smsg := v_error_smsg || 'Invalid Payment Term -->>';
  END Validate_terms_basis;*/  ---Commented for Loading the Merchandise Vendors  
------------------------------------------------------------------------------------

 PROCEDURE validate_state_name(p_state_name IN VARCHAR2,
                               v_error_smsg OUT varchar2) IS
    v_state_name varchar2(240);
  BEGIN

    SELECT meaning
      into v_state_name
      FROM fnd_lookup_values
     WHERE lookup_type = 'ABRL_STATE_CODE'
      AND  UPPER(meaning) = UPPER(p_state_name);

  EXCEPTION
    when no_data_found then
      v_error_smsg := v_error_smsg ||
                      'State name does not exist in Oracle Financials-->>';
    when too_many_rows then
      v_state_name  := null;
      v_error_smsg := v_error_smsg || 'Multiple States found-->>';
    When Others Then
      v_state_name  := null;
      v_error_smsg := v_error_smsg || 'Invalid Payment Term -->>';
  END Validate_state_name;


 -------------------commented by Praveen on 04 sep 09------------------------------------------
  /*PROCEDURE validate_pur_site_flag(p_operating_unit IN  VARCHAR2,
                                   p_vendor_name IN  VARCHAR2,
                                   v_error_smsg    OUT varchar2) IS
    v_pur_site_flag varchar2(1);
  BEGIN
   SELECT purchasing_site_flag
     INTO v_pur_site_flag
     FROM xxabrl_supplier_sites_int i
    WHERE vendor_site_code like 'PUR%'
      AND vendor_name =p_vendor_name
      AND operating_unit = p_operating_unit;
  EXCEPTION
    when no_data_found then
      v_error_smsg := v_error_smsg ||
                      'PUR site flag is not enabled';
    When Others Then
      v_pur_site_flag  := null;
      v_error_smsg := v_error_smsg || 'Invalid Pur Site Flag';
  END validate_pur_site_flag;

-------------------Added by Praveen------------------------------------------
  PROCEDURE validate_pay_site_flag(p_operating_unit IN  VARCHAR2,
                                   p_vendor_name   IN  VARCHAR2,
                                   v_error_smsg    OUT varchar2) IS
    v_pay_site_flag varchar2(1);
  BEGIN
   SELECT pay_site_flag
     INTO v_pay_site_flag
     FROM xxabrl_supplier_sites_int i
    WHERE vendor_site_code like 'PAY%'
     AND vendor_name =p_vendor_name
     AND operating_unit = p_operating_unit ;
  EXCEPTION
    when no_data_found then
      v_error_smsg := v_error_smsg ||
                      'PAY site flag is not enabled';
    When Others Then
      v_pay_site_flag  := null;
      v_error_smsg := v_error_smsg || 'Invalid Pay Site Flag';
  END validate_pay_site_flag;


-------------------Added by Praveen------------------------------------------
  PROCEDURE validate_ret_site_flag(p_operating_unit IN  VARCHAR2,
                                   p_vendor_name IN  VARCHAR2,
                                   v_error_smsg    OUT varchar2) IS
    v_rfq_only_site_flag varchar2(1);
  BEGIN
   SELECT rfq_only_site_flag
     INTO v_rfq_only_site_flag
     FROM xxabrl_supplier_sites_int i
    WHERE vendor_site_code like 'RET%'
     AND vendor_name = p_vendor_name
     AND operating_unit = p_operating_unit;
  EXCEPTION
    when no_data_found then
      v_error_smsg := v_error_smsg ||
                      'RET site flag is not enabled';
    When Others Then
      v_rfq_only_site_flag  := null;
      v_error_smsg := v_error_smsg || 'Invalid RET Site Flag';
  END validate_ret_site_flag;

-------------------Added by Praveen------------------------------------------
  PROCEDURE validate_ho_site_flag(p_operating_unit IN  VARCHAR2,
                                  p_vendor_name   IN  VARCHAR2,
                                  p_pur_site_flag IN  VARCHAR2,
                                  p_pay_site_flag IN  VARCHAR2,
                                  p_ret_site_flag IN  VARCHAR2,
                                  v_error_smsg    OUT varchar2) IS
    v_pur_site_flag varchar2(1);
    v_pay_site_flag varchar2(1);
    v_rfq_only_site_flag varchar2(1);
  BEGIN

   SELECT purchasing_site_flag,
          pay_site_flag,
          rfq_only_site_flag
     INTO v_pur_site_flag,
          v_pay_site_flag,
          v_rfq_only_site_flag
     FROM xxabrl_supplier_sites_int
    WHERE vendor_site_code like 'HO%'
     AND vendor_name =p_vendor_name
     AND operating_unit = p_operating_unit ;

     if v_pur_site_flag is not null then
        v_error_smsg := v_error_smsg ||
                      'PUR site flag Should not be enabled';
      end if;

     if v_pay_site_flag is not null then
        v_error_smsg := v_error_smsg ||
                      'PAY site flag Should not be enabled';
     end if;

     if v_rfq_only_site_flag is not null then
        v_error_smsg := v_error_smsg ||
                      'RET site flag Should not be enabled';
     end if;
   EXCEPTION
    When Others Then
        v_error_smsg := v_error_smsg || 'Invalid HO Site Flag';
  END validate_ho_site_flag;*/
  
  -------------------commented by Praveen on 04 sep 09------------------------------------------


  PROCEDURE DERIVE_SHIT_TO_LOCATION_ID(P_SHIP_TO_LOC_CODE IN varchar2,
                                       P_SHIP_TO_LOC_ID   OUT NUMBER,
                                       v_error_smsg       OUT varchar2) is
    v_ship_to_loc_id number(10);
  begin
    select location_id
      into v_ship_to_loc_id
      from hr_locations
     where location_code = P_SHIP_TO_LOC_CODE;

    P_SHIP_TO_LOC_ID := v_ship_to_loc_id;

  EXCEPTION
    when no_data_found then
      v_error_smsg := v_error_smsg ||
                      'ship TO location_code does not exist in Oracle Financials-->>';
    when too_many_rows then
      v_ship_to_loc_id := null;
      v_error_smsg     := v_error_smsg ||
                          'Multiple ship TO location_code found-->>';
    When Others Then
      v_ship_to_loc_id := null;
      v_error_smsg     := v_error_smsg ||
                          'Invalid ship TO location_code -->>';
  END DERIVE_SHIT_TO_LOCATION_ID;

  ----------------------------------------------------------------------------

  PROCEDURE DERIVE_BILL_TO_LOCATION_ID(P_BILL_TO_LOC_CODE IN varchar2,
                                       P_BILL_TO_LOC_ID   OUT NUMBER,
                                       v_error_smsg       OUT varchar2) is
    v_bill_to_loc_id number(10);
  begin
    select location_id
      into v_bill_to_loc_id
      from hr_locations
     where location_code = P_BILL_TO_LOC_CODE;

    P_BILL_TO_LOC_ID := v_bill_to_loc_id;

  EXCEPTION
    when no_data_found then
      v_error_smsg := v_error_smsg ||
                      'Bill TO Location_Code does not exist in Oracle Financials-->>';
    when too_many_rows then
      v_bill_to_loc_id := null;
      v_error_smsg     := v_error_smsg ||
                          'Multiple location_code found-->>';
    When Others Then
      v_bill_to_loc_id := null;
      v_error_smsg     := v_error_smsg ||
                          'Invalid bill tolocation_code -->>';
  END DERIVE_BILL_TO_LOCATION_ID;

  -----------------------------------------------------------------------------

  PROCEDURE DERIVE_EMPLOYEE_ID(P_VENDOR_NUMBER IN varchar2,
                               P_EMPLOYEE_ID   OUT NUMBER,
                               v_error_hmsg    OUT varchar2) is
    V_EMPOYEE_ID number(10);
  begin
    select person_id
      INTO V_EMPOYEE_ID
      from per_all_people_f
     where EMPLOYEE_NUMBER = P_VENDOR_NUMBER
       and EFFECTIVE_START_DATE <= trunc(sysdate)
       and EFFECTIVE_END_DATE >= trunc(sysdate);

    P_EMPLOYEE_ID := V_EMPOYEE_ID;

  EXCEPTION
    when no_data_found then
      V_EMPOYEE_ID := null;

    when too_many_rows then
      V_EMPOYEE_ID := null;
      v_error_hmsg := v_error_hmsg ||
                      'Multiple EMPLOYERS AS SAME SUPPLIER-->>';
    When Others Then
      V_EMPOYEE_ID := null;
      v_error_hmsg := v_error_hmsg || 'INVALID SUPLLIER NAME -->>';
  END DERIVE_EMPLOYEE_ID;

  ------------------------------------------------------------------------------
  PROCEDURE DERIVE_TERM_ID(P_TERM_NAME  IN varchar2,
                           P_TERM_ID    OUT NUMBER,
                           v_error_smsg OUT varchar2) is
    v_term_id number(10);
  begin
    select term_id into v_term_id from AP_TERMS where NAME = P_TERM_NAME;

    P_TERM_ID := v_term_id;

  EXCEPTION
    when no_data_found then
      v_error_smsg := v_error_smsg ||
                      'TERM NAME does not exist in Oracle Financials-->>';
    when too_many_rows then
      v_term_id    := null;
      v_error_smsg := v_error_smsg || 'Multiple TERM NAME found-->>';
    When Others Then
      v_term_id    := null;
      v_error_smsg := v_error_smsg || 'Invalid  TERM NAME -->>';
  END DERIVE_TERM_ID;

  -----------------------------------------------------------------------------
  PROCEDURE validate_vendor_number(p_Vendor_Number IN VARCHAR2,
                                   v_error_hmsg    OUT varchar2) IS
    v_data_count number;
  BEGIN
    SELECT count(segment1)
      INTO v_data_count
      FROM po_vendors
     WHERE segment1 = p_Vendor_Number;

    If V_Data_Count >= 1 Then
      v_error_hmsg := v_error_hmsg ||
                      ' Duplicate vendor Number exist in the  Table for the Supplier-->>';
    End If;

    V_Data_Count := 0;

  EXCEPTION
    WHEN OTHERS then
      V_Data_Count := 0;
      v_error_hmsg := v_error_hmsg || ' Invalid vendor Number-->>';

  END validate_vendor_number;

  --------------------------------------------------------------------------------------

  PROCEDURE validate_vendor_name(p_Vendor_Name IN VARCHAR2,
                                 v_error_hmsg  OUT varchar2) IS
    v_data_count number;
  BEGIN
    SELECT count(vendor_name)
      INTO v_data_count
      FROM po_vendors
     WHERE vendor_name = p_Vendor_Name;

    If V_Data_Count >= 1 Then
      v_error_hmsg := v_error_hmsg ||
                      ' Duplicate vendor Name exist in the  Table for the Supplier-->>';
    End If;

    V_Data_Count := 0;

  EXCEPTION
    WHEN OTHERS then
      V_Data_Count := 0;
      v_error_hmsg := v_error_hmsg || 'Invalid vendor Name-->>';

  END validate_vendor_name;

  -----------------------------------------------------------------------------------------
  PROCEDURE validate_vendor_site_code(p_vendor_site_code IN VARCHAR2,
                                      p_vendor_name      IN VARCHAR2,
                                      P_OPERATING_UNIT     IN VARCHAR2,
                                      v_error_smsg       OUT varchar2) IS
    v_data_count number;
  BEGIN
    /*SELECT count(*)
      INTO v_data_count
      FROM po_vendors pv, po_vendor_sites_all pvs
     WHERE pv.VENDOR_ID = pvs.VENDOR_ID
       and pvs.vendor_site_code = p_vendor_site_code
       and pv.VENDOR_NAME = p_vendor_name;*/
        SELECT
count(*) INTO v_data_count
      FROM po_vendors pv, po_vendor_sites_all pvs
      ,org_organization_definitions ood
     WHERE pv.VENDOR_ID = pvs.VENDOR_ID
     and ood.ORGANIZATION_NAME=P_OPERATING_UNIT
            and pvs.vendor_site_code = p_vendor_site_code
       and pv.VENDOR_NAME = p_vendor_name
       and ood.operating_unit=pvs.org_id;

    If V_Data_Count > 0 Then
      v_error_smsg := v_error_smsg ||
                      ' Duplicate vendor site code exist in the  Table for the Supplier-->>';
    End If;

    V_Data_Count := 0;

  EXCEPTION
    WHEN OTHERS then
      V_Data_Count := 0;
      v_error_smsg := v_error_smsg || ' Invalid vendor site code-->>';

  END validate_vendor_site_code;
  ---------------------------------------------------------------------------------------------
  PROCEDURE validate_vendor_type(p_vendor_type_lookup_code IN VARCHAR2,
                                 v_error_hmsg              OUT varchar2) IS
    v_vendor_type_lookup_code varchar2(240);

  BEGIN
    SELECT lookup_code
      INTO v_vendor_type_lookup_code
      FROM po_lookup_codes
     WHERE lookup_type = 'VENDOR TYPE'
       AND lookup_code = p_vendor_type_lookup_code
       AND enabled_flag = 'Y'
       AND NVL(inactive_date, SYSDATE + 1) > SYSDATE
       AND lookup_code in ('MERCHANDISE','INTERNAL VENDORS','CONSUMABLES');

  EXCEPTION
    when no_data_found then
      v_error_hmsg := v_error_hmsg ||
                      'Vendor Type should be in MERCHANDISE or INTERNAL VENDORS or CONSUMABLES';-->>';
    when too_many_rows then
      v_vendor_type_lookup_code := null;
      v_error_hmsg              := v_error_hmsg ||
                                   'lookup_code present for the Vendor type lookup code-->>';
    When Others Then
      v_vendor_type_lookup_code := null;
      v_error_hmsg              := v_error_hmsg ||
                                   'Invalid lookup_code -->>';
  END validate_vendor_type;

  --------------------------------------------------------------------------------------------------
  PROCEDURE validate_operating_unit(P_OPERATING_UNIT IN VARCHAR2,
                                    v_error_smsg     OUT varchar2) IS
    V_OPERATING_UNIT varchar2(240);

  BEGIN
    fnd_file.PUT_LINE(fnd_file.LOG, 'operating unit:' || P_OPERATING_UNIT);
    Select Short_code
      Into V_OPERATING_UNIT
      From HR_Operating_Units
     Where name = P_OPERATING_UNIT;

  EXCEPTION
    When no_data_found then
      v_error_smsg := v_error_smsg ||
                      'Selected Org Id does not exist in Oracle Financials';
      fnd_file.put_line(fnd_file.output,
                        '........................................................................');

    When too_many_rows then
      v_error_smsg := v_error_smsg ||
                      'Multiple Org Id exist in Oracle Financials';
      fnd_file.put_line(fnd_file.output,
                        '........................................................................');

    When Others THEN
      v_error_smsg := v_error_smsg || 'Invalid Org Id Selected ';
      fnd_file.put_line(fnd_file.output,
                        '........................................................................');
  END validate_operating_unit;

  -----------------------------------------------------------------------------------------------
  PROCEDURE validate_pay_group(P_PAY_GROUP  IN VARCHAR2,
                               X_PAY_GROUP  out VARCHAR2,
                               v_error_smsg OUT varchar2) IS
    V_PAY_GROUP varchar2(240);
  BEGIN
    SELECT LOOKUP_CODE
      INTO V_PAY_GROUP
      FROM FND_LOOKUP_VALUES_VL
     WHERE lookup_type = 'PAY GROUP'
       AND UPPER(LOOKUP_CODE) = UPPER(P_PAY_GROUP);

    X_PAY_GROUP := V_PAY_GROUP;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      v_error_smsg := 'Pay Group ' || P_PAY_GROUP ||
                      ' not defined in Oracle';
    WHEN TOO_MANY_ROWS THEN
      v_error_smsg := 'Too many values found for Pay Group ' || P_PAY_GROUP;
  END;
  ---------------------------------------------------------------------------
  PROCEDURE validate_country_code(P_COUNTRY_CODE IN VARCHAR2,
                                  v_error_smsg   OUT varchar2) IS
    V_COUNTRY_CODE varchar2(240);

  BEGIN
    SELECT territory_code
      INTO V_COUNTRY_CODE
      FROM fnd_territories_vl
     WHERE UPPER(territory_code) = UPPER(P_COUNTRY_CODE)
       and obsolete_flag = 'N';

  EXCEPTION
    When no_data_found then
      v_error_smsg := v_error_smsg || 'No Country code Present for Country';

    When too_many_rows then
      v_error_smsg := v_error_smsg ||
                      'Multiple No Country code Present for Country';

    When Others THEN
      v_error_smsg := v_error_smsg ||
                      'Invalid No Country code Present for Country ';

  END validate_country_code;

  ------------------------------------------------------------------------------------------------
  PROCEDURE validate_payment_method(P_PAY_METHOD_CODE IN VARCHAR2,
                                    v_error_smsg      OUT varchar2) IS
    V_PAY_METHOD_CODE varchar2(240);

  BEGIN
    SELECT Payment_Method_code
      into V_PAY_METHOD_CODE
      FROM iby_payment_methods_vl
     WHERE UPPER(Payment_Method_code) = Trim(UPPER(P_PAY_METHOD_CODE));

  EXCEPTION
    When no_data_found then
      v_error_smsg := v_error_smsg ||
                      'Payment method does not exist in Oracle Financials';

    When too_many_rows then
      v_error_smsg := v_error_smsg ||
                      'Multiple Payment method  exist in Oracle Financials';

    When Others THEN
      v_error_smsg := v_error_smsg || 'Invalid Payment method ';

  END validate_payment_method;

  --------------------------------------------------------------------------------------------
  PROCEDURE XXABRL_INSERT_VENDOR_INFO IS
    v_user_id number := FND_PROFILE.value('USER_ID');
    v_resp_id number := FND_PROFILE.value('RESP_ID');
    v_appl_id number := FND_PROFILE.value('RESP_APPL_ID');

    /*CURSOR APS_C1 IS
    Select ROWID,
           VENDOR_NAME,
           Vendor_Number,
           Vendor_Name_ALT,
           EMPLOYEE_ID,
           VENDOR_TYPE_LOOKUP_CODE,
           PRINCIPAL_VENDOR,
           LEGAL_STATUS,
           CEO_NAME,
           CEO_TITLE,
           SHIP_TO_LOCATION_CODE,
           BILL_TO_LOCATION_CODE,
           TERMS_NAME,
           START_DATE_ACTIVE,
           END_DATE_ACTIVE,
           SMALL_BUSINESS_FLAG,
           HOLD_FLAG,
           PURCHASING_HOLD_REASON,
           HOLD_ALL_PAYMENTS_FLAG,
           HOLD_FUTURE_PAYMENTS_FLAG,
           HOLD_UNMATCHED_INVOICES_FLAG,
           MIN_ORDER_AMOUNT,
           TAX_VERIFICATION_DATE,
           VAT_REGISTRATION_NUM,
           PAYMENT_PRIORITY,
           EXCLUDE_FREIGHT_FROM_DISCOUNT,
           CUSTOMER_NUM,
           GLOBAL_ATTRIBUTE_CATEGORY,
           Primary_Vendor_Category,
           MICR_CODE,
           NEFT_CODE,
           RTGS_CODE,
           APMC_Registration,
           Drug_License,
           Liquor_License,
           MSMED,
           --  NAV_VENDOR_NUM,
           TAX_REGION,
           TAX_PAYER_TYPE
      From XXABRL_SUPPLIER_INT
     WHERE NVL(PROCESS_FLAG, 'N') = 'V';*/

    CURSOR APS_C2 IS
      Select ROWID,
             OPERATING_UNIT,
             VENDOR_NAME,
             VENDOR_SITE_CODE,
             ADDRESS_LINE1,
             ADDRESS_LINE2,
             CITY,
             COUNTY,
             STATE,
             COUNTRY,
             ZIP,
             AREA_CODE,
             PHONE,
             MOBILE_NO,
             FAX,
             FAX_AREA_CODE,
             EMAIL_ADDRESS,
             PURCHASING_SITE_FLAG,
             PAY_SITE_FLAG,
             RFQ_ONLY_SITE_FLAG,
             INACTIVE_DATE,
             INVOICE_AMOUNT_LIMIT,
             BILL_TO_LOCATION_CODE,
             SHIP_TO_LOCATION_CODE,
             TERMS_NAME,
             BILL_TO_LOCATION_ID,
             SHIP_TO_LOCATION_ID,
             TERM_ID,
             PAY_GROUP_LOOKUP_CODE,
             PAYMENT_METHOD_LOOKUP_CODE,
             NAV_VENDOR_NUM,
             Vendor_Bank_Name,
             Vendor_Branch_Name,
             Vendor_Account_Number,
             contact_first_name,
             contact_last_name,
             DERIVED_PAY_GROUP,
             vendor_id
        From XXABRL_SUPPLIER_SITES_INT
       WHERE NVL(PROCESS_FLAG, 'N') = 'V';
    CURSOR APS_C3 IS
    Select ROWID,
           OPERATING_UNIT,
           VENDOR_NAME,
           VENDOR_SITE_CODE,
           TITLE,
           FIRST_NAME,
           MIDDLE_NAME,
           LAST_NAME,
           EMAIL_ADDRESS,
           PHONE_AREA_CODE,
           PHONE,
           FAX_AREA_CODE,
           FAX,
           vendor_id,
           vendor_site_id
      FROM XXABRL_SUP_SITE_CONT_INT
     WHERE NVL(PROCESS_FLAG, 'N') = 'V';
    ln_vendor_interface_id      AP_SUPPLIERS_INT.vendor_interface_id%TYPE;
    ln_vendor_site_interface_id NUMBER;
    ln_vendor_site_cont_int_id  NUMBER;
  BEGIN
    FND_FILE.PUT_LINE(FND_FILE.LOG, 'Starting Inserting Vendors');
    FOR lr_Site_rec IN APS_C2 LOOP
      --
      -- We just need to create US Sites in US OU only else we need to create every other site
      -- in all the existing OU except US
      --
      ln_vendor_site_interface_id := NULL;
      BEGIN
        SELECT AP_SUPPLIER_SITES_INT_S.NEXTVAL
          INTO ln_vendor_site_interface_id
          FROM DUAL;
      EXCEPTION
        WHEN OTHERS THEN
          FND_FILE.PUT_LINE(FND_FILE.LOG,
                            'Unable to derive vendor_site_interface_id');
      END;
      FND_FILE.PUT_LINE(FND_FILE.LOG,
                        'Inserting Vendor Site' ||
                        lr_Site_rec.VENDOR_SITE_CODE);
      begin
        INSERT INTO AP_SUPPLIER_SITES_INT
          (VENDOR_INTERFACE_ID,
           VENDOR_SITE_INTERFACE_ID,
           OPERATING_UNIT_NAME,
           --,VENDOR_NAME
           VENDOR_SITE_CODE,
           ADDRESS_LINE1,
           ADDRESS_LINE2,
           CITY,
           COUNTY,
           STATE,
           COUNTRY,
           ZIP,
           AREA_CODE,
           PHONE,
           TELEX,
           FAX,
           FAX_AREA_CODE,
           EMAIL_ADDRESS,
           PURCHASING_SITE_FLAG,
           PAY_SITE_FLAG,
           RFQ_ONLY_SITE_FLAG,
           INACTIVE_DATE,
           INVOICE_AMOUNT_LIMIT,
           BILL_TO_LOCATION_CODE,
           SHIP_TO_LOCATION_CODE,
           TERMS_NAME,
           BILL_TO_LOCATION_ID,
           SHIP_TO_LOCATION_ID,
           TERMS_ID,
           PAY_GROUP_LOOKUP_CODE,
           PAYMENT_METHOD_LOOKUP_CODE,
           ATTRIBUTE_CATEGORY,
           Attribute1,
           Attribute2,
           Attribute3,
           Attribute4,
           Attribute5,
           Attribute6,
           CREATION_DATE,
           CREATED_BY,
           vendor_id)
        VALUES
          (ln_vendor_interface_id,
           ln_vendor_site_interface_id,
           lr_Site_rec.OPERATING_UNIT,
           --, lr_Site_rec.VENDOR_NAME
           SUBSTR(lr_Site_rec.VENDOR_SITE_CODE, 1, 15),
           lr_Site_rec.ADDRESS_LINE1,
           lr_Site_rec.ADDRESS_LINE2,
           SUBSTR(lr_Site_rec.CITY, 1, 15),
           lr_Site_rec.COUNTY,
           lr_Site_rec.STATE,
           lr_Site_rec.COUNTRY,
           lr_Site_rec.ZIP,
           lr_Site_rec.AREA_CODE,
           lr_Site_rec.PHONE,
           lr_Site_rec.MOBILE_NO,
           lr_Site_rec.FAX,
           lr_Site_rec.FAX_AREA_CODE,
           lr_Site_rec.EMAIL_ADDRESS,
           upper(lr_Site_rec.PURCHASING_SITE_FLAG),
           upper(lr_Site_rec.PAY_SITE_FLAG),
           upper(lr_Site_rec.RFQ_ONLY_SITE_FLAG),
           lr_Site_rec.INACTIVE_DATE,
           lr_Site_rec.INVOICE_AMOUNT_LIMIT,
           lr_Site_rec.BILL_TO_LOCATION_CODE,
           lr_Site_rec.SHIP_TO_LOCATION_CODE,
           lr_Site_rec.TERMS_NAME,
           lr_Site_rec.BILL_TO_LOCATION_ID,
           lr_Site_rec.SHIP_TO_LOCATION_ID,
           lr_Site_rec.TERM_ID,
           upper(lr_Site_rec.DERIVED_PAY_GROUP),
           lr_Site_rec.PAYMENT_METHOD_LOOKUP_CODE,
           'Supplier Site Additional Info',
           lr_Site_rec.NAV_VENDOR_NUM,
           lr_Site_rec.Vendor_Bank_Name,
           lr_Site_rec.Vendor_Branch_Name,
           lr_Site_rec.Vendor_Account_Number,
           lr_Site_rec.CONTACT_FIRST_NAME,
           lr_Site_rec.CONTACT_LAST_NAME,
           SYSDATE,
           v_user_id,
           lr_Site_rec.vendor_id);

      exception
        when others then
          FND_FILE.PUT_LINE(FND_FILE.LOG,
                            'Error while inserting sites' || sqlerrm);
      end;



      Update XXABRL_SUPPLIER_SITES_INT
         Set process_Flag = 'Y'
       Where Rowid = lr_Site_rec.ROWID;
      Commit;

    END LOOP;

    FOR lr_Site_cont_rec IN APS_C3 LOOP
        --
        -- We just need to create US Sites in US OU only else we need to create every other site
        -- in all the existing OU except US
        --
        ln_vendor_site_cont_int_id := NULL;
        BEGIN
          SELECT PO_VENDOR_CONTACTS_S.nextval
            INTO ln_vendor_site_cont_int_id
            FROM DUAL;
        EXCEPTION
          WHEN OTHERS THEN
            FND_FILE.PUT_LINE(FND_FILE.LOG,
                              'Unable to derive vendor_site_Contact_interface_id');
        END;

        FND_FILE.PUT_LINE(FND_FILE.LOG,
                          'Inserting Site Contact' ||
                          lr_Site_cont_rec.FIRST_NAME);

        INSERT INTO AP_SUP_SITE_CONTACT_INT
          (VENDOR_INTERFACE_ID,
           VENDOR_CONTACT_INTERFACE_ID,
           OPERATING_UNIT_NAME,
           -- ,VENDOR_NAME
           VENDOR_SITE_CODE,
           PREFIX,
           FIRST_NAME,
           MIDDLE_NAME,
           LAST_NAME,
           EMAIL_ADDRESS,
           AREA_CODE,
           PHONE,
           FAX_AREA_CODE,
           FAX,
           CREATION_DATE,
           CREATED_BY,
           vendor_id,
           vendor_site_id
           )
        VALUES
          (ln_vendor_interface_id,
           ln_vendor_site_cont_int_id,
           lr_Site_cont_rec.OPERATING_UNIT,
           --  , lr_Site_cont_rec.VENDOR_NAME         ,
           SUBSTR(lr_Site_cont_rec.VENDOR_SITE_CODE, 1, 15),
           lr_Site_cont_rec.TITLE,
           SUBSTR(lr_Site_cont_rec.FIRST_NAME, 1, 15),
           lr_Site_cont_rec.MIDDLE_NAME,
           SUBSTR(lr_Site_cont_rec.LAST_NAME, 1, 15),
           lr_Site_cont_rec.EMAIL_ADDRESS,
           lr_Site_cont_rec.PHONE_AREA_CODE,
           nvl(lr_Site_cont_rec.PHONE, '.'),
           lr_Site_cont_rec.FAX_AREA_CODE,
           lr_Site_cont_rec.FAX,
           SYSDATE,
           v_user_id,
           lr_Site_cont_rec.vendor_id,
           lr_Site_cont_rec.vendor_site_id
           );

        Update XXABRL_SUP_SITE_CONT_INT
           Set process_flag = 'Y'
         Where Rowid = lr_Site_cont_rec.ROWID;
        Commit;
      END LOOP;

    COMMIT;
  END XXABRL_INSERT_VENDOR_INFO;
END XXABRL_INSERT_VENDOR_V2_PKG; 
/


CREATE OR REPLACE PACKAGE BODY APPS.jai_gst_diagnostic_pkg
AS
-- $Header jai_gst_dgeneral_pkg.sql,Version 3 2017/07/14  ALLSHAIK  Exp $
   PROCEDURE jai_locaplist (
      errbuf         OUT      VARCHAR2,
      retcode        OUT      VARCHAR2,
      p_param_id1    IN       VARCHAR2,
      p_param_val1   IN       VARCHAR2 DEFAULT 'Invoice_Id',
      p_param_id2    IN       VARCHAR2,
      p_param_val2   IN       VARCHAR2 DEFAULT 'Vendor_Id',
      p_param_id3    IN       VARCHAR2,
      p_param_val3   IN       VARCHAR2,
      p_param_id4    IN       VARCHAR2,
      p_param_val4   IN       VARCHAR2 DEFAULT NULL
   )
   IS
   BEGIN
      --DBMS_OUTPUT.put_line ('B4 Calling jai_gst_dgeneral_pkg.main_prc ');
      jai_gst_dgeneral_pkg.main_prc (p_start_date           => SYSDATE,
                                 p_end_date             => SYSDATE,
                                 p_file_name            => 'LOCAPLIST',
                                 p_org_ids              => NULL,
                                 p_trx_type             => NULL,
                                 p_ledger_id            => NULL,
                                 p_per_name             => NULL,
                                 p_max_output_rows      => 1000,
                                 p_print_to_stdout      => 'N',
                                 p_param_id1            => p_param_id1,
                                 p_param_val1           => p_param_val1,
                                 p_param_id2            => p_param_id2,
                                 p_param_val2           => p_param_val2,
                                 p_param_id3            => p_param_id3,
                                 p_param_val3           => p_param_val3,
                                 p_param_id4            => p_param_id4,
                                 p_param_val4           => p_param_val4,
                                 p_debug_mode           => 'N'
                                );
   END;

   PROCEDURE jai_locporct (
      errbuf         OUT      VARCHAR2,
      retcode        OUT      VARCHAR2,
      p_param_id1    IN       VARCHAR2,
      p_param_val1   IN       VARCHAR2 DEFAULT 'PO_Number',
      p_param_id2    IN       VARCHAR2,
      p_param_val2   IN       VARCHAR2 DEFAULT 'Organization_Id',
      p_param_id3    IN       VARCHAR2,
      p_param_val3   IN       VARCHAR2,
      p_param_id4    IN       VARCHAR2,
      p_param_val4   IN       VARCHAR2 DEFAULT NULL
   )
   IS
   BEGIN
   
   FND_FILE.PUT_LINE(FND_FILE.LOG,'Optional receipt number: '||p_param_id3);
   
      -- DBMS_OUTPUT.put_line ('B4 Calling jai_gst_dgeneral_pkg.main_prc ');
      jai_gst_dgeneral_pkg.main_prc (p_start_date           => SYSDATE,
                                 p_end_date             => SYSDATE,
                                 p_file_name            => 'LOCPORCT',
                                 p_org_ids              => NULL,
                                 p_trx_type             => NULL,
                                 p_ledger_id            => NULL,
                                 p_per_name             => NULL,
                                 p_max_output_rows      => 2500,
                                 p_print_to_stdout      => 'N',
                                 p_param_id1            => p_param_id1,
                                 p_param_val1           => p_param_val1,
                                 p_param_id2            => p_param_id2,
                                 p_param_val2           => p_param_val2,
                                 p_param_id3            => p_param_id3,
                                 p_param_val3           => p_param_val3,
                                 p_param_id4            => p_param_id4,
                                 p_param_val4           => p_param_val4,
                                 p_debug_mode           => 'N'
                                );
   END;

   PROCEDURE jai_locomlist (
      errbuf         OUT      VARCHAR2,
      retcode        OUT      VARCHAR2,
      p_param_id1    IN       VARCHAR2,
      p_param_val1   IN       VARCHAR2 DEFAULT 'Order Header ID',
      p_param_id2    IN       VARCHAR2,
      p_param_val2   IN       VARCHAR2 DEFAULT 'Org ID',
      p_param_id3    IN       VARCHAR2,
      p_param_val3   IN       VARCHAR2,
      p_param_id4    IN       VARCHAR2,
      p_param_val4   IN       VARCHAR2 DEFAULT 'Order Line ID'
   )
   IS
   BEGIN
      --DBMS_OUTPUT.put_line ('B4 Calling jai_gst_dgeneral_pkg.main_prc ');
      jai_gst_dgeneral_pkg.main_prc (p_start_date           => SYSDATE,
                                 p_end_date             => SYSDATE,
                                 p_file_name            => 'LOCOMLIST',
                                 p_org_ids              => NULL,
                                 p_trx_type             => NULL,
                                 p_ledger_id            => NULL,
                                 p_per_name             => NULL,
                                 p_max_output_rows      => 2500,
                                 p_print_to_stdout      => 'N',
                                 p_param_id1            => p_param_id1,
                                 p_param_val1           => p_param_val1,
                                 p_param_id2            => p_param_id2,
                                 p_param_val2           => p_param_val2,
                                 p_param_id3            => p_param_id3,
                                 p_param_val3           => p_param_val3,
                                 p_param_id4            => p_param_id4,
                                 p_param_val4           => p_param_val4,
                                 p_debug_mode           => 'N'
                                );
   END;

   PROCEDURE jai_locarlist (
      errbuf         OUT      VARCHAR2,
      retcode        OUT      VARCHAR2,
      p_param_id1    IN       VARCHAR2,
      p_param_val1   IN       VARCHAR2 DEFAULT 'Customer Trx ID',
      p_param_id2    IN       VARCHAR2,
      p_param_val2   IN       VARCHAR2 DEFAULT 'Organization_Id',
      p_param_id3    IN       VARCHAR2,
      p_param_val3   IN       VARCHAR2,
      p_param_id4    IN       VARCHAR2,
      p_param_val4   IN       VARCHAR2 DEFAULT NULL
   )
   IS
   BEGIN
      --DBMS_OUTPUT.put_line ('B4 Calling jai_gst_dgeneral_pkg.main_prc ');
      jai_gst_dgeneral_pkg.main_prc (p_start_date           => SYSDATE,
                                 p_end_date             => SYSDATE,
                                 p_file_name            => 'LOCARLIST',
                                 p_org_ids              => NULL,
                                 p_trx_type             => NULL,
                                 p_ledger_id            => NULL,
                                 p_per_name             => NULL,
                                 p_max_output_rows      => 2500,
                                 p_print_to_stdout      => 'N',
                                 p_param_id1            => p_param_id1,
                                 p_param_val1           => p_param_val1,
                                 p_param_id2            => p_param_id2,
                                 p_param_val2           => p_param_val2,
                                 p_param_id3            => p_param_id3,
                                 p_param_val3           => p_param_val3,
                                 p_param_id4            => p_param_id4,
                                 p_param_val4           => p_param_val4,
                                 p_debug_mode           => 'N'
                                );
   END;

   PROCEDURE jai_locfalist (
      errbuf         OUT      VARCHAR2,
      retcode        OUT      VARCHAR2,
      p_param_id1    IN       VARCHAR2,
      p_param_val1   IN       VARCHAR2 DEFAULT 'Block_id',
      p_param_id2    IN       VARCHAR2,
      p_param_val2   IN       VARCHAR2 DEFAULT 'Start_date',
      p_param_id3    IN       VARCHAR2,
      p_param_val3   IN       VARCHAR2 DEFAULT 'End_Date',
      p_param_id4    IN       VARCHAR2,
      p_param_val4   IN       VARCHAR2 DEFAULT NULL
   )
   IS
   BEGIN
      --DBMS_OUTPUT.put_line ('B4 Calling jai_gst_dgeneral_pkg.main_prc ');
      jai_gst_dgeneral_pkg.main_prc
                     (p_start_date           => SYSDATE,
                      p_end_date             => SYSDATE,
                      p_file_name            => 'LOCFALIST',
                      p_org_ids              => NULL,
                      p_trx_type             => NULL,
                      p_ledger_id            => NULL,
                      p_per_name             => NULL,
                      p_max_output_rows      => 2500,
                      p_print_to_stdout      => 'N',
                      p_param_id1            => p_param_id1,
                      p_param_val1           => p_param_val1,
                      p_param_id2            => fnd_date.canonical_to_date
                                                                  (p_param_id2),
                      p_param_val2           => p_param_val2,
                      p_param_id3            => fnd_date.canonical_to_date
                                                                  (p_param_id3),
                      p_param_val3           => p_param_val3,
                      p_param_id4            => p_param_id4,
                      p_param_val4           => p_param_val4,
                      p_debug_mode           => 'N'
                     );
   END;

   PROCEDURE jai_locstatus (
      errbuf         OUT      VARCHAR2,
      retcode        OUT      VARCHAR2,
      p_param_id1    IN       VARCHAR2,
      p_param_val1   IN       VARCHAR2 DEFAULT NULL,
      p_param_id2    IN       VARCHAR2,
      p_param_val2   IN       VARCHAR2 DEFAULT NULL,
      p_param_id3    IN       VARCHAR2,
      p_param_val3   IN       VARCHAR2 DEFAULT NULL,
      p_param_id4    IN       VARCHAR2,
      p_param_val4   IN       VARCHAR2 DEFAULT NULL
   )
   IS
   BEGIN
      --DBMS_OUTPUT.put_line ('B4 Calling jai_gst_dgeneral_pkg.main_prc ');
      jai_gst_dgeneral_pkg.main_prc (p_start_date           => SYSDATE,
                                 p_end_date             => SYSDATE,
                                 p_file_name            => 'LOCSTATUS',
                                 p_org_ids              => NULL,
                                 p_trx_type             => NULL,
                                 p_ledger_id            => NULL,
                                 p_per_name             => NULL,
                                 p_max_output_rows      => 7000,
                                 p_print_to_stdout      => 'N',
                                 p_param_id1            => p_param_id1,
                                 p_param_val1           => p_param_val1,
                                 p_param_id2            => p_param_id2,
                                 p_param_val2           => p_param_val2,
                                 p_param_id3            => p_param_id3,
                                 p_param_val3           => p_param_val3,
                                 p_param_id4            => p_param_id4,
                                 p_param_val4           => p_param_val4,
                                 p_debug_mode           => 'N'
                                );
   END;

   PROCEDURE il_main (
      errbuf        OUT      VARCHAR2,
      retcode       OUT      VARCHAR2,
      p_diag_type   IN       VARCHAR2,
      p_param_id1   IN       VARCHAR2,  --Invoice Id
      p_param_id2   IN       VARCHAR2  DEFAULT NULL,  -- PO Header Id
      p_param_id    IN       VARCHAR2  DEFAULT NULL,  -- RCV Header Id
      p_param_id3   IN       VARCHAR2, -- Sales Order ID
      p_param_id4   IN       VARCHAR2, -- Sales Order Line ID
      p_param_id5   IN       VARCHAR2, -- RA Customer Trx ID
      p_param_id6   IN       VARCHAR2, -- Book Type Code
      p_param_id7   IN       VARCHAR2, -- Start Date
      p_param_id8   IN       VARCHAR2  -- End Date
   )
   IS
      l_errout            VARCHAR2 (100);
      l_retcode           VARCHAR2 (100);
      l_vendor_id         NUMBER;
      l_organization_id   NUMBER;
      l_rectnum           VARCHAR2 (100);
      l_ponum             VARCHAR2(100);   --added by HTHIPPAR
      l_org_id            NUMBER;
      l_order_type_id     NUMBER;
      l_customer_trxid    NUMBER;
      l_errormsg          VARCHAR2 (100);
   /* Param id 1 will be Ap invoice id /Receipt Number/Order Number
      Param id 2 vendor id /organization id ,Order Type id
      Param id 3 org id
      param id 4  Customer Trx id
      */
   BEGIN
      IF p_diag_type = 'LOCAPLIST'
      THEN
         IF p_param_id1 IS NOT NULL
         THEN
            BEGIN
               SELECT vendor_id
                 INTO l_vendor_id
                 FROM ap_invoices_all
                WHERE invoice_id = p_param_id1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_errormsg := 'Enter Valid AP Invoice ID';
                  RAISE;
            END;

            jai_gst_diagnostic_pkg.jai_locaplist (errbuf            => l_errout,
                                              retcode           => l_retcode,
                                              p_param_id1       => p_param_id1,
                                              p_param_val1      => 'Invoice_Id',
                                              p_param_id2       => l_vendor_id,
                                              p_param_val2      => 'Vendor_Id',
                                              p_param_id3       => NULL,
                                              p_param_val3      => NULL,
                                              p_param_id4       => NULL,
                                              p_param_val4      => NULL
                                             );
         END IF;

         COMMIT;
      ELSIF p_diag_type = 'LOCPORCT'
      
      /* Added by HTHIPPAR 10-Jun-2017
      Adding the logic to handle when either PO header id or receipt header id is given.
        If po header id is given then we are going to etract all the receipts against that PO.
        If receipt header id is given then we are going to extract only specific to that receipt     
        */   
        
      THEN
      
      -- Checking for the valid po header id
         IF  (p_param_id2 IS NOT NULL AND  p_param_id IS NULL ) 
           OR (p_param_id2 IS  NULL AND  p_param_id IS NOT NULL ) 
           OR (p_param_id2 IS NOT NULL AND  p_param_id IS NOT NULL ) 
         
         THEN
         
         IF p_param_id2  IS NOT NULL THEN
         
            BEGIN
               /*SELECT ship_to_org_id, receipt_num
                 INTO l_organization_id, l_rectnum
                 FROM rcv_shipment_headers
                WHERE shipment_header_id = p_param_id2;*/
                
                SELECT ship_to_organization_id, segment1
                 INTO l_organization_id, l_ponum
                 FROM po_headers_all poh, po_lines_all pol, po_line_locations_all pll
                WHERE poh.po_header_id=pll.po_header_id
                and pol.po_line_id=pll.po_line_id
                and poh.po_header_id = p_param_id2;
                
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_errormsg := 'Enter Valid PO Header ID';
                  RAISE;
            END;
           
          END IF;
          
         -- Added by HTHIPPAR  10-Jun-2017
         -- Checking for the valid shipment header id
         
            IF p_param_id IS NOT NULL  THEN
         
            BEGIN
            
               SELECT ship_to_org_id, receipt_num
                 INTO l_organization_id, l_rectnum
                 FROM rcv_shipment_headers
                WHERE shipment_header_id = p_param_id;
                
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_errormsg := 'Enter Valid RCV Shipment Header ID';
                  RAISE;
            END;
            
          /*  
            -- Added by HTHIPPAR 10-Jun-2017
            -- When the shipment_header_id alone is given then we should fetch the respective po_header_id and pass as a parameter.
            
            
            BEGIN
            
            -- Yet to handle the logic to capture multiple POs for a single receipt
                                   
                                      SELECT  Max(pha.segment1)  
                                      INTO    l_ponum
                                      FROM   po_headers_all pha,
                                            rcv_shipment_lines  rsl,
                                            rcv_shipment_headers rsh
                              WHERE  1=1
                              AND     pha.po_header_id=rsl.po_header_id
                              AND     rsl.shipment_header_id=rsh.shipment_header_id
                              AND     rsh.shipment_header_id  =  p_param_id;
            
            EXCEPTION
               
               WHEN NO_DATA_FOUND THEN
                  l_errormsg := null;
                  
               WHEN OTHERS  THEN
                  l_errormsg := 'Error While fetching the PO header id against  RCV Shipment Header ID';
                  RAISE;
            END;
            
            */
            
            END IF;

            jai_gst_diagnostic_pkg.jai_locporct
                                           (errbuf            => l_errout,
                                            retcode           => l_retcode,
                                            p_param_id1       => l_ponum,
                                            p_param_val1      => 'PO Number',
                                            p_param_id2       => l_organization_id,
                                            p_param_val2      => 'Organization Id',
                                            p_param_id3       => p_param_id,
                                            p_param_val3      => 'Receipt Number',
                                            p_param_id4       => NULL,
                                            p_param_val4      => NULL
                                           );
                                           
            COMMIT;
            
         END IF;
         
      ELSIF p_diag_type = 'LOCOMLIST'
      THEN
         IF p_param_id3 IS NOT NULL
         THEN
            BEGIN
               SELECT org_id, order_type_id
                 INTO l_org_id, l_order_type_id
                 FROM oe_order_headers_all
                WHERE header_id = p_param_id3;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_errormsg := 'Enter Valid Order Header ID';
                  RAISE;
            END;
            IF p_param_id4 IS NOT NULL
            THEN
                BEGIN 
                   SELECT org_id
                     INTO l_org_id
                     FROM oe_order_lines_all
                    WHERE header_id = p_param_id3
                      AND line_id = p_param_id4;
                EXCEPTION
                   WHEN OTHERS
                   THEN
                      l_errormsg := 'Enter Valid Order Line ID';
                      RAISE;
                END;
            END IF;

            jai_gst_diagnostic_pkg.jai_locomlist
                                         (errbuf            => l_errout,
                                          retcode           => l_retcode,
                                          p_param_id1       => p_param_id3,
                                          p_param_val1      => 'Order Header ID',
                                          p_param_id2       => l_org_id,
                                          p_param_val2      => 'Operating Unit ID',
                                          p_param_id3       => l_order_type_id,
                                          p_param_val3      => 'Order Type ID',
                                          p_param_id4       => p_param_id4,
                                          p_param_val4      => 'Order Line ID'
                                         );
            COMMIT;
         END IF;
      ELSIF p_diag_type = 'LOCARLIST'
      THEN
         IF p_param_id5 IS NOT NULL
         THEN
            BEGIN
               SELECT customer_trx_id, org_id
                 INTO l_customer_trxid, l_organization_id
                 FROM ra_customer_trx_all
                WHERE customer_trx_id = p_param_id5;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_errormsg := 'Enter Valid Customer Trx ID';
                  RAISE;
            END;

            jai_gst_diagnostic_pkg.jai_locarlist
                                           (errbuf            => l_errout,
                                            retcode           => l_retcode,
                                            p_param_id1       => p_param_id5,
                                            p_param_val1      => 'Customertrx ID',
                                            p_param_id2       => l_organization_id,
                                            p_param_val2      => 'Organization ID',
                                            p_param_id3       => NULL,
                                            p_param_val3      => NULL,
                                            p_param_id4       => NULL,
                                            p_param_val4      => NULL
                                           );
            COMMIT;
         END IF;
      ELSIF p_diag_type = 'LOCFALIST'
      THEN
         fnd_file.put_line (fnd_file.LOG, 'p_diag_type ' || p_diag_type);
         fnd_file.put_line (fnd_file.LOG, 'p_param_id6 ' || p_param_id6);
         fnd_file.put_line (fnd_file.LOG, 'p_param_id7 ' || p_param_id7);
         fnd_file.put_line (fnd_file.LOG, 'p_param_id8' || p_param_id8);

         IF     p_param_id6 IS NOT NULL
            AND p_param_id7 IS NOT NULL
            AND NVL (p_param_id8, SYSDATE) IS NOT NULL
         THEN
            jai_gst_diagnostic_pkg.jai_locfalist
                                         (errbuf            => l_errout,
                                          retcode           => l_retcode,
                                          p_param_id1       => UPPER
                                                                  (p_param_id6),
                                          p_param_val1      => 'BOOK_TYPE_CODE',
                                          p_param_id2       => p_param_id7,
                                          p_param_val2      => 'Start Date',
                                          p_param_id3       => NVL
                                                                  (p_param_id8,
                                                                   TRUNC
                                                                      (SYSDATE)
                                                                  ),
                                          p_param_val3      => 'End Date',
                                          p_param_id4       => NULL,
                                          p_param_val4      => NULL
                                         );
            COMMIT;
         END IF;
      ELSIF p_diag_type = 'LOCSTATUS'
      THEN
         jai_gst_diagnostic_pkg.jai_locstatus (errbuf            => l_errout,
                                           retcode           => l_retcode,
                                           p_param_id1       => NULL,
                                           p_param_val1      => NULL,
                                           p_param_id2       => NULL,
                                           p_param_val2      => NULL,
                                           p_param_id3       => NULL,
                                           p_param_val3      => NULL,
                                           p_param_id4       => NULL,
                                           p_param_val4      => NULL
                                          );
         COMMIT;
      END IF;
      
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line ('Err Msg :' || l_errormsg);
         DBMS_OUTPUT.put_line ('Err Code :' || SQLERRM);
         fnd_file.put_line (fnd_file.LOG, 'Err Msg :' || l_errormsg);
         fnd_file.put_line (fnd_file.LOG, SQLERRM);
   -- RAISE;
   END;
END jai_gst_diagnostic_pkg; 
/


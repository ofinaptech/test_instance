CREATE OR REPLACE PACKAGE BODY APPS.Xxabrl_Asset_Trnf_Pkg AS
PROCEDURE xxabrl_asset_trn_proc(ERRBUFF             OUT VARCHAR2,
                                RETCODE             OUT NUMBER,
                                p_book_code       IN VARCHAR2,
                                p_date_from    IN VARCHAR2,
                                p_date_to       IN VARCHAR2
                                ) AS

-- variable declaration
query_string             LONG;

TYPE ref_cur IS REF CURSOR;

c                        ref_cur;

v_asset_id NUMBER;

-- report name -> XXABRL Asset Transfer Report

-- summery parameters declaration


-- columns to display on report

TYPE c_rec IS RECORD (  v_asset_id                fa_additions_v.ASSET_ID%TYPE,
                        v_S_LOCATION_NAME         FND_FLEX_VALUES_VL .DESCRIPTION%TYPE,
                        v_S_LOCATION_CODE         FA_LOCATIONS.SEGMENT1%TYPE,
                         V_ASSET_DESC             fa_additions_v.DESCRIPTION%TYPE,
                        v_vendor_name             po_vendors.VENDOR_NAME%TYPE,
                        v_VENDOR_CODE             PO_VENDORS.SEGMENT1%TYPE,
                        v_INVOICE_NUMBER          FA_INVOICE_DETAILS_V.invoice_NUMBER%TYPE,
                        v_invoice_date            AP_INVOICES_ALL.INVOICE_DATE%TYPE,
                        v_PO_NUMBER               FA_INVOICE_DETAILS_V.PO_NUMBER%TYPE,
                        v_po_date                 po_headers_all.CREATION_DATE%TYPE,
                        v_RECEIPT_NUM             rcv_shipment_headers.RECEIPT_NUM%TYPE,
                        v_receipt_date            rcv_shipment_headers.CREATION_DATE%TYPE,--(30,2),
                        v_S_QTY_RECVD             NUMBER,--(30,2),
                        v_TRANSACTION_DATE_ENTERED      FA_TRANSACTION_HISTORY_TRX_V.TRANSACTION_DATE_ENTERED%TYPE,--(30,2),
                        v_T_LOCATION              FA_LOCATIONS.SEGMENT1%TYPE,--(30,2),
                        v_T_LOCATION_NAME         FND_FLEX_VALUES_VL .DESCRIPTION%TYPE,--(30,2),
                        v_T_UNIT_ASSIGNED         FA_DISTRIBUTION_HISTORY.UNITS_ASSIGNED%TYPE
                          );

v_rec                    c_rec;

STR_DATE LONG;
STR_ASSET_BOOK  LONG;
inv_count NUMBER;
v_date_from DATE;
v_date_to  DATE;

BEGIN
-- query string for the tds
query_string:=
'SELECT  FDH_IN.asset_id
       ,FFV_IN.DESCRIPTION S_LOCATION_NAME
       ,FL_IN.SEGMENT1 S_LOCATION_CODE
       ,FAV.DESCRIPTION ASSET_DESC
       ,INV.vendor_name
       ,INV.VENDOR_CODE
       ,INV.INVOICE_NUMBER
       ,INV.invoice_date
       ,INV.PO_NUMBER
       ,INV.po_date
       ,INV.RECEIPT_NUM
       ,INV.receipt_date
       ,FDH_IN.UNITS_ASSIGNED S_QTY_RECVD
       ,FTHT.TRANSACTION_DATE_ENTERED
       ,FL_TO.SEGMENT1 T_LOCATION
       ,FFV_TO.DESCRIPTION T_LOCATION_NAME
       ,FDH_TO.UNITS_ASSIGNED T_UNIT_ASSIGNED
FROM fa_additions_v fav
    ,FA_DISTRIBUTION_HISTORY FDH_IN
    ,FA_TRANSACTION_HISTORY_TRX_V FTHT
    ,FA_LOCATIONS FL_IN
    ,FA_DISTRIBUTION_HISTORY FDH_TO
    ,FA_LOCATIONS FL_TO
    ,FND_FLEX_VALUES_VL  FFV_IN
    ,FND_FLEX_VALUE_SETS FFVS_IN
    ,FND_FLEX_VALUES_VL  FFV_TO
    ,FND_FLEX_VALUE_SETS FFVS_TO
    ,(SELECT DISTINCT ASSET_ID
                     --PV.VENDOR_NAME
                     ,PV.SEGMENT1 VENDOR_CODE
                     ,fidv.PO_NUMBER
                     ,TRUNC(pOH.CREATION_DATE) PO_DATE
                     ,fidv.INVOICE_NUMBER
                     ,api.invoice_date
                     ,fidv.vendor_name
                     ,rsh.RECEIPT_NUM
                     ,rsh.CREATION_DATE receipt_date
      FROM FA_INVOICE_DETAILS_V fidv
            ,po_vendors pv
          ,ap_invoices_all api
          ,po_headers_all poh
          ,rcv_transactions rt
          ,rcv_shipment_headers rsh
       WHERE pv.vendor_id(+)=fidv.po_vendor_id
       AND      api.invoice_id(+)=fidv.invoice_id
       AND poh.segment1(+)=fidv.po_number
       AND rt.po_header_id(+)=poh.po_header_id
       AND rsh.SHIPMENT_HEADER_ID(+)=rt.SHIPMENT_HEADER_ID
       AND rt.TRANSACTION_TYPE(+)=''RECEIVE'') INV
WHERE fav.asset_id=FDH_IN.asset_id
AND FDH_IN.TRANSACTION_HEADER_ID_OUT IS NOT NULL
AND FDH_IN.location_id<>FDH_TO.location_id
AND FTHT.ASSET_ID=FDH_IN.ASSET_ID
AND FDH_IN.TRANSACTION_HEADER_ID_OUT=FTHT.TRANSACTION_HEADER_ID
AND FTHT.TRANSACTION_TYPE_CODE =''TRANSFER''
AND FDH_IN.TRANSACTION_HEADER_ID_OUT=FDH_TO.TRANSACTION_HEADER_ID_IN
AND FDH_IN.location_id=FL_IN.location_id
AND FDH_TO.location_id=FL_TO.location_id
AND FFV_IN.FLEX_VALUE_MEANING=FL_IN.SEGMENT1
AND FFV_IN.FLEX_VALUE_SET_ID=FFVS_IN.FLEX_VALUE_SET_ID
AND FFVS_IN.FLEX_VALUE_SET_NAME=''ABRL_GL_Location''
AND FFV_TO.FLEX_VALUE_MEANING=FL_TO.SEGMENT1
AND FFV_TO.FLEX_VALUE_SET_ID=FFVS_TO.FLEX_VALUE_SET_ID
AND FFVS_TO.FLEX_VALUE_SET_NAME=''ABRL_GL_Location''
AND INV.ASSET_ID(+)=FDH_IN.asset_id ';








IF p_book_code  IS NOT NULL THEN

STR_ASSET_BOOK  := ' and FTHT.BOOK_TYPE_CODE = '''|| p_book_code||'''' ;

END IF;

IF p_date_from IS NOT NULL AND  p_date_to  IS NOT NULL THEN

SELECT TO_DATE(p_date_from,'yyyy/mm/dd hh24:mi:ss') INTO v_date_from FROM dual;
SELECT TO_DATE(p_date_to,'yyyy/mm/dd hh24:mi:ss') INTO v_date_to FROM dual;

str_date:= ' and FTHT.TRANSACTION_DATE_ENTERED between '''||v_date_from||''' and ''' || v_date_to||'''';

END IF;




-- settign where conditions according to parameters

    /*  IF p_approved_flag IS NOT NULL
      THEN
         SELECT    (SELECT DECODE (p_approved_flag,
                                   'Y', 'and api.APPROVAL_STATUS_LOOKUP_CODE =',
                                   'N', 'and api.APPROVAL_STATUS_LOOKUP_CODE <>'
                                  )
                      FROM DUAL)
                || ''''
                || 'APPROVED'
                || ''''
           INTO approved_string
           FROM DUAL;
      END IF;*/

---------------------------------- ------------------
Fnd_File.put_line (Fnd_File.LOG, 'executing query ');

-- making final query according to parameters


query_string := query_string || STR_ASSET_BOOK || str_date;



-- parameter printing

Fnd_File.put_line (Fnd_File.output,
                         'ABRL Asset Transfer Report'
                        );
      Fnd_File.put_line (Fnd_File.output, ' ');
      Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'As On Date'
                         || CHR (9)
                         || SYSDATE
                         || CHR (9)
                        );
      Fnd_File.put_line (Fnd_File.output, ' ');
      Fnd_File.put_line (Fnd_File.output, 'REPORT PARAMETRS');
      Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Asset Book Name'
                         || CHR (9)
                         ||  p_book_code
                         || CHR (9)
                        );
      Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Date From'
                         || CHR (9)
                         || p_date_from
                         || CHR (9)
                        );
      Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Date To'
                         || CHR (9)
                         ||  p_date_to
                         || CHR (9)
                        );

-- display labels of the report
      Fnd_File.put_line (Fnd_File.output, ' ');
      Fnd_File.put_line (Fnd_File.output, ' ');
      Fnd_File.put_line (Fnd_File.output,
                           'Asset ID'
                         || CHR (9)
                         || 'Original Location Name'
                         || CHR (9)
                         || 'Original Location code'
                         || CHR (9)
                                     || 'Description Of Asset'
                         || CHR (9)
                         || 'Vendor Name'
                         || CHR (9)
                                     || 'Vendor code'
                         || CHR (9)
                                     || 'Invoice No.'
                         || CHR (9)
                                     || 'Invoice Date'
                         || CHR (9)
                                     || 'PO Number'
                         || CHR (9)
                                     || 'PO Date'
                         || CHR (9)
                         || 'GRN No.'
                         || CHR (9)
                         || 'GRN Date'
                         || CHR (9)
                         || 'Qty Recvd'
                         || CHR (9)
                         || 'Date Of Transfer'
                         || CHR (9)
                         || 'Transfer To Location'
                         || CHR (9)
                                     || 'Transfer To Location Code'
                         || CHR (9)
                         || 'qty Transfered'
                         || CHR (9)
                         || 'Balance QTY in Hand'
                         );





Fnd_File.put_line (Fnd_File.LOG,QUERY_STRING);

---------------------------------- ------------------
--Fnd_File.put_line (Fnd_File.output, 'before opening the cursor ');

       OPEN c
       FOR query_string;

      LOOP
         FETCH c
          INTO v_rec;

          EXIT WHEN c%NOTFOUND;


          ---------------------------------- ------------------
--Fnd_File.put_line (Fnd_File.output, 'executing ');
         IF NVL ( v_asset_id , 1) <> v_rec.v_asset_id
         THEN
            IF inv_count > 0
            THEN
               --print total vnd amt chr(9),
               Fnd_File.put_line (Fnd_File.output,
                                     'TOTAL'
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                                  || ' '
                                  || CHR (9)
                                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                                  || ' '
                                  || CHR (9)
                                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  );
            END IF;

            v_asset_id  := v_rec.v_asset_id ;

/*
            v_tot_inv_amt      :=v_rec.v_invoice_amount;
            v_tot_tds_amt      :=v_rec.v_tds_amount;
            v_tot_basic        :=v_rec.v_base_amount;
            v_tot_sur          :=v_rec.v_surcharge_amount;
            v_tot_cess         :=v_rec.v_cess_amount;
            v_tot_she_cess     :=v_rec.v_sh_cess_amount;
            v_tot_net_amt_due  :=v_rec.v_net_amount_due;*/

         ELSE
/*
            v_tot_inv_amt      :=v_tot_inv_amt     +   v_rec.v_invoice_amount;
            v_tot_tds_amt      :=v_tot_tds_amt     +   v_rec.v_tds_amount;
            v_tot_basic        :=v_tot_basic       +   v_rec.v_base_amount;
            v_tot_sur          :=v_tot_sur         +   v_rec.v_surcharge_amount;
            v_tot_cess         :=v_tot_cess        +   v_rec.v_cess_amount;
            v_tot_she_cess     :=v_tot_she_cess    +   v_rec.v_sh_cess_amount;
            v_tot_net_amt_due  :=v_tot_net_amt_due +   v_rec.v_net_amount_due;
*/        NULL;
         END IF;
         -- printing the data
         ---------------------------------- ------------------
--Fnd_File.put_line (Fnd_File.output, 'printing');




         Fnd_File.put_line (Fnd_File.output,
                               v_rec.v_asset_id
                            || CHR (9)
                            || v_rec.v_S_LOCATION_NAME
                            || CHR (9)
                            || v_rec.v_S_LOCATION_CODE
                            || CHR (9)
                            || v_rec.V_ASSET_DESC
                            || CHR (9)
                            || v_rec.v_vendor_name
                            || CHR (9)
                            || v_rec.v_VENDOR_CODE
                            || CHR (9)
                            || v_rec.v_INVOICE_NUMBER
                            || CHR (9)
                            || v_rec.v_invoice_date
                            || CHR (9)
                            || v_rec.v_PO_NUMBER
                            || CHR (9)
                            || v_rec.v_po_date
                            || CHR (9)
                            || v_rec.v_RECEIPT_NUM
                            || CHR (9)
                            || v_rec.v_receipt_date
                            || CHR (9)
                            || TO_CHAR(v_rec.v_S_QTY_RECVD )
                            || CHR (9)
                            || v_rec.v_TRANSACTION_DATE_ENTERED
                            || CHR (9)
                            || v_rec.v_T_LOCATION
                            || CHR (9)
                            || v_rec.v_T_LOCATION_NAME
                            || CHR (9)
                            || v_rec.v_T_UNIT_ASSIGNED
                            );



      /*  VEN_count := VEN_count + 1;
         inv_count := inv_count + 1;

        v_grnd_tot_inv_amt      :=v_grnd_tot_inv_amt     +    v_rec.v_invoice_amount;
        v_grnd_tot_tds_amt      :=v_grnd_tot_tds_amt     +    v_rec.v_tds_amount;
        v_grnd_tot_basic        :=v_grnd_tot_basic       +    v_rec.v_base_amount;
        v_grnd_tot_sur          :=v_grnd_tot_sur         +    v_rec.v_surcharge_amount ;
        v_grnd_tot_cess         :=v_grnd_tot_cess        +    v_rec.v_cess_amount;
        v_grnd_tot_she_cess     :=v_grnd_tot_she_cess    +    v_rec.v_sh_cess_amount;
        v_grnd_tot_net_amt_due  :=v_grnd_tot_net_amt_due +    v_rec.v_net_amount_due;
*/
       END LOOP;

   /*     Fnd_File.put_line (Fnd_File.output,
                                     'TOTAL'
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || TO_CHAR(v_tot_inv_amt)
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                                  || TO_CHAR(v_tot_tds_amt)
                                  || CHR (9)
                                                  || TO_CHAR(v_tot_basic)
                                  || CHR (9)
                                  || TO_CHAR(v_tot_sur)
                                  || CHR (9)
                                                  || TO_CHAR(v_tot_cess)
                                  || CHR (9)
                                                  || TO_CHAR(v_tot_she_cess)
                                  || CHR (9)
                                  || v_tot_net_amt_due
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  );
       Fnd_File.put_line (Fnd_File.output,
                                     'Grand Total'
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '--to_char(v_grnd_tot_inv_amt)
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                                  || TO_CHAR(v_grnd_tot_tds_amt)
                                  || CHR (9)
                                                  || TO_CHAR(v_grnd_tot_basic)
                                  || CHR (9)
                                  || TO_CHAR(v_grnd_tot_sur)
                                  || CHR (9)
                                                  || TO_CHAR(v_grnd_tot_cess)
                                  || CHR (9)
                                                  || TO_CHAR(v_grnd_tot_she_cess)
                                  || CHR (9)
                                  || TO_CHAR(v_grnd_tot_net_amt_due)
                                  || CHR (9)
                                  || ' '
                                  || CHR (9)
                                  || ' '
                                  );*/
    CLOSE c;

---------------------------------- ------------------
--Fnd_File.put_line (Fnd_File.output, 'amount closing the cursor'||v_grnd_tot_inv_amt );
END xxabrl_asset_trn_proc;
END  Xxabrl_Asset_Trnf_Pkg; 
/


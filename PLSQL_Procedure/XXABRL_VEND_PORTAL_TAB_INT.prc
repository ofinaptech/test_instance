CREATE OR REPLACE procedure APPS.xxabrl_vend_portal_tab_int(errbuf out varchar2,
                                 retcode out varchar2,
                                  P_FROM_DATE IN  VARCHAR2,
                                 P_TO_DATE IN   VARCHAR2,
                                 P_ORG_ID  NUMBER
                                 )
AS
v_from_date      VARCHAR2 (11):= TO_CHAR (TO_DATE (SUBSTR (p_from_date, 1, 10), 'RRRR/MM/DD'),'DD-MON-YYYY');
 v_to_date        VARCHAR2 (11):= TO_CHAR (TO_DATE (SUBSTR (p_to_date, 1, 10), 'RRRR/MM/DD'),'DD-MON-YYYY');

type ARRAY is table of xxabrl.xxabrl_vendor_portal_12%rowtype;

l_data ARRAY;

cursor c1 is
SELECT   povs.vendor_site_code, api.invoice_type_lookup_code INVOICE_TYPE,
            api.invoice_num, api.invoice_date, api.description description,
            apd.dist_code_combination_id ccid,gl.SEGMENT3 SBU ,(select FFVL.description FROM apps.fnd_flex_values_vl ffvl
        WHERE ffvl.FLEX_VALUE_SET_ID=1013466
        AND ffvl.FLEX_VALUE=gl.SEGMENT3
         )SBU_desc,
         gl.segment4 LOCATION_CODE
         ,(SELECT FFVL.description FROM apps.fnd_flex_values_vl ffvl
        WHERE ffvl.FLEX_VALUE_SET_ID=1013467
        AND ffvl.FLEX_VALUE=gl.SEGMENT4) Location_desc,
        gl.segment6 ACCOUNT_CODE,
          (SELECT FFVL.description FROM apps.fnd_flex_values_vl ffvl
        WHERE ffvl.FLEX_VALUE_SET_ID=1013469
        AND ffvl.FLEX_VALUE=gl.SEGMENT6) account_desc,
             api.invoice_currency_code,
            DECODE (api.invoice_type_lookup_code,
                    'CREDIT', ABS (z.amt_val)
                     - ABS (NVL (api.discount_amount_taken, 0)),
0
                   ) dr_val,
            DECODE (api.invoice_type_lookup_code,
                    'CREDIT', 0,
                    z.amt_val - NVL (api.discount_amount_taken, 0)
                   ) cr_val,null prepay_amt_remaing ,
            TO_CHAR (api.doc_sequence_value)payment_num,
            /*null pay_accounting_date,*/
            NULL check_number, null check_date , pov.segment1 VENDOR_NUM, pov.vendor_name,
            pov.vendor_type_lookup_code VENDOR_TYPE, apd.po_distribution_id,
            hr.SHORT_CODE OPERATING_UNIT , aBA.batch_NAME,
             api.invoice_id,
            null PAY_accounting_date,
            NULL payment_type_flag, NULL bank_account_name,
            NULL bank_account_num,NULL Bank_UTI_no, api.invoice_amount, api.amount_paid,
           z.accounting_date GL_DATE,NULL PREPAY_INVOICE_ID,(select NAME from ap_terms AT,apps.ap_invoices_all ai
            where AT.term_id=AI.TERMS_ID
            and ai.invoice_id=api.invoice_id) TERM_NAME,Api.PAY_GROUP_LOOKUP_CODE PAY_GROUP,
            APIL.REFERENCE_1  PPI_Invoice_Number,
         api.ATTRIBUTE2 GRN_Number,
              Api.ATTRIBUTE3 GRN_Date,null Hold_flag
       FROM apps.ap_invoices_all api,
       APPS.AP_BATCHES_ALL ABA ,
            apps.ap_invoice_lines_all apil,
            apps.ap_invoice_distributions_all apd,
               --apps.ap_invoice_distributions_all apd1,
            apps.ap_suppliers pov,
            apps.gl_code_combinations gl,
           --- apps.ap_payment_schedules_all aps,
            apps.hr_operating_units hr,
            apps.ap_supplier_sites_all povs,
           (SELECT   NVL (SUM (apd.amount), 0) amt_val, api.invoice_id,apd.ACCOUNTING_DATE
                 FROM apps.ap_invoices_all api,
                      apps.ap_invoice_lines_all apil,
                      apps.ap_invoice_distributions_all apd,
                      apps.ap_suppliers pov,
                      apps.ap_supplier_sites_all povs
                WHERE api.invoice_id = apd.invoice_id
                  AND apil.invoice_id = api.invoice_id
                  AND apil.line_number = apd.invoice_line_number
                  AND api.vendor_id = pov.vendor_id
                  and api.invoice_type_lookup_code <> 'PREPAYMENT'
                  AND api.vendor_site_id = povs.vendor_site_id
               --   AND apd.match_status_flag = 'A'
                  and apd.ACCOUNTING_DATE between '1-apr-2011'and '31-mar-2012'
                  AND apil.line_type_lookup_code <> 'PREPAY'
             GROUP BY api.invoice_id,apd.ACCOUNTING_DATE) z
      WHERE api.invoice_id = z.invoice_id
        AND api.invoice_id = apd.invoice_id
        AND ABA.BATCH_ID(+)=API.BATCH_ID
        and api.org_id=hr.ORGANIZATION_ID
       -- and apd.invoice_id=apd1.invoice_id
        and gl.CODE_COMBINATION_ID=api.ACCTS_PAY_CODE_COMBINATION_ID
        AND apil.invoice_id = api.invoice_id
        --and aps.INVOICE_ID=api.INVOICE_ID
        AND apil.line_number = apd.invoice_line_number
         AND api.vendor_site_id = povs.vendor_site_id
         -- and api.invoice_id =58378410
         AND apd.ROWID =
               (SELECT ROWID
                  FROM APPS.ap_invoice_distributions_all
                 WHERE ROWNUM = 1
                   AND invoice_id = apd.invoice_id)                
        AND api.vendor_id = pov.vendor_id
        AND api.invoice_type_lookup_code <> 'PREPAYMENT'
        and ap_invoices_pkg.get_approval_status
                     (api.invoice_id,
                      api.invoice_amount,
                      api.payment_status_flag,
                      api.invoice_type_lookup_code
                     ) in ('APPROVED','CANCELLED')
       -- AND apd.match_status_flag = 'A'
       -- and nvl(aps.AMOUNT_REMAINING,0)<>0
      --  and apd.ACCOUNTING_DATE<'1-apr-2012'
      --  and api.invoice_num='DN_DELKN_AUG'11_475'
         -- and pov.segment1='9700381'
          and api.org_id= P_ORG_ID--in ('104974824',101629786,101629804,104659800,104659802,66703510,101371820)
       --  and api.org_ID =901--,801,961,861)
         --AND apd.line_type_lookup_code  IN ( 'ITEM','MISCELLANEOUS')
 -- and api.invoice_id='78135429'
   --- AND (trunc(api.LAST_UPDATE_DATE) >=P_RUN_DATE OR
   ---   trunc(apiL.LAST_UPDATE_DATE) >= P_RUN_DATE  OR
   --   trunc(apD.LAST_UPDATE_DATE) >= P_RUN_DATE )
       and apd.ACCOUNTING_DATE between  v_from_date and  v_TO_date
         AND (   (api.invoice_type_lookup_code <> 'DEBIT')
             OR (api.invoice_type_lookup_code = 'DEBIT') 
             )
             UNION ALL
   SELECT    povs.vendor_site_code, DECODE(API.INVOICE_TYPE_LOOKUP_CODE,'PREPAYMENT',API.INVOICE_TYPE_LOOKUP_CODE,'PAYMENT') INVOICE_TYPE,
            api.invoice_num, api.invoice_date, apd.description description,
            apd.dist_code_combination_id ccid,gl.SEGMENT3 ,(select FFVL.description FROM apps.fnd_flex_values_vl ffvl
        WHERE ffvl.FLEX_VALUE_SET_ID=1013466
        AND ffvl.FLEX_VALUE=gl.SEGMENT3
         )SBU_desc,gl.segment4
         ,(SELECT FFVL.description FROM apps.fnd_flex_values_vl ffvl
        WHERE ffvl.FLEX_VALUE_SET_ID=1013467
        AND ffvl.FLEX_VALUE=gl.SEGMENT4) Location_desc,
          gl.segment6,
          (SELECT FFVL.description FROM apps.fnd_flex_values_vl ffvl
        WHERE ffvl.FLEX_VALUE_SET_ID=1013469
        AND ffvl.FLEX_VALUE=gl.SEGMENT6) account_desc,api.payment_currency_code,
            DECODE (api.invoice_type_lookup_code,
                    'CREDIT', DECODE (status_lookup_code, 'VOIDED', 0, 0),
                    app.amount
                   ) dr_val,
            DECODE (api.invoice_type_lookup_code,
                    'CREDIT', DECODE (status_lookup_code,
                                      'VOIDED', app.amount,
                                      ABS (app.amount)
                                     ),
0
                   ) cr_val, null pre_amt_remaing,
            DECODE (api.payment_status_flag,
                    'Y', TO_CHAR (apc.doc_sequence_value),
                    'P', TO_CHAR (apc.doc_sequence_value),
                    TO_CHAR (apc.doc_sequence_value),'N',TO_CHAR (apc.doc_sequence_value)
                   ) payment_num,
           /* DECODE (api.payment_status_flag,
                    'Y', TO_CHAR (app.accounting_date, 'dd-MON-yyyy'),
                    'P', TO_CHAR (app.accounting_date, 'dd-MON-yyyy')
                   ) pay_accounting_date,*/
            DECODE (api.payment_status_flag,
                    'Y', TO_CHAR (apc.check_number),
                    'P', TO_CHAR (apc.check_number)
                   ) check_number,apc.check_date ,
            pov.segment1 VENDOR_NUM, pov.vendor_name, pov.vendor_type_lookup_code VENDOR_TYPE,
            apd.po_distribution_id,hr.SHORT_CODE OPERATING_UNIT ,
            aBA.batch_NAME, api.invoice_id,
            app.accounting_date, decode(apc.payment_type_flag,'Q','QUICK','M', 'MANUAL','R','REFUND') PAYMENT_TYPE, apc.bank_account_name,
            apc.bank_account_num,apc.attribute3 Bank_UTI_no, NULL invoice_amount, decode(api.INVOICE_TYPE_LOOKUP_CODE,'PREPAYMENT',API.AMOUNT_PAID,NULL) AMOUNT_PAID ,
            APC.CHECK_DATE gl_date,NULL PREPAY_INVOICE_ID,NULL TERM_NAME,NULL PAY_GROUP,NULL PPI_Invoice_Number,
            NULL GRN_Number, NULL GRN_DATE,APS.HOLD_FLAG
       FROM APPS.ap_invoices_all api,
            APPS.ap_invoice_lines_all apil,
            APPS.AP_BATCHES_ALL ABA,
            apps.gl_code_combinations gl,
            APPS.ap_invoice_distributions_all apd,
            APPS.AP_SUPPLIERS pov,
            apps.hr_operating_units hr,
            APPS.ap_invoice_payments_all app,
          apps.ap_payment_schedules_all aps,
            APPS.ap_checks_all apc,
            APPS.AP_SUPPLIER_sites_all povs
      WHERE api.invoice_id = apd.invoice_id
        AND apil.invoice_id = api.invoice_id
        AND ABA.BATCH_ID(+)=API.BATCH_ID
       and aps.INVOICE_ID(+)=api.INVOICE_ID
        and hr.ORGANIZATION_ID=api.ORG_ID
        and gl.CODE_COMBINATION_ID=api.ACCTS_PAY_CODE_COMBINATION_ID
        AND apil.line_number = apd.invoice_line_number
        AND apd.ROWID =
               (SELECT ROWID
                  FROM APPS.ap_invoice_distributions_all
                 WHERE ROWNUM = 1
                   AND invoice_id = apd.invoice_id)
                  -- AND match_status_flag = 'A')
        AND api.vendor_id = pov.vendor_id
        AND app.invoice_id = api.invoice_id
        AND app.check_id = apc.check_id
        AND apc.status_lookup_code IN
               ('CLEARED', 'NEGOTIABLE', 'VOIDED', 'RECONCILED UNACCOUNTED',
                'RECONCILED', 'CLEARED BUT UNACCOUNTED')
        --AND apd.match_status_flag = 'A'
        AND api.vendor_site_id = povs.vendor_site_id
       AND api.invoice_type_lookup_code <> 'PREPAYMENT'
       -- and nvl(aps.AMOUNT_REMAINING,0)<>0
   --  and   api.invoice_id='78135429'
     and api.org_id= P_ORG_ID
     -- and api.org_ID =901--,801,961,861)
      --and api.invoice_id in ('104974824',101629786,101629804,104659800,104659802,66703510,101371820)
      --and api.invoice_num in ('100390137','HN-3221257','DN SM 06219/Hyd/Chq/Off/07/12-13')
       AND  APP.ACCOUNTING_DATE between v_from_date and  v_TO_date ;
       
       
       BEGIN
       OPEN C1;
       LOOP
       
       FETCH C1 BULK COLLECT INTO L_DATA ;
       
       FORALL i IN 1..l_data.COUNT
       INSERT INTO XXABRL.xxabrl_vendor_portal_12 VALUES l_data(i);
       EXIT WHEN c1%NOTFOUND;
    END LOOP;
    CLOSE c1;
    
    COMMIT;
    
    END xxabrl_vend_portal_tab_int; 
/


CREATE OR REPLACE PACKAGE BODY APPS.XXABRL_BANK_BALANCE_PKG
AS
 /*
  =========================================================================================================
  ||   Filename   : XXABRL_RESA_AR_CONV_PKG.sql
  ||   Description : Script is used to mold ReSA data for AR
  ||
  ||   Version     Date            Author              Modification
  ||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
  ||   1.0.0       13-MON-2008    Hans Raj Kasana      New Development
  ||   1.0.1       24-SEP-2009    Shailesh Bharambe    Opening Balance modification
  ||   1.0.2       05-jan-2010    Shailesh Bharambe    Description of the payables query having TAB char so removed TAB char.  
  ||   1.0.3       11-Feb-2010    Shailesh Bharambe    Removing to_char from the cash management query as its not recognizing the 2010 data .
  ||   1.0.4       27-APr-2010    Shailesh Bharambe    In receivable data the reversed  receipt should come the gl_date wise as per the receipt history table
  ||   1.0.5       11-Jan-2010    Mitul                Added Opearting Unit Field 
  ||   1.0.6       19-OCt-2011    Nijam                Added logic for Reversed Receipt MOUNT IS NEGATIVE
  ||   1.0.6        14-Jun-2012   Vikash Kumar         Added logic for Cash Clearing account,Receipt Clearing account,Remittance Account in the Manual Journal Select Query
  ||   1.0.7        08-Aug-2012   Vikash Kumar         Increased the Description size for 'General Ledger' source query as earlier some of the lines were not coming
  ||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
  ========================================================================================================*/
PROCEDURE Print_log(
                      p_str IN VARCHAR2,
                      p_debug_flag IN VARCHAR2
                      )
  AS
  BEGIN
       IF p_debug_flag = 'Y' THEN

          Fnd_File.PUT_LINE(Fnd_File.LOG,p_str);
       END IF;

       IF p_debug_flag = 'O' THEN

          Fnd_File.PUT_LINE(Fnd_File.Output,p_str);
       END IF;
  END;
PROCEDURE Main(
               Err_Buf OUT VARCHAR2,
               Ret_code OUT NUMBER,
               p_BANK_ACCOUNT_ID IN NUMBER,
               p_period IN VARCHAR2,
               p_to_date1 IN VARCHAR2,
               p_show_un_reconciled IN VARCHAR2,
               p_report_format  IN VARCHAR2
               )
AS
v_cnt NUMBER :=0;
v_org_id NUMBER := Fnd_Profile.VALUE('ORG_ID');
gc_open_bal_status VARCHAR2(100) := 'FOUND';
v_Bank_seg3        VARCHAR2(100);
v_Bank_seg6        VARCHAR2(100);
p_to_date DATE;
 p_bank_account_no VARCHAR2(250);

BEGIN

v_org_id := Fnd_Profile.VALUE('ORG_ID');
--delete FROM XXABRL_BANK_BAL_DATA_GT222;
--commit;

-- added by shailesh on 18 march 2009
-- passing the p_bank_account_no from parameter but here we need to pass bank account name only so retrving the bank account name
BEGIN

SELECT BANK_ACCOUNT_NAME
INTO p_bank_account_no
FROM CE_BANK_ACCOUNTS CBA
WHERE BANK_ACCOUNT_ID=p_BANK_ACCOUNT_ID;

EXCEPTION WHEN OTHERS THEN
p_bank_account_no :='';
Print_log('Bank Not Found','Y');
END ;

SELECT TO_CHAR(TO_DATE(p_to_date1,'yyyy/mm/dd hh24:mi:ss'),'dd-mon-yyyy') INTO p_to_date FROM dual;

--Print_log('OU: '||v_org_id,'O');

/*begin
mo_global.set_policy_context('S',v_org_id);
end;*/

BEGIN
     SELECT vl.FLEX_VALUE INTO v_Bank_seg6
            FROM CE_BANK_ACCOUNTS         CBA,
                 gl_code_combinations_kfv gc,
                 fnd_flex_values_vl       vl,
                 fnd_flex_value_sets      vs
           WHERE TRUNC(NVL(CBA.End_date, SYSDATE)) >= TRUNC(SYSDATE)
             AND CBA.ASSET_CODE_COMBINATION_ID NOT IN
                 (1191, 1209, 1402, 1481, 9014)
             AND CBA.bank_account_name = 'CORP HDFC BANK-9518'
             AND gc.segment6 = vl.FLEX_VALUE
             AND vl.FLEX_VALUE_SET_ID = vs.flex_value_set_id
             AND CBA.ASSET_CODE_COMBINATION_ID = gc.CODE_COMBINATION_ID;

EXCEPTION
WHEN NO_DATA_FOUND THEN
Print_log('Bank Segement 6 is not defined','Y');
WHEN TOO_MANY_ROWS THEN
Print_log('Multiple Bank Segement 6 is defined','Y');
WHEN OTHERS THEN
Print_log('Exception in Bank Segement 6 : '||SQLERRM,'Y');
END;



BEGIN


Print_log('Start Of queries','Y');

Print_log('Organization ID'||v_org_id,'Y');

fnd_file.put_line(fnd_file.log,'org id before query From Profile Option is  '||v_org_id);

  INSERT INTO XXABRL_BANK_BAL_DATA_GT
  -->> Openning Balance
  /*
     Updated by shailesh on 02-FEB-2009
     query modification :- Added receipt clearing ccid  and remmitance ccid condition.

     addition of extra code identified by -- after the row

     added code is as :-
           from clause :-
                CE_BANK_ACCT_USES_ALL cbau,       --
                CE_GL_ACCOUNTS_CCID cgac          --
           Where Clause:-
                 OR  BA.Code_Combination_Id = cgac.RECEIPT_CLEARING_CCID      --
                 OR  BA.Code_Combination_Id = cgac.REMITTANCE_CCID            --

                 AND cbau.bank_acct_use_id=cgac.bank_acct_use_id  --
                 AND cba.bank_account_id=cbau.bank_account_id     --
  */
    SELECT 
  CBA.bank_account_name,
  /*BA.Period_Name*/
  TO_CHAR(ADD_MONTHS(TO_DATE(BA.Period_Name,'MON-YY'),1),'MON-YY') rDate,
  'GL' SOURCE,
  'Op.Balance' Particulars,
  '' Party_Name,
  '' Description,
  '' Docu_No,
  SUM(DECODE('E',
                        'E',
                        DECODE(BA.currency_code,
                               'INR',
                               DECODE(BA.actual_flag,
                                      'A',
                                      (NVL(BA.begin_balance_dr_beq, 0) -
                                      NVL(BA.begin_balance_cr_beq, 0)) +
                                      (NVL(BA.period_net_dr_beq, 0) -
                                      NVL(BA.period_net_cr_beq, 0)),
                                      (NVL(BA.begin_balance_dr, 0) -
                                      NVL(BA.begin_balance_cr, 0)) +
                                      (NVL(BA.period_net_dr, 0) -
                                      NVL(BA.period_net_cr, 0))),
                               (NVL(BA.begin_balance_dr, 0) -
                               NVL(BA.begin_balance_cr, 0)) +
                               (NVL(BA.period_net_dr, 0) -
                               NVL(BA.period_net_cr, 0))),
                        (NVL(BA.begin_balance_dr, 0) -
                        NVL(BA.begin_balance_cr, 0)) +
                        (NVL(BA.period_net_dr, 0) - NVL(BA.period_net_cr, 0))) / 1) Amount,
    0  Cu_Balance,
    '' Reconciliation_Status,
    '' Reconciliation_Date,
    '' Remarks
    ,'' Voucher_number
    ,cbau.org_id
    FROM
    GL_BALANCES BA,
    CE_BANK_ACCOUNTS CBA ,
    CE_BANK_ACCT_USES_ALL cbau,       --
    CE_GL_ACCOUNTS_CCID cgac          --
   WHERE
   (
   BA.Code_Combination_Id = CBA.CASH_CLEARING_CCID
   OR  BA.Code_Combination_Id = CBA.ASSET_CODE_COMBINATION_ID
   OR  BA.Code_Combination_Id = cgac.RECEIPT_CLEARING_CCID      --
   OR  BA.Code_Combination_Id = cgac.REMITTANCE_CCID            --
   )
   AND
   CBA.BANK_ACCOUNT_NAME =NVL(p_bank_account_no,CBA.BANK_ACCOUNT_NAME)
   AND BA.Period_Name = NVL(p_period,BA.Period_Name)
   AND cbau.bank_acct_use_id=cgac.bank_acct_use_id  --
   AND cba.bank_account_id=cbau.bank_account_id     --
-- commented  by shailesh on 14-sep-2009 as the opening balance is not comming properly.
-- following condition commented
-- and cbau.org_id = v_org_id 
  -- and cbau.org_id = 146
  and DECODE(v_org_id,147,1,cbau.org_id) = DECODE(v_org_id,147,1,v_org_id)
--  and cbau.org_id = v_org_id 
--AND ROWNUM=1
   GROUP BY
   CBA.bank_account_name,cbau.org_id,
   BA.Period_Name
   UNION ALL
   -->> Payments
   /*select
         acv.BANK_ACCOUNT_NAME,
         to_char(acv.CHECK_DATE,'DD-MON-YYYY') CHECK_DATE,
         'Payables' Source,
         'Payments' Particulars,
         acv.CURRENT_VENDOR_NAME Party_Name,
         (SELECT Description FROM AP_INVOICE_PAYMENTS_V ipv
         WHERE acv.PAYMENT_ID = ipv.PAYMENT_ID(+)
         and rownum =1) Description,
         to_char(acv.check_number) Docu_No,
         acv.AMOUNT * -1 AMOUNT_AMT,     --Payment should show -ve
         0 Curr_Balance,
         (select status from   CE_STATEMENT_LINES
         where bank_trx_number(+) =to_char(acv.check_number)) Reconciliation_Status,
         to_char(acv.check_date,'DD-MON-YYYY') Reconciliation_Date,
         '' Remarks
    from \*AP_CHECKS_V*\apps.AP_CHECKS_V2 acv
    where acv.bank_account_name =nvl(p_bank_account_no,acv.bank_account_name)
    and acv.CHECK_DATE between add_months(to_date(p_period,'MON-YY'),1) and to_date(p_to_date,'DD-MON-RRRR')*/
    --and acv.CHECK_STATUS='Reconciled'
    --Union All
  -->> Receipts
  /*select (SELECT cba.bank_account_name
                  FROM ce_bank_acct_uses_all  ba,
                  ce_bank_accounts   cba,
                  ce_bank_branches_v bb
                  WHERE bb.bank_institution_type = 'BANK'
                  AND bb.branch_party_id = cba.bank_branch_id
                  AND cba.bank_account_id = ba.bank_account_id
                  AND cba.account_classification = 'INTERNAL'
                  AND ba.bank_acct_use_id = REMIT_BANK_ACCT_USE_ID)
         BANK_ACCOUNT_NAME,
         to_char(RECEIPT_DATE,'DD-MON-YYYY') RECEIPT_DATE,
         'Receivables' Source,
         'Receipt' Particulars,
         (SELECT Hp.Party_name
          FROM   HZ_PARTIES HP
            ,HZ_CUST_ACCOUNTS       HCA
                ,HZ_CUST_ACCT_SITES_ALL HCAS
                ,HZ_CUST_SITE_USES_ALL  HCSUA
          where
      hca.party_id = hp.party_id
      AND hcas.cust_account_id       = hca.cust_account_id
          AND   hcsua.cust_acct_site_id    = hcas.cust_acct_site_id
          AND   hcsua.site_use_code        = 'BILL_TO'
      AND hcsua.site_use_id = CUSTOMER_SITE_USE_ID
      )
          Party_Name,
         COMMENTS    Description,
         to_char(RECEIPT_NUMBER) Document_No,
         Amount Amount,
         0 Cur_Balance,
         State_DSP Reconciliation_Status,
         to_char(RECEIPT_DATE,'DD-MON-YYYY') Reconciliation_Date,
         '' Remarks
    from \*ar_cash_receipts_all*\ AR_CASH_RECEIPTS_V
    where
    to_char(RECEIPT_DATE,'DD-MON-YYYY') between add_months(to_date(p_period,'MON-YY'),1) and to_date(p_to_date,'DD-MON-YYYY')
    And
  (SELECT cba.bank_account_name
                  FROM ce_bank_acct_uses_all  ba,
                  ce_bank_accounts   cba,
                  ce_bank_branches_v bb
                  WHERE bb.bank_institution_type = 'BANK'
                  AND bb.branch_party_id = cba.bank_branch_id
                  AND cba.bank_account_id = ba.bank_account_id
                  AND cba.account_classification = 'INTERNAL'
                  AND ba.bank_acct_use_id = REMIT_BANK_ACCT_USE_ID) = p_bank_account_no
                  --AND State_DSP <> 'Reversed'
  Union All*/
  -->> Bank Transfer
  SELECT distinct
srcBA.bank_account_name Bank_Ac_Name,
         TO_CHAR(PaymentTransactionsEO.TRANSACTION_DATE,'DD-MON-YYYY') rDate,
         'Cash_Management' SOURCE,
         'Bank_Transfer' Particulars,
         DECODE(cf.cashflow_direction,'PAYMENT',destBA.bank_account_name,
         (SELECT
         srcBA.bank_account_name
         FROM CE_BANK_ACCOUNTS srcBA
         WHERE
         srcBA.bank_account_id =
         PaymentTransactionsEO.source_bank_account_id
         )) Party_name,
         SUBSTR(PaymentTransactionsEO.TRANSACTION_DESCRIPTION,1,30) DESCRIPTION,
         TO_CHAR(cf.trxn_reference_number) Doc_Num,
         DECODE(cf.cashflow_direction,'PAYMENT',PaymentTransactionsEO.PAYMENT_AMOUNT * -1,PaymentTransactionsEO.PAYMENT_AMOUNT) Amount,
         0 Cur_Balance ,
         PaymentTransactionsEO.TRXN_STATUS_CODE Reconciliation_Status,
         TO_CHAR(PaymentTransactionsEO.Transaction_date,'DD-MON-YYYY') Reconciliation_Date,
         '' Remarks
         ,'' Voucher_number
          ,0 org_id
    FROM CE_PAYMENT_TRANSACTIONS PaymentTransactionsEO,
         CE_BANK_ACCOUNTS        srcBA,
         CE_BANK_ACCOUNTS        destBA,
         CE_BANK_BRANCHES_V      srcBB,
         CE_BANK_BRANCHES_V      destBB,
         XLE_ENTITY_PROFILES     srcLE,
         XLE_ENTITY_PROFILES     destLE,
         HZ_PARTY_SITES          hps,
         IBY_PAYMENT_METHODS_VL  ipm,
         CE_LOOKUPS              lk1,
         CE_LOOKUPS              lk2,
         CE_SYSTEM_PARAMETERS    csp,
         CE_TRXNS_SUBTYPE_CODES  tst,
         IBY_PAYMENT_REASONS_VL  pr,
         ce_cashflows            cf
   WHERE (
         srcBA.bank_account_id =
         PaymentTransactionsEO.source_bank_account_id
         OR
         srcBA.bank_account_id =
         PaymentTransactionsEO.DESTINATION_BANK_ACCOUNT_ID
         )
     AND srcBB.branch_party_id(+) = srcBA.bank_branch_id
     AND srcLE.legal_entity_id(+) =
         PaymentTransactionsEO.source_legal_entity_id
     AND destBA.bank_account_id(+) =
         PaymentTransactionsEO.destination_bank_account_id
     AND destBB.branch_party_id(+) = destBA.bank_branch_id
     AND destLE.legal_entity_id(+) =
         PaymentTransactionsEO.destination_legal_entity_id
     AND srcLE.party_id(+) = PaymentTransactionsEO.source_party_id
     AND destLE.party_id(+) = PaymentTransactionsEO.destination_party_id
     AND hps.party_id(+) = PaymentTransactionsEO.destination_party_id
     AND hps.identifying_address_flag(+) = 'Y'
     AND ipm.payment_method_code(+) =
         PaymentTransactionsEO.payment_method_code
     AND lk1.lookup_type = 'CE_BAT_STATUS'
     AND lk1.lookup_code = PaymentTransactionsEO.trxn_status_code
     AND lk2.lookup_type = 'YES/NO'
     AND lk2.lookup_code = PaymentTransactionsEO.settle_by_system_flag
     AND tst.trxn_subtype_code_id(+) =
         PaymenttransactionsEO.trxn_subtype_code_id
     AND csp.legal_entity_id(+) = srcBA.account_owner_org_id
     AND pr.payment_reason_code(+) =
         PaymentTransactionsEO.payment_reason_code
  AND srcBA.BANK_ACCOUNT_NAME = NVL(p_bank_account_no,srcBA.BANK_ACCOUNT_NAME)
 -- AND TO_CHAR(PaymentTransactionsEO.TRANSACTION_DATE,'DD-MON-YYYY') BETWEEN ADD_MONTHS(TO_DATE(p_period,'MON-YY'),1) AND TO_DATE(p_to_date1,'yyyy/mm/dd hh24:mi:ss')
 -- above condition commented on 11-feb-10 for Removing to_char from the cash management query as its not recognizing the 2010 data New condition as follows.
  AND PaymentTransactionsEO.TRANSACTION_DATE BETWEEN ADD_MONTHS(TO_DATE(p_period,'MON-YY'),1) AND TO_DATE(p_to_date1,'yyyy/mm/dd hh24:mi:ss')
  AND PaymentTransactionsEO.TRXN_STATUS_CODE = 'SETTLED'
  AND PaymentTransactionsEO.trxn_ReferencE_number = cf.trxn_ReferencE_number
  AND cf.CASHFLOW_BANK_ACCOUNT_ID = srcBA.bank_account_id
  /*SELECT srcBA.bank_account_name Bank_Ac_Name,
         to_char(PaymentTransactionsEO.TRANSACTION_DATE,'DD-MON-YYYY') rDate,
         'Cash_Management' Source,
         'Bank_Transfer' Particulars,
         destBA.bank_account_name Party_name,
         PaymentTransactionsEO.TRANSACTION_DESCRIPTION DESCRIPTION,
         (select to_char(cf.trxn_reference_number)
         from
         ce_cashflows            cf
         where
         PaymentTransactionsEO.trxn_ReferencE_number = cf.trxn_ReferencE_number(+)
         and cf.CASHFLOW_DIRECTION    ='PAYMENT') Doc_Num,
         PaymentTransactionsEO.PAYMENT_AMOUNT * -1 Amount, --Bank Transfer should show -ve
         0 Cur_Balance ,
         PaymentTransactionsEO.TRXN_STATUS_CODE Reconciliation_Status,
         to_char(PaymentTransactionsEO.Transaction_date,'DD-MON-YYYY') Reconciliation_Date,
         '' Remarks
    FROM CE_PAYMENT_TRANSACTIONS PaymentTransactionsEO,
         CE_BANK_ACCOUNTS        srcBA,
         CE_BANK_ACCOUNTS        destBA,
         CE_BANK_BRANCHES_V      srcBB,
         CE_BANK_BRANCHES_V      destBB,
         XLE_ENTITY_PROFILES     srcLE,
         XLE_ENTITY_PROFILES     destLE,
         HZ_PARTY_SITES          hps,
         IBY_PAYMENT_METHODS_VL  ipm,
         CE_LOOKUPS              lk1,
         CE_LOOKUPS              lk2,
         CE_SYSTEM_PARAMETERS    csp,
         CE_TRXNS_SUBTYPE_CODES  tst,
         IBY_PAYMENT_REASONS_VL  pr
   WHERE srcBA.bank_account_id(+) =
         PaymentTransactionsEO.source_bank_account_id
     AND srcBB.branch_party_id(+) = srcBA.bank_branch_id
     AND srcLE.legal_entity_id(+) =
         PaymentTransactionsEO.source_legal_entity_id
     AND destBA.bank_account_id(+) =
         PaymentTransactionsEO.destination_bank_account_id
     AND destBB.branch_party_id(+) = destBA.bank_branch_id
     AND destLE.legal_entity_id(+) =
         PaymentTransactionsEO.destination_legal_entity_id
     AND srcLE.party_id(+) = PaymentTransactionsEO.source_party_id
     AND destLE.party_id(+) = PaymentTransactionsEO.destination_party_id
     AND hps.party_id(+) = PaymentTransactionsEO.destination_party_id
     AND hps.identifying_address_flag(+) = 'Y'
     AND ipm.payment_method_code(+) =
         PaymentTransactionsEO.payment_method_code
     and lk1.lookup_type = 'CE_BAT_STATUS'
     AND lk1.lookup_code = PaymentTransactionsEO.trxn_status_code
     AND lk2.lookup_type = 'YES/NO'
     AND lk2.lookup_code = PaymentTransactionsEO.settle_by_system_flag
     AND tst.trxn_subtype_code_id(+) =
         PaymenttransactionsEO.trxn_subtype_code_id
     AND csp.legal_entity_id(+) = srcBA.account_owner_org_id
     AND pr.payment_reason_code(+) =
         PaymentTransactionsEO.payment_reason_code
  AND srcBA.bank_account_name = nvl(p_bank_account_no,srcBA.bank_account_name)
  AND to_char(PaymentTransactionsEO.TRANSACTION_DATE,'DD-MON-YYYY') between add_months(to_date(p_period,'MON-YY'),1) and to_date(p_to_date,'DD-MON-YYYY')
  AND PaymentTransactionsEO.TRXN_STATUS_CODE = 'SETTLED'*/
  -->> Journals
  UNION ALL
  /*

  Updated by shailesh on 31 jan 2009.
  Added following code in this query and changes are mentioned by -- in following query
  Desc:-
  After addition of these condition it will not fetch the journals which are comming
  from payable,receivable and cash managament

     gl_je_headers_v  gjh,        --
     GL_JE_BATCHES_HEADERS_V gjbh --

     gjlv.je_header_id=gjh.je_header_id AND                              --
         gjh.je_batch_id=gjbh.je_batch_id      AND                           --
         gjbh.je_header_id=gjh.je_header_id  AND                             --
         gjbh.JE_SOURCE NOT IN ( 'Receivables','Payables','Cash Management') and --
  */
  select Bank_Acct_Name,
EFFECTIVE_DATE,
SOURCE,
Particulars,
Party_Name,
Description,
DOC_Number,
amount,
Cu_Balance,
Reconciliation_Status,
Reconciliation_Date,
Remarks,
Voucher_number,
org_id
from(
  SELECT distinct (SELECT
          CBA.bank_account_name
          FROM apps.CE_BANK_ACCOUNTS CBA,
          apps.CE_BANK_ACCT_USES_ALL cbau,       --
           apps.CE_GL_ACCOUNTS_CCID cgac
          WHERE 1=1
          AND cbau.bank_acct_use_id=cgac.bank_acct_use_id  --
         AND cba.bank_account_id=cbau.bank_account_id 
          AND (gjlv.CODE_COMBINATION_ID =CBA.ASSET_CODE_COMBINATION_ID 
         or gjlv.CODE_COMBINATION_ID=cba.CASH_CLEARING_CCID
         or gjlv.CODE_COMBINATION_ID=cgac.RECEIPT_CLEARING_CCID
         or gjlv.CODE_COMBINATION_ID=cgac.REMITTANCE_CCID 
         )
          AND TRUNC(NVL(CBA.End_date,SYSDATE)) >= TRUNC(SYSDATE)
          AND UPPER(BANK_ACCOUNT_NAME) NOT LIKE UPPER('%Not%Use%')
          and rownum<=1
        -- AND CBA.ASSET_CODE_COMBINATION_ID NOT IN (1191,1209,1402,1481,9014)
          ) Bank_Acct_Name, --Derive Bank Acct Name
         TO_CHAR(gjlv.EFFECTIVE_DATE,'DD-MON-YYYY') EFFECTIVE_DATE,
         'General Ledger' SOURCE,
         'Journal' Particulars,
         '' Party_Name,
         gjlv.Description, --added by Vikash Kumar on 24-Jul-2012 as some of the lines were not coming in the report.
         --SUBSTR(gjlv.Description,1,30) Description,  -- put identifier of column gjlv.  --commented by Vikash Kumar on 24-Jul-2012 as some of the lines were not coming in the report.
         (SELECT TO_CHAR(DOC_SEQUENCE_VALUE) DOC_SEQUENCE_VALUE
            FROM gl_je_headers
           WHERE JE_HEADER_ID = gjlv.JE_HEADER_ID) DOC_Number,
           -- added by shailesh on 19th june as the cumulative total is not comming due to accounted _cr is having null value
           -- so applied the nvl condition for it.
          decode((gjlv.ACCOUNTED_DR),0,NVL(gjlv.ACCOUNTED_CR,0)*-1,null,NVL(gjlv.ACCOUNTED_CR,0)*-1,(gjlv.ACCOUNTED_DR)) amount,
         0 Cu_Balance,
         DECODE(gjlv.STATUS,'P','POSTED',gjlv.STATUS) Reconciliation_Status,
         '' Reconciliation_Date,
         '' Remarks
         ,'' Voucher_number
          ,gjh.org_id
          ,gjlv.JE_LINE_NUM
    FROM GL_JE_LINES_V gjlv,
    gl_je_headers_v  gjh,        --
    GL_JE_BATCHES_HEADERS_V gjbh, --
    CE_BANK_ACCOUNTS CBA,
    apps.CE_BANK_ACCT_USES_ALL cbau,       --
    apps.CE_GL_ACCOUNTS_CCID cgac
   WHERE 1=1
   and gjlv.je_header_id=gjh.je_header_id                                 
      AND   gjh.je_batch_id=gjbh.je_batch_id                                 
        AND gjbh.je_header_id=gjh.je_header_id  
         AND cbau.bank_acct_use_id=cgac.bank_acct_use_id  
        AND cba.bank_account_id=cbau.bank_account_id                                                       
        AND   gjbh.JE_SOURCE NOT IN ( 'Receivables','Payables','Cash Management')  
 AND  TO_CHAR(gjlv.EFFECTIVE_DATE,'DD-MON-YYYY') BETWEEN ADD_MONTHS(TO_DATE(p_period,'MON-YY'),1) AND TO_DATE(p_to_date1,'yyyy/mm/dd hh24:mi:ss')
   -- added by shailesh on feb 27022009 original code rimmed
      AND CBA.BANK_ACCOUNT_NAME= p_bank_account_no
         AND (gjlv.CODE_COMBINATION_ID =CBA.ASSET_CODE_COMBINATION_ID 
         or gjlv.CODE_COMBINATION_ID=cba.CASH_CLEARING_CCID
         or gjlv.CODE_COMBINATION_ID=cgac.RECEIPT_CLEARING_CCID
         or gjlv.CODE_COMBINATION_ID=cgac.REMITTANCE_CCID 
         )
          AND TRUNC(NVL(CBA.End_date,SYSDATE)) >= TRUNC(SYSDATE)
          AND UPPER(BANK_ACCOUNT_NAME) NOT LIKE UPPER('%Not%Use%')) ;
          --AND CBA.ASSET_CODE_COMBINATION_ID NOT IN (1191,1209,1402,1481,9014);
   /*
   AND
   --gjlv.segment6 = nvl(v_Bank_seg6,gjlv.segment6)
   (SELECT
          CBA.bank_account_name
          FROM CE_BANK_ACCOUNTS CBA
          WHERE
          CBA.ASSET_CODE_COMBINATION_ID =gjlv.CODE_COMBINATION_ID
          AND TRUNC(NVL(CBA.End_date,SYSDATE)) >= TRUNC(SYSDATE)
          AND CBA.ASSET_CODE_COMBINATION_ID NOT IN (1191,1209,1402,1481,9014)
          ) = p_bank_account_no*/
  --group by  gjlv.CODE_COMBINATION_ID, gjlv.EFFECTIVE_DATE,gjlv.JE_HEADER_ID,gjlv.STATUS

  Print_log('Opening bal : ledger : bank transfer data inserted','Y');
  
    

  FOR v_cur_org IN (SELECT organization_id FROM hr_operating_units)
  LOOP

  BEGIN
       mo_global.set_policy_context('S',v_cur_org.organization_id);
  END;

  Print_log('payemnets and receipts for org :- '||v_cur_org.organization_id,'Y');

   Print_log('Inserting Receipts','Y');
  INSERT INTO XXABRL_BANK_BAL_DATA_GT
  -- added by shailesh on 3-4-2009 updating the logic of query removeing the DISTINCT
   SELECT DATA.BANK_ACCOUNT_NAME,
        -- DATA. PAYMENT_TYPE_FLAG,
    --     DATA.TYPE,
         DATA.CHECK_DATE,
         DATA.SOURCE,
         DATA.Particulars,
         DATA.Party_Name,
         DATA.Description,
         DATA.Docu_No,
       --  acv.AMOUNT * -1 AMOUNT_AMT,     --Payment should show -ve
        -- DECODE(acv.CHECK_STATUS,'Negotiable',AIPV.AMOUNT*(-1),AIPV.AMOUNT) AMOUNT_AMT,
        --DATA.AMOUNT_AMT ori,
            -- added by shailesh if payment type R type is Negotiable and chck status Voided then amount should come negative
        DECODE(PAYMENT_TYPE_FLAG,'R',DECODE(TYPE,'Negotiable',DECODE(CHECK_STATUS,'Voided',DATA.AMOUNT_AMT*(-1),DATA.AMOUNT_AMT),DATA.AMOUNT_AMT),DATA.AMOUNT_AMT) AMOUNT_AMT,   --Payment should show -ve
      --  DECODE(TYPE,'a',DECODE(chk_sta,'b',amt,dt_amt)
        DATA.Curr_Balance,
         DATA.CHECK_STATUS,
         DATA.Reconciliation_Date,
         DATA.Remarks
         ,DATA.Voucher_number
         ,DATA.org_id
FROM (
SELECT
         acv.BANK_ACCOUNT_NAME,
         acv. PAYMENT_TYPE_FLAG,
         'Negotiable' TYPE,
         acv.CHECK_STATUS,
         TO_CHAR(acv.CHECK_DATE,'DD-MON-YYYY') CHECK_DATE,
         'Payables' SOURCE,
         'Payments' Particulars,
         acv.CURRENT_VENDOR_NAME Party_Name,
         (SELECT SUBSTR(XXABRL_SPC_CHARACTER(Description),1,30) FROM AP_INVOICE_PAYMENTS_V ipv
         WHERE acv.PAYMENT_ID = ipv.PAYMENT_ID(+)
         AND ROWNUM =1) Description,
         TO_CHAR(acv.check_number) Docu_No,
       --  acv.AMOUNT * -1 AMOUNT_AMT,     --Payment should show -ve
        -- DECODE(acv.CHECK_STATUS,'Negotiable',AIPV.AMOUNT*(-1),AIPV.AMOUNT) AMOUNT_AMT,
        --acv.AMOUNT*(-1)  AMOUNT_AMT,   --Payment should show -ve
          -- added by shailesh on 3 rd april if payment type is refund   and check status is voided then  amt should come positive  else amount shouls negative
              DECODE(payment_type_flag,'R',(DECODE(acv.CHECK_STATUS,'Voided',acv.AMOUNT,acv.AMOUNT*(-1))),acv.AMOUNT*(-1))   AMOUNT_AMT,
             0 Curr_Balance,
         (SELECT status FROM   CE_STATEMENT_LINES
         WHERE bank_trx_number(+) =TO_CHAR(acv.check_number)
         AND amount=acv.AMOUNT) Reconciliation_Status,
         TO_CHAR(acv.check_date,'DD-MON-YYYY') Reconciliation_Date,
         '' Remarks
         ,acv.doc_sequence_value Voucher_number
         ,ACV.ORG_ID
    FROM AP_CHECKS_V acv
    WHERE -- acv.BANK_ACCOUNT_NAME =NVL(p_bank_account_no,acv.BANK_ACCOUNT_NAME)
            -- added by the shailesh on 24 apr 09 as bank name "NCR DEUTSCHE BANK-3002" to "DL DEUTSCHE BANK-3002"  changed so data of this bank is not comming
      acv.CURRENT_BANK_ACCOUNT_NAME =NVL(p_bank_account_no,acv.CURRENT_BANK_ACCOUNT_NAME)
   --   AND aipv.check_id=acv.check_id
    --  AND acv.CHECK_STATUS='Negotiable'
      -- AND ACV.CHECK_NUMBER= 35247
    AND acv.CHECK_DATE BETWEEN ADD_MONTHS(TO_DATE(p_period,'MON-YY'),1) AND TO_DATE(p_to_date1,'yyyy/mm/dd hh24:mi:ss')
    -- added by shailesh for void payments data on 3-3-2009 for void payments check status should be Negotiable
                UNION ALL
    -- void payments
                     SELECT
         acv.BANK_ACCOUNT_NAME,
         acv. PAYMENT_TYPE_FLAG,
         'VOIDE' TYPE,
         acv.CHECK_STATUS,
         TO_CHAR(acv.CHECK_DATE,'DD-MON-YYYY') CHECK_DATE,
         'Payables' SOURCE,
         'Payments' Particulars,
         acv.CURRENT_VENDOR_NAME Party_Name,
         (SELECT SUBSTR(XXABRL_SPC_CHARACTER(Description),1,30) FROM AP_INVOICE_PAYMENTS_V ipv
         WHERE acv.PAYMENT_ID = ipv.PAYMENT_ID(+)
         AND ROWNUM =1) Description,
         TO_CHAR(acv.check_number) Docu_No,
       --  acv.AMOUNT * -1 AMOUNT_AMT,     --Payment should show -ve
        AIPV.AMOUNT*-1 AMOUNT_AMT,
        --acv.AMOUNT*(-1)  AMOUNT_AMT,   --Payment should show -ve
         0 Curr_Balance,
         (SELECT status FROM   CE_STATEMENT_LINES
         WHERE bank_trx_number(+) =TO_CHAR(acv.check_number)
         AND amount=AIPV.AMOUNT) Reconciliation_Status,
         TO_CHAR(aipv.ACCOUNTING_DATE,'DD-MON-YYYY') Reconciliation_Date,
         '' Remarks
        ,acv.doc_sequence_value Voucher_number
        ,acv.ORG_ID
    FROM AP_CHECKS_V /*apps.AP_CHECKS_V2*/ acv ,AP_INVOICE_PAYMENTS_v aipv
    WHERE --acv.BANK_ACCOUNT_NAME =NVL(p_bank_account_no,acv.BANK_ACCOUNT_NAME)
      -- added by the shailesh on 24 apr 09 as bank name "NCR DEUTSCHE BANK-3002" to "DL DEUTSCHE BANK-3002"  changed so data of this bank is not comming
      acv.CURRENT_BANK_ACCOUNT_NAME =NVL(p_bank_account_no,acv.CURRENT_BANK_ACCOUNT_NAME)
      AND  aipv.check_id(+)=acv.check_id
      AND acv.CHECK_STATUS='Voided'
    --      FOLLOWING CONDITION ADDED BY SHAILESH ON 17TH APRIL 2009 AS THE VOIDED ENTRIES ARE TAKING FROM CHECK DATE IT SHOULD TAKEN FROM GL_DATE
      -- ORIGINAL COND:-       AND acv.CHECK_DATE BETWEEN ADD_MONTHS(TO_DATE(p_period,'MON-YY'),1) AND TO_DATE(p_to_date1,'yyyy/mm/dd hh24:mi:ss')
              AND aipv.ACCOUNTING_DATE BETWEEN ADD_MONTHS(TO_DATE(p_period,'MON-YY'),1) AND TO_DATE(p_to_date1,'yyyy/mm/dd hh24:mi:ss')
       -- AND acv.check_number IN (10229,46651)
      --AND AIPV.AMOUNT<0
        -- Added by shailesh on 3-4-2009 for void payments. Picking up only first effect (not the reversal entry flag:REVERSAL_INV_PMT_ID)
     AND aipv.REVERSAL_INV_PMT_ID IS NOT  NULL
            -- AND ACV.CHECK_NUMBER= 35247
       ) DATA
      ORDER BY DATA.Docu_No;







  /*

   SELECT DATA.BANK_ACCOUNT_NAME,
        -- DATA. PAYMENT_TYPE_FLAG,
    --     DATA.TYPE,
         DATA.CHECK_DATE,
         DATA.SOURCE,
         DATA.Particulars,
         DATA.Party_Name,
         DATA.Description,
         DATA.Docu_No,
       --  acv.AMOUNT * -1 AMOUNT_AMT,     --Payment should show -ve
        -- DECODE(acv.CHECK_STATUS,'Negotiable',AIPV.AMOUNT*(-1),AIPV.AMOUNT) AMOUNT_AMT,
        --DATA.AMOUNT_AMT ori,
            -- added by shailesh if payment type R type is Negotiable and chck status Voided then amount should come negative
        DECODE(PAYMENT_TYPE_FLAG,'R',DECODE(TYPE,'Negotiable',DECODE(CHECK_STATUS,'Voided',DATA.AMOUNT_AMT*(-1),DATA.AMOUNT_AMT),DATA.AMOUNT_AMT),DATA.AMOUNT_AMT) AMOUNT_AMT,   --Payment should show -ve
      --  DECODE(TYPE,'a',DECODE(chk_sta,'b',amt,dt_amt)
        DATA.Curr_Balance,
         DATA.CHECK_STATUS,
         DATA.Reconciliation_Date,
         DATA.Remarks
FROM (
SELECT
         acv.BANK_ACCOUNT_NAME,
         acv. PAYMENT_TYPE_FLAG,
         'Negotiable' TYPE,
         acv.CHECK_STATUS,
         TO_CHAR(acv.CHECK_DATE,'DD-MON-YYYY') CHECK_DATE,
         'Payables' SOURCE,
         'Payments' Particulars,
         acv.CURRENT_VENDOR_NAME Party_Name,
         (SELECT SUBSTR(Description,1,30) FROM AP_INVOICE_PAYMENTS_V ipv
         WHERE acv.PAYMENT_ID = ipv.PAYMENT_ID(+)
         AND ROWNUM =1) Description,
         TO_CHAR(acv.check_number) Docu_No,
       --  acv.AMOUNT * -1 AMOUNT_AMT,     --Payment should show -ve
        -- DECODE(acv.CHECK_STATUS,'Negotiable',AIPV.AMOUNT*(-1),AIPV.AMOUNT) AMOUNT_AMT,
        --acv.AMOUNT*(-1)  AMOUNT_AMT,   --Payment should show -ve
          -- added by shailesh on 3 rd april if payment type is refund   and check status is voided then  amt should come positive  else amount shouls negative
              DECODE(payment_type_flag,'R',(DECODE(acv.CHECK_STATUS,'Voided',acv.AMOUNT,acv.AMOUNT*(-1))),acv.AMOUNT*(-1))   AMOUNT_AMT,
             0 Curr_Balance,
         (SELECT status FROM   CE_STATEMENT_LINES
         WHERE bank_trx_number(+) =TO_CHAR(acv.check_number)
         AND amount=acv.AMOUNT) Reconciliation_Status,
         TO_CHAR(acv.check_date,'DD-MON-YYYY') Reconciliation_Date,
         '' Remarks
    FROM AP_CHECKS_V acv
    WHERE  acv.BANK_ACCOUNT_NAME =NVL(p_bank_account_no,acv.BANK_ACCOUNT_NAME)
   --   AND aipv.check_id=acv.check_id
    --  AND acv.CHECK_STATUS='Negotiable'
    AND acv.CHECK_DATE BETWEEN ADD_MONTHS(TO_DATE(p_period,'MON-YY'),1) AND TO_DATE(p_to_date1,'yyyy/mm/dd hh24:mi:ss')
    -- added by shailesh for void payments data on 3-3-2009 for void payments check status should be Negotiable
                UNION ALL
    -- void payments
                SELECT
         acv.BANK_ACCOUNT_NAME,
         acv. PAYMENT_TYPE_FLAG,
         'VOIDE' TYPE,
         acv.CHECK_STATUS,
         TO_CHAR(acv.CHECK_DATE,'DD-MON-YYYY') CHECK_DATE,
         'Payables' SOURCE,
         'Payments' Particulars,
         acv.CURRENT_VENDOR_NAME Party_Name,
         (SELECT SUBSTR(Description,1,30) FROM AP_INVOICE_PAYMENTS_V ipv
         WHERE acv.PAYMENT_ID = ipv.PAYMENT_ID(+)
         AND ROWNUM =1) Description,
         TO_CHAR(acv.check_number) Docu_No,
       --  acv.AMOUNT * -1 AMOUNT_AMT,     --Payment should show -ve
        --AIPV.AMOUNT AMOUNT_AMT,
            -- added by shailesh on 23 apr 09 as we want 2nd effect data with effect of gl_date
            acv.AMOUNT * -1 AMOUNT_AMT,
        --acv.AMOUNT*(-1)  AMOUNT_AMT,   --Payment should show -ve
         0 Curr_Balance,
         (SELECT status FROM   CE_STATEMENT_LINES
         WHERE bank_trx_number(+) =TO_CHAR(acv.check_number)
         AND amount=AIPV.AMOUNT) Reconciliation_Status,
         TO_CHAR(aipv.ACCOUNTING_DATE,'DD-MON-YYYY') Reconciliation_Date,
         '' Remarks
    FROM AP_CHECKS_V  acv ,AP_INVOICE_PAYMENTS_v aipv
    WHERE acv.BANK_ACCOUNT_NAME =NVL(p_bank_account_no,acv.BANK_ACCOUNT_NAME)
      AND  aipv.check_id(+)=acv.check_id
      AND acv.CHECK_STATUS='Voided'
    --      FOLLOWING CONDITION ADDED BY SHAILESH ON 17TH APRIL 2009 AS THE VOIDED ENTRIES ARE TAKING FROM CHECK DATE IT SHOULD TAKEN FROM GL_DATE
      -- ORIGINAL COND:-       AND acv.CHECK_DATE BETWEEN ADD_MONTHS(TO_DATE(p_period,'MON-YY'),1) AND TO_DATE(p_to_date1,'yyyy/mm/dd hh24:mi:ss')
              AND aipv.ACCOUNTING_DATE BETWEEN ADD_MONTHS(TO_DATE(p_period,'MON-YY'),1) AND TO_DATE(p_to_date1,'yyyy/mm/dd hh24:mi:ss')
       -- AND acv.check_number IN (10229,46651)
      --AND AIPV.AMOUNT<0
        -- Added by shailesh on 3-4-2009 for void payments. Picking up only first effect (not the reversal entry flag:REVERSAL_INV_PMT_ID)
     --AND aipv.REVERSAL_INV_PMT_ID IS NULL
       -- added by shailesh on 23 apr 09 want reversal payment entry second effect as the data shuld fetch as p[er the gl_date
         AND aipv.REVERSAL_INV_PMT_ID IS NOT NULL
      ) DATA
      ORDER BY DATA.Docu_No;

        */

  /*  --- distinct query logic
  SELECT DISTINCT DATA.BANK_ACCOUNT_NAME,
        -- DATA. PAYMENT_TYPE_FLAG,
    --     DATA.TYPE,
         DATA.CHECK_DATE,
         DATA.SOURCE,
         DATA.Particulars,
         DATA.Party_Name,
         DATA.Description,
         DATA.Docu_No,
       --  acv.AMOUNT * -1 AMOUNT_AMT,     --Payment should show -ve
        -- DECODE(acv.CHECK_STATUS,'Negotiable',AIPV.AMOUNT*(-1),AIPV.AMOUNT) AMOUNT_AMT,
        --DATA.AMOUNT_AMT ori,
            -- added by shailesh if payment type R type is Negotiable and chck status Voided then amount should come negative
        DECODE(PAYMENT_TYPE_FLAG,'R',DECODE(TYPE,'Negotiable',DECODE(CHECK_STATUS,'Voided',DATA.AMOUNT_AMT*(-1),DATA.AMOUNT_AMT),DATA.AMOUNT_AMT),DATA.AMOUNT_AMT) AMOUNT_AMT,   --Payment should show -ve
      --  DECODE(TYPE,'a',DECODE(chk_sta,'b',amt,dt_amt)
        DATA.Curr_Balance,
         DATA.CHECK_STATUS,
         DATA.Reconciliation_Date,
         DATA.Remarks
FROM (
SELECT DISTINCT
         acv.BANK_ACCOUNT_NAME,
         acv. PAYMENT_TYPE_FLAG,
         'Negotiable' TYPE,
         acv.CHECK_STATUS,
         TO_CHAR(acv.CHECK_DATE,'DD-MON-YYYY') CHECK_DATE,
         'Payables' SOURCE,
         'Payments' Particulars,
         acv.CURRENT_VENDOR_NAME Party_Name,
         (SELECT SUBSTR(Description,1,30) FROM AP_INVOICE_PAYMENTS_V ipv
         WHERE acv.PAYMENT_ID = ipv.PAYMENT_ID(+)
         AND ROWNUM =1) Description,
         TO_CHAR(acv.check_number) Docu_No,
       --  acv.AMOUNT * -1 AMOUNT_AMT,     --Payment should show -ve
        -- DECODE(acv.CHECK_STATUS,'Negotiable',AIPV.AMOUNT*(-1),AIPV.AMOUNT) AMOUNT_AMT,
        --acv.AMOUNT*(-1)  AMOUNT_AMT,   --Payment should show -ve
          -- added by shailesh on 3 rd april if payment type is refund   and check status is voided then  amt should come positive  else amount shouls negative
              DECODE(payment_type_flag,'R',(DECODE(acv.CHECK_STATUS,'Voided',acv.AMOUNT,acv.AMOUNT*(-1))),acv.AMOUNT*(-1))   AMOUNT_AMT,
             0 Curr_Balance,
         (SELECT status FROM   CE_STATEMENT_LINES
         WHERE bank_trx_number(+) =TO_CHAR(acv.check_number)
         AND amount=AIPV.AMOUNT) Reconciliation_Status,
         TO_CHAR(acv.check_date,'DD-MON-YYYY') Reconciliation_Date,
         '' Remarks
    FROM AP_CHECKS_V acv ,AP_INVOICE_PAYMENTS_v aipv
    WHERE  acv.BANK_ACCOUNT_NAME =NVL(p_bank_account_no,acv.BANK_ACCOUNT_NAME)
      AND aipv.check_id=acv.check_id
    --  AND acv.check_number IN (10229,46651)
    --  AND acv.CHECK_STATUS='Negotiable'
    AND acv.CHECK_DATE BETWEEN ADD_MONTHS(TO_DATE(p_period,'MON-YY'),1) AND TO_DATE(p_to_date1,'yyyy/mm/dd hh24:mi:ss')
    -- added by shailesh for void payments data on 3-3-2009 for void payments check status should be Negotiable
                UNION ALL
    -- void payments
                SELECT
         acv.BANK_ACCOUNT_NAME,
         acv. PAYMENT_TYPE_FLAG,
         'VOIDE' TYPE,
         acv.CHECK_STATUS,
         TO_CHAR(acv.CHECK_DATE,'DD-MON-YYYY') CHECK_DATE,
         'Payables' SOURCE,
         'Payments' Particulars,
         acv.CURRENT_VENDOR_NAME Party_Name,
         (SELECT SUBSTR(Description,1,30) FROM AP_INVOICE_PAYMENTS_V ipv
         WHERE acv.PAYMENT_ID = ipv.PAYMENT_ID(+)
         AND ROWNUM =1) Description,
         TO_CHAR(acv.check_number) Docu_No,
       --  acv.AMOUNT * -1 AMOUNT_AMT,     --Payment should show -ve
        AIPV.AMOUNT AMOUNT_AMT,
        --acv.AMOUNT*(-1)  AMOUNT_AMT,   --Payment should show -ve
         0 Curr_Balance,
         (SELECT status FROM   CE_STATEMENT_LINES
         WHERE bank_trx_number(+) =TO_CHAR(acv.check_number)
         AND amount=AIPV.AMOUNT) Reconciliation_Status,
         TO_CHAR(acv.check_date,'DD-MON-YYYY') Reconciliation_Date,
         '' Remarks
    FROM AP_CHECKS_V  acv ,AP_INVOICE_PAYMENTS_v aipv
    WHERE acv.BANK_ACCOUNT_NAME =NVL(p_bank_account_no,acv.BANK_ACCOUNT_NAME)
      AND  aipv.check_id(+)=acv.check_id
      AND acv.CHECK_STATUS='Voided'
          AND acv.CHECK_DATE BETWEEN ADD_MONTHS(TO_DATE(p_period,'MON-YY'),1) AND TO_DATE(p_to_date1,'yyyy/mm/dd hh24:mi:ss')
     -- AND acv.check_number IN (10229,46651)
      --AND AIPV.AMOUNT<0
        -- Added by shailesh on 3-4-2009 for void payments. Picking up only first effect (not the reversal entry flag:REVERSAL_INV_PMT_ID)
     AND aipv.REVERSAL_INV_PMT_ID IS NULL
      ) DATA
      ORDER BY DATA.Docu_No;  */
  /* query on 4 mar 2009
  SELECT DISTINCT
         acv.BANK_ACCOUNT_NAME,
         TO_CHAR(acv.CHECK_DATE,'DD-MON-YYYY') CHECK_DATE,
         'Payables' SOURCE,
         'Payments' Particulars,
         acv.CURRENT_VENDOR_NAME Party_Name,
         (SELECT SUBSTR(Description,1,30) FROM AP_INVOICE_PAYMENTS_V ipv
         WHERE acv.PAYMENT_ID = ipv.PAYMENT_ID(+)
         AND ROWNUM =1) Description,
         TO_CHAR(acv.check_number) Docu_No,
       --  acv.AMOUNT * -1 AMOUNT_AMT,     --Payment should show -ve
        -- DECODE(acv.CHECK_STATUS,'Negotiable',AIPV.AMOUNT*(-1),AIPV.AMOUNT) AMOUNT_AMT,
        acv.AMOUNT*(-1)  AMOUNT_AMT,   --Payment should show -ve
         0 Curr_Balance,
         (SELECT status FROM   CE_STATEMENT_LINES
         WHERE bank_trx_number(+) =TO_CHAR(acv.check_number)
         AND amount=AIPV.AMOUNT) Reconciliation_Status,
         TO_CHAR(acv.check_date,'DD-MON-YYYY') Reconciliation_Date,
         '' Remarks
    FROM AP_CHECKS_V  acv ,AP_INVOICE_PAYMENTS_v aipv
    WHERE acv.bank_account_name =NVL(p_bank_account_no,acv.bank_account_name)
      AND aipv.check_id=acv.check_id
    AND acv.CHECK_DATE BETWEEN ADD_MONTHS(TO_DATE(p_period,'MON-YY'),1) AND TO_DATE(p_to_date1,'yyyy/mm/dd hh24:mi:ss')
    -- added by shailesh for void payments data on 3-3-2009 for void payments check status should be Negotiable
    UNION ALL
    -- void payments
    SELECT
         acv.BANK_ACCOUNT_NAME,
         TO_CHAR(acv.CHECK_DATE,'DD-MON-YYYY') CHECK_DATE,
         'Payables' SOURCE,
         'Payments' Particulars,
         acv.CURRENT_VENDOR_NAME Party_Name,
         (SELECT SUBSTR(Description,1,30) FROM AP_INVOICE_PAYMENTS_V ipv
         WHERE acv.PAYMENT_ID = ipv.PAYMENT_ID(+)
         AND ROWNUM =1) Description,
         TO_CHAR(acv.check_number) Docu_No,
       --  acv.AMOUNT * -1 AMOUNT_AMT,     --Payment should show -ve
        AIPV.AMOUNT*(-1) AMOUNT_AMT,
        --acv.AMOUNT*(-1)  AMOUNT_AMT,   --Payment should show -ve
         0 Curr_Balance,
         (SELECT status FROM   CE_STATEMENT_LINES
         WHERE bank_trx_number(+) =TO_CHAR(acv.check_number)
         AND amount=AIPV.AMOUNT) Reconciliation_Status,
         TO_CHAR(acv.check_date,'DD-MON-YYYY') Reconciliation_Date,
         '' Remarks
    FROM AP_CHECKS_V  acv ,AP_INVOICE_PAYMENTS_v aipv
    WHERE acv.bank_account_name =NVL(p_bank_account_no,acv.bank_account_name)
      AND aipv.check_id=acv.check_id
      AND acv.CHECK_STATUS='Voided'
--      AND acv.check_number=11560
      AND AIPV.AMOUNT<0
      AND acv.CHECK_DATE BETWEEN ADD_MONTHS(TO_DATE(p_period,'MON-YY'),1) AND TO_DATE(p_to_date1,'yyyy/mm/dd hh24:mi:ss')
     ORDER BY acv.check_number;
    */

 Print_log('Inserting Payments','Y');
  ------------------
  INSERT INTO XXABRL_BANK_BAL_DATA_GT
  SELECT (SELECT cba.bank_account_name
                  FROM ce_bank_acct_uses_all  ba,
                  ce_bank_accounts   cba,
                  ce_bank_branches_v bb
                  WHERE bb.bank_institution_type = 'BANK'
                  AND bb.branch_party_id = cba.bank_branch_id
                  AND cba.bank_account_id = ba.bank_account_id
                  AND cba.account_classification = 'INTERNAL'
                  AND ba.bank_acct_use_id = REMIT_BANK_ACCT_USE_ID)
         BANK_ACCOUNT_NAME,
         TO_CHAR(RECEIPT_DATE,'DD-MON-YYYY') RECEIPT_DATE,
         'Receivables' SOURCE,
         'Receipt' Particulars,
         (SELECT Hp.Party_name
          FROM   HZ_PARTIES HP
            ,HZ_CUST_ACCOUNTS       HCA
                ,HZ_CUST_ACCT_SITES_ALL HCAS
                ,HZ_CUST_SITE_USES_ALL  HCSUA
          WHERE
      hca.party_id = hp.party_id
      AND hcas.cust_account_id       = hca.cust_account_id
          AND   hcsua.cust_acct_site_id    = hcas.cust_acct_site_id
          AND   hcsua.site_use_code        = 'BILL_TO'
      AND hcsua.site_use_id = CUSTOMER_SITE_USE_ID
      )
          Party_Name,
         SUBSTR(COMMENTS,1,30)    Description,
         TO_CHAR(RECEIPT_NUMBER) Document_No,
         --Amount Amount,
         -- ADDED BY SHAILESH ON 18 FEB 2009 REV STATUS AMOUNT IS NEGATIVE
         Amount AMOUNT,
         0 Cur_Balance,
         State_DSP Reconciliation_Status,
         TO_CHAR(RECEIPT_DATE,'DD-MON-YYYY') Reconciliation_Date,
         '' Remarks
         , document_number Voucher_number
         ,ORG_ID
    FROM /*ar_cash_receipts_all*/ AR_CASH_RECEIPTS_V
    WHERE
    TO_CHAR(GL_DATE,'DD-MON-YYYY') BETWEEN ADD_MONTHS(TO_DATE(p_period,'MON-YY'),1) AND TO_DATE(p_to_date1,'yyyy/mm/dd hh24:mi:ss')
    AND
  (SELECT cba.BANK_ACCOUNT_NAME
                  FROM ce_bank_acct_uses_all  ba,
                  ce_bank_accounts   cba,
                  ce_bank_branches_v bb
                  WHERE bb.bank_institution_type = 'BANK'
                  AND bb.branch_party_id = cba.bank_branch_id
                  AND cba.bank_account_id = ba.bank_account_id
                  AND cba.account_classification = 'INTERNAL'
                  AND ba.bank_acct_use_id = REMIT_BANK_ACCT_USE_ID) = p_bank_account_no
  UNION ALL
  SELECT (SELECT cba.bank_account_name
                  FROM ce_bank_acct_uses_all  ba,
                  ce_bank_accounts   cba,
                  ce_bank_branches_v bb
                  WHERE bb.bank_institution_type = 'BANK'
                  AND bb.branch_party_id = cba.bank_branch_id
                  AND cba.bank_account_id = ba.bank_account_id
                  AND cba.account_classification = 'INTERNAL'
                  AND ba.bank_acct_use_id = REMIT_BANK_ACCT_USE_ID)
         BANK_ACCOUNT_NAME,
         TO_CHAR(RECEIPT_DATE,'DD-MON-YYYY') RECEIPT_DATE,
         'Receivables' SOURCE,
         'Receipt' Particulars,
         (SELECT Hp.Party_name
          FROM   HZ_PARTIES HP
            ,HZ_CUST_ACCOUNTS       HCA
                ,HZ_CUST_ACCT_SITES_ALL HCAS
                ,HZ_CUST_SITE_USES_ALL  HCSUA
          WHERE
      hca.party_id = hp.party_id
      AND hcas.cust_account_id       = hca.cust_account_id
          AND   hcsua.cust_acct_site_id    = hcas.cust_acct_site_id
          AND   hcsua.site_use_code        = 'BILL_TO'
      AND hcsua.site_use_id = CUSTOMER_SITE_USE_ID
      )
          Party_Name,
         SUBSTR(COMMENTS,1,30)    Description,
         TO_CHAR(RECEIPT_NUMBER) Document_No,
         --Amount Amount,
         -- ADDED BY Nijam ON 19 OCT 2011 REV STATUS AMOUNT IS NEGATIVE
         --DECODE(RECEIPT_STATUS,'REV',Amount*(-1),Amount) aMOUNT,
              DECODE(acrh.status,'REVERSED',acr.Amount*(-1),acr.aMOUNT) aMOUNT,
         0 Cur_Balance,
         State_DSP Reconciliation_Status,
         TO_CHAR(RECEIPT_DATE,'DD-MON-YYYY') Reconciliation_Date,
         '' Remarks
              ,  document_number Voucher_number
              ,acr.org_id
    FROM /*ar_cash_receipts_all*/ AR_CASH_RECEIPTS_V acr,ar_cash_receipt_history_all acrh
    WHERE
    TO_CHAR(acrh.GL_DATE,'DD-MON-YYYY') BETWEEN ADD_MONTHS(TO_DATE(p_period,'MON-YY'),1) AND TO_DATE(p_to_date1,'yyyy/mm/dd hh24:mi:ss')
    AND    State_DSP='Reversed'
    and acrh.cash_receipt_id=acr.cash_receipt_id
    and acrh.status='REVERSED'
    and (SELECT cba.BANK_ACCOUNT_NAME
                  FROM ce_bank_acct_uses_all  ba,
                  ce_bank_accounts   cba,
                  ce_bank_branches_v bb
                  WHERE bb.bank_institution_type = 'BANK'
                  AND bb.branch_party_id = cba.bank_branch_id
                  AND cba.bank_account_id = ba.bank_account_id
                  AND cba.account_classification = 'INTERNAL'
                  AND ba.bank_acct_use_id = REMIT_BANK_ACCT_USE_ID) = p_bank_account_no
                  ORDER BY RECEIPT_DATE;

  /* original query on 4 mar 2009
  SELECT (SELECT cba.bank_account_name
                  FROM ce_bank_acct_uses_all  ba,
                  ce_bank_accounts   cba,
                  ce_bank_branches_v bb
                  WHERE bb.bank_institution_type = 'BANK'
                  AND bb.branch_party_id = cba.bank_branch_id
                  AND cba.bank_account_id = ba.bank_account_id
                  AND cba.account_classification = 'INTERNAL'
                  AND ba.bank_acct_use_id = REMIT_BANK_ACCT_USE_ID)
         BANK_ACCOUNT_NAME,
         TO_CHAR(RECEIPT_DATE,'DD-MON-YYYY') RECEIPT_DATE,
         'Receivables' SOURCE,
         'Receipt' Particulars,
         (SELECT Hp.Party_name
          FROM   HZ_PARTIES HP
            ,HZ_CUST_ACCOUNTS       HCA
                ,HZ_CUST_ACCT_SITES_ALL HCAS
                ,HZ_CUST_SITE_USES_ALL  HCSUA
          WHERE
      hca.party_id = hp.party_id
      AND hcas.cust_account_id       = hca.cust_account_id
          AND   hcsua.cust_acct_site_id    = hcas.cust_acct_site_id
          AND   hcsua.site_use_code        = 'BILL_TO'
      AND hcsua.site_use_id = CUSTOMER_SITE_USE_ID
      )
          Party_Name,
         SUBSTR(COMMENTS,1,30)    Description,
         TO_CHAR(RECEIPT_NUMBER) Document_No,
         --Amount Amount,
         -- ADDED BY SHAILESH ON 18 FEB 2009 REV STATUS AMOUNT IS NEGATIVE
         DECODE(RECEIPT_STATUS,'REV',Amount*(-1),Amount) aMOUNT,
         0 Cur_Balance,
         State_DSP Reconciliation_Status,
         TO_CHAR(RECEIPT_DATE,'DD-MON-YYYY') Reconciliation_Date,
         '' Remarks
    FROM --  ar_cash_receipts_all
    AR_CASH_RECEIPTS_V
    WHERE
    TO_CHAR(GL_DATE,'DD-MON-YYYY') BETWEEN ADD_MONTHS(TO_DATE(p_period,'MON-YY'),1) AND TO_DATE(p_to_date1,'yyyy/mm/dd hh24:mi:ss')
    AND
  (SELECT cba.bank_account_name
                  FROM ce_bank_acct_uses_all  ba,
                  ce_bank_accounts   cba,
                  ce_bank_branches_v bb
                  WHERE bb.bank_institution_type = 'BANK'
                  AND bb.branch_party_id = cba.bank_branch_id
                  AND cba.bank_account_id = ba.bank_account_id
                  AND cba.account_classification = 'INTERNAL'
                  AND ba.bank_acct_use_id = REMIT_BANK_ACCT_USE_ID) = p_bank_account_no
                  ORDER BY RECEIPT_DATE;
*/

  END LOOP;



EXCEPTION
WHEN OTHERS THEN
     Print_log('Error: Exception in Data insert into GT. '||SQLERRM,'Y');
END ;

--COMMIT;
---------
---------
BEGIN
 
 -- following query is added by shailesh on 15 dec 09 as the opening balance is not properly displaying in report. prev query is 
 -- returning the 0 rows even if the op bal value is there so added following condition.
 -- original query commented after theis query
 select count(1) INTO v_cnt
 from XXABRL_BANK_BAL_DATA_GT
 where particulars = 'Op.Balance'
 and source = 'GL';

/*
  SELECT COUNT(1) INTO v_cnt
    FROM GL_BALANCES BA, CE_BANK_ACCOUNTS CBA
   WHERE (BA.Code_Combination_Id = CBA.CASH_CLEARING_CCID OR
         BA.Code_Combination_Id = CBA.ASSET_CODE_COMBINATION_ID)
     AND CBA.BANK_ACCOUNT_NAME =
         NVL(p_bank_account_no, CBA.BANK_ACCOUNT_NAME)
     AND BA.Period_Name = NVL(p_period, BA.Period_Name);
*/
     IF v_cnt = 0 THEN
        gc_open_bal_status :='NOT-FOUND';
     ELSE
        gc_open_bal_status :='FOUND';
     END IF;

EXCEPTION
  WHEN NO_DATA_FOUND THEN
   Print_log('Error: No data found in opening balance ','Y');
  gc_open_bal_status :='NOT-FOUND';

    /*insert into XXABRL_BANK_BAL_DATA_GT
      SELECT '' bank_account_name,
             '' rDate,
             'GL' Source,
             'Op.Balance' Particulars,
             '' Party_Name,
             '' Description,
             '' Docu_No,
             0  Amount,
             0  Cu_Balance,
             '' Reconciliation_Status,
             '' Reconciliation_Date,
             '' Remarks
        FROM Dual;*/

END;

Print_log('start of data display ','Y');

--COMMIT;

Display_date(gc_open_bal_status);

Print_log('end of data display ','Y');


END Main;

PROCEDURE Display_date(p_open_bal_status IN VARCHAR2)
AS
v_hdr_str VARCHAR2(3000);
v_line_str VARCHAR2(3000);
v_date VARCHAR2(3000);
v_CURR_BALANCE2 NUMBER(15,2) :=0;
v_debug_out VARCHAR2(1) := 'O';
v_amount NUMBER:=0;
BEGIN

Print_log(',,,ABRL Bank Balance Report',v_debug_out);
Print_log('',v_debug_out);
Print_log('',v_debug_out);

SELECT 'Run Date:,'||TO_CHAR(SYSDATE,'DD-MON-YYYY') INTO v_date FROM dual;
Print_log(v_date,v_debug_out);
Print_log('',v_debug_out);

v_hdr_str := 'BANK_ACCOUNT_NO,DATE,SOURCE,PARTICULARS,PARTY_NAME,DESCRIPTION,DOCUMENT_NO,AMOUNT,CURR_BALANCE,RECON_STATUS,RECON_DATE,REMARKS,VOUCHER_NUMBER,ORG_NAME';

Print_log(v_hdr_str,v_debug_out);

IF p_open_bal_status ='NOT-FOUND' THEN

   Fnd_File.PUT_LINE(Fnd_File.LOG,'open balance status in if (NOT-FOUND )cond. '||p_open_bal_status);
   Print_log(',,GL,Op.Balance,,,,0,0',v_debug_out);

ELSE
    
    Fnd_File.PUT_LINE(Fnd_File.LOG,'open balance status in else cond '||p_open_bal_status);
FOR v_cur_Data IN
             (
                         SELECT REPLACE(tt.BANK_ACCOUNT_NO,',',' ') BANK_ACCOUNT_NO,
                    REPLACE(tt.RDATE,',',' ') RDATE,
                    REPLACE(tt.SOURCE,',',' ') SOURCE,
                    REPLACE(tt.PARTICULARS,',',' ') PARTICULARS,
                    REPLACE(tt.PARTY_NAME,',',' ') PARTY_NAME,
                    REPLACE(REPLACE(tt.RDESCRIPTION,CHR(10),''),',',' ') RDESCRIPTION,
                    REPLACE(tt.DOCUMENT_NO,',',' ') DOCUMENT_NO,
                    REPLACE(tt.AMOUNT,',',' ') AMOUNT,
                    REPLACE(tt.CURR_BALANCE,',',' ') CURR_BALANCE,
                    REPLACE(tt.RECON_STATUS,',',' ') RECON_STATUS,
                    REPLACE(tt.RECON_DATE,',',' ') RECON_DATE,
                    REPLACE(tt.REMARKS,',',' ') REMARKS,
                    REPLACE(TT.VOUCHER_NUMBER,',',' ') VOUCHER_NUMBER,
                    REPLACE(NULL,',',' ') ORG_NAME,
                    DECODE(tt.PARTICULARS,
                           'Op.Balance',
                           1,
                           'Payments',
                           2,
                           'Receipt',
                           3,
                           'Bank_Transfer',
                           4,
                           'Journal',
                           5,
                           9) SrNO,
                    TRUNC(SUM(amount)
                          OVER(ORDER BY 1 ROWS UNBOUNDED PRECEDING)) V_CURR_BALANCE
               FROM XXABRL_BANK_BAL_DATA_GT tt
               WHERE SOURCE = 'GL'
               AND ROWNUM=1
                              )

     LOOP

     v_CURR_BALANCE2 := v_CURR_BALANCE2 + v_cur_Data.AMOUNT;

     Print_log('v_cur_Data.AMOUNT: '|| TO_CHAR(v_CURR_BALANCE2),'Y');

     v_line_str :=
     REPLACE(v_cur_Data.BANK_ACCOUNT_NO,CHR(10),'') ||','||
     REPLACE(v_cur_Data.rDATE,CHR(10),'')           ||','||
     REPLACE(v_cur_Data.SOURCE,CHR(10),'')          ||','||
     REPLACE(v_cur_Data.PARTICULARS,CHR(10),'')     ||','||
     REPLACE(v_cur_Data.PARTY_NAME,CHR(10),'')      ||','||
     REPLACE(v_cur_Data.rDESCRIPTION,CHR(10),'')    ||','||
     REPLACE(v_cur_Data.DOCUMENT_NO,CHR(10),'')     ||','||
     REPLACE(v_cur_Data.AMOUNT,CHR(10),'')          ||','||
     TO_CHAR(v_CURR_BALANCE2)                       ||','||
     REPLACE(v_cur_Data.RECON_STATUS,CHR(10),'')    ||','||
     REPLACE(v_cur_Data.RECON_DATE,CHR(10),'')      ||','||
     REPLACE(v_cur_Data.REMARKS,CHR(10),'')         ||','||
     REPLACE(v_cur_Data.VOUCHER_NUMBER,CHR(10),'')  ||','||
     REPLACE(v_cur_Data.ORG_NAME,CHR(10),'') ;

     Print_log(v_line_str,v_debug_out);

     v_line_str :=NULL;
     END LOOP;
END IF;

    --v_CURR_BALANCE2 :=0;

     FOR v_cur_Data IN
             (

             SELECT REPLACE(tt.BANK_ACCOUNT_NO,',',' ') BANK_ACCOUNT_NO,
                    REPLACE(tt.RDATE,',',' ') RDATE,
                    REPLACE(tt.SOURCE,',',' ') SOURCE,
                    REPLACE(tt.PARTICULARS,',',' ') PARTICULARS,
                    REPLACE(tt.PARTY_NAME,',',' ') PARTY_NAME,
                    REPLACE(REPLACE(tt.RDESCRIPTION,CHR(10),''),',',' ') RDESCRIPTION,
                    REPLACE(tt.DOCUMENT_NO,',',' ') DOCUMENT_NO,
                    REPLACE(tt.AMOUNT,',',' ') AMOUNT,
                    REPLACE(tt.CURR_BALANCE,',',' ') CURR_BALANCE,
                    REPLACE(tt.RECON_STATUS,',',' ') RECON_STATUS,
                    REPLACE(tt.RECON_DATE,',',' ') RECON_DATE,
                    REPLACE(tt.REMARKS,',',' ') REMARKS,
                     REPLACE(TT.VOUCHER_NUMBER,',',' ') VOUCHER_NUMBER,
                     REPLACE((select hr.NAME from apps.hr_operating_units hr where hr.ORGANIZATION_ID=TT.ORG_ID),',',' ') ORG_NAME,
                    DECODE(tt.PARTICULARS,
                           'Op.Balance',
                           1,
                           'Payments',
                           2,
                           'Receipt',
                           3,
                           'Bank_Transfer',
                           4,
                           'Journal',
                           5,
                           9) SrNO,
                    /*TRUNC(SUM(amount)
                          OVER(ORDER BY 1 ROWS UNBOUNDED PRECEDING))*/ 0 V_CURR_BALANCE
               FROM XXABRL_BANK_BAL_DATA_GT tt
               WHERE SOURCE <> 'GL'
               AND NVL(tt.RECON_STATUS,'X') <>'U'
              ORDER BY /*decode(tt.PARTICULARS,
                              'Op.Balance',
                              1,
                              'Payments',
                              2,
                              'Receipt',
                              3,
                              'Bank_Transfer',
                              4,
                              'Journal',
                              5,
                              9),*/
                              TO_DATE(rdate,'DD-MON-YYYY')
                              )

     LOOP

     v_CURR_BALANCE2 := v_CURR_BALANCE2 + v_cur_Data.AMOUNT;
     
     Print_log('v_cur_Data.AMOUNT: '|| TO_CHAR(v_cur_Data.AMOUNT),'Y');

     v_line_str :=
     REPLACE(v_cur_Data.BANK_ACCOUNT_NO,CHR(10),'') ||','||
     REPLACE(v_cur_Data.rDATE,CHR(10),'')           ||','||
     REPLACE(v_cur_Data.SOURCE,CHR(10),'')          ||','||
     REPLACE(v_cur_Data.PARTICULARS,CHR(10),'')     ||','||
     REPLACE(v_cur_Data.PARTY_NAME,CHR(10),'')      ||','||
     REPLACE(v_cur_Data.rDESCRIPTION,CHR(10),'')    ||','||
     REPLACE(v_cur_Data.DOCUMENT_NO,CHR(10),'')     ||','||
     REPLACE(v_cur_Data.AMOUNT,CHR(10),'')          ||','||
     TO_CHAR(v_CURR_BALANCE2)                       ||','||
     REPLACE(v_cur_Data.RECON_STATUS,CHR(10),'')    ||','||
     REPLACE(v_cur_Data.RECON_DATE,CHR(10),'')      ||','||
     REPLACE(v_cur_Data.REMARKS,CHR(10),'')         ||','||
     REPLACE(v_cur_Data.VOUCHER_NUMBER,CHR(10),'')   ||','||
     REPLACE(v_cur_Data.org_NAME,CHR(10),'');

     v_amount := TO_CHAR(v_CURR_BALANCE2)  ;
     Print_log(v_line_str,v_debug_out);

     v_line_str :=NULL;
     END LOOP;
---------------------------

     FOR v_cur_Data IN
             (
               SELECT * FROM
                 (
                 SELECT BANK_ACCOUNT_NO,'' rDATE,'' SOURCE,'Cl.Balance' PARTICULARS,'' PARTY_NAME,'' rDESCRIPTION,'' DOCUMENT_NO,'' AMOUNT,
                 SUM(amount) OVER () V_CURR_BALANCE
                 FROM XXABRL_BANK_BAL_DATA_GT
                 )
               WHERE ROWNUM =1
             )

     LOOP

     v_line_str :=
     REPLACE(v_cur_Data.BANK_ACCOUNT_NO,CHR(0),'') ||','||
     REPLACE(v_cur_Data.rDATE,CHR(0),'')            ||','||
     REPLACE(v_cur_Data.SOURCE,CHR(0),'')          ||','||
     REPLACE(v_cur_Data.PARTICULARS,CHR(0),'')     ||','||
     REPLACE(v_cur_Data.PARTY_NAME,CHR(0),'')      ||','||
     REPLACE(v_cur_Data.rDESCRIPTION,CHR(0),'')     ||','||
     REPLACE(v_cur_Data.DOCUMENT_NO,CHR(0),'')     ||','||
     REPLACE(v_cur_Data.AMOUNT,CHR(0),'')          ||','||
     REPLACE( v_amount,CHR(0),'') ;        -- ||','||
     --REPLACE(v_cur_Data.V_CURR_BALANCE,CHR(0),'');


     Print_log(v_line_str,v_debug_out);

     v_line_str :=NULL;
     END LOOP;
     COMMIT;
Print_log('',v_debug_out);
Print_log('',v_debug_out);
Print_log(',,,*** End of  Report ***',v_debug_out);

END Display_date;

END XXABRL_BANK_BALANCE_PKG; 
/


CREATE OR REPLACE PROCEDURE APPS.XXABRL_NAV_INVOICE_VALIDATE(Errbuf       OUT VARCHAR2
                  ,RetCode      OUT NUMBER) is

/*
  ========================
=========================
=========================
=========================
======
  ||   Filename   : XXABRL_APNAV_PKG_INV_Interface.sql
  ||   Description : Script is used to mold Navision data for AP
  ||
  ||   Version     Date            Author              Modification
  ||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
  ||   1.0.0       25-Aug-2014    Dhiresh More         data validation and move to interface for all OU
  ||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
  ||
  ||
  ========================
=========================
=========================
=========================
=====*/



CURSOR APT_C1 (P_Data_Source             VARCHAR2)
      IS
         SELECT  AII.ORG_ID, AILI.attribute1 DC_LOCATION, AII.gl_date, count(AII.invoice_id) REC_CNT
                    FROM ap_invoices_interface AII , ap_invoice_lines_interface AILI
                   WHERE AII.invoice_id  =  AILI.invoice_id
                    and Trim(Upper(NVL(AII.SOURCE,'')))   =    Trim(Upper(P_Data_Source))
                    and AII.GL_DATE >=   trunc(sysdate)-180
                    and NVL(AII.STATUS,'N') <> 'PROCESSED'
                    group by AII.ORG_ID, AILI.attribute1, AII.gl_date;
      
      
                    
v_user_id               number:=FND_PROFILE.value('USER_ID');
v_resp_id               number:=FND_PROFILE.value('RESP_ID');
v_appl_id               number:=FND_PROFILE.value('RESP_APPL_ID');
V_Batch_Name            Varchar2(50);
V_Record_Count          Number:=0;
v_req_id                number;
v_org_id                Number;
V_Status                Varchar2(1);
V_Data_Source           Varchar2(15):='NAVISION';     


    Begin
            For APT_R1 in APT_C1(Trim(Upper('NAVISION')))
        Loop
                    Exit When APT_C1%notfound;                  
              
                         
                            --Operating Unit name search ---------------------
           Begin
             V_Record_Count :=Null;
             V_Org_ID := Null;
              
             V_Record_Count:=APT_R1.REC_CNT; 
             V_Org_ID := APT_R1.Org_ID;    
             V_Batch_Name:=Null;

              Select  V_Data_Source||'-'||Substr(FFVV.description,1,30)||'-'||TO_CHAR (APT_R1.GL_Date, 'DD-MON-RRRR')
              Into      V_Batch_Name
              From    FND_FLEX_VALUES_VL FFVV,
                      FND_FLEX_VALUE_SETS FFVS
              Where   FFVS.Flex_Value_Set_Name    = 'ABRL_GL_Location'
              And     FFVS.Flex_Value_Set_Id    = FFVV.Flex_Value_Set_Id
              And     FFVV.Flex_Value            = APT_R1.DC_LOCATION;

              EXCEPTION
                    When Others Then
                         V_Batch_Name:=V_Data_Source||'-'||TO_CHAR (APT_R1.GL_Date, 'DD-MON-RRRR');
            End;

               If v_record_count>0 Then
                  fnd_global.apps_initialize (
                        user_id=> v_user_id,
                        resp_id=> v_resp_id,
                        resp_appl_id=> v_appl_id
                     );

                Commit;

                v_req_id :=fnd_request.submit_request (
                              'SQLAP',
                              'APXIIMPT',
                              'Payables Open Interface Import'||V_Data_Source,
                               NULL,
                               FALSE,
                               V_Org_ID,
                               V_Data_Source, -- Parameter Data source
                               NULL, -- Group id
                               V_Batch_Name,
                               NULL,
                               NULL,
                               NULL,
                               'N',
                               'N',
                               'N',
                               'Y',
                               CHR (0)
                        );
                Commit;
                
                fnd_file.put_line(fnd_file.output,'Please see the output of Payables OPEN Invoice Import program request id :'||v_req_id);
                fnd_file.put_line(fnd_file.output,'........................................................................' );

                Loop
                     Begin
                        V_Status:=Null;

                        Select Status_Code
                        Into     V_Status
                        from      Fnd_Concurrent_Requests
                        where  Request_id=v_req_id;

                        If V_Status in ('C','D','E','G','X') then
                               Exit;
                        End If;

                       EXCEPTION
                        When Others Then
                              Exit;

                     End;
    
                End Loop;
    
            End If;



                
         End Loop;       


    End XXABRL_NAV_INVOICE_VALIDATE; 
/


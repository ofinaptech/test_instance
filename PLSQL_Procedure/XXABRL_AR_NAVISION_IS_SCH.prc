CREATE OR REPLACE PROCEDURE APPS.xxabrl_ar_navision_is_sch (
   errbuf    OUT   VARCHAR2,
   retcode   OUT   NUMBER
)
IS
   v_req_id          NUMBER;
   v_req_id1         NUMBER;
   v_file_path_l     VARCHAR2 (1000);
   v_file_path_d     VARCHAR2 (1000);
   lb_wait           BOOLEAN;
   lb_wait1          BOOLEAN;
   le_load1          EXCEPTION;
   le_load           EXCEPTION;
   lc_phase          VARCHAR2 (500);
   lc_status         VARCHAR2 (500);
   lc_dev_phase      VARCHAR2 (500);
   lc_dev_status     VARCHAR2 (500);
   lc_message        VARCHAR2 (2000);
   lc_phase_d        VARCHAR2 (500);
   lc_status_d       VARCHAR2 (500);
   lc_dev_phase_d    VARCHAR2 (500);
   lc_dev_status_d   VARCHAR2 (500);
   lc_message_d      VARCHAR2 (2000);
   lc_error_flag     VARCHAR2 (1)    := 'N';
   lc_error_flag1    VARCHAR2 (1)    := 'N';
   gn_success        NUMBER          := 0;
   gn_error          NUMBER          := 2;

   CURSOR xx_main_l
   IS
      SELECT filename
        FROM dir_list
       WHERE REVERSE (SUBSTR (REVERSE (filename), 1, 13)) LIKE '%_L_%';

   CURSOR xx_main_d
   IS
      SELECT filename
        FROM dir_list
       WHERE REVERSE (SUBSTR (REVERSE (filename), 1, 13)) LIKE '%_D_%';
BEGIN
  get_dir_list( '/ebiz/shared_u01/abrlftp/ftpdir/interface/Navision/INST_Sales');   --  PROD
 --  get_dir_list ('/ebiz/app01/source');

   FOR xx_line_rec IN xx_main_l
   LOOP
      SELECT    --'/ebiz/app01/source/'
    '/ebiz/shared_u01/abrlftp/ftpdir/interface/Navision/INST_Sales/' -- PROD
             || xx_line_rec.filename
        INTO v_file_path_l
        FROM DUAL;

      fnd_file.put_line
         (fnd_file.LOG,
          'XXABRL Navision AR Invoice Inst Sales Line Loader Program starting'
         );
-- +===================================================================+
-- Submitting the : Lines Data Load" Concurrent program
-- +===================================================================+
      v_req_id :=
         fnd_request.submit_request ('XXABRL',
                                     'XXABRL_AR_INVOICE_INSA_LINE',
                                     '',
                                     '',
                                     FALSE,
                                     v_file_path_l
                                    );
      COMMIT;

      IF (v_req_id = 0)
      THEN
         RAISE le_load;
      ELSE
         fnd_file.put_line (fnd_file.LOG,
                               'Submitted line Program with request id :'
                            || TO_CHAR (v_req_id)
                           );
      END IF;

      -- +===================================================================+
-- Waiting for the conccurrent request to get completed
-- +===================================================================+
      lb_wait :=
         fnd_concurrent.wait_for_request (request_id      => v_req_id,
                                          INTERVAL        => 10,
                                          max_wait        => 0,
                                          phase           => lc_phase,
                                          status          => lc_status,
                                          dev_phase       => lc_dev_phase,
                                          dev_status      => lc_dev_status,
                                          MESSAGE         => lc_message
                                         );
      -- Do not remove this commit, concurrent program will not start without this
      COMMIT;

      -- +===================================================================+
-- Checking  line Program status
-- +===================================================================+
      IF    (    (   UPPER (lc_dev_status) = 'WARNING'
                  OR UPPER (lc_dev_status) = 'ERROR'
                 )
             AND UPPER (lc_dev_phase) = 'COMPLETE'
            )
         OR (    (UPPER (lc_status) = 'WARNING' OR UPPER (lc_status) = 'ERROR'
                 )
             AND UPPER (lc_phase) = 'COMPLETED'
            )
      THEN
         lc_error_flag := 'Y';
      END IF;

      -- +===================================================================+
-- Checking the status of line program of concurrent Program
-- +===================================================================+
      IF lc_error_flag = 'Y'
      THEN
         RAISE le_load;
      END IF;
   END LOOP;

    get_dir_list( '/ebiz/shared_u01/abrlftp/ftpdir/interface/Navision/INST_Sales');   --  PROD
  -- get_dir_list ('/ebiz/app01/source');

   FOR xx_dist_rec IN xx_main_d
   LOOP
      SELECT   -- '/ebiz/app01/source/'
   '/ebiz/shared_u01/abrlftp/ftpdir/interface/Navision/INST_Sales/'  -- PROD
             || xx_dist_rec.filename
        INTO v_file_path_d
        FROM DUAL;

      fnd_file.put_line
         (fnd_file.LOG,
          'XXABRL Navision AR Invoice Inst Sales distribution Loader Program starting'
         );
-- +===================================================================+
-- Submitting the : dist Data Load" Concurrent program
-- +===================================================================+
      v_req_id1 :=
         fnd_request.submit_request ('XXABRL',
                                     'XXABRL_AR_INVOICE_INSA_DIST',
                                     '',
                                     '',
                                     FALSE,
                                     v_file_path_d
                                    );
      COMMIT;

      IF (v_req_id1 = 0)
      THEN
         RAISE le_load1;
      ELSE
         fnd_file.put_line
                      (fnd_file.LOG,
                          'Submitted  distribution Program with request id :'
                       || TO_CHAR (v_req_id1)
                      );
      END IF;

      -- +===================================================================+
-- Waiting for the conccurrent request to get completed
-- +===================================================================+
      lb_wait1 :=
         fnd_concurrent.wait_for_request (request_id      => v_req_id1,
                                          INTERVAL        => 10,
                                          max_wait        => 0,
                                          phase           => lc_phase_d,
                                          status          => lc_status_d,
                                          dev_phase       => lc_dev_phase_d,
                                          dev_status      => lc_dev_status_d,
                                          MESSAGE         => lc_message_d
                                         );
      -- Do not remove this commit, concurrent program will not start without this
      COMMIT;

  -- +===================================================================+
-- Checking  dist Program status
-- +===================================================================+
      IF    (    (   UPPER (lc_dev_status_d) = 'WARNING'
                  OR UPPER (lc_dev_status_d) = 'ERROR'
                 )
             AND UPPER (lc_dev_phase_d) = 'COMPLETE'
            )
         OR (    (   UPPER (lc_status_d) = 'WARNING'
                  OR UPPER (lc_status_d) = 'ERROR'
                 )
             AND UPPER (lc_phase_d) = 'COMPLETED'
            )
      THEN
         lc_error_flag1 := 'Y';
      END IF;

      IF lc_error_flag1 = 'Y'
      THEN
         RAISE le_load1;
      END IF;
   END LOOP;
/*    ELSE
      retcode := gn_success;
      errbuf :=
            ' processing successful(line). Please check Log file for request ID '
         || v_req_id;
      fnd_file.put_line (fnd_file.LOG, v_file_path_l || v_req_id);
      UTL_FILE.fcopy (src_location       => 'INST_SALES',
                      src_filename       => v_file_path_l,
                      dest_location      => 'INST_SALES_ARCHIVED',
                      dest_filename      => v_file_path_l,
                      start_line         => 1,
                      end_line           => NULL
                     );
      UTL_FILE.fremove ('/usr/tmp', v_file_path_l);
   END IF;

     -- +===================================================================+
-- Checking the status of dist program of concurrent Program
-- +===================================================================+
   IF lc_error_flag1 = 'Y'
   THEN
      RAISE le_load1;
   ELSE
      retcode := gn_success;
      errbuf :=
            ' processing successful(dist). Please check Log file for request ID '
         || v_req_id;
      fnd_file.put_line (fnd_file.LOG, v_file_path_l || v_req_id);
      UTL_FILE.fcopy (src_location       => 'INST_SALES',
                      src_filename       => v_file_path_d,
                      dest_location      => 'INST_SALES_ARCHIVED',
                      dest_filename      => v_file_path_d,
                      start_line         => 1,
                      end_line           => NULL
                     );
      UTL_FILE.fremove ('/usr/tmp', v_file_path_d);
   END IF;*/
EXCEPTION
   WHEN le_load
   THEN
      retcode := gn_error;
      errbuf := 'line Program completed with error';
   WHEN le_load1
   THEN
      retcode := gn_error;
      errbuf := 'dist Program completed with error';
   WHEN OTHERS
   THEN
      retcode := gn_error;
      errbuf := SUBSTR (SQLERRM, 1, 200);
END; 
/


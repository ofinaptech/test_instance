CREATE OR REPLACE PACKAGE APPS.JAI_GSTR2A_PKG AUTHID CURRENT_USER AS
 /* $Header: JAI_GSTR2A_PKG_PKS.pls 120.0.12010000.4 2017/11/22 07:23:01 adasi noship $ */
   P_CONC_REQUEST_ID   NUMBER;


   --
   -----------------------------------------
   --Public Methods Declarations
   -----------------------------------------

   PROCEDURE append_errbuf (p_msg IN VARCHAR2);

   /*
FUNCTION get_tax_invoice_value (p_trx_id           NUMBER,
                                   p_det_factor_id    NUMBER DEFAULT NULL,
                                   p_item_id          NUMBER DEFAULT NULL)
      RETURN NUMBER;
*/


   FUNCTION get_tax_invoice_value (p_trx_id           NUMBER,
                                   p_det_factor_id    NUMBER DEFAULT NULL,
                                   p_item_id          NUMBER DEFAULT NULL,
                                   p_entity_code      VARCHAR2 DEFAULT NULL,
                                   p_trx_type         VARCHAR2 DEFAULT NULL)
      RETURN NUMBER;


   /*
      FUNCTION get_taxable_amt (p_trx_id           NUMBER,
                                p_det_factor_id    NUMBER DEFAULT NULL,
                                p_item_id          NUMBER DEFAULT NULL)
         RETURN NUMBER;
   */


   FUNCTION get_taxable_amt (p_trx_id           NUMBER,
                             p_det_factor_id    NUMBER DEFAULT NULL,
                             p_item_id          NUMBER DEFAULT NULL,
                             p_entity_code      VARCHAR2 DEFAULT NULL,
                             p_trx_type         VARCHAR2 DEFAULT NULL)
      RETURN NUMBER;

   --  PROCEDURE populate_gstin_file_data;

   -- retry the match process for the trx_id or tax_inv_number
   PROCEDURE retry_matching (--p_record_type     VARCHAR2 DEFAULT 'GSTIN',
                             p_tax_inv_num VARCHAR2, p_tax_inv_date DATE);

   PROCEDURE populate_gstr2a_ebs_data (p_trx_id IN NUMBER);

   --   (P_FIRST_PTY_REG_NUM   IN VARCHAR2 DEFAULT NULL,
   --                            P_PERIOD_NAME         IN VARCHAR2);

   PROCEDURE extract_gstr2a_ebs_data (
      P_FIRST_PTY_REG_NUM   IN VARCHAR2 DEFAULT NULL,
      P_PERIOD_NAME         IN VARCHAR2);

   --updating the status

   PROCEDURE update_status (p_fpty_reg_num     VARCHAR2 DEFAULT NULL,
                            p_tprty_reg_num    VARCHAR2 DEFAULT NULL,
                            p_tax_inv_num      VARCHAR2 DEFAULT NULL,
                            p_tax_inv_date     DATE DEFAULT NULL,
                            P_status           VARCHAR2,
                            p_response_code    VARCHAR2,
                            p_response_desc    VARCHAR2,
                            -- p_trx_id            NUMBER DEFAULT NULL,
                            --                            p_tax_line_id      NUMBER DEFAULT NULL         --,
                            p_rate             NUMBER DEFAULT NULL,
                            p_tax_type         VARCHAR2 DEFAULT NULL);

   --1st Party and 3rd Party validation
   PROCEDURE Parties_Valdation;

   --trx details validation
   PROCEDURE tax_inv_dtls_validate;

   PROCEDURE un_match_ebs_records (
      P_FIRST_PTY_REG_NUM   IN VARCHAR2 DEFAULT NULL,
      P_PERIOD_NAME         IN VARCHAR2);

   PROCEDURE match_records (P_FIRST_PTY_REG_NUM   IN VARCHAR2 DEFAULT NULL,
                            P_PERIOD_NAME         IN VARCHAR2);

   PROCEDURE match_process_main (
      errbuf                   OUT NOCOPY VARCHAR2,
      retcode                  OUT NOCOPY NUMBER,
      P_FIRST_PTY_REG_NUM   IN     VARCHAR2 DEFAULT NULL,
      P_PERIOD_NAME         IN     VARCHAR2,
	  p_file_type			IN     VARCHAR2 DEFAULT 'CSV');

   /* -- Commented By Malleswara Rao on 20-Nov-2017

   PROCEDURE process_data (errbuf       OUT NOCOPY VARCHAR2,
                           retcode      OUT NOCOPY VARCHAR2);


   PROCEDURE from_xml_to_relational (p_xml     IN            XMLTYPE,
                                     errbuf       OUT NOCOPY VARCHAR2,
                                     retcode      OUT NOCOPY VARCHAR2);

   */ -- Commented By Malleswara Rao on 20-Nov-2017
END JAI_GSTR2A_PKG;
/


CREATE OR REPLACE Package Body APPS.XXABRL_AP_PKG_INV_Interface IS

PROCEDURE INVOICE_VALIDATE(Errbuf       OUT VARCHAR2
                  ,RetCode      OUT NUMBER
                  ,P_Org_Id        In  NUMBER
                  ,P_Data_Source      IN  VARCHAR2) is


    CURSOR APT_C1(CP_OPERATING_UNIT            VARCHAR2
              ,CP_Data_Source             VARCHAR2)
      IS    Select ROWID,
            OPERATING_UNIT            ,INVOICE_NUM
            ,SOURCE                ,INVOICE_TYPE_LOOKUP_CODE
            ,INVOICE_DATE            ,VENDOR_NUMBER
             ,VENDOR_DC_CODE            ,PAYMENT_METHOD_LOOKUP_CODE
            ,TERMS_NAME                ,INVOICE_AMOUNT
                ,INVOICE_CURRENCY_CODE        ,EXCHANGE_RATE
                ,EXCHANGE_DATE            ,EXCHANGE_RATE_TYPE
            ,DESCRIPTION            ,GL_DATE
            ,GRN_NUMBER                ,DC_LOCATION
        From    XXABRL_AP_INVOICES_INT
        Where Trim(Upper(NVL(SOURCE,CP_Data_Source)))             =    Trim(Upper(CP_Data_Source))
        And    Trim(Upper(NVL(OPERATING_UNIT,CP_OPERATING_UNIT)))    =    Trim(Upper(CP_OPERATING_UNIT))
        And    NVL(INTERFACED_FLAG,'N')     in    ('N','E');

   CURSOR APT_C2(CP_INVOICE_NUM             VARCHAR2
              ,CP_VENDOR_NUMBER             VARCHAR2
              ,CP_VENDOR_DC_CODE         VARCHAR2
            ,CP_OPERATING_UNIT            VARCHAR2
            ,CP_Data_Source             VARCHAR2
            ,CP_GRN_NUMBER             VARCHAR2)
    IS    Select ROWID,
            INVOICE_NUM                ,VENDOR_NUMBER
            ,VENDOR_DC_CODE            ,LINE_NUMBER
            ,AMOUNT
            ,ACCOUNTING_DATE            ,CC
            ,CO                    ,STATE_SBU
            ,LOCATION                ,MERCHANDISE
            ,ACCOUNT                ,INTERCOMPANY
            ,FUTURE
            ,DESCRIPTION            ,TDS_TAX_NAME
        From    XXABRL_AP_INVOICE_LINES_INT
        Where    Trim(Upper(SOURCE))        =    Trim(Upper(CP_Data_Source))
        And    Trim(Upper(OPERATING_UNIT))    =    Trim(Upper(CP_OPERATING_UNIT))
        And    NVL(INTERFACED_FLAG,'N')     <>    'Y'
        And    INVOICE_NUM                =    CP_INVOICE_NUM
        And    VENDOR_NUMBER            =    CP_VENDOR_NUMBER
        And    VENDOR_DC_CODE            =    CP_VENDOR_DC_CODE
        And    NVL(Reference_1,CP_GRN_NUMBER)=    CP_GRN_NUMBER
        Order by Line_Number;


    CURSOR APT_C3(CP_OPERATING_UNIT            VARCHAR2
            ,CP_Data_Source             VARCHAR2
            )
    IS     Select To_Char(Creation_Date,'DD-MON-YYYY') Creation_Date
             ,count(Invoice_Num) Rec_Count
        from      XXABRL_AP_INVOICES_INT
        Where  Trim(Upper(Operating_Unit))    =    Trim(Upper(CP_OPERATING_UNIT))
        And     Trim(Upper(SOURCE))        =    Trim(Upper(CP_Data_Source))
        And    Interfaced_Flag='E'
        Group by To_Char(Creation_Date,'DD-MON-YYYY');

    v_org_id               number;
    v_set_of_books_id         number:=FND_PROFILE.value('GL_SET_OF_BKS_ID');
    v_user_id               number:=FND_PROFILE.value('USER_ID');
    v_resp_id               number:=FND_PROFILE.value('RESP_ID');
    v_appl_id               number:=FND_PROFILE.value('RESP_APPL_ID');
    V_OPERATING_UNIT        HR_Operating_Units.Short_Code%type;
    v_error_hmsg             varchar2(4000);
    v_error_lmsg             varchar2(4000);
    v_error_almsg        varchar2(4000);
    V_Fun_Curr            varchar2(15);
    v_currency                varchar2(15);
    v_term_name               ap_terms_tl.name%type;
     v_tax_name                varchar2(50);
    v_source_lookup           varchar2(50);
    v_pay_lookup_code         varchar2(50);
      v_Inv_Line_Amt        Number:=0;
    V_State_Sbu            gl_code_combinations.Segment3%type;
     v_vendor_id                 po_vendors.vendor_id%type;
    v_vendor_name             po_vendors.vendor_name%type;
     v_vendor_site_name        po_vendor_sites_all.vendor_site_code%type;
    v_vendor_site_id          po_vendor_sites_all.vendor_site_id%type;
    v_record_count             number:=0;
    v_line_count             number:=0;
    v_Data_Count             number;
    v_Code_Combination_id    number;
    v_Error_Line_Count    number:=0;
    v_inv_type            varchar2(30);
    v_Error_Rec_count         number:=0;
    v_OK_Rec_count             number:=0;
    V_Seg_Status        Varchar2(100);
    V_Pay_Site_Flag        Varchar2(1);

Begin

    Begin
                Select     Short_code
                Into        V_OPERATING_UNIT
                From       HR_Operating_Units
                Where      Organization_Id =P_Org_Id;
    EXCEPTION
                When no_data_found then
                      fnd_file.put_line(fnd_file.output,'Selected Org Id does not exist in Oracle Financials' );
                    fnd_file.put_line(fnd_file.output,'........................................................................' );

                  When too_many_rows then
                        fnd_file.put_line(fnd_file.output,'Multiple Org Id exist in Oracle Financials' );
                    fnd_file.put_line(fnd_file.output,'........................................................................' );


                     When Others THEN
                    fnd_file.put_line(fnd_file.output,'Invalid Org Id Selected ' );
                    fnd_file.put_line(fnd_file.output,'........................................................................' );

    End;





     Update     XXABRL_AP_INVOICES_INT
       Set OPERATING_UNIT='AP SM TSRL'
       WHERE GL_DATE>'31-MAR-2012'
       AND  OPERATING_UNIT='TSRL AP SM'
       AND  NVL(INTERFACED_FLAG,'N') = 'N' ;

     Update     XXABRL_AP_INVOICE_LINES_INT
       Set OPERATING_UNIT='AP SM TSRL'
       WHERE ACCOUNTING_DATE>'31-MAR-2012'
       AND  OPERATING_UNIT='TSRL AP SM'
       AND  NVL(INTERFACED_FLAG,'N') = 'N' ;


       Update     XXABRL_AP_INVOICES_INT
       Set OPERATING_UNIT='KA SM TSRL'
       WHERE GL_DATE>'31-MAR-2012'
       AND  OPERATING_UNIT='TSRL KA SM'
       AND  NVL(INTERFACED_FLAG,'N') = 'N' ;


          Update XXABRL_AP_INVOICE_LINES_INT
        Set OPERATING_UNIT='KA SM TSRL'
       WHERE ACCOUNTING_DATE>'31-MAR-2012'
       AND  OPERATING_UNIT='TSRL KA SM'
       AND  NVL(INTERFACED_FLAG,'N') = 'N' ;



       Update     XXABRL_AP_INVOICES_INT
       Set OPERATING_UNIT='KA RDC TSR'
       WHERE GL_DATE>'31-MAR-2012'
       AND  OPERATING_UNIT='TSRLSRDC'
       AND  NVL(INTERFACED_FLAG,'N') = 'N' ;


           Update XXABRL_AP_INVOICE_LINES_INT
       Set OPERATING_UNIT='KA RDC TSR'
       WHERE ACCOUNTING_DATE>'31-MAR-2012'
       AND  OPERATING_UNIT='TSRLSRDC'
       AND  NVL(INTERFACED_FLAG,'N') = 'N' ;



       Update     XXABRL_AP_INVOICES_INT
       Set OPERATING_UNIT='KA HM TSRL'
       WHERE GL_DATE>'31-MAR-2012'
       AND  OPERATING_UNIT='TSRL KA HM'
       AND  NVL(INTERFACED_FLAG,'N') = 'N' ;

        Update XXABRL_AP_INVOICE_LINES_INT
       Set OPERATING_UNIT='KA HM TSRL'
       WHERE ACCOUNTING_DATE>'31-MAR-2012'
       AND  OPERATING_UNIT='TSRL KA HM'
       AND  NVL(INTERFACED_FLAG,'N') = 'N' ;

       COMMIT;






    Update     XXABRL_AP_INVOICES_INT
    Set         CREATED_BY                 =     v_user_id
    Where     Trim(Upper(SOURCE))        =    Trim(Upper(P_Data_Source))
    And        Trim(Upper(OPERATING_UNIT))    =    Trim(Upper(V_OPERATING_UNIT))
    And        NVL(INTERFACED_FLAG,'N')     =    'N'
    And         CREATED_BY is Null;

    Update     XXABRL_AP_INVOICE_LINES_INT
    Set         CREATED_BY                 =     v_user_id
    Where     Trim(Upper(SOURCE))        =    Trim(Upper(P_Data_Source))
    And        Trim(Upper(OPERATING_UNIT))    =    Trim(Upper(V_OPERATING_UNIT))
    And        NVL(INTERFACED_FLAG,'N')     =    'N'
    And         CREATED_BY is Null;

    Commit;

    --Deleting previous error message if any--

    UPDATE     XXABRL_AP_INVOICES_INT
    SET         ERROR_MESSAGE= Null
    Where       Trim(Upper(SOURCE))        =    Trim(Upper(P_Data_Source))
    And        Trim(Upper(OPERATING_UNIT))    =    Trim(Upper(V_OPERATING_UNIT))
    And        NVL(INTERFACED_FLAG,'N')     in    ('N','E')
    And         ERROR_MESSAGE             is     not null;

    UPDATE     XXABRL_AP_INVOICE_LINES_INT
    SET         ERROR_MESSAGE= Null
    Where        Trim(Upper(SOURCE))        =    Trim(Upper(P_Data_Source))
    And        Trim(Upper(OPERATING_UNIT))    =    Trim(Upper(V_OPERATING_UNIT))
    And        NVL(INTERFACED_FLAG,'N')     in    ('N','E')
    And         ERROR_MESSAGE             is     not null;

    Commit;

    --Get Functional Currency

    Begin
         Select Currency_code
         Into   V_Fun_Curr
         from   GL_SETS_OF_BOOKS
         where  set_of_books_id=v_set_of_books_id;
    Exception
        When Others then
        V_Fun_Curr:=Null;
    End;

    fnd_file.put_line(fnd_file.output,'Validating Following Invoices' );
    fnd_file.put_line(fnd_file.output,'........................................................................' );

    For APT_R1 in APT_C1(Trim(Upper(V_OPERATING_UNIT))
                   ,Trim(Upper(P_Data_Source)))
    Loop
        Exit When APT_C1%notfound;
          v_record_count        :=    APT_C1%ROWCOUNT;
        v_error_hmsg        :=    null;
        v_error_lmsg        :=    null;
        v_vendor_id            :=    null;
        v_vendor_name        :=    null;
        v_vendor_site_name     :=    null;
        v_vendor_site_id      :=    null;
        v_org_id            :=    null;
        v_Inv_Line_Amt        :=    0;


        --Validation for Invoice Number

        If APT_R1.invoice_num is Null Then
            v_error_hmsg:='Invoice Number is Null-->>';
        End if;

        --Validation for Invoice Date
         If APT_R1.invoice_date is Null Then
            v_error_hmsg:=v_error_hmsg||'Invoice Date is Null-->>';
         End if;

        -- Validating for Source

        If APT_R1.source is null then
            v_error_hmsg:=v_error_hmsg||'Source is null-->';
        Else
            Begin
                SELECT lookup_code
                Into      v_source_lookup
                FROM      ap_lookup_codes
                WHERE  lookup_type = 'SOURCE'
                AND      upper(lookup_code)=Trim(upper(APT_R1.source))
                And      Nvl(Enabled_flag,'N')='Y'
                And     Sysdate Between nvl(Start_date_Active,sysdate) and nvl(Inactive_date,sysdate);
            EXCEPTION
                When no_data_found then
                    v_error_hmsg:= v_error_hmsg ||APT_R1.source||' Source does not exist in Oracle Financials-->>';
                When too_many_rows then
                    v_term_name:=null;
                    v_error_hmsg:=v_error_hmsg||APT_R1.source||' Multiple Source found-->>';
                     When Others THEN
                    v_error_hmsg:=v_error_hmsg||APT_R1.source||' Wrong Source Code-->>';
            END;
        End If;

        -- Validating for DC Location

        If APT_R1.DC_LOCATION is null Then
            v_error_hmsg:=v_error_hmsg||'DC Location is null-->';
        Else

             V_Seg_Status:=Null;

             V_Seg_Status:=ACCOUNT_SEG_STATUS(APT_R1.DC_LOCATION,'ABRL_GL_Location');

             If V_Seg_Status is Not Null then
                 v_error_lmsg:=v_error_lmsg||'Invalid DC Location '||APT_R1.DC_LOCATION||'-->>';
             End If;


        End If;

        -- Validating Operating Units

        If APT_R1.OPERATING_UNIT is Null Then
            v_error_hmsg:=v_error_hmsg||'Operating Unit is Null-->>';
        /*Else
            Begin
                Select     Organization_Id
                Into        v_org_id
                From       HR_Operating_Units
                Where      Trim(Upper(Short_code))    =    Trim(Upper(APT_R1.OPERATING_UNIT));

            EXCEPTION
                when no_data_found then
                        v_error_hmsg:=v_error_hmsg||'Operating Unit does not exists-->>';
                when too_many_rows then
                        v_error_hmsg:=v_error_hmsg||'Multiple Operating Unit found-->>';
                      When Others Then
                         v_error_hmsg:=v_error_hmsg||'Operating Unit does not exists-->>';

            End;
        */
        End If;

    If APT_R1.INVOICE_TYPE_LOOKUP_CODE is Null then
        v_error_hmsg:=v_error_hmsg||'Invoice Type is Null-->>';
    Else
        Begin
             Select     lookup_code
                  Into    v_inv_type
                  From     ap_lookup_codes
                 Where     UPPER(lookup_code) = UPPER(APT_R1.INVOICE_TYPE_LOOKUP_CODE)
           And    lookup_type = 'INVOICE TYPE';

        Exception
        When no_data_found THEN
            v_error_hmsg:=v_error_hmsg||APT_R1.INVOICE_TYPE_LOOKUP_CODE||' Invoice Type does not exists-->>';
        when too_many_rows then
            v_error_hmsg:=v_error_hmsg||APT_R1.INVOICE_TYPE_LOOKUP_CODE||' Multiple Invoice Type found-->>';
        When others THEN
            v_error_hmsg:=v_error_hmsg||APT_R1.INVOICE_TYPE_LOOKUP_CODE||' Invalid Invoice Type-->>';
             End;

    End If;
        --Validation for Supplier Number

        If APT_R1.vendor_number is Null Then
            v_error_hmsg:=v_error_hmsg||'Vendor Number is Null-->>';

        End If;

        --Validation for Supplier Site
        If APT_R1.vendor_DC_Code is Null Then
              v_error_hmsg:=v_error_hmsg||'Vendor DC Code is Null-->>';
        End if;

        -- Validation for Vendor Information
        If APT_R1.vendor_number is Not Null and APT_R1.vendor_DC_Code is Not Null Then

            Begin
                SELECT     PV.Vendor_Id,            PV.Vendor_Name,
                        PVS.vendor_site_code,        PVS.vendor_site_id
                Into        v_vendor_id                ,v_vendor_name
                        ,v_vendor_site_name        ,v_vendor_site_id
                FROM       po_vendor_sites_all PVS,
                           po_vendors pv,
                           XXABRL_AP_VENDOR_MAP_INT XAVM
                WHERE      Trim(Upper(XAVM.DATA_SOURCE))        =    Trim(Upper(APT_R1.Source))
                AND        Trim(Upper(XAVM.ER_DC_CODE))        =     Trim(Upper(APT_R1.vendor_DC_Code))
                AND           XAVM.ER_VENDOR_NUMBER            =     APT_R1.vendor_number
                AND           XAVM.OF_VENDOR_NUMBER            =    PV.Segment1
                AND           Trim(Upper(XAVM.OF_VENDOR_SITE_CODE))=    Trim(Upper(PVS.Vendor_site_code))
                AND           PV.Vendor_id                =    PVS.Vendor_id
                AND        XAVM.OF_ORGANIZATION_ID         =    PVS.Org_id
                AND        PVS.Org_id                    =    P_Org_Id;


            EXCEPTION
                when no_data_found then
                    v_error_hmsg:=v_error_hmsg||APT_R1.vendor_DC_Code||'-'||APT_R1.vendor_number||'- Vendor Number mapping does not exists-->>';
                    v_vendor_id            :=    null;
                    v_vendor_name        :=    null;
                    v_vendor_site_name     :=    null;
                    v_vendor_site_id      :=    null;
                when too_many_rows then
                    v_error_hmsg:=v_error_hmsg||APT_R1.vendor_DC_Code||'-'||APT_R1.vendor_number||'- Multiple Vendor Number mapping found-->>';
                    v_vendor_id            :=    null;
                    v_vendor_name        :=    null;
                    v_vendor_site_name     :=    null;
                    v_vendor_site_id      :=    null;

                      WHEN OTHERS THEN
                     v_error_hmsg:=v_error_hmsg||APT_R1.vendor_DC_Code||'-'||APT_R1.vendor_number||'- Vendor Number mapping does not exists-->>';
                         v_vendor_id            :=    null;
                    v_vendor_name        :=    null;
                    v_vendor_site_name     :=    null;
                    v_vendor_site_id      :=    null;

            End;
        End If;

         If     v_vendor_id is not Null
         And      v_vendor_site_id is not Null   Then

            Begin
                V_Data_Count:=0;

                Select Count(pv.Vendor_ID)
                Into     V_Data_Count
                From   po_vendors pv,
                         po_vendor_sites_all pvs
                Where  pv.Vendor_Id                =     v_vendor_id
                And    pv.Vendor_Id                =    pvs.Vendor_Id
                And    pvs.Vendor_Site_Id            =    v_vendor_site_id
                And     Sysdate Between nvl(pv.Start_date_Active,sysdate) and nvl(pv.End_Date_Active,sysdate)
                And     NVL(pv.Enabled_Flag,'Y')        =    'Y'
                And     NVL(pvs.Inactive_Date,Sysdate)    >=    Sysdate
                And     pvs.Org_id                    =    P_Org_Id;

                If V_Data_Count=0 Then
                      v_error_hmsg:=v_error_hmsg||V_vendor_name||' Vendor Or '||v_vendor_site_name||' Vendor Site is inactive -->>';
                End If;

                V_Data_Count:=0;

                EXCEPTION
                   WHEN OTHERS then
                        V_Data_Count:=0;
                      v_error_hmsg:=v_error_hmsg||V_vendor_name||' Vendor Or '||v_vendor_site_name||' Vendor Site is inactive -->>';
            End;
         End If;

        -- Validation for Invoice Number duplication

         If       APT_R1.invoice_num is Not Null
         And    v_vendor_id is not Null
         And      v_vendor_site_id is not Null
         Then
            Begin
                V_Data_Count:=0;

                Select count(Invoice_Num)
                Into   V_Data_Count
                From   XXABRL_AP_INVOICES_INT
                Where  Trim(Upper(SOURCE))         =    Trim(Upper(APT_R1.Source))
                And     Trim(Upper(OPERATING_UNIT))    =    Trim(Upper(APT_R1.OPERATING_UNIT))
                And      VENDOR_NUMBER            =    APT_R1.VENDOR_NUMBER
                 And     VENDOR_DC_CODE            =    APT_R1.VENDOR_DC_CODE
                And    Invoice_Num            =    APT_R1.invoice_num;


                If V_Data_Count>1 Then
                      v_error_hmsg:=v_error_hmsg||APT_R1.invoice_num||' Duplicate Invoice Number exist current file / Stage Table for the Supplier'||V_vendor_name||'-->>';
                End If;

                V_Data_Count:=0;

                EXCEPTION
                     WHEN OTHERS then
                          V_Data_Count:=0;
                          v_error_hmsg:=v_error_hmsg||APT_R1.invoice_num||' Invalid Invocie Number-->>';

            End;

            Begin
                V_Data_Count:=0;

                 Select count(Invoice_Num)
                Into     V_Data_Count
                From      Ap_Invoices_all
                Where  Vendor_id        =    v_vendor_id
                --And    Vendor_site_id    =    v_vendor_site_id  -- Nilesh DT 13-APR-08
                And    Org_id            =     p_org_id
                And    Invoice_Num    =    APT_R1.invoice_num;

                If V_Data_Count<>0 Then
                      v_error_hmsg:=v_error_hmsg||APT_R1.invoice_num||' Invoice Number already exist for the Supplier '||V_vendor_name||'-->>';
                End If;

                V_Data_Count:=0;

            EXCEPTION
                 WHEN OTHERS then
                      V_Data_Count:=0;
                      v_error_hmsg:=v_error_hmsg||APT_R1.invoice_num||' Invalid Invocie Number-->>';
            End;
         End If;

         IF     v_vendor_id is not Null
         And      v_vendor_site_id is not Null Then

            V_Pay_Site_Flag:='N';

             Begin
                Select NVL(Pay_Site_Flag,'N')
                Into     V_Pay_Site_Flag
                from      po_vendor_sites_all
                Where  Vendor_id        =    v_vendor_id
                And    Vendor_site_id    =    v_vendor_site_id
                And    Org_id            =     p_org_id;

                If NVL(V_Pay_Site_Flag,'N')<>'Y' Then
                    v_error_hmsg:=v_error_hmsg||V_vendor_name||'-'||v_vendor_site_name ||' Supplier site for this supplier is not a pay site -->>';
                End If;
            EXCEPTION
                 WHEN OTHERS then
                     v_error_hmsg:=v_error_hmsg||V_vendor_name||'-'||v_vendor_site_name ||' Supplier site for this supplier is not a pay site -->>';
                    V_Pay_Site_Flag:='N';
            End;
         End If;

        -- Validation for Payment Method

        /*BEGIN
            If APT_R1.payment_method_Lookup_Code is not null then

                SELECT  lookup_code
                into       v_pay_lookup_code
                FROM    ap_lookup_codes
                WHERE   lookup_type='PAYMENT METHOD'
                AND     UPPER(lookup_code)=Trim(UPPER(APT_R1.payment_method_Lookup_Code))
                And       Nvl(Enabled_flag,'N')='Y'
                And      Sysdate Between nvl(Start_date_Active,sysdate) and nvl(Inactive_Date,sysdate);
            End If;
        EXCEPTION
            when no_data_found then
                v_error_hmsg:= v_error_hmsg ||APT_R1.payment_method_Lookup_Code|| 'Payment method does not exist in Oracle Financials-->>';
            when too_many_rows then
                   v_error_hmsg:= v_error_hmsg || APT_R1.payment_method_Lookup_Code||' Multiple Payment method records found-->>';
                 When    Others Then
                     v_error_hmsg:= v_error_hmsg ||APT_R1.payment_method_Lookup_Code||' Wrong Payment method -->>';
        END;*/

         IF    v_vendor_site_id is not Null Then
            v_pay_lookup_code:=Null;

            BEGIN
                Select iepm.Payment_Method_code
                Into     v_pay_lookup_code
                From      iby_external_payees_all iep,
                      iby_ext_party_pmt_mthds iepm
                Where  iep.supplier_site_id=v_vendor_site_id
                And      iep.Ext_Payee_id=iepm.ext_pmt_party_id;


            EXCEPTION
                when no_data_found then
                    --v_error_hmsg:= v_error_hmsg || 'Payment method does not exist in Supplier Site '||V_vendor_name||'-'||v_vendor_site_name ||'-->>';
                    v_pay_lookup_code:=APT_R1.payment_method_Lookup_Code;
                when too_many_rows then
                       --v_error_hmsg:= v_error_hmsg || 'Multiple Payment method records found '||V_vendor_name||'-'||v_vendor_site_name ||'-->>';
                    v_pay_lookup_code:=APT_R1.payment_method_Lookup_Code;
                     When    Others Then
                         --v_error_hmsg:= v_error_hmsg || 'Wrong Payment method '||V_vendor_name||'-'||v_vendor_site_name ||'-->>';
                    v_pay_lookup_code:=APT_R1.payment_method_Lookup_Code;
            END;

        End If;


        -- Validation for Invoice Amount
        If APT_R1.Invoice_Amount is Null Then
              v_error_hmsg:=v_error_hmsg||'Invoice Amount is Null-->>';
        End If;

        -- Validation for Invoice Amount with Transaction Type

        If UPPER(APT_R1.INVOICE_TYPE_LOOKUP_CODE)='STANDARD' and APT_R1.Invoice_Amount < 0 then

              v_error_hmsg:=v_error_hmsg||'STANDARD Invoice Amount is always positive-->>';

        Elsif UPPER(APT_R1.INVOICE_TYPE_LOOKUP_CODE)='CREDIT' and APT_R1.Invoice_Amount >= 0 then

              v_error_hmsg:=v_error_hmsg||'CREDIT MEMO Amount is always negative-->>';

        End If;

    -- Validation for Invoice Currency

        If APT_R1.Invoice_currency_Code is null then
            v_error_hmsg:=v_error_hmsg||'Currency Code is null -->';
        else
            BEGIN
                SELECT currency_code
                Into      v_currency
                FROM   fnd_currencies
                WHERE  upper(currency_code)=Trim(upper(APT_R1.Invoice_Currency_Code));

                If upper(trim(v_currency)) <> upper(trim(V_Fun_Curr)) and APT_R1.exchange_date is null Then
                        v_error_hmsg:=v_error_hmsg||'Exchange Date is Null -->>';
                End if;

            EXCEPTION
                When Others Then
                     v_error_hmsg:=v_error_hmsg||'Invalid Currency -->>';
                END;
        End If;

    -- Validation for Payment Terms
    /*
        IF APT_R1.Terms_Name IS Not NULL THEN
                v_term_name:=null;
            BEGIN
                   SELECT name
                  into   v_term_name
                  FROM   ap_terms_tl
                  WHERE  upper(trim(name))=upper(trim(APT_R1.Terms_Name))
                  and sysdate between nvl(start_date_active,sysdate) and nvl(end_date_active,sysdate);
            EXCEPTION
                when no_data_found then
                    v_error_hmsg:= v_error_hmsg || 'Payment Terms does not exist in Oracle Financials-->>';
                when too_many_rows then
                    v_term_name:=null;
                    v_error_hmsg:=v_error_hmsg||'Multiple Payment Terms found-->>';

                     When    Others Then
                    v_term_name:=null;
                    v_error_hmsg:=v_error_hmsg||'Invalid Payment Term -->>';
                 END;
        END IF;
    */

        IF v_vendor_id is Not Null And v_vendor_site_id is Not Null THEN
                v_term_name:=null;
            BEGIN
                 Select     name
                Into        v_term_name
                From       Po_Vendor_Sites_all PVS
                           ,Ap_terms_tl      AT
                Where      PVS.Vendor_id           =    v_vendor_id
                And        PVS.Vendor_Site_id      =    v_vendor_site_id
                And        PVS.Terms_Id        =     AT.Term_Id
                And        sysdate between nvl(AT.start_date_active,sysdate) and nvl(AT.end_date_active,sysdate);
            EXCEPTION
                when no_data_found then
                    v_error_hmsg:= v_error_hmsg || 'Payment Terms does not defined at Vendor Site '||V_vendor_name||'-'||v_vendor_site_name ||'-->>';
                    v_term_name:=null;
                when too_many_rows then
                    v_error_hmsg:=v_error_hmsg||'Multiple Payment Terms found '||V_vendor_name||'-'||v_vendor_site_name ||'-->>';
                    v_term_name:=null;
                     When    Others Then
                      v_term_name:=null;
                      v_error_hmsg:=v_error_hmsg||'Invalid Payment Term -->>';
                 END;
        END IF;


    -- Validation for GLdate

        If APT_R1.GL_date is Null Then

            v_error_hmsg:=v_error_hmsg||'GL Date is Null-->>';

        Else
            V_Data_Count:=0;
            BEGIN
                SELECT     count(*)
                Into        V_Data_Count
                FROM       gl_period_statuses gps,
                        fnd_application fna
                WHERE      fna.application_short_name    = 'SQLAP'
                And        gps.application_id        = fna.application_id
                And        gps.closing_status        = 'O'
                And         gps.set_of_books_id        = v_set_of_books_id
                And        APT_R1.GL_Date between gps.start_date and gps.end_date;

                If V_Data_Count=0 then
                    v_error_hmsg:= v_error_hmsg||APT_R1.GL_Date ||' GL Date is not in Open Period';
                End If;
            EXCEPTION
                 WHEN OTHERS then
                       v_error_hmsg:=v_error_hmsg||APT_R1.GL_Date||' GL Date is not in Open Period-->>';
                END;

        End If;
    v_Error_Line_Count:=0;
    v_error_almsg    :=Null;
    For APT_R2 in APT_C2(APT_R1.INVOICE_NUM
                    ,APT_R1.VENDOR_NUMBER
                    ,APT_R1.VENDOR_DC_CODE
                  ,APT_R1.OPERATING_UNIT
                  ,APT_R1.SOURCE
                  ,APT_R1.GRN_NUMBER)
    Loop
        Exit When APT_C2%notfound;

        v_line_count        :=APT_C2%ROWCOUNT;
        V_error_lmsg        :=null;
        v_tax_name            :=null;
        V_Data_Count        :=0;
        V_Code_Combination_id    :=null;

        -- Validation for Invoice Line Number
        If APT_R2.LINE_NUMBER is Null then

            v_error_lmsg:=v_error_lmsg||'Invoice Line Number is Null-->>';

        Elsif APT_R2.LINE_NUMBER <>v_line_count Then

            v_error_lmsg:=v_error_lmsg||'Invoice Line Number is not in Sequence-->>';

        End If;

        -- Validation for Invoice Line Amount

        If APT_R2.Amount is Null Then

              v_error_lmsg:=v_error_lmsg||'Invoice Line Amount is Null-->>';

        End If;

        v_Inv_Line_Amt:=Nvl(v_Inv_Line_Amt,0) + Nvl(APT_R2.Amount,0);


        -- Validation for GLdate

        If APT_R2.Accounting_date is Null Then

            v_error_lmsg:=v_error_lmsg||'Accounting Date is Null-->>';

        Else

            BEGIN
                SELECT     count(*)
                Into        V_Data_Count
                FROM       gl_period_statuses gps,
                        fnd_application fna
                WHERE      fna.application_short_name    = 'SQLAP'
                And        gps.application_id        = fna.application_id
                And        gps.closing_status        = 'O'
                And        APT_R2.Accounting_Date between gps.start_date and gps.end_date;

                If V_Data_Count=0 then
                    v_error_lmsg:= v_error_lmsg||'Accounting Date is not in Open Period';
                End If;
            EXCEPTION
                 WHEN OTHERS then
                       v_error_lmsg:=v_error_lmsg||'Accounting Date is not in Open Period-->>';
                END;

        End If;

    /*    If     APT_R2.DIST_CODE_CONCATENATED is Null Then

            v_error_lmsg:=v_error_lmsg||'Charge Account Code is Null-->>';
        Else
*/
        -- Validation for CO Seg
        If     APT_R2.CO is Null then

             v_error_lmsg:=v_error_lmsg||'Accounting Segment Company Value is Null-->>';
        Else

            V_Seg_Status:=Null;

             V_Seg_Status:=ACCOUNT_SEG_STATUS(APT_R2.CO,'ABRL_GL_CO');

             If V_Seg_Status is Not Null then
                 v_error_lmsg:=v_error_lmsg||'Invalid Company Segment '||APT_R2.CO||'-->>';
             End If;

        End If;
        -- Validation for CC Seg
        If     APT_R2.CC is Null then

             v_error_lmsg:=v_error_lmsg||'Accounting Segment Cost Center Value is Null-->>';

        Else
            V_Seg_Status:=Null;

             V_Seg_Status:=ACCOUNT_SEG_STATUS(APT_R2.CC,'ABRL_GL_CC');

             If V_Seg_Status is Not Null then
                 v_error_lmsg:=v_error_lmsg||'Invalid Cost Center Segment '||APT_R2.CC||'-->>';
             End If;
        End If;

        -- Validation for Location Seg
        If     APT_R2.Location is Null then

             v_error_lmsg:=v_error_lmsg||'Accounting Segment Location Value is Null-->>';
        Else
            V_Seg_Status:=Null;

             V_Seg_Status:=ACCOUNT_SEG_STATUS(APT_R2.Location,'ABRL_GL_Location');

             If V_Seg_Status is Not Null then
                 v_error_lmsg:=v_error_lmsg||'Invalid Location Segment '||APT_R2.Location||'-->>';
             End If;
          End If;

        -- Validation for Merchandise Seg

        If     APT_R2.Merchandise is Null then

             v_error_lmsg:=v_error_lmsg||'Accounting Segment Merchandise Value is Null-->>';

        Else
            V_Seg_Status:=Null;

             V_Seg_Status:=ACCOUNT_SEG_STATUS(APT_R2.Merchandise,'ABRL_GL_Merchandize');

             If V_Seg_Status is Not Null then
                 v_error_lmsg:=v_error_lmsg||'Invalid Merchandise Segment '||APT_R2.Merchandise||'-->>';
             End If;
        End If;

        -- Validation for Account Seg

        If     APT_R2.Account is Null then

             v_error_lmsg:=v_error_lmsg||'Accounting Segment Natural Account Value is Null-->>';
        Else
            V_Seg_Status:=Null;

             V_Seg_Status:=ACCOUNT_SEG_STATUS(APT_R2.Account,'ABRL_GL_Account');

             If V_Seg_Status is Not Null then
                 v_error_lmsg:=v_error_lmsg||'Invalid Account Segment '||APT_R2.Account||'-->>';
             End If;

        End If;
        -- Validation for Intercompany Seg
        If     APT_R2.Intercompany is Null then

             v_error_lmsg:=v_error_lmsg||'Accounting Segment Intercompany Value is Null-->>';
        Else
            V_Seg_Status:=Null;

             V_Seg_Status:=ACCOUNT_SEG_STATUS(APT_R2.Intercompany,'ABRL_GL_STATE_SBU');

             If V_Seg_Status is Not Null then
                 v_error_lmsg:=v_error_lmsg||'Invalid Intercompany Segment '||APT_R2.Intercompany||'-->>';
             End If;
        End If;

        -- Validation for Future Seg
        If     APT_R2.Future is Null then

             v_error_lmsg:=v_error_lmsg||'Accounting Segment Future Value is Null-->>';

        Else
            V_Seg_Status:=Null;

             V_Seg_Status:=ACCOUNT_SEG_STATUS(APT_R2.Future,'ABRL_GL_Future');

             If V_Seg_Status is Not Null then
                 v_error_lmsg:=v_error_lmsg||'Invalid Future Segment '||APT_R2.Future||'-->>';
             End If;

        End If;


         If v_vendor_id is not Null And v_vendor_site_id is not Null then


            V_State_Sbu:=Null;

            Begin

                Select     GCC.segment3
                Into        V_State_Sbu
                From      gl_code_combinations GCC
                         ,Po_Vendor_Sites_all PVS
                         ,Po_Vendors   PV
                Where     PV.Vendor_Id    =    v_vendor_id
                And          PV.Vendor_id    =    PVS.Vendor_Id
                And       PVS.Vendor_Site_Id=    v_vendor_site_id
                And        PVS.Org_ID        =    P_Org_Id
                And       PVS.Accts_Pay_Code_Combination_Id=GCC.Code_Combination_Id;

            EXCEPTION
                When no_data_found then
                       v_error_lmsg:=v_error_lmsg||'Accounting Code missing at Vendor Site '||v_vendor_name||' '||v_vendor_site_name||'-->>';
                     V_State_Sbu:= Null;
                     When too_many_rows then
                       v_error_lmsg:=v_error_lmsg||'Multiple Charge Account Code exists for '||v_vendor_name||' '||v_vendor_site_name||'-->>';
                         V_State_Sbu:= Null;
                When Others Then
                       v_error_lmsg:=v_error_lmsg||'Accounting Code missing at Vendor Site '||v_vendor_name||' '||v_vendor_site_name||'-->>';
                     V_State_Sbu:= Null;
            End;
        End If;
            Begin
                V_Code_Combination_id:=Null;

                Select      Code_Combination_id
                Into          V_Code_Combination_id
                From         GL_CODE_COMBINATIONS
                where     Segment1    =APT_R2.CO
                And       Segment2    =APT_R2.CC
                And       Segment3    =V_State_Sbu
                And       Segment4    =APT_R2.Location
                And       Segment5    =APT_R2.Merchandise
                And       Segment6    =APT_R2.Account
                And       Segment7    =APT_R2.Intercompany
                And       Segment8    =APT_R2.Future;
            EXCEPTION
                When no_data_found then
                       v_error_lmsg:=v_error_lmsg||'Charge Account Code '||APT_R2.CO||'.'||APT_R2.CC||'.'||V_State_Sbu||'.'||APT_R2.Location||'.'||APT_R2.Merchandise||'.'||APT_R2.Account||'.'||APT_R2.Intercompany||'.'||APT_R2.Future||' does not exists-->>';
                       When too_many_rows then
                       v_error_lmsg:=v_error_lmsg||'Multiple Charge Account Code '||APT_R2.CO||'.'||APT_R2.CC||'.'||V_State_Sbu||'.'||APT_R2.Location||'.'||APT_R2.Merchandise||'.'||APT_R2.Account||'.'||APT_R2.Intercompany||'.'||APT_R2.Future||' exists-->>';
                       When Others Then
                       v_error_lmsg:=v_error_lmsg||'Charge Account Code '||APT_R2.CO||'.'||APT_R2.CC||'.'||V_State_Sbu||'.'||APT_R2.Location||'.'||APT_R2.Merchandise||'.'||APT_R2.Account||'.'||APT_R2.Intercompany||'.'||APT_R2.Future||' does not exists-->>';
            End;
    --    End If;
        -- Validation for Tax Codes
        If APT_R2.TDS_tax_name is Not Null Then
            BEGIN
                v_tax_name:=Null;

                SELECT Tax_name
                Into      v_tax_name
                FROM   JAI_CMN_TAXES_ALL
                WHERE  upper(Tax_Name)    =    Trim(upper(APT_R2.TDS_tax_name))
                AND    org_id        =    P_org_id
                AND     Sysdate Between nvl(Start_date,sysdate) and nvl(End_Date,sysdate);
            EXCEPTION
                When no_data_found then
                       v_error_lmsg:=v_error_lmsg||'Tax Name does not exists-->>';
                       When too_many_rows then
                       v_error_lmsg:=v_error_lmsg||'Multiple Tax Name exists-->>';
                       When Others Then
                       v_error_lmsg:=v_error_lmsg||'Tax Name does not exists-->>';
                 END;
          End If;


    --Updating the error message in table

        IF v_error_lmsg is not null then

            Update XXABRL_AP_INVOICE_LINES_INT
            Set      error_message    =    v_error_lmsg
                ,Interfaced_Flag  =     'E'
            Where ROWID=APT_R2.ROWID;
            v_Error_Line_Count    :=    v_Error_Line_Count+1;

        Else
            Update XXABRL_AP_INVOICE_LINES_INT
            Set DIST_CODE_COMBINATION_ID  = V_Code_Combination_id
               ,Org_Id                = P_Org_Id
               ,TDS_tax_name            = v_tax_name
               ,Interfaced_Flag          = 'V'
            Where ROWID=APT_R2.ROWID;

        End If;
        If v_error_lmsg is not Null Then
             v_error_almsg:=v_error_almsg||'Line No :'||v_line_count||' '||v_error_lmsg;
        End If;
       End Loop;

        If v_Inv_Line_Amt<>APT_R1.Invoice_Amount then
            v_error_hmsg:=v_error_hmsg||'Invoice Header and Line Amount is not matching';
        End If;

--Updating the error message in table

        IF v_error_hmsg is not null then

            Update XXABRL_AP_INVOICES_INT
            Set  error_message    =    v_error_hmsg
                ,Interfaced_Flag  = 'E'
            Where Rowid=APT_R1.ROWID;

            v_Error_Rec_count:=v_Error_Rec_count+1;

            Commit;

            fnd_file.put_line(fnd_file.output,v_record_count||' )' ||'Invoice Number : '||APT_R1.invoice_num ||' ,'||'Vendor Number : '||APT_R1.vendor_number);
            fnd_file.put_line(fnd_file.output,'Header Error :'||v_error_hmsg);

            If v_error_almsg is not null then
                fnd_file.put_line(fnd_file.output,'Line Error :'||v_error_almsg);
            End If;


        Elsif v_error_hmsg is null and v_Error_Line_Count<>0 then

            Update XXABRL_AP_INVOICES_INT
            Set       error_message    = v_Error_Line_Count||' Errors in Interface Lines'
                ,Interfaced_Flag  = 'E'
            Where Rowid=APT_R1.ROWID;

            v_Error_Rec_count:=v_Error_Rec_count+1;

            Commit;

            fnd_file.put_line(fnd_file.output,v_record_count||' )' ||'Invoice Number : '||APT_R1.invoice_num ||' ,'||'Vendor Number : '||APT_R1.vendor_number);
            fnd_file.put_line(fnd_file.output,'Header Error :'||v_error_hmsg);

            If v_error_almsg is not null then
                fnd_file.put_line(fnd_file.output,'Line Error :'||v_error_almsg);
            End If;

        Else

            Update XXABRL_AP_INVOICES_INT
            Set       VENDOR_ID         =    v_vendor_id
                ,VENDOR_SITE_ID    =    v_vendor_site_id
                ,Org_Id        =     P_Org_Id
                ,TERMS_NAME        =    v_term_name
                ,Payment_Method_Lookup_Code=    NVL(v_pay_lookup_code,'CHECK')
                ,error_message    =    Null
                ,Interfaced_Flag  =    'V'
            Where Rowid=APT_R1.ROWID;

            v_OK_Rec_count:=v_OK_Rec_count+1;
            Commit;
            fnd_file.put_line(fnd_file.output,v_record_count||' )' ||'Invoice Number : '||APT_R1.invoice_num ||' ,'||'Vendor Number : '||APT_R1.vendor_number);
            fnd_file.put_line(fnd_file.output,'Status : Valid Invoice');

        End If;

    End Loop;
If v_Error_Rec_count>0 then
    retcode:=1;
End If;

fnd_file.put_line(fnd_file.output,'........................................................................' );


    For APT_R3 in APT_C3(Trim(Upper(V_OPERATING_UNIT))
                   ,Trim(Upper(P_Data_Source)))
    Loop
        Exit When APT_C3%notfound;
        fnd_file.put_line(fnd_file.output,APT_R3.Creation_Date||' Records with error :'||APT_R3.Rec_Count);

    End Loop;

fnd_file.put_line(fnd_file.output,'Total Number of Records with error :'||v_Error_Rec_count);
fnd_file.put_line(fnd_file.output,'Total Number of Valid Records      :'||v_OK_Rec_count);
fnd_file.put_line(fnd_file.output,'........................................................................' );


v_source_lookup:=P_Data_Source;
V_Org_Id       :=P_Org_Id;

INVOICE_INSERT(V_Org_Id,v_source_lookup);

End;

PROCEDURE INVOICE_INSERT(P_Org_Id          IN  NUMBER
                 ,P_Data_Source      IN  VARCHAR2) is

CURSOR APT_C_LOC
    IS   Select Unique DC_LOCATION, GL_Date
        From    XXABRL_AP_INVOICES_INT
        Where Trim(Upper(SOURCE))        =    Trim(Upper(P_Data_Source))
        And    Org_Id                =     P_Org_Id
        And    NVL(INTERFACED_FLAG,'N')     =    'V'
        And    ERROR_MESSAGE is Null;


CURSOR APT_C1(P_DC_LOCATION                   VARCHAR2
          ,P_GL_Date                Date
          )
      IS   Select ROWID,
            OPERATING_UNIT            ,INVOICE_NUM
            ,SOURCE                ,INVOICE_TYPE_LOOKUP_CODE
            ,INVOICE_DATE            ,VENDOR_NUMBER
             ,VENDOR_DC_CODE            ,PAYMENT_METHOD_LOOKUP_CODE
            ,TERMS_NAME            ,INVOICE_AMOUNT
                ,INVOICE_CURRENCY_CODE        ,EXCHANGE_RATE
                ,EXCHANGE_DATE            ,EXCHANGE_RATE_TYPE
            ,DESCRIPTION            ,GL_DATE
            ,DC_LOCATION            ,GRN_NUMBER
            ,VENDOR_ID             ,VENDOR_SITE_ID
            ,Org_Id                ,TERM_ID
        From    XXABRL_AP_INVOICES_INT
        Where Trim(Upper(SOURCE))        =    Trim(Upper(P_Data_Source))
        And    Org_Id                =     P_Org_Id
        And    NVL(INTERFACED_FLAG,'N')     =    'V'
        And    DC_LOCATION                =    P_DC_LOCATION
        And    GL_Date                =    P_GL_Date
        And    ERROR_MESSAGE is Null
        Order by GRN_NUMBER;

    CURSOR APT_C2(P_INVOICE_NUM             VARCHAR2
              ,P_VENDOR_NUMBER             VARCHAR2
              ,P_VENDOR_DC_CODE              VARCHAR2
             )
    IS    Select ROWID,
            INVOICE_NUM                ,VENDOR_NUMBER
            ,VENDOR_DC_CODE            ,LINE_NUMBER
            ,LINE_TYPE_LOOKUP_CODE        ,AMOUNT
            ,ACCOUNTING_DATE            ,DIST_CODE_COMBINATION_ID
            ,DESCRIPTION            ,TDS_TAX_NAME
            ,REFERENCE_1            ,REFERENCE_2
            ,Org_id
        From
            XXABRL_AP_INVOICE_LINES_INT
        Where    Trim(Upper(SOURCE))        =    Trim(Upper(P_Data_Source))
        And    Org_Id                =     P_Org_Id
        And    NVL(INTERFACED_FLAG,'N')     =    'V'
        And    ERROR_MESSAGE is Null
        And    INVOICE_NUM                =    P_INVOICE_NUM
        And    VENDOR_NUMBER            =    P_VENDOR_NUMBER
        And    VENDOR_DC_CODE            =    P_VENDOR_DC_CODE;

v_invoice_id Number;
--v_org_id               number;
v_user_id               number:=FND_PROFILE.value('USER_ID');
v_resp_id               number:=FND_PROFILE.value('RESP_ID');
v_appl_id               number:=FND_PROFILE.value('RESP_APPL_ID');
v_req_id              number;
v_record_count             number:=0;
V_Doc_Category        Varchar2(50);
V_Batch_Name        Varchar2(50);
V_Status            Varchar2(1);
Begin
--v_org_id    :=P_Org_Id;
-----
-- Inserting data into the Header table AP_INVOICES_INTEFACE
-----

For APT_R_LOC in APT_C_LOC
loop
    Exit When APT_C_LOC%notfound;

    For APT_R1 in APT_C1(APT_R_LOC.DC_LOCATION
                  ,APT_R_LOC.GL_DATE
                  )
    loop
        Exit When APT_C1%notfound;

        v_record_count := APT_C1%ROWCOUNT;


        SELECT ap_invoices_interface_s.NEXTVAL
            INTO      v_invoice_id
        FROM      DUAL;

        V_Doc_Category:=Null;

        If Trim(Upper(APT_R1.invoice_type_lookup_code))='STANDARD' Then
            V_Doc_Category:='STD INV';
        Elsif Trim(Upper(APT_R1.invoice_type_lookup_code))='CREDIT' Then
            V_Doc_Category:='CRM INV';
        Else
            V_Doc_Category:=Null;
        End If;

        Insert into ap_invoices_interface
                 (INVOICE_ID,                      INVOICE_NUM,
                    INVOICE_TYPE_LOOKUP_CODE,        INVOICE_DATE,
                   VENDOR_ID,                        VENDOR_SITE_ID,
                  INVOICE_AMOUNT,                INVOICE_CURRENCY_CODE,
                  EXCHANGE_DATE,                    TERMS_NAME,
                  DESCRIPTION,                      SOURCE,
                  PAYMENT_METHOD_CODE,           DOC_CATEGORY_CODE,
                   GL_DATE,                    ORG_ID,
                  TAXATION_COUNTRY,
                  CREATED_BY,                CREATION_DATE,
                  LAST_UPDATED_BY,            LAST_UPDATE_DATE)
                 VALUES
                   (v_invoice_id,                Trim(APT_R1.invoice_num),
                  Trim(APT_R1.invoice_type_lookup_code),    Trim(APT_R1.invoice_date),
                  Trim(APT_R1.vendor_id),        Trim(APT_R1.vendor_site_id),
                  Trim(APT_R1.Invoice_Amount),    Trim(APT_R1.Invoice_currency_code),
                  Trim(APT_R1.exchange_date),        Trim(APT_R1.Terms_Name),
                  Trim(APT_R1.description),        Trim(APT_R1.source),
                  Trim(APT_R1.payment_method_Lookup_Code), V_Doc_Category,
                  Trim(APT_R1.GL_date),            APT_R1.org_id,
                  'IN',
                  v_user_id,                sysdate,
                  v_user_id,                sysdate);

        Commit;

        --
            -- Inserting records into Detail table : AP_INVOICE_LINES_INTERFACE
            --

    For APT_R2 in APT_C2(APT_R1.INVOICE_NUM
                    ,APT_R1.VENDOR_NUMBER
                    ,APT_R1.VENDOR_DC_CODE
                   )
    loop
        Exit When APT_C2%notfound;


        INSERT INTO ap_invoice_lines_interface
                     (invoice_id,                         invoice_line_id,
                line_number,                        line_type_lookup_code,
                      amount,                              accounting_date,
                  DIST_CODE_COMBINATION_ID,
                Reference_1,                Reference_2,
                Description,                Org_id,
                Attribute_Category,
                Attribute1,                    Attribute2,
                  created_by,                         creation_date,
                  last_updated_by,                    last_update_date)
                VALUES(v_invoice_id,                   ap_invoice_lines_interface_s.NEXTVAL,
                      APT_R2.LINE_NUMBER,            Trim(APT_R2.LINE_TYPE_LOOKUP_CODE),
                    Trim(APT_R2.AMOUNT),            Trim(APT_R2.ACCOUNTING_DATE),
                    Trim(APT_R2.DIST_CODE_COMBINATION_ID),
                Trim(NVL(APT_R2.REFERENCE_1,APT_R1.GRN_NUMBER)),Trim(APT_R2.REFERENCE_2),
                Trim(APT_R2.DESCRIPTION),        APT_R2.org_id,
                Trim('E-RETAIL PAYABLE INVOICE'),
                Trim(APT_R1.DC_LOCATION),        Trim(APT_R2.TDS_TAX_NAME),
                v_user_id,                      sysdate,
                    v_user_id,                      sysdate);


            Update XXABRL_AP_INVOICE_LINES_INT
            Set      Interfaced_Flag  =  'Y'
                ,Interfaced_Date  = Sysdate
            Where Rowid=APT_R2.ROWID;

            Commit;

    End Loop;

            Update XXABRL_AP_INVOICES_INT
            Set      Interfaced_Flag  =  'Y'
                ,Interfaced_Date  = Sysdate
            Where Rowid=APT_R1.ROWID;
        Commit;
    End Loop;


    Begin
        V_Batch_Name:=Null;

        Select  P_Data_Source||'-'||Substr(FFVV.description,1,30)||'-'||TO_CHAR (APT_R_LOC.GL_Date, 'DD-MON-RRRR')
        Into      V_Batch_Name
        From    FND_FLEX_VALUES_VL FFVV,
                 FND_FLEX_VALUE_SETS FFVS
        Where   FFVS.Flex_Value_Set_Name    = 'ABRL_GL_Location'
        And     FFVS.Flex_Value_Set_Id    = FFVV.Flex_Value_Set_Id
        And     FFVV.Flex_Value            = APT_R_LOC.DC_LOCATION;

   EXCEPTION
         When Others Then
              V_Batch_Name:=P_Data_Source||'-'||TO_CHAR (APT_R_LOC.GL_Date, 'DD-MON-RRRR');

    End;

fnd_file.put_line(fnd_file.output,'Number of Records (Invoices )Inserted in Interface Table :'||v_record_count);
fnd_file.put_line(fnd_file.output,'........................................................................' );
-----
-- Run Payable Open Iterface for Interface data into Oracle Payables
----
    If v_record_count>0 Then
           fnd_global.apps_initialize (
                user_id=> v_user_id,
                resp_id=> v_resp_id,
                resp_appl_id=> v_appl_id
             );

        Commit;

        v_req_id :=fnd_request.submit_request (
                      'SQLAP',
                    'APXIIMPT',
                    'Payables Open Interface Import'||P_Data_Source,
                NULL,
                      FALSE,
                p_org_id,
                    P_Data_Source, -- Parameter Data source
                    NULL, -- Group id
                     V_Batch_Name,
                    NULL,
                  NULL,
                  NULL,
                  'N',
              'N',
                'N',
              'Y',
                    CHR (0)
                );
        Commit;
        fnd_file.put_line(fnd_file.output,'Please see the output of Payables OPEN Invoice Import program request id :'||v_req_id);
        fnd_file.put_line(fnd_file.output,'........................................................................' );

        Loop
             Begin
                V_Status:=Null;

                Select Status_Code
                Into     V_Status
                from      Fnd_Concurrent_Requests
                where  Request_id=v_req_id;

                If V_Status in ('C','D','E','G','X') then
                       Exit;
                End If;

            EXCEPTION
                When Others Then
                      Exit;

             End;
        End Loop;
    End If;
End Loop;

End;
FUNCTION ACCOUNT_SEG_STATUS(P_Seg_Value IN Varchar2,
                      P_Seg_Desc  IN Varchar2) return Varchar2 Is
V_Count    Number:=0;
Begin

    Select Count(FFVV.Flex_Value)
    Into   V_Count
    From   FND_FLEX_VALUES_VL FFVV,
           FND_FLEX_VALUE_SETS FFVS
    Where  Upper(FFVS.Flex_Value_Set_Name)    = Upper(P_Seg_Desc)
    And    FFVS.Flex_Value_Set_Id            = FFVV.Flex_Value_Set_Id
    And    FFVV.Flex_Value                = P_Seg_Value;

        If V_Count=1 Then
            Return Null;
        Else
            Return 'Invalid Value';
        End If;

    EXCEPTION
        When Others Then
            Return 'Invalid Value';

End    ACCOUNT_SEG_STATUS;


END XXABRL_AP_PKG_INV_Interface;
/


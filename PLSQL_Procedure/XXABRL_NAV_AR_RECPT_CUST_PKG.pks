CREATE OR REPLACE PACKAGE APPS.XXABRL_NAV_AR_RECPT_CUST_PKG IS
  PROCEDURE CUST_RECEIPT_VALIDATE(p_errbuf       OUT VARCHAR2 
                             ,p_retcode          OUT NUMBER 
                             ,p_action           IN  VARCHAR2 
                             ,P_Data_Source      IN VARCHAR2 
                             ,p_receipt_to_apply IN VARCHAR2 
                             ,p_apply_all_flag   IN VARCHAR2 
                             ,p_gl_date         IN VARCHAR2
                             );
  PROCEDURE CUST_RECEIPT_INSERT(P_Org_Id      IN NUMBER
                               ,P_Data_Source IN VARCHAR2
                               ,p_gl_date     IN DATE
                               );

  PROCEDURE CUST_RECEIPT_APPLY_CALL(P_Ref_Number IN VARCHAR2
                                   ,P_GL_DATE    IN DATE);

  PROCEDURE CUST_RECEIPT_APPLY_DATA(
                                ARR_R2             IN  xxabrl_navi_ar_receipt_stg2%ROWTYPE
                               ,p_Rcpt_Amt_To_Apply   NUMBER
                               ,x_Rcpt_Bal_Amt    OUT NUMBER
                               ,x_exit_flag       OUT VARCHAR2
                               );

END XXABRL_NAV_AR_RECPT_CUST_PKG; 
/


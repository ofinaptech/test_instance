CREATE OR REPLACE PROCEDURE APPS.MIS_RECO_STDINV_PAY (
   errbuf      OUT      VARCHAR2,
   retcode     OUT      VARCHAR2,
   form_date   IN       VARCHAR2,
   to_date1    IN       VARCHAR2
)
AS
   p_from_run_date     DATE
                     := NVL (fnd_date.canonical_to_date (form_date), SYSDATE);
   p_to_run_date       DATE
                      := NVL (fnd_date.canonical_to_date (to_date1), SYSDATE);
   ap_file             UTL_FILE.file_type;
   g_conc_request_id   NUMBER        := fnd_profile.VALUE ('CONC_REQUEST_ID');
   
cursor invoice_std_pay
is    
SELECT '1', 
api.invoice_type_lookup_code invoice_type,
       REPLACE (api.invoice_num, '^', '//') invoice_num, 
       api.invoice_date,
       DECODE (api.invoice_type_lookup_code,
               'CREDIT', ABS (z.amt_val)- ABS (NVL (api.discount_amount_taken, 0)),0) dr_val,
       DECODE (api.invoice_type_lookup_code,
               'CREDIT', 0,
               z.amt_val - NVL (api.discount_amount_taken, 0) ) cr_val,
       TO_CHAR (api.doc_sequence_value) pay_doc_num, 
       pov.segment1 vendor_num, 
       pov.vendor_name, 
       api.invoice_id,
       api.invoice_amount, 
       api.amount_paid,
       z.accounting_date gl_date
  FROM apps.ap_invoices_all api,
       apps.ap_invoice_lines_all apil,
       apps.ap_invoice_distributions_all apd,
       apps.ap_suppliers pov,
       apps.gl_code_combinations gl,
       apps.ap_supplier_sites_all povs,
       (SELECT   NVL (SUM (apd.amount), 0) amt_val, api.invoice_id,
                 apd.accounting_date
            FROM apps.ap_invoices_all api,
                 apps.ap_invoice_lines_all apil,
                 apps.ap_invoice_distributions_all apd
           WHERE api.invoice_id = apd.invoice_id
             AND apil.invoice_id = api.invoice_id
             AND apil.line_number = apd.invoice_line_number
              AND TO_DATE (TO_CHAR (apd.accounting_date, 'DD-MON-YY'))
                          BETWEEN p_from_run_date
                              AND p_to_run_date  
             AND api.invoice_type_lookup_code <> 'PREPAYMENT'
             AND apil.line_type_lookup_code <> 'PREPAY'
        GROUP BY api.invoice_id, apd.accounting_date) z
 WHERE api.invoice_id = z.invoice_id
   AND api.invoice_id = apd.invoice_id
   AND gl.code_combination_id = api.accts_pay_code_combination_id
   AND apil.invoice_id = api.invoice_id
   AND apil.line_number = apd.invoice_line_number
   AND api.vendor_site_id = povs.vendor_site_id
    AND TO_DATE (TO_CHAR (z.accounting_date, 'DD-MON-YY'))
                BETWEEN p_from_run_date
                    AND p_to_run_date
   AND apd.ROWID = (SELECT ROWID
                      FROM apps.ap_invoice_distributions_all
                     WHERE ROWNUM = 1 AND invoice_id = apd.invoice_id)
   AND api.vendor_id = pov.vendor_id
   AND api.invoice_type_lookup_code <> 'PREPAYMENT'
    AND ap_invoices_pkg.get_approval_status (api.invoice_id,
                                                  api.invoice_amount,
                                                  api.payment_status_flag,
                                                  api.invoice_type_lookup_code
                                                 ) IN
                                                    ('APPROVED', 'CANCELLED')
   AND (   (api.invoice_type_lookup_code <> 'DEBIT')
        OR (api.invoice_type_lookup_code = 'DEBIT')
       )
--and api.invoice_num in('13/2016-2017','KT/FEB/16-3')
UNION ALL
SELECT '2',
       DECODE (api.invoice_type_lookup_code,
               'PREPAYMENT', api.invoice_type_lookup_code,
               'PAYMENT') invoice_type,
       REPLACE (api.invoice_num, '^', '//') invoice_num, 
       api.invoice_date,
       DECODE (api.invoice_type_lookup_code,
               'CREDIT', DECODE (status_lookup_code, 'VOIDED', 0, 0),app.amount) dr_val,
       DECODE (api.invoice_type_lookup_code,
               'CREDIT', DECODE (status_lookup_code,'VOIDED', app.amount,ABS (app.amount)),0) cr_val,
       DECODE (api.payment_status_flag,
               'Y', TO_CHAR (apc.doc_sequence_value),
               'P', TO_CHAR (apc.doc_sequence_value),
               TO_CHAR (apc.doc_sequence_value), 'N',
               TO_CHAR (apc.doc_sequence_value)
              ) pay_doc_num,
         pov.segment1 vendor_num,
         pov.vendor_name,
        api.invoice_id, api.invoice_amount, api.amount_paid,      
       apc.check_date gl_date
  FROM apps.ap_invoices_all api,
       apps.ap_invoice_lines_all apil,
       apps.gl_code_combinations gl,
       apps.ap_invoice_distributions_all apd,
       apps.ap_suppliers pov,
       apps.ap_invoice_payments_all app,
       apps.ap_payment_schedules_all aps,
       apps.ap_checks_all apc,
       apps.ap_supplier_sites_all povs
 WHERE api.invoice_id = apd.invoice_id
   AND apil.invoice_id = api.invoice_id
   AND aps.invoice_id(+) = api.invoice_id
   AND gl.code_combination_id = api.accts_pay_code_combination_id
   AND apil.line_number = apd.invoice_line_number
  AND TO_DATE (TO_CHAR (app.accounting_date, 'DD-MON-YY'))
                BETWEEN p_from_run_date
                    AND p_to_run_date
   AND apd.ROWID = (SELECT ROWID
                      FROM apps.ap_invoice_distributions_all
                     WHERE ROWNUM = 1 AND invoice_id = apd.invoice_id)
   AND api.vendor_id = pov.vendor_id
   AND app.invoice_id = api.invoice_id
   AND app.check_id = apc.check_id
   AND apc.status_lookup_code IN
          ('CLEARED', 'NEGOTIABLE', 'VOIDED', 'RECONCILED UNACCOUNTED',
           'RECONCILED', 'CLEARED BUT UNACCOUNTED')
   AND api.vendor_site_id = povs.vendor_site_id
   AND api.invoice_type_lookup_code <> 'PREPAYMENT';
 --  and api.invoice_num in('13/2016-2017','KT/FEB/16-3');

BEGIN
   ap_file :=
      UTL_FILE.fopen ('VENDOR_INVOICE_PORTAL',
                      'MIS_RECO_STDINV_PAY' || g_conc_request_id || '.txt',
                      'w'
                     );
   UTL_FILE.put_line (ap_file,                  
                        'INVOICE_TYPE'
                      || '^'
                      || 'INVOICE NUMBER'
                      || '^'
                      || 'INVOICE DATE'
                      || '^'
                      || 'DR VAL'
                      || '^'
                      || 'CR VAL'
                      || '^'
                      || 'DOC NUM'
                      || '^'
                      || 'VENDOR NUMBER'
                      || '^'
                      || 'VENDOR NAME'
                      || '^'
                      || 'INVOICE ID'
                      || '^'
                      || 'INVOICE AMOUNT'
                      || '^'
                      || 'AMOUNT PAID'
                      || '^'
                      || 'GL DATE'
                     );

   FOR ap_rec IN invoice_std_pay
   LOOP
      UTL_FILE.put_line (ap_file,
                           ap_rec.invoice_type
                         || '^'
                         || ap_rec.invoice_num
                         || '^'
                         || ap_rec.invoice_date
                         || '^'
                         || ap_rec.dr_val
                         || '^'
                         || ap_rec.cr_val
                         || '^'
                         || ap_rec.pay_doc_num
                         || '^'
                         || ap_rec.vendor_num
                         || '^'
                         || ap_rec.vendor_name
                         || '^'
                         || ap_rec.invoice_id
                         || '^'
                         || ap_rec.invoice_amount
                         || '^'
                         || ap_rec.amount_paid
                         || '^'
                         || ap_rec.gl_date
                        );
   END LOOP;

   UTL_FILE.fclose (ap_file);
EXCEPTION
   WHEN TOO_MANY_ROWS
   THEN
      DBMS_OUTPUT.put_line ('Too Many Rows');
   WHEN NO_DATA_FOUND
   THEN
      DBMS_OUTPUT.put_line ('No Data Found');
   WHEN UTL_FILE.invalid_path
   THEN
      raise_application_error (-20000, 'utl_file.invalid_path');
   WHEN UTL_FILE.invalid_mode
   THEN
      raise_application_error (-20000, 'utl_file.invalid_mode');
   WHEN UTL_FILE.invalid_filehandle
   THEN
      raise_application_error (-20000, 'utl_file.invalid_filehandle');
   WHEN UTL_FILE.invalid_operation
   THEN
      raise_application_error (-20000, 'utl_file.invalid_operation');
   WHEN UTL_FILE.read_error
   THEN
      raise_application_error (-20001, 'utl_file.read_error');
   WHEN UTL_FILE.write_error
   THEN
      raise_application_error (-20001, 'utl_file.write_error');
   WHEN UTL_FILE.internal_error
   THEN
      raise_application_error (-20001, 'utl_file.internal_error');
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line (SUBSTR (   'Error '
                                    || TO_CHAR (SQLCODE)
                                    || ': '
                                    || SQLERRM,
                                    1,
                                    255
                                   )
                           );
      UTL_FILE.fclose (ap_file);
END; 
/


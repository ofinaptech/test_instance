CREATE OR REPLACE PROCEDURE APPS.GST_ABRL_AP_PREPAY_INV_PROC (
   errbuf              OUT      VARCHAR2,
   retcode             OUT      NUMBER,
   p_org_id            IN       NUMBER,
   p_from_date         IN       VARCHAR2,
   p_to_date           IN       VARCHAR2,
   p_from_vendor_num   IN       VARCHAR2,
   p_to_vendor_num     IN       VARCHAR2,
-- p_vendor_site       IN       VARCHAR2,
   p_from_voucher      IN       NUMBER,
   p_to_voucher        IN       NUMBER
)
IS
   v_prepay_amount             NUMBER := 0;
   v_prepay_applied_amount NUMBER := 0;
   v_prepay_amount_remaining   NUMBER := 0;
BEGIN
/*==============================================
   ||   Procedure Name  : APPS.GST_ABRL_AP_PREPAY_INV_PROC
   ||   Description          : ABRL Prepayment Open Invoices Report
   ||
   ||        Date                               Author                       Modification
   ||  ~~~~~~~~                     ~~~~~~~~~~~          ~~~~~~~~~~~~~~~~~
   ||      26-Sep-2016                  Lokesh                      New Development
================================================*/
   fnd_file.put_line
      (fnd_file.output,
       'INVOICE_TYPE^INVOICE_NUM^INVOICE_DATE^GL_DATE^INVOICE_ID^ACCOUNT^VOUCHER_NUM^VENDOR_NUM^VENDOR_NAME^VENDOR_TYPE^VENDOR_SITE_CODE^ORG_ID^OPERATING_UNIT^PREPAY_AMOUNT^PREPAY_APPLIED_AMOUNT^PREPAY_AMOUNT_REMAINING^STATUS'
      );

   FOR prepay_main IN
      (SELECT DISTINCT api.invoice_type_lookup_code invoice_type,
                       replace (api.invoice_num,'^','//') invoice_num, 
                       api.invoice_date, 
                       api.gl_date,
                       api.invoice_id, 
                       gl.concatenated_segments ACCOUNT,
                       api.doc_sequence_value voucher_num,
                       aps.segment1 vendor_num, aps.vendor_name,
                       aps.vendor_type_lookup_code vendor_type,
                       apsa.vendor_site_code vendor_site_name, api.org_id,
                       hr.NAME operating_unit,
                       api.invoice_amount prepay_amount,
                       nvl(b.Prepay_Applied_Amount,0) Prepay_Applied_Amount,
                       DECODE
                          (api.invoice_type_lookup_code,
                           'PREPAYMENT', ap_prepay_utils_pkg.get_prepay_amount_remaining
                                                               (api.invoice_id),
                           SUM (NVL (apps.amount_remaining, 0))
                          ) prepay_amount_remaining,                       
                       --       api.amount_paid,
                       ap_invoices_pkg.get_approval_status
                                         (api.invoice_id,
                                          api.invoice_amount,
                                          api.payment_status_flag,
                                          api.invoice_type_lookup_code
                                         ) status
                  FROM apps.ap_invoices_all api,
                       apps.ap_invoice_lines_all apil,
                       apps.ap_invoice_distributions_all apd,
                       ap_payment_schedules_all apps,
                       apps.ap_suppliers aps,
                       apps.gl_code_combinations_kfv gl,
                       apps.hr_operating_units hr,
                       apps.ap_supplier_sites_all apsa,
                       (SELECT   NVL (SUM (apd.amount), 0) amt_val,
                                 api.invoice_id
                            FROM apps.ap_invoices_all api,
                                 apps.ap_invoice_lines_all apil,
                                 apps.ap_invoice_distributions_all apd,
                                 apps.ap_suppliers aps,
                                 apps.ap_supplier_sites_all apsa
                           WHERE api.invoice_id = apd.invoice_id
                             AND apil.invoice_id = api.invoice_id
                             AND apil.line_number = apd.invoice_line_number
                             AND api.vendor_id = aps.vendor_id
                             AND aps.vendor_id = apsa.vendor_id
                             AND api.vendor_site_id = apsa.vendor_site_id
                             AND api.invoice_type_lookup_code = 'PREPAYMENT'
                        GROUP BY api.invoice_id) a,
                        (SELECT     (-1)* SUM ((ail.amount - NVL (ail.included_tax_amount, 0)))prepay_applied_amount,
                                                                                                                                                ai2.invoice_id
    FROM apps.ap_invoices_all ai,
         apps.ap_invoices_all ai2,
         apps.ap_invoice_lines_all ail,
         apps.ap_suppliers pv
   WHERE ai.invoice_id = ail.invoice_id
     AND ai2.invoice_id = ail.prepay_invoice_id
--AND   ail.amount                       < 0
     AND NVL (ail.discarded_flag, 'N') <> 'Y'
     AND ail.line_type_lookup_code = 'PREPAY'
     AND ai.vendor_id = pv.vendor_id
     AND ai.invoice_type_lookup_code NOT IN ('PREPAYMENT', 'CREDIT', 'DEBIT')
GROUP BY ai2.invoice_id)b      
                 WHERE 1 = 1
                   AND api.invoice_id = a.invoice_id
                   AND api.invoice_id=b.invoice_id(+)
                   AND api.invoice_id = apd.invoice_id
                   AND api.invoice_id = apps.invoice_id
                   AND api.org_id = hr.organization_id
                   AND gl.code_combination_id = apd.dist_code_combination_id
                   AND apil.invoice_id = api.invoice_id
                   AND apil.line_number = apd.invoice_line_number
                   AND apd.ROWID =
                          (SELECT ROWID
                             FROM apps.ap_invoice_distributions_all
                            WHERE 1 = 1
                              AND ROWNUM = 1
                              AND invoice_id = apd.invoice_id)
                   AND api.vendor_id = aps.vendor_id
                   AND aps.vendor_id = apsa.vendor_id
                   AND api.vendor_site_id = apsa.vendor_site_id
                   AND api.invoice_type_lookup_code = 'PREPAYMENT'
                   AND hr.NAME NOT LIKE 'TSRL%'
                   AND ap_invoices_pkg.get_approval_status
                                                 (api.invoice_id,
                                                  api.invoice_amount,
                                                  api.payment_status_flag,
                                                  api.invoice_type_lookup_code
                                                 ) IN
                                          ('APPROVED','UNAPPROVED','UNPAID', 'AVAILABLE')
                     -- and  api.gl_date between '01-aug-2016' and '10-aug-2016'
                   --   and aps.SEGMENT1='7500728'
                   AND TRUNC (api.gl_date)
                          BETWEEN TO_DATE (p_from_date,
                                           'YYYY/MM/DD HH24:MI:SS'
                                          )
                              AND TO_DATE (p_to_date, 'YYYY/MM/DD HH24:MI:SS')
                   AND api.org_id = NVL (p_org_id, api.org_id)
                   --    AND hr.NAME = NVL (p_operating_unit, hr.NAME)
                   AND aps.segment1 BETWEEN NVL (p_from_vendor_num,
                                                 aps.segment1
                                                )
                                        AND NVL (p_to_vendor_num,
                                                 aps.segment1)
                   --         AND apsa.vendor_site_code = NVL (p_vendor_site, apsa.vendor_site_code)
                   AND NVL (api.doc_sequence_value, '-9999999')
                          BETWEEN NVL (p_from_voucher,
                                       NVL (api.doc_sequence_value,
                                            '-9999999')
                                      )
                              AND NVL (p_to_voucher,
                                       NVL (api.doc_sequence_value,
                                            '-9999999')
                                      )
              GROUP BY api.invoice_num,
                       api.invoice_date,
                       api.invoice_id,
                       gl.concatenated_segments,
                       api.doc_sequence_value,
                       aps.segment1,
                       aps.vendor_name,
                       aps.vendor_type_lookup_code,
                       apsa.vendor_site_code,
                       api.org_id,
                       hr.NAME,
                       api.invoice_amount,
                       api.invoice_type_lookup_code,
                       api.gl_date,
                       api.payment_status_flag,
                       b.Prepay_Applied_Amount)
   LOOP
      v_prepay_amount := prepay_main.prepay_amount + v_prepay_amount;
      v_prepay_applied_amount:=prepay_main.prepay_applied_amount+v_prepay_applied_amount;
      v_prepay_amount_remaining :=prepay_main.prepay_amount_remaining + v_prepay_amount_remaining;
      fnd_file.put_line (fnd_file.output,
                            prepay_main.invoice_type
                         || '^'
                         || prepay_main.invoice_num
                         || '^'
                         || prepay_main.invoice_date
                         || '^'
                         || prepay_main.gl_date
                         || '^'
                         || prepay_main.invoice_id
                         || '^'
                         || prepay_main.ACCOUNT
                         || '^'
                         || prepay_main.voucher_num
                         || '^'
                         || prepay_main.vendor_num
                         || '^'
                         || prepay_main.vendor_name
                         || '^'
                         || prepay_main.vendor_type
                         || '^'
                         || prepay_main.vendor_site_name
                         || '^'
                         || prepay_main.org_id
                         || '^'
                         || prepay_main.operating_unit
                         || '^'
                         || prepay_main.prepay_amount
                         || '^'
                         || prepay_main.Prepay_Applied_Amount
                         ||'^'
                         || prepay_main.prepay_amount_remaining
                         || '^'
                         || prepay_main.status
                        );
   END LOOP;

   fnd_file.put_line (fnd_file.output, ' ');
   fnd_file.put_line (fnd_file.output,
                         'Total Prepay Amount'
                      || '^^^^^^^^^^^^^'
                      || v_prepay_amount
                     );
  fnd_file.put_line (fnd_file.output,
                         'Total Prepay Applied Amount'
                      || '^^^^^^^^^^^^^^'
                      || v_prepay_applied_amount
                     );
   fnd_file.put_line (fnd_file.output,
                         'Total Prepay Amount Remaining'
                      || '^^^^^^^^^^^^^^^'
                      || v_prepay_amount_remaining
                     );
EXCEPTION
   WHEN OTHERS
   THEN
      fnd_file.put_line (fnd_file.LOG,
                         SUBSTR ('Error ' || TO_CHAR (SQLCODE) || ': '
                                 || SQLERRM,
                                 1,
                                 255
                                )
                        );
END; 
/


CREATE OR REPLACE PROCEDURE APPS.XXABRL_MIS_RECO_DATA(errbuf out varchar2,
                                 retcode out varchar2,
                                 P_from_DATE IN  VARCHAR2,
                                  P_TO_DATE IN  VARCHAR2
                                 )
AS
V_FROM_DATE    VARCHAR2 (11):= TO_CHAR (TO_DATE (SUBSTR (p_from_date, 1, 10), 'RRRR/MM/DD'),'DD-MON-YYYY');
V_TO_DATE    VARCHAR2 (11):= TO_CHAR (TO_DATE (SUBSTR (p_TO_date, 1, 10), 'RRRR/MM/DD'),'DD-MON-YYYY');
V_L_FILE         VARCHAR2(200);
V_FINANCE_YEAR   NUMBER;
L_RUN_DATE    VARCHAR2(250);

CURSOR CUR_PERIOD_NAME1 IS
select aia.DOC_SEQUENCE_VALUE ,aia.AMOUNT_PAID,aia.INVOICE_TYPE_LOOKUP_CODE,aps.segment1,
aia.gl_date,aia.org_id,hr.short_code
   from apps.ap_invoices_all aia,apps.ap_suppliers aps,apps.hr_operating_units hr
   where aia.vendor_id=aps.vendor_id
   and hr.ORGANIZATION_ID=aia.org_id
   AND  TRUNC(AIA.CREATION_DATE) BETWEEN V_FROM_DATE AND V_TO_DATE;


x_id utl_file.file_type;
BEGIN



   SELECT 'ABRL_OFMIS_RECO_FILE'||'_'||'V'||'_'||'.txt'INTO V_L_FILE FROM dual;
  --SELECT 'XXABRL_HYPN_MKTNG_'||p_period_name||'_DATA'||NVL(TO_CHAR(TO_DATE(sysdate,'DD-MON-YYYY'),'DDMMYY'),TO_CHAR(SYSDATE-3,'DDMMYY'))||'.csv' INTO V_L_FILE FROM dual;
  x_id:=utl_file.fopen('VENDOR_INVOICE_PORTAL',V_L_FILE,'W');
utl_file.put_line(x_id,  'DOC_NUM'||'^'||
                              'Invoice Payment Amount'||'^'||
                              'INVOICE TYPE'||'^'||
                              'VENDOR NO'||'^'||
                              'GL DATE'||'^'||
                              'OU ID'||'^'||
                             'OPERATING UNIT'
                              );
  --utl_file.put_line(x_id,'VENDOR PAID INVOICES LIST DETAILS................................................->');

--utl_file.put_line(x_id,'VENDOR STANDARD INVOICES LIST........................................................->');
 FOR R IN CUR_PERIOD_NAME1 LOOP

 utl_file.put_line(x_id,   R.DOC_SEQUENCE_VALUE||'^'||
                              R.AMOUNT_PAID||'^'||
                              R.INVOICE_TYPE_LOOKUP_CODE||'^'||                           
                              R.segment1||'^'||
                              R.gl_date||'^'||
                              R.short_code|| CHR(13) || CHR(10)
                              );

  end loop;
  --utl_file.put_line(x_id,'cur2.................................->');

  

  EXCEPTION
 WHEN TOO_MANY_ROWS THEN
    dbms_output.PUT_LINE('Too Many Rows');
 WHEN NO_DATA_FOUND THEN
    dbms_output.PUT_LINE('No Data Found');
 WHEN utl_file.invalid_path THEN
    RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_path');
 WHEN utl_file.invalid_mode THEN
     RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_mode');
 WHEN utl_file.invalid_filehandle THEN
     RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_filehandle');
 WHEN utl_file.invalid_operation THEN
     RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_operation');
 WHEN utl_file.read_error THEN
     RAISE_APPLICATION_ERROR(-20001, 'utl_file.read_error');
 WHEN utl_file.write_error THEN
     RAISE_APPLICATION_ERROR(-20001, 'utl_file.write_error');
 WHEN utl_file.internal_error THEN
     RAISE_APPLICATION_ERROR(-20001, 'utl_file.internal_error');
 WHEN OTHERS THEN
      dbms_output.put_line(SubStr('Error '||TO_CHAR(SQLCODE)||': '||SQLERRM, 1,255));
utl_file.fclose(x_id);
END; 
/


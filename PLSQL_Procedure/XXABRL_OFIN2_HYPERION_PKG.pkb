CREATE OR REPLACE PACKAGE BODY APPS.XXABRL_OFIN2_HYPERION_PKG
 IS
 /****************************************************************************************************************
                 OBJECT INFORMATION

 Object Name : XXABRL_OFIN2_HYPERION_PKG

 Description : Procedure to get application wise Load files for Hyperion 

  Change Record:
 =================================================================================================================
 Version    Date            Author                  Remarks
 =======   ==========  =============                ==============================================================
 1.0.0     11-Nov-09   Praveen Kumar                Intial Version 
 1.0.1     28-Jan-10   Praveen Kumar                Table name changed from"GL_BALANCES" to "XXABRL_GL_BALANCES" 
 1.0.2     09-Feb-10   Praveen Kumar                Included OHDS-HM in Hyper Ops Load File     
*****************************************************************************************************************/
 
 PROCEDURE XXABRL_HYPN_DATA_CALLING(errbuf out varchar2,
                                     retcode out varchar2,
                                     p_period_name varchar2,
                                     p_version_no number,
                                     p_param_data varchar2)
  AS
  v_errbuf  VARCHAR2(100);
  v_retcode VARCHAR2(1);   
  v_count   NUMBER; 
  v_insert_flag CHAR(1); 
  v_insert_flag1 CHAR(1);    
  v_count1  NUMBER;
  v_result  BOOLEAN;                               
  BEGIN
    BEGIN
    
      BEGIN
        SELECT NVL(INSERT_FLAG,'N')
          INTO V_INSERT_FLAG
          FROM XXABRL_GL_BALANCES_TABLE  A
          WHERE A.PERIOD = P_PERIOD_NAME
           AND A.VERSION_NO = P_VERSION_NO;
           FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'Flag '||V_INSERT_FLAG);
           EXCEPTION WHEN OTHERS THEN
           V_INSERT_FLAG := 'N';
           FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'Flag is not available'); 
       END; 
         
       
          IF v_insert_flag = 'Y' THEN
          
              IF p_param_data = 'SUPER_OPS' THEN  
                XXABRL_OFIN2_HYPERION_PKG.XXABRL_HYPN_SUPER_OPS_DATA(V_errbuf,V_retcode,p_period_name,p_version_no,p_param_data);
               ELSIF p_param_data = 'HYPER_OPS' THEN
                XXABRL_OFIN2_HYPERION_PKG.XXABRL_HYPN_HYPER_OPS_DATA(V_errbuf,V_retcode,p_period_name,p_version_no,p_param_data);   
              ELSIF p_param_data = 'SCM_OPS' THEN
                XXABRL_OFIN2_HYPERION_PKG.XXABRL_HYPN_SCM_OPS_DATA(V_errbuf,V_retcode,p_period_name,p_version_no,p_param_data); 
              ELSIF p_param_data = 'SUPER_MCD' THEN
                XXABRL_OFIN2_HYPERION_PKG.XXABRL_HYPN_SUPER_MCD_DATA(V_errbuf,V_retcode,p_period_name,p_version_no,p_param_data); 
              ELSIF p_param_data = 'HYPER_MCD' THEN
                XXABRL_OFIN2_HYPERION_PKG.XXABRL_HYPN_HYPER_MCD_DATA(V_errbuf,V_retcode,p_period_name,p_version_no,p_param_data);
              ELSIF p_param_data = 'SCM_MCD' THEN
                XXABRL_OFIN2_HYPERION_PKG.XXABRL_HYPN_SCM_MCD_DATA(V_errbuf,V_retcode,p_period_name,p_version_no,p_param_data);
              ELSIF p_param_data = 'OHDS_MCD' THEN
                XXABRL_OFIN2_HYPERION_PKG.XXABRL_HYPN_OHDS_MCD_DATA(V_errbuf,V_retcode,p_period_name,p_version_no,p_param_data);
              ELSIF p_param_data = 'OHDS_OPS' THEN    
                XXABRL_OFIN2_HYPERION_PKG.XXABRL_HYPN_OHDS_OPS_DATA(V_errbuf,V_retcode,p_period_name,p_version_no,p_param_data);
              ELSIF p_param_data = 'MARKETING' THEN
                XXABRL_OFIN2_HYPERION_PKG.XXABRL_HYPN_MKTNG_DATA(V_errbuf,V_retcode,p_period_name,p_version_no,p_param_data);
              ELSIF p_param_data = 'CONSOL' THEN
                XXABRL_OFIN2_HYPERION_PKG.XXABRL_HYPN_CONSOL_DATA(V_errbuf,V_retcode,p_period_name,p_version_no,p_param_data);              
              ELSIF p_param_data = 'V_MERCH' THEN    
                XXABRL_OFIN2_HYPERION_PKG.XXABRL_HYPN_VMERCH_DATA(V_errbuf,V_retcode,p_period_name,p_version_no,p_param_data);                
              END IF;
        
          ELSIF v_insert_flag = 'N' THEN
           FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'Please Check the insert flag in the form');            
          END IF;
      END;
            
  END XXABRL_HYPN_DATA_CALLING;                              
 
 
 PROCEDURE XXABRL_HYPN_SUPER_OPS_DATA(errbuf out varchar2,
                                     retcode out varchar2,
                                     p_period_name varchar2,
                                     p_version_no number,
                                     p_param_data varchar2)
as
V_L_FILE         VARCHAR2(200);
V_FINANCE_YEAR   NUMBER;

/*-------------------------------------------------------------------------*/
  ------------Cursor for getting the Financial first period name------------
/*-------------------------------------------------------------------------*/
                 
  CURSOR CUR_PERIOD_NAME IS 
  SELECT  DISTINCT UPPER(a.period_name) PERIOD_NAME
    FROM  gl_period_statuses a, gl_period_statuses b
   WHERE  a.application_id = 101
     AND  b.application_id = 101
     AND  a.period_type = b.period_type
     AND  a.period_year = b.period_year
     AND  b.period_name = upper(p_period_name)
     AND  a.period_num =
          (SELECT min(c.period_num)
             FROM gl_period_statuses c
            WHERE c.application_id = 101               
              AND c.period_year = a.period_year
              AND c.period_type = a.period_type
         GROUP BY c.period_year);

/*==========================================================================*/
         ------------Cursor for getting Super Ops Data------------
/*==========================================================================*/
         
CURSOR CUR_SUPER_OPS(p_first_periond_name VARCHAR2) IS
SELECT   private_lable
        ,catchment
        ,fin_year
        ,scenario
        ,VERSION
        ,product
        ,entity
        ,view1
        ,period
        ,hyp_accnt
        ,SUM (DECODE (hyperion_accnt_type,
                      'Owners'' equity', -DATA,
                      'Revenue', -DATA,
                      'Liability', -DATA,
                      'Expense', DATA,
                      'Asset', DATA
                     )
             ) DATA-------------------SM Super Ops------------------
    FROM ((SELECT   'PL999' private_lable
                   ,'C9999' catchment
                   ,'FY' || SUBSTR (GLB.period_year, 3, 4) fin_year
                   ,'Actual' scenario
                   ,'V2000' VERSION
                   ,merch.hypn_member product
                   ,loc.super_member entity
                   ,'W0004' view1
                   ,SUBSTR (p_period_name, 1, 3) period
                   ,accnt.sm_ops hyp_accnt
                   ,accnt.hyperion_accnt_type
                   ,SUM (DECODE (glb.period_name,
                                 p_period_name, NVL (period_net_dr, 0)
                                  - NVL (period_net_cr, 0)
                                  + NVL (begin_balance_dr, 0)
                                  - NVL (begin_balance_cr, 0),0)
                        ) DATA
               FROM xxabrl_hyperion_accnt_mapping accnt,
                    gl_code_combinations glcc,
                    XXABRL_GL_BALANCES glb,
                    xxabrl_hyperion_loc_mapping loc,
                    xxabrl_hyperion_merch_mapping merch
              WHERE 1 = 1
                AND GLB.period_name IN (p_period_name, p_first_periond_name)
                AND GLB.code_combination_id = glcc.code_combination_id
                AND loc.ofin_loc_code = glcc.segment4
                AND accnt.ofin_account = glcc.segment6
                AND merch.ofin_member = glcc.segment5
                AND glcc.summary_flag = 'N'
                AND glcc.template_id IS NULL
                AND accnt.sm_ops <> 'N'
                AND accnt.sm_ops IS NOT NULL
                AND loc.super_member IS NOT NULL
                AND loc.entity_type = 'Supermarket'
                AND accnt.hyperion_accnt_type IN
                       ('Owners'' equity', 'Revenue', 'Liability', 'Expense',
                        'Asset')
           GROUP BY 'FY' || SUBSTR (GLB.period_year, 3, 4),
                    merch.hypn_member,
                    accnt.sm_ops,
                    accnt.hyperion_accnt_type,
                    loc.super_member)
          UNION ALL -------------------DC Super Ops------------------
          (SELECT   'PL999' private_lable
                   ,'C9999' catchment
                   ,'FY' || SUBSTR (GLB.period_year, 3, 4) fin_year
                   ,'Actual' scenario
                   ,'V2000' VERSION
                   ,merch.hypn_member product
                   ,loc.super_member entity
                   ,'W0004' view1
                   ,SUBSTR (p_period_name, 1, 3) period
                   ,accnt.dc_super_ops hyp_accnt
                   ,accnt.hyperion_accnt_type,
                    SUM (DECODE (GLB.period_name,
                                 p_period_name, NVL (period_net_dr, 0)
                                  - NVL (period_net_cr, 0)
                                  + NVL (begin_balance_dr, 0)
                                  - NVL (begin_balance_cr, 0),0)
                        ) DATA
               FROM xxabrl_hyperion_accnt_mapping accnt,
                    gl_code_combinations glcc,
                    XXABRL_GL_BALANCES GLB,
                    xxabrl_hyperion_loc_mapping loc,
                    xxabrl_hyperion_merch_mapping merch
              WHERE 1 = 1
                AND GLB.period_name IN (p_period_name, p_first_periond_name)
                AND GLB.code_combination_id = glcc.code_combination_id
                AND loc.ofin_loc_code = glcc.segment4
                AND accnt.ofin_account = glcc.segment6
                AND merch.ofin_member = glcc.segment5
                AND glcc.summary_flag = 'N'
                AND glcc.template_id IS NULL
                AND accnt.dc_super_ops <> 'N'
                AND accnt.dc_super_ops IS NOT NULL
                AND loc.super_member IS NOT NULL
                AND loc.entity_type = 'DC'
                AND accnt.hyperion_accnt_type IN
                       ('Owners'' equity', 'Revenue', 'Liability', 'Expense',
                        'Asset')
           GROUP BY 'FY' || SUBSTR (GLB.period_year, 3, 4),
                    merch.hypn_member,
                    accnt.dc_super_ops,
                    accnt.hyperion_accnt_type,
                    loc.super_member)
          UNION ALL-------------------RDC Super Ops------------------
          (SELECT   'PL999' private_lable
                   ,'C9999' catchment
                   ,'FY' || SUBSTR (GLB.period_year, 3, 4) fin_year
                   ,'Actual' scenario
                   ,'V2000' VERSION
                   , merch.hypn_member product
                   , loc.super_member entity
                   , 'W0004' view1
                   , SUBSTR (p_period_name, 1, 3) period
                   , accnt.rdc_super_ops hyp_accnt
                   , accnt.hyperion_accnt_type
                   , SUM (DECODE (GLB.period_name,
                                 p_period_name, NVL (period_net_dr, 0)
                                  - NVL (period_net_cr, 0)
                                  + NVL (begin_balance_dr, 0)
                                  - NVL (begin_balance_cr, 0),0)
                        ) DATA
               FROM xxabrl_hyperion_accnt_mapping accnt,
                    gl_code_combinations glcc,
                    XXABRL_GL_BALANCES GLB,
                    xxabrl_hyperion_loc_mapping loc,
                    xxabrl_hyperion_merch_mapping merch
              WHERE 1 = 1
                AND GLB.period_name IN (p_period_name, p_first_periond_name)
                AND GLB.code_combination_id = glcc.code_combination_id
                AND loc.ofin_loc_code = glcc.segment4
                AND accnt.ofin_account = glcc.segment6
                AND merch.ofin_member = glcc.segment5
                AND glcc.summary_flag = 'N'
                AND glcc.template_id IS NULL
                AND accnt.rdc_super_ops <> 'N'
                AND accnt.rdc_super_ops IS NOT NULL
                AND loc.super_member IS NOT NULL
                AND loc.entity_type = 'RDC'
                AND accnt.hyperion_accnt_type IN
                       ('Owners'' equity', 'Revenue', 'Liability', 'Expense',
                        'Asset')
           GROUP BY 'FY' || SUBSTR (GLB.period_year, 3, 4),
                    merch.hypn_member,
                    accnt.rdc_super_ops,
                    accnt.hyperion_accnt_type,
                    loc.super_member)
          UNION ALL-------------------RPC Super Ops------------------
          (SELECT   'PL999' private_lable
                   ,'C9999' catchment
                   ,'FY' || SUBSTR (GLB.period_year, 3, 4) fin_year
                   ,'Actual' scenario
                   ,'V2000' VERSION
                   , merch.hypn_member product
                   , loc.super_member entity
                   , 'W0004' view1
                   , SUBSTR (p_period_name, 1, 3) period
                   , accnt.rpc_super_ops hyp_accnt
                   , accnt.hyperion_accnt_type
                   , SUM (DECODE (GLB.period_name,
                                 p_period_name, NVL (period_net_dr, 0)
                                  - NVL (period_net_cr, 0)
                                  + NVL (begin_balance_dr, 0)
                                  - NVL (begin_balance_cr, 0),0)
                        ) DATA
               FROM xxabrl_hyperion_accnt_mapping accnt,
                    gl_code_combinations glcc,
                    XXABRL_GL_BALANCES GLB,
                    xxabrl_hyperion_loc_mapping loc,
                    xxabrl_hyperion_merch_mapping merch
              WHERE 1 = 1
                AND GLB.period_name IN (p_period_name, p_first_periond_name)
                AND GLB.code_combination_id = glcc.code_combination_id
                AND loc.ofin_loc_code = glcc.segment4
                AND accnt.ofin_account = glcc.segment6
                AND merch.ofin_member = glcc.segment5
                AND glcc.summary_flag = 'N'
                AND glcc.template_id IS NULL
                AND accnt.rpc_super_ops <> 'N'
                AND accnt.rpc_super_ops IS NOT NULL
                AND loc.super_member IS NOT NULL
                AND loc.entity_type = 'RPC'
                AND accnt.hyperion_accnt_type IN
                       ('Owners'' equity', 'Revenue', 'Liability', 'Expense',
                        'Asset')
           GROUP BY 'FY' || SUBSTR (GLB.period_year, 3, 4),
                    merch.hypn_member,
                    accnt.rpc_super_ops,
                    accnt.hyperion_accnt_type,
                    loc.super_member)
          UNION ALL-------------------CC Super Ops------------------
          (SELECT   'PL999' private_lable
                   ,'C9999' catchment
                   ,'FY' || SUBSTR (GLB.period_year, 3, 4) fin_year
                   ,'Actual' scenario
                   ,'V2000' VERSION
                   , merch.hypn_member product
                   , loc.super_member entity
                   ,'W0004' view1
                   , SUBSTR (p_period_name, 1, 3) period
                   , accnt.cc_super_ops hyp_accnt
                   , accnt.hyperion_accnt_type
                   , SUM (DECODE (GLB.period_name,
                                 p_period_name, NVL (period_net_dr, 0)
                                  - NVL (period_net_cr, 0)
                                  + NVL (begin_balance_dr, 0)
                                  - NVL (begin_balance_cr, 0),0)
                        ) DATA
               FROM xxabrl_hyperion_accnt_mapping accnt,
                    gl_code_combinations glcc,
                    XXABRL_GL_BALANCES GLB,
                    xxabrl_hyperion_loc_mapping loc,
                    xxabrl_hyperion_merch_mapping merch
              WHERE 1 = 1
                AND GLB.period_name IN (p_period_name, p_first_periond_name)
                AND GLB.code_combination_id = glcc.code_combination_id
                AND loc.ofin_loc_code = glcc.segment4
                AND accnt.ofin_account = glcc.segment6
                AND merch.ofin_member = glcc.segment5
                AND glcc.summary_flag = 'N'
                AND glcc.template_id IS NULL
                AND accnt.cc_super_ops <> 'N'
                AND accnt.cc_super_ops IS NOT NULL
                AND loc.super_member IS NOT NULL
                AND loc.entity_type = 'CC'
                AND accnt.hyperion_accnt_type IN
                       ('Owners'' equity', 'Revenue', 'Liability', 'Expense',
                        'Asset')
           GROUP BY 'FY' || SUBSTR (GLB.period_year, 3, 4),
                    merch.hypn_member,
                    accnt.cc_super_ops,
                    accnt.hyperion_accnt_type,
                    loc.super_member)
          UNION ALL-------------------FNV Super Ops------------------
          (SELECT   'PL999' private_lable
                   ,'C9999' catchment
                   ,'FY' || SUBSTR (GLB.period_year, 3, 4) fin_year
                   ,'Actual' scenario, 'V2000' VERSION,
                    merch.hypn_member product, loc.super_member entity,
                    'W0004' view1, SUBSTR (p_period_name, 1, 3) period,
                    accnt.fnv_super_ops hyp_accnt, accnt.hyperion_accnt_type,
                    SUM (DECODE (GLB.period_name,
                                 p_period_name, NVL (period_net_dr, 0)
                                  - NVL (period_net_cr, 0)
                                  + NVL (begin_balance_dr, 0)
                                  - NVL (begin_balance_cr, 0),0)
                        ) DATA
               FROM xxabrl_hyperion_accnt_mapping accnt,
                    gl_code_combinations glcc,
                    XXABRL_GL_BALANCES GLB,
                    xxabrl_hyperion_loc_mapping loc,
                    xxabrl_hyperion_merch_mapping merch
              WHERE 1 = 1
                AND GLB.period_name IN (p_period_name, p_first_periond_name)
                AND GLB.code_combination_id = glcc.code_combination_id
                AND loc.ofin_loc_code = glcc.segment4
                AND accnt.ofin_account = glcc.segment6
                AND merch.ofin_member = glcc.segment5
                AND glcc.summary_flag = 'N'
                AND glcc.template_id IS NULL
                AND accnt.fnv_super_ops <> 'N'
                AND accnt.fnv_super_ops IS NOT NULL
                AND loc.super_member IS NOT NULL
                AND loc.entity_type = 'FnV'
                AND accnt.hyperion_accnt_type IN
                       ('Owners'' equity', 'Revenue', 'Liability', 'Expense',
                        'Asset')
           GROUP BY 'FY' || SUBSTR (GLB.period_year, 3, 4),
                    merch.hypn_member,
                    accnt.fnv_super_ops,
                    accnt.hyperion_accnt_type,
                    loc.super_member)
          UNION ALL-------------------OFFICE Super Ops------------------
          (SELECT   'PL999' private_lable
                   ,'C9999' catchment
                   ,'FY' || SUBSTR (GLB.period_year, 3, 4) fin_year
                   ,'Actual' scenario
                   ,'V2000' VERSION
                   , merch.hypn_member product
                   , loc.super_member entity
                   ,'W0004' view1
                   , SUBSTR (p_period_name, 1, 3) period
                   , accnt.office_super_super_ops hyp_accnt
                   , accnt.hyperion_accnt_type
                   , SUM (DECODE (GLB.period_name,
                                 p_period_name, NVL (period_net_dr, 0)
                                  - NVL (period_net_cr, 0)
                                  + NVL (begin_balance_dr, 0)
                                  - NVL (begin_balance_cr, 0),0)
                        ) DATA
               FROM xxabrl_hyperion_accnt_mapping accnt,
                    gl_code_combinations glcc,
                    XXABRL_GL_BALANCES GLB,
                    xxabrl_hyperion_loc_mapping loc,
                    xxabrl_hyperion_merch_mapping merch
              WHERE 1 = 1
                AND GLB.period_name IN (p_period_name, p_first_periond_name)
                AND GLB.code_combination_id = glcc.code_combination_id
                AND loc.ofin_loc_code = glcc.segment4
                AND accnt.ofin_account = glcc.segment6
                AND merch.ofin_member = glcc.segment5
                AND glcc.summary_flag = 'N'
                AND glcc.template_id IS NULL
                AND accnt.office_super_super_ops <> 'N'
                AND accnt.office_super_super_ops IS NOT NULL
                AND loc.super_member IS NOT NULL
                AND loc.entity_type = 'OHDS-SM'
                AND accnt.hyperion_accnt_type IN
                       ('Owners'' equity', 'Revenue', 'Liability', 'Expense',
                        'Asset')
           GROUP BY 'FY' || SUBSTR (GLB.period_year, 3, 4),
                    merch.hypn_member,
                    accnt.office_super_super_ops,
                    accnt.hyperion_accnt_type,
                    loc.super_member))
   WHERE DATA <> 0
GROUP BY (private_lable,
          catchment,
          fin_year,
          scenario,
          VERSION,
          product,
          entity,
          view1,
          period,
          hyp_accnt
         );
          
     x_id utl_file.file_type;
     
 BEGIN
    
    BEGIN
    SELECT DISTINCT SUBSTR (glb.period_year, 3, 4)
      INTO v_finance_year 
      FROM XXABRL_GL_BALANCES glb 
     WHERE period_name = p_period_name;
     EXCEPTION
     WHEN OTHERS THEN 
     v_finance_year:= NULL;
    END;
     
    FOR i IN CUR_PERIOD_NAME LOOP
    BEGIN
    XXABRL_OFIN2_HYPERION_PKG.XXABRL_HYPN_DATA_VALIDAION(p_period_name); 
          SELECT 'SUPER_OPS_FY'||v_finance_year||'_'||substr(p_period_name,1,3)||'_'||'V'||p_version_no||'.csv'INTO V_L_FILE FROM dual;
          --SELECT 'XXABRL_HYPN_SUPER_OPS_'||p_period_name||'_DATA'||NVL(TO_CHAR(TO_DATE(sysdate,'DD-MON-YYYY'),'DDMMYY'),TO_CHAR(SYSDATE-3,'DDMMYY'))||'.csv' INTO V_L_FILE FROM dual; 
     x_id:=utl_file.fopen('/ebiz/u01/PROD/db/tech_st/10.2.0/appsutil/outbound/PROD_msoradbprod',V_L_FILE,'W');          
       FOR j IN CUR_SUPER_OPS(i.period_name) LOOP
       utl_file.put_line(x_id,j.private_lable||','||
                                  j.catchment||','||
                                  j.fin_year||','||
                                  j.scenario||','||
                                  j.VERSION||','||
                                  j.product||','||
                                  j.entity||','||
                                  j.view1||','||
                                  j.period||','||
                                  j.hyp_accnt||','||
                                  j.DATA);        
        END LOOP;
    EXCEPTION 
    WHEN TOO_MANY_ROWS THEN
        dbms_output.PUT_LINE('Too Many Rows');    
    WHEN NO_DATA_FOUND THEN
        dbms_output.PUT_LINE('No Data Found');    
    WHEN utl_file.invalid_path THEN
        RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_path');
    WHEN utl_file.invalid_mode THEN
         RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_mode');
    WHEN utl_file.invalid_filehandle THEN
         RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_filehandle');
    WHEN utl_file.invalid_operation THEN
         RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_operation');
    WHEN utl_file.read_error THEN
         RAISE_APPLICATION_ERROR(-20001, 'utl_file.read_error');
    WHEN utl_file.write_error THEN
         RAISE_APPLICATION_ERROR(-20001, 'utl_file.write_error');
    WHEN utl_file.internal_error THEN
         RAISE_APPLICATION_ERROR(-20001, 'utl_file.internal_error');
    WHEN OTHERS THEN
          dbms_output.put_line(SubStr('Error '||TO_CHAR(SQLCODE)||': '||SQLERRM, 1,255));
     END;    
   END LOOP;
   
utl_file.fclose(x_id);
end XXABRL_HYPN_SUPER_OPS_DATA;



PROCEDURE XXABRL_HYPN_HYPER_OPS_DATA(errbuf out varchar2,
                                     retcode out varchar2,
                                     p_period_name varchar2,
                                     p_version_no number,
                                     p_param_data varchar2)as
V_L_FILE         VARCHAR2(200);
V_FINANCE_YEAR   NUMBER;
/*-------------------------------------------------------------------------*/
  ------------Cursor for getting the Financial first period name------------
/*-------------------------------------------------------------------------*/
                 
  CURSOR CUR_PERIOD_NAME IS 
  SELECT  DISTINCT UPPER(a.period_name) PERIOD_NAME
    FROM  gl_period_statuses a, gl_period_statuses b
   WHERE  a.application_id = 101
     AND  b.application_id = 101
     AND  a.period_type = b.period_type
     AND  a.period_year = b.period_year
     AND  b.period_name = upper(p_period_name)
     AND  a.period_num =
          (SELECT min(c.period_num)
             FROM gl_period_statuses c
            WHERE c.application_id = 101               
              AND c.period_year = a.period_year
              AND c.period_type = a.period_type
         GROUP BY c.period_year);
       
    
/*-------------------------------------------------------------------------*/
  ------------Cursor for getting the HYPER OPS DATA name------------
/*-------------------------------------------------------------------------*/        
         
CURSOR CUR_HYPER_OPS(p_first_periond_name VARCHAR2) IS
SELECT   private_lable, fin_year, scenario, VERSION, product, entity, view1,
         period, hyp_accnt,
         SUM (DECODE (hyperion_accnt_type,
                      'Owners'' equity', -DATA,
                      'Revenue', -DATA,
                      'Liability', -DATA,
                      'Expense', DATA,
                      'Asset', DATA
                     )
             ) DATA
    FROM ((SELECT   'PL999' private_lable,
                    'FY' || SUBSTR (GLB.period_year, 3, 4) fin_year,
                    'Actual' scenario, 
                    'V2000' VERSION,
                    merch.hypn_member product, 
                    loc.hyper_member entity,
                    'W0004' view1, 
                    accnt.hm_ops hyp_accnt,
                    accnt.hyperion_accnt_type,
                    SUBSTR (p_period_name, 1, 3) period,
                    SUM (DECODE (GLB.period_name,
                                 p_period_name, NVL (period_net_dr, 0)
                                  - NVL (period_net_cr, 0)
                                  + NVL (begin_balance_dr, 0)
                                  - NVL (begin_balance_cr, 0),
                                 0
                                )
                        ) DATA
               FROM apps.xxabrl_hyperion_accnt_mapping accnt,
                    apps.gl_code_combinations glcc,
                    apps.XXABRL_GL_BALANCES GLB,
                    apps.xxabrl_hyperion_loc_mapping loc,
                    apps.xxabrl_hyperion_merch_mapping merch
              WHERE 1 = 1
                AND GLB.period_name IN (p_period_name, p_first_periond_name)
                AND GLB.code_combination_id = glcc.code_combination_id
                AND loc.ofin_loc_code = glcc.segment4
                AND accnt.ofin_account = glcc.segment6
                AND merch.ofin_member = glcc.segment5
                AND glcc.summary_flag = 'N'
                AND glcc.template_id IS NULL
                AND accnt.hm_ops <> 'N'
                AND accnt.hm_ops IS NOT NULL
                AND loc.hyper_member IS NOT NULL
                AND loc.entity_type = 'Hypermarket'
                AND accnt.hyperion_accnt_type IN
                       ('Owners'' equity', 'Revenue', 'Liability', 'Expense',
                        'Asset')
           GROUP BY 'FY' || SUBSTR (GLB.period_year, 3, 4),
                    merch.hypn_member,
                    accnt.hm_ops,
                    accnt.hyperion_accnt_type,
                    loc.hyper_member
      UNION ALL
           SELECT   'PL999' private_lable,
                    'FY' || SUBSTR (GLB.period_year, 3, 4) fin_year,
                    'Actual' scenario, 
                    'V2000' VERSION,
                    merch.hypn_member product, 
                    loc.hyper_member entity,
                    'W0004' view1, 
                    accnt.hm_ops hyp_accnt,
                    accnt.hyperion_accnt_type,
                    SUBSTR (p_period_name, 1, 3) period,
                    SUM (DECODE (GLB.period_name,
                                 p_period_name, NVL (period_net_dr, 0)
                                  - NVL (period_net_cr, 0)
                                  + NVL (begin_balance_dr, 0)
                                  - NVL (begin_balance_cr, 0),
                                 0
                                )
                        ) DATA
               FROM apps.xxabrl_hyperion_accnt_mapping accnt,
                    apps.gl_code_combinations glcc,
                    apps.XXABRL_GL_BALANCES GLB,
                    apps.xxabrl_hyperion_loc_mapping loc,
                    apps.xxabrl_hyperion_merch_mapping merch
              WHERE 1 = 1
                AND GLB.period_name IN (p_period_name, p_first_periond_name)
                AND GLB.code_combination_id = glcc.code_combination_id
                AND loc.ofin_loc_code = glcc.segment4
                AND accnt.ofin_account = glcc.segment6
                AND merch.ofin_member = glcc.segment5
                AND glcc.summary_flag = 'N'
                AND glcc.template_id IS NULL                
                AND accnt.office_hyper_hyper_ops <> 'N'
                AND accnt.office_hyper_hyper_ops IS NOT NULL 
                AND loc.hyper_member IS NOT NULL
                AND loc.entity_type = 'OHDS-HM'
                AND accnt.hyperion_accnt_type IN
                       ('Owners'' equity', 'Revenue', 'Liability', 'Expense',
                        'Asset')
           GROUP BY 'FY' || SUBSTR (GLB.period_year, 3, 4),
                    merch.hypn_member,
                    accnt.hm_ops,
                    accnt.hyperion_accnt_type,
                    loc.hyper_member))
   WHERE DATA <> 0
GROUP BY (private_lable,
          fin_year,
          scenario,
          VERSION,
          product,
          entity,
          view1,
          period,
          hyp_accnt
         );   
         
 x_id utl_file.file_type;
 --V_L_FILE VARCHAR2(200);
     
 BEGIN    
    
    BEGIN
    SELECT DISTINCT SUBSTR (glb.period_year, 3, 4)
      INTO v_finance_year 
      FROM XXABRL_GL_BALANCES glb 
     WHERE period_name = p_period_name;
     EXCEPTION
     WHEN OTHERS THEN 
     v_finance_year:= NULL;
    END;
   
    FOR i IN CUR_PERIOD_NAME LOOP 
    BEGIN  
    XXABRL_OFIN2_HYPERION_PKG.XXABRL_HYPN_DATA_VALIDAION(p_period_name);
         SELECT 'HYPER_OPS_FY'||v_finance_year||'_'||substr(p_period_name,1,3)||'_'||'V'||p_version_no||'.csv'INTO V_L_FILE FROM dual;         
      --SELECT 'XXABRL_HYPN_HYPER_OPS_'||p_period_name||'_DATA'||NVL(TO_CHAR(TO_DATE(sysdate,'DD-MON-YYYY'),'DDMMYY'),TO_CHAR(SYSDATE-3,'DDMMYY'))||'.csv' INTO V_L_FILE FROM dual;        
      x_id:=utl_file.fopen('/ebiz/u01/PROD/db/tech_st/10.2.0/appsutil/outbound/PROD_msoradbprod',V_L_FILE,'W');      
       FOR k IN CUR_HYPER_OPS(i.period_name) LOOP
        utl_file.put_line(x_id,k.private_lable||','||            
                                  k.fin_year||','||
                                  k.scenario||','||
                                  k.VERSION||','||
                                  k.product||','||
                                  k.entity||','||
                                  k.view1||','||
                                  k.period||','||
                                  k.hyp_accnt||','||
                                  k.DATA);

        END LOOP;  
        EXCEPTION 
     WHEN TOO_MANY_ROWS THEN
        dbms_output.PUT_LINE('Too Many Rows');    
     WHEN NO_DATA_FOUND THEN
        dbms_output.PUT_LINE('No Data Found');    
     WHEN utl_file.invalid_path THEN
        RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_path');
     WHEN utl_file.invalid_mode THEN
         RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_mode');
     WHEN utl_file.invalid_filehandle THEN
         RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_filehandle');
     WHEN utl_file.invalid_operation THEN
         RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_operation');
     WHEN utl_file.read_error THEN
         RAISE_APPLICATION_ERROR(-20001, 'utl_file.read_error');
     WHEN utl_file.write_error THEN
         RAISE_APPLICATION_ERROR(-20001, 'utl_file.write_error');
     WHEN utl_file.internal_error THEN
         RAISE_APPLICATION_ERROR(-20001, 'utl_file.internal_error');
     WHEN OTHERS THEN
          dbms_output.put_line(SubStr('Error '||TO_CHAR(SQLCODE)||': '||SQLERRM, 1,255));
     END;        
  END LOOP;        
  utl_file.fclose(x_id);
END XXABRL_HYPN_HYPER_OPS_DATA;
                                          

PROCEDURE XXABRL_HYPN_SCM_OPS_DATA(errbuf out varchar2,
                                     retcode out varchar2,
                                     p_period_name varchar2,
                                     p_version_no number,
                                     p_param_data varchar2)
AS
V_L_FILE         VARCHAR2(200);
V_FINANCE_YEAR   NUMBER;
  
/*-------------------------------------------------------------------------*/
  ------------Cursor for getting the Financial first period name------------
/*-------------------------------------------------------------------------*/
                 
  CURSOR CUR_PERIOD_NAME IS 
  SELECT  DISTINCT UPPER(a.period_name) PERIOD_NAME
    FROM  gl_period_statuses a, gl_period_statuses b
   WHERE  a.application_id = 101
     AND  b.application_id = 101
     AND  a.period_type = b.period_type
     AND  a.period_year = b.period_year
     AND  b.period_name = upper(p_period_name)
     AND  a.period_num =
          (SELECT min(c.period_num)
             FROM gl_period_statuses c
            WHERE c.application_id = 101               
              AND c.period_year = a.period_year
              AND c.period_type = a.period_type
         GROUP BY c.period_year);
/*-------------------------------------------------------------------------*/
  ------------Cursor for getting the SCM OPS DATA name------------
/*-------------------------------------------------------------------------*/        
         
CURSOR CUR_SCM_OPS(p_first_periond_name VARCHAR2) IS
SELECT   fin_year, scenario, VERSION, entity, view1, period, ACCOUNT,
         SUM (DECODE (hyperion_accnt_type,
                      'Owners'' equity', -DATA,
                      'Revenue', -DATA,
                      'Liability', -DATA,
                      'Expense', DATA,
                      'Asset', DATA
                     )
             ) DATA ------------Cursor for getting the SCM DC ops--------------
    FROM ((SELECT   'FY' || SUBSTR (GLB.period_year, 3, 4) fin_year,
                    'Actual' scenario, 'V2000' VERSION, loc.scm_member entity,
                    'W0004' view1, SUBSTR (p_period_name, 1, 3) period,
                    accnt.dc_scm_ops ACCOUNT, accnt.hyperion_accnt_type,
                    SUM (DECODE (GLB.period_name,
                                 p_period_name, NVL (period_net_dr, 0)
                                  - NVL (period_net_cr, 0)
                                  + NVL (begin_balance_dr, 0)
                                  - NVL (begin_balance_cr, 0),
                                 0
                                )
                        ) DATA
               FROM xxabrl_hyperion_accnt_mapping accnt,
                    gl_code_combinations glcc,
                    XXABRL_GL_BALANCES GLB,
                    xxabrl_hyperion_loc_mapping loc
              WHERE 1 = 1
                AND GLB.period_name IN (p_period_name, p_first_periond_name)
                AND GLB.code_combination_id = glcc.code_combination_id
                AND loc.ofin_loc_code = glcc.segment4
                AND accnt.ofin_account = glcc.segment6
                AND glcc.summary_flag = 'N'
                AND glcc.template_id IS NULL
                AND accnt.dc_scm_ops <> 'N'
                AND accnt.dc_scm_ops IS NOT NULL
                AND loc.scm_member IS NOT NULL
                AND loc.entity_type = 'DC'
                AND accnt.hyperion_accnt_type IN
                       ('Owners'' equity', 'Revenue', 'Liability', 'Expense',
                        'Asset')
           GROUP BY 'FY' || SUBSTR (GLB.period_year, 3, 4),
                    accnt.dc_scm_ops,
                    accnt.hyperion_accnt_type,
                    loc.scm_member)
          UNION ALL------------Cursor for getting the SCM RDC ops--------------
          (SELECT   'FY' || SUBSTR (GLB.period_year, 3, 4) fin_year,
                    'Actual' scenario, 'V2000' VERSION, loc.scm_member entity,
                    'W0004' view1, SUBSTR (p_period_name, 1, 3) period,
                    accnt.rdc_scm_ops ACCOUNT, accnt.hyperion_accnt_type,
                    SUM (DECODE (GLB.period_name,
                                 p_period_name, NVL (period_net_dr, 0)
                                  - NVL (period_net_cr, 0)
                                  + NVL (begin_balance_dr, 0)
                                  - NVL (begin_balance_cr, 0),
                                 0
                                )
                        ) DATA
               FROM xxabrl_hyperion_accnt_mapping accnt,
                    gl_code_combinations glcc,
                    XXABRL_GL_BALANCES GLB,
                    xxabrl_hyperion_loc_mapping loc
              WHERE 1 = 1
                AND GLB.period_name IN (p_period_name, p_first_periond_name)
                AND GLB.code_combination_id = glcc.code_combination_id
                AND loc.ofin_loc_code = glcc.segment4
                AND accnt.ofin_account = glcc.segment6
                AND glcc.summary_flag = 'N'
                AND glcc.template_id IS NULL
                AND accnt.rdc_scm_ops <> 'N'
                AND accnt.rdc_scm_ops IS NOT NULL
                AND loc.scm_member IS NOT NULL
                AND loc.entity_type = 'RDC'
                AND accnt.hyperion_accnt_type IN
                       ('Owners'' equity', 'Revenue', 'Liability', 'Expense',
                        'Asset')
           GROUP BY 'FY' || SUBSTR (GLB.period_year, 3, 4),
                    accnt.rdc_scm_ops,
                    accnt.hyperion_accnt_type,
                    loc.scm_member)
          UNION ALL----------------Cursor for getting the SCM RPC ops------------
          (SELECT   'FY' || SUBSTR (GLB.period_year, 3, 4) fin_year,
                    'Actual' scenario, 'V2000' VERSION, loc.scm_member entity,
                    'W0004' view1, SUBSTR (p_period_name, 1, 3) period,
                    accnt.rpc_scm_ops ACCOUNT, accnt.hyperion_accnt_type,
                    SUM (DECODE (GLB.period_name,
                                 p_period_name, NVL (period_net_dr, 0)
                                  - NVL (period_net_cr, 0)
                                  + NVL (begin_balance_dr, 0)
                                  - NVL (begin_balance_cr, 0),
                                 0
                                )
                        ) DATA
               FROM xxabrl_hyperion_accnt_mapping accnt,
                    gl_code_combinations glcc,
                    XXABRL_GL_BALANCES GLB,
                    xxabrl_hyperion_loc_mapping loc
              WHERE 1 = 1
                AND GLB.period_name IN (p_period_name, p_first_periond_name)
                AND GLB.code_combination_id = glcc.code_combination_id
                AND loc.ofin_loc_code = glcc.segment4
                AND accnt.ofin_account = glcc.segment6
                AND glcc.summary_flag = 'N'
                AND glcc.template_id IS NULL
                AND accnt.rpc_scm_ops <> 'N'
                AND accnt.rpc_scm_ops IS NOT NULL
                AND loc.scm_member IS NOT NULL
                AND loc.entity_type = 'RPC'
                AND accnt.hyperion_accnt_type IN
                       ('Owners'' equity', 'Revenue', 'Liability', 'Expense',
                        'Asset')
           GROUP BY 'FY' || SUBSTR (GLB.period_year, 3, 4),
                    accnt.rpc_scm_ops,
                    accnt.hyperion_accnt_type,
                    loc.scm_member)
          UNION ALL---------------Cursor for getting the SCM CC ops-------------
            (SELECT   'FY' || SUBSTR (GLB.period_year, 3, 4) fin_year,
                    'Actual' scenario, 'V2000' VERSION, loc.scm_member entity,
                    'W0004' view1, SUBSTR (p_period_name, 1, 3) period,
                    accnt.cc_scm_ops ACCOUNT, accnt.hyperion_accnt_type,
                    SUM (DECODE (GLB.period_name,
                                 p_period_name, NVL (period_net_dr, 0)
                                  - NVL (period_net_cr, 0)
                                  + NVL (begin_balance_dr, 0)
                                  - NVL (begin_balance_cr, 0),
                                 0
                                )
                        ) DATA
               FROM xxabrl_hyperion_accnt_mapping accnt,
                    gl_code_combinations glcc,
                    XXABRL_GL_BALANCES GLB,
                    xxabrl_hyperion_loc_mapping loc
              WHERE 1 = 1
                AND GLB.period_name IN (p_period_name, p_first_periond_name)
                AND GLB.code_combination_id = glcc.code_combination_id
                AND loc.ofin_loc_code = glcc.segment4
                AND accnt.ofin_account = glcc.segment6
                AND glcc.summary_flag = 'N'
                AND glcc.template_id IS NULL
                AND accnt.cc_scm_ops <> 'N'
                AND accnt.cc_scm_ops IS NOT NULL
                AND loc.scm_member IS NOT NULL
                AND loc.entity_type = 'CC'
                AND accnt.hyperion_accnt_type IN
                       ('Owners'' equity', 'Revenue', 'Liability', 'Expense',
                        'Asset')
           GROUP BY 'FY' || SUBSTR (GLB.period_year, 3, 4),
                    accnt.cc_scm_ops,
                    accnt.hyperion_accnt_type,
                    loc.scm_member)
          UNION ALL------------------Cursor for getting the SCM FNV ops---------
          (SELECT   'FY' || SUBSTR (GLB.period_year, 3, 4) fin_year,
                    'Actual' scenario, 'V2000' VERSION, loc.scm_member entity,
                    'W0004' view1, SUBSTR (p_period_name, 1, 3) period,
                    accnt.fnv_scm_ops ACCOUNT, accnt.hyperion_accnt_type,
                    SUM (DECODE (GLB.period_name,
                                 p_period_name, NVL (period_net_dr, 0)
                                  - NVL (period_net_cr, 0)
                                  + NVL (begin_balance_dr, 0)
                                  - NVL (begin_balance_cr, 0),
                                 0
                                )
                        ) DATA
               FROM xxabrl_hyperion_accnt_mapping accnt,
                    gl_code_combinations glcc,
                    XXABRL_GL_BALANCES GLB,
                    xxabrl_hyperion_loc_mapping loc
              WHERE 1 = 1
                AND GLB.period_name IN (p_period_name, p_first_periond_name)
                AND GLB.code_combination_id = glcc.code_combination_id
                AND loc.ofin_loc_code = glcc.segment4
                AND accnt.ofin_account = glcc.segment6
                AND glcc.summary_flag = 'N'
                AND glcc.template_id IS NULL
                AND accnt.fnv_scm_ops <> 'N'
                AND accnt.fnv_scm_ops IS NOT NULL
                AND loc.scm_member IS NOT NULL
                AND loc.entity_type = 'FnV'
                AND accnt.hyperion_accnt_type IN
                       ('Owners'' equity', 'Revenue', 'Liability', 'Expense',
                        'Asset')
           GROUP BY 'FY' || SUBSTR (GLB.period_year, 3, 4),
                    accnt.fnv_scm_ops,
                    accnt.hyperion_accnt_type,
                    loc.scm_member))
   WHERE DATA <> 0
GROUP BY (fin_year, scenario, VERSION, entity, view1, period, ACCOUNT);
         
 x_id utl_file.file_type;
     
 BEGIN   
   BEGIN
    SELECT DISTINCT SUBSTR (glb.period_year, 3, 4)
      INTO v_finance_year 
      FROM XXABRL_GL_BALANCES glb 
     WHERE period_name = p_period_name;
     EXCEPTION
     WHEN OTHERS THEN 
     v_finance_year:= NULL;
    END;  
    
    FOR i IN CUR_PERIOD_NAME LOOP
    BEGIN 
    XXABRL_OFIN2_HYPERION_PKG.XXABRL_HYPN_DATA_VALIDAION(p_period_name); 
         SELECT 'SCM_OPS_FY'||v_finance_year||'_'||substr(p_period_name,1,3)||'_'||'V'||p_version_no||'.csv'INTO V_L_FILE FROM dual;       
      --SELECT 'XXABRL_HYPN_SCM_OPS_'||p_period_name||'_DATA'||NVL(TO_CHAR(TO_DATE(sysdate,'DD-MON-YYYY'),'DDMMYY'),TO_CHAR(SYSDATE-3,'DDMMYY'))||'.csv' INTO V_L_FILE FROM dual;
      x_id:=utl_file.fopen('/ebiz/u01/PROD/db/tech_st/10.2.0/appsutil/outbound/PROD_msoradbprod',V_L_FILE,'W');      
      FOR l IN CUR_SCM_OPS(i.period_name) LOOP
        utl_file.put_line(x_id,   l.fin_year||','||
                                  l.scenario||','||
                                  l.VERSION||','||                                  
                                  l.entity||','||                                  
                                  l.view1||','||
                                  l.period||','||
                                  l.account||','||                                  
                                  l.DATA);

        END LOOP;
    EXCEPTION 
     WHEN TOO_MANY_ROWS THEN
        dbms_output.PUT_LINE('Too Many Rows');    
     WHEN NO_DATA_FOUND THEN
        dbms_output.PUT_LINE('No Data Found');    
     WHEN utl_file.invalid_path THEN
        RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_path');
     WHEN utl_file.invalid_mode THEN
         RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_mode');
     WHEN utl_file.invalid_filehandle THEN
         RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_filehandle');
     WHEN utl_file.invalid_operation THEN
         RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_operation');
     WHEN utl_file.read_error THEN
         RAISE_APPLICATION_ERROR(-20001, 'utl_file.read_error');
     WHEN utl_file.write_error THEN
         RAISE_APPLICATION_ERROR(-20001, 'utl_file.write_error');
     WHEN utl_file.internal_error THEN
         RAISE_APPLICATION_ERROR(-20001, 'utl_file.internal_error');
     WHEN OTHERS THEN
          dbms_output.put_line(SubStr('Error '||TO_CHAR(SQLCODE)||': '||SQLERRM, 1,255));
    END;        
  END LOOP;        
  utl_file.fclose(x_id);
END XXABRL_HYPN_SCM_OPS_DATA;


PROCEDURE XXABRL_HYPN_SUPER_MCD_DATA(errbuf out varchar2,
                                     retcode out varchar2,
                                     p_period_name varchar2,
                                     p_version_no number,
                                     p_param_data varchar2)  
AS
V_L_FILE         VARCHAR2(200);
V_FINANCE_YEAR   NUMBER;
/*-------------------------------------------------------------------------*/
  ------------Cursor for getting the Financial first period name------------
/*-------------------------------------------------------------------------*/
                 
  CURSOR CUR_PERIOD_NAME IS 
  SELECT  DISTINCT UPPER(a.period_name) PERIOD_NAME
    FROM  gl_period_statuses a, gl_period_statuses b
   WHERE  a.application_id = 101
     AND  b.application_id = 101
     AND  a.period_type = b.period_type
     AND  a.period_year = b.period_year
     AND  b.period_name = upper(p_period_name)
     AND  a.period_num =
          (SELECT min(c.period_num)
             FROM gl_period_statuses c
            WHERE c.application_id = 101               
              AND c.period_year = a.period_year
              AND c.period_type = a.period_type
         GROUP BY c.period_year);
/*-------------------------------------------------------------------------*/
  ------------Cursor for getting the SCM OPS DATA name------------
/*-------------------------------------------------------------------------*/        
         
CURSOR CUR_SUPER_MCD(p_first_periond_name VARCHAR2) IS
SELECT   store_types, fin_year, scenario, VERSION, fa_employee, entity, view1,
         period, hyp_accnt,
         SUM (DECODE (hyperion_accnt_type,
                      'Owners'' equity', -DATA,
                      'Revenue', -DATA,
                      'Liability', -DATA,
                      'Expense', DATA,
                      'Asset', DATA
                     )
             ) DATA
    FROM ((SELECT   'ST999' store_types,
                    'FY' || SUBSTR (GLB.period_year, 3, 4) fin_year,
                    'Actual' scenario, 'V2000' VERSION,
                    fa_emp.sm_fa_emp fa_employee, loc.super_member entity,
                    'W0004' view1, SUBSTR (p_period_name, 1, 3) period,
                    accnt.sm_mcd hyp_accnt, accnt.hyperion_accnt_type,
                    SUM (DECODE (GLB.period_name,
                                 p_period_name, NVL (period_net_dr, 0)
                                  - NVL (period_net_cr, 0)
                                  + NVL (begin_balance_dr, 0)
                                  - NVL (begin_balance_cr, 0),
                                 0
                                )
                        ) DATA
               FROM xxabrl_hyperion_accnt_mapping accnt,
                    gl_code_combinations glcc,
                    XXABRL_GL_BALANCES GLB,
                    xxabrl_hyperion_loc_mapping loc,
                    xxabrl_hypn_gl_fa_emp_mapping fa_emp
              WHERE 1 = 1
                AND GLB.period_name IN (p_period_name, p_first_periond_name)
                AND GLB.code_combination_id = glcc.code_combination_id
                AND loc.ofin_loc_code = glcc.segment4
                AND accnt.ofin_account = glcc.segment6
                AND accnt.ofin_account = fa_emp.ofin_account
                AND accnt.sm_mcd = fa_emp.sm_mcd
                AND glcc.summary_flag = 'N'
                AND glcc.template_id IS NULL
                AND accnt.sm_mcd <> 'N'
                AND accnt.sm_mcd IS NOT NULL
                AND loc.super_member IS NOT NULL
                AND loc.entity_type = 'Supermarket'
                AND accnt.hyperion_accnt_type IN
                       ('Owners'' equity', 'Revenue', 'Liability', 'Expense',
                        'Asset')
           GROUP BY 'FY' || SUBSTR (GLB.period_year, 3, 4),
                    sm_fa_emp,
                    accnt.sm_mcd,
                    accnt.hyperion_accnt_type,
                    loc.super_member))
   WHERE DATA <> 0
GROUP BY store_types,fin_year,scenario,VERSION,fa_employee,
         entity,view1,period,hyp_accnt;
    
 x_id utl_file.file_type;
     
 BEGIN  
  BEGIN
    SELECT DISTINCT SUBSTR (glb.period_year, 3, 4)
      INTO v_finance_year 
      FROM XXABRL_GL_BALANCES glb 
     WHERE period_name = p_period_name;
     EXCEPTION
     WHEN OTHERS THEN 
     v_finance_year:= NULL;
    END;
     
    FOR i IN CUR_PERIOD_NAME LOOP
    BEGIN 
    XXABRL_OFIN2_HYPERION_PKG.XXABRL_HYPN_DATA_VALIDAION(p_period_name);  
        SELECT 'SUPER_MCD_FY'||v_finance_year||'_'||substr(p_period_name,1,3)||'_'||'V'||p_version_no||'.csv'INTO V_L_FILE FROM dual;     
      --SELECT 'XXABRL_HYPN_SUPER_MCD_'||p_period_name||'_DATA'||NVL(TO_CHAR(TO_DATE(sysdate,'DD-MON-YYYY'),'DDMMYY'),TO_CHAR(SYSDATE-3,'DDMMYY'))||'.csv' INTO V_L_FILE FROM dual;
      x_id:=utl_file.fopen('/ebiz/u01/PROD/db/tech_st/10.2.0/appsutil/outbound/PROD_msoradbprod',V_L_FILE,'W');      
      FOR m IN CUR_SUPER_MCD(i.period_name) LOOP
        utl_file.put_line(x_id,   m.Store_Types||','||
                                  m.fin_year||','||
                                  m.scenario||','||
                                  m.VERSION||','||
                                  m.FA_Employee||','||                                  
                                  m.entity||','||                                  
                                  m.VIEW1||','||
                                  m.period||','||
                                  m.HYP_ACCNT||','||                                  
                                  m.DATA);
        END LOOP;
        EXCEPTION 
     WHEN TOO_MANY_ROWS THEN
        dbms_output.PUT_LINE('Too Many Rows');    
     WHEN NO_DATA_FOUND THEN
        dbms_output.PUT_LINE('No Data Found');    
     WHEN utl_file.invalid_path THEN
        RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_path');
     WHEN utl_file.invalid_mode THEN
         RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_mode');
     WHEN utl_file.invalid_filehandle THEN
         RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_filehandle');
     WHEN utl_file.invalid_operation THEN
         RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_operation');
     WHEN utl_file.read_error THEN
         RAISE_APPLICATION_ERROR(-20001, 'utl_file.read_error');
     WHEN utl_file.write_error THEN
         RAISE_APPLICATION_ERROR(-20001, 'utl_file.write_error');
     WHEN utl_file.internal_error THEN
         RAISE_APPLICATION_ERROR(-20001, 'utl_file.internal_error');
     WHEN OTHERS THEN
          dbms_output.put_line(SubStr('Error '||TO_CHAR(SQLCODE)||': '||SQLERRM, 1,255));
    END;  
  END LOOP;        
  utl_file.fclose(x_id);
END XXABRL_HYPN_SUPER_MCD_DATA;


PROCEDURE XXABRL_HYPN_HYPER_MCD_DATA(errbuf out varchar2,
                                     retcode out varchar2,
                                     p_period_name varchar2,
                                     p_version_no number,
                                     p_param_data varchar2)
AS
V_L_FILE         VARCHAR2(200);  
V_FINANCE_YEAR   NUMBER;                                     
/*-------------------------------------------------------------------------*/
  ------------Cursor for getting the Financial first period name------------
/*-------------------------------------------------------------------------*/
                 
  CURSOR CUR_PERIOD_NAME IS 
  SELECT  DISTINCT UPPER(a.period_name) PERIOD_NAME
    FROM  gl_period_statuses a, gl_period_statuses b
   WHERE  a.application_id = 101
     AND  b.application_id = 101
     AND  a.period_type = b.period_type
     AND  a.period_year = b.period_year
     AND  b.period_name = upper(p_period_name)
     AND  a.period_num =
          (SELECT min(c.period_num)
             FROM gl_period_statuses c
            WHERE c.application_id = 101               
              AND c.period_year = a.period_year
              AND c.period_type = a.period_type
         GROUP BY c.period_year);
/*-------------------------------------------------------------------------*/
  ------------Cursor for getting the SCM OPS DATA name------------
/*-------------------------------------------------------------------------*/        
         
CURSOR CUR_HYPER_MCD(p_first_periond_name VARCHAR2) IS
SELECT   fin_year, scenario, VERSION, hm_product, fa_employee, entity, view1,
         period, hyp_accnt,
         SUM (DECODE (hyperion_accnt_type,
                      'Owners'' equity', -DATA,
                      'Revenue', -DATA,
                      'Liability', -DATA,
                      'Expense', DATA,
                      'Asset', DATA
                     )
             ) DATA
    FROM ((SELECT   'FY' || SUBSTR (GLB.period_year, 3, 4) fin_year,
                    'Actual' scenario, 'V2000' VERSION,
                    'HP998' hm_product,--merch.hypn_member
                    fa_emp.hm_fa_emp fa_employee, loc.hyper_member entity,
                    'W0004' view1, SUBSTR (p_period_name, 1, 3) period,
                    accnt.hm_mcd hyp_accnt, accnt.hyperion_accnt_type,
                    SUM (DECODE (GLB.period_name,
                                 p_period_name, NVL (period_net_dr, 0)
                                  - NVL (period_net_cr, 0)
                                  + NVL (begin_balance_dr, 0)
                                  - NVL (begin_balance_cr, 0),
                                 0
                                )
                        ) DATA
               FROM xxabrl_hyperion_accnt_mapping accnt,
                    gl_code_combinations glcc,
                    XXABRL_GL_BALANCES GLB,
                    xxabrl_hyperion_loc_mapping loc,
                   -- xxabrl_hyperion_merch_mapping merch,
                    xxabrl_hypn_gl_fa_emp_mapping fa_emp
              WHERE 1 = 1
                AND GLB.period_name IN (p_period_name, p_first_periond_name)
                AND GLB.code_combination_id = glcc.code_combination_id
                AND loc.ofin_loc_code = glcc.segment4
                AND accnt.ofin_account = glcc.segment6
                --AND merch.ofin_member = glcc.segment5
                AND accnt.ofin_account = fa_emp.ofin_account
                AND accnt.hm_mcd = fa_emp.hm_mcd
                AND glcc.summary_flag = 'N'
                AND glcc.template_id IS NULL
                AND accnt.hm_mcd <> 'N'
                AND accnt.hm_mcd IS NOT NULL
                AND loc.hyper_member IS NOT NULL
                AND loc.entity_type = 'Hypermarket'
                AND accnt.hyperion_accnt_type IN
                       ('Owners'' equity', 'Revenue', 'Liability', 'Expense',
                        'Asset')
           GROUP BY 'FY' || SUBSTR (GLB.period_year, 3, 4),
                   -- merch.hypn_member,
                    hm_fa_emp,
                    accnt.hm_mcd,
                    accnt.hyperion_accnt_type,
                    loc.hyper_member))
   WHERE DATA <> 0
GROUP BY fin_year,scenario,VERSION,hm_product,fa_employee,
         entity,view1,period,hyp_accnt;
    
 x_id utl_file.file_type;
     
 BEGIN   
   
  BEGIN
    SELECT DISTINCT SUBSTR (glb.period_year, 3, 4)
      INTO v_finance_year 
      FROM XXABRL_GL_BALANCES glb 
     WHERE period_name = p_period_name;
     EXCEPTION
     WHEN OTHERS THEN 
     v_finance_year:= NULL;
    END;
      
    FOR i IN CUR_PERIOD_NAME LOOP
    BEGIN 
    XXABRL_OFIN2_HYPERION_PKG.XXABRL_HYPN_DATA_VALIDAION(p_period_name);
      SELECT 'HYPER_MCD_FY'||v_finance_year||'_'||substr(p_period_name,1,3)||'_'||'V'||p_version_no||'.csv'INTO V_L_FILE FROM dual;        
      --SELECT 'XXABRL_HYPN_HYPER_MCD_'||p_period_name||'_DATA'||NVL(TO_CHAR(TO_DATE(sysdate,'DD-MON-YYYY'),'DDMMYY'),TO_CHAR(SYSDATE-3,'DDMMYY'))||'.csv' INTO V_L_FILE FROM dual;
      x_id:=utl_file.fopen('/ebiz/u01/PROD/db/tech_st/10.2.0/appsutil/outbound/PROD_msoradbprod',V_L_FILE,'W');
       FOR n IN CUR_HYPER_MCD(i.period_name) LOOP
        utl_file.put_line(x_id,   n.fin_year||','||
                                  n.scenario||','||
                                  n.VERSION||','||
                                  n.HM_PRODUCT||','||
                                  n.FA_EMPLOYEE||','||
                                  n.entity||','||                                  
                                  n.VIEW1||','||
                                  n.period||','||
                                  n.HYP_ACCNT||','||                                  
                                  n.DATA);
       END LOOP;
       EXCEPTION 
     WHEN TOO_MANY_ROWS THEN
        dbms_output.PUT_LINE('Too Many Rows');    
     WHEN NO_DATA_FOUND THEN
        dbms_output.PUT_LINE('No Data Found');    
     WHEN utl_file.invalid_path THEN
        RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_path');
     WHEN utl_file.invalid_mode THEN
         RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_mode');
     WHEN utl_file.invalid_filehandle THEN
         RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_filehandle');
     WHEN utl_file.invalid_operation THEN
         RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_operation');
     WHEN utl_file.read_error THEN
         RAISE_APPLICATION_ERROR(-20001, 'utl_file.read_error');
     WHEN utl_file.write_error THEN
         RAISE_APPLICATION_ERROR(-20001, 'utl_file.write_error');
     WHEN utl_file.internal_error THEN
         RAISE_APPLICATION_ERROR(-20001, 'utl_file.internal_error');
     WHEN OTHERS THEN
          dbms_output.put_line(SubStr('Error '||TO_CHAR(SQLCODE)||': '||SQLERRM, 1,255));
    END;
  END LOOP;        
  utl_file.fclose(x_id);
END XXABRL_HYPN_HYPER_MCD_DATA;



PROCEDURE XXABRL_HYPN_SCM_MCD_DATA(errbuf out varchar2,
                                     retcode out varchar2,
                                     p_period_name varchar2,
                                     p_version_no number,
                                     p_param_data varchar2)  
AS
V_L_FILE         VARCHAR2(200);
V_FINANCE_YEAR   NUMBER;
/*-------------------------------------------------------------------------*/
  ------------Cursor for getting the Financial first period name------------
/*-------------------------------------------------------------------------*/
                 
  CURSOR CUR_PERIOD_NAME IS 
  SELECT  DISTINCT UPPER(a.period_name) PERIOD_NAME
    FROM  gl_period_statuses a, gl_period_statuses b
   WHERE  a.application_id = 101
     AND  b.application_id = 101
     AND  a.period_type = b.period_type
     AND  a.period_year = b.period_year
     AND  b.period_name = upper(p_period_name)
     AND  a.period_num =
          (SELECT min(c.period_num)
             FROM gl_period_statuses c
            WHERE c.application_id = 101               
              AND c.period_year = a.period_year
              AND c.period_type = a.period_type
         GROUP BY c.period_year);
/*-------------------------------------------------------------------------*/
  ------------Cursor for getting the SCM OPS DATA name------------
/*-------------------------------------------------------------------------*/        
         
CURSOR CUR_SCM_MCD(p_first_periond_name VARCHAR2) IS
SELECT   fin_year, scenario, VERSION, fa_employee, entity, view1, period,
         ACCOUNT,
         SUM (DECODE (hyperion_accnt_type,
                      'Owners'' equity', -DATA,
                      'Revenue', -DATA,
                      'Liability', -DATA,
                      'Expense', DATA,
                      'Asset', DATA
                     )
             ) DATA
    FROM ((SELECT   'FY' || SUBSTR (GLB.period_year, 3, 4) fin_year,
                    'Actual' scenario, 'V2000' VERSION,
                    fa_emp.dc_fa_emp fa_employee, loc.scm_member entity,
                    'W0004' view1, SUBSTR (p_period_name, 1, 3) period,
                    accnt.dc_scm_mcd ACCOUNT, accnt.hyperion_accnt_type,
                    SUM (DECODE (GLB.period_name,
                                 p_period_name, NVL (period_net_dr, 0)
                                  - NVL (period_net_cr, 0)
                                  + NVL (begin_balance_dr, 0)
                                  - NVL (begin_balance_cr, 0),
                                 0
                                )
                        ) DATA
               FROM xxabrl_hyperion_accnt_mapping accnt,
                    gl_code_combinations glcc,
                    XXABRL_GL_BALANCES GLB,
                    xxabrl_hyperion_loc_mapping loc,
                    xxabrl_hypn_gl_fa_emp_mapping fa_emp
              WHERE 1 = 1
                AND GLB.period_name IN (p_period_name, p_first_periond_name)
                AND GLB.code_combination_id = glcc.code_combination_id
                AND loc.ofin_loc_code = glcc.segment4
                AND accnt.ofin_account = glcc.segment6
                AND accnt.ofin_account = fa_emp.ofin_account
                AND accnt.dc_scm_mcd = fa_emp.dc_scm_mcd
                AND glcc.summary_flag = 'N'
                AND glcc.template_id IS NULL
                AND accnt.dc_scm_mcd <> 'N'
                AND accnt.dc_scm_mcd IS NOT NULL
                AND loc.scm_member IS NOT NULL
                AND loc.entity_type = 'DC'
                AND accnt.hyperion_accnt_type IN
                       ('Owners'' equity', 'Revenue', 'Liability', 'Expense',
                        'Asset')
           GROUP BY 'FY' || SUBSTR (GLB.period_year, 3, 4),
                    fa_emp.dc_fa_emp,
                    accnt.dc_scm_mcd,
                    accnt.hyperion_accnt_type,
                    loc.scm_member)
          UNION ALL-----------------Cursor for getting the SCM RDC mcd----------
          (SELECT   'FY' || SUBSTR (GLB.period_year, 3, 4) fin_year,
                    'Actual' scenario, 'V2000' VERSION,
                    fa_emp.rdc_fa_emp fa_employee, loc.scm_member entity,
                    'W0004' view1, SUBSTR (p_period_name, 1, 3) period,
                    accnt.rdc_scm_mcd ACCOUNT, accnt.hyperion_accnt_type,
                    SUM (DECODE (GLB.period_name,
                                 p_period_name, NVL (period_net_dr, 0)
                                  - NVL (period_net_cr, 0)
                                  + NVL (begin_balance_dr, 0)
                                  - NVL (begin_balance_cr, 0),
                                 0
                                )
                        ) DATA
               FROM xxabrl_hyperion_accnt_mapping accnt,
                    gl_code_combinations glcc,
                    XXABRL_GL_BALANCES GLB,
                    xxabrl_hyperion_loc_mapping loc,
                    xxabrl_hypn_gl_fa_emp_mapping fa_emp
              WHERE 1 = 1
                AND GLB.period_name IN (p_period_name, p_first_periond_name)
                AND GLB.code_combination_id = glcc.code_combination_id
                AND loc.ofin_loc_code = glcc.segment4
                AND accnt.ofin_account = glcc.segment6
                AND accnt.ofin_account = fa_emp.ofin_account
                AND accnt.rdc_scm_mcd = fa_emp.rdc_scm_mcd
                AND glcc.summary_flag = 'N'
                AND glcc.template_id IS NULL
                AND accnt.rdc_scm_mcd <> 'N'
                AND accnt.rdc_scm_mcd IS NOT NULL
                AND loc.scm_member IS NOT NULL
                AND loc.entity_type = 'RDC'
                AND accnt.hyperion_accnt_type IN
                       ('Owners'' equity', 'Revenue', 'Liability', 'Expense',
                        'Asset')
           GROUP BY 'FY' || SUBSTR (GLB.period_year, 3, 4),
                    fa_emp.rdc_fa_emp,
                    accnt.rdc_scm_mcd,
                    accnt.hyperion_accnt_type,
                    loc.scm_member)
          UNION ALL------------------Cursor for getting the SCM RPC mcd---------
          (SELECT   'FY' || SUBSTR (GLB.period_year, 3, 4) fin_year,
                    'Actual' scenario, 'V2000' VERSION,
                    fa_emp.rpc_fa_emp fa_employee, loc.scm_member entity,
                    'W0004' view1, SUBSTR (p_period_name, 1, 3) period,
                    accnt.rpc_scm_mcd ACCOUNT, accnt.hyperion_accnt_type,
                    SUM (DECODE (GLB.period_name,
                                 p_period_name, NVL (period_net_dr, 0)
                                  - NVL (period_net_cr, 0)
                                  + NVL (begin_balance_dr, 0)
                                  - NVL (begin_balance_cr, 0),
                                 0
                                )
                        ) DATA
               FROM xxabrl_hyperion_accnt_mapping accnt,
                    gl_code_combinations glcc,
                    XXABRL_GL_BALANCES GLB,
                    xxabrl_hyperion_loc_mapping loc,
                    xxabrl_hypn_gl_fa_emp_mapping fa_emp
              WHERE 1 = 1
                AND GLB.period_name IN (p_period_name, p_first_periond_name)
                AND GLB.code_combination_id = glcc.code_combination_id
                AND loc.ofin_loc_code = glcc.segment4
                AND accnt.ofin_account = glcc.segment6
                AND accnt.ofin_account = fa_emp.ofin_account
                AND accnt.rpc_scm_mcd = fa_emp.rpc_scm_mcd
                AND glcc.summary_flag = 'N'
                AND glcc.template_id IS NULL
                AND accnt.rpc_scm_mcd <> 'N'
                AND accnt.rpc_scm_mcd IS NOT NULL
                AND loc.scm_member IS NOT NULL
                AND loc.entity_type = 'RPC'
                AND accnt.hyperion_accnt_type IN
                       ('Owners'' equity', 'Revenue', 'Liability', 'Expense',
                        'Asset')
           GROUP BY 'FY' || SUBSTR (GLB.period_year, 3, 4),
                    fa_emp.rpc_fa_emp,
                    accnt.rpc_scm_mcd,
                    accnt.hyperion_accnt_type,
                    loc.scm_member)
          UNION ALL----------Cursor for getting the SCM CC mcd-------------------
          (SELECT   'FY' || SUBSTR (GLB.period_year, 3, 4) fin_year,
                    'Actual' scenario, 'V2000' VERSION,
                    fa_emp.cc_fa_emp fa_employee, loc.scm_member entity,
                    'W0004' view1, SUBSTR (p_period_name, 1, 3) period,
                    accnt.cc_scm_mcd ACCOUNT, accnt.hyperion_accnt_type,
                    SUM (DECODE (GLB.period_name,
                                 p_period_name, NVL (period_net_dr, 0)
                                  - NVL (period_net_cr, 0)
                                  + NVL (begin_balance_dr, 0)
                                  - NVL (begin_balance_cr, 0),
                                 0
                                )
                        ) DATA
               FROM xxabrl_hyperion_accnt_mapping accnt,
                    gl_code_combinations glcc,
                    XXABRL_GL_BALANCES GLB,
                    xxabrl_hyperion_loc_mapping loc,
                    xxabrl_hypn_gl_fa_emp_mapping fa_emp
              WHERE 1 = 1
                AND GLB.period_name IN (p_period_name, p_first_periond_name)
                AND GLB.code_combination_id = glcc.code_combination_id
                AND loc.ofin_loc_code = glcc.segment4
                AND accnt.ofin_account = glcc.segment6
                AND accnt.ofin_account = fa_emp.ofin_account
                AND accnt.cc_scm_mcd = fa_emp.cc_scm_mcd
                AND glcc.summary_flag = 'N'
                AND glcc.template_id IS NULL
                AND accnt.cc_scm_mcd <> 'N'
                AND accnt.cc_scm_mcd IS NOT NULL
                AND loc.scm_member IS NOT NULL
                AND loc.entity_type = 'CC'
                AND accnt.hyperion_accnt_type IN
                       ('Owners'' equity', 'Revenue', 'Liability', 'Expense',
                        'Asset')
           GROUP BY 'FY' || SUBSTR (GLB.period_year, 3, 4),
                    fa_emp.cc_fa_emp,
                    accnt.cc_scm_mcd,
                    accnt.hyperion_accnt_type,
                    loc.scm_member)
          UNION ALL----------Cursor for getting the SCM FNV mcd---------------
          (SELECT   'FY' || SUBSTR (GLB.period_year, 3, 4) fin_year,
                    'Actual' scenario, 'V2000' VERSION,
                    fa_emp.fnv_fa_emp fa_employee, loc.scm_member entity,
                    'W0004' view1, SUBSTR (p_period_name, 1, 3) period,
                    accnt.fnv_scm_mcd ACCOUNT, accnt.hyperion_accnt_type,
                    SUM (DECODE (GLB.period_name,
                                 p_period_name, NVL (period_net_dr, 0)
                                  - NVL (period_net_cr, 0)
                                  + NVL (begin_balance_dr, 0)
                                  - NVL (begin_balance_cr, 0),
                                 0
                                )
                        ) DATA
               FROM xxabrl_hyperion_accnt_mapping accnt,
                    gl_code_combinations glcc,
                    XXABRL_GL_BALANCES GLB,
                    xxabrl_hyperion_loc_mapping loc,
                    xxabrl_hypn_gl_fa_emp_mapping fa_emp
              WHERE 1 = 1
                AND GLB.period_name IN (p_period_name, p_first_periond_name)
                AND GLB.code_combination_id = glcc.code_combination_id
                AND loc.ofin_loc_code = glcc.segment4
                AND accnt.ofin_account = glcc.segment6
                AND accnt.ofin_account = fa_emp.ofin_account
                AND accnt.fnv_scm_mcd = fa_emp.fnv_scm_mcd
                AND glcc.summary_flag = 'N'
                AND glcc.template_id IS NULL
                AND accnt.fnv_scm_mcd <> 'N'
                AND accnt.fnv_scm_mcd IS NOT NULL
                AND loc.scm_member IS NOT NULL
                AND loc.entity_type = 'FnV'
                AND accnt.hyperion_accnt_type IN
                       ('Owners'' equity', 'Revenue', 'Liability', 'Expense',
                        'Asset')
           GROUP BY 'FY' || SUBSTR (GLB.period_year, 3, 4),
                    fa_emp.fnv_fa_emp,
                    accnt.fnv_scm_mcd,
                    accnt.hyperion_accnt_type,
                    loc.scm_member))
   WHERE DATA <> 0
GROUP BY fin_year,
         scenario,
         VERSION,
         fa_employee,
         entity,
         view1,
         period,
         ACCOUNT;
    
 x_id utl_file.file_type;
     
 BEGIN   
 
 BEGIN
    SELECT DISTINCT SUBSTR (glb.period_year, 3, 4)
      INTO v_finance_year 
      FROM XXABRL_GL_BALANCES glb 
     WHERE period_name = p_period_name;
     EXCEPTION
     WHEN OTHERS THEN 
     v_finance_year:= NULL;
    END;
   
    FOR i IN CUR_PERIOD_NAME LOOP
    BEGIN 
    XXABRL_OFIN2_HYPERION_PKG.XXABRL_HYPN_DATA_VALIDAION(p_period_name); 
         SELECT 'SCM_MCD_FY'||v_finance_year||'_'||substr(p_period_name,1,3)||'_'||'V'||p_version_no||'.csv'INTO V_L_FILE FROM dual;   
        ---SELECT 'XXABRL_HYPN_SCM_MCD_'||p_period_name||'_DATA'||NVL(TO_CHAR(TO_DATE(sysdate,'DD-MON-YYYY'),'DDMMYY'),TO_CHAR(SYSDATE-3,'DDMMYY'))||'.csv' INTO V_L_FILE FROM dual;
      x_id:=utl_file.fopen('/ebiz/u01/PROD/db/tech_st/10.2.0/appsutil/outbound/PROD_msoradbprod',V_L_FILE,'W');
      FOR o IN CUR_SCM_MCD(i.period_name) LOOP
        utl_file.put_line(x_id,   o.fin_year||','||
                                  o.scenario||','||
                                  o.VERSION||','||                                 
                                  o.FA_EMPLOYEE||','||
                                  o.entity||','||                                  
                                  o.VIEW1||','||
                                  o.period||','||
                                  o.account||','||                                  
                                  o.DATA);      
       END LOOP;
       EXCEPTION 
     WHEN TOO_MANY_ROWS THEN
        dbms_output.PUT_LINE('Too Many Rows');    
     WHEN NO_DATA_FOUND THEN
        dbms_output.PUT_LINE('No Data Found');    
     WHEN utl_file.invalid_path THEN
        RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_path');
     WHEN utl_file.invalid_mode THEN
         RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_mode');
     WHEN utl_file.invalid_filehandle THEN
         RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_filehandle');
     WHEN utl_file.invalid_operation THEN
         RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_operation');
     WHEN utl_file.read_error THEN
         RAISE_APPLICATION_ERROR(-20001, 'utl_file.read_error');
     WHEN utl_file.write_error THEN
         RAISE_APPLICATION_ERROR(-20001, 'utl_file.write_error');
     WHEN utl_file.internal_error THEN
         RAISE_APPLICATION_ERROR(-20001, 'utl_file.internal_error');
     WHEN OTHERS THEN
          dbms_output.put_line(SubStr('Error '||TO_CHAR(SQLCODE)||': '||SQLERRM, 1,255));
    END;
  END LOOP;        
  utl_file.fclose(x_id);
END XXABRL_HYPN_SCM_MCD_DATA;

PROCEDURE XXABRL_HYPN_OHDS_MCD_DATA(errbuf out varchar2,
                                     retcode out varchar2,
                                     p_period_name varchar2,
                                     p_version_no number,
                                     p_param_data varchar2)
AS
V_L_FILE         VARCHAR2(200); 
V_FINANCE_YEAR   NUMBER;                                      
/*-------------------------------------------------------------------------*/
  ------------Cursor for getting the Financial first period name------------
/*-------------------------------------------------------------------------*/
                 
  CURSOR CUR_PERIOD_NAME IS 
  SELECT  DISTINCT UPPER(a.period_name) PERIOD_NAME
    FROM  gl_period_statuses a, gl_period_statuses b
   WHERE  a.application_id = 101
     AND  b.application_id = 101
     AND  a.period_type = b.period_type
     AND  a.period_year = b.period_year
     AND  b.period_name = upper(p_period_name)
     AND  a.period_num =
          (SELECT min(c.period_num)
             FROM gl_period_statuses c
            WHERE c.application_id = 101               
              AND c.period_year = a.period_year
              AND c.period_type = a.period_type
         GROUP BY c.period_year);
/*-------------------------------------------------------------------------*/
           ------------Cursor for getting the OHDS MCD DATA ------------
/*-------------------------------------------------------------------------*/        
         
CURSOR CUR_OHDS_MCD(p_first_periond_name VARCHAR2) IS
SELECT   format, fin_year, scenario, VERSION, employee_category, department,
         fa_employee, entity, view1, period, hyp_accnt,
         SUM (DECODE (hyperion_accnt_type,
                      'Owners'' equity', -DATA,
                      'Revenue', -DATA,
                      'Liability', -DATA,
                      'Expense', DATA,
                      'Asset', DATA
                     )
             ) DATA
    FROM ((SELECT   sbu.hypn_member format,
                    'FY' || SUBSTR (GLB.period_year, 3, 4) fin_year,
                    'Actual' scenario, 'V2000' VERSION,
                    fa_emp.office_super_ohds_mcd_emp_cat employee_category,
                    cc.hypn_member department,
                    fa_emp.office_super_ohds_mcd_fa_emp fa_employee,
                    loc.ohds_member entity, 'W0004' view1,
                    SUBSTR (p_period_name, 1, 3) period,
                    accnt.office_super_ohds_mcd hyp_accnt,
                    accnt.hyperion_accnt_type,
                    SUM (DECODE (GLB.period_name,
                                 p_period_name, NVL (period_net_dr, 0)
                                  - NVL (period_net_cr, 0)
                                  + NVL (begin_balance_dr, 0)
                                  - NVL (begin_balance_cr, 0),
                                 0
                                )
                        ) DATA
               FROM xxabrl_hyperion_accnt_mapping accnt,
                    gl_code_combinations glcc,
                    XXABRL_GL_BALANCES GLB,
                    xxabrl_hyperion_loc_mapping loc,
                    xxabrl_hyperion_sbu_mapping sbu,
                    xxabrl_hyperion_cc_mapping cc,
                    xxabrl_hypn_gl_fa_emp_mapping fa_emp
              WHERE 1 = 1
                AND GLB.period_name IN (p_period_name, p_first_periond_name)
                AND GLB.code_combination_id = glcc.code_combination_id
                AND loc.ofin_loc_code = glcc.segment4
                AND accnt.ofin_account = glcc.segment6
                AND accnt.ofin_account = fa_emp.ofin_account
                AND accnt.office_super_ohds_mcd = fa_emp.office_super_ohds_mcd
                AND sbu.ofin_member = glcc.segment3
                AND cc.ofin_member = glcc.segment2
                AND glcc.summary_flag = 'N'
                AND glcc.template_id IS NULL
                AND accnt.office_super_ohds_mcd <> 'N'
                AND accnt.office_super_ohds_mcd IS NOT NULL
                AND loc.ohds_member IS NOT NULL
                AND loc.entity_type IN ('OHDS-SM','OHDS-HM','CORP')
                AND accnt.hyperion_accnt_type IN
                       ('Owners'' equity', 'Revenue', 'Liability', 'Expense',
                        'Asset')
           GROUP BY sbu.hypn_member,
                    'FY' || SUBSTR (GLB.period_year, 3, 4),
                    office_super_ohds_mcd_emp_cat,
                    cc.hypn_member,
                    fa_emp.office_super_ohds_mcd_fa_emp,
                    glcc.segment5,
                    accnt.office_super_ohds_mcd,
                    accnt.hyperion_accnt_type,
                    loc.ohds_member))
   WHERE DATA <> 0
GROUP BY format,fin_year,scenario,VERSION,
         employee_category,department,fa_employee,
         entity,view1,period,hyp_accnt;
    
 x_id utl_file.file_type;
     
 BEGIN     
 
  BEGIN
    SELECT DISTINCT SUBSTR (glb.period_year, 3, 4)
      INTO v_finance_year 
      FROM XXABRL_GL_BALANCES glb 
     WHERE period_name = p_period_name;
     EXCEPTION
     WHEN OTHERS THEN 
     v_finance_year:= NULL;
   END;
 
    FOR i IN CUR_PERIOD_NAME LOOP    
    BEGIN
    XXABRL_OFIN2_HYPERION_PKG.XXABRL_HYPN_DATA_VALIDAION(p_period_name);
        SELECT 'OHDS_MCD_FY'||v_finance_year||'_'||substr(p_period_name,1,3)||'_'||'V'||p_version_no||'.csv'INTO V_L_FILE FROM dual;
        --SELECT 'XXABRL_HYPN_OHDS_MCD_'||p_period_name||'_DATA'||NVL(TO_CHAR(TO_DATE(sysdate,'DD-MON-YYYY'),'DDMMYY'),TO_CHAR(SYSDATE-3,'DDMMYY'))||'.csv' INTO V_L_FILE FROM dual;
      x_id:=utl_file.fopen('/ebiz/u01/PROD/db/tech_st/10.2.0/appsutil/outbound/PROD_msoradbprod',V_L_FILE,'W');
      FOR p IN CUR_OHDS_MCD(i.period_name) LOOP
        utl_file.put_line(x_id,   p.format||','||
                                  p.fin_year||','||
                                  p.scenario||','||                                 
                                  p.version||','||
                                  p.employee_category||','||                                  
                                  p.department||','||
                                  p.fa_employee||','||
                                  p.entity||','|| 
                                  p.view1||','||
                                  p.Period||','||
                                  p.hyp_accnt||','||
                                  p.DATA);
                                  
       END LOOP;
       EXCEPTION 
     WHEN TOO_MANY_ROWS THEN
        dbms_output.PUT_LINE('Too Many Rows');    
     WHEN NO_DATA_FOUND THEN
        dbms_output.PUT_LINE('No Data Found');    
     WHEN utl_file.invalid_path THEN
        RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_path');
     WHEN utl_file.invalid_mode THEN
         RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_mode');
     WHEN utl_file.invalid_filehandle THEN
         RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_filehandle');
     WHEN utl_file.invalid_operation THEN
         RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_operation');
     WHEN utl_file.read_error THEN
         RAISE_APPLICATION_ERROR(-20001, 'utl_file.read_error');
     WHEN utl_file.write_error THEN
         RAISE_APPLICATION_ERROR(-20001, 'utl_file.write_error');
     WHEN utl_file.internal_error THEN
         RAISE_APPLICATION_ERROR(-20001, 'utl_file.internal_error');
     WHEN OTHERS THEN
          dbms_output.put_line(SubStr('Error '||TO_CHAR(SQLCODE)||': '||SQLERRM, 1,255));
    END;       
  END LOOP;        
  utl_file.fclose(x_id);
END XXABRL_HYPN_OHDS_MCD_DATA;


PROCEDURE XXABRL_HYPN_OHDS_OPS_DATA(errbuf out varchar2,
                                     retcode out varchar2,
                                     p_period_name varchar2,
                                     p_version_no number,
                                     p_param_data varchar2)
AS
V_L_FILE         VARCHAR2(200); 
V_FINANCE_YEAR   NUMBER;                                      
/*-------------------------------------------------------------------------*/
  ------------Cursor for getting the Financial first period name------------
/*-------------------------------------------------------------------------*/
                 
  CURSOR CUR_PERIOD_NAME IS 
  SELECT  DISTINCT UPPER(a.period_name) PERIOD_NAME
    FROM  gl_period_statuses a, gl_period_statuses b
   WHERE  a.application_id = 101
     AND  b.application_id = 101
     AND  a.period_type = b.period_type
     AND  a.period_year = b.period_year
     AND  b.period_name = upper(p_period_name)
     AND  a.period_num =
          (SELECT min(c.period_num)
             FROM gl_period_statuses c
            WHERE c.application_id = 101               
              AND c.period_year = a.period_year
              AND c.period_type = a.period_type
         GROUP BY c.period_year);
/*-------------------------------------------------------------------------*/
  ------------Cursor for getting the OHDS OPS ------------
/*-------------------------------------------------------------------------*/        

CURSOR CUR_OHDS_OPS(p_first_periond_name VARCHAR2) IS
SELECT   format, fin_year, scenario, VERSION, employee_category, department,
         entity, view1, period, hyp_accnt,
         SUM (DECODE (hyperion_accnt_type,
                      'Owners'' equity', -DATA,
                      'Revenue', -DATA,
                      'Liability', -DATA,
                      'Expense', DATA,
                      'Asset', DATA
                     )
             ) DATA
    FROM ((SELECT   sbu.hypn_member format,
                    'FY' || SUBSTR (GLB.period_year, 3, 4) fin_year,
                    'Actual' scenario, 'V2000' VERSION,
                    'EC998' employee_category, cc.hypn_member department,
                    loc.ohds_member entity, 'W0004' view1,
                    SUBSTR (p_period_name, 1, 3) period,
                    accnt.office_super_ohds_ops hyp_accnt,
                    accnt.hyperion_accnt_type,
                    SUM (DECODE (GLB.period_name,
                                 p_period_name, NVL (period_net_dr, 0)
                                  - NVL (period_net_cr, 0)
                                  + NVL (begin_balance_dr, 0)
                                  - NVL (begin_balance_cr, 0),
                                 0
                                )
                        ) DATA
               FROM xxabrl_hyperion_accnt_mapping accnt,
                    gl_code_combinations glcc,
                    xxabrl_gl_balances GLB,
                    xxabrl_hyperion_loc_mapping loc,
                    xxabrl_hyperion_sbu_mapping sbu,
                    xxabrl_hyperion_cc_mapping cc
              WHERE 1 = 1
                AND GLB.period_name IN (p_period_name, p_first_periond_name)
                AND GLB.code_combination_id = glcc.code_combination_id
                AND loc.ofin_loc_code = glcc.segment4
                AND accnt.ofin_account = glcc.segment6
                AND sbu.ofin_member = glcc.segment3
                AND cc.ofin_member = glcc.segment2
                AND glcc.summary_flag = 'N'
                AND glcc.template_id IS NULL
                AND accnt.office_super_ohds_ops <> 'N'
                AND accnt.office_super_ohds_ops IS NOT NULL
                AND loc.ohds_member IS NOT NULL
                AND loc.entity_type IN ('OHDS-SM','OHDS-HM','CORP')
                AND accnt.hyperion_accnt_type IN
                       ('Owners'' equity', 'Revenue', 'Liability', 'Expense',
                        'Asset')
           GROUP BY sbu.hypn_member,
                    'FY' || SUBSTR (GLB.period_year, 3, 4),
                    cc.hypn_member,
                    glcc.segment5,
                    accnt.office_super_ohds_ops,
                    accnt.hyperion_accnt_type,
                    loc.ohds_member))
   WHERE DATA <> 0
GROUP BY format,fin_year,scenario,VERSION,employee_category,
         department,entity,view1,period,hyp_accnt;    
 x_id utl_file.file_type;
     
 BEGIN     
 
  BEGIN
    SELECT DISTINCT SUBSTR (glb.period_year, 3, 4)
      INTO v_finance_year 
      FROM XXABRL_GL_BALANCES glb 
     WHERE period_name = p_period_name;
     EXCEPTION
     WHEN OTHERS THEN 
     v_finance_year:= NULL;
   END;
 
    FOR i IN CUR_PERIOD_NAME LOOP
    BEGIN
    XXABRL_OFIN2_HYPERION_PKG.XXABRL_HYPN_DATA_VALIDAION(p_period_name);   
        SELECT 'OHDS_OPS_FY'||v_finance_year||'_'||substr(p_period_name,1,3)||'_'||'V'||p_version_no||'.csv'INTO V_L_FILE FROM dual;
      --SELECT 'XXABRL_HYPN_OHDS_OPS_'||p_period_name||'_DATA'||NVL(TO_CHAR(TO_DATE(sysdate,'DD-MON-YYYY'),'DDMMYY'),TO_CHAR(SYSDATE-3,'DDMMYY'))||'.csv' INTO V_L_FILE FROM dual;
      x_id:=utl_file.fopen('/ebiz/u01/PROD/db/tech_st/10.2.0/appsutil/outbound/PROD_msoradbprod',V_L_FILE,'W');
      FOR q IN CUR_OHDS_OPS(i.period_name) LOOP
        utl_file.put_line(x_id,   q.format||','||
                                  q.fin_year||','||
                                  q.scenario||','||                                 
                                  q.version||','||
                                  q.employee_category||','||                                  
                                  q.department||','||                                  
                                  q.entity||','|| 
                                  q.view1||','||
                                  q.Period||','||
                                  q.hyp_accnt||','||
                                  q.DATA);
                                  
       END LOOP;
       EXCEPTION 
     WHEN TOO_MANY_ROWS THEN
        dbms_output.PUT_LINE('Too Many Rows');    
     WHEN NO_DATA_FOUND THEN
        dbms_output.PUT_LINE('No Data Found');    
     WHEN utl_file.invalid_path THEN
        RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_path');
     WHEN utl_file.invalid_mode THEN
         RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_mode');
     WHEN utl_file.invalid_filehandle THEN
         RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_filehandle');
     WHEN utl_file.invalid_operation THEN
         RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_operation');
     WHEN utl_file.read_error THEN
         RAISE_APPLICATION_ERROR(-20001, 'utl_file.read_error');
     WHEN utl_file.write_error THEN
         RAISE_APPLICATION_ERROR(-20001, 'utl_file.write_error');
     WHEN utl_file.internal_error THEN
         RAISE_APPLICATION_ERROR(-20001, 'utl_file.internal_error');
     WHEN OTHERS THEN
          dbms_output.put_line(SubStr('Error '||TO_CHAR(SQLCODE)||': '||SQLERRM, 1,255));
    END;
  END LOOP;        
  utl_file.fclose(x_id);
END XXABRL_HYPN_OHDS_OPS_DATA;

PROCEDURE XXABRL_HYPN_MKTNG_DATA(errbuf out varchar2,
                                     retcode out varchar2,
                                     p_period_name varchar2,
                                     p_version_no number,
                                     p_param_data varchar2)
AS
V_L_FILE         VARCHAR2(200);   
V_FINANCE_YEAR   NUMBER;                                    
/*-------------------------------------------------------------------------*/
  ------------Cursor for getting the Financial first period name------------
/*-------------------------------------------------------------------------*/
                 
  CURSOR CUR_PERIOD_NAME IS 
  SELECT  DISTINCT UPPER(a.period_name) PERIOD_NAME
    FROM  gl_period_statuses a, gl_period_statuses b
   WHERE  a.application_id = 101
     AND  b.application_id = 101
     AND  a.period_type = b.period_type
     AND  a.period_year = b.period_year
     AND  b.period_name = upper(p_period_name)
     AND  a.period_num =
          (SELECT min(c.period_num)
             FROM gl_period_statuses c
            WHERE c.application_id = 101               
              AND c.period_year = a.period_year
              AND c.period_type = a.period_type
         GROUP BY c.period_year);
/*-------------------------------------------------------------------------*/
       ------------Cursor for getting the Marketing DATA ------------
/*-------------------------------------------------------------------------*/        
         
CURSOR CUR_MKTG(p_first_periond_name VARCHAR2) IS
SELECT   fin_year, scenario, VERSION, publication, entity, view1, period,
         hyp_accnt,
         SUM (DECODE (hyperion_accnt_type,
                      'Owners'' equity', -DATA,
                      'Revenue', -DATA,
                      'Liability', -DATA,
                      'Expense', DATA,
                      'Asset', DATA
                     )
             ) DATA
    FROM ((SELECT   'FY' || SUBSTR (GLB.period_year, 3, 4) fin_year,
                    'Actual' scenario, 'V2000' VERSION, 'PN998' publication,
                    loc.mktg_member entity, 'YTM' view1,
                    accnt.office_super_mktg hyp_accnt,
                    accnt.hyperion_accnt_type,
                    SUBSTR (p_period_name, 1, 3) period,
                    SUM (DECODE (GLB.period_name,
                                 p_period_name, NVL (period_net_dr, 0)
                                  - NVL (period_net_cr, 0)
                                  + NVL (begin_balance_dr, 0)
                                  - NVL (begin_balance_cr, 0),
                                 0
                                )
                        ) DATA
               FROM xxabrl_hyperion_accnt_mapping accnt,
                    gl_code_combinations glcc,
                    XXABRL_GL_BALANCES GLB,
                    xxabrl_hyperion_loc_mapping loc
              WHERE 1 = 1
                AND GLB.period_name IN (p_period_name, p_first_periond_name)
                AND GLB.code_combination_id = glcc.code_combination_id
                AND loc.ofin_loc_code = glcc.segment4
                AND accnt.ofin_account = glcc.segment6
                AND glcc.summary_flag = 'N'
                AND glcc.template_id IS NULL
                AND accnt.office_super_mktg <> 'N'
                AND accnt.office_super_mktg IS NOT NULL
                AND loc.mktg_member IS NOT NULL
                AND entity_type IN ('OHDS-SM','Supermarket','CORP')
                AND accnt.hyperion_accnt_type IN
                       ('Owners'' equity', 'Revenue', 'Liability', 'Expense',
                        'Asset')
           GROUP BY 'FY' || SUBSTR (GLB.period_year, 3, 4),
                    accnt.office_super_mktg,
                    accnt.hyperion_accnt_type,
                    loc.mktg_member))
   WHERE DATA <> 0
GROUP BY fin_year,scenario,VERSION,publication,
         entity,view1,period,hyp_accnt;
             
 x_id utl_file.file_type;
     
 BEGIN     
 
  BEGIN
    SELECT DISTINCT SUBSTR (glb.period_year, 3, 4)
      INTO v_finance_year 
      FROM XXABRL_GL_BALANCES glb 
     WHERE period_name = p_period_name;
     EXCEPTION
     WHEN OTHERS THEN 
     v_finance_year:= NULL;
   END;
 
    FOR i IN CUR_PERIOD_NAME LOOP
    BEGIN    
    XXABRL_OFIN2_HYPERION_PKG.XXABRL_HYPN_DATA_VALIDAION(p_period_name); 
       SELECT 'MKTG_MKTG_FY'||v_finance_year||'_'||substr(p_period_name,1,3)||'_'||'V'||p_version_no||'.csv'INTO V_L_FILE FROM dual;  
      --SELECT 'XXABRL_HYPN_MKTNG_'||p_period_name||'_DATA'||NVL(TO_CHAR(TO_DATE(sysdate,'DD-MON-YYYY'),'DDMMYY'),TO_CHAR(SYSDATE-3,'DDMMYY'))||'.csv' INTO V_L_FILE FROM dual;
      x_id:=utl_file.fopen('/ebiz/u01/PROD/db/tech_st/10.2.0/appsutil/outbound/PROD_msoradbprod',V_L_FILE,'W');
       FOR r IN CUR_MKTG(i.period_name) LOOP
        utl_file.put_line(x_id,   r.fin_year||','||
                                  r.scenario||','||                                 
                                  r.version||','||
                                  r.Publication||','|| 
                                  r.entity||','|| 
                                  r.view1||','||
                                  r.Period||','||
                                  r.hyp_accnt||','||
                                  r.DATA);
                                  
       END LOOP;
    EXCEPTION 
     WHEN TOO_MANY_ROWS THEN
        dbms_output.PUT_LINE('Too Many Rows');    
     WHEN NO_DATA_FOUND THEN
        dbms_output.PUT_LINE('No Data Found');    
     WHEN utl_file.invalid_path THEN
        RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_path');
     WHEN utl_file.invalid_mode THEN
         RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_mode');
     WHEN utl_file.invalid_filehandle THEN
         RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_filehandle');
     WHEN utl_file.invalid_operation THEN
         RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_operation');
     WHEN utl_file.read_error THEN
         RAISE_APPLICATION_ERROR(-20001, 'utl_file.read_error');
     WHEN utl_file.write_error THEN
         RAISE_APPLICATION_ERROR(-20001, 'utl_file.write_error');
     WHEN utl_file.internal_error THEN
         RAISE_APPLICATION_ERROR(-20001, 'utl_file.internal_error');
     WHEN OTHERS THEN
          dbms_output.put_line(SubStr('Error '||TO_CHAR(SQLCODE)||': '||SQLERRM, 1,255));
    END;         
  END LOOP;        
  utl_file.fclose(x_id);
END XXABRL_HYPN_MKTNG_DATA;


PROCEDURE XXABRL_HYPN_CONSOL_DATA(errbuf out varchar2,
                                  retcode out varchar2,
                                  p_period_name varchar2,
                                  p_version_no number,
                                  p_param_data varchar2)
AS
V_L_FILE         VARCHAR2(200); 
V_FINANCE_YEAR   NUMBER;                                      
/*-------------------------------------------------------------------------*/
  ------------Cursor for getting the Financial first period name------------
/*-------------------------------------------------------------------------*/
                 
  CURSOR CUR_PERIOD_NAME IS 
  SELECT  DISTINCT UPPER(a.period_name) PERIOD_NAME
    FROM  gl_period_statuses a, gl_period_statuses b
   WHERE  a.application_id = 101
     AND  b.application_id = 101
     AND  a.period_type = b.period_type
     AND  a.period_year = b.period_year
     AND  b.period_name = upper(p_period_name)
     AND  a.period_num =
          (SELECT min(c.period_num)
             FROM gl_period_statuses c
            WHERE c.application_id = 101               
              AND c.period_year = a.period_year
              AND c.period_type = a.period_type
         GROUP BY c.period_year);
/*-------------------------------------------------------------------------*/
       ------------Cursor for getting the Marketing DATA ------------
/*-------------------------------------------------------------------------*/        
         
CURSOR CUR_CONSOL(p_first_periond_name VARCHAR2) IS
SELECT   view1, format, fin_year, scenario, VERSION, entity, MOVEMENT, period,
         hyp_accnt,
         SUM (DECODE (hyperion_accnt_type,
                      'Owners'' equity', -DATA,
                      'Revenue', -DATA,
                      'Liability', -DATA,
                      'Expense', DATA,
                      'Asset', DATA
                     )
             ) DATA
    FROM ((SELECT   'W0004' view1, sbu.hypn_member format,
                    'FY' || SUBSTR (GLB.period_year, 3, 4) fin_year,
                    'Actual' scenario, 'V2000' VERSION,
                    loc.consol_member entity, DECODE(accnt.sm_consol,'A7121','M0001','M0003') MOVEMENT,
                    SUBSTR (p_period_name, 1, 3) period,
                    accnt.sm_consol hyp_accnt, accnt.hyperion_accnt_type,
                    SUM (DECODE (GLB.period_name,
                                 p_period_name, NVL (period_net_dr, 0)
                                  - NVL (period_net_cr, 0)
                                  + NVL (begin_balance_dr, 0)
                                  - NVL (begin_balance_cr, 0),
                                 0
                                )
                        ) DATA
               FROM xxabrl_hyperion_accnt_mapping accnt,
                    gl_code_combinations glcc,
                    XXABRL_GL_BALANCES GLB,
                    xxabrl_hyperion_loc_mapping loc,
                    xxabrl_hyperion_sbu_mapping sbu
              WHERE 1 = 1
                AND GLB.period_name IN (p_period_name, p_first_periond_name)
                AND GLB.code_combination_id = glcc.code_combination_id
                AND loc.ofin_loc_code = glcc.segment4
                AND accnt.ofin_account = glcc.segment6
                AND sbu.ofin_member = glcc.segment3
                AND glcc.summary_flag = 'N'
                AND glcc.template_id IS NULL
                AND accnt.sm_consol <> 'N'
                AND accnt.sm_consol IS NOT NULL
                AND loc.consol_member IS NOT NULL
                AND loc.entity_type = 'Supermarket'
                AND accnt.hyperion_accnt_type IN
                       ('Owners'' equity', 'Revenue', 'Liability', 'Expense',
                        'Asset')
           GROUP BY sbu.hypn_member,
                    'FY' || SUBSTR (GLB.period_year, 3, 4),
                    glcc.segment5,
                    accnt.sm_consol,
                    accnt.hyperion_accnt_type,
                    loc.consol_member)
          UNION ALL      -------------------------Hyper-----------------------
          (SELECT   'W0004' view1, sbu.hypn_member format,
                    'FY' || SUBSTR (GLB.period_year, 3, 4) fin_year,
                    'Actual' scenario, 'V2000' VERSION,
                    loc.consol_member entity, DECODE(accnt.hm_consol,'A7121','M0001','M0003') MOVEMENT,
                    SUBSTR (p_period_name, 1, 3) period,
                    accnt.hm_consol hyp_accnt, accnt.hyperion_accnt_type,
                    SUM (DECODE (GLB.period_name,
                                 p_period_name, NVL (period_net_dr, 0)
                                  - NVL (period_net_cr, 0)
                                  + NVL (begin_balance_dr, 0)
                                  - NVL (begin_balance_cr, 0),
                                 0
                                )
                        ) DATA
               FROM xxabrl_hyperion_accnt_mapping accnt,
                    gl_code_combinations glcc,
                    XXABRL_GL_BALANCES GLB,
                    xxabrl_hyperion_loc_mapping loc,
                    xxabrl_hyperion_sbu_mapping sbu
              WHERE 1 = 1
                AND GLB.period_name IN (p_period_name, p_first_periond_name)
                AND GLB.code_combination_id = glcc.code_combination_id
                AND loc.ofin_loc_code = glcc.segment4
                AND accnt.ofin_account = glcc.segment6
                AND sbu.ofin_member = glcc.segment3
                AND glcc.summary_flag = 'N'
                AND glcc.template_id IS NULL
                AND accnt.hm_consol <> 'N'
                AND accnt.hm_consol IS NOT NULL
                AND loc.consol_member IS NOT NULL
                AND loc.entity_type = 'Hypermarket'
                AND accnt.hyperion_accnt_type IN
                       ('Owners'' equity', 'Revenue', 'Liability', 'Expense',
                        'Asset')
           GROUP BY sbu.hypn_member,
                    'FY' || SUBSTR (GLB.period_year, 3, 4),
                    glcc.segment5,
                    accnt.hm_consol,
                    accnt.hyperion_accnt_type,
                    loc.consol_member)
          UNION ALL-----------------------SCM-----------------------
                   -------------------------DC----------------------
          (SELECT   'W0004' view1, sbu.hypn_member format,
                    'FY' || SUBSTR (GLB.period_year, 3, 4) fin_year,
                    'Actual' scenario, 'V2000' VERSION,
                    loc.consol_member entity, DECODE(accnt.dc_scm_consol,'A7121','M0001','M0003') MOVEMENT,
                    SUBSTR (p_period_name, 1, 3) period,
                    accnt.dc_scm_consol hyp_accnt, accnt.hyperion_accnt_type,
                    SUM (DECODE (GLB.period_name,
                                 p_period_name, NVL (period_net_dr, 0)
                                  - NVL (period_net_cr, 0)
                                  + NVL (begin_balance_dr, 0)
                                  - NVL (begin_balance_cr, 0),
                                 0
                                )
                        ) DATA
               FROM xxabrl_hyperion_accnt_mapping accnt,
                    gl_code_combinations glcc,
                    XXABRL_GL_BALANCES GLB,
                    xxabrl_hyperion_loc_mapping loc,
                    xxabrl_hyperion_sbu_mapping sbu
              WHERE 1 = 1
                AND GLB.period_name IN (p_period_name, p_first_periond_name)
                AND GLB.code_combination_id = glcc.code_combination_id
                AND loc.ofin_loc_code = glcc.segment4
                AND accnt.ofin_account = glcc.segment6
                AND sbu.ofin_member = glcc.segment3
                AND glcc.summary_flag = 'N'
                AND glcc.template_id IS NULL
                AND accnt.dc_scm_consol <> 'N'
                AND accnt.dc_scm_consol IS NOT NULL
                AND loc.consol_member IS NOT NULL
                AND loc.entity_type = 'DC'
                AND accnt.hyperion_accnt_type IN
                       ('Owners'' equity', 'Revenue', 'Liability', 'Expense',
                        'Asset')
           GROUP BY sbu.hypn_member,
                    'FY' || SUBSTR (GLB.period_year, 3, 4),
                    glcc.segment5,
                    accnt.dc_scm_consol,
                    accnt.hyperion_accnt_type,
                    loc.consol_member)
          UNION ALL -------------------------RPC-----------------------
          (SELECT   'W0004' view1, sbu.hypn_member format,
                    'FY' || SUBSTR (GLB.period_year, 3, 4) fin_year,
                    'Actual' scenario, 'V2000' VERSION,
                    loc.consol_member entity, DECODE(accnt.rpc_consol,'A7121','M0001','M0003') MOVEMENT,
                    SUBSTR (p_period_name, 1, 3) period,
                    accnt.rpc_consol hyp_accnt, accnt.hyperion_accnt_type,
                    SUM (DECODE (GLB.period_name,
                                 p_period_name, NVL (period_net_dr, 0)
                                  - NVL (period_net_cr, 0)
                                  + NVL (begin_balance_dr, 0)
                                  - NVL (begin_balance_cr, 0),
                                 0
                                )
                        ) DATA
               FROM xxabrl_hyperion_accnt_mapping accnt,
                    gl_code_combinations glcc,
                    XXABRL_GL_BALANCES GLB,
                    xxabrl_hyperion_loc_mapping loc,
                    xxabrl_hyperion_sbu_mapping sbu
              WHERE 1 = 1
                AND GLB.period_name IN (p_period_name, p_first_periond_name)
                AND GLB.code_combination_id = glcc.code_combination_id
                AND loc.ofin_loc_code = glcc.segment4
                AND accnt.ofin_account = glcc.segment6
                AND sbu.ofin_member = glcc.segment3
                AND glcc.summary_flag = 'N'
                AND glcc.template_id IS NULL
                AND accnt.rpc_consol <> 'N'
                AND accnt.rpc_consol IS NOT NULL
                AND loc.consol_member IS NOT NULL
                AND loc.entity_type = 'RPC'
                AND accnt.hyperion_accnt_type IN
                       ('Owners'' equity', 'Revenue', 'Liability', 'Expense',
                        'Asset')
           GROUP BY sbu.hypn_member,
                    'FY' || SUBSTR (GLB.period_year, 3, 4),
                    glcc.segment5,
                    accnt.rpc_consol,
                    accnt.hyperion_accnt_type,
                    loc.consol_member)
          UNION ALL------------------------------FNV----------------------------
          (SELECT   'W0004' view1, sbu.hypn_member format,
                    'FY' || SUBSTR (GLB.period_year, 3, 4) fin_year,
                    'Actual' scenario, 'V2000' VERSION,
                    loc.consol_member entity, DECODE(accnt.fnv_consol,'A7121','M0001','M0003') MOVEMENT,
                    SUBSTR (p_period_name, 1, 3) period,
                    accnt.fnv_consol hyp_accnt, accnt.hyperion_accnt_type,
                    SUM (DECODE (GLB.period_name,
                                 p_period_name, NVL (period_net_dr, 0)
                                  - NVL (period_net_cr, 0)
                                  + NVL (begin_balance_dr, 0)
                                  - NVL (begin_balance_cr, 0),
                                 0
                                )
                        ) DATA
               FROM xxabrl_hyperion_accnt_mapping accnt,
                    gl_code_combinations glcc,
                    XXABRL_GL_BALANCES GLB,
                    xxabrl_hyperion_loc_mapping loc,
                    xxabrl_hyperion_sbu_mapping sbu
              WHERE 1 = 1
                AND GLB.period_name IN (p_period_name, p_first_periond_name)
                AND GLB.code_combination_id = glcc.code_combination_id
                AND loc.ofin_loc_code = glcc.segment4
                AND accnt.ofin_account = glcc.segment6
                AND sbu.ofin_member = glcc.segment3
                AND glcc.summary_flag = 'N'
                AND glcc.template_id IS NULL
                AND accnt.fnv_consol <> 'N'
                AND accnt.fnv_consol IS NOT NULL
                AND loc.consol_member IS NOT NULL
                AND loc.entity_type IN ('FnV')
                AND accnt.hyperion_accnt_type IN
                       ('Owners'' equity', 'Revenue', 'Liability', 'Expense',
                        'Asset')
           GROUP BY sbu.hypn_member,
                    'FY' || SUBSTR (GLB.period_year, 3, 4),
                    glcc.segment5,
                    accnt.fnv_consol,
                    accnt.hyperion_accnt_type,
                    loc.consol_member)
          UNION ALL-----------------------------------CC-----------------------
          (SELECT   'W0004' view1, sbu.hypn_member format,
                    'FY' || SUBSTR (GLB.period_year, 3, 4) fin_year,
                    'Actual' scenario, 'V2000' VERSION,
                    loc.consol_member entity, DECODE(accnt.cc_consol,'A7121','M0001','M0003') MOVEMENT,
                    SUBSTR (p_period_name, 1, 3) period,
                    accnt.cc_consol hyp_accnt, accnt.hyperion_accnt_type,
                    SUM (DECODE (GLB.period_name,
                                 p_period_name, NVL (period_net_dr, 0)
                                  - NVL (period_net_cr, 0)
                                  + NVL (begin_balance_dr, 0)
                                  - NVL (begin_balance_cr, 0),
                                 0
                                )
                        ) DATA
               FROM xxabrl_hyperion_accnt_mapping accnt,
                    gl_code_combinations glcc,
                    XXABRL_GL_BALANCES GLB,
                    xxabrl_hyperion_loc_mapping loc,
                    xxabrl_hyperion_sbu_mapping sbu
              WHERE 1 = 1
                AND GLB.period_name IN (p_period_name, p_first_periond_name)
                AND GLB.code_combination_id = glcc.code_combination_id
                AND loc.ofin_loc_code = glcc.segment4
                AND accnt.ofin_account = glcc.segment6
                AND sbu.ofin_member = glcc.segment3
                AND glcc.summary_flag = 'N'
                AND glcc.template_id IS NULL
                AND accnt.cc_consol <> 'N'
                AND accnt.cc_consol IS NOT NULL
                AND loc.consol_member IS NOT NULL
                AND loc.entity_type IN ('CC')
                AND accnt.hyperion_accnt_type IN
                       ('Owners'' equity', 'Revenue', 'Liability', 'Expense',
                        'Asset')
           GROUP BY sbu.hypn_member,
                    'FY' || SUBSTR (GLB.period_year, 3, 4),
                    glcc.segment5,
                    cc_consol,
                    accnt.hyperion_accnt_type,
                    loc.consol_member)
          UNION ALL-------------------------RDC-----------------------
          (SELECT   'W0004' view1, sbu.hypn_member format,
                    'FY' || SUBSTR (GLB.period_year, 3, 4) fin_year,
                    'Actual' scenario, 'V2000' VERSION,
                    loc.consol_member entity, DECODE(accnt.rdc_consol,'A7121','M0001','M0003') MOVEMENT,
                    SUBSTR (p_period_name, 1, 3) period,
                    accnt.rdc_consol hyp_accnt, accnt.hyperion_accnt_type,
                    SUM (DECODE (GLB.period_name,
                                 p_period_name, NVL (period_net_dr, 0)
                                  - NVL (period_net_cr, 0)
                                  + NVL (begin_balance_dr, 0)
                                  - NVL (begin_balance_cr, 0),
                                 0
                                )
                        ) DATA
               FROM xxabrl_hyperion_accnt_mapping accnt,
                    gl_code_combinations glcc,
                    XXABRL_GL_BALANCES GLB,
                    xxabrl_hyperion_loc_mapping loc,
                    xxabrl_hyperion_sbu_mapping sbu
              WHERE 1 = 1
                AND GLB.period_name IN (p_period_name, p_first_periond_name)
                AND GLB.code_combination_id = glcc.code_combination_id
                AND loc.ofin_loc_code = glcc.segment4
                AND accnt.ofin_account = glcc.segment6
                AND sbu.ofin_member = glcc.segment3
                AND glcc.summary_flag = 'N'
                AND glcc.template_id IS NULL
                AND accnt.rdc_consol <> 'N'
                AND accnt.rdc_consol IS NOT NULL
                AND loc.consol_member IS NOT NULL
                AND loc.entity_type IN ('RDC')
                AND accnt.hyperion_accnt_type IN
                       ('Owners'' equity', 'Revenue', 'Liability', 'Expense',
                        'Asset')
           GROUP BY sbu.hypn_member,
                    'FY' || SUBSTR (GLB.period_year, 3, 4),
                    glcc.segment5,
                    accnt.rdc_consol,
                    accnt.hyperion_accnt_type,
                    loc.consol_member)
          UNION ALL-------------------Office _Super-----------------------
          (SELECT   'W0004' view1, sbu.hypn_member format,
                    'FY' || SUBSTR (GLB.period_year, 3, 4) fin_year,
                    'Actual' scenario, 'V2000' VERSION,
                    loc.consol_member entity, DECODE(accnt.office_super_consol,'A7121','M0001','M0003') MOVEMENT,
                    SUBSTR (p_period_name, 1, 3) period,
                    accnt.office_super_consol hyp_accnt,
                    accnt.hyperion_accnt_type,
                    SUM (DECODE (GLB.period_name,
                                 p_period_name, NVL (period_net_dr, 0)
                                  - NVL (period_net_cr, 0)
                                  + NVL (begin_balance_dr, 0)
                                  - NVL (begin_balance_cr, 0),
                                 0
                                )
                        ) DATA
               FROM xxabrl_hyperion_accnt_mapping accnt,
                    gl_code_combinations glcc,
                    XXABRL_GL_BALANCES GLB,
                    xxabrl_hyperion_loc_mapping loc,
                    xxabrl_hyperion_sbu_mapping sbu
              WHERE 1 = 1
                AND GLB.period_name IN (p_period_name, p_first_periond_name)
                AND GLB.code_combination_id = glcc.code_combination_id
                AND loc.ofin_loc_code = glcc.segment4
                AND accnt.ofin_account = glcc.segment6
                AND sbu.ofin_member = glcc.segment3
                AND glcc.summary_flag = 'N'
                AND glcc.template_id IS NULL
                AND accnt.office_super_consol <> 'N'
                AND accnt.office_super_consol IS NOT NULL
                AND loc.consol_member IS NOT NULL
                AND loc.entity_type IN ('OHDS-SM')
                AND accnt.hyperion_accnt_type IN
                       ('Owners'' equity', 'Revenue', 'Liability', 'Expense',
                        'Asset')
           GROUP BY sbu.hypn_member,
                    'FY' || SUBSTR (GLB.period_year, 3, 4),
                    glcc.segment5,
                    accnt.office_super_consol,
                    accnt.hyperion_accnt_type,
                    loc.consol_member)
          UNION ALL--------------------Office _Hyper-----------------------
          (SELECT   'W0004' view1, sbu.hypn_member format,
                    'FY' || SUBSTR (GLB.period_year, 3, 4) fin_year,
                    'Actual' scenario, 'V2000' VERSION,
                    loc.consol_member entity, DECODE(accnt.office_hyper_consol,'A7121','M0001','M0003') MOVEMENT,
                    SUBSTR (p_period_name, 1, 3) period,
                    accnt.office_hyper_consol hyp_accnt,
                    accnt.hyperion_accnt_type,
                    SUM (DECODE (GLB.period_name,
                                 p_period_name, NVL (period_net_dr, 0)
                                  - NVL (period_net_cr, 0)
                                  + NVL (begin_balance_dr, 0)
                                  - NVL (begin_balance_cr, 0),
                                 0
                                )
                        ) DATA
               FROM xxabrl_hyperion_accnt_mapping accnt,
                    gl_code_combinations glcc,
                    XXABRL_GL_BALANCES GLB,
                    xxabrl_hyperion_loc_mapping loc,
                    xxabrl_hyperion_sbu_mapping sbu
              WHERE 1 = 1
                AND GLB.period_name IN (p_period_name, p_first_periond_name)
                AND GLB.code_combination_id = glcc.code_combination_id
                AND loc.ofin_loc_code = glcc.segment4
                AND accnt.ofin_account = glcc.segment6
                AND sbu.ofin_member = glcc.segment3
                AND glcc.summary_flag = 'N'
                AND glcc.template_id IS NULL
                AND accnt.office_hyper_consol <> 'N'
                AND accnt.office_hyper_consol IS NOT NULL
                AND loc.consol_member IS NOT NULL
                AND loc.entity_type IN ('OHDS-HM')
                AND accnt.hyperion_accnt_type IN
                       ('Owners'' equity', 'Revenue', 'Liability', 'Expense',
                        'Asset')
           GROUP BY sbu.hypn_member,
                    'FY' || SUBSTR (GLB.period_year, 3, 4),
                    glcc.segment5,
                    office_hyper_consol,
                    accnt.hyperion_accnt_type,
                    loc.consol_member)
          UNION ALL--------------------Office _Corporate-----------------------
          (SELECT   'W0004' view1, sbu.hypn_member format,
                    'FY' || SUBSTR (GLB.period_year, 3, 4) fin_year,
                    'Actual' scenario, 'V2000' VERSION,
                    loc.consol_member entity, DECODE(accnt.corp_consol,'A7121','M0001','M0003') MOVEMENT,
                    SUBSTR (p_period_name, 1, 3) period,
                    accnt.corp_consol hyp_accnt, accnt.hyperion_accnt_type,
                    SUM (DECODE (GLB.period_name,
                                 p_period_name, NVL (period_net_dr, 0)
                                  - NVL (period_net_cr, 0)
                                  + NVL (begin_balance_dr, 0)
                                  - NVL (begin_balance_cr, 0),
                                 0
                                )
                        ) DATA
               FROM xxabrl_hyperion_accnt_mapping accnt,
                    gl_code_combinations glcc,
                    XXABRL_GL_BALANCES GLB,
                    xxabrl_hyperion_loc_mapping loc,
                    xxabrl_hyperion_sbu_mapping sbu
              WHERE 1 = 1
                AND GLB.period_name IN (p_period_name, p_first_periond_name)
                AND GLB.code_combination_id = glcc.code_combination_id
                AND loc.ofin_loc_code = glcc.segment4
                AND accnt.ofin_account = glcc.segment6
                AND sbu.ofin_member = glcc.segment3
                AND glcc.summary_flag = 'N'
                AND glcc.template_id IS NULL
                AND accnt.corp_consol <> 'N'
                AND accnt.corp_consol IS NOT NULL
                AND loc.consol_member IS NOT NULL
                AND loc.entity_type IN ('CORP')
                AND accnt.hyperion_accnt_type IN
                       ('Owners'' equity', 'Revenue', 'Liability', 'Expense',
                        'Asset')
           GROUP BY sbu.hypn_member,
                    'FY' || SUBSTR (GLB.period_year, 3, 4),
                    glcc.segment5,
                    accnt.corp_consol,
                    accnt.hyperion_accnt_type,
                    loc.consol_member))
   WHERE DATA <> 0
GROUP BY view1,format,fin_year, scenario,
         VERSION,entity,MOVEMENT,period,hyp_accnt;                
 x_id utl_file.file_type;
     
 BEGIN     
 
  BEGIN
    SELECT DISTINCT SUBSTR (glb.period_year, 3, 4)
      INTO v_finance_year 
      FROM XXABRL_GL_BALANCES glb 
     WHERE period_name = p_period_name;
     EXCEPTION
     WHEN OTHERS THEN 
     v_finance_year:= NULL;
   END;
 
    FOR i IN CUR_PERIOD_NAME LOOP
    BEGIN
    XXABRL_OFIN2_HYPERION_PKG.XXABRL_HYPN_DATA_VALIDAION(p_period_name);
         SELECT 'CONSOL_CONSOL_FY'||v_finance_year||'_'||substr(p_period_name,1,3)||'_'||'V'||p_version_no||'.csv'INTO V_L_FILE FROM dual;
         --SELECT 'XXABRL_HYPN_CONSOL_'||p_period_name||'_DATA'||NVL(TO_CHAR(TO_DATE(sysdate,'DD-MON-YYYY'),'DDMMYY'),TO_CHAR(SYSDATE-3,'DDMMYY'))||'.csv' INTO V_L_FILE FROM dual;
      x_id:=utl_file.fopen('/ebiz/u01/PROD/db/tech_st/10.2.0/appsutil/outbound/PROD_msoradbprod',V_L_FILE,'W');
       FOR r IN CUR_CONSOL(i.period_name) LOOP
        utl_file.put_line(x_id,   r.view1||','||
                                  r.format||','||                                 
                                  r.fin_year||','||
                                  r.scenario||','|| 
                                  r.VERSION||','|| 
                                  r.entity||','||
                                  r.MOVEMENT||','||
                                  r.period||','||
                                  r.hyp_accnt||','||
                                  r.DATA);
                                                  
       END LOOP;
       EXCEPTION 
     WHEN TOO_MANY_ROWS THEN
        dbms_output.PUT_LINE('Too Many Rows');    
     WHEN NO_DATA_FOUND THEN
        dbms_output.PUT_LINE('No Data Found');    
     WHEN utl_file.invalid_path THEN
        RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_path');
     WHEN utl_file.invalid_mode THEN
         RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_mode');
     WHEN utl_file.invalid_filehandle THEN
         RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_filehandle');
     WHEN utl_file.invalid_operation THEN
         RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_operation');
     WHEN utl_file.read_error THEN
         RAISE_APPLICATION_ERROR(-20001, 'utl_file.read_error');
     WHEN utl_file.write_error THEN
         RAISE_APPLICATION_ERROR(-20001, 'utl_file.write_error');
     WHEN utl_file.internal_error THEN
         RAISE_APPLICATION_ERROR(-20001, 'utl_file.internal_error');
     WHEN OTHERS THEN
          dbms_output.put_line(SubStr('Error '||TO_CHAR(SQLCODE)||': '||SQLERRM, 1,255));
    END;    
  END LOOP;        
  utl_file.fclose(x_id);
END XXABRL_HYPN_CONSOL_DATA;



PROCEDURE XXABRL_HYPN_VMERCH_DATA(errbuf out varchar2,
                                  retcode out varchar2,
                                  p_period_name varchar2,
                                  p_version_no number,
                                  p_param_data varchar2)
AS
V_L_FILE         VARCHAR2(200);  
V_FINANCE_YEAR   NUMBER;                                     
/*-------------------------------------------------------------------------*/
  ------------Cursor for getting the Financial first period name------------
/*-------------------------------------------------------------------------*/
                 
  CURSOR CUR_PERIOD_NAME IS 
  SELECT  DISTINCT UPPER(a.period_name) PERIOD_NAME
    FROM  gl_period_statuses a, gl_period_statuses b
   WHERE  a.application_id = 101
     AND  b.application_id = 101
     AND  a.period_type = b.period_type
     AND  a.period_year = b.period_year
     AND  b.period_name = upper(p_period_name)
     AND  a.period_num =
          (SELECT min(c.period_num)
             FROM gl_period_statuses c
            WHERE c.application_id = 101               
              AND c.period_year = a.period_year
              AND c.period_type = a.period_type
         GROUP BY c.period_year);
/*-------------------------------------------------------------------------*/
       ------------Cursor for getting the Marketing DATA ------------
/*-------------------------------------------------------------------------*/        
         
CURSOR CUR_VMERCH(p_first_periond_name VARCHAR2) IS
SELECT   fin_year, scenario, VERSION, vm, entity, view1, period,
         hyp_accnt,
         SUM (DECODE (hyperion_accnt_type,
                      'Owners'' equity', -DATA,
                      'Revenue', -DATA,
                      'Liability', -DATA,
                      'Expense', DATA,
                      'Asset', DATA
                     )
             ) DATA
    FROM ((SELECT   'FY' || SUBSTR (GLB.period_year, 3, 4) fin_year,
                    'Actual' scenario, 'V2000' VERSION, 'VM998' VM,
                    loc.v_merch_member entity, 'YTM' view1,
                    accnt.corp_vmerch  hyp_accnt,
                    accnt.hyperion_accnt_type,
                    SUBSTR (p_period_name, 1, 3) period,
                    SUM (DECODE (GLB.period_name,
                                 p_period_name, NVL (period_net_dr, 0)
                                  - NVL (period_net_cr, 0)
                                  + NVL (begin_balance_dr, 0)
                                  - NVL (begin_balance_cr, 0),
                                 0
                                )
                        ) DATA
               FROM xxabrl_hyperion_accnt_mapping accnt,
                    gl_code_combinations glcc,
                    XXABRL_GL_BALANCES GLB,
                    xxabrl_hyperion_loc_mapping loc
              WHERE 1 = 1
                AND GLB.period_name IN (p_period_name, p_first_periond_name)
                AND GLB.code_combination_id = glcc.code_combination_id
                AND loc.ofin_loc_code = glcc.segment4
                AND accnt.ofin_account = glcc.segment6
                AND glcc.summary_flag = 'N'
                AND glcc.template_id IS NULL
                AND accnt.corp_vmerch <> 'N'
                AND accnt.corp_vmerch IS NOT NULL
                AND loc.v_merch_member IS NOT NULL
                AND entity_type IN ('OHDS-SM','CORP')
                AND accnt.hyperion_accnt_type IN
                       ('Owners'' equity', 'Revenue', 'Liability', 'Expense',
                        'Asset')
           GROUP BY 'FY' || SUBSTR (GLB.period_year, 3, 4),
                    accnt.corp_vmerch ,
                    accnt.hyperion_accnt_type,
                    loc.v_merch_member))
   WHERE DATA <> 0
GROUP BY fin_year,scenario,VERSION,vm,
         entity,view1,period,hyp_accnt;
             
 x_id utl_file.file_type;
     
 BEGIN     
 
  BEGIN
    SELECT DISTINCT SUBSTR (glb.period_year, 3, 4)
      INTO v_finance_year 
      FROM XXABRL_GL_BALANCES glb 
     WHERE period_name = p_period_name;
     EXCEPTION
     WHEN OTHERS THEN 
     v_finance_year:= NULL;
   END;
 
    FOR i IN CUR_PERIOD_NAME LOOP
    BEGIN
      XXABRL_OFIN2_HYPERION_PKG.XXABRL_HYPN_DATA_VALIDAION(p_period_name);
          SELECT 'V_MERCH_VM_FY'||v_finance_year||'_'||substr(p_period_name,1,3)||'_'||'V'||p_version_no||'.csv'INTO V_L_FILE FROM dual;   
      ---SELECT 'XXABRL_HYPN_VMERCH_'||p_period_name||'_DATA'||NVL(TO_CHAR(TO_DATE(sysdate,'DD-MON-YYYY'),'DDMMYY'),TO_CHAR(SYSDATE-3,'DDMMYY'))||'.csv' INTO V_L_FILE FROM dual;
      x_id:=utl_file.fopen('/ebiz/u01/PROD/db/tech_st/10.2.0/appsutil/outbound/PROD_msoradbprod',V_L_FILE,'W');
       FOR r IN CUR_VMERCH(i.period_name) LOOP
        utl_file.put_line(x_id,   r.fin_year||','||
                                  r.scenario||','||                                 
                                  r.version||','||
                                  r.VM||','|| 
                                  r.entity||','|| 
                                  r.view1||','||
                                  r.Period||','||
                                  r.hyp_accnt||','||
                                  r.DATA);
                                  
       END LOOP;
       EXCEPTION 
     WHEN TOO_MANY_ROWS THEN
        dbms_output.PUT_LINE('Too Many Rows');    
     WHEN NO_DATA_FOUND THEN
        dbms_output.PUT_LINE('No Data Found');    
     WHEN utl_file.invalid_path THEN
        RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_path');
     WHEN utl_file.invalid_mode THEN
         RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_mode');
     WHEN utl_file.invalid_filehandle THEN
         RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_filehandle');
     WHEN utl_file.invalid_operation THEN
         RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_operation');
     WHEN utl_file.read_error THEN
         RAISE_APPLICATION_ERROR(-20001, 'utl_file.read_error');
     WHEN utl_file.write_error THEN
         RAISE_APPLICATION_ERROR(-20001, 'utl_file.write_error');
     WHEN utl_file.internal_error THEN
         RAISE_APPLICATION_ERROR(-20001, 'utl_file.internal_error');
     WHEN OTHERS THEN
          dbms_output.put_line(SubStr('Error '||TO_CHAR(SQLCODE)||': '||SQLERRM, 1,255));
    END;        
  END LOOP;        
  utl_file.fclose(x_id);
END XXABRL_HYPN_VMERCH_DATA;

PROCEDURE XXABRL_HYPN_DATA_VALIDAION(p_period_name varchar2)
                    
IS

CURSOR CUR_PERIOD_NAME IS 
  SELECT  DISTINCT UPPER(a.period_name) PERIOD_NAME
    FROM  gl_period_statuses a, gl_period_statuses b
   WHERE  a.application_id = 101
     AND  b.application_id = 101
     AND  a.period_type = b.period_type
     AND  a.period_year = b.period_year
     AND  b.period_name = p_period_name
     AND  a.period_num =
          (SELECT min(c.period_num)
             FROM gl_period_statuses c
            WHERE c.application_id = 101               
              AND c.period_year = a.period_year
              AND c.period_type = a.period_type
         GROUP BY c.period_year);

CURSOR CUR_ACCNT (p_first_period_name VARCHAR2)    
is       
SELECT segment1,segment2,segment3,segment4,
       segment5,segment6,segment7,segment8,
       seg1, seg2, seg3,seg4,seg5,seg6,seg7,seg8,
       data from(
(SELECT   glcc.segment1,glcc.segment2,glcc.segment3,glcc.segment4,
         glcc.segment5,glcc.segment6,glcc.segment7,glcc.segment8,
         (select ffvl.DESCRIPTION 
           from fnd_flex_values_vl ffvl,
                fnd_flex_value_sets ffvs 
          where ffvl.flex_value_set_id = ffvs.flex_value_set_id  
            and flex_value_set_name = 'ABRL_GL_CO' 
            and FLEX_VALUE_MEANING = glcc1.segment1)seg1,
            (select ffvl.DESCRIPTION 
           from fnd_flex_values_vl ffvl,
                fnd_flex_value_sets ffvs 
          where ffvl.flex_value_set_id = ffvs.flex_value_set_id 
          and flex_value_set_name = 'ABRL_GL_CC' 
            and FLEX_VALUE_MEANING = glcc1.segment2)seg2,
            (select ffvl.DESCRIPTION 
           from fnd_flex_values_vl ffvl,
                fnd_flex_value_sets ffvs 
          where ffvl.flex_value_set_id = ffvs.flex_value_set_id 
            and flex_value_set_name = 'ABRL_GL_State_SBU' 
            and FLEX_VALUE_MEANING = glcc1.segment3)seg3,
            (select ffvl.DESCRIPTION 
           from fnd_flex_values_vl ffvl,
                fnd_flex_value_sets ffvs 
          where ffvl.flex_value_set_id = ffvs.flex_value_set_id 
            and flex_value_set_name = 'ABRL_GL_Location' 
            and FLEX_VALUE_MEANING = glcc1.segment4)seg4,
            (select ffvl.DESCRIPTION 
           from fnd_flex_values_vl ffvl,
                fnd_flex_value_sets ffvs 
          where ffvl.flex_value_set_id = ffvs.flex_value_set_id 
            AND flex_value_set_NAME = 'ABRL_GL_Merchandize' 
            and FLEX_VALUE_MEANING = glcc1.segment5)seg5,
            (select ffvl.DESCRIPTION 
           from fnd_flex_values_vl ffvl,
                fnd_flex_value_sets ffvs 
          where ffvl.flex_value_set_id = ffvs.flex_value_set_id
             AND flex_value_set_NAME = 'ABRL_GL_Account' 
            and FLEX_VALUE_MEANING = glcc1.segment6)seg6,
            (select ffvl.DESCRIPTION 
           from fnd_flex_values_vl ffvl,
                fnd_flex_value_sets ffvs 
          where ffvl.flex_value_set_id = ffvs.flex_value_set_id 
            AND flex_value_set_NAME = 'ABRL_GL_State_SBU' 
            and FLEX_VALUE_MEANING = glcc1.segment7)seg7,
            (select ffvl.DESCRIPTION 
           from fnd_flex_values_vl ffvl,
                fnd_flex_value_sets ffvs 
          where ffvl.flex_value_set_id = ffvs.flex_value_set_id 
           and flex_value_set_name = 'ABRL_GL_Future'
            and FLEX_VALUE_MEANING = glcc1.segment8)seg8,      
         SUM (DECODE (GLB.period_name,
                                                  p_period_name, NVL
                                                               (period_net_dr,
                                                                0
                                                               )
                                                   - NVL (period_net_cr, 0)
                                                   + NVL (begin_balance_dr, 0)
                                                   - NVL (begin_balance_cr, 0),
                                                  0
                                                 )
                                         ) DATA
    FROM gl_code_combinations glcc,
         gl_code_combinations glcc1,         
         XXABRL_GL_BALANCES GLB
   WHERE 1 = 1
     AND GLB.period_name IN
            (p_period_name,p_first_period_name)
     AND glcc.code_combination_id =glcc1.code_combination_id             
     AND GLB.code_combination_id = glcc.code_combination_id
     AND glcc.summary_flag = 'N'
     AND glcc.template_id IS NULL     
     and  not exists(select 1 from 
          xxabrl_hyperion_accnt_mapping accnt 
          where accnt.OFIN_account = glcc.segment6)
           group by  glcc.segment1, glcc.segment2, glcc.segment3,
         glcc.segment4, glcc.segment5, glcc.segment6, glcc.segment7,
         glcc.segment8, glcc1.segment1, glcc1.segment2, glcc1.segment3,
         glcc1.segment4, glcc1.segment5, glcc1.segment6, glcc1.segment7,
         glcc1.segment8
         ))where data <> 0;          
CURSOR CUR_LOC(p_first_period_name VARCHAR2)    
IS          
SELECT segment1, segment2, segment3, segment4, segment5,
       segment6, segment7, segment8, seg1, seg2, seg3,
       seg4, seg5, seg6, seg7, seg8, data from(
(SELECT  glcc.segment1,glcc.segment2,glcc.segment3,glcc.segment4,
         glcc.segment5,glcc.segment6,glcc.segment7,glcc.segment8,
         (select ffvl.DESCRIPTION 
           from fnd_flex_values_vl ffvl,
                fnd_flex_value_sets ffvs 
          where ffvl.flex_value_set_id = ffvs.flex_value_set_id  
            and flex_value_set_name = 'ABRL_GL_CO' 
            and FLEX_VALUE_MEANING = glcc1.segment1)seg1,
            (select ffvl.DESCRIPTION 
           from fnd_flex_values_vl ffvl,
                fnd_flex_value_sets ffvs 
          where ffvl.flex_value_set_id = ffvs.flex_value_set_id 
          and flex_value_set_name = 'ABRL_GL_CC' 
            and FLEX_VALUE_MEANING = glcc1.segment2)seg2,
            (select ffvl.DESCRIPTION 
           from fnd_flex_values_vl ffvl,
                fnd_flex_value_sets ffvs 
          where ffvl.flex_value_set_id = ffvs.flex_value_set_id 
            and flex_value_set_name = 'ABRL_GL_State_SBU' 
            and FLEX_VALUE_MEANING = glcc1.segment3)seg3,
            (select ffvl.DESCRIPTION 
           from fnd_flex_values_vl ffvl,
                fnd_flex_value_sets ffvs 
          where ffvl.flex_value_set_id = ffvs.flex_value_set_id 
            and flex_value_set_name = 'ABRL_GL_Location' 
            and FLEX_VALUE_MEANING = glcc1.segment4)seg4,
            (select ffvl.DESCRIPTION 
           from fnd_flex_values_vl ffvl,
                fnd_flex_value_sets ffvs 
          where ffvl.flex_value_set_id = ffvs.flex_value_set_id 
            AND flex_value_set_NAME = 'ABRL_GL_Merchandize' 
            and FLEX_VALUE_MEANING = glcc1.segment5)seg5,
            (select ffvl.DESCRIPTION 
           from fnd_flex_values_vl ffvl,
                fnd_flex_value_sets ffvs 
          where ffvl.flex_value_set_id = ffvs.flex_value_set_id
             AND flex_value_set_NAME = 'ABRL_GL_Account' 
            and FLEX_VALUE_MEANING = glcc1.segment6)seg6,
            (select ffvl.DESCRIPTION 
           from fnd_flex_values_vl ffvl,
                fnd_flex_value_sets ffvs 
          where ffvl.flex_value_set_id = ffvs.flex_value_set_id 
            AND flex_value_set_NAME = 'ABRL_GL_State_SBU' 
            and FLEX_VALUE_MEANING = glcc1.segment7)seg7,
            (select ffvl.DESCRIPTION 
           from fnd_flex_values_vl ffvl,
                fnd_flex_value_sets ffvs 
          where ffvl.flex_value_set_id = ffvs.flex_value_set_id 
           and flex_value_set_name = 'ABRL_GL_Future'
            and FLEX_VALUE_MEANING = glcc1.segment8)seg8,      
         SUM (DECODE (GLB.period_name,
                                                  p_period_name, NVL
                                                               (period_net_dr,
                                                                0
                                                               )
                                                   - NVL (period_net_cr, 0)
                                                   + NVL (begin_balance_dr, 0)
                                                   - NVL (begin_balance_cr, 0),
                                                  0
                                                 )
                                         ) DATA
    FROM gl_code_combinations glcc,
         gl_code_combinations glcc1,         
         XXABRL_GL_BALANCES GLB
   WHERE 1 = 1
     AND GLB.period_name IN
            (p_period_name,p_first_period_name)
     AND glcc.code_combination_id =glcc1.code_combination_id             
     AND GLB.code_combination_id = glcc.code_combination_id
     AND glcc.summary_flag = 'N'
     AND glcc.template_id IS NULL     
     and  not exists(select 1 from 
          xxabrl_hyperion_loc_mapping loc
          where loc.ofin_loc_code = glcc.segment4)
           group by  glcc.segment1,
                     glcc.segment2,
         glcc.segment3, glcc.segment4, glcc.segment5, glcc.segment6,
         glcc.segment7, glcc.segment8, glcc1.segment1,glcc1.segment2,
         glcc1.segment3,glcc1.segment4, glcc1.segment5, glcc1.segment6,
         glcc1.segment7, glcc1.segment8
         ))where data <> 0; 
         
CURSOR CUR_MERCH (p_first_period_name VARCHAR2)    
is          
SELECT segment1, segment2, segment3, segment4,
       segment5, segment6, segment7, segment8,
       seg1, seg2, seg3,seg4,seg5,seg6,seg7,seg8,
       data from(
(SELECT  glcc.segment1,glcc.segment2,glcc.segment3,glcc.segment4,
         glcc.segment5,glcc.segment6,glcc.segment7,glcc.segment8,
         (select ffvl.DESCRIPTION 
           from fnd_flex_values_vl ffvl,
                fnd_flex_value_sets ffvs 
          where ffvl.flex_value_set_id = ffvs.flex_value_set_id  
            and flex_value_set_name = 'ABRL_GL_CO' 
            and FLEX_VALUE_MEANING = glcc1.segment1)seg1,
            (select ffvl.DESCRIPTION 
           from fnd_flex_values_vl ffvl,
                fnd_flex_value_sets ffvs 
          where ffvl.flex_value_set_id = ffvs.flex_value_set_id 
          and flex_value_set_name = 'ABRL_GL_CC' 
            and FLEX_VALUE_MEANING = glcc1.segment2)seg2,
            (select ffvl.DESCRIPTION 
           from fnd_flex_values_vl ffvl,
                fnd_flex_value_sets ffvs 
          where ffvl.flex_value_set_id = ffvs.flex_value_set_id 
            and flex_value_set_name = 'ABRL_GL_State_SBU' 
            and FLEX_VALUE_MEANING = glcc1.segment3)seg3,
            (select ffvl.DESCRIPTION 
           from fnd_flex_values_vl ffvl,
                fnd_flex_value_sets ffvs 
          where ffvl.flex_value_set_id = ffvs.flex_value_set_id 
            and flex_value_set_name = 'ABRL_GL_Location' 
            and FLEX_VALUE_MEANING = glcc1.segment4)seg4,
            (select ffvl.DESCRIPTION 
           from fnd_flex_values_vl ffvl,
                fnd_flex_value_sets ffvs 
          where ffvl.flex_value_set_id = ffvs.flex_value_set_id 
            AND flex_value_set_NAME = 'ABRL_GL_Merchandize' 
            and FLEX_VALUE_MEANING = glcc1.segment5)seg5,
            (select ffvl.DESCRIPTION 
           from fnd_flex_values_vl ffvl,
                fnd_flex_value_sets ffvs 
          where ffvl.flex_value_set_id = ffvs.flex_value_set_id
             AND flex_value_set_NAME = 'ABRL_GL_Account' 
            and FLEX_VALUE_MEANING = glcc1.segment6)seg6,
            (select ffvl.DESCRIPTION 
           from fnd_flex_values_vl ffvl,
                fnd_flex_value_sets ffvs 
          where ffvl.flex_value_set_id = ffvs.flex_value_set_id 
            AND flex_value_set_NAME = 'ABRL_GL_State_SBU' 
            and FLEX_VALUE_MEANING = glcc1.segment7)seg7,
            (select ffvl.DESCRIPTION 
           from fnd_flex_values_vl ffvl,
                fnd_flex_value_sets ffvs 
          where ffvl.flex_value_set_id = ffvs.flex_value_set_id 
           and flex_value_set_name = 'ABRL_GL_Future'
            and FLEX_VALUE_MEANING = glcc1.segment8)seg8,       
         SUM (DECODE (GLB.period_name,
                                                  p_period_name, NVL
                                                               (period_net_dr,
                                                                0
                                                               )
                                                   - NVL (period_net_cr, 0)
                                                   + NVL (begin_balance_dr, 0)
                                                   - NVL (begin_balance_cr, 0),
                                                  0
                                                 )
                                         ) DATA
    FROM gl_code_combinations glcc,
         gl_code_combinations glcc1,         
         XXABRL_GL_BALANCES GLB
   WHERE 1 = 1
     AND GLB.period_name IN
            (p_period_name,p_first_period_name)
     AND glcc.code_combination_id =glcc1.code_combination_id             
     AND GLB.code_combination_id = glcc.code_combination_id
     AND glcc.summary_flag = 'N'
     AND glcc.template_id IS NULL     
     and  not exists(select 1 from 
          xxabrl_hyperion_merch_mapping merch
          where merch.ofin_member = glcc.segment5)
           group by  glcc.segment1, glcc.segment2,glcc.segment3,glcc.segment4,
         glcc.segment5,glcc.segment6,glcc.segment7,glcc.segment8,glcc1.segment1,
         glcc1.segment2,glcc1.segment3,glcc1.segment4,glcc1.segment5,glcc1.segment6,
         glcc1.segment7, glcc1.segment8
         ))where data <> 0;
CURSOR CUR_SBU (p_first_period_name VARCHAR2)    
is          
SELECT segment1, segment2, segment3, segment4,
       segment5, segment6, segment7, segment8,
       seg1, seg2, seg3,seg4,seg5,seg6,seg7,seg8,
       data from(
(SELECT   glcc.segment1,glcc.segment2,glcc.segment3,glcc.segment4,
         glcc.segment5,glcc.segment6,glcc.segment7,glcc.segment8,
         (select ffvl.DESCRIPTION 
           from fnd_flex_values_vl ffvl,
                fnd_flex_value_sets ffvs 
          where ffvl.flex_value_set_id = ffvs.flex_value_set_id  
            and flex_value_set_name = 'ABRL_GL_CO' 
            and FLEX_VALUE_MEANING = glcc1.segment1)seg1,
            (select ffvl.DESCRIPTION 
           from fnd_flex_values_vl ffvl,
                fnd_flex_value_sets ffvs 
          where ffvl.flex_value_set_id = ffvs.flex_value_set_id 
          and flex_value_set_name = 'ABRL_GL_CC' 
            and FLEX_VALUE_MEANING = glcc1.segment2)seg2,
            (select ffvl.DESCRIPTION 
           from fnd_flex_values_vl ffvl,
                fnd_flex_value_sets ffvs 
          where ffvl.flex_value_set_id = ffvs.flex_value_set_id 
            and flex_value_set_name = 'ABRL_GL_State_SBU' 
            and FLEX_VALUE_MEANING = glcc1.segment3)seg3,
            (select ffvl.DESCRIPTION 
           from fnd_flex_values_vl ffvl,
                fnd_flex_value_sets ffvs 
          where ffvl.flex_value_set_id = ffvs.flex_value_set_id 
            and flex_value_set_name = 'ABRL_GL_Location' 
            and FLEX_VALUE_MEANING = glcc1.segment4)seg4,
            (select ffvl.DESCRIPTION 
           from fnd_flex_values_vl ffvl,
                fnd_flex_value_sets ffvs 
          where ffvl.flex_value_set_id = ffvs.flex_value_set_id 
            AND flex_value_set_NAME = 'ABRL_GL_Merchandize' 
            and FLEX_VALUE_MEANING = glcc1.segment5)seg5,
            (select ffvl.DESCRIPTION 
           from fnd_flex_values_vl ffvl,
                fnd_flex_value_sets ffvs 
          where ffvl.flex_value_set_id = ffvs.flex_value_set_id
             AND flex_value_set_NAME = 'ABRL_GL_Account' 
            and FLEX_VALUE_MEANING = glcc1.segment6)seg6,
            (select ffvl.DESCRIPTION 
           from fnd_flex_values_vl ffvl,
                fnd_flex_value_sets ffvs 
          where ffvl.flex_value_set_id = ffvs.flex_value_set_id 
            AND flex_value_set_NAME = 'ABRL_GL_State_SBU' 
            and FLEX_VALUE_MEANING = glcc1.segment7)seg7,
            (select ffvl.DESCRIPTION 
           from fnd_flex_values_vl ffvl,
                fnd_flex_value_sets ffvs 
          where ffvl.flex_value_set_id = ffvs.flex_value_set_id 
           and flex_value_set_name = 'ABRL_GL_Future'
            and FLEX_VALUE_MEANING = glcc1.segment8)seg8,        
         SUM (DECODE (GLB.period_name,
                                                  p_period_name, NVL
                                                               (period_net_dr,
                                                                0
                                                               )
                                                   - NVL (period_net_cr, 0)
                                                   + NVL (begin_balance_dr, 0)
                                                   - NVL (begin_balance_cr, 0),
                                                  0
                                                 )
                                         ) DATA
    FROM gl_code_combinations glcc,
         gl_code_combinations glcc1,         
         XXABRL_GL_BALANCES GLB
   WHERE 1 = 1
     AND GLB.period_name IN
            (p_period_name,p_first_period_name)
     AND glcc.code_combination_id =glcc1.code_combination_id             
     AND GLB.code_combination_id = glcc.code_combination_id
     AND glcc.summary_flag = 'N'
     AND glcc.template_id IS NULL     
     and  not exists(select 1 from 
          xxabrl_hyperion_sbu_mapping sbu
          where sbu.ofin_member = glcc.segment3)
           group by  glcc.segment1, glcc.segment2,glcc.segment3,glcc.segment4,
         glcc.segment5,glcc.segment6,glcc.segment7,glcc.segment8,glcc1.segment1,
         glcc1.segment2,glcc1.segment3,glcc1.segment4,glcc1.segment5,glcc1.segment6,
         glcc1.segment7, glcc1.segment8
         ))where data <> 0;

CURSOR CUR_CC (p_first_period_name VARCHAR2)    
is          
SELECT segment1, segment2, segment3, segment4,
       segment5, segment6, segment7, segment8,
       seg1, seg2, seg3,seg4,seg5,seg6,seg7,seg8,
       data from(
(SELECT   glcc.segment1,glcc.segment2,glcc.segment3,glcc.segment4,
         glcc.segment5,glcc.segment6,glcc.segment7,glcc.segment8,
         (select ffvl.DESCRIPTION 
           from fnd_flex_values_vl ffvl,
                fnd_flex_value_sets ffvs 
          where ffvl.flex_value_set_id = ffvs.flex_value_set_id  
            and flex_value_set_name = 'ABRL_GL_CO' 
            and FLEX_VALUE_MEANING = glcc1.segment1)seg1,
            (select ffvl.DESCRIPTION 
           from fnd_flex_values_vl ffvl,
                fnd_flex_value_sets ffvs 
          where ffvl.flex_value_set_id = ffvs.flex_value_set_id 
          and flex_value_set_name = 'ABRL_GL_CC' 
            and FLEX_VALUE_MEANING = glcc1.segment2)seg2,
            (select ffvl.DESCRIPTION 
           from fnd_flex_values_vl ffvl,
                fnd_flex_value_sets ffvs 
          where ffvl.flex_value_set_id = ffvs.flex_value_set_id 
            and flex_value_set_name = 'ABRL_GL_State_SBU' 
            and FLEX_VALUE_MEANING = glcc1.segment3)seg3,
            (select ffvl.DESCRIPTION 
           from fnd_flex_values_vl ffvl,
                fnd_flex_value_sets ffvs 
          where ffvl.flex_value_set_id = ffvs.flex_value_set_id 
            and flex_value_set_name = 'ABRL_GL_Location' 
            and FLEX_VALUE_MEANING = glcc1.segment4)seg4,
            (select ffvl.DESCRIPTION 
           from fnd_flex_values_vl ffvl,
                fnd_flex_value_sets ffvs 
          where ffvl.flex_value_set_id = ffvs.flex_value_set_id 
            AND flex_value_set_NAME = 'ABRL_GL_Merchandize' 
            and FLEX_VALUE_MEANING = glcc1.segment5)seg5,
            (select ffvl.DESCRIPTION 
           from fnd_flex_values_vl ffvl,
                fnd_flex_value_sets ffvs 
          where ffvl.flex_value_set_id = ffvs.flex_value_set_id
             AND flex_value_set_NAME = 'ABRL_GL_Account' 
            and FLEX_VALUE_MEANING = glcc1.segment6)seg6,
            (select ffvl.DESCRIPTION 
           from fnd_flex_values_vl ffvl,
                fnd_flex_value_sets ffvs 
          where ffvl.flex_value_set_id = ffvs.flex_value_set_id 
            AND flex_value_set_NAME = 'ABRL_GL_State_SBU' 
            and FLEX_VALUE_MEANING = glcc1.segment7)seg7,
            (select ffvl.DESCRIPTION 
           from fnd_flex_values_vl ffvl,
                fnd_flex_value_sets ffvs 
          where ffvl.flex_value_set_id = ffvs.flex_value_set_id 
           and flex_value_set_name = 'ABRL_GL_Future'
            and FLEX_VALUE_MEANING = glcc1.segment8)seg8,        
         SUM (DECODE (GLB.period_name,
                                                  p_period_name, NVL
                                                               (period_net_dr,
                                                                0
                                                               )
                                                   - NVL (period_net_cr, 0)
                                                   + NVL (begin_balance_dr, 0)
                                                   - NVL (begin_balance_cr, 0),
                                                  0
                                                 )
                                         ) DATA
    FROM gl_code_combinations glcc,
         gl_code_combinations glcc1,         
         XXABRL_GL_BALANCES GLB
   WHERE 1 = 1
     AND GLB.period_name IN
            (p_period_name,p_first_period_name)
     AND glcc.code_combination_id =glcc1.code_combination_id             
     AND GLB.code_combination_id = glcc.code_combination_id
     AND glcc.summary_flag = 'N'
     AND glcc.template_id IS NULL     
     and  not exists(select 1 from 
          xxabrl_hyperion_cc_mapping cc
          where cc.ofin_member = glcc.segment2)
           group by  glcc.segment1, glcc.segment2,glcc.segment3,glcc.segment4,
         glcc.segment5,glcc.segment6,glcc.segment7,glcc.segment8,glcc1.segment1,
         glcc1.segment2,glcc1.segment3,glcc1.segment4,glcc1.segment5,glcc1.segment6,
         glcc1.segment7, glcc1.segment8
         ))where data <> 0;
           
BEGIN
FOR i IN CUR_PERIOD_NAME LOOP

  FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'List of accounts not available in Hyperion');  
  FND_FILE.PUT_LINE(FND_FILE.OUTPUT,' ');
  FOR j IN CUR_ACCNT(i.period_name) LOOP
   FND_FILE.PUT_LINE(FND_FILE.OUTPUT,j.segment1|| CHR (9)||
                                     j.segment2|| CHR (9)||
                                     j.segment3|| CHR (9)||
                                     j.segment4|| CHR (9)||
                                     j.segment5|| CHR (9)||
                                     j.segment6|| CHR (9)||
                                     j.segment7|| CHR (9)||
                                     j.segment8|| CHR (9)||
                                     j.seg1|| CHR (9)||
                                     j.seg2|| CHR (9)||
                                     j.seg3|| CHR (9)||
                                     j.seg4|| CHR (9)||
                                     j.seg5|| CHR (9)||
                                     j.seg6|| CHR (9)||
                                     j.seg7|| CHR (9)||
                                     j.seg8|| CHR (9)||
                                     j.data);
 END LOOP;
 FND_FILE.PUT_LINE(FND_FILE.OUTPUT,' ');
 FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'List of Locations not available in Hyperion'); 
 FND_FILE.PUT_LINE(FND_FILE.OUTPUT,' '); 
 FOR k IN CUR_LOC(i.period_name) LOOP
   --FND_FILE.PUT_LINE(FND_FILE.OUTPUT,k.code_combination|| CHR (9)||k.Description|| CHR (9)||k.data);
   FND_FILE.PUT_LINE(FND_FILE.OUTPUT,k.segment1|| CHR (9)||
                                     k.segment2|| CHR (9)||
                                     k.segment3|| CHR (9)||
                                     k.segment4|| CHR (9)||
                                     k.segment5|| CHR (9)||
                                     k.segment6|| CHR (9)||
                                     k.segment7|| CHR (9)||
                                     k.segment8|| CHR (9)||
                                     k.seg1|| CHR (9)||
                                     k.seg2|| CHR (9)||
                                     k.seg3|| CHR (9)||
                                     k.seg4|| CHR (9)||
                                     k.seg5|| CHR (9)||
                                     k.seg6|| CHR (9)||
                                     k.seg7|| CHR (9)||
                                     k.seg8|| CHR (9)||
                                     k.data);
 END LOOP;
 FND_FILE.PUT_LINE(FND_FILE.OUTPUT,' ');
 FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'List of mechandise not available in Hyperion'); 
 FND_FILE.PUT_LINE(FND_FILE.OUTPUT,' '); 
 FOR l IN CUR_MERCH(i.period_name) LOOP
   --FND_FILE.PUT_LINE(FND_FILE.OUTPUT,k.code_combination|| CHR (9)||k.Description|| CHR (9)||k.data);
   FND_FILE.PUT_LINE(FND_FILE.OUTPUT,l.segment1|| CHR (9)||
                                     l.segment2|| CHR (9)||
                                     l.segment3|| CHR (9)||
                                     l.segment4|| CHR (9)||
                                     l.segment5|| CHR (9)||
                                     l.segment6|| CHR (9)||
                                     l.segment7|| CHR (9)||
                                     l.segment8|| CHR (9)||
                                     l.seg1|| CHR (9)||
                                     l.seg2|| CHR (9)||
                                     l.seg3|| CHR (9)||
                                     l.seg4|| CHR (9)||
                                     l.seg5|| CHR (9)||
                                     l.seg6|| CHR (9)||
                                     l.seg7|| CHR (9)||
                                     l.seg8|| CHR (9)||
                                     l.data);
 END LOOP;
 FND_FILE.PUT_LINE(FND_FILE.OUTPUT,' ');
 FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'List of SBU not available in Hyperion'); 
 FND_FILE.PUT_LINE(FND_FILE.OUTPUT,' '); 
 FOR m IN CUR_SBU(i.period_name) LOOP
   --FND_FILE.PUT_LINE(FND_FILE.OUTPUT,k.code_combination|| CHR (9)||k.Description|| CHR (9)||k.data);
   FND_FILE.PUT_LINE(FND_FILE.OUTPUT,m.segment1|| CHR (9)||
                                     m.segment2|| CHR (9)||
                                     m.segment3|| CHR (9)||
                                     m.segment4|| CHR (9)||
                                     m.segment5|| CHR (9)||
                                     m.segment6|| CHR (9)||
                                     m.segment7|| CHR (9)||
                                     m.segment8|| CHR (9)||
                                     m.seg1|| CHR (9)||
                                     m.seg2|| CHR (9)||
                                     m.seg3|| CHR (9)||
                                     m.seg4|| CHR (9)||
                                     m.seg5|| CHR (9)||
                                     m.seg6|| CHR (9)||
                                     m.seg7|| CHR (9)||
                                     m.seg8|| CHR (9)||
                                     m.data);
 END LOOP;
 FND_FILE.PUT_LINE(FND_FILE.OUTPUT,' ');
 FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'List of CC not available in Hyperion'); 
 FND_FILE.PUT_LINE(FND_FILE.OUTPUT,' '); 
 FOR n IN CUR_CC(i.period_name) LOOP
   --FND_FILE.PUT_LINE(FND_FILE.OUTPUT,k.code_combination|| CHR (9)||k.Description|| CHR (9)||k.data);
   FND_FILE.PUT_LINE(FND_FILE.OUTPUT,n.segment1|| CHR (9)||
                                     n.segment2|| CHR (9)||
                                     n.segment3|| CHR (9)||
                                     n.segment4|| CHR (9)||
                                     n.segment5|| CHR (9)||
                                     n.segment6|| CHR (9)||
                                     n.segment7|| CHR (9)||
                                     n.segment8|| CHR (9)||
                                     n.seg1|| CHR (9)||
                                     n.seg2|| CHR (9)||
                                     n.seg3|| CHR (9)||
                                     n.seg4|| CHR (9)||
                                     n.seg5|| CHR (9)||
                                     n.seg6|| CHR (9)||
                                     n.seg7|| CHR (9)||
                                     n.seg8|| CHR (9)||
                                     n.data);
 END LOOP;
 
END LOOP;
END XXABRL_HYPN_DATA_VALIDAION;

END XXABRL_OFIN2_HYPERION_PKG; 
/


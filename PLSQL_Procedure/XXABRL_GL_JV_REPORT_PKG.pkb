CREATE OR REPLACE PACKAGE BODY APPS.xxabrl_gl_jv_report_pkg
AS
PROCEDURE xxabrl_gl_jv_report_proc(ERRBUFF OUT VARCHAR2,
                                RETCODE OUT NUMBER,
                               P_SOURCE   VARCHAR2,
                               P_FROM_SBU NUMBER,
                               P_TO_SBU  NUMBER,
                               -- P_FROM_LOCATION NUMBER,
                               -- P_TO_LOCATION NUMBER,
                              P_FROM_ACCOUNT NUMBER,
                              P_TO_ACCOUNT NUMBER,
                              P_FROM_GL_DATE varchar2,
                              P_TO_GL_DATE varchar2,
                              P_FROM_DOC_NO NUMBER,
                              P_TO_DOC_NO   NUMBER
                              )
IS
       query_string             LONG;
       --order_by_string          LONG;
       source_string             LONG;
      -- inv_count                NUMBER;
       sbu_string               LONG;
       --location_string          LONG;
       gl_account_string        LONG;
       gl_date_string           LONG;
       gl_document_no_string    LONG;
      -- closing_balance         NUMBER;
     -- cr_total                 NUMBER;
     -- dr_total                 NUMBER;
TYPE ref_cur IS REF CURSOR;
      c                        ref_cur;
      TYPE c_rec IS RECORD (
                            v_batch_name           gl_je_batches.NAME%TYPE,
                            v_SOURCE               gl_je_headers.je_source%TYPE,
                            v_CATEGORY             gl_je_headers.je_category%TYPE,
                            v_batch_status        VARCHAR2(100),
                            v_gl_date              gl_je_headers.default_effective_date%TYPE,
                            --v_line_number          gl_je_lines.je_line_num%TYPE,
                            v_account_code         gl_code_combinations_kfv.concatenated_segments%TYPE,
                            --v_SBU_desc             VARCHAR2(250),
                            --v_Location_desc        VARCHAR2(250),
                            v_account_desc         VARCHAR2(250),
                            v_dr_amount            gl_je_lines.accounted_dr%TYPE,
                            v_cr_amount            gl_je_lines.accounted_cr%TYPE,
                            v_journal_description  gl_je_lines.description%TYPE,
                            --v_Customer_Number      VARCHAR2(250),
                           -- v_Customer_Name        VARCHAR2(250),
                            v_document_number      gl_je_headers.doc_sequence_value%TYPE,
                            v_Created_By          fnd_user.user_name%TYPE
         );
      v_rec                    c_rec;
    V_LEDGER_ID NUMBER;
    V_RESP_ID NUMBER := Fnd_Profile.VALUE('RESP_ID');
    V_LEDGER_COND VARCHAR2(1000);
    BEGIN
  query_string :=
  'SELECT GLB.NAME batch_name,
       glh.je_source SOURCE, glh.je_category CATEGORY,
       DECODE (GLB.status,
               ''P'', ''Posted'',
               ''U'', ''Unposted'',
               GLB.status
              ) batch_status,
       glh.default_effective_date gl_date,
       --gll.je_line_num line_number,
       gl.concatenated_segments account_code,
       (SELECT FFVL.description FROM APPS.fnd_flex_values_vl ffvl
        WHERE ffvl.FLEX_VALUE_SET_ID=1013469
        AND ffvl.FLEX_VALUE=gl.SEGMENT6) account_desc,
      /* (SELECT FFVL.description FROM APPS.fnd_flex_values_vl ffvl
        WHERE ffvl.FLEX_VALUE_SET_ID=1013466
        AND ffvl.FLEX_VALUE=gl.SEGMENT3) SBU_desc,
         (SELECT FFVL.description FROM APPS.fnd_flex_values_vl ffvl
        WHERE ffvl.FLEX_VALUE_SET_ID=1013467
        AND ffvl.FLEX_VALUE=gl.SEGMENT4) Location_desc,*/
       GLL.ACCOUNTED_DR dr_amount,
      GLL.ACCOUNTED_CR cr_amount,
      gll.description journal_description,
     -- NULL customer_name,
     -- NULL customer_Number,
       glh.doc_sequence_value document_number,
       fu.user_name Created_By
     --gl.segment3 SBU,
     --gl.segment4 LOCATION,
     --gl.segment6 GL_account,
     --GLH.LEDGER_ID LEDGER_ID
FROM
       APPS.gl_je_headers glh,
       APPS.gl_je_lines gll,
       APPS.gl_code_combinations_kfv gl,
       APPS.fnd_user fu,
       APPS.gl_je_batches GLB
WHERE glh.je_header_id = gll.je_header_id
  AND gl.code_combination_id = gll.code_combination_id
  AND glh.je_batch_id = GLB.je_batch_id
  AND gll.created_by=fu.user_id';
  
 BEGIN
 SELECT DEFAULT_LEDGER_ID INTO V_LEDGER_ID
     FROM FND_PROFILE_OPTION_VALUES fpop,
      FND_PROFILE_OPTIONS fpo     ,
      gl_access_sets GAS
      WHERE --level_value=51664--cp_resp_id  --52299 -- resp id
      fpop.PROFILE_OPTION_ID =fpo.PROFILE_OPTION_ID
      AND GAS.ACCESS_SET_ID=FPOP.profile_option_value
        AND level_value=V_RESP_ID --51664
      AND PROFILE_OPTION_NAME='GL_ACCESS_SET_ID';

      EXCEPTION
      WHEN OTHERS THEN
      Fnd_File.put_line (Fnd_File.LOG,'NO LEDGER FOUND. ');
      V_LEDGER_ID:=0;

END;
  
source_string:= ' AND glh.je_source = '''|| P_SOURCE||'''';
sbu_string:= ' AND gl.segment3 BETWEEN '''||P_FROM_SBU||''' AND '''||P_TO_SBU||'''';
gl_account_string:=' AND gl.segment6 BETWEEN '''||P_FROM_ACCOUNT||''' AND '''||P_TO_ACCOUNT||'''';
gl_date_string:=' AND glh.default_effective_date BETWEEN '''||to_date(P_FROM_GL_DATE,'YYYY/MM/DD HH24:MI:SS')||''' AND '''||to_date(P_TO_GL_DATE,'YYYY/MM/DD HH24:MI:SS')||'''';
--gl_date_string:=' AND glh.default_effective_date BETWEEN '''||to_date(P_FROM_GL_DATE,'DD-MON-YYYY')||''' AND '''||to_date(P_TO_GL_DATE,'DD-MON-YYYY')||'''';
--gl_date_string:=' AND to_date(to_char(glh.default_effective_date,''DD-MON-YY'')) BETWEEN '''||P_FROM_GL_DATE||''' AND '''||P_TO_GL_DATE||'''';
gl_document_no_string:='AND glh.doc_sequence_value BETWEEN '''||P_FROM_DOC_NO||''' AND '''||P_TO_DOC_NO||'''';
V_LEDGER_COND := ' AND GLH.LEDGER_ID = '||V_LEDGER_ID;

  -- ADDING LEDGER ID CONDITION
  query_string:=query_string|| V_LEDGER_COND;


-- Checking the Source Parameter
 IF(P_SOURCE IS NULL) THEN
         query_string:=query_string;
         ELSE
         query_string :=query_string||source_string;
         END IF;
 -- Checking For the from SBU and To SBU Parameter
IF (P_FROM_SBU IS NULL AND P_TO_SBU IS NULL)
      THEN
         query_string := query_string;
     ELSE
         query_string := query_string ||sbu_string;
      END IF;
-- Checking For the from GL ACCOUNT and To GL ACCOUNT Parameter
IF (P_FROM_ACCOUNT IS NULL AND P_TO_ACCOUNT IS NULL)
      THEN
         query_string := query_string;
     ELSE
         query_string := query_string ||gl_account_string;
      END IF;
-- Checking GL Date Parameter
IF (P_FROM_GL_DATE IS NOT NULL AND P_TO_GL_DATE IS NOT NULL)
      THEN
         query_string := query_string || gl_date_string;
      ELSIF (P_FROM_GL_DATE IS NULL AND P_TO_GL_DATE IS NULL)
      THEN
         query_string := query_string;
      END IF;
--checking Document number parameter
IF (P_FROM_DOC_NO IS NULL AND P_TO_DOC_NO IS NULL)
      THEN
         query_string := query_string;
     ELSE
         query_string := query_string ||gl_document_no_string;
      END IF;
      
 Fnd_File.put_line (Fnd_File.LOG,
                         'string: '||query_string
                        );
      Fnd_File.put_line (Fnd_File.output,
                         'ABRL General Ledger JV Report'|| CHR (9)
                        );
      Fnd_File.put_line (Fnd_File.output, ' ');
      Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'As on Date'
                         || CHR (9)
                         || SYSDATE
                         || CHR (9)
                        );
      Fnd_File.put_line (Fnd_File.output, ' ');
      Fnd_File.put_line (Fnd_File.output, 'REPORT PARAMETRS ');
   Fnd_File.put_line (Fnd_File.output, ' ');
Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Source'
                         || CHR (9)
                         || P_source
                         || CHR (9)
                        );
Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'From SBU'
                         || CHR (9)
                         || P_FROM_SBU
                         || CHR (9)
                        );
        Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'To SBU'
                         || CHR (9)
                         || P_TO_SBU
                         || CHR (9)
                        );
         Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'From GL Account'
                         || CHR (9)
                         || P_FROM_ACCOUNT
                         || CHR (9)
                        );
        Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'To GL Account'
                         || CHR (9)
                         || P_TO_ACCOUNT
                         || CHR (9)
                        );
       Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'From GL Date'
                         || CHR (9)
                         || P_FROM_GL_DATE
                         || CHR (9)
                        );
        Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'To GL Date'
                         || CHR (9)
                         || P_TO_GL_DATE
                         || CHR (9)
                        );
Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Document Seq Num From'
                         || CHR (9)
                         || P_FROM_DOC_NO
                         || CHR (9)
                        );
        Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Document Seq Num To'
                         || CHR (9)
                         || P_TO_DOC_NO
                         || CHR (9)
                        );
Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Ledger ID'
                         || CHR (9)
                         || V_LEDGER_ID
                         || CHR (9)
                        );

----display fields
 Fnd_File.put_line (Fnd_File.output, ' ');
      Fnd_File.put_line (Fnd_File.output,
                           'Batch Name'
                         || CHR (9)
                         || 'Source'
                         || CHR (9)
                         || 'Category'
                         || CHR (9)
                         || 'Batch Status'
                         ||  CHR (9)
                         || 'GL Date'
                         || CHR (9)
                         || 'Account Code'
                         ||  CHR (9)
                         || 'Account_Desc'
                         ||  CHR (9)
                         || 'Dr Amount'
                         || CHR (9)
                         || 'Cr Amount'
                         || CHR (9)
                         || 'Journal Description'
                         || CHR (9)
                         || 'Document Number'
                         ||  CHR (9)
                         || 'Created by'
                        );
                  -- query_string:=query_string;
                   
         OPEN c
       FOR query_string;
      LOOP
         FETCH c
          INTO v_rec;
         EXIT WHEN c%NOTFOUND;
         Fnd_File.put_line (Fnd_File.output,
                             v_rec.v_batch_name
                            || CHR (9)
                            || v_rec.v_SOURCE
                            || CHR (9)
                            || v_rec.v_CATEGORY
                            || CHR (9)
                            || v_rec.v_batch_status
                            || CHR (9)
                            || v_rec.v_gl_date
                            || CHR (9)
                            || v_rec.v_account_code
                            || CHR (9)
                            || v_rec.v_account_desc
                            || CHR (9)
                            || v_rec.v_dr_amount
                            || CHR (9)
                            || v_rec.v_cr_amount
                            || CHR (9)
                            || v_rec.v_journal_description
                            || CHR (9)
                            ||v_rec.v_document_number
                            || CHR (9)
                            || v_rec.v_Created_By
                            );
Fnd_File.put_line (Fnd_File.LOG,
                         'string: '||query_string
                        );

END LOOP;
CLOSE c;
END xxabrl_gl_jv_report_proc;
END xxabrl_gl_jv_report_pkg; 
/


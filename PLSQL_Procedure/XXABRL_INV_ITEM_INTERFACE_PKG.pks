CREATE OR REPLACE PACKAGE APPS.xxabrl_inv_item_interface_pkg
AS
 /*
  =======================================================================
  ||   filename   : xxabrl_inv_item_interface_pkg
  ||   description : Items migration script
  ||
  ||   version     date            author           modification
  ||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~   ~~~~~~~~~~~~~~~
  ||   1.0.0       25-May-2010   praveen kumar    new development
  ||   1.0.1       10-JUN-2010   praveen/Sai     New Custom Table Created.Hard Coded Org Values Removed.
 ========================================================================*/
   PROCEDURE xxabrl_item_main (
      errbuf     OUT      VARCHAR2,
      retcode    OUT      VARCHAR2,
      p_action   IN       VARCHAR2
   );

   PROCEDURE xxabrl_item_validate;

   PROCEDURE xxabrl_item_validate_insert;
END xxabrl_inv_item_interface_pkg; 
/


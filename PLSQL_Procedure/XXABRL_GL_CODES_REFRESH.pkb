CREATE OR REPLACE PACKAGE BODY APPS.xxabrl_gl_codes_refresh
IS
/*=========================================================================================================
||   Program Name    : XXABRL GL Accounts Outbound Refresh
||   Description : This Program is used for to send the gl accounts info to users
||
||     Version                        Date                             Author                                Modification
||  ~~~~~~~~             ~~~~~~~~~~~~           ~~~~~~~~~~~~~~~~~~          ~~~~~~~~~~~~~~~~~
|| 11.2.0.4.0                 15-Mar-2019               Lokesh Poojari
||  ~~~~~~~~             ~~~~~~~~~~~~           ~~~~~~~~~~~~~~~~~~          ~~~~~~~~~~~~~~~~~
||
============================================================================================================*/
   PROCEDURE xxabrl_main_pkg (
      errbuf    OUT      VARCHAR2,
      retcode   OUT      NUMBER,
      p_type    IN       VARCHAR2
   )
   IS
   BEGIN
      IF p_type = 'SBU'
      THEN
         xxabrl_ou_sbu_refresh;
      END IF;

      IF p_type = 'LOC'
      THEN
         xxabrl_locations_refresh;
      END IF;

      IF p_type = 'CC'
      THEN
         xxabrl_cc_refresh;
      END IF;
   END xxabrl_main_pkg;

   PROCEDURE xxabrl_ou_sbu_refresh
   IS
   BEGIN
      DBMS_SNAPSHOT.REFRESH ('XXABRL_OU_SBU_DESC_MV', 'A');
      fnd_file.put_line (fnd_file.LOG, 'SBU And ORG Refreshed Successfully');
   EXCEPTION
      WHEN OTHERS
      THEN
         fnd_file.put_line (fnd_file.LOG,
                            'Error=>' || SQLCODE || 'ErrorMsg=>' || SQLERRM
                           );
   END xxabrl_ou_sbu_refresh;

   PROCEDURE xxabrl_locations_refresh
   IS
   BEGIN
      DBMS_SNAPSHOT.REFRESH ('XXABRL_LOCATIONS_DESC_MV', 'A');
      fnd_file.put_line (fnd_file.LOG, 'GL Locatons Refreshed Successfully');
   EXCEPTION
      WHEN OTHERS
      THEN
         fnd_file.put_line (fnd_file.LOG,
                            'Error=>' || SQLCODE || 'ErrorMsg=>' || SQLERRM
                           );
   END xxabrl_locations_refresh;

   PROCEDURE xxabrl_cc_refresh
   IS
   BEGIN
      DBMS_SNAPSHOT.REFRESH ('XXABRL_CC_DESC_MV', 'A');
      fnd_file.put_line (fnd_file.LOG, 'GL CC Refreshed Successfully');
   EXCEPTION
      WHEN OTHERS
      THEN
         fnd_file.put_line (fnd_file.LOG,
                            'Error=>' || SQLCODE || 'ErrorMsg=>' || SQLERRM
                           );
   END xxabrl_cc_refresh;
END xxabrl_gl_codes_refresh; 
/


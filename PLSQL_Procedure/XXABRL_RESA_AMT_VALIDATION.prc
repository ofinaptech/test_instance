CREATE OR REPLACE PROCEDURE APPS.xxabrl_resa_amt_validation (
   errbuf           OUT      VARCHAR2,
   retcode          OUT      VARCHAR2,
   --P_ORG_CODE       IN  VARCHAR2,
   p_batch_source   IN       VARCHAR2
)
IS
/*****************************************************************************************************************
                 OBJECT INFORMATION

 Object Name : XXABRL_NAV_AMT_VALIDATION

 Description : Procedure to Check the sales,Distribution and Receipt Amounts are same for gl date with store wise

  Change Record:
 =================================================================================================================
 Version    Date            Author        Remarks
 =======   ==========   =============     ========================================================================
 1.0.0     22-Feb-10    Praveen Kumar     New Development
 1.0.1    08-Mar-10     Praveen Kumar     Org Code Parameter has been Commneted to Schedule from Corp Office OU
 1.0.2    18-May-10    Praveen Kumar      Validation has been changed to from counted amount to Tender Amount
 1.0.3    24-May-10    Praveen Kumar      Validation AMount has been changed from 1 Rs To 3 Rs.
 1.0.4    07-Jul-10    Sayikrishna        Line level ,distribution and Receipt Amount feild is Changed from TENDER Amount
 1.0.5    26-Jul-10    Sayikrishna        Line level ,distribution and Receipt Amount feild is Changed to TENDER Amount
                                          Resa is Different from Navision,E-retail.
 1.0.6    11-Nov-2010  Govindraj T        As per the discussion on 10-Nov-2010, To update the flag as 'T' for the customer '2202041'

******************************************************************************************************************/
   CURSOR c2 (                            ---cp_org_code            VARCHAR2,
              cp_batch_source_name VARCHAR2
                                           --cp_gl_date             DATE
   )
   IS
      SELECT   nril.org_code, gl_date, customer_name
          FROM apps.xxabrl_navi_ar_int_line_stg2 nril
         WHERE UPPER (NVL (interfaced_flag, 'I')) IN ('I', 'T')
           AND UPPER (NVL (interface_line_context, cp_batch_source_name)) =
                                                  UPPER (cp_batch_source_name)
           AND TRUNC (gl_date) >= '1-apr-17'    --added by govind on 27-oct-17
      ---AND upper(NVL (org_code, cp_org_code)) = upper(cp_org_code)
      GROUP BY nril.org_code, gl_date, customer_name;

   CURSOR xx_update_tender
   IS
--      SELECT ROWID, s.*
--        FROM xxabrl.xxabrl_update_actual_tender s
--       WHERE 1 = 1 AND NVL (status_flag, 'N') IN ('N', 'E');
--
      SELECT   store_code, tender_code, tender_description,
               SUM (counted_amount) counted_amount,
               SUM (deposited_amount) deposited_amount, ofin_code,
               transaction_date
          FROM xxabrl.xxabrl_update_actual_tender
         WHERE 1 = 1 AND NVL (status_flag, 'N') IN ('N', 'E')
      GROUP BY store_code,
               tender_code,
               tender_description,
               ofin_code,
               transaction_date
      ORDER BY transaction_date;

   v_line_amount       NUMBER          := 0;
   v_dist_amount       NUMBER          := 0;
   v_rec_amount        NUMBER          := 0;
   v_line_tend_amt     NUMBER          := 0;
   v_dist_tend_amt     NUMBER          := 0;
   v_rec_tend_amt      NUMBER          := 0;
   v_tot_rec_count     NUMBER          := 0;
   v_tot_valid_count   NUMBER          := 0;
   v_tot_err_count     NUMBER          := 0;
   v_nature            VARCHAR2 (1000);
   v_tran_type         VARCHAR2 (1000);
   v_description       VARCHAR2 (1000);
   v_er_tender_type    VARCHAR2 (1000);
   error_flag          BOOLEAN         := FALSE;
   error_msg           VARCHAR2 (3000) := NULL;
BEGIN
------Zopnow Return to make it possitive sign added by Govind on 17-apr-17
--   BEGIN
--      UPDATE apps.xxabrl_navi_ra_int_dist_stg2
--         SET tender_amount = tender_amount * -1
--       WHERE 1 = 1
--         AND (interface_line_attribute1, interface_line_attribute2) IN (
--                SELECT interface_line_attribute1, interface_line_attribute2
--                  FROM apps.xxabrl_navi_ar_int_line_stg2
--                 WHERE 1 = 1
--                   AND interface_line_context = 'ABRL_RESA'
--                   AND interfaced_flag IN ('I', 'T')
--                   AND freeze_flag = 'N'
--                   AND gl_date >= '1-apr-17'
--                   AND tender_amount LIKE '%-%'
--                   AND transaction_type = 'Zopnow Return');

   --      COMMIT;
--   EXCEPTION
--      WHEN OTHERS
--      THEN
--         fnd_file.put_line (fnd_file.output,
--                            'Error in Updating Zopnow Return Sign'
--                           );
--   END;

   --   BEGIN
--      UPDATE apps.xxabrl_navi_ar_int_line_stg2
--         SET tender_amount = tender_amount * -1
--       WHERE 1 = 1
--         AND interface_line_context = 'ABRL_RESA'
--         AND gl_date >= '1-apr-17'
--         AND interfaced_flag IN ('I', 'T')
--         AND freeze_flag = 'N'
--         AND tender_amount LIKE '%-%'
--         AND transaction_type = 'Zopnow Return';

   --      COMMIT;
--   EXCEPTION
--      WHEN OTHERS
--      THEN
--         fnd_file.put_line (fnd_file.output,
--                            'Error in Updating Zopnow Return Sign'
--                           );
--   END;

   ----Zopnow Return to make it possitive sign added by Govind on 17-apr-17
   FOR r2 IN c2 (p_batch_source)                    --, p_gl_date)P_ORG_CODE,
   LOOP
      EXIT WHEN c2%NOTFOUND;
      v_tot_rec_count := v_tot_rec_count + 1;
      fnd_file.put_line (fnd_file.output,
                            'Entered into Cursor for Customer'
                         || ' '
                         || r2.customer_name
                        );

      BEGIN
         SELECT SUM (NVL (nril.tender_amount, 0))
           INTO v_line_amount                              ---,v_line_tend_amt
           FROM apps.xxabrl_navi_ar_int_line_stg2 nril
          WHERE UPPER (nril.org_code) = UPPER (r2.org_code)
            AND nril.gl_date = r2.gl_date
            AND nril.customer_name = r2.customer_name
            AND UPPER (nril.interfaced_flag) IN ('I', 'T');

         fnd_file.put_line (fnd_file.output,
                            'Sales Total Amount' || ' ' || v_line_amount
                           );
      EXCEPTION
         WHEN OTHERS
         THEN
            v_line_amount := 0;
      END;

      BEGIN
         SELECT SUM (NVL (nrid.tender_amount, 0))
           INTO v_dist_amount                              ---,v_dist_tend_amt
           FROM xxabrl.xxabrl_navi_ra_int_dist_stg2 nrid
          WHERE (interface_line_attribute1, interface_line_attribute2) IN (
                   SELECT interface_line_attribute1,
                          interface_line_attribute2
                     FROM apps.xxabrl_navi_ar_int_line_stg2 nril
                    WHERE UPPER (nril.org_code) = UPPER (r2.org_code)
                      AND nril.gl_date = r2.gl_date
                      AND nril.customer_name = r2.customer_name)
            AND UPPER (nrid.interfaced_flag) IN ('I', 'T');

         fnd_file.put_line (fnd_file.output,
                            'Distribution Total Amount' || ' '
                            || v_dist_amount
                           );
      EXCEPTION
         WHEN OTHERS
         THEN
            v_dist_amount := 0;
      END;

      BEGIN
         SELECT SUM (NVL (rar.tender_amount, 0))
           INTO v_rec_amount                                ---,v_rec_tend_amt
           FROM xxabrl_navi_ar_receipt_stg2 rar
          WHERE UPPER (rar.operating_unit) = UPPER (r2.org_code)
            AND rar.gl_date = r2.gl_date
            AND rar.er_customer_number = r2.customer_name
            AND UPPER (rar.interfaced_flag) IN ('I', 'T');

         fnd_file.put_line (fnd_file.output,
                            'Receipts Total Amount' || ' ' || v_rec_amount
                           );
      EXCEPTION
         WHEN OTHERS
         THEN
            v_rec_amount := 0;
      END;

      IF    v_line_amount <> v_dist_amount
         OR (v_line_amount) - (v_rec_amount) NOT BETWEEN -3 AND 3
      --- or v_line_amount <>  v_line_tend_amt
      THEN
         fnd_file.put_line (fnd_file.output, 'Updating to T');
         fnd_file.put_line (fnd_file.output, ' ');
         v_tot_err_count := v_tot_err_count + 1;

         UPDATE apps.xxabrl_navi_ar_int_line_stg2 nril
            SET nril.interfaced_flag = 'T'
          WHERE UPPER (nril.org_code) = UPPER (r2.org_code)
            AND nril.gl_date = r2.gl_date
            AND nril.customer_name = r2.customer_name
            AND UPPER (nril.interfaced_flag) IN ('I', 'T');

         UPDATE xxabrl.xxabrl_navi_ra_int_dist_stg2 nrid
            SET nrid.interfaced_flag = 'T'
          WHERE (interface_line_attribute1, interface_line_attribute2) IN (
                   SELECT interface_line_attribute1,
                          interface_line_attribute2
                     FROM apps.xxabrl_navi_ar_int_line_stg2 nril
                    WHERE UPPER (nril.org_code) = r2.org_code
                      AND nril.gl_date = r2.gl_date
                      AND nril.customer_name = r2.customer_name)
            AND UPPER (nrid.interfaced_flag) IN ('I', 'T');

         UPDATE xxabrl_navi_ar_receipt_stg2 rar
            SET rar.interfaced_flag = 'T'
          WHERE UPPER (rar.operating_unit) = r2.org_code
            AND rar.gl_date = r2.gl_date
            AND rar.er_customer_number = r2.customer_name
            AND rar.interfaced_flag IN ('I', 'T');

         fnd_file.put_line (fnd_file.output, ' ');
      ELSE
         fnd_file.put_line (fnd_file.output, 'Updating to N');
         fnd_file.put_line (fnd_file.output, ' ');
         v_tot_valid_count := v_tot_valid_count + 1;

         UPDATE apps.xxabrl_navi_ar_int_line_stg2 nril
            SET nril.interfaced_flag = 'N'
          WHERE UPPER (nril.org_code) = UPPER (r2.org_code)
            AND nril.gl_date = r2.gl_date
            AND nril.customer_name = r2.customer_name
            AND UPPER (nril.interfaced_flag) IN ('I', 'T');

         UPDATE xxabrl.xxabrl_navi_ra_int_dist_stg2 nrid
            SET nrid.interfaced_flag = 'N'
          WHERE (interface_line_attribute1, interface_line_attribute2) IN (
                   SELECT interface_line_attribute1,
                          interface_line_attribute2
                     FROM apps.xxabrl_navi_ar_int_line_stg2 nril
                    WHERE UPPER (nril.org_code) = UPPER (r2.org_code)
                      AND nril.gl_date = r2.gl_date
                      AND nril.customer_name = r2.customer_name)
            AND UPPER (nrid.interfaced_flag) IN ('I', 'T');

         UPDATE xxabrl_navi_ar_receipt_stg2 rar
            SET rar.interfaced_flag = 'N'
          WHERE UPPER (rar.operating_unit) = UPPER (r2.org_code)
            AND rar.gl_date = r2.gl_date
            AND rar.er_customer_number = r2.customer_name
            AND UPPER (rar.interfaced_flag) IN ('I', 'T');
      END IF;

      COMMIT;
      fnd_file.put_line (fnd_file.output,
                         '====================================== '
                        );
      fnd_file.put_line (fnd_file.output,
                         'Toal No.of Records = ' || v_tot_rec_count
                        );
      fnd_file.put_line (fnd_file.output,
                         'Toal No.of Valid Records = ' || v_tot_valid_count
                        );
      fnd_file.put_line (fnd_file.output,
                         'Toal No.of Error Records = ' || v_tot_err_count
                        );
   END LOOP;

   /*  As per the discussion on 10-Nov-2010, To update the flag as 'T' for the customer '2202041'  in RESA */
   BEGIN
      FOR r3 IN (SELECT *
                   FROM apps.xxabrl_navi_ar_int_line_stg2
                  WHERE 1 = 1
                    AND interface_line_context = 'ABRL_RESA'
                    AND customer_name = '2202041'
                    AND UPPER (NVL (interfaced_flag, 'I')) IN ('I', 'T', 'N'))
      LOOP
         UPDATE apps.xxabrl_navi_ar_int_line_stg2 nril
            SET nril.interfaced_flag = 'T'
          WHERE UPPER (nril.org_code) = UPPER (r3.org_code)
            AND nril.gl_date = r3.gl_date
            AND nril.customer_name = r3.customer_name
            AND UPPER (nril.interfaced_flag) IN ('I', 'T', 'N');

         UPDATE xxabrl.xxabrl_navi_ra_int_dist_stg2 nrid
            SET nrid.interfaced_flag = 'T'
          WHERE (interface_line_attribute1, interface_line_attribute2) IN (
                   SELECT interface_line_attribute1,
                          interface_line_attribute2
                     FROM apps.xxabrl_navi_ar_int_line_stg2 nril
                    WHERE UPPER (nril.org_code) = r3.org_code
                      AND nril.gl_date = r3.gl_date
                      AND nril.customer_name = r3.customer_name)
            AND UPPER (nrid.interfaced_flag) IN ('I', 'T', 'N');

         UPDATE xxabrl_navi_ar_receipt_stg2 rar
            SET rar.interfaced_flag = 'T'
          WHERE UPPER (rar.operating_unit) = r3.org_code
            AND rar.gl_date = r3.gl_date
            AND rar.er_customer_number = r3.customer_name
            AND rar.interfaced_flag IN ('I', 'T', 'N');

         COMMIT;
      END LOOP;
   END;

   BEGIN
      UPDATE apps.xxabrl_navi_ra_int_dist_stg2
         SET amount = tender_amount
       WHERE (interface_line_attribute1, interface_line_attribute2) IN (
                SELECT interface_line_attribute1, interface_line_attribute2
                  FROM apps.xxabrl_navi_ar_int_line_stg2
                 WHERE gl_date >= '01-Oct-15'
                   -- AND org_code LIKE '%HM%'
                   AND UPPER (description) LIKE '%STORE%CARD%'
                   AND freeze_flag = 'N'
                   AND UPPER (batch_source_name) = UPPER ('RESA')
                   AND amount = 0);

      UPDATE apps.xxabrl_navi_ar_int_line_stg2
         SET amount = tender_amount
       WHERE gl_date >= '01-Oct-15'
         --   AND org_code LIKE '%HM%'
         AND UPPER (description) LIKE '%STORE%CARD%'
         AND freeze_flag = 'N'
         AND UPPER (batch_source_name) = UPPER ('RESA')
         AND amount = 0;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         NULL;
   END;

   BEGIN
      FOR xx_main IN xx_update_tender
      LOOP
         error_msg := NULL;
         error_flag := FALSE;

         BEGIN
            SELECT attribute3, attribute4, attribute5
              INTO v_nature, v_tran_type, v_description
              FROM fnd_lookup_values_vl
             WHERE SYSDATE BETWEEN NVL (start_date_active, SYSDATE)
                               AND NVL (end_date_active, SYSDATE)
               AND enabled_flag = 'Y'
               AND lookup_type = 'RESA_AR_TOTAL_ID_LKP'
               AND attribute_category = 'RESA_AR_TOTAL_ID_LKP'
               --  AND description = p_resa_cur.id_type
               AND attribute1 = xx_main.tender_code;
         EXCEPTION
            WHEN OTHERS
            THEN
               error_flag := TRUE;
               error_msg :=
                     'Error to get transaction_nature=>'
                  || SQLCODE
                  || ':'
                  || SQLERRM;
         END;

         IF error_flag = TRUE
         THEN
            UPDATE xxabrl.xxabrl_update_actual_tender
               SET status_flag = 'E',
                   error_msg = error_msg
             WHERE store_code = xx_main.store_code
               AND tender_code = xx_main.tender_code
               AND transaction_date = xx_main.transaction_date
               AND ofin_code = xx_main.ofin_code;
         ELSIF error_flag = FALSE
         THEN
            IF UPPER (v_nature) = 'TRANSACTION'
            THEN
               UPDATE apps.xxabrl_navi_ra_int_dist_stg2
                  SET tender_amount = -1 * ROUND (xx_main.counted_amount, 2)
                WHERE (interface_line_attribute1, interface_line_attribute2) IN (
                         SELECT interface_line_attribute1,
                                interface_line_attribute2
                           FROM apps.xxabrl_navi_ar_int_line_stg2
                          WHERE gl_date = xx_main.transaction_date
                            AND customer_name = xx_main.ofin_code
                            AND transaction_type = v_tran_type
                            AND description = v_description
                            AND interfaced_flag = 'N'
                            AND freeze_flag = 'N');

               UPDATE apps.xxabrl_navi_ar_int_line_stg2
                  SET actual_amount = -1 * ROUND (xx_main.counted_amount, 2)
                WHERE gl_date = xx_main.transaction_date
                  AND customer_name = xx_main.ofin_code
                  AND transaction_type = v_tran_type
                  AND description = v_description
                  AND interfaced_flag = 'N'
                  AND freeze_flag = 'N';

               IF SQL%FOUND
               THEN
                  UPDATE xxabrl.xxabrl_update_actual_tender
                     SET status_flag = 'Y'
                   WHERE store_code = xx_main.store_code
                     AND tender_code = xx_main.tender_code
                     AND transaction_date = xx_main.transaction_date
                     AND ofin_code = xx_main.ofin_code;
               END IF;
            ELSIF UPPER (v_nature) = 'RECEIPT'
            THEN
               SELECT b.description
                 INTO v_er_tender_type
                 FROM apps.fnd_flex_value_sets a, apps.fnd_flex_values_vl b
                WHERE a.flex_value_set_id = b.flex_value_set_id
                  AND flex_value_set_name = 'RESA Roll up 1'
                  AND b.flex_value_meaning = TO_CHAR (xx_main.tender_code);

               UPDATE xxabrl_navi_ar_receipt_stg2
                  SET actual_amount = ROUND (xx_main.counted_amount, 2),
                      deposited_amount = ROUND (xx_main.deposited_amount, 2),
                      deposited_date =
                               TO_DATE (xx_main.transaction_date, 'DD-MON-YY')
--                            + 2   commented by Madan on 23-Oct-2018
--                      deposited_amount = xx_main.counted_amount
               WHERE  gl_date = xx_main.transaction_date
                  AND er_customer_number = xx_main.ofin_code
                  AND er_tender_type = v_er_tender_type
                  AND receipt_method = v_tran_type
                  AND interfaced_flag = 'N'
                  AND freeze_flag = 'N';

               IF SQL%FOUND
               THEN
                  UPDATE xxabrl.xxabrl_update_actual_tender
                     SET status_flag = 'Y'
                   WHERE store_code = xx_main.store_code
                     AND tender_code = xx_main.tender_code
                     AND transaction_date = xx_main.transaction_date
                     AND ofin_code = xx_main.ofin_code;
               END IF;
            END IF;
         END IF;
      END LOOP;

      fnd_file.put_line (fnd_file.output, 'Actual Amt Updated sucessfully');
      COMMIT;
   END;
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      fnd_file.put_line (fnd_file.LOG,
                         'Error occured=>' || SQLCODE || ':' || SQLERRM
                        );
      retcode := 1;
END xxabrl_resa_amt_validation; 
/


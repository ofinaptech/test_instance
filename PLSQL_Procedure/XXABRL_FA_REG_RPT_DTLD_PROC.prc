CREATE OR REPLACE PROCEDURE APPS.xxabrl_fa_reg_rpt_dtld_proc
                              ( errbuf VARCHAR2
                               ,retcode NUMBER
                               ,p_book_code        FA_BOOKS.BOOK_TYPE_CODE%TYPE
                               --,p_period_name_from   fa_deprn_periods.PERIOD_COUNTER%type
                               ,p_period_name_to   fa_deprn_periods.PERIOD_COUNTER%type
                               ,dummy              NUMBER
                             ,p_cat_segment1      FA_CATEGORIES.SEGMENT1%TYPE
                            ,p_location          FA_LOCATIONS.SEGMENT1%TYPE
                                                          )
AS
 /*
  =================================================================================================================================================
  ||   Filename   : XXABRL_FA_REG_RPT_PROC.sql
  ||   Description : Script is used to mold Asset Register Data
  ||
  ||   Version     Date            Author              Modification
  ||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
  ||   1.0       05-OCT-2009    Naresh Hasti         New Development
  ||   1.1       30-oct-09      Naresh Hasti         Asset gl code column is taken from gl_code_combinations table.
  ||   1.2       25-nov-09      Naresh Hasti         Added the condition to pick the retaired asset in the report.
  ||   1.3       13-jul-11      mitul                Added the assign Qty Field
  ||   1.4       01-mar-2012    Vikash Kumar         Added six fields final_cost_assigned,deprn_assigned,deprn_till_date_assigned,accumulated_dep_assigned ,
                                                     opening_acc_dep,opening_acc_dep_assigned
  ||   1.5      04-apr-2012     Vikash Kumar         Added four fields Old_asset_number,Old_asset_original_cost,  Year, project_number( These fields added for taking reference in TSRL Migration
  ||   1.6      28-sep-2012     Vikash Kumar         Added   Book Type Code,change the logic for BOOK_TYPE_CODE parameter(new valueset attached XXABRL_FA_BOOKS instead of ABRL_BOOK_TYPE_CODE)
  ||   1.7      10-Mar-2021     Pawan Sahu           Added Item Code, Location Name, PO Number and Date, Vendor Name and Code, GRN Number and Date, Invoice Number and Date
  ||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
  =================================================================================================================================================*/
l_concat_segments  VARCHAR2(500);
v_period_name varchar2(15);
CURSOR cur_data
IS
SELECT   bk.book_type_code,
         TO_CHAR (bk.date_placed_in_service, 'DD-MON-YYYY') dpis,
         fm.attribute17 old_asset_no, ad.asset_number ass_no,
         fd.units_assigned, ad.current_units qty, loc.segment1 asset_location,
         fak.segment1 YEAR, fak.segment2 project_number,
         cat.segment1 major_cat, cat.segment2 min_cat1, cat.segment3 min_cat2,
         ad2.description asset_desc, fm.attribute18 old_asset_original_cost,
         bk.original_cost, bk.COST final_cost,
         fr.nbv_retired,                        --retired_cost-retired_dpr_cost
         gcck.concatenated_segments asset_gl_code,
         gcck1.concatenated_segments dep_exp_gl_code,
         gcck2.concatenated_segments dep_reserve_gl_code,
         (bk.basic_rate * 100) dep_com_act, bk.deprn_method_code,
         fr.date_retired, fr.proceeds_of_sale, fdp.period_name,
         (CASE WHEN fds.period_counter < p_period_name_to
                THEN 0
               ELSE fds.deprn_amount
         END
         ) deprn_amount,
         DECODE (xxabrl_get_ytd_deprn (fd.asset_id, p_book_code, p_period_name_to), 0, 0, -1, fds.ytd_deprn) ytd_value,
         fds.deprn_reserve ltd_value, fth.transaction_name,
         fm.reviewer_comments,
         ROUND (((bk.COST / ad.current_units) * fd.units_assigned), 2) final_cost_assigned,
         ROUND ((((CASE WHEN fds.period_counter < p_period_name_to
                             THEN 0
                        ELSE fds.deprn_amount
                   END) / ad.current_units) * fd.units_assigned), 2) deprn_assigned,
         ROUND(((DECODE (xxabrl_get_ytd_deprn (fd.asset_id, p_book_code, p_period_name_to), 0, 0, -1, fds.ytd_deprn) / ad.current_units) * fd.units_assigned), 2) deprn_till_date_assigned,
         ROUND (((fds.deprn_reserve / ad.current_units) * fd.units_assigned), 2) accumulated_dep_assigned,
         ROUND((fds.deprn_reserve - DECODE (xxabrl_get_ytd_deprn (fd.asset_id, p_book_code, p_period_name_to), 0, 0, -1, fds.ytd_deprn)), 2) opening_acc_dep,
         ROUND((((fds.deprn_reserve / ad.current_units) * fd.units_assigned) - (  (  DECODE (xxabrl_get_ytd_deprn (fd.asset_id, p_book_code, p_period_name_to), 0, 0, -1, fds.ytd_deprn) / ad.current_units) * fd.units_assigned)), 2) opening_acc_dep_assigned
         -- V 1.7 Start
        ,po_grn.item_code
        ,po_grn.po_number
        ,po_grn.po_date
        ,po_grn.vendor_name
        ,po_grn.vendor_number
        ,po_grn.grn_number
        ,po_grn.grn_date
        ,loc_descr.description asset_location_name
        ,po_grn.invoice_num invoice_number -- nvl (po_grn.invoice_num, fm.invoice_number) invoice_number
        ,po_grn.invoice_date invoice_date  -- nvl (po_grn.invoice_date, fm.invoice_date) invoice_date
         -- V 1.7 End
    FROM apps.fa_asset_history ah,
         apps.fa_books bk,
         apps.fa_categories cat,
         apps.fa_lookups lookups_at,
         apps.fa_lookups lookups_nu,
         apps.fa_lookups lookups_ol,
         apps.fa_lookups lookups_iu,
         apps.fa_lookups lookups_pt,
         apps.fa_lookups lookups_12,
         apps.fa_additions_tl ad2,
         apps.fa_additions_b ad,
         --,fa_asset_invoices ai
         apps.fa_distribution_history fd,
         apps.fa_locations loc,
         apps.fa_retirements fr,
         apps.fa_transaction_headers fth,
         apps.fa_deprn_summary fds,
         apps.fa_deprn_periods fdp,
         apps.fa_books_bas bkprev,
         apps.fa_mass_additions fm,
         apps.fa_distribution_accounts fda,
         apps.gl_code_combinations_kfv gcck,
         apps.gl_code_combinations_kfv gcck1,
         apps.gl_code_combinations_kfv gcck2,
         apps.fa_asset_keywords fak
         -- V 1.7 Start
        ,apps.fnd_flex_value_sets       loc_set
        ,apps.fnd_flex_values           loc_val
        ,apps.fnd_flex_values_tl        loc_descr
        ,(SELECT distinct
                 asset_id
                ,ail.invoice_id
                ,aia1.invoice_num
                ,aia1.invoice_date
                ,ail.line_number
                ,msib.segment1 item_code
                ,po.segment1 po_number
                ,trunc(po.creation_date) po_date
                ,pv.vendor_name
                ,pv.segment1 vendor_number
                ,rsh.receipt_num grn_number
                ,trunc(rsh.creation_date) grn_date
            FROM apps.ap_invoices_all aia1
                ,apps.ap_invoice_distributions_all aida
                ,apps.ap_invoice_lines_all      ail
                ,apps.rcv_transactions          rt
                ,apps.rcv_shipment_headers      rsh
                ,apps.mtl_system_items_b        msib
                ,apps.po_headers_all            po
                ,apps.po_vendors                pv
                ,(select asset_id, invoice_id, invoice_line_number
                    from apps.fa_asset_invoices
                    group by asset_id, invoice_id, invoice_line_number) fai
          WHERE  1=1
                 and aia1.invoice_id                = ail.invoice_id
                 and ail.invoice_id                 = aida.invoice_id(+)
                 and ail.line_number                = aida.invoice_line_number(+)
                 and ail.line_type_lookup_code      = 'ITEM'
                 and ail.inventory_item_id          = msib.inventory_item_id(+)
                 and ail.org_id                     = msib.organization_id(+)
                 and aida.rcv_transaction_id        = rt.transaction_id(+)
                 and rt.shipment_header_id          = rsh.shipment_header_id(+)
                 and ail.po_header_id               = po.po_header_id(+)
                 and pv.vendor_id(+)                = po.vendor_id
                 and ail.invoice_id                 = fai.invoice_id
                 and ail.line_number                = fai.invoice_line_number
                 and fai.asset_id                   is not null
         ) po_grn
         -- V 1.7 End
   WHERE 1 = 1
     AND ad.asset_id = ad2.asset_id
     AND ad.parent_asset_id IS NULL
     AND ad2.LANGUAGE = USERENV ('LANG')
     AND ah.asset_id = ad.asset_id
     AND ah.date_effective <= SYSDATE
     AND NVL (ah.date_ineffective, SYSDATE + 1) > SYSDATE
     AND ah.category_id = cat.category_id
     AND bk.asset_id = ad.asset_id
     AND bk.book_type_code LIKE '' || '%' || p_book_code || '%' || ''
     AND lookups_at.lookup_code = ad.asset_type
     AND lookups_at.lookup_type = 'ASSET TYPE'
     AND lookups_nu.lookup_code = ad.new_used
     AND lookups_nu.lookup_type = 'NEWUSE'
     AND lookups_ol.lookup_code = ad.owned_leased
     AND lookups_ol.lookup_type = 'OWNLEASE'
     AND lookups_iu.lookup_code = ad.in_use_flag
     AND lookups_iu.lookup_type = 'YESNO'
     AND lookups_pt.lookup_code(+) = ad.property_type_code
     AND lookups_pt.lookup_type(+) = 'PROPERTY TYPE'
     AND lookups_12.lookup_code(+) = ad.property_1245_1250_code
     AND lookups_12.lookup_type(+) = '1245/1250 PROPERTY'
     --and  ad.asset_id = ai.asset_id(+)   --------- for invoice date
     AND ad.asset_id = fd.asset_id                     -- for distribution qty
     AND ad.asset_number = fm.asset_number(+)
     AND fm.fixed_assets_units(+) <> 0
     AND fd.location_id = loc.location_id                      -- for location
     AND bk.asset_id = fr.asset_id(+)
     AND bk.book_type_code = fr.book_type_code(+)
     AND ad.asset_id = NVL (fth.asset_id, ad.asset_id)
     AND NVL (fth.book_type_code, bk.book_type_code) = bk.book_type_code
     AND fr.transaction_header_id_in = fth.transaction_header_id(+)
     AND fr.transaction_header_id_in(+) = bk.transaction_header_id_in
     --and fr.retirement_prorate_convention(+) not like  'abrl retmt' ---retired asset could not come
     AND fdp.book_type_code = fds.book_type_code
     AND fdp.period_counter = fds.period_counter
     AND fds.deprn_run_date IN (SELECT MAX (deprn_run_date)
                                  FROM apps.fa_deprn_summary fds1
                                 WHERE fds1.asset_id = fds.asset_id)
     AND (bkprev.transaction_header_id_out(+) = bk.transaction_header_id_in)    --and nvl (bkprev.cost, bk.cost - 1) != bk.cost
     AND fd.date_ineffective IS NULL
     AND bk.date_ineffective IS NULL
     AND ad.asset_id = fds.asset_id(+)
     AND fd.distribution_id = fda.distribution_id
     AND fda.asset_cost_account_ccid = gcck.code_combination_id
     AND fda.deprn_expense_account_ccid = gcck1.code_combination_id
     AND fda.deprn_reserve_account_ccid = gcck2.code_combination_id
     --and fdp.period_counter between nvl(:p_period_name_from,fdp.period_counter) and nvl(:p_period_name_to,fdp.period_counter)
     AND fdp.period_counter <= p_period_name_to
     AND cat.category_id = NVL (p_cat_segment1, cat.category_id)
     AND loc.segment1 = NVL (p_location, loc.segment1)
     AND ad.asset_key_ccid = fm.asset_key_ccid(+)
     ---added on 13-mar-2012, because duplicate assets were showing----
     AND ad.asset_key_ccid = fak.code_combination_id
     -- V 1.7 Start
     and loc_set.flex_value_set_name(+) = 'ABRL_GL_Location'
     and loc_set.flex_value_set_id(+) = loc_val.flex_value_set_id
     and loc_val.flex_value_id = loc_descr.flex_value_id(+)
     and loc_val.flex_value(+) = loc.segment1
     and  ad.asset_id = po_grn.asset_id(+)
     -- V 1.7 End
ORDER BY ad.asset_number;

BEGIN
   begin
     SELECT  PERIOD_NAME into v_period_name
     FROM APPS.FA_DEPRN_PERIODS
     where PERIOD_COUNTER=p_period_name_to;
     EXCEPTION
       WHEN OTHERS THEN
     v_period_name :=null;
  end;
BEGIN
        SELECT segment1||'.'|| segment2||'.'|| segment3 INTO l_concat_segments
        FROM fa_categories
        WHERE category_id = p_cat_segment1;
    EXCEPTION
    WHEN OTHERS THEN
        Fnd_File.put_line(Fnd_File.OUTPUT,'Exception found in selecting categories');
    END;
    Fnd_File.put_line(Fnd_File.OUTPUT,CHR(9)            ||'XXABRL Fixed Asset Register Report');
    Fnd_File.put_line(Fnd_File.OUTPUT,CHR(9)            ||'                   ');
    Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'As on Date:'
                         || CHR (9)
                         || SYSDATE
                         || CHR (9)
                        );
      Fnd_File.put_line (Fnd_File.output, ' ');
      Fnd_File.put_line (Fnd_File.output, 'REPORT PARAMETERS ');
      Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'BOOK TYPE CODE:'
                         || CHR (9)
                         || p_book_code
                         || CHR (9)
                        );
    Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Period Name:'
                         || CHR (9)
                         || v_period_name
                         || CHR (9)
                        );
  Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Category Segments:'
                         || CHR (9)
                         || l_concat_segments
                         || CHR (9)
                        );
      Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Asset Location:'
                         || CHR (9)
                         || p_location
                         || CHR (9)
                        );
      Fnd_File.put_line(Fnd_File.OUTPUT,CHR(9) ||'Book Type Code'||CHR(9)
       ||'Date in Service'||CHR(9)
                               ||'Old Asset Number'||CHR(9)
                               ||'Asset Number'||CHR(9)
                               ||'Quantity'||CHR(9)
                               ||'Assigned Quantity'||CHR(9)
                                      ||'Asset location'||CHR(9)
                                      ||'Year'||CHR(9)
                                      ||'Project Number'||CHR(9)
                                      ||'Major Category'||CHR(9)
                                   ||'Minor Category1'||CHR(9)
                               ||'Minor category2'||CHR(9)
                                ||'Asset Description'||CHR(9)
                                ||'Original Cost of Old Assets'||CHR(9)
                                      ||'Original Cost of Assets'||CHR(9)
                                      ||'Final Cost of Assets'||CHR(9)
                                      ||'Final Cost Assigned'||CHR(9)
                                      ||'Depreciation charged for Month'||CHR(9)
                                      ||'Deprn for the Month Assigned'||CHR(9)
                                      ||'Depreciation charged till Date'||CHR(9)
                                      ||'Deprn charged till Date Assigned'||CHR(9)
                                      ||'Accumulated Depreciation'||CHR(9)
                                      ||'Accumulated Depreciation Assigned'||CHR(9)
                                      ||'Opening Acc Depreciation'||CHR(9)
                                      ||'Opening Acc Depreciation Assigned'||CHR(9)
                                   ||'WDV Cost of Retired Asset'||CHR(9)
                                   ||'Asset GL Code'||CHR(9)
                                   ||'Dep.Exp GL Code'||CHR(9)
                                   ||'Dep Reserve  GL Code'||CHR(9)
                                      ||'Depreciation Companies Act'||CHR(9)
                                      ||'Overriding Depreciation method'||CHR(9)
                                                 ||'Sale of Asset Date'||CHR(9)
                               ||'Realisation Value'||CHR(9)
                               ||'Remarks for sale of Assets'||CHR(9)
                 ||'REVIEWER COMMENTS'||CHR(9)
                 -- V 1.0.7 Start
                 ||'Item Code'||CHR(9)
                 ||'Asset Location Name'||CHR(9)
                 ||'PO Number'||CHR(9)
                 ||'PO Date'||CHR(9)
                 ||'Vendor Name'||CHR(9)
                 ||'Vendor Code'||CHR(9)
                 ||'GRN Number'||CHR(9)
                 ||'GRN Date'||CHR(9)
                 ||'Invoice Number'||CHR(9)
                 ||'Invoice Date'||CHR(9)
                 -- V 1.0.7 End
                );
                FOR rec_data IN cur_data
       LOOP
                  --Printing Local Variable Values to output.
                  Fnd_File.put_line(Fnd_File.OUTPUT,CHR(9) ||rec_data.BOOK_TYPE_CODE||CHR(9)
                  ||rec_data.dpis||CHR(9)
                  ||rec_data.old_asset_no||CHR(9)
                               ||rec_data.ASS_NO||CHR(9)
                               ||rec_data.QTY||CHR(9)           -- qty
                                       ||rec_data.UNITS_ASSIGNED||CHR(9)
                                      ||rec_data.asset_location||CHR(9)
                                      ||rec_data.year||CHR(9)
                                      ||rec_data.project_number||CHR(9)
                                      ||rec_data.Major_cat||CHR(9)
                                   ||rec_data.min_cat1||CHR(9)
                               ||rec_data.min_cat2||CHR(9)
                               ||rec_data.asset_desc||CHR(9)
                               ||rec_data.old_asset_original_cost||CHR(9)
                                      ||rec_data.original_cost||CHR(9)
                                      ||rec_data.final_cost||CHR(9)
                                      ||rec_data.final_cost_assigned||CHR(9)
                                      ||rec_data.deprn_amount||CHR(9)
                                      ||rec_data.deprn_assigned||CHR(9)
                                      ||rec_data.ytd_value||CHR(9)     -- dep charged till date
                                      ||rec_data.deprn_till_date_assigned||CHR(9)
                                      ||rec_data.ltd_value||CHR(9)     -- accumulated dep
                                      ||rec_data.accumulated_dep_assigned||CHR(9)
                                      ||rec_data.opening_acc_dep||CHR(9)
                                      ||rec_data.opening_acc_dep_assigned||CHR(9)
                                   ||rec_data.nbv_retired||CHR(9)
                                   ||rec_data.asset_gl_code||CHR(9)
                                   ||rec_data.dep_exp_gl_code||CHR(9)
                                   ||rec_data.dep_reserve_gl_code||CHR(9)
                                      ||rec_data.dep_com_act||CHR(9)
                                      ||rec_data.deprn_method_code||CHR(9)
                                      ||rec_data.date_retired||CHR(9)
                               ||rec_data.proceeds_of_sale||CHR(9)
                               ||rec_data.transaction_name||CHR(9)
                 ||rec_data.REVIEWER_COMMENTS||CHR(9)
                 -- V 1.0.7 Start
                 ||rec_data.item_code||CHR(9)
                 ||rec_data.asset_location_name||CHR(9)
                 ||rec_data.po_number||CHR(9)
                 ||rec_data.po_date||CHR(9)
                 ||rec_data.vendor_name||CHR(9)
                 ||rec_data.vendor_number||CHR(9)
                 ||rec_data.grn_number||CHR(9)
                 ||rec_data.grn_date||CHR(9)
                 ||rec_data.invoice_number||CHR(9)
                 ||rec_data.invoice_date||CHR(9)
                 -- V 1.0.7 End
                               );
       END LOOP;
END xxabrl_fa_reg_rpt_dtld_proc;
/


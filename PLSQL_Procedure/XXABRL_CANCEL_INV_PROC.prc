CREATE OR REPLACE PROCEDURE APPS.xxabrl_cancel_inv_proc (
   retcode     OUT      VARCHAR2,
   errbuf      OUT      VARCHAR2
)
AS
   l_message_name                 VARCHAR2 (1000);
   l_invoice_amount               NUMBER;
   l_base_amount                  NUMBER;
   l_tax_amount                   NUMBER;
   l_temp_cancelled_amount        NUMBER;
   l_cancelled_by                 VARCHAR2 (1000);
   l_cancelled_amount             NUMBER;
   l_cancelled_date               DATE;
   l_last_update_date             DATE;
   l_original_prepayment_amount   NUMBER;
   l_pay_curr_invoice_amount      NUMBER;
   l_token                        VARCHAR2 (100);
   l_boolean                      BOOLEAN;
   l_user_id                      NUMBER          := 0;
   l_resp_id                      NUMBER          := 20639;
   l_appl_id                      NUMBER          := 200;

   CURSOR c_inv_det
   IS
      SELECT DISTINCT aia.*
                 FROM apps.ap_invoices_all aia,
                      xxabrl.xxabrl_cancel_invoice_det_stg stg
                WHERE 1 = 1
                  AND stg.invoice_id = aia.invoice_id
                  AND stg.vendor_id = aia.vendor_id
                  AND stg.org_id = aia.org_id
                  AND stg.invoice_num = aia.invoice_num
            --      AND aia.gl_date = '22-jun-2018'
                  AND aia.SOURCE = 'RETEK'
                  AND aia.payment_status_flag = 'N';
BEGIN
   fnd_global.apps_initialize (l_user_id, l_resp_id, l_appl_id);

   FOR l_inv_rec IN c_inv_det
   LOOP
      mo_global.init ('SQLAP');
      mo_global.set_policy_context ('S', l_inv_rec.org_id);
      fnd_file.put_line
         (fnd_file.output,
             'Calling API ap_cancel_pkg.ap_cancel_single_invoice to Cancel Invoice:'
          || l_inv_rec.invoice_num
         );
      fnd_file.put_line
             (fnd_file.LOG,
              '**************************************************************'
             );
      l_boolean :=
         ap_cancel_pkg.ap_cancel_single_invoice
                (p_invoice_id                      => l_inv_rec.invoice_id,
                 p_last_updated_by                 => l_inv_rec.last_updated_by,
                 p_last_update_login               => l_inv_rec.last_update_login,
                 p_accounting_date                 => l_inv_rec.gl_date,
                 p_message_name                    => l_message_name,
                 p_invoice_amount                  => l_invoice_amount,
                 p_base_amount                     => l_base_amount,
                 p_temp_cancelled_amount           => l_temp_cancelled_amount,
                 p_cancelled_by                    => l_cancelled_by,
                 p_cancelled_amount                => l_cancelled_amount,
                 p_cancelled_date                  => l_cancelled_date,
                 p_last_update_date                => l_last_update_date,
                 p_original_prepayment_amount      => l_original_prepayment_amount,
                 p_pay_curr_invoice_amount         => l_pay_curr_invoice_amount,
                 p_token                           => l_token,
                 p_calling_sequence                => NULL
                );

      IF l_boolean
      THEN
         fnd_file.put_line (fnd_file.LOG,
                               'Successfully Cancelled the Invoice => '
                            || l_inv_rec.invoice_num
                           );
         COMMIT;
      ELSE
         fnd_file.put_line (fnd_file.LOG,
                               'Failed to Cancel the Invoice => '
                            || l_inv_rec.invoice_num
                           );
         ROLLBACK;
      END IF;
   END LOOP;
END xxabrl_cancel_inv_proc; 
/


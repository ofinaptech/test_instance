CREATE OR REPLACE PROCEDURE APPS.gst_abrl_recover_tax_prvn_data (
   errbuf           OUT      VARCHAR2,
   retcode          OUT      NUMBER,
   p_from_period    IN       VARCHAR2,
   p_to_period      IN       VARCHAR2,
   p_from_account   IN       VARCHAR2,
   p_to_account     IN       VARCHAR2
)
IS
   v_from_period   VARCHAR2 (20)
                    := NVL (fnd_date.canonical_to_date (p_from_period), NULL);
   v_to_period     VARCHAR2 (20)
                      := NVL (fnd_date.canonical_to_date (p_to_period), NULL);

   CURSOR xx_rec
   IS
      SELECT DISTINCT hou.NAME operating_unit, po.segment1 po_number,
                      po.po_date,
                      xxabrl_hsn_sac_no_grn (rsh.shipment_header_id) hsn_sac,
                      (SELECT   SUM (po_unit_price * quantity)
                                                              grn
                           FROM apps.rcv_transactions
                          WHERE 1 = 1
                            AND shipment_header_id = rt.shipment_header_id
                            AND transaction_type = 'DELIVER'
                       GROUP BY shipment_header_id) grn_amount,
                      
                      --  (rt.po_unit_price * rt.quantity) grn_amount,
                      grn_ccid.grn_ccid, tax_ccid.entered_dr tax_amount_dr,
                      tax_ccid.entered_cr tax_amount_cr, tax_ccid.tax_ccid,
                      rsh.receipt_num receipt_num,
                      rsh.creation_date grn_date
                 FROM apps.rcv_shipment_headers rsh,
                      apps.rcv_shipment_lines rsl,
                      apps.rcv_transactions rt,
                      apps.hr_operating_units hou,
                      (SELECT kfv.concatenated_segments grn_ccid,
                              rcv_transaction_id
                         FROM apps.rcv_receiving_sub_ledger rsl,
                              apps.gl_code_combinations_kfv kfv
                        WHERE rsl.accounting_line_type = 'Charge'
                          AND rsl.code_combination_id =
                                                       kfv.code_combination_id) grn_ccid,
                      (SELECT kfv.concatenated_segments tax_ccid,
                              jour.tax_line_id, jour.entered_cr,
                              jour.entered_dr, jour.transaction_date
                         FROM apps.jai_tax_journal_entries jour,
                              apps.gl_code_combinations_kfv kfv
                        WHERE kfv.code_combination_id =
                                                      jour.code_combination_id
                          AND jour.user_je_source_name = 'Financials India'
                          AND jour.user_je_category_name IN
                                                 ('PO Receiving', 'Delivery')
                          AND kfv.segment6 BETWEEN NVL (p_from_account,
                                                        kfv.segment6
                                                       )
                                               AND NVL (p_to_account,
                                                        kfv.segment6
                                                       )) tax_ccid,
                      (SELECT   SUM (rec_tax_amt_tax_curr) tax_amount,
                                tax_line_id, trx_id, creation_date,
                                entity_code
                           FROM apps.jai_tax_lines
                          WHERE 1 = 1
                            AND rec_tax_amt_tax_curr <> 0
                            AND entity_code = 'RCV_TRANSACTION'
                       GROUP BY tax_line_id,
                                trx_id,
                                creation_date,
                                entity_code) jai_tax,
                      (SELECT segment1, TRUNC (creation_date) po_date,
                              po_header_id
                         FROM po_headers_all) po
                WHERE 1 = 1
                  --AND rsh.receipt_num = '9230003273'
                  AND rsh.shipment_header_id = rsl.shipment_header_id
                  AND rsl.shipment_line_id = rt.shipment_line_id
                  AND rt.transaction_type = 'DELIVER'
                  AND rt.transaction_id = grn_ccid.rcv_transaction_id
                  AND rt.shipment_header_id = jai_tax.trx_id
                  AND tax_ccid.tax_line_id = jai_tax.tax_line_id
                  AND rsh.ship_to_org_id = hou.organization_id
                  AND rt.po_header_id = po.po_header_id
                  AND TRUNC (tax_ccid.transaction_date)
                         BETWEEN NVL (v_from_period,
                                      tax_ccid.transaction_date)
                             AND NVL (v_to_period, tax_ccid.transaction_date)
             ORDER BY rsh.receipt_num;
BEGIN
   fnd_file.put_line
               (fnd_file.output,
                LPAD (' GST Provision Data Based On Recoverable Service Tax',
                      100
                     )
               );
   fnd_file.put_line (fnd_file.output,
                      LPAD ('Report Date : ' || SYSDATE, 160));
   fnd_file.put_line (fnd_file.output, 'From Date  ' || v_from_period);
   fnd_file.put_line (fnd_file.output, 'To Date    ' || v_to_period);
   fnd_file.put_line
      (fnd_file.output,
       '-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------'
      );
   fnd_file.put_line (fnd_file.output,
                         LPAD ('Po Number', 14)
                      || '|'
                      || LPAD ('Po Date', 14)
                      || '|'
                      || LPAD ('Receipt Number', 20)
                      || '|'
                      || LPAD ('GRN Amount', 18)
                      || '|'
                      || LPAD ('GRN Code Combiantions', 32)
                      || '|'
                      || LPAD ('Tax Amount CR', 32)
                      || '|'
                      || LPAD ('Tax Amount DR', 21)
                      || '|'
                      || LPAD ('Tax Code Code Combiantions', 35)
                      || '|'
                      || LPAD ('HSN/SAC', 40)
                      || '|'
                      || LPAD ('Operating Unit', 30)
                     );
   fnd_file.put_line (fnd_file.output, '');

   FOR xx_main IN xx_rec
   LOOP
      fnd_file.put_line (fnd_file.output,
                            LPAD (xx_main.po_number, 16)
                         || '|'
                         || LPAD (xx_main.po_date, 12)
                         || '|'
                         || LPAD (xx_main.receipt_num, 20)
                         || '|'
                         || LPAD (xx_main.grn_amount, 15)
                         || '|'
                         || LPAD (xx_main.grn_ccid, 40)
                         || '|'
                         || LPAD (NVL (xx_main.tax_amount_cr, 0), 20)
                         || '|'
                         || LPAD (NVL (xx_main.tax_amount_dr, 0), 20)
                         || '|'
                         || LPAD (xx_main.tax_ccid, 40)
                         || '|'
                         || LPAD (xx_main.hsn_sac, 20)
                         || '|'
                         || LPAD (RPAD (xx_main.operating_unit, 30), 40)
                        );
   END LOOP;
END; 
/


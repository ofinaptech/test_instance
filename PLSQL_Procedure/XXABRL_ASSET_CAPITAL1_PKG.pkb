CREATE OR REPLACE PACKAGE BODY APPS.XXABRL_ASSET_CAPITAL1_PKG IS
/*==============================================================================================
  ||   Filename   : xxabrl_asset_capital1_pkg.sql  
  ||   Description : ABRL Asset Capitalization and Transfer Program 
  ||
  ||   Version     Date            Author              Modification
  ||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~         ~~~~~~~~~~~~~~ 
  ||   1.0.1      07-dec-2009   Praveen Kumar        Depriciation assignment form has null ccid 
                                                     after uploading the csv for capitalization. 
                                                     
       1.0.2       22-Jan-2009  Praveen Kumar/Govind   Asset category logic has been changed.
                                                  
  ||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~   ~    ~~~~~~~~~~~~~
  ==============================================================================================*/

PROCEDURE XXABRL_ASSET_DEFAULT_PROC(ERRBUFF             OUT VARCHAR2,
                                    RETCODE             OUT NUMBER) IS
  -- Variable Declaration
  v_err_flag VARCHAR2(1);
  v_err_msg VARCHAR2(5000);
  v_category_id NUMBER;
  v_location_id NUMBER;
  v_vendor_id NUMBER;
  v_asset_key_ccid NUMBER;
  v_pay_ccid NUMBER;
  v_exp_ccid NUMBER;
  v_BOOK_TYPE_CODE VARCHAR2(50);
  v_all_count    NUMBER:=0;
  v_error_count  NUMBER:=0;
  v_insert_count  NUMBER:=0;
  ln_exp_ccid          NUMBER;
  ln_loc_ccid          NUMBER;
  V_SEGMENTS FND_FLEX_EXT.SEGMENTARRAY;
  l_struct_num         NUMBER; 
  return_value      BOOLEAN;
  v_acc_ccid NUMBER;
  ln_ccid     VARCHAR2(10);
  lc_combinations VARCHAR2(50);
  ln_segment1 VARCHAR2(10);
  ln_segment2 VARCHAR2(10);
  ln_segment3 VARCHAR2(10);
  ln_segment4 VARCHAR2(10);
  ln_segment5 VARCHAR2(10);
  ln_segment6 VARCHAR2(10);
  ln_segment7 VARCHAR2(10);
  ln_segment8 VARCHAR2(10);

CURSOR cur_data --(p_fdate DATE, p_tdate DATE)
IS


SELECT mass_addition_id
      ,BOOK_TYPE_CODE
      ,location_id
      ,expense_code_combination_id
      ,asset_category_id
      ,payables_code_combination_id
 FROM fa_mass_additions
WHERE posting_status in ('NEW','ON HOLD')
  AND feeder_system_name=UPPER('ORACLE PAYABLES');
  --and mass_addition_id in (231098,231099,231157,231158,231159,231160)
  --AND asset_category_id IS NOT NULL;
  
  CURSOR C1 IS 
  SELECT ZZ.MEANING,AID.INVOICE_DISTRIBUTION_ID,AID.INVOICE_ID
 FROM APPS.FA_MASS_ADDITIONS XX,APPS.AP_INVOICE_DISTRIBUTIONS_ALL AID, APPS.FND_LOOKUP_VALUES ZZ
WHERE AID.INVOICE_DISTRIBUTION_ID=XX.INVOICE_DISTRIBUTION_ID
AND XX.INVOICE_ID=AID.INVOICE_ID
AND ZZ.LOOKUP_CODE=AID.ORG_ID
AND XX.INVOICE_ID IS NOT NULL
AND  LOOKUP_TYPE='ASSET BOOK DEFAULTATION'
--AND INVOICE_ID=71718429
AND FEEDER_SYSTEM_NAME='ORACLE PAYABLES'
AND POSTING_STATUS IN ('NEW','ON HOLD');

-- Main Begin
BEGIN
     Fnd_File.put_line (Fnd_File.LOG,'Begenning of 1st program ');

      FOR rec_data IN cur_data
    LOOP
       v_err_flag := NULL; 

      IF rec_data.PAYABLES_CODE_COMBINATION_ID IS NOT NULL THEN
        BEGIN
        
        UPDATE fa_mass_additions fm1
        SET location_id = (SELECT location_id
                           FROM fa_locations
                   WHERE segment1 = ( SELECT segment4
                                      FROM gl_code_combinations
                              WHERE code_combination_id = (SELECT payables_code_combination_id
                                                            FROM fa_mass_additions fm2
                                            WHERE fm2.mass_addition_id = rec_data.mass_addition_id
                      )))
             WHERE  fm1.mass_addition_id = rec_data.mass_addition_id;

            commit;
            Fnd_File.put_line (Fnd_File.LOG,'Location Updated for mass addition ID   '||rec_data.mass_addition_id);
        EXCEPTION
        WHEN OTHERS THEN
        v_err_flag := 'E';
             Fnd_File.put_line (Fnd_File.LOG,'Others Exception Found in while updating/defaulting Location  '||SQLCODE||SQLERRM ||'for Mass Add ID :'||rec_data.mass_addition_id );

        END;
        END IF;

        BEGIN

            SELECT  segment6
            INTO ln_segment6
            FROM gl_code_combinations
            WHERE code_combination_id =(SELECT fc.DEPRN_EXPENSE_ACCOUNT_CCID FROM fa_category_books fc
                                         WHERE fc.category_id = rec_data.ASSET_CATEGORY_ID
                                           AND book_type_code= rec_data.book_type_code);
            dbms_output.put_line ('segment6-'||ln_segment6);

        EXCEPTION
        WHEN OTHERS THEN
               v_err_flag := 'E';
               dbms_output.put_line ('ln_ccid error'||v_err_flag);
             Fnd_File.put_line (Fnd_File.LOG,'Others Exception Found in selecting Segment1,2,6 query  '||SQLCODE||SQLERRM ||'for Mass Add ID :'||rec_data.mass_addition_id );

        END;

        BEGIN

        SELECT segment1,segment2,segment3,segment4
        INTO ln_segment1,ln_segment2,ln_segment3,ln_segment4
        FROM gl_code_combinations
        WHERE code_combination_id  =(SELECT fm.payables_code_combination_id FROM fa_mass_additions fm
                                     WHERE fm.mass_addition_id = rec_data.mass_addition_id );
                                     
          dbms_output.put_line ('segment1,segment2,segment3,segment4,'||ln_segment1||'-'||ln_segment2||'-'||ln_segment3||'-'||ln_segment4);

        SELECT '00','000','0000'
        INTO ln_segment5,ln_segment7,ln_segment8
        FROM dual;
        
           dbms_output.put_line ('segment5,segment7,segment8-'||ln_segment5||'-'||ln_segment7||'-'||ln_segment8);

        EXCEPTION
        WHEN OTHERS THEN
        v_err_flag := 'E';
         dbms_output.put_line ('seg1,2,3,4 error'||v_err_flag);
             Fnd_File.put_line (Fnd_File.LOG,'Others Exception Found in selecting Segment3 and 4 query- '||SQLCODE||SQLERRM ||'for Mass Add ID :'||rec_data.mass_addition_id);

        END;
        
        BEGIN 
          SELECT SOB.CHART_OF_ACCOUNTS_ID
            INTO L_STRUCT_NUM
            FROM GL_SETS_OF_BOOKS SOB 
            WHERE NAME = 'ABRL Ledger'
              AND ROWNUM =1;
          EXCEPTION
           WHEN OTHERS THEN            
            v_err_flag := 'E';
             Fnd_File.put_line (Fnd_File.LOG,'Others Exception Chart of Accounts query  '||SQLCODE||SQLERRM ||'for Mass Add ID :'||rec_data.mass_addition_id );
          END;
                   

         BEGIN         
         SELECT code_combination_id
           INTO ln_ccid
           FROM gl_code_combinations           
          WHERE segment1 = ln_segment1
            AND segment2 = ln_segment2
            AND segment3 = ln_segment3
            AND segment4 = ln_segment4
            AND segment5 = ln_segment5
            AND segment6 = ln_segment6
            AND segment7 = ln_segment7
            AND segment8 = ln_segment8;
         dbms_output.put_line ('After Begin  CCID'||ln_ccid);
         
             EXCEPTION           
                WHEN OTHERS               
                 THEN
                 V_SEGMENTS(1)         :=ln_segment1;
                 V_SEGMENTS(2)         := ln_segment2;
                 V_SEGMENTS(3)         := ln_segment3;
                 V_SEGMENTS(4)         := ln_segment4;
                 V_SEGMENTS(5)         := ln_segment5;
                 V_SEGMENTS(6)         := ln_segment6;
                 V_SEGMENTS(7)         := ln_segment7;
                 V_SEGMENTS(8)         := ln_segment8;
                   DBMS_OUTPUT.PUT_LINE ('INSIDE NO DATA FOUND');
                END;
            
                 RETURN_VALUE := FND_FLEX_EXT.GET_COMBINATION_ID(APPLICATION_SHORT_NAME     => 'SQLGL',
                                                        KEY_FLEX_CODE          => 'GL#',
                                                        STRUCTURE_NUMBER       => L_STRUCT_NUM,
                                                        VALIDATION_DATE        => SYSDATE,
                                                        N_SEGMENTS             => 8,
                                                        SEGMENTS               => V_SEGMENTS,
                                                        COMBINATION_ID         => V_ACC_CCID,        
                                                        DATA_SET               => -1);
         IF ln_ccid IS NULL THEN                                                         
           BEGIN         
                SELECT distinct code_combination_id
                INTO  ln_ccid
                FROM  gl_code_combinations
                      ,fa_mass_additions fm
                WHERE segment1 = ln_segment1
                AND   segment2 = ln_segment2
                AND   segment3 = ln_segment3
                AND   segment4 = ln_segment4
                AND   segment5 = ln_segment5
                AND   segment6 = ln_segment6
                AND   segment7 = ln_segment7
                AND   segment8 = ln_segment8;
             EXCEPTION
             WHEN OTHERS THEN
             DBMS_OUTPUT.PUT_LINE ('INSIDE NO DATA FOUND');
             v_err_flag := 'E';
                Fnd_File.put_line (Fnd_File.LOG,'Others Exception Found in CCID  query  '||SQLCODE||SQLERRM ||'for Mass Add ID :'||rec_data.mass_addition_id );
             END;
         END IF;
       
        IF v_err_flag IS NULL AND ln_ccid IS NOT NULL THEN
          BEGIN 
               
            dbms_output.put_line ('before updating mass additions '||v_err_flag);
            UPDATE fa_mass_additions
            SET EXPENSE_CODE_COMBINATION_ID = to_char(ln_ccid)
            WHERE mass_addition_id = rec_data.mass_addition_id;  
            commit;
            Fnd_File.put_line (Fnd_File.Output,'Updated Expence CCID for Mass Add ID :'||rec_data.mass_addition_id );

            EXCEPTION
            WHEN OTHERS THEN
              Fnd_File.put_line (Fnd_File.LOG,'Others Exception Found Updating CCID  '||SQLCODE||SQLERRM ||'for Mass Add ID :'||rec_data.mass_addition_id );
            END;

        END IF;

      END LOOP;
      
      
         
      FOR I1 IN C1
      LOOP
      
       BEGIN

         UPDATE APPS.FA_MASS_ADDITIONS
          SET 
          BOOK_TYPE_CODE=I1.MEANING
        WHERE INVOICE_ID=I1.INVOICE_ID
         AND INVOICE_DISTRIBUTION_ID=I1.INVOICE_DISTRIBUTION_ID;
         
         
         EXCEPTION
            WHEN OTHERS THEN
         
           Fnd_File.put_line (Fnd_File.LOG,'BOOKS NOT UPDATE');
         
          
           END;
           
         END LOOP;
         
         
         
     
          
         
       
      

 COMMIT;
END XXABRL_ASSET_DEFAULT_PROC;
END XXABRL_ASSET_CAPITAL1_PKG; 
/


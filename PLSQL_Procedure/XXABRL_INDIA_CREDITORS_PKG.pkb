CREATE OR REPLACE PACKAGE BODY APPS.Xxabrl_India_Creditors_pkg
AS
PROCEDURE XXABRL_INDIA_CREDITORS_PROC(ERRBUFF OUT VARCHAR2,
                                      RETCODE OUT NUMBER,
                                      P_OPERATING_UNIT IN VARCHAR2,
                                      P_VENDOR_NAME      VARCHAR2,
                                      P_VENDOR_SITE      VARCHAR2,
                                      P_FROM_VENDOR      VARCHAR2,
                                      P_TO_VENDOR         VARCHAR2,
                                      P_FROM_GL_DATE     VARCHAR2,
                                      P_TO_GL_DATE       VARCHAR2,
                                      P_FROM_VOCHER      VARCHAR2,
                                      P_TO_VOCHER        VARCHAR2
                                     ) AS

CURSOR CUR_ORG_ID(cp_resp_id NUMBER) IS
SELECT aa.ORGANIZATION_ID FROM (
SELECT organization_id
  FROM fnd_profile_option_values fpop,
       fnd_profile_options fpo,
       per_security_organizations_v pso
 WHERE fpop.profile_option_id = fpo.profile_option_id
   AND level_value = cp_resp_id
   AND pso.security_profile_id = fpop.profile_option_value
   AND profile_option_name = 'XLA_MO_SECURITY_PROFILE_LEVEL'
UNION
SELECT TO_NUMBER (fpop.profile_option_value) ORGANIZATION_ID
  FROM fnd_profile_option_values fpop, fnd_profile_options fpo
 WHERE fpop.profile_option_id = fpo.profile_option_id
   AND level_value = cp_resp_id
   AND profile_option_name = 'ORG_ID')AA;



      query_string             LONG;
      op_string                LONG;
      vendor_string            LONG;
      range_ven_string         LONG;
      vendor_site_string       LONG;
      GL_DATE_STRING           LONG;
      INV_DATE_STRING          LONG;
      range_vocher_string      LONG;
      order_by_string          LONG;
      inv_count                NUMBER;
      cr_total                 NUMBER;
      dr_total                 NUMBER;
      closing_balance          NUMBER;
      LIST_ORG_ID              LONG;
      PARA_ORG_ID              LONG;
     -- LIST_VEN_STRING          LONG;

 V_RESPID             NUMBER := Fnd_Profile.VALUE('RESP_ID');


 TYPE ref_cur IS REF CURSOR;

      c                        ref_cur;

      TYPE c_rec IS RECORD (
         v_operating_unit           hr_operating_units.NAME%TYPE,
         v_vendor_name              po_vendors.vendor_name%TYPE,
         v_vendor_number            po_vendors.segment1%TYPE,
         v_vendor_site              PO_VENDOR_SITES_ALL.vendor_site_code%TYPE,
         v_batch_name               AP_BATCHES_ALL.Batch_Name%TYPE,
         v_vocher_num               ap_invoices_all.DOC_SEQUENCE_VALUE%TYPE,
         v_invoice_type             VARCHAR2(250),
         v_invoice_num              ap_invoices_all.invoice_num%TYPE,
		     v_invoice_date             ap_invoices_all.invoice_date%TYPE,
         v_gl_date                  ap_invoices_all.gl_date%TYPE,
         v_chek_num                 ap_checks_all.check_number%TYPE,
     --    v_invoice_amount           ap_invoices_all.invoice_amount%TYPE,
         v_account_code             gl_code_combinations_kfv.CONCATENATED_SEGMENTS%TYPE,
         v_Accounted                VARCHAR2(200),
         v_account_desc             fnd_flex_values_vl.DESCRIPTION%TYPE,
         v_invoice_desc             ap_invoices_all.description%TYPE,
         v_bank_name                VARCHAR2(250),
     --    v_line_number              xla_ae_lines.ae_line_num%TYPE,
      --   v_line_desc                xla_ae_lines.description%TYPE,
         v_cr_amount                xla_ae_lines.ACCOUNTED_CR%TYPE,
         v_dr_amount                xla_ae_lines.ACCOUNTED_DR%TYPE
         );

v_rec                    c_rec;
   BEGIN

   query_string :='SELECT
AA.Opearting_unit,
AA.Vendor_Name,
AA.Vendor_Number,
AA.Vendor_Site,
AA.Batch_Name,
AA.Voucher_Number,
AA.Invoice_Type,
AA.Invoice_Number,
AA.Invoice_Date,
AA.Gl_Date,
AA.CHECK_NUMBER,
AA.Account_Code,
AA.Accounted,
AA.gl_acct_description,
AA.Invoice_Description,
AA.bank_account_name,
--AA.Cr_Amount,
--AA.Dr_Amount
DECODE(aa.invoice_type,''PREPAYMENT'',DECODE(aa.status,''UNPAID'',AA.Cr_Amount,AA.Dr_Amount),AA.Cr_Amount) Cr_Amount,
DECODE(aa.invoice_type,''PREPAYMENT'',DECODE(aa.status,''FULL'',AA.Dr_Amount,AA.Cr_Amount),AA.Dr_Amount) Dr_Amount
FROM
(
--360084483
--440031444
SELECT  AP_INVOICES_PKG.GET_APPROVAL_STATUS(api.INVOICE_ID,
                                         api.INVOICE_AMOUNT,
                                         api.PAYMENT_STATUS_FLAG,
                                         api.INVOICE_TYPE_LOOKUP_CODE) status,
									--	 api.INVOICE_TYPE_LOOKUP_CODE invoice_type,
     hou.NAME Opearting_unit,
     PO.vendor_Name Vendor_Name,
     PO.segment1 Vendor_Number,
     povs.VENDOR_SITE_CODE Vendor_Site,
     apb.BATCH_NAME Batch_Name,
     api.DOC_SEQUENCE_VALUE Voucher_Number,
     api.INVOICE_TYPE_LOOKUP_CODE Invoice_Type,
     api.INVOICE_NUM Invoice_Number,
     api.INVOICE_DATE Invoice_Date,
     apid.ACCOUNTING_DATE Gl_Date,
     NULL CHECK_NUMBER,
     gl.CONCATENATED_SEGMENTS Account_Code,
     DECODE(AP_INVOICES_PKG.GET_POSTING_STATUS( ApI.INVOICE_ID),''Y'',''Yes'',''N'',''NO'',''P'',''Partial'',
            AP_INVOICES_PKG.GET_POSTING_STATUS( ApI.INVOICE_ID)) Accounted,
     FFVV.description gl_acct_description,
     api.DESCRIPTION Invoice_Description,
     NULL bank_account_name,
     APID.amount Cr_Amount,
     NULL Dr_Amount,
     po.vendor_id,
     hou.organization_id
FROM po_vendors po,
     po_vendor_sites_all povs,
     hr_operating_units hou,
     ap_invoices_all api,
     ap_invoice_distributions_all apid,
     AP_BATCHES_ALL apb,
     gl_code_combinations_kfv gl,
     fnd_flex_values_vl ffvv
WHERE 1=1
AND po.vendor_id=povs.vendor_id
--AND api.invoice_num IN (''Prepayment-25-jan-2009'',''01-OCT-2008 TO 25-NOV-2008'')
AND api.invoice_id=apid.invoice_id
AND apid.DIST_CODE_COMBINATION_ID=gl.CODE_COMBINATION_ID
AND gl.segment6 = ffvv.flex_value
AND  ffvv.flex_value_set_id=1013469
AND povs.ORG_ID=hou.ORGANIZATION_ID
AND po.VENDOR_ID=api.VENDOR_ID
AND povs.vendor_site_id=api.vendor_site_id
AND api.BATCH_ID=apb.BATCH_ID(+)
AND  AP_INVOICES_PKG.GET_APPROVAL_STATUS(api.INVOICE_ID,
                                         api.INVOICE_AMOUNT,
                                         api.PAYMENT_STATUS_FLAG,
                                         api.INVOICE_TYPE_LOOKUP_CODE) IN (''APPROVED'',''UNPAID'',''FULL'')
UNION ALL
SELECT  AP_INVOICES_PKG.GET_APPROVAL_STATUS(api.INVOICE_ID,
                                         api.INVOICE_AMOUNT,
                                         api.PAYMENT_STATUS_FLAG,
                                         api.INVOICE_TYPE_LOOKUP_CODE) status,
								--		 api.INVOICE_TYPE_LOOKUP_CODE invoice_type,
     hou.NAME Opearting_unit,
     PO.vendor_Name Vendor_Name,
     PO.segment1 Vendor_Number,
     povs.VENDOR_SITE_CODE Vendor_Site,
     apb.BATCH_NAME Batch_Name,
     ac.DOC_SEQUENCE_VALUE Voucher_Number,
     alc1.displayed_field Invoice_Type,
     api.INVOICE_NUM Invoice_Number,
     ac.CHECK_DATE Invoice_Date,
     aip.accounting_date  Gl_Date,
     ac.CHECK_NUMBER,
     gl.CONCATENATED_SEGMENTS Account_Code,
     DECODE(ap_checks_pkg.get_posting_status (ac.check_id),''P'',''Partial'',''E'',''Error'',
             ''N'',''Unprocessed'',''S'',''Processing'',''Y'',''Processed'',ap_checks_pkg.get_posting_status (ac.check_id))Accounted,
     (SELECT FFVV.description
     FROM fnd_flex_values_vl ffvv
     WHERE ffvv.flex_value_set_id=1013469
     AND ffvv.flex_value=gl.segment6) gl_acct_description,
     api.DESCRIPTION Invoice_Description,
     cba.bank_account_name bank_account_name,
     --XLA.ACCOUNTED_DR Cr_Amount,
     NULL Cr_Amount,
     (aip.amount + NVL((SELECT NVL(SUM(AMOUNT),0)
	                 FROM AP_INVOICE_LINES_ALL aila
    					WHERE api.INVOICE_ID = aila.prepay_invoice_id),0))Dr_Amount,
     po.vendor_id,
     hou.organization_id
FROM po_vendors po,
     po_vendor_sites_all povs,
     hr_operating_units hou,
     ap_invoices_all api,
     ap_invoice_payments_all aip,
     ap_checks_all ac,
     ce_bank_accounts cba,
      ap_lookup_codes alc1,
     ce_bank_acct_uses_all cbau,
     AP_BATCHES_ALL apb,
     gl_code_combinations_kfv gl,
     (SELECT DISTINCT xlh.EVENT_ID,/*xll.ACCOUNTED_CR,xll.ACCOUNTED_DR,*/xll.CODE_COMBINATION_ID
     FROM xla_ae_headers xlh,xla_ae_lines xll
     WHERE xlh.AE_HEADER_ID=xll.AE_HEADER_ID
     AND xll.ACCOUNTING_CLASS_CODE=''CASH_CLEARING'') xla
WHERE 1=1
AND po.vendor_id=povs.vendor_id
AND xla.CODE_COMBINATION_ID=gl.CODE_COMBINATION_ID(+)
AND xla.EVENT_ID(+)=aip.ACCOUNTING_EVENT_ID
AND povs.ORG_ID=hou.ORGANIZATION_ID
AND aip.invoice_id = api.invoice_id
AND aip.check_id = ac.check_id(+)
AND ac.ce_bank_acct_use_id = cbau.bank_acct_use_id
AND alc1.lookup_type = ''PAYMENT TYPE''
AND alc1.lookup_code = ac.payment_type_flag
AND cbau.bank_account_id = cba.bank_account_id
AND po.VENDOR_ID=api.VENDOR_ID
AND povs.vendor_site_id=api.vendor_site_id
AND api.BATCH_ID=apb.BATCH_ID(+)
--AND api.invoice_num IN (''Prepayment-25-jan-2009'',''01-OCT-2008 TO 25-NOV-2008'')
AND  AP_INVOICES_PKG.GET_APPROVAL_STATUS(api.INVOICE_ID,
                                         api.INVOICE_AMOUNT,
                                         api.PAYMENT_STATUS_FLAG,
                                         api.INVOICE_TYPE_LOOKUP_CODE) IN (''CANCELLED'',''APPROVED'',''AVAILABLE'',''UNPAID'')
) AA
WHERE 1=1';

/*
  query_string :=
  'SELECT
AA.Opearting_unit,
AA.Vendor_Name,
AA.Vendor_Number,
AA.Vendor_Site,
AA.Batch_Name,
AA.Voucher_Number,
AA.Invoice_Type,
AA.Invoice_Number,
AA.Invoice_Date,
AA.Gl_Date,
AA.CHECK_NUMBER,
AA.Account_Code,
AA.Accounted,
AA.gl_acct_description,
AA.Invoice_Description,
AA.bank_account_name,
AA.Cr_Amount,
AA.Dr_Amount
FROM
(SELECT
     hou.NAME Opearting_unit,
     PO.vendor_Name Vendor_Name,
     PO.segment1 Vendor_Number,
     povs.VENDOR_SITE_CODE Vendor_Site,
     apb.BATCH_NAME Batch_Name,
     api.DOC_SEQUENCE_VALUE Voucher_Number,
     api.INVOICE_TYPE_LOOKUP_CODE Invoice_Type,
     api.INVOICE_NUM Invoice_Number,
     api.INVOICE_DATE Invoice_Date,
     apid.ACCOUNTING_DATE Gl_Date,
     NULL CHECK_NUMBER,
     gl.CONCATENATED_SEGMENTS Account_Code,
     DECODE(AP_INVOICES_PKG.GET_POSTING_STATUS( ApI.INVOICE_ID),''Y'',''Yes'',''N'',''NO'',''P'',''Partial'',
            AP_INVOICES_PKG.GET_POSTING_STATUS( ApI.INVOICE_ID)) Accounted,
     FFVV.description gl_acct_description,
     api.DESCRIPTION Invoice_Description,
     NULL bank_account_name,
     APID.amount Cr_Amount,
     NULL Dr_Amount,
     po.vendor_id,
     hou.organization_id
FROM po_vendors po,
     po_vendor_sites_all povs,
     hr_operating_units hou,
     ap_invoices_all api,
     ap_invoice_distributions_all apid,
     AP_BATCHES_ALL apb,
     gl_code_combinations_kfv gl,
     fnd_flex_values_vl ffvv
WHERE 1=1
AND po.vendor_id=povs.vendor_id
AND api.invoice_id=apid.invoice_id
AND apid.DIST_CODE_COMBINATION_ID=gl.CODE_COMBINATION_ID
AND gl.segment6 = ffvv.flex_value
AND  ffvv.flex_value_set_id=1013469
AND povs.ORG_ID=hou.ORGANIZATION_ID
AND po.VENDOR_ID=api.VENDOR_ID
AND povs.vendor_site_id=api.vendor_site_id
AND api.BATCH_ID=apb.BATCH_ID
AND  AP_INVOICES_PKG.GET_APPROVAL_STATUS(api.INVOICE_ID,
                                         api.INVOICE_AMOUNT,
                                         api.PAYMENT_STATUS_FLAG,
                                         api.INVOICE_TYPE_LOOKUP_CODE) IN (''APPROVED'',''AVAILABLE'',''UNPAID'',''FULL'')
UNION
SELECT
     hou.NAME Opearting_unit,
     PO.vendor_Name Vendor_Name,
     PO.segment1 Vendor_Number,
     povs.VENDOR_SITE_CODE Vendor_Site,
     apb.BATCH_NAME Batch_Name,
     ac.DOC_SEQUENCE_VALUE Voucher_Number,
     alc1.displayed_field Invoice_Type,
     api.INVOICE_NUM Invoice_Number,
     ac.CHECK_DATE Invoice_Date,
     aip.accounting_date  Gl_Date,
     ac.CHECK_NUMBER,
     gl.CONCATENATED_SEGMENTS Account_Code,
     DECODE(ap_checks_pkg.get_posting_status (ac.check_id),''P'',''Partial'',''E'',''Error'',
             ''N'',''Unprocessed'',''S'',''Processing'',''Y'',''Processed'',ap_checks_pkg.get_posting_status (ac.check_id))Accounted,
     (SELECT FFVV.description
     FROM fnd_flex_values_vl ffvv
     WHERE ffvv.flex_value_set_id=1013469
     AND ffvv.flex_value=gl.segment6) gl_acct_description,
     api.DESCRIPTION Invoice_Description,
     cba.bank_account_name bank_account_name,
     --XLA.ACCOUNTED_DR Cr_Amount,
     null Cr_Amount,
     aip.amount Dr_Amount,
     po.vendor_id,
     hou.organization_id
FROM po_vendors po,
     po_vendor_sites_all povs,
     hr_operating_units hou,
     ap_invoices_all api,
     ap_invoice_payments_all aip,
     ap_checks_all ac,
     ce_bank_accounts cba,
      ap_lookup_codes alc1,
     ce_bank_acct_uses_all cbau,
     AP_BATCHES_ALL apb,
     gl_code_combinations_kfv gl,
     (SELECT xlh.EVENT_ID,xll.ACCOUNTED_CR,xll.ACCOUNTED_DR,xll.CODE_COMBINATION_ID
     FROM xla_ae_headers xlh,xla_ae_lines xll
     WHERE xlh.AE_HEADER_ID=xll.AE_HEADER_ID
     AND xll.ACCOUNTING_CLASS_CODE=''CASH_CLEARING'') xla
WHERE 1=1
AND po.vendor_id=povs.vendor_id
AND xla.CODE_COMBINATION_ID=gl.CODE_COMBINATION_ID(+)
AND xla.EVENT_ID(+)=aip.ACCOUNTING_EVENT_ID
AND povs.ORG_ID=hou.ORGANIZATION_ID
AND aip.invoice_id = api.invoice_id
AND aip.check_id = ac.check_id(+)
AND ac.ce_bank_acct_use_id = cbau.bank_acct_use_id
AND alc1.lookup_type = ''PAYMENT TYPE''
AND alc1.lookup_code = ac.payment_type_flag
AND cbau.bank_account_id = cba.bank_account_id
AND po.VENDOR_ID=api.VENDOR_ID
AND povs.vendor_site_id=api.vendor_site_id
AND api.BATCH_ID=apb.BATCH_ID
AND  AP_INVOICES_PKG.GET_APPROVAL_STATUS(api.INVOICE_ID,
                                         api.INVOICE_AMOUNT,
                                         api.PAYMENT_STATUS_FLAG,
                                         api.INVOICE_TYPE_LOOKUP_CODE) IN (''APPROVED'',''AVAILABLE'',''UNPAID'',''FULL'')
) AA
WHERE 1=1 ';
*/


op_string:=' AND AA.Opearting_unit = '''|| p_operating_unit|| '''';
vendor_string:= ' AND AA.VENDOR_id='''|| P_VENDOR_NAME ||'''';
--range_ven_string:= ' AND AA.Vendor_Number between '''||P_FROM_VENDOR||''' AND '''||P_TO_VENDOR||'''';
range_ven_string:= ' AND AA.Vendor_Number = '''||P_FROM_VENDOR||'''';
vendor_site_string:=' AND AA.Vendor_Site='''|| P_VENDOR_SITE ||'''';
GL_DATE_STRING:=' AND to_date(to_char(AA.Gl_Date,''DD-MON-YY'')) BETWEEN '''||P_FROM_GL_DATE||''' AND '''||P_TO_GL_DATE||'''';
INV_DATE_STRING :=' AND AA.Gl_Date = AA.Gl_Date';
range_vocher_string:= 'AND AA.Voucher_Number between '''||P_FROM_VOCHER||''' AND '''||P_TO_VOCHER||'''';
order_by_string:=' ORDER BY AA.Gl_Date,AA.Vendor_Number,AA.Vendor_Site,AA.Invoice_Number,AA.Cr_Amount';

   LIST_ORG_ID:='';
     PARA_ORG_ID  := ' AND AA.ORGANIZATION_ID in (' ;

    FOR K IN CUR_ORG_ID(V_RESPID) LOOP
         LIST_ORG_ID := K.ORGANIZATION_ID||','||NVL(LIST_ORG_ID,'0')  ;
    END LOOP;

     LIST_ORG_ID:=PARA_ORG_ID || LIST_ORG_ID ||') ';




   -- Checking  For Operating Unit Parameter

         IF(p_operating_unit IS NULL) THEN
         query_string:=query_string||LIST_ORG_ID;
         ELSE
         query_string :=query_string||op_string;
         END IF;


  -- Checking  For Vendor Name Parameter

         IF(P_VENDOR_NAME IS NULL) THEN
         query_string:=query_string;
         ELSE
         query_string :=query_string||vendor_string;
         END IF;

  -- Checking  For Vendor Site Name Parameter

         IF(P_VENDOR_SITE IS NULL) THEN
         query_string:=query_string;
         ELSE
         query_string :=query_string||vendor_site_string;
         END IF;

  -- Checking GL Date Parameter

     IF (P_FROM_GL_DATE IS NOT NULL AND P_TO_GL_DATE IS NOT NULL)
      THEN
         query_string := query_string || GL_DATE_STRING;
      ELSIF (P_FROM_GL_DATE IS NULL AND P_TO_GL_DATE IS NULL)
      THEN
         query_string := query_string || INV_DATE_STRING;
      END IF;


  -- Checking For the from Vendor Number and To Vendor Number Parameter

      IF (P_FROM_VENDOR IS NULL AND P_TO_VENDOR IS NULL)
      THEN
         query_string := query_string;
     ELSE
         query_string := query_string ||range_ven_string;
      END IF;

     -- Checking For the from Vocher Number and To Vocher Number Parameter

      IF (P_FROM_VOCHER IS NULL AND P_FROM_VOCHER IS NULL)
      THEN
         query_string := query_string;
     ELSE
         query_string := query_string ||range_vocher_string;
      END IF;



      Fnd_File.put_line (Fnd_File.output,
                         'ABRL - India Creditors Ledger'
                        );
      Fnd_File.put_line (Fnd_File.output, ' ');
      Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'As on Date'
                         || CHR (9)
                         || SYSDATE
                         || CHR (9)
                        );
      Fnd_File.put_line (Fnd_File.output, ' ');
      Fnd_File.put_line (Fnd_File.output, 'REPORT PARAMETRS ');
      Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'OPERATING UNIT'
                         || CHR (9)
                         || p_operating_unit
                         || CHR (9)
                        );

        Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Vendor Name'
                         || CHR (9)
                         || P_VENDOR_NAME
                         || CHR (9)
                        );
        Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'From Vendor Number'
                         || CHR (9)
                         || P_FROM_VENDOR
                         || CHR (9)
                        );

         Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'To Vendor Number'
                         || CHR (9)
                         || P_TO_VENDOR
                         || CHR (9)
                          );

          Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'From GL Date'
                         || CHR (9)
                         || P_FROM_GL_DATE
                         || CHR (9)
                        );
                 Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'To GL Date'
                         || CHR (9)
                         || P_TO_GL_DATE
                         || CHR (9)
                        );

               Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'Vendor Site'
                         || CHR (9)
                         || P_VENDOR_SITE
                         || CHR (9)
                        );


                Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'From Voucher Number'
                         || CHR (9)
                         || P_FROM_VOCHER
                         || CHR (9)
                        );

                Fnd_File.put_line (Fnd_File.output,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'To Voucher Number'
                         || CHR (9)
                         || P_TO_VOCHER
                         || CHR (9)
                        );


  Fnd_File.put_line (Fnd_File.output, ' ');
      Fnd_File.put_line (Fnd_File.output, ' ');
      Fnd_File.put_line (Fnd_File.output,
                          'Operating Unit'
                         || CHR (9)
                         || 'Vendor Name'
                         || CHR (9)
                         || 'Vendor Number'
                         || CHR (9)
                         || 'Vendor Site'
                         || CHR (9)
                         || 'Batch Name'
                         || CHR (9)
                         || 'Voucher Number'
                         || CHR (9)
                         || 'Invoice Type/Payment'
                         || CHR (9)
                         || 'Invoice Number'
                         || CHR (9)
                         || 'Invoice Date/Payment Date'
                         || CHR (9)
                         || 'Gl Date'
                         || CHR (9)
                         || 'Check Number'
                         || CHR (9)
                        /* || 'Invoice Amount'
                         || CHR (9)
                         || 'Line Number'
                         || CHR (9)
                         || 'Line Description'
                         || CHR (9)
                         || ''
                         || CHR (9) */
                         || 'Account Code'
                         || CHR (9)
                         || 'Accounted'
                         || CHR (9)
                         || 'Account Description'
                         || CHR (9)
                         || 'Invoice Description'
                         || CHR (9)
                         || 'Bank Account Name'
                         || CHR (9)
                         || 'DR Amount'
                         || CHR (9)
                         || 'CR Amount'
                        );

                   query_string:=query_string||order_by_string;

                   Fnd_File.put_line (Fnd_File.LOG,
                         'string: '||query_string
                        );

  inv_count:=0;
  cr_total:=0;
  dr_total:=0;
  closing_balance:=0;

         OPEN c
       FOR query_string;
      LOOP
         FETCH c
          INTO v_rec;
         EXIT WHEN c%NOTFOUND;

         Fnd_File.put_line (Fnd_File.output,
                             v_rec.v_operating_unit
                            || CHR (9)
                            || v_rec.v_Vendor_name
                            || CHR (9)
                            || v_rec.v_Vendor_Number
                            || CHR (9)
                            || v_rec.v_vendor_site
                            || CHR (9)
                            || v_rec.v_batch_name
                            || CHR (9)
                            || v_rec.v_vocher_num
                            || CHR (9)
                            || v_rec.v_invoice_type
                            || CHR (9)
                            || v_rec.v_invoice_num
                            || CHR (9)
                            || v_rec.v_invoice_date
                            || CHR (9)
                            || v_rec.v_gl_date
                            || CHR (9)
                            || v_rec.v_chek_num
                            || CHR (9)
                            ||v_rec.v_account_code
                            || CHR (9)
                            ||v_rec.v_Accounted
                            || CHR (9)
                            || v_rec.v_account_desc
                            || CHR (9)
                            || v_rec.v_invoice_desc
                            || CHR (9)
                            || v_rec.v_bank_name
                            || CHR (9)
                            || v_rec.v_dr_amount
                            || CHR (9)
                            ||v_rec.v_cr_amount
                            );

                   cr_total:=cr_total+NVL(v_rec.v_cr_amount,0);
                   dr_total:=dr_total+NVL(v_rec.v_dr_amount,0);

                   inv_count :=inv_count+1;

END LOOP;

 Fnd_File.put_line (Fnd_File.LOG,
                         'CR Total : '||cr_total
                        );

   Fnd_File.put_line (Fnd_File.LOG,
                         'DR Total : '||dr_total
                        );

                    Fnd_File.put_line (Fnd_File.output,
                            ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
						             || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || 'Grand Total'
                         || CHR (9)
                         || dr_total
                         ||  CHR (9)
                         || cr_total
                         );

             closing_balance:= (cr_total)-(dr_total);

    Fnd_File.put_line (Fnd_File.output, ' ');

              Fnd_File.put_line (Fnd_File.output,
                            ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || 'Closing Balance  '
                         || CHR (9)
                         || closing_balance
                         );

CLOSE c;

END XXABRL_INDIA_CREDITORS_PROC;
END Xxabrl_India_Creditors_pkg;
/


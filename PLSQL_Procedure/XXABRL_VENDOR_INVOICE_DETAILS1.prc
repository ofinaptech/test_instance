CREATE OR REPLACE PROCEDURE APPS.XXABRL_VENDOR_INVOICE_DETAILS1(errbuf out varchar2,
                                 retcode out varchar2,P_VENDOR_NUM IN VARCHAR2,--P_ORG_ID IN NUMBER,
                                 P_FROM_DATE IN  VARCHAR2,
                                 P_TO_DATE IN   VARCHAR2
                                 )
AS
v_from_date      VARCHAR2 (11):= TO_CHAR (TO_DATE (SUBSTR (p_from_date, 1, 10), 'RRRR/MM/DD'),'DD-MON-YYYY');
 v_to_date        VARCHAR2 (11):= TO_CHAR (TO_DATE (SUBSTR (p_to_date, 1, 10), 'RRRR/MM/DD'),'DD-MON-YYYY');
V_L_FILE         VARCHAR2(200);
V_FINANCE_YEAR   NUMBER;
L_VENDOR_NAME    VARCHAR2(250);

CURSOR CUR_PERIOD_NAME1 IS
  SELECT 1 REFF,XACL.VENDOR_NUMBER VENDOR_CODE ,XACL.VENDOR_NAME,(SELECT NAME FROM HR_OPERATING_UNITS HR
WHERE  XACL.ORG_ID=HR.ORGANIZATION_ID) OU
,XACL.VENDOR_SITE_CODE SITE_CODE,XACL.INVOICE_NUM,
XACL.INVOICE_DATE,XACL.GL_DATE,XACL.INVOICE_TYPE_LOOKUP_CODE INVOICE_TYPE,XACL.DESCRIPTION,
XACL.CR_VAL DR,XACL.DR_VAL CR,XACL.DR_VAL PAID_AMOUNT,(SELECT NVL (SUM (apil2.prepay_amount_applied), 0)
               FROM xxabrl_ap_view_prepays_v apil2
              WHERE apil2.invoice_id = xacl.invoice_id  and xacl.DR_VAL  =0
               ) prepay_amount,XACL.ACCOUNTING_DATE PAID_DATE,xacl.BANK_ACCOUNT_NAME Vendor_Bank_Name,--povs.ATTRIBUTE3 Vendor_Branch_Name,
 xacl.BANK_ACCOUNT_NUM Vendor_Account_Number,/*XACL.CR_VAL-XACL.DR_VAL-(SELECT NVL (SUM (apil2.prepay_amount_applied), 0)
               FROM xxabrl_ap_view_prepays_v apil2
              WHERE apil2.invoice_id = xacl.invoice_id and apil2.ACCOUNTING_DATE between :v_from_date and  :v_to_date and xacl.DR_VAL  =0
               ) balance*/
xacl.CCID,XACL.CHECK_NUMBER
from xxabrl_vendor_invoice2_p XACL
where xacl.INVOICE_AMOUNT-nvl(xacl.AMOUNT_PAID,0)<>0
  and xacl.GL_DATE <'1-apr-2012'
   AND xacl.INVOICE_TYPE_LOOKUP_CODE IN ('STANDARD','DEBIT')
  and   XACL.ORG_ID not in (84,87,89,91,86,663,93)
  and XACL.VENDOR_NUMBER=P_VENDOR_NUM
UNION
 SELECT 1 REFF,XACL.VENDOR_NUMBER VENDOR_CODE ,XACL.VENDOR_NAME,(SELECT NAME FROM HR_OPERATING_UNITS HR
WHERE  XACL.ORG_ID=HR.ORGANIZATION_ID) OU
,XACL.VENDOR_SITE_CODE SITE_CODE,XACL.INVOICE_NUM,
XACL.INVOICE_DATE,XACL.GL_DATE,XACL.INVOICE_TYPE_LOOKUP_CODE INVOICE_TYPE,XACL.DESCRIPTION,
XACL.DR_VAL*-1 DR,XACL.CR_VAL CR,XACL.DR_VAL PAID_AMOUNT,(SELECT NVL (SUM (apil2.prepay_amount_applied), 0)
               FROM xxabrl_ap_view_prepays_v apil2
              WHERE apil2.invoice_id = xacl.invoice_id  and xacl.DR_VAL  =0
               ) prepay_amount,XACL.ACCOUNTING_DATE PAID_DATE,xacl.BANK_ACCOUNT_NAME Vendor_Bank_Name,--povs.ATTRIBUTE3 Vendor_Branch_Name,
 xacl.BANK_ACCOUNT_NUM Vendor_Account_Number,/*XACL.CR_VAL-XACL.DR_VAL-(SELECT NVL (SUM (apil2.prepay_amount_applied), 0)
               FROM xxabrl_ap_view_prepays_v apil2
              WHERE apil2.invoice_id = xacl.invoice_id and apil2.ACCOUNTING_DATE between :v_from_date and  :v_to_date and xacl.DR_VAL  =0
               ) balance*/
xacl.CCID,XACL.CHECK_NUMBER
from xxabrl_vendor_invoice2_p XACL
where xacl.INVOICE_AMOUNT-nvl(xacl.AMOUNT_PAID,0)<>0
  and xacl.GL_DATE <'1-apr-2012'
   AND xacl.INVOICE_TYPE_LOOKUP_CODE IN ('CREDIT')
  and   XACL.ORG_ID not in (84,87,89,91,86,663,93)
  and XACL.VENDOR_NUMBER=P_VENDOR_NUM
  UNION
select 1 reff,PV.SEGMENT1 vendor_code,PV.vendor_name,(SELECT NAME FROM HR_OPERATING_UNITS HR
WHERE  api.ORG_ID=HR.ORGANIZATION_ID) OU,
NULL SITE_CODE,API.INVOICE_NUM,API.invoice_date,API.GL_DATE,API.INVOICE_TYPE_LOOKUP_CODE invoice_type,
api.DESCRIPTION,api.INVOICE_AMOUNT DR,NULL CR,NULL PAID_AMOUNT,
(SELECT NVL (SUM (apil2.prepay_amount_applied), 0)
               FROM xxabrl_ap_view_prepays_v apil2
              WHERE apil2.invoice_id = API.invoice_id and apil2.ACCOUNTING_DATE <v_to_date
               ) prepay_amount,(SELECT ACCOUNTING_DATE FROM APPS.AP_INVOICES_ALL AIA,APPS.AP_INVOICE_LINES_ALL AIAL
               WHERE AIA.INVOICE_ID=AIAL.INVOICE_ID
               AND AIAL.LINE_TYPE_LOOKUP_CODE='PREPAY'
               AND AIA.INVOICE_ID=API.INVOICE_ID) PAID_DATE ,NULL VENDOR_BANK_NAME,
               NULL VENDOR_ACCOUNT_NUMBER,NULL CCID,NULL CHECK_NUMBER  from ap_invoices_all API,
               PO_VENDORS PV--po_vendor_sites_all povs
   where API.invoice_id in(select distinct Aip.INVOICE_ID from xxabrl_ap_view_prepays_v apil2,ap_invoices_all aip,po_vendors pv
 WHERE   APIL2.INVOICE_ID =aip.INVOICE_ID
 and pv.VENDOR_ID=aip.VENDOR_ID
 AND aip.INVOICE_TYPE_LOOKUP_CODE IN ('STANDARD')
 and pv.SEGMENT1=P_VENDOR_NUM
 AND AIP.gl_date<v_from_date
  and APIL2.ACCOUNTING_DATE  between v_from_date   and v_to_date )
  --AND XACL.ORG_ID=84)
  --and xacl.gl_date>:v_from_date)
  AND API.VENDOR_ID=PV.VENDOR_ID
   and API.INVOICE_AMOUNT=API.AMOUNT_PAID
  UNION
select 1 reff,SEGMENT1 vendor_code,vendor_name,(SELECT NAME FROM HR_OPERATING_UNITS HR
WHERE  api.ORG_ID=HR.ORGANIZATION_ID) OU,
NULL SITE_CODE,INVOICE_NUM,invoice_date,GL_DATE,API.INVOICE_TYPE_LOOKUP_CODE invoice_type,api.DESCRIPTION,api.INVOICE_AMOUNT DR,NULL CR,NULL PAID_AMOUNT,
(SELECT NVL (SUM (apil2.prepay_amount_applied), 0)
               FROM xxabrl_ap_view_prepays_v apil2
              WHERE apil2.invoice_id = API.invoice_id and apil2.ACCOUNTING_DATE between v_from_date and  v_to_date
               ) prepay_amount,NULL PAID_DATE ,NULL VENDOR_BANK_NAME,NULL VENDOR_ACCOUNT_NUMBER,NULL CCID,NULL CHECK_NUMBER  from ap_invoices_all API,PO_VENDORS PV--po_vendor_sites_all povs
   where invoice_id in(select xacl.invoice_id from ap_invoice_payments_all xacl,ap_invoices_all aip,po_vendors pv
 WHERE   xacl.INVOICE_ID=aip.INVOICE_ID
 and pv.VENDOR_ID=aip.VENDOR_ID
 AND aip.INVOICE_TYPE_LOOKUP_CODE IN ('STANDARD','CREDIT','DEBIT')
 and pv.SEGMENT1=P_VENDOR_NUM
  and xacl.ACCOUNTING_DATE   between v_from_date   and v_to_date
  and   XACL.ORG_ID not in (84,87,89,91,86,663,93)
    and aip.gl_date<v_from_date )
  --AND XACL.ORG_ID=84)
  --and xacl.gl_date>:v_from_date)
  AND API.VENDOR_ID=PV.VENDOR_ID
  and INVOICE_AMOUNT=AMOUNT_PAID
  --ORDER BY API.INVOICE_NUM
 -- AND POVS.VENDOR_ID=PV.VENDOR_ID
 UNION
SELECT 1 REFF,XACL.VENDOR_NUMBER VENDOR_CODE ,XACL.VENDOR_NAME,(SELECT NAME FROM HR_OPERATING_UNITS HR
WHERE  XACL.ORG_ID=HR.ORGANIZATION_ID) OU
,XACL.VENDOR_SITE_CODE SITE_CODE,XACL.INVOICE_NUM,
XACL.INVOICE_DATE,XACL.GL_DATE,  XACL.INVOICE_TYPE_LOOKUP_CODE INVOICE_TYPE,XACL.DESCRIPTION,
XACL.CR_VAL DR,XACL.DR_VAL CR,XACL.DR_VAL PAID_AMOUNT,(SELECT NVL (SUM (apil2.prepay_amount_applied), 0)
               FROM xxabrl_ap_view_prepays_v apil2
              WHERE apil2.invoice_id = xacl.invoice_id and apil2.ACCOUNTING_DATE between v_from_date and  v_to_date and xacl.DR_VAL  =0
               ) prepay_amount,XACL.ACCOUNTING_DATE PAID_DATE,xacl.BANK_ACCOUNT_NAME Vendor_Bank_Name,--povs.ATTRIBUTE3 Vendor_Branch_Name,
 xacl.BANK_ACCOUNT_NUM Vendor_Account_Number,/*XACL.CR_VAL-XACL.DR_VAL-(SELECT NVL (SUM (apil2.prepay_amount_applied), 0)
               FROM xxabrl_ap_view_prepays_v apil2
              WHERE apil2.invoice_id = xacl.invoice_id and apil2.ACCOUNTING_DATE between :v_from_date and  :v_to_date and xacl.DR_VAL  =0
               ) balance*/
xacl.CCID,XACL.CHECK_NUMBER
FROM xxabrl_vendor_invoice2_p XACL
  WHERE XACL.VENDOR_NUMBER=P_VENDOR_NUM
  --AND XACL.INVOICE_ID=APIL2.INVOICE_ID
  AND XACL.INVOICE_TYPE_LOOKUP_CODE='STANDARD'
  and xacl.gl_date  between v_from_date   and v_to_date
  -- AND XACL.INVOICE_NUM IN('DN_BANG_AUG_233','BN_1761')
  --AND XACL.INVOICE_TYPE_LOOKUP_CODE IN('DEBIT','STANDARD')
  --and XACL.CHECK_NUMBER is NOT null
 -- AND POVS.ORG_ID=XACL.ORG_ID
   --AND XACL.ORG_ID=P_ORG_ID
   ORDER BY INVOICE_NUM;

 CURSOR CUR_PERIOD_NAME2 IS
 SELECT 1 REFF,XACL.VENDOR_NUMBER VENDOR_CODE ,XACL.VENDOR_NAME,(SELECT NAME FROM HR_OPERATING_UNITS HR
WHERE  XACL.ORG_ID=HR.ORGANIZATION_ID) OU
,XACL.VENDOR_SITE_CODE SITE_CODE,XACL.INVOICE_NUM,
XACL.INVOICE_DATE,XACL.GL_DATE,XACL.INVOICE_TYPE_LOOKUP_CODE INVOICE_TYPE,XACL.DESCRIPTION,
XACL.CR_VAL DR,XACL.DR_VAL CR,XACL.DR_VAL PAID_AMOUNT,null PREPAY_AMOUNT,XACL.ACCOUNTING_DATE PAID_DATE,xacl.BANK_ACCOUNT_NAME Vendor_Bank_Name,--povs.ATTRIBUTE3 Vendor_Branch_Name,
 xacl.BANK_ACCOUNT_NUM Vendor_Account_Number,
xacl.CCID,XACL.CHECK_NUMBER
FROM xxabrl_vendor_invoice2_p XACL
  WHERE 1=1
  --AND ACA.CHECK_ID=XACL.
   AND XACL.VENDOR_NUMBER=P_VENDOR_NUM--'1921781'
  AND XACL.INVOICE_TYPE_LOOKUP_CODE='DEBIT'
  and xacl.GL_DATE between v_from_date   and v_to_date
  -- AND XACL.INVOICE_NUM IN('DN_BANG_AUG_233','BN_1761')
  --AND XACL.INVOICE_TYPE_LOOKUP_CODE IN('DEBIT','STANDARD')
  --and XACL.CHECK_NUMBER is NOT null
  ------------------------------- AND ( (XACL.PAYMENT_TYPE_FLAG IS NULL)
  ------------------------------ OR (XACL.PAYMENT_TYPE_FLAG<>'R'))
  --AND XACL.ORG_ID=P_ORG_ID
   ORDER BY XACL.INVOICE_NUM;

   CURSOR CUR_PERIOD_NAME3 IS
SELECT 1 REFF,XACL.VENDOR_NUMBER VENDOR_CODE ,XACL.VENDOR_NAME,(SELECT NAME FROM HR_OPERATING_UNITS HR
WHERE  XACL.ORG_ID=HR.ORGANIZATION_ID) OU
,XACL.VENDOR_SITE_CODE SITE_CODE,XACL.INVOICE_NUM,
XACL.INVOICE_DATE,XACL.GL_DATE,XACL.INVOICE_TYPE_LOOKUP_CODE INVOICE_TYPE,XACL.DESCRIPTION,
XACL.DR_VAL*-1 DR,XACL.CR_VAL CR,XACL.DR_VAL PAID_AMOUNT,null PREPAY_AMOUNT,XACL.ACCOUNTING_DATE PAID_DATE,xacl.BANK_ACCOUNT_NAME Vendor_Bank_Name,--povs.ATTRIBUTE3 Vendor_Branch_Name,
 xacl.BANK_ACCOUNT_NUM Vendor_Account_Number,
xacl.CCID,XACL.CHECK_NUMBER
FROM xxabrl_vendor_invoice2_p XACL
  WHERE 1=1
  --AND ACA.CHECK_ID=XACL.
  AND XACL.VENDOR_NUMBER=P_VENDOR_NUM--'1921781'
  AND XACL.INVOICE_TYPE_LOOKUP_CODE='CREDIT'
  and xacl.GL_DATE between v_from_date   and v_to_date
  -- AND XACL.INVOICE_NUM IN('DN_BANG_AUG_233','BN_1761')
  --AND XACL.INVOICE_TYPE_LOOKUP_CODE IN('DEBIT','STANDARD')
  --and XACL.CHECK_NUMBER is NOT null
   --------------AND ( (XACL.PAYMENT_TYPE_FLAG IS NULL)
   ---------------OR (XACL.PAYMENT_TYPE_FLAG<>'R'))
    --AND XACL.ORG_ID=P_ORG_ID
   ORDER BY XACL.INVOICE_NUM;

   CURSOR CUR_PERIOD_NAME4 IS
    select * FROM (SELECT 1 REFF,XACL.VENDOR_NUMBER VENDOR_CODE ,XACL.VENDOR_NAME,(SELECT NAME FROM HR_OPERATING_UNITS HR
WHERE  XACL.ORG_ID=HR.ORGANIZATION_ID) OU
,XACL.VENDOR_SITE_CODE SITE_CODE,XACL.INVOICE_NUM,
XACL.INVOICE_DATE,XACL.GL_DATE,XACL.INVOICE_TYPE_LOOKUP_CODE INVOICE_TYPE,XACL.DESCRIPTION,
(select SUM(APIL2.PREPAY_AMOUNT_APPLIED) from  xxabrl_ap_view_prepays_v apil2
WHERE APIL2.ACCOUNTING_DATE<'1-APR-2012'
AND APIL2.PREPAY_INVOICE_ID=XACL.INVOICE_ID) DR,XACL.DR_VAL CR,XACL.DR_VAL PAID_AMOUNT,NULL PREPAY_AMOUNT,XACL.ACCOUNTING_DATE PAID_DATE,xacl.BANK_ACCOUNT_NAME Vendor_Bank_Name,--povs.ATTRIBUTE3 Vendor_Branch_Name,
 xacl.BANK_ACCOUNT_NUM Vendor_Account_Number,
xacl.CCID,XACL.CHECK_NUMBER FROM xxabrl_vendor_invoice2_p xacl
WHERE XACL.INVOICE_TYPE_LOOKUP_CODE='PREPAYMENT'
AND XACL.VENDOR_NUMBER=P_VENDOR_NUM
--AND XACL.ORG_ID=84
AND XACL.GL_DATE<'1-APR-2012'
 and   XACL.ORG_ID not in  (84,87,89,91,86,663,93)  ) con
WHERE CON.CR-CON.DR<>0
AND CON.CR>0
ORDER BY CON.INVOICE_NUM;

CURSOR CUR_PERIOD_NAME5 IS
SELECT 1 REFF,XACL.VENDOR_NUMBER VENDOR_CODE ,XACL.VENDOR_NAME,(SELECT NAME FROM HR_OPERATING_UNITS HR
WHERE  XACL.ORG_ID=HR.ORGANIZATION_ID) OU
,XACL.VENDOR_SITE_CODE SITE_CODE,XACL.INVOICE_NUM,
XACL.INVOICE_DATE,XACL.GL_DATE,XACL.INVOICE_TYPE_LOOKUP_CODE INVOICE_TYPE,XACL.DESCRIPTION,
XACL.CR_VAL DR,XACL.DR_VAL CR,XACL.DR_VAL PAID_AMOUNT,null PREPAY_AMOUNT,XACL.ACCOUNTING_DATE PAID_DATE,xacl.BANK_ACCOUNT_NAME Vendor_Bank_Name,--povs.ATTRIBUTE3 Vendor_Branch_Name,
 xacl.BANK_ACCOUNT_NUM Vendor_Account_Number,
xacl.CCID,XACL.CHECK_NUMBER
FROM xxabrl_vendor_invoice2_p XACL
  WHERE 1=1
  --AND ACA.CHECK_ID=XACL.
 AND XACL.VENDOR_NUMBER=P_VENDOR_NUM--'1921781'
  AND XACL.INVOICE_TYPE_LOOKUP_CODE='PREPAYMENT'
 and xacl.GL_DATE between v_from_date   and v_to_date
  -- AND XACL.INVOICE_NUM IN('DN_BANG_AUG_233','BN_1761')
  --AND XACL.INVOICE_TYPE_LOOKUP_CODE IN('DEBIT','STANDARD')
  --and XACL.CHECK_NUMBER is NOT null
--AND XACL.ORG_ID=P_ORG_ID
   ORDER BY XACL.INVOICE_NUM;


x_id utl_file.file_type;
BEGIN

SELECT VENDOR_NAME INTO L_VENDOR_NAME FROM APPS.AP_SUPPLIERS
WHERE SEGMENT1=P_VENDOR_NUM;

   SELECT 'ABRL_INVOICE_DETAILS_SUMMARY'||'_'||'V'||'_'||L_VENDOR_NAME||P_VENDOR_NUM||'.txt'INTO V_L_FILE FROM dual;
  --SELECT 'XXABRL_HYPN_MKTNG_'||p_period_name||'_DATA'||NVL(TO_CHAR(TO_DATE(sysdate,'DD-MON-YYYY'),'DDMMYY'),TO_CHAR(SYSDATE-3,'DDMMYY'))||'.csv' INTO V_L_FILE FROM dual;
  x_id:=utl_file.fopen('VENDOR_INVOICE_PORTAL',V_L_FILE,'W');
utl_file.put_line(x_id,  'VENDOR NUMBER'||'^'||
                              'VENDOR NAME'||'^'||
                              'OPERATING UNIT'||'^'||
                              'SITE NAME'||'^'||
                              'INVOICE NUM'||'^'||
                              'INVOICE DATE'||'^'||
                              'GL DATE'||'^'||
                              'INVOICE TYPE'||'^'||
                              'DESCRIPTION'||'^'||
                              'FUTURE USE'||'^'||
                              'INVOICE AMOUNT'||'^'||
                              'AMOUNT PAID'||'^'||
                             -- 'PIAD AMOUNT'||'|'||
                              'PREPAY AMOUNT'||'^'||
                              'PAID DATE'||'^'||
                              'VENDOR BANK NAME'||'^'||
                              --'Vendor_Branch_Name'||'|'||
                              'VENDOR ACCOUNT NUMBER'||'^'||
                              --'RTGS_IFSC_Code'||'|'||
                              'CCID'||'^'||
                              'CHECK NUMBER'
                              );
  --utl_file.put_line(x_id,'VENDOR PAID INVOICES LIST DETAILS................................................->');

--utl_file.put_line(x_id,'VENDOR STANDARD INVOICES LIST........................................................->');
 FOR r IN CUR_PERIOD_NAME1 LOOP

 utl_file.put_line(x_id,   r.VENDOR_CODE||'^'||
                              r.VENDOR_NAME||'^'||
                              r.OU||'^'||
                              r.SITE_CODE||'^'||
                              r.INVOICE_NUM||'^'||
                              r.INVOICE_DATE||'^'||
                               r.GL_DATE||'^'||
                              r.INVOICE_TYPE ||'^'||
                              r.DESCRIPTION||'^'||
                              NULL||'^'||
                              r.DR||'^'||
                              r.CR||'^'||
                              --r.PAID_AMOUNT||'|'||
                              r.PREPAY_AMOUNT||'^'||
                              r.PAID_DATE||'^'||
                              r.Vendor_Bank_Name||'^'||
                              --r.Vendor_Branch_Name||'|'||
                              r.Vendor_Account_Number ||'^'||
                              --r.RTGS_IFSC_Code||'|'||
                              r.CCID||'^'||
                              r.CHECK_NUMBER);

  end loop;
  --utl_file.put_line(x_id,'cur2.................................->');

  FOR r1 IN CUR_PERIOD_NAME2 LOOP

  utl_file.put_line(x_id,   r1.VENDOR_CODE||'^'||
                              r1.VENDOR_NAME||'^'||
                              r1.OU||'^'||
                              r1.SITE_CODE||'^'||
                              r1.INVOICE_NUM||'^'||
                              r1.INVOICE_DATE||'^'||
                                r1.GL_DATE||'^'||
                              r1.INVOICE_TYPE ||'^'||
                              r1.DESCRIPTION||'^'||
                               NULL||'^'||
                              r1.DR||'^'||
                              r1.CR||'^'||
                              --r1.PAID_AMOUNT||'|'||
                             null||'^'||
                              r1.PAID_DATE||'^'||
                              r1.Vendor_Bank_Name||'^'||
                              --r1.Vendor_Branch_Name||'|'||
                              r1.Vendor_Account_Number ||'^'||
                              --r1.RTGS_IFSC_Code||'|'||
                               r1.CCID||'^'||
                              r1.CHECK_NUMBER
                              );
  END LOOP;

  --utl_file.put_line(x_id,'cur3..................................->');

 FOR r2 IN CUR_PERIOD_NAME3 LOOP

 utl_file.put_line(x_id,   r2.VENDOR_CODE||'^'||
                              r2.VENDOR_NAME||'^'||
                              r2.OU||'^'||
                              r2.SITE_CODE||'^'||
                              r2.INVOICE_NUM||'^'||
                              r2.INVOICE_DATE||'^'||
                                r2.GL_DATE||'^'||
                              r2.INVOICE_TYPE ||'^'||
                              r2.DESCRIPTION||'^'||
                               NULL||'^'||
                              r2.DR||'^'||
                              r2.CR||'^'||
                             -- r2.PAID_AMOUNT||'|'||
                              null||'^'||
                              r2.PAID_DATE||'^'||
                              r2.Vendor_Bank_Name||'^'||
                              --r2.Vendor_Branch_Name||'|'||
                              r2.Vendor_Account_Number ||'^'||
                              --r2.RTGS_IFSC_Code||'|'||
                               r2.CCID||'^'||
                              r2.CHECK_NUMBER
                              );
  end loop;

 -- utl_file.put_line(x_id,'cur4.................................->');

 FOR r3 IN CUR_PERIOD_NAME4 LOOP

 utl_file.put_line(x_id,   r3.VENDOR_CODE||'^'||
                              r3.VENDOR_NAME||'^'||
                              r3.OU||'^'||
                              r3.SITE_CODE||'^'||
                              r3.INVOICE_NUM||'^'||
                              r3.INVOICE_DATE||'^'||
                                r3.GL_DATE||'^'||
                              r3.INVOICE_TYPE ||'^'||
                              r3.DESCRIPTION||'^'||
                               NULL||'^'||
                              r3.DR||'^'||
                              r3.CR||'^'||
                             -- r3.PAID_AMOUNT||'|'||
                               null||'^'||
                              r3.PAID_DATE||'^'||
                              r3.Vendor_Bank_Name||'^'||
                              --r3.Vendor_Branch_Name||'|'||
                              r3.Vendor_Account_Number ||'^'||
                              --r3.RTGS_IFSC_Code||'|'||
                               r3.CCID||'^'||
                              r3.CHECK_NUMBER
                              );
  end loop;

  --utl_file.put_line(x_id,'cur5..................................->');

  FOR r4 IN CUR_PERIOD_NAME5 LOOP

 utl_file.put_line(x_id,   r4.VENDOR_CODE||'^'||
                              r4.VENDOR_NAME||'^'||
                              r4.OU||'^'||
                              r4.SITE_CODE||'^'||
                              r4.INVOICE_NUM||'^'||
                              r4.INVOICE_DATE||'^'||
                                r4.GL_DATE||'^'||
                              r4.INVOICE_TYPE ||'^'||
                              r4.DESCRIPTION||'^'||
                               NULL||'^'||
                              r4.DR||'^'||
                              r4.CR||'^'||
                             -- r4.PAID_AMOUNT||'|'||
                               null||'^'||
                              r4.PAID_DATE||'^'||
                              r4.Vendor_Bank_Name||'^'||
                              --r3.Vendor_Branch_Name||'|'||
                              r4.Vendor_Account_Number ||'^'||
                              --r3.RTGS_IFSC_Code||'|'||
                               r4.CCID||'^'||
                              r4.CHECK_NUMBER
                              );
  end loop;


 --utl_file.put_line(x_id,'VENDOR UNPAID INVOICES LIST..................................->');
 -- utl_file.put_line(x_id,'VENDOR UNPAID STANDARD INVOICES LIST..................................->');



     /* FOR r4 IN CUR_PERIOD_NAME5 LOOP

     utl_file.put_line(x_id,   r4.VENDOR_CODE||'|'||
                              r4.VENDOR_NAME||'|'||
                              r4.OU||'|'||
                              r4.SITE_CODE||'|'||
                              r4.INVOICE_NUM||'|'||
                              r4.INVOICE_DATE||'|'||
                              r4.INVOICE_TYPE ||'|'||
                              r4.DESCRIPTION||'|'||
                              r4.DR||'|'||
                              r4.CR||'|'||
                              r4.PAID_AMOUNT||'|'||
                               r4.PREPAY_AMOUNT||'|'||
                              r4.PAID_DATE||'|'||
                              r4.Vendor_Bank_Name||'|'||
                              --r4.Vendor_Branch_Name||'|'||
                              r4.Vendor_Account_Number ||'|'||
                              --r4.RTGS_IFSC_Code||'|'||
                               r4.CCID||'|'||
                              );
  end loop;
   --utl_file.put_line(x_id,'VENDOR UNPAID DEBIT INVOICES LIST..................................->');
    FOR r5 IN CUR_PERIOD_NAME6 LOOP

     utl_file.put_line(x_id,   r5.VENDOR_CODE||'|'||
                              r5.VENDOR_NAME||'|'||
                              r5.OU||'|'||
                              r5.SITE_CODE||'|'||
                              r5.INVOICE_NUM||'|'||
                              r5.INVOICE_DATE||'|'||
                              r5.INVOICE_TYPE ||'|'||
                              r5.DESCRIPTION||'|'||
                              r5.DR||'|'||
                              r5.CR||'|'||
                              r5.PAID_AMOUNT||'|'||
                               null||'|'||
                              r5.PAID_DATE||'|'||
                              r5.Vendor_Bank_Name||'|'||
                              --r5.Vendor_Branch_Name||'|'||
                              r5.Vendor_Account_Number ||'|'||
                              --r5.RTGS_IFSC_Code||'|'||
                               r5.CCID||'|'||
                              r5.CHECK_NUMBER
  end loop;

  --utl_file.put_line(x_id,'VENDOR UNPAID CREDIT INVOICES LIST..................................->');
   FOR r6 IN CUR_PERIOD_NAME7 LOOP

     utl_file.put_line(x_id,   r6.VENDOR_CODE||'|'||
                              r6.VENDOR_NAME||'|'||
                              r6.OU||'|'||
                              r6.SITE_CODE||'|'||
                              r6.INVOICE_NUM||'|'||
                              r6.INVOICE_DATE||'|'||
                              r6.INVOICE_TYPE ||'|'||
                              r6.DESCRIPTION||'|'||
                              r6.DR||'|'||
                              r6.CR||'|'||
                              r6.PAID_AMOUNT||'|'||
                               null||'|'||
                              r6.PAID_DATE||'|'||
                              r6.Vendor_Bank_Name||'|'||
                              --r6.Vendor_Branch_Name||'|'||
                              r6.Vendor_Account_Number ||'|'||
                              --r6.RTGS_IFSC_Code||'|'||
                               r6.CCID||'|'||
                              r6.CHECK_NUMBER
                              );

  --utl_file.put_line(x_id,'VENDOR PARTIAL PAID INVOICES LIST..................................->');

  FOR r7 IN CUR_PERIOD_NAME8 LOOP

     utl_file.put_line(x_id,   r7.VENDOR_CODE||'|'||
                              r7.VENDOR_NAME||'|'||
                              r7.OU||'|'||
                              r7.SITE_CODE||'|'||
                              r7.INVOICE_NUM||'|'||
                              r7.INVOICE_DATE||'|'||
                              r7.INVOICE_TYPE ||'|'||
                              r7.DESCRIPTION||'|'||
                              r7.DR||'|'||
                              r7.CR||'|'||
                              r7.PAID_AMOUNT||'|'||
                                r7.PREPAY_AMOUNT||'|'||
                              r7.PAID_DATE||'|'||
                              --r6.Vendor_Branch_Name||'|'||
                              r7.Vendor_Account_Number ||'|'||
                             -- r7.RTGS_IFSC_Code||'|'||
                               r7.CCID||'|'||
                              r7.CHECK_NUMBER
                              );
  end loop;*/


  EXCEPTION
 WHEN TOO_MANY_ROWS THEN
    dbms_output.PUT_LINE('Too Many Rows');
 WHEN NO_DATA_FOUND THEN
    dbms_output.PUT_LINE('No Data Found');
 WHEN utl_file.invalid_path THEN
    RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_path');
 WHEN utl_file.invalid_mode THEN
     RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_mode');
 WHEN utl_file.invalid_filehandle THEN
     RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_filehandle');
 WHEN utl_file.invalid_operation THEN
     RAISE_APPLICATION_ERROR(-20000, 'utl_file.invalid_operation');
 WHEN utl_file.read_error THEN
     RAISE_APPLICATION_ERROR(-20001, 'utl_file.read_error');
 WHEN utl_file.write_error THEN
     RAISE_APPLICATION_ERROR(-20001, 'utl_file.write_error');
 WHEN utl_file.internal_error THEN
     RAISE_APPLICATION_ERROR(-20001, 'utl_file.internal_error');
 WHEN OTHERS THEN
      dbms_output.put_line(SubStr('Error '||TO_CHAR(SQLCODE)||': '||SQLERRM, 1,255));
utl_file.fclose(x_id);
END; 
/


CREATE OR REPLACE PACKAGE BODY APPS.XXABRL_RECEIPT_REC_PKG AS
PROCEDURE XXABRL_RECEIPT_REC_PROC (ERRBUFF OUT VARCHAR2,
                                      RETCODE OUT NUMBER,
									                  --  P_LEDGER IN VARCHAR2,
                                      P_FROM_CUSTOMER IN VARCHAR2,
                                      P_TO_CUSTOMER IN VARCHAR2,
									                    P_LIST_OF_BANKS IN VARCHAR2,
                                      P_RECEIPT_METHOD IN VARCHAR2,
                                      P_RECEIPT_FROM_DATE IN DATE,
                                      P_RECEIPT_TO_DATE IN DATE,
                                      P_GL_FROM_DATE    IN DATE,
                                      P_GL_TO_DATE      IN DATE,
                                     -- P_BANK_ACCOUNT_NUM IN VARCHAR2,
                                      P_RECEIPT_STATUS IN VARCHAR2
                                      )  IS

   QUERY_STRING LONG;
   LEDGER_STRING LONG;
   RANGE_CUSTOMER_STRING LONG;
   LIST_BANKS_STRING LONG;
   RECEIPT_METHOD_STRING LONG;
   RANGE_RECEIPT_DATE_STRING LONG;
   RECEIPT_DATE_STRING LONG;
   --BANK_ACCOUNT_STRING LONG;
   RECEIPT_STATUS_STRING LONG;
   RANGE_GL_DATE_STRING LONG;
   GL_DATE_STRING   LONG;

   TYPE REF_CUR IS REF CURSOR ;
C REF_CUR;

TYPE C_REC IS RECORD(
V_BANK_NAME               AR_CASH_RECEIPTS_V.REMIT_BANK_NAME%TYPE,
V_BANK_ACC_NUM             AR_CASH_RECEIPTS_V.REMIT_BANK_ACCOUNT%TYPE,
V_CUSTOMER_NUMBER             AR_CASH_RECEIPTS_V.CUSTOMER_NUMBER%TYPE,
V_CUSTOMER_NAME              AR_CASH_RECEIPTS_V.CUSTOMER_NAME%TYPE,
V_DOCUMENT_NUMBER              AR_CASH_RECEIPTS_V.DOCUMENT_NUMBER%TYPE,
V_NAME                        AR_RECEIPT_METHODS.NAME%TYPE,
V_RECEIPT_NUMBER            AR_CASH_RECEIPTS_V.RECEIPT_NUMBER%TYPE,
V_RECEIPT_DATE             AR_CASH_RECEIPTS_V.RECEIPT_DATE%TYPE,
V_RECEIPT_AMOUNT           AR_CASH_RECEIPTS_V.AMOUNT%TYPE,
V_RECEIPT_STATUS              AR_CASH_RECEIPTS_V.STATE%TYPE,
V_BANK_DEBIT_DATE                      AR_CASH_RECEIPT_HISTORY_ALL.TRX_DATE%TYPE
 );

V_REC C_REC;

    BEGIN

 QUERY_STRING:='SELECT
 ACR.REMIT_BANK_NAME        REMIT_BANK_NAME
,ACR.REMIT_BANK_ACCOUNT     REMIT_BANK_ACCOUNT
,ACR.CUSTOMER_NUMBER        CUSTOMER_NUMBER
,ACR.CUSTOMER_NAME          CUSTOMER_NAME
,ACR.DOCUMENT_NUMBER 		DOCUMENT_NUMBER
,ARM.NAME                   RECEIPT_METHOD
,ACR.RECEIPT_NUMBER         RECEIPT_NUMBER
,ACR.RECEIPT_DATE           RECEIPT_DATE
,ACR.AMOUNT				         RECEIPT_AMOUNT
,ACR.STATE                 RECEIPT_STATUS
,(SELECT TRX_DATE  FROM AR_CASH_RECEIPT_HISTORY_ALL ACH
WHERE  ACR.CASH_RECEIPT_ID=ACH.CASH_RECEIPT_ID
AND ach.status=''CLEARED''
 AND ROWNUM=1)  BANK_DEBIT_DATE
FROM AR_CASH_RECEIPTS_V  ACR
,AR_RECEIPT_METHODS   ARM
 WHERE ACR.RECEIPT_METHOD_ID=ARM.RECEIPT_METHOD_ID ';

 /*' SELECT
 ACR.REMIT_BANK_NAME        REMIT_BANK_NAME
,ACR.REMIT_BANK_ACCOUNT     REMIT_BANK_ACCOUNT
,ACR.CUSTOMER_NUMBER        CUSTOMER_NUMBER
,ACR.CUSTOMER_NAME          CUSTOMER_NAME
,ACR.DOCUMENT_NUMBER 		DOCUMENT_NUMBER
,ARM.NAME                   RECEIPT_METHOD
,ACR.RECEIPT_NUMBER         RECEIPT_NUMBER
,ACR.RECEIPT_DATE           RECEIPT_DATE
,ACR.AMOUNT				   RECEIPT_AMOUNT
,ACR.STATE                 RECEIPT_STATUS
,ACR.GL_DATE               BANK_DEBIT_DATE
FROM AR_CASH_RECEIPTS_V  ACR
,AR_RECEIPT_METHODS   ARM
 ,HZ_CUST_ACCOUNTS_ALL HCA
 ,HZ_CUST_ACCT_SITES_ALL  HCAS
 ,HZ_PARTIES  HP
 ,HZ_PARTY_SITES HS
 --,AR_CASH_RECEIPT_HISTORY_ALL ACH
WHERE ACR.RECEIPT_METHOD_ID=ARM.RECEIPT_METHOD_ID
 AND HCA.CUST_ACCOUNT_ID=ACR.CUSTOMER_ID
 AND hp.PARTY_ID=hs.PARTY_ID
AND  hs.PARTY_ID=hca.PARTY_ID
AND hca.CUST_ACCOUNT_ID=hcas.CUST_ACCOUNT_ID ';
*/

--LEDGER_STRING:='AND OOD.SET_OF_BOOKS_ID ='''||P_LEDGER||''' ';

RANGE_CUSTOMER_STRING:=' AND ACR.CUSTOMER_NUMBER BETWEEN '''||P_FROM_CUSTOMER ||''' AND '''||P_TO_CUSTOMER||''' ';

LIST_BANKS_STRING:=' AND ACR.REMIT_BANK_ACCOUNT = '''||P_LIST_OF_BANKS||''' ';

--RANGE_RECEIPT_DATE_STRING:=' AND ACR.RECEIPT_DATE BETWEEN '''||P_RECEIPT_FROM_DATE||''' AND '''||P_RECEIPT_TO_DATE||''' ';
RANGE_RECEIPT_DATE_STRING:='AND to_date(to_char(ACR.RECEIPT_DATE,''DD-MON-YY'')) BETWEEN '''||P_RECEIPT_FROM_DATE||''' AND '''||P_RECEIPT_TO_DATE||'''';
RECEIPT_DATE_STRING:=' AND ACR.RECEIPT_DATE = ACR.RECEIPT_DATE ';

RECEIPT_METHOD_STRING:=' AND ARM.NAME ='''||P_RECEIPT_METHOD||''' ';

RANGE_GL_DATE_STRING:='AND to_date(to_char(ACR.GL_DATE  ,''DD-MON-YY'')) BETWEEN '''||P_GL_FROM_DATE||''' AND '''||P_GL_TO_DATE||'''';
GL_DATE_STRING:=' AND ACR.GL_DATE   = ACR.GL_DATE  ';

--BANK_ACCOUNT_STRING:=' AND AC.BANK_ACCOUNT_NUM ='''||P_BANK_ACCOUNT_NUM||''' ';

RECEIPT_STATUS_STRING:=' AND ACR.STATE ='''||P_RECEIPT_STATUS||''' ';


  /* IF(P_LEDGER IS NOT NULL) THEN
   QUERY_STRING:=QUERY_STRING||LEDGER_STRING;
   ELSIF(P_LEDGER IS NULL) THEN
   QUERY_STRING:=QUERY_STRING;
   END IF;
   */



	IF P_FROM_CUSTOMER IS NOT NULL AND P_TO_CUSTOMER IS NOT NULL  THEN
        QUERY_STRING:= QUERY_STRING || RANGE_CUSTOMER_STRING;
  ELSE
  QUERY_STRING:= QUERY_STRING ;
END IF;


	IF  P_LIST_OF_BANKS IS NOT NULL THEN
        QUERY_STRING:= QUERY_STRING || LIST_BANKS_STRING;
  ELSE
  QUERY_STRING:= QUERY_STRING;
END IF;


   	IF  P_RECEIPT_METHOD IS NOT NULL THEN
        QUERY_STRING:= QUERY_STRING || RECEIPT_METHOD_STRING;
  ELSE
  QUERY_STRING:= QUERY_STRING;
END IF;


    IF (P_RECEIPT_FROM_DATE IS NOT NULL AND P_RECEIPT_TO_DATE IS NOT NULL) THEN
        QUERY_STRING:= QUERY_STRING ||RANGE_RECEIPT_DATE_STRING;
    ELSIF (P_RECEIPT_FROM_DATE IS  NULL AND P_RECEIPT_TO_DATE IS NULL) THEN
         QUERY_STRING:= QUERY_STRING ||RECEIPT_DATE_STRING;
END IF;



  IF (P_GL_FROM_DATE IS NOT NULL AND P_GL_TO_DATE IS NOT NULL) THEN
        QUERY_STRING:= QUERY_STRING ||RANGE_GL_DATE_STRING;
    ELSIF (P_GL_FROM_DATE IS  NULL AND P_GL_TO_DATE IS NULL) THEN
         QUERY_STRING:= QUERY_STRING ||GL_DATE_STRING;
END IF;

   /* IF P_BANK_ACCOUNT_NUM IS NOT NULL THEN
	    QUERY_STRING:=QUERY_STRING||BANK_ACCOUNT_STRING;
    ELSIF P_BANK_ACCOUNT_NUM IS  NULL THEN
		QUERY_STRING:=QUERY_STRING ;
    END IF;
    */

	IF P_RECEIPT_STATUS IS NOT NULL THEN
	   QUERY_STRING:=QUERY_STRING||RECEIPT_STATUS_STRING;
	 ELSIF P_RECEIPT_STATUS IS NULL THEN
	   QUERY_STRING:=QUERY_STRING||'ORDER BY ACR.STATE DESC ';
	   END IF;


	   Fnd_File.put_line(Fnd_File.LOG,'string: '||QUERY_STRING);

		   Fnd_File.put_line(Fnd_File.OUTPUT,'ABRL RECEIPT RECONCILIATION STATUS REPORT');

		    Fnd_File.put_line(Fnd_File.OUTPUT,' ');

      Fnd_File.put_line(Fnd_File.OUTPUT,' ');

	  Fnd_File.put_line(Fnd_File.OUTPUT,CHR(9) ||'REPORT PARAMETERS ');


              /*  Fnd_File.put_line(Fnd_File.OUTPUT,
                CHR(9)  || 'Ledger ' || CHR(9) ||
              P_LEDGER|| CHR(9));*/
			/*  Fnd_File.put_line(Fnd_File.OUTPUT,
                 CHR(9) || 'Bank Account List ' || CHR(9) ||
              P_BANK_ACCOUNT_NUM|| CHR(9));
*/
							Fnd_File.PUT_LINE(Fnd_File.OUTPUT,
                CHR(9) || 'Customer Low ' || CHR(9) ||
                     P_FROM_CUSTOMER || CHR(9));

                     Fnd_File.put_line(Fnd_File.OUTPUT,
                CHR(9)  || 'Customer High ' || CHR(9) ||
                        P_TO_CUSTOMER|| CHR(9));


        Fnd_File.put_line(Fnd_File.OUTPUT,
                CHR(9) || 'Bank List' || CHR(9) ||
                                P_LIST_OF_BANKS|| CHR(9));


    Fnd_File.put_line(Fnd_File.OUTPUT,
                CHR(9) || 'Receipt Method' || CHR(9) ||
                                P_RECEIPT_METHOD|| CHR(9));

                Fnd_File.put_line(Fnd_File.OUTPUT,
                CHR(9) || 'Receipt Date From '   ||
              P_RECEIPT_FROM_DATE||
                CHR(9) || 'Receipt Date To '    ||
              P_RECEIPT_TO_DATE|| CHR(9));



			    Fnd_File.put_line(Fnd_File.OUTPUT,
                CHR(9) || 'GL Date Low' || CHR(9) ||
                                P_GL_FROM_DATE|| CHR(9));


      Fnd_File.put_line(Fnd_File.OUTPUT,
                CHR(9) || 'GL Date Low' || CHR(9) ||
                                P_GL_TO_DATE|| CHR(9));

                Fnd_File.put_line(Fnd_File.OUTPUT,
                CHR(9) || 'Receipt Status' || CHR(9) ||
              P_RECEIPT_STATUS|| CHR(9));




                 Fnd_File.put_line(Fnd_File.OUTPUT,' ');
                   Fnd_File.put_line(Fnd_File.OUTPUT,' ');


                       Fnd_File.put_line(Fnd_File.OUTPUT,
					     'Bank Account Name'  || CHR(9) ||
						 'Bank Account Num'   || CHR(9) ||
					     'Customer Number'    || CHR(9) ||
               'Customer Name'      || CHR(9) ||
						 'Document Number'          || CHR(9) ||
						 'Receipt Method'     || CHR(9) ||
						 'Receipt NUMBER'       || CHR(9) ||
						 'Receipt DATE '       || CHR(9) ||
						 'Receipt Amount'     || CHR(9) ||
             'Receipt Status' || CHR(9)||
             'Bank Debit Date' || CHR(9) ) ;

              OPEN C FOR QUERY_STRING;

				LOOP

				FETCH C INTO V_REC;

				EXIT WHEN C%NOTFOUND;

				Fnd_File.put_line(Fnd_File.OUTPUT,
				   v_rec.V_BANK_NAME  || CHR(9) ||
				   v_rec.V_BANK_ACC_NUM || CHR(9) ||
           v_rec.V_CUSTOMER_NUMBER || CHR(9) ||
				   v_rec.V_CUSTOMER_NAME	|| CHR(9) ||
				   v_rec.V_DOCUMENT_NUMBER	|| CHR(9) ||
				   v_rec.V_NAME      || CHR(9) ||
				   v_rec.V_RECEIPT_NUMBER  || CHR(9) ||
				   v_rec.V_RECEIPT_DATE  || CHR(9) ||
				   v_rec.V_RECEIPT_AMOUNT   || CHR(9) ||
				   v_rec.V_RECEIPT_STATUS || CHR (9)||
           v_rec.V_BANK_DEBIT_DATE     || CHR (9)
				                          );

                   END LOOP;

				  CLOSE C;

				END XXABRL_RECEIPT_REC_PROC;

			END   XXABRL_RECEIPT_REC_PKG;
/


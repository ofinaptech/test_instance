CREATE OR REPLACE PACKAGE BODY APPS.XXABRL_AP_INV_RECON_PKG12 AS
PROCEDURE XXABRL_AP_INV_RECON_PROC12(ERRBUFF             OUT VARCHAR2,
                                RETCODE             OUT NUMBER,
                                P_SOURCE            IN  VARCHAR2,
                                        P_OPERATING_UNIT    IN  VARCHAR2,
                                P_INV_DATE_FROM     IN VARCHAR2,
                                P_INV_DATE_TO       IN VARCHAR2
                                ) AS
          v_LINE_DIFF             NUMBER(20,2);
          v_HEADER_DIFF           NUMBER(20,2);
CURSOR CUR1(CP_SOURCE VARCHAR2,CP_OPERATING_UNIT VARCHAR2,CP_INV_DATE_FROM VARCHAR2,CP_INV_DATE_TO VARCHAR2) IS
SELECT   xana.operating_unit, xana.invoice_date,
         xana.vendor_number vendor_number, xana.invoice_num, xana.grn_number,
         NVL (xana.header_amount, 0) nav_header_amount,
         NVL (xana.line_amount, 0) nav_line_amount,
         NVL ((SELECT NVL (aia.invoice_amount, 0)
                 FROM apps.ap_invoices_all aia
                WHERE aia.invoice_id = aili.invoice_id),
              0) ofin_header_amount,
         NVL (SUM (aili.amount), 0) ofin_line_amount
    FROM apps.xxabrl_ap_nav_recon_audit xana,
         apps.ap_invoice_lines_all aili,
         apps.hr_operating_units hou
   WHERE xana.invoice_date = aili.accounting_date(+)
     AND xana.grn_number = aili.reference_1(+)
     AND xana.operating_unit = hou.short_code
-- and aili.org_id=hou.organization_id
  and  aili.org_id(+)=CP_OPERATING_UNIT
     AND xana.source=CP_SOURCE
     AND hou.organization_id =CP_OPERATING_UNIT
    --and  xana.operating_unit='ABRL ROM'
     and xana.invoice_date BETWEEN NVL (TO_DATE(CP_INV_DATE_FROM,'YYYY/MM/DD HH24:MI:SS'), xana.invoice_date)
                            AND NVL (TO_DATE(CP_INV_DATE_TO,'YYYY/MM/DD HH24:MI:SS'), xana.invoice_date)
     --and xana.grn_number='PDN0910/000001'
     GROUP BY xana.operating_unit,
         xana.invoice_date,
         xana.vendor_number,
         xana.invoice_num,
         xana.grn_number,
         xana.header_amount,
         xana.line_amount,
         aili.invoice_id;
BEGIN
FND_FILE.PUT_LINE (FND_FILE.OUTPUT,
                         'XXABRL AP INVOICE RECONCILIATION REPORT'||'('||P_SOURCE||')'
                        );
      FND_FILE.PUT_LINE (FND_FILE.OUTPUT, ' ');
      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'AS ON DATE'
                         || CHR (9)
                         || SYSDATE
                         || CHR (9)
                        );
      FND_FILE.PUT_LINE (FND_FILE.OUTPUT, ' ');
      FND_FILE.PUT_LINE (FND_FILE.OUTPUT, 'REPORT PARAMETRS');
      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'SOURCE'
                         || CHR (9)
                         || P_SOURCE
                         || CHR (9)
                        );
      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'OPERATING UNIT'
                         || CHR (9)
                         ||  P_OPERATING_UNIT
                         || CHR (9)
                        );
      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'INVOICE DATE FROM'
                         || CHR (9)
                         || P_INV_DATE_FROM
                         || CHR (9)
                        );
      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'INVOICE DATE TO'
                         || CHR (9)
                         || P_INV_DATE_TO
                         || CHR (9)
                        );
-------------------------------------- DISPLAY LABELS OF THE REPORT --------------------------------------------
      FND_FILE.PUT_LINE (FND_FILE.OUTPUT, ' ');
      FND_FILE.PUT_LINE (FND_FILE.OUTPUT, ' ');
      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,
                           'OPERATING UNIT'
                         || CHR (9)
                         || 'INVOICE DATE'
                         || CHR (9)
                         || 'VENDOR NUMBER'
                         || CHR (9)
                         || 'INVOICE NUMBER'
                         || CHR (9)
                         || 'GRN NUMBER'
                         || CHR (9)
                         || 'NAVISION HEADER AMOUNT'
                         || CHR (9)
                         || 'NAVISION LINE AMOUNT'
                         || CHR (9)
                         || 'ORACLE HEADER AMOUNT'
                         || CHR (9)
                         || 'ORACLE LINE AMOUNT'
                         || CHR (9)
                         ||'HEADER DIFF'
                         || CHR (9)
                         ||'LINE DIFF'
                         );
FOR REC1 IN CUR1(P_SOURCE,P_OPERATING_UNIT,P_INV_DATE_FROM,P_INV_DATE_TO) LOOP
EXIT WHEN CUR1%NOTFOUND;
BEGIN
v_HEADER_DIFF :=0;
v_HEADER_DIFF :=REC1.nav_header_amount-REC1.ofin_header_amount;
END;
BEGIN
v_LINE_DIFF :=0;
v_LINE_DIFF :=REC1.nav_line_amount-rec1.ofin_line_amount;
END;
-------------------------- PRINTING THE DATA  ----------------------------------------------------------------
          FND_FILE.PUT_LINE (FND_FILE.OUTPUT,
                               REC1.OPERATING_UNIT
                            || CHR (9)
                            ||   REC1.INVOICE_DATE
                            || CHR (9)
                            || REC1.VENDOR_NUMBER
                            || CHR (9)
                            || REC1.INVOICE_NUM
                            || CHR (9)
                            || REC1.GRN_NUMBER
                            || CHR (9)
                            || REC1.nav_header_amount
                            || CHR (9)
                            || REC1.nav_line_amount
                            || CHR (9)
                            || REC1.ofin_header_amount
                            || CHR (9)
                            || rec1.ofin_line_amount
                            || CHR (9)
                            ||v_HEADER_DIFF
                            || CHR (9)
                            ||v_LINE_DIFF
                            );
END LOOP;
EXCEPTION
When OTHERS then
fnd_file.put_line(fnd_file.output,'No Data Found.');
END XXABRL_AP_INV_RECON_PROC12;
END  XXABRL_AP_INV_RECON_PKG12; 
/


CREATE OR REPLACE PROCEDURE APPS.ar_sales_reg_proc (
   errorbuf      OUT      VARCHAR2,
   retcode       OUT      VARCHAR2,
   p_org_id      IN       NUMBER,
   p_from_date   IN       VARCHAR2,
   p_to_date     IN       VARCHAR2
)
IS
BEGIN
   /*
        ========================
     =========================
     =========================
     =========================
     ====
        ||   Procedure Name  : APPS.AR_SALES_REG_PROC
        ||   Description :  GST ABRL AR - Sales Register Report
        ||
        ||    Date                                     Author                                    Modification
        || ~~~~~~~~~~~            ~~~~~~~~~~~~~~~~~~          ~~~~~~~~~~~~~~~~~
        || 06-JUN-2018             Lokesh Poojari                          New Development
        ========================
     =========================
     =========================
     =========================
     ====*/
   BEGIN
      fnd_file.put_line
         (fnd_file.output,
          'TYPE|TRX NUM|TRX DATE|DOC SEQ VAL|DOC DATE|OU NAME|SELF GSTIN|CUST NUM|CUST NAME|CUST ADDRESS|CUST GSTIN|ITEM CODE|ITEM DESC|HSN SAC|SBU|SBU DESC|QNT|TAXABLE AMOUNT|GST RATE|SGST TAX|CGST TAX|IGST TAX|TOT INV AMOUNT'
         );

      FOR sal_ser_rep IN
         (         
         SELECT TYPE, trx_number, trx_date, doc_sequence_value,
                 document_date, operating_unit, self_gstin, customer_no,
                 customer_name, customer_address, customer_gstin, item_code,
                 NVL (item_description, item_description1) item_descripiton,
                 hsn_sac, sbu, sbu_desc, qnt, taxable_amount,
                 ROUND (  (  NVL (igst_tax, 0)
                           + NVL (cgst_tax, 0)
                           + NVL (sgst_tax, 0)
                          )
                        / taxable_amount
                        * 100,
                        2
                       ) gst_rate,
               --  trx_line_number, 
                 sgst_tax, cgst_tax, igst_tax,
                 tot_inv_amoiunt
            FROM (SELECT DISTINCT rctl.TYPE, rcta.trx_number, rcta.trx_date,
                                  rcta.doc_sequence_value,
                                  rcta.creation_date document_date,
                                  jtl.trx_line_number, hou.NAME operating_unit,
                                  (SELECT DISTINCT registration_number
                                              FROM apps.jai_party_regs jpr,
                                                   apps.jai_party_reg_lines jprl
                                             WHERE 1 = 1
                                               AND jpr.party_reg_id =
                                                             jprl.party_reg_id
                                               AND jpr.party_id = rcta.org_id
                                               AND jpr.party_type_code = 'OU'
                                               AND jpr.org_classification_code =
                                                                     'TRADING'
                                                AND jprl.registration_type_code =
                                                                              'GSTIN'
                                                                              )
                                                                   self_gstin,
                                  hca.account_number customer_no,
                                  hp.party_name customer_name,
                                     hp.address1
                                  || ','
                                  || hp.address2
                                  || ','
                                  || hp.address3
                                  || ','
                                  || hp.address4 customer_address,
                                  (SELECT DISTINCT jprl.registration_number
                                              FROM apps.hz_cust_accounts hc,
                                                   apps.hz_cust_acct_sites_all hcas,
                                                   apps.jai_party_regs jpr,
                                                   apps.jai_party_reg_lines jprl
                                             WHERE 1 = 1
                                               AND hc.cust_account_id =
                                                          hcas.cust_account_id
                                               AND hc.cust_account_id =
                                                                  jpr.party_id
                                               AND jpr.party_site_id =
                                                        hcas.cust_acct_site_id
                                               AND hc.account_number =
                                                            hca.account_number
                                               AND jpr.org_id = hcas.org_id
                                               AND jpr.party_reg_id =
                                                             jprl.party_reg_id
                                               AND registration_type_code =
                                                                       'GSTIN'
                                               AND ROWNUM <= 1)
                                                               customer_gstin,
                                  (SELECT msib.segment1
                                     FROM apps.mtl_system_items_b msib
                                    WHERE 1 = 1
                                      AND inventory_item_id = jtl.item_id
                                      AND organization_id = rcta.org_id)
                                                                    item_code,
                                  (SELECT msib.description
                                     FROM apps.mtl_system_items_b msib
                                    WHERE 1 = 1
                                      AND msib.inventory_item_id = jtl.item_id
                                      AND organization_id = rcta.org_id
                                      AND ROWNUM <= 1) item_description,
                                  (SELECT rctla.description
                                     FROM apps.ra_customer_trx_lines_all rctla
                                    WHERE 1 = 1
                                      AND rctla.customer_trx_id =
                                                          rcta.customer_trx_id
                                      AND ROWNUM <= 1) item_description1,
                                  NVL (jtdl.hsn_code, jtdl.sac_code) hsn_sac,
                                  (SELECT DISTINCT segment3
                                              FROM apps.gl_code_combinations_kfv kfv
                                             WHERE 1 = 1
                                               AND kfv.code_combination_id =
                                                      rctlg.code_combination_id
                                               AND ROWNUM <= 1) sbu,
                                  (SELECT ffvl.description
                                     FROM apps.fnd_flex_values_vl ffvl,
                                          apps.gl_code_combinations_kfv kfv
                                    WHERE ffvl.flex_value_set_id = 1013466
                                      AND ffvl.flex_value = kfv.segment3
                                      AND rctlg.code_combination_id =
                                                       kfv.code_combination_id)
                                                                     sbu_desc,
                                  (SELECT SUM (trx_line_quantity)
                                     FROM apps.jai_tax_det_fct_lines_v
                                    WHERE trx_id = jtdl.trx_id
                                      AND trx_line_id = jtdl.trx_line_id) qnt,
                                  (SELECT NVL (SUM (amount),
                                               0)
                                     FROM apps.ra_cust_trx_line_gl_dist_all
                                    WHERE customer_trx_id =
                                                          rcta.customer_trx_id
                                      AND customer_trx_line_id =
                                                               jtl.trx_line_id
                                      AND account_class = 'REV')
                                                               taxable_amount,
                                  (SELECT NVL
                                             (SUM
                                                 (DECODE
                                                     (jtl.rec_tax_amt_funcl_curr,
                                                      0, jtl.nrec_tax_amt_trx_curr,
                                                      jtl.rec_tax_amt_funcl_curr
                                                     )
                                                 ),
                                              0
                                             ) tax_amount
                                     FROM apps.jai_tax_lines
                                    WHERE trx_id = jtdl.trx_id
                                      AND trx_line_id = jtdl.trx_line_id
                                      AND entity_code = 'TRANSACTIONS'
                                      AND tax_rate_code LIKE '%SGST%')
                                                                     sgst_tax,
                                  (SELECT NVL
                                             (SUM
                                                 (DECODE
                                                     (jtl.rec_tax_amt_funcl_curr,
                                                      0, jtl.nrec_tax_amt_trx_curr,
                                                      jtl.rec_tax_amt_funcl_curr
                                                     )
                                                 ),
                                              0
                                             ) tax_amount
                                     FROM apps.jai_tax_lines
                                    WHERE trx_id = jtdl.trx_id
                                      AND trx_line_id = jtdl.trx_line_id
                                      AND entity_code = 'TRANSACTIONS'
                                      AND tax_rate_code LIKE '%CGST%')
                                                                     cgst_tax,
                                  (SELECT NVL
                                             (SUM
                                                 (DECODE
                                                     (jtl.rec_tax_amt_funcl_curr,
                                                      0, jtl.nrec_tax_amt_trx_curr,
                                                      jtl.rec_tax_amt_funcl_curr
                                                     )
                                                 ),
                                              0
                                             ) tax_amount
                                     FROM apps.jai_tax_lines
                                    WHERE trx_id = jtdl.trx_id
                                      AND trx_line_id = jtdl.trx_line_id
                                      AND entity_code = 'TRANSACTIONS'
                                      AND tax_rate_code LIKE '%IGST%')
                                                                     igst_tax,
                                  (SELECT NVL (SUM (amount),
                                               0
                                              )
                                     FROM apps.ra_cust_trx_line_gl_dist_all
                                    WHERE customer_trx_id =
                                                          rcta.customer_trx_id
                                      AND account_class <> 'REC')
                                                              tot_inv_amoiunt
                             FROM apps.ra_cust_trx_types_all rctl,
                                  apps.ra_customer_trx_all rcta,
                                  apps.hr_operating_units hou,
                                  apps.hz_cust_accounts hca,
                                  apps.hz_parties hp,
                                  apps.jai_tax_lines jtl,
                                  apps.jai_tax_det_fct_lines_v jtdl,
                                  apps.ra_cust_trx_line_gl_dist_all rctlg
                            WHERE 1 = 1
                              AND rctl.cust_trx_type_id =
                                                         rcta.cust_trx_type_id
                              AND rcta.org_id = hou.organization_id
                              AND hca.party_id = hp.party_id
                              AND jtl.party_id = hca.cust_account_id
                              AND jtl.trx_id = rcta.customer_trx_id
                              AND jtl.trx_id = jtdl.trx_id
                              AND jtl.trx_line_id = jtdl.trx_line_id
                              AND jtl.det_factor_id = jtdl.det_factor_id
                              AND rcta.customer_trx_id = rctlg.customer_trx_id
                              AND UPPER (hca.customer_class_code) IN
                                     ('CAPEX TRANSFER', 'OTHERS', 'SCRAP',
                                      'SHOPPING MALL', 'SIS')
                              AND TRUNC (rctlg.gl_date)
                                     BETWEEN TO_DATE (p_from_date,
                                                      'YYYY/MM/DD HH24:MI:SS'
                                                     )
                                         AND TO_DATE (p_to_date,
                                                      'YYYY/MM/DD HH24:MI:SS'
                                                     )
                              AND hou.organization_id =
                                           NVL (p_org_id, hou.organization_id)                                  
                                                                               --  AND rcta.trx_number = '12690'
                                                                              -- AND rctlg.gl_date BETWEEN '01-apr-2018' AND '30-apr-2018'
         ) aa
           WHERE 1 = 1
             AND (   aa.taxable_amount <> 0
                  OR aa.cgst_tax <> 0
                  OR aa.sgst_tax <> 0
                  OR aa.igst_tax <> 0
                 )
                 )
      LOOP
         fnd_file.put_line (fnd_file.output,
                               sal_ser_rep.TYPE
                            || '|'
                            || sal_ser_rep.trx_number
                            || '|'
                            || sal_ser_rep.trx_date
                            || '|'
                            || sal_ser_rep.doc_sequence_value
                            || '|'
                            || sal_ser_rep.document_date
                            || '|'
                            || sal_ser_rep.operating_unit
                            || '|'
                            || sal_ser_rep.self_gstin
                            || '|'
                            || sal_ser_rep.customer_no
                            || '|'
                            || sal_ser_rep.customer_name
                            || '|'
                            || sal_ser_rep.customer_address
                            || '|'
                            || sal_ser_rep.customer_gstin
                            || '|'
                            || sal_ser_rep.item_code
                            || '|'
                            || sal_ser_rep.item_descripiton
                            || '|'
                            || sal_ser_rep.hsn_sac
                            || '|'
                            || sal_ser_rep.sbu
                            || '|'
                            || sal_ser_rep.sbu_desc
                            || '|'
                            || sal_ser_rep.qnt
                            || '|'
                            || sal_ser_rep.taxable_amount
                            || '|'
                            || sal_ser_rep.gst_rate
                            || '|'
                            || sal_ser_rep.sgst_tax
                            || '|'
                            || sal_ser_rep.cgst_tax
                            || '|'
                            || sal_ser_rep.igst_tax
                            || '|'
                            || sal_ser_rep.tot_inv_amoiunt
                           );
      END LOOP;
   END;
END; 
/


CREATE OR REPLACE PROCEDURE APPS.xxabrl_add_ou_site (
   errbuf    OUT   VARCHAR2,
   retcode   OUT   NUMBER
)
IS
   flag   VARCHAR2 (200);

   CURSOR c1
   IS
      SELECT ss.ROWID, ss.*
        FROM xxabrl.xxabrl_add_ou_sites ss
       WHERE NVL (status_flag, 'N') = 'N';
-- AND org_id = 1501;
BEGIN
   FOR i IN c1
   LOOP
      BEGIN
         UPDATE xxabrl.xxabrl_add_ou_sites
            SET ou_name =
                   (SELECT DISTINCT hou.NAME
                               FROM apps.hr_operating_units hou
                              WHERE 1 = 1
                                AND hou.organization_id = i.org_id
                                AND hou.organization_id NOT IN
                                       (663, 801, 821, 841, 861, 881, 901,
                                        921, 941, 961, 981, 83, 84, 85, 86,
                                        87, 88, 89, 90, 91, 92, 93, 94, 95,
                                        96, 681, 682, 121, 122, 82, 541, 101,
                                        102, 1361)
                                AND ROWNUM <= 1)
          WHERE ROWID = i.ROWID;
      EXCEPTION
         WHEN OTHERS
         THEN
             fnd_file.put_line (fnd_file.LOG,'error in Ou name update');
            flag := 'E';

            UPDATE xxabrl.xxabrl_add_ou_sites
               SET  status_flag= 'E'
             WHERE ROWID = i.ROWID;

            COMMIT;
      END;

      BEGIN
         UPDATE xxabrl.xxabrl_add_ou_sites
            SET existing_ou_name =
                   ((SELECT DISTINCT hou.NAME
                                FROM apps.ap_suppliers aps,
                                     apps.ap_supplier_sites_all apsa,
                                     apps.hr_operating_units hou
                               WHERE 1 = 1
                                 AND aps.vendor_id = apsa.vendor_id
                                 AND apsa.org_id = hou.organization_id
                                 AND aps.segment1 = i.parent_vendor
                                 AND apsa.attribute14 = i.retek_vendor_code
                                 AND apsa.org_id NOT IN
                                        (663, 801, 821, 841, 861, 881, 901,
                                         921, 941, 961, 981, 83, 84, 85, 86,
                                         87, 88, 89, 90, 91, 92, 93, 94, 95,
                                         96, 681, 682, 121, 122, 82, 541, 101,
                                         102, 1361)
                               --  AND aps.end_date_active IS NULL
                                -- AND apsa.inactive_date IS NULL
                                 --AND assa.org_id = xx_rec.org_id
                                 AND ROWNUM <= 1))
          WHERE ROWID = i.ROWID;
      EXCEPTION
         WHEN OTHERS
         THEN
             fnd_file.put_line (fnd_file.LOG,'error in exsting Ou name update');
            flag := 'E';

            UPDATE xxabrl.xxabrl_add_ou_sites
               SET status_flag= 'E'
             WHERE ROWID = i.ROWID;

            COMMIT;
      END;

      BEGIN
         UPDATE xxabrl.xxabrl_add_ou_sites
            SET existing_org_id =
                   ((SELECT DISTINCT hou.organization_id
                                FROM apps.ap_suppliers aps,
                                     apps.ap_supplier_sites_all apsa,
                                     apps.hr_operating_units hou
                               WHERE 1 = 1
                                 AND aps.vendor_id = apsa.vendor_id
                                 AND apsa.org_id = hou.organization_id
                                 AND aps.segment1 = i.parent_vendor
                                 AND apsa.attribute14 = i.retek_vendor_code
                                 AND apsa.org_id NOT IN
                                        (663, 801, 821, 841, 861, 881, 901,
                                         921, 941, 961, 981, 83, 84, 85, 86,
                                         87, 88, 89, 90, 91, 92, 93, 94, 95,
                                         96, 681, 682, 121, 122, 82, 541, 101,
                                         102, 1361)
                             --    AND aps.end_date_active IS NULL
                            --     AND apsa.inactive_date IS NULL
                                 --AND assa.org_id = xx_rec.org_id
                                 AND ROWNUM <= 1))
          WHERE ROWID = i.ROWID;

         COMMIT;
      EXCEPTION
         WHEN OTHERS
         THEN
             fnd_file.put_line (fnd_file.LOG,'error in existing org id update');
            flag := 'E';

            UPDATE xxabrl.xxabrl_add_ou_sites
               SET status_flag= 'E'
             WHERE ROWID = i.ROWID;

            COMMIT;
      END;
   END LOOP;

   FOR xx_rec IN c1
   LOOP
      flag := 'N';
         fnd_file.put_line
                       (fnd_file.LOG,
                        '---------------------------------------------------');
     fnd_file.put_line (fnd_file.LOG,'inserting data into Supplier Interrface Table ');

      BEGIN
         INSERT INTO apps.ap_supplier_sites_int
                     (vendor_site_interface_id, vendor_id, vendor_site_code,
                      address_line1, address_line2, address_line3,
                      address_line4, city, state, country, zip, attribute5,
                      attribute6, area_code, phone, fax_area_code, fax,
                      payment_method_lookup_code, terms_date_basis,
                      terms_name, purchasing_site_flag, pay_site_flag,
                      org_id, operating_unit_name, attribute_category,
                      attribute14, attribute15, party_site_id, location_id,
                      creation_date, attribute8, attribute7,
                      global_attribute2, global_attribute5,
                      global_attribute6, global_attribute7,
                      global_attribute8, global_attribute_category,
                      attribute1, attribute2, attribute3, attribute4,
                      attribute9, attribute10, attribute11, attribute12,
                      attribute13, vendor_site_code_alt,
                      pay_group_lookup_code, match_option,
                      invoice_currency_code, hold_all_payments_flag,
                      hold_unmatched_invoices_flag,
                      hold_future_payments_flag, hold_reason, address_style)
            SELECT apps.ap_supplier_sites_int_s.NEXTVAL, asl.vendor_id,
                   vendor_site_code, RTRIM (address_line1),
                   RTRIM (address_line2), RTRIM (address_line3),
                   RTRIM (address_line4), RTRIM (city), RTRIM (state),
                   RTRIM (country), zip, asl.attribute5, asl.attribute6,
                   area_code, phone, fax_area_code, fax,
                   ieppm.payment_method_code, asl.terms_date_basis, ap.NAME,
                   purchasing_site_flag, pay_site_flag, xx_rec.org_id,
                   xx_rec.ou_name, asl.attribute_category, asl.attribute14,
                   asl.attribute15, NULL, hz.location_id, SYSDATE,
                   asl.attribute8, asl.attribute7, asl.global_attribute2,
                   asl.global_attribute5, asl.global_attribute6,
                   asl.global_attribute7, asl.global_attribute8,
                   asl.global_attribute_category, asl.attribute1,
                   asl.attribute2, asl.attribute3, asl.attribute4,
                   asl.attribute9, asl.attribute10, asl.attribute11,
                   asl.attribute12, asl.attribute13,
                   asl.vendor_site_code_alt, asl.pay_group_lookup_code,
                   asl.match_option, asl.invoice_currency_code,
                   asl.hold_all_payments_flag,
                   asl.hold_unmatched_invoices_flag,
                   asl.hold_future_payments_flag, asl.hold_reason,
                   asl.address_style
              FROM apps.ap_suppliers asp,
                   apps.ap_supplier_sites_all asl,
                   apps.hr_operating_units hr,
                   apps.ap_terms ap,
                   apps.hz_party_sites hz,
                   (SELECT supplier_site_id, ext_payee_id
                      FROM apps.iby_external_payees_all) iepa,
                   (SELECT payment_method_code, ext_pmt_party_id,
                           primary_flag
                      FROM apps.iby_ext_party_pmt_mthds) ieppm
             WHERE 1 = 1
               AND segment1 = xx_rec.parent_vendor
               AND asl.attribute14 = xx_rec.retek_vendor_code
               AND asl.vendor_id = asp.vendor_id
               AND org_id = xx_rec.existing_org_id
               AND asl.org_id = hr.organization_id
               AND ap.term_id = asl.terms_id
               AND hz.location_id = asl.location_id
               AND hz.status = 'A'
               AND hz.party_site_id = asl.party_site_id
               AND iepa.supplier_site_id(+) = asl.vendor_site_id
               AND ieppm.ext_pmt_party_id(+) = iepa.ext_payee_id
               AND ieppm.primary_flag(+) = 'Y';
             --  AND asp.end_date_active IS NULL
              -- AND asl.inactive_date IS NULL;

         UPDATE xxabrl.xxabrl_add_ou_sites
            SET status_flag = 'Y'
          WHERE ROWID = xx_rec.ROWID;
          
          
   fnd_file.put_line (fnd_file.LOG,
                            'Successfully inserted into interface'
                           );
         fnd_file.put_line (fnd_file.LOG, 'PARENT_VENDOR=>' || xx_rec.PARENT_VENDOR);
         fnd_file.put_line (fnd_file.LOG,
                               'RETEK_VENDOR_CODE=>'
                            || xx_rec.RETEK_VENDOR_CODE
                           );
         fnd_file.put_line (fnd_file.LOG, 'OU_NAME=>' || xx_rec.OU_NAME);
         fnd_file.put_line
                        (fnd_file.LOG,
                         '---------------------------------------------------'
                        );
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (fnd_file.LOG,
                                  'error occured=>'
                               || xx_rec.RETEK_VENDOR_CODE
                               || '=>'
                               || 'OU_NAME=>'
                               || xx_rec.org_id
                              );
            fnd_file.put_line (fnd_file.LOG, SQLCODE || '=>' || SQLERRM);
                  
      END;

      COMMIT;
   END LOOP;
END; 
/


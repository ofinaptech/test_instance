CREATE OR REPLACE PROCEDURE APPS.xxabrl_axis_pay_advise_rep1 (
   errbuf    OUT   VARCHAR2,
   retcode   OUT   VARCHAR2
)
AS
   v_req_id   NUMBER;
BEGIN
   v_req_id :=
      fnd_request.submit_request
                           ('XXABRL',
                            'XXABRL_AXIS_PAYMENT_ADVISE_REP',
                            '',
                            '',
                            FALSE,
                            TO_CHAR (  LAST_DAY (ADD_MONTHS (TRUNC (SYSDATE),
                                                             -1
                                                            )
                                                )
                                     + 1,
                                     'YYYY/MM/DD HH24:MI:SS'
                                    ),
                            TO_CHAR (TRUNC (SYSDATE) - 1,
                                     'YYYY/MM/DD HH24:MI:SS'
                                    ),
                            '',
                            ''
                           );
   COMMIT;
   DBMS_OUTPUT.put_line ('Request_id:' || v_req_id);
   DBMS_OUTPUT.put_line (SUBSTR ('Error ' || TO_CHAR (SQLCODE) || ': '
                                 || SQLERRM,
                                 1,
                                 255
                                )
                        );
END; 
/


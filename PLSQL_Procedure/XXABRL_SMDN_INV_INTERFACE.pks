CREATE OR REPLACE PACKAGE APPS.XXABRL_SMDN_INV_INTERFACE IS
       PROCEDURE DEBIT_NOTE_VALIDATE_INVOICES(Errbuf       OUT VARCHAR2
                                  ,RetCode      OUT NUMBER);
       PROCEDURE DEBIT_NOTE_INSERT_TRANSACTION(p_org_id IN NUMBER,
                             p_vendor_site_id IN NUMBER,
                             p_vendor_id IN NUMBER,
                             p_payment_method_lookup_code IN VARCHAR2);
END XXABRL_SMDN_INV_INTERFACE; 
/


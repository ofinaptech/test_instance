CREATE OR REPLACE Package APPS.XXABRL_PAYMENT_BATCH_PKG IS

gc_header_str varchar2(3000) := 'Transaction_Type~Beneficiary_Code~Beneficiary_Account_Number~Instrument_Amount~Beneficiary_Name~Drawee_Location~Print_Location~Bene_Address_1~Bene_Address_2~Bene_Address_3~Bene_Address_4~Bene_Address_5~Instruction_Reference_Number~Customer_Reference_Number~Payment_details_1~Payment_details_2~Payment_details_3~Payment_details_4~Payment_details_5~Payment_details_6~Payment_details_7~Cheque_Number~Chq / Trn Date~MICR_Number~IFC_Code~Bene_Bank_Name~Bene_Bank_Branch_Name~Beneficiary_email_ id';


Procedure MAIN(
                             P_Errbuf      OUT VARCHAR2,
                             P_RetCode     OUT NUMBER,
                             p_vendor_id IN VARCHAR2,                             
                             p_start_date IN Date,
                             p_end_date IN Date,
                             p_org_code IN VARCHAR2,                             
                             p_org_id IN NUMBER,
                             p_checkrun_id IN VARCHAR2
                             );



Procedure LOAD_GLOBAL_TABLE(
                             /*P_Errbuf      OUT VARCHAR2,
                             P_RetCode     OUT NUMBER,*/
                             p_start_date IN Date,
                             p_end_date IN Date,
                             p_org_id IN NUMBER,
                             p_checkrun_id IN VARCHAR2,
                             p_vendor_id IN VARCHAR2);

Procedure DISPLAY_DATA(p_debug_flag IN Varchar2,p_checkrun_id IN VARCHAR2);

Procedure Print_log(p_str IN Varchar2, p_debug_flag in Varchar2);


END XXABRL_PAYMENT_BATCH_PKG;
/


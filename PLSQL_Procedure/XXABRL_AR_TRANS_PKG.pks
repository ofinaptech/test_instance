CREATE OR REPLACE PACKAGE APPS.XXABRL_AR_TRANS_PKG AS
PROCEDURE  XXABRL_AR_TRANS_PROC (ERRBUFF OUT VARCHAR2,
                                    RETCODE OUT number,
                                    P_FROM_CUSTOMER  VARCHAR2,
                                    P_TO_CUSTOMER   VARCHAR2,
                                   P_LIST_CUSTOMER  VARCHAR2,
                                    P_TRX_FROM_NUM VARCHAR2,
                                    P_TRX_TO_NUM  VARCHAR2,
                                    P_TRX_FROM_DATE  DATE,
                                    P_TRX_TO_DATE DATE,
                                    P_TRX_TYPE VARCHAR2,
                                    P_TRX_STATUS VARCHAR2                                                                 
                                    );
                                   END XXABRL_AR_TRANS_PKG; 
/


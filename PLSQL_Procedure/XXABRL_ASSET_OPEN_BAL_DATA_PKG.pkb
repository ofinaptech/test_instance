CREATE OR REPLACE PACKAGE BODY APPS.XXABRL_ASSET_OPEN_BAL_DATA_PKG IS

PROCEDURE XXABRL_ASSET_LOAD(ERRBUFF             OUT VARCHAR2,
                                RETCODE             OUT NUMBER,
                                P_PRG_FLAG   IN VARCHAR2) IS

 /**********************************************************************************************************************************************
                        WIPRO Infotech Ltd, Mumbai, India

            Name: XXABRL Asset Load Program Package

            Change Record: -- Created by Amresh
           =========================================================================================================================
           Version   Date          Author                    Remarks                  Documnet Ref
           =======   ==========   =============             ==================  =====================================================
           1.0.0                                          Initial Version
           1.0.1     07-Sep-2012   Amresh Kumar Chutke    Added two columns ATTRIBUTE17 and ATTRIBUTE18 
                                                          to the cursors CUR_VALIDATE and CUR_INSERT, so that it will insert
                                                          the records in the mass additions base table. 
           1.0.2     07-Sep-2012   Amresh Kumar Chutke    Enhanced the performance of the package
                                                           by adding a condition which will only pick up current
                                                           dated records based on creation_date  
           1.0.3     12-Sep-2012   Amresh Kumar Chutke    Added one column ATTRIBUTE19 to store Old Mass Addition ID for Migration Purpose.
           
            Usage : To validate, update asset records into stage table and insert into mass additions base table.   
  ************************************************************************************************************************************************/


BEGIN

IF P_PRG_FLAG = 'Y' THEN
BEGIN
XXABRL_ASSET_DATA_VAL_proc;
END;
END IF;

IF P_PRG_FLAG = 'N' THEN
BEGIN
--XXABRL_ASSET_DATA_VAL_proc;
XXABRL_ASSET_DATA_INS_proc;
END;
END IF;


END     XXABRL_ASSET_LOAD;

-- Procedure For Validation Asset data
PROCEDURE XXABRL_ASSET_DATA_VAL_PROC
IS


-- sequence " asset_sr_no_seq" is created for storing the serial number for asset because if asset
--record not inserted in base table and no error occure then we have to re select that record.


  CURSOR CUR_VALIDATE IS

  SELECT ROWID
        ,SR_NO
    ,description
        ,MAJOR
        ,MINOR1
        ,MINOR2
        ,MANUFACTURER_NAME
        ,BOOK_TYPE_NAME
        ,DATE_PLACED_IN_SERVICE
        ,FIXED_ASSETS_COST
        ,BASIC_RATE
        ,ADJUSTED_RATE
        ,PAYABLES_UNITS
        ,FIXED_ASSETS_UNITS
        ,PAY_CCID_CO
        ,PAY_CCID_DEPT
        ,PAY_CCID_STATE_SBU
        ,PAY_CCID_LOCATION
        ,PAY_CCID_MERCHANDIZE
        ,PAY_CCID_ACCOUNT
        ,PAY_CCID_INTERCOMPANY
        ,PAY_CCID_FUTURE
        ,EXP_CCID_CO
        ,EXP_CCID_DEPT
        ,EXP_CCID_STATE_SBU
        ,EXP_CCID_LOCATION
        ,EXP_CCID_MERCHANDIZE
        ,EXP_CCID_ACCOUNT
        ,EXP_CCID_INTERCOMPANY
        ,EXP_CCID_FUTURE
        ,LOCATION_CODES
        ,REVIEWER_COMMENTS
        ,INVOICE_NUMBER
        ,PO_VENDOR_NUMBER
        ,PO_NUMBER
        ,POSTING_STATUS
        ,QUEUE_NAME
        ,PAYABLES_COST
        ,DEPRECIATE_FLAG
        ,YEAR_data
        ,PROJECT_NUMBER
        ,ASSET_TYPE
        ,DEPRN_RESERVE
        ,YTD_DEPRN
    ,SALVAGE_VALUE
    ,ACCOUNTING_DATE
        ,INVENTORIAL
        ,OWNED_LEASED
        ,ATTRIBUTE17 -- added by Amresh on 07-Sep-2012
        ,ATTRIBUTE18 -- added by Amresh on 07-Sep-2012
        ,ATTRIBUTE19 -- added by Amresh on 12-Sep-2012
        ,ERROR_FLAG
        ,error_message
  FROM xxabrl_fa_data_load_stg_new3
  WHERE ERROR_FLAG IN ('E','N');





  -- Variable Declaration
  v_err_flag VARCHAR2(1);
  v_err_msg VARCHAR2(5000);

  v_category_id NUMBER;
  v_location_id NUMBER;
  v_vendor_id NUMBER;
  v_asset_key_ccid NUMBER;
  v_pay_ccid NUMBER;
  v_exp_ccid NUMBER;
  v_BOOK_TYPE_CODE VARCHAR2(50);


  v_all_count    NUMBER:=0;
  v_error_count  NUMBER:=0;
  v_insert_count  NUMBER:=0;

        ln_exp_ccid          NUMBER;
      ln_loc_ccid          NUMBER;

            v_segments fnd_flex_ext.segmentarray;
      v_segments1 fnd_flex_ext.segmentarray;

      l_struct_num         NUMBER;

           retrun_value     BOOLEAN;

BEGIN

   DBMS_OUTPUT.PUT_LINE('start of prog');

   FOR I IN CUR_VALIDATE
   LOOP
   ---
   Fnd_File.put_line (Fnd_File.LOG,'start for SR_NO : '||I.SR_NO);
   -- Validation For catagory Values
   BEGIN



        IF i.MINOR2 IS NULL THEN

        SELECT category_id
        INTO   v_category_id
        FROM   fa_categories_b
        WHERE   segment1 =  i.MAJOR
        AND     segment2 =  i.MINOR1
        AND     segment3 IS NULL;

        ELSE

        SELECT category_id
        INTO   v_category_id
        FROM   fa_categories_b
        WHERE   segment1 = i.MAJOR
        AND     segment2 =  i.MINOR1
        AND     segment3 =  i.MINOR2;

        END IF;

        

   EXCEPTION
         WHEN OTHERS
         THEN





        v_category_id:=NULL;
        v_err_flag := 'E';
        v_err_msg :=  'No Category Found for Desire combinition of catagories';

     Fnd_File.put_line (Fnd_File.LOG,'No Category Found for Desire combinition of catagories : for sl no '||I.SR_NO);

   END;

   -- Validation For the Location
   BEGIN
         SELECT location_id
           INTO v_location_id
           FROM fa_locations
          WHERE UPPER (segment1) = UPPER (i.LOCATION_CODES);

   EXCEPTION
         WHEN OTHERS
         THEN
            v_err_flag := 'E';
            v_err_msg :=  ' - Location is not available in application or Location is wrong''for sl no '||I.SR_NO;

         Fnd_File.put_line (Fnd_File.LOG,' Location is not available in application or Location is wrong: '||i.LOCATION_CODES || ' for sl no '||I.SR_NO);
   END;


   -- Validation For PO Vendor Number


   BEGIN

 IF i.PO_VENDOR_NUMBER IS NOT NULL THEN
   SELECT vendor_id
   INTO v_vendor_id
   FROM po_vendors
   WHERE UPPER(segment1) = UPPER(i.PO_VENDOR_NUMBER);
 END IF;
   EXCEPTION
            WHEN OTHERS
            THEN

            v_err_flag := 'E';
            v_err_msg := ' -  Vendor Name not found in application';

        Fnd_File.put_line (Fnd_File.LOG,'Error on vendor name derivation : '||i.PO_VENDOR_NUMBER || ' for sl no '||I.SR_NO);


   END;





   -- Validation For Asset KeyFlex Field


   BEGIN
IF i.PROJECT_NUMBER IS NOT NULL  THEN
        SELECT CODE_COMBINATION_ID
        INTO v_asset_key_ccid
        FROM FA_ASSET_KEYWORDS
        WHERE
       SEGMENT1 = i.YEAR_data
        AND segment2=i.PROJECT_NUMBER;
   END IF;
   EXCEPTION
            WHEN OTHERS THEN
            v_err_flag := 'E';
            v_err_msg :=  ' -  Combinition of Project_Number and Year is not Found ';

     Fnd_File.put_line (Fnd_File.LOG,'Error on Project No and Year derivation : '|| ' '|| 'Project NO:'||i.PROJECT_NUMBER||' '|| 'Year:'||i.YEAR_data ||'for sl no '||I.SR_NO);


   END;




   -- Validation for Payable Accounting code ( Payables CCID )
    BEGIN
         SELECT code_combination_id
           INTO v_pay_ccid
           FROM gl_code_combinations
          WHERE segment1 = i.PAY_CCID_CO
            AND segment2 = i.PAY_CCID_DEPT
            AND segment3 = i.PAY_CCID_STATE_SBU
            AND segment4 = i.PAY_CCID_LOCATION
            AND segment5 = i.PAY_CCID_MERCHANDIZE
            AND segment6 = i.PAY_CCID_ACCOUNT
            AND segment7 = i.PAY_CCID_INTERCOMPANY
            AND segment8 = i.PAY_CCID_FUTURE;


      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
    /*         v_err_flag := 'E';
             v_err_msg :=  ' - Payable Accounting code Not Found';

       Fnd_File.put_line (Fnd_File.LOG,'Exception on Payable Accounting code derivation :  for sl no '||I.SR_NO);
       Fnd_File.put_line (Fnd_File.LOG,'Exception on Payable Accounting code ccid_co : ' ||i.PAY_CCID_CO);
       Fnd_File.put_line (Fnd_File.LOG,'Exception on Payable Accounting code PAY_CCID_DEPT : ' ||i.PAY_CCID_DEPT);
       Fnd_File.put_line (Fnd_File.LOG,'Exception on Payable Accounting code PAY_CCID_STATE_SBU : '|| i.PAY_CCID_STATE_SBU);
       Fnd_File.put_line (Fnd_File.LOG,'Exception on Payable Accounting code PAY_CCID_LOCATION : '|| i.PAY_CCID_LOCATION);
       Fnd_File.put_line (Fnd_File.LOG,'Exception on Payable Accounting code PAY_CCID_MERCHANDIZE : ' ||i.PAY_CCID_MERCHANDIZE);
          Fnd_File.put_line (Fnd_File.LOG,'Exception on Payable Accounting code PAY_CCID_ACCOUNT : '|| i.PAY_CCID_ACCOUNT);
          Fnd_File.put_line (Fnd_File.LOG,'Exception on Payable Accounting code PAY_CCID_INTERCOMPANY : '|| i.PAY_CCID_INTERCOMPANY);
          Fnd_File.put_line (Fnd_File.LOG,'Exception on Payable Accounting code PAY_CCID_FUTURE : '|| i.PAY_CCID_FUTURE); */


-------------------------------------------------------------------------------------------------------
           v_segments(1)         := i.pay_ccid_co;
             v_segments(2)         := i.pay_ccid_dept;
             v_segments(3)         := i.pay_ccid_state_sbu;
             v_segments(4)         := i.pay_ccid_location ;
             v_segments(5)         := i.pay_ccid_merchandize;
             v_segments(6)         := i.pay_ccid_account;
         v_segments(7)         := i.pay_ccid_intercompany;
         v_segments(8)         := i.pay_ccid_future;



    SELECT chart_of_accounts_id
    INTO l_struct_num
    FROM gl_sets_of_books
    WHERE NAME = 'TSRL AP Ledger';


    retrun_value := fnd_flex_ext.get_combination_id(application_short_name     => 'SQLGL',
                                                    key_flex_code          => 'GL#',
                                                    structure_number       => l_struct_num,
                                                    validation_date        => SYSDATE,
                                                    n_segments             => 8,
                                                    segments               => v_segments,
                                                    combination_id         => v_pay_ccid,
                                                    data_set               => -1);





-------------------------------------------------------------------------------------------------------

      END;


   -- Validation for Expanse Accounting code ( Expense CCID )
   BEGIN
         SELECT code_combination_id
           INTO v_exp_ccid
           FROM gl_code_combinations
          WHERE segment1 = i.EXP_CCID_CO
            AND segment2 = i.EXP_CCID_DEPT
            AND segment3 = i.EXP_CCID_STATE_SBU
            AND segment4 = i.EXP_CCID_LOCATION
            AND segment5 = i.EXP_CCID_MERCHANDIZE
            AND segment6 = i.EXP_CCID_ACCOUNT
            AND segment7 = i.EXP_CCID_INTERCOMPANY
            AND segment8 = i.EXP_CCID_FUTURE;



      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
    /*         v_err_flag := 'E';
             v_err_msg :=  ' - Expanse Accounting code Not Found';
         Fnd_File.put_line (Fnd_File.LOG,'Error on Expense Accounting code derivation :  for sl no:' ||I.SR_NO);

       Fnd_File.put_line (Fnd_File.LOG,'Exception on Expense Accounting code ccid_co : ' ||i.EXP_CCID_CO );
       Fnd_File.put_line (Fnd_File.LOG,'Exception on Expense Accounting code PAY_CCID_DEPT :'|| i.EXP_CCID_DEPT);
       Fnd_File.put_line (Fnd_File.LOG,'Exception on Expense Accounting code PAY_CCID_STATE_SBU : ' ||i.EXP_CCID_STATE_SBU);
       Fnd_File.put_line (Fnd_File.LOG,'Exception on Expense Accounting code PAY_CCID_LOCATION :' ||i.EXP_CCID_LOCATION);
       Fnd_File.put_line (Fnd_File.LOG,'Exception on Expense Accounting code PAY_CCID_MERCHANDIZE : ' ||i.EXP_CCID_MERCHANDIZE);
          Fnd_File.put_line (Fnd_File.LOG,'Exception on Expense Accounting code PAY_CCID_ACCOUNT : ' ||i.EXP_CCID_ACCOUNT);
          Fnd_File.put_line (Fnd_File.LOG,'Exception on Expense Accounting code PAY_CCID_INTERCOMPANY : ' ||i.EXP_CCID_INTERCOMPANY);
          Fnd_File.put_line (Fnd_File.LOG,'Exception on Expense Accounting code PAY_CCID_FUTURE : '||i.EXP_CCID_FUTURE); */


             v_segments1(1)         := i.exp_ccid_co;
             v_segments1(2)         := i.exp_ccid_dept;
             v_segments1(3)         := i.exp_ccid_state_sbu;
             v_segments1(4)         := i.exp_ccid_location;
             v_segments1(5)         := i.exp_ccid_merchandize;
             v_segments1(6)         := i.exp_ccid_account;
             v_segments1(7)         := i.exp_ccid_intercompany;
             v_segments1(8)         := i.exp_ccid_future;

    SELECT chart_of_accounts_id
    INTO l_struct_num
    FROM gl_sets_of_books
    WHERE NAME = 'TSRL AP Ledger';

    retrun_value := fnd_flex_ext.get_combination_id(application_short_name     => 'SQLGL',
                                                    key_flex_code          => 'GL#',
                                                    structure_number       => l_struct_num,
                                                    validation_date        => SYSDATE,
                                                    n_segments             => 8,
                                                    segments               => v_segments1,
                                                    combination_id         => v_exp_ccid,
                                                    data_set               => -1);

      END;


    --- VALIDATION FOR BOOK TYPE CODE
      BEGIN

           SELECT book_type_code
           INTO v_BOOK_TYPE_CODE
           FROM FA_BOOK_CONTROLS_SEC
           WHERE
           book_type_name  = i.book_type_name;

      EXCEPTION
            WHEN OTHERS THEN
            v_err_flag := 'E';
            v_err_msg :=  ' -  Book Type Name (Code) is Not Found    for sl no '||I.SR_NO;

         Fnd_File.put_line (Fnd_File.LOG,'Book Type Name (Code) is Not Found : '||i.SR_NO );

      END;



      IF v_err_flag = 'E' THEN
         UPDATE xxabrl_fa_data_load_stg_new3
         SET error_flag     =  'E',
             error_message  =  v_err_msg
         --WHERE ROWID=I.ROWID;
     WHERE  TRUNC(CREATION_DATE)=TRUNC(SYSDATE)
     and sr_no = I.sr_no;

         v_err_flag:='';
         v_err_msg:='';

    --     Fnd_File.put_line (Fnd_File.LOG,'Error Occured In : '||i.SR_NO ||' -  '|| v_err_msg );
         v_error_count := v_error_count + 1;
--COMMIT;

      ELSE

          UPDATE xxabrl_fa_data_load_stg_new3
          SET error_flag                   = 'V'
             ,error_message                = ''
             ,asset_category_id            = v_category_id
             ,expense_code_combination_id  = v_exp_ccid
             ,payables_code_combination_id = v_pay_ccid
             ,location_id                  = v_location_id
             ,asset_key_ccid               = v_asset_key_ccid
             ,po_vendor_id                 = v_vendor_id
             ,book_type_name               = v_BOOK_TYPE_CODE
        --  WHERE ROWID=I.ROWID;
      WHERE  TRUNC(CREATION_DATE)=TRUNC(SYSDATE)
      and sr_no = I.sr_no;

          v_insert_count := v_insert_count + 1;

      END IF;



      v_all_count :=  v_all_count + 1;
   END LOOP;
 COMMIT;


     FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'----------------------------------------------------------' );
     FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'                      Validation                          ' );
     FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'Total Lines : '|| v_all_count );
     FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'Total Lines With Error : '|| v_error_count );
     FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'Total Lines Successfully Inserted : '|| v_insert_count );
     FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'----------------------------------------------------------' );

END XXABRL_ASSET_DATA_VAL_PROC;

--- Procedure For The inserting the staging table data in interface table

------------------------------------------------------------------------------------------------------------------------------------------------------------------

                                      -- Insert procedure
------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROCEDURE XXABRL_ASSET_DATA_INS_PROC
IS

CURSOR CUR_INSERT IS

  SELECT ROWID
        ,SR_NO
        ,DESCRIPTION
        ,MAJOR
        ,MINOR1
        ,MINOR2
        ,MANUFACTURER_NAME
        ,BOOK_TYPE_NAME
        ,DATE_PLACED_IN_SERVICE
        ,FIXED_ASSETS_COST
        ,BASIC_RATE
        ,ADJUSTED_RATE
        ,PAYABLES_UNITS
        ,FIXED_ASSETS_UNITS
        ,PAY_CCID_CO
        ,PAY_CCID_DEPT
        ,PAY_CCID_STATE_SBU
        ,PAY_CCID_LOCATION
        ,PAY_CCID_MERCHANDIZE
        ,PAY_CCID_ACCOUNT
        ,PAY_CCID_INTERCOMPANY
        ,PAY_CCID_FUTURE
        ,EXP_CCID_CO
        ,EXP_CCID_DEPT
        ,EXP_CCID_STATE_SBU
        ,EXP_CCID_LOCATION
        ,EXP_CCID_MERCHANDIZE
        ,EXP_CCID_ACCOUNT
        ,EXP_CCID_INTERCOMPANY
        ,EXP_CCID_FUTURE
--        ,LOCATION_ID
        ,LOCATION_CODES
        ,REVIEWER_COMMENTS
        ,INVOICE_NUMBER
        ,PO_VENDOR_NUMBER
        ,PO_NUMBER
        ,POSTING_STATUS
        ,QUEUE_NAME
        ,PAYABLES_COST
        ,DEPRECIATE_FLAG
        ,YEAR_data
        ,PROJECT_NUMBER
        ,ASSET_TYPE
        ,DEPRN_RESERVE
        ,YTD_DEPRN
    ,SALVAGE_VALUE
    ,ACCOUNTING_DATE
        ,INVENTORIAL
        ,OWNED_LEASED
        ,ATTRIBUTE17 -- added by Amresh on 07-Sep-2012
        ,ATTRIBUTE18 -- added by Amresh on 07-Sep-2012
        ,ATTRIBUTE19 -- added by Amresh on 12-Sep-2012
    ,asset_category_id
    ,expense_code_combination_id
    ,payables_code_combination_id
    ,location_id
    ,asset_key_ccid
    ,po_vendor_id
    ,ERROR_MESSAGE
    ,ERROR_FLAG
  FROM xxabrl_fa_data_load_stg_new3
  WHERE ERROR_FLAG IN ('P','V');



  /*  SELECT * FROM APPS.FA_MASS_ADDITIONS

  UPDATE APPS.FA_MASS_ADDITIONS
  SET QUEUE_NAME ='DELETE',
  POSTING_STATUS='DELETE'
  */


  --SR_NO IN ( 958562,  932583)
  --AND ROWNUM<=1000;
/*
 SELECT COUNT(*),CREATION_DATE FROM FA_MASS_ADDITIONS
 GROUP BY CREATION_DATE
  */
/*
  UPDATE xxabrl_fa_data_load_stg_new3
 SET FIXED_ASSETS_UNITS = REPLACE (FIXED_ASSETS_UNITS,',','')
 WHERE ERROR_FLAG IN ('E')
  */

  /*

  SELECT COUNT(*), error_message FROM xxabrl_fa_data_load_stg_new3
  GROUP BY error_message
  */

  /*
  SELECT invoice_number FROM xxabrl_fa_data_load_stg_new3
  WHERE LENGTH(invoice_number) >50

  UPDATE xxabrl_fa_data_load_stg_new3
  SET invoice_number=''
  WHERE LENGTH(invoice_number) >50
  */
 /*
 UPDATE xxabrl_fa_data_load_stg_new3
 SET PAYABLES_COST = REPLACE (PAYABLES_COST,',','')
 WHERE ERROR_FLAG IN ('E')
 */

  v_err_flag    VARCHAR2(1);
  v_err_message VARCHAR2(5000);

  v_all_count  NUMBER:=0;
  v_error_count  NUMBER:=0;
  v_insert_count  NUMBER:=0;

BEGIN

     FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'  Inside insert procedure  ' );
     FOR I IN CUR_INSERT
     LOOP
  -- FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'  Inside insert loop  ' );
         BEGIN

            INSERT INTO fa_mass_additions
                  (mass_addition_id
            ,DESCRIPTION
            ,asset_category_id
            ,book_type_code
            ,manufacturer_name
            ,date_placed_in_service
            ,fixed_assets_cost
            ,BASIC_RATE
            ,ADJUSTED_RATE
            ,payables_units
            ,fixed_assets_units
            ,payables_code_combination_id
            ,expense_code_combination_id
            ,location_id
            ,feeder_system_name
            ,reviewer_comments
            ,invoice_number
            ,po_vendor_id
                  ,PO_NUMBER
            ,posting_status
            ,queue_name
            ,payables_cost
            ,depreciate_flag
            ,asset_key_ccid
            ,asset_type
            ,deprn_reserve
            ,ytd_deprn
            ,inventorial
            ,owned_leased
            ,ATTRIBUTE17 -- added by Amresh on 07-Sep-2012
            ,ATTRIBUTE18 -- added by Amresh on 07-Sep-2012
            ,ATTRIBUTE19 -- added by Amresh on 12-Sep-2012
                  ,LAST_UPDATE_DATE
                  ,LAST_UPDATED_BY
                  ,CREATED_BY
                  ,CREATION_DATE
                  )
            VALUES
            (fa_mass_additions_s.NEXTVAL
            ,i.DESCRIPTION
            ,i.asset_category_id
            ,i.book_type_name
            ,i.manufacturer_name
            ,i.date_placed_in_service
            ,i.fixed_assets_cost
            ,I.BASIC_RATE
            ,I.ADJUSTED_RATE
            ,i.payables_units
            ,i.fixed_assets_units
            ,i.payables_code_combination_id
            ,i.expense_code_combination_id
            ,i.location_id
            ,'Spreadsheet'
            ,i.reviewer_comments
            ,i.invoice_number
            ,i.po_vendor_id
            ,i.po_number
            ,i.POSTING_STATUS
            ,i.QUEUE_NAME
            ,i.payables_cost
            ,i.depreciate_flag
            ,i.asset_key_ccid
            ,i.asset_type
            ,i.deprn_reserve
            ,i.ytd_deprn
            ,'YES'
            ,'OWNED'
            ,i.ATTRIBUTE17 -- added by Amresh on 07-Sep-2012
            ,i.ATTRIBUTE18 -- added by Amresh on 07-Sep-2012
            ,i.ATTRIBUTE19 -- added by Amresh on 12-Sep-2012
            ,SYSDATE
                ,-1
                  ,-1
                  ,SYSDATE
        );

        /*  one single row insert query

        INSERT INTO fa_mass_additions
            (mass_addition_id
            ,DESCRIPTION
            ,asset_category_id
            ,book_type_code
           -- ,transaction_date
            ,manufacturer_name
            ,date_placed_in_service
            ,fixed_assets_cost
            ,payables_units
            ,fixed_assets_units
            ,payables_code_combination_id
            ,expense_code_combination_id
            ,location_id
            ,feeder_system_name
            ,reviewer_comments
            ,invoice_number --
            ,po_vendor_id
                  ,PO_NUMBER
            ,posting_status
            ,queue_name
            ,payables_cost
            ,depreciate_flag
            ,asset_key_ccid
            ,asset_type
            ,deprn_reserve
            ,ytd_deprn --
            ,inventorial
            ,owned_leased
                  ,LAST_UPDATE_DATE
                  ,LAST_UPDATED_BY
                  ,CREATED_BY
                  ,CREATION_DATE
            )
            VALUES
            (fa_mass_additions_s.NEXTVAL
            ,'Consultancy'
            ,6001
            ,'ABRL HO BOOK'
         --   ,TO_DATE('01-dec-2008','dd-mon-rrrr')
            ,'Op'--
            ,TO_DATE('26-Dec-07','dd-mon-rrrr')
            ,132714.00
            ,1
            ,1
            ,3978142
            ,4152032---
            ,3168
            ,'LEGACY'
            ,'BLDG\LH\00008'
            ,'PPI/07-08/0000010746'
            ,538061
            ,'RA2/PO31-9/IV2'
            ,'POST'
            ,'POST'--
            ,132714.00
            ,'YES'
            ,1--
            ,'CAPITALIZED'
            , 24779.00
            , 17744.00--
            ,'YES'
            ,'OWNED'
            ,SYSDATE
            ,-1
            ,-1
            ,SYSDATE
        );

        */


         EXCEPTION
               WHEN OTHERS THEN
                     v_err_flag    := 'E';
                     v_err_message := 'Error while inserting the record into interface table - '|| SQLCODE||'  '||SQLERRM;
         END;

         IF V_ERR_FLAG = 'E' THEN
            UPDATE xxabrl_fa_data_load_stg_new3
            SET   error_flag    = 'E',  --- we put v here so that it will take this rejected recors next time
                  error_message =  v_err_message
            WHERE ROWID=i.ROWID;
            COMMIT;

             v_error_count :=  v_error_count + 1;
         ELSE
              UPDATE xxabrl_fa_data_load_stg_new3
            SET   error_flag    = 'P',  --- we put v here so that it will take this rejected recors next time
                  error_message =  ''
            WHERE ROWID=i.ROWID;

             v_insert_count :=  v_insert_count + 1;

            COMMIT;
         END IF;

         v_all_count := v_all_count + 1;

     END LOOP;

     FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'----------------------------------------------------------' );
     FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'             Insert To Interface Table                    ' );
     FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'Total Lines : '|| v_all_count );
     FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'Total Lines With Error : '|| v_error_count );
     FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'Total Lines Successfully Inserted : '|| v_insert_count );
     FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'----------------------------------------------------------' );

END XXABRL_ASSET_DATA_INS_PROC;



END XXABRL_ASSET_OPEN_BAL_DATA_PKG; 
/


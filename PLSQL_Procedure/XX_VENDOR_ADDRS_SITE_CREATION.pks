CREATE OR REPLACE PACKAGE APPS.xx_vendor_addrs_site_creation
IS
   PROCEDURE xxabrl_vendor_creation (errbuf OUT VARCHAR2, retcode OUT NUMBER);

   PROCEDURE xx_vendor_location_creation (
      errbuf    OUT   VARCHAR2,
      retcode   OUT   NUMBER
   );

   PROCEDURE xx_vendor_party_site_creation (
      errbuf    OUT   VARCHAR2,
      retcode   OUT   NUMBER
   );

   PROCEDURE xx_vendor_site_creation (errbuf OUT VARCHAR2, retcode OUT NUMBER);
END xx_vendor_addrs_site_creation; 
/


CREATE OR REPLACE PACKAGE BODY APPS.xxabrl_happay_ap_inv_interface
IS
---
---    OBJECT INFORMATION
---
---    OBJECT NAME : xxabrl_happy_ap_inv_interface
---
---    DESCRIPTION : MIGRATION OF INVOICES FROM HAPPAY TO ORACLE PAYABLES
---
---    CHANGE RECORD:
---    ---------------------

   ---    VERSION    DATE                AUTHOR                              REMARKS
---    -------------------------------------------------------------------------------
---
---
   PROCEDURE validate_invoices (errbuf OUT VARCHAR2, retcode OUT NUMBER)
   IS
      v_invoice_num                  VARCHAR2 (50)           := NULL;
      v_count                        NUMBER                  := NULL;
      v_vat                          NUMBER                  := NULL;
      v_data_count                   NUMBER;
      v_inactive_site                NUMBER;
      v_error_flag                   VARCHAR2 (1)            := NULL;
      v_error_line_flag              VARCHAR2 (1)            := NULL;
      v_hdr_err_msg                  VARCHAR2 (10000)         := NULL;
      v_line_err_msg                 VARCHAR2 (15000)         := NULL;
      v_error_almsg                  VARCHAR2 (18000)         := NULL;
      v_vendor_id                    NUMBER (15)             := NULL;
      v_vendor_site_id               NUMBER (15)             := NULL;
      v_vendor_site_code             VARCHAR2 (30)           := NULL;
      v_invoice_currency_code        VARCHAR2 (30)           := NULL;
      v_payment_method_lookup_code   VARCHAR2 (30)           := NULL;
      v_dist_code_concatenated       VARCHAR2 (300)          := NULL;
--        FOLLOWING VARIAVBLE CREATED BY SHAILESH ON 25 OCT 2008 FOR STORING THE DEFAULT ACCOUNTING SEGMENT VALUES FOR VENDOR
      l_dist_code_concatenated       VARCHAR2 (300)          := NULL;
      l_nat_acct                     VARCHAR2 (10);
--        WILL STORE THE TERMS_ID FROM THE VENDOR MASTER SEE THE VALIDATION FOR DETAILS
      l_terms_id                     NUMBER;
--        CAPTURE THE LOCATION FROM DIST_CODE_COMBINITION (SEGMENT4) VALUE
      l_location                     VARCHAR2 (10);
      v_check_flag                   VARCHAR2 (1)            := NULL;
      v_fun_curr                     VARCHAR2 (15)           := NULL;
      v_flag_count                   NUMBER                  := 0;
      v_set_of_books_id              NUMBER
                                    := fnd_profile.VALUE ('GL_SET_OF_BKS_ID');
      v_pay_lookup_code              VARCHAR2 (50)           := NULL;
      v_term_name                    ap_terms_tl.NAME%TYPE;
      v_error_head_count             NUMBER                  := 0;
      v_success_record_count         NUMBER                  := 0;
      v_error_line_count             NUMBER                  := 0;
      v_line_type_lookup_code        VARCHAR2 (25)           := NULL;
      v_inv_line_amt                 NUMBER                  := 0;
    --V_USER_ID                                  NUMBER              :
--    FND_PROFILE.VALUE('USER_ID');
    --V_RESP_ID                                  NUMBER              :
--FND_PROFILE.VALUE('RESP_ID');
    --V_APPL_ID                                  NUMBER              :
--FND_PROFILE.VALUE('RESP_APPL_ID');
      v_inv_type                     VARCHAR2 (30)           := NULL;
      v_total_error_records          NUMBER                  := 0;
      v_line_count                   NUMBER                  := 0;
      v_total_records                NUMBER                  := 0;
      v_sup_status                   VARCHAR2 (1)            := NULL;
      v_dummy_flag                   VARCHAR2 (1)            := NULL;
      v_purchasing_site_flag         VARCHAR2 (1)            := NULL;
      v_rfq_only_site_flag           VARCHAR2 (1)            := NULL;
      p_org_id                       NUMBER                  := NULL;
      p_vendor_site_id               NUMBER (15)             := NULL;
      p_vendor_id                    NUMBER (15)             := NULL;
      p_payment_method_lookup_code   VARCHAR2 (30)           := NULL;
      v_pay_site_flag                VARCHAR2 (1)            := NULL;
      v_pay_count                    NUMBER;
      v_rfq_count                    NUMBER;
      v_pur_count                    NUMBER;
      v_ho_count                     NUMBER;
      v_org_cnt                      NUMBER                  := 0;
       x_concatenated_segments        VARCHAR2 (100);
      nv_error_counter               NUMBER;
      x_val_retcode                  NUMBER;
         v_chart_of_accounts_id  NUMBER;

---
---    Declaration of 'INVOICES' cursor
---
      CURSOR cur_inv_hdr
      IS
         SELECT aps.ROWID, aps.invoice_num, aps.invoice_type_lookup_code,
                aps.invoice_date, aps.vendor_num, aps.terms_id,
                ROUND (aps.invoice_amount, 2) invoice_amount,
                aps.invoice_currency_code, aps.exchange_date,
                aps.exchange_rate_type, aps.description, aps.gl_date,
                aps.GROUP_ID, aps.vendor_email_address, aps.SOURCE,
                aps.terms_date, aps.accts_pay_code_concatenated,
                aps.error_flag, aps.error_msg, aps.goods_received_date,
                aps.org_id, aps.vendor_site_id
           FROM apps.xxabrl_happay_ap_h_stage aps
          WHERE 1 = 1
            AND TRUNC (gl_date) > '01-FEB-18'
            AND NVL (aps.error_flag, 'N') IN ('N', 'E');

      CURSOR cur_inv_line (
         p_inv_num      VARCHAR2,
---                p_vendor_site_id NUMBER,
         p_vendor_num   NUMBER,
         p_gl_date      DATE
      )
      IS
         SELECT als.ROWID, als.vendor_num, als.vendor_site_id,
                als.invoice_num, als.line_number, als.line_type_lookup_code,
                als.amount amount, als.accounting_date,
                als.dist_code_concatenated, als.description, als.tds_tax_name,
                als.wct_tax_name, als.service_tax, als.vat
           FROM apps.xxabrl_happay_ap_l_stage als
          WHERE NVL (als.error_flag, 'N') <> 'Y'                 --in('N','E')
            AND als.invoice_num = p_inv_num
            AND als.vendor_num = p_vendor_num
            AND als.accounting_date = p_gl_date;

      --     01.12.2013    Bala.SK    not required due to Unique Vendor Solution        */

      ---
---        01.12.2013    Bala.SK    required due to Unique Vendor Solution
---
---
      CURSOR cur_check_flags (
         p_vendor_num        VARCHAR2,
         cp_org_id           NUMBER,
         cp_vendor_site_id   NUMBER
      )
      IS
         SELECT asups.org_id, asups.vendor_site_id, asup.vendor_id,
                asups.payment_method_lookup_code, asups.pay_site_flag,
                asups.purchasing_site_flag, asups.rfq_only_site_flag
           FROM ap_suppliers asup, ap_supplier_sites_all asups
          WHERE 1 = 1
            AND asup.vendor_id = asups.vendor_id
--            AND (    NVL (asups.pay_site_flag, 'N') = 'Y'
--                 AND NVL (asups.purchasing_site_flag, 'N') = 'N'
--                 AND NVL (asups.rfq_only_site_flag, 'N') = 'N'
--                )
            AND asup.segment1 = p_vendor_num
            AND asups.org_id = cp_org_id
            AND asups.vendor_site_id =
                                 NVL (cp_vendor_site_id, asups.vendor_site_id)
            AND NVL (TRUNC (asups.inactive_date), TRUNC (SYSDATE)) >=
                                                               TRUNC (SYSDATE);
   BEGIN                                                                -- 1st
---    01.11.2013    BALA.SK            PROGRAM STARTS
---
      fnd_file.put_line
         (fnd_file.output,
          '                             AP Invoice Interface Validation Report'
         );
      fnd_file.put_line (fnd_file.output,
                            '    Run Date      : '
                         || TO_CHAR (TRUNC (SYSDATE), 'DD-MON-RRRR')
                        );
      fnd_file.put_line (fnd_file.output, CHR (10));
      fnd_file.put_line
         (fnd_file.output,
          '-------------------------------------------------------------------------------------------------'
         );
--    FND_FILE.PUT_LINE (FND_FILE.OUTPUT,CHR(10));
--    FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'File Date     : '||TO_CHAR(c_file_rec.file_date,'DD-MON-RRRR:HH24:MI:SS'));
--    FND_FILE.PUT_LINE (FND_FILE.OUTPUT,CHR(10));
      fnd_file.put_line (fnd_file.output,
                            RPAD ('Vendor No', 20)
                         || RPAD ('Invoice No', 20)
                         || RPAD ('Error Message', 400)
                        );
      fnd_file.put_line (fnd_file.output,
                         ' SET OF BOOK ID  ->  ' || v_set_of_books_id
                        );

---    05.03.2014        BALA.SK
      SELECT COUNT (invoice_num)
        INTO v_total_records
        FROM apps.xxabrl_happay_ap_h_stage
       WHERE 1 = 1
         AND TRUNC (gl_date) > '31-MAR-13'
         AND NVL (error_flag, 'N') IN ('N', 'E');

--    Get Functional Currency
      BEGIN
         SELECT currency_code
           INTO v_fun_curr
           FROM gl_sets_of_books
          WHERE set_of_books_id = v_set_of_books_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            v_fun_curr := NULL;
      END;

---        1ST LOOP
      FOR rec_inv_hdr IN cur_inv_hdr
      LOOP
         EXIT WHEN cur_inv_hdr%NOTFOUND;
         v_error_flag := NULL;
         v_hdr_err_msg := NULL;
         v_vendor_id := NULL;
         v_vendor_site_id := NULL;
         v_invoice_currency_code := NULL;
         v_payment_method_lookup_code := NULL;
--    REC_INV_HDR.VENDOR_NUM                     := NULL;
         v_check_flag := 'N';
         v_pay_lookup_code := NULL;
--    V_ERROR_HEAD_COUNT                        := 0;
         v_inv_line_amt := 0;
         v_inv_type := NULL;
--    V_SUCCESS_RECORD_COUNT                    := 0;
         v_sup_status := 'N';
--    P_ORG_ID                                           := NULL;
--    P_VENDOR_SITE_ID                              := NULL;
--    P_VENDOR_ID                                        := NULL;
--    P_PAYMENT_METHOD_LOOKUP_CODE     := NULL;
--    V_PAY_SITE_FLAG                                := NULL;
         v_flag_count := 0;
         v_pay_count := 0;
         v_rfq_count := 0;
         v_pur_count := 0;
         v_ho_count := 0;

--    ORG ID VALIDATION ADDED BY SHAILESH ON 22 JUNE 2009
         BEGIN
            fnd_file.put_line (fnd_file.LOG,
                                  'VALIDATION FOR ORG ID -> '
                               || rec_inv_hdr.org_id
                              );

            SELECT COUNT (organization_id)
              INTO v_org_cnt
              FROM apps.hr_operating_units hou
             WHERE hou.organization_id = rec_inv_hdr.org_id;
--    WE NEED TO SKIP FULL VALIDATION IN THIS CASE IF ORG_ID IS NULL
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               v_error_flag := 'Y';
               v_hdr_err_msg := 'ORG IS NULL OR ORG NOT AVAILABLE';
               fnd_file.put_line (fnd_file.LOG,
                                     'ORG IS NULL OR ORG NOT AVAILABLE-> '
                                  || v_error_flag
                                 );
            WHEN OTHERS
            THEN
               v_error_flag := 'Y';
               v_hdr_err_msg := 'ORG IS NULL OR ORG NOT AVAILABLE';
               fnd_file.put_line (fnd_file.LOG,
                                  'Error in ORG ID -> ' || v_error_flag
                                 );
         END;

         IF v_org_cnt = 0
         THEN
            v_error_flag := 'Y';
            v_hdr_err_msg := 'ORG IS NULL OR ORG NOT AVAILABLE';
            p_vendor_site_id := NULL;
            p_vendor_id := NULL;
            p_payment_method_lookup_code := NULL;
            fnd_file.put_line (fnd_file.LOG,
                               'INVALID ORG ID -> ' || v_error_flag
                              );
         END IF;

---    FND_FILE.PUT_LINE(FND_FILE.log,'Error Flag -> '||v_error_flag);
         IF rec_inv_hdr.vendor_num IS NULL
         THEN
            DBMS_OUTPUT.put_line ('NO SUPPLIER FOR INVOICE NUMBER');
         ELSE
            fnd_file.put_line
                     (fnd_file.LOG,
                         'ORG ID PARAMETER --> cur_inv_hdr 2ND FOR LOOP --> '
                      || rec_inv_hdr.org_id
                     );

            SELECT COUNT (*)
              INTO v_pay_count
              FROM ap_suppliers asup, ap_supplier_sites_all asups
             WHERE 1 = 1
               AND asup.vendor_id = asups.vendor_id
               AND asup.segment1 = rec_inv_hdr.vendor_num
               AND asups.org_id = rec_inv_hdr.org_id
               AND NVL (TRUNC (asups.inactive_date), TRUNC (SYSDATE)) > =
                                                               TRUNC (SYSDATE)
               --  AND NVL (asups.pay_site_flag, 'N') = 'Y'
               AND asups.vendor_site_id =
                        NVL (rec_inv_hdr.vendor_site_id, asups.vendor_site_id);
         END IF;

---    Check for suppliers status
         IF rec_inv_hdr.vendor_num IS NOT NULL
         THEN
            BEGIN
               SELECT 'Y'
                 INTO v_sup_status
                 FROM ap_suppliers
                WHERE 1 = 1
                  AND segment1 = rec_inv_hdr.vendor_num
                  AND SYSDATE BETWEEN NVL (start_date_active, SYSDATE)
                                  AND NVL (end_date_active, SYSDATE);
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  v_error_flag := 'Y';
                  v_hdr_err_msg := 'INVALID SUPPLIER';
                  fnd_file.put_line (fnd_file.output,
                                        RPAD (NVL (rec_inv_hdr.vendor_num,
                                                   ' '),
                                              20
                                             )
                                     || RPAD (rec_inv_hdr.invoice_num, 20)
                                     || v_hdr_err_msg
                                    );
            END;
         END IF;

         IF v_sup_status = 'Y'
         THEN
---    2ND LOOP
            FOR rec_check_flags IN
               cur_check_flags (rec_inv_hdr.vendor_num,
                                rec_inv_hdr.org_id,
                                rec_inv_hdr.vendor_site_id
                               )
            LOOP
               EXIT WHEN cur_check_flags%NOTFOUND;

--        ADDED BY GOVIND ON 24 JUNE 2009 VENDOR SITE VALIDATION
               BEGIN
                  IF rec_check_flags.vendor_site_id IS NULL
                  THEN
                     v_error_flag := 'Y';
                     fnd_file.put_line (fnd_file.output,
                                           'VENDOR SITE IS NOT AVAILABLE-> '
                                        || v_error_flag
                                       );
                     p_vendor_site_id := NULL;
                  END IF;
               END;

               IF cur_check_flags%NOTFOUND
               THEN
                  v_error_flag := 'Y';
                  v_hdr_err_msg := 'SUPPLIER IS NOT AVIALABLE';
                  fnd_file.put_line (fnd_file.output,
                                        RPAD (NVL (rec_inv_hdr.vendor_num,
                                                   ' '),
                                              20
                                             )
                                     || RPAD (rec_inv_hdr.invoice_num, 20)
                                     || v_hdr_err_msg
                                    );

                  CLOSE cur_check_flags;
               ELSE
                  IF v_pay_count > 1
                  THEN
                     v_error_flag := 'Y';
                     v_hdr_err_msg := 'MULTIPLE PAY SITE DEFINED';
                     fnd_file.put_line (fnd_file.output,
                                           RPAD (NVL (rec_inv_hdr.vendor_num,
                                                      ' '
                                                     ),
                                                 20
                                                )
                                        || RPAD (rec_inv_hdr.invoice_num, 20)
                                        || v_hdr_err_msg
                                       );
                     EXIT;
                  END IF;

                  IF v_pay_count = 1
                  THEN
                     IF NVL (rec_check_flags.pay_site_flag, 'N') = 'Y'
                     THEN
                        p_org_id := rec_check_flags.org_id;
                        p_vendor_site_id := rec_check_flags.vendor_site_id;
                        p_vendor_id := rec_check_flags.vendor_id;
                        p_payment_method_lookup_code :=
                                   rec_check_flags.payment_method_lookup_code;
                     END IF;
                  ELSE
                     IF v_pay_count = 0
                     THEN
                        v_error_flag := 'Y';
                        v_hdr_err_msg := 'NO PAY SITE DEFINED FOR THE VENDOR';
                        fnd_file.put_line
                                       (fnd_file.output,
                                           RPAD (NVL (rec_inv_hdr.vendor_num,
                                                      ' '
                                                     ),
                                                 20
                                                )
                                        || RPAD (rec_inv_hdr.invoice_num, 20)
                                        || v_hdr_err_msg
                                       );
                        EXIT;
                     END IF;

                     v_error_flag := 'Y';
                     v_hdr_err_msg :=
                        'SUPPLIER SITES SETUP IN APPS NOT AS PER VENDOR OUTBOUND DEFINATION';
                     fnd_file.put_line (fnd_file.output,
                                           RPAD (NVL (rec_inv_hdr.vendor_num,
                                                      ' '
                                                     ),
                                                 20
                                                )
                                        || RPAD (rec_inv_hdr.invoice_num, 20)
                                        || v_hdr_err_msg
                                       );
                     EXIT;
                  END IF;                    --        IF V_PAY_COUNT = 1 THEN
               END IF;              --        IF CUR_CHECK_FLAGS%NOTFOUND THEN
            END LOOP;                --        REC_CHECK_FLAGS        2ND LOOP
         END IF;                        --          IF V_SUP_STATUS = 'Y' THEN

         IF p_vendor_site_id IS NULL
         THEN
            v_error_flag := 'Y';
            v_hdr_err_msg :=
                  v_hdr_err_msg
               || '-'
               || ' VENDOR SITE NOT FOUND FOR VENDOR '
               || rec_inv_hdr.vendor_num
               || '  ';
            fnd_file.put_line (fnd_file.LOG,
                                  'ORG IS NULL OR ORG NOT AVAILABLE-> '
                               || v_error_flag
                              );
            fnd_file.put_line (fnd_file.output,
                                  RPAD (NVL (rec_inv_hdr.vendor_num, ' '), 20)
                               || RPAD (rec_inv_hdr.invoice_num, 20)
                               || v_hdr_err_msg
                              );
         END IF;

         ---        Validation for Invoice Type Lookup Code
         IF rec_inv_hdr.invoice_type_lookup_code IS NULL
         THEN
            v_hdr_err_msg := v_hdr_err_msg || 'INVOICE TYPE IS NULL';
         ELSE
            BEGIN
               SELECT lookup_code
                 INTO v_inv_type
                 FROM ap_lookup_codes
                WHERE UPPER (lookup_code) =
                                  UPPER (rec_inv_hdr.invoice_type_lookup_code)
                  AND lookup_type = 'INVOICE TYPE';
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  v_error_flag := 'Y';
                  v_hdr_err_msg := 'INVOICE TYPE DOES NOT EXISTS';
                  fnd_file.put_line (fnd_file.output,
                                        RPAD (NVL (rec_inv_hdr.vendor_num,
                                                   ' '),
                                              20
                                             )
                                     || RPAD (rec_inv_hdr.invoice_num, 20)
                                     || v_hdr_err_msg
                                    );
               WHEN TOO_MANY_ROWS
               THEN
                  v_error_flag := 'Y';
                  v_hdr_err_msg := 'MULTIPLE INVOICE TYPE FOUND';
               WHEN OTHERS
               THEN
                  v_error_flag := 'Y';
                  v_hdr_err_msg := 'INVALID INVOICE TYPE';
                  fnd_file.put_line (fnd_file.output,
                                        RPAD (NVL (rec_inv_hdr.vendor_num,
                                                   ' '),
                                              20
                                             )
                                     || RPAD (rec_inv_hdr.invoice_num, 20)
                                     || v_hdr_err_msg
                                    );
            END;
         END IF;

---        Check Invoice Number Already exists and Invoice Number Duplication
         IF rec_inv_hdr.invoice_num IS NOT NULL AND v_sup_status = 'Y'
         THEN
            BEGIN
               v_data_count := 0;

               SELECT COUNT (invoice_num)
                 INTO v_data_count
                 FROM ap_invoices_all
                WHERE invoice_num = rec_inv_hdr.invoice_num
                  AND vendor_id = p_vendor_id
             -- added by shailesh on 20 june 2009 as org id comming from CSV file to staging table.
             --AND org_id = p_org_id;
--             AND vendor_site_id=rec_inv_hdr.vendor_site_id             --- added by Dhiresh on 17 Jul 2014
                  AND org_id = rec_inv_hdr.org_id;

               IF v_data_count <> 0
               THEN
                  v_error_flag := 'Y';
                  v_hdr_err_msg :=
                        'INVOICE NUMBER ('
                     || ' '
                     || rec_inv_hdr.invoice_num
                     || ' '
                     || ') ALREADY EXISTS FOR SUPPLIER'
                     || ' '
                     || rec_inv_hdr.vendor_num;
                  fnd_file.put_line (fnd_file.output,
                                        RPAD (NVL (rec_inv_hdr.vendor_num,
                                                   ' '),
                                              20
                                             )
                                     || RPAD (rec_inv_hdr.invoice_num, 20)
                                     || v_hdr_err_msg
                                    );
               END IF;

               v_data_count := 0;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  v_error_flag := 'Y';
                  v_hdr_err_msg := 'INVOICE NUMBER DOES NOT EXISTS';
                  fnd_file.put_line (fnd_file.output,
                                        RPAD (NVL (rec_inv_hdr.vendor_num,
                                                   ' '),
                                              20
                                             )
                                     || RPAD (rec_inv_hdr.invoice_num, 20)
                                     || v_hdr_err_msg
                                    );
               WHEN OTHERS
               THEN
                  v_error_flag := 'Y';
                  v_hdr_err_msg := SQLERRM;
            END;

            BEGIN
               v_data_count := 0;

               SELECT COUNT (invoice_num)
                 INTO v_data_count
                 FROM xxabrl_happay_ap_h_stage
                WHERE invoice_num = rec_inv_hdr.invoice_num
--               AND vendor_site_id=rec_inv_hdr.vendor_site_id         --- added by Dhiresh on 17 Jul 2014
                  AND org_id = rec_inv_hdr.org_id
                  --- added by Dhiresh on 17 Jul 2014
                  AND vendor_num = rec_inv_hdr.vendor_num;

               IF v_data_count > 1
               THEN
                  v_error_flag := 'Y';
                  v_hdr_err_msg :=
                        'DUPLICATE INVOICE NUMBER.THIS INVOICE ALREADY EXISTS FOR'
                     || RPAD (' ', 20)
                     || 'SUPPLIER '
                     || ' '
                     || rec_inv_hdr.vendor_num
                     || ' '
                     || 'AND SUPPLIER SITE'
                     || ' '
                     || v_vendor_site_code;
                  fnd_file.put_line (fnd_file.output,
                                        RPAD (NVL (rec_inv_hdr.vendor_num,
                                                   ' '),
                                              20
                                             )
                                     || RPAD (rec_inv_hdr.invoice_num, 20)
                                     || v_hdr_err_msg
                                    );
               END IF;

               v_data_count := 0;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  v_error_flag := 'Y';
                  v_hdr_err_msg := 'INVOICE NUMBER DOES NOT EXISTS';
                  fnd_file.put_line (fnd_file.output,
                                        RPAD (NVL (rec_inv_hdr.vendor_num,
                                                   ' '),
                                              20
                                             )
                                     || RPAD (rec_inv_hdr.invoice_num, 20)
                                     || v_hdr_err_msg
                                    );
               WHEN OTHERS
               THEN
                  v_error_flag := 'Y';
                  v_hdr_err_msg := v_hdr_err_msg || ' ' || SQLERRM;
            END;
         --   ELSE
         --         v_error_flag := 'Y';
         --         v_hdr_err_msg := v_hdr_err_msg ||' '|| 'INVOICE NUMBER SHOULD NOT BE NULL';
         --         FND_FILE.PUT_LINE (FND_FILE.OUTPUT,v_hdr_err_msg);
         END IF;

---        Validate for Invoice Amount with Transaction Type
         IF     UPPER (rec_inv_hdr.invoice_type_lookup_code) = 'STANDARD'
            AND rec_inv_hdr.invoice_amount < 0
         THEN
            v_error_flag := 'Y';
            v_hdr_err_msg :=
                    'INVOICE AMOUNT CAN NOT BE NEGATIVE FOR STANDARD INVOICE';
            fnd_file.put_line (fnd_file.output,
                                  RPAD (NVL (rec_inv_hdr.vendor_num, ' '), 20)
                               || RPAD (rec_inv_hdr.invoice_num, 20)
                               || v_hdr_err_msg
                              );
         ELSIF     UPPER (rec_inv_hdr.invoice_type_lookup_code) = 'CREDIT'
               AND rec_inv_hdr.invoice_amount >= 0
         THEN
            v_error_flag := 'Y';
            v_hdr_err_msg :=
                         'INVOICE AMOUNT CAN NOT BE POSITIVE FOR CREDIT MEMO';
            fnd_file.put_line (fnd_file.output,
                                  RPAD (NVL (rec_inv_hdr.vendor_num, ' '), 20)
                               || RPAD (rec_inv_hdr.invoice_num, 20)
                               || v_hdr_err_msg
                              );
         END IF;

---    Validate the currency_code in invoice header table
         IF rec_inv_hdr.invoice_currency_code IS NOT NULL
         THEN
            BEGIN
               SELECT currency_code
                 INTO v_invoice_currency_code
                 FROM fnd_currencies
                WHERE UPPER (currency_code) =
                              TRIM (UPPER (rec_inv_hdr.invoice_currency_code));

               IF     UPPER (TRIM (v_invoice_currency_code)) <>
                                                     UPPER (TRIM (v_fun_curr))
                  AND rec_inv_hdr.exchange_date IS NULL
               THEN
                  v_error_flag := 'Y';
                  v_hdr_err_msg := 'EXCHANGE DATE IS NULL';
                  fnd_file.put_line (fnd_file.output,
                                        RPAD (NVL (rec_inv_hdr.vendor_num,
                                                   ' '),
                                              20
                                             )
                                     || RPAD (rec_inv_hdr.invoice_num, 20)
                                     || v_hdr_err_msg
                                    );
               END IF;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  v_error_flag := 'Y';
                  v_hdr_err_msg :=
                        rec_inv_hdr.invoice_currency_code
                     || ' _ '
                     || ' INVOICE_CURRENCY_CODE is not valid';
                  fnd_file.put_line (fnd_file.output,
                                        RPAD (NVL (rec_inv_hdr.vendor_num,
                                                   ' '),
                                              20
                                             )
                                     || RPAD (rec_inv_hdr.invoice_num, 20)
                                     || v_hdr_err_msg
                                    );
            END;
         ELSE
            v_error_flag := 'Y';
            v_hdr_err_msg := 'INVOICE CURRENCY CODE SHOULD NOT BE NULL';
            fnd_file.put_line (fnd_file.output,
                                  RPAD (NVL (rec_inv_hdr.vendor_num, ' '), 20)
                               || RPAD (rec_inv_hdr.invoice_num, 20)
                               || v_hdr_err_msg
                              );
         END IF;

         BEGIN
            SELECT NAME, apt.term_id
              INTO v_term_name, l_terms_id
              FROM ap_terms_tl apt, po_vendor_sites_all pvsa
             WHERE pvsa.terms_id = apt.term_id
               AND pvsa.vendor_id = p_vendor_id
               AND pvsa.vendor_site_id = p_vendor_site_id
               AND NVL (apt.enabled_flag, 'N') = 'Y'
               AND SYSDATE BETWEEN NVL (apt.start_date_active, SYSDATE)
                               AND NVL (apt.end_date_active, SYSDATE);
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               l_terms_id := NULL;
            WHEN TOO_MANY_ROWS
            THEN
               v_error_flag := 'Y';
               v_hdr_err_msg := 'MULTIPLE TERM IDS FOUND';
               fnd_file.put_line (fnd_file.output,
                                     RPAD (NVL (rec_inv_hdr.vendor_num, ' '),
                                           20
                                          )
                                  || RPAD (rec_inv_hdr.invoice_num, 20)
                                  || v_hdr_err_msg
                                 );
            WHEN OTHERS
            THEN
               v_error_flag := 'Y';
               v_hdr_err_msg := ' TERM ID';
               fnd_file.put_line (fnd_file.output,
                                     RPAD (NVL (rec_inv_hdr.vendor_num, ' '),
                                           20
                                          )
                                  || RPAD (rec_inv_hdr.invoice_num, 20)
                                  || v_hdr_err_msg
                                 );
         END;

---    Validation for  ACCTS_PAY_CODE_CONCATENATED
---    New validation for the dist code concatenated  added by shailesh on 25 OCT 2008
---    DIST CODE CONCATENATED SEGMENTS - Default accounting segments from vendor master table
---    it will pick up the only segm,ent6 (natural acct0 value and update the acct code concatenated from the hdr table.
         BEGIN
            SELECT    SUBSTR (rec_inv_hdr.accts_pay_code_concatenated, 1, 22)
                   || gcc.segment6
                   || SUBSTR (rec_inv_hdr.accts_pay_code_concatenated, 29, 9)
              INTO l_dist_code_concatenated
              FROM po_vendors pv,
                   po_vendor_sites_all pvsa,
                   gl_code_combinations gcc
             WHERE pv.vendor_id = pvsa.vendor_id
               AND gcc.code_combination_id =
                                            pvsa.accts_pay_code_combination_id
               AND pv.vendor_id = p_vendor_id
               AND pvsa.vendor_site_id = p_vendor_site_id;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               v_error_line_flag := 'Y';
               v_line_err_msg :=
                          'ACCOUNTING SEGMENTS NOT MAPPED WITH VENDOR MASTER';
            --FND_FILE.PUT_LINE (FND_FILE.OUTPUT,RPAD(rec_inv_line.vendor_num, 20)
            --                                      || RPAD(rec_inv_line.invoice_num, 20)
            --                                     || v_line_err_msg);
            WHEN OTHERS
            THEN
               v_error_line_flag := 'Y';
               v_line_err_msg := 'INVALID DIST CODE CONCATENATED VALUE';
         --FND_FILE.PUT_LINE (FND_FILE.OUTPUT,v_line_err_msg);
         END;

---
---        01.03.2014        BALA.SK
---
         IF     (   SUBSTR (rec_inv_hdr.vendor_num, 1, 2) = '70'
                 OR SUBSTR (rec_inv_hdr.vendor_num, 1, 2) = '75'
                )
            AND rec_inv_hdr.vendor_site_id IS NOT NULL
            AND SUBSTR (l_dist_code_concatenated, 23, 6) <> '362009'
         THEN
            l_dist_code_concatenated :=
                  SUBSTR (l_dist_code_concatenated, 1, 22)
               || '362009'
               || SUBSTR (l_dist_code_concatenated, 29, 9);
         END IF;

---
---        01.03.2014        BALA.SK
---

         ---    Validation for GLdate
         BEGIN
            SELECT set_of_books_id
              INTO v_set_of_books_id
              FROM org_organization_definitions
             WHERE organization_id = rec_inv_hdr.org_id;

            fnd_file.put_line (fnd_file.output,
                               'SOB-ID: ' || v_set_of_books_id
                              );
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               fnd_file.put_line (fnd_file.output,
                                     'SOB not defined for org id: '
                                  || rec_inv_hdr.org_id
                                 );
            WHEN TOO_MANY_ROWS
            THEN
               fnd_file.put_line (fnd_file.output,
                                     'Multiple SOB defined for org id: '
                                  || rec_inv_hdr.org_id
                                 );
            WHEN OTHERS
            THEN
               fnd_file.put_line
                              (fnd_file.output,
                                  'Exception while deriving SOB for org id: '
                               || rec_inv_hdr.org_id
                              );
         END;

         IF rec_inv_hdr.gl_date IS NULL
         THEN
            v_error_flag := 'Y';
            v_hdr_err_msg := 'GL DATE IS NULL';
            fnd_file.put_line (fnd_file.output, v_hdr_err_msg);
         ELSE
            v_data_count := 0;

            BEGIN
               SELECT COUNT (*)
                 INTO v_data_count
                 FROM gl_period_statuses gps, fnd_application fna
                WHERE fna.application_short_name = 'SQLAP'
                  AND gps.application_id = fna.application_id
                  AND gps.closing_status = 'O'
                  AND gps.set_of_books_id = v_set_of_books_id
                  AND TRUNC (NVL (rec_inv_hdr.gl_date, SYSDATE))
                         BETWEEN gps.start_date
                             AND gps.end_date;

               --   AND     TO_DATE('08-OCT-08','DD-MON-YYYY')) BETWEEN GPS.START_DATE AND GPS.END_DATE
               --   AND     TRUNC(NVL(TO_DATE('08-OCT-08','DD-MON-YYYY',SYSDATE)) BETWEEN GPS.START_DATE AND GPS.END_DATE;
               IF v_data_count = 0
               THEN
                  v_error_flag := 'Y';
                  v_hdr_err_msg := 'GL DATE IS NOT IN OPEN PERIOD';
                  fnd_file.put_line (fnd_file.output,
                                        RPAD (NVL (rec_inv_hdr.vendor_num,
                                                   ' '),
                                              20
                                             )
                                     || RPAD (rec_inv_hdr.invoice_num, 20)
                                     || v_hdr_err_msg
                                    );
               END IF;
            EXCEPTION
               WHEN OTHERS
               THEN
                  v_error_flag := 'Y';
                  v_hdr_err_msg := 'GL DATE IS NOT IN OPEN PERIOD';
                  fnd_file.put_line (fnd_file.output,
                                        RPAD (NVL (rec_inv_hdr.vendor_num,
                                                   ' '),
                                              20
                                             )
                                     || RPAD (rec_inv_hdr.invoice_num, 20)
                                     || v_hdr_err_msg
                                    );
            END;
         END IF;

---    DBMS_OUTPUT.PUT_LINE('outsite if part' || ' ' || v_error_flag || ' ' || 'value for ');
         v_error_line_count := 0;
         v_error_almsg := NULL;

---        3RD LOOP
         FOR rec_inv_line IN cur_inv_line (rec_inv_hdr.invoice_num,
---                                    rec_inv_hdr.vendor_site_id,
                                           rec_inv_hdr.vendor_num,
                                           rec_inv_hdr.gl_date
                                          )
         LOOP
            v_line_count := cur_inv_line%ROWCOUNT;
            v_dist_code_concatenated := NULL;
            v_error_line_flag := NULL;
            v_line_err_msg := NULL;
            v_line_type_lookup_code := NULL;
            v_data_count := 0;

            -- IF rec_inv_line.line_type_lookup_code IS NOT NULL THEN
            --    SELECT line_type_lookup_code
            --    INTO   v_line_type_lookup_code
            --    FROM   apps.xxabrl_happay_ap_l_stage
            --    WHERE  invoice_num := rec_inv_line.invoice_num
            --    AND    line_number := rec_inv_line.line_number
            --    AND    line_type_lookup_code := rec_inv_line.line_type_lookup_code;
            IF rec_inv_line.line_type_lookup_code <> 'ITEM'
            THEN
               v_line_type_lookup_code := 'MISCELLANEOUS';
            ELSE
               v_line_type_lookup_code := 'ITEM';
            END IF;

 /*************************************************************************************************************/
/* VAT Accounting Codes are not allowed from 1st July 2017 due to GST                                                                       */
/*************************************************************************************************************/
            BEGIN
               IF (    (SUBSTR (rec_inv_line.dist_code_concatenated, 23, 6) IN
                                                     (226101, 226106, 364026)
                       --     OR rec_inv_line.description LIKE '%VAT%'
                       )
                   AND rec_inv_line.accounting_date > '30-JUN-17'
                  )
               THEN
                  v_error_line_flag := 'Y';
                  v_line_err_msg :=
                         'VAT Regime Accounts are not accepted in GST Regime';
               END IF;
            EXCEPTION
               WHEN OTHERS
               THEN
                  NULL;
            END;

---            Validation for Accounting date
            IF rec_inv_line.accounting_date IS NULL
            THEN
               v_error_line_flag := 'Y';
               v_line_err_msg := 'ACCOUNTING DATE IS NULL';
---        FND_FILE.PUT_LINE (FND_FILE.OUTPUT,v_line_err_msg);
            ELSE
               BEGIN
                  SELECT COUNT (*)
                    INTO v_data_count
                    FROM gl_period_statuses gps, fnd_application fna
                   WHERE fna.application_short_name = 'SQLAP'
                     AND gps.application_id = fna.application_id
                     AND gps.closing_status = 'O'
                     AND gps.set_of_books_id = v_set_of_books_id
                     AND TRUNC (NVL (rec_inv_line.accounting_date, SYSDATE))
                            BETWEEN gps.start_date
                                AND gps.end_date;

                  IF v_data_count = 0
                  THEN
                     v_error_line_flag := 'Y';
                     v_line_err_msg :=
                           'ACCOUNTING DATE IS NOT IN OPEN PERIOD'
                        || CHR (10)
                        || RPAD (' ', 20);
---    FND_FILE.PUT_LINE (FND_FILE.OUTPUT,v_line_err_msg);
                  END IF;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     v_error_line_flag := 'Y';
                     v_line_err_msg :=
                                      'ACCOUNTING DATE IS NOT IN OPEN PERIOD';
               --  FND_FILE.PUT_LINE (FND_FILE.OUTPUT,v_line_err_msg);
               END;
            END IF;

---    Validation for Invoice Line Amount
            IF rec_inv_line.amount IS NULL
            THEN
               v_error_line_flag := 'Y';
               v_line_err_msg :=
                     'INVOICE LINE AMOUNT IS NULL'
                  || RPAD (' ', 20)
                  || RPAD (' ', 20)
                  || CHR (10)
                  || RPAD (' ', 20)
                  || RPAD (' ', 20);
               fnd_file.put_line (fnd_file.output, v_line_err_msg);
            END IF;

            fnd_file.put_line (fnd_file.output,
                                  'rec_inv_line.Amount: '
                               || NVL (rec_inv_line.amount, 0)
                              );
            v_inv_line_amt :=
                         NVL (v_inv_line_amt, 0)
                         + NVL (rec_inv_line.amount, 0);

---        Validate the concatenated_segments in invoice Lines
---         Following Auto Create Concatenated Segments validation addeb by Lokesh Poojari on 06-Feb-2019.
            IF rec_inv_line.dist_code_concatenated IS NULL
            THEN
               v_error_line_flag := 'Y';
               v_line_err_msg :=
                     'DIST CODE CONCATENATED VALUE IS NULL'
                  || RPAD (' ', 20)
                  || RPAD (' ', 20)
                  || CHR (10)
                  || RPAD (' ', 20)
                  || RPAD (' ', 20);
---    FND_FILE.PUT_LINE (FND_FILE.OUTPUT,v_line_err_msg);
            ELSE
               BEGIN
                  v_dist_code_concatenated := NULL;
                  x_concatenated_segments := NULL;
                  nv_error_counter := 0;

                  SELECT code_combination_id
                    INTO v_dist_code_concatenated
                    FROM gl_code_combinations_kfv
                   WHERE 1 = 1
                     AND enabled_flag = 'Y'
                     AND segment1 =
                            SUBSTR (rec_inv_line.dist_code_concatenated, 1, 2)
                     AND segment2 =
                            SUBSTR (rec_inv_line.dist_code_concatenated, 4, 3)
                     AND segment3 =
                            SUBSTR (rec_inv_line.dist_code_concatenated, 8, 3)
                     AND segment4 =
                            SUBSTR (rec_inv_line.dist_code_concatenated, 12,
                                    7)
                     AND segment5 =
                            SUBSTR (rec_inv_line.dist_code_concatenated, 20,
                                    2)
                     AND segment6 =
                            SUBSTR (rec_inv_line.dist_code_concatenated, 23,
                                    6)
                     AND segment7 =
                            SUBSTR (rec_inv_line.dist_code_concatenated, 30,
                                    3)
                     AND segment8 =
                            SUBSTR (rec_inv_line.dist_code_concatenated, 34,
                                    4);
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     v_line_err_msg :=
                           v_line_err_msg
                        || ' Invalid Segments : '
                        || SUBSTR (rec_inv_line.dist_code_concatenated, 1, 2)
                        || '.'
                        || SUBSTR (rec_inv_line.dist_code_concatenated, 4, 3)
                        || '.'
                        || SUBSTR (rec_inv_line.dist_code_concatenated, 8, 3)
                        || '.'
                        || SUBSTR (rec_inv_line.dist_code_concatenated, 12, 7)
                        || '.'
                        || SUBSTR (rec_inv_line.dist_code_concatenated, 20, 2)
                        || '.'
                        || SUBSTR (rec_inv_line.dist_code_concatenated, 23, 6)
                        || '.'
                        || SUBSTR (rec_inv_line.dist_code_concatenated, 30, 3)
                        || '.'
                        || SUBSTR (rec_inv_line.dist_code_concatenated, 34, 4);
                     v_error_line_flag := 'Y';
                     x_val_retcode := 1;
                     nv_error_counter := 1;
                     fnd_file.put_line
                               (fnd_file.LOG,
                                   '   '
                                || rec_inv_hdr.invoice_num
                                || '.  '
                                || ' Invalid Segments: '
                                || SUBSTR
                                         (rec_inv_line.dist_code_concatenated,
                                          1,
                                          2
                                         )
                                || '.'
                                || SUBSTR
                                         (rec_inv_line.dist_code_concatenated,
                                          4,
                                          3
                                         )
                                || '.'
                                || SUBSTR
                                         (rec_inv_line.dist_code_concatenated,
                                          8,
                                          3
                                         )
                                || '.'
                                || SUBSTR
                                         (rec_inv_line.dist_code_concatenated,
                                          12,
                                          7
                                         )
                                || '.'
                                || SUBSTR
                                         (rec_inv_line.dist_code_concatenated,
                                          20,
                                          2
                                         )
                                || '.'
                                || SUBSTR
                                         (rec_inv_line.dist_code_concatenated,
                                          23,
                                          6
                                         )
                                || '.'
                                || SUBSTR
                                         (rec_inv_line.dist_code_concatenated,
                                          30,
                                          3
                                         )
                                || '.'
                                || SUBSTR
                                         (rec_inv_line.dist_code_concatenated,
                                          34,
                                          4
                                         )
                               );

                     BEGIN
                        v_chart_of_accounts_id := 50328;
                        x_concatenated_segments :=
                              SUBSTR (rec_inv_line.dist_code_concatenated,
                                      1,
                                      2
                                     )
                           || '.'
                           || SUBSTR (rec_inv_line.dist_code_concatenated,
                                      4,
                                      3
                                     )
                           || '.'
                           || SUBSTR (rec_inv_line.dist_code_concatenated,
                                      8,
                                      3
                                     )
                           || '.'
                           || SUBSTR (rec_inv_line.dist_code_concatenated,
                                      12,
                                      7
                                     )
                           || '.'
                           || SUBSTR (rec_inv_line.dist_code_concatenated,
                                      20,
                                      2
                                     )
                           || '.'
                           || SUBSTR (rec_inv_line.dist_code_concatenated,
                                      23,
                                      6
                                     )
                           || '.'
                           || SUBSTR (rec_inv_line.dist_code_concatenated,
                                      30,
                                      3
                                     )
                           || '.'
                           || SUBSTR (rec_inv_line.dist_code_concatenated,
                                      34,
                                      4
                                     );

                        SELECT fnd_flex_ext.get_ccid
                                            ('SQLGL',
                                             'GL#',
                                             50328,
                                             TO_CHAR (SYSDATE,
                                                      'YYYY/MM/DD HH24:MI:SS'
                                                     ),
                                             x_concatenated_segments
                                            )
                          INTO v_dist_code_concatenated
                          FROM DUAL;

                        IF v_dist_code_concatenated = 0
                        THEN
                           v_line_err_msg :=
                                 v_line_err_msg
                              || ' Invalid Code Combination : '
                              || SUBSTR (rec_inv_line.dist_code_concatenated,
                                         1,
                                         2
                                        )
                              || '.'
                              || SUBSTR (rec_inv_line.dist_code_concatenated,
                                         4,
                                         3
                                        )
                              || '.'
                              || SUBSTR (rec_inv_line.dist_code_concatenated,
                                         8,
                                         3
                                        )
                              || '.'
                              || SUBSTR (rec_inv_line.dist_code_concatenated,
                                         12,
                                         7
                                        )
                              || '.'
                              || SUBSTR (rec_inv_line.dist_code_concatenated,
                                         20,
                                         2
                                        )
                              || '.'
                              || SUBSTR (rec_inv_line.dist_code_concatenated,
                                         23,
                                         6
                                        )
                              || '.'
                              || SUBSTR (rec_inv_line.dist_code_concatenated,
                                         30,
                                         3
                                        )
                              || '.'
                              || SUBSTR (rec_inv_line.dist_code_concatenated,
                                         34,
                                         4
                                        );
                           v_error_line_flag := 'Y';
                           x_val_retcode := 1;
                           fnd_file.put_line
                               (fnd_file.LOG,
                                   '   '
                                || rec_inv_hdr.invoice_num
                                || '.  '
                                || ' Invalid Code Combination : '
                                || SUBSTR
                                         (rec_inv_line.dist_code_concatenated,
                                          1,
                                          2
                                         )
                                || '.'
                                || SUBSTR
                                         (rec_inv_line.dist_code_concatenated,
                                          4,
                                          3
                                         )
                                || '.'
                                || SUBSTR
                                         (rec_inv_line.dist_code_concatenated,
                                          8,
                                          3
                                         )
                                || '.'
                                || SUBSTR
                                         (rec_inv_line.dist_code_concatenated,
                                          12,
                                          7
                                         )
                                || '.'
                                || SUBSTR
                                         (rec_inv_line.dist_code_concatenated,
                                          20,
                                          2
                                         )
                                || '.'
                                || SUBSTR
                                         (rec_inv_line.dist_code_concatenated,
                                          23,
                                          6
                                         )
                                || '.'
                                || SUBSTR
                                         (rec_inv_line.dist_code_concatenated,
                                          30,
                                          3
                                         )
                                || '.'
                                || SUBSTR
                                         (rec_inv_line.dist_code_concatenated,
                                          34,
                                          4
                                         )
                               );
                        ELSE
                           IF NVL (nv_error_counter, 0) = 1
                           THEN
                              v_error_line_flag := 'N';
                              x_val_retcode := 0;
                           END IF;
                        END IF;
                     END;
                  WHEN OTHERS
                  THEN
                     v_line_err_msg :=
                           v_line_err_msg
                        || 'Unexpected Segments Error'
                        || SQLERRM;
                     v_error_line_flag := 'Y';
                     x_val_retcode := 1;
                     fnd_file.put_line (fnd_file.LOG,
                                           '   '
                                        || rec_inv_hdr.invoice_num
                                        || '.  '
                                        || ' Unexpected Segments Error'
                                        || SQLERRM
                                       );
               END;
            END IF;

            fnd_file.put_line (fnd_file.LOG,
                               'Sss -line  Error flag ' || v_error_line_flag
                              );

---        Updating Lines Staging
            IF NVL (v_error_line_flag, 'N') = 'Y'
            THEN
               --DBMS_OUTPUT.PUT_LINE('Updating Lines Staging' || ' ' || v_error_flag || ' ' || 'value for ');

               -- FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'SHAILESH - updating line staging error ');
               UPDATE apps.xxabrl_happay_ap_l_stage
                  SET error_flag = 'E',
                      error_msg = v_line_err_msg
                WHERE ROWID = rec_inv_line.ROWID;

               COMMIT;
               --                  UPDATE  apps.xxabrl_happay_ap_h_stage
               --                  SET   ERROR_FLAG ='E'
               --                   ,ERROR_MSG = 'Line validation failed'|| v_line_err_msg
               --                  WHERE ROWID = rec_inv_hdr.ROWID
               --               AND invoice_num=rec_inv_line.invoice_num;
               v_error_line_count := v_error_line_count + 1;
            ELSE
---        FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'SHAILESH - updating line staging NOT IN error ');
               UPDATE apps.xxabrl_happay_ap_l_stage
                  SET error_flag = 'V',
                      line_type_lookup_code = v_line_type_lookup_code,
                      error_msg = NULL
                WHERE ROWID = rec_inv_line.ROWID;

               COMMIT;
            END IF;

            IF v_line_err_msg IS NOT NULL
            THEN
               v_error_almsg :=
                     v_error_almsg
                  || 'LINE NO :'
                  || TO_CHAR (v_line_count)
                  || ' '
                  || v_line_err_msg;
            END IF;
         END LOOP;                      --    FOR rec_inv_line        3rd LOOP

         fnd_file.put_line (fnd_file.output,
                            'LINE AMOUNT   ->  ' || v_inv_line_amt
                           );
         fnd_file.put_line (fnd_file.output,
                            'HDR AMOUNT   ->  ' || rec_inv_hdr.invoice_amount
                           );

         IF (ROUND (v_inv_line_amt, 2) - ROUND (rec_inv_hdr.invoice_amount, 2)
            )  
            BETWEEN -1 AND 1
         THEN
            NULL;
         ELSE
            v_error_flag := 'Y';
            v_hdr_err_msg := 'INVOICE HEADER AND LINE AMOUNT IS NOT MATCHING';
            fnd_file.put_line (fnd_file.output,
                                  RPAD (NVL (rec_inv_hdr.vendor_num, ' '), 20)
                               || RPAD (rec_inv_hdr.invoice_num, 20)
                               || v_hdr_err_msg
                              );
         END IF;

         fnd_file.put_line (fnd_file.LOG,
                            'Sss - hdr Error flag ' || v_error_flag
                           );

---        Updating  Header Staging
         IF NVL (v_error_flag, 'N') = 'Y'
         THEN
---    FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'SHAILESH - updating HDR staging error ');
            UPDATE apps.xxabrl_happay_ap_h_stage
               SET error_flag = 'E',
                   error_msg = v_hdr_err_msg
             WHERE ROWID = rec_inv_hdr.ROWID;

            COMMIT;
            ---    added by Gonvind on 25 june 2009
            p_vendor_site_id := NULL;
            --              UPDATE  apps.xxabrl_happay_ap_l_stage
            --              SET     ERROR_FLAG ='E',ERROR_MSG = 'Header validation failed'|| v_hdr_err_msg
            --              WHERE   1=1--ROWID = rec_inv_line.ROWID
            --             AND     invoice_num = rec_inv_hdr.invoice_num;
            v_error_head_count := v_error_head_count + 1;

            IF v_error_almsg IS NOT NULL
            THEN
               fnd_file.put_line (fnd_file.output,
                                     RPAD (NVL (rec_inv_hdr.vendor_num, ' '),
                                           20
                                          )
                                  || RPAD (rec_inv_hdr.invoice_num, 20)
                                  || 'LINE ERROR :'
                                  || v_error_almsg
                                 );
            END IF;
         ELSIF v_hdr_err_msg IS NULL AND v_error_line_count <> 0
         THEN
---    FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'SHAILESH - updating HDR staging error ');
            UPDATE apps.xxabrl_happay_ap_h_stage
               SET error_flag = 'E',
                   error_msg = v_error_line_count || 'ERROR IN LINES'
             WHERE ROWID = rec_inv_hdr.ROWID;

            p_vendor_site_id := NULL;

            IF v_error_almsg IS NOT NULL
            THEN
               fnd_file.put_line (fnd_file.output,
                                     RPAD (NVL (rec_inv_hdr.vendor_num, ' '),
                                           20
                                          )
                                  || RPAD (rec_inv_hdr.invoice_num, 20)
                                  || 'LINE ERROR :'
                                  || v_error_almsg
                                 );
            END IF;
         ELSE             --              IF NVL(v_error_flag, 'N') = 'Y' THEN
---    DBMS_OUTPUT.PUT_LINE('inseide else part' || ' ' || v_error_flag || ' ' || 'value for ');

            /*
            UPDATE  apps.xxabrl_happay_ap_h_stage
             SET     ERROR_FLAG ='V',
                    ---vendor_site_id=v_vendor_site_id,
                    vendor_num=rec_inv_hdr.vendor_num ,
                    ERROR_MSG = NULL
             WHERE ROWID = rec_inv_hdr.ROWID;
             */

            -- Updated by Shailesh on 25 OCT 2008
            -- As insert program is running only for the one vendor site for all vendors in stg table so we include the cloumn vendor site id in hdr stg table
            -- and it will store the site id for respective vendor
            -- it will also store the terms id if it is available in vendor master

            --    20TH DEC 2014
            BEGIN
               fnd_file.put_line
                   (fnd_file.LOG,
                    '20th Dec 2014 Updating APPS.xxabrl_happay_ap_h_stage : '
                   );
               fnd_file.put_line (fnd_file.LOG,
                                     'TESTING : '
                                  || 'INVOICE_NUM : '
                                  || rec_inv_hdr.invoice_num
                                 );
               fnd_file.put_line (fnd_file.LOG,
                                     'Testing : '
                                  || 'Vendor_num : '
                                  || rec_inv_hdr.vendor_num
                                 );
               fnd_file.put_line (fnd_file.LOG,
                                     'Testing : '
                                  || ' VENDOR_ID  : '
                                  || p_vendor_id
                                 );
               fnd_file.put_line (fnd_file.LOG,
                                     'Testing : '
                                  || ' DESCRIPTION : '
                                  || rec_inv_hdr.description
                                 );
               --    FND_FILE.PUT_LINE (FND_FILE.log,'Testing : ' ||' VENDOR_SITE_ID  : ' || P_VENDOR_SITE_ID);
               --    FND_FILE.PUT_LINE (FND_FILE.log,'Testing : ' ||' TERMS_ID   : ' ||L_TERMS_ID);
               --    FND_FILE.PUT_LINE (FND_FILE.log,'Testing : ' ||' PAYMENT_METHOD_LOOKUP_CODE : ' || P_PAYMENT_METHOD_LOOKUP_CODE);
               fnd_file.put_line (fnd_file.LOG,
                                     'Testing : '
                                  || ' ACCTS_PAY_CODE_CONCATENATED: '
                                  || l_dist_code_concatenated
                                 );

               UPDATE apps.xxabrl_happay_ap_h_stage
                  SET error_flag = 'V',
-- VENDOR_SITE_ID=V_VENDOR_SITE_ID,
                      vendor_num = rec_inv_hdr.vendor_num,
                      vendor_site_id = p_vendor_site_id,
                      terms_id = l_terms_id,
-- ORG_ID                      = P_ORG_ID,
                      vendor_id = p_vendor_id,
                      payment_method_lookup_code =
                                                  p_payment_method_lookup_code,
                      accts_pay_code_concatenated = l_dist_code_concatenated,
                      error_msg = NULL
                WHERE ROWID = rec_inv_hdr.ROWID;

               COMMIT;
            EXCEPTION
               WHEN OTHERS
               THEN
                  fnd_file.put_line
                                   (fnd_file.LOG,
                                       'ERROR WHILE UPDATING STAGE HEADER : '
                                    || SQLCODE
                                   );
                  fnd_file.put_line (fnd_file.LOG, ' ERROR CODE: ' || SQLERRM);
            END;

--    20TH DEC 2014

            ---    IF YOU WANT TO INSERT THE DATA FOR MULTI ORG THEN CREATE COLUMN ORG_ID IN  APPS.xxabrl_happay_ap_h_stage TABLE
---    AND  PUT THIS STMT IN ABOVE UPDATE CONDITION  ORG_ID=P_ORG_ID
---    ALSO INCLUDE THIS COLOUMN IN INSERT STMT WHILE INSERTING TO INTERFACE TABLE FOR HDR

            --  added by Gonvind on 25 june 2009
            p_vendor_site_id := NULL;
--    Retek Matched Invoices
            v_success_record_count := v_success_record_count + 1;
         --    DBMS_OUTPUT.PUT_LINE('before end loop' || ' ' || v_success_record_count || ' ' || 'value for ');
         END IF;      --                  IF NVL(v_error_flag, 'N') = 'Y' THEN
      END LOOP;                                                 --    1st loop

      fnd_file.put_line (fnd_file.LOG,
                            'ORG ID --> INSERT TRANSACTION PASS ORG_ID -> '
                         || p_org_id
                        );
      insert_transaction (p_org_id,
                          p_vendor_site_id,
                          p_vendor_id,
                          p_payment_method_lookup_code
                         );

      --IF NVL(v_line_err_msg,'N') <> NULL THEN
      --   v_error_head_count := v_error_line_count +1 ;
      --END IF;

      --v_error_head_count := v_error_line_count +1;

      --SELECT COUNT(invoice_num)
      --INTO   v_total_records
      --FROM   apps.xxabrl_happay_ap_h_stage
      --WHERE  NVL(error_flag,'N') in ('N','E');
      IF v_error_head_count > 0
      THEN
         retcode := 1;
      END IF;

      fnd_file.put_line
         (fnd_file.output,
          '-------------------------------------------------------------------------------------------------'
         );
      fnd_file.put_line (fnd_file.output, CHR (10));
      fnd_file.put_line (fnd_file.output, CHR (10));
      fnd_file.put_line (fnd_file.output, CHR (10));
      fnd_file.put_line (fnd_file.output,
                            'TOTAL NUMBER OF INVOICES FETCHED :'
                         || v_total_records
                        );
      fnd_file.put_line (fnd_file.output,
                            'TOTAL NUMBER OF RECORDS WITH ERROR :'
                         || v_error_head_count
                        );
      fnd_file.put_line (fnd_file.output,
                            'TOTAL NUMBER OF INVOICES PROCESSED :'
                         || v_success_record_count
                        );
      --DBMS_OUTPUT.PUT_LINE('TOTAL NUMBER OF RECORDS WITH ERROR :'|| v_error_head_count);
      --DBMS_OUTPUT.PUT_LINE('TOTAL NUMBER OF INVOICES PROCESSED      :'|| v_success_record_count);
      fnd_file.put_line
         (fnd_file.output,
          '........................................................................'
         );
   END validate_invoices;

--      End of VALIDATE_INVOICES for all tables

   /*=================================Procedure INSERT_TRANSACTION =============================
   =========================*/
   PROCEDURE insert_transaction (
      p_org_id                       IN   NUMBER,
      p_vendor_site_id               IN   NUMBER,
      p_vendor_id                    IN   NUMBER,
      p_payment_method_lookup_code   IN   VARCHAR2
   )
   IS
      v_error_flag                   VARCHAR2 (1);
      v_errmsg                       VARCHAR2 (6000);
      v_header_id                    NUMBER;
      v_line_id                      NUMBER;
      v_errorinvoice                 BOOLEAN;
      v_errorlines                   BOOLEAN;
      --v_org_id                                NUMBER :=NULL;-- := 82;
      --v_user_id                                  NUMBER     :=FND_PROFILE.value('USER_ID');
      --v_resp_id                                  NUMBER     :=FND_PROFILE.value('RESP_ID');
      --v_appl_id                                  NUMBER     :=FND_PROFILE.value('RESP_APPL_ID');
      --v_req_id                                    NUMBER;
      v_record_count                 NUMBER          := 0;
      v_org_id                       NUMBER;
      v_vendor_site_id               NUMBER (15);
      v_vendor_id                    NUMBER (15);
      v_payment_method_lookup_code   VARCHAR2 (30);
      -- added by shailesh for storing the location of the retake payable invoice
      l_location                     VARCHAR2 (50);

      /*
         CURSOR   cur_inv_hdr IS
            SELECT    rowid,aps.*
         FROM        apps.xxabrl_happay_ap_h_stage aps
         WHERE       NVL(ERROR_FLAG,'N') = 'V';
      */

      ---     Added by shailesh on 25 OCT 2008
---    Above commented cursor replaced by new cursor
      CURSOR cur_inv_hdr
      IS
         SELECT aps.ROWID, aps.invoice_num, aps.invoice_type_lookup_code,
                aps.invoice_date, aps.vendor_num, aps.terms_id,
                ROUND (aps.invoice_amount, 2) invoice_amount,
                aps.invoice_currency_code, aps.exchange_date,
                aps.exchange_rate_type, aps.description, aps.gl_date,
                aps.GROUP_ID, aps.vendor_email_address, aps.terms_date,
                aps.accts_pay_code_concatenated, aps.error_flag,
                aps.error_msg, aps.vendor_site_id, aps.org_id, aps.vendor_id,
                aps.payment_method_lookup_code, aps.goods_received_date
           -- New coloumn added in the  xxabrl.xxabrl_happay_ap_h_stage
         FROM   apps.xxabrl_happay_ap_h_stage aps
          WHERE 1 = 1
            AND TRUNC (aps.gl_date) > '31-MAR-17'
            AND NVL (aps.error_flag, 'N') = 'V';

      /*
      CURSOR   cur_inv_line(p_invoice_num VARCHAR2,
                            p_vendor_num NUMBER) IS
       SELECT   rowid,als.*
       FROM     apps.xxabrl_happay_ap_l_stage  als
       WHERE    NVL(ERROR_FLAG,'N') = 'V'
       AND      als.invoice_num = p_invoice_num
      ---AND      als.vendor_site_id = p_vendor_site_id
      AND      als.vendor_num = p_vendor_num;
      */

      -- Added by shailesh on 25 OCT 2008
      -- Above commented cursor replaced by new cursor
      CURSOR cur_inv_line (p_invoice_num VARCHAR2, p_vendor_num NUMBER)
      IS
         SELECT als.ROWID, als.vendor_num, als.vendor_site_id,
                als.invoice_num, als.line_number, als.line_type_lookup_code,
                ROUND (als.amount, 2) amount, als.accounting_date,
                als.dist_code_concatenated, als.description, als.tds_tax_name,
                als.wct_tax_name, als.service_tax, als.vat, als.error_flag,
                als.error_msg
           FROM apps.xxabrl_happay_ap_l_stage als
          WHERE NVL (error_flag, 'N') = 'V'
            AND als.invoice_num = p_invoice_num
            ---AND      als.vendor_site_id = p_vendor_site_id
            AND als.vendor_num = p_vendor_num;
   BEGIN
--   commented by shailesh on 20th june 2009
    --v_org_id                     := p_org_id;
    --v_vendor_site_id             := p_vendor_site_id;
    --v_vendor_id                  := p_vendor_id;
    --v_payment_method_lookup_code := p_payment_method_lookup_code;
      FOR rec_inv_hdr IN cur_inv_hdr
      LOOP
         EXIT WHEN cur_inv_hdr%NOTFOUND;
         v_header_id := NULL;
         v_errorinvoice := NULL;
         v_errmsg := NULL;
         v_record_count := cur_inv_hdr%ROWCOUNT;
         --v_payment_method_lookup_code          := NULL;
         --v_org_id                              := NULL;
         --v_vendor_id                           := NULL;

         --           SELECT asr.vendor_id,assa.org_id,assa.payment_method_lookup_code
         --           INTO   v_vendor_id,v_org_id,v_payment_method_lookup_code
         --           FROM   ap_suppliers asr,ap_supplier_sites_all assa
         --           WHERE  asr.vendor_id=assa.vendor_id
         --           AND    asr.segment1=rec_inv_hdr.vendor_num --'9800012'--'987676'--'9800012'
         --           AND       assa.VENDOR_SITE_ID=rec_inv_hdr.vendor_site_id
         --           AND    org_id = fnd_profile.value('org_id');
         v_header_id := 0;

         SELECT ap_invoices_interface_s.NEXTVAL
           INTO v_header_id
           FROM DUAL;

         BEGIN
            fnd_file.put_line (fnd_file.LOG,
                                  'INSERTING RECORD FOR  -> '
                               || rec_inv_hdr.invoice_num
                               || '  '
                               || rec_inv_hdr.org_id
                              );

            INSERT INTO ap_invoices_interface
                        (invoice_id, invoice_num,
                         invoice_type_lookup_code,
                         invoice_date, vendor_id,
                         vendor_site_id,
                         invoice_amount,
                         invoice_currency_code
                                              --,EXCHANGE_RATE
            ,
                         exchange_rate_type,
                         exchange_date, terms_id,
                         description
                                    --,TERMS_NAME
            ,            last_update_date, last_updated_by,
                         last_update_login, creation_date, created_by
                                                                     --,GROUP_ID
            ,
                         gl_date, org_id,
                         vendor_email_address, SOURCE,
                         terms_date,
                         payment_method_code,
                         goods_received_date
                                            -- added by shailesh on 25 oct 2008
            ,
                         accts_pay_code_concatenated
                        )
                 VALUES (v_header_id, rec_inv_hdr.invoice_num,
                         rec_inv_hdr.invoice_type_lookup_code,
                         rec_inv_hdr.invoice_date, rec_inv_hdr.vendor_id,
                         rec_inv_hdr.vendor_site_id
                                                   -- CHANGE BY SHAILESH ON 25 oct2008 ORIGINAL IS  v_vendor_site_id
            ,
                         rec_inv_hdr.invoice_amount,
                         rec_inv_hdr.invoice_currency_code
                                                          --,rec_inv_hdr.EXCHANGE_RATE
            ,
                         rec_inv_hdr.exchange_rate_type,
                         rec_inv_hdr.exchange_date, rec_inv_hdr.terms_id,
                         rec_inv_hdr.description
                                                --,rec_inv_hdr.TERMS_NAME
            ,            SYSDATE, 3,
                         3, SYSDATE, 3
                                      --,rec_inv_hdr.GROUP_ID
            ,
                         rec_inv_hdr.gl_date, rec_inv_hdr.org_id,
                         rec_inv_hdr.vendor_email_address, 'HAPPAY',
                         rec_inv_hdr.terms_date,
                         -- v_payment_method_lookup_code,
                         rec_inv_hdr.payment_method_lookup_code,
                         -- above stmt is changed by shailesh on 20th june 2009
                         rec_inv_hdr.goods_received_date
                                                        -- added by shailesh on 25 oct 2008
            ,
                         rec_inv_hdr.accts_pay_code_concatenated
                        );

            COMMIT;
            v_errorinvoice := FALSE;
         EXCEPTION
            WHEN OTHERS
            THEN
               v_errorinvoice := TRUE;
               v_errmsg :=
                     'AFTER INSERT INTO AP INVOICES INTERFACE FAILED IN EXCEPTION'
                  || SUBSTR (SQLERRM, 1, 150);
               fnd_file.put_line (fnd_file.output, v_errmsg);
         END;

         IF v_errorinvoice = TRUE
         THEN
            UPDATE apps.xxabrl_happay_ap_h_stage
               SET error_flag = 'E',
                   error_msg = v_errmsg
             WHERE ROWID = rec_inv_hdr.ROWID;

            --and CURRENT OF cur_inv_hdr;
            UPDATE apps.xxabrl_happay_ap_l_stage
               SET error_flag = 'E',
                   error_msg = 'Header Inserting failed' || v_errmsg
             WHERE invoice_num = rec_inv_hdr.invoice_num;
         --and CURRENT OF cur_inv_hdr;
         ELSE
            UPDATE apps.xxabrl_happay_ap_h_stage
               SET error_flag = 'P',
                   error_msg =
                             'SUCCESSFULLY INSERTED IN HEADER INTERFACE TABLE'
             WHERE ROWID = rec_inv_hdr.ROWID;
         --and CURRENT OF cur_inv_hdr;
         END IF;

         FOR rec_inv_line IN cur_inv_line (rec_inv_hdr.invoice_num
                                                                  --,rec_inv_hdr.vendor_site_id
                            ,
                                           rec_inv_hdr.vendor_num
                                          )
         LOOP
            EXIT WHEN cur_inv_line%NOTFOUND;
            v_errorinvoice := FALSE;
            v_errmsg := NULL;
            v_line_id := NULL;

            SELECT ap_invoice_lines_interface_s.NEXTVAL
              INTO v_line_id
              FROM DUAL;

            BEGIN
               -- following code added by shailesh on 26 OCT 2008
               -- It will extract the segment4 from dist_code_combinition and  get the location  for the line level dff
               BEGIN
                  SELECT ffvv.flex_value                   --,ffvv.description
                    INTO l_location
                    FROM fnd_flex_values_vl ffvv, fnd_flex_value_sets ffvs
                   WHERE flex_value_set_name = 'ABRL_GL_Location'
                     AND ffvs.flex_value_set_id = ffvv.flex_value_set_id
                     AND ffvv.flex_value =
                            SUBSTR (rec_inv_line.dist_code_concatenated, 12,
                                    7);
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_location := NULL;
               END;

               /*
               SELECT * FROM AP_INVOICES_INTERFACE
               WHERE INVOICE_NUM='INV_9983'

               SELECT * FROM AP_INVOICE_LINES_INTERFACE
               WHERE  INVOICE_ID=700042
               */

               /*
               select * from AP_INVOICES_INTERFACE
               where invoice_num in ('INV_9983','INV_10373')

               select * from AP_INVOICE_LINES_INTERFACE
               where invoice_id= 700041 --700042
               */
               INSERT INTO ap_invoice_lines_interface
                           (invoice_id,
                            invoice_line_id,
                            line_number,
                            line_type_lookup_code,
                            amount
                                  --,TAX_CODE
               ,
                            dist_code_concatenated,
                            description,
                            accounting_date, org_id,
                            attribute_category,
                            attribute1, last_updated_by, last_update_date,
                            last_update_login, created_by, creation_date
                           )
                    VALUES (v_header_id,
                            v_line_id          --,rec_inv_line.INVOICE_LINE_ID
                                     ,
                            rec_inv_line.line_number,
                            rec_inv_line.line_type_lookup_code,
                            rec_inv_line.amount
                                               --,rec_inv_line.TAX_CODE
               ,
                            rec_inv_line.dist_code_concatenated,
                            rec_inv_line.description,
                            rec_inv_line.accounting_date,
                                                         --v_org_id,
                                                         -- ADDED BY SHAILESH ON 11 MAY 2009  ORG ID IS COMMING FROM HEADER
                                                         rec_inv_hdr.org_id,
                            'HAPPAY PAYABLE INVOICE'       --ADDED BY SHAILESH
                                                    ,
                            l_location                    -- ADDED BY SHAILESH
                                      , 3, SYSDATE,
                            3, 3, SYSDATE
                           );

               COMMIT;
               v_errorlines := FALSE;
            EXCEPTION
               WHEN OTHERS
               THEN
                  v_errorlines := TRUE;
                  v_errmsg :=
                        'AFTER INSERT INTO AP INVOICE LINES INTERFACE FAILED IN EXCEPTION'
                     || SUBSTR (SQLERRM, 1, 150);
                  fnd_file.put_line (fnd_file.output, v_errmsg);
            END;

            IF v_errorlines = TRUE
            THEN
               UPDATE apps.xxabrl_happay_ap_l_stage
                  SET error_flag = 'E',
                      error_msg = v_errmsg
                WHERE ROWID = rec_inv_line.ROWID;
            --and CURRENT OF cur_inv_line;
            ELSE
               UPDATE apps.xxabrl_happay_ap_l_stage
                  SET error_flag = 'P',
                      error_msg =
                               'SUCCESSFULLY INSERTED IN LINE INTERFACE TABLE'
                WHERE ROWID = rec_inv_line.ROWID;
            -- and CURRENT OF cur_inv_line;
            END IF;
         END LOOP;
---
---    RUN PAYABLE OPEN ITERFACE FOR INTERFACE DATA INTO ORACLE PAYABLES
      END LOOP;

---    Added on 21-jun-2008

      ---
---    COMMENTED BY SHAILESH ON 22 JUNE 2009 FOR TEMP TESTING.
---    DELETE FROM xxabrl_happay_ap_h_stage WHERE ERROR_FLAG = 'P';
---    DELETE FROM xxabrl_happay_ap_l_stage WHERE ERROR_FLAG = 'P';
---
      COMMIT;
   END insert_transaction;
--        END OF PROCEDURE INSERT_TRANSACTION
END xxabrl_happay_ap_inv_interface; 
/


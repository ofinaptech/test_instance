CREATE OR REPLACE PACKAGE BODY APPS.xxmrl_tax_inv_bkdt_load_pkg
IS
         /*
      ========================
   =========================
   =========================
   =========================
   ====
      || Concurrent Program Name : MRL Indias116 To AP All OU data load
      || Package Name                    : APPS.xxmrl_tax_inv_bkdt_load_pkg
      || Description                           : Auto Submiting the Payables Open Interface Import Program For Each Operating Unit
      ========================================================================================================================================
      ||  Version              Date                                 Author                                                                              Description                                                                                             Remarks
      || ~~~~~~~      ~~~~~~~~~~~            ~~~~~~~~~~~~~~~~~~                                    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~                                                        ~~~~~~~~~~~~~~~~~~
      ||  1.0.0            25-Sep-2019                Lokesh Poojari              Auto Submiting the Payables Open Interface Import Program For Each Operating Unit               New Development
      ========================================================================================================================================
   =========================
   =========================
   =========================
   ====*/
   PROCEDURE xxmrl_wait_for_req_prc2 (
      p_request_id   IN   NUMBER,
      p_req_name     IN   VARCHAR2
   )
   AS
      l_sup_phase           VARCHAR2 (100);
      l_sup_status          VARCHAR2 (100);
      l_sup_phase1          VARCHAR2 (100);
      l_sup_status1         VARCHAR2 (100);
      l_sup_errbuff         VARCHAR2 (500);
      l_sup_b_call_status   BOOLEAN;
   BEGIN
      IF p_request_id = 0
      THEN
         fnd_file.put_line
                        (fnd_file.LOG,
                         'Error in Invoice Validation AND Conversion Program'
                        );
      ELSE
         COMMIT;
         l_sup_b_call_status :=
            fnd_concurrent.wait_for_request (request_id      => p_request_id,
                                             INTERVAL        => 5,
                                             max_wait        => 0,
                                             phase           => l_sup_phase,
                                             status          => l_sup_status,
                                             dev_phase       => l_sup_phase1,
                                             dev_status      => l_sup_status1,
                                             MESSAGE         => l_sup_errbuff
                                            );

         --
         IF l_sup_b_call_status
         THEN
            --
            IF (l_sup_phase1 = 'COMPLETE' AND l_sup_status1 = 'NORMAL')
            THEN
               fnd_file.put_line (fnd_file.LOG,
                                     p_req_name
                                  || TO_CHAR (p_request_id)
                                  || 'Completed Succesfully'
                                 );
            ELSE
               fnd_file.put_line (fnd_file.LOG,
                                     p_req_name
                                  || TO_CHAR (p_request_id)
                                  || ' Error'
                                 );
            END IF;
         --
         ELSE
            fnd_file.put_line (fnd_file.LOG,
                                  p_req_name
                               || TO_CHAR (p_request_id)
                               || ' Not Completed'
                              );
         END IF;
      END IF;
   END xxmrl_wait_for_req_prc2;

   PROCEDURE xxmrl_tax_inv_dt_load_prc (
      errbuf    OUT   VARCHAR2,
      retcode   OUT   NUMBER
   )
   IS
      CURSOR cur_ap_data
      IS
         SELECT DISTINCT org_id
                    FROM ap_invoices_interface
                   WHERE 1=1
                     AND SOURCE = 'INDIAS116'
                     AND status IS NULL;

      v_org_name     VARCHAR2 (30);
      v_batch_name   VARCHAR2 (100);
      v_req_id       NUMBER;
      v_user_id      NUMBER         := fnd_profile.VALUE ('USER_ID');
      v_resp_id      NUMBER         := fnd_profile.VALUE ('RESP_ID');
      v_appl_id      NUMBER         := fnd_profile.VALUE ('RESP_APPL_ID');
   BEGIN
      FOR i IN cur_ap_data
      LOOP
         BEGIN
            SELECT short_code
              INTO v_org_name
              FROM apps.hr_operating_units
             WHERE organization_id = i.org_id;

            v_batch_name :=
                      'INDIAS116 Invoices/' || v_org_name || '/' || TRUNC (SYSDATE);
         EXCEPTION
            WHEN OTHERS
            THEN
               v_org_name := '';
         END;

         BEGIN
            mo_global.set_policy_context ('M', i.org_id);
         END;

         -- we will submit this req through all corp ou
         fnd_global.apps_initialize (user_id           => v_user_id,
                                     resp_id           => v_resp_id,
                                     resp_appl_id      => v_appl_id
                                    );
         -----
         -- Run Payable Open Interface for Interface data into Oracle Payables
         ----
         fnd_file.put_line (fnd_file.output,
                            'Submitting  Payable Open Iterface:'
                           );
         v_req_id :=
            fnd_request.submit_request ('SQLAP',
                                        'APXIIMPT',
                                        '',
                                        NULL,
                                        FALSE,
                                        i.org_id,
                                        'INDIAS116',       -- Parameter Data source
                                        NULL,                      -- Group id
                                        v_batch_name,
                                        NULL,
                                        NULL,
                                        TRUNC (SYSDATE),
                                        'N',
                                        'N',
                                        'N',
                                        'Y',
                                        CHR (0)
                                       );
         COMMIT;
         fnd_file.put_line (fnd_file.output,
                            ' Payable Open Iterface Request id :' || v_req_id
                           );
         xxmrl_wait_for_req_prc2 (v_req_id, 'Payable Open Iterface');
      END LOOP;
   END xxmrl_tax_inv_dt_load_prc;
END xxmrl_tax_inv_bkdt_load_pkg; 
/


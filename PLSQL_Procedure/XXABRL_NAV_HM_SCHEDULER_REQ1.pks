CREATE OR REPLACE PACKAGE APPS.XXABRL_NAV_HM_SCHEDULER_REQ1 IS
  PROCEDURE Print_log(
                      p_str IN VARCHAR2,
                      p_debug_flag IN VARCHAR2
                      );

  PROCEDURE CONCPROG_SCHEDULER(
                             Errbuf         OUT VARCHAR2,
                             RetCode        OUT NUMBER,
                             p_module IN VARCHAR2,
                             p_Run_Date IN VARCHAR2,
                             p_ser IN VARCHAR2
                            -- p_gl_date VARCHAR2
                             );
END XXABRL_NAV_HM_SCHEDULER_REQ1; 
/


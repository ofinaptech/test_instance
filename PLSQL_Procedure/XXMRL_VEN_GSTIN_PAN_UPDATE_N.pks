CREATE OR REPLACE PACKAGE APPS.xxmrl_ven_gstin_pan_update_n
IS
   PROCEDURE xxmrl_main_pkg (errbuf       OUT VARCHAR2,
                             retcode      OUT NUMBER,
                             p_type    IN     VARCHAR2);

   PROCEDURE xxmrl_vendor_gstin_update_n;

   PROCEDURE xxmrl_vendor_pan_update_n;

   PROCEDURE xxmrl_vendor_sec_pan_update_n;

   PROCEDURE xxmrl_vendor_rep_code_update_n;

   PROCEDURE xxmrl_vendor_tds_type_update_n;
END xxmrl_ven_gstin_pan_update_n; 
/


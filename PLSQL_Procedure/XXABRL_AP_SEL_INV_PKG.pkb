CREATE OR REPLACE PACKAGE BODY APPS.XXABRL_AP_SEL_INV_PKG
AS
PROCEDURE XXABRL_EXP_SEL_INV_PROG(errbuf out varchar2,
                                     retcode out varchar2,
                                     p_checkrun_name varchar2) 
  AS
/*********************************************************************************************************
                 WIPRO Infotech Ltd, Mumbai, India

 Object Name : XXABRL_AP_SEL_INV_PKG

 Description : Procedure to Export AP selected invoices for Payment Batch 

  Change Record:
 =========================================================================================================
 Version    Date            Author                              Remarks
 =======   ==========     =============                         ==========================================
 1.0      18-August-09   Praveen Kumar (Wipro Infotech Ltd)    New Development
***********************************************************************************************************/
V_L_FILE         VARCHAR2(100); 

      query_string             LONG;
      op_string                LONG;      
      order_by_string          LONG;
      
 V_RESPID             NUMBER := Fnd_Profile.VALUE('RESP_ID');


 TYPE ref_cur IS REF CURSOR;

      c                        ref_cur;

      TYPE c_rec IS RECORD (
         v_checkrun_name        ap_selected_invoices_all.checkrun_name%TYPE,
         v_invoice_id           ap_selected_invoices_all.invoice_id%TYPE,
         v_vendor_id            ap_selected_invoices_all.vendor_id%TYPE,
         v_vendor_site_id       ap_selected_invoices_all.vendor_site_id%TYPE,
         v_vendor_num           ap_selected_invoices_all.vendor_num%TYPE,
         v_vendor_name          ap_selected_invoices_all.vendor_name%TYPE,
         v_vendor_site_code     ap_selected_invoices_all.vendor_site_code%TYPE,       
         v_city                 ap_selected_invoices_all.city%TYPE,
         v_state                ap_selected_invoices_all.state%TYPE,     
         v_zip                  ap_selected_invoices_all.zip%TYPE,
         v_country              ap_selected_invoices_all.country%TYPE,
         v_invoice_num          ap_selected_invoices_all.invoice_num%TYPE,         
         v_invoice_date         ap_selected_invoices_all.invoice_date%TYPE,
         v_voucher_num          ap_selected_invoices_all.voucher_num%TYPE,
         v_invoice_amount       ap_selected_invoices_all.invoice_amount%TYPE,
         v_due_date             ap_selected_invoices_all.due_date%TYPE,
         v_invoice_description  ap_selected_invoices_all.invoice_description%TYPE,
         v_ok_to_pay_flag       ap_selected_invoices_all.ok_to_pay_flag%TYPE,
         v_checkrun_id          ap_selected_invoices_all.checkrun_id%TYPE,
         v_payment_currency_code ap_selected_invoices_all.payment_currency_code%TYPE,
         v_reffernce             ap_invoice_lines_all.reference_1%TYPE,
         v_payment_amount        ap_selected_invoices_all.payment_amount%TYPE,
         v_amount_paid           ap_selected_invoices_all.amount_paid%TYPE                     
         );
v_rec                    c_rec;

   BEGIN

   query_string :='select  asia.checkrun_name
        ,asia.invoice_id
        ,asia.vendor_id
        ,asia.vendor_site_id
        ,asia.vendor_num
        ,asia.vendor_name
        ,asia.vendor_site_code      
        ,asia.city
        ,asia.state
        ,asia.zip
        ,asia.country
        ,to_char(asia.invoice_num) invoice_num
        ,asia.invoice_date
        ,asia.voucher_num
        ,nvl(asia.invoice_amount,0)
        ,asia.due_date
        ,asia.invoice_description
        ,asia.ok_to_pay_flag
        ,asia.checkrun_id
        ,asia.payment_currency_code             
        ,(select distinct b.REFERENCE_1 
          from ap_invoices_all a,
               ap_invoice_lines_all b
         where a.invoice_id = b.invoice_id
           and a.invoice_id = asia.invoice_id
           and b.line_type_lookup_code = ''ITEM'') reffernce
           ,asia.payment_amount
           ,api.amount_paid      
   from ap_selected_invoices_all asia,
        ap_invoices_all api
  where asia.invoice_id = api.invoice_id';


op_string:=' and checkrun_name = '''|| p_checkrun_name|| '''';
order_by_string:=' ORDER BY asia.invoice_id';

      Fnd_File.put_line (Fnd_File.output,
                          'CHECKRUN NAME'
                         || CHR (9)
                         || 'INVOICE ID'
                         || CHR (9)
                         || 'VENDOR ID'
                         || CHR (9)
                         || 'VENDOR SITE ID'
                         || CHR (9)
                         || 'VENDOR NUM'
                         || CHR (9)
                         || 'VENDOR NAME'
                         || CHR (9)
                         || 'VENDOR SITE CODE'
                         || CHR (9)
                         || 'CITY'
                         || CHR (9)
                         || 'STATE'
                         || CHR (9)
                         || 'ZIP'
                         || CHR (9)
                         || 'COUNTRY'
                         || CHR (9)                       
                         || 'INVOICE NUM'
                         || CHR (9)
                         || 'INVOICE DATE'
                         || CHR (9)
                         || 'VOUCHER NUM'
                         || CHR (9)
                         || 'INVOICE AMOUNT'
                         || CHR (9)
                         || 'DUE DATE'
                         || CHR (9)
                         || 'INVOICE DESCRIPTION'
                         || CHR (9)
                         || 'OK TO PAY FLAG'
                         || CHR (9)
                         || 'CHECKRUN ID'
                         || CHR (9)
                         || 'PAYMENT CURRENCY CODE' 
                         || CHR (9)
                         || 'REFFERNCE'
                         || CHR (9)
                         || 'PAYMENT_AMOUNT' 
                         || CHR (9)
                         || 'AMOUNT_PAID'                         
                        );

                   query_string:=query_string||op_string||order_by_string;

                   Fnd_File.put_line (Fnd_File.LOG,
                         'string: '||query_string
                        );

    OPEN c
       FOR query_string;
      LOOP
         FETCH c
          INTO v_rec;
         EXIT WHEN c%NOTFOUND;

         Fnd_File.put_line (Fnd_File.output,
                            v_rec.v_checkrun_name
                         || CHR (9)
                         || v_rec.v_invoice_id
                         || CHR (9)
                         || v_rec.v_vendor_id
                         || CHR (9)
                         || v_rec.v_vendor_site_id
                         || CHR (9)
                         || v_rec.v_vendor_num
                         || CHR (9)
                         || v_rec.v_vendor_name
                         || CHR (9)
                         || v_rec.v_vendor_site_code
                         || CHR (9)
                         || v_rec.v_city
                         || CHR (9)
                         || v_rec.v_state
                         || CHR (9)
                         || v_rec.v_zip
                         || CHR (9)
                         || v_rec.v_country
                         || CHR (9)                       
                         || v_rec.v_invoice_num
                         || CHR (9)
                         || v_rec.v_invoice_date
                         || CHR (9)
                         || v_rec.v_voucher_num
                         || CHR (9)
                         || v_rec.v_invoice_amount
                         || CHR (9)
                         || v_rec.v_due_date
                         || CHR (9)
                         || v_rec.v_invoice_description
                         || CHR (9)
                         || v_rec.v_ok_to_pay_flag
                         || CHR (9)
                         || v_rec.v_checkrun_id
                         || CHR (9)
                         || v_rec.v_payment_currency_code
                         || CHR (9)
                         || v_rec.v_reffernce 
                         || CHR (9)
                         || v_rec.v_payment_amount 
                         || CHR (9)
                         || v_rec.v_amount_paid                          
                        );

END LOOP;
CLOSE C;
COMMIT;

END XXABRL_EXP_SEL_INV_PROG;
END XXABRL_AP_SEL_INV_PKG; 
/


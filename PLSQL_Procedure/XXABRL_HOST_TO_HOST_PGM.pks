CREATE OR REPLACE PACKAGE APPS.XXABRL_HOST_TO_HOST_PGM
IS
   PROCEDURE XXABRL_HOST_HOST_IMPORT (
      x_err_buf    OUT   VARCHAR2,
      x_ret_code   OUT   NUMBER
   );

   PROCEDURE XXABRL_UPDATE_HOST_HOST (
      x_err_buf    OUT      VARCHAR2,
      x_ret_code   OUT      NUMBER,
      v_flag       IN       VARCHAR2
   );
END xxabrl_host_to_host_pgm; 
/


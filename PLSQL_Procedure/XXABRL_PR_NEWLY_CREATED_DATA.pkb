CREATE OR REPLACE PACKAGE BODY APPS.xxabrl_pr_newly_created_data
IS
   PROCEDURE xxabrl_main_pkg (
      errbuf    OUT      VARCHAR2,
      retcode   OUT      NUMBER,
      p_type    IN       VARCHAR2
   )
   IS
   BEGIN
      IF p_type = 'H'
      THEN
         xxabrl_prs_daily_data;
      END IF;

      IF p_type = 'LD'
      THEN
         xxabrl_prs_lines_dist_det;
      END IF;

      IF p_type = 'AH'
      THEN
         xxabrl_prs_action_history_det;
      END IF;
   END xxabrl_main_pkg;

   PROCEDURE xxabrl_prs_daily_data
   IS
   BEGIN
      EXECUTE IMMEDIATE 'truncate table xxabrliproc.xxabrl_pr_headers';

      FOR buf IN
         (SELECT DISTINCT prh.requisition_header_id, prh.preparer_id,
                          (SELECT full_name
                             FROM apps.per_all_people_f
                            WHERE person_id = prh.preparer_id
                              AND (   effective_end_date IS NULL
                                   OR effective_end_date >= SYSDATE
                                  )) preparer,
                          prh.org_id,
                          (SELECT NAME
                             FROM apps.hr_operating_units
                            WHERE organization_id = prh.org_id)
                                                              operating_unit,
                          prh.last_update_date, prh.last_updated_by,
                          (SELECT    user_name
                                  || '-'
                                  || description
                             FROM apps.fnd_user
                            WHERE user_id = prh.last_updated_by)
                                                           last_updated_name,
                          prh.segment1 pr_no, prh.creation_date,
                          prh.created_by,
                          (SELECT user_name || '-'
                                  || description
                             FROM apps.fnd_user
                            WHERE user_id = prh.created_by) craeted_name,
                          prh.description, prh.authorization_status,
                          prh.note_to_authorizer, prh.attribute1 department,
                          prh.attribute2 budgeted_amount,
                          prh.attribute7 budget_code,
                          prh.attribute3 contact_person,
                          prh.attribute4 tel_no,
                          prh.attribute5 freight_octroi, prh.cancel_flag,
                          prh.attribute6 type_of_expense, prh.attribute8,
                          prh.attribute9, prh.attribute10, prh.attribute11,
                          prh.attribute12, prh.attribute13, prh.attribute14,
                          prh.attribute15,
                          NVL (prh.closed_code, 'OPEN') closed_code,
                          (SELECT MAX (action_date)
                             FROM apps.po_action_history
                            WHERE object_id =
                                     prh.requisition_header_id
                              AND action_code = 'APPROVE')
                                                         final_approved_date
                     FROM apps.po_requisition_headers_all prh
                    WHERE 1 = 1
                      AND (   TRUNC (prh.creation_date) = TRUNC (SYSDATE)
                           OR TRUNC (prh.creation_date) = TRUNC (SYSDATE - 1)
                          ))
      --      AND prh.segment1 = '112000078')
      LOOP
         INSERT INTO xxabrliproc.xxabrl_pr_headers
                     (requisition_header_id, preparer_id,
                      preparer, org_id, operating_unit,
                      last_update_date, last_updated_by,
                      last_updated_name, pr_no, creation_date,
                      created_by, craeted_name, description,
                      authorization_status, note_to_authorizer,
                      department, budgeted_amount, budget_code,
                      contact_person, tel_no, freight_octroi,
                      cancel_flag, type_of_expense, attribute8,
                      attribute9, attribute10, attribute11,
                      attribute12, attribute13, attribute14,
                      attribute15, closed_code,
                      final_approved_date, process_flag
                     )
              VALUES (buf.requisition_header_id, buf.preparer_id,
                      buf.preparer, buf.org_id, buf.operating_unit,
                      buf.last_update_date, buf.last_updated_by,
                      buf.last_updated_name, buf.pr_no, buf.creation_date,
                      buf.created_by, buf.craeted_name, buf.description,
                      buf.authorization_status, buf.note_to_authorizer,
                      buf.department, buf.budgeted_amount, buf.budget_code,
                      buf.contact_person, buf.tel_no, buf.freight_octroi,
                      buf.cancel_flag, buf.type_of_expense, buf.attribute8,
                      buf.attribute9, buf.attribute10, buf.attribute11,
                      buf.attribute12, buf.attribute13, buf.attribute14,
                      buf.attribute15, buf.closed_code,
                      buf.final_approved_date, 'N'
                     );

         COMMIT;
      END LOOP;
   END xxabrl_prs_daily_data;

   PROCEDURE xxabrl_prs_lines_dist_det
   IS
   BEGIN
      EXECUTE IMMEDIATE 'truncate table xxabrliproc.xxabrl_lines_dist';

      FOR buf IN
         (SELECT DISTINCT prl.requisition_line_id, prl.requisition_header_id,
                          prl.line_num, prl.line_type_id, prl.category_id,
                          (SELECT segment1 || '.' || segment2
                             FROM apps.mtl_categories_vl
                            WHERE category_id = prl.category_id)
                                                               category_name,
                          prl.item_id, prl.item_description,
                          prl.unit_meas_lookup_code, prl.unit_price,
                          prl.quantity, prl.deliver_to_location_id,
                          (SELECT location_code
                             FROM apps.hr_locations
                            WHERE location_id =
                                     deliver_to_location_id)
                                                    deliver_to_location_name,
                          prl.to_person_id,
                          (SELECT full_name
                             FROM apps.per_all_people_f
                            WHERE person_id = prl.to_person_id
                              AND (   effective_end_date IS NULL
                                   OR effective_end_date >= SYSDATE
                                  )) to_person_name,
                          prl.last_update_date, prl.last_updated_by,
                          (SELECT    user_name
                                  || '-'
                                  || description
                             FROM apps.fnd_user
                            WHERE user_id = prl.last_updated_by)
                                                           last_updated_name,
                          prl.last_update_login, prl.creation_date,
                          prl.created_by,
                          (SELECT user_name || '-'
                                  || description
                             FROM apps.fnd_user
                            WHERE user_id = prl.created_by) craeted_name,
                          prl.quantity_delivered, prl.suggested_buyer_id,
                          (SELECT full_name
                             FROM apps.per_all_people_f
                            WHERE person_id =
                                     prl.suggested_buyer_id
                              AND (   effective_end_date IS NULL
                                   OR effective_end_date >= SYSDATE
                                  )) suggested_buyer_name,
                          prl.need_by_date, prl.line_location_id,
                          prl.justification, prl.note_to_agent,
                          prl.note_to_receiver, prl.cancel_flag,
                          prl.destination_type_code,
                          prl.destination_organization_id,
                          prl.quantity_cancelled, prl.cancel_date,
                          prl.cancel_reason, prl.closed_code,
                          prl.destination_context, prl.closed_reason,
                          prl.closed_date, prl.org_id, prl.requester_email,
                          prl.base_unit_price, prd.distribution_id,
                          prd.set_of_books_id,
                           CAST
                             (prd.req_line_quantity AS VARCHAR2 (2000)
                             ) req_line_quantity,
                          prd.code_combination_id charge_account_id,
                          (SELECT concatenated_segments
                             FROM apps.gl_code_combinations_kfv
                            WHERE code_combination_id =
                                                       prd.code_combination_id)
                                                     charge_account_segments,
                          prd.accrual_account_id accrual_account_id,
                          (SELECT concatenated_segments
                             FROM apps.gl_code_combinations_kfv
                            WHERE code_combination_id = prd.accrual_account_id)
                                                    accrual_account_segments,
                          prd.variance_account_id,
                          (SELECT concatenated_segments
                             FROM apps.gl_code_combinations_kfv
                            WHERE code_combination_id =
                                                       prd.variance_account_id)
                                                   variance_account_segments,
                          prd.distribution_num, prd.allocation_type
                     FROM apps.po_requisition_lines_all prl,
                          apps.po_req_distributions_all prd
                    WHERE prl.requisition_line_id = prd.requisition_line_id
                      AND (   TRUNC (prl.creation_date) = TRUNC (SYSDATE)
                           OR TRUNC (prl.creation_date) = TRUNC (SYSDATE - 1)
                          )
                 --AND prl.requisition_header_id = 1388
          ORDER BY        prl.requisition_header_id,
                          prl.requisition_line_id,
                          prl.line_num)
      LOOP
         INSERT INTO xxabrliproc.xxabrl_lines_dist
                     (requisition_line_id, requisition_header_id,
                      line_num, line_type_id, category_id,
                      category_name, item_id, item_description,
                      unit_meas_lookup_code, unit_price,
                      quantity, deliver_to_location_id,
                      deliver_to_location_name, to_person_id,
                      to_person_name, last_update_date,
                      last_updated_by, last_updated_name,
                      creation_date, created_by, craeted_name,
                      quantity_delivered, suggested_buyer_id,
                      suggested_buyer_name, need_by_date,
                      line_location_id, justification,
                      note_to_agent, note_to_receiver,
                      cancel_flag, destination_type_code,
                      destination_organization_id,
                      quantity_cancelled, cancel_date,
                      cancel_reason, closed_code,
                      destination_context, closed_reason,
                      closed_date, org_id, requester_email,
                      base_unit_price, distribution_id,
                      set_of_books_id, charge_account_id,
                      charge_account_segments, accrual_account_id,
                      accrual_account_segments, variance_account_id,
                      variance_account_segments, distribution_num,
                      allocation_type, req_line_quantity, process_flag
                     )
              VALUES (buf.requisition_line_id, buf.requisition_header_id,
                      buf.line_num, buf.line_type_id, buf.category_id,
                      buf.category_name, buf.item_id, buf.item_description,
                      buf.unit_meas_lookup_code, buf.unit_price,
                      buf.quantity, buf.deliver_to_location_id,
                      buf.deliver_to_location_name, buf.to_person_id,
                      buf.to_person_name, buf.last_update_date,
                      buf.last_updated_by, buf.last_updated_name,
                      buf.creation_date, buf.created_by, buf.craeted_name,
                      buf.quantity_delivered, buf.suggested_buyer_id,
                      buf.suggested_buyer_name, buf.need_by_date,
                      buf.line_location_id, buf.justification,
                      buf.note_to_agent, buf.note_to_receiver,
                      buf.cancel_flag, buf.destination_type_code,
                      buf.destination_organization_id,
                      buf.quantity_cancelled, buf.cancel_date,
                      buf.cancel_reason, buf.closed_code,
                      buf.destination_context, buf.closed_reason,
                      buf.closed_date, buf.org_id, buf.requester_email,
                      buf.base_unit_price, buf.distribution_id,
                      buf.set_of_books_id, buf.charge_account_id,
                      buf.charge_account_segments, buf.accrual_account_id,
                      buf.accrual_account_segments, buf.variance_account_id,
                      buf.variance_account_segments, buf.distribution_num,
                      buf.allocation_type, buf.req_line_quantity, 'N'
                     );

         COMMIT;
      END LOOP;
   END xxabrl_prs_lines_dist_det;

   PROCEDURE xxabrl_prs_action_history_det
   IS
   BEGIN
      EXECUTE IMMEDIATE 'truncate table xxabrliproc.xxabrl_pr_action_dets';

      FOR buf IN
         (SELECT DISTINCT pah.object_id requisition_header_id, pah.object_id,
                          pah.object_type_code, pah.object_sub_type_code,
                          pah.sequence_num, pah.last_update_date,
                          pah.last_updated_by, pah.creation_date,
                          pah.created_by, action_code, action_date,
                          employee_id,
                          (SELECT full_name
                             FROM apps.per_all_people_f
                            WHERE person_id = pah.employee_id
                              AND (   effective_end_date IS NULL
                                   OR effective_end_date >= SYSDATE
                                  )) approval_name,
                          pah.note
                     FROM apps.po_action_history pah,
                          apps.po_requisition_headers_all prh
                    WHERE 1 = 1
                      AND (   TRUNC (prh.creation_date) = TRUNC (SYSDATE)
                           OR TRUNC (prh.creation_date) = TRUNC (SYSDATE - 1)
                          )
                      AND pah.object_id = prh.requisition_header_id
                 --AND prh.requisition_header_id = 1388
          ORDER BY        pah.object_id, pah.sequence_num)
      LOOP
         INSERT INTO xxabrliproc.xxabrl_pr_action_dets
                     (requisition_header_id, object_id,
                      object_type_code, object_sub_type_code,
                      sequence_num, last_update_date,
                      last_updated_by, creation_date,
                      created_by, action_code, action_date,
                      employee_id, approval_name, note, process_flag
                     )
              VALUES (buf.requisition_header_id, buf.object_id,
                      buf.object_type_code, buf.object_sub_type_code,
                      buf.sequence_num, buf.last_update_date,
                      buf.last_updated_by, buf.creation_date,
                      buf.created_by, buf.action_code, buf.action_date,
                      buf.employee_id, buf.approval_name, buf.note, 'N'
                     );

         COMMIT;
      END LOOP;
   END xxabrl_prs_action_history_det;
END xxabrl_pr_newly_created_data; 
/


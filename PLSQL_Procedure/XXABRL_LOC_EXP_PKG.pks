CREATE OR REPLACE PACKAGE APPS.XXABRL_LOC_EXP_PKG IS

PROCEDURE XXABRL_LOC_EXP_PROC(errbuf        OUT VARCHAR2,
                                retcode       OUT NUMBER,
                                P_FROM_PERIOD IN VARCHAR2,
                                P_TO_PERIOD   IN VARCHAR2,
                                P_FROM_LOCATION IN VARCHAR2,
                                P_TO_LOCATION   IN VARCHAR2,
                                 P_FROM_ACCOUNT   IN VARCHAR2,
                                P_TO_ACCOUNT  IN VARCHAR2,
                                P_STATE_SBU  IN VARCHAR2
                               -- P_LEDGER_ID IN VARCHAR2 
                               );
                  END XXABRL_LOC_EXP_PKG;
/


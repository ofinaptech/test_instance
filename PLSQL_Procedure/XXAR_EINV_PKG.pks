CREATE OR REPLACE PACKAGE APPS.xxar_einv_pkg
AS
    g_user_id       number := fnd_global.user_id;
    g_request_id    number := apps.fnd_global.conc_request_id;

    PROCEDURE export_einv (errbuf                out varchar2,
                           retcode               out number,
                           p_reprocess_from_date in varchar2);
END xxar_einv_pkg;
/


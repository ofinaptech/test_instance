CREATE OR REPLACE Package Body APPS.XXABRL_RESA_AR_RECPT_IMP_PKG IS

/*
  =========================================================================================================
  ||   Filename   : XXABRL_RESA_AR_CONV_PKG.sql
  ||   Description : Script is used to mold ReSA data for AR
  ||
  ||   Version     Date            Author              Modification
  ||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
  ||   1.0.0      10-may-2009   Shailesh Bharambe      New Development
  ||   1.0.1      26-nov-09     Naresh Hasti           passing the org_id to this RECEIPT_APPLY_DATA procedure
  ||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
  ========================================================================================================*/


  Procedure RECEIPT_VALIDATE(p_errbuf      OUT VARCHAR2,
                             p_retcode     OUT NUMBER,
                             p_action IN VARCHAR2,
                             P_Data_Source IN VARCHAR2,
                             p_receipt_to_apply IN varchar2,
                             p_apply_all_flag IN varchar2,
                             p_org_id in number
                             ) AS
    --
    -- Declaring cursor for validating New and Error receipt transaction
    --
    CURSOR ARR_C1(P_OPERATING_UNIT VARCHAR2) IS
      Select ROWID,
             DATA_SOURCE,
             RECEIPT_NUMBER,
             OPERATING_UNIT,
             RECEIPT_DATE,
             DEPOSIT_DATE,
             GL_DATE,
             CURRENCY_CODE,
             EXCHANGE_RATE_TYPE,
             EXCHANGE_RATE,
             EXCHANGE_DATE,
             RECEIPT_AMOUNT,
             ER_CUSTOMER_NUMBER,
             ER_DC_CODE,
             COMMENTS,
             ER_TENDER_TYPE,
             SALES_TRANSACTION_NUMBER,
             OF_CUSTOMER_NUMBER,
             RECEIPT_METHOD,
             BANK_ACCOUNT_NAME,
             BANK_ACCOUNT_NUMBER,
             INVOICE_NUMBER
        From XXABRL_RESA_AR_RECEIPT_INT
       Where Upper(trim(DATA_SOURCE)) = Upper(trim(P_DATA_SOURCE))
         And Upper(Trim(NVL(OPERATING_UNIT, P_OPERATING_UNIT))) =
             Upper(Trim(P_OPERATING_UNIT))
         And NVL(INTERFACED_FLAG, 'N') in ('N', 'E');
    --v_Orgid             Number := Fnd_Profile.VALUE('ORG_ID');
    v_set_of_bks_id     Number := Fnd_Profile.VALUE('GL_SET_OF_BKS_ID');
    V_user_Id           Number := Fnd_profile.VALUE('USER_ID');
    V_Fun_Curr          Varchar2(10);
    v_currency          fnd_currencies.Currency_Code%Type;
    V_Operating_Unit    HR_Operating_Units.Short_Code%type;
    V_bank_account_Name CE_bank_accounts.bank_account_name%TYPE;
    V_Bank_Account_num  CE_bank_accounts.bank_account_num%TYPE;
    V_Error_Count       Number := 0;
    v_OK_Rec_count      Number := 0;
    V_Error_Message     Varchar2(1000);
    v_record_count      Number := 0;
    V_Data_Count        Number := 0;
    V_Cust_Account_Id   Number;
    V_Customer_Number   Hz_cust_accounts_all.Account_Number%Type;
    V_Invoice_Number    Varchar2(20);
    V_Amount_Applied    Number;
    V_site_use_id       Number;
    V_Receipt_Method    ar_receipt_methods.name%TYPE;
    v_REMIT_BANK_ACCT_USE_ID varchar2(240);
   -- P_Org_Id number := Fnd_Profile.VALUE('ORG_ID');
  Begin
    Begin
      select short_code 
        Into V_OPERATING_UNIT
        from hr_operating_units
       Where Organization_Id = P_Org_Id;
        fnd_file.put_line(fnd_file.output,
                          'OU / Org ID '||V_OPERATING_UNIT ||' / '|| P_Org_Id);       
    EXCEPTION
      When no_data_found then
        fnd_file.put_line(fnd_file.output,
                          'Selected Org Id does not exist in Oracle Financials');
        fnd_file.put_line(fnd_file.output,
                          '........................................................................');
      When too_many_rows then
        fnd_file.put_line(fnd_file.output,
                          'Multiple Org Id exist in Oracle Financials');
        fnd_file.put_line(fnd_file.output,
                          '........................................................................');
      When Others THEN
        fnd_file.put_line(fnd_file.output, 'Invalid Org Id Selected ');
        fnd_file.put_line(fnd_file.output,
                          '........................................................................');
    End;
    fnd_file.put_line(fnd_file.output, 'Receipts/Invoice to APPLY '||nvl(p_receipt_to_apply,'NULL'));
    ----
    -- Update user id in new record 
    ----
    Update XXABRL_RESA_AR_RECEIPT_INT
       Set CREATED_BY = v_user_id
     Where Trim(Upper(DATA_SOURCE)) = Trim(Upper(P_Data_Source))
       And Trim(Upper(OPERATING_UNIT)) = Trim(Upper(V_OPERATING_UNIT))
       And NVL(INTERFACED_FLAG, 'N') = 'N'
       And CREATED_BY is Null;
    ----
    -- Remove error message before validating 
    ----
    UPDATE XXABRL_RESA_AR_RECEIPT_INT
       Set ERROR_MESSAGE = Null
     Where Trim(Upper(DATA_SOURCE)) = Trim(Upper(P_Data_Source))
       And Trim(Upper(OPERATING_UNIT)) = Trim(Upper(V_OPERATING_UNIT))
       And NVL(INTERFACED_FLAG, 'N') in ('N', 'E')
       And ERROR_MESSAGE is not null;
    Commit;
    fnd_file.put_line(fnd_file.output,
                      'Following AR Receipt Information are validating');
    fnd_file.put_line(fnd_file.output,
                      '........................................................................');
    ----
    -- Select functional currency
    ----
    Begin
      Select Currency_code
        Into V_Fun_Curr
        from GL_SETS_OF_BOOKS
       where set_of_books_id = v_set_of_bks_id;
    Exception
      When Others then
        V_Fun_Curr := Null;
    End;
    fnd_file.put_line(fnd_file.output,'***--- AR Receipt Validating Starts ---***');
    FOR ARR_R1 IN ARR_C1(V_OPERATING_UNIT) LOOP
      EXIT WHEN ARR_C1%NOTFOUND;
      v_record_count    := ARR_C1%ROWCOUNT;
      V_Error_Message   := Null;
      V_Data_Count      := Null;
      V_Cust_Account_Id := Null;
      V_site_use_id     := Null;
      v_currency        := Null;
      V_Receipt_Method  := Null;
      ----
      --  Receipt Number validation
      ---
      fnd_file.put_line(fnd_file.output,'----------------------------------------------------');      
      fnd_file.put_line(fnd_file.output,'### Validating Receipt ('||ARR_R1.Receipt_Number||')');      
      If ARR_R1.Receipt_Number is null then
        V_Error_Message := V_Error_Message || 'Receipr Number is Null';
      End If;
      ----
      --  Receipt Amount validation
      ---
      If ARR_R1.Receipt_Amount is null then
        V_Error_Message := V_Error_Message || 'Receipr Amount is Null';
      Elsif ARR_R1.Receipt_Amount <= 0 then
        V_Error_Message := V_Error_Message || 'Receipr Amount is negative';
      End IF;
      ----
      --  Receipt Date validation
      ---
      If ARR_R1.Receipt_Date is null then
        V_Error_Message := V_Error_Message || 'Receipr Date is Null';
      End If;
      ----
      --  Deposite Date validation
      ---
      If ARR_R1.Deposit_Date is null then
        V_Error_Message := V_Error_Message || 'Deposite Date is Null';
      End If;
      ----
      --  GL Date validation, should be in AR Open Period
      ---
      If ARR_R1.GL_Date is null then
        V_Error_Message := V_Error_Message || 'GL Date is Null';
      Else
        BEGIN
          V_Data_Count := 0;
          Select Count(gps.Period_Name)
            Into V_Data_Count
            From gl_period_statuses gps, fnd_application fna
           Where fna.application_short_name = 'AR'
             And fna.application_id = gps.application_id
             And gps.closing_status = 'O'
             And gps.set_of_books_id = v_set_of_bks_id
             And ARR_R1.GL_date between gps.start_date and gps.end_date;
          If V_Data_Count = 0 then
            V_Error_Message := V_Error_Message ||
                               'GL date is not in AR Open Period -->>';
          End If;
        EXCEPTION
          WHEN OTHERS then
            V_Error_Message := V_Error_Message ||
                               'GL Period not open in AR -->>';
        END;
      End If;
      ----
      --  Currency Code validation
      ---
      If ARR_R1.Currency_Code is null then
        V_Error_Message := V_Error_Message || 'Currency is null --->>';
      Else
        BEGIN
          SELECT currency_code
            Into v_currency
            FROM fnd_currencies
           WHERE upper(currency_code) = Trim(upper(ARR_R1.Currency_Code));
          If upper(trim(v_currency)) <> upper(trim(V_Fun_Curr)) and
             ARR_R1.exchange_date is null Then
            V_Error_Message := V_Error_Message ||
                               'Exchange Date is Null -->>';
          End if;
        EXCEPTION
          When no_data_found then
            V_Error_Message := V_Error_Message ||
                               'Currency Code Does Not Exist-->>';
          When too_many_rows then
            V_Error_Message := V_Error_Message ||
                               'Multiple Currency Found-->>';
          When Others Then
            V_Error_Message := V_Error_Message || 'Invalid Currency -->>';
        END;
      End If;
      ----
      --  Customer Number validation
      ---
      If ARR_R1.ER_Customer_Number is null then
        V_Error_Message := V_Error_Message ||
                           'Feeder System Customer Number is Null-->>';
      End If;
      If ARR_R1.ER_DC_CODE is null then
        V_Error_Message := V_Error_Message || 'DC Code is Null-->>';
      End If;
      If ARR_R1.ER_Customer_Number is not Null And
         ARR_R1.ER_DC_CODE is not Null Then
        Begin
          V_Cust_Account_Id := Null;
          V_site_use_id     := Null;
          Select Unique hca.Account_Number,
                 hca.Cust_Account_Id,
                 hcsu.site_use_id
            Into V_Customer_Number, V_Cust_Account_Id, V_site_use_id
            From hz_cust_accounts_all         hca,
                 hz_cust_acct_sites_all       hcas,
                 hz_party_sites               hps,
                 hz_cust_site_uses_all        hcsu
                 --,xxabrl_ar_cust_bkacc_map_int xac
           Where /*ER_DC_Code = ARR_R1.ER_DC_Code
             And ER_Customer_Number = ARR_R1.ER_Customer_Number
             And hca.Account_Number = xac.OF_Customer_Number
             And */
             hca.Account_Number = ARR_R1.ER_Customer_Number
             And hca.cust_account_id = hcas.cust_account_id
             And hcas.org_id = P_Org_id
             And hcas.cust_acct_site_id = hcsu.cust_acct_site_id(+)
             And hcas.party_site_id = hps.party_site_id
             --And hps.Party_Site_Number = xac.OF_Customer_Site_Code
             And hcsu.site_use_code = 'BILL_TO'
             And hcas.bill_to_flag = 'P'
             And hcas.status = 'A';
             Fnd_File.PUT_LINE(fnd_file.OUTPUT,'Account Number '|| ARR_R1.ER_Customer_Number);
             Fnd_File.PUT_LINE(fnd_file.OUTPUT,'Cust Acct ID '|| V_Cust_Account_Id);
             Fnd_File.PUT_LINE(fnd_file.OUTPUT,'Site Use ID '|| V_site_use_id);                          
        Exception
          When no_data_found then
            V_Error_Message := V_Error_Message ||
                               'Customer Number Or Site in not exist in this Org -->>';
          When too_many_rows then
            V_Error_Message := V_Error_Message ||
                               'Multiple Customer Number Or Site found in this Org -->>';
          When Others Then
            V_Error_Message := V_Error_Message ||
                               'Customer Number Or Site in not exist in this Org -->>';
        End;
      End If;
      ----
      --  Duplicate Receipt Number check
      ---
      If ARR_R1.Receipt_Number is not null then
        Begin
          V_Data_Count := 0;
          Select Count(Receipt_Number)
            Into V_Data_Count
            From XXABRL_RESA_AR_RECEIPT_INT
           Where Trim(Upper(P_Data_Source)) = Trim(Upper(P_Data_Source))
             And Trim(Upper(Operating_Unit)) =
                 Trim(Upper(V_Operating_Unit))
             And ER_DC_Code = ARR_R1.ER_DC_Code
             And OF_Customer_Number = ARR_R1.ER_Customer_Number
             And Trim(Receipt_Number) = Trim(ARR_R1.Receipt_Number)
             And Rowid <> ARR_R1.Rowid;
          If V_Data_Count > 0 then
            V_Error_Message := V_Error_Message ||
                               'Duplicate Receipt Number exist in Staging table -->>';
          End If;
        EXCEPTION
          WHEN OTHERS then
            V_Error_Message := V_Error_Message ||
                               'Error In Receipt Number -->>';
        END;
        Begin
          V_Data_Count := 0;
          Select Count(Receipt_Number)
            Into V_Data_Count
            From ar_cash_receipts_all
           Where Trim(Receipt_Number) = Trim(ARR_R1.Receipt_Number)
             And Pay_From_Customer = V_Cust_Account_Id
             And Org_Id = P_Org_Id;
          If V_Data_Count > 0 then
            V_Error_Message := V_Error_Message ||
                               'Receipt Number exist in Oracle Financials -->>';
          End If;
        EXCEPTION
          WHEN OTHERS then
            V_Error_Message := V_Error_Message ||
                               'Error In Receipt Number -->>';
        END;
      End If;
      ----
      --  Receipt Method validation
      ---
      /*
      If ARR_R1.ER_Tender_Type is null then
        V_Error_Message := V_Error_Message || 'Tender Type is null --->>';
      Else
        Begin
          V_Receipt_Method   := Null;
          V_Bank_Account_num := Null;
          Select OF_Receipt_Methods, OF_Bank_Account_Name
            Into V_Receipt_Method, V_Bank_Account_Name
            From XXABRL_AR_CUST_BKACC_MAP_INT
           Where ER_DC_Code = ARR_R1.ER_DC_CODE
             And ER_Customer_Number = ARR_R1.ER_Customer_Number
             And ER_Tender_Type = ARR_R1.ER_Tender_Type
             And OF_Organization_id = P_Org_Id;
        Exception
          When no_data_found then
            V_Error_Message := V_Error_Message ||
                               'Tender Type Mapping does not exist -->>';
          When too_many_rows then
            V_Error_Message := V_Error_Message ||
                               'Multiple Tender Type found -->>';
          When Others then
            V_Error_Message := V_Error_Message ||
                               'Invalid Tender Type -->>';
        End;
      End If;
      */
      If ARR_R1.Receipt_Method is Not Null Then
        Begin
          select name
            Into V_Receipt_Method
            from ar_receipt_methods
           where Upper(name) = trim(Upper(ARR_R1.Receipt_Method));
           fnd_file.PUT_LINE(fnd_file.OUTPUT,'V_Receipt_Method ' ||V_Receipt_Method);
        EXCEPTION
          When no_data_found then
            V_Error_Message := V_Error_Message || 'Mapped Receipt Method ' ||
                               V_Receipt_Method || ' does not exist -->>';
          When too_many_rows then
            V_Error_Message := V_Error_Message || 'Multiple ' ||
                               V_Receipt_Method ||
                               ' Receipt Method found -->>';
          When Others then
            V_Error_Message := V_Error_Message || 'Invalid Receipt_Method ' ||
                               V_Receipt_Method || '  -->>';
        End;
      End If;
      ----
      --  Bank Account validation
      ----
      If V_Bank_Account_num is Not Null then
        Begin
          Select Bank_Account_Num
            Into V_Bank_Account_num
            From Ce_bank_accounts
           Where Trim(Upper(Bank_Account_Name)) =
                 Trim(Upper(V_Bank_Account_Name));
          --And    Org_id    = P_Org_Id;
        EXCEPTION
          When no_data_found then
            V_Error_Message := V_Error_Message || 'Bank Account Number ' ||
                               V_Bank_Account_num || ' does not exist -->>';
          When too_many_rows then
            V_Error_Message := V_Error_Message || 'Multiple ' ||
                               V_Bank_Account_num ||
                               ' Bank Account Number found -->>';
          When Others then
            V_Error_Message := V_Error_Message ||
                               'Invalid Bank Acount Number ' ||
                               V_Bank_Account_num || ' -->>';
        End;
      End If;
      ----
      --  Get amount to be applied 
      ---
      If ARR_R1.Sales_Transaction_Number is Null Then
        V_Error_Message := V_Error_Message ||
                           'Sales Transaction Number is Null -->>';
      Else
      NULL;
      /*
      Fnd_file.PUT_LINE(fnd_file.output,'Sales_Transaction_Number '||ARR_R1.Sales_Transaction_Number);
        Begin
          V_Invoice_Number := Null;
          Select Trx_Number
            Into V_Invoice_Number
            From Ra_Customer_Trx_All
           Where Bill_To_Customer_Id = V_Cust_Account_Id
             And Bill_To_Site_Use_Id = V_site_use_id
             And Org_Id = P_Org_Id
             And Interface_Header_Attribute1 =
                 ARR_R1.Sales_Transaction_Number;
        EXCEPTION
          When no_data_found then
            V_Invoice_Number := Null;
            V_Error_Message  := V_Error_Message ||
                                'Sales Transaction Number ' ||
                                ARR_R1.Sales_Transaction_Number ||
                                ' does not exist in Current Organization-->>';
          When too_many_rows then
            V_Invoice_Number := Null;
            V_Error_Message  := V_Error_Message ||
                                'Multiple Sales Transaction Number ' ||
                                ARR_R1.Sales_Transaction_Number ||
                                ' Found -->>';
          When Others then
            V_Invoice_Number := Null;
            V_Error_Message  := V_Error_Message ||
                                'Invalid Sales Transaction Number ' ||
                                ARR_R1.Sales_Transaction_Number || ' -->>';
        End;
        If V_Invoice_Number Is Not Null Then
          V_Amount_Applied := 0;
          Begin
            Select Amount_Due_Remaining
              Into V_Amount_Applied
              From ar_payment_schedules_all
             Where Trx_number = V_Invoice_Number
               And Customer_id = V_Cust_Account_Id
               And Customer_site_use_id = V_site_use_id
               And Invoice_Currency_code = ARR_R1.CURRENCY_CODE
               And Org_id = P_Org_Id
               And Class = 'INV';
            If V_Amount_Applied = 0 Then
              V_Error_Message := V_Error_Message ||
                                 'Invoice O/S Amount is Zero, you can not apply this receipt-->>';
            End If;
            If V_Amount_Applied > ARR_R1.Receipt_Amount then
              V_Amount_Applied := ARR_R1.Receipt_Amount;
            End If;
          EXCEPTION
            When no_data_found then
              V_Error_Message := V_Error_Message || 'Invoice Number ' ||
                                 V_Invoice_Number ||
                                 ' does not exist in Current Organization-->>';
            When too_many_rows then
              V_Error_Message := V_Error_Message || 'Multiple ' ||
                                 V_Invoice_Number ||
                                 ' Invoice Number Found -->>';
            When Others then
              V_Error_Message := V_Error_Message ||
                                 'Invalid Invoice Number ' ||
                                 V_Invoice_Number || ' -->>';
          End;
        End If;*/
      End If;
      -- Get Remitt to Account ID
      If (ARR_R1.BANK_ACCOUNT_NAME is not null) 
         and (ARR_R1.BANK_ACCOUNT_NUMBER is not null) 
         and (ARR_R1.RECEIPT_METHOD is not null)
         then
      Begin
          select REMIT_BANK_ACCT_USE_ID
          into v_REMIT_BANK_ACCT_USE_ID
          FROM AR_RECEIPT_METHOD_ACCOUNTS_all
          WHERE  org_id= P_Org_Id AND 
          remit_bank_acct_use_id IN
                ( 
                SELECT ba.bank_acct_use_id
                FROM ce_bank_acct_uses_all  ba,
                ce_bank_accounts   cba,
                ce_bank_branches_v bb
                WHERE bb.bank_name like '%'
                AND bb.bank_branch_name like '%'
                AND bb.bank_institution_type = 'BANK'
                AND bb.branch_party_id = cba.bank_branch_id
                AND cba.bank_account_id = ba.bank_account_id
                AND cba.account_classification = 'INTERNAL'
                AND ba.org_id= P_Org_Id
                AND cba.bank_account_name = ARR_R1.BANK_ACCOUNT_NAME --'MH HDFC BANK - 1234'
                AND cba.bank_account_num = ARR_R1.BANK_ACCOUNT_NUMBER-- '45678901234'
                )
          and RECEIPT_METHOD_ID = (select receipt_method_id
            from ar_receipt_methods
           where Upper(name) = Upper(ARR_R1.RECEIPT_METHOD))
          order by REMIT_BANK_ACCT_USE_ID;
      Exception
      when no_data_found then
              V_Error_Message := V_Error_Message || 'Bank_Account Name/Number  ' ||
                                 ARR_R1.BANK_ACCOUNT_NAME ||' / ' || ARR_R1.BANK_ACCOUNT_NUMBER  ||
                                 ' does not exist in Current Organization-->>';      
      when too_many_rows then
              V_Error_Message := V_Error_Message || 'Bank_Account Name/Number  ' ||
                                 ARR_R1.BANK_ACCOUNT_NAME ||' / ' || ARR_R1.BANK_ACCOUNT_NUMBER  ||
                                 ' Multiple declaration-->>';            
      When others then
              V_Error_Message := V_Error_Message || 'Exception in Bank_Account Name/Number  ' ||
                                 ARR_R1.BANK_ACCOUNT_NAME ||' / ' || ARR_R1.BANK_ACCOUNT_NUMBER;            
      End;
      Else
          V_Error_Message := V_Error_Message || 'Receipt_Method/Bank_Account_Name/Acct_Number is NULL';      
      End If;
      IF V_Error_Message IS NOT NULL Then
        ----
        --  Update error message
        ---
        UPDATE XXABRL_RESA_AR_RECEIPT_INT
           SET ERROR_MESSAGE = V_Error_Message, INTERFACED_FLAG = 'E'
         WHERE ROWID = ARR_R1.ROWID;
        Commit;
        V_Error_Count := V_Error_Count + 1;
        fnd_file.put_line(fnd_file.output, v_record_count || '--');
        fnd_file.put_line(fnd_file.output,
                          ARR_R1.Receipt_Number || '-->' || '-->' ||
                          V_Error_Message);
      Else
        ----
        --  Record is valid, update additional required info
        ---
        UPDATE XXABRL_RESA_AR_RECEIPT_INT
           SET OF_Customer_Number  = Trim(V_Customer_Number),
               customer_id         = Trim(V_Cust_Account_Id),
               customer_site_id    = Trim(V_site_use_id),
               Receipt_Method      = Trim(nvl(V_Receipt_Method,Receipt_Method)),
               Bank_Account_Number = nvl(V_Bank_Account_num,Bank_Account_Number),
               Invoice_Number      = V_Invoice_Number,
               Amount_Applied      = V_Amount_Applied,
               Org_Id              = P_Org_Id,
               REMIT_BANK_ACC_ID   = v_REMIT_BANK_ACCT_USE_ID,
               Interfaced_Flag     = 'V'
         WHERE ROWID = ARR_R1.ROWID;
        Commit;
        v_OK_Rec_count := v_OK_Rec_count + 1;
        fnd_file.put_line(fnd_file.output, v_record_count || '--');
        fnd_file.put_line(fnd_file.output,
                          ARR_R1.Receipt_Number || '-->' ||
                          '-->Data without error');
      End If;
    End Loop;
    If V_Error_Count > 0 then
      P_RetCode := 1;
    End If;
    fnd_file.put_line(fnd_file.output,
                      '........................................................................');
    fnd_file.put_line(fnd_file.output,
                      'Number of Records with error :' || V_Error_Count);
    fnd_file.put_line(fnd_file.output,
                      'Number of Valid Records :' || v_OK_Rec_count);
    fnd_file.put_line(fnd_file.output,
                      '........................................................................');
    ----
    -- Calling Receipt Insert prog
    ----
    if p_action ='N' and V_Error_Count = 0 then
       --Create to Receipt
       RECEIPT_INSERT(P_Org_Id, P_Data_Source);
       --######## TEST Code
       --######## TEST Code       
    End if;
    if p_apply_all_flag = 'Y' then
       For v_Cur_Apply in (Select distinct sales_transaction_number from
                           XXABRL_RESA_AR_RECEIPT_INT
                           where
                           interfaced_flag ='C'
                           --and org_id = Fnd_Profile.VALUE('ORG_ID')
                           AND org_id = p_org_id)  --added by Naresh on 25-nov-09
       Loop
           RECEIPT_APPLY_CALL(P_Ref_Number => v_Cur_Apply.sales_transaction_number);
       End Loop;                    
    elsif p_apply_all_flag = 'N' then
       if p_receipt_to_apply is not null then
          RECEIPT_APPLY_CALL(P_Ref_Number => p_receipt_to_apply);
       End if;   
    End if;
  End RECEIPT_VALIDATE;
  PROCEDURE RECEIPT_APPLY_DATA(
                               ARR_R2 IN XXABRL_RESA_AR_RECEIPT_INT%ROWTYPE
                               ,p_Rcpt_Amt_To_Apply Number
                               ,x_Rcpt_Bal_Amt OUT Number
                               ,x_exit_flag OUT varchar2
                               ,x_org_id in number
                               )
  AS
    Cursor Cur_Open_Inv(p_Interface_Header_Attribute1 varchar2) is
select *
  from
  (
  select 
    AMOUNT_DUE_ORIGINAL,
    AMOUNT_DUE_REMAINING,
    rcta.TRX_NUMBER,
    rcta.TRX_DATE    
    from 
    Ra_Customer_Trx_ALL rcta
    ,Ar_Payment_Schedules_All apsa
    where rcta.customer_trx_id = apsa.customer_trx_id
    and apsa.AMOUNT_DUE_REMAINING    <>0
    and rcta.Interface_Header_Attribute1 = p_Interface_Header_Attribute1
    order by AMOUNT_DUE_ORIGINAL 
    )
    where rownum = 1;    
/*    select 
    AMOUNT_DUE_ORIGINAL,
    AMOUNT_DUE_REMAINING
    from 
    Ra_Customer_Trx_All rcta
    ,Ar_Payment_Schedules_All apsa
    where rcta.customer_trx_id = apsa.customer_trx_id
    and apsa.AMOUNT_DUE_REMAINING    <>0
    and rcta.Interface_Header_Attribute1 = p_Interface_Header_Attribute1
    and rownum = 1
    order by AMOUNT_DUE_ORIGINAL
    ;*/
    l_return_status   VARCHAR2(1);
    l_msg_count       NUMBER;
    l_msg_data        VARCHAR2(240);
    l_count           NUMBER;
    l_cash_receipt_id NUMBER;
    l_msg_data_out    VARCHAR2(2000);
    l_mesg            VARCHAR2(240);
    p_count           number;
    l_msg_index_out   number := 0;
    V_Ok_Receipt      Number := 0;
    V_Error_Receipt   Number := 0;
    v_Rcpt_Amt_To_Apply Number :=0;
    v_Open_Inv Number :=0;
  Begin
  Begin
    select 
      count(1)
      into v_Open_Inv
            from 
      RA_CUSTOMER_TRX/*_GT*/ rcta
      ,Ar_Payment_Schedules_All apsa
      where rcta.customer_trx_id = apsa.customer_trx_id
      and apsa.AMOUNT_DUE_REMAINING    <>0
      and rcta.Interface_Header_Attribute1 = ARR_R2.sales_transaction_number;
      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'Opn.Inv.Cnt: '||nvl(v_Open_Inv,0));                      
      if  v_Open_Inv = 0 then
          x_Rcpt_Bal_Amt := p_Rcpt_Amt_To_Apply;
          x_exit_flag :='Z'; --Force Exit      
      End if;
  Exception
  when others then
  FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'Error while deriving Open Invoice Count ');                
  End;    
  For open_inv in Cur_Open_Inv(ARR_R2.sales_transaction_number)
  Loop
      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'Applying Inv# '||open_inv.TRX_NUMBER);                
      if open_inv.AMOUNT_DUE_REMAINING < 0 then
      -- Credit Note Invoice -ve (i.e. Amount -100)
         v_Rcpt_Amt_To_Apply := open_inv.AMOUNT_DUE_ORIGINAL;
      elsif  open_inv.AMOUNT_DUE_REMAINING >= p_Rcpt_Amt_To_Apply then
      -- Invoice >= Receipt Amt same
         v_Rcpt_Amt_To_Apply := p_Rcpt_Amt_To_Apply;       
      elsif  (open_inv.AMOUNT_DUE_REMAINING < p_Rcpt_Amt_To_Apply) and (open_inv.AMOUNT_DUE_REMAINING>0) then         
      -- Invoice < Receipt Amt same      
         v_Rcpt_Amt_To_Apply := open_inv.AMOUNT_DUE_REMAINING;                
      End if;
      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,':Rcpt_Amt_Avail '||p_Rcpt_Amt_To_Apply);
      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,':Inv_Amt_To_Apply '||v_Rcpt_Amt_To_Apply);
      ar_receipt_api_pub.Apply (
                p_api_version            => 1.0,
                p_init_msg_list          => FND_API.G_TRUE,
                p_commit                 => fnd_api.g_false,                          
                p_validation_level       => fnd_api.g_valid_level_full,
                x_return_status          => l_return_status,
                x_msg_count              => l_msg_count,
                x_msg_data               => l_msg_data,  
                ----                        
                p_trx_number             => open_inv.TRX_NUMBER,
                p_receipt_number         => ARR_R2.Receipt_Number,
                p_amount_applied         => v_Rcpt_Amt_To_Apply, --<<<<<<<<
--                p_amount_applied_from    => ARR_R2.RECEIPT_AMOUNT,
                p_apply_date             => open_inv.TRX_DATE,/*,
                p_gl_date                => ARR_R2.Gl_Date*/
                p_org_id         =>    x_org_id
          );
          IF l_return_status <> FND_API.G_RET_STS_SUCCESS THEN
            FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                              '***Error in Receipt Apply #: ' || ARR_R2.RECEIPT_NUMBER);
            IF l_msg_count >= 1 THEN
              l_msg_data_out := l_msg_data_out || ' ~ ' || l_msg_data;
              FOR i IN 0 .. l_msg_count LOOP
                FND_MSG_PUB.GET(p_msg_index     => i,
                                p_encoded       => 'F',
                                p_data          => l_msg_data,
                                p_msg_index_out => l_msg_index_out);
                l_msg_data_out := l_msg_data_out || ' ~ ' || l_msg_data;
              END LOOP;
            END IF;
            V_Error_Receipt := V_Error_Receipt + 1;
            -----
            -- Update error message
            -----
            UPDATE XXABRL_RESA_AR_RECEIPT_INT
               SET INTERFACED_FLAG = 'EA', ERROR_MESSAGE = l_msg_data_out    --EA: Error in Apply
             WHERE Receipt_Number = ARR_R2.Receipt_Number;
            Commit;
            FND_FILE.PUT_LINE(FND_FILE.OUTPUT, '***API Error: '||l_msg_data_out);
          ELSE
            -----
            -- Update interfaced flag
            -----                                                                                           
            UPDATE XXABRL_RESA_AR_RECEIPT_INT
               SET INTERFACED_FLAG = 'A'     --Successfully APPLIED
             WHERE Receipt_Number = ARR_R2.Receipt_Number;
            V_OK_Receipt := V_OK_Receipt + 1;
            FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                              '##Receipt(Inv) Applied Successfully : ' || ARR_R2.RECEIPT_NUMBER||'('||open_inv.TRX_NUMBER||')');
            Commit;
          END IF;
          x_exit_flag :='N'; --Default
          if open_inv.AMOUNT_DUE_REMAINING < 0 then
          -- Credit Note Invoice -ve (i.e. Amount -100)
             IF l_return_status = FND_API.G_RET_STS_SUCCESS THEN
                  x_Rcpt_Bal_Amt := abs(open_inv.AMOUNT_DUE_ORIGINAL) + p_Rcpt_Amt_To_Apply;
                  x_exit_flag :='N'; --Force Exit
             Else
                  x_Rcpt_Bal_Amt := 0; --abs(open_inv.AMOUNT_DUE_ORIGINAL) + p_Rcpt_Amt_To_Apply;
                  x_exit_flag :='Y'; --Force Exit             
             End If;             
          elsif  open_inv.AMOUNT_DUE_REMAINING >= p_Rcpt_Amt_To_Apply then
          -- Invoice >= Receipt Amt same
             x_Rcpt_Bal_Amt := 0;       
             x_exit_flag :='N'; --Force Exit             
          elsif  (open_inv.AMOUNT_DUE_REMAINING < p_Rcpt_Amt_To_Apply) and (open_inv.AMOUNT_DUE_REMAINING>0) then         
          -- Invoice < Receipt Amt same      
             x_Rcpt_Bal_Amt := p_Rcpt_Amt_To_Apply - open_inv.AMOUNT_DUE_REMAINING;       
             x_exit_flag :='N'; --Force Exit                 
          End if;          
          FND_FILE.PUT_LINE(FND_FILE.OUTPUT,':Rcpt_Amt_Balance '||x_Rcpt_Bal_Amt);          
      End Loop;        
  End RECEIPT_APPLY_DATA;
  Procedure RECEIPT_APPLY_CALL(P_Ref_Number IN VARCHAR2) 
  As
  Cursor Cur_Rcpt is
  Select * from
  XXABRL_RESA_AR_RECEIPT_INT
  where
  interfaced_flag ='C'
  and sales_transaction_number =P_Ref_Number
  order by receipt_amount desc
  ; -- Create Cash done
  v_Rcpt_Amt_To_Apply Number :=0;
  v_Rcpt_Bal_Amt Number :=0;
  v_exit_flag varchar2(2) :='N';
  Begin
  FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'::::::::::::::::::::::::::::::::::::::');  
  FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'Sales Transaction Number(i.e. Inv/Recpt Common #): '||P_Ref_Number);            
  For c_Cur_Rcpt in Cur_Rcpt
  Loop
      v_Rcpt_Amt_To_Apply := c_Cur_Rcpt.receipt_amount;
      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'1. Receipt# (Amount) '||c_Cur_Rcpt.Receipt_Number||' ('||c_Cur_Rcpt.receipt_amount||')');          
      Loop
          --Rollback here, Get Status Flag per receipt / Commit etc    
          RECEIPT_APPLY_DATA(c_Cur_Rcpt,v_Rcpt_Amt_To_Apply,v_Rcpt_Bal_Amt,v_exit_flag,c_Cur_Rcpt.org_id);
          v_Rcpt_Amt_To_Apply :=v_Rcpt_Bal_Amt;
          if v_exit_flag = 'Z' then
             FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'Opn.Inv.Cnt# Zero');          
          End If;   
          Exit when (v_Rcpt_Bal_Amt <=0 or v_exit_flag in('Y','Z'));
          --Exit when 1=1;          
      End Loop;
      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'........................................');  
  End Loop;
  End RECEIPT_APPLY_CALL;
  Procedure RECEIPT_INSERT(P_Org_Id In NUMBER, P_Data_Source IN VARCHAR2) AS
    --
    -- Declaring cursor for selecting valid receipt transaction
    --
    CURSOR ARR_C2 IS
      Select ROWID,
             DATA_SOURCE,
             RECEIPT_NUMBER,
             OPERATING_UNIT,
             RECEIPT_DATE,
             DEPOSIT_DATE,
             GL_DATE,
             CURRENCY_CODE,
             EXCHANGE_RATE_TYPE,
             EXCHANGE_RATE,
             EXCHANGE_DATE,
             Receipt_Amount,
             ER_Customer_Number,
             ER_DC_CODE,
             COMMENTS,
             ER_TENDER_TYPE,
             Sales_Transaction_Number,
             OF_Customer_Number,
             Receipt_Method,
             Bank_Account_Name,
             Bank_Account_Number,
             Invoice_Number,
             Customer_Id,
             Customer_Site_Id,
             Amount_Applied,
             REMIT_BANK_ACC_ID
        From XXABRL_RESA_AR_RECEIPT_INT
       Where Upper(trim(DATA_SOURCE)) = Upper(trim(P_DATA_SOURCE))
         And Org_Id = P_Org_Id
         And NVL(INTERFACED_FLAG, 'N') in ('V','EC');
    v_record_count Number := 0;
    --v_attribute_rec         AR_RECEIPT_API_PUB.attribute_rec_type;
    l_return_status   VARCHAR2(1);
    l_msg_count       NUMBER;
    l_msg_data        VARCHAR2(240);
    l_count           NUMBER;
    l_cash_receipt_id NUMBER;
    l_msg_data_out    VARCHAR2(2000);
    l_mesg            VARCHAR2(240);
    p_count           number;
    l_msg_index_out   number := 0;
    V_Ok_Receipt      Number := 0;
    V_Error_Receipt   Number := 0;
  Begin
/*    begin
         mo_global.set_policy_context('S',84);
         arp_global.init_global(84);
         arp_standard.init_standard(84);
    end; 
*/  
    FOR ARR_R2 IN ARR_C2 LOOP
      EXIT WHEN ARR_C2%NOTFOUND;
      v_record_count := ARR_C2%ROWCOUNT;
      l_msg_data_out := null;
      ----
      -- Receipt API to create and Apply recipt
      ----
      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'P_Org_Id : ' || P_Org_Id);
FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'p_currency_code               =>: ' || ARR_R2.CURRENCY_CODE);
FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'p_amount                      =>: ' || ARR_R2.Receipt_Amount);
FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'p_receipt_number              =>: ' || ARR_R2.RECEIPT_NUMBER);
FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'p_receipt_date                =>: ' || ARR_R2.RECEIPT_DATE);
FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'p_gl_date                     =>: ' || ARR_R2.GL_DATE);
FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'p_customer_id                 =>: ' || ARR_R2.Customer_Id);
FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'p_customer_site_use_id        =>: ' || ARR_R2.Customer_Site_Id);
FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'p_deposit_date                =>: ' || ARR_R2.DEPOSIT_DATE);
FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'p_receipt_method_name         =>: ' || ARR_R2.Receipt_Method);
FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'p_remittance_bank_account_num =>: ' || ARR_R2.Bank_Account_Number);
FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'p_receipt_comments            =>: ' || ARR_R2.COMMENTS);
FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'p_trx_number                  =>: ' || ARR_R2.Invoice_Number);
FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'p_amount_applied              =>: ' || ARR_R2.Amount_Applied);
FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'p_apply_date                  =>: ' || ARR_R2.GL_DATE);
FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'p_apply_gl_date               =>: ' || ARR_R2.GL_DATE);
FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'p_cr_id                       =>: ' || l_cash_receipt_id);
FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'p_org_id                      =>: ' || P_Org_Id);      
  ar_receipt_api_pub.create_cash (
         p_api_version                  => 1.0
        ,p_init_msg_list                => fnd_api.g_true
        ,p_commit                       => fnd_api.g_false
        ,p_validation_level             => fnd_api.g_valid_level_full
        ,x_return_status                => l_return_status
        ,x_msg_count                    => l_msg_count
        ,x_msg_data                     => l_msg_data
        ,p_currency_code                => ARR_R2.CURRENCY_CODE
        ,p_amount                       => ARR_R2.Receipt_Amount
        ,p_receipt_number               => ARR_R2.RECEIPT_NUMBER
        ,p_receipt_date                 => ARR_R2.RECEIPT_DATE
        ,p_cr_id                        => l_cash_receipt_id  --OUT
        ,p_receipt_method_name          => ARR_R2.Receipt_Method
        ,p_customer_number              => ARR_R2.OF_CUSTOMER_NUMBER --Customer_Id
        ,p_comments                     => ARR_R2.COMMENTS
--        ,p_customer_receipt_reference   => SUBSTR(p_rhp_receipt.sender_to_receiver_info,1,30)
        ,p_remittance_bank_account_id   => ARR_R2.REMIT_BANK_ACC_ID
        ,p_org_id                       => P_Org_Id
        
      );
/*      AR_RECEIPT_API_PUB.Create_and_apply(p_api_version                 => 1.0,
                                          p_init_msg_list               => FND_API.G_TRUE,
                                          p_commit                      => FND_API.G_TRUE,
                                          p_validation_level            => FND_API.G_VALID_LEVEL_FULL,
                                          x_return_status               => l_return_status,
                                          x_msg_count                   => l_msg_count,
                                          x_msg_data                    => l_msg_data,
                                          p_currency_code               => ARR_R2.CURRENCY_CODE,
                                          p_amount                      => ARR_R2.Receipt_Amount,
                                          p_receipt_number              => ARR_R2.RECEIPT_NUMBER,
                                          p_receipt_date                => ARR_R2.RECEIPT_DATE,
                                          p_gl_date                     => ARR_R2.GL_DATE,
                                          p_customer_id                 => ARR_R2.Customer_Id,
                                          p_customer_site_use_id        => ARR_R2.Customer_Site_Id,
                                          p_deposit_date                => ARR_R2.DEPOSIT_DATE,
                                          p_receipt_method_name         => ARR_R2.Receipt_Method,
                                          p_remittance_bank_account_num => ARR_R2.Bank_Account_Number,
                                          p_receipt_comments            => ARR_R2.COMMENTS,
                                          p_trx_number                  => ARR_R2.Invoice_Number,
                                          p_amount_applied              => ARR_R2.Amount_Applied,
                                          p_apply_date                  => ARR_R2.GL_DATE,
                                          p_apply_gl_date               => ARR_R2.GL_DATE,
                                          p_cr_id                       => l_cash_receipt_id,
                                          p_org_id                      => P_Org_Id);*/
      IF l_return_status <> FND_API.G_RET_STS_SUCCESS THEN
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                          'Receipt Number : ' || ARR_R2.RECEIPT_NUMBER ||
                          ' Error while creating receipt.');
        IF l_msg_count >= 1 THEN
          l_msg_data_out := l_msg_data_out || ' ~ ' || l_msg_data;
          FOR i IN 0 .. l_msg_count LOOP
            FND_MSG_PUB.GET(p_msg_index     => i,
                            p_encoded       => 'F',
                            p_data          => l_msg_data,
                            p_msg_index_out => l_msg_index_out);
            l_msg_data_out := l_msg_data_out || ' ~ ' || l_msg_data;
          END LOOP;
        END IF;
        V_Error_Receipt := V_Error_Receipt + 1;
        -----
        -- Update error message
        -----
        UPDATE XXABRL_RESA_AR_RECEIPT_INT
           SET INTERFACED_FLAG = 'EC', ERROR_MESSAGE = l_msg_data_out    -- EC: Error in Creation
         WHERE ROWID = ARR_R2.ROWID;
        Commit;
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT, l_msg_data_out);
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                          '-----------------------------------------');
      ELSE
        -----
        -- Update interfaced flag
        -----                                                                                           
        UPDATE XXABRL_RESA_AR_RECEIPT_INT
           SET INTERFACED_FLAG = 'C'
         WHERE ROWID = ARR_R2.ROWID;
        V_OK_Receipt := V_OK_Receipt + 1;
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                          'Receipt Number : ' || ARR_R2.RECEIPT_NUMBER ||
                          '  created successfully.');
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                          '-----------------------------------------');
        Commit;
      END IF;
    End Loop;
    FND_FILE.PUT_LINE(FND_FILE.OUTPUT, 'Receipt Created :' || V_OK_Receipt);
    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                      'Error Receipt d :' || V_Error_Receipt);
    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,
                      '-----------------------------------------');
  End RECEIPT_INSERT;
End XXABRL_RESA_AR_RECPT_IMP_PKG; 
/


CREATE OR REPLACE PACKAGE BODY APPS.XXABRL_INVALID_GRN_DETAIL_PKG is  
/* ***********************************************************************************************
  --                              Aditya Birla Retail Limited
  ************************************************************************************************* 
  =========================================================================================================
  ||   Filename     : XXABRL_INVALID_GRN_DETAIL_PKG.sql
  ||   Description : Script is used to get Instances of GRN been done after cut off period of 26th of every month.
  ||
  ||   Version     Date            Author              Modification
  ||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
  ||   1.0.0       28-AUG-2012    Vikash Kumar        New Development
  
  ========================================================================================================*/
                                       
PROCEDURE XXABRL_INVALID_GRN_PRC
(                                                      errbuf  out  VARCHAR2, -- is a Out parameter which is used to store error message whenever a program gets into an exception block
                                                       retcode  out  NUMBER,    -- is a Out parameter which is used to record the status of the concurrent request
                                                       P_from_date varchar2
                                                      -- P_to_date date
                                                     )
AS
/* ***********************************************************************************************
  --                              Aditya Birla Retail Limited
  ************************************************************************************************* 
  =========================================================================================================
  ||   Filename     : XXABRL_INVALID_GRN_PRC.sql
  ||   Description : Script is used to get Instances of GRN been done after cut off period of 26th of every month.
  ||
  ||   Version     Date            Author              Modification
  ||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
  ||   1.0.0       28-AUG-2012    Vikash Kumar        New Development
  
  ========================================================================================================*/

--Local variable Declaration

l_date varchar2(20);
l_day_month varchar2(20);
begin  -- --Main begin for procedure XXABRL_INVALID_GRN_PRC

begin

--assigning values to local variables
/*Below select query is used to get the Month and Year 
from the Parameter date and concatenated with 26th as we have to run the query from 26th of every month*/


select ''||26||'-'||TO_CHAR (TO_DATE (SUBSTR (P_from_date,1,10),'RRRR/MM/DD'),'MON-YYYY')||'' into l_date from dual;

/*Below select query is used to get last day of month 
from the Parameter date as we have to run the query upto last date of every month*/

select TO_CHAR (last_day(TO_DATE (SUBSTR (P_from_date, 1, 10), 'RRRR/MM/DD')),'DD-MON-YYYY') into l_day_month from dual;
--select ''||26||substr(to_char(trunc(P_from_date),'DD-MON-YYYY'),3)||'' into v_date from dual;
fnd_file.put_line(fnd_file.output,'Program Run From Date is '||l_date);
fnd_file.put_line(fnd_file.output,'Program Run to Date is '||l_day_month);
exception
when others then
--v_date:='26-JUL-2012';
fnd_file.put_line(fnd_file.log,'Exception found');
end;

/* Priting the labels for parameter*/ 

FND_FILE.PUT_LINE(FND_FILE.OUTPUT,CHR(9)            ||'XXABRL Invalid GRN Report');
FND_FILE.PUT_LINE(FND_FILE.OUTPUT,CHR(9)            ||'                   ');
FND_FILE.PUT_LINE (FND_FILE.OUTPUT, 'REPORT PARAMETRS ');
FND_FILE.PUT_LINE (FND_FILE.OUTPUT,
                            CHR (9)
                         || CHR (9)
                         || CHR (9)
                         || 'From date:'
                         || CHR (9)
                         || P_from_date
                         || CHR (9)
                        );

 /* Priting the labels for the Report */  
 
       FND_FILE.PUT_LINE(FND_FILE.OUTPUT,CHR(9)
                                      ||'Operating Unit'||CHR(9)
                                      ||'PO Number'||CHR(9)
                                      ||'GRN Number'||CHR(9)
                                      ||'GRN Date'||CHR(9)
                                      ||'GRN creator'||CHR(9)
                                      );
  ---below is cursor query,is used to fetch GRN data which have created after cut Off period of Month (i.e b/w 26th and last day of month)
for grn_rec in
 (select distinct hou.NAME operating_unit,
 ph.SEGMENT1 po_number,
 rsh.RECEIPT_NUM grn_num,
 trunc(rsh.CREATION_DATE) grn_date,
 fu.DESCRIPTION grn_creator
-- papf.FULL_NAME grn_creator
 from apps.rcv_shipment_headers rsh,
 apps.rcv_transactions rt,
 apps.po_headers_all ph,
 apps.hr_operating_units hou,
 apps.fnd_user fu
 ---apps.per_all_people_f papf
 where 1=1
 and rt.SHIPMENT_HEADER_ID=rsh.SHIPMENT_HEADER_ID
 and rt.PO_HEADER_ID=ph.PO_HEADER_ID
 and ph.ORG_ID=hou.ORGANIZATION_ID
 and rsh.CREATED_BY=fu.USER_ID
 and nvl(fu.END_DATE,sysdate+1)>sysdate
-- and rsh.CREATED_BY=papf.PERSON_ID(+)
 --and nvl(papf.EFFECTIVE_END_DATE,sysdate+1)>sysdate
 and trunc(rsh.CREATION_DATE) between l_date and l_day_month)
 loop
 
 --printing cursor values to output
 FND_FILE.PUT_LINE(FND_FILE.OUTPUT,CHR(9)
                                      ||grn_rec.operating_unit||CHR(9)
                                      ||grn_rec.po_number||CHR(9)
                                      ||grn_rec.grn_num||CHR(9)           
                                      ||grn_rec.grn_date||CHR(9)   
                                      ||grn_rec.grn_creator||CHR(9)
                                      );
end loop;
end;
end XXABRL_INVALID_GRN_DETAIL_PKG; 
/


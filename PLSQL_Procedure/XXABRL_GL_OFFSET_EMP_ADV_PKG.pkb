CREATE OR REPLACE PACKAGE BODY APPS.XXABRL_GL_OFFSET_EMP_ADV_PKG IS

 /*
  =========================================================================================================
  ||   Filename   : XXABRL_GL_OFFSET_EMP_ADV_PKG.sql
  ||   Description : Script is used to list Employee ticketing advance data
  ||
  ||   Version     Date            Author              Modification
  ||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
  ||   1.0.0       25-NOV-2008    Shailesh Bharambe      New Development
  ||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
  ||
  ||
  ========================================================================================================*/
 

  PROCEDURE GL_OFFSET_EMP_ADV_PRINT(Errbuf         OUT VARCHAR2,
                             RetCode        OUT NUMBER,
                             p_org_id in NUMBER,
                             P_FROM_GL_DATE IN VARCHAR2,
                             P_TO_GL_DATE    IN VARCHAR2
                             ) AS
 

CURSOR CUR_INV_LINE_DATA(cp_from_gl_date varchar2,cp_to_gl_date varchar2,cp_org_id NUMBER)    IS
select hou.name,
       API.INVOICE_NUM, --
       PV.SEGMENT1, --
       PV.VENDOR_NAME , --
       ail.attribute3 employee_number, --
       ppf.full_name Employee_name, --
       ail.attribute4 advanced_type, --
       ail.attribute5 Invoice_no, --
       aid.accounting_date, --
       --AIL.AMOUNT,
       aid.amount, --
       api.doc_sequence_value voucher_number, --
       ail.description --
from apps.ap_invoices_all api,
     apps.ap_invoice_lines_all ail,
     APPS.PO_VENDORS PV,
     APPS.PER_ALL_PEOPLE_F PPF,
     apps.gl_code_combinations_kfv gll,
     apps.ap_invoice_distributions_all aid,
     apps.hr_operating_units hou
where api.invoice_id=ail.invoice_id
--and api.invoice_id = cp_inv_id     
AND PV.VENDOR_ID=API.VENDOR_ID
AND PPF.EMPLOYEE_NUMBER(+)=ail.attribute3
AND SYSDATE BETWEEN PPF.EFFECTIVE_START_DATE(+) AND EFFECTIVE_END_DATE(+)
and aid.dist_code_combination_id = gll.code_combination_id
and aid.invoice_line_number=ail.line_number
and aid.invoice_id=api.invoice_id
--and gll.code_combination_id=cp_ccid
and gll.segment6='242002'
and hou.organization_id=api.org_id
and api.org_id =nvl(p_org_id,api.org_id) 
and aid.accounting_date between to_date(cp_from_gl_date,'yyyy/mm/dd hh24:mi:ss')
and to_date(cp_to_gl_date,'yyyy/mm/dd hh24:mi:ss') ;





v_cnt_lin number :=0;

r_count number:=999;
prev_batch_name varchar2(1000):='';
prev_lin_num number:=0;
prv_acct_code varchar2(100):='';


BEGIN



Fnd_File.put_line (Fnd_File.output, ' ');
      Fnd_File.put_line (Fnd_File.output, ' ');
      Fnd_File.put_line (Fnd_File.output,
                          'Operating Unit'  
                         || CHR (9)
                         ||'Invoice Number'  
                         || CHR (9)
                         || 'Line Description'                     
                         || CHR (9)
                         || 'Vendor Number'     
                         || CHR (9)
                         || 'Vendor Name'     
                         || CHR (9)
                         || 'Amount'
                         || CHR (9)
                         || 'Voucher Number'
                         || CHR (9)
                         || 'Employee Number'    
                         || CHR (9)
                         || 'Employee Name'     
                         || CHR (9)
                         || 'Advance Type'     
                         || CHR (9)
                         || 'Invoice No from DFF'
                         || CHR (9)
                         || 'Accounting Date'      
                        );
                        
for j in  CUR_INV_LINE_DATA(P_FROM_GL_DATE,P_TO_GL_DATE,p_org_id) loop

    Fnd_File.put_line (Fnd_File.output,
                          j.name  
                         || CHR (9)
                         || j.INVOICE_NUM  
                         || CHR (9)
                         || j.description                   
                         || CHR (9)
                         || j.SEGMENT1     
                         || CHR (9)
                         || j.VENDOR_NAME  
                         || CHR (9)
                         || j.amount   
                         || CHR (9)
                         || j.voucher_number   
                         || CHR (9)
                         || j.employee_number    
                         || CHR (9)
                         || j.Employee_name    
                         || CHR (9)
                         || j.advanced_type     
                         || CHR (9)
                         || j.Invoice_no
                         || CHR (9)
                         || j.accounting_date        
                        );
        
 /*end if;
   */                 
end loop; 

     



   
        
END GL_OFFSET_EMP_ADV_PRINT; 







PROCEDURE GL_OFFSET_EMP_ADV_PRINT_old(Errbuf         OUT VARCHAR2,
                             RetCode        OUT NUMBER,
                             P_FROM_GL_DATE IN VARCHAR2,
                             P_TO_GL_DATE    IN VARCHAR2
                             ) AS
 
   
CURSOR CUR_GL_DATA(cp_from_date varchar2,cp_to_date varchar2) IS
select  data.entity_id, 
        data.Invoice_id, 
      --  gll.je_header_id,
        data.name,
        data.je_source,
        data.je_category,--,gl.concatenated_segment 
        data.gl_date,
        data.journal_description,
        data.je_line_num,
          data.document_number,
        data.batch_status,  -- 
       data.SBU,  -- 
       data.LOCATION,  -- 
       data.GL_account,
       data.concatenated_segments ,
        data.entered_dr,
       data.entered_cr,
        data.ACCOUNTED_AMT_DR,
        data.ACCOUNTED_AMT_CR,
        DATA.code_combination_id
from (        
select  xah.entity_id, 
        to_number(xte.source_id_int_1) Invoice_id, 
      --  gll.je_header_id,
        glb.name,
        glh.je_source,
        glh.je_category,--,gl.concatenated_segment 
        glh.default_effective_date gl_date,
        gll.description journal_description,
        gll.je_line_num,
          NVL(glir.SUBLEDGER_DOC_SEQUENCE_VALUE,glh.doc_sequence_value) document_number,
        DECODE (GLB.status, 
               'P', 'Posted', 
               'U', 'Unposted', 
               GLB.status 
              ) batch_status,  -- 
       gl.segment3 SBU,  -- 
       gl.segment4 LOCATION,  -- 
       gl.segment6 GL_account,
       gl.concatenated_segments ,
        gll.entered_dr,
        gll.entered_cr,
/*        sum(glir.reference_9) ACCOUNTED_AMT_DR,
        sum(glir.reference_10) ACCOUNTED_AMT_CR,
        sum(xla.accounted_dr) xla_acct_dr,
        sum(xla.accounted_cr) xla_acct_cr*/
        sum(xla.accounted_dr) ACCOUNTED_AMT_DR,
        sum(xla.accounted_cr) ACCOUNTED_AMT_CR,
        gl.code_combination_id
from apps.gl_je_headers glh, 
       apps.gl_je_lines gll, 
       apps.gl_code_combinations_kfv gl, 
       apps.gl_je_batches GLB,
        --APPS.XLA_AE_HEADERS XAH     ,
        apps.gl_import_references glir ,    
        apps.xla_ae_lines xla ,
         apps.xla_ae_headers xah
         ,xla.xla_transaction_entities XTE 
 where glh.je_header_id = gll.je_header_id 
  AND gl.code_combination_id = gll.code_combination_id 
   AND glh.je_batch_id = GLB.je_batch_id   
      AND gll.je_header_id = glir.je_header_id (+)     
 aND gll.je_line_num = glir.je_line_num (+)  
   AND glir.gl_sl_link_id = xla.gl_sl_link_id (+)     
 AND glir.gl_sl_link_table = xla.gl_sl_link_table (+) 
 AND NVL(xla.accounted_cr,999999)!=0         
   AND NVL(xla.accounted_dr,999999)!=0     
   and gl.segment6='242002'
   --and glh.default_effective_date between to_date(cp_from_date,'yyyy/mm/dd hh24:mi:ss')
  --and to_date(cp_to_date,'yyyy/mm/dd hh24:mi:ss')  
   --and glb.name='Payables A 1018044 26824276 3'--'CORP OFFICE PAYABLE/01-MAY-09 Payables A 25867 24384944 2'--'Payables A 23867 23570752 3' 
  --and gll.je_line_num=7
  --and NVL(glir.SUBLEDGER_DOC_SEQUENCE_VALUE,glh.doc_sequence_value)='360018256'
     and xah.ae_header_id=xla.ae_header_id
         AND xte.entity_id=xah.entity_id 
       AND XAH.EVENT_TYPE_CODE <> 'PREPAYMENT UNAPPLIED'
   group by  xah.entity_id,xte.source_id_int_1 , 
        glb.name,
        glh.je_source,
        glh.je_category, 
        glh.default_effective_date ,
        gll.description ,
        gll.je_line_num,
        DECODE (GLB.status, 
               'P', 'Posted', 
               'U', 'Unposted', 
               GLB.status 
              ) ,  -- 
       gl.segment3 ,  -- 
       gl.segment4,  -- 
       gl.segment6 ,
       NVL(glir.SUBLEDGER_DOC_SEQUENCE_VALUE,glh.doc_sequence_value),
       gl.concatenated_segments ,
        gll.entered_dr,
        gll.entered_cr,gl.code_combination_id
order by gll.je_line_num,xah.entity_id, xte.source_id_int_1
) data
order by data.name,data.je_line_num,data.concatenated_segments,data.Invoice_id;







CURSOR CUR_GL_DATA_OTHER(cp1_from_date varchar2,cp2_to_date varchar2) IS
        SELECT GLB.NAME name,
       glh.je_source SOURCE, 
       glh.je_category CATEGORY,
       glh.default_effective_date gl_date,
       gll.description journal_description,
       gll.je_line_num line_number,
       glh.doc_sequence_value document_number,
        DECODE (GLB.status,
               'P', 'Posted',
               'U', 'Unposted',
               GLB.status
              ) batch_status,
     gl.segment3 SBU,
     gl.segment4 LOCATION,
     gl.segment6 GL_account,
      gl.concatenated_segments account_code,
      GLL.ACCOUNTED_DR dr_amount,
      GLL.ACCOUNTED_CR cr_amount
FROM
       apps.gl_je_headerS glh,
       apps.gl_je_lines gll,
       apps.gl_code_combinations_kfv gl,
       apps.fnd_user fu,
       apps.gl_je_batches GLB
WHERE glh.je_header_id = gll.je_header_id
  AND gl.code_combination_id = gll.code_combination_id
  AND glh.je_batch_id = GLB.je_batch_id
  AND gll.created_by=fu.user_id
  and gl.segment6='242002'   
   --and glh.default_effective_date between to_date(cp1_from_date,'yyyy/mm/dd hh24:mi:ss')
 -- and to_date(cp2_to_date,'yyyy/mm/dd hh24:mi:ss')  
  -- and glb.name like  '2478808 21-MAY-2009 20:59:02%'
  AND UPPER(glh.je_source)  IN ('MANUAL','SPREADSHEET');



CURSOR CUR_INV_LINE_DATA(cp_inv_id number,cp_ccid number)    IS
select API.INVOICE_NUM,
       PV.SEGMENT1,
       PV.VENDOR_NAME ,
       ail.attribute3 employee_number,
       ppf.full_name Employee_name,
       ail.attribute4 advanced_type,
       ail.attribute5 Invoice_no,
       --AIL.AMOUNT,
       aid.amount,
       api.doc_sequence_value voucher_number,
       ail.description
from apps.ap_invoices_all api,
     apps.ap_invoice_lines_all ail,
     APPS.PO_VENDORS PV,
     APPS.PER_ALL_PEOPLE_F PPF,
     gl_code_combinations_kfv gll,
    ap_invoice_distributions_all aid
where api.invoice_id=ail.invoice_id
and api.invoice_id = cp_inv_id     
AND PV.VENDOR_ID=API.VENDOR_ID
AND PPF.EMPLOYEE_NUMBER(+)=ail.attribute3
AND SYSDATE BETWEEN PPF.EFFECTIVE_START_DATE(+) AND EFFECTIVE_END_DATE(+)
and aid.dist_code_combination_id = gll.code_combination_id
and aid.invoice_line_number=ail.line_number
and aid.invoice_id=api.invoice_id
and gll.code_combination_id=cp_ccid
and gll.segment6='242002';



/*

select API.INVOICE_NUM,PV.SEGMENT1,PV.VENDOR_NAME ,default_dist_ccid,default_dist_ccid,
ail.attribute3 employee_number
,ppf.full_name Employee_name
,ail.attribute4 advanced_type
,ail.attribute5 Invoice_no
,ail.amount
from apps.ap_invoices_all api,
     apps.ap_invoice_lines_all ail,
     APPS.PO_VENDORS PV,
    APPS.PER_ALL_PEOPLE_F PPF,
    gl_code_combinations_kfv gll,
    ap_invoice_distributions_all aid
where api.invoice_id=ail.invoice_id
and api.invoice_id in ( 976033)
AND PV.VENDOR_ID=API.VENDOR_ID
AND PPF.EMPLOYEE_NUMBER(+)=ail.attribute3
AND SYSDATE BETWEEN PPF.EFFECTIVE_START_DATE(+) AND EFFECTIVE_END_DATE(+)
and aid.dist_code_combination_id = gll.code_combination_id
and aid.invoice_line_number=ail.line_number
and aid.invoice_id=api.invoice_id
and gll.segment6='242002'
*/


v_cnt_lin number :=0;

r_count number:=999;
prev_batch_name varchar2(1000):='';
prev_lin_num number:=0;
prv_acct_code varchar2(100):='';
BEGIN



Fnd_File.put_line (Fnd_File.output, ' ');
      Fnd_File.put_line (Fnd_File.output, ' ');
      Fnd_File.put_line (Fnd_File.output,
                           'Batch Name'
                         || CHR (9)
                         || 'Source'
                         || CHR (9)
                         || 'Category'
                         || CHR (9)
                         || 'GL Date'
                         || CHR (9)
                         || 'Journal Description'
                         || CHR (9)
                         || 'Line Number'
                         || CHR (9)
                         || 'Batch Status'
                         || CHR (9)
                         || 'Account Code'
                         || CHR (9)
                         || 'Document Number' 
                         || CHR (9)
                         || 'GL Dr Amount'
                         || CHR (9)
                         || 'GL Cr Amount'
                         || CHR (9)
                         || 'AP ACCOUNTED AMT DR'
                         || CHR (9)
                         || 'AP ACCOUNTED AMT CR'   
                         || CHR (9)
                         || 'Invoice Number'  
                          || CHR (9)
                         || 'Line Description'                     
                         || CHR (9)
                         || 'Vendor Number'     
                         || CHR (9)
                         || 'Vendor Name'     
                         || CHR (9)
                         || 'Amount'
                         || CHR (9)
                         || 'Voucher Number'
                         || CHR (9)
                         || 'Employee Number'    
                         || CHR (9)
                         || 'Employee Name'     
                         || CHR (9)
                         || 'Advance Type'     
                         || CHR (9)
                         || 'Invoice No from DFF'      
                        );

        
for i in   CUR_GL_DATA(P_FROM_GL_DATE ,
                             P_TO_GL_DATE    ) loop
   
   
 
if prev_batch_name = i.name and prev_lin_num =i.je_line_num and  prv_acct_code =i.concatenated_segments then


null;
else
   
 Fnd_File.put_line (Fnd_File.output,
                           i.name
                         || CHR (9)
                         || i.je_source
                         || CHR (9)
                         || i.je_category
                         || CHR (9)
                         || i.gl_date
                         || CHR (9)
                         || i.Journal_Description
                         || CHR (9)
                         || i.je_line_num
                         || CHR (9)
                         || i.batch_status
                         || CHR (9)
                         || i.concatenated_segments
                         || CHR (9)
                         || i.document_number
                         || CHR (9)
                         || i.entered_dr
                         || CHR (9)
                         || i.entered_cr
                         || CHR (9)
                         || i.ACCOUNTED_AMT_DR
                         || CHR (9)
                         || i.ACCOUNTED_AMT_CR       
                        );
v_cnt_lin :=0;
end if;

prev_batch_name := i.name; 
prev_lin_num :=i.je_line_num;
prv_acct_code :=i.concatenated_segments;

for j in   CUR_INV_LINE_DATA(i.invoice_id,i.code_combination_id)  loop
 
  v_cnt_lin:=v_cnt_lin + 1;
   Fnd_File.put_line (Fnd_File.log,i.invoice_id|| ' - ' ||v_cnt_lin);
 /* if v_cnt_lin = 1 then
     Fnd_File.put_line (Fnd_File.output,
                            i.name
                         || CHR (9)
                         || i.je_source
                         || CHR (9)
                         || i.je_category
                         || CHR (9)
                         || i.gl_date
                         || CHR (9)
                         || i.Journal_Description
                         || CHR (9)
                         || i.je_line_num
                         || CHR (9)
                         || i.batch_status
                         || CHR (9)
                         || i.concatenated_segments
                         || CHR (9)
                         || i.document_number
                         || CHR (9)
                         || i.entered_dr
                         || CHR (9)
                         || i.entered_cr
                         || CHR (9)
                         || i.ACCOUNTED_AMT_DR
                         || CHR (9)
                         || i. ACCOUNTED_AMT_CR 
                         || CHR (9)
                         || j.INVOICE_NUM                   
                         || CHR (9)
                         || j.description                 
                         || CHR (9)
                         || j.SEGMENT1     
                         || CHR (9)
                         || j.VENDOR_NAME  
                         || CHR (9)
                         || j.amount   
                         || CHR (9)
                         || j.employee_number    
                         || CHR (9)
                         || j.Employee_name    
                         || CHR (9)
                         || j.advanced_type     
                         || CHR (9)
                         || j.Invoice_no        
                        );
 else
     */                    
  Fnd_File.put_line (Fnd_File.output,
                           ''
                         || CHR (9)
                         || ''
                         || CHR (9)
                         || ''
                         || CHR (9)
                         || ''
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ' '
                         || CHR (9)
                         || ''
                         || CHR (9)
                         || ''   
                         || CHR (9)
                         || ''   
                         || CHR (9)
                         || j.INVOICE_NUM  
                         || CHR (9)
                         || j.description                   
                         || CHR (9)
                         || j.SEGMENT1     
                         || CHR (9)
                         || j.VENDOR_NAME  
                         || CHR (9)
                         || j.amount   
                         || CHR (9)
                         || j.voucher_number   
                         || CHR (9)
                         || j.employee_number    
                         || CHR (9)
                         || j.Employee_name    
                         || CHR (9)
                         || j.advanced_type     
                         || CHR (9)
                         || j.Invoice_no      
                        );
        
 /*end if;
   */                 
end loop; 

end loop;      



--- THIS LOOP WILL PRINT THE DATA FOR OTHER THAN PAYABLES 

FOR K IN CUR_GL_DATA_OTHER(P_FROM_GL_DATE ,
                             P_TO_GL_DATE    )  LOOP


Fnd_File.put_line (Fnd_File.output,
                           k.name
                         || CHR (9)
                         || k.source
                         || CHR (9)
                         || k.category
                         || CHR (9)
                         || k.gl_date
                         || CHR (9)
                         || k.Journal_Description
                         || CHR (9)
                         || k.line_number
                         || CHR (9)
                         || k.batch_status
                         || CHR (9)
                         || k.account_code
                         || CHR (9)
                         || k.document_number
                         || CHR (9)
                         || k.dr_amount
                         || CHR (9)
                         || k.cr_amount
                         || CHR (9)
                         || ''
                         || CHR (9)
                         || ''
                         || CHR (9)
                         || ''
                         || CHR (9)
                         || ''
                         || CHR (9)
                         || to_char(k.cr_amount + k.dr_amount)
                         || CHR (9)
                         || ''
                         || CHR (9)
                         || ''
                         || CHR (9)
                         || ''
                         || CHR (9)
                         || ''
                         || CHR (9)
                         || ''
                        );

END LOOP;

   
        
END GL_OFFSET_EMP_ADV_PRINT_old;                     
  
END XXABRL_GL_OFFSET_EMP_ADV_PKG; 
/


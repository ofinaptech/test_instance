CREATE OR REPLACE PROCEDURE APPS.GL_ACCOUNT_ANALYSIS_REPORT (
     errbuf      OUT      VARCHAR2,
     retcode     OUT      NUMBER,
   form_date   IN   VARCHAR2,
   to_date1    IN   VARCHAR2
)
AS
   v_file              UTL_FILE.file_type;
   g_conc_request_id   NUMBER        := fnd_profile.VALUE ('CONC_REQUEST_ID');
   p_from_run_date     DATE
                     := NVL (fnd_date.canonical_to_date (form_date), SYSDATE);
   p_to_run_date       DATE
                      := NVL (fnd_date.canonical_to_date (to_date1), SYSDATE);

   CURSOR xx_main
   IS
      SELECT aa.batch_name, aa.SOURCE, aa.CATEGORY, aa.gl_date,
             aa.line_number, aa.co, aa.account_code, aa.sbu_desc,
             aa.location_desc, aa.account_desc, aa.dr_amount, aa.cr_amount,
             aa.journal_description, aa.customer_number, aa.customer_name,
             
 --sum(AA.dr_amount),
--sum(AA.cr_amount),
             aa.document_number, aa.batch_status, aa.created_by,
             (SELECT NAME
                FROM hr_operating_units
               WHERE organization_id = aa.org_id) ou_name
        FROM (SELECT GLB.NAME batch_name, glh.je_source SOURCE,
                     glh.je_category CATEGORY,
                     glh.default_effective_date gl_date,
                     gll.je_line_num line_number,
                     gl.concatenated_segments account_code,
                     (SELECT ffvl.description
                        FROM apps.fnd_flex_values_vl ffvl,
                             apps.gl_code_combinations_kfv gl
                       WHERE ffvl.flex_value_set_id = 1013464
                         AND ffvl.flex_value = gl.segment1) co_desc,
                     (SELECT ffvl.description
                        FROM apps.fnd_flex_values_vl ffvl
                       WHERE ffvl.flex_value_set_id = 1013466
                         AND ffvl.flex_value = gl.segment3) sbu_desc,
                     (SELECT ffvl.description
                        FROM apps.fnd_flex_values_vl ffvl
                       WHERE ffvl.flex_value_set_id = 1013467
                         AND ffvl.flex_value = gl.segment4) location_desc,
                     (SELECT ffvl.description
                        FROM apps.fnd_flex_values_vl ffvl
                       WHERE ffvl.flex_value_set_id = 1013469
                         AND ffvl.flex_value = gl.segment6) account_desc,
                     DECODE (xla.accounted_dr,
                             NULL, DECODE (xla.accounted_cr,
                                           NULL, gll.accounted_dr
                                          ),
                             xla.accounted_dr
                            ) dr_amount,
                     DECODE (xla.accounted_cr,
                             NULL, DECODE (xla.accounted_dr,
                                           NULL, gll.accounted_cr
                                          ),
                             xla.accounted_cr
                            ) cr_amount,
                     gll.description journal_description,
                     CASE
                        WHEN UPPER (glh.je_source) =
                                                'PAYABLES'
                           THEN (SELECT po.segment1
                                   FROM apps.po_vendors po
                                  WHERE po.vendor_id =
                                              xla.party_id
                                    AND ROWNUM < 2)
                        WHEN UPPER (glh.je_source) = 'RECEIVABLES'
                           THEN       /*(SELECT  ar.account_number
                                      FROM hz_parties hzp, hz_cust_accounts ar
                                      WHERE ar.party_id = hzp.party_id
                                      AND ar.cust_account_id=xla.party_id
                                      AND ROWNUM<2 ) */
                               DECODE
                                   (glh.je_category,
                                    'Receipts', (SELECT account_number
                                                   FROM apps.hz_cust_accounts_all hca,
                                                        apps.hz_parties hp,
                                                        apps.ar_cash_receipts_all acra,
                                                        xla.xla_transaction_entities xte,
                                                        apps.xla_ae_headers xah
                                                  WHERE hca.cust_account_id =
                                                           acra.pay_from_customer
                                                    AND hca.party_id =
                                                                   hp.party_id
                                                    AND acra.cash_receipt_id =
                                                           xte.source_id_int_1
                                                    AND xah.entity_id =
                                                                 xte.entity_id
                                                    AND xah.ae_header_id =
                                                              xla.ae_header_id),
                                    (SELECT ar.account_number
                                       FROM apps.hz_parties hzp,
                                            apps.hz_cust_accounts ar
                                      WHERE ar.party_id = hzp.party_id
                                        AND ar.cust_account_id = xla.party_id
                                        AND ROWNUM < 2)
                                   )
                     END AS customer_number,
                     CASE
                        WHEN UPPER (glh.je_source) =
                                                  'PAYABLES'
                           THEN (SELECT po.vendor_name
                                   FROM apps.po_vendors po
                                  WHERE po.vendor_id =
                                                xla.party_id)
                        WHEN UPPER (glh.je_source) =
                                               'RECEIVABLES'
                           THEN
                               /*(SELECT  hzp.party_name
                               FROM hz_parties hzp, hz_cust_accounts ar
                               WHERE ar.party_id = hzp.party_id
                               AND ar.cust_account_id=xla.party_id
                               )*/
                               DECODE
                                  (glh.je_category,
                                   'Receipts', (SELECT hp.party_name
                                                  FROM apps.hz_cust_accounts_all hca,
                                                       apps.hz_parties hp,
                                                       apps.ar_cash_receipts_all acra,
                                                       xla.xla_transaction_entities xte,
                                                       apps.xla_ae_headers xah
                                                 WHERE hca.cust_account_id =
                                                          acra.pay_from_customer
                                                   AND hca.party_id =
                                                                   hp.party_id
                                                   AND acra.cash_receipt_id =
                                                           xte.source_id_int_1
                                                   AND xah.entity_id =
                                                                 xte.entity_id
                                                   AND xah.ae_header_id =
                                                              xla.ae_header_id),
                                   (SELECT hzp.party_name
                                      FROM apps.hz_parties hzp,
                                           apps.hz_cust_accounts ar
                                     WHERE ar.party_id = hzp.party_id
                                       AND ar.cust_account_id = xla.party_id)
                                  )
                     END AS customer_name,
                     
                       /*CASE WHEN UPPER(glh.je_source)='PAYABLES'
                           THEN (SELECT po.segment1
                                FROM po_vendors po
                                WHERE po.VENDOR_ID=xla.PARTY_ID
                                AND ROWNUM<2)
                           WHEN UPPER(glh.je_source)='RECEIVABLES'
                           THEN (SELECT  ar.account_number
                                FROM hz_parties hzp, hz_cust_accounts ar
                                WHERE ar.party_id = hzp.party_id
                                AND ar.cust_account_id=xla.party_id
                                AND ROWNUM<2 )
                       END AS Customer_Number,
                       CASE WHEN UPPER(glh.je_source)='PAYABLES'
                           THEN (SELECT po.vendor_name
                                FROM po_vendors po
                                WHERE po.VENDOR_ID=xla.PARTY_ID
                                )
                           WHEN UPPER(glh.je_source)='RECEIVABLES'
                           THEN (SELECT  hzp.party_name
                                FROM hz_parties hzp, hz_cust_accounts ar
                                WHERE ar.party_id = hzp.party_id
                                AND ar.cust_account_id=xla.party_id
                                )
                        END AS Customer_Name,*/
                     /* NVL(glir.SUBLEDGER_DOC_SEQUENCE_VALUE,glh.doc_sequence_value) document_number,*/
                     /*
                             decode( glh.je_category,'Credit Memos',(select distinct rac.DOC_SEQUENCE_VALUE
                                                   from
                                                   XLA_AE_HEADERS xxla,
                                                   RA_CUST_TRX_LINE_GL_DIST_ALL cust,
                                                   RA_CUSTOMER_TRX_ALL rac
                                                   where
                                                   xxla.EVENT_ID=cust.EVENT_ID and
                                                   cust.CUSTOMER_TRX_ID=rac.CUSTOMER_TRX_ID and
                                                   cust.ORG_ID=rac.ORG_ID and
                                                   xxla.AE_HEADER_ID=xla.AE_HEADER_ID),
                                             'Purchase Invoices',(SELECT XXLA.DOC_SEQUENCE_VALUE FROM
                                                                       XLA_AE_HEADERS xxla
                                                                       WHERE
                                                                       XXLA.AE_HEADER_ID=XLA.AE_HEADER_ID )
                                             ,nvl(glir.SUBLEDGER_DOC_SEQUENCE_VALUE,glh.doc_sequence_value)) document_number,
                             */
                     NVL
                        (DECODE
                             (glh.je_category,
                              'Credit Memos', (SELECT DISTINCT rac.doc_sequence_value
                                                          FROM apps.xla_ae_headers xxla,
                                                               apps.ra_cust_trx_line_gl_dist_all cust,
                                                               apps.ra_customer_trx_all rac
                                                         WHERE xxla.event_id =
                                                                  cust.event_id
                                                           AND cust.customer_trx_id =
                                                                  rac.customer_trx_id
                                                           AND cust.org_id =
                                                                    rac.org_id
                                                           AND xxla.ae_header_id =
                                                                  xla.ae_header_id),
                              (SELECT xxla.doc_sequence_value
                                 FROM apps.xla_ae_headers xxla
                                WHERE xxla.ae_header_id = xla.ae_header_id)
                             ),
                         NVL (glir.subledger_doc_sequence_value,
                              glh.doc_sequence_value
                             )
                        ) document_number,
                     DECODE (GLB.status,
                             'P', 'Posted',
                             'U', 'Unposted',
                             GLB.status
                            ) batch_status,
                     NULL created_by, gl.segment1 co, gl.segment3 sbu,
                     gl.segment4 LOCATION, gl.segment6 gl_account,
                     glh.ledger_id ledger_id, gl.code_combination_id,
                     xla.created_by created_byxla,
                     gll.created_by created_bygll,
                     (SELECT DISTINCT security_id_int_1
                                 FROM xla.xla_transaction_entities xlat,
                                      apps.xla_ae_headers xlah
                                WHERE xlat.application_id = xla.application_id
                                  AND xlat.entity_id = xlah.entity_id
                                  AND xlah.ae_header_id = xla.ae_header_id)
                                                                       org_id
                FROM apps.gl_je_headers glh,
                     apps.gl_je_lines gll,
                     apps.gl_code_combinations_kfv gl,
                     apps.gl_je_batches GLB,
                     apps.xla_ae_lines xla,
                     --    xla_ae_headers xlah,
                     apps.gl_import_references glir
               WHERE glh.je_header_id = gll.je_header_id
                 AND gll.je_header_id = glir.je_header_id(+)
                 AND gll.je_line_num = glir.je_line_num(+)
                 AND glir.gl_sl_link_id = xla.gl_sl_link_id(+)
                 AND glir.gl_sl_link_table = xla.gl_sl_link_table(+)
--       AND SUBSTR(GL.SEGMENT6,1,1) = '4'                            --    13TH NOVEMBER 2014
                 AND gl.code_combination_id = gll.code_combination_id
--  and xla.AE_HEADER_ID=xlah.AE_HEADER_ID
                 AND glh.je_batch_id = GLB.je_batch_id
                 AND UPPER (glh.je_source) IN ('PAYABLES', 'RECEIVABLES')
                 AND NVL (xla.accounted_cr, 999999) != 0
                 AND NVL (xla.accounted_dr, 999999) != 0
              UNION ALL
              SELECT GLB.NAME batch_name, glh.je_source SOURCE,
                     glh.je_category CATEGORY,
                     glh.default_effective_date gl_date,
                     gll.je_line_num line_number,
                     gl.concatenated_segments account_code,
                     (SELECT ffvl.description
                        FROM apps.fnd_flex_values_vl ffvl,
                             apps.gl_code_combinations_kfv gl
                       WHERE ffvl.flex_value_set_id = 1013464
                         AND ffvl.flex_value = gl.segment1) co_desc,
                     (SELECT ffvl.description
                        FROM apps.fnd_flex_values_vl ffvl
                       WHERE ffvl.flex_value_set_id = 1013466
                         AND ffvl.flex_value = gl.segment3) sbu_desc,
                     (SELECT ffvl.description
                        FROM apps.fnd_flex_values_vl ffvl
                       WHERE ffvl.flex_value_set_id = 1013467
                         AND ffvl.flex_value = gl.segment4) location_desc,
                     (SELECT ffvl.description
                        FROM apps.fnd_flex_values_vl ffvl
                       WHERE ffvl.flex_value_set_id = 1013469
                         AND ffvl.flex_value = gl.segment6) account_desc,
                     gll.accounted_dr dr_amount, gll.accounted_cr cr_amount,
                     gll.description journal_description, NULL customer_name,
                     NULL customer_number,
                     glh.doc_sequence_value document_number,
                     DECODE (GLB.status,
                             'P', 'Posted',
                             'U', 'Unposted',
                             GLB.status
                            ) batch_status,
                     fu.user_name created_by, gl.segment1 co, gl.segment3 sbu,
                     gl.segment4 LOCATION, gl.segment6 gl_account,
                     glh.ledger_id ledger_id, NULL, NULL, NULL, NULL
                FROM apps.gl_je_headers glh,
                     apps.gl_je_lines gll,
                     apps.gl_code_combinations_kfv gl,
                     apps.fnd_user fu,
                     apps.gl_je_batches GLB
               WHERE glh.je_header_id = gll.je_header_id
--       AND SUBSTR(GL.SEGMENT6,1,1) = '4'                --    13TH NOVEMBER 2014
                 AND gl.code_combination_id = gll.code_combination_id
                 AND glh.je_batch_id = GLB.je_batch_id
                 AND gll.created_by = fu.user_id
                 AND UPPER (glh.je_source) NOT IN ('PAYABLES', 'RECEIVABLES')) aa
       WHERE 1 = 1
--AND        aa.ledger_id = 2101
         AND TO_DATE (TO_CHAR (aa.gl_date, 'DD-MON-YY')) BETWEEN p_from_run_date
                                                             AND p_to_run_date
--    AND aa.co = '31'                             -- parameter
--    AND aa.sbu IN ('751', '752')                         -- parameter
         AND aa.gl_account = '362001';                            -- parameter
--    AND AA.CUSTOMER_NUMBER IN ('9706559','9706560','9706561','9706587','9701320')
--    group by AA.Customer_Number,AA.Customer_Name,AA.document_number
      --   AND ROWNUM = 1;                                          -- parameter
--    AND AA.CUSTOMER_NUMBER IN ('9706559','9706560','9706561','9706587','9701320')
--    group by AA.Customer_Number,AA.Customer_Name,AA.document_number
BEGIN
   v_file :=
      UTL_FILE.fopen ('DATA_PUMP',
                         'GL_ACCOUNT_ANALYSIS_REPORT'
                      || g_conc_request_id
                      || '.csv',
                      'w'
                     );
   UTL_FILE.put_line (v_file,
                         'BATCH_NAME'
                      || '^'
                      || 'SOURCE'
                      || '^'
                      || 'CATEGORY'
                      || '^'
                      || 'GL_DATE'
                      || '^'
                      || 'LINE_NUMBER'
                      || '^'
                      || 'CO'
                      || '^'
                      || 'ACCOUNT_CODE'
                      || '^'
                      || 'SBU_DESC'
                      || '^'
                      || 'LOCATION_DESC'
                      || '^'
                      || 'ACCOUNT_DESC'
                      || '^'
                      || 'DR_AMOUNT'
                      || '^'
                      || 'CR_AMOUNT'
                      || '^'
                      || 'JOURNAL_DESCRIPTION'
                      || '^'
                      || 'CUSTOMER_NUMBER'
                      || '^'
                      || 'CUSTOMER_NAME'
                      || '^'
                      || 'DOCUMENT_NUMBER'
                      || '^'
                      || 'BATCH_STATUS'
                      || '^'
                      || 'CREATED_BY'
                      || '^'
                      || 'OU_NAME'
                      || '^'
                     );

   FOR xx_rec IN xx_main
   LOOP
      UTL_FILE.put_line (v_file,
                            xx_rec.batch_name
                         || '^'
                         || xx_rec.SOURCE
                         || '^'
                         || xx_rec.CATEGORY
                         || '^'
                         || xx_rec.gl_date
                         || '^'
                         || xx_rec.line_number
                         || '^'
                         || xx_rec.co
                         || '^'
                         || xx_rec.account_code
                         || '^'
                         || xx_rec.sbu_desc
                         || '^'
                         || xx_rec.location_desc
                         || '^'
                         || xx_rec.account_desc
                         || '^'
                         || xx_rec.dr_amount
                         || '^'
                         || xx_rec.cr_amount
                         || '^'
                         || xx_rec.journal_description
                         || '^'
                         || xx_rec.customer_number
                         || '^'
                         || xx_rec.customer_name
                         || '^'
                         || xx_rec.document_number
                         || '^'
                         || xx_rec.batch_status
                         || '^'
                         || xx_rec.created_by
                         || '^'
                         || xx_rec.ou_name
                         || '^'
                        );
   END LOOP;

   UTL_FILE.fclose (v_file);
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      UTL_FILE.fclose (v_file);
      fnd_file.put_line (fnd_file.LOG, 'No Data Found');
   WHEN UTL_FILE.invalid_path
   THEN
      UTL_FILE.fclose (v_file);
      fnd_file.put_line (fnd_file.LOG, 'Invalid Path');
   WHEN UTL_FILE.invalid_filehandle
   THEN
      UTL_FILE.fclose (v_file);
      fnd_file.put_line (fnd_file.LOG, 'Invalid File Handle');
   WHEN UTL_FILE.invalid_operation
   THEN
      UTL_FILE.fclose (v_file);
      fnd_file.put_line (fnd_file.LOG, 'Invalid Operation');
   WHEN UTL_FILE.read_error
   THEN
      UTL_FILE.fclose (v_file);
      fnd_file.put_line (fnd_file.LOG, 'Read Error');
   WHEN UTL_FILE.write_error
   THEN
      UTL_FILE.fclose (v_file);
      fnd_file.put_line (fnd_file.LOG, 'Write Error');
   WHEN UTL_FILE.internal_error
   THEN
      UTL_FILE.fclose (v_file);
      fnd_file.put_line (fnd_file.LOG, 'Internal Error');
   WHEN UTL_FILE.file_open
   THEN
      UTL_FILE.fclose (v_file);
      fnd_file.put_line (fnd_file.LOG, 'File is Open');
   WHEN UTL_FILE.invalid_filename
   THEN
      UTL_FILE.fclose (v_file);
      fnd_file.put_line (fnd_file.LOG, 'Invalid File Name');
   WHEN OTHERS
   THEN
      UTL_FILE.fclose (v_file);
      fnd_file.put_line (fnd_file.LOG, 'Unknown Error' || SQLERRM);
      UTL_FILE.fclose (v_file);
END; 
/


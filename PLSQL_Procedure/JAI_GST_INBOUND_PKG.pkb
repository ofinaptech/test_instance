CREATE OR REPLACE PACKAGE body APPS.JAI_GST_INBOUND_PKG AS
  /* $Header: jai_gst_inbound_pkg.plb 120.0.12010000.15 2020/09/30 08:32:15 ppluna noship $ */
  g_fetch_limit   NUMBER := 10000;

--TYPE INBD_TBL_TYP IS TABLE OF JAI_GST_REPT_INBOUND_INTERFACE%rowtype;
   FUNCTION get_hsn_code_id (l_hsn_code IN VARCHAR2)
      RETURN VARCHAR2
   IS
      CURSOR hsncodeid
      IS
         SELECT reporting_code_id
           FROM jai_reporting_codes
          WHERE reporting_code = l_hsn_code;

      l_hsncode_id   NUMBER;
   BEGIN
      OPEN hsncodeid;

      FETCH hsncodeid
       INTO l_hsncode_id;

      CLOSE hsncodeid;

      RETURN l_hsncode_id;
   END;

   PROCEDURE jai_process_inbound_main (
      errbuff                 OUT NOCOPY      VARCHAR2,
      retcode                 OUT NOCOPY      VARCHAR2,
      p_file_id               IN              NUMBER DEFAULT NULL,
      p_file_name             IN              VARCHAR2 DEFAULT NULL,
      p_form_type             IN              VARCHAR2 DEFAULT NULL,
      p_tax_regime            IN              VARCHAR2,
      p_first_party_reg_num   IN              VARCHAR2,
      p_return_period         IN              VARCHAR2,
      p_section               IN              VARCHAR2 DEFAULT NULL
   )
   IS
      lr_par_rec           inb_param_rec_type;
      lv_process_status    VARCHAR2 (100);
      lv_process_message   VARCHAR2 (2000);
      l_api_name           VARCHAR2 (1000)
                            := 'jai_gst_inbound_pkg.jai_process_inbound_main';
      ln_request_id        NUMBER             := fnd_global.conc_request_id;
   BEGIN
      jai_debug (p_api_name      => l_api_name,
                 p_msg_type      => 'DEBUG',
                 p_msg_desc      => 'Assign parameter record'
                );
      lr_par_rec.first_party_reg_num := p_first_party_reg_num;
      lr_par_rec.return_period := p_return_period;
      lr_par_rec.file_id := p_file_id;
      lr_par_rec.form_type := p_form_type;
      lr_par_rec.file_name := p_file_name;
      jai_debug (p_api_name      => l_api_name,
                 p_msg_type      => 'DEBUG',
                 p_msg_desc      =>    'before invoking JAI_PROCESS_INBOUND '
                                    || ' FIRST_PARTY_REG_NUM '
                                    || lr_par_rec.first_party_reg_num
                                    || ' RETURN_PERIOD '
                                    || lr_par_rec.return_period
                                    || ' File ID '
                                    || lr_par_rec.file_id
                                    || ' FORM_TYPE '
                                    || lr_par_rec.form_type
                                    || ' FILE_NAME '
                                    || lr_par_rec.file_name
                );

      /*
         if file is not null then
                              < Ensure the file id is mapped to jai_gst_rept_inbound_interface.batch id
                                 if p_form_type ='ANX1-ERROR'
                                   JAI_PROCESS_ANX1 ( Separate local procedure considering maintenance )
                                    <Convert json and Insert jai_gst_rept_inbound_interface
                                  elsif  p_form_type in ('ANX2-INBOUND','ANX2-ERROR') then
                                      jai_process_anx2(Separate local procedure considering maintenance )
                                       <Convert json and Insert into jai_gst_rept_inbound_interface >
                                   elsif p_form_Type in ('IRN-INBOUND','IRN-ERROR')
                                        jai_jsontotbl_IRN(Separate local procedure considering maintenance )
                                       <Convert json and Insert into jai_gst_rept_inbound_interface >

                                   end if;
                             end if;

                                jai_repository_update_pvt(p_File_id in default null
                                      p_first_party in default null
                                      p_Period in default null
                                      p_SECTION IN default null
                                      p_tax_regime in default null
                                      P_tax_invnum in default null )

                             */

      --start additions by vkaranam for IRN inbound
      IF p_form_type IN ('IRN-INBOUND', 'IRN-ERROR')
      THEN
         jai_apex_json_pkg.jai_jsontotbl_irn (p_file_id              => p_file_id,
                            p_form_type            => p_form_type,
                            p_request_id           => ln_request_id,
                            p_process_status       => lv_process_status,
                            p_process_message      => lv_process_message
                           );
         jai_debug
            (p_api_name      => l_api_name,
             p_msg_type      => 'DEBUG',
             p_msg_desc      =>    'afeter invoking jai_jsontotbl_irn lv_process_status'
                                || lv_process_status
                                || ' process message '
                                || lv_process_message
            );
      END IF;
/*commented by vkaranam for bug#31849515
      IF p_form_type IN ('ANX1-ERROR')
      THEN
          jai_apex_json_pkg.jai_jsontotbl_anx1 (p_file_id              => p_file_id,
                            p_form_type            => p_form_type,
                            p_request_id           => ln_request_id,
                            p_process_status       => lv_process_status,
                            p_process_message      => lv_process_message
                           );
      END IF;

      IF p_form_type IN ('ANX2-INBOUND', 'ANX2-ERROR')
      THEN
         jai_process_anx2 (p_param_rec_type       => lr_par_rec,
                           p_call_from            => l_api_name,
                           p_process_status       => lv_process_status,
                           p_process_message      => lv_process_message
                          );
      END IF;
	  */
	  jai_return_repository_update(
     	   p_file_id        ,
    p_form_type       ,
	 p_first_party_reg_num ,
      p_return_period      ,
      p_section      ,
      'JAI_PROCESS_INBOUND_MAIN',
      p_request_id     ,
      lv_process_status  ,
      lv_process_message
   );

      jai_debug
         (p_api_name      => l_api_name,
          p_msg_type      => 'DEBUG',
          p_msg_desc      =>    'afeter invoking JAI_PROCESS_INBOUND lv_process_status'
                             || lv_process_status
                             || ' process message '
                             || lv_process_message
         );
      errbuff := 'request submitted';
      retcode := 0;
   EXCEPTION
      WHEN OTHERS
      THEN
         jai_debug
              (p_api_name      => l_api_name,
               p_msg_type      => 'ERROR',
               p_msg_desc      =>    'ERROR JAI_PROCESS_INBOUND lv_process_status'
                                  || lv_process_status
                                  || ' process message '
                                  || lv_process_message
              );
         lv_process_status := 'E';
         lv_process_message := SQLERRM;
         fnd_file.put_line (fnd_file.output, lv_process_message);
         errbuff := '-->: ' || lv_process_message;
         retcode := '2';
   END jai_process_inbound_main;

   PROCEDURE jai_gst_process_recovery (
      errbuff         OUT NOCOPY      VARCHAR2,
      retcode         OUT NOCOPY      VARCHAR2,
      p_batch_id      IN              NUMBER DEFAULT NULL,
      p_form_type     IN              VARCHAR2 DEFAULT NULL,
      p_first_party   IN              VARCHAR2 DEFAULT NULL,
      p_period        IN              VARCHAR2 DEFAULT NULL,
      p_section       IN              VARCHAR2 DEFAULT NULL,
      p_tax_regime    IN              VARCHAR2 DEFAULT NULL,
      p_tax_invnum    IN              VARCHAR2 DEFAULT NULL
   )
   IS
   BEGIN
      --jai_gst_inbound_pkg.jai_anx2_action
      NULL;
   END jai_gst_process_recovery;


      /*======================================================================+
   | FILE HANDLING PROCEDURES AND FUNCTIONS                                |
   +=======================================================================*/
   PROCEDURE create_db_dir (
      p_dir_name   IN OUT NOCOPY   VARCHAR2,
      p_dir_path   IN              VARCHAR2
   )
   IS
   BEGIN
      NULL;
   END create_db_dir;

   FUNCTION jai_file_to_clob (
      p_file_path   IN   VARCHAR2,
      p_filename    IN   VARCHAR2,
      p_src_csid    IN   NUMBER := DBMS_LOB.default_csid
   )
      RETURN CLOB
   AS
   BEGIN
      NULL;
   END jai_file_to_clob;

   PROCEDURE jai_load_jason_file (
      p_file_id                NUMBER,
      p_file_type     IN       VARCHAR2,
      p_status     in   OUT   nocopy   VARCHAR2,
      p_process_msg  in  OUT  nocopy    VARCHAR2
   )
   IS
      CURSOR c1
      IS
         SELECT *
           FROM fnd_lobs
          WHERE file_id = p_file_id;

      lr_par_rec           jai_gst_inbound_pkg.inb_param_rec_type;
      lv_process_status    VARCHAR2 (100);
      lv_process_message   VARCHAR2 (100);
      v_file_clob          CLOB;
      v_file_size          INTEGER                     := DBMS_LOB.lobmaxsize;
      v_dest_offset        INTEGER                                := 1;
      v_src_offset         INTEGER                                := 1;
      v_blob_csid          NUMBER                    := DBMS_LOB.default_csid;
      v_lang_context       NUMBER                := DBMS_LOB.default_lang_ctx;
      v_warning            INTEGER;
      v_length             NUMBER;
      l_file_name          VARCHAR2 (250);
   BEGIN
      l_file_name := '';

      FOR i IN c1
      LOOP
         SELECT REPLACE (DECODE (INSTR (i.file_name, '/'),
                                 0, i.file_name,
                                 SUBSTR (i.file_name,
                                         INSTR (i.file_name, '/') + 1
                                        )
                                ),
                         ' ',
                         '_'
                        )
           INTO l_file_name
           FROM DUAL;

         DBMS_LOB.createtemporary (v_file_clob, TRUE);
         DBMS_LOB.converttoclob (v_file_clob,
                                 i.file_data,
                                 v_file_size,
                                 v_dest_offset,
                                 v_src_offset,
                                 v_blob_csid,
                                 v_lang_context,
                                 v_warning
                                );

         INSERT INTO jai_lobs
                     (file_id, file_data, file_name, file_type,
                      file_status, process_msg
                     )
              VALUES (i.file_id, v_file_clob, i.file_name, p_file_type,
                      'Success', ''
                     );

         p_status := 'Upload Success';
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         fnd_file.put_line (fnd_file.LOG,
                               'Jason File Upload Err file_name =>'
                            || l_file_name
                            || 'Err Msg => '
                            || SQLERRM
                           );
         p_status := 'Upload Failed';
         p_process_msg :=
               'Jason File Upload Err file_name =>'
            || l_file_name
            || 'Err Msg => '
            || SQLERRM;
   END jai_load_jason_file;

   PROCEDURE jai_debug (
      p_api_name   IN   VARCHAR2,
      p_msg_type   IN   VARCHAR2,
      p_msg_desc   IN   VARCHAR2
   )
   IS
   BEGIN
      g_current_runtime_level := fnd_log.g_current_runtime_level;
    IF UPPER (p_msg_type) IN ('LOG')
         THEN
      fnd_file.put_line (fnd_file.LOG, p_msg_type || ' For==>' || p_msg_desc);

      END IF;
      IF (g_level_procedure >= g_current_runtime_level)
      THEN
         fnd_log.STRING (g_level_procedure,
                         g_module_name || p_api_name,
                         p_msg_type || p_msg_desc
                        );
      END IF;
   END jai_debug;

   PROCEDURE jai_repository_update_pvt (
      p_inbd_tbl                          inbd_tbl_typ,
      p_call_from         IN              VARCHAR2 DEFAULT NULL,
      p_request_id        IN              NUMBER DEFAULT NULL,
      p_process_status    OUT NOCOPY      VARCHAR2,
      p_process_message   OUT NOCOPY      VARCHAR2
   )
   IS
      l_api_name           VARCHAR2 (100)  := 'JAI_REPOSITORY_UPDATE_PVT';
      lv_process_message   VARCHAR2 (1000);
      lv_process_message   VARCHAR2 (100);
   BEGIN
      FORALL i IN p_inbd_tbl.FIRST .. p_inbd_tbl.LAST SAVE EXCEPTIONS
         UPDATE jai_tax_lines jtx
            SET reporting_status_flag = (p_inbd_tbl (i).gstn_acck_status),
                gstn_acck_status = p_inbd_tbl (i).gstn_acck_status,
                --    gstn_payload_id = tl_get_inbd_dtls(i).gstn_acck_id,
                match_status_flag = p_inbd_tbl (i).match_status_flag,
                action_flag = p_inbd_tbl (i).action_flag,
                last_update_date = SYSDATE,
                last_updated_by = p_request_id,
                record_type_code = 'GSTIN_INB_UPDT'
          WHERE jtx.trx_id = p_inbd_tbl (i).trx_id
            AND jtx.tax_invoice_num =
                   NVL (p_inbd_tbl (i).tax_invoice_number,
                        jtx.tax_invoice_num)
            AND EXISTS (
                   SELECT 1
                     FROM jai_tax_det_factors jdt
                    WHERE jdt.det_factor_id = jtx.det_factor_id
                      AND jdt.tax_invoice_num =
                             NVL (p_inbd_tbl (i).tax_invoice_number,
                                  jtx.tax_invoice_num
                                 )
                      AND NVL (jdt.hsn_code_id, 0) =
                             NVL (p_inbd_tbl (i).hsn_code_id,
                                  NVL (jdt.hsn_code_id, 0)
                                 ));
      jai_debug
              (p_api_name      => l_api_name,
               p_msg_type      => 'Debug',
               p_msg_desc      =>    ' Number of records  updated in jai_tax_LINES'
                                  || SQL%ROWCOUNT
              );
      FORALL i IN p_inbd_tbl.FIRST .. p_inbd_tbl.LAST SAVE EXCEPTIONS
         UPDATE jai_tax_det_factors jtd
            SET reporting_status_flag = (p_inbd_tbl (i).gstn_acck_status),
                gstn_acck_status = p_inbd_tbl (i).gstn_acck_status,
                -- gstn_payload_id = P_INBD_TBL(i).gstn_acck_id,
                match_status_flag = p_inbd_tbl (i).match_status_flag,
                -- action_flag=P_INBD_TBL(i).action_flag,
                last_update_date = SYSDATE,
                last_updated_by = p_request_id,
                record_type_code = 'GSTIN_INB_UPDT'
          WHERE jtd.trx_id = p_inbd_tbl (i).trx_id
            AND jtd.tax_invoice_num =
                   NVL (p_inbd_tbl (i).tax_invoice_number,
                        jtd.tax_invoice_num)
            AND NVL (jtd.hsn_code_id, 0) =
                    NVL (p_inbd_tbl (i).hsn_code_id, NVL (jtd.hsn_code_id, 0));
      jai_debug
         (p_api_name      => l_api_name,
          p_msg_type      => 'Debug',
          p_msg_desc      =>    ' Number of records  updated in jai_tax_det_factors'
                             || SQL%ROWCOUNT
         );
      FORALL i IN p_inbd_tbl.FIRST .. p_inbd_tbl.LAST SAVE EXCEPTIONS
         UPDATE jai_gst_return_summary jrs
            SET reporting_status_flag = (p_inbd_tbl (i).gstn_acck_status),
                response_code = (p_inbd_tbl (i).ERROR_CODE),
                response_desc = (p_inbd_tbl (i).error_desc),
                remarks = (p_inbd_tbl (i).remarks),
                --gstn_acck_status=P_INBD_TBL(i).gstn_acck_status,
                gstn_payload_id = p_inbd_tbl (i).gstn_acck_id,
                match_status_flag = p_inbd_tbl (i).match_status_flag,
                action_flag = p_inbd_tbl (i).action_flag,
                last_update_date = SYSDATE,
                last_updated_by = p_request_id,
                record_type_code = 'GSTIN_INB_UPDT'
          WHERE jrs.trx_id = p_inbd_tbl (i).trx_id
            AND jrs.tax_invoice_number =
                   NVL (p_inbd_tbl (i).tax_invoice_number,
                        jrs.tax_invoice_number
                       )
            AND NVL (jrs.hsn_code_id, 0) =
                    NVL (p_inbd_tbl (i).hsn_code_id, NVL (jrs.hsn_code_id, 0));
      jai_debug
         (p_api_name      => l_api_name,
          p_msg_type      => 'Debug',
          p_msg_desc      =>    ' Number of records  updated in jai_gst_return_summary'
                             || SQL%ROWCOUNT
         );
         /*
          jai_debug (p_api_name      => l_api_name,
                                    p_msg_type        => 'Debug',
                                    P_MSG_DESC         =>     ' before update jai_tax_lines'||
                                                              ' p_reporting_status_flag '||p_reporting_status_flag
                                                              ||'p_gstn_acck_status -> '||p_gstn_acck_status
                                                              ||'p_gstn_acck_id-> '|| p_gstn_acck_id
                                                              ||'p_match_status_flag-> '|| p_match_status_flag
                                                              ||'p_action_flag-> '|| p_action_flag
                                                              ||'p_tax_invoice_number-> '|| p_tax_invoice_number
                                                              ||' p_batch_id '||p_batch_id
                                                              ||' p_hsn_code_id '||p_hsn_code_id
                     );

      */

      --update to jai_rgm_recovery_lines will be done with RCM
      p_process_status := 'S';
      p_process_message := 'Repository Update is Succesfull';
   EXCEPTION
      WHEN OTHERS
      THEN
         jai_debug (p_api_name      => l_api_name,
                    p_msg_type      => 'Debug',
                    p_msg_desc      =>    ' Error while updating repository '
                                       || SQLERRM
                   );
         p_process_status := 'E';
         p_process_message := '--> in Repository Update ' || SQLERRM;
         fnd_file.put_line (fnd_file.output, p_process_message);
         app_exception.raise_exception;
   END;


/*===========================================================================+
  | PROCEDURE                                                                 |
  |   JAI_PROCESS_ANX2                                                              |
  |                                                                           |
  | DESCRIPTION                                                               |
  |    Invoked from jai_process_inbound  if form_type is ANX_2               |
  |    1) update the error information if any ,action info from jai_gst_inbound_interface    |
  |    table to Tax repository of ANX-2 details.                               |
  |    2) Invoke  JAI_ANX2_ACTION for processing the recovery.                 |
    | SCOPE - Public                                                             |
  |                                                                            |
  | NOTES                                                                      |
  |                                                                            |
  | MODIFICATION HISTORY                                                       |
  |                                                                            |
  +===========================================================================*/
   PROCEDURE jai_process_anx2 (
      p_param_rec_type                    inb_param_rec_type,
      p_call_from         IN              VARCHAR2 DEFAULT NULL,
      p_process_status    OUT NOCOPY      VARCHAR2,
      p_process_message   OUT NOCOPY      VARCHAR2
   )
   AS
     ln_1 number;
   BEGIN
null;
  END jai_process_anx2;

/*===========================================================================+
  | PROCEDURE                                                                 |
  |   JAI_PROCESS_ANX2                                                              |
  |                                                                           |
  | DESCRIPTION                                                               |
  |    Invoked from jai_process_inbound  if form_type is ANX_2               |
  |    1) update the error information if any ,action info from jai_gst_inbound_interface    |
  |    table to Tax repository of ANX-2 details.                               |
  |    2) Invoke  JAI_ANX2_ACTION for processing the recovery.                 |
    | SCOPE - Public                                                             |
  |                                                                            |
  | NOTES                                                                      |
  |                                                                            |
  | MODIFICATION HISTORY                                                       |
  |                                                                            |
  +===========================================================================*/
   PROCEDURE jai_anx2_action (
      p_param_rec_type                          inb_param_rec_type,
      p_call_from               IN              VARCHAR2 DEFAULT NULL,
      p_hsn_code_id             IN              NUMBER DEFAULT NULL,
      p_match_status            IN              VARCHAR2 DEFAULT NULL,
      p_action_flag             IN              VARCHAR2 DEFAULT NULL,
      p_reporting_status_flag   IN              VARCHAR2,
      p_gstn_acck_status        IN              VARCHAR2,
      p_gstn_acck_id            IN              NUMBER DEFAULT NULL,
      p_process_status          OUT NOCOPY      VARCHAR2,
      p_process_message         OUT NOCOPY      VARCHAR2
   )
   AS
      lv_test               VARCHAR2 (1);

      CURSOR get_recovery_elg
      IS
         SELECT jtl.*
           FROM jai_tax_lines jtl
          WHERE jtl.application_id = 200
            AND jtl.entity_code = 'AP_INVOICES'
            AND jtl.trx_type IN ('STANDARD', 'DEBIT')
            AND trx_id IN (
                   SELECT trx_id
                     FROM jai_gst_return_summary jrs
                    WHERE jrs.first_party_primary_reg_num =
                                          p_param_rec_type.first_party_reg_num
--AND jtx.org_id = NVL (P_operating_unit_id, jtx.org_id)
                      AND jrs.period_name = p_param_rec_type.return_period
                      AND jrs.form_type = p_param_rec_type.form_type
                      AND last_updated_by = p_request_id)
            AND jtl.first_party_primary_reg_num =
                                          p_param_rec_type.first_party_reg_num
            AND jtl.recoverable_flag = 'Y'
            AND NVL (jtl.rec_tax_amt_funcl_curr, 0) <> 0
            AND NVL (jtl.self_assessed_flag, 'N') = 'N'
            AND last_updated_by = p_request_id;

      TYPE rc IS REF CURSOR;

      lc_cursor             rc;
      lr_tax_line           jai_tax_lines%ROWTYPE;
      lr_doc_type           jai_rgm_recovery_pkg.doc_type_rec;
      lv_process_status     VARCHAR2 (50);
      lv_process_message    VARCHAR2 (2000);
      l_api_name   CONSTANT VARCHAR2 (30)                 := 'JAI_ANX2_ACTION';

      --28351481
      CURSOR get_det_fact (cp_det_factor_id NUMBER)
      IS
         SELECT *
           FROM jai_tax_det_factors
          WHERE det_factor_id = cp_det_factor_id;

      lr_det_factors        jai_tax_det_factors%ROWTYPE;
      lv_where              VARCHAR2 (2000);
   BEGIN
/*Case 1: Exact Match,Recoverable in GSTN,ERP,Accepted
1) loop through jai_ with reporting_status_flag as "UG_S'
gstin,tax_invoice_date falling between p_taxinv_from_date,p_taxinv_to_date.
2) if p_match_result='EXACT_MATCH' and P_ACTION_FLAG='ACCEPT'
if p_itc_entitled_flag='Y' then
--process the recovery for recoverable taxes
process_recovery;
end if;
*/

      /* fetch the  records which are eligible for recovery*/
      lv_where :=
            'select * from jai_tax_lines jtl
             where jtl.application_id=200
           and jtl.entity_code=''AP_INVOICES''
 AND jtl.trx_type in (''STANDARD'',''DEBIT'')
  and trx_id in (select trx_id from jai_gst_return_summary jrs
where jii.first_party_primary_reg_num = '''
         || p_param_rec_type.first_party_reg_num
         || '''
--AND jtx.org_id = NVL (P_operating_unit_id, jtx.org_id)
 AND jii.period_name='''
         || p_param_rec_type.return_period
         || '''
 and jii.form_type='''
         || p_param_rec_type.form_type
         || '''
 and last_updated_by='''
         || p_request_id
         || '''
 '')''
 AND jtl.first_party_primary_reg_num='''
         || p_param_rec_type.first_party_reg_num
         || '''
 And jtl.recoverable_flag=''Y'' AND nvl(jtl.rec_tax_amt_funcl_curr,0)<>0
 AND JTL.ACTION_FLAG=''A''
 AND nvl(jtl.self_assessed_flag,''N'') = ''N''';
/*call to generate the schedules in jai_rgm_recovery_lines*/
      jai_rgm_recovery_pkg.init_claim_schedule (lv_where);

      /*Call to process the recovery*/
      OPEN lc_cursor FOR lv_where;

      LOOP
         FETCH lc_cursor
          INTO lr_tax_line;

         EXIT WHEN lc_cursor%NOTFOUND;

         IF (g_level_procedure >= g_current_runtime_level)
         THEN
            fnd_log.STRING (g_level_procedure,
                            g_module_name || l_api_name,
                               'Get tax line:'
                            || ' tax_line_id->'
                            || lr_tax_line.tax_line_id
                           );
         END IF;

         IF lr_tax_line.tax_line_id > 0
         THEN
            /*mani added 'if' condition for bug 26729227 we are updating tax_line_id to tax_line_id*-1 in data fixes , those records should not be processed*/
            lr_doc_type.application_id := lr_tax_line.application_id;
            lr_doc_type.entity_code := lr_tax_line.entity_code;
            lr_doc_type.event_class_code := lr_tax_line.event_class_code;
            lr_doc_type.event_type_code := lr_tax_line.event_type_code;
            lr_doc_type.trx_type := lr_tax_line.trx_type;

                                                              --Added by Qinglei for bug#18534519 on 16-Apr-2014
            --2802
            OPEN get_det_fact (lr_tax_line.det_factor_id);

            FETCH get_det_fact
             INTO lr_det_factors;

            CLOSE get_det_fact;

            jai_rgm_recovery_pkg.process_recovery_pub
                     (pv_action                => 'CLAIM',
                      pr_document_type         => lr_doc_type,
                      pn_document_id           => lr_tax_line.trx_id,
                      pn_document_line_id      => lr_tax_line.trx_line_id,
                      pn_tax_line_id           => lr_tax_line.tax_line_id,
                      pv_call_from             => 'jai_inbound_process.process_recovery',
                      pn_batch_id              => p_request_id,
                      pv_account_type          => 'Regular',
                      pd_process_date          => TRUNC (SYSDATE),
                      p_process_status         => lv_process_status,
                      p_process_message        => lv_process_message,
                      pr_det_factors_rec       => lr_det_factors,
                      pr_tax_line_rec          => lr_tax_line
                     );

            IF lv_process_status <> 'SS'
            THEN
               fnd_message.set_name ('JA', lv_process_message);
               app_exception.raise_exception;
            END IF;
         END IF;
      END LOOP;

      CLOSE lc_cursor;
   EXCEPTION
      WHEN OTHERS
      THEN
         jai_debug (p_api_name      => l_api_name,
                    p_msg_type      => 'Debug',
                    p_msg_desc      => '--> Processing ANX1 ' || SQLERRM
                   );
         RAISE;
   END jai_anx2_action;


   procedure jai_populate_interface (p_inbd_tbl     inbd_tbl_typ,
                              p_file_id           IN NUMBER DEFAULT NULL,
    p_form_type         IN VARCHAR2 DEFAULT NULL,
    p_request_id        IN VARCHAR2 default null,
	P_CALL_FROM        IN VARCHAR2 default null,
    p_process_status    IN OUT NOCOPY VARCHAR2,
    p_process_message   IN OUT NOCOPY VARCHAR2
                              )

is
l_api_name varchar2(50):='jai_populate_interface';
lv_process_status varchar2(10);
lv_process_message varchar2(300);
BEGIN

for i in p_inbd_tbl.first..p_inbd_tbl.last
loop

 jai_debug
                              (p_api_name      => l_api_name,
                               p_msg_type      => 'Debug',
                               p_msg_desc      => 'before insert into JAI_GST_REPT_INBOUND_INTERFACE '||
							   ' p_file_id '||p_file_id||
							   ' p_form_type '||p_form_type||
							   ' p_call_from '||p_call_from||
							   ' p_request_id '||p_request_id
                              );

INSERT INTO jai_gst_rept_inbound_interface
(
                       INBOUND_INTF_LINE_ID        ,
FIRST_PARTY_PRIMARY_REG_NUM,
PERIOD_NAME,
FORM_TYPE,
SECTION_CODE,
UPLOAD_DATE,
THIRD_PARTY_PRIMARY_REG_NUM,
TRX_TYPE,
SEC7ACT,
SHIP_FROM_STATE,
BILL_TO_STATE,
PLACE_OF_SUPPLY,
SUPPLY_TYPE,
ACTION_FLAG,
TAX_INVOICE_NUMBER,
TAX_INVOICE_DATE,
TAX_INVOICE_VALUE,
HSN_CODE_ID,
HSN_CODE,
TAX_RATE_PERCENTAGE,
DIFFERENTIAL_TAX_RATE,
TAXABLE_BASIS,
CGST,
SGST,
IGST,
CESS,
CHKSUM,
TRX_NUMBER,
TRX_ID,
TRX_DATE,
SHIPPABLE_PORT_CODE,
SHIPPABLE_BILL_NO,
SHIPPABLE_BILL_DATE,
ITC_ENTITLEMENT_FLAG,
ITC_AVAILED_FLAG,
REC_TAX_AMT_FUNCL_CURR_CGST,
REC_TAX_AMT_FUNCL_CURR_SGST,
REC_TAX_AMT_FUNCL_CURR_IGST,
REC_TAX_AMT_FUNCL_CURR_CESS,
AMENDMENT_FLAG,
AMENDED_TRX_ID,
AMENDED_TRX_NUMBER,
AMENDED_TRX_DATE,
APPLIED_FROM_TRX_ID,
ORIGINAL_TAX_INVOICE_NUM,
ORIGINAL_TAX_INVOICE_DATE,
ITC_SUMMARY,
ITC_SECTION_CODE,
TOTAL_NO_OF_DOCS,
TOTAL_DOCS_VAL,
TOTAL_CGST,
TOTAL_SGST,
TOTAL_IGST,
GSTN_ACCK_STATUS,
ERROR_CODE,
ERROR_DESC,
GSTN_ACCK_ID,
CPF_STATUS,
MATCH_STATUS_FLAG,
MATCH_WITH_TOLERANCE,
MATCH_STATUS_REASON,
BATCH_ID,
REMARKS,
REQUEST_ID,
RECORD_TYPE_CODE,
CREATED_BY,
CREATION_DATE,
LAST_UPDATED_BY,
LAST_UPDATE_DATE,
LAST_UPDATE_LOGIN,
OBJECT_VERSION_NUMBER,
ATTRIBUTE1,
ATTRIBUTE2,
ATTRIBUTE3,
ATTRIBUTE4,
ATTRIBUTE5,
ATTRIBUTE6,
ATTRIBUTE7,
ATTRIBUTE8,
ATTRIBUTE9,
ATTRIBUTE10,
ATTRIBUTE11,
ATTRIBUTE12,
invoice_reference_number,
qr_code_response,
GSTN_ACCK_DATE
)
values
(
JAI_GST_REPT_INBOUND_INTER_S.NEXTVAL          ,
p_inbd_tbl(i).FIRST_PARTY_PRIMARY_REG_NUM,
p_inbd_tbl(i).PERIOD_NAME,
nvl(p_form_type,p_inbd_tbl(i).FORM_TYPE),
p_inbd_tbl(i).SECTION_CODE,
p_inbd_tbl(i).UPLOAD_DATE,
p_inbd_tbl(i).THIRD_PARTY_PRIMARY_REG_NUM,
p_inbd_tbl(i).TRX_TYPE,
p_inbd_tbl(i).SEC7ACT,
p_inbd_tbl(i).SHIP_FROM_STATE,
p_inbd_tbl(i).BILL_TO_STATE,
p_inbd_tbl(i).PLACE_OF_SUPPLY,
p_inbd_tbl(i).SUPPLY_TYPE,
p_inbd_tbl(i).ACTION_FLAG,
p_inbd_tbl(i).TAX_INVOICE_NUMBER,
p_inbd_tbl(i).TAX_INVOICE_DATE,
p_inbd_tbl(i).TAX_INVOICE_VALUE,
p_inbd_tbl(i).HSN_CODE_ID,
p_inbd_tbl(i).HSN_CODE,
p_inbd_tbl(i).TAX_RATE_PERCENTAGE,
p_inbd_tbl(i).DIFFERENTIAL_TAX_RATE,
p_inbd_tbl(i).TAXABLE_BASIS,
p_inbd_tbl(i).CGST,
p_inbd_tbl(i).SGST,
p_inbd_tbl(i).IGST,
p_inbd_tbl(i).CESS,
p_inbd_tbl(i).CHKSUM,
p_inbd_tbl(i).trx_number,
p_inbd_tbl(i).TRX_ID,
p_inbd_tbl(i).trx_date,
p_inbd_tbl(i).SHIPPABLE_PORT_CODE,
p_inbd_tbl(i).SHIPPABLE_BILL_NO,
p_inbd_tbl(i).SHIPPABLE_BILL_DATE,
p_inbd_tbl(i).ITC_ENTITLEMENT_FLAG,
p_inbd_tbl(i).ITC_AVAILED_FLAG,
p_inbd_tbl(i).REC_TAX_AMT_FUNCL_CURR_CGST,
p_inbd_tbl(i).REC_TAX_AMT_FUNCL_CURR_SGST,
p_inbd_tbl(i).REC_TAX_AMT_FUNCL_CURR_IGST,
p_inbd_tbl(i).REC_TAX_AMT_FUNCL_CURR_CESS,
p_inbd_tbl(i).AMENDMENT_FLAG,
p_inbd_tbl(i).AMENDED_TRX_ID,
p_inbd_tbl(i).AMENDED_TRX_NUMBER,
p_inbd_tbl(i).AMENDED_TRX_DATE,
p_inbd_tbl(i).APPLIED_FROM_TRX_ID,
p_inbd_tbl(i).ORIGINAL_TAX_INVOICE_NUM,
p_inbd_tbl(i).ORIGINAL_TAX_INVOICE_DATE,
p_inbd_tbl(i).ITC_SUMMARY,
p_inbd_tbl(i).ITC_SECTION_CODE,
p_inbd_tbl(i).TOTAL_NO_OF_DOCS,
p_inbd_tbl(i).TOTAL_DOCS_VAL,
p_inbd_tbl(i).TOTAL_CGST,
p_inbd_tbl(i).TOTAL_SGST,
p_inbd_tbl(i).TOTAL_IGST,
p_inbd_tbl(i).GSTN_ACCK_STATUS,
p_inbd_tbl(i).ERROR_CODE,
p_inbd_tbl(i).ERROR_DESC,
p_inbd_tbl(i).GSTN_ACCK_ID,
p_inbd_tbl(i).CPF_STATUS,
p_inbd_tbl(i).MATCH_STATUS_FLAG,
p_inbd_tbl(i).MATCH_WITH_TOLERANCE,
p_inbd_tbl(i).MATCH_STATUS_REASON,
p_file_id,
p_inbd_tbl(i).REMARKS,
p_inbd_tbl(i).REQUEST_ID,
p_inbd_tbl(i).RECORD_TYPE_CODE,
p_inbd_tbl(i).CREATED_BY,
nvl(p_inbd_tbl(i).CREATION_DATE,sysdate),
p_request_id,
nvl(p_inbd_tbl(i).LAST_UPDATE_DATE,sysdate),
p_inbd_tbl(i).LAST_UPDATE_LOGIN,
p_inbd_tbl(i).OBJECT_VERSION_NUMBER,
p_inbd_tbl(i).ATTRIBUTE1,
p_inbd_tbl(i).ATTRIBUTE2,
p_inbd_tbl(i).ATTRIBUTE3,
p_inbd_tbl(i).ATTRIBUTE4,
p_inbd_tbl(i).ATTRIBUTE5,
p_inbd_tbl(i).ATTRIBUTE6,
p_inbd_tbl(i).ATTRIBUTE7,
p_inbd_tbl(i).ATTRIBUTE8,
p_inbd_tbl(i).ATTRIBUTE9,
p_inbd_tbl(i).ATTRIBUTE10,
p_inbd_tbl(i).ATTRIBUTE11,
p_inbd_tbl(i).ATTRIBUTE12,
p_inbd_tbl(i).invoice_reference_number,
p_inbd_tbl(i).qr_code_response,
p_inbd_tbl(i).GSTN_ACCK_DATE
);

end loop;

commit;

p_process_status:='S';
p_process_message :='Succesfully populated jai_gst_rept_inbound_interface table';


exception
when others then


lv_process_message:=sqlerrm;
 jai_debug
                              (p_api_name      => l_api_name,
                               p_msg_type      => 'LOG',
                               p_msg_desc      => 'EXCEPTION occured while insert into JAI_GST_REPT_INBOUND_INTERFACE '||
							  lv_process_message
                              );

p_process_status:='E';
p_process_message:=lv_process_message;
app_exception.raise_exception;

END jai_populate_interface;


 PROCEDURE jai_return_repository_update(
     	   p_file_id           IN NUMBER DEFAULT NULL,
    p_form_type         IN VARCHAR2 DEFAULT NULL,
	 p_first_party_reg_num   IN              VARCHAR2 DEFAULT NULL,
      p_return_period         IN              VARCHAR2  DEFAULT NULL,
      p_section      IN              VARCHAR2  DEFAULT NULL,
      p_call_from         IN              VARCHAR2 DEFAULT NULL,
      p_request_id        IN              NUMBER DEFAULT NULL,
      p_process_status    OUT NOCOPY      VARCHAR2,
      p_process_message   OUT NOCOPY      VARCHAR2
   )
is

cursor get_inbd_dtls
is
select JII.*  from JAI_GST_REPT_INBOUND_INTERFACE JII
where (p_file_id is null or batch_id=p_file_id)
and form_type=p_form_type
and first_party_primary_reg_num=nvl(p_first_party_reg_num,first_party_primary_reg_num)
--and tax_invoice_number=nvl(p_tax_invoice_number ,tax_invoice_number)
and (section_code is null or section_code=p_section)
AND nvl(RECORD_TYPE_CODE,'???') <> 'REPOSITORY_UPDATED';




l_api_name varchar2(100):='jai_return_repository_update';
l_last_fetch          BOOLEAN := FALSE;
--TYPE get_inbd_dtls_typ IS TABLE OF get_inbd_dtls%ROWTYPe    INDEX BY BINARY_INTEGER;
tl_get_inbd_dtls                      INBD_TBL_TYP;
			-- lV_err_msg         varchar2(1000);
            TYPE get_blob_type IS TABLE OF jai_irn_details%ROWTYPe    INDEX BY BINARY_INTEGER;
            tl_get_blob get_blob_type;


ln_request_id NUMBER := FND_GLOBAL.conc_request_id;
lv_process_status varchar2(100);
lv_process_message varchar2(1000);
lv_err_msg  varchar2(1000);
  l_err_count       number;
  bulk_errors       exception;
     lv_blob BLOB;
  pragma exception_init(bulk_errors, -24381);


 BEGIN


               OPEN get_inbd_dtls;
               FETCH get_inbd_dtls
               BULK COLLECT INTO tl_get_inbd_dtls LIMIT g_limit_size;
			   			   close get_inbd_dtls;






IF p_form_type = 'IRN-INBOUND'
      THEN
	  /*
	  for i in tl_get_inbd_dtls.FIRST..tl_get_inbd_dtls.last
      loop
       tl_get_blob(i).qr_code_image:=jai_client_extn_pkg.f_qr_as_bmp(tl_get_inbd_dtls (i).qr_code_response,'L');
       end loop;
	   */
	  FORALL I IN tl_get_inbd_dtls.FIRST..tl_get_inbd_dtls.last SAVE EXCEPTIONS


		    UPDATE jai_irn_details jid
               SET invoice_reference_number =
                                        tl_get_inbd_dtls (i).invoice_reference_number,
								qr_code_image	=	jai_client_extn_pkg.f_qr_as_bmp(tl_get_inbd_dtls (i).qr_code_response,'L'),
							--	QR_CODE_image= tl_get_blob(i).qr_code_image,
                   qr_code_response = tl_get_inbd_dtls (i).qr_code_response,
                   status = tl_get_inbd_dtls (i).gstn_acck_Status,
                   acknowledgement_number =tl_get_inbd_dtls (i).gstn_acck_id,
                   acknowledgement_date = tl_get_inbd_dtls (i).GSTN_ACCK_DATE,
                   ERROR_CODE = tl_get_inbd_dtls (i).ERROR_CODE,
                   error_message = tl_get_inbd_dtls (i).error_desc,
                   request_id = tl_get_inbd_dtls (i).request_id
             WHERE jid.seller_gstin = tl_get_inbd_dtls (i).first_party_primary_reg_num
               AND jid.document_number = (tl_get_inbd_dtls(i). tax_invoice_number);
 FORALL I IN tl_get_inbd_dtls.FIRST..tl_get_inbd_dtls.last SAVE EXCEPTIONS
            UPDATE jai_tax_det_factors jtx
               SET invoice_reference_number =
                                        tl_get_inbd_dtls (i).invoice_reference_number
             WHERE det_factor_id IN (
                      SELECT det_factor_id
                        FROM jai_irn_details jid
                       WHERE jid.seller_gstin = tl_get_inbd_dtls (i).first_party_primary_reg_num
                         AND jid.document_number =
                                              (tl_get_inbd_dtls (i).tax_invoice_number
                                         ));



      ELSIF p_form_type = 'IRN-ERROR'
      THEN

	  FORALL I IN tl_get_inbd_dtls.FIRST..tl_get_inbd_dtls.last SAVE EXCEPTIONS
            UPDATE jai_irn_details jid
               SET status = tl_get_inbd_dtls (i).gstn_acck_status,
                   ERROR_CODE = tl_get_inbd_dtls (i).ERROR_CODE,
                   error_message = tl_get_inbd_dtls (i).error_desc,
                   request_id = p_request_id
             WHERE jid.seller_gstin = tl_get_inbd_dtls (i).first_party_primary_reg_num
               AND jid.document_number = (tl_get_inbd_dtls (i).tax_invoice_number);

FORALL I IN tl_get_inbd_dtls.FIRST..tl_get_inbd_dtls.last SAVE EXCEPTIONS
			   UPDATE jai_tax_det_factors jtx
               SET invoice_reference_number = NULL
             WHERE det_factor_id IN (
                      SELECT det_factor_id
                        FROM jai_irn_details jid
                       WHERE jid.seller_gstin = tl_get_inbd_dtls (i).first_party_primary_reg_num
                         AND jid.document_number =
                                              (tl_get_inbd_dtls (i).tax_invoice_number
                                              ));


elsif p_form_type is not null
then

FORALL I IN tl_get_inbd_dtls.FIRST..tl_get_inbd_dtls.last SAVE EXCEPTIONS
    UPDATE jai_tax_lines jtx
    SET
	     reporting_status_flag = (tl_get_inbd_dtls(i).gstn_acck_statuS),
		gstn_acck_status=tl_get_inbd_dtls(i).gstn_acck_status,
    --    gstn_payload_id = P_INBD_TBL(i).gstn_acck_id,
		match_status_flag=tl_get_inbd_dtls(i).match_status_flag,
	    action_flag=tl_get_inbd_dtls(i).action_flag,
		        last_update_date = SYSDATE
				,last_updated_by=p_request_id,
        record_type_code = 'GSTIN_INB_UPDT'
  WHERE
jtx.TRX_ID=tl_get_inbd_dtls(i).trx_id
AND jtx.tax_invoice_num =nvl(tl_get_inbd_dtls(i).tax_invoice_number,jtx.tax_invoice_num)
    AND   EXISTS (
        SELECT
            1
        FROM
            jai_tax_det_factors jdt
        WHERE
            jdt.det_factor_id = jtx.det_factor_id
            AND   jdt.tax_invoice_num = nvl(tl_get_inbd_dtls(i).tax_invoice_number,jtx.tax_invoice_num)
            AND   nvl(jdt.hsn_code_id,0) = nvl(tl_get_inbd_dtls(i).hsn_code_id, nvl(jdt.hsn_code_id,0))
    );
	 jai_debug (p_api_name      => l_api_name,
                              p_msg_type        => 'Debug',
                              P_MSG_DESC         =>     ' Number of records  updated in jai_tax_LINES'||
														sql%rowcount
                );

FORALL I IN tl_get_inbd_dtls.FIRST..tl_get_inbd_dtls.last SAVE EXCEPTIONS
   UPDATE jai_tax_det_factors jtd
                     SET
	     reporting_status_flag = (tl_get_inbd_dtls(i).gstn_acck_statuS),
		gstn_acck_status=tl_get_inbd_dtls(i).gstn_acck_status,
       -- gstn_payload_id = P_INBD_TBL(i).gstn_acck_id,
		match_status_flag=tl_get_inbd_dtls(i).match_status_flag,
	   -- action_flag=P_INBD_TBL(i).action_flag,
		        last_update_date = SYSDATE
				,last_updated_by=p_request_id,
        record_type_code = 'GSTIN_INB_UPDT'
WHERE
JTD.TRX_ID=tl_get_inbd_dtls(i).trx_id
AND JTD.tax_invoice_num =nvl(tl_get_inbd_dtls(i).tax_invoice_number,jtd.tax_invoice_num)
AND NVL (jtd.hsn_code_id, 0) =nvl(tl_get_inbd_dtls(i).hsn_code_id,nvl(jtd.hsn_code_id,0));

 jai_debug (p_api_name      => l_api_name,
                              p_msg_type        => 'Debug',
                              P_MSG_DESC         =>     ' Number of records  updated in jai_tax_det_factors'||
														sql%rowcount
                );


   FORALL I IN tl_get_inbd_dtls.FIRST..tl_get_inbd_dtls.last SAVE EXCEPTIONS
   update jai_gst_return_summary JRS
     SET
	    reporting_status_flag = (tl_get_inbd_dtls(i).gstn_acck_statuS),
		response_code = (tl_get_inbd_dtls(i).error_code),
		response_desc = (tl_get_inbd_dtls(i).error_desc),
		remarks =(tl_get_inbd_dtls(i).remarks),
		--gstn_acck_status=P_INBD_TBL(i).gstn_acck_status,
        gstn_payload_id = tl_get_inbd_dtls(i).gstn_acck_id,
		match_status_flag=tl_get_inbd_dtls(i).match_status_flag,
	    action_flag=tl_get_inbd_dtls(i).action_flag,
		        last_update_date = SYSDATE
				,last_updated_by=p_request_id,
        record_type_code = 'GSTIN_INB_UPDT'
		where JRS.TRX_ID=tl_get_inbd_dtls(i).trx_id
AND JRS.tax_invoice_number =nvl(tl_get_inbd_dtls(i).tax_invoice_number,JRS.tax_invoice_number)
AND NVL (JRS.hsn_code_id, 0) =nvl(tl_get_inbd_dtls(i).hsn_code_id,nvl(JRS.hsn_code_id,0));

		jai_debug (p_api_name      => l_api_name,
                              p_msg_type        => 'Debug',
                              P_MSG_DESC         =>     ' Number of records  updated in jai_gst_return_summary'||sql%rowcount
                );


	end if;

		update jai_gst_rept_inbound_interface
		set record_type_code='REPOSITORY_UPDATED',
		UPLOAD_DATE=SYSDATE
where ( p_file_id is null or batch_id=p_file_id)
and form_type=p_form_type
and first_party_primary_reg_num=nvl(p_first_party_reg_num,first_party_primary_reg_num)
--and tax_invoice_number=nvl(p_tax_invoice_number ,tax_invoice_number)
 and (section_code is null or section_code=p_section);

EXCEPTION
   when bulk_errors then
      l_err_count := sql%bulk_exceptions.count;

      jai_cmn_utils_pkg.write_fnd_log_msg(l_api_name, '--> Count = '||l_err_count);

      for i in 1 .. l_err_count loop
        -- Print out details of each error during bulk insert
      lv_err_msg  :=  '-->#: ' || i
              || '; Array index: '
              || sql%bulk_exceptions(i).error_index ||  ': ';
			  /* dbms_output.put_line('index:'||sql%bulk_exceptions(i).error_index);
            dbms_output.put_line('code:'||sql%bulk_exceptions(i).error_code);
            dbms_output.put_line('message:');
         dbms_output.put_line(sqlerrm(sql%bulk_exceptions(i).error_code)); */
		 jai_cmn_utils_pkg.write_fnd_log_msg(l_api_name,'exception msg '||sqlerrm(sql%bulk_exceptions(i).error_code));

       jai_debug (p_api_name      => l_api_name,
                              p_msg_type        => 'ERROR',
                              P_MSG_DESC         =>    '--> Processing ANX1 '||lv_err_msg
                             );
      end loop;
	  p_process_status:='E';
	  p_process_message:='-->  Processing tax repository update '||lv_err_msg;
	  app_exception.raise_exception;
when others then
    jai_debug (p_api_name      => l_api_name,
                              p_msg_type        => 'ERROR',
                              P_MSG_DESC         =>    '--> Processing tax_repository update '||sqlerrm
                             );
							    p_process_status:='E';
	  p_process_message:='-->  Processing tax_repository update '||sqlerrm;
	  app_exception.raise_exception;


end jai_return_repository_update;

END jai_gst_inbound_pkg;
/


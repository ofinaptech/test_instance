CREATE OR REPLACE PACKAGE BODY APPS.xxabrl_ven_payadvice_utr_pkg
IS
   PROCEDURE xxabrl_ven_main_utr_pkg (errbuf OUT VARCHAR2, retcode OUT NUMBER)
   IS
   BEGIN
      xxabrl_ven_payadvice_utr_prc;
   END;

   PROCEDURE xxabrl_ven_payadvice_utr_prc
   IS
   BEGIN
      EXECUTE IMMEDIATE 'truncate table XXABRLIPROC.XXABRL_VPORTAL_PAYADVICE_UTR';

      FOR cur IN (SELECT distinct CASE
                            WHEN hou.NAME LIKE '%HM%'
                               THEN 'HM'
                            ELSE 'SM'
                         END format,
                         hou.NAME operating_unit, aps.segment1 vendor_num,
                         aps.vendor_id, aps.vendor_name,
                         aps.vendor_type_lookup_code vendor_type,
                         apsa.vendor_site_code vendor_site,
                         apc.bank_account_name abrl_bank_name,
                       --  aip.bank_account_num abrl_bank_account,
                         apc.bank_account_num abrl_bank_account,
                         apc.check_number, aip.invoice_id, apc.check_id,
                         (SELECT invoice_num
                            FROM apps.ap_invoices_all aia
                           WHERE invoice_id = aip.invoice_id) invoice_num,
                         (SELECT invoice_date
                            FROM apps.ap_invoices_all aia
                           WHERE invoice_id = aip.invoice_id) invoice_date,
                       --  aip.ACCOUNTING_DATE  gl_date,
                        (SELECT gl_date
                            FROM apps.ap_invoices_all aia
                           WHERE invoice_id = aip.invoice_id) gl_date,
                         (SELECT invoice_amount
                            FROM apps.ap_invoices_all aia
                           WHERE invoice_id = aip.invoice_id) invoice_amount,
                          zz.amt transactional_amount,
                         apc.check_date transaction_value_date,
                         apc.doc_sequence_value doc_num,
                         apsa.attribute7 beneficiary_neft_ifsc_code,
                         apsa.attribute8 beneficiary_rtgs_ifsc_code,
                         apsa.attribute4 Vendor_Account_Number,
                         apc.attribute3 utr_number, 
                         apc.attribute6 utr_updation_date,
                         apc.attribute_category h2h_bank,
                         apc.attribute1 h2h_status,
                         apc.attribute2 h2h_reversal_status,
                         apc.status_lookup_code status,
                         (SELECT invoice_type_lookup_code
                            FROM apps.ap_invoices_all aia
                           WHERE invoice_id = aip.invoice_id) invoice_type
                    FROM apps.ap_suppliers aps,
                         apps.ap_supplier_sites_all apsa,
                         apps.ap_checks_all apc,
                         apps.ap_invoice_payments_all aip,
                         apps.hr_operating_units hou,
                          (SELECT   NVL (SUM (aip.amount), 0) amt,
                                         --   aip.payment_num,
                                            aip.invoice_id,  
                                            aip.check_id,
                                         --aip.accounting_date
                                            trunc(aip.LAST_UPDATE_DATE)
                         FROM 
                                   apps.ap_invoice_payments_all aip,
                                   apps.ap_checks_all apc
                WHERE 1=1
                 and aip.check_id=apc.check_id
             --     and apc.check_id=414480176
             GROUP BY aip.invoice_id,
                                 --  aip.accounting_date,
                              trunc(aip.LAST_UPDATE_DATE),
                              --  aip.payment_num,
                                aip.check_id,invoice_id) zz                   
                     WHERE 1 = 1
                       AND  zz.invoice_id=aip.invoice_id
                      AND zz.check_id=aip.check_id
                     AND aps.vendor_id = apsa.vendor_id
                     AND apsa.vendor_id = apc.vendor_id
                     AND apsa.vendor_site_id = apc.vendor_site_id
                     AND apc.check_id = aip.check_id
                     AND aip.org_id = hou.organization_id
                     AND apc.attribute3 is not null
                     and TO_DATE (apc.attribute6) between trunc(sysdate)-7 and trunc(sysdate)-1
                 --and aip. invoice_id=453393117
                 --and apc.attribute3 is not null
                  --and apc.check_id=90489951
         --       AND aps.segment1 IN ('7010801', '7001708', '6000374', '3007078')
                                 )
      LOOP
         INSERT INTO XXABRLIPROC.XXABRL_VPORTAL_PAYADVICE_UTR
                     (format, operating_unit, vendor_num,
                      vendor_id, vendor_name, vendor_type,
                      vendor_site, abrl_bank_name,
                      abrl_bank_account, check_number,
                      invoice_id, check_id, invoice_num,
                      invoice_date, 
                      gl_date,
                      invoice_amount,
                      transactional_amount, transaction_value_date,
                      doc_num, beneficiary_neft_ifsc_code,
                      beneficiary_rtgs_ifsc_code, 
                      Vendor_Account_Number,
                      utr_number,
                      utr_updation_date,
                       h2h_bank,
                      h2h_status, h2h_reversal_status,status,invoice_type,
                      process_flag
                     )
              VALUES (cur.format, cur.operating_unit, cur.vendor_num,
                      cur.vendor_id, cur.vendor_name, cur.vendor_type,
                      cur.vendor_site, cur.abrl_bank_name,
                      cur.abrl_bank_account, cur.check_number,
                      cur.invoice_id, cur.check_id, cur.invoice_num,
                      cur.invoice_date, 
                      cur.gl_date,
                      cur.invoice_amount,
                      cur.transactional_amount, cur.transaction_value_date,
                      cur.doc_num, cur.beneficiary_neft_ifsc_code,
                      cur.beneficiary_rtgs_ifsc_code,
                      cur.Vendor_Account_Number,
                       cur.utr_number,
                       cur.utr_updation_date,
                       cur.h2h_bank,
                      cur.h2h_status, cur.h2h_reversal_status,cur.status,cur.invoice_type,
                      'weekly'
                     );

         COMMIT;
      END LOOP;
   END;
END; 
/


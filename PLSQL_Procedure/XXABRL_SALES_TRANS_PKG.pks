CREATE OR REPLACE package APPS.xxabrl_sales_trans_pkg as
procedure xxabrl_sales_trn_proc(ERRBUFF             OUT VARCHAR2,
                                RETCODE             OUT NUMBER,
                                p_cust_num_list     IN varchar2,
                                p_cust_num_from     IN varchar2,
                                p_cust_num_to       IN varchar2,
                                p_trans_type_from   IN varchar2,
                                p_trans_type_to     IN varchar2,
                                p_trans_date_from   IN varchar2,
                                p_trans_date_to     IN varchar2,
                                p_gl_date           IN varchar2,
                                p_can_trans         IN varchar2,
                                P_ORG_ID            IN NUMBER
                               );
end xxabrl_sales_trans_pkg; 
/


CREATE OR REPLACE PROCEDURE APPS.XXABRL_RFQ_REPORT(errbuff       OUT VARCHAR2,
                                              retcode       OUT VARCHAR2,
                                              P_ORG_ID      IN NUMBER,
                                              P_RFQ_NUM     IN NUMBER,
                                              P_VENDOR_NAME IN VARCHAR2) AS
  l_request_id NUMBER;
  L_ORG_ID     NUMBER;
  CURSOR RFQ(P_RFQ_NUM NUMBER, P_VENDOR_NAME VARCHAR2) IS
    SELECT pov.vendor_name, pov.SEGMENT1
      FROM po_headers_all poh, po_rfq_vendors prv, po_vendors pov
     WHERE poh.type_lookup_code = 'RFQ'
       AND poh.po_header_id = prv.po_header_id
       AND prv.vendor_id = pov.vendor_id
       AND TO_NUMBER(poh.segment1) = P_RFQ_NUM
       AND pov.vendor_name = NVL(P_VENDOR_NAME, pov.vendor_name)
     ORDER BY TO_NUMBER(pov.SEGMENT1);
BEGIN
  --  L_ORG_ID := FND_PROFILE.VALUE('ORG_ID');
  FND_FILE.PUT_LINE(FND_FILE.LOG,
                    'Starting ABRL Request For Quotation' || ' ' ||
                    P_ORG_ID);
  FOR REC_RFQ IN RFQ(P_RFQ_NUM, P_VENDOR_NAME) LOOP
    l_request_id := fnd_request.submit_request('XXABRL', --Program Application short name
                                               'XXABRLRFQ', --Program short name
                                               NULL,
                                               NULL, --DEFAULT
                                               FALSE, --DEFAULT
                                               P_ORG_ID,
                                               P_RFQ_NUM,
                                               REC_RFQ.vendor_name);
    COMMIT;
  END LOOP;
END;
/


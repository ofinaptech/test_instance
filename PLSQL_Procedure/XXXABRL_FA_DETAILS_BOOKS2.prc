CREATE OR REPLACE PROCEDURE APPS.xxxabrl_fa_details_books2 (
   errbuf             VARCHAR2,
   retcode            NUMBER,
   p_book_code        fa_books.book_type_code%TYPE,
   p_period_name_to   fa_deprn_periods.period_counter%TYPE
)
AS
   v_period_name   VARCHAR2 (15);

   CURSOR cur_data
   IS
      SELECT   bk.book_type_code,
               TO_CHAR (bk.date_placed_in_service, 'DD-MON-YYYY') dpis,
               ad.asset_number ass_no, fd.units_assigned,
               ad.attribute17 old_asset_no, ad.attribute18 old_asset_cost,
               ad.attribute19 old_date_in_serivce, ad.attribute20 acc_dep,
               ad.attribute21 asset_countable, ad.current_units qty,
               
               --,cat.CATEGORY_ID
               loc.segment1 asset_location, cat.segment1 major_cat,
               cat.segment2 min_cat1, cat.segment3 min_cat2,
               ad2.description asset_desc, bk.original_cost,
               bk.COST final_cost, fr.nbv_retired,
               gcck.concatenated_segments asset_gl_code,
               gcck1.concatenated_segments dep_exp_gl_code,
               gcck2.concatenated_segments dep_reserve_gl_code,
               (bk.basic_rate * 100) dep_com_act, bk.deprn_method_code,
               fr.date_retired, fr.proceeds_of_sale, fdp.period_name,
               (CASE
                   WHEN fds.period_counter < p_period_name_to
                      THEN 0
                   ELSE fds.deprn_amount
                END
               ) deprn_amount,
               DECODE (get_ytd_deprn_2 (fd.asset_id,
                                        p_book_code,
                                        p_period_name_to
                                       ),
                       0, 0,
                       -1, fds.ytd_deprn
                      ) ytd_value,
               fds.deprn_reserve ltd_value, fth.transaction_name,
               fm.reviewer_comments,
               ROUND (((bk.COST / ad.current_units) * fd.units_assigned),
                      2
                     ) final_cost_assigned,
               ROUND ((  (  (CASE
                                WHEN fds.period_counter < p_period_name_to
                                   THEN 0
                                ELSE fds.deprn_amount
                             END
                            )
                          / ad.current_units
                         )
                       * fd.units_assigned
                      ),
                      2
                     ) deprn_assigned,
               ROUND
                  ((  (  DECODE (get_ytd_deprn_2 (fd.asset_id,
                                                  p_book_code,
                                                  p_period_name_to
                                                 ),
                                 0, 0,
                                 -1, fds.ytd_deprn
                                )
                       / ad.current_units
                      )
                    * fd.units_assigned
                   ),
                   2
                  ) deprn_till_date_assigned,
               ROUND ((  (fds.deprn_reserve / ad.current_units)
                       * fd.units_assigned
                      ),
                      2
                     ) accumulated_dep_assigned,
               ROUND
                  ((  fds.deprn_reserve
                    - DECODE (get_ytd_deprn_2 (fd.asset_id,
                                               p_book_code,
                                               p_period_name_to
                                              ),
                              0, 0,
                              -1, fds.ytd_deprn
                             )
                   ),
                   2
                  ) opening_acc_dep,
               ROUND
                  ((  (  (fds.deprn_reserve / ad.current_units)
                       * fd.units_assigned
                      )
                    - (  (  DECODE (get_ytd_deprn_2 (fd.asset_id,
                                                     p_book_code,
                                                     p_period_name_to
                                                    ),
                                    0, 0,
                                    -1, fds.ytd_deprn
                                   )
                          / ad.current_units
                         )
                       * fd.units_assigned
                      )
                   ),
                   2
                  ) opening_acc_dep_assigned
          FROM apps.fa_asset_history ah,
               apps.fa_books bk,
               apps.fa_categories cat,
               apps.fa_lookups lookups_at,
               apps.fa_lookups lookups_nu,
               apps.fa_lookups lookups_ol,
               apps.fa_lookups lookups_iu,
               apps.fa_lookups lookups_pt,
               apps.fa_lookups lookups_12,
               apps.fa_additions_tl ad2,
               apps.fa_additions_b ad
                                     --,FA_ASSET_INVOICES ai
      ,
               apps.fa_distribution_history fd,
               apps.fa_locations loc,
               apps.fa_retirements fr,
               apps.fa_transaction_headers fth,
               apps.fa_deprn_summary fds,
               apps.fa_deprn_periods fdp,
               apps.fa_books_bas bkprev,
               apps.fa_mass_additions fm        ----added for reviewer commnet
                                        ,
               apps.fa_distribution_accounts fda,
               apps.gl_code_combinations_kfv gcck,
               apps.gl_code_combinations_kfv gcck1,
               apps.gl_code_combinations_kfv gcck2
         WHERE ad.asset_id = ad2.asset_id
           AND ad.parent_asset_id IS NULL
           AND ad2.LANGUAGE = USERENV ('LANG')
           AND ah.asset_id = ad.asset_id
           AND ah.date_effective <= SYSDATE
           AND NVL (ah.date_ineffective, SYSDATE + 1) > SYSDATE
           AND ah.category_id = cat.category_id
           AND bk.asset_id = ad.asset_id
           AND bk.book_type_code = p_book_code
           AND lookups_at.lookup_code = ad.asset_type
           AND lookups_at.lookup_type = 'ASSET TYPE'
           AND lookups_nu.lookup_code = ad.new_used
           AND lookups_nu.lookup_type = 'NEWUSE'
           AND lookups_ol.lookup_code = ad.owned_leased
           AND lookups_ol.lookup_type = 'OWNLEASE'
           AND lookups_iu.lookup_code = ad.in_use_flag
           AND lookups_iu.lookup_type = 'YESNO'
           AND lookups_pt.lookup_code(+) = ad.property_type_code
           AND lookups_pt.lookup_type(+) = 'PROPERTY TYPE'
           AND lookups_12.lookup_code(+) = ad.property_1245_1250_code
           AND lookups_12.lookup_type(+) = '1245/1250 PROPERTY'
           --AND  ad.asset_id = ai.asset_id(+)   --------- for invoice date
           AND ad.asset_id = fd.asset_id               -- for distribution qty
           AND ad.asset_number = fm.asset_number(+)
           AND fm.fixed_assets_units(+) <> 0
           AND fd.location_id = loc.location_id                -- for location
           AND bk.asset_id = fr.asset_id(+)
           AND bk.book_type_code = fr.book_type_code(+)
           AND ad.asset_id = NVL (fth.asset_id, ad.asset_id)
           AND NVL (fth.book_type_code, bk.book_type_code) = bk.book_type_code
           AND fr.transaction_header_id_in = fth.transaction_header_id(+)
           AND fr.transaction_header_id_in(+) = bk.transaction_header_id_in
           --and fr.RETIREMENT_PRORATE_CONVENTION(+) not like  'ABRL RETMT' ---retired asset could not come
           AND fdp.book_type_code = fds.book_type_code
           AND fdp.period_counter = fds.period_counter
           AND fds.deprn_run_date IN (SELECT MAX (deprn_run_date)
                                        FROM apps.fa_deprn_summary fds1
                                       WHERE fds1.asset_id = fds.asset_id)
           --and fds.DEPRN_SOURCE_CODE='BOOKS'          ----added on 09-nov-09 for only one line
           --and fd.DATE_EFFECTIVE=bk.DATE_EFFECTIVE
           AND (bkprev.transaction_header_id_out(+) =
                                                   bk.transaction_header_id_in
                                                                              --AND NVL (bkprev.COST, bk.COST - 1) != bk.COST
               )
         --  AND fd.date_ineffective IS NULL
           AND bk.date_ineffective IS NULL
           AND ad.asset_id = fds.asset_id(+)
           AND fd.distribution_id = fda.distribution_id
           AND fda.asset_cost_account_ccid = gcck.code_combination_id
           AND fda.deprn_expense_account_ccid = gcck1.code_combination_id
           AND fda.deprn_reserve_account_ccid = gcck2.code_combination_id
           --and fdp.PERIOD_COUNTER BETWEEN nvl(:P_PERIOD_NAME_FROM,fdp.PERIOD_COUNTER) AND nvl(:P_PERIOD_NAME_TO,fdp.PERIOD_COUNTER)
           AND fdp.period_counter <= p_period_name_to
             --AND cat.category_id = NVL (p_cat_segment1, cat.category_id)
           --  AND loc.segment1 = NVL (p_location, loc.segment1)
           AND ad.asset_key_ccid = fm.asset_key_ccid(+)
            ---added on 13-mar-2012, because duplicate assets were showing----
      --and fm.ASSET_KEY_CCID is not null
      --and fds.PERIOD_COUNTER <=:P_PERIOD_NAME_TO
      --and ad.ASSET_NUMBER in (1018084,1000001,1000003,10000002,1000006,1009924)
      ORDER BY ad.asset_number;
BEGIN
   BEGIN
      SELECT period_name
        INTO v_period_name
        FROM apps.fa_deprn_periods
       WHERE period_counter = p_period_name_to;
   EXCEPTION
      WHEN OTHERS
      THEN
         v_period_name := NULL;
   END;

   fnd_file.put_line (fnd_file.output,
                      CHR (9) || 'XXABRL Fixed Asset Register Report -1APR15'
                     );
   fnd_file.put_line (fnd_file.output, CHR (9) || '                   ');
   fnd_file.put_line (fnd_file.output,
                         CHR (9)
                      || CHR (9)
                      || CHR (9)
                      || 'As on Date:'
                      || CHR (9)
                      || SYSDATE
                      || CHR (9)
                     );
   fnd_file.put_line (fnd_file.output, ' ');
   fnd_file.put_line (fnd_file.output, 'REPORT PARAMETRS ');
   fnd_file.put_line (fnd_file.output,
                         CHR (9)
                      || CHR (9)
                      || CHR (9)
                      || 'BOOK TYPE CODE:'
                      || CHR (9)
                      || p_book_code
                      || CHR (9)
                     );
   fnd_file.put_line (fnd_file.output,
                         CHR (9)
                      || CHR (9)
                      || CHR (9)
                      || 'Period Name:'
                      || CHR (9)
                      || v_period_name
                      || CHR (9)
                     );
   --Printing Output
   fnd_file.put_line (fnd_file.output,
                         CHR (9)
                      || 'BOOK TYPE CODE'
                      || CHR (9)
                      || 'Date in Service'
                      || CHR (9)
                      || 'Asset Number'
                      || CHR (9)
                      || 'Old Asset Number'
                      || CHR (9)
                      || 'Quantity'
                      || CHR (9)
                      || 'Assigned Quantity'
                      || CHR (9)
                      || 'Asset location'
                      || CHR (9)
                      || 'Major Category'
                      || CHR (9)
                      || 'Minor Category1'
                      || CHR (9)
                      || 'Minor category2'
                      || CHR (9)
                      || 'Asset Description'
                      || CHR (9)
                      || 'Original Cost of Assets'
                      || CHR (9)
                      || 'Final Cost of Assets'
                      || CHR (9)
                      || 'Final Cost Assigned'
                      || CHR (9)
                      || 'Depreciation charged for Month'
                      || CHR (9)
                      || 'Deprn for the Month Assigned'
                      || CHR (9)
                      || 'Depreciation charged till Date'
                      || CHR (9)
                      || 'Deprn charged till Date Assigned'
                      || CHR (9)
                      || 'Accumulated Depreciation'
                      || CHR (9)
                      || 'Accumulated Depreciation Assigned'
                      || CHR (9)
                      || 'Opening Acc Depreciation'
                      || CHR (9)
                      || 'Opening Acc Depreciation Assigned'
                      || CHR (9)
                      || 'WDV Cost of Retired Asset'
                      || CHR (9)
                      || 'Asset GL Code'
                      || CHR (9)
                      || 'Dep.Exp GL Code'
                      || CHR (9)
                      || 'Dep Reserve  GL Code'
                      || CHR (9)
                      || 'Depreciation Companies Act'
                      || CHR (9)
                      || 'Overriding Depreciation method'
                      || CHR (9)
                      --   ||'Depreciation Rate- Income Tax'||CHR(9)
                      || 'Sale of Asset Date'
                      || CHR (9)
                      || 'Realisation Value'
                      || CHR (9)
                      || 'Remarks for sale of Assets'
                      || CHR (9)
                      || 'REVIEWER COMMENTS'
                      || CHR (9)
                      || 'Old Data Place in Service'
                      || CHR (9)
                      || 'Old Original cost'
                      || CHR (9)
                      || 'old Accumulated Depreciation'
                      || CHR (9)
                      || 'Asset Countable'
                      || ' '
                      || CHR (9)
                     );

   FOR rec_data IN cur_data
   LOOP
      --Printing Local Variable Values to output.
      fnd_file.put_line (fnd_file.output,
                            CHR (9)
                         || rec_data.book_type_code
                         || CHR (9)
                         || rec_data.dpis
                         || CHR (9)
                         || rec_data.ass_no
                         || CHR (9)
                         || rec_data.old_asset_no
                         || CHR (9)
                         || rec_data.qty
                         || CHR (9)                                     -- qty
                         || rec_data.units_assigned
                         || CHR (9)
                         || rec_data.asset_location
                         || CHR (9)
                         || rec_data.major_cat
                         || CHR (9)
                         || rec_data.min_cat1
                         || CHR (9)
                         || rec_data.min_cat2
                         || CHR (9)
                         || rec_data.asset_desc
                         || CHR (9)
                         || rec_data.original_cost
                         || CHR (9)
                         || rec_data.final_cost
                         || CHR (9)
                         || rec_data.final_cost_assigned
                         || CHR (9)
                         || rec_data.deprn_amount
                         || CHR (9)
                         || rec_data.deprn_assigned
                         || CHR (9)
                         || rec_data.ytd_value
                         || CHR (9)                   -- dep charged till date
                         || rec_data.deprn_till_date_assigned
                         || CHR (9)
                         || rec_data.ltd_value
                         || CHR (9)                         -- accumulated dep
                         || rec_data.accumulated_dep_assigned
                         || CHR (9)
                         || rec_data.opening_acc_dep
                         || CHR (9)
                         || rec_data.opening_acc_dep_assigned
                         || CHR (9)
                         || rec_data.nbv_retired
                         || CHR (9)
                         || rec_data.asset_gl_code
                         || CHR (9)
                         || rec_data.dep_exp_gl_code
                         || CHR (9)
                         || rec_data.dep_reserve_gl_code
                         || CHR (9)
                         || rec_data.dep_com_act
                         || CHR (9)
                         || rec_data.deprn_method_code
                         || CHR (9)
                         || rec_data.date_retired
                         || CHR (9)
                         || rec_data.proceeds_of_sale
                         || CHR (9)
                         || rec_data.transaction_name
                         || CHR (9)
                         || rec_data.reviewer_comments
                         || CHR (9)
                         || rec_data.old_date_in_serivce
                         || CHR (9)
                         || rec_data.old_asset_cost
                         || CHR (9)
                         || rec_data.acc_dep
                         || CHR (9)
                         || rec_data.asset_countable
                         || CHR (9)
                        );
   END LOOP;
END xxxabrl_fa_details_books2; 
/


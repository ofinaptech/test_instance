CREATE OR REPLACE PROCEDURE APPS.xxmrl_po_req_auto_prc (
   errbuf    out   varchar2,
   retcode   out   varchar2
)
IS
/*========================================================================================================================================
|| Concurrent Program Name : XXMRL PR Inbound Interface Program
|| Procedure Name          : APPS.xxmrl_po_req_auto_prc
|| Description             : PR Auto Created on Daily basis
==========================================================================================================================================
|| Version   Date           Author                    Description
|| ---------------------------------------------------------------------------------------------------------------------------------------
|| 1.0       12-Aug-2019    Lokesh Poojari            Initial version. PR Auto Created on Daily basis
|| 1.2       07-Apr-2021    Pawan Sahu                Changes to catch exceptions more clearly and continue processing next PR in case
                                                                                        current PR goes into exception.
|| 1.3       02-Aug-2021   Suraj Pawar               Added Buyer logic and validation 
========================================================================================================================================*/
   v_error_flag                varchar2 (1);
   v_error_msg                 varchar2 (500);
   v_record_id                 number;
   v_dist_seq_id               number;
   v_trans_id                  number;
   v_int_source_line_id        number;
   v_item_id                   number;
   v_organization_id           number;
   v_location_id               number;
   v_org_id                    number;
   v_charge_account_id         number;
   v_preparer_id               number;
   -- v_to_person_id         number;
   --  v_errorlines           boolean;
   v_error                     boolean;
   v_val_retcode               number;
   v_ref_count                 number         := 0;
   v_charge_account_segments   varchar2 (100);
   v_concatenated_segments     varchar2 (100);
   nv_error_counter            number;
   --v_val_retcode             number;
   v_chart_of_accounts_id      number;
   l_charge_account_id         number;
   v_buyer_id                           number;
   v_buyer_name                 varchar2 (100);

   cursor cur_header_pr
   is
      select prh.rowid, org_id, pr_ref_num, destination_type_code,
             --  interface_source_code,
             source_type_code, unit_meas_lookup_code, line_type_id, item_id,
             charge_account_segments, line_num, quantity, unit_price,
             destination_organization_id, deliver_to_location_id,
             employee_number, preparer_id, to_person_id, suggested_buyer_id,
             note_to_agent, note_to_receiver, authorization_status,
             approved_date, description, attribute1, attribute2, attribute3,
             attribute4, attribute6, attribute7, operating_unit,
             creation_date
        from xxabrl.xxmrl_po_req_h_int prh
       where 1 = 1
         and nvl (prh.error_flag, 'N') in ('N', 'E'); -- V1.2 commented
--         and nvl (prh.error_flag, 'N') in ('N');        -- V1.2 added to not re-process error records
BEGIN
   fnd_file.put_line (fnd_file.log, 'Validation started for staging table');

   FOR rec_header_pr IN cur_header_pr LOOP
      v_error_flag              := null;
      v_error_msg               := null;
      v_item_id                 := null;
      v_organization_id         := null;
      v_location_id             := null;
      v_charge_account_id       := null;
      v_charge_account_id       := null;
      v_charge_account_segments := null;
      v_concatenated_segments   := null;
      --v_val_retcode           :=null;
      v_org_id                  := null;
      l_charge_account_id       := null;

      begin
         select po_ri_dist_sequence_s.nextval
           into v_dist_seq_id
           from dual;

         select po_requisitions_interface_s.nextval
           into v_trans_id
           from dual;

         select xxabrl_req_line_seq_int.nextval
           into v_int_source_line_id
           from dual;

-------------------------------------------Validation Starts-------------------------------------------------
-- Validation For PR_REF_Num
-------------------------------------------------------------------------------------------------------------
         if rec_header_pr.pr_ref_num is null
         then
            v_error_flag := 'E';
            v_error_msg := v_error_msg || 'PR_REF_NUM Can not be Null';
            fnd_file.put_line
                   (fnd_file.log, 'PR REF NUM can not be Null for the Org Id----------> ' || rec_header_pr.org_id);
         end if;

-------------------------------------------------------------------------------------------------------------
/* Validation For Duplicate PR_REF_Num
-------------------------------------------------------------------------------------------------------------

         ---check the PR Ref Num was already existed or not
         begin
            select count (segment1)
              into v_ref_count
              from apps.po_requisition_headers_all
             where 1 = 1
               and trunc (creation_date) >= '01-Aug-2019'
               and to_char (segment1) = to_char (rec_header_pr.pr_ref_num);

            if v_ref_count > 0 then
               v_error_flag := 'E';
               v_error_msg  := 'Duplicate PR Ref Number.This PR Ref Number already exists ';
               fnd_file.put_line (fnd_file.log, 'Duplicate PR Ref Number.This PR Ref Number already exists----------> ' || rec_header_pr.pr_ref_num);
            else
               v_error := false;
            end if;
         exception
            when others then
               v_error_flag := 'E';
               v_error_msg := sqlcode || ': ' || sqlerrm;
         end;

-------------------------------------------------------------------------------------------------------------
-- Validation For  Item Classification if  require
-------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------
-- Validation For  SAC Code  if  require
-------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------
-- Validation For interface_source_code
-------------------------------------------------------------------------------------------------------------
/*  if rec_header_pr.interface_source_code is null then
     v_error_flag := 'E';
     v_error_msg := 'INTERFACE_SOURCE_CODE Can not be Null';
  end if;  */

-------------------------------------------------------------------------------------------------------------
-- Validation For Destination_Type_Code
-------------------------------------------------------------------------------------------------------------
         if rec_header_pr.destination_type_code is null then
            v_error_flag := 'E';
            v_error_msg  := v_error_msg || 'DESTINATION_TYPE_CODE Can not be Null ';
            fnd_file.put_line (fnd_file.log, 'DESTINATION TYPE CODE can not be Null for the PR Ref Num----------> ' || rec_header_pr.pr_ref_num);
         end if;

-------------------------------------------------------------------------------------------------------------
-- Validation For Item_Id
-------------------------------------------------------------------------------------------------------------
         if rec_header_pr.item_id is null then
            v_error_flag := 'E';
            v_error_msg := v_error_msg || 'ITEM_ID Can not be Null ';
            fnd_file.put_line (fnd_file.log, 'TEM ID can not be Null for the PR Ref Num----------> ' || rec_header_pr.pr_ref_num);
         else
            begin
               select distinct inventory_item_id
                 into v_item_id
                 from mtl_system_items_b
                where inventory_item_id = rec_header_pr.item_id
                  and enabled_flag='Y'
                  and end_date_active is null
                  and inventory_item_status_code='Active'
                  and item_type='ABRL CAPEX';
            exception
               when others then
                  -- v_item_id:=null
                  v_error_flag := 'E';
                  v_error_msg := v_error_msg || 'Error Occurred while Retrieving The ITEM id ' || sqlerrm;
                  fnd_file.put_line (fnd_file.log,'Error Occurred while Retrieving The ITEM id----------> ' || rec_header_pr.item_id);
            end;
         end if;

-------------------------------------------------------------------------------------------------------------
-- Validation For charge_account_segments with Auto Create  if not Exist
-------------------------------------------------------------------------------------------------------------
/*   IF rec_header_pr.charge_account_segments IS NULL
   THEN
      v_error_flag := 'E';
      v_error_msg := v_error_msg || 'CHARGE_ACCOUNT can not be null';
   ELSE
      BEGIN
         SELECT DISTINCT code_combination_id
                    INTO v_charge_account_id
                    FROM gl_code_combinations_kfv kfv
                   --  xxmrl_po_req_h_int prl
         WHERE           concatenated_segments =
                                   rec_header_pr.charge_account_segments;
                     --AND line_num = rec_header_pr.line_num;
                   --  AND pr_ref_num = rec_header_pr.pr_ref_num;
      EXCEPTION
         WHEN OTHERS
         THEN
            v_error_flag := 'E';
            v_error_msg :=
                  v_error_msg
               || 'Error Occurred while Retrieving the Charge_account_id'
               || SQLERRM;
      END;
   END IF; */
--    begin
         if rec_header_pr.charge_account_segments is null then
            v_error_flag := 'E';
            v_error_msg := v_error_msg || ' Charge account segments value is NULL';
         else
            begin
               v_charge_account_id := null;
               v_concatenated_segments := null;
               nv_error_counter := 0;

               select code_combination_id
                 into v_charge_account_id
                 from gl_code_combinations_kfv glcc
                where 1 = 1
                  and end_date_active is null
                  and concatenated_segments = rec_header_pr.charge_account_segments; -- V1.2 added
                  -- V1.2 commented
                 /*and segment1 =
                          substr (rec_header_pr.charge_account_segments, 1, 2)
                  and segment2 =
                          substr (rec_header_pr.charge_account_segments, 4, 3)
                  and segment3 =
                          substr (rec_header_pr.charge_account_segments, 8, 3)
                  and segment4 =
                         substr (rec_header_pr.charge_account_segments, 12, 7)
                  and segment5 =
                         substr (rec_header_pr.charge_account_segments, 20, 2)
                  and segment6 =
                         substr (rec_header_pr.charge_account_segments, 23, 6)
                  and segment7 =
                         substr (rec_header_pr.charge_account_segments, 30, 3)
                  and segment8 =
                         substr (rec_header_pr.charge_account_segments, 34, 4); */
            exception
               when no_data_found
               then
                  v_error_msg := v_error_msg || ' Invalid Segments : ' || rec_header_pr.charge_account_segments;
                  --  v_error_flag := 'y';
                  --     v_val_retcode := 1;
                  -- nv_error_counter := 1;
                  fnd_file.put_line (fnd_file.log, rec_header_pr.pr_ref_num || '. ' || ' Invalid Segments : ' || rec_header_pr.charge_account_segments);

                  begin
                     v_chart_of_accounts_id := 50328;
                     v_concatenated_segments := rec_header_pr.charge_account_segments; -- V1.2 added
                     -- V1.2 commented
                     /*      substr (rec_header_pr.charge_account_segments, 1, 2)
                        || '.'
                        || substr (rec_header_pr.charge_account_segments, 4, 3)
                        || '.'
                        || substr (rec_header_pr.charge_account_segments, 8, 3)
                        || '.'
                        || substr (rec_header_pr.charge_account_segments, 12, 7)
                        || '.'
                        || substr (rec_header_pr.charge_account_segments, 20, 2)
                        || '.'
                        || substr (rec_header_pr.charge_account_segments, 23, 6)
                        || '.'
                        || substr (rec_header_pr.charge_account_segments, 30, 3)
                        || '.'
                        || substr (rec_header_pr.charge_account_segments, 34, 4); */

                     select fnd_flex_ext.get_ccid ('SQLGL', 'GL#', 50328, to_char (sysdate, 'YYYY/MM/DD HH24:MI:SS'), v_concatenated_segments)
                       into v_charge_account_segments
                       from dual;

                     if v_charge_account_segments = 0 then
                        v_error_msg := v_error_msg || ' Invalid Code Combination : ' ||rec_header_pr.charge_account_segments; -- V1.2 added

                        --V1.2 commented
                        /* || substr (rec_header_pr.charge_account_segments, 1, 2)
                           || '.'
                           || substr (rec_header_pr.charge_account_segments, 4, 3)
                           || '.'
                           || substr (rec_header_pr.charge_account_segments, 8, 3)
                           || '.'
                           || substr (rec_header_pr.charge_account_segments, 12, 7)
                           || '.'
                           || substr (rec_header_pr.charge_account_segments, 20, 2)
                           || '.'
                           || substr (rec_header_pr.charge_account_segments, 23, 6)
                           || '.'
                           || substr (rec_header_pr.charge_account_segments, 30, 3)
                           || '.'
                           || substr (rec_header_pr.charge_account_segments, 34, 4); */
                        v_error_flag := 'E';
                        v_val_retcode := 1;
                        fnd_file.put_line (fnd_file.log, rec_header_pr.pr_ref_num || '. Invalid Code Combination : ' || rec_header_pr.charge_account_segments); --V1.2 added
                           -- V1.2 commented
                         /*|| substr (rec_header_pr.charge_account_segments, 1, 2)
                           || '.'
                           || substr (rec_header_pr.charge_account_segments, 4, 3)
                           || '.'
                           || substr (rec_header_pr.charge_account_segments, 8, 3)
                           || '.'
                           || substr (rec_header_pr.charge_account_segments, 12, 7)
                           || '.'
                           || substr (rec_header_pr.charge_account_segments, 20, 2)
                           || '.'
                           || substr (rec_header_pr.charge_account_segments, 23, 6)
                           || '.'
                           || substr (rec_header_pr.charge_account_segments, 30, 3)
                           || '.'
                           || substr (rec_header_pr.charge_account_segments, 34, 4); */
                     else
                        if nvl (nv_error_counter, 0) = 1 then
                           v_error_flag := 'N';
                           v_val_retcode := 0;
                        end if;
                     end if;
                  end;
               when others then
                  v_error_msg := v_error_msg || 'Unexpected Segments Error ' || sqlerrm;
                  v_error_flag := 'E';
                  v_val_retcode := 1;
                  fnd_file.put_line (fnd_file.log, rec_header_pr.pr_ref_num || '. Unexpected Segments Error ' || sqlerrm);
            end;
         end if;

         begin
            select distinct code_combination_id
              into l_charge_account_id
              from gl_code_combinations_kfv kfv
              --  xxmrl_po_req_h_int prl
             where concatenated_segments = rec_header_pr.charge_account_segments
               and end_date_active is null;
         exception
            when others then
               v_error_flag := 'E';
               v_error_msg := v_error_msg || 'Error Occurred while Retrieving the Charge_account_id ' || sqlerrm;
         end;
         --     end;

-------------------------------------------------------------------------------------------------------------
-- Validation For  PR Quantiy
-------------------------------------------------------------------------------------------------------------
         if (rec_header_pr.quantity is null or rec_header_pr.quantity =0) then
            v_error_flag := 'E';
            v_error_msg := v_error_msg || 'QUANTITY Can not be Null or Zero ';
            fnd_file.put_line (fnd_file.log, 'Quantity can not be Null or Zero for the PR Ref Num----------> ' || rec_header_pr.pr_ref_num);
         end if;

-------------------------------------------------------------------------------------------------------------
-- Validation For  Employee_Number
-------------------------------------------------------------------------------------------------------------
         if rec_header_pr.employee_number is null then
            v_error_flag := 'E';
            v_error_msg := v_error_msg || 'Employee_number  Can not be Null ';
            fnd_file.put_line (fnd_file.log, 'Employee Number can not be Null for the PR Ref Num----------> ' || rec_header_pr.pr_ref_num);
         else
            begin
               select distinct per.person_id
                 into v_preparer_id
                 from apps.per_all_people_f per,
                      apps.per_all_assignments_f paaf
                where 1 = 1
                  and employee_number = rec_header_pr.employee_number
                  and trunc (sysdate) between paaf.effective_start_date and paaf.effective_end_date
                  and per.person_id = paaf.person_id;
            exception
               when others then
                  v_error_flag := 'E';
                  v_error_msg := v_error_msg || 'Error occurred while Retrieving the Employee_number ' || sqlerrm;
                  fnd_file.put_line (fnd_file.log, 'Error Occurred while Retrieving The Employee Number----------> ' || rec_header_pr.employee_number);
            end;
         end if;

-------------------------------------------------------------------------------------------------------------
-- Validation For  unit_meas_lookup_code
-------------------------------------------------------------------------------------------------------------
         if rec_header_pr.unit_meas_lookup_code is null
         then
            v_error_flag := 'E';
            v_error_msg := v_error_msg || 'unit_meas_lookup_code Can not be Null ';
            fnd_file.put_line (fnd_file.log, 'Unit Meas Lookup code can not be Null for the PR Ref Num----------> ' || rec_header_pr.pr_ref_num);
         end if;

-------------------------------------------------------------------------------------------------------------
-- Validation For  destination_organization_id
-------------------------------------------------------------------------------------------------------------
         if rec_header_pr.destination_organization_id is null then
            v_error_flag := 'E';
            v_error_msg := v_error_msg || 'DESTINATION_ORGANIZATION Can not be Null ';
            fnd_file.put_line (fnd_file.log, 'Destination Organization Id can not be Null for the PR Ref Num----------> ' || rec_header_pr.pr_ref_num);
         else
            begin
               select organization_id
                 into v_organization_id
                 from org_organization_definitions ood
                where organization_id = rec_header_pr.destination_organization_id;
            exception
               when others then
                  v_error_flag := 'E';
                  v_error_msg := v_error_msg || 'Eroor occurred while Retrieving The Inventory org_id ' || sqlerrm;
                  fnd_file.put_line (fnd_file.log, 'Error occurred while Retrieving the Destination Org Id----------> ' || rec_header_pr.org_id);
            end;
         end if;

-------------------------------------------------------------------------------------------------------------
-- Validation For deliver_to_location_id
-------------------------------------------------------------------------------------------------------------
         if rec_header_pr.deliver_to_location_id is null then
            v_error_flag := 'E';
            v_error_msg := v_error_msg || 'DELIVER_TO_LOCATION can not be null ';
            fnd_file.put_line (fnd_file.log, 'Deliver To Location can not be Null for the PR Ref Num----------> ' || rec_header_pr.pr_ref_num);
         else
            begin
               select location_id
                 into v_location_id
                 from hr_locations
                where location_id = rec_header_pr.deliver_to_location_id
                and inactive_date is null;
            exception
               when others then
                  v_error_flag := 'E';
                  v_error_msg := v_error_msg || 'Error occurred while Retrieving  The Deliver_to_Location_id ' || sqlerrm;
                  fnd_file.put_line (fnd_file.log, 'Error Occurred while Retrieving The Deliver to Location_id----------> ' || rec_header_pr.deliver_to_location_id);
            end;
         end if;

-------------------------------------------------------------------------------------------------------------
-- Validation For org_id
-------------------------------------------------------------------------------------------------------------
         if rec_header_pr.org_id is null then
            v_error_flag := 'E';
            v_error_msg := v_error_msg || 'ORG_ID can not be null';
            fnd_file.put_line (fnd_file.log, 'ORG_ID can not be null for the PR Ref Num----------> ' || rec_header_pr.pr_ref_num);
         else
            begin
               select organization_id
                 into v_org_id
                 from hr_operating_units
                where organization_id = rec_header_pr.org_id;
            exception
               when others then
                  v_error_flag := 'E';
                  v_error_msg := v_error_msg || 'Error Occurred while Retrieving the org_id ' || sqlerrm;
                  fnd_file.put_line (fnd_file.log, 'error Occured while Retrieving the org_id----------> ' || rec_header_pr.org_id);
            end;
         end if;

-------------------------------------------------------------------------------------------------------------
-- Validation For Unit_Price
-------------------------------------------------------------------------------------------------------------
         if rec_header_pr.unit_price is null then
            v_error_flag := 'E';
            v_error_msg := v_error_msg || 'UNIT_PRICE can not be null';
            fnd_file.put_line (fnd_file.log,
                   'Unit Price can not be null for the PR Ref Num----------> ' || rec_header_pr.pr_ref_num);
         end if;

-------------------------------------------------------------------------------------------------------------
-- Validation For Buyer
-------------------------------------------------------------------------------------------------------------
         if rec_header_pr.suggested_buyer_id is not null then
--            v_error_flag := 'E';
--            v_error_msg := v_error_msg || 'Suggested Buyer Id can not be null';
--            fnd_file.put_line (fnd_file.log,
--                   'Unit Price can not be null for the PR Ref Num----------> ' || rec_header_pr.pr_ref_num);
--          else 
             /* Formatted on 2021/06/10 13:33 (Formatter Plus v4.8.8) */
                BEGIN
                   SELECT DISTINCT pa.agent_id--, per.full_name
                              INTO v_buyer_id--, v_buyer_name
                              FROM po_agents pa, per_all_people_f per, fnd_user fu
                             WHERE 1 = 1
                               AND pa.agent_id = per.person_id
                               AND fu.employee_id = per.person_id
                               AND fu.end_date IS NULL
                               --AND PA.END_DATE_ACTIVE IS NULL
                               AND pa.end_date_active IS NULL
                               AND pa.agent_id = TRIM(rec_header_pr.suggested_buyer_id);
                EXCEPTION
                   WHEN OTHERS
                   THEN
                      v_error_flag := 'E';
                      v_error_msg :=
                            v_error_msg
                         || 'Error Occurred while Retrieving the Suggested Buyer id'
                         || SQLERRM;
                      fnd_file.put_line
                                   (fnd_file.LOG,
                                       'error Occured while Retrieving the Suggested Buyer id----------> '
                                    || rec_header_pr.org_id
                                   );
                END;                   
         end if;

-------------------------------------------Validation Ends---------------------------------------------------

-------------------------------------------------------------------------------------------------------------
-- INSERTION IN REQUSITION INTERFACE
-------------------------------------------------------------------------------------------------------------
         begin

            if v_error_flag is null then
               fnd_file.put_line (fnd_file.log, 'Inserting Data into Po Requisitions Interface Table');
                INSERT INTO po_requisitions_interface_all
                           (creation_date, transaction_id,
                            interface_source_code, batch_id, org_id,
                            destination_type_code,
                            authorization_status, preparer_id,
                            req_number_segment1, header_description,
                            header_attribute1, header_attribute2,
                            header_attribute3, header_attribute4,
                            --  header_attribute5,
                            header_attribute6, header_attribute7,
                            source_type_code,
                            unit_of_measure,
                            line_type_id, quantity,
                            destination_organization_id,
                            deliver_to_location_id,
                            -- line_attribute5,
                            item_id, charge_account_id,
                            deliver_to_requestor_id, unit_price,
                            multi_distributions, req_dist_sequence_id,
                            interface_source_line_id,
                            suggested_buyer_id
--                            suggested_buyer_name
                           )
                    VALUES (SYSDATE, v_trans_id,
                            'POR', 1, rec_header_pr.org_id,
                            rec_header_pr.destination_type_code,
                            rec_header_pr.authorization_status, v_preparer_id,
                            rec_header_pr.pr_ref_num, rec_header_pr.description,
                            rec_header_pr.attribute1, rec_header_pr.attribute2,
                            rec_header_pr.attribute3, rec_header_pr.attribute4,
                            --   rec_header_pr.pr_ref_num,
                            rec_header_pr.attribute6, rec_header_pr.attribute7,
                            rec_header_pr.source_type_code,
                            rec_header_pr.unit_meas_lookup_code,
                            1, rec_header_pr.quantity,
                            rec_header_pr.destination_organization_id,
                            rec_header_pr.deliver_to_location_id,
                            --rec_header_pr.pr_ref_num,
                            rec_header_pr.item_id, l_charge_account_id,
                            v_preparer_id, rec_header_pr.unit_price,
                            'Y', v_dist_seq_id,
                            v_int_source_line_id,v_buyer_id
--                            , v_buyer_name
                           );
                update xxmrl_po_req_h_int
                  set error_flag = 'P',
                      error_msg = 'Successfully inserted in interface table'
                where 1 = 1         --  and pr_ref_num = rec_header_pr.pr_ref_num
                  and rowid = rec_header_pr.rowid;

               commit;
               fnd_file.put_line (fnd_file.LOG, 'Successfully inserted into interface. PR REF NUM => ' || rec_header_pr.pr_ref_num);
               fnd_file.put_line (fnd_file.LOG, 'ORG_ID =>' || rec_header_pr.org_id || ' OU_NAME =>' || rec_header_pr.operating_unit);
               fnd_file.put_line (fnd_file.LOG, '---------------------------------------------------');
            --      EXCEPTION
            --         WHEN OTHERS
            --         THEN
            --            fnd_file.put_line (fnd_file.LOG,
            --                                  'error occured=>'
            --                               || rec_header_pr.pr_ref_num
            --                               || '=>'
            --                               || 'OU_NAME=>'
            --                               || rec_header_pr.org_id
            --                              );
            --            fnd_file.put_line (fnd_file.LOG, SQLCODE || '=>' || SQLERRM);
            else
               update xxmrl_po_req_h_int
                  set error_flag = v_error_flag,
                      error_msg = v_error_msg
                where 1 = 1         --  and pr_ref_num = rec_header_pr.pr_ref_num
                  and rowid = rec_header_pr.rowid;
                commit;
            end if;
         exception when others then
           fnd_file.put_line (fnd_file.log, 'Error inserting into interface table or updating stage table: '|| sqlerrm);
         end;

         commit;
      end;
      fnd_file.put_line (fnd_file.log, 'v_error_msg for PR: '|| rec_header_pr.pr_ref_num ||', '|| nvl(trim(v_error_msg), 'No Error.'));
      fnd_file.put_line (fnd_file.log, ' ');
   END LOOP;
exception
   when others then
      v_error := true;
      fnd_file.put_line (fnd_file.log, 'Exception in Main: '|| sqlerrm);
      retcode := 1;
      commit;
end xxmrl_po_req_auto_prc; 
/


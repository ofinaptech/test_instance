CREATE OR REPLACE PACKAGE BODY APPS.xxabrl_pay_details
IS
   PROCEDURE xxabrl_main_pkg (
      errbuf    OUT      VARCHAR2,
      retcode   OUT      NUMBER,
      p_type    IN       VARCHAR2
   )
   IS
   BEGIN
      IF p_type = 'Daily'
      THEN
         xxabrl_pay_daily_data;
      END IF;

      IF p_type = 'Dump'
      THEN
         xxabrl_pay_dump;
      END IF;
   END xxabrl_main_pkg;

   PROCEDURE xxabrl_pay_daily_data
   IS
   BEGIN
      EXECUTE IMMEDIATE 'truncate table  xxabrliproc.xxabrl_payment_details';

      FOR buf IN
         (SELECT DISTINCT hou.short_code ou, aia.invoice_id, aia.invoice_num,
                          aia.invoice_date, aia.invoice_amount,
                          aia.amount_paid, aia.doc_sequence_value inv_doc,
                          aca.check_id, aca.check_number, aca.check_date,
                          aip.amount paid_amount, aca.checkrun_name,
                          aca.bank_account_name, aca.bank_account_num,
                          aca.status_lookup_code, asp.segment1 vendor_code,
                          aca.vendor_name, aca.vendor_site_id,
                          assa.attribute14 retek_code, aca.vendor_site_code,
                          aca.doc_sequence_value pay_doc,
                          aip.accounting_date payment_accounting_date,
                          gcc.concatenated_segments pay_accounts,
                          aip.period_name
                     FROM apps.ap_checks_all aca,
                          apps.ap_invoice_payments_all aip,
                          apps.ap_suppliers asp,
                          apps.ap_supplier_sites_all assa,
                          apps.hr_operating_units hou,
                          apps.gl_code_combinations_kfv gcc,
                          apps.ap_invoices_all aia
                    WHERE aca.check_id = aip.check_id
                      --AND aip.invoice_id = 381385161
                        --AND aca.check_id = 156767849
                      AND aca.vendor_id = asp.vendor_id
                      AND aca.vendor_site_id = assa.vendor_site_id
                      AND aca.org_id = hou.organization_id
                      AND aip.invoice_id = aia.invoice_id
                      and asp.vendor_type_lookup_code<>'MERCHANDISE'
                      --AND TRUNC (aca.check_date) BETWEEN '1-OCT-12' AND '31-MAR-13'
                      AND TRUNC (aca.creation_date) = TRUNC (SYSDATE) - 1
--            AND EXISTS (
--                   SELECT 1
--                     FROM apps.ap_invoice_distributions_all a
--                    WHERE a.invoice_id = aip.invoice_id
--                      AND TRUNC (a.accounting_date) BETWEEN '1-apr-13'
--                                                        AND '31-mar-14')
                      AND aip.accts_pay_code_combination_id =
                                                       gcc.code_combination_id(+)
                 ORDER BY aca.check_id)
-- AND aca.status_lookup_code != 'VOID')
      LOOP
         INSERT INTO xxabrliproc.xxabrl_payment_details
                     (ou, invoice_id, invoice_num,
                      invoice_date, invoice_amount, amount_paid,
                      inv_doc, check_id, check_number,
                      check_date, paid_amount, checkrun_name,
                      bank_account_name, bank_account_num,
                      status_lookup_code, vendor_code,
                      vendor_name, vendor_site_id, retek_code,
                      vendor_site_code, pay_doc,
                      payment_accounting_date, pay_accounts,
                      period_name, process_flag, importdate
                     )
              VALUES (buf.ou, buf.invoice_id, buf.invoice_num,
                      buf.invoice_date, buf.invoice_amount, buf.amount_paid,
                      buf.inv_doc, buf.check_id, buf.check_number,
                      buf.check_date, buf.paid_amount, buf.checkrun_name,
                      buf.bank_account_name, buf.bank_account_num,
                      buf.status_lookup_code, buf.vendor_code,
                      buf.vendor_name, buf.vendor_site_id, buf.retek_code,
                      buf.vendor_site_code, buf.pay_doc,
                      buf.payment_accounting_date, buf.pay_accounts,
                      buf.period_name, 'N', TRUNC (SYSDATE)
                     );

         COMMIT;
      END LOOP;
   END xxabrl_pay_daily_data;

   PROCEDURE xxabrl_pay_dump
   IS
   BEGIN
      EXECUTE IMMEDIATE 'truncate table  xxabrliproc.xxabrl_payment_details';

      FOR buf IN
         (SELECT DISTINCT hou.short_code ou, aia.invoice_id, aia.invoice_num,
                          aia.invoice_date, aia.invoice_amount,
                          aia.amount_paid, aia.doc_sequence_value inv_doc,
                          aca.check_id, aca.check_number, aca.check_date,
                          aip.amount paid_amount, aca.checkrun_name,
                          aca.bank_account_name, aca.bank_account_num,
                          aca.status_lookup_code, asp.segment1 vendor_code,
                          aca.vendor_name, aca.vendor_site_id,
                          assa.attribute14 retek_code, aca.vendor_site_code,
                          aca.doc_sequence_value pay_doc,
                          aip.accounting_date payment_accounting_date,
                          gcc.concatenated_segments pay_accounts,
                          aip.period_name
                     FROM apps.ap_checks_all aca,
                          apps.ap_invoice_payments_all aip,
                          apps.ap_suppliers asp,
                          apps.ap_supplier_sites_all assa,
                          apps.hr_operating_units hou,
                          apps.gl_code_combinations_kfv gcc,
                          apps.ap_invoices_all aia
                    WHERE aca.check_id = aip.check_id
                      --    AND aip.invoice_id = 381385161
                          --AND aca.check_id = 156767849
                      AND aca.vendor_id = asp.vendor_id
                      AND aca.vendor_site_id = assa.vendor_site_id
                      and asp.vendor_type_lookup_code<>'MERCHANDISE'
                       AND aca.org_id = hou.organization_id
                      AND aip.invoice_id = aia.invoice_id
                     -- AND TRUNC (aip.accounting_date) >= '1-apr-12'
--            AND EXISTS (
--                   SELECT 1
--                     FROM apps.ap_invoice_distributions_all a
--                    WHERE a.invoice_id = aip.invoice_id
                      AND TRUNC (aip.accounting_date) >= '01-jul-17'
                      AND aip.accts_pay_code_combination_id =
                                                       gcc.code_combination_id(+)
                 ORDER BY aca.check_id)
-- AND aca.status_lookup_code != 'VOID')
      LOOP
         INSERT INTO xxabrliproc.xxabrl_payment_details
                     (ou, invoice_id, invoice_num,
                      invoice_date, invoice_amount, amount_paid,
                      inv_doc, check_id, check_number,
                      check_date, paid_amount, checkrun_name,
                      bank_account_name, bank_account_num,
                      status_lookup_code, vendor_code,
                      vendor_name, vendor_site_id, retek_code,
                      vendor_site_code, pay_doc,
                      payment_accounting_date, pay_accounts,
                      period_name, process_flag, importdate
                     )
              VALUES (buf.ou, buf.invoice_id, buf.invoice_num,
                      buf.invoice_date, buf.invoice_amount, buf.amount_paid,
                      buf.inv_doc, buf.check_id, buf.check_number,
                      buf.check_date, buf.paid_amount, buf.checkrun_name,
                      buf.bank_account_name, buf.bank_account_num,
                      buf.status_lookup_code, buf.vendor_code,
                      buf.vendor_name, buf.vendor_site_id, buf.retek_code,
                      buf.vendor_site_code, buf.pay_doc,
                      buf.payment_accounting_date, buf.pay_accounts,
                      buf.period_name, 'D', TRUNC (SYSDATE)
                     );

         COMMIT;
      END LOOP;
   END xxabrl_pay_dump;
END xxabrl_pay_details; 
/


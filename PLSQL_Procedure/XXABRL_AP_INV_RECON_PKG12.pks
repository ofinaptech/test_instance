CREATE OR REPLACE PACKAGE APPS.XXABRL_AP_INV_RECON_PKG12 AS
PROCEDURE XXABRL_AP_INV_RECON_PROC12(ERRBUFF             OUT VARCHAR2,
                                RETCODE             OUT NUMBER,
                                P_SOURCE            IN  VARCHAR2,
                                        P_OPERATING_UNIT    IN  VARCHAR2,
                                P_INV_DATE_FROM     IN VARCHAR2,
                                P_INV_DATE_TO       IN VARCHAR2
                                );
end XXABRL_AP_INV_RECON_PKG12; 
/


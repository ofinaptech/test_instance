CREATE OR REPLACE PACKAGE BODY APPS.XXABRL_NAV_HM_SCHEDULER_REQ1 IS

/*
=========================================================================================================
||   Filename   : XXABRL_NAV_HM_SCHEDULER_REQ1.sql
||   Description : Script is used to Load and validate AP and AR Data 
||
||   Version     Date            Author              Modification
||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
||   1.0.0       23-sep-2008    Hansraj Kasana       New Development
||   1.0.1       02-aug-2009    SHAILESH BHARAMBE    Added the logic for Reim to AP automation
||   1.0.2       23-sep-2009    SHAILESH BHARAMBE    Changes in AR NAVISAION sysdate - 2
||   1.0.3       30-sep-2009    Shailesh Bharambe    Added org_id parameter in request set requests calling
||   1.0.4       06-oct-2009    Shailesh Bharambe    Changed the ar file picking logic made it as sysdate-1 
     1.0.5       24-Aug-2012    Amresh Chutke        Changed the Program Name from XXABRL_CONCPROG_SCHEDULER_PKG
                                                     to XXABRL_NAV_HM_SCHEDULER_REQ1 (more appropriate) and commented
||     
||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
||
||   Usage : This script is used to load data from FTP where data from Navision HM is generated 
            for Payables and Receivables.
            After the Data load, its validated and and inserted into Interface table and 
            Standard Import Program will post the entries to Base.
||
========================================================================================================*/
/*  This request is scheduled between 08:00 PM and 10:00 PM, the same day when the file is generated on FTP ,
    as this will pick up files based on SYSDATE logic*/
    /*  Modified by Amresh on 24 Aug 2012, as file name contains sysdate 
             and this request is scheduled between 08:00 PM and 10:00 PM */

  PROCEDURE Print_log(
                      p_str IN VARCHAR2,
                      p_debug_flag IN VARCHAR2
                      )
  AS
  BEGIN
       IF p_debug_flag = 'Y' THEN
          Fnd_File.PUT_LINE(Fnd_File.LOG,p_str);
       END IF;

       IF p_debug_flag = 'O' THEN
          Fnd_File.PUT_LINE(Fnd_File.OUTPUT,p_str);
       END IF;
  END;

  PROCEDURE CONCPROG_SCHEDULER(errbuf   OUT VARCHAR2,
                               RetCode  OUT NUMBER,
                               p_module IN VARCHAR2, --AR_NAVISION or AP_NAVISION or ...
                               p_Run_Date IN VARCHAR2, --Backlog
                               p_ser IN VARCHAR2
                             --  p_gl_date VARCHAR2 -- added by shailesh on 28 feb 2009 gl_date
                               ) AS
    v_File_Name     VARCHAR2(250);
    conc_request_id NUMBER;
    stop_program EXCEPTION;
    success BOOLEAN;
    v_short_code    VARCHAR2(25);
    v_L_File        VARCHAR2(250);
    v_D_File        VARCHAR2(250);
    v_T_File        VARCHAR2(250);
    v_H_File        VARCHAR2(250);
    v_errmsg        VARCHAR2(250);
    v_Print_log     VARCHAR2(1) :='Y';
    v_DC_CODE       VARCHAR2(15);
    v_ddtt          VARCHAR2(15);
    v_request_id    VARCHAR2(15);
    v_batch_source_id NUMBER;
    mail_request_id   NUMBER;
    v_log_file_name   VARCHAR2(150);
    t_module          VARCHAR2(150);
    v_module          VARCHAR2(150);
p_gl_date  varchar2 (30):='';
    --AP
    V_Batch_Name      VARCHAR2(150);
    v_IN_DIR        VARCHAR2(300) :='/ebiz/shared_u01/abrlftp/ftpdir/interface/Navision/' ; --'$NAV_DATA/interface/Navision/';
    v_org_id        VARCHAR2(5);
    V_GL_DATE VARCHAR2(40);
    
  BEGIN

      BEGIN


      v_org_id  := Fnd_Profile.VALUE('ORG_ID');
      Print_log('Running ORG ID: '||v_org_id,'Y');

        SELECT REPLACE(p_module,' ','') INTO v_module FROM dual; -- AP_NAVISION,,
        SELECT SUBSTR(v_module,1,INSTR(v_module,',')-1) INTO t_module FROM dual; -- AP_NAVISION
        SELECT REPLACE(SUBSTR(p_module,INSTR(p_module,',')+1),'_',' ') INTO v_short_code FROM dual; --  , 

        Print_log('Combined Param: '||v_module,'Y');
        Print_log('Derived Module: '||t_module,'Y');
        Print_log('Derived ORG Short Code: '||v_short_code,'Y');

        t_module := v_module;

        IF v_org_id IS NOT NULL THEN

           BEGIN
             SELECT
             short_code INTO v_short_code
             FROM hr_operating_units
             WHERE organization_id = v_org_id;

           EXCEPTION
           WHEN NO_DATA_FOUND THEN
                v_errmsg := 'Org ID/Organization name not found';
                RAISE stop_program;
           END;

        END IF;



        Print_Log('P_Module Paramter: '||NVL(t_module,'NULL'),v_Print_log);

        --********************************
        --Navision AR Automation
        --********************************
        IF t_module = 'AR_NAVISION' THEN

         IF v_short_code IS NOT NULL THEN
           BEGIN
             SELECT DECODE(p_ser,'','',P_SER||'_')||DESCRIPTION
               INTO v_DC_CODE
               FROM FND_LOOKUP_VALUES_VL
              WHERE SYSDATE BETWEEN NVL(START_DATE_ACTIVE, SYSDATE) AND
                    NVL(END_DATE_ACTIVE, SYSDATE)
                AND ENABLED_FLAG = 'Y'
                AND LOOKUP_TYPE = 'NAV_DC_OU_MAP'
                AND MEANING = v_short_code;


           EXCEPTION
             WHEN NO_DATA_FOUND THEN
               v_errmsg := 'Error:DC Code Mapping not defined in Lookup-NAV_DC_OU_MAP(AR) :'||v_short_code;
               RAISE stop_program;
           END;
        END IF;


        BEGIN
          SELECT batch_source_id INTO v_batch_source_id
          FROM
          ra_batch_sources_all
          WHERE
          BATCH_SOURCE_TYPE    ='FOREIGN'
          AND STATUS =    'A'
          AND NAME ='Navision'
          AND org_id = v_org_id;
           EXCEPTION
           WHEN NO_DATA_FOUND THEN
                v_errmsg := 'Navision batch source not found';
                RAISE stop_program;
           WHEN TOO_MANY_ROWS THEN
                v_errmsg := 'Multiple Navision batch source found';
                RAISE stop_program;
           WHEN OTHERS THEN
                v_errmsg := 'Exception in Navision batch source ';
                RAISE stop_program;
        END;

        Print_Log('Stating AR_NAVISION...',v_Print_log);
        v_IN_DIR := v_IN_DIR||'AR/';
          BEGIN

            success := fnd_submit.set_request_set('XXABRL','XXABRL_NAV_AR_REQ_SET');
            --Start Request Set

            -- ADDED BY SHAILESH ON 24 SEP 2009 
            -- CHANGES DONE AS A SYSDATE - 2 
            --Generate Data File Names
            --added by shailesh on 06-oct-2009  change the ar file pickup date sysdate -1 
            SELECT 'NAV_AR_'||v_DC_CODE||'_L_'||NVL(TO_CHAR(TO_DATE(p_Run_Date,'DD-MON-YYYY'),'DDMMYY'),TO_CHAR(SYSDATE-1,'DDMMYY'))||'.csv' INTO v_L_File FROM dual;
            SELECT 'NAV_AR_'||v_DC_CODE||'_D_'||NVL(TO_CHAR(TO_DATE(p_Run_Date,'DD-MON-YYYY'),'DDMMYY'),TO_CHAR(SYSDATE-1,'DDMMYY'))||'.csv' INTO v_D_File FROM dual;
            SELECT 'NAV_AR_'||v_DC_CODE||'_T_'||NVL(TO_CHAR(TO_DATE(p_Run_Date,'DD-MON-YYYY'),'DDMMYY'),TO_CHAR(SYSDATE-1,'DDMMYY'))||'.csv' INTO v_T_File FROM dual;

    --- ADDED BY SHAILESH ON 07-MAR 2009

            IF p_Run_Date IS NULL THEN
               select TO_CHAR(TRUNC(SYSDATE)-1,'YYYY/MM/DD HH24:MI:SS') INTO V_GL_DATE from dual;
            ELSE
                -- select TO_CHAR(TO_DATE(p_Run_Date,'DD-MON-YYYY') INTO V_GL_DATE from dual;
                  select TO_CHAR(TO_DATE(p_Run_Date,'DD-MON-YYYY'),'YYYY/MM/DD HH24:MI:SS') INTO V_GL_DATE from dual;
            END IF;

            Print_Log('GL DATE :- ' || V_GL_DATE,v_Print_log);
            --Submit Line Loader
            IF (success) THEN
               Print_Log('Submitting AR-Line Loader...',v_Print_log);
               success := fnd_submit.submit_program('XXABRL',
                                             'XXABRLARINVOICELINE', --XXABRL Navision AR Invoice Line Loader Program
                                             'STAGE5',
                                             v_IN_DIR||v_L_File);

              IF (NOT success) THEN
                 v_errmsg := 'Error:in AR-Line Loader Submit';
                 RAISE stop_program;
              END IF;

            END IF;

            --Submit Dist Loader
            IF (success) THEN
               Print_Log('Submitting AR-Dist Loader...',v_Print_log);
               success := fnd_submit.submit_program('XXABRL',
                                             'XXABRLARINVOICEDIST', --XXABRL Navision AR Invoice Distribution Loader Program
                                             'STAGE6',
                                             v_IN_DIR||v_D_File);

              IF (NOT success) THEN
                 v_errmsg := 'Error:in AR-Dist Loader Submit';
                 RAISE stop_program;
              END IF;

            END IF;

            --Submit Tender Loader
            IF (success) THEN
               Print_Log('Submitting AR-Tender Loader...',v_Print_log);
               success := fnd_submit.submit_program('XXABRL',
                                             'XXABRLNAVIARRECEIPTLDR', --XXABRL Navision AR Receipts Loader Program
                                             'STAGE7',
                                             v_IN_DIR||v_T_File);

              IF (NOT success) THEN
                 v_errmsg := 'Error:in AR-Tender Loader Submit';
                 RAISE stop_program;
              END IF;

            END IF;

            --Submit File Move
            IF (success) THEN

              fnd_file.put_line(FND_FILE.LOG, 'Submitting File_Move...');

              success := fnd_submit.submit_program('XXABRL',
                                                   'XXABRL_NAVITOAR_FM', --XXABRL Navision To AR - File Move Program
                                                   'STAGE10',
                                                   v_L_File,
                                                   v_D_File,
                                                   v_T_File);
              IF (NOT success) THEN
                 v_errmsg := 'Error:in File Move Submit';
                 RAISE stop_program;
              END IF;

            END IF;

            --AR Validation Program (Validation and Interface)
            IF (success) THEN

              fnd_file.put_line(FND_FILE.LOG, 'Submitting Navision AR Validation/Interface...');

              success := fnd_submit.submit_program('XXABRL',
                                                   'XXABRL_NAV_AR_INV_IMP_PKG', --XXABRL Navision AR Invoice Conversion Program
                                                   'STAGE20',
                                                   -- added by shailesh bharambe on 24 sep 2009
                                                   -- paas org_id parameter
                                                    v_org_id,
                                                   'ABRL_NAVISION',
                                                   'N',
                                                   V_GL_DATE  --p_gl_date -- added by shailesh on 28 feb 2009 gl date wise run
                                                   );
              IF (NOT success) THEN
                 v_errmsg := 'Error:in Navision AR Validation/Interface Submit';
                 RAISE stop_program;
              END IF;
            END IF;

            --AR Autoinvoice Master Program
            IF (success) THEN

          fnd_file.put_line(FND_FILE.LOG, 'Submitting Autoinvoice Master/Import Program...');
    /*
          success := fnd_submit.submit_program('AR',
                                                       'RAXMTR', --'RAXTRX' changed by shailesh,
                                                   'STAGE30',
                                                     1,   --Number of Instances
                                                  null , --Organization 
                                                         -- 'T',
                                                        v_batch_source_id,  --Batch Source Id
                                                           'Navision',  --Batch Source Name
                                                           SYSDATE,   --Default Date
                                                           NULL, --Transaction Flexfield
                                                     NULL, -- Transaction Type
                                                   NULL, --(Low) Bill To Customer Number
                                                          NULL, --(High) Bill To Customer Number
                                                       NULL, --(Low) Bill To Customer Name
                                                     NULL, --(High) Bill To Customer Name
                                                           NULL, --(Low) GL Date
                                                   NULL, --(High) GL Date
                                                       NULL, --(Low) Ship Date 
                                                     NULL, --(High) Ship Date
                                                           NULL, --(Low) Transaction Number
                                                       NULL, --(High) Transaction  Number
                                                     NULL, --(Low) Sales Order Number
                                                           NULL, --(High) Sales Order Number
                                                        NULL, --(Low) Invoice Date
                                                     NULL, --(High) Invoice Date
                                                     NULL, --(Low) Ship To Customer Number
                                                     NULL, --(High) Ship To Customer Number
                                                           NULL, --(Low) Ship To Customer Name
                                                       NULL, --(High) Ship To Customer Name
                                                     NULL, --Base Due Date on Trx Date
                                                            NULL, --Due Date Adjustment Days
                                                         v_org_id  --Ord Id
                                                           --,CHR(0)
                                                     );*/

             success := fnd_submit.submit_program('AR',
                                                       'RAXTRX', -- Autoinvoice Import Program
                                                   'STAGE30',
                                                     'MAIN',
                                                          'T',
                                                        v_batch_source_id,
                                                           'Navision',
                                                           SYSDATE,
                                                           NULL,
                                                     NULL, -- Parameter Data source
                                                   NULL,
                                                          NULL,
                                                       NULL,
                                                     NULL,
                                                           V_GL_DATE, -- GL_DATE FROM 
                                                       V_GL_DATE, -- GL_DATE TO 
                                                     NULL,
                                                           NULL,
                                                       NULL,
                                                     NULL,
                                                           NULL,
                                                        NULL,
                                                     NULL,
                                                     NULL,
                                                     NULL,
                                                           NULL,
                                                       NULL,
                                                     NULL,
                                                            'N',
                                                         'Y',
                                                           NULL,
                                                           v_org_id
                                                         --,CHR(0)
                                                     );

              IF (NOT success) THEN
                 v_errmsg := 'Error:in Submitting Autoinvoice Master Program';
                 RAISE stop_program;
              END IF;
            END IF;

            --AR Receipts Program (Validation and Conversion)
            IF (success) THEN

              fnd_file.put_line(FND_FILE.LOG, 'Submitting Navision AR Receipts Validation/Conversion...');

              success := fnd_submit.submit_program('XXABRL',
                                                   'XXABRL_NAV_AR_RECPT_IMP_PKG', --XXABRL Navision AR Receipts Conversion Program
                                                   'STAGE40',
                                                   'N',
                                                   'ABRL_NAVISION',
                                                   NULL,
                                                   'Y',
                                                   V_GL_DATE,--p_gl_date ---- added by shailesh on 28 feb 2009 gl date wise run
                                                   -- added  by shailesh on 24 sep 2009 
                                                   -- paas org_id parameter
                                                   v_org_id
                                                   );
              IF (NOT success) THEN
                 v_errmsg := 'Error:in Navision AR Receipts Validation/Conversion submit';
                 RAISE stop_program;
              END IF;
            END IF;

            conc_request_id := fnd_submit.submit_set(NULL, FALSE);
            --Submit Request
            COMMIT;

            IF conc_request_id >0 THEN
               fnd_file.put_line(FND_FILE.LOG,
                              'Navision AR Request-Submited Successfully ID: ' ||conc_request_id);


           EXECUTE IMMEDIATE 'Truncate table xxabrl.Mailer_Request_AR';
          -- updated by shailesh on 19 feb 2009 table name updated
            INSERT INTO Mailer_Request_AR VALUES(conc_request_id,v_L_File,t_module,'P');
            -- original code for above changes
           --  insert into Mailer_Request_AR values(conc_request_id,v_L_File,t_module,'P');
            COMMIT;
            END IF;

          EXCEPTION
            WHEN OTHERS THEN
              fnd_file.put_line(FND_FILE.LOG,v_errmsg);
              fnd_file.put_line(FND_FILE.LOG,
                                'Exception in Navision AR Request-Submit: ' || SQLERRM);

          END;

        END IF;

        --*****************************
        --AP Navision Automation
        --*****************************

        IF t_module = 'AP_NAVISION' THEN

        Print_Log('Stating AP_NAVISION...',v_Print_log);

         IF v_short_code IS NOT NULL THEN
           BEGIN

             SELECT DESCRIPTION || NVL(TO_CHAR(p_ser),'')
               INTO v_DC_CODE
               FROM FND_LOOKUP_VALUES_VL
              WHERE SYSDATE BETWEEN NVL(START_DATE_ACTIVE, SYSDATE) AND
                    NVL(END_DATE_ACTIVE, SYSDATE)
                AND ENABLED_FLAG = 'Y'
                AND LOOKUP_TYPE = 'NAV_AP_DC_OU_MAP'
                AND MEANING = v_short_code;


           EXCEPTION
             WHEN NO_DATA_FOUND THEN
               v_errmsg := 'Error:DC Code Mapping not defined in Lookup-NAV_AP_DC_OU_MAP(AP) :'||v_short_code;
               RAISE stop_program;
           END;
        END IF;

          BEGIN
            v_IN_DIR := v_IN_DIR||'AP/';
            success := fnd_submit.set_request_set('XXABRL','XXABRL_INV_LOAD_REQSET_NV');

            --Generate Data File Names
            -- Modified by shailesh on 15 sep 2009 previous logic to take data as sysdate - 2 
            -- now its change to sysdate - 1
            /*  Modified by Amresh on 24 Aug 2012, as file name contains sysdate 
             and this request is scheduled between 08:00 PM and 10:00 PM */
            
            SELECT 'NAV_AP_'||v_DC_CODE||'_H_'||TO_CHAR(SYSDATE,'DDMMYY')||'.csv' INTO v_L_File FROM dual;
            SELECT 'NAV_AP_'||v_DC_CODE||'_L_'||TO_CHAR(SYSDATE,'DDMMYY')||'.csv' INTO v_D_File FROM dual;


            --Submit Header Loader
            IF (success) THEN
               Print_Log('Submitting AP-Header Loader...',v_Print_log);
               success := fnd_submit.submit_program('XXABRL',
                                             'XXABRL_AP_INVOICE_INT_LOAD', --ABRL Payables Interface Invoice Header Load
                                             'Stage 10',
                                             v_IN_DIR||v_L_File);

              IF (NOT success) THEN
                 v_errmsg := 'Error:in AP-Header Loader Submit';
                 RAISE stop_program;
              END IF;

            END IF;

            --Submit Line Loader
            IF (success) THEN
               Print_Log('Submitting AP-Line Loader...',v_Print_log);
               success := fnd_submit.submit_program('XXABRL',
                                             'XXABRL_AP_INV_LINES_INT_LOAD', -- ABRL Payables Interface Invoice Lines Load
                                             'Stage 20',
                                             v_IN_DIR||v_D_File);

              IF (NOT success) THEN
                 v_errmsg := 'Error:in AP-Line Loader Submit';
                 RAISE stop_program;
              END IF;

            END IF;


            --Submit File Move
            IF (success) THEN

              fnd_file.put_line(FND_FILE.LOG, 'Submitting File_Move...');

              success := fnd_submit.submit_program('XXABRL',
                                                   'XXABRL_APINVINTFM_NV', -- ABRL Payables Invoice Interface File Move Navision
                                                   'stage 30',
                                                   v_L_File,
                                                   v_D_File
                                                   );
              IF (NOT success) THEN
                 v_errmsg := 'Error:in Navision AP-File Move Submit';
                 RAISE stop_program;
              END IF;

            END IF;

            --AP Validation Program (Validation and Interface)
            IF (success) THEN

              fnd_file.put_line(FND_FILE.LOG, 'Submitting Navision AP Validation/Interface...');

              success := fnd_submit.submit_program('XXABRL',
                                                   'XXABRL_APNAV_PKG_INV', -- ABRL Navision Payable Invoices Validation and Interface
                                                   'Stage 40',
                                                   v_org_id,
                                                   'NAVISION');
              IF (NOT success) THEN
                 v_errmsg := 'Error:in Navision AP Validation/Interface Submit';
                 RAISE stop_program;
              END IF;
            END IF;



           BEGIN
             V_Batch_Name := NULL;

             SELECT 'NAVISION' || '-' || v_short_code || '-' ||
                    TO_CHAR(SYSDATE, 'DD-MON-RRRR')
               INTO V_Batch_Name
               FROM dual;

           EXCEPTION
             WHEN OTHERS THEN
               V_Batch_Name := 'NAVISION' || '-' || TO_CHAR(SYSDATE, 'DD-MON-RRRR');

           END;

           --AP Payable Open Invoice Import Program


            IF (success) THEN

              fnd_file.put_line(FND_FILE.LOG, 'Payable Open Invoice Import Program...');

              success := fnd_submit.submit_program(
                                                   'SQLAP',
                                                   'APXIIMPT', -- Payables Open Interface Import
                                                   'Stage 50',
                                                   v_org_id,        --OU
                                                   'NAVISION',      --Batch Source
                                                   NULL,            --Group
                                                   V_Batch_Name,    --Batch Name
                                                   NULL,            --Hold Name
                                                   NULL,            -- HOld Reason
                                                   NULL,            --GL Date
                                                   'N',             --Purge
                                                   'N',             --Trace Switch
                                                   'N',             --Debug Switch
                                                   'Y',             --Summarize Report
                                                    NULL,           --Commit Batch Size
                                                    NULL,           --User id
                                                    NULL            --Login Id
                                                   );

              IF (NOT success) THEN
                 v_errmsg := 'Error:in Payable Open Invoice Import Program...';
                 RAISE stop_program;
              END IF;
            END IF;


            conc_request_id := fnd_submit.submit_set(NULL, FALSE);
            COMMIT;

            IF conc_request_id >0 THEN
               fnd_file.put_line(FND_FILE.LOG,
                              'Navision AP Request-Submited Successfully ID: ' ||conc_request_id);

            EXECUTE IMMEDIATE 'Truncate table xxabrl.Mailer_Request';
            INSERT INTO Mailer_Request VALUES(conc_request_id,v_L_File,t_module,'P');
            COMMIT;
            END IF;

          EXCEPTION
            WHEN OTHERS THEN
              fnd_file.put_line(FND_FILE.LOG,v_errmsg);
              fnd_file.put_line(FND_FILE.LOG,
                                'Exception in Navision AP Request-Submit: ' || SQLERRM);

          END;
        END IF;


         --*****************************
        --ReIM To AP Automation
        -- added by shailesh on 14-1-09
        --*****************************

        IF t_module = 'ReIM_AP' THEN



        Print_Log('Stating ReIM_AP...',v_Print_log);


          BEGIN
            v_IN_DIR := v_IN_DIR||'ReIM_AP/';
            success := fnd_submit.set_request_set('XXABRL','FNDRSSUB952'); -- XXABRL ReIM To AP Request Set

            --Generate Data File Names

            SELECT 'ABRL_REIM_AP_LDRH'||TO_CHAR(SYSDATE-1,'DDMMYY')||'.csv' INTO v_L_File  FROM dual;
            SELECT 'ABRL_REIM_AP_LDRD'||TO_CHAR(SYSDATE-1,'DDMMYY')||'.csv' INTO v_D_File   FROM dual;


            --Submit Header Loader
            IF (success) THEN
               Print_Log('Submitting REIM_AP-Header Loader...',v_Print_log);
               success := fnd_submit.submit_program('XXABRL',
                                             'XXABRL_REIM_AP_INV_HDR', -- ABRL ReIM to AP Invoice Headers Load
                                             'Stage 10',
                                             v_IN_DIR||v_L_File);

              IF (NOT success) THEN
                 v_errmsg := 'Error:in ReIM_AP-Header Loader Submit';
                 RAISE stop_program;
              END IF;

            END IF;

            --Submit Line Loader
            IF (success) THEN
               Print_Log('Submitting AP-Line Loader...',v_Print_log);
               success := fnd_submit.submit_program('XXABRL',
                                             'XXABRL_REIM_AP_INV_LINES', -- ABRL ReIM To AP Invoice Lines Load
                                             'Stage 20',
                                             v_IN_DIR||v_D_File);

              IF (NOT success) THEN
                 v_errmsg := 'Error:in ReIM_AP-Line Loader Submit';
                 RAISE stop_program;
              END IF;

            END IF;


            --Submit Header File Move
            IF (success) THEN

              fnd_file.put_line(FND_FILE.LOG, 'Submitting Header File_Move...');

              success := fnd_submit.submit_program('XXABRL',
                                                   'XXABRLREIMTOAP_H_FM', -- XXABRL ReIM TO AP - Header File Move Program
                                                   'stage 30',
                                                   v_L_File
                                                   );
              IF (NOT success) THEN
                 v_errmsg := 'Error:in Navision ReIM_AP-File Header Move Submit';
                 RAISE stop_program;
              END IF;

            END IF;

            --Submit Line File Move
            IF (success) THEN

              fnd_file.put_line(FND_FILE.LOG, 'Submitting Line  File_Move...');

              success := fnd_submit.submit_program('XXABRL',
                                                   'XXABRLREIMTOAP_D_FM', -- XXABRL ReIM TO AP - Line File Move Program
                                                   'stage 40',
                                                   v_D_File
                                                   );
              IF (NOT success) THEN
                 v_errmsg := 'Error:in ReIM_AP-File Line Move Submit';
                 RAISE stop_program;
              END IF;

            END IF;


            --AP Validation Program (Validation and Interface)
            IF (success) THEN

              fnd_file.put_line(FND_FILE.LOG, 'Submitting ReIM_AP Validation/Interface...');

              success := fnd_submit.submit_program('XXABRL',
                                                   'XXABRL_REIM_AP_INV_INTERFACE', -- ABRL ReIM To AP Invoice Interface
                                                   'Stage 50'
                                                  );
              IF (NOT success) THEN
                 v_errmsg := 'Error:in ReIM_AP Validation/Interface Submit';
                 RAISE stop_program;
              END IF;
            END IF;




            IF (success) THEN

              fnd_file.put_line(FND_FILE.LOG, 'Payable Open Invoice Import Program...');

              success := fnd_submit.submit_program(
                                                   'SQLAP',
                                                   'APXIIMPT', -- Payables Open Interface Import
                                                   'Stage 60',
                                                   NULL,            --OU
                                                   NULL,            --Batch Source
                                                   NULL,            --Group
                                                   'N/A',           --Batch Name
                                                   NULL,            --Hold Name
                                                   NULL,            -- HOld Reason
                                                   NULL,            --GL Date
                                                   'N',             --Purge
                                                   'N',             --Trace Switch
                                                   'N',             --Debug Switch
                                                   'Y',             --Summarize Report
                                                    NULL,           --Commit Batch Size
                                                    NULL,           --User id
                                                    NULL            --Login Id
                                                   );

              IF (NOT success) THEN
                 v_errmsg := 'Error:in Payable Open Invoice Import Program...';
                 RAISE stop_program;
              END IF;
            END IF;




            conc_request_id := fnd_submit.submit_set(NULL, FALSE);
            COMMIT;

            IF conc_request_id >0 THEN
               fnd_file.put_line(FND_FILE.LOG,
                              'ReIM_AP Request-Submited Successfully ID: ' ||conc_request_id);


            EXECUTE IMMEDIATE 'Truncate table xxabrl.Mailer_Request';
            INSERT INTO Mailer_Request VALUES(conc_request_id,v_L_File,t_module,'P');
            COMMIT;
            END IF;





          EXCEPTION
            WHEN OTHERS THEN
              fnd_file.put_line(FND_FILE.LOG,v_errmsg);
              fnd_file.put_line(FND_FILE.LOG,
                                'Exception in ReIM_AP Request-Submit: ' || SQLERRM);

          END;
        END IF;
          --************************************************************

          --******************************
          -- RMS TO GL
          --added by shailesh on 15-1-09
          --******************************
        IF t_module = 'RMS_GL' THEN



        Print_Log('Stating RMS_GL...',v_Print_log);


          BEGIN
            v_IN_DIR := v_IN_DIR||'RMS_GL/';

            success := fnd_submit.set_request_set('XXABRL','FNDRSSUB932');

            --Generate Data File Names

           SELECT 'ABRL_RMS_GL_LDR'||TO_CHAR(SYSDATE,'DDMMYY')||'.csv' INTO v_L_File FROM Dual;



            --Submit File Loader for GL
            IF (success) THEN
               Print_Log('Submitting RMS_GL Loader...',v_Print_log);
               success := fnd_submit.submit_program('XXABRL',
                                             'XXABRL_GL_INT_LDR', -- ABRL RMS to GL Loader
                                             'Stage 10',
                                             v_IN_DIR||v_L_File);

              IF (NOT success) THEN
                 v_errmsg := 'Error:in RMS_GL Loader Submit';
                 RAISE stop_program;
              END IF;

            END IF;



            --Submit File Move
            IF (success) THEN

              fnd_file.put_line(FND_FILE.LOG, 'Submitting Header File_Move...');

              success := fnd_submit.submit_program('XXABRL',
                                                   'XXABRLRMSTOGL_FM', -- XXABRL RMS TO GL - File Move Program
                                                   'stage 20',
                                                   v_L_File
                                                   );
              IF (NOT success) THEN
                 v_errmsg := 'Error:in RMS_GL -File Move Submit';
                 RAISE stop_program;
              END IF;

            END IF;




            --AP Validation Program (Validation, Interface and GL Import Program)
            IF (success) THEN

              fnd_file.put_line(FND_FILE.LOG, 'Submitting Navision RMS_GL Validation/Interface/Import...');

              success := fnd_submit.submit_program('XXABRL',
                                                   'XXABRL_GL_INTER_PKG', -- ABRL RMS to GL Interface Program
                                                   'Stage 30'
                                                  );
              IF (NOT success) THEN
                 v_errmsg := 'Error:in RMS_GL Validation/Interface Submit';
                 RAISE stop_program;
              END IF;
            END IF;


            conc_request_id := fnd_submit.submit_set(NULL, FALSE);
            COMMIT;

            IF conc_request_id >0 THEN
               fnd_file.put_line(FND_FILE.LOG,
                              'RMS_GL Request-Submited Successfully ID: ' ||conc_request_id);


            EXECUTE IMMEDIATE 'Truncate table xxabrl.Mailer_Request';
            INSERT INTO Mailer_Request VALUES(conc_request_id,v_L_File,t_module,'P');
            COMMIT;
            END IF;





          EXCEPTION
            WHEN OTHERS THEN
              fnd_file.put_line(FND_FILE.LOG,v_errmsg);
              fnd_file.put_line(FND_FILE.LOG,
                                'Exception in RMS_GL Request-Submit: ' || SQLERRM);

          END;
        END IF;
        ---************************************************************************************




      EXCEPTION
         WHEN stop_program THEN
         Print_log(v_errmsg,v_Print_log);
         RetCode :=1;
      END;

  END CONCPROG_SCHEDULER;

END XXABRL_NAV_HM_SCHEDULER_REQ1; 
/


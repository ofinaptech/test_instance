CREATE OR REPLACE PACKAGE BODY APPS.XXABRL_CCID_PKG AS
PROCEDURE XXABRL_CCID_PROC (ERRBUFF OUT VARCHAR2,
                                    RETCODE OUT number, 
                                    P_FROM_SEGMENT1     IN VARCHAR2,
                                    P_TO_SEGMENT1     IN VARCHAR2,
                                   P_FROM_SEGMENT2     IN VARCHAR2,
                                   P_TO_SEGMENT2     IN VARCHAR2,
                                   P_FROM_SEGMENT3     IN VARCHAR2,
                                   P_TO_SEGMENT3     IN VARCHAR2,
                                   P_FROM_SEGMENT4     IN VARCHAR2,
                                   P_TO_SEGMENT4     IN VARCHAR2,
                                   P_FROM_SEGMENT5     IN VARCHAR2,
                                   P_TO_SEGMENT5     IN VARCHAR2,
                                   P_FROM_SEGMENT6     IN VARCHAR2,
                                   P_TO_SEGMENT6     IN VARCHAR2,
                                   P_FROM_SEGMENT7     IN VARCHAR2,
                                   P_TO_SEGMENT7    IN VARCHAR2,
                                   P_FROM_SEGMENT8     IN VARCHAR2,  
                                   P_TO_SEGMENT8     IN VARCHAR2,                            
                                    P_FROM_UPDATE IN VARCHAR2,
                                    P_TO_UPDATE IN VARCHAR2,                                 
                                    P_ENABLED_FLAG IN CHAR                                    
                                    ) IS
   CURSOR C1 IS 
           select ffvv.DESCRIPTION                    STATE_SBU
   ,gck.CONCATENATED_SEGMENTS        CONCATENATED_SEGMENTS
   , gck.CODE_COMBINATION_ID         CODE_COMBINATION_ID
   , gck.LAST_UPDATE_DATE            LAST_UPDATE_DATE
 ,gck.ENABLED_FLAG                   ENABLED_FLAG
 ,gck.DETAIL_POSTING_ALLOWED         DETAIL_POSTING_ALLOWED
 ,gck.DETAIL_BUDGETING_ALLOWED       DETAIL_BUDGETING_ALLOWED
 ,gck.START_DATE_ACTIVE              START_DATE_ACTIVE
 ,gck.END_DATE_ACTIVE                END_DATE_ACTIVE
 from gl_code_combinations_kfv gck,
 fnd_flex_values_vl ffvv, fnd_flex_value_sets ffvs
 where ffvs.flex_value_set_name = 'ABRL_GL_State_SBU'
           AND ffvs.flex_value_set_id = ffvv.flex_value_set_id
           and ffvv.flex_value= gck.segment3          
           AND gck.ENABLED_FLAG=NVL(P_ENABLED_FLAG,gck.ENABLED_FLAG)
           AND TO_date(gck.LAST_UPDATE_DATE,'DD-MON-YY') 
BETWEEN NVL(P_FROM_UPDATE,TO_CHAR(gck.LAST_UPDATE_DATE,'dd-mon-yy'))
AND NVL(P_TO_UPDATE,TO_CHAR(gck.LAST_UPDATE_DATE,'dd-mon-yy'))
AND GCK.segment1 BETWEEN NVL(P_FROM_SEGMENT1,GCK.segment1)AND NVL(P_TO_SEGMENT1,GCK.segment1)
AND GCK.segment2 BETWEEN NVL(P_FROM_SEGMENT2,GCK.segment2)AND NVL(P_TO_SEGMENT2,GCK.segment2)
AND GCK.segment3 BETWEEN NVL(P_FROM_SEGMENT3,GCK.segment3) AND NVL(P_TO_SEGMENT3,GCK.segment3)
AND GCK.segment4 BETWEEN NVL(P_FROM_SEGMENT4,GCK.segment4) AND NVL(P_TO_SEGMENT4,GCK.segment4)
AND GCK.segment5 BETWEEN NVL(P_FROM_SEGMENT5,GCK.segment5) AND NVL(P_TO_SEGMENT5,GCK.segment5)
AND GCK.segment6 BETWEEN NVL(P_FROM_SEGMENT6,GCK.segment6) AND NVL(P_TO_SEGMENT6,GCK.segment6)
AND GCK.segment7 BETWEEN NVL(P_FROM_SEGMENT7,GCK.segment7) AND NVL(P_TO_SEGMENT7,GCK.segment7)
AND GCK.segment8 BETWEEN NVL(P_FROM_SEGMENT8,GCK.segment8) AND NVL(P_TO_SEGMENT8,GCK.segment8); 
 BEGIN
         Fnd_File.put_line(Fnd_File.OUTPUT,'ABRL CCIDs LIST REPORT');
        Fnd_File.put_line(Fnd_File.OUTPUT,' ');
         Fnd_File.put_line(Fnd_File.OUTPUT,
                      CHR(9) || CHR(9) || CHR(9) || 'As on Date' || CHR(9) ||
                      sysdate || CHR(9));
          Fnd_File.put_line(Fnd_File.OUTPUT,' ');
        Fnd_File.put_line(Fnd_File.OUTPUT,'REPORT PARAMETRS ');               
                          Fnd_File.put_line(Fnd_File.OUTPUT,
                      CHR(9) || CHR(9) || CHR(9) || ' CO Low' || CHR(9) ||
                      P_FROM_SEGMENT1|| CHR(9));
                       Fnd_File.put_line(Fnd_File.OUTPUT,
                      CHR(9) || CHR(9) || CHR(9) || ' CO High' || CHR(9) ||
                      P_TO_SEGMENT1|| CHR(9));
                      Fnd_File.put_line(Fnd_File.OUTPUT,
                      CHR(9) || CHR(9) || CHR(9) || 'CC Low' || CHR(9) ||
                      P_FROM_SEGMENT2 || CHR(9));
                       Fnd_File.put_line(Fnd_File.OUTPUT,
                      CHR(9) || CHR(9) || CHR(9) || 'CC High' || CHR(9) ||
                      P_TO_SEGMENT2 || CHR(9));
                      Fnd_File.put_line(Fnd_File.OUTPUT,
                      CHR(9) || CHR(9) || CHR(9) || ' State SBU Low' || CHR(9) ||
                      P_FROM_SEGMENT3|| CHR(9));
                      Fnd_File.put_line(Fnd_File.OUTPUT,
                      CHR(9) || CHR(9) || CHR(9) || ' State SBU High' || CHR(9) ||
                      P_TO_SEGMENT3|| CHR(9));                           
                      Fnd_File.put_line(Fnd_File.OUTPUT,
                      CHR(9) || CHR(9) || CHR(9) || ' Location Low' || CHR(9) ||
                      P_FROM_SEGMENT4 || CHR(9));
                       Fnd_File.put_line(Fnd_File.OUTPUT,
                      CHR(9) || CHR(9) || CHR(9) || ' Location High' || CHR(9) ||
                      P_TO_SEGMENT4 || CHR(9));
                        Fnd_File.put_line(Fnd_File.OUTPUT,
                      CHR(9) || CHR(9) || CHR(9) || ' Merchandize Low' || CHR(9) ||
                      P_FROM_SEGMENT5|| CHR(9));
                      Fnd_File.put_line(Fnd_File.OUTPUT,
                      CHR(9) || CHR(9) || CHR(9) || ' Merchandize High' || CHR(9) ||
                      P_TO_SEGMENT5|| CHR(9));
                      Fnd_File.put_line(Fnd_File.OUTPUT,
                      CHR(9) || CHR(9) || CHR(9) || ' Account Low' || CHR(9) ||
                      P_FROM_SEGMENT6 || CHR(9));
                       Fnd_File.put_line(Fnd_File.OUTPUT,
                      CHR(9) || CHR(9) || CHR(9) || ' Account High' || CHR(9) ||
                      P_TO_SEGMENT6 || CHR(9));
                       Fnd_File.put_line(Fnd_File.OUTPUT,
                      CHR(9) || CHR(9) || CHR(9) || ' Intercompany Low' || CHR(9) ||
                      P_FROM_SEGMENT7 || CHR(9));
                        Fnd_File.put_line(Fnd_File.OUTPUT,
                      CHR(9) || CHR(9) || CHR(9) || ' Intercompany High' || CHR(9) ||
                      P_TO_SEGMENT7 || CHR(9));
                       Fnd_File.put_line(Fnd_File.OUTPUT,
                      CHR(9) || CHR(9) || CHR(9) || ' Spare Low' || CHR(9) ||
                      P_FROM_SEGMENT8 || CHR(9));
                       Fnd_File.put_line(Fnd_File.OUTPUT,
                      CHR(9) || CHR(9) || CHR(9) || ' Spare High' || CHR(9) ||
                      P_TO_SEGMENT8 || CHR(9));
     Fnd_File.put_line(Fnd_File.OUTPUT,
                      CHR(9) || CHR(9) || CHR(9) || 'Updated From Date' || CHR(9) ||
                      P_FROM_UPDATE || CHR(9));
     Fnd_File.put_line(Fnd_File.OUTPUT,
                    CHR(9) || CHR(9) || CHR(9) || 'Updated To Date' || CHR(9) ||
                    P_TO_UPDATE || CHR(9));
       Fnd_File.put_line(Fnd_File.OUTPUT,
                    CHR(9) || CHR(9) || CHR(9) || 'Enabled Flag' || CHR(9) ||
                     P_ENABLED_FLAG  || CHR(9)); 
		   Fnd_File.put_line(Fnd_File.OUTPUT,' ');
		      Fnd_File.put_line(Fnd_File.OUTPUT,' ');
          fnd_File.put_line(Fnd_File.OUTPUT,
                          'State SBU'          || CHR(9) ||
                          'Code Combination'          || CHR(9) ||
                          'CCID No'      || CHR(9) ||
                          'Last Updated on'        || CHR(9) ||
						  'Enabled'        || CHR(9) ||
						  'Allow Posting'       || CHR(9) ||
						  'Allow Budgeting'    || CHR(9) ||
						  'Effective Date From'         || CHR(9) ||
						  'Effective Date To'         || CHR(9) 
						 );
             for V in C1 loop
             Fnd_File.put_line(Fnd_File.OUTPUT,
                      V.STATE_SBU    || CHR(9) ||
                      V.CONCATENATED_SEGMENTS   || CHR(9) ||
                      V.CODE_COMBINATION_ID || CHR(9) ||
                      V.LAST_UPDATE_DATE  || CHR(9) ||
					  V.ENABLED_FLAG || CHR(9) ||
					  V.DETAIL_POSTING_ALLOWED   || CHR(9) ||
					  V.DETAIL_BUDGETING_ALLOWED  || CHR(9) ||
					  V.START_DATE_ACTIVE    || CHR(9) ||
					   V.END_DATE_ACTIVE   || CHR(9)
					   );
             end loop;
            END XXABRL_CCID_PROC;
             END XXABRL_CCID_PKG;
/


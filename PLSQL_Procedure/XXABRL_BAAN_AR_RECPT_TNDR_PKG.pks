CREATE OR REPLACE PACKAGE APPS.xxabrl_BAAN_ar_recpt_tndr_pkg
IS
   PROCEDURE receipt_validate (
      p_errbuf             OUT      VARCHAR2,
      p_retcode            OUT      NUMBER,
      p_action             IN       VARCHAR2,
      p_data_source        IN       VARCHAR2,
      p_receipt_to_apply   IN       VARCHAR2,
      p_apply_all_flag     IN       VARCHAR2,
      p_org_id             IN       NUMBER
   );

   PROCEDURE receipt_insert (p_org_id IN NUMBER, p_data_source IN VARCHAR2);

   PROCEDURE receipt_apply_call (p_ref_number IN VARCHAR2);

   PROCEDURE receipt_apply_data (
      arr_r2                IN       xxabrl_navi_ar_receipt_stg2%ROWTYPE,
      p_rcpt_amt_to_apply            NUMBER,
      x_rcpt_bal_amt        OUT      NUMBER,
      x_exit_flag           OUT      VARCHAR2,
      x_org_id              IN       NUMBER
   );
END xxabrl_BAAN_ar_recpt_tndr_pkg; 
/


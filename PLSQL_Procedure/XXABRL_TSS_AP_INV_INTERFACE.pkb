CREATE OR REPLACE PACKAGE BODY APPS.xxabrl_tss_ap_inv_interface
IS
---
---    OBJECT INFORMATION
---
---    OBJECT NAME : XXABRL_TSS_AP_INV_INTERFACE
---
---    DESCRIPTION : MIGRATION OF INVOICES FROM TOTAL STORES TO ORACLE PAYABLES
---
---    CHANGE RECORD:
---    ---------------------

   ---    VERSION    DATE                AUTHOR                              REMARKS
---    -------------------------------------------------------------------------------
---    1.1         JUL    2008    HANSRAJ - WIPRO        ORIGINAL
---    1.2        NOV    2013    BALA.SK - ABRL        UNIQUE VENDOR SOLUTION
---
---
   PROCEDURE validate_invoices (errbuf OUT VARCHAR2, retcode OUT NUMBER)
   IS
      v_invoice_num                  VARCHAR2 (50)           := NULL;
      v_count                        NUMBER                  := NULL;
      v_data_count                   NUMBER;
      v_inactive_site                NUMBER;
      v_error_flag                   VARCHAR2 (1)            := NULL;
      v_error_line_flag              VARCHAR2 (1)            := NULL;
      v_hdr_err_msg                  VARCHAR2 (1000)         := NULL;
      v_line_err_msg                 VARCHAR2 (1000)         := NULL;
      v_error_almsg                  VARCHAR2 (4000)         := NULL;
      v_vendor_id                    NUMBER (15)             := NULL;
      v_vendor_site_id               NUMBER (15)             := NULL;
      v_vendor_site_code             VARCHAR2 (30)           := NULL;
      v_invoice_currency_code        VARCHAR2 (30)           := NULL;
      v_payment_method_lookup_code   VARCHAR2 (30)           := NULL;
      v_dist_code_concatenated       VARCHAR2 (300)          := NULL;
--        FOLLOWING VARIAVBLE CREATED BY SHAILESH ON 25 OCT 2008 FOR STORING THE DEFAULT ACCOUNTING SEGMENT VALUES FOR VENDOR
      l_dist_code_concatenated       VARCHAR2 (300)          := NULL;
      l_nat_acct                     VARCHAR2 (10);
--        WILL STORE THE TERMS_ID FROM THE VENDOR MASTER SEE THE VALIDATION FOR DETAILS
      l_terms_id                     NUMBER;
--        CAPTURE THE LOCATION FROM DIST_CODE_COMBINITION (SEGMENT4) VALUE
      l_location                     VARCHAR2 (10);
      v_check_flag                   VARCHAR2 (1)            := NULL;
      v_fun_curr                     VARCHAR2 (15)           := NULL;
      v_flag_count                   NUMBER                  := 0;
      v_set_of_books_id              NUMBER
                                    := fnd_profile.VALUE ('GL_SET_OF_BKS_ID');
      v_pay_lookup_code              VARCHAR2 (50)           := NULL;
      v_term_name                    ap_terms_tl.NAME%TYPE;
      v_error_head_count             NUMBER                  := 0;
      v_success_record_count         NUMBER                  := 0;
      v_error_line_count             NUMBER                  := 0;
      v_line_type_lookup_code        VARCHAR2 (25)           := NULL;
      v_inv_line_amt                 NUMBER                  := 0;
    --V_USER_ID                                  NUMBER              :
--    FND_PROFILE.VALUE('USER_ID');
    --V_RESP_ID                                  NUMBER              :
--FND_PROFILE.VALUE('RESP_ID');
    --V_APPL_ID                                  NUMBER              :
--FND_PROFILE.VALUE('RESP_APPL_ID');
      v_inv_type                     VARCHAR2 (30)           := NULL;
      v_total_error_records          NUMBER                  := 0;
      v_line_count                   NUMBER                  := 0;
      v_total_records                NUMBER                  := 0;
      v_sup_status                   VARCHAR2 (1)            := NULL;
      v_dummy_flag                   VARCHAR2 (1)            := NULL;
      v_purchasing_site_flag         VARCHAR2 (1)            := NULL;
      v_rfq_only_site_flag           VARCHAR2 (1)            := NULL;
      p_org_id                       NUMBER                  := NULL;
      p_vendor_site_id               NUMBER (15)             := NULL;
      p_vendor_id                    NUMBER (15)             := NULL;
      p_payment_method_lookup_code   VARCHAR2 (30)           := NULL;
      v_pay_site_flag                VARCHAR2 (1)            := NULL;
      v_pay_count                    NUMBER;
      v_rfq_count                    NUMBER;
      v_pur_count                    NUMBER;
      v_ho_count                     NUMBER;
      v_org_cnt                      NUMBER                  := 0;

---
---    Declaration of 'INVOICES' cursor
---
      CURSOR cur_inv_hdr
      IS
         SELECT aps.ROWID, aps.invoice_num, aps.invoice_type_lookup_code,
                aps.invoice_date, aps.vendor_num, aps.terms_id,
                ROUND (aps.invoice_amount, 2) invoice_amount,
                aps.invoice_currency_code, aps.exchange_date,
                aps.exchange_rate_type, aps.description, aps.gl_date,
                aps.GROUP_ID, aps.vendor_email_address, aps.terms_date,
                aps.accts_pay_code_concatenated, aps.error_flag,
                aps.error_msg, aps.goods_received_date, aps.org_id,
                aps.vendor_site_id
           FROM xxabrl.xxabrl_baan_ap_h_stage aps
          WHERE 1 = 1
            AND TRUNC (gl_date) > '31-MAR-14'
            AND NVL (aps.error_flag, 'N') IN ('N', 'E')
            AND (source='BAAN' OR source='TSS_OPEN_INVOICE') ;

-- AND    APS.INVOICE_NUM IN ('INV_10563');

      /*
                   Cursor  Cur_Inv_Line(P_Inv_Num Varchar2,
                           --P_Vendor_Site_Id Number,
                           P_Vendor_Num Number) Is
                   Select  Als.Rowid,Als.*
                   From       Apps.xxabrl_baan_ap_l_stage Als
                   Where    Nvl(Als.Error_Flag,'N') <>'Y' --In('N','E')
                      And     Als.Invoice_Num = P_Inv_Num
      --            And     Als.Vendor_Site_Id = P_Vendor_Site_Id
                      And     Als.Vendor_Num = P_Vendor_Num;

      */

      ---    Added by shailesh on 25 OCT 2008
---    Above commented cursor replaced by new cursor
      CURSOR cur_inv_line (
         p_inv_num      VARCHAR2,
---                p_vendor_site_id NUMBER,
         p_vendor_num   VARCHAR2,
         p_gl_date      DATE
      )
      IS
         SELECT als.ROWID, als.vendor_num, als.vendor_site_id,
                als.invoice_num, als.line_number, als.line_type_lookup_code,
                als.amount amount, als.accounting_date,
                als.dist_code_concatenated, als.description, als.tds_tax_name,
                als.wct_tax_name, als.service_tax, als.vat
           FROM xxabrl.xxabrl_baan_ap_l_stage als
          WHERE NVL (als.error_flag, 'N') <> 'Y'                 --in('N','E')
            AND als.invoice_num = p_inv_num
            AND als.vendor_num = p_vendor_num
            AND als.accounting_date = p_gl_date;

/*     23rd Feb 2012
Suggested by Bala SK and carried out by Dhiresh More
above modifications have been carried out by incorporating invoice line cursor is taking processed data too for validating in invoice line cursor
to avoid fetching of processed data for a particular vendor as vendors keep repeating the same running serial numbers
every financial year
*/

      /*    01.12.2013    Bala.SK    not required due to Unique Vendor Solution

      CURSOR cur_check_flags(p_vendor_num VARCHAR2,CP_ORG_ID NUMBER) IS
        SELECT asups.org_id,
               asups.vendor_site_id,
               asup.vendor_id,
               asups.payment_method_lookup_code,
               asups.pay_site_flag,
               asups.PURCHASING_SITE_FLAG,
               asups.RFQ_ONLY_SITE_FLAG
          FROM ap_suppliers asup, ap_supplier_sites_all asups
         WHERE ASUP.VENDOR_ID = ASUPS.VENDOR_ID
           AND ((NVL(ASUPS.PAY_SITE_FLAG, 'N') = 'N' AND
               NVL(ASUPS.PURCHASING_SITE_FLAG, 'N') = 'N' AND
               NVL(ASUPS.RFQ_ONLY_SITE_FLAG, 'N') = 'Y') OR
               (NVL(ASUPS.PAY_SITE_FLAG, 'N') = 'N' AND
               NVL(ASUPS.PURCHASING_SITE_FLAG, 'N') = 'Y' AND
               NVL(ASUPS.RFQ_ONLY_SITE_FLAG, 'N') = 'N') OR
               (NVL(ASUPS.PAY_SITE_FLAG, 'N') = 'Y' AND
               NVL(ASUPS.PURCHASING_SITE_FLAG, 'N') = 'N' AND
               NVL(ASUPS.RFQ_ONLY_SITE_FLAG, 'N') = 'N') OR
               (NVL(ASUPS.PAY_SITE_FLAG, 'N') = 'N' AND
               NVL(ASUPS.PURCHASING_SITE_FLAG, 'N') = 'N' AND
               NVL(ASUPS.RFQ_ONLY_SITE_FLAG, 'N') = 'N'))
           AND asup.segment1 = p_vendor_num
           -- ADDED BY SHAILESH ON 24 APR 2009 NEW COL ADDED IN FILE ORG ID SO THE SUPPLER SITE SHOULD COME ON THE BASIS OF ORG ID
           AND ASUPS.ORG_ID = CP_ORG_ID
           AND NVL(TRUNC(asups.INACTIVE_DATE), trunc(SYSDATE)) >= TRUNC(SYSDATE);


      01.12.2013    Bala.SK    not required due to Unique Vendor Solution        */

      ---
---        01.12.2013    Bala.SK    required due to Unique Vendor Solution
---
---
      CURSOR cur_check_flags (
         p_vendor_num        VARCHAR2,
         cp_org_id           NUMBER,
         cp_vendor_site_id   NUMBER
      )
      IS
         SELECT asups.org_id, asups.vendor_site_id, asup.vendor_id,
                asups.payment_method_lookup_code, asups.pay_site_flag,
                asups.purchasing_site_flag, asups.rfq_only_site_flag
           FROM ap_suppliers asup, ap_supplier_sites_all asups
          WHERE 1 = 1
            AND asup.vendor_id = asups.vendor_id
            AND (    NVL (asups.pay_site_flag, 'N') = 'Y'
               --  AND NVL (asups.purchasing_site_flag, 'N') = 'N'
                -- AND NVL (asups.rfq_only_site_flag, 'N') = 'N'
                )
            AND asups.attribute1 = p_vendor_num
            AND asups.org_id = cp_org_id
            AND asups.vendor_site_id =
                                 NVL (cp_vendor_site_id, asups.vendor_site_id)
            AND NVL (TRUNC (asups.inactive_date), TRUNC (SYSDATE)) >=
                                                               TRUNC (SYSDATE);
   BEGIN
      ----03-Nov-15  new routine added for total store to fetch vendor_id and vendor_site_id Based on
      -- BAAN no(attribute1) and org_id
      BEGIN
         UPDATE xxabrl.xxabrl_baan_ap_h_stage xahs
            SET xahs.vendor_site_id =
                   (SELECT vendor_site_id
                      FROM apps.ap_supplier_sites_all asl
                     WHERE org_id = xahs.org_id
                       AND attribute1 = xahs.vendor_num),
                xahs.vendor_id =
                   (SELECT DISTINCT asl.vendor_id
                               FROM apps.ap_supplier_sites_all asl
                              WHERE org_id = xahs.org_id
                                AND attribute1 = xahs.vendor_num)
          WHERE vendor_id IS NULL AND TRUNC (creation_date) >= '01-Nov-2015';

         COMMIT;
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (fnd_file.LOG,
                                  'Error for updating Total store =>'
                               || SQLCODE
                               || ':'
                               || SQLERRM
                              );
      END;

/*

-- 1st
--    20th December 2014        Automation
--    Due to legal entity merger ...    As Usual ReIM doesn't send the proper segments data, OFIN has to massage the data like TSRL ledger Migration
      BEGIN
         UPDATE apps.xxabrl_baan_ap_h_stage xahs
            SET xahs.accts_pay_code_concatenated =
                      '11.'
                   || SUBSTR (xahs.accts_pay_code_concatenated, 4, 4)
                   || DECODE (SUBSTR (xahs.accts_pay_code_concatenated, 8, 3),
                              '812', '635',
                              '772', '620',
                              '773', '630',
                              '792', '640',
                              '752', '610',
                              '754', '615',
                              '775', '625',
                              '751', '645'
                             )
                   || SUBSTR (xahs.accts_pay_code_concatenated, 11, 37),
                org_id =
                   DECODE (SUBSTR (xahs.accts_pay_code_concatenated, 8, 3),
                           '812', '1524',
                           '772', '1521',
                           '773', '1523',
                           '792', '1525',
                           '752', '1481',
                           '754', '1501',
                           '775', '1522',
                           '751', '1721'
                          ),
                vendor_site_id =
                   (SELECT bala.vendor_site_id
                      FROM apps.ap_supplier_sites_all bala
                     WHERE 1 = 1
                       AND bala.org_id =
                              DECODE (SUBSTR (accts_pay_code_concatenated,
                                              8,
                                              3
                                             ),
                                      '812', '1524',
                                      '772', '1521',
                                      '773', '1523',
                                      '792', '1525',
                                      '752', '1481',
                                      '754', '1501',
                                      '775', '1522',
                                      '751', '1721'
                                     )
                       AND bala.vendor_id IN (
                                              SELECT vendor_id
                                                FROM apps.ap_suppliers
                                               WHERE segment1 =
                                                               xahs.vendor_num)
                       AND UPPER (bala.vendor_site_code) =
                              (SELECT UPPER (ass.vendor_site_code)
                                 FROM apps.ap_supplier_sites_all ass
                                WHERE 1 = 1
                                  AND ass.vendor_site_id = xahs.vendor_site_id))
--GL_DATE = TRUNC(SYSDATE)
--ERROR_FLAG = 'N',
--ERROR_MSG = NULL
         WHERE  1 = 1
            AND TRUNC (creation_date) >= '20-DEC-2014'        --TRUNC(SYSDATE)
            AND NVL (error_flag, 'N') IN ('N', 'E')
            AND org_id IN (901, 861, 841, 961, 941, 821);

         COMMIT;

--
--
--
         UPDATE apps.xxabrl_baan_ap_h_stage xahs
            SET xahs.accts_pay_code_concatenated =
                      '11.'
                   || SUBSTR (xahs.accts_pay_code_concatenated, 4, 4)
                   || DECODE (SUBSTR (xahs.accts_pay_code_concatenated, 8, 3),
                              '812', '635',
                              '772', '620',
                              '773', '630',
                              '792', '640',
                              '752', '610',
                              '754', '615',
                              '775', '625',
                              '751', '645'
                             )
                   || SUBSTR (xahs.accts_pay_code_concatenated, 11, 37),
                org_id =
                   DECODE (SUBSTR (xahs.accts_pay_code_concatenated, 8, 3),
                           '812', '1524',
                           '772', '1521',
                           '773', '1523',
                           '792', '1525',
                           '752', '1481',
                           '754', '1501',
                           '775', '1522',
                           '751', '1721'
                          ),
                vendor_site_id =
                   (SELECT bala.vendor_site_id
                      FROM apps.ap_supplier_sites_all bala
                     WHERE 1 = 1
                       AND bala.org_id =
                              DECODE (SUBSTR (accts_pay_code_concatenated,
                                              8,
                                              3
                                             ),
                                      '812', '1524',
                                      '772', '1521',
                                      '773', '1523',
                                      '792', '1525',
                                      '752', '1481',
                                      '754', '1501',
                                      '775', '1522',
                                      '751', '1721'
                                     )
                       AND bala.vendor_id IN (
                                              SELECT vendor_id
                                                FROM apps.ap_suppliers
                                               WHERE segment1 =
                                                               xahs.vendor_num)
                       AND UPPER (bala.vendor_site_code) =
                              (SELECT UPPER (ass.vendor_site_code)
                                 FROM apps.ap_supplier_sites_all ass
                                WHERE 1 = 1
                                  AND ass.vendor_site_id = xahs.vendor_site_id))
--GL_DATE        = TRUNC(SYSDATE)
--ERROR_FLAG        = 'N',
--ERROR_MSG        = NULL
         WHERE  1 = 1
            AND TRUNC (creation_date) >= '20-DEC-2014'        --TRUNC(SYSDATE)
            AND NVL (error_flag, 'N') IN ('N', 'E')
--     SPECIALLY FOR 801 ONLY
            AND org_id = 801
            AND SUBSTR (accts_pay_code_concatenated, 8, 3) = '752';

--     SPECIALLY FOR 801 ONLY

         COMMIT;
--
--
--
         UPDATE apps.xxabrl_baan_ap_h_stage xahs
            SET xahs.accts_pay_code_concatenated =
                      '11.'
                   || SUBSTR (xahs.accts_pay_code_concatenated, 4, 4)
                   || DECODE (SUBSTR (xahs.accts_pay_code_concatenated, 8, 3),
                              '812', '635',
                              '772', '620',
                              '773', '630',
                              '792', '640',
                              '752', '610',
                              '754', '615',
                              '775', '625',
                              '751', '645'
                             )
                   || SUBSTR (xahs.accts_pay_code_concatenated, 11, 37),
                org_id =
                   DECODE (SUBSTR (xahs.accts_pay_code_concatenated, 8, 3),
                           '812', '1524',
                           '772', '1521',
                           '773', '1523',
                           '792', '1525',
                           '752', '1481',
                           '754', '1501',
                           '775', '1522',
                           '751', '1721'
                          ),
                vendor_site_id =
                   (SELECT bala.vendor_site_id
                      FROM apps.ap_supplier_sites_all bala
                     WHERE 1 = 1
                       AND bala.org_id =
                              DECODE (SUBSTR (accts_pay_code_concatenated,
                                              8,
                                              3
                                             ),
                                      '812', '1524',
                                      '772', '1521',
                                      '773', '1523',
                                      '792', '1525',
                                      '752', '1481',
                                      '754', '1501',
                                      '775', '1522',
                                      '751', '1721'
                                     )
                       AND bala.vendor_id IN (
                                              SELECT vendor_id
                                                FROM apps.ap_suppliers
                                               WHERE segment1 =
                                                               xahs.vendor_num)
                       AND UPPER (bala.vendor_site_code) =
                              (SELECT UPPER (ass.vendor_site_code)
                                 FROM apps.ap_supplier_sites_all ass
                                WHERE 1 = 1
                                  AND ass.vendor_site_id = xahs.vendor_site_id))
--GL_DATE        = TRUNC(SYSDATE)
--ERROR_FLAG        = 'N',
--ERROR_MSG        = NULL
         WHERE  1 = 1
            AND TRUNC (creation_date) >= '20-DEC-2014'        --TRUNC(SYSDATE)
            AND NVL (error_flag, 'N') IN ('N', 'E')
--     SPECIALLY FOR 801 ONLY
            AND org_id = 801
            AND SUBSTR (accts_pay_code_concatenated, 8, 3) = '754';

--     SPECIALLY FOR 801 ONLY
         COMMIT;

--
--
--
         UPDATE apps.xxabrl_baan_ap_l_stage a
            SET a.dist_code_concatenated =
                      '11.'
                   || SUBSTR (a.dist_code_concatenated, 4, 4)
                   || DECODE (SUBSTR (a.dist_code_concatenated, 8, 3),
                              '812', '635',
                              '772', '620',
                              '773', '630',
                              '792', '640',
                              '752', '610',
                              '754', '615',
                              '775', '625',
                              '751', '645'
                             )
                   || SUBSTR (a.dist_code_concatenated, 11, 37)
--ACCOUNTING_DATE = TRUNC(SYSDATE)
--ERROR_FLAG = 'N',
--ERROR_MSG = NULL
         WHERE  1 = 1
            AND TRUNC (creation_date) >= '20-DEC-2014'        --TRUNC(SYSDATE)
            AND SUBSTR (dist_code_concatenated, 8, 3) IN
                     ('812', '772', '773', '792', '752', '754', '775', '751');

         COMMIT;
      END;

--    20th December 2014        Automation
--    Due to legal entity merger ...    As Usual ReIM doesn't send the proper segments data, OFIN has to massage the data like TSRL ledger Migration

*/

      --
---    01.11.2013    BALA.SK            PROGRAM STARTS
---
      fnd_file.put_line
         (fnd_file.output,
          '                             AP Invoice Interface Validation Report'
         );
      fnd_file.put_line (fnd_file.output,
                            '    Run Date      : '
                         || TO_CHAR (TRUNC (SYSDATE), 'DD-MON-RRRR')
                        );
      fnd_file.put_line (fnd_file.output, CHR (10));
      fnd_file.put_line
         (fnd_file.output,
          '-------------------------------------------------------------------------------------------------'
         );
--    FND_FILE.PUT_LINE (FND_FILE.OUTPUT,CHR(10));
--    FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'File Date     : '||TO_CHAR(c_file_rec.file_date,'DD-MON-RRRR:HH24:MI:SS'));
--    FND_FILE.PUT_LINE (FND_FILE.OUTPUT,CHR(10));
      fnd_file.put_line (fnd_file.output,
                            RPAD ('Vendor No', 20)
                         || RPAD ('Invoice No', 20)
                         || RPAD ('Error Message', 400)
                        );
      fnd_file.put_line (fnd_file.output,
                         ' SET OF BOOK ID  ->  ' || v_set_of_books_id
                        );

---    05.03.2014        BALA.SK
      SELECT COUNT (invoice_num)
        INTO v_total_records
        FROM xxabrl.xxabrl_baan_ap_h_stage
       WHERE 1 = 1
         AND TRUNC (gl_date) > '31-MAR-15'
         AND NVL (error_flag, 'N') IN ('N', 'E');

--    Get Functional Currency
      BEGIN
         SELECT currency_code
           INTO v_fun_curr
           FROM gl_sets_of_books
          WHERE set_of_books_id = v_set_of_books_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            v_fun_curr := NULL;
      END;

---        1ST LOOP
      FOR rec_inv_hdr IN cur_inv_hdr
      LOOP
         EXIT WHEN cur_inv_hdr%NOTFOUND;
         v_error_flag := NULL;
         v_hdr_err_msg := NULL;
         v_vendor_id := NULL;
         v_vendor_site_id := NULL;
         v_invoice_currency_code := NULL;
         v_payment_method_lookup_code := NULL;
--    REC_INV_HDR.VENDOR_NUM                     := NULL;
         v_check_flag := 'N';
         v_pay_lookup_code := NULL;
--    V_ERROR_HEAD_COUNT                        := 0;
         v_inv_line_amt := 0;
         v_inv_type := NULL;
--    V_SUCCESS_RECORD_COUNT                    := 0;
         v_sup_status := 'N';
--    P_ORG_ID                                           := NULL;
--    P_VENDOR_SITE_ID                              := NULL;
--    P_VENDOR_ID                                        := NULL;
--    P_PAYMENT_METHOD_LOOKUP_CODE     := NULL;
--    V_PAY_SITE_FLAG                                := NULL;
         v_flag_count := 0;
         v_pay_count := 0;
         v_rfq_count := 0;
         v_pur_count := 0;
         v_ho_count := 0;

--    ORG ID VALIDATION ADDED BY SHAILESH ON 22 JUNE 2009
         BEGIN
            fnd_file.put_line (fnd_file.LOG,
                                  'VALIDATION FOR ORG ID -> '
                               || rec_inv_hdr.org_id
                              );

            SELECT COUNT (organization_id)
              INTO v_org_cnt
              FROM apps.hr_operating_units hou
             WHERE hou.organization_id = rec_inv_hdr.org_id;
--    WE NEED TO SKIP FULL VALIDATION IN THIS CASE IF ORG_ID IS NULL
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               v_error_flag := 'Y';
               v_hdr_err_msg := 'ORG IS NULL OR ORG NOT AVAILABLE';
               fnd_file.put_line (fnd_file.LOG,
                                     'ORG IS NULL OR ORG NOT AVAILABLE-> '
                                  || v_error_flag
                                 );
            WHEN OTHERS
            THEN
               v_error_flag := 'Y';
               v_hdr_err_msg := 'ORG IS NULL OR ORG NOT AVAILABLE';
               fnd_file.put_line (fnd_file.LOG,
                                  'Error in ORG ID -> ' || v_error_flag
                                 );
         END;

         IF v_org_cnt = 0
         THEN
            v_error_flag := 'Y';
            v_hdr_err_msg := 'ORG IS NULL OR ORG NOT AVAILABLE';
            p_vendor_site_id := NULL;
            p_vendor_id := NULL;
            p_payment_method_lookup_code := NULL;
            fnd_file.put_line (fnd_file.LOG,
                               'INVALID ORG ID -> ' || v_error_flag
                              );
         END IF;

---    FND_FILE.PUT_LINE(FND_FILE.log,'Error Flag -> '||v_error_flag);
         IF rec_inv_hdr.vendor_num IS NULL
         THEN
            DBMS_OUTPUT.put_line ('NO SUPPLIER FOR INVOICE NUMBER');
         ELSE
            fnd_file.put_line
                     (fnd_file.LOG,
                         'ORG ID PARAMETER --> cur_inv_hdr 2ND FOR LOOP --> '
                      || rec_inv_hdr.org_id
                     );

            SELECT COUNT (*)
              INTO v_pay_count
              FROM ap_suppliers asup, ap_supplier_sites_all asups
             WHERE 1 = 1
               AND asup.vendor_id = asups.vendor_id
               AND asups.attribute1 = rec_inv_hdr.vendor_num
               AND asups.org_id = rec_inv_hdr.org_id
               AND NVL (TRUNC (asups.inactive_date), TRUNC (SYSDATE)) > =
                                                               TRUNC (SYSDATE)
               AND NVL (asups.pay_site_flag, 'N') = 'Y'
               AND asups.vendor_site_id =
                        NVL (rec_inv_hdr.vendor_site_id, asups.vendor_site_id);
         END IF;

---    Check for suppliers status
         IF rec_inv_hdr.vendor_num IS NOT NULL
         THEN
            BEGIN
               SELECT 'Y'
                 INTO v_sup_status
                 FROM ap_supplier_sites_all
                WHERE 1 = 1
                  AND attribute1 = rec_inv_hdr.vendor_num
                  AND org_id = rec_inv_hdr.org_id
                  AND inactive_date IS NULL;
--                  AND SYSDATE BETWEEN NVL (, SYSDATE)
--                                  AND NVL (inactive_date, SYSDATE);
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  v_error_flag := 'Y';
                  v_hdr_err_msg := 'INVALID SUPPLIER';
                  fnd_file.put_line (fnd_file.output,
                                        RPAD (NVL (rec_inv_hdr.vendor_num,
                                                   ' '),
                                              20
                                             )
                                     || RPAD (rec_inv_hdr.invoice_num, 20)
                                     || v_hdr_err_msg
                                    );
            END;
         END IF;

         IF v_sup_status = 'Y'
         THEN
---    2ND LOOP
            FOR rec_check_flags IN
               cur_check_flags (rec_inv_hdr.vendor_num,
                                rec_inv_hdr.org_id,
                                rec_inv_hdr.vendor_site_id
                               )
            LOOP
               EXIT WHEN cur_check_flags%NOTFOUND;

--        ADDED BY GOVIND ON 24 JUNE 2009 VENDOR SITE VALIDATION
               BEGIN
                  IF rec_check_flags.vendor_site_id IS NULL
                  THEN
                     v_error_flag := 'Y';
                     fnd_file.put_line (fnd_file.output,
                                           'VENDOR SITE IS NOT AVAILABLE-> '
                                        || v_error_flag
                                       );
                     p_vendor_site_id := NULL;
                  END IF;
               END;

               IF cur_check_flags%NOTFOUND
               THEN
                  v_error_flag := 'Y';
                  v_hdr_err_msg := 'SUPPLIER IS NOT AVIALABLE';
                  fnd_file.put_line (fnd_file.output,
                                        RPAD (NVL (rec_inv_hdr.vendor_num,
                                                   ' '),
                                              20
                                             )
                                     || RPAD (rec_inv_hdr.invoice_num, 20)
                                     || v_hdr_err_msg
                                    );

                  CLOSE cur_check_flags;
               ELSE
                  IF v_pay_count > 1
                  THEN
                     v_error_flag := 'Y';
                     v_hdr_err_msg := 'MULTIPLE PAY SITE DEFINED';
                     fnd_file.put_line (fnd_file.output,
                                           RPAD (NVL (rec_inv_hdr.vendor_num,
                                                      ' '
                                                     ),
                                                 20
                                                )
                                        || RPAD (rec_inv_hdr.invoice_num, 20)
                                        || v_hdr_err_msg
                                       );
                     EXIT;
                  END IF;

                  IF v_pay_count = 1
                  THEN
                     IF NVL (rec_check_flags.pay_site_flag, 'N') = 'Y'
                     THEN
                        p_org_id := rec_check_flags.org_id;
                        p_vendor_site_id := rec_check_flags.vendor_site_id;
                        p_vendor_id := rec_check_flags.vendor_id;
                        p_payment_method_lookup_code :=
                                   rec_check_flags.payment_method_lookup_code;
                     END IF;
                  ELSE
                     IF v_pay_count = 0
                     THEN
                        v_error_flag := 'Y';
                        v_hdr_err_msg := 'NO PAY SITE DEFINED FOR THE VENDOR';
                        fnd_file.put_line
                                       (fnd_file.output,
                                           RPAD (NVL (rec_inv_hdr.vendor_num,
                                                      ' '
                                                     ),
                                                 20
                                                )
                                        || RPAD (rec_inv_hdr.invoice_num, 20)
                                        || v_hdr_err_msg
                                       );
                        EXIT;
                     END IF;

                     v_error_flag := 'Y';
                     v_hdr_err_msg :=
                        'SUPPLIER SITES SETUP IN APPS NOT AS PER VENDOR OUTBOUND DEFINATION';
                     fnd_file.put_line (fnd_file.output,
                                           RPAD (NVL (rec_inv_hdr.vendor_num,
                                                      ' '
                                                     ),
                                                 20
                                                )
                                        || RPAD (rec_inv_hdr.invoice_num, 20)
                                        || v_hdr_err_msg
                                       );
                     EXIT;
                  END IF;                    --        IF V_PAY_COUNT = 1 THEN
               END IF;              --        IF CUR_CHECK_FLAGS%NOTFOUND THEN
            END LOOP;                --        REC_CHECK_FLAGS        2ND LOOP
         END IF;                        --          IF V_SUP_STATUS = 'Y' THEN

--                                If P_Org_Id Is Not Null Then
--
--                                   Select  Set_Of_Books_Id
--                                   Into V_Set_Of_Books_Id
--                                   From Hr_Operating_Units
--                                   Where Organization_Id=P_Org_Id;
--
--                                 End If;

         --         VALIDATION FOR INACTIVE SITE

         /*
                                 If P_Vendor_Site_Id Is Not Null And P_Vendor_Id Is Not Null Then

                                   V_Inactive_Site := 0;
                                   Begin


                                     Select Count(*)
                                       Into V_Inactive_Site
                                       From Ap_Suppliers Asup, Ap_Supplier_Sites_All Asups
                                      Where Asup.Vendor_Id = Asups.Vendor_Id
                                        And Asups.Vendor_Site_Id = P_Vendor_Site_Id
                                        And Asup.Vendor_Id = P_Vendor_Id;
                                      --  And Nvl(Trunc(Asups.Inactive_Date), Trunc(Sysdate)) <= Trunc(Sysdate);

                                     If V_Inactive_Site > 0 Then
                                       V_Error_Flag  := 'Y';
                                       V_Hdr_Err_Msg := 'Vendor Site Is Inactive';
                                       Fnd_File.Put_Line(Fnd_File.Output,
                                             Rpad(Nvl(Rec_Inv_Hdr.Vendor_Num, ' '), 20) ||
                                             Rpad(Rec_Inv_Hdr.Invoice_Num, 20) ||
                                             V_Hdr_Err_Msg);
                                     End If;
                                   Exception
                                     When Others Then
                                       V_Error_Flag  := 'Y';
                                       V_Hdr_Err_Msg := 'Vendor Site Is Inactive';
                                       Fnd_File.Put_Line(Fnd_File.Output,
                                             Rpad(Nvl(Rec_Inv_Hdr.Vendor_Num, ' '), 20) ||
                                             Rpad(Rec_Inv_Hdr.Invoice_Num, 20) ||
                                             V_Hdr_Err_Msg);
                                   End;

                                 End If;
         */

         ---    ADDED BY SHAILESH ON 24 JUNE 2009 AS VENDOR SITE ID IS NOT AVAILABLE IN OFIN FOR SOME VENDOR.
         IF p_vendor_site_id IS NULL
         THEN
            v_error_flag := 'Y';
            v_hdr_err_msg :=
                  v_hdr_err_msg
               || '-'
               || ' VENDOR SITE NOT FOUND FOR VENDOR '
               || rec_inv_hdr.vendor_num
               || '  ';
            fnd_file.put_line (fnd_file.LOG,
                                  'ORG IS NULL OR ORG NOT AVAILABLE-> '
                               || v_error_flag
                              );
            fnd_file.put_line (fnd_file.output,
                                  RPAD (NVL (rec_inv_hdr.vendor_num, ' '), 20)
                               || RPAD (rec_inv_hdr.invoice_num, 20)
                               || v_hdr_err_msg
                              );
         END IF;

---            Begin
---               Select Asr.Segment1
---               Into   Rec_Inv_Hdr.Vendor_Num
---               From   Ap_Suppliers Asr,Ap_Supplier_Sites_All Assa
---               Where  Asr.Vendor_Id=Assa.Vendor_Id
---               And    Asr.Segment1=Rec_Inv_Hdr.Vendor_Num
---               And    Assa.Pay_Site_Flag ='Y';
---               ---And    Assa.Vendor_Site_Id=Rec_Inv_Hdr.Vendor_Site_Id; -- Added By Lalit Dt 26-Jun-2008
---               ---And    Org_Id = Fnd_Profile.Value('Org_Id');
---               V_Check_Flag :='Y';
---            Exception
---               When No_Data_Found Then
---                  V_Error_Flag  := 'Y';
---                  V_Hdr_Err_Msg := 'Invoice Not Processed As The Vendor Site Id In Reim Is Not Matching With Vendor Site Id In Apps';
---                  Fnd_File.Put_Line (Fnd_File.Output,Rpad(Nvl(Rec_Inv_Hdr.Vendor_Num,' '), 20)
---                                                          || Rpad(Rec_Inv_Hdr.Invoice_Num, 20)
---                                                          || V_Hdr_Err_Msg);
---               When Too_Many_Rows Then
---                  V_Check_Flag :='N';
---                  V_Error_Flag  := 'Y';
---                  V_Hdr_Err_Msg := 'Invoice Not Processed As Multiple Pay Site Defined For Vendor';
---                  Fnd_File.Put_Line (Fnd_File.Output,Rpad(Nvl(Rec_Inv_Hdr.Vendor_Num,' '), 20)
---                                                     || Rpad(Rec_Inv_Hdr.Invoice_Num, 20)
---                                                     || V_Hdr_Err_Msg);
---                When Others Then
---                  V_Error_Flag  := 'Y';
---                  V_Hdr_Err_Msg := 'Error In Deriving Operating Unit' || Sqlerrm;
---                  Fnd_File.Put_Line (Fnd_File.Output,Rpad(Nvl(Rec_Inv_Hdr.Vendor_Num,' '), 20)
---                                                     || Rpad(Rec_Inv_Hdr.Invoice_Num, 20)
---                                                     || V_Hdr_Err_Msg);
---            End;
---            If Nvl(V_Check_Flag,'N') = 'Y' Then
---              -- Begin
---                  Open Cur_Get_Values(Rec_Inv_Hdr.Vendor_Num);--,Rec_Inv_Hdr.Vendor_Site_Id);
---                  Fetch Cur_Get_Values Into V_Vendor_Site_Id,V_Vendor_Site_Code,V_Org_Id,V_Vendor_Id,V_Payment_Method_Lookup_Code;
---                  If Cur_Get_Values%Notfound Then
---                     V_Error_Flag  := 'Y';
---                     V_Hdr_Err_Msg := 'Invoice Not Processed As The Vendor Site Id In Reim Is Not Matching With Vendor Site Id In Apps';
---                    Fnd_File.Put_Line (Fnd_File.Output,Rpad(Nvl(Rec_Inv_Hdr.Vendor_Num,' '), 20)
---                                                     || Rpad(Rec_Inv_Hdr.Invoice_Num, 20)
---                                                     || V_Hdr_Err_Msg);
---                     Close Cur_Get_Values;
---                  Else
---                     Close Cur_Get_Values;
---                     --Raise Dup_Run_Id;
---                  End If;
---            End If;
---
---
--- If V_Org_Id Is Not Null Then
---
---    Select  Name
---    Into V_Operating_Unit
---    From Hr_Operating_Units
---    Where Organization_Id=V_Org_Id;
---
---End If;
---
---End If;
---
---End If;

         ---        Validation for Invoice Type Lookup Code
         IF rec_inv_hdr.invoice_type_lookup_code IS NULL
         THEN
            v_hdr_err_msg := v_hdr_err_msg || 'INVOICE TYPE IS NULL';
         ELSE
            BEGIN
               SELECT lookup_code
                 INTO v_inv_type
                 FROM ap_lookup_codes
                WHERE UPPER (lookup_code) =
                                  UPPER (rec_inv_hdr.invoice_type_lookup_code)
                  AND lookup_type = 'INVOICE TYPE';
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  v_error_flag := 'Y';
                  v_hdr_err_msg := 'INVOICE TYPE DOES NOT EXISTS';
                  fnd_file.put_line (fnd_file.output,
                                        RPAD (NVL (rec_inv_hdr.vendor_num,
                                                   ' '),
                                              20
                                             )
                                     || RPAD (rec_inv_hdr.invoice_num, 20)
                                     || v_hdr_err_msg
                                    );
               WHEN TOO_MANY_ROWS
               THEN
                  v_error_flag := 'Y';
                  v_hdr_err_msg := 'MULTIPLE INVOICE TYPE FOUND';
               WHEN OTHERS
               THEN
                  v_error_flag := 'Y';
                  v_hdr_err_msg := 'INVALID INVOICE TYPE';
                  fnd_file.put_line (fnd_file.output,
                                        RPAD (NVL (rec_inv_hdr.vendor_num,
                                                   ' '),
                                              20
                                             )
                                     || RPAD (rec_inv_hdr.invoice_num, 20)
                                     || v_hdr_err_msg
                                    );
            END;
         END IF;

---        Check Invoice Number Already exists and Invoice Number Duplication
         IF rec_inv_hdr.invoice_num IS NOT NULL AND v_sup_status = 'Y'
         THEN
            BEGIN
               v_data_count := 0;

               SELECT COUNT (invoice_num)
                 INTO v_data_count
                 FROM ap_invoices_all
                WHERE invoice_num = rec_inv_hdr.invoice_num
                  AND vendor_id = p_vendor_id
             -- added by shailesh on 20 june 2009 as org id comming from CSV file to staging table.
             --AND org_id = p_org_id;
--             AND vendor_site_id=rec_inv_hdr.vendor_site_id             --- added by Dhiresh on 17 Jul 2014
                  AND org_id = rec_inv_hdr.org_id;

               IF v_data_count <> 0
               THEN
                  v_error_flag := 'Y';
                  v_hdr_err_msg :=
                        'INVOICE NUMBER ('
                     || ' '
                     || rec_inv_hdr.invoice_num
                     || ' '
                     || ') ALREADY EXISTS FOR SUPPLIER'
                     || ' '
                     || rec_inv_hdr.vendor_num;
                  fnd_file.put_line (fnd_file.output,
                                        RPAD (NVL (rec_inv_hdr.vendor_num,
                                                   ' '),
                                              20
                                             )
                                     || RPAD (rec_inv_hdr.invoice_num, 20)
                                     || v_hdr_err_msg
                                    );
               END IF;

               v_data_count := 0;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  v_error_flag := 'Y';
                  v_hdr_err_msg := 'INVOICE NUMBER DOES NOT EXISTS';
                  fnd_file.put_line (fnd_file.output,
                                        RPAD (NVL (rec_inv_hdr.vendor_num,
                                                   ' '),
                                              20
                                             )
                                     || RPAD (rec_inv_hdr.invoice_num, 20)
                                     || v_hdr_err_msg
                                    );
               WHEN OTHERS
               THEN
                  v_error_flag := 'Y';
                  v_hdr_err_msg := SQLERRM;
            END;

            BEGIN
               v_data_count := 0;

               SELECT COUNT (invoice_num)
                 INTO v_data_count
                 FROM xxabrl.xxabrl_baan_ap_h_stage
                WHERE invoice_num = rec_inv_hdr.invoice_num
--               AND vendor_site_id=rec_inv_hdr.vendor_site_id         --- added by Dhiresh on 17 Jul 2014
                  AND org_id = rec_inv_hdr.org_id
                  --- added by Dhiresh on 17 Jul 2014
                  AND vendor_num = rec_inv_hdr.vendor_num;

               IF v_data_count > 1
               THEN
                  v_error_flag := 'Y';
                  v_hdr_err_msg :=
                        'DUPLICATE INVOICE NUMBER.THIS INVOICE ALREADY EXISTS FOR'
                     || RPAD (' ', 20)
                     || 'SUPPLIER '
                     || ' '
                     || rec_inv_hdr.vendor_num
                     || ' '
                     || 'AND SUPPLIER SITE'
                     || ' '
                     || v_vendor_site_code;
                  fnd_file.put_line (fnd_file.output,
                                        RPAD (NVL (rec_inv_hdr.vendor_num,
                                                   ' '),
                                              20
                                             )
                                     || RPAD (rec_inv_hdr.invoice_num, 20)
                                     || v_hdr_err_msg
                                    );
               END IF;

               v_data_count := 0;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  v_error_flag := 'Y';
                  v_hdr_err_msg := 'INVOICE NUMBER DOES NOT EXISTS';
                  fnd_file.put_line (fnd_file.output,
                                        RPAD (NVL (rec_inv_hdr.vendor_num,
                                                   ' '),
                                              20
                                             )
                                     || RPAD (rec_inv_hdr.invoice_num, 20)
                                     || v_hdr_err_msg
                                    );
               WHEN OTHERS
               THEN
                  v_error_flag := 'Y';
                  v_hdr_err_msg := v_hdr_err_msg || ' ' || SQLERRM;
            END;
         --   ELSE
         --         v_error_flag := 'Y';
         --         v_hdr_err_msg := v_hdr_err_msg ||' '|| 'INVOICE NUMBER SHOULD NOT BE NULL';
         --         FND_FILE.PUT_LINE (FND_FILE.OUTPUT,v_hdr_err_msg);
         END IF;

---        Validate for Invoice Amount with Transaction Type
         IF     UPPER (rec_inv_hdr.invoice_type_lookup_code) = 'STANDARD'
            AND rec_inv_hdr.invoice_amount < 0
         THEN
            v_error_flag := 'Y';
            v_hdr_err_msg :=
                    'INVOICE AMOUNT CAN NOT BE NEGATIVE FOR STANDARD INVOICE';
            fnd_file.put_line (fnd_file.output,
                                  RPAD (NVL (rec_inv_hdr.vendor_num, ' '), 20)
                               || RPAD (rec_inv_hdr.invoice_num, 20)
                               || v_hdr_err_msg
                              );
         ELSIF     UPPER (rec_inv_hdr.invoice_type_lookup_code) = 'CREDIT'
               AND rec_inv_hdr.invoice_amount >= 0
         THEN
            v_error_flag := 'Y';
            v_hdr_err_msg :=
                         'INVOICE AMOUNT CAN NOT BE POSITIVE FOR CREDIT MEMO';
            fnd_file.put_line (fnd_file.output,
                                  RPAD (NVL (rec_inv_hdr.vendor_num, ' '), 20)
                               || RPAD (rec_inv_hdr.invoice_num, 20)
                               || v_hdr_err_msg
                              );
         END IF;

---    Validate the currency_code in invoice header table
         IF rec_inv_hdr.invoice_currency_code IS NOT NULL
         THEN
            BEGIN
               SELECT currency_code
                 INTO v_invoice_currency_code
                 FROM fnd_currencies
                WHERE UPPER (currency_code) =
                              TRIM (UPPER (rec_inv_hdr.invoice_currency_code));

               IF     UPPER (TRIM (v_invoice_currency_code)) <>
                                                     UPPER (TRIM (v_fun_curr))
                  AND rec_inv_hdr.exchange_date IS NULL
               THEN
                  v_error_flag := 'Y';
                  v_hdr_err_msg := 'EXCHANGE DATE IS NULL';
                  fnd_file.put_line (fnd_file.output,
                                        RPAD (NVL (rec_inv_hdr.vendor_num,
                                                   ' '),
                                              20
                                             )
                                     || RPAD (rec_inv_hdr.invoice_num, 20)
                                     || v_hdr_err_msg
                                    );
               END IF;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  v_error_flag := 'Y';
                  v_hdr_err_msg :=
                        rec_inv_hdr.invoice_currency_code
                     || ' _ '
                     || ' INVOICE_CURRENCY_CODE is not valid';
                  fnd_file.put_line (fnd_file.output,
                                        RPAD (NVL (rec_inv_hdr.vendor_num,
                                                   ' '),
                                              20
                                             )
                                     || RPAD (rec_inv_hdr.invoice_num, 20)
                                     || v_hdr_err_msg
                                    );
            END;
         ELSE
            v_error_flag := 'Y';
            v_hdr_err_msg := 'INVOICE CURRENCY CODE SHOULD NOT BE NULL';
            fnd_file.put_line (fnd_file.output,
                                  RPAD (NVL (rec_inv_hdr.vendor_num, ' '), 20)
                               || RPAD (rec_inv_hdr.invoice_num, 20)
                               || v_hdr_err_msg
                              );
         END IF;

---      Validate the payment_method_lookup_code in invoice header

         /*

             IF rec_inv_hdr.payment_method_lookup_code IS NOT NULL THEN

                  BEGIN
                      SELECT  lookup_code
                     INTO       v_pay_lookup_code
                     FROM    ap_lookup_codes
                     WHERE   lookup_type='PAYMENT METHOD'
                     AND     UPPER(lookup_code)=Trim(UPPER(rec_inv_hdr.payment_method_lookup_code))
                     AND       NVL(Enabled_flag,'N')='Y'
                     AND      SYSDATE BETWEEN NVL(Start_date_Active,sysdate) AND NVL(Inactive_Date,sysdate);
                   EXCEPTION
                       WHEN NO_DATA_FOUND THEN
                           v_error_flag := 'Y';
                           v_hdr_err_msg := rec_inv_hdr.payment_method_lookup_code||' _ '||'PAYMENT METHOD LOOKUP CODE DOES NOT EXISTS IN APPLICATION';
                        FND_FILE.PUT_LINE (FND_FILE.OUTPUT,v_hdr_err_msg);
                     WHEN TOO_MANY_ROWS THEN
                           v_error_flag := 'Y';
                           v_hdr_err_msg := ' MULTIPLE PAYMENT METHOD LOOKUP CODE FOUND';
                        FND_FILE.PUT_LINE (FND_FILE.OUTPUT,v_hdr_err_msg);
                     WHEN OTHERS THEN
                           v_error_flag := 'Y';
                           v_hdr_err_msg := ' INVALID PAYMENT METHOD LOOKUP CODE';
                        FND_FILE.PUT_LINE (FND_FILE.OUTPUT,v_hdr_err_msg);
                   END;
                END IF;


         ---        Validate payment terms in invoice header

                  IF rec_inv_hdr.terms_id IS NOT NULL THEN
                   BEGIN
                       SELECT  name
                      INTO       v_term_name
                      FROM    ap_terms_tl
                      WHERE   term_id =rec_inv_hdr.terms_id
                      AND       NVL(Enabled_flag,'N')='Y'
                      AND      SYSDATE BETWEEN NVL(start_date_active,sysdate) AND NVL(end_date_active,sysdate);
                    EXCEPTION
                        WHEN NO_DATA_FOUND THEN
                            v_error_flag := 'Y';
                            v_hdr_err_msg := 'TERM ID DOES NOT EXISTS IN APPLICATION';
                         FND_FILE.PUT_LINE (FND_FILE.OUTPUT,RPAD(NVL(rec_inv_hdr.vendor_num,' '), 20)
                                                            || RPAD(rec_inv_hdr.INVOICE_NUM, 20)
                                                            || v_hdr_err_msg);
                      WHEN TOO_MANY_ROWS THEN
                            v_error_flag := 'Y';
                            v_hdr_err_msg := 'MULTIPLE TERM IDS FOUND';
                         FND_FILE.PUT_LINE (FND_FILE.OUTPUT,RPAD(NVL(rec_inv_hdr.vendor_num,' '), 20)
                                                            || RPAD(rec_inv_hdr.INVOICE_NUM, 20)
                                                            || v_hdr_err_msg);
                      WHEN OTHERS THEN
                            v_error_flag := 'Y';
                            v_hdr_err_msg := ' INVALID TERM ID';
                         FND_FILE.PUT_LINE (FND_FILE.OUTPUT,RPAD(NVL(rec_inv_hdr.vendor_num,' '), 20)
                                                            || RPAD(rec_inv_hdr.INVOICE_NUM, 20)
                                                            || v_hdr_err_msg);
                    END;
                 END IF;
         */

         ---    VALIDATE PAYMENT TERMS IN INVOICE HEADER
---    ABOVE CODE COMMENTED BY SHAILESH ON 25 OCT 2008 AND WRITTEN FOLLOWING NEW VALIDATION FOR TERMS ID
---     TERMS_ID IS TAKEN FROM VENDOR MASTER
         BEGIN
            SELECT NAME, apt.term_id
              INTO v_term_name, l_terms_id
              FROM ap_terms_tl apt, po_vendor_sites_all pvsa
             WHERE pvsa.terms_id = apt.term_id
               AND pvsa.vendor_id = p_vendor_id
               AND pvsa.vendor_site_id = p_vendor_site_id
               AND NVL (apt.enabled_flag, 'N') = 'Y'
               AND SYSDATE BETWEEN NVL (apt.start_date_active, SYSDATE)
                               AND NVL (apt.end_date_active, SYSDATE);
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               l_terms_id := NULL;
            WHEN TOO_MANY_ROWS
            THEN
               v_error_flag := 'Y';
               v_hdr_err_msg := 'MULTIPLE TERM IDS FOUND';
               fnd_file.put_line (fnd_file.output,
                                     RPAD (NVL (rec_inv_hdr.vendor_num, ' '),
                                           20
                                          )
                                  || RPAD (rec_inv_hdr.invoice_num, 20)
                                  || v_hdr_err_msg
                                 );
            WHEN OTHERS
            THEN
               v_error_flag := 'Y';
               v_hdr_err_msg := ' TERM ID';
               fnd_file.put_line (fnd_file.output,
                                     RPAD (NVL (rec_inv_hdr.vendor_num, ' '),
                                           20
                                          )
                                  || RPAD (rec_inv_hdr.invoice_num, 20)
                                  || v_hdr_err_msg
                                 );
         END;

---    Validation for  ACCTS_PAY_CODE_CONCATENATED
---    New validation for the dist code concatenated  added by shailesh on 25 OCT 2008
---    DIST CODE CONCATENATED SEGMENTS - Default accounting segments from vendor master table
---    it will pick up the only segm,ent6 (natural acct0 value and update the acct code concatenated from the hdr table.
         BEGIN
            SELECT    SUBSTR (rec_inv_hdr.accts_pay_code_concatenated, 1, 22)
                   || gcc.segment6
                   || SUBSTR (rec_inv_hdr.accts_pay_code_concatenated, 29, 9)
              INTO l_dist_code_concatenated
              FROM po_vendors pv,
                   po_vendor_sites_all pvsa,
                   gl_code_combinations gcc
             WHERE pv.vendor_id = pvsa.vendor_id
               AND gcc.code_combination_id =
                                            pvsa.accts_pay_code_combination_id
               AND pv.vendor_id = p_vendor_id
               AND pvsa.vendor_site_id = p_vendor_site_id;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               v_error_line_flag := 'Y';
               v_line_err_msg :=
                          'ACCOUNTING SEGMENTS NOT MAPPED WITH VENDOR MASTER';
            --FND_FILE.PUT_LINE (FND_FILE.OUTPUT,RPAD(rec_inv_line.vendor_num, 20)
            --                                      || RPAD(rec_inv_line.invoice_num, 20)
            --                                     || v_line_err_msg);
            WHEN OTHERS
            THEN
               v_error_line_flag := 'Y';
               v_line_err_msg := 'INVALID DIST CODE CONCATENATED VALUE';
         --FND_FILE.PUT_LINE (FND_FILE.OUTPUT,v_line_err_msg);
         END;

---
---        01.03.2014        BALA.SK
---
         IF     SUBSTR (rec_inv_hdr.vendor_num, 1, 2) = '70'
            AND rec_inv_hdr.vendor_site_id IS NOT NULL
            AND SUBSTR (l_dist_code_concatenated, 23, 6) <> '362009'
         THEN
            l_dist_code_concatenated :=
                  SUBSTR (l_dist_code_concatenated, 1, 22)
               || '362009'
               || SUBSTR (l_dist_code_concatenated, 29, 9);
         END IF;

---
---        01.03.2014        BALA.SK
---

         ---    Validation for GLdate
         BEGIN
            SELECT set_of_books_id
              INTO v_set_of_books_id
              FROM org_organization_definitions
             WHERE organization_id = rec_inv_hdr.org_id;

            fnd_file.put_line (fnd_file.output,
                               'SOB-ID: ' || v_set_of_books_id
                              );
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               fnd_file.put_line (fnd_file.output,
                                     'SOB not defined for org id: '
                                  || rec_inv_hdr.org_id
                                 );
            WHEN TOO_MANY_ROWS
            THEN
               fnd_file.put_line (fnd_file.output,
                                     'Multiple SOB defined for org id: '
                                  || rec_inv_hdr.org_id
                                 );
            WHEN OTHERS
            THEN
               fnd_file.put_line
                              (fnd_file.output,
                                  'Exception while deriving SOB for org id: '
                               || rec_inv_hdr.org_id
                              );
         END;

         IF rec_inv_hdr.gl_date IS NULL
         THEN
            v_error_flag := 'Y';
            v_hdr_err_msg := 'GL DATE IS NULL';
            fnd_file.put_line (fnd_file.output, v_hdr_err_msg);
         ELSE
            v_data_count := 0;

            BEGIN
               SELECT COUNT (*)
                 INTO v_data_count
                 FROM gl_period_statuses gps, fnd_application fna
                WHERE fna.application_short_name = 'SQLAP'
                  AND gps.application_id = fna.application_id
                  AND gps.closing_status = 'O'
                  AND gps.set_of_books_id = v_set_of_books_id
                  AND TRUNC (NVL (rec_inv_hdr.gl_date, SYSDATE))
                         BETWEEN gps.start_date
                             AND gps.end_date;

               --   AND     TO_DATE('08-OCT-08','DD-MON-YYYY')) BETWEEN GPS.START_DATE AND GPS.END_DATE
               --   AND     TRUNC(NVL(TO_DATE('08-OCT-08','DD-MON-YYYY',SYSDATE)) BETWEEN GPS.START_DATE AND GPS.END_DATE;
               IF v_data_count = 0
               THEN
                  v_error_flag := 'Y';
                  v_hdr_err_msg := 'GL DATE IS NOT IN OPEN PERIOD';
                  fnd_file.put_line (fnd_file.output,
                                        RPAD (NVL (rec_inv_hdr.vendor_num,
                                                   ' '),
                                              20
                                             )
                                     || RPAD (rec_inv_hdr.invoice_num, 20)
                                     || v_hdr_err_msg
                                    );
               END IF;
            EXCEPTION
               WHEN OTHERS
               THEN
                  v_error_flag := 'Y';
                  v_hdr_err_msg := 'GL DATE IS NOT IN OPEN PERIOD';
                  fnd_file.put_line (fnd_file.output,
                                        RPAD (NVL (rec_inv_hdr.vendor_num,
                                                   ' '),
                                              20
                                             )
                                     || RPAD (rec_inv_hdr.invoice_num, 20)
                                     || v_hdr_err_msg
                                    );
            END;
         END IF;

---    DBMS_OUTPUT.PUT_LINE('outsite if part' || ' ' || v_error_flag || ' ' || 'value for ');
         v_error_line_count := 0;
         v_error_almsg := NULL;
---        3RD LOOP
         fnd_file.put_line (fnd_file.LOG,
                               'rec_inv_hdr.invoice_num =>'
                            || rec_inv_hdr.invoice_num
                           );
         fnd_file.put_line (fnd_file.LOG,
                               'rec_inv_hdr.vendor_num =>'
                            || rec_inv_hdr.vendor_num
                           );
         fnd_file.put_line (fnd_file.LOG,
                            'rec_inv_hdr.gl_date =>' || rec_inv_hdr.gl_date
                           );

         FOR rec_inv_line IN cur_inv_line (rec_inv_hdr.invoice_num,
---                                    rec_inv_hdr.vendor_site_id,
                                           rec_inv_hdr.vendor_num,
                                           rec_inv_hdr.gl_date
                                          )
         LOOP
            fnd_file.put_line (fnd_file.LOG, ' 3RD LOOP');
            v_line_count := cur_inv_line%ROWCOUNT;
            v_dist_code_concatenated := NULL;
            v_error_line_flag := NULL;
            v_line_err_msg := NULL;
            v_line_type_lookup_code := NULL;
            v_data_count := 0;

            -- IF rec_inv_line.line_type_lookup_code IS NOT NULL THEN
            --    SELECT line_type_lookup_code
            --    INTO   v_line_type_lookup_code
            --    FROM   apps.xxabrl_baan_ap_l_stage
            --    WHERE  invoice_num := rec_inv_line.invoice_num
            --    AND    line_number := rec_inv_line.line_number
            --    AND    line_type_lookup_code := rec_inv_line.line_type_lookup_code;
            IF rec_inv_line.line_type_lookup_code <> 'ITEM'
            THEN
               v_line_type_lookup_code := 'MISCELLANEOUS';
            ELSE
               v_line_type_lookup_code := 'ITEM';
            END IF;

---            Validation for Accounting date
            IF rec_inv_line.accounting_date IS NULL
            THEN
               v_error_line_flag := 'Y';
               v_line_err_msg := 'ACCOUNTING DATE IS NULL';
---        FND_FILE.PUT_LINE (FND_FILE.OUTPUT,v_line_err_msg);
            ELSE
               BEGIN
                  SELECT COUNT (*)
                    INTO v_data_count
                    FROM gl_period_statuses gps, fnd_application fna
                   WHERE fna.application_short_name = 'SQLAP'
                     AND gps.application_id = fna.application_id
                     AND gps.closing_status = 'O'
                     AND gps.set_of_books_id = v_set_of_books_id
                     AND TRUNC (NVL (rec_inv_line.accounting_date, SYSDATE))
                            BETWEEN gps.start_date
                                AND gps.end_date;

                  IF v_data_count = 0
                  THEN
                     v_error_line_flag := 'Y';
                     v_line_err_msg :=
                           'ACCOUNTING DATE IS NOT IN OPEN PERIOD'
                        || CHR (10)
                        || RPAD (' ', 20);
---    FND_FILE.PUT_LINE (FND_FILE.OUTPUT,v_line_err_msg);
                  END IF;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     v_error_line_flag := 'Y';
                     v_line_err_msg :=
                                      'ACCOUNTING DATE IS NOT IN OPEN PERIOD';
               --  FND_FILE.PUT_LINE (FND_FILE.OUTPUT,v_line_err_msg);
               END;
            END IF;

---    Validation for Invoice Line Amount
            IF rec_inv_line.amount IS NULL
            THEN
               v_error_line_flag := 'Y';
               v_line_err_msg :=
                     'INVOICE LINE AMOUNT IS NULL'
                  || RPAD (' ', 20)
                  || RPAD (' ', 20)
                  || CHR (10)
                  || RPAD (' ', 20)
                  || RPAD (' ', 20);
               fnd_file.put_line (fnd_file.output, v_line_err_msg);
            END IF;

            fnd_file.put_line (fnd_file.output,
                                  'rec_inv_line.Amount: '
                               || NVL (rec_inv_line.amount, 0)
                              );
            v_inv_line_amt :=
                         NVL (v_inv_line_amt, 0)
                         + NVL (rec_inv_line.amount, 0);

---        Validate the concatenated_segments in invoice Lines
---         Following validation of concatenated segments removed by shailesh on 25 OCt 2008.
---        As system will take default accounting segment value from vendor master.
            IF rec_inv_line.dist_code_concatenated IS NULL
            THEN
               v_error_line_flag := 'Y';
               v_line_err_msg :=
                     'DIST CODE CONCATENATED VALUE IS NULL'
                  || RPAD (' ', 20)
                  || RPAD (' ', 20)
                  || CHR (10)
                  || RPAD (' ', 20)
                  || RPAD (' ', 20);
---    FND_FILE.PUT_LINE (FND_FILE.OUTPUT,v_line_err_msg);
            ELSE
               BEGIN
---
---        05.03.2013    BALA.SK
---

                  ---            SELECT CODE_COMBINATION_ID
---              INTO V_DIST_CODE_CONCATENATED
---              FROM GL_CODE_COMBINATIONS_KFV
---             WHERE TRIM(CONCATENATED_SEGMENTS) = TRIM(REC_INV_LINE.DIST_CODE_CONCATENATED);
                  SELECT code_combination_id
                    INTO v_dist_code_concatenated
                    FROM apps.gl_code_combinations gcc
                   WHERE 1 = 1
---        AND GCC.SEGMENT1 = '31'
                     AND gcc.segment1 =
                            SUBSTR (rec_inv_line.dist_code_concatenated, 1, 2)
---        AND GCC.SEGMENT2 = '000'
                     AND gcc.segment2 =
                            SUBSTR (rec_inv_line.dist_code_concatenated, 4, 3)
---        AND GCC.SEGMENT3 = '775'
                     AND gcc.segment3 =
                            SUBSTR (rec_inv_line.dist_code_concatenated, 8, 3)
---        AND GCC.SEGMENT4 = '0000058'
                     AND gcc.segment4 =
                            SUBSTR (rec_inv_line.dist_code_concatenated, 12,
                                    7)
---        AND GCC.SEGMENT5 = '00'
                     AND gcc.segment5 =
                            SUBSTR (rec_inv_line.dist_code_concatenated, 20,
                                    2)
---        AND GCC.SEGMENT6 = '361905'
                     AND gcc.segment6 =
                            SUBSTR (rec_inv_line.dist_code_concatenated, 23,
                                    6)
---        AND GCC.SEGMENT7 = '000'
                     AND gcc.segment7 =
                            SUBSTR (rec_inv_line.dist_code_concatenated, 30,
                                    3)
---        AND GCC.SEGMENT8 = '0000'
                     AND gcc.segment8 =
                            SUBSTR (rec_inv_line.dist_code_concatenated, 34,
                                    4);
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     v_error_line_flag := 'Y';
                     v_line_err_msg :=
                           'DIST CODE CONCATENATED VALUE'
                        || ' '
                        || rec_inv_line.dist_code_concatenated
                        || ' '
                        || 'IS NOT VALID';
---        FND_FILE.PUT_LINE (FND_FILE.OUTPUT,RPAD(rec_inv_line.vendor_num, 20)
---                                              || RPAD(rec_inv_line.invoice_num, 20) || v_line_err_msg);
                  WHEN OTHERS
                  THEN
                     v_error_line_flag := 'Y';
                     v_line_err_msg := 'INVALID DIST CODE CONCATENATED VALUE';
---        FND_FILE.PUT_LINE (FND_FILE.OUTPUT,v_line_err_msg);
               END;
            END IF;

            fnd_file.put_line (fnd_file.LOG,
                               'Sss -line  Error flag ' || v_error_line_flag
                              );

---        Updating Lines Staging
            IF NVL (v_error_line_flag, 'N') = 'Y'
            THEN
               --DBMS_OUTPUT.PUT_LINE('Updating Lines Staging' || ' ' || v_error_flag || ' ' || 'value for ');

               -- FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'SHAILESH - updating line staging error ');
               UPDATE xxabrl.xxabrl_baan_ap_l_stage
                  SET error_flag = 'E',
                      error_msg = v_line_err_msg
                WHERE ROWID = rec_inv_line.ROWID;

               COMMIT;
               --                  UPDATE  apps.xxabrl_baan_ap_h_stage
               --                  SET   ERROR_FLAG ='E'
               --                   ,ERROR_MSG = 'Line validation failed'|| v_line_err_msg
               --                  WHERE ROWID = rec_inv_hdr.ROWID
               --               AND invoice_num=rec_inv_line.invoice_num;
               v_error_line_count := v_error_line_count + 1;
            ELSE
---        FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'SHAILESH - updating line staging NOT IN error ');
               UPDATE xxabrl.xxabrl_baan_ap_l_stage
                  SET error_flag = 'V',
                      line_type_lookup_code = v_line_type_lookup_code,
                      error_msg = NULL
                WHERE ROWID = rec_inv_line.ROWID;

               COMMIT;
            END IF;

            IF v_line_err_msg IS NOT NULL
            THEN
               v_error_almsg :=
                     v_error_almsg
                  || 'LINE NO :'
                  || TO_CHAR (v_line_count)
                  || ' '
                  || v_line_err_msg;
            END IF;
         END LOOP;                      --    FOR rec_inv_line        3rd LOOP

         fnd_file.put_line (fnd_file.output,
                            'LINE AMOUNT   ->  ' || v_inv_line_amt
                           );
         fnd_file.put_line (fnd_file.output,
                            'HDR AMOUNT   ->  ' || rec_inv_hdr.invoice_amount
                           );

         IF (ROUND (v_inv_line_amt, 2) - ROUND (rec_inv_hdr.invoice_amount, 2)
            ) BETWEEN -1 AND 1
         THEN
            NULL;
         ELSE
            v_error_flag := 'Y';
            v_hdr_err_msg := 'INVOICE HEADER AND LINE AMOUNT IS NOT MATCHING';
            fnd_file.put_line (fnd_file.output,
                                  RPAD (NVL (rec_inv_hdr.vendor_num, ' '), 20)
                               || RPAD (rec_inv_hdr.invoice_num, 20)
                               || v_hdr_err_msg
                              );
         END IF;

         fnd_file.put_line (fnd_file.LOG,
                            'Sss - hdr Error flag ' || v_error_flag
                           );

---        Updating  Header Staging
         IF NVL (v_error_flag, 'N') = 'Y'
         THEN
---    FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'SHAILESH - updating HDR staging error ');
            UPDATE xxabrl.xxabrl_baan_ap_h_stage
               SET error_flag = 'E',
                   error_msg = v_hdr_err_msg
             WHERE ROWID = rec_inv_hdr.ROWID;

            COMMIT;
            ---    added by Gonvind on 25 june 2009
            p_vendor_site_id := NULL;
            --              UPDATE  apps.xxabrl_baan_ap_l_stage
            --              SET     ERROR_FLAG ='E',ERROR_MSG = 'Header validation failed'|| v_hdr_err_msg
            --              WHERE   1=1--ROWID = rec_inv_line.ROWID
            --             AND     invoice_num = rec_inv_hdr.invoice_num;
            v_error_head_count := v_error_head_count + 1;

            IF v_error_almsg IS NOT NULL
            THEN
               fnd_file.put_line (fnd_file.output,
                                     RPAD (NVL (rec_inv_hdr.vendor_num, ' '),
                                           20
                                          )
                                  || RPAD (rec_inv_hdr.invoice_num, 20)
                                  || 'LINE ERROR :'
                                  || v_error_almsg
                                 );
            END IF;
         ELSIF v_hdr_err_msg IS NULL AND v_error_line_count <> 0
         THEN
---    FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'SHAILESH - updating HDR staging error ');
            UPDATE xxabrl.xxabrl_baan_ap_h_stage
               SET error_flag = 'E',
                   error_msg = v_error_line_count || 'ERROR IN LINES'
             WHERE ROWID = rec_inv_hdr.ROWID;

            p_vendor_site_id := NULL;

            IF v_error_almsg IS NOT NULL
            THEN
               fnd_file.put_line (fnd_file.output,
                                     RPAD (NVL (rec_inv_hdr.vendor_num, ' '),
                                           20
                                          )
                                  || RPAD (rec_inv_hdr.invoice_num, 20)
                                  || 'LINE ERROR :'
                                  || v_error_almsg
                                 );
            END IF;
         ELSE             --              IF NVL(v_error_flag, 'N') = 'Y' THEN
---    DBMS_OUTPUT.PUT_LINE('inseide else part' || ' ' || v_error_flag || ' ' || 'value for ');

            /*
            UPDATE  apps.xxabrl_baan_ap_h_stage
             SET     ERROR_FLAG ='V',
                    ---vendor_site_id=v_vendor_site_id,
                    vendor_num=rec_inv_hdr.vendor_num ,
                    ERROR_MSG = NULL
             WHERE ROWID = rec_inv_hdr.ROWID;
             */

            -- Updated by Shailesh on 25 OCT 2008
            -- As insert program is running only for the one vendor site for all vendors in stg table so we include the cloumn vendor site id in hdr stg table
            -- and it will store the site id for respective vendor
            -- it will also store the terms id if it is available in vendor master

            --    20TH DEC 2014
            BEGIN
               fnd_file.put_line
                     (fnd_file.LOG,
                      '20th Dec 2014 Updating APPS.xxabrl_baan_ap_h_stage : '
                     );
               fnd_file.put_line (fnd_file.LOG,
                                     'TESTING : '
                                  || 'INVOICE_NUM : '
                                  || rec_inv_hdr.invoice_num
                                 );
               fnd_file.put_line (fnd_file.LOG,
                                     'Testing : '
                                  || 'Vendor_num : '
                                  || rec_inv_hdr.vendor_num
                                 );
               fnd_file.put_line (fnd_file.LOG,
                                     'Testing : '
                                  || ' VENDOR_ID  : '
                                  || p_vendor_id
                                 );
               fnd_file.put_line (fnd_file.LOG,
                                     'Testing : '
                                  || ' DESCRIPTION : '
                                  || rec_inv_hdr.description
                                 );
               --    FND_FILE.PUT_LINE (FND_FILE.log,'Testing : ' ||' VENDOR_SITE_ID  : ' || P_VENDOR_SITE_ID);
               --    FND_FILE.PUT_LINE (FND_FILE.log,'Testing : ' ||' TERMS_ID   : ' ||L_TERMS_ID);
               --    FND_FILE.PUT_LINE (FND_FILE.log,'Testing : ' ||' PAYMENT_METHOD_LOOKUP_CODE : ' || P_PAYMENT_METHOD_LOOKUP_CODE);
               fnd_file.put_line (fnd_file.LOG,
                                     'Testing : '
                                  || ' ACCTS_PAY_CODE_CONCATENATED: '
                                  || l_dist_code_concatenated
                                 );

               UPDATE xxabrl.xxabrl_baan_ap_h_stage
                  SET error_flag = 'V',
-- VENDOR_SITE_ID=V_VENDOR_SITE_ID,
                      vendor_num = rec_inv_hdr.vendor_num,
                      vendor_site_id = p_vendor_site_id,
                      terms_id = l_terms_id,
-- ORG_ID                      = P_ORG_ID,
                      vendor_id = p_vendor_id,
                      payment_method_lookup_code =
                                                  p_payment_method_lookup_code,
                      accts_pay_code_concatenated = l_dist_code_concatenated,
                      error_msg = NULL
                WHERE ROWID = rec_inv_hdr.ROWID;

               COMMIT;
            EXCEPTION
               WHEN OTHERS
               THEN
                  fnd_file.put_line
                                   (fnd_file.LOG,
                                       'ERROR WHILE UPDATING STAGE HEADER : '
                                    || SQLCODE
                                   );
                  fnd_file.put_line (fnd_file.LOG, ' ERROR CODE: ' || SQLERRM);
            END;

--    20TH DEC 2014

            ---    IF YOU WANT TO INSERT THE DATA FOR MULTI ORG THEN CREATE COLUMN ORG_ID IN  APPS.xxabrl_baan_ap_h_stage TABLE
---    AND  PUT THIS STMT IN ABOVE UPDATE CONDITION  ORG_ID=P_ORG_ID
---    ALSO INCLUDE THIS COLOUMN IN INSERT STMT WHILE INSERTING TO INTERFACE TABLE FOR HDR

            --  added by Gonvind on 25 june 2009
            p_vendor_site_id := NULL;
--    Retek Matched Invoices
            v_success_record_count := v_success_record_count + 1;
         --    DBMS_OUTPUT.PUT_LINE('before end loop' || ' ' || v_success_record_count || ' ' || 'value for ');
         END IF;      --                  IF NVL(v_error_flag, 'N') = 'Y' THEN
      END LOOP;                                                 --    1st loop

      fnd_file.put_line (fnd_file.LOG,
                            'ORG ID --> INSERT TRANSACTION PASS ORG_ID -> '
                         || p_org_id
                        );
      insert_transaction (p_org_id,
                          p_vendor_site_id,
                          p_vendor_id,
                          p_payment_method_lookup_code
                         );

      --IF NVL(v_line_err_msg,'N') <> NULL THEN
      --   v_error_head_count := v_error_line_count +1 ;
      --END IF;

      --v_error_head_count := v_error_line_count +1;

      --SELECT COUNT(invoice_num)
      --INTO   v_total_records
      --FROM   apps.xxabrl_baan_ap_h_stage
      --WHERE  NVL(error_flag,'N') in ('N','E');
      IF v_error_head_count > 0
      THEN
         retcode := 1;
      END IF;

      fnd_file.put_line
         (fnd_file.output,
          '-------------------------------------------------------------------------------------------------'
         );
      fnd_file.put_line (fnd_file.output, CHR (10));
      fnd_file.put_line (fnd_file.output, CHR (10));
      fnd_file.put_line (fnd_file.output, CHR (10));
      fnd_file.put_line (fnd_file.output,
                            'TOTAL NUMBER OF INVOICES FETCHED :'
                         || v_total_records
                        );
      fnd_file.put_line (fnd_file.output,
                            'TOTAL NUMBER OF RECORDS WITH ERROR :'
                         || v_error_head_count
                        );
      fnd_file.put_line (fnd_file.output,
                            'TOTAL NUMBER OF INVOICES PROCESSED :'
                         || v_success_record_count
                        );
      --DBMS_OUTPUT.PUT_LINE('TOTAL NUMBER OF RECORDS WITH ERROR :'|| v_error_head_count);
      --DBMS_OUTPUT.PUT_LINE('TOTAL NUMBER OF INVOICES PROCESSED      :'|| v_success_record_count);
      fnd_file.put_line
         (fnd_file.output,
          '........................................................................'
         );
   END validate_invoices;

--      End of VALIDATE_INVOICES for all tables

   /*=================================Procedure INSERT_TRANSACTION =============================
   =========================*/
   PROCEDURE insert_transaction (
      p_org_id                       IN   NUMBER,
      p_vendor_site_id               IN   NUMBER,
      p_vendor_id                    IN   NUMBER,
      p_payment_method_lookup_code   IN   VARCHAR2
   )
   IS
      v_error_flag                   VARCHAR2 (1);
      v_errmsg                       VARCHAR2 (6000);
      v_header_id                    NUMBER;
      v_line_id                      NUMBER;
      v_errorinvoice                 BOOLEAN;
      v_errorlines                   BOOLEAN;
      --v_org_id                                NUMBER :=NULL;-- := 82;
      --v_user_id                                  NUMBER     :=FND_PROFILE.value('USER_ID');
      --v_resp_id                                  NUMBER     :=FND_PROFILE.value('RESP_ID');
      --v_appl_id                                  NUMBER     :=FND_PROFILE.value('RESP_APPL_ID');
      --v_req_id                                    NUMBER;
      v_record_count                 NUMBER          := 0;
      v_org_id                       NUMBER;
      v_vendor_site_id               NUMBER (15);
      v_vendor_id                    NUMBER (15);
      v_payment_method_lookup_code   VARCHAR2 (30);
      -- added by shailesh for storing the location of the retake payable invoice
      l_location                     VARCHAR2 (50);

      /*
         CURSOR   cur_inv_hdr IS
            SELECT    rowid,aps.*
         FROM        apps.xxabrl_baan_ap_h_stage aps
         WHERE       NVL(ERROR_FLAG,'N') = 'V';
      */

      ---     Added by shailesh on 25 OCT 2008
---    Above commented cursor replaced by new cursor
      CURSOR cur_inv_hdr
      IS
         SELECT aps.ROWID, aps.invoice_num, aps.invoice_type_lookup_code,
                aps.invoice_date, aps.vendor_num, aps.terms_id,
                ROUND (aps.invoice_amount, 2) invoice_amount,
                aps.invoice_currency_code, aps.exchange_date,
                aps.exchange_rate_type, aps.description, aps.gl_date,
                aps.GROUP_ID, aps.vendor_email_address, aps.terms_date,
                aps.accts_pay_code_concatenated, aps.error_flag,
                aps.error_msg, aps.vendor_site_id, aps.org_id, aps.vendor_id,
                aps.payment_method_lookup_code, aps.goods_received_date,aps.SOURCE
           -- New coloumn added in the  xxabrl.xxabrl_baan_ap_h_stage
         FROM   xxabrl.xxabrl_baan_ap_h_stage aps
          WHERE 1 = 1
            AND TRUNC (aps.gl_date) > '31-MAR-13'
            AND NVL (aps.error_flag, 'N') = 'V'
            and (source='BAAN' OR source='TSS_OPEN_INVOICE');

      /*
      CURSOR   cur_inv_line(p_invoice_num VARCHAR2,
                            p_vendor_num NUMBER) IS
       SELECT   rowid,als.*
       FROM     apps.xxabrl_baan_ap_l_stage  als
       WHERE    NVL(ERROR_FLAG,'N') = 'V'
       AND      als.invoice_num = p_invoice_num
      ---AND      als.vendor_site_id = p_vendor_site_id
      AND      als.vendor_num = p_vendor_num;
      */

      -- Added by shailesh on 25 OCT 2008
      -- Above commented cursor replaced by new cursor
      CURSOR cur_inv_line (p_invoice_num VARCHAR2, p_vendor_num VARCHAR2)
      IS
         SELECT als.ROWID, als.vendor_num, als.vendor_site_id,
                als.invoice_num, als.line_number, als.line_type_lookup_code,
                ROUND (als.amount, 2) amount, als.accounting_date,
                als.dist_code_concatenated, als.description, als.tds_tax_name,
                als.wct_tax_name, als.service_tax, als.vat, als.error_flag,
                als.error_msg
           FROM xxabrl.xxabrl_baan_ap_l_stage als
          WHERE NVL (error_flag, 'N') = 'V'
            AND als.invoice_num = p_invoice_num
            ---AND      als.vendor_site_id = p_vendor_site_id
            AND als.vendor_num = p_vendor_num;
   BEGIN
--   commented by shailesh on 20th june 2009
    --v_org_id                     := p_org_id;
    --v_vendor_site_id             := p_vendor_site_id;
    --v_vendor_id                  := p_vendor_id;
    --v_payment_method_lookup_code := p_payment_method_lookup_code;
      FOR rec_inv_hdr IN cur_inv_hdr
      LOOP
         EXIT WHEN cur_inv_hdr%NOTFOUND;
         v_header_id := NULL;
         v_errorinvoice := NULL;
         v_errmsg := NULL;
         v_record_count := cur_inv_hdr%ROWCOUNT;
         --v_payment_method_lookup_code          := NULL;
         --v_org_id                              := NULL;
         --v_vendor_id                           := NULL;

         --           SELECT asr.vendor_id,assa.org_id,assa.payment_method_lookup_code
         --           INTO   v_vendor_id,v_org_id,v_payment_method_lookup_code
         --           FROM   ap_suppliers asr,ap_supplier_sites_all assa
         --           WHERE  asr.vendor_id=assa.vendor_id
         --           AND    asr.segment1=rec_inv_hdr.vendor_num --'9800012'--'987676'--'9800012'
         --           AND       assa.VENDOR_SITE_ID=rec_inv_hdr.vendor_site_id
         --           AND    org_id = fnd_profile.value('org_id');
         v_header_id := 0;

         SELECT ap_invoices_interface_s.NEXTVAL
           INTO v_header_id
           FROM DUAL;

         BEGIN
            fnd_file.put_line (fnd_file.LOG,
                                  'INSERTING RECORD FOR  -> '
                               || rec_inv_hdr.invoice_num
                               || '  '
                               || rec_inv_hdr.org_id
                              );

            INSERT INTO ap_invoices_interface
                        (invoice_id, invoice_num,
                         invoice_type_lookup_code,
                         invoice_date, vendor_id,
                         vendor_site_id,
                         invoice_amount,
                         invoice_currency_code
                                              --,EXCHANGE_RATE
            ,
                         exchange_rate_type,
                         exchange_date, terms_id,
                         description
                                    --,TERMS_NAME
            ,            last_update_date, last_updated_by,
                         last_update_login, creation_date, created_by
                                                                     --,GROUP_ID
            ,
                         gl_date, org_id,
                         vendor_email_address, SOURCE,
                         terms_date,
                         payment_method_code,
                         goods_received_date
                                            -- added by shailesh on 25 oct 2008
            ,
                         accts_pay_code_concatenated
                        )
                 VALUES (v_header_id, rec_inv_hdr.invoice_num,
                         rec_inv_hdr.invoice_type_lookup_code,
                         rec_inv_hdr.invoice_date, rec_inv_hdr.vendor_id,
                         rec_inv_hdr.vendor_site_id
                                                   -- CHANGE BY SHAILESH ON 25 oct2008 ORIGINAL IS  v_vendor_site_id
            ,
                         rec_inv_hdr.invoice_amount,
                         rec_inv_hdr.invoice_currency_code
                                                          --,rec_inv_hdr.EXCHANGE_RATE
            ,
                         rec_inv_hdr.exchange_rate_type,
                         rec_inv_hdr.exchange_date, rec_inv_hdr.terms_id,
                         rec_inv_hdr.description
                                                --,rec_inv_hdr.TERMS_NAME
            ,            SYSDATE, 3,
                         3, SYSDATE, 3
                                      --,rec_inv_hdr.GROUP_ID
            ,
                         rec_inv_hdr.gl_date, rec_inv_hdr.org_id,
                         rec_inv_hdr.vendor_email_address, rec_inv_hdr.SOURCE,
                         rec_inv_hdr.terms_date,
                         -- v_payment_method_lookup_code,
                         rec_inv_hdr.payment_method_lookup_code,
                         -- above stmt is changed by shailesh on 20th june 2009
                         rec_inv_hdr.goods_received_date
                                                        -- added by shailesh on 25 oct 2008
            ,
                         rec_inv_hdr.accts_pay_code_concatenated
                        );

            COMMIT;
            v_errorinvoice := FALSE;
         EXCEPTION
            WHEN OTHERS
            THEN
               v_errorinvoice := TRUE;
               v_errmsg :=
                     'AFTER INSERT INTO AP INVOICES INTERFACE FAILED IN EXCEPTION'
                  || SUBSTR (SQLERRM, 1, 150);
               fnd_file.put_line (fnd_file.output, v_errmsg);
         END;

         IF v_errorinvoice = TRUE
         THEN
            UPDATE xxabrl.xxabrl_baan_ap_h_stage
               SET error_flag = 'E',
                   error_msg = v_errmsg
             WHERE ROWID = rec_inv_hdr.ROWID;

            --and CURRENT OF cur_inv_hdr;
            UPDATE xxabrl.xxabrl_baan_ap_l_stage
               SET error_flag = 'E',
                   error_msg = 'Header Inserting failed' || v_errmsg
             WHERE invoice_num = rec_inv_hdr.invoice_num;
         --and CURRENT OF cur_inv_hdr;
         ELSE
            UPDATE xxabrl.xxabrl_baan_ap_h_stage
               SET error_flag = 'P',
                   error_msg =
                             'SUCCESSFULLY INSERTED IN HEADER INTERFACE TABLE'
             WHERE ROWID = rec_inv_hdr.ROWID;
         --and CURRENT OF cur_inv_hdr;
         END IF;

         FOR rec_inv_line IN cur_inv_line (rec_inv_hdr.invoice_num
                                                                  --,rec_inv_hdr.vendor_site_id
                            ,
                                           rec_inv_hdr.vendor_num
                                          )
         LOOP
            EXIT WHEN cur_inv_line%NOTFOUND;
            v_errorinvoice := FALSE;
            v_errmsg := NULL;
            v_line_id := NULL;

            SELECT ap_invoice_lines_interface_s.NEXTVAL
              INTO v_line_id
              FROM DUAL;

            BEGIN
               -- following code added by shailesh on 26 OCT 2008
               -- It will extract the segment4 from dist_code_combinition and  get the location  for the line level dff
               BEGIN
                  SELECT ffvv.flex_value                   --,ffvv.description
                    INTO l_location
                    FROM fnd_flex_values_vl ffvv, fnd_flex_value_sets ffvs
                   WHERE flex_value_set_name = 'ABRL_GL_Location'
                     AND ffvs.flex_value_set_id = ffvv.flex_value_set_id
                     AND ffvv.flex_value =
                            SUBSTR (rec_inv_line.dist_code_concatenated, 12,
                                    7);
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_location := NULL;
               END;

               /*
               SELECT * FROM AP_INVOICES_INTERFACE
               WHERE INVOICE_NUM='INV_9983'

               SELECT * FROM AP_INVOICE_LINES_INTERFACE
               WHERE  INVOICE_ID=700042
               */

               /*
               select * from AP_INVOICES_INTERFACE
               where invoice_num in ('INV_9983','INV_10373')

               select * from AP_INVOICE_LINES_INTERFACE
               where invoice_id= 700041 --700042
               */
               INSERT INTO ap_invoice_lines_interface
                           (invoice_id,
                            invoice_line_id,
                            line_number,
                            line_type_lookup_code,
                            amount
                                  --,TAX_CODE
               ,
                            dist_code_concatenated,
                            description,
                            accounting_date, org_id,
                            attribute_category,
                            attribute1, last_updated_by, last_update_date,
                            last_update_login, created_by, creation_date
                           )
                    VALUES (v_header_id,
                            v_line_id          --,rec_inv_line.INVOICE_LINE_ID
                                     ,
                            rec_inv_line.line_number,
                            rec_inv_line.line_type_lookup_code,
                            rec_inv_line.amount
                                               --,rec_inv_line.TAX_CODE
               ,
                            rec_inv_line.dist_code_concatenated,
                            rec_inv_line.description,
                            rec_inv_line.accounting_date,
                                                         --v_org_id,
                                                         -- ADDED BY SHAILESH ON 11 MAY 2009  ORG ID IS COMMING FROM HEADER
                                                         rec_inv_hdr.org_id,
                            'BAAN PAYABLE INVOICE'        --ADDED BY SHAILESH
                                                   ,
                            l_location                    -- ADDED BY SHAILESH
                                      , 3, SYSDATE,
                            3, 3, SYSDATE
                           );

               COMMIT;
               v_errorlines := FALSE;
            EXCEPTION
               WHEN OTHERS
               THEN
                  v_errorlines := TRUE;
                  v_errmsg :=
                        'AFTER INSERT INTO AP INVOICE LINES INTERFACE FAILED IN EXCEPTION'
                     || SUBSTR (SQLERRM, 1, 150);
                  fnd_file.put_line (fnd_file.output, v_errmsg);
            END;

            IF v_errorlines = TRUE
            THEN
               UPDATE xxabrl.xxabrl_baan_ap_l_stage
                  SET error_flag = 'E',
                      error_msg = v_errmsg
                WHERE ROWID = rec_inv_line.ROWID;
            --and CURRENT OF cur_inv_line;
            ELSE
               UPDATE xxabrl.xxabrl_baan_ap_l_stage
                  SET error_flag = 'P',
                      error_msg =
                               'SUCCESSFULLY INSERTED IN LINE INTERFACE TABLE'
                WHERE ROWID = rec_inv_line.ROWID;
            -- and CURRENT OF cur_inv_line;
            END IF;
         END LOOP;
---
---    RUN PAYABLE OPEN ITERFACE FOR INTERFACE DATA INTO ORACLE PAYABLES
---

      /*

          If v_record_count>0 Then
                            FND_GLOBAL.APPS_INITIALIZE (
                                  user_id=> v_user_id,
                                  resp_id=> v_resp_id,
                                  resp_appl_id=> v_appl_id
                               );
                         COMMIT;
                         v_req_id :=fnd_request.submit_request (
                                        'SQLAP',
                                      'APXIIMPT',
                                       'Payables Open Interface Import'||P_Data_Source,
                                  NULL,
                                        FALSE,
                               p_org_id,
                                      P_Data_Source, -- Parameter Data source
                                       NULL, -- Group id
                                    V_Batch_Name,
                                       NULL,
                                    NULL,
                                    NULL,
                                    'N',
                              'N',
                                 'N',
                              'Y',
                                       CHR (0)
                                  );
                         Commit;
                         fnd_file.put_line(fnd_file.output,'Please see the output of Payables OPEN Invoice Import program request id :'||v_req_id);
                         fnd_file.put_line(fnd_file.output,'........................................................................' );
                         Loop
                            Begin
                               V_Status:=Null;
                               Select Status_Code
                               Into     V_Status
                               from      Fnd_Concurrent_Requests
                               where  Request_id=v_req_id;
                               If V_Status in ('C','D','E','G','X') then
                                     Exit;
                               End If;
                            EXCEPTION
                               When Others Then
                                     Exit;
                            End;
                         End Loop;
                   End If;

      */
      END LOOP;

---    Added on 21-jun-2008

      ---
---    COMMENTED BY SHAILESH ON 22 JUNE 2009 FOR TEMP TESTING.
---    DELETE FROM xxabrl_baan_ap_h_stage WHERE ERROR_FLAG = 'P';
---    DELETE FROM xxabrl_baan_ap_l_stage WHERE ERROR_FLAG = 'P';
---
      COMMIT;
   END insert_transaction;
--        END OF PROCEDURE INSERT_TRANSACTION

--
--
--

/*    01.12.2013 Bala.SK     Not Required This Update Was Required Due To Tsrl Migration As Reim Wasn't Sending Correct Org_Id Properly And Now Its Ok

--
--
--


--    Payables Header Stage Updation Of Code Combination  Forsegment 3

            Update Apps.xxabrl_baan_ap_h_stage Aps
            Set Accts_Pay_Code_Concatenated=Substr(Aps.Accts_Pay_Code_Concatenated,1,7)||'752'||Substr(Aps.Accts_Pay_Code_Concatenated,11,37),
            Org_Id=801
            Where  Nvl(Aps.Error_Flag, 'N') In ('N', 'E')
           And Gl_Date>'31-Mar-2012'
            And Org_Id=84;

            Commit;


            Update Apps.xxabrl_baan_ap_h_stage Aps
            Set Accts_Pay_Code_Concatenated=Substr(Aps.Accts_Pay_Code_Concatenated,1,7)||'812'
            ||Substr(Aps.Accts_Pay_Code_Concatenated,11,37),
            Org_Id=901
            Where  Nvl(Aps.Error_Flag, 'N') In ('N', 'E')
           And Gl_Date>'31-Mar-2012'
            And Org_Id=89;


             Commit;

            Update Apps.xxabrl_baan_ap_h_stage Aps
            Set Accts_Pay_Code_Concatenated=Substr(Aps.Accts_Pay_Code_Concatenated,1,7)||'772'
            ||Substr(Aps.Accts_Pay_Code_Concatenated,11,37),
            Org_Id=861
            Where   Nvl(Aps.Error_Flag, 'N') In ('N', 'E')
            And Gl_Date>'31-Mar-2012'
            And Org_Id=87;


            Commit;


        Update Apps.xxabrl_baan_ap_h_stage Aps
            Set Accts_Pay_Code_Concatenated=Substr(Aps.Accts_Pay_Code_Concatenated,1,7)||'792'
            ||Substr(Aps.Accts_Pay_Code_Concatenated,11,37),
            Org_Id=961
            Where     Nvl(Aps.Error_Flag, 'N') In ('N', 'E')
            And Gl_Date>'31-Mar-2012'
            And Org_Id=91;

          Commit;



----Lines Tables


            Update Apps.xxabrl_baan_ap_l_stage Als
            Set Dist_Code_Concatenated=Substr(Dist_Code_Concatenated,1,7)||'752'
            ||Substr(Dist_Code_Concatenated,11,37)
             Where  (Invoice_Num,Vendor_Num) In (Select Invoice_Num,Vendor_Num From xxabrl_baan_ap_h_stage Aps
            Where   Nvl(Aps.Error_Flag, 'N') In ('N', 'E')
           And Gl_Date>'31-Mar-2012'
            And Org_Id=801)
          And Accounting_Date>'31-Mar-2012'
            And Nvl(Als.Error_Flag, 'N') In ('N', 'E');


            Update Apps.xxabrl_baan_ap_l_stage Als
            Set Dist_Code_Concatenated=Substr(Dist_Code_Concatenated,1,7)||'812'
            ||Substr(Dist_Code_Concatenated,11,37)
            Where  (Invoice_Num,Vendor_Num) In (Select Invoice_Num,Vendor_Num From xxabrl_baan_ap_h_stage Aps
            Where Gl_Date>'31-Mar-2012'
            And Org_Id=901
            And Nvl(Aps.Error_Flag, 'N') In ('N', 'E'))
            And Accounting_Date>'31-Mar-2012'
            And Nvl(Als.Error_Flag, 'N') In ('N', 'E');


            Update Apps.xxabrl_baan_ap_l_stage Als
            Set Dist_Code_Concatenated=Substr(Dist_Code_Concatenated,1,7)||'772'
            ||Substr(Dist_Code_Concatenated,11,37)
            Where (Invoice_Num,Vendor_Num) In (Select Invoice_Num,Vendor_Num From xxabrl_baan_ap_h_stage Aps
            Where Gl_Date>'31-Mar-2012'
            And Org_Id=861
            And Nvl(Aps.Error_Flag, 'N') In ('N', 'E'))
            And Accounting_Date>'31-Mar-2012'
            And Nvl(Als.Error_Flag, 'N') In ('N', 'E');


            Update Apps.xxabrl_baan_ap_l_stage Als
            Set Dist_Code_Concatenated=Substr(Dist_Code_Concatenated,1,7)||'792'
            ||Substr(Dist_Code_Concatenated,11,37)
            Where  (Invoice_Num,Vendor_Num) In (Select Invoice_Num,Vendor_Num From xxabrl_baan_ap_h_stage Aps
            Where  Nvl(Aps.Error_Flag, 'N') In ('N', 'E')
            And Gl_Date>'31-Mar-2012'
            And Org_Id=961)
            And Accounting_Date>'31-Mar-2012'
            And Nvl(Als.Error_Flag, 'N') In ('N', 'E');

            Commit;

01.12.2013 Bala.Sk     Not Required This Update Was Required Due To Tsrl Migration As Reim Wasn't Sending Correct Org_Id Properly And Now Its Ok
*/

/*    01.11.2013    Bala.Sk    Not Required Due To Unique Vendor Solution


    For Rec_Inv_Hdr In Cur_Inv_Hdr Loop
      Exit When Cur_Inv_Hdr%Notfound;

     P_Org_Id:=0;
     P_Vendor_Site_Id :=0;
     P_Vendor_Id:=0;
      V_Error_Flag                 := Null;
      V_Hdr_Err_Msg                := Null;
      V_Vendor_Id                  := Null;
      V_Vendor_Site_Id             := Null;
      V_Invoice_Currency_Code      := Null;
      V_Payment_Method_Lookup_Code := Null;
      -- Rec_Inv_Hdr.Vendor_Num                        := Null;
      V_Check_Flag      := 'N';
      V_Pay_Lookup_Code := Null;
      --V_Error_Head_Count                   := 0;
      V_Inv_Line_Amt := 0;
      V_Inv_Type     := Null;
      --V_Success_Record_Count                := 0;
      V_Sup_Status := 'N';

      --    P_Org_Id                               := Null;
      --    P_Vendor_Site_Id                       := Null;
      --    P_Vendor_Id                            := Null;
      --    P_Payment_Method_Lookup_Code           := Null;
      --    V_Pay_Site_Flag                        := Null;
      V_Flag_Count := 0;
      V_Pay_Count  := 0;
      V_Rfq_Count  := 0;
      V_Pur_Count  := 0;
      V_Ho_Count   := 0;


--    Added By Shailesh On 19 June 2009 As Org Id Is Comming Null In Data File So If Org Id Is Null
--    Validation For The Org Id If It Is Null In Staging Table It Will Give Error

 Begin

        Fnd_File.Put_Line(Fnd_File.Log,'Validation For Org Id -> '||Rec_Inv_Hdr.Org_Id);

      Select Count(Organization_Id)
      Into V_Org_Cnt
      From Apps.Hr_Operating_Units Hou
      Where Hou.Organization_Id=Rec_Inv_Hdr.Org_Id;

    ---- We Need To Skip Full Validation In This Case If Org_Id Is Null

      Exception

      When No_Data_Found Then
       V_Error_Flag  := 'Y';
       V_Hdr_Err_Msg := 'Org Is Null Or Org Not Available';
       Fnd_File.Put_Line(Fnd_File.Log,'Org Is Null Or Org Not Available-> '||V_Error_Flag);
      When Others Then
             V_Error_Flag  := 'Y';
             V_Hdr_Err_Msg := 'Org Is Null Or Org Not Available';

     -- Fnd_File.Put_Line(Fnd_File.Log,'Error In Org Id -> '||V_Error_Flag);

    End ;


If V_Org_Cnt = 0 Then
     V_Error_Flag := 'Y';
     V_Hdr_Err_Msg := 'Org Is Null Or Org Not Available';
      P_Vendor_Site_Id             := Null;
      P_Vendor_Id                  := Null;
      P_Payment_Method_Lookup_Code := Null;
   --  Fnd_File.Put_Line(Fnd_File.Log,'Error Occured In If Cond-> '||V_Error_Flag);
 End If;


    --    Fnd_File.Put_Line(Fnd_File.Log,'Error Flag -> '||V_Error_Flag);

      If Rec_Inv_Hdr.Vendor_Num Is Null Then

        Dbms_Output.Put_Line('No Supplier For Invoice Number');

      Else

    --  Cur_Inv_Hdr

    Fnd_File.Put_Line(Fnd_File.Log,'Org Id Parameter --> Cur_Inv_Hdr --> '||Rec_Inv_Hdr.Org_Id);


    Select Count(*) Into V_Pay_Count
    From Ap_Suppliers Asup, Ap_Supplier_Sites_All Asups
    Where 1=1
    And    Asup.Vendor_Id                                                     = Asups.Vendor_Id
    And     Asup.Segment1                                                     = Rec_Inv_Hdr.Vendor_Num
    And     Asups.Org_Id                                                        = Rec_Inv_Hdr.Org_Id
    And     Nvl(Trunc(Asups.Inactive_Date), Trunc(Sysdate))        >= Trunc(Sysdate)
    And     Nvl(Asups.Pay_Site_Flag, 'N')                                     = 'Y';


        Select Count(*)
          Into V_Flag_Count
          From Ap_Suppliers Asup, Ap_Supplier_Sites_All Asups
         Where Asup.Vendor_Id = Asups.Vendor_Id
           And Asup.Segment1 = Rec_Inv_Hdr.Vendor_Num
           -- Following Condition Added By Shailesh On  27 Apr 2009 Count Will Come According To Org_Id
           And Asups.Org_Id=Rec_Inv_Hdr.Org_Id
           --And Asups.Inactive_Date Is Null
          And Nvl(Trunc(Asups.Inactive_Date), Trunc(Sysdate)) >= Trunc(Sysdate)
           And ((Nvl(Asups.Pay_Site_Flag, 'N') = 'N' And
               Nvl(Asups.Purchasing_Site_Flag, 'N') = 'N' And
               Nvl(Asups.Rfq_Only_Site_Flag, 'N') = 'Y') Or
               (Nvl(Asups.Pay_Site_Flag, 'N') = 'N' And
               Nvl(Asups.Purchasing_Site_Flag, 'N') = 'Y' And
               Nvl(Asups.Rfq_Only_Site_Flag, 'N') = 'N') Or
               (Nvl(Asups.Pay_Site_Flag, 'N') = 'Y' And
               Nvl(Asups.Purchasing_Site_Flag, 'N') = 'N' And
               Nvl(Asups.Rfq_Only_Site_Flag, 'N') = 'N') Or
               (Nvl(Asups.Pay_Site_Flag, 'N') = 'N' And
               Nvl(Asups.Purchasing_Site_Flag, 'N') = 'N' And
               Nvl(Asups.Rfq_Only_Site_Flag, 'N') = 'N'));


        Select Count(*)
          Into V_Rfq_Count
          From Ap_Suppliers Asup, Ap_Supplier_Sites_All Asups
         Where Asup.Vendor_Id = Asups.Vendor_Id
          -- Following Condition Added By Shailesh On  27 Apr 2009 Count Will Come According To Org_Id
           And Asups.Org_Id=Rec_Inv_Hdr.Org_Id
           And Nvl(Trunc(Asups.Inactive_Date), Trunc(Sysdate)) >= Trunc(Sysdate)
           And Asup.Segment1 = Rec_Inv_Hdr.Vendor_Num
           And Nvl(Asups.Rfq_Only_Site_Flag, 'N') = 'Y';

        Select Count(*)
          Into V_Pur_Count
          From Ap_Suppliers Asup, Ap_Supplier_Sites_All Asups
         Where Asup.Vendor_Id = Asups.Vendor_Id
          -- Following Condition Added By Shailesh On  27 Apr 2009 Count Will Come According To Org_Id
           And Asups.Org_Id=Rec_Inv_Hdr.Org_Id
           And Nvl(Trunc(Asups.Inactive_Date), Trunc(Sysdate)) >= Trunc(Sysdate)
           And Asup.Segment1 = Rec_Inv_Hdr.Vendor_Num
           And Nvl(Asups.Purchasing_Site_Flag, 'N') = 'Y';

        Select Count(*)
          Into V_Ho_Count
          From Ap_Suppliers Asup, Ap_Supplier_Sites_All Asups
         Where Asup.Vendor_Id = Asups.Vendor_Id
          -- Following Condition Added By Shailesh On  27 Apr 2009 Count Will Come According To Org_Id
           And Asups.Org_Id=Rec_Inv_Hdr.Org_Id
           And Asup.Segment1 = Rec_Inv_Hdr.Vendor_Num
           And Nvl(Trunc(Asups.Inactive_Date), Trunc(Sysdate)) >= Trunc(Sysdate)
           And Nvl(Asups.Pay_Site_Flag, 'N') = 'N'
           And Nvl(Asups.Rfq_Only_Site_Flag, 'N') = 'N'
           And Nvl(Asups.Purchasing_Site_Flag, 'N') = 'N';


      End If;

-- ======================= Check For Suppliers Status=================

      If Rec_Inv_Hdr.Vendor_Num Is Not Null Then

        Begin

          Select 'Y'
            Into V_Sup_Status
            From Ap_Suppliers
           Where Segment1 = Rec_Inv_Hdr.Vendor_Num
             And Sysdate Between Nvl(Start_Date_Active, Sysdate) And
                 Nvl(End_Date_Active, Sysdate); -- Added By Lalit Dt 27-Jun-2008


        Exception
          When No_Data_Found Then
            V_Error_Flag  := 'Y';
            V_Hdr_Err_Msg := 'Invalid Supplier';
            Fnd_File.Put_Line(Fnd_File.Output,
                              Rpad(Nvl(Rec_Inv_Hdr.Vendor_Num, ' '), 20) ||
                              Rpad(Rec_Inv_Hdr.Invoice_Num, 20) ||
                              V_Hdr_Err_Msg);
        End;

      End If;

      If V_Sup_Status = 'Y' Then

        For Rec_Check_Flags In Cur_Check_Flags(Rec_Inv_Hdr.Vendor_Num,Rec_Inv_Hdr.Org_Id) Loop
          Exit When Cur_Check_Flags%Notfound;

          --- Added By Govind On 24 June 2009 Vendor Site Id Validation.

          Begin

               If  Rec_Check_Flags.Vendor_Site_Id Is Null Then

               V_Error_Flag  := 'Y';

               Fnd_File.Put_Line(Fnd_File.Log,'Org Is Null Or Org Not Available ');
               Fnd_File.Put_Line(Fnd_File.Output,'Vendor Site Is Not Available-> '||V_Error_Flag);

               P_Vendor_Site_Id             := Null;

               End If;

          End;


          If Cur_Check_Flags%Notfound Then

            V_Error_Flag  := 'Y';
            V_Hdr_Err_Msg := 'Supplier Is Not Avialable';
            Fnd_File.Put_Line(Fnd_File.Output,
                              Rpad(Nvl(Rec_Inv_Hdr.Vendor_Num, ' '), 20) ||
                              Rpad(Rec_Inv_Hdr.Invoice_Num, 20) ||
                              V_Hdr_Err_Msg);
            Close Cur_Check_Flags;

          Else

            If V_Pay_Count > 1 Then

              V_Error_Flag  := 'Y';
              V_Hdr_Err_Msg := 'Multiple Pay Site Defined';
              Fnd_File.Put_Line(Fnd_File.Output,
                                Rpad(Nvl(Rec_Inv_Hdr.Vendor_Num, ' '), 20) ||
                                Rpad(Rec_Inv_Hdr.Invoice_Num, 20) ||
                                V_Hdr_Err_Msg);
              Exit;

            End If;


--  01.11.2013    Bala.Sk    Not Required Due To Unique Vendor Solution

            If V_Pay_Count = 1 And V_Rfq_Count = 1 And V_Pur_Count = 1 And V_Ho_Count = 1 Then

              If V_Flag_Count = 4 Then

                If Nvl(Rec_Check_Flags.Pay_Site_Flag, 'N') = 'Y' Then

                  P_Org_Id                                          := Rec_Check_Flags.Org_Id;
                  P_Vendor_Site_Id                       := Rec_Check_Flags.Vendor_Site_Id;
                  P_Vendor_Id                                    := Rec_Check_Flags.Vendor_Id;
                  P_Payment_Method_Lookup_Code      := Rec_Check_Flags.Payment_Method_Lookup_Code;


             --- Reimed By Shailesh On 24 Apr 2009 Removed The Condition From It As Its Directly Comming From File

               --   Update Apps.xxabrl_baan_ap_h_stage
               --   Set Org_Id = Rec_Check_Flags.Org_Id
               --   Where Rowid = Rec_Inv_Hdr.Rowid;

                End If;

              Else                        --        If V_Flag_Count = 4 Then

                V_Error_Flag  := 'Y';
                V_Hdr_Err_Msg := 'Four Supplier Sites Not Defined In Apps';
                Fnd_File.Put_Line(Fnd_File.Output,
                                  Rpad(Nvl(Rec_Inv_Hdr.Vendor_Num, ' '), 20) ||
                                  Rpad(Rec_Inv_Hdr.Invoice_Num, 20) ||
                                  V_Hdr_Err_Msg);
                Exit;
              End If;

            Else            --        If V_Pay_Count = 1 And V_Rfq_Count = 1 And V_Pur_Count = 1 And V_Ho_Count = 1 Then

              If V_Pay_Count = 0 Then

                V_Error_Flag  := 'Y';
                V_Hdr_Err_Msg := 'No Pay Site Defined For The Vendor';
                Fnd_File.Put_Line(Fnd_File.Output,
                                  Rpad(Nvl(Rec_Inv_Hdr.Vendor_Num, ' '), 20) ||
                                  Rpad(Rec_Inv_Hdr.Invoice_Num, 20) ||
                                  V_Hdr_Err_Msg);
                Exit;
              End If;

              If V_Rfq_Count = 0 Then
                V_Error_Flag  := 'Y';
                V_Hdr_Err_Msg := 'No Return Site Defined For The Vendor';
                Fnd_File.Put_Line(Fnd_File.Output,
                                  Rpad(Nvl(Rec_Inv_Hdr.Vendor_Num, ' '), 20) ||
                                  Rpad(Rec_Inv_Hdr.Invoice_Num, 20) ||
                                  V_Hdr_Err_Msg);
                Exit;
              End If;

              If V_Pur_Count = 0 Then
                V_Error_Flag  := 'Y';
                V_Hdr_Err_Msg := 'No Purchase Site Defined For The Vendor';
                Fnd_File.Put_Line(Fnd_File.Output,
                                  Rpad(Nvl(Rec_Inv_Hdr.Vendor_Num, ' '), 20) ||
                                  Rpad(Rec_Inv_Hdr.Invoice_Num, 20) ||
                                  V_Hdr_Err_Msg);
                Exit;
              End If;

              If V_Ho_Count = 0 Then
                V_Error_Flag  := 'Y';
                V_Hdr_Err_Msg := 'No Ho Site Defined For The Vendor';
                Fnd_File.Put_Line(Fnd_File.Output,
                                  Rpad(Nvl(Rec_Inv_Hdr.Vendor_Num, ' '), 20) ||
                                  Rpad(Rec_Inv_Hdr.Invoice_Num, 20) ||
                                  V_Hdr_Err_Msg);
                Exit;
              End If;

              V_Error_Flag  := 'Y';
              V_Hdr_Err_Msg := 'Supplier Sites Setup In Apps Not As Per Vendor Outbound Defination';
              Fnd_File.Put_Line(Fnd_File.Output,
                                Rpad(Nvl(Rec_Inv_Hdr.Vendor_Num, ' '), 20) ||
                                Rpad(Rec_Inv_Hdr.Invoice_Num, 20) ||
                                V_Hdr_Err_Msg);
              Exit;

            End If;            --        If V_Pay_Count = 1 And V_Rfq_Count = 1 And V_Pur_Count = 1 And V_Ho_Count = 1 Then




--   01.11.2013    Bala.Sk    Not Required Due To Unique Vendor Solution


            If V_Pay_Count = 1 Then

                If Nvl(Rec_Check_Flags.Pay_Site_Flag, 'N') = 'Y' Then

                  P_Org_Id                        := Rec_Check_Flags.Org_Id;
                  P_Vendor_Site_Id                    := Rec_Check_Flags.Vendor_Site_Id;
                  P_Vendor_Id                        := Rec_Check_Flags.Vendor_Id;
                  P_Payment_Method_Lookup_Code        := Rec_Check_Flags.Payment_Method_Lookup_Code;

                End If;

        Else

              If V_Pay_Count = 0 Then

                V_Error_Flag  := 'Y';
                V_Hdr_Err_Msg := 'No Pay Site Defined For The Vendor';
                Fnd_File.Put_Line(Fnd_File.Output,
                                  Rpad(Nvl(Rec_Inv_Hdr.Vendor_Num, ' '), 20) ||
                                  Rpad(Rec_Inv_Hdr.Invoice_Num, 20) ||
                                  V_Hdr_Err_Msg);
                Exit;

              End If;

              V_Error_Flag  := 'Y';
              V_Hdr_Err_Msg := 'Supplier Sites Setup In Apps Not As Per Vendor Outbound Defination';
              Fnd_File.Put_Line(Fnd_File.Output,
                                Rpad(Nvl(Rec_Inv_Hdr.Vendor_Num, ' '), 20) ||
                                Rpad(Rec_Inv_Hdr.Invoice_Num, 20) ||
                                V_Hdr_Err_Msg);
              Exit;

            End If;            --                 If V_Pay_Count = 1 Then

          End If;                --                If Cur_Check_Flags%Notfound Then

        End Loop;

        End If;

    End Loop;


    01.11.2013    Bala.Sk    Not Required Due To Unique Vendor Solution

*/
END xxabrl_tss_ap_inv_interface; 
/


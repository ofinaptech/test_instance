CREATE OR REPLACE PROCEDURE APPS.XXABRL_VENDOR_MIG_INFO(err_buf out varchar2,retcode number) IS
    v_user_id number := FND_PROFILE.value('USER_ID');
    v_resp_id number := FND_PROFILE.value('RESP_ID');
    v_appl_id number := FND_PROFILE.value('RESP_APPL_ID');
    ln_vendor_site_interface_id number;
    CURSOR APS_C2 IS
      Select ROWID,
            a.OPERATING_UNIT,
VENDOR_SITE_CODE,
ADDRESS_LINE1,
ADDRESS_LINE2,
ADDRESS_LINE3,
CITY,
COUNTRY,
STATE,
ZIP,
AREA_CODE,
PHONE,
FAX,
FAX_AREA_CODE,
EMAIL_ADDRESS,
PURCHASING_SITE_FLAG,
PAY_SITE_FLAG,
A.ATTENTION_AR_FLAG,
A.SHIPPING_CONTROL,
A.INACTIVE_DATE,
A.TAX_REPORTING_SITE_FLAG,
A.INVOICE_AMOUNT_LIMIT,
A.PAY_GROUP_LOOKUP_CODE,
a.PAYMENT_METHOD_LOOKUP_CODE,
a.NAV_VENDOR_NUM,
a.VENDOR_BANK_NAME,
a.VENDOR_BRANCH_NAME,
a.VENDOR_ACCOUNT_NUMBER,
A.SHIP_TO_LOCATION_ID,
A.BILL_TO_LOCATION_ID,
A.TERM_ID,
A.VENDOR_ID,
A.VENDOR_SITE_ID,
a.ATTRIBUTE5,
a.ATTRIBUTE6,
a.ATTRIBUTE7,
a.ATTRIBUTE8,
a.ATTRIBUTE9,
a.ATTRIBUTE10,
a.ATTRIBUTE11,
a.ATTRIBUTE12,
a.ATTRIBUTE13,
a.ATTRIBUTE14,
a.ORG_ID
 From XXABRL_SUPPLIER_SITES_MIG_INT a
       WHERE NVL(PROCESS_FLAG, 'N') = 'V';
    /*CURSOR APS_C3 IS
      Select ROWID,
             OPERATING_UNIT,
             VENDOR_NAME,
             VENDOR_SITE_CODE,
             --TITLE,
             FIRST_NAME,
             --  MIDDLE_NAME,
             LAST_NAME,
             EMAIL_ADDRESS,
             PHONE_AREA_CODE,
             PHONE,
             FAX_AREA_CODE,
             FAX,
             vendor_id,
             vendor_site_id
        FROM XXABRL_SUPSITE_CONTMIGR_INT
       WHERE NVL(PROCESS_FLAG, 'N') = 'V';
    ln_vendor_interface_id      AP_SUPPLIERS_INT.vendor_interface_id%TYPE;
    ln_vendor_site_interface_id NUMBER;
    ln_vendor_site_cont_int_id  NUMBER;*/
  BEGIN
    FND_FILE.PUT_LINE(FND_FILE.LOG, 'Starting Inserting Vendors');
    FOR lr_Site_rec IN APS_C2 LOOP
        --
        -- We just need to create US Sites in US OU only else we need to create every other site
        -- in all the existing OU except US
        --
        ln_vendor_site_interface_id := NULL;
        BEGIN
          SELECT AP_SUPPLIER_SITES_INT_S.NEXTVAL
            INTO ln_vendor_site_interface_id
            FROM DUAL;
        EXCEPTION
          WHEN OTHERS THEN
            FND_FILE.PUT_LINE(FND_FILE.LOG,
                              'Unable to derive vendor_site_interface_id');
        END;
        FND_FILE.PUT_LINE(FND_FILE.LOG,
                          'Inserting Vendor Site' ||
                          lr_Site_rec.VENDOR_SITE_CODE);  
        begin
          INSERT INTO AP_SUPPLIER_SITES_INT
            (--VENDOR_INTERFACE_ID,
             VENDOR_SITE_INTERFACE_ID,
             OPERATING_UNIT_NAME,
             --,VENDOR_NAME            
             VENDOR_SITE_CODE,
             ADDRESS_LINE1,
             ADDRESS_LINE2,
             ADDRESS_LINE3,
             CITY,
             STATE,
             COUNTRY,
             ZIP,
             AREA_CODE,
             PHONE,
             FAX,
             FAX_AREA_CODE,
             EMAIL_ADDRESS,
             PURCHASING_SITE_FLAG,
             PAY_SITE_FLAG,
             -- RFQ_ONLY_SITE_FLAG,
             INACTIVE_DATE,
             INVOICE_AMOUNT_LIMIT,
              PAY_GROUP_LOOKUP_CODE,
             PAYMENT_METHOD_LOOKUP_CODE,
             ATTRIBUTE_CATEGORY,
             Attribute1,
             Attribute2,
             Attribute3,
             Attribute4,
             BILL_TO_LOCATION_ID,
             SHIP_TO_LOCATION_ID,
             TERMS_ID,
             vendor_id,
             Attribute15,
           CREATION_DATE,
             CREATED_BY,
             Attribute5,
             Attribute6,
             Attribute7,
             Attribute8,
             Attribute9,
             Attribute10,
             Attribute11,
             Attribute12,
             Attribute13,
             Attribute14,
             ORG_ID
             )
          VALUES
            (--ln_vendor_interface_id,
             ln_vendor_site_interface_id,
             lr_Site_rec.OPERATING_UNIT,
             --, lr_Site_rec.VENDOR_NAME
             SUBSTR(lr_Site_rec.VENDOR_SITE_CODE, 1, 15),
             lr_Site_rec.ADDRESS_LINE1,
             lr_Site_rec.ADDRESS_LINE2,
             lr_Site_rec.ADDRESS_LINE3,
             SUBSTR(lr_Site_rec.CITY, 1, 15),
            -- lr_Site_rec.COUNTY,
             lr_Site_rec.STATE,
             lr_Site_rec.COUNTRY,
             lr_Site_rec.ZIP,
             lr_Site_rec.AREA_CODE,
             lr_Site_rec.PHONE,
             --  to_char(decode(lr_Site_rec.PHONE, 0, null, lr_Site_rec.PHONE)),
             /*decode(*/
             lr_Site_rec.FAX /*, 0, null, lr_Site_rec.FAX)*/,
             /*decode(*/
             lr_Site_rec.FAX_AREA_CODE /*,
                                 0,
                                 null,
                                 lr_Site_rec.FAX_AREA_CODE)*/,
             lr_Site_rec.EMAIL_ADDRESS,
             upper(lr_Site_rec.PURCHASING_SITE_FLAG),
             upper(lr_Site_rec.PAY_SITE_FLAG),
             --upper(lr_Site_rec.RFQ_ONLY_SITE_FLAG),
             lr_Site_rec.INACTIVE_DATE,
             lr_Site_rec.INVOICE_AMOUNT_LIMIT,
             upper(lr_Site_rec.PAY_GROUP_LOOKUP_CODE),
             lr_Site_rec.PAYMENT_METHOD_LOOKUP_CODE,
             --lr_Site_rec.TERMS_N lr_Site_rec.TERM_ID,
             'Supplier Site Additional Info',
             lr_Site_rec.NAV_VENDOR_NUM,
             lr_Site_rec.Vendor_Bank_Name,
             lr_Site_rec.Vendor_Branch_Name,
             lr_Site_rec.Vendor_Account_Number,
             lr_Site_rec.BILL_TO_LOCATION_ID,
             lr_Site_rec.SHIP_TO_LOCATION_ID,
             lr_Site_rec.TERM_ID,
              lr_Site_rec.VENDOR_ID,
              lr_Site_rec.VENDOR_SITE_Id,
             SYSDATE,
             v_user_id,
             lr_Site_rec.Attribute5,
             lr_Site_rec.Attribute6,
             lr_Site_rec.Attribute7,
             lr_Site_rec.Attribute8,
             lr_Site_rec.Attribute9,
             lr_Site_rec.Attribute10,
             lr_Site_rec.Attribute11,
              lr_Site_rec.Attribute12,
               lr_Site_rec.Attribute13,
              lr_Site_rec.Attribute14,
              lr_Site_rec.org_id
             );
        exception
          when others then
            FND_FILE.PUT_LINE(FND_FILE.LOG,
                              'error while inserting site for vendor ' ||
                              ln_vendor_site_interface_id || sqlerrm);
        end;
        Update XXABRL_SUPPLIER_SITES_MIG_INT
           Set process_Flag = 'Y'
         Where Rowid = lr_Site_rec.ROWID;
        Commit;
      END LOOP;
      end XXABRL_VENDOR_MIG_INFO;
/


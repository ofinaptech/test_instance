CREATE OR REPLACE PACKAGE BODY APPS.xxabrl_asset_capital3_pkg
 /*
  ==============================================================================================
  ||   Filename   : xxabrl_asset_capital3_pkg.sql  
  ||   Description : ABRL Asset Capitalization and Transfer Program 
  ||
  ||   Version     Date            Author        Modification
  ||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~   ~~~~~~~~~~~~~~ 
  ||   1.0.1      07-dev-2009   Praveen Kumar    Depriciation assignment form has null ccid 
                                                 after uploading the csv for capitalization. 
  ||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~   ~~~~~~~~~~~~~~
  ==============================================================================================*/
IS
   PROCEDURE xxabrl_asset_cap3_load (
      errbuff      OUT      VARCHAR2,
      retcode      OUT      NUMBER,
      p_prg_flag   IN       VARCHAR2
   )
   IS
   BEGIN
      IF p_prg_flag = 'Y'
      THEN
         BEGIN
            xxabrl_asset_cap3_val_proc;
         END;
      END IF;

      IF p_prg_flag = 'N'
      THEN
         BEGIN
            xxabrl_asset_cap3_val_proc;
            xxabrl_asset_cap3_ins_proc;
         END;
      END IF;
   END xxabrl_asset_cap3_load;

-- Procedure For Validation Asset data
   PROCEDURE xxabrl_asset_cap3_val_proc
   IS
-- sequence " asset_sr_no_seq" is created for storing the serial number for asset because if asset
--record not inserted in base table and no error occure then we have to re select that record.

      /*CURSOR CUR_VALIDATE_ASSET_VALUES IS

  SELECT ROWID
        ,SR_NO
    ,description
        ,MAJOR
        ,MINOR1
        ,MINOR2
        ,MANUFACTURER_NAME
        ,DATE_PLACED_IN_SERVICE
        ,FIXED_ASSETS_COST
        ,FIXED_ASSETS_UNITS
        ,PAY_CCID_CO
        ,PAY_CCID_DEPT
        ,PAY_CCID_STATE_SBU
        ,PAY_CCID_LOCATION
        ,PAY_CCID_MERCHANDIZE
        ,PAY_CCID_ACCOUNT
        ,PAY_CCID_INTERCOMPANY
        ,PAY_CCID_FUTURE
        ,LOCATION_CODES
        ,REVIEWER_COMMENTS
        ,INVOICE_NUMBER
        ,PO_VENDOR_NUMBER
        ,PO_NUMBER
        ,YEAR_data
        ,PROJECT_NUMBER
        ,ERROR_FLAG
        ,error_message
    ,QUEUE_NAME
    ,POSTING_STATUS
  FROM xxabrl_fa_cap3_data_load_stg
  WHERE ERROR_FLAG IN ('E','N');*/
      CURSOR cur_validate
      IS
         SELECT a.ROWID, a.sr_no, a.description, a.major, a.minor1, a.minor2,
                a.manufacturer_name, a.date_placed_in_service,
                a.fixed_assets_cost, a.fixed_assets_units, a.pay_ccid_co,
                a.pay_ccid_dept, a.pay_ccid_state_sbu, a.pay_ccid_location,
                a.pay_ccid_merchandize, a.pay_ccid_account,
                a.pay_ccid_intercompany, a.pay_ccid_future, a.location_codes,
                a.reviewer_comments, a.invoice_number, a.po_vendor_number,
                a.po_number, a.year_data, a.project_number, a.error_flag,
                b.book_type_code, a.error_message, a.queue_name,
                a.posting_status
           FROM xxabrl_fa_cap3_data_load_stg a, fa_mass_additions b
          WHERE a.sr_no = b.mass_addition_id AND error_flag IN ('E', 'N');

      -- Variable Declaration
      v_err_flag                 VARCHAR2 (1);
      v_err_msg                  VARCHAR2 (5000);
      v_category_id              NUMBER;
      v_location_id              NUMBER;
      v_vendor_id                NUMBER;
      v_asset_key_ccid           NUMBER;
      v_pay_ccid                 NUMBER;
      v_exp_ccid                 NUMBER;
      v_book_type_code           VARCHAR2 (50);
      v_fixed_assets_units_flg   CHAR (1);
      v_all_count                NUMBER                    := 0;
      v_error_count              NUMBER                    := 0;
      v_insert_count             NUMBER                    := 0;
      ln_exp_ccid                NUMBER;
      ln_loc_ccid                NUMBER;
      v_segments                 fnd_flex_ext.segmentarray;
      v_segments1                fnd_flex_ext.segmentarray;
      l_struct_num               NUMBER;
      retrun_value               BOOLEAN;
      v_deprn_expense_acct       VARCHAR2 (25);
   BEGIN
      fnd_file.put_line
                    (fnd_file.LOG,
                     'for loop starts for validating FIXED_ASSETS_UNITS_FLG '
                    );                                                --suresh
       /*FOR J IN CUR_VALIDATE_ASSET_VALUES

      LOOP
      fnd_file.PUT_LINE(fnd_file.LOG,'In side for loop  for validating FIXED_ASSETS_UNITS_FLG '); --suresh

        BEGIN
        SELECT FIXED_ASSETS_UNITS
         INTO v_FIXED_ASSETS_UNITS_FLG
         FROM FA_MASS_ADDITIONS FM
        WHERE  FM.MASS_ADDITION_ID = J.SR_NO
          AND   FM.FIXED_ASSETS_UNITS <> J.FIXED_ASSETS_UNITS;
        --  and fm.rowid=j.rowid;  --suresh
       EXCEPTION
        WHEN OTHERS THEN
        v_FIXED_ASSETS_UNITS_FLG := 'N';
        END;
        END LOOP;*/  --suresh

      -- IF  v_FIXED_ASSETS_UNITS_FLG <> 'N'  THEN    --suresh
      fnd_file.put_line (fnd_file.LOG, 'FIXED_ASSETS_UNITS_FLG <> ''N''');
                                                                      --suresh

      BEGIN
         -- DBMS_OUTPUT.PUT_LINE('start of prog');
         fnd_file.put_line (fnd_file.LOG, 'Validation program starts here');
                                                                     --suresh

         FOR i IN cur_validate
         LOOP
            ---
            fnd_file.put_line (fnd_file.LOG, 'start for SR_NO : ' || i.sr_no);

            -- Validation For catagory Values
            BEGIN
                                
               SELECT category_id
                 INTO v_category_id
                 FROM fa_categories_b
                WHERE segment1 || '.' || segment2 || '.' || segment3 =
                         i.major || '.' || i.minor1 || '.'
                         || NVL (i.minor2, '');
                                          --suresh
            EXCEPTION
               WHEN OTHERS
               THEN
                  v_category_id := NULL;
                  v_err_flag := 'E';
                  v_err_msg :=
                     'No Category Found for Desire combinition of catagories';
                  fnd_file.put_line
                     (fnd_file.LOG,
                         'No Category Found for Desire combinition of catagories : for sl no '
                      || i.sr_no
                      || v_category_id
                     );
            END;                                                      --suresh

            ----Depriciation Account Update
            BEGIN
               IF v_category_id IS NOT NULL
               THEN
                  SELECT a.deprn_expense_acct
                    INTO v_deprn_expense_acct
                    FROM fa_category_books a
                   WHERE a.category_id = v_category_id
                     AND book_type_code = i.book_type_code;
               END IF;
                             --suresh
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  v_deprn_expense_acct := NULL;
                  v_err_flag := 'E';
                  v_err_msg :=
                     'Depriciation Expence Account Not Found for This Category';
                  fnd_file.put_line (fnd_file.LOG,
                                        'validation for category_id valid'
                                     || v_category_id
                                    );
               WHEN OTHERS
               THEN
                  v_deprn_expense_acct := NULL;
                  v_err_flag := 'E';
                  v_err_msg :=
                     'Depriciation Expence Account Not Found for This Category';
                  fnd_file.put_line
                     (fnd_file.LOG,
                         'Depriciation Expence Account Not Found for This Category : for sl no '
                      || i.sr_no
                      || v_category_id
                     );
            END;

            -- Validation For the Location
            BEGIN
               SELECT location_id
                 INTO v_location_id
                 FROM fa_locations
                WHERE UPPER (segment1) = UPPER (i.location_codes);
                                               
            EXCEPTION
               WHEN OTHERS
               THEN
                  v_err_flag := 'E';
                  v_err_msg :=
                        ' - Location is not available in application or Location is wrong''for sl no '
                     || i.sr_no;
                  fnd_file.put_line
                     (fnd_file.LOG,
                         ' Location is not available in application or Location is wrong: '
                      || i.location_codes
                      || ' for sl no '
                      || i.sr_no
                     );
            END;

            -- Validation For PO Vendor Number
            BEGIN
               IF i.po_vendor_number IS NOT NULL
               THEN
                  SELECT vendor_id
                    INTO v_vendor_id
                    FROM po_vendors
                   WHERE UPPER (segment1) = UPPER (i.po_vendor_number);
               END IF;

                                               --suresh
            EXCEPTION
               WHEN OTHERS
               THEN
                  v_err_flag := 'E';
                  v_err_msg := ' -  Vendor Name not found in application';
                  fnd_file.put_line (fnd_file.LOG,
                                        'Error on vendor name derivation : '
                                     || i.po_vendor_number
                                     || ' for sl no '
                                     || i.sr_no
                                    );
            END;

            -- Validation For Asset KeyFlex Field
            BEGIN
               IF i.project_number IS NOT NULL
               THEN
                  SELECT code_combination_id
                    INTO v_asset_key_ccid
                    FROM fa_asset_keywords
                   WHERE segment1 = i.year_data
                         AND segment2 = i.project_number;
               END IF;
                                                                      --suresh
            EXCEPTION
               WHEN OTHERS
               THEN
                  v_err_flag := 'E';
                  v_err_msg :=
                     ' -  Combinition of Project_Number and Year is not Found ';
                  fnd_file.put_line
                             (fnd_file.LOG,
                                 'Error on Project No and Year derivation : '
                              || ' '
                              || 'Project NO:'
                              || i.project_number
                              || ' '
                              || 'Year:'
                              || i.year_data
                              || 'for sl no '
                              || i.sr_no
                             );
            END;

            -- Validation for Payable Accounting code ( Payables CCID )
            BEGIN
               fnd_file.put_line (fnd_file.LOG, 'validation for ccid starts');
                                                                     --suresh

               SELECT code_combination_id
                 INTO v_pay_ccid
                 FROM gl_code_combinations
                WHERE segment1 = i.pay_ccid_co
                  AND segment2 = i.pay_ccid_dept
                  AND segment3 = i.pay_ccid_state_sbu
                  AND segment4 = i.pay_ccid_location
                  AND segment5 = i.pay_ccid_merchandize
                  AND segment6 = v_deprn_expense_acct    ---i.PAY_CCID_ACCOUNT
                  AND segment7 = i.pay_ccid_intercompany
                  AND segment8 = i.pay_ccid_future;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  /*         v_err_flag := 'E';
                           v_err_msg :=  ' - Payable Accounting code Not Found';

                     Fnd_File.put_line (Fnd_File.LOG,'Exception on Payable Accounting code derivation :  for sl no '||I.SR_NO);
                     Fnd_File.put_line (Fnd_File.LOG,'Exception on Payable Accounting code ccid_co : ' ||i.PAY_CCID_CO);
                     Fnd_File.put_line (Fnd_File.LOG,'Exception on Payable Accounting code PAY_CCID_DEPT : ' ||i.PAY_CCID_DEPT);
                     Fnd_File.put_line (Fnd_File.LOG,'Exception on Payable Accounting code PAY_CCID_STATE_SBU : '|| i.PAY_CCID_STATE_SBU);
                     Fnd_File.put_line (Fnd_File.LOG,'Exception on Payable Accounting code PAY_CCID_LOCATION : '|| i.PAY_CCID_LOCATION);
                     Fnd_File.put_line (Fnd_File.LOG,'Exception on Payable Accounting code PAY_CCID_MERCHANDIZE : ' ||i.PAY_CCID_MERCHANDIZE);
                        Fnd_File.put_line (Fnd_File.LOG,'Exception on Payable Accounting code PAY_CCID_ACCOUNT : '|| i.PAY_CCID_ACCOUNT);
                        Fnd_File.put_line (Fnd_File.LOG,'Exception on Payable Accounting code PAY_CCID_INTERCOMPANY : '|| i.PAY_CCID_INTERCOMPANY);
                        Fnd_File.put_line (Fnd_File.LOG,'Exception on Payable Accounting code PAY_CCID_FUTURE : '|| i.PAY_CCID_FUTURE); */

                  -------------------------------------------------------------------------------------------------------
                  v_segments (1) := i.pay_ccid_co;
                  v_segments (2) := i.pay_ccid_dept;
                  v_segments (3) := i.pay_ccid_state_sbu;
                  v_segments (4) := i.pay_ccid_location;
                  v_segments (5) := i.pay_ccid_merchandize;
                  v_segments (6) := v_deprn_expense_acct;
                                                       ---i.pay_ccid_account;
                  v_segments (7) := i.pay_ccid_intercompany;
                  v_segments (8) := i.pay_ccid_future;

                  SELECT chart_of_accounts_id
                    INTO l_struct_num
                    FROM gl_sets_of_books
                   WHERE NAME = 'ABRL Ledger';

                  retrun_value :=
                     fnd_flex_ext.get_combination_id
                                           (application_short_name      => 'SQLGL',
                                            key_flex_code               => 'GL#',
                                            structure_number            => l_struct_num,
                                            validation_date             => SYSDATE,
                                            n_segments                  => 8,
                                            segments                    => v_segments,
                                            combination_id              => v_pay_ccid,
                                            data_set                    => -1
                                           );
-------------------------------------------------------------------------------------------------------
            END;
              
            IF v_err_flag = 'E'
            THEN
               UPDATE xxabrl_fa_cap3_data_load_stg
                  SET error_flag = 'E',
                      error_message = v_err_msg
                WHERE ROWID = i.ROWID      
                      AND sr_no = i.sr_no;
               
               v_error_count := v_error_count + 1;

            ELSE
               UPDATE xxabrl_fa_cap3_data_load_stg
                  SET error_flag = 'V',
                      error_message = '',
                      asset_category_id = v_category_id
             --,expense_code_combination_id  = v_exp_ccid
               ,      payables_code_combination_id = v_pay_ccid,
                      location_id = v_location_id,
                      asset_key_ccid = v_asset_key_ccid,
                      po_vendor_id = v_vendor_id
            --       ,book_type_name               = v_BOOK_TYPE_CODE
                     --  WHERE ROWID=I.ROWID;
               WHERE  sr_no = i.sr_no AND ROWID = i.ROWID;            --suresh

               v_insert_count := v_insert_count + 1;
            END IF;

            v_all_count := v_all_count + 1;
         END LOOP;
      END;

      COMMIT;
      fnd_file.put_line
                 (fnd_file.output,
                  '----------------------------------------------------------'
                 );
      fnd_file.put_line
                 (fnd_file.output,
                  '                      Validation                          '
                 );
      fnd_file.put_line (fnd_file.output, 'Total Lines : ' || v_all_count);
      fnd_file.put_line (fnd_file.output,
                         'Total Lines With Error : ' || v_error_count
                        );
      fnd_file.put_line (fnd_file.output,
                            'Total Lines Successfully Validated : '
                         || v_insert_count
                        );
      fnd_file.put_line
                 (fnd_file.output,
                  '----------------------------------------------------------'
                 );
    -- ELSIF v_FIXED_ASSETS_UNITS_FLG = 'N' THEN
     --    UPDATE xxabrl_fa_cap3_data_load_stg SET ERROR_FLAG = 'E';
      -- END IF;---suresh
   END xxabrl_asset_cap3_val_proc;

--- Procedure For The inserting the staging table data in interface table

   ------------------------------------------------------------------------------------------------------------------------------------------------------------------

   -- Insert procedure
------------------------------------------------------------------------------------------------------------------------------------------------------------------
   PROCEDURE xxabrl_asset_cap3_ins_proc
   IS
      ll_description              VARCHAR2 (100);
      ll_asset_category_id        NUMBER;
      ll_manufacturer_name        VARCHAR2 (100);
      ll_date_placed_in_service   DATE;
      ll_fixed_assets_cost        NUMBER;
      ll_fixed_assets_units       NUMBER;
      ll_payables_ccid            NUMBER;
      ll_location_id              NUMBER;
      ll_reviewer_comments        VARCHAR2 (60);
      ll_invoice_number           VARCHAR2 (50);
      ll_po_vendor_id             NUMBER;
      ll_po_number                VARCHAR2 (50);
      ll_asset_key_ccid           NUMBER;

      CURSOR cur_update
      IS
         SELECT ROWID, sr_no, description, major, minor1, minor2,
                manufacturer_name, date_placed_in_service, fixed_assets_cost,
                fixed_assets_units, pay_ccid_co, pay_ccid_dept,
                pay_ccid_state_sbu, pay_ccid_location, pay_ccid_merchandize,
                pay_ccid_account, pay_ccid_intercompany, pay_ccid_future,
                location_codes, reviewer_comments, invoice_number,
                po_vendor_number, po_number, year_data, project_number,
                posting_status, queue_name, asset_category_id
--    ,expense_code_combination_id
                ,
                payables_code_combination_id, location_id, asset_key_ccid,
                po_vendor_id
           FROM xxabrl_fa_cap3_data_load_stg
          WHERE error_flag IN ('V');

      v_err_flag                  VARCHAR2 (1);
      v_err_message               VARCHAR2 (5000);
      v_all_count                 NUMBER          := 0;
      v_error_count               NUMBER          := 0;
      v_insert_count              NUMBER          := 0;
   BEGIN
      
      FOR i IN cur_update
      LOOP      

         BEGIN
            SELECT description, asset_category_id,
                   manufacturer_name, date_placed_in_service,
                   fixed_assets_cost, fixed_assets_units,
                   payables_code_combination_id, location_id,
                   reviewer_comments, invoice_number, po_vendor_id,
                   po_number, asset_key_ccid
              INTO ll_description, ll_asset_category_id,
                   ll_manufacturer_name, ll_date_placed_in_service,
                   ll_fixed_assets_cost, ll_fixed_assets_units,
                   ll_payables_ccid, ll_location_id,
                   ll_reviewer_comments, ll_invoice_number, ll_po_vendor_id,
                   ll_po_number, ll_asset_key_ccid
              FROM fa_mass_additions
             WHERE mass_addition_id = i.sr_no;

               IF  UPPER(NVL(ll_description,'XX')) <> UPPER(NVL(i.description,'XX'))
            OR  NVL(ll_ASSET_CATEGORY_ID,0) <> NVL(i.asset_category_id,0)
            OR  UPPER(NVL(ll_MANUFACTURER_NAME,'XX')) <> UPPER(NVL(i.manufacturer_name,'XX'))
            OR  NVL(ll_DATE_PLACED_IN_SERVICE,SYSDATE) <> NVL(i.date_placed_in_service,SYSDATE)
            OR  NVL(ll_FIXED_ASSETS_COST,0) <> NVL(i.fixed_assets_cost,0)
            --OR  NVL(ll_FIXED_ASSETS_UNITS,0) <> NVL(i.fixed_assets_units,0)   --suresh
            OR  NVL(ll_PAYABLES_CCID,0) <> NVL(i.payables_code_combination_id,0)
            OR  NVL(ll_LOCATION_ID,0) <> NVL(i.location_id,0)
            OR  UPPER(NVL(ll_REVIEWER_COMMENTS,'XX')) <> UPPER(NVL(i.reviewer_comments,'XX'))
            OR  UPPER(NVL(ll_INVOICE_NUMBER,'XX')) <> UPPER(NVL(i.invoice_number,'XX'))
            OR  NVL(ll_PO_VENDOR_ID,0) <> NVL(i.po_vendor_id,0)
            OR  UPPER(NVL(ll_PO_NUMBER,'XX')) <> UPPER(NVL(i.po_number,'XX'))
            OR  NVL(ll_asset_key_ccid,0) <> NVL(i.asset_key_ccid,0)
            THEN
            UPDATE fa_mass_additions
               SET description = i.description,
                   asset_category_id = i.asset_category_id            --suresh
                                                          ,
                   manufacturer_name = i.manufacturer_name,
                   date_placed_in_service = TO_DATE (i.date_placed_in_service)
                  --,fixed_assets_cost = i.fixed_assets_cost
                  --,fixed_assets_units = i.fixed_assets_units --suresh
                  --  ,payables_code_combination_id = i.payables_code_combination_id   --suresh
                  ,expense_code_combination_id =
                      i.payables_code_combination_id
                                     --i.expense_code_combination_id  --suresh                                                    ,
                   ,location_id = i.location_id,
                   reviewer_comments = i.reviewer_comments,
                   invoice_number = i.invoice_number,
                   po_vendor_id = i.po_vendor_id,
                   po_number = i.po_number,
                   asset_key_ccid = i.asset_key_ccid,
                   posting_status = i.posting_status,
                   queue_name = i.posting_status,
                   last_update_date = SYSDATE,
                   last_updated_by = fnd_global.user_id,
                   created_by = fnd_global.user_id,
                   creation_date = SYSDATE
             WHERE mass_addition_id = i.sr_no;
           END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               v_err_flag := 'E';
               v_err_message :=
                     'Error while updating the record into interface table - '
                  || SQLCODE
                  || '  '
                  || SQLERRM;
         END;

         IF v_err_flag = 'E'
         THEN
            UPDATE xxabrl_fa_cap3_data_load_stg
               SET error_flag = 'E',
         --- we put v here so that it will take this rejected recors next time
                   error_message = v_err_message
             WHERE sr_no = i.sr_no;

            COMMIT;
            v_error_count := v_error_count + 1;
         ELSE
            UPDATE xxabrl_fa_cap3_data_load_stg
               SET error_flag = 'V',
         --- we put v here so that it will take this rejected recors next time
                   error_message = ''
             WHERE sr_no = i.sr_no;

            v_insert_count := v_insert_count + 1;
            COMMIT;
         END IF;

         v_all_count := v_all_count + 1;
      END LOOP;

      fnd_file.put_line
                 (fnd_file.output,
                  '----------------------------------------------------------'
                 );
      fnd_file.put_line
                 (fnd_file.output,
                  '             Updated into Interface Table                    '
                 );
      fnd_file.put_line (fnd_file.output, 'Total Lines : ' || v_all_count);
      fnd_file.put_line (fnd_file.output,
                         'Total Lines With Error : ' || v_error_count
                        );
      fnd_file.put_line (fnd_file.output,
                            'Total Lines Successfully Updated : '
                         || v_insert_count
                        );
      fnd_file.put_line
                 (fnd_file.output,
                  '----------------------------------------------------------'
                 );
   END xxabrl_asset_cap3_ins_proc;
END xxabrl_asset_capital3_pkg; 
/


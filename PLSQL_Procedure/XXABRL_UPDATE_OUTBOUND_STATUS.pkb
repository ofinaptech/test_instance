CREATE OR REPLACE PACKAGE BODY APPS.XXABRL_Update_Outbound_Status
IS
    PROCEDURE Update_Status(err_buf OUT VARCHAR2, ret_code OUT NUMBER,P_PROG_NAME IN VARCHAR2)
    IS
    V_LAST_UPDATED_BY NUMBER;
    l_err_buf        varchar2(2000);
    l_ret_code       number:= 0;
     BEGIN
      
         --XXABRL_Update_Outbound_Status.ERR_FILE_PROG(err_buf => l_err_buf,ret_code => l_ret_code);
         
        BEGIN
        V_LAST_UPDATED_BY := Fnd_profile.VALUE('USER_ID');
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
             V_LAST_UPDATED_BY :='UN-KNOWN';
             fnd_file.put_line(fnd_file.output,'Status Update: User name not found');
        WHEN OTHERS THEN
             fnd_file.put_line(fnd_file.output,'Status Update: Error while finding user name '||SQLERRM);

        END;

        BEGIN
            INSERT INTO
            XXABRL_Outbound_Status (PROG_NAME,LAST_UPDATED_BY)
            VALUES(P_PROG_NAME,V_LAST_UPDATED_BY);

            COMMIT;
            EXCEPTION
            WHEN OTHERS THEN
                 fnd_file.put_line(fnd_file.output,'Status Update: Error while inseting '||SQLERRM);
                 ret_code := 1;
        END;
        
     END Update_Status;


   /********************* Procedure for giving the error log for Supplier Outbound Interface******************/
   
   PROCEDURE ERR_FILE_PROG (err_buf OUT VARCHAR2, 
                            ret_code OUT NUMBER)
                            
   IS
   v_count NUMBER;
   v_vendor_pur_site VARCHAR2(100); 
   v_purchasing_site_flag varchar2(1);   
   v_vendor_ret_site VARCHAR2(100);
   v_rfq_only_site_flag varchar2(1);   
   v_vendor_pay_site VARCHAR2(100);
   v_pay_site_flag varchar2(1);   
   v_vendor_ho_site VARCHAR2(100);
   v_tax_region VARCHAR2(10);
   v_tax_payer VARCHAR2(10);
   v_vendor_type VARCHAR2(50); 
   v_state_name  VARCHAR2(150);
   v_pay_method_code VARCHAR2(50);
   v_pay_method_code1 VARCHAR2(50);
   v_err_msg VARCHAR2(15000);
   v_err_flg VARCHAR2(1);
   p_run_date  DATE; 
     
     
      
   CURSOR CUR_ERR(P_RUN_DATE DATE) IS 
   
   SELECT DISTINCT 
          pv.vendor_id,
          pv.segment1,
          org_id          
   FROM
         apps.po_vendors pv
        ,apps.po_vendor_sites_all pvsa
        ,apps.po_vendor_contacts pvc
        ,apps.jai_ap_tds_vendor_hdrs jvh
        ,apps.jai_cmn_vendor_sites jcv
  WHERE pv.vendor_id = pvsa.vendor_id AND
        pvsa.vendor_site_id = pvc.vendor_site_id(+) AND
        jcv.vendor_id(+) = pvsa.vendor_id AND 
        jcv.vendor_site_id(+) = pvsa.vendor_site_id AND 
        jvh.vendor_id(+) = pvsa.vendor_id AND 
        jvh.vendor_site_id(+) = pvsa.vendor_site_id AND
        ((pv.segment1 between  2000000  and 2999999 ) or (pv.segment1 between  1000 and 3999)) and 
        substr(pv.segment1,1,2) <> '97' 
        /*and ( (pvsa.PURCHASING_SITE_FLAG = 'Y' and  SUBSTR(pvsa.VENDOR_SITE_CODE,1,3) = 'PUR')
             OR (pvsa.RFQ_ONLY_SITE_FLAG = 'Y' and  SUBSTR(pvsa.VENDOR_SITE_CODE,1,3) = 'RET' )
             OR (pvsa.PAY_SITE_FLAG = 'Y' and  SUBSTR(pvsa.VENDOR_SITE_CODE,1,3) = 'PAY')
             OR (NVL(pvsa.PAY_SITE_FLAG,'N')='N' and NVL(pvsa.RFQ_ONLY_SITE_FLAG,'N')='N' AND NVL(pvsa.PURCHASING_SITE_FLAG,'N')='N' AND SUBSTR(pvsa.VENDOR_SITE_CODE,1,2) = 'HO')
             )
        and pv.VENDOR_TYPE_LOOKUP_CODE IN ('MERCHANDISE','CONSUMABLES','INTERNAL VENDORS') */AND   
        TRUNC(SYSDATE) BETWEEN TRUNC(PV.START_DATE_ACTIVE) AND NVL(TRUNC(PV.END_DATE_ACTIVE),SYSDATE) AND
        TRUNC(NVL(pvsa.inactive_date,SYSDATE)) <=TRUNC(SYSDATE) AND
        (TRUNC(pv.LAST_UPDATE_DATE) >= trunc(p_run_date)
        OR 
        TRUNC(pvsa.LAST_UPDATE_DATE) >= trunc(p_run_date)
        OR 
        TRUNC(pvc.LAST_UPDATE_DATE) >= trunc(p_run_date)
        OR 
        TRUNC(jvh.LAST_UPDATE_DATE) >= trunc(p_run_date)
        OR 
        TRUNC(jcv.LAST_UPDATE_DATE) >= trunc(p_run_date)
        )order by segment1 asc;
 
   BEGIN
   
   SELECT to_char(max(trunc(last_run_date)),'DD-MON-YYYY') P_RUN_DATE 
     INTO p_run_date
     FROM XXABRL_Outbound_Status 
    WHERE prog_name = 'SUPPLIER' 
    GROUP BY prog_name;
   
    FOR cur_err_r1 IN cur_err(P_RUN_DATE) LOOP
        v_err_msg:= null;
        v_err_flg:= null;
    
      BEGIN 
       SELECT count(1)
       into v_count  
              from apps.po_vendors pv,
                   apps.po_vendor_sites_all pvsa
             where pv.vendor_id = pvsa.vendor_id 
               and pv.vendor_id = cur_err_r1.vendor_id
               and pvsa.org_id = cur_err_r1.org_id
               and pv.global_attribute10 is not null
               and pv.global_attribute11 is not null
                AND pv.VENDOR_TYPE_LOOKUP_CODE IN
                         ('MERCHANDISE', 'CONSUMABLES', 'INTERNAL VENDORS')
                and ((purchasing_site_flag = 'Y' and
                     rownum = 1 and
                     pvsa.creation_date = (select max(pvsa.creation_date) 
                                        from po_vendor_sites_all pvsa where vendor_id = cur_err_r1.vendor_id
                                         and pvsa.org_id = cur_err_r1.org_id
                                         AND TRUNC(NVL(pvsa.inactive_date,SYSDATE)) <=TRUNC(SYSDATE) 
                                         and pvsa.purchasing_site_flag = 'Y')  
                                         and length(pvsa.ADDRESS_LINE1||pvsa.ADDRESS_LINE2)>=4  
                                       and substr(vendor_site_code, 1, 3) = 'PUR') or ----1
                   (rfq_only_site_flag = 'Y' and
                    rownum = 1 and
                    pvsa.creation_date = (select max(pvsa.creation_date) from po_vendor_sites_all pvsa 
                                            WHERE vendor_id = cur_err_r1.vendor_id
                                              AND TRUNC(NVL(pvsa.inactive_date,SYSDATE)) <=TRUNC(SYSDATE) 
                                              AND pvsa.org_id = cur_err_r1.org_id
                                              and pvsa.rfq_only_site_flag = 'Y')
                                              and length(pvsa.ADDRESS_LINE1||pvsa.ADDRESS_LINE2)>=4
                                               and substr(vendor_site_code, 1, 3) = 'RET') or    -----2
                   (pay_site_flag = 'Y' and
                   rownum = 1 and
                   pvsa.creation_date = (select max(pvsa.creation_date) from po_vendor_sites_all pvsa where vendor_id = cur_err_r1.vendor_id
                                              AND TRUNC(NVL(pvsa.inactive_date,SYSDATE)) <=TRUNC(SYSDATE) 
                                              AND pvsa.org_id = cur_err_r1.org_id
                                              and pvsa.pay_site_flag = 'Y') 
                                              and length(pvsa.ADDRESS_LINE1||pvsa.ADDRESS_LINE2)>=4
                 and  substr(vendor_site_code, 1, 3) = 'PAY')Or ----3
                    (nvl(pay_site_flag, 'N') = 'N')
                   and nvl(rfq_only_site_flag, 'N') = 'N' 
                   and nvl(purchasing_site_flag, 'N') = 'N'
                   and substr(vendor_site_code, 1, 2) = 'HO'
                   and pvsa.creation_date = (select max(creation_date) 
                                         from po_vendor_sites_all pvsa                                         
                                        where pvsa.vendor_id = cur_err_r1.vendor_id
                                          and pvsa.org_id = cur_err_r1.org_id
                                          AND TRUNC(NVL(pvsa.inactive_date,SYSDATE)) <=TRUNC(SYSDATE)  
                                          and nvl(pay_site_flag,'N') = 'N'
                                          and nvl(rfq_only_site_flag, 'N') = 'N' 
                                          and nvl(purchasing_site_flag, 'N') = 'N'
                                          and length(pvsa.ADDRESS_LINE1||pvsa.ADDRESS_LINE2)>=4
                                          and substr(vendor_site_code, 1, 2) = 'HO')) ;
                                          
                     IF v_count <= 4 THEN
             
                     FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'------------------------------------------------------------------');
              
              /********************* Validation for testing PUR Site and Flag******************/
                            
                BEGIN
                   SELECT vendor_site_code
                     INTO v_vendor_pur_site 
                     FROM po_vendor_sites_all pvsa
                    WHERE vendor_id = cur_err_r1.vendor_id
                      AND pvsa.org_id = cur_err_r1.org_id                      
                      AND creation_date = (select max(pvsa.creation_date) 
                                            from po_vendor_sites_all pvsa 
                                           where vendor_id = cur_err_r1.vendor_id
                                             AND TRUNC(NVL(pvsa.inactive_date,SYSDATE)) >= TRUNC(SYSDATE) 
                                             AND pvsa.org_id = cur_err_r1.org_id                                             
                                             and substr(vendor_site_code, 1, 3) = 'PUR')
                      AND SUBSTR(vendor_site_code, 1, 3) = 'PUR'
                       AND TRUNC(NVL(pvsa.inactive_date,SYSDATE)) >= TRUNC(SYSDATE);
                 
                  IF v_vendor_pur_site IS NOT NULL THEN
                    BEGIN                  
                       SELECT purchasing_site_flag
                         INTO v_purchasing_site_flag
                         FROM po_vendor_sites_all pvsa
                        WHERE vendor_id = cur_err_r1.vendor_id
                          AND pvsa.org_id = cur_err_r1.org_id                          
                          AND creation_date = (select max(pvsa.creation_date) 
                                                from po_vendor_sites_all pvsa 
                                               where vendor_id = cur_err_r1.vendor_id
                                                 AND pvsa.org_id = cur_err_r1.org_id
                                                 AND TRUNC(NVL(pvsa.inactive_date,SYSDATE)) >= TRUNC(SYSDATE)                                                  
                                                 and substr(vendor_site_code, 1, 3) = 'PUR')
                          AND SUBSTR(vendor_site_code, 1, 3) = 'PUR'
                           AND TRUNC(NVL(pvsa.inactive_date,SYSDATE)) >= TRUNC(SYSDATE)
                          AND purchasing_site_flag = 'Y' 
                          and length(pvsa.ADDRESS_LINE1||pvsa.ADDRESS_LINE2)>=4;
                    EXCEPTION
                       WHEN NO_DATA_FOUND THEN 
                         v_err_flg:='E'; 
                         v_err_msg:= v_err_msg||','||'Please check the Purchasing Site Flag';                            
                         FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'Please check the Purchasing Site Flag/Address should be greater than 4 Character for the Supplier No-'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);
                         WHEN OTHERS THEN 
                         v_err_flg:='E'; 
                         v_err_msg:= v_err_msg||','||'Please check the Purchasing Site Flag';                            
                         FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'Please check the Purchasing Site Flag/Address should be greater than 4 Character for the Supplier No-'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);
                      END;
                  END IF;
                EXCEPTION
                   WHEN NO_DATA_FOUND THEN
                      v_err_flg:='E';
                      v_err_msg:= v_err_msg||','||'PUR Site is not created for the Supplier Number';
                      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'PUR Site is not created for the Supplier No-'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);
                   WHEN OTHERS THEN
                      v_err_flg:='E';
                      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'Invalid PUR Site for the Supplier No-'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);
                END;
                  
               /********************* Validation for testing RFQ Site and Flag******************/  
                 
                BEGIN
                   SELECT vendor_site_code
                     INTO v_vendor_ret_site
                     FROM po_vendor_sites_all pvsa
                    WHERE vendor_id = cur_err_r1.vendor_id
                      AND pvsa.org_id = cur_err_r1.org_id                      
                      AND creation_date = (select max(pvsa.creation_date) 
                                             from po_vendor_sites_all pvsa 
                                            where vendor_id = cur_err_r1.vendor_id
                                              AND pvsa.org_id = cur_err_r1.org_id
                                              AND TRUNC(NVL(pvsa.inactive_date,SYSDATE)) >= TRUNC(SYSDATE)                                               
                                              and substr(vendor_site_code, 1, 3) = 'RET')                                              
                      AND SUBSTR(vendor_site_code, 1, 3) = 'RET'
                       AND TRUNC(NVL(pvsa.inactive_date,SYSDATE)) >= TRUNC(SYSDATE);
                      
                  IF v_vendor_ret_site IS NOT NULL THEN
                    BEGIN
                     SELECT rfq_only_site_flag 
                     INTO v_rfq_only_site_flag 
                     FROM po_vendor_sites_all pvsa
                    WHERE vendor_id = cur_err_r1.vendor_id
                      AND pvsa.org_id = cur_err_r1.org_id                      
                      AND creation_date = (select max(pvsa.creation_date) 
                                             from po_vendor_sites_all pvsa 
                                            where vendor_id = cur_err_r1.vendor_id
                                              AND pvsa.org_id = cur_err_r1.org_id
                                              AND TRUNC(NVL(pvsa.inactive_date,SYSDATE)) >= TRUNC(SYSDATE)                                               
                                              and substr(vendor_site_code, 1, 3) = 'RET')                                              
                      AND SUBSTR(vendor_site_code, 1, 3) = 'RET'
                       AND TRUNC(NVL(pvsa.inactive_date,SYSDATE)) >= TRUNC(SYSDATE)
                      AND rfq_only_site_flag = 'Y'
                      and length(pvsa.ADDRESS_LINE1||pvsa.ADDRESS_LINE2)>=4;            
                    EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                     v_err_flg:='E';
                     v_err_msg:= v_err_msg||','||'Please check the RET Site Flag';
                     FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'Please check the RET Site Flag/Address should be greater than 4 Character for the Supplier No-'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);
                     WHEN OTHERS THEN
                     v_err_flg:='E';
                     v_err_msg:= v_err_msg||','||'Please check the RET Site Flag';
                     FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'Please check the RET Site Flag/Address should be greater than 4 Character for the Supplier No-'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);
                    END; 
                  END IF;
                      
                  EXCEPTION
                   WHEN NO_DATA_FOUND THEN
                      v_err_flg:='E'; 
                      v_err_msg:= v_err_msg||','||'RET Site is not created ';
                      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'RET Site is not created for the Supplier No-'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);
                   WHEN OTHERS THEN
                      v_err_flg:='E';
                      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'Invalid RET Site for the Supplier No-'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);                   
                  END;
                 
               /********************* Validation for testing PAY Site and Flag******************/   
                 
                BEGIN
                   SELECT vendor_site_code
                     INTO v_vendor_pay_site
                     FROM po_vendor_sites_all pvsa
                    WHERE vendor_id = cur_err_r1.vendor_id
                      AND pvsa.org_id = cur_err_r1.org_id                      
                      AND creation_date = (select max(pvsa.creation_date) 
                                             from po_vendor_sites_all pvsa 
                                            where vendor_id = cur_err_r1.vendor_id
                                              AND pvsa.org_id = cur_err_r1.org_id
                                              AND TRUNC(NVL(pvsa.inactive_date,SYSDATE)) >= TRUNC(SYSDATE)                                               
                                              and substr(vendor_site_code, 1, 3) = 'PAY')
                      AND SUBSTR(vendor_site_code, 1, 3) = 'PAY'
                       AND TRUNC(NVL(pvsa.inactive_date,SYSDATE)) >= TRUNC(SYSDATE);
                      
                  IF v_vendor_pay_site IS NOT NULL THEN
                   BEGIN
                     SELECT pay_site_flag 
                       INTO v_pay_site_flag 
                       FROM po_vendor_sites_all pvsa
                      WHERE vendor_id = cur_err_r1.vendor_id
                        AND pvsa.org_id = cur_err_r1.org_id 
                        AND creation_date = (select max(pvsa.creation_date) 
                                               from po_vendor_sites_all pvsa 
                                              where vendor_id = cur_err_r1.vendor_id
                                                AND pvsa.org_id = cur_err_r1.org_id 
                                                AND TRUNC(NVL(pvsa.inactive_date,SYSDATE)) >= TRUNC(SYSDATE) 
                                                and substr(vendor_site_code, 1, 3) = 'PAY')
                        AND SUBSTR(vendor_site_code, 1, 3) = 'PAY'
                         AND TRUNC(NVL(pvsa.inactive_date,SYSDATE)) >= TRUNC(SYSDATE)
                        AND pay_site_flag = 'Y'
                        and length(pvsa.ADDRESS_LINE1||pvsa.ADDRESS_LINE2)>=4;
                   EXCEPTION
                     WHEN NO_DATA_FOUND THEN
                     v_err_flg:='E'; 
                     v_err_msg:= v_err_msg||','||'Please check the Pay site flag';
                     FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'Please check the Pay site flag/Address should be greater than 4 Character for the Supplier No-'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);
                     WHEN OTHERS THEN
                      v_err_flg:='E';
                      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'Invalid PAY Site Flag/Address should be greater than 4 Character for the Supplier No-'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);
                   END;
                  END IF;
                      
                 EXCEPTION
                   WHEN NO_DATA_FOUND THEN
                      v_err_flg:='E'; 
                      v_err_msg:= v_err_msg||','||'Please check the Pay site flag';
                      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'PAY Site is not created for the Supplier Number-'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);
                   WHEN OTHERS THEN
                      v_err_flg:='E';
                      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'Invalid PAY Site for the Supplier No-'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);
                 END;
                 
               /********************* Validation for testing HO Site and Flag******************/   
                 
                BEGIN
                   SELECT vendor_site_code
                     INTO v_vendor_ho_site
                     FROM po_vendor_sites_all pvsa
                    WHERE vendor_id = cur_err_r1.vendor_id
                      AND pvsa.org_id = cur_err_r1.org_id                      
                      AND creation_date = (select max(creation_date) 
                                         from po_vendor_sites_all pvsa                                         
                                        where pvsa.vendor_id = cur_err_r1.vendor_id
                                          AND pvsa.org_id = cur_err_r1.org_id 
                                          AND TRUNC(NVL(pvsa.inactive_date,SYSDATE)) >= TRUNC(SYSDATE)                                                                                  
                                          and substr(vendor_site_code, 1, 2) = 'HO') 
                      AND SUBSTR(vendor_site_code, 1, 2) = 'HO'
                       AND TRUNC(NVL(pvsa.inactive_date,SYSDATE)) >= TRUNC(SYSDATE);
                      
                      
                   IF v_vendor_ho_site IS NOT NULL THEN
                     BEGIN
                      SELECT vendor_site_code
                        INTO v_vendor_ho_site
                        FROM po_vendor_sites_all pvsa
                       WHERE vendor_id = cur_err_r1.vendor_id
                         AND pvsa.org_id = cur_err_r1.org_id                         
                         AND creation_date = (select max(creation_date) 
                                         from po_vendor_sites_all pvsa                                         
                                        where pvsa.vendor_id = cur_err_r1.vendor_id
                                          AND pvsa.org_id = cur_err_r1.org_id
                                          AND TRUNC(NVL(pvsa.inactive_date,SYSDATE)) >= TRUNC(SYSDATE)                                                                                    
                                          and substr(vendor_site_code, 1, 2) = 'HO') 
                         AND SUBSTR(vendor_site_code, 1, 2) = 'HO'
                         AND nvl(pay_site_flag,'N') = 'N'
                         AND nvl(rfq_only_site_flag, 'N') = 'N' 
                         AND nvl(purchasing_site_flag, 'N') = 'N'
                         AND TRUNC(NVL(pvsa.inactive_date,SYSDATE)) >= TRUNC(SYSDATE)
                         and length(pvsa.ADDRESS_LINE1||pvsa.ADDRESS_LINE2)>=4;
                      
                       EXCEPTION
                        WHEN OTHERS THEN
                          v_err_flg:='E';
                          v_err_msg:= v_err_msg||','||'Please check the HO Site Flag';
                          FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'Please check the HO Site Flag/Address should be greater than 4 Character for the Supplier No-'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);
                        END;
                       END IF;
                                            
                   EXCEPTION
                   WHEN OTHERS THEN
                      v_err_flg:='E';
                      v_err_msg:= v_err_msg||','||'HO Site is not created'; 
                      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'HO Site is not created for the Supplier No-'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);                     
                   END;
                 
                /********************* Validation for Vat Region******************/  
                
                 BEGIN
                  SELECT global_attribute10
                    INTO v_tax_region 
                    FROM po_vendors pv 
                   WHERE vendor_id = cur_err_r1.vendor_id;
                   
                   IF v_tax_region IS NULL THEN
                   v_err_flg:='E';
                   v_err_msg:= v_err_msg||','||'Tax Region is not entered';
                   FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'Tax Region is not entered for the Supplier No-'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);
                   END IF;
                   
                  EXCEPTION
                   WHEN OTHERS THEN
                      v_err_flg:='E'; 
                      v_err_msg:= v_err_msg||','||'Invalid Tax Region';
                      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'Invalid Tax Region for the Supplier No'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);
                   END;
                          
                /********************* Validation for Tax Payer ******************/  
                
                 BEGIN
                  SELECT global_attribute11
                    INTO v_tax_payer 
                    FROM po_vendors pv 
                   WHERE vendor_id = cur_err_r1.vendor_id;
                   
                   IF v_tax_payer IS NULL THEN
                      v_err_flg:='E';
                      v_err_msg:= v_err_msg||','||'Tax Payer is not entered';
                   FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'Tax Payer is not entered for the Supplier No-'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);
                   END IF;
                   
                   EXCEPTION                  
                   WHEN OTHERS THEN
                      v_err_flg:='E';
                      v_err_msg:= v_err_msg||','||'Invalid Tax Payer'; 
                      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'Invalid Tax Payer for the Supplier No-'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);
                   END;

                /********************* Validation for Vendor Type******************/  
                
                 BEGIN
                  SELECT vendor_type_lookup_code
                    INTO v_vendor_type 
                    FROM po_vendors pv 
                   WHERE vendor_id = cur_err_r1.vendor_id
                     AND vendor_type_lookup_code IN ('MERCHANDISE','INTERNAL VENDORS','CONSUMABLES');
                     
                   IF v_vendor_type IS NULL THEN
                   v_err_flg:='E';
                   v_err_msg:= v_err_msg||','||'Vendor Type Should be in MERCHANDISE or INTERNAL VENDORS or CONSUMABLES';
                   FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'Vendor Type Should be in MERCHANDISE or INTERNAL VENDORS or CONSUMABLES for the Supplier No'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);
                   END IF;
                 
                   
                  EXCEPTION
                   WHEN OTHERS THEN
                      v_err_flg:='E';
                      v_err_msg:= v_err_msg||','||'Invalid Vendor Type Lookup Code'; 
                      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'Invalid Vendor Type Lookup Code for the Supplier No'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);
                   END;
                   
                   
                /********************* Validation for Payment Method For PUR site******************/  
                
                 BEGIN
                 
                    SELECT iepm.payment_method_code
                      INTO v_pay_method_code 
                      FROM apps.iby_external_payees_all iep,
                           apps.iby_ext_party_pmt_mthds iepm,
                           po_vendor_sites_all pvsa
                     WHERE iep.supplier_site_id = pvsa.vendor_site_id
                       AND iep.ext_payee_id = iepm.ext_pmt_party_id
                       AND pvsa.vendor_id = cur_err_r1.vendor_id
                       AND pvsa.org_id = cur_err_r1.org_id 
                       AND iepm.primary_flag = 'Y'
                       AND TRUNC (NVL (pvsa.inactive_date, SYSDATE)) >= TRUNC (SYSDATE)
                       AND ( ( SUBSTR(pvsa.vendor_site_code,1,3) = 'PUR'));
                 
                 /*SELECT default_payment_method_code
                   INTO v_pay_method_code 
                   FROM po_vendor_sites_all pvsa,
                        iby_external_payees_all iepa 
                  WHERE 1=1--PVSA.VENDOR_SITE_ID =67829
                    AND pvsa.vendor_id = cur_err_r1.vendor_id
                    AND TRUNC(NVL(pvsa.inactive_date,SYSDATE)) >= TRUNC(SYSDATE)                    
                    AND pvsa.org_id = cur_err_r1.org_id
                    AND iepa.supplier_site_id = pvsa.vendor_site_id
                    AND ( ( SUBSTR(pvsa.vendor_site_code,1,3) = 'PUR'));*/
                  
                   IF v_pay_method_code IS NULL THEN
                   v_err_flg:='E'; 
                   v_err_msg:= v_err_msg||','||'Payment Method is Not Entered For PUR site';
                   FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'Payment Method is Not Entered For PUR site for the Supplier No'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);
                   END IF;
                 
                   
                  EXCEPTION
                  WHEN TOO_MANY_ROWS THEN
                       v_err_flg:='E'; 
                       v_err_msg:= v_err_msg||','||'Too Many Payment Methods Found for PUR Site';
                      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'Payment Method is Not Entered For PUR site Supplier No -'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);
                   WHEN OTHERS THEN
                        v_err_flg:='E'; 
                        v_err_msg:= v_err_msg||','||'Invalid Payment Method For PUR site';
                      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'Invalid Payment Method For PUR site for the Supplier No'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);
                   END;
                                          
                   
                /********************* Validation for Payment Method For PAY site******************/  
                
                 BEGIN
                 
                  SELECT iepm.payment_method_code
                      INTO v_pay_method_code 
                      FROM apps.iby_external_payees_all iep,
                           apps.iby_ext_party_pmt_mthds iepm,
                           po_vendor_sites_all pvsa
                     WHERE iep.supplier_site_id = pvsa.vendor_site_id
                       AND iep.ext_payee_id = iepm.ext_pmt_party_id
                       AND pvsa.vendor_id = cur_err_r1.vendor_id
                       AND pvsa.org_id = cur_err_r1.org_id
                       AND iepm.primary_flag = 'Y' 
                       AND TRUNC (NVL (pvsa.inactive_date, SYSDATE)) >= TRUNC (SYSDATE)
                       AND ( ( SUBSTR(pvsa.vendor_site_code,1,3) = 'PAY'));
                                   
                   IF v_pay_method_code IS NULL THEN
                      v_err_flg:='E';
                      v_err_msg:= v_err_msg||','||'Payment Method is Not Entered For PAY Site';
                   FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'Payment Method is Not Entered For Pay Site for the Supplier No'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);
                   END IF;
                 
                   
                  EXCEPTION
                  WHEN TOO_MANY_ROWS THEN
                       v_err_flg:='E';
                       v_err_msg:= v_err_msg||','||'Too Many Payment Methods Found for PAY Site'; 
                      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'Payment Method is Not Entered For Pay Site for the Supplier No'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);
                   WHEN OTHERS THEN
                      v_err_flg:='E'; 
                      v_err_msg:= v_err_msg||','||'Invalid Payment Method For PAY Site';
                      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'Invalid Payment Method For Pay Site for the Supplier No'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);
                   END;
                   
                    /********************* Validation for Payment Method For RET site******************/  
                
                 BEGIN
                 
                  SELECT iepm.payment_method_code
                      INTO v_pay_method_code 
                      FROM apps.iby_external_payees_all iep,
                           apps.iby_ext_party_pmt_mthds iepm,
                           po_vendor_sites_all pvsa
                     WHERE iep.supplier_site_id = pvsa.vendor_site_id
                       AND iep.ext_payee_id = iepm.ext_pmt_party_id
                       AND pvsa.vendor_id = cur_err_r1.vendor_id
                       AND pvsa.org_id = cur_err_r1.org_id 
                       AND iepm.primary_flag = 'Y'
                       AND TRUNC (NVL (pvsa.inactive_date, SYSDATE)) >= TRUNC (SYSDATE)
                       AND ( ( SUBSTR(pvsa.vendor_site_code,1,3) = 'RET'));
                  
                   IF v_pay_method_code IS NULL THEN
                      v_err_flg:='E';
                      v_err_msg:= v_err_msg||','||'Payment Method is Not Entered For RET Site';
                   FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'Payment Method is Not Entered For RET site for the Supplier No'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);
                   END IF;
                 
                   
                  EXCEPTION
                  WHEN TOO_MANY_ROWS THEN
                       v_err_flg:='E'; 
                       v_err_msg:= v_err_msg||','||'Too Many Payment Methods Found for Pay Site For RET Site';
                      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'Payment Method is Not Entered For RET site for the Supplier No'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);
                   WHEN OTHERS THEN
                        v_err_flg:='E'; 
                        v_err_msg:= v_err_msg||','||'Invalid Payment Method for RET Site';
                      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'Invalid Payment Method For RET site for the Supplier No'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);
                   END;
                                          
                   
                /********************* Validation for Payment Method For HO site******************/  
                
                 BEGIN
                 
                  SELECT iepm.payment_method_code
                      INTO v_pay_method_code 
                      FROM apps.iby_external_payees_all iep,
                           apps.iby_ext_party_pmt_mthds iepm,
                           po_vendor_sites_all pvsa
                     WHERE iep.supplier_site_id = pvsa.vendor_site_id
                       AND iep.ext_payee_id = iepm.ext_pmt_party_id
                       AND pvsa.vendor_id = cur_err_r1.vendor_id
                       AND pvsa.org_id = cur_err_r1.org_id 
                       AND iepm.primary_flag = 'Y'
                       AND TRUNC (NVL (pvsa.inactive_date, SYSDATE)) >= TRUNC (SYSDATE)
                       AND ( ( SUBSTR(pvsa.vendor_site_code,1,2) = 'HO'));
                  
                   IF v_pay_method_code IS NULL THEN
                      v_err_flg:='E';
                      v_err_msg:= v_err_msg||','||'Payment Method is Not Entered For HO Site';
                   FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'Payment Method is Not Entered For HO Site for the Supplier No'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);
                   END IF;
                 
                   
                  EXCEPTION
                  WHEN TOO_MANY_ROWS THEN
                       v_err_flg:='E'; 
                       v_err_msg:= v_err_msg||','||'Too Many Payment Methods Found for Pay Site HO Site';
                      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'Payment Method is Not Entered For HO Site for the Supplier No'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);
                   WHEN OTHERS THEN
                        v_err_flg:='E'; 
                        v_err_msg:= v_err_msg||','||'Invalid Payment Method for HO Site';
                      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'Invalid Payment Method for HO Site for the Supplier No'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);
                   END;
                                          
                    
                /********************* Validation For State Name  Pur Site ******************/  
                
                 BEGIN
                  SELECT distinct state
                    INTO v_state_name 
                    FROM po_vendor_sites_all pvsa,
                         po_vendors pv 
                   WHERE pv.vendor_id = pvsa.vendor_id
                     AND TRUNC(NVL(pvsa.inactive_date,SYSDATE)) >= TRUNC(SYSDATE)
                     AND pv.vendor_id = cur_err_r1.vendor_id
                     AND pvsa.org_id = cur_err_r1.org_id                                        
                     AND (SUBSTR(pvsa.VENDOR_SITE_CODE,1,3) = 'PUR');
                         
                       IF v_state_name IS NOT NULL THEN
                          BEGIN                          
                           SELECT meaning
                             INTO v_state_name
                             FROM fnd_lookup_values
                            WHERE lookup_type = 'ABRL_STATE_CODE'
                              AND UPPER(meaning)=UPPER(v_state_name);
                           EXCEPTION        
                             WHEN NO_DATA_FOUND THEN
                              v_err_flg:='E';
                              v_err_msg:= v_err_msg||','||'State Name is Not Valid PUR Site';
                              FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'State Name for PUR Site is Not Valid for the Supplier No'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);
                              WHEN OTHERS THEN
                              v_err_flg:='E'; 
                              v_err_msg:= v_err_msg||','||'State Name is not as per the format in PUR Site'; 
                              FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'State Name is not as per the format for the Supplier No'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);    
                           END;                                    
                      END IF;   
                      
                  EXCEPTION
                   WHEN NO_DATA_FOUND THEN
                        v_err_flg:='E'; 
                        v_err_msg:= v_err_msg||','||'State Name is Not entered for PUR Site';
                      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'State Name is Not entered for PUR Site Supplier No'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);
                   WHEN TOO_MANY_ROWS THEN
                        v_err_flg:='E';
                        v_err_msg:= v_err_msg||','||'Too many records Found for State Name in PUR Site';
                      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'Too many records Found for State Name in PUR Site the Supplier No'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);                   
                   WHEN OTHERS THEN
                        v_err_flg:='E';
                        v_err_msg:= v_err_msg||','||'State Name is not as per the format in PUR site'; 
                      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'State Name is not as per the format in PUR site for the Supplier No'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);
                   END;
                   
                   /********************* Validation For State Name  Pay Site ******************/  
                
                 BEGIN
                  SELECT distinct state
                    INTO v_state_name 
                    FROM po_vendor_sites_all pvsa,
                         po_vendors pv 
                   WHERE pv.vendor_id = pvsa.vendor_id
                     AND TRUNC(NVL(pvsa.inactive_date,SYSDATE)) >= TRUNC(SYSDATE)
                     AND pv.vendor_id = cur_err_r1.vendor_id
                     AND pvsa.org_id = cur_err_r1.org_id                                        
                     AND (SUBSTR(pvsa.VENDOR_SITE_CODE,1,3) = 'PAY');
                         
                       IF v_state_name IS NOT NULL THEN
                          BEGIN                          
                           SELECT meaning
                             INTO v_state_name
                             FROM fnd_lookup_values
                            WHERE lookup_type = 'ABRL_STATE_CODE'
                              AND UPPER(meaning)=UPPER(v_state_name);
                           EXCEPTION        
                             WHEN NO_DATA_FOUND THEN
                              v_err_flg:='E';
                              v_err_msg:= v_err_msg||','||'State Name is Not Valid in PAY Site';
                              FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'State Name for PAY Site is Not Valid for the Supplier No'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);
                              WHEN OTHERS THEN
                              v_err_flg:='E'; 
                              v_err_msg:= v_err_msg||','||'State Name is not as per the format in pay site'; 
                              FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'State Name is not as per the format for PAY Site the Supplier No'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);    
                           END;                                    
                      END IF;   
                      
                  EXCEPTION
                   WHEN NO_DATA_FOUND THEN
                        v_err_flg:='E'; 
                        v_err_msg:= v_err_msg||','||'State Name is Not entered for PAY Site';
                      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'State Name is Not entered for PUR Site Supplier No'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);
                   WHEN TOO_MANY_ROWS THEN
                        v_err_flg:='E';
                        v_err_msg:= v_err_msg||','||'Too many records Found for State Name in PAY Site';
                      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'Too many records Found for State Name in PAY Site the Supplier No'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);                   
                   WHEN OTHERS THEN
                        v_err_flg:='E';
                        v_err_msg:= v_err_msg||','||'State Name is not as per the format FOR PAY SITE'; 
                      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'State Name is not as per the format for PAY Site Supplier No'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);
                   END;
                   
                    /********************* Validation For State Name  Ret Site ******************/  
                
                 BEGIN
                  SELECT distinct state
                    INTO v_state_name 
                    FROM po_vendor_sites_all pvsa,
                         po_vendors pv 
                   WHERE pv.vendor_id = pvsa.vendor_id
                     AND TRUNC(NVL(pvsa.inactive_date,SYSDATE)) >= TRUNC(SYSDATE)
                     AND pv.vendor_id = cur_err_r1.vendor_id
                     AND pvsa.org_id = cur_err_r1.org_id                                        
                     AND (SUBSTR(pvsa.VENDOR_SITE_CODE,1,3) = 'RET');
                         
                       IF v_state_name IS NOT NULL THEN
                          BEGIN                          
                           SELECT meaning
                             INTO v_state_name
                             FROM fnd_lookup_values
                            WHERE lookup_type = 'ABRL_STATE_CODE'
                              AND UPPER(meaning)=UPPER(v_state_name);
                           EXCEPTION        
                             WHEN NO_DATA_FOUND THEN
                              v_err_flg:='E';
                              v_err_msg:= v_err_msg||','||'State Name is Not Valid RET Site';
                              FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'State Name for RET Site is Not Valid for the Supplier No'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);
                              WHEN OTHERS THEN
                              v_err_flg:='E'; 
                              v_err_msg:= v_err_msg||','||'State Name is not as per the format in RET site'; 
                              FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'State Name is not as per the format for RET Site Supplier No'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);    
                           END;                                    
                      END IF;   
                      
                  EXCEPTION
                   WHEN NO_DATA_FOUND THEN
                        v_err_flg:='E'; 
                        v_err_msg:= v_err_msg||','||'State Name is Not entered for RET Site';
                      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'State Name is Not entered for RET Site Supplier No'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);
                   WHEN TOO_MANY_ROWS THEN
                        v_err_flg:='E';
                        v_err_msg:= v_err_msg||','||'Too many records Found for State Name in RET Site';
                      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'Too many records Found for State Name in RET Site the Supplier No'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);                   
                   WHEN OTHERS THEN
                        v_err_flg:='E';
                        v_err_msg:= v_err_msg||','||'State Name is not as per RET Site '; 
                      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'State Name is not as per the format for RET Site Supplier No'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);
                   END;
                   
                   
                    /********************* Validation For State Name  HO Site ******************/  
                
                 BEGIN
                  SELECT distinct state
                    INTO v_state_name 
                    FROM po_vendor_sites_all pvsa,
                         po_vendors pv 
                   WHERE pv.vendor_id = pvsa.vendor_id
                     AND TRUNC(NVL(pvsa.inactive_date,SYSDATE)) >= TRUNC(SYSDATE)
                     AND pv.vendor_id = cur_err_r1.vendor_id
                     AND pvsa.org_id = cur_err_r1.org_id                                        
                     AND (SUBSTR(pvsa.VENDOR_SITE_CODE,1,2) = 'HO');
                         
                       IF v_state_name IS NOT NULL THEN
                          BEGIN                          
                           SELECT meaning
                             INTO v_state_name
                             FROM fnd_lookup_values
                            WHERE lookup_type = 'ABRL_STATE_CODE'
                              AND UPPER(meaning)=UPPER(v_state_name);
                           EXCEPTION        
                             WHEN NO_DATA_FOUND THEN
                              v_err_flg:='E';
                              v_err_msg:= v_err_msg||','||'State Name is Not Valid HO Site';
                              FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'State Name for HO Site is Not Valid for the Supplier No'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);
                              WHEN OTHERS THEN
                              v_err_flg:='E'; 
                              v_err_msg:= v_err_msg||','||'State Name is not as per the format in HO site'; 
                              FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'State Name is not as per the format for HO Site the Supplier No'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);    
                           END;                                    
                      END IF;   
                      
                  EXCEPTION
                   WHEN NO_DATA_FOUND THEN
                        v_err_flg:='E'; 
                        v_err_msg:= v_err_msg||','||'State Name is Not entered for HO Site';
                      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'State Name is Not entered for HO Site Supplier No'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);
                   WHEN TOO_MANY_ROWS THEN
                        v_err_flg:='E';
                        v_err_msg:= v_err_msg||','||'Too many records Found for State Name in HO Site';
                      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'Too many records Found for State Name in HO Site the Supplier No'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);                   
                   WHEN OTHERS THEN
                        v_err_flg:='E';
                        v_err_msg:= v_err_msg||','||'State Name is not as per the format'; 
                      FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'State Name is not as per the format for HO Site Supplier No'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);
                   END;
                
                FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'------------------------------------------------------------------');   
                   
                   ELSE 
                    v_err_flg:='E';
                    v_err_msg:= v_err_msg||','||'Invalid Number of Sites'; 
                    FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'Supplier having more than 4 sites Supplier No'||CUR_ERR_R1.segment1||' org id-'||cur_err_r1.org_id);
                    
                 FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'------------------------------------------------------------------');   
                    
             END IF;
             
           
        END;
         
         /****Inserting the errored vendors into temp table to avoid getting 
         the data from Outbound interface report *******/  
         
         IF v_err_flg ='E' THEN         
         INSERT INTO
             xxabrl_supplier_err_data 
                                (vendor_id,
                                 vendor_num)                           
                         values
                               (cur_err_r1.vendor_id,
                                cur_err_r1.segment1);
                                
         commit;
         END IF;         
     
     END LOOP;
     
    END;
 
    
END XXABRL_Update_Outbound_Status; 
/


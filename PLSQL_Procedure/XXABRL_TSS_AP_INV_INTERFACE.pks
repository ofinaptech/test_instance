CREATE OR REPLACE PACKAGE APPS.xxabrl_tss_ap_inv_interface
IS
   PROCEDURE validate_invoices (errbuf OUT VARCHAR2, retcode OUT NUMBER);

   PROCEDURE insert_transaction (
      p_org_id                       IN   NUMBER,
      p_vendor_site_id               IN   NUMBER,
      p_vendor_id                    IN   NUMBER,
      p_payment_method_lookup_code   IN   VARCHAR2
   );
END xxabrl_tss_ap_inv_interface; 
/


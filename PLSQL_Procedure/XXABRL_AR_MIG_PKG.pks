CREATE OR REPLACE PACKAGE APPS.xxabrl_ar_mig_pkg
IS
   PROCEDURE invoice_validate (
      errbuf           OUT      VARCHAR2,
      retcode          OUT      NUMBER,
      /*P_Org_Id       In NUMBER,*/
      p_batch_source   IN       VARCHAR2,
      p_action                  VARCHAR2,
     p_gl_date                 VARCHAR2
   );

   PROCEDURE invoice_insert (
      p_org_id         IN       NUMBER,
      p_batch_source   IN       VARCHAR2,
     -- p_gl_date        IN       DATE,
      x_ret_code       OUT      NUMBER
   );

   FUNCTION account_seg_status (
      p_seg_value   IN   VARCHAR2,
      p_seg_desc    IN   VARCHAR2
   )
      RETURN VARCHAR2;
END xxabrl_ar_mig_pkg; 
/


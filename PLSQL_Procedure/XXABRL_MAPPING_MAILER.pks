CREATE OR REPLACE PACKAGE APPS.XXABRL_MAPPING_MAILER IS
---
PROCEDURE XXABRL_VENDOR_MAP_MAILER ( errbuf     out VARCHAR2
                                  ,      retcode    out VARCHAR2
                                   );
--This Procedure will Sending mail all Vendor Mapping Error


PROCEDURE XXABRL_CUSTOMER_MAP_MAILER (  errbuf     out VARCHAR2
                                  ,  retcode    out VARCHAR2);
--This Procedure will Sending mail all Customer Mapping Error
end XXABRL_MAPPING_MAILER; 
/


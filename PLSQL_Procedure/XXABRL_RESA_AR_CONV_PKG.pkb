CREATE OR REPLACE PACKAGE BODY APPS.XXABRL_RESA_AR_CONV_PKG IS
 /*
  =========================================================================================================
  ||   Filename   : XXABRL_RESA_AR_CONV_PKG.sql
  ||   Description : Script is used to mold ReSA data for AR
  ||
  ||   Version     Date            Author              Modification
  ||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
  ||   1.0.0       13-MON-2008    Hans Raj Kasana      New Development
  ||   1.0.1       25-AUG-2009    Shailesh Bharambe    Change Request
  ||  ~~~~~~~~    ~~~~~~~~~~~   ~~~~~~~~~~~~~~~~~~    ~~~~~~~~~~~~~~~~~
  ========================================================================================================*/
-- Date     Change Requirement/Done/Impact
--=====     ==============================
--26/11     Logic: Lines count rejected shd be equal to lines processed, if not reject data files
--25/08/09  Logic: Customer account type should be STORE and it should be active at site level
--
--
--
--
--
--
--
--
--
--
--
--
--========================================================================================================
  gn_conc_req_id NUMBER := fnd_global.conc_request_id; -- To print the concurrent request id
  gn_user_id     NUMBER := fnd_global.user_id;
  /*************************************************************************************************************/
  /* Main Procedure calling */
  /*************************************************************************************************************/
  PROCEDURE MAIN_PROC(x_err_buf    OUT VARCHAR2,
                      x_ret_code   OUT NUMBER,
                      p_action     IN VARCHAR2,
                      p_debug_flag IN VARCHAR2) IS
    ln_val_ins_retcode     NUMBER := 0; --variable to return the status
    ln_val_retcode         NUMBER := 0;
    ln_val_new_retcode     NUMBER := 0;
    ln_val_ins_new_retcode NUMBER := 0;
    v_tot_cnt              NUMBER := 0;
    v_tot_cnt1             NUMBER := 0;
    v_tot_cnt2             NUMBER := 0;
    v_succ_cnt             NUMBER := 0;
    v_succ_cnt1            NUMBER := 0;
    v_succ_cnt2            NUMBER := 0;
    v_fail_cnt             NUMBER := 0;
    v_fail_cnt1            NUMBER := 0;
    v_fail_cnt2            NUMBER := 0;
    v_user_name            VARCHAR2(100) := NULL;
    v_ret_code             NUMBER;
  BEGIN
    Validate_ReSA(p_debug_flag,v_ret_code);
    x_ret_code :=v_ret_code;
    IF p_action = 'N' THEN
      Insert_ReSA('ABRL_RESA',v_ret_code);
      x_ret_code :=v_ret_code;
    END IF;
  END MAIN_PROC;
  /*************************************************************************************************************/
  /* Procedure for Printing Log */
  /*************************************************************************************************************/
  PROCEDURE Print_log(p_msg IN VARCHAR2, p_debug_flag VARCHAR2) AS
  BEGIN
    IF p_debug_flag = 'Y' THEN
      Fnd_File.PUT_LINE(fnd_file.LOG, p_msg);
    END IF;
  END Print_log;
  PROCEDURE Validate_ReSA(p_debug_flag IN VARCHAR2, p_ret_code OUT NUMBER) AS
    CURSOR cur_val IS
      SELECT tt.ROWID, tt.*
        FROM XXABRL_RESA_AR_INT tt 
       WHERE INTERFACE_FLAG IN ('N', 'E')  
       AND ORG_ID <> 144; 
    v_error_str VARCHAR2(999);
    x_error_msg VARCHAR2(240);
    v_valid_cnt NUMBER := 0;
    v_total_cnt NUMBER := 0;
    v_error_cnt NUMBER := 0;
  BEGIN
    Fnd_File.PUT_LINE(fnd_file.LOG, '### ReSA Validation Starts ###');
    FOR v_cur_val IN cur_val LOOP
      v_error_str := NULL;
      Fnd_File.PUT_LINE(fnd_file.LOG,
                        'Validating SEQ_NO#('||v_cur_val.s_num||')  --------------------');
      --=== Store / Customer Validation
      IF v_cur_val.STORE IS NOT NULL THEN
        
      --  Print_log('sTART OF cUSTOMER vALIDATION: ' , p_debug_flag); 
        Validate_customer(v_cur_val.STORE, v_cur_val.ROWID,v_cur_val.org_id, x_error_msg);
       -- Print_log('eND OF cUSTOMER vALIDATION: ' , p_debug_flag);
        
        IF x_error_msg IS NOT NULL THEN
          v_error_str := v_error_str || ';' || x_error_msg;
        END IF;
      ELSE
        v_error_str := v_error_str || ';' || 'STORE_CODE is NULL';
      END IF;
      --=== Curr Code Validation
      IF v_cur_val.CURRENCY_CODE IS NOT NULL THEN
        Validate_Currency_code(v_cur_val.CURRENCY_CODE, x_error_msg);
        IF x_error_msg IS NOT NULL THEN
          v_error_str := v_error_str || ';' || x_error_msg;
        END IF;
      ELSE
        v_error_str := v_error_str || ';' || 'CURR_CODE is NULL';
      END IF;
      --=== Curr Conv Code Validation
      IF v_cur_val.CURRENCY_CONVERSION_TYPE IS NOT NULL THEN
        Validate_Curr_Conv_code(v_cur_val.CURRENCY_CONVERSION_TYPE,
                                x_error_msg);
        IF x_error_msg IS NOT NULL THEN
          v_error_str := v_error_str || ';' || x_error_msg;
        END IF;
      ELSE
        v_error_str := v_error_str || ';' || 'CURR_CONV_CODE is NULL';
      END IF;
      --=== CCID Validation
      IF v_cur_val.CODE_COMBINATION_ID IS NOT NULL THEN
        Validate_CCID(v_cur_val.CODE_COMBINATION_ID,
                      v_cur_val.ROWID,
                      x_error_msg);
        IF x_error_msg IS NOT NULL THEN
          v_error_str := v_error_str || ';' || x_error_msg;
        END IF;
      ELSE
        v_error_str := v_error_str || ';' || 'CCID is NULL';
      END IF;
      --=== TRX/GL Date Validation
      IF v_cur_val.ACCOUNTING_DATE IS NULL THEN
        v_error_str := v_error_str || ';' || 'TRX/GL Date is NULL';
      END IF;
      --=====
      IF v_error_str IS NOT NULL THEN
        UPDATE XXABRL_RESA_AR_INT int_ln
           SET INTERFACE_FLAG = 'E', ERROR_MESSAGE = v_error_str
         WHERE ROWID = v_cur_val.ROWID;
        Print_log('***Record Error: ' || v_error_str, p_debug_flag);
        v_error_cnt := v_error_cnt + 1;
      ELSE
        UPDATE XXABRL_RESA_AR_INT int_ln
           SET INTERFACE_FLAG = 'V', ERROR_MESSAGE = v_error_str
         WHERE ROWID = v_cur_val.ROWID;
        Print_log('Record Valid: ' || v_error_str, p_debug_flag);
        v_valid_cnt := v_valid_cnt + 1;
      END IF;
      v_total_cnt := v_total_cnt + 1;
      COMMIT;
    END LOOP;
    Fnd_File.PUT_LINE(fnd_file.LOG,
                      '========================================================');
    Fnd_File.PUT_LINE(fnd_file.LOG, 'Total Records: ' || v_total_cnt);
    Fnd_File.PUT_LINE(fnd_file.LOG, 'Total Valid: ' || v_valid_cnt);
    Fnd_File.PUT_LINE(fnd_file.LOG, 'Total Errors: ' || v_error_cnt);
    Fnd_File.PUT_LINE(fnd_file.LOG,
                      '========================================================');
    IF v_error_cnt>0 THEN
       p_ret_code :=1;
    END IF;
    Fnd_File.PUT_LINE(fnd_file.LOG, '### ReSA Validation Ends ###');
  END Validate_ReSA;
  --== Validation for Customer /Store
  PROCEDURE Validate_customer(p_store     IN VARCHAR2,
                              p_rowid     VARCHAR2,
                              p_org_id   IN VARCHAR2, --suresh
                              p_error_msg OUT VARCHAR2) AS
    v_CUST_ACCOUNT_ID   NUMBER;
    v_cust_Acct_site_id NUMBER;
    v_ACCOUNT_NUMBER    VARCHAR2(25);
    v_GL_ID_REC         VARCHAR2(25);
    v_Store_Rece        VARCHAR2(25);
    v_org_id            VARCHAR2(25);
    v_Organization_code VARCHAR2(25);
  BEGIN
    --last four number of Cust-account number
    BEGIN
      
      /*SELECT cust_account_id, account_number
        INTO v_CUST_ACCOUNT_ID, v_ACCOUNT_NUMBER
        FROM hz_cust_accounts hca
       WHERE REVERSE(SUBSTR(REVERSE(hca.account_number), 1, 4)) = p_store;*/
       -- following code added by shailesh on 25th aug 2009 
       -- Added 2 more conditions customer class code should be STORE and 
       -- customer site should be active.
       Begin
        
        
       SELECT hca.cust_account_id, hca.account_number
        INTO v_CUST_ACCOUNT_ID, v_ACCOUNT_NUMBER
        FROM hz_cust_accounts hca,hz_cust_acct_sites_all hcac
               WHERE  REVERSE(SUBSTR(REVERSE(hca.account_number), 1, 4)) = p_store AND 
                 CUSTOMER_CLASS_CODE = 'STORE'
               AND hca.cust_account_id=hcac.cust_account_id
               AND hcac.status='A'
                AND hcac.org_id=p_org_id;
       
       EXCEPTION
            WHEN NO_DATA_FOUND THEN
              p_error_msg := 'No store found '  ||
                             p_store;
              
            WHEN TOO_MANY_ROWS THEN
              p_error_msg := 'Multiple Store Receivable  Found A' ||
                             p_store;
              
            WHEN OTHERS THEN
              p_error_msg := 'Exception while getting the store account  ' ||
                             p_store; 
          END;
          
       
      BEGIN
        SELECT hcas.cust_Acct_site_id,
               hcsu.GL_ID_REC --Customer Receivable account
              --, hcas.org_id    --suresh
          INTO v_cust_Acct_site_id, v_GL_ID_REC --,v_org_id
          FROM hz_cust_accounts_all   hca,
               hz_cust_acct_sites_all hcas,
               hz_party_sites         hps,
               hz_cust_site_uses_all  hcsu
         WHERE hca.cust_account_id = v_CUST_ACCOUNT_ID
           AND hca.cust_account_id = hcas.cust_account_id
           AND hcas.org_id = P_Org_id --Need to validate for all ORG's data ***--suresh
           AND hcas.org_id NOT IN (201) -- A wrong ORG created/ cust site created in 201
           AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id(+)
           AND hcas.party_site_id = hps.party_site_id
           --And hps.Party_Site_Number = xac.OF_Customer_Site_Code
           AND hcsu.site_use_code = 'BILL_TO'
           AND hcas.bill_to_flag = 'P'
           AND hcas.status = 'A';
           Print_log('Derived v_cust_Acct_site_id '||v_cust_Acct_site_id,'Y');
           Print_log('Derived v_GL_ID_REC '||v_GL_ID_REC,'Y');
           Print_log('Derived v_org_id '||v_org_id,'Y');
        IF v_GL_ID_REC IS NULL THEN
          p_error_msg := 'Store Revenue account not defined for ' ||
                         p_store;
        ELSE
          BEGIN
            SELECT segment6
              INTO v_Store_Rece
              FROM gl_code_combinations_kfv
             WHERE code_combination_id = v_GL_ID_REC
               AND enabled_flag = 'Y';
          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              p_error_msg := 'Store Receivable CCID/GL not Found for ' ||
                             p_store;
            WHEN TOO_MANY_ROWS THEN
              p_error_msg := 'Multiple Store Receivable CCID/GL Found for ' ||
                             p_store;
            WHEN OTHERS THEN
              p_error_msg := 'Exception while deriving  Receivable CCID/GL for ' ||
                             p_store;
          END;
        END IF;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          p_error_msg := 'Bill_TO Site not Found for ' || p_store;
        WHEN TOO_MANY_ROWS THEN
          p_error_msg := 'Multiple Bill_TO Site Found for ' || p_store;
        WHEN OTHERS THEN
          p_error_msg := 'Exception while deriving  Bill_TO Site for ' ||
                         p_store;
      END;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        p_error_msg := 'Store not defined in Oracle ' || p_store;
      WHEN TOO_MANY_ROWS THEN
        p_error_msg := 'Multiple Store defined for ' || p_store;
      WHEN OTHERS THEN
        p_error_msg := 'Exception while deriving Store for ' || p_store;
    END;
    Print_log(p_error_msg,'Y');
    --== Find ORG CODE
    BEGIN
      SELECT short_code
        INTO v_Organization_code
        FROM hr_operating_units
       WHERE organization_id = P_org_id;--v_org_id; ---suresh
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        p_error_msg := 'Store Org_ID not defined in Oracle ' || p_store;
      WHEN TOO_MANY_ROWS THEN
        p_error_msg := 'Multiple Store Org_ID defined for ' || p_store;
      WHEN OTHERS THEN
        p_error_msg := 'Exception while deriving Store Org_ID for ' ||
                       p_store;
    END;--suresh
    --== Updating Derived Customer/Store Data
    IF p_error_msg IS NULL THEN
      BEGIN
        UPDATE XXABRL_RESA_AR_INT int_ln
           SET int_ln.NV_CUSTOMER_ID = v_CUST_ACCOUNT_ID,
               ACCOUNT_NUMBER        = v_ACCOUNT_NUMBER,
               CUST_REC_ACCT         = v_Store_Rece,
               ORG_CODE              = v_Organization_code
         WHERE ROWID = p_rowid;
      EXCEPTION
        WHEN OTHERS THEN
          p_error_msg := 'Exception while derived Store details for ' ||
                         p_store;
      END;
    END IF;
    Print_log(p_error_msg,'Y');
  EXCEPTION
    WHEN OTHERS THEN
      fnd_file.PUT_LINE(fnd_file.LOG,
                        'Exception in Store/Customer validation');
  END Validate_customer;
  --==========================================
  PROCEDURE Validate_Currency_code(p_curr_code IN VARCHAR2,
                                   p_error_msg OUT VARCHAR2) AS
    V_Fun_Curr VARCHAR2(240);
  BEGIN
    SELECT Currency_code
      INTO V_Fun_Curr
      FROM GL_SETS_OF_BOOKS
     WHERE set_of_books_id = v_set_of_bks_id;
    p_error_msg := NULL;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      p_error_msg := 'Curr_Code not defined in Oracle ' || p_curr_code;
    WHEN TOO_MANY_ROWS THEN
      p_error_msg := 'Multiple Curr_Code defined for ' || p_curr_code;
    WHEN OTHERS THEN
      p_error_msg := 'Exception while deriving Curr_Code for ' ||
                     p_curr_code;
  END Validate_Currency_code;
  PROCEDURE Validate_Curr_Conv_code(p_curr_conv_code IN VARCHAR2,
                                    p_error_msg      OUT VARCHAR2) AS
    V_Curr_conv_code VARCHAR2(240);
  BEGIN
    SELECT conversion_type
      INTO V_Curr_conv_code
      FROM GL_DAILY_CONVERSION_TYPES
     WHERE UPPER(conversion_type) = UPPER(p_curr_conv_code);
    p_error_msg := NULL;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      p_error_msg := 'Curr_Conv_Code not defined in Oracle ' ||
                     p_curr_conv_code;
    WHEN TOO_MANY_ROWS THEN
      p_error_msg := 'Multiple Curr_Conv_Code defined for ' ||
                     p_curr_conv_code;
    WHEN OTHERS THEN
      p_error_msg := 'Exception while deriving Curr_Conv_Code for ' ||
                     p_curr_conv_code;
  END Validate_Curr_Conv_code;
  PROCEDURE Validate_CCID(p_CCID      IN VARCHAR2,
                          p_rowid     VARCHAR2,
                          p_error_msg OUT VARCHAR2) AS
    v_CCID     VARCHAR2(240);
    v_Segment1 VARCHAR2(240);
    v_Segment2 VARCHAR2(240);
    v_Segment3 VARCHAR2(240);
    v_Segment4 VARCHAR2(240);
    v_Segment5 VARCHAR2(240);
    v_Segment6 VARCHAR2(240);
    v_Segment7 VARCHAR2(240);
    v_Segment8 VARCHAR2(240);
  BEGIN
    SELECT code_combination_id,
           segment1,
           segment2,
           segment3,
           segment4,
           segment5,
           segment6,
           segment7,
           segment8
      INTO v_CCID,
           v_Segment1,
           v_Segment2,
           v_Segment3,
           v_Segment4,
           v_Segment5,
           v_Segment6,
           v_Segment7,
           v_Segment8
      FROM gl_code_combinations_kfv
     WHERE code_combination_id = p_CCID
       AND enabled_flag = 'Y'
    --and char_of_account_id =50328
    ;
    BEGIN
      UPDATE XXABRL_RESA_AR_INT int_ln
         SET Segment1 = v_Segment1,
             Segment2 = v_Segment2,
             Segment3 = v_Segment3,
             Segment4 = v_Segment4,
             Segment5 = v_Segment5,
             Segment6 = v_Segment6,
             Segment7 = v_Segment7,
             Segment8 = v_Segment8
       WHERE ROWID = p_rowid;
    EXCEPTION
      WHEN OTHERS THEN
        p_error_msg := 'Exception while updating segments Inv_CCID for ' ||
                       p_CCID;
    END;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      p_error_msg := 'CCID not defined in Oracle ' || p_CCID;
    WHEN TOO_MANY_ROWS THEN
      p_error_msg := 'Multiple CCID defined for ' || p_CCID;
    WHEN OTHERS THEN
      p_error_msg := 'Exception while deriving CCID for ' || p_CCID;
  END Validate_CCID;
  --== Derive_Tran_Type
PROCEDURE Derive_Tran_Type(p_Resa_cur    IN XXABRL_RESA_AR_INT%ROWTYPE,
                           p_Nature      OUT VARCHAR2,
                           p_Tran_Type   OUT VARCHAR2,
                           p_Description OUT VARCHAR2,
                           p_error_msg   OUT VARCHAR2) AS
  v_Roll_Up2      VARCHAR2(240);
  v_Nature      VARCHAR2(240);
  v_Tran_Type   VARCHAR2(240);
  v_Description VARCHAR2(240);
BEGIN
  Print_log('Derive Tran-Type for ID_TYPE: '||p_Resa_cur.ID_TYPE ,'N');
  Print_log('Derive Tran-Type for Roll_Up1: '||p_Resa_cur.ROLLUP_LEVEL_1 ,'N');
  BEGIN
    SELECT
    --MEANING,
    --DESCRIPTION,
    --ATTRIBUTE1, --<<in where clause
     ATTRIBUTE2, ATTRIBUTE3, ATTRIBUTE4, ATTRIBUTE5
      INTO v_Roll_Up2, v_Nature, v_Tran_Type, v_Description
      FROM FND_LOOKUP_VALUES_VL
     WHERE SYSDATE BETWEEN NVL(START_DATE_ACTIVE, SYSDATE) AND
           NVL(END_DATE_ACTIVE, SYSDATE)
       AND ENABLED_FLAG = 'Y'
       AND LOOKUP_TYPE = 'RESA_AR_TOTAL_ID_LKP'
       AND ATTRIBUTE_CATEGORY = 'RESA_AR_TOTAL_ID_LKP'
       AND DESCRIPTION = p_Resa_cur.ID_TYPE
       AND DECODE(ATTRIBUTE1,'All',p_Resa_cur.ROLLUP_LEVEL_1,ATTRIBUTE1) = p_Resa_cur.ROLLUP_LEVEL_1
       AND DECODE(ATTRIBUTE2,'All',p_Resa_cur.ROLLUP_LEVEL_2,ATTRIBUTE2) = p_Resa_cur.ROLLUP_LEVEL_2
--       AND ATTRIBUTE1 = decode(v_ATTRIBUTE1,'All','All',p_Resa_cur.ROLLUP_LEVEL_1)
--       AND ATTRIBUTE2 = decode(v_ATTRIBUTE2,'All','All',p_Resa_cur.ROLLUP_LEVEL_2)
--       AND ATTRIBUTE2 = decode(DESCRIPTION,'TENDER_TOT','All','ROUNDAMT','All','NETSALES','All',p_Resa_cur.ROLLUP_LEVEL_2)
       ;
       Print_log('Derived Nature: '||v_Nature ,'N');
       Print_log('Derived Tran-Type: '||v_Tran_Type ,'N');
       Print_log('Derived Desc: '||v_Description ,'N');
       p_Nature      := v_Nature;
       p_Tran_Type   := v_Tran_Type;
       p_Description := v_Description;
       RETURN;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      p_error_msg := 'Tran-Type Lookup not defined for ' ||
                     p_Resa_cur.ID_TYPE || '/' || p_Resa_cur.ROLLUP_LEVEL_1;
    WHEN TOO_MANY_ROWS THEN
      p_error_msg := 'Multiple Tran-Type Lookup defined for ' ||
                     p_Resa_cur.ID_TYPE || '/' || p_Resa_cur.ROLLUP_LEVEL_1;
    WHEN OTHERS THEN
      p_error_msg := 'Exception while deriving Tran-Type Lookup for ' ||
                     p_Resa_cur.ID_TYPE || '/' || p_Resa_cur.ROLLUP_LEVEL_1;
  END;
  Print_log(p_error_msg,'Y');
END Derive_Tran_Type;
  PROCEDURE Derive_Bank_Dtl(p_Store               IN XXABRL_RESA_AR_INT%ROWTYPE,
                            p_Tran_Type           IN VARCHAR2,
                            x_Bank_Account_Name   OUT VARCHAR2,
                            x_Bank_Account_Number OUT VARCHAR2,
                            x_description         OUT VARCHAR2,
                            x_error_msg           OUT VARCHAR2) AS
    v_Bank_Account_Name   VARCHAR2(240);
    v_Bank_Account_Number VARCHAR2(240);
    v_description         VARCHAR2(240);
  BEGIN
    IF p_Tran_Type IS NOT NULL THEN
    Print_log('Find Bank Details for :'||p_Tran_Type,'Y');
    --Print_log('afetr bank details','Y');
      BEGIN
           SELECT description INTO v_description
                  FROM FND_FLEX_VALUES_VL
                  WHERE UPPER(FLEX_VALUE_MEANING) = UPPER(p_Store.ROLLUP_LEVEL_1)
                  AND flex_value_set_id =
                  (SELECT flex_value_set_id
                  FROM fnd_flex_value_sets
                  WHERE UPPER(flex_value_set_name) = UPPER('RESA Roll up 1'));
                  x_description := v_description;
                   
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          x_error_msg := 'Description not Found for Rol_up1' ||
                         p_Store.ROLLUP_LEVEL_1;
        WHEN TOO_MANY_ROWS THEN
          x_error_msg := 'Multiple Description Found for Rol_up1' ||
                         p_Store.ROLLUP_LEVEL_1;
        WHEN OTHERS THEN
          x_error_msg := 'Exception in Description for Rol_up1' ||
                         p_Store.ROLLUP_LEVEL_1||SQLERRM;
      END;
   --   Print_log('starting the getting bank acct name and number' ,'Y');
    BEGIN
      SELECT
      --MEANING,
      --DESCRIPTION,
      --ATTRIBUTE1,
       ATTRIBUTE2, ATTRIBUTE3
        INTO v_Bank_Account_Name, v_Bank_Account_Number
        FROM FND_LOOKUP_VALUES_VL
       WHERE SYSDATE BETWEEN NVL(START_DATE_ACTIVE, SYSDATE) AND
             NVL(END_DATE_ACTIVE, SYSDATE)
         AND ENABLED_FLAG = 'Y'
         AND LOOKUP_TYPE = 'RESA_AR_BANK_LKP'
         AND ATTRIBUTE_CATEGORY = 'RESA_AR_BANK_LKP'
         AND ATTRIBUTE1 = p_Tran_Type
         AND DESCRIPTION = p_Store.ACCOUNT_NUMBER
         AND DECODE(ATTRIBUTE4,NULL,p_Store.ROLLUP_LEVEL_1,ATTRIBUTE4) = p_Store.ROLLUP_LEVEL_1;
         x_Bank_Account_Name := v_Bank_Account_Name;
         x_Bank_Account_Number := v_Bank_Account_Number;
        
        
     --   Print_log('Bank account name and account number found....' ,'Y');
        
    EXCEPTION
       
      WHEN NO_DATA_FOUND THEN
     -- Print_log('Bank account name and account number not found....' ,'Y');
        x_error_msg := 'Bank Acct Name/Number not Found for ' ||
                       p_Store.ACCOUNT_NUMBER;
      WHEN TOO_MANY_ROWS THEN
     -- Print_log('Bank account name and account number not found....' ,'Y');
        x_error_msg := 'Multiple Bank Acct Name/Number Found for ' ||
                       p_Store.ACCOUNT_NUMBER;
      WHEN OTHERS THEN
     -- Print_log('Bank account name and account number not found....' ,'Y');
        x_error_msg := 'Exception while deriving Bank Acct Name/Number for ' ||
                       p_Store.ACCOUNT_NUMBER;
    END;
    END IF;
    NULL;
  END;
  FUNCTION ACCOUNT_SEG_STATUS(P_Seg_Value IN VARCHAR2,
                              P_Seg_Desc  IN VARCHAR2) RETURN VARCHAR2 IS
    V_Count NUMBER := 0;
  BEGIN
    SELECT COUNT(FFVV.Flex_Value)
      INTO V_Count
      FROM FND_FLEX_VALUES_VL FFVV, FND_FLEX_VALUE_SETS FFVS
     WHERE UPPER(FFVS.Flex_Value_Set_Name) = UPPER(P_Seg_Desc)
       AND FFVS.Flex_Value_Set_Id = FFVV.Flex_Value_Set_Id
       AND FFVV.Flex_Value = P_Seg_Value;
    IF V_Count = 1 THEN
      RETURN NULL;
    ELSE
      RETURN 'Invalid Value';
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN 'Invalid Value';
  END ACCOUNT_SEG_STATUS;
  PROCEDURE Insert_ReSA(p_source_name VARCHAR2, p_ret_code OUT NUMBER) AS
    CURSOR cur_ins IS
      SELECT /*tt.row_num, */
       tt.*
        FROM XXABRL_RESA_AR_INT tt
       WHERE CUST_REC_ACCT <> segment6
         --AND store=1601
         AND INTERFACE_FLAG IN ('V')
         AND ORG_ID <> 144 ;
    v_INTERFACE_LINE_CONTEXT      VARCHAR2(240) := 'ABRL_RESA';
    v_INTERFACE_LINE_ATTRIBUTE1   VARCHAR2(240);
    v_INTERFACE_LINE_ATTRIBUTE2   NUMBER        :=0;
    v_BATCH_SOURCE_NAME           VARCHAR2(240) := 'ReSA';
    v_SET_OF_BOOK_NAME            VARCHAR2(240) := Fnd_Profile.VALUE('GL_SET_OF_BKS_ID');
    v_LINE_TYPE                   VARCHAR2(240) := 'LINE';
    v_DESCRIPTION                 VARCHAR2(240) := NULL; --<< from 'Derive_Tran_Type'
    v_CURRENCY_CODE               VARCHAR2(240) := 'INR';
    v_AMOUNT                      VARCHAR2(240) :=0;
    v_TRANSACTION_TYPE            VARCHAR2(240) :=NULL;
    v_TRX_DATE                    DATE          :=NULL;
    v_GL_DATE                     DATE          :=NULL;
    v_ORIG_SYSTEM_BATCH_NAME      VARCHAR2(240) :=NULL;
    v_STORE_NAME                  VARCHAR2(240) :=NULL;
    v_ORIG_SYSTEM_BILL_ADDRESS_ID VARCHAR2(240) :=NULL;
    v_CONVERSION_TYPE             VARCHAR2(240) :='Corporate';
    v_CONVERSION_DATE             DATE          :=NULL;
    v_CONVERSION_RATE             VARCHAR2(240) :=NULL;
    v_TERM_ID                     VARCHAR2(240) :='IMMEDIATE';
    v_ATTRIBUTE_CATEGORY          VARCHAR2(240) :='ReSA RECEIVABLES INVOICE';
    v_ATTRIBUTE1_TAX              VARCHAR2(240) :=NULL;
    v_OPERATING_UNIT              VARCHAR2(240) :=NULL;
    v_ORG_ID                      VARCHAR2(240) :=NULL;  --suresh
    v_CREATED_BY                  VARCHAR2(240) :=Fnd_Profile.VALUE('USER_ID');
    v_CREATION_DATE               DATE          :=SYSDATE;
    v_LAST_UPDATED_BY             NUMBER        :=Fnd_Profile.VALUE('USER_ID');
    v_LAST_UPDATE_DATE            DATE          :=SYSDATE;
    v_AMOUNT_DR                   NUMBER        :=0;
    v_RECEIPT_NUMBER           VARCHAR2(30);
    v_TRX_TYPE                 VARCHAR2(30);
    --==========================================
    x_Nature      VARCHAR2(240);
    x_Tran_Type   VARCHAR2(240);
    x_Description VARCHAR2(240);
    x_error_msg   VARCHAR2(240);
    x_Bank_Account_Name   VARCHAR2(240);
    x_Bank_Account_Number VARCHAR2(240);
    x_bnk_err_msg         VARCHAR2(240);
    x_Tran_err_msg VARCHAR2(240);
    x_vc_Nature    VARCHAR2(240);
    v_Valid_Process              NUMBER :=0;
    v_Invalid_Process            NUMBER :=0;
    v_s_num                      NUMBER :=-1;
    v_Ins_Exception              EXCEPTION;
    v_Insert_Exception           EXCEPTION;
    v_error_msg                  VARCHAR2(2400);
  BEGIN
  
   SELECT COUNT(1) INTO v_Valid_Process
        FROM XXABRL_RESA_AR_INT tt
       WHERE CUST_REC_ACCT <> segment6 
         AND INTERFACE_FLAG IN ('V','P')
         AND ORG_ID <> 144;        
   SELECT COUNT(1) INTO v_Invalid_Process
        FROM XXABRL_RESA_AR_INT tt
       WHERE CUST_REC_ACCT = segment6 
         AND INTERFACE_FLAG IN ('V','P') 
         AND ORG_ID <> 144; 
   Print_log('Valid Records for ReSA Invoice/Receipts: '||v_Valid_Process,'Y');
   Print_log('In-Valid Records for ReSA Invoice/Receipts: '||v_Invalid_Process,'Y');
    
    IF v_Valid_Process <> v_Invalid_Process THEN
       RAISE v_Ins_Exception;
    END IF;
    
    Fnd_File.PUT_LINE(fnd_file.LOG, '### ReSA Data-Population Starts ###');
    v_INTERFACE_LINE_ATTRIBUTE2:=0;
    Print_log('inserting data','Y');  
    FOR v_cur_ins IN cur_ins
     LOOP
      Fnd_File.PUT_LINE(fnd_file.LOG, 'Inserting ('||v_cur_ins.S_NUM||')');
      BEGIN
        SELECT
        TO_CHAR(v_cur_ins.ACCOUNTING_DATE, 'DDMMYY') ||'S'||v_cur_ins.STORE -- << Need to take as date
        ,ROUND(v_cur_ins.AMOUNT_CR,2)
        ,ROUND(v_cur_ins.AMOUNT_DR,2)
        ,v_cur_ins.ACCOUNTING_DATE
        ,v_cur_ins.ACCOUNTING_DATE
        ,'ReSA'||TO_CHAR(v_cur_ins.ACCOUNTING_DATE,'DDMMYY')
        ,v_cur_ins.ACCOUNT_NUMBER
        ,v_cur_ins.ORG_CODE
        ,v_cur_ins.ORG_ID    ---suresh
        ,v_cur_ins.s_num
        INTO
        v_INTERFACE_LINE_ATTRIBUTE1
        ,v_AMOUNT
        ,v_AMOUNT_DR
        ,v_TRX_DATE
        ,v_GL_DATE
        ,v_ORIG_SYSTEM_BATCH_NAME
        ,v_STORE_NAME
        ,v_OPERATING_UNIT
        ,v_org_id   --suresh
        ,v_s_num
        FROM dual;
      EXCEPTION
        WHEN OTHERS THEN
        Print_log('Exception in Data-Selection from cursor: '||SQLERRM,'Y');
      END;
      Print_log('Roll-Up#1: '||v_cur_ins.ROLLUP_LEVEL_1,'Y');
      Print_log('Roll-Up#2: '||v_cur_ins.ROLLUP_LEVEL_2,'Y');
      Print_log('Amount CR: '||v_AMOUNT,'Y');
      Print_log('Amount DR: '||v_AMOUNT_DR,'Y');
     
      Derive_Tran_Type(p_Resa_cur    => v_cur_ins,
                       p_Nature      => x_Nature,
                       p_Tran_Type   => x_Tran_Type,
                       p_Description => x_Description,
                       p_error_msg   => x_Tran_err_msg);
                       
      v_TRANSACTION_TYPE := NULL; --Mapping
      Print_log(':Tran_Type: '||x_Tran_Type,'Y');
      Fnd_file.PUT_LINE(fnd_file.LOG,':Line Nature: '||x_Nature);
      Print_log(':Derive_Tran_Type Error Msg: '||x_Tran_err_msg,'Y');
      IF x_Nature = 'Transaction' AND (x_Tran_err_msg IS NULL) THEN
         --Fnd_File.PUT_LINE(fnd_file.LOG, 'As Transaction ('||v_cur_ins.S_NUM||')');
        --Insert invoice
        IF x_Tran_Type IS NOT NULL THEN
          BEGIN
            SELECT
            TYPE  INTO
            v_TRX_TYPE
            FROM ra_cust_trx_types_all tran, hr_operating_units ood
            WHERE
            UPPER(tran.NAME) = UPPER(x_Tran_Type)
            AND ood.organization_id = tran.org_id
            AND ood.short_code = v_cur_ins.ORG_CODE;
            IF v_TRX_TYPE = 'CM' THEN
               --v_AMOUNT := v_AMOUNT_DR * -1;
               IF v_AMOUNT >0 THEN
                  v_AMOUNT := v_AMOUNT * -1;               --08/12/2008
               ELSIF v_AMOUNT <0 THEN
                  v_AMOUNT := ABS(v_AMOUNT);
               END IF;
            END IF;
          EXCEPTION
          WHEN NO_DATA_FOUND THEN
               fnd_file.put_line(fnd_file.LOG,'Invalid Transaction Type  '||x_Tran_Type);
          WHEN TOO_MANY_ROWS THEN
               fnd_file.put_line(fnd_file.LOG,'Multiple definition for Transaction Type '||x_Tran_Type);
          WHEN OTHERS THEN
               fnd_file.put_line(fnd_file.LOG,'Exception while validating Transaction Type '||x_Tran_Type);
          END;
        END IF;
        v_INTERFACE_LINE_ATTRIBUTE2 := v_INTERFACE_LINE_ATTRIBUTE2+1;
        
      --   Print_log('type : '||x_Nature,'Y');
        
        BEGIN
         -- Print_log('Inserting Line','Y');
          INSERT INTO XXABRL_RESA_RA_INT_LINES_ALL
          (
            INTERFACE_LINE_CONTEXT,
            INTERFACE_LINE_ATTRIBUTE1,
            INTERFACE_LINE_ATTRIBUTE2,
            BATCH_SOURCE_NAME,
            SOB_ID,
            LINE_TYPE,
            DESCRIPTION,
            CURRENCY_CODE,
            AMOUNT,
            TRANSACTION_TYPE,
            TRX_DATE,
            GL_DATE,
            ORIG_SYSTEM_BATCH_NAME,
            CUSTOMER_NAME,
            ORIG_SYSTEM_BILL_ADDRESS_ID,
            CONVERSION_TYPE,
            CONVERSION_DATE,
            CONVERSION_RATE,
            TERM_ID,
            ATTRIBUTE_CATEGORY,
            ATTRIBUTE1,
            ORG_CODE,
            CREATED_BY,
            CREATION_DATE,
            LAST_UPDATED_BY,
            LAST_UPDATE_DATE,
            INTERFACED_FLAG,
            ORG_ID,   --suresh
            Error_Message --to carry Seq_Num
            )
            VALUES
            (
            v_INTERFACE_LINE_CONTEXT,
            v_INTERFACE_LINE_ATTRIBUTE1,
            v_INTERFACE_LINE_ATTRIBUTE2,
            v_BATCH_SOURCE_NAME,
            'ABRL LEDGER',
            v_LINE_TYPE,
            x_Description,
            v_CURRENCY_CODE,
            v_AMOUNT,
            x_Tran_Type,
            v_TRX_DATE,
            v_GL_DATE,
            v_ORIG_SYSTEM_BATCH_NAME,
            v_STORE_NAME,
            v_ORIG_SYSTEM_BILL_ADDRESS_ID,
            v_CONVERSION_TYPE,
            v_CONVERSION_DATE,
            v_CONVERSION_RATE,
            v_TERM_ID,
            NULL,--v_ATTRIBUTE_CATEGORY , --Tax
            NULL,--x_Description, --Tax
            v_OPERATING_UNIT,
            v_CREATED_BY,
            v_CREATION_DATE,
            v_LAST_UPDATED_BY,
            v_LAST_UPDATE_DATE,
            'N',
            v_org_id,  --suresh
            v_s_num
            );
            UPDATE XXABRL_RESA_AR_INT
            SET INTERFACE_FLAG = 'P'
            WHERE record_number = v_cur_ins.record_number;
            --commit;
            Print_log('Line Inserted','Y');
           EXCEPTION
           WHEN OTHERS THEN
                Fnd_File.PUT_LINE(fnd_file.LOG,'Exception in while inserting data for Invoice-Lines: '||SQLERRM);
                v_error_msg := v_error_msg || 'Error Raise while inserting data for ReSA';
                RAISE v_Insert_Exception;
           END;
        --Insert Distribution
        BEGIN
         -- Print_log('Inserting distribution','Y');
          INSERT INTO XXABRL_RESA_RA_INT_DIST_ALL
          (
          INTERFACE_DISTRIBUTION_ID,
          INTERFACE_LINE_ID,
          INTERFACE_LINE_CONTEXT,
          INTERFACE_LINE_ATTRIBUTE1,
          INTERFACE_LINE_ATTRIBUTE2,
          ACCOUNT_CLASS,
          AMOUNT,
          ORG_ID,
          DESCRIPTION,
          CODE_COMBINATION_ID,
          SEGMENT1,
          SEGMENT2,
          SEGMENT3,
          SEGMENT4,
          SEGMENT5,
          SEGMENT6,
          SEGMENT7,
          SEGMENT8,
          CREATED_BY,
          CREATION_DATE,
          LAST_UPDATED_BY,
          LAST_UPDATE_DATE,
          LAST_UPDATE_LOGIN,
          INTERFACED_FLAG,
          Error_Message --to carry Seq_Num
          )
          VALUES
          (
          NULL,
          NULL,
          v_INTERFACE_LINE_CONTEXT,
          v_INTERFACE_LINE_ATTRIBUTE1,
          v_INTERFACE_LINE_ATTRIBUTE2,
          'REV',
          v_AMOUNT,
          v_org_id, ---v_OPERATING_UNIT,  --suresh
          x_Description,
          NULL,--CCID
          v_cur_ins.segment1,
          v_cur_ins.segment2,
          v_cur_ins.segment3,
          v_cur_ins.segment4,
          v_cur_ins.segment5,
          v_cur_ins.segment6,
          v_cur_ins.segment7,
          v_cur_ins.segment8,
          v_CREATED_BY,
          v_CREATION_DATE,
          v_LAST_UPDATED_BY,
          v_LAST_UPDATE_DATE,
          NULL,
          'N',
          v_s_num
          );
          UPDATE XXABRL_RESA_AR_INT
          SET INTERFACE_FLAG = 'P'
          WHERE record_number = v_cur_ins.record_number;
          --commit;
          Print_log('Line-Dist Inserted','Y');
           EXCEPTION
           WHEN OTHERS THEN
                Fnd_File.PUT_LINE(fnd_file.LOG,'Exception in while inserting data for Distributions: '||SQLERRM);
                RAISE v_Insert_Exception;
           END;
      ELSIF x_Nature = 'Receipt' THEN
        Derive_Bank_Dtl(v_cur_ins,
                        x_Tran_Type,
                        x_Bank_Account_Name,
                        x_Bank_Account_Number,
                        x_description,
                        x_bnk_err_msg);
       
       Fnd_File.PUT_LINE(fnd_file.LOG,'Bank derived successfully shailesh '|| x_bnk_err_msg);
       
       
        IF x_bnk_err_msg IS NULL THEN
          --Insert receipt data
          --  Print_log('Inserting Receipts '||v_INTERFACE_LINE_ATTRIBUTE1||SUBSTR(x_description,1,19) ||' - bank dtl : '|| x_Bank_Account_Name
          --    || '  '|| x_Bank_Account_Number,'Y');
          BEGIN
            INSERT INTO XXABRL_RESA_AR_RECEIPT_INT
              (
              DATA_SOURCE,
              RECEIPT_NUMBER,
              OPERATING_UNIT,
              RECEIPT_DATE,
              DEPOSIT_DATE,
              GL_DATE,
              CURRENCY_CODE,
              EXCHANGE_RATE_TYPE,
              EXCHANGE_RATE,
              EXCHANGE_DATE,
              RECEIPT_AMOUNT,
              ER_CUSTOMER_NUMBER,
              ER_DC_CODE,
              COMMENTS,
              ER_TENDER_TYPE,
              SALES_TRANSACTION_NUMBER,
              OF_CUSTOMER_NUMBER,
              CUSTOMER_ID,
              CUSTOMER_SITE_ID,
              RECEIPT_METHOD,
              BANK_ACCOUNT_NAME,
              BANK_ACCOUNT_NUMBER,
              INVOICE_NUMBER,
              AMOUNT_APPLIED,
              ORG_NAME,
              CREATED_BY,
              CREATION_DATE,
              INTERFACED_FLAG,
              Error_Message --to carry Seq_Num
              )
              VALUES
              (
              v_INTERFACE_LINE_CONTEXT
--              ,v_INTERFACE_LINE_ATTRIBUTE1||substr(x_Tran_Type,1,19)
              ,v_INTERFACE_LINE_ATTRIBUTE1||SUBSTR(x_description,1,19)
              ,v_OPERATING_UNIT
              ,v_TRX_DATE
              ,v_TRX_DATE
              ,v_TRX_DATE
              ,'INR'
              ,NULL
              ,NULL
              ,NULL
              ,v_AMOUNT_DR     --v_AMOUNT
              ,v_cur_ins.ACCOUNT_NUMBER
              ,'NV_DC_CODE'
              ,'COMMENTS'
              ,'NV_TENDER_TYPE'
              ,v_INTERFACE_LINE_ATTRIBUTE1
              ,NULL
              ,NULL
              ,NULL
              ,x_Tran_Type
              ,x_Bank_Account_Name
              ,x_Bank_Account_Number
              ,NULL
              ,v_AMOUNT
              ,v_OPERATING_UNIT
              ,v_CREATED_BY
              ,v_CREATION_DATE
              ,'N',
              v_s_num
              );
              UPDATE XXABRL_RESA_AR_INT
              SET INTERFACE_FLAG = 'P'
              WHERE record_number = v_cur_ins.record_number;
              --commit;
              Print_log('Receipt Inserted','Y');
           EXCEPTION
           WHEN OTHERS THEN
                Fnd_File.PUT_LINE(fnd_file.LOG,'Exception in while inserting data for Receipts: '||SQLERRM);
                RAISE v_Insert_Exception;
           END;
          NULL;
        ELSIF x_bnk_err_msg IS NOT NULL THEN
            v_error_msg := v_error_msg || 'Error Raise while Bank-Deriving for ReSA ';
            RAISE v_Insert_Exception;
        END IF;
      ELSIF x_Tran_err_msg IS NOT NULL THEN
            v_error_msg := v_error_msg || 'Error Raise while Tran-Deriving for ReSA ';
            RAISE v_Insert_Exception;
      END IF;
      Fnd_File.PUT_LINE(fnd_file.LOG,'--------------------------------------------------------');
    END LOOP;
    COMMIT;
  EXCEPTION
  WHEN v_Ins_Exception THEN
       Fnd_File.PUT_LINE(fnd_file.LOG,'Count for Valid_Process / Invalid_Process Records not same');
       p_ret_code :=1;
  WHEN v_Insert_Exception THEN
       Fnd_File.PUT_LINE(fnd_file.LOG,'Error Raise while inserting data for ReSA' || sqlerrm );
       ROLLBACK;
       p_ret_code :=1;
  WHEN OTHERS THEN
       Fnd_File.PUT_LINE(fnd_file.LOG,'Exception in ReSA Insert: '||SQLERRM);
       p_ret_code :=1;
  END Insert_ReSA;
END XXABRL_RESA_AR_CONV_PKG; 
/

